﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.AspNet.FriendlyUrls;

public partial class authenticate : FrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {

		//return;
		try {

			
			
			var token = Request.Headers["cps-token"];
			var host = Request.Headers["cps-host"];

		//	ErrorLog.Write(HttpContext.Current, 0, "token>" + token + " , host>" + host);

			var result = new AuthenticationAction().Validate(token , host , Request.UserHostAddress);

	//		ErrorLog.Write(this.Context, 0, result.ToJson());

			JsonWriter.Write(result, this.Context, true);
			



		}catch(Exception ex) {
			ErrorLog.Write(this.Context , 0 , ex.ToString());
		}

	}
	
}