﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using CommonLib;

public partial class AuthenticationAction {
	public AuthenticationAction()
	{
	}

	public class SiteInfo {
		public string host;
		public string login_url;
		public string logout_url;
	}

	public List<SiteInfo> sites {
		get {
			var result = new List<SiteInfo>();
			
			var url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
			var domain = "compassion.or.kr";
			if(url.IndexOf("compassionko.org") > -1) {
				domain = "compassionko.org";
			} else if(url.IndexOf("compassionkr.com") > -1) {
				domain = "compassionkr.com";
			}

            //result.Add(new SiteInfo() { host = "test10.tekville.com", login_url = "http://training.nkcompassion.or.kr/?r=compassion&a=ssoLogin&token=", logout_url = "http://test10.tekville.com/?r=compassion&a=logout" });
            result.Add(new SiteInfo() { host = "training.nkcompassion.or.kr", login_url = "http://training.nkcompassion.or.kr/?r=compassion&a=ssoLogin&token=", logout_url = "http://training.nkcompassion.or.kr/?r=compassion&a=logout" });
            result.Add(new SiteInfo() { host = "m." + domain, login_url = "http://m."+ domain + "/sso/login/?token=", logout_url = "http://m."+ domain + "/sso/logout/" });
			result.Add(new SiteInfo() { host = "www." + domain, login_url = "http://www."+ domain + "/sso/login/?token=", logout_url = "http://www."+ domain + "/sso/logout/" });
            result.Add(new SiteInfo() { host = "local.www." + domain, login_url = "http://www.local." + domain + "/sso/login/?token=", logout_url = "http://www.local." + domain + "/sso/logout/" });

            return result;
		}
		
		/*
		
		*/
		/*
		
		*/
	}


	public Authentication.Result Validate( string token, string host, string ip) {
		
		var result = new Authentication.Result() {
			success = false
		};
		

		if(string.IsNullOrEmpty(token)) {
			result.errCode = Authentication.enumErrCode.EmptyToken;
			result.message = "유효하지 않은 token 정보 입니다.";
			return result;
		}

		// token 유효성 검증
		var values = token.Decrypt().Split('|');
		var uuid = values[0];
		var token_ip = values[1];
		var date = DateTime.Parse(values[2]);           // 만료일시
		var remainMins = date.Subtract(DateTime.Now).TotalMinutes;


		/*
		1. 허용된 cps-host 인지 
		*/
		if(!sites.Select(p=>p.host).Contains(host) && host != HttpContext.Current.Request.Url.Host) {
			
			result.errCode = Authentication.enumErrCode.UnauthorizedHost;
			result.message = "인증되지 않은 host 입니다.";
			return result;
		}

		/* 
		2. 만료시간이 지난 토큰 조회
		*/
		if(remainMins < 0) {
			
			result.errCode = Authentication.enumErrCode.ExpireToken;
			result.message = "기간 만료된 token 입니다.";
			return result;
		}

        /*
		3. IP가 일치하는지 
		*/
        //	ErrorLog.Write(HttpContext.Current ,0 , token_ip + " : " + ip);
        /*
		if(token_ip != ip) {
			
			result.errCode = Authentication.enumErrCode.InvalidIPAddress;
			result.message = "token 발급된 IP와 인증요청한 IP가 다릅니다.";
			return result;
		}
		*/

        /*
		3. 토큰에 해당하는 회원정보가 있는지
		*/
        using (AuthDataContext dao = new AuthDataContext())
        {
            //var data = dao.user_token.Where(p => p.ut_key == token).Take(1).ToList();
            //var entity = dao.user_token.FirstOrDefault(p => p.ut_key == token);
            var entity = www6.selectQFAuth<user_token>("ut_key", token);

            if (entity == null)
            {

                result.errCode = Authentication.enumErrCode.InvalidToken;
                result.message = "유효한 토큰이 아닙니다." + token;
                return result;
            }

            if (!entity.ut_active)
            {

                result.errCode = Authentication.enumErrCode.LogoutToken;
                result.message = "로그아웃된 토큰";
                return result;
            }

            var user = entity.ut_value.Decrypt().ToObject<FrontLoginSession.LoginUser>();

            result.data = user;
        }
		
		result.success = true;
		result.errCode = Authentication.enumErrCode.Success;
		result.message = "정상 token 입니다.";

		return result;
	}

}