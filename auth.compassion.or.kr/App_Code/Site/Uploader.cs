﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;
using NLog;
using System.Net;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Drawing;

public class Uploader
{
	public Uploader()
	{
	}

	public enum FileGroup {
		
		file_company,
	}

	public static string GetRoot(FileGroup group) {
		
		return GetRoot(group , null);
	}

	public static string GetRoot(FileGroup group, string arg) {
		switch (group) {
			default:
				return "";
			case FileGroup.file_company:
				return ConfigurationManager.AppSettings["file_company"];
			

		}
	}

	public class UploadResult {
		public bool success {
			get;
			set;
		}
		public string name {
			get;
			set;
		}
		public Int64 size {
			get;
			set;
		}
		public string msg {
			get;
			set;
		}
	}

	public UploadResult saveToFileServer(HttpContext context, string temp_file, string path, string fileName, string rename , string delete_filename ) {

		string domain = ConfigurationManager.AppSettings["domain_file"];
		if (domain == "") {
			domain = context.Request.Domain();
		}

		using (WebClient client = new WebClient()) {
			byte[] res = client.UploadFile(string.Format("{0}/common/handler/upload.common.ashx?fileDir={1}&rename={2}&fname={3}&dfile={4}", domain, path, rename, fileName , delete_filename), "post", temp_file );
			var resText = Encoding.UTF8.GetString(res);

			return JsonConvert.DeserializeObject<UploadResult>(resText);
		}

	}

	public UploadResult Upload(HttpContext context, bool rename, string path, string fileName, string temp_file , string delete_filename ) {

		UploadResult result = new UploadResult() {
			success = false
		};

		result = saveToFileServer(context, temp_file, path, fileName, rename ? "y" : "n" , delete_filename);

		File.Delete(temp_file);

		return result;
	}

	public UploadResult Upload(HttpContext context, bool rename, string path, HttpPostedFile file) {

		UploadResult result = null;

		if (file != null && file.ContentLength > 0) {

			string tempDir = ConfigurationManager.AppSettings["temp"];

			if (!Directory.Exists(context.Server.MapPath(tempDir))) {
				Directory.CreateDirectory(context.Server.MapPath(tempDir));
			}

			string fname = file.FileName.Split('\\')[file.FileName.Split('\\').Length - 1];
			string fileName = fname.GetUniqueName(context, tempDir);
			string temp_file = context.Server.MapPath(tempDir) + fileName;

			file.SaveAs(temp_file);

			result = this.Upload(context, rename , path ,fileName,  temp_file , "");
			
		}

		return result;
	}

	public UploadResult Upload(HttpContext context, bool rename, string path, string fileName, Image image , string delete_filename) {

		UploadResult result = null;
		
		string tempDir = ConfigurationManager.AppSettings["temp"];

		if (!Directory.Exists(context.Server.MapPath(tempDir))) {
			Directory.CreateDirectory(context.Server.MapPath(tempDir));
		}

		fileName = fileName.GetUniqueName(context, tempDir);
		string temp_file = context.Server.MapPath(tempDir) + fileName;

		image.Save(temp_file);
		
		result = this.Upload(context, rename, path, fileName, temp_file , delete_filename);

		return result;
	}

}