﻿$(function () {

    $page.init();
    //이름 초기화
    $("#user_name").keyup(function () {
        if ($("#user_name").val() != "") {
            $("[data-id=check_name]").hide();
        }
    });

    //생년월일 초기화
    $(".sel_type1").change(function () {
        if ($("#user_birth_yyyy").val() != "" && $("#user_birth_mm").val() != "" && $("#user_birth_dd").val() != "") {
            $("[data-id=birth_check]").hide();
        }
    });
    //휴대폰 초기화 

    $("[data-id=input_phone_no1]").keyup(function () {
        if ($("[data-id=input_phone_no1]").val() != "") {
            $("[data-id=msg_phone_cert]").hide();
        }
    });
    //이메일 초기화 

    $("#email").keyup(function () {
        if ($("#email").val() != "") {
            $("[data-id=msg_email1_cert]").hide();
        }
    });

    //이메일인증시 초기화 

    $("#email_2").keyup(function () {
        if ($("#email_2").val() != "") {
            $("[data-id=check_user_email]").hide();
        }
    });

    //보호자동의 초기화
    $("#agree_parent").click(function () {
        $("[data-id=parent_check]").hide();
    });

    //전화번호 초기화
    $("#user_phone").keyup(function () {
        if ($("#user_phone").val() != "") {
            $("[data-id=check_user_phone]").hide();
        }
    });

    //아이디 초기화

    $("[data-id=input_user_id]").keyup(function () {
        if ($("[data-id=input_user_id]").val() != "") {
            $("[data-id=msg_user_id]").hide();
        }
    });

    //비밀번호 초기화

    $("#user_pwd").keyup(function () {
        if ($("user_pwd").val() != "") {
            $("[data-id=msg_user_pwd]").hide();
        }
    });

    $("#re_user_pwd").keyup(function () {
        if ($("re_user_pwd").val() != "") {
            $("[data-id=msg_user_pwd]").hide();
        }
    });

    $("#user_birth_mm").change(function () {
        
        if ($("#user_birth_mm").val() == 02) {
            $('a[href="#30"]').hide();
            $('a[href="#31"]').hide();
        } else {
            $('a[href="#30"]').show();
            $('a[href="#31"]').show();
        }
        
    });

});


var $page = {

    timer: null,

    init: function () {
		
        // 회원아이디 체크 이벤트
        this.setUserIdEvent();

        // 생년월일값으로 14세 미만체크
        this.setBirthDateEvent();

        // 보호자 동의 
        this.setParentIdentificationEvent();

        // 본인인증-휴대폰 이벤트
        this.setIdentificationByPhoneEvent();

        // 본인인증-이메일 이벤트
        this.setIdentificationByEmailEvent();

        // 가입하기 버튼
        $("#btn_submit").click(function () {

            return $page.onSubmit();
        });

        $("#chk1").click(function () {
            if ($("#chk1").is(":checked")) {
                $(".no_phone").show();
                $(".have_phone").hide();
                $(".no_phone input").val("");
                $("[data-id=check_user_email").hide();
                $("[data-id=btn_email_confirm").hide();
            } else {
                $(".no_phone").hide();
                $(".have_phone").show();
                $(".have_phone input").val("");
                $("#cert_no").hide();
                $("[data-id=msg_phone_cert").hide();
            }
        });

        $("#agree_parent").click(function () {
            if ($("#agree_parent").is(":checked")) {
                $("[data-id=parent_check]").hide();
            }
        });

        $("button.open").click(function () {
            $(".tooltip").fadeIn();
            return false;
        });

        $("button.close").click(function () {
            $(".tooltip").fadeOut();
            return false;
        });

    },

    // 확인
    onSubmit: function () {
        //  이름
        if ($("#user_name").val() == "") {
            scrollTo($(".w980"))
            setTimeout(function () {
                $("#user_name").focus();
            }, 100);

            $("[data-id=check_name]").html("이름을 입력해 주세요").addClass("guide_comment2").show();
            return false;
        }
        // 생년월일
        if ($("#user_birth_yyyy").val() == "" || $("#user_birth_mm").val() == "" || $("#user_birth_dd").val() == "") {
            scrollTo($(".w980"))
            $("[data-id=birth_check]").html("생년월일(년)을 선택해주세요.").addClass("guide_comment2").show();

            return false;
        }

        if ($("#user_birth_mm").val() == "") {
            //$("#user_birth_mm").focus();
            scrollTo($(".w980"))
            $("[data-id=birth_check]").html("생년월일(월)을 선택해주세요.").addClass("guide_comment2").show();
            return false;
        }

        if ($("#user_birth_dd").val() == "") {
            //$("#user_birth_dd").focus();
            scrollTo($(".w980"))
            $("[data-id=birth_check]").html("생년월일(일)을 선택해주세요.").addClass("guide_comment2").show();
            return false;
        }

        // todo

        // 본인인증
        if ($("#user_identification_method").val() == "") {
            $("[data-id=check_cert]").html("본인확인을 해주세요.").addClass("guide_comment2").show();
            scrollTo($(".w980"))

            return false;
        }

        if ($("#user_identification_method").val() == "phone") {
            var check_email1 = emailValidation.check($("[data-id=input_email1]").val());
            if (!check_email1.result) {
                $("[data-id=msg_email1_cert]").html(check_email1.msg).show();
                scrollTo($(".w980"))
                return false;
            }

            $("#user_email").val($("[data-id=input_email1]").val());
        } else if ($("#user_identification_method").val() == "email") {

            if (!/^[0-9.]{9,12}$/.test($("#user_phone").val())) {
                
                $("[data-id=check_user_phone]").html("전화번호를 정확히 입력해주세요.").addClass("guide_comment2").show();
                $("#user_phone").focus();
                return false;
            }
        }

        // 14세미만 부모동의

        if ($("#user_class").val() == "14세미만" && $("#parent_cert").val() == "") {
            $("[data-id=parent_check]").html("14세미만 아동은 보호자동의가 필요합니다.").addClass("guide_comment2").show();
            scrollTo("#agree_receive");
            $("#agree_parent").focus();
            return false;
        }
        if ($("#user_class").val() == "14세미만" && !$("#agree_parent").prop("checked")) {
            $("[data-id=parent_check]").html("보호자동의를 체크해주세요").addClass("guide_comment2").show();
            scrollTo("#agree_receive");
            $("#agree_parent").focus();
            return false;
        }


        // 아이디
        // todo : 중복검사 , 아이디유효성 검사
        if ($("#user_id").val() == "") {
            $("[data-id=msg_user_id]").removeClass("guide_comment1");
            $("[data-id=msg_user_id]").html("6~12자 이내의 영문 소문자+숫자를 입력해주세요.").addClass("guide_comment2").show();
            $("[data-id=btn_check_id]").focus();

            return false;
        }
        var id_result = idValidation.check($("[data-id=input_user_id]"), 6, 12);
        
        if (!id_result.result) {
            $("[data-id=msg_user_id]").html(id_result.msg).removeClass("guide_comment1").addClass("guide_comment2").show();
            return false;
        }
        $("#user_id").val($("[data-id=input_user_id]").val());

        // 비번
        var pwd_result = passwordValidation.check($("#user_pwd"), $("#re_user_pwd"), 5, 15, $("[data-id=input_user_id]"));
        
        if (!pwd_result.result) {
            
            $("#user_pwd").focus();
            $("[data-id=msg_user_pwd]").html(pwd_result.msg).show();

            if (pwd_result.msg.indexOf("확인") == -1) {
                $("#user_pwd").focus();
            } else {
                $("#re_user_pwd").focus();
            }

            return false;
        }



        return true;
    },

    // 보호자동의
    setParentIdentificationEvent: function () {

        // 휴대폰 인증
        $("[data-id=btn_parent_identification_by_phone]").click(function () {
            cert_openPopup("phone");

            return false;
        });

        // 아이핀 인증
        $("[data-id=btn_parent_identification_by_ipin]").click(function () {
            cert_openPopup("ipin");
            return false;
        });

    },

    // 생년월일값으로 14세 미만체크
    setBirthDateEvent: function () {

        $("#user_birth_yyyy").change(function () {
            check();
        });

        $("#user_birth_mm").change(function () {
            check();
        });

        $("#user_birth_dd").change(function () {
            check();
        });

        var check = function () {
            var yyyy = $("#user_birth_yyyy").val();
            var mm = $("#user_birth_mm").val();
            var dd = $("#user_birth_dd").val();

            if (yyyy == "" || mm == "" || dd == "") {
                $("[data-id=pn_parent]").hide();
                return;
            }

            var yyyymmdd = yyyy + mm + dd;

            var today = getToday();
            var result = today - parseInt(yyyymmdd) - 140000;
            if (result < 0) { //if(result < 0) less than 14 
                //	console.log("14세미만");
                $("#user_class").val("14세미만");
                $("[data-id=pn_parent]").show();
            } else {
                //	console.log("14세이상");
                $("#user_class").val("14세이상");
                $("[data-id=pn_parent]").hide();
            }
        }

        var getToday = function () {
            now = new Date();
            year = now.getFullYear();
            month = now.getMonth() + 1;
            day = now.getDate();
            if (month < 10) month = "0" + month;
            if (day < 10) day = "0" + day;
            return year + '' + month + '' + day;
        }

    },

    // 보인인증-휴대폰 이벤트
    setIdentificationByPhoneEvent: function () {

        $("[data-id=input_phone_no1]").keyup(function () {
            $("[data-id=msg_phone_cert]").hide();
            $(".hide_phone_cert").show();
            $("#user_identification_method").val("");
        })

        $("[data-id=btn_identification_by_phone]").click(function () {

            $("[data-id=pn_identification_by_phone]").show();
            $("[data-id=pn_identification_by_email]").hide();

            return false;
        })

        // 인증요청
        $("[data-id=btn_phone_no1]").click(function () {

            var phone = $("[data-id=input_phone_no1]").val();
            var name = $("#user_name").val();
            if (name == "") {
                $("#user_name").focus();
                $("[data-id=check_name]").html("이름을 입력해 주세요").addClass("guide_comment2").show();
                return false;
            }
            var birth_yyyy = $("#user_birth_yyyy").val();
            var birth_mm = $("#user_birth_mm").val();
            var birth_dd = $("#user_birth_dd").val();
            if (birth_yyyy == "") {
                $("#user_birth_yyyy").focus();
                $("[data-id=birth_check]").html("생년월일(년)을 선택해주세요.").addClass("guide_comment2").show();
                return false;
            }
            if (birth_mm == "") {
                $("#user_birth_mm").focus();
                $("[data-id=birth_check]").html("생년월일(월)을 선택해주세요.").addClass("guide_comment2").show();
                return false;
            }
            if (birth_dd == "") {
                $("#user_birth_dd").focus();
                $("[data-id=birth_check]").html("생년월일(일)을 선택해주세요.").addClass("guide_comment2").show();
                return false;
            }
            var birth = birth_yyyy + "-" + birth_mm + "-" + birth_dd;

            if (!/^[0-9.]{10,11}$/.test(phone)) {
                $("[data-id=msg_phone_cert]").html("휴대폰번호를 정확히 입력해주세요.").addClass("guide_comment2").show();
                $("[data-id=input_phone_no1]").focus();
                return false;
            }

            $("#cert_no").show();

            $("#user_mobile").val(phone);

            if ($page.timer != null)
                window.clearTimeout($page.timer);

            // 연속클릭방지
            $page.timer = window.setTimeout(function () {

                $.get("/api/join.ashx", { t: "send-phone", c: phone, n: name, b: birth }, function (r) {

                    $page.timer = null;
                    if (r.success) {
                        $("[data-id=msg_phone_cert]").removeClass("guide_comment2")
                        $("[data-id=msg_phone_cert]").html("문자메세지를 발송하였습니다.").addClass("guide_comment1").show();
                    } else {
                        if (confirm(r.message)) {

                            if (r.action == "exist-online-id") {
                                location.href = "/login/"
                            } else if (r.action == "exist-offline-id") {
                                location.href = "/join/offline/"
                            }
                        } else {
                            return false;
                        }
                    }

                });

            }, 300);

            return false;
        });

        // 인증확인
        $("[data-id=btn_phone_confirm]").click(function () {

            var code = $("[data-id=input_phone_confirm]").val();

            $.get("/api/join.ashx", { t: "check-phone", c: code }, function (r) {

                $("[data-id=msg_phone_cert]").removeClass("guide_comment1").removeClass("guide_comment2").show();

                if (r.success) {
                    //2018-04-17 이종진 - 기존후원 웹간편가입으로 인해 추가
                    if (r.message == "exist-offline-id") {
                        alert("이미 후원회원으로 가입되어 있습니다.\r\n기존후원 간편가입 페이지로 이동합니다.");
                        location.href = "/join/offline/form";
                    } else {
                        $("#user_identification_method").val("phone");		// 인증이 완료되면 인증수단 저장
                        $("#cert_no , .hide_phone_cert").hide();
                        //$(".hide_phone_cert").hide();
                        $("[data-id=msg_phone_cert]").html("인증이 완료되었습니다");
                        $("[data-id=msg_phone_cert]").addClass("guide_comment1");
                        $("[data-id=check_cert]").hide();
                        $("#chk1").attr("disabled", true);
                    }
                } else {
                    $("[data-id=msg_phone_cert]").html("인증이 완료되지 않았습니다");
                    $("[data-id=msg_phone_cert]").addClass("guide_comment2");
                }

            });

            return false;
        });

    },

    // 보인인증-이메일 이벤트
    setIdentificationByEmailEvent: function () {
        $("[data-id=btn_identification_by_email]").click(function () {

            $("[data-id=pn_identification_by_phone]").hide();
            $("[data-id=pn_identification_by_email]").show();

            return false;
        });

        // 인증요청
        $("[data-id=btn_email2]").click(function () {

            var email = $("[data-id=input_email2]").val();
            var name = $("#user_name").val();
            if (name == "") {
                $("#user_name").focus();
                $("[data-id=check_name]").html("이름을 입력해 주세요").addClass("guide_comment2").show();
                return false;
            }
            var birth_yyyy = $("#user_birth_yyyy").val();
            var birth_mm = $("#user_birth_mm").val();
            var birth_dd = $("#user_birth_dd").val();
            if (birth_yyyy == "") {
                $("#user_birth_yyyy").focus();
                $("[data-id=birth_check]").html("생년월일(년)을 선택해주세요.").addClass("guide_comment2").show();
                return false;
            }
            if (birth_mm == "") {
                $("#user_birth_mm").focus();
                $("[data-id=birth_check]").html("생년월일(년)을 선택해주세요.").addClass("guide_comment2").show();
                return false;
            }
            if (birth_dd == "") {
                $("#user_birth_dd").focus();
                $("[data-id=birth_check]").html("생년월일(년)을 선택해주세요.").addClass("guide_comment2").show();
                return false;
            }
            var birth = birth_yyyy + "-" + birth_mm + "-" + birth_dd;


            var check_email2 = emailValidation.check($("[data-id=input_email2]").val());
            if (!check_email2.result) {
                $("[data-id=check_user_email]").html(check_email2.msg).addClass("guide_comment2").show();
                $("#email_2").focus();
                return false;
            }

            $("#user_email").val(email);

            if ($page.timer != null)
                window.clearTimeout($page.timer);

            // 연속클릭방지
            $page.timer = window.setTimeout(function () {

                $.get("/api/join.ashx", { t: "send-email", c: email, n: name, b: birth }, function (r) {

                    $page.timer = null;
                    if (r.success) {
                        $("[data-id=check_user_email]").removeClass("guide_comment2")
                        $("[data-id=check_user_email]").html("메일을 발송하였습니다.").addClass("guide_comment1").show();
                        $("[data-id=btn_email_confirm]").show();
                    } else {
                        if (confirm(r.message)) {

                            if (r.action == "exist-online-id") {
                                location.href = "/login/"
                            } else if (r.action == "exist-offline-id") {
                                location.href = "/join/offline/"
                            }

                        } else {
                            return false;
                        }
                    }

                });

            }, 1000);

            return false;
        });

        // 인증확인
        $("[data-id=btn_email_confirm]").click(function () {

            var email = $("#user_email").val();

            $.get("/api/join.ashx", { t: "check-email", c: email }, function (r) {

                $("[data-id=check_user_email]").removeClass("guide_comment1").removeClass("guide_comment2");

                if (r.success) {
                    //2018-04-17 이종진 - 기존후원 웹간편가입으로 인해 추가
                    if (r.message == "exist-offline-id") {
                        alert("이미 후원회원으로 가입되어 있습니다.\r\n기존후원 간편가입 페이지로 이동합니다.");
                        location.href = "/join/offline/form";
                    }
                    else {
                        $("#user_identification_method").val("email");		// 인증이 완료되면 인증수단 저장
                        $(".hide_mail_cert").hide();
                        $("[data-id=check_user_email]").html("인증이 완료되었습니다").addClass("guide_comment1").show();
                        $("[data-id=check_cert]").hide();
                        $("#chk1").attr("disabled", true);
                    }
                } else {
                    $("[data-id=check_user_email]").html("인증이 완료되지 않았습니다").addClass("guide_comment2").show();
                }

            });

            return false;
        });

    },

    // 회원 아이디 체크
    setUserIdEvent: function () {

        var input = $("input[data-id=input_user_id]");
        var msg = $("[data-id=msg_user_id]");
        //console.log(input.val().length);
		
        $("[data-id=input_user_id]").focusout(function () {
        
            $("[data-id=msg_user_id]").css("display", "none")
            msg.html("");
            check();
        });
               

        /*
        $("[data-id=btn_check_id").bind("click", function () {
            $("[data-id=msg_user_id]").css("display", "none")
            msg.html("");
            check();

        });
        */
        /*
        input.search({
            autoCompleteMinLength: 6,
            checkDefault: "no",
            empty: "",
            msg: "",
            onSearch: function (sender) {
                check();
                return false;
            },

            onAutoComplete: function (arg, sender) {
                check();
            },

            button: $("[data-id=btn_check_id]")
        });
        */

        var check = function () {

            $("#user_id").val("");

            
            var id_result = idValidation.check(input, 6, 12);
            $("[data-id=msg_user_id]").removeClass("guide_comment2");
            if (!id_result.result) {
                $("[data-id=msg_user_id]").addClass("guide_comment2").show();
                msg.html(id_result.msg);
                return;
            }


            var user_id = input.val();
            $.get("/api/join.ashx", { t: "exist-id", c: user_id }, function (r) {
                $("[data-id=msg_user_id]").removeClass("guide_comment2");
                if (r.success) {
                    if (r.data.exist) {
                        $("[data-id=msg_user_id]").addClass("guide_comment2").show();
                        msg.html("이미 사용중입니다.");
                    } else {
                        $("[data-id=msg_user_id]").addClass("guide_comment1").show();
                        $("#user_id").val(r.data.user_id);
                        msg.html("사용 가능한 아이디입니다.");
                    }

                }

            });

        }

    }

}

// phone 은 휴대폰인증인 경우만 있음
// method = ipin or phone

var cert_setCertResult = function (method, result, ci, di, name, birth, sex, phone) {
    if (result == "Y") {
        $("[data-id=msg_parent_identification]").addClass("guide_comment1").html("보호자 동의 인증이 완료되었습니다.").show();

        $("[data-id=parent_check]").hide();
        //$("[data-id=msg_parent_identification]").hide();

        $("#parent_cert").val((method == "ipin" ? "보호자 아이핀 인증" : "보호자 휴대폰 인증"));

        $(".hide_parent_cert").hide();
        $("#parent_name").val(name);
        $("#parent_juminId").val(birth.substring(2, 8) + "0000000");
        if (phone) $("#parent_mobile").val(phone);

    } else {
        $("[data-id=msg_parent_identification]").removeClass("guide_comment1");
        $("[data-id=msg_parent_identification]").addClass("guide_comment2").html("보호자 동의 인증에 실패했습니다.").show();
    }



}


