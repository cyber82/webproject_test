﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
public partial class join_company_default : FrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		


	}

    protected void btn_submit_Click( object sender, EventArgs e ) {

        if(base.IsRefresh) {
            return;
        }

        var userid = user_id.Value;
        var sponsor_name = user_name.Text;
        var mobile = user_mobile.Value;
        var phone = user_phone.Value;
        var email = user_email.Value;

        var parent_name = "";
        var parent_juminId = "";
        var parent_mobile = "";
        var parent_email = "";

        #region param
        var SponsorID = ""; //후원자ID
        var ConID = ""; //후원자영문ID
        var ConIDCheck = "";//후원자IDCheckDigit
        var SponsorName = sponsor_name;//후원자명
        var JuminID = ""; //주민/법인번호
        var ComRegistration = comp_register_no.Value;//사업자등록번호
        var PassportID = ""; //여권번호
        var FirstName = ""; //영문이름First
        var LastName = ""; //영문이름Last
        var LocationType = "국내"; //거주구분 : 국내,국외,미주

        var USConID = ""; //미주후원자ID
        var LanguageType = "한글"; //사용언어 : 한글,영어,일어
        var TranslationFlag = "N"; //번역여부플래그 : YN
        var Correspondence = ""; //영문서신구분
        var ReligionType = ""; //종교코드        
        var ChurchName = "";//교회명
        var ChannelType = CodeAction.ChannelType; //가입채널
        var MotiveSponsor = ""; //가입동기후원자
        var MotiveName = ""; //가입동기후원자명  
        var MotiveCode = "";

        var MotiveCodeName = "";
        var RecommendType = "";
        var CharacterType = ""; //상담속성(성격)
        DateTime? BirthDate = null; //생년월일
        var BirthDateClass = "";  //양력,음력구분(L,S)
        var SponsorshipLevel = "";  //후원등급
        var GenderCode = "C"; //성별
        var CurrentUse = "Y"; //사용여부Flag
        var CampaignID = ""; //캠페인ID
        var CampaignTitle = "";  //캠페인제목	  

        var Remark = "";  //비고
        var UserID = userid;  //LoginID
        var JobCode = "";  //직업
        var UserDate = DateTime.Now;//웹가입일
        var UserClass = "기업"; //후원자구분(14세이상,미만,국내외국인,해외내국인,기업)
        DateTime? CertifyDate = null; //승인일
        var CertifyOrgan = ""; //승인기관	
        var DI = ""; // 중복가입정보
        var CI = ""; // 중복가입정보
        var ParentName = parent_name; //14세미만일때 부모님정보
        var ParentJuminID = parent_juminId; //14세미만일때 부모님정보

        var ParentMobile = parent_mobile; //14세미만일때 부모님정보
        var ParentEmail = parent_email; //14세미만일때 부모님정보
        var UserPW = user_pwd.Text; //패스워드
        var TR = "";        // 어린이편지번역여부
        var ManagerName = manager_name.Text;
        var FilePath = file_path.Value;
        var RegisterNo = comp_register_no.Value;
        #endregion

        if(string.IsNullOrEmpty(UserID)) {
            base.AlertWithJavascript("아이디를 입력해주세요");
            return;
        }

        #region 1. 웹회원 등록

        using (AuthDataContext dao = new AuthDataContext())
        {

            var sponsorId = new SponsorAction().GetNewSponsorID();
            if (string.IsNullOrEmpty(sponsorId))
            {
                base.AlertWithJavascript("시스템 장애발생 잠시 후 다시 시도해주세요");
                return;
            }

            //var result = dao.sp_tSponsorMaster_insert_f(CodeAction.ChannelType, "", sponsorId, "", ConIDCheck, JuminID, FirstName, LastName, SponsorName, ComRegistration, null, BirthDateClass, GenderCode, CampaignID, CampaignTitle,
            //Remark, UserID, UserClass, CertifyDate, CertifyOrgan, ParentName, ParentJuminID, ParentMobile, UserPW, ManagerName, FilePath, user_identification_method.Value, mobile, email, phone,
            //user_group.Value, !agree_receive.Checked, !agree_receive.Checked, null, null, null).First();

            Object[] op1 = new Object[] { "ChannelType", "SponsorID", "tempSponsorID", "ConID", "ConIDCheck", "JuminID", "FirstName", "LastName", "SponsorName", "ComRegistration", "BirthDate", "BirthDateClass", "GenderCode", "CampaignID", "CampaignTitle", "Remark", "UserID", "UserClass", "CertifyDate", "CertifyOrgan", "ParentName", "ParentJuminID", "ParentMobile", "UserPW", "ManagerName", "FilePath", "AuthMethod", "Mobile", "Email", "Phone", "UserGroup", "AgreePhone", "AgreeEmail", "ZipCode", "Addr1", "Addr2" };
            Object[] op2 = new Object[] { CodeAction.ChannelType, "", sponsorId, "", ConIDCheck, JuminID, FirstName, LastName, SponsorName, ComRegistration, null, BirthDateClass, GenderCode, CampaignID, CampaignTitle, Remark, UserID, UserClass, CertifyDate, CertifyOrgan, ParentName, ParentJuminID, ParentMobile, UserPW, ManagerName, FilePath, user_identification_method.Value, mobile, email, phone, user_group.Value, !agree_receive.Checked, !agree_receive.Checked, null, null, null };
            var list = www6.selectSPAuth("sp_tSponsorMaster_insert_f", op1, op2).DataTableToList<sp_tSponsorMaster_insert_fResult>();
            var result = list.First();

            if (result.Result != 'Y')
            {     // 등록실패

                base.AlertWithJavascript(result.Result_String);
                return;

            }


        }
        #endregion

        if(Session["SPONSOR_RETURN_URL"] != null) {
            var returnUrl = Session["SPONSOR_RETURN_URL"].ToString();
            Session["SPONSOR_RETURN_URL"] = null;

            this.ViewState["userId"] = UserID;
            this.ViewState["userPwd"] = UserPW;
            this.ViewState["returnUrl"] = returnUrl;
            ph_ex_frm.Visible = true;
            return;

        }

        var dash_phone = "";
        var phone_type = "";

        if(mobile == "") {
            if(phone.Count() == 10) {
                dash_phone = string.Format("{0}-{1}-{2}", phone.Substring(0, 3), phone.Substring(3, 3), phone.Substring(6, 4));
                phone_type = "전화번호";
            } else if(phone.Count() == 11) {
                dash_phone = string.Format("{0}-{1}-{2}", phone.Substring(0, 3), phone.Substring(3, 4), phone.Substring(7, 4));
                phone_type = "전화번호";

            } else {
                dash_phone = "전화번호 형식이 잘못되었습니다.";
                phone_type = "전화번호";
            }

        } else {
            if(mobile.Count() == 10) {
                dash_phone = string.Format("{0}-{1}-{2}", mobile.Substring(0, 3), mobile.Substring(3, 3), mobile.Substring(6, 4));
                phone_type = "휴대폰";
            } else if(mobile.Count() == 11) {
                dash_phone = string.Format("{0}-{1}-{2}", mobile.Substring(0, 3), mobile.Substring(3, 4), mobile.Substring(7, 4));
                phone_type = "휴대폰";
            } else {
                dash_phone = "휴대폰번호 형식이 잘못되었습니다.";
                phone_type = "휴대폰";
            }
        }

        var dash_ComRegistration = string.Format("{0}-{1}-{2}", ComRegistration.Substring(0, 3), ComRegistration.Substring(3, 2), ComRegistration.Substring(5, 5));
        var args = new Dictionary<string, string> {
                    {"{userId}" , UserID},
                    {"{SponsorName}", SponsorName },
                    {"{ComRegistration}", dash_ComRegistration },
                    {"{ManagerName}", ManagerName },
                    {"{mobile}", dash_phone },
                    {"{email}", email },
                    {"{phone_type}", phone_type }

        };

        this.SendMail(email, args);
        Response.Redirect("complete");
	}

    void SendMail(string email, Dictionary<string, string> args ) {
        /*
        Email.Send(HttpContext.Current, Email.SystemSender, new List<string>() { email },
            "[한국컴패션] 회원가입을 축하드립니다.", " /mail/join.html",
            args, null);
        */
        Email.Send(HttpContext.Current, Email.SystemSender, new List<string>() { ConfigurationManager.AppSettings["emailCorpAdmin"] },
          "[한국컴패션] 기업/단체 회원가입 승인요청 메일입니다.", " /mail/approval.html", args, null);


    }
}