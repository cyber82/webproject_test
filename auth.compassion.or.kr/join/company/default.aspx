﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="join_company_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript" src="/assets/jquery/wisekit/validation/id.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/email.js"></script>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript" src="/join/company/default.js"></script>

    <script type="text/javascript">
        $(function () {
	        var uploader = attachUploader("btn_file_path");
	        uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_company)%>";
	        uploader._settings.data.rename = "y";
	        uploader._settings.data.limit = 2048;
        });

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" runat="server" id="user_id" value="" />
    <input type="hidden" runat="server" id="user_identification_method" value="" />
    <input type="hidden" runat="server" id="user_email" value="" />
    <input type="hidden" runat="server" id="user_phone" value="" />
    <input type="hidden" runat="server" id="user_mobile" value="" />

    <input type="hidden" runat="server" id="comp_register_no" value="" />
    <input type="hidden" runat="server" id="file_path" value="" />
    <input type="hidden" runat="server" id="user_group" value="" />
    

    <!-- sub body -->
    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>회원가입</h1>
                <span class="desc">한국컴패션 회원가입하시고, 더 많은 어린이 사랑을 만나 보세요</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents member">

            <div class="w980">
                <div class="tab_info">
                    <a href="/login/" class="login">로그인</a>
                    <a href="/find/pwd/" class="find_pw">비밀번호 찾기</a>

                    <span class="nec_info">표시는 필수입력 사항입니다.</span>
                </div>

                <h2 class="txt_hidden">기업 / 단체 회원</h2>
                <div class="member_type">
                    <a href="/join/"><span></span>개인회원 (간편가입)</a>
                    <%--<a href="/join/offline/"><span></span>기존 후원자 (웹 회원전환)</a>--%>
                    <a href="/join/company/" class="on"><span></span>기업 / 단체 회원</a>
                </div>

                <div class="tab_type3 group" >
                    <button class="btn_user_group on" data-placeholder="기업명" data-user-group="기업"><span >기업</span></button>
                    <button class="btn_user_group" data-placeholder="단체명" data-user-group="단체"><span>단체</span></button>
                    <button class="btn_user_group" data-placeholder="교회명" data-user-group="교회"><span>교회</span></button>
                    <!-- 클릭 시, button 태그에 클래스 "on" 추가 -->
                </div>

                <!-- 기업/단체정보 -->
                <div class="input_div">
                    <div class="login_field"><span>기업/<br />
                        단체정보</span></div>
                    <div class="login_input">

                        <table class="tbl_join">
                            <colgroup>
                                <col style="width: 400px" />
                                <col style="width: *" />
                            </colgroup>
                            <caption>기업/단체정보 입력 테이블</caption>
                            <tbody>
                                <tr>
                                    <td>
                                        <label for="name" class="hidden">기업명</label>
                                        <asp:TextBox runat="server" ID="user_name" class="input_type1" placeholder="기업명"></asp:TextBox>
                                        <span class="guide_comment2" data-id="check_company_name" style="display:none"></span>
                                        <!-- placeholder에 "기업명/단체명/교회명"이 선택적으로 보여짐 -->
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="company_num" class="hidden">사업자번호</label>
                                        <input type="text" id="company_num" data-id="comp_register_no" maxlength="10" class="input_type1 number_only" placeholder="사업자번호('-'없이 입력)" />
                                        <span class="guide_comment2" data-id="msg_comp_register_no" style="display:none"></span>
                                    </td>

                                    <td><span class="pos1">
                                        <button class="btn_type8" data-id="btn_check_comp_register_no">중복검사</button></span></td>
                                </tr>
                                    
                            </tbody>
                        </table>

                    </div>
                </div>
                <!--// -->

                <div class="input_div">
                    <div class="login_field"><span>담당자정보<br />
                        및 본인확인</span></div>
                    <div class="login_input">

                        <table class="tbl_join">
                            <colgroup>
                                <col style="width: 400px" />
                                <col style="width: *" />
                            </colgroup>
                            <caption>담당자정보 및 본인확인 입력 테이블</caption>
                            <tbody>
                                <tr>
                                    <td>
                                        <label for="manager_name" class="hidden">담당자이름</label>
                                        <asp:TextBox runat="server" ID="manager_name" class="input_type1" placeholder="담당자이름"></asp:TextBox>
                                        <span class="guide_comment2" data-id="check_manager_name" style="display:none"></span>
                                        <span class="guide_comment1" data-id="check_cert" style="display: none"></span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="checkbox_ui" id="phone_check">
                                            <input type="checkbox" class="css_checkbox" id="chk1" />
                                            <label for="chk1" class="css_label font1">휴대폰이 없는 경우</label>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                                <!-- 휴대폰이 있는 경우 -->
                                            
      
                                <tr class="have_phone">
                                    <td>
                                        <label for="phone" class="hidden hide_phone_cert">휴대폰번호</label>
                                         <input type="text" id="phone" data-id="input_phone_no1" class="input_type1 number_only" maxlength="11" placeholder="휴대폰 번호 (-없이 입력)" />
                                        <span class="guide_comment1" data-id="msg_phone_cert" style="display: none"></span>
                                    </td>
                                    <td><span class="pos1"><button class="btn_type7 hide_phone_cert" data-id="btn_phone_no1">인증</button></span></td>
                                </tr>
                                <tr class="have_phone" id="cert_no" style="display:none">
                                    <td>
                                        <label for="cert" class="hidden">인증번호</label>
                                        <input type="text" id="cert" data-id="input_phone_confirm" class="input_type1 number_only" placeholder="인증번호" />
                                    </td>
                                    <td><span class="pos1">
                                        <button class="btn_type7 hide_phone_cert" data-id="btn_phone_confirm">확인</button></span></td>
                                </tr>
                                <tr class="have_phone" >
                                    <td>
                                        <label for="email" class="hidden">이메일</label>
                                         <input type="text" id="email" data-id="input_email1" class="input_type1" placeholder="이메일" />
                                        <span class="guide_comment2" data-id="msg_email1_cert" style="display: none"></span>
                                    </td>
                                    <td></td>
                                </tr>
                                <!--// 휴대폰이 있는 경우 -->
                                <!-- 휴대폰이 없는 경우 -->
                                <tr class="no_phone" style="display:none">
                                    <td>
                                        <label for="email_2" class="hidden">이메일</label>
                                        <input type="text" id="email_2" data-id="input_email2" class="input_type1" placeholder="이메일" /><br />
                                        <span class="guide_comment1" data-id="check_user_email" style="display: none"></span>
                                        <p class="pt5 mb10 hide_mail_cert">
                                            <span class="s_con1">입력하신 이메일에서 인증코드를 클릭하셨으면 [확인]버튼을 클릭해주세요.</span>
                                        </p>
                                    </td>
                                    <td><span class="pos1 hide_mail_cert" >
                                        <button class="btn_type7 mr5" data-id="btn_email2">인증</button>
                                        <button class="btn_type7" data-id="btn_email_confirm">확인</button></span></td>
                                </tr>
                                <tr class="no_phone" style="display:none">
                                    <td>
                                        <label for="phone_2" class="hidden">전화번호</label>
                                        <input type="text" id="phone_2" data-id="input_phone_no2" class="input_type1 number_only" maxlength="11" placeholder="전화번호 (-없이 입력)" />
                                        <span class="guide_comment1" data-id="check_user_phone" style="display: none"></span>
                                    </td>
                                    <td></td>
                                </tr>
                                <!--// 휴대폰이 없는 경우 -->
                                <tr>
                                    <td></script>
										<span class="checkbox_ui">
                                            <input type="checkbox" class="css_checkbox" runat="server" id="agree_receive" />
                                            <label for="agree_receive" class="css_label font2">이메일/SMS를 수신하지 않겠습니다.</label>
                                        </span>
                                        <span class="info_guide">
                                            <button class="open">가이드보기</button>
                                            <!-- tooltip -->
                                            <span class="tooltip">
                                                <span class="tit">이메일/SMS 수신여부에 동의하신 분에게는</span><br />
                                                <ul>
                                                    <li>컴패션 후원과 관련한 필수 안내사항과, 매월 발행되는
														‘이메일 뉴스레터', 연 1회 발행되는 '오프라인 뉴스레터'
														의 PDF파일을 메일로 보내드립니다.
                                                    </li>
                                                    <li>휴대폰으로 이벤트 및 행사 알림 메시지를 보내드립니다.
														(일반 전화번호 제외, 휴대폰번호로만 보내드립니다.)
                                                    </li>
                                                </ul>
                                                <button class="close">닫기</button>
                                                <span class="arr"></span>
                                            </span>
                                            <!--// -->
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!--// -->

                <!-- id/pw -->
                <div class="input_div">
                    <div class="login_field"><span>ID / PW</span></div>
                    <div class="login_input">

                        <table class="tbl_join">
                            <colgroup>
                                <col style="width: 400px" />
                                <col style="width: *" />
                            </colgroup>
                            <caption>아이디/비밀번호 입력 테이블</caption>
                            <tbody>
                                <tr>
                                    <td>
                                        <label for="phone" class="hidden">아이디</label>
                                        <input type="text" data-id="input_user_id" class="input_type1" placeholder="아이디(6~12자 이내의 영문 소문자+숫자)" maxlength="12" />
                                        <span class="guide_comment2" data-id="msg_user_id" style="display:none"></span>
                                    </td>
                                    <td style="display:none"><span class="pos1">
                                        <button class="btn_type8" data-id="btn_check_id">중복검사</button></span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="user_pwd" class="hidden">비밀번호</label>
                                        <asp:TextBox runat="server" ID="user_pwd" TextMode="Password" MaxLength="15" class="input_type1" placeholder="비밀번호(띄어쓰기 없이 영문,숫자,특수문자 3가지 조합으로 5-15자)"></asp:TextBox>


                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="re_user_pwd" class="hidden">비밀번호 재확인</label>
                                        <asp:TextBox runat="server" ID="re_user_pwd" TextMode="Password" MaxLength="15" class="input_type1" placeholder="비밀번호 재확인"></asp:TextBox>
                                        <span class="guide_comment2" data-id="msg_user_pwd" style="display:none"></span>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!--// -->

                <!-- 첨부파일 -->
                <div class="input_div">
                    <div class="login_field no">
                        <label for="attach">첨부파일</label></div>
                    <div class="login_input">

                        <table class="tbl_join">
                            <caption>첨부파일 등록 테이블</caption>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="btn_attach clear2 relative">
                                             <input type="text" runat="server" data-id="file_path" value="" class="input_type1 fl mr10" style="width: 400px;" readonly="readonly"/>
                                            <a href="#" class="btn_type8 fl" id="btn_file_path"><span>파일선택</span></a>
                                        </div>
                                        <p class="pt5"><span class="s_con1">사업자등록증을 첨부해주세요.</span></p>
                                        <p><span class="s_con1">첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</span></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!--// -->

                <!-- 약관동의 -->
                <div class="input_div agreement">
                    <div class="login_field"><span>약관동의</span></div>
                    <div class="login_input">

                        <div class="box">
                            <p>이용약관을 확인하였으며,<br />
                                한국컴패션 서비스 이용을 위해 이용약관에 동의합니다.</p>
                            <a href="/etc/terms" target="_blank" class="btn_s_type2">약관 전문 보기</a>
                        </div>

                        <div class="box">
                            <p>개인정보 수집 및 이용에 대한 안내를 확인하였으며,<br />수집·이용에 동의합니다.</p>
                            <a href="/etc/privacy" target="_blank" class="btn_s_type2">개인정보 처리방침 전문 보기</a>
                        </div>

                    </div>
                    <p class="confirm">이용 약관 및 개인 정보 이용 안내를 확인하였으며, 위 내용에 동의합니다.</p>

                    <div class="tac"><asp:LinkButton runat="server" class="btn_type1" ID="btn_submit" OnClick="btn_submit_Click">동의하고 회원가입</asp:LinkButton></div>
                </div>
                <!--// -->

                <div class="contact"><span>회원가입이 어려우시거나 본인확인/인증이 안되시는 경우 한국컴패션으로 연락 주시기 바랍니다. <em>후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</em></span></div>
            </div>

        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>
    <!--// sub body -->

   
</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<asp:PlaceHolder runat="server" ID="ph_ex_frm" Visible="false">
	<form id="exfrm" method="post" action="/login/?action=REFER">
		<input type="hidden" id="userId" name="userId" value="<%:this.ViewState["userId"].ToString() %>" />
		<input type="hidden" id="userPwd" name="userPwd" value="<%:this.ViewState["userPwd"].ToString() %>"/>
		<input type="hidden" id="r" name="r" value="<%:this.ViewState["returnUrl"].ToString() %>"/>
	</form>

        <script type="text/javascript">
            $(function () {
                $("#exfrm").submit();
            })
        </script>

    </asp:PlaceHolder>
</asp:Content>
