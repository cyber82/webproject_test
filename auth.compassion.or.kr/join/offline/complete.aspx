﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="join_offline_complete" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript">
		$(function () {
		    var main_img_path = "/common/img/page/member/member_main_bg";
		    var random = Math.floor((Math.random() * 3) + 1);

		    $("#main_img").attr("style", "background: url('" + main_img_path + random + ".jpg') no-repeat center top;");

		});
		history.pushState("complete", "complete", "complete");

		window.onpopstate = function (event) {
		    location.href = "/login";
		};

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	

    
    <!-- sub body -->
	<section class="sub_body">

		<!-- s: sub contents -->
		<!---------- member_main_bg1 ~ 3 까지 랜덤으로 노출 ----------->
		<div class="subContents member done" id="main_img">
			<div class="w980">
				<div class="wrap">
					<div class="logo"><img src="/common/img/common/logo_1.png" alt="Compassion 로고" /></div>
					<div class="done_box">
						<h1 class="tit">회원가입 완료</h1>
						<p class="con1">한국컴패션 후원자님 <br />한국컴패션 웹사이트 회원으로 가입이 완료되었습니다.<br />감사합니다.</p>
                        <a runat="server" id="btn_main" class="btn_b_type4 mb30">메인 페이지로 이동</a>
						<div class="con2">
							후원자님!<br />
							기존의 한국컴패션 후원내역도 확인하고 나의<br />
							후원 어린이에게 편지쓰기, 기부금영수증 출력<br />
							등 다양한 서비스를 이용해보세요.
							<a href="#"  runat="server" id="btn_sponsor"  class="btn_b_type1">기존 후원내역 확인</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--// e: sub contents -->

    </section>
    <!--// sub body -->

	

</asp:Content>
