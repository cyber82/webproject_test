﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="form.aspx.cs" Inherits="join_offline_form" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/id.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/email.js"></script>
    <script type="text/javascript" src="/join/offline/form.js"></script>

    <script type="text/javascript">
        $(function () {


        });

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" runat="server" id="user_id" value="" />



    <!-- sub body -->
    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>회원가입</h1>
                <span class="desc">한국컴패션 회원가입하시고, 더 많은 어린이 사랑을 만나 보세요</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents member">

            <div class="w980">
                <div class="tab_info">
                    <a href="/login/" class="login">로그인</a>
                    <a href="/find/pwd/" class="find_pw">비밀번호 찾기</a>

                    <span class="nec_info">표시는 필수입력 사항입니다.</span>
                </div>

                <h2 class="txt_hidden">기존 후원자 (웹 회원전환)</h2>
                <div class="member_type">
                    <a href="/join/" class="on"><span></span>개인회원 (간편가입)</a>
                    <%--<a href="/join/offline/" class="on"><span></span>기존 후원자 (웹 회원전환)</a>--%>
                    <a href="/join/company/"><span></span>기업 / 단체 회원</a>
                </div>

                <!-- 기본정보/본인확인 -->
                <div class="input_div">
                    <div class="login_field"><span>기본정보/<br />
                        본인확인</span></div>
                    <div class="login_input">

                        <table class="tbl_join">
                            <colgroup>
                                <col style="width: 400px" />
                                <col style="width: *" />
                            </colgroup>
                            <caption>기본정보/본인확인 입력 테이블</caption>
                            <tbody>
                                <tr>
                                    <td>
                                        <label for="user_name" class="hidden">이름</label>
                                        <asp:TextBox runat="server" ID="user_name" class="input_type1" placeholder="이름"></asp:TextBox>
                                        <span class="guide_comment1" data-id="check_name" style="display: none"></span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="user_birth_yyyy" class="hidden">생년월일(년)</label>
                                        <span class="sel_type1 fl mr10" style="width: 180px">
                                            <asp:DropDownList runat="server" ID="user_birth_yyyy" class="custom_sel" name=""></asp:DropDownList>
                                        </span>
                                        <label for="user_birth_mm" class="hidden">생년월일(월)</label>
                                        <span class="sel_type1 fl mr10" style="width: 100px">
                                            <asp:DropDownList runat="server" ID="user_birth_mm" class="custom_sel" name=""></asp:DropDownList>
                                        </span>
                                        <label for="user_birth_dd" class="hidden">생년월일(일)</label>
                                        <span class="sel_type1 fl" style="width: 100px">
                                            <asp:DropDownList runat="server" ID="user_birth_dd" class="custom_sel" name=""></asp:DropDownList>

                                        </span>
                                        <span class="guide_comment2" data-id="birth_check" style="display: none"></span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="user_phone" class="hidden">휴대폰번호</label>
                                        <asp:TextBox runat="server" data-id="input_phone_no1" ID="user_phone" MaxLength="11" class="input_type1 number_only" placeholder="휴대폰번호 (-없이 입력)"></asp:TextBox>
                                        <span class="guide_comment1" data-id="msg_phone_cert" style="display: none"></span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="user_email" class="hidden">이메일</label>
                                        <asp:TextBox runat="server" ID="user_email" data-id="input_email" MaxLength="100" class="input_type1" placeholder="이메일"></asp:TextBox>
                                        <span class="guide_comment1" data-id="check_user_email" style="display: none"></span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>

                                        <span class="checkbox_ui">
                                            <input type="checkbox" class="css_checkbox" runat="server" id="agree_receive" />
                                            <label for="agree_receive" class="css_label font2">이메일/SMS를 수신하지 않겠습니다.</label>
                                        </span>
                                        <span class="info_guide">
                                            <button class="open">가이드보기</button>
                                            <!-- tooltip -->
                                            <span class="tooltip">
                                                <span class="tit">이메일/SMS 수신여부에 동의하신 분에게는</span><br />
                                                <ul>
                                                    <li>컴패션 후원과 관련한 필수 안내사항과, 매월 발행되는
														‘이메일 뉴스레터', 연 1회 발행되는 '오프라인 뉴스레터'
														의 PDF파일을 메일로 보내드립니다.
                                                    </li>
                                                    <li>휴대폰으로 이벤트 및 행사 알림 메시지를 보내드립니다.
														(일반 전화번호 제외, 휴대폰번호로만 보내드립니다.)
                                                    </li>
                                                </ul>
                                                <button class="close">닫기</button>
                                                <span class="arr"></span>
                                            </span>
                                            <!--// -->
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!--// -->

                <!-- id/pw -->
                <div class="input_div">
                    <div class="login_field"><span>ID / PW</span></div>
                    <div class="login_input">

                        <table class="tbl_join">
                            <colgroup>
                                <col style="width: 400px" />
                                <col style="width: *" />
                            </colgroup>
                            <caption>아이디/비밀번호 입력 테이블</caption>
                            <tbody>
                                <tr>
                                    <td>
                                        <label for="input_user_id" class="hidden">아이디</label>
                                        <input type="text" id="input_user_id" data-id="input_user_id" maxlength="12" class="input_type1" placeholder="아이디 (6~12자 이내의 영문 소문자 + 숫자)" />
                                        <span class="guide_comment2" data-id="msg_user_id" style="display:none"></span>
                                    </td>
                                    <td style="display:none"><span class="pos1">
                                        <button class="btn_type8" data-id="btn_check_id">중복검사</button></span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="user_pwd" class="hidden">비밀번호</label>
                                        <asp:TextBox runat="server" ID="user_pwd" TextMode="Password" MaxLength="15" class="input_type1" placeholder="비밀번호(띄어쓰기 없이 영문,숫자,특수문자 3가지 조합으로 5-15자)"></asp:TextBox>


                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="re_user_pwd" class="hidden">비밀번호 재확인</label>
                                        <asp:TextBox runat="server" ID="re_user_pwd" TextMode="Password" MaxLength="15" class="input_type1" placeholder="비밀번호 재확인"></asp:TextBox>
                                        <span class="guide_comment2" data-id="msg_user_pwd" style="display:none"></span>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!--// -->

                <!-- 약관동의 -->
                <div class="input_div agreement">
                    <div class="login_field"><span>약관동의</span></div>
                    <div class="login_input">

                        <div class="box">
                            <p>이용약관을 확인하였으며,<br />
                                한국컴패션 서비스 이용을 위해 이용약관에 동의합니다.</p>
                            <a href="/etc/terms" target="_blank" class="btn_s_type2">약관 전문 보기</a>
                        </div>

                        <div class="box">
                            <p>개인정보 수집 및 이용에 대한 안내를 확인하였으며,<br />
                                한국컴패션 서비스 이용을 위해 이용약관에 동의합니다.</p>
                            <a href="/etc/privacy" target="_blank" class="btn_s_type2">약관 전문 보기</a>
                        </div>

                    </div>
                    <p class="confirm">이용 약관 및 개인 정보 이용 안내를 확인하였으며, 위 내용에 동의합니다.</p>

                    <div class="tac">
                        <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="btn_type1">동의하고 회원가입</asp:LinkButton></a></div>
                </div>
                <!--// -->

                <div class="contact"><span>회원가입이 어려우시거나 본인확인/인증이 안되시는 경우 한국컴패션으로 연락 주시기 바랍니다. <em>후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</em></span></div>
            </div>

        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>
    <!--// sub body -->
</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<asp:PlaceHolder runat="server" ID="ph_ex_frm" Visible="false">
	<form id="exfrm" method="post" action="/login/?action=REFER">
		<input type="hidden" id="userId" name="userId" value="<%:this.ViewState["userId"].ToString() %>" />
		<input type="hidden" id="userPwd" name="userPwd" value="<%:this.ViewState["userPwd"].ToString() %>"/>
		<input type="hidden" id="r" name="r" value="<%:this.ViewState["returnUrl"].ToString() %>"/>
	</form>

        <script type="text/javascript">
            $(function () {
                $("#exfrm").submit();
            })
        </script>

    </asp:PlaceHolder>
</asp:Content>
