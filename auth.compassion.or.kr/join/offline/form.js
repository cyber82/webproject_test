﻿$(function () {

    $page.init();

    //이름 초기화
    $("#user_name").keyup(function () {
        if ($("#user_name").val() != "") {
            $("[data-id=check_name]").hide();
        }
    });

    //생년월일 초기화
    $(".sel_type1").change(function () {
        if ($("#user_birth_yyyy").val() != "" && $("#user_birth_mm").val() != "" && $("#user_birth_dd").val() != "") {
            $("[data-id=birth_check]").hide();
        }
    });

    //휴대폰 초기화 

    $("#user_phone").keyup(function () {
        if ($("#user_phone").val() != "") {
            $("[data-id=msg_phone_cert]").hide();
        }
    });

   
    //이메일 초기화 

    $("#user_email").keyup(function () {
        if ($("#user_email").val() != "") {
            $("[data-id=check_user_email]").hide();
        }
    });
    //아이디 초기화

    $("[data-id=input_user_id]").keyup(function () {
        if ($("[data-id=input_user_id]").val() != "") {
            $("[data-id=msg_user_id]").hide();
        }
    });

    //비밀번호 초기화

    $("#user_pwd").keyup(function () {
        if ($("user_pwd").val() != "") {
            $("[data-id=msg_user_pwd]").hide();
        }
    });

    $("#re_user_pwd").keyup(function () {
        if ($("re_user_pwd").val() != "") {
            $("[data-id=msg_user_pwd]").hide();
        }
    });

    $("#user_birth_mm").change(function () {
        console.log($("#user_birth_mm").val())
        console.log($("#user_birth_mm").val() == 02)
        if ($("#user_birth_mm").val() == 02) {
            $('a[href="#30"]').hide();
            $('a[href="#31"]').hide();
        } else {
            $('a[href="#30"]').show();
            $('a[href="#31"]').show();
        }

    });

});


var $page = {

	timer: null,

	init: function () {

		// 회원아이디 체크 이벤트
		this.setUserIdEvent();
	
		// 가입하기 버튼
		$("#btn_submit").click(function () {

			return $page.onSubmit();

		});

		$("button.open").click(function () {
		    $(".tooltip").fadeIn();
		    return false;
		});
		$("button.close").click(function () {
		    $(".tooltip").fadeOut();
		    return false;
		});
		
	},

	// 확인
	onSubmit: function () {
	    // 이름 , 전화번호 , 이메일
	    var phone = $("[data-id=input_phone_no1]").val();
	    var name = $("#user_name").val();
	    var email = $("#user_email").val()

	    if ($("#user_name").val() == "") {
	        $("#user_name").focus();
	        $("[data-id=check_name]").html("이름을 입력해 주세요").addClass("guide_comment2").show();
	        return false;
	    }

	    // 생년월일
	    if ($("#user_birth_yyyy").val() == "" || $("#user_birth_mm").val() == "" || $("#user_birth_dd").val() == "") {
	        $("#user_birth_yyyy").focus();
	        $("[data-id=birth_check]").html("생년월일(년)을 선택해주세요.").addClass("guide_comment2").show();
	        return false;
	    }

	    if ($("#user_birth_mm").val() == "") {
	        $("#user_birth_mm").focus();
	        $("[data-id=birth_check]").html("생년월일(월)을 선택해주세요.").addClass("guide_comment2").show();
	        return false;
	    }

	    if ($("#user_birth_dd").val() == "") {
	        $("#user_birth_dd").focus();
	        $("[data-id=birth_check]").html("생년월일(일)을 선택해주세요.").addClass("guide_comment2").show();
	        return false;
	    }

	    if ($("#user_phone").val() == "") {
	        $("[data-id=msg_phone_cert]").html("휴대폰번호를 입력해주세요.").addClass("guide_comment2").show();
	        $("[data-id=input_phone_no1]").focus();
	        return false;
	    }

	    if (!/^[0-9.]{10,11}$/.test(phone)) {
	        $("[data-id=msg_phone_cert]").html("휴대폰번호를 정확히 입력해주세요.").addClass("guide_comment2").show();
	        $("[data-id=input_phone_no1]").focus();
	        return false;
	    }

	    if ($("#user_email").val() == "") {
	        $("[data-id=check_user_email]").html("이메일을 입력해주세요.").addClass("guide_comment2").show();
	        $("[data-id=input_email]").focus();
	        return false;
	    }

	    if (!/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(email)) {
	        $("[data-id=check_user_email]").html("이메일을 정확히 입력해주세요.").addClass("guide_comment2").show();
	        $("[data-id=input_email]").focus();
	        return false;
	    }



	
		// 아이디
		// todo : 중복검사 , 아이디유효성 검사
		if ($("#user_id").val() == "") {
		    $("[data-id=msg_user_id]").html("6~12자 이내의 영문 소문자+숫자를 입력해주세요.").addClass("guide_comment2").show();
			$("[data-id=btn_check_id]").focus();
			return false;
		}
		var id_result = idValidation.check($("[data-id=input_user_id]"), 6, 12);
		$("[data-id=msg_user_id]").removeClass("guide_comment2").show();
		if (!id_result.result) {
		    $("[data-id=msg_user_id]").html(id_result.msg).addClass("guide_comment2").show();
			return false;
		}
		$("#user_id").val($("[data-id=input_user_id]").val());

		// 비번
		var pwd_result = passwordValidation.check($("#user_pwd"), $("#re_user_pwd"), 5, 15, $("[data-id=input_user_id]"));
		if (!pwd_result.result) {
			$("#user_pwd").focus();
			$("[data-id=msg_user_pwd]").html(pwd_result.msg).addClass("guide_comment2").show();

			if (pwd_result.msg.indexOf("확인") == -1) {
			    $("#user_pwd").focus();
			} else {
			    $("#re_user_pwd").focus();
			}

			return false;
		}

		return true;
	},


	// 회원 아이디 체크
	setUserIdEvent: function () {

		var input = $("input[data-id=input_user_id]");
		var msg = $("[data-id=msg_user_id]");


		$("[data-id=input_user_id]").focusout(function () {

		    $("[data-id=msg_user_id]").css("display", "none")
		    msg.html("");
		    check();
		});

        /*
		$("[data-id=btn_check_id]").bind("click", function () {
            
		    $("[data-id=msg_user_id]").css("display", "none")
		    msg.html("");
		    check();
		});
        */
        /*
		input.search({
			autoCompleteMinLength: 6,
			checkDefault: "no",
			empty: "",
			msg: "",
			onSearch: function (sender) {
				check();
				return false;
			},

			onAutoComplete: function (arg, sender) {
				check();
			},

			button: $("[data-id=btn_check_id]")
		});
        */
		var check = function () {

			$("#user_id").val("");
            
			var id_result = idValidation.check(input, 6, 12);
			if (!id_result.result) {
				msg.html(id_result.msg);
				return;
			}
			var user_id = input.val();
			$.get("/api/join.ashx", { t: "exist-id", c: user_id }, function (r) {
				
			    if (r.success) {
			        $("[data-id=msg_user_id]").removeClass("guide_comment2")
				    if (r.data.exist) {
				        $("[data-id=msg_user_id]").addClass("guide_comment2").show();
						msg.html("이미 사용중입니다.");
				    } else {
				        $("[data-id=msg_user_id]").addClass("guide_comment1").show();
						$("#user_id").val(r.data.user_id);
						msg.html("사용 가능합니다.");
					}

				}

			});

		}

	}

}