﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="nomatch.aspx.cs" Inherits="join_offline_nomatch" %>

<div style="background: transparent; width: 500px;">

<div class="pop_type1 w500">
		<div class="pop_title">
			<span>안내</span>
			<button class="pop_close"><span><img src="/common/img/btn/close_1.png" ng-click="modal.close($event)" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content common_info no_match">
			<div class="con">
				<p>
					컴패션 후원자로 일치하는 정보가 없습니다.<br />
					일반 개인회원으로 가입됩니다.
				</p>
				<div class="tac mb50">
					<a href="/join/" class="btn_type1">회원가입 계속하기</a>
				</div>

				<div class="desc">
					<em class="em2">1) 최근 결연서를 통해 후원신청을 하셨나요?</em><br />
					후원자님의 후원신청이 등록 중에 있습니다. 먼저 웹회원으로 가입하시면<br />
					1~2주 안에 로그인 후 마이컴패션에서 후원내역을 확인하실 수 있습니다.<br /><br /><br />

					<em class="em2">2) 현재 후원 중이신데 일치하는 정보가 없다고 확인되시나요?</em><br />
					먼저 웹회원으로 가입하신 후 후원지원팀으로 연락주시면<br />
					후원정보를 등록해드리도록 하겠습니다.<br />
				</div>
			</div>
			
			<p class="info_txt">후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a></p>

		</div>
	</div>

</div>
