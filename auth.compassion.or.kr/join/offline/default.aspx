﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="join_offline_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/cert/cert.js"></script>
    <script type="text/javascript">
        $(function () {

            // 휴대폰 인증
            $("[data-id=btn_cert_by_phone]").click(function () {
                cert_openPopup("phone");

                return false;
            })

            // 아이핀 인증
            $("[data-id=btn_cert_by_ipin]").click(function () {
                cert_openPopup("ipin");

                return false;
            })

        });

        // 본인인증 결과 응답
        // result = Y or N , birth = yyyyMMdd
        // sex = M or F
        // method = ipin or phone
        var cert_setCertResult = function (method, result, ci, di, name, birth, sex) {
            if (result != "Y") {
                alert("인증에 실패했습니다.");
                return;
            }
            checkCI(method, ci);
        }

        var checkCI = function (method, ci) {
            console.log(ci);
        
            $.get("/api/join.ashx", { t: "find-offuser", c: ci }, function (r) {

                if (r.success) {
                    location.href = "form";
                } else {
                    if (r.action == "exist") {

                        if (confirm(r.message + "\n" + "아이디 : " + r.data.user_id)) {
                            location.href = "/login/";
                        } else {
                            return false;
                        }
                        
                    } else if (r.action == "notfound") {
                        $(".loading-container").css("display","block");
                        
                        //if (confirm("일치하는 회원정보가 없습니다. 간편회원으로 가입하시겠습니까?")) {
                          //  location.href = "/join/";
                        //}
                        
                    } else {
                        alert(r.message);
                    }
                }

            });
        
            return false;

        }


    </script>
    <!--
    <script type="text/javascript">
        (function () {

            var app = angular.module('cps.page', []);

            app.controller("defaultCtrl", function ($scope, $http, popup) {
                //  팝업
                $scope.modal = {
                    instance: null,
                    init: function () {
                        // 팝업

                        popup.init($scope, "/join/offline/nomatch", function (modal) {
                            $scope.modal.instance = modal;
                        });
                    },

                    show: function () {

                        $scope.modal.instance.show();


                    },

                    close: function ($event) {
                        $event.preventDefault();
                        if (!$scope.modal.instance)
                            return;
                        $scope.modal.instance.hide();

                    },
                }
                $scope.modal.init();

            });

        })();
    </script>
    -->
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    
    <!-- sub body -->
    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>회원가입</h1>
                <span class="desc">한국컴패션 회원가입하시고, 더 많은 어린이 사랑을 만나 보세요</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents member">

            <div class="w980">
                <div class="tab_info">
                    <a href="/login/" class="login">로그인</a>
                    <a href="/find/pwd/" class="find_pw">비밀번호 찾기</a>
                </div>
                <div>

                    <h2 class="txt_hidden">기존 후원자 (웹 회원전환) - 인증하기</h2>
                    <div class="member_type">
                        <a href="/join/"><span></span>개인회원 (간편가입)</a>
                        <a href="/join/offline/" class="on"><span></span>기존 후원자 (웹 회원전환)</a>
                        <a href="/join/company/"><span></span>기업 / 단체 회원</a>
                    </div>

                    <div class="existing">

                        <p class="comment1">
                            컴패션 후원자님!<br />
                            웹회원으로 전환하시면 후원내역관리 및 후원 어린이에게
                            <br />
                            편지쓰기, 기부금 영수증 출력 등의 서비스를 이용하실 수 있습니다.
                        </p>

                        <div class="cert clear2 mb50">
                            <!-- 휴대폰인증 -->
                            <div class="fl">
                                <div class="box phone">
                                    <p class="tit">휴대폰 인증</p>
                                    <p class="con">
                                        본인 명의의 휴대전화번호로 본인인증을<br />
                                        원하시는 분은 아래 버튼을 클릭해주세요
                                    </p>

                                    <a class="btn_s_type2" data-id="btn_cert_by_phone">휴대폰 인증하기</a>
                                </div>
                                <p class="s_con1">본인명의의 휴대폰으로만 본인인증이 가능합니다.</p>
                            </div>
                            <!--// -->

                            <!-- 아이핀인증 -->
                            <div class="fr">
                                <div class="box ipin">
                                    <p class="tit">아이핀(i-PIN) 인증</p>
                                    <p class="con">
                                        가상 주민등록번호 아이핀으로 본인인증을<br />
                                        원하시는 분은 아래 버튼을 클릭해주세요
                                    </p>

                                    <a class="btn_s_type2" data-id="btn_cert_by_ipin">아이핀 인증하기</a>
                                </div>
                                <p class="s_con1">아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다.</p>
                            </div>
                            <!--// -->
                        </div>

                        <div class="contact"><span>회원가입이 어려우시거나 본인확인/인증이 안되시는 경우 한국컴패션으로 연락 주시기 바랍니다. <em>후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</em></span></div>

                    </div>

                </div>

            </div>
            <!--// e: sub contents -->

            <div class="h100"></div>

            <!-- 팝업 -->
            <div class="loading-container" style="position: fixed; z-index: 100000; width: 100%; height: 100%; left: 0px; top: 0px; display: none; opacity: 1;">
                <div style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; opacity: 0.8; background-color: rgb(0, 0, 0);"></div>
                <div style="width: 100%; left: 35%; position: absolute; top: 118px; opacity: 1;">

                    <div style="background: transparent; width: 500px;">

                        <div class="pop_type1 w500">
                            <div class="pop_title">
                                <span>안내</span>
                                <button class="pop_close"><span>
                                    <img src="/common/img/btn/close_1.png" onclick="$('.loading-container').hide()" alt="팝업닫기"></span></button>
                            </div>

                            <div class="pop_content common_info no_match">
                                <div class="con">
                                    <p>
                                        컴패션 후원자로 일치하는 정보가 없습니다.<br>
                                        일반 개인회원으로 가입됩니다.
                                    </p>
                                    <div class="tac mb50">
                                        <a href="/join/" class="btn_type1">회원가입 계속하기</a>
                                    </div>

                                    <div class="desc" style="width:445px">
                                        <em class="em2">1) 최근 결연서를 통해 후원신청을 하셨나요?</em><br>
                                        후원자님의 후원신청이 등록 중에 있습니다. 먼저 웹회원으로 가입하시면<br>
                                        1~2주 안에 로그인 후 마이컴패션에서 후원내역을 확인하실 수 있습니다.<br>
                                        <br>
                                        <br>

                                        <em class="em2">2) 현재 후원 중이신데 일치하는 정보가 없다고 확인되시나요?</em><br>
                                        먼저 웹회원으로 가입하신 후 후원지원팀으로 연락주시면<br>
                                        후원정보를 등록해드리도록 하겠습니다.<br>
                                    </div>
                                </div>

                                <p class="info_txt">후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a></p>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- -->
    </section>

  
    <!--// sub body -->

</asp:Content>
