﻿<%@ WebHandler Language="C#" Class="api_join" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_join : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest(HttpContext context) {

		var t = context.Request["t"].EmptyIfNull();

		// 중복아이디 체크
		if(t == "exist-id") {
			this.CheckUserId(context);
		} else if(t == "exist-comp-register-no") {
			this.CheckCompRegisterNo(context);
			// 이메일 인증요청
		} else if(t == "send-email") {
			this.SendEmail(context);
			// 이메일 인증확인
		} else if(t == "check-email") {
			this.CheckEmail(context);
			// 휴대폰 인증요청
		} else if(t == "send-phone") {
			this.SendPhone(context);
			// 이메일 인증확인
		} else if(t == "check-phone") {
			this.CheckPhone(context);
			// 오프라인 계정정보 조회 CI & DI 사용
		} else if(t == "find-offuser") {
			this.FindOfflineUser(context);
		}


	}


	// 오프라인 계정정보 조회 CI & DI 사용
	// success : true = 컴파스에 CI & DI 일치 정보 있음 
	// has_account : true = WEB DB에 계정정보 있음 -> 로그인 유도로 이동(아이디 노출)
	void FindOfflineUser(HttpContext context ) {

		// 동일 도메인 www 에서 오는것만 허용 
		//	context.Response.AppendHeader("Access-Control-Allow-Origin", context.Request.Domain().Replace("https://auth" , "https://www"));
		var c = context.Request["c"].EmptyIfNull();     // ci
		new SponsorAction().FindOfflineUser(c).Write(context);
	}


	// success : true = 이미 가입됨
	void CheckCompRegisterNo(HttpContext context ) {

		var c = context.Request["c"].EmptyIfNull();     // ComRegistration
		var sName = context.Request["n"].EmptyIfNull();     // 기업/단체/교회명
		new JoinAction().CheckCompRegisterNo(c, sName).Write(context);

	}

	// 휴대폰 인증요청 메일 보내기
	// success : true = 문자 전송완료
	void SendPhone(HttpContext context ) {
		var n = context.Request["n"].EmptyIfNull();     // 이름
		var c = context.Request["c"].EmptyIfNull();     // phone
		var b = context.Request["b"].EmptyIfNull();     // 생년월일 yyyy-MM-dd

		new JoinAction().SendPhone(n, c, b).Write(context);
	}

	// success : true = 인증완료
	void CheckPhone(HttpContext context ) {

		var c = context.Request["c"].EmptyIfNull();

		new JoinAction().CheckPhone(c).Write(context);

	}

	// 이메일 인증요청 메일 보내기
	// success : true = 메일 전송완료
	void SendEmail(HttpContext context ) {
		var c = context.Request["c"].EmptyIfNull();     // email
		var n = context.Request["n"].EmptyIfNull();     // 이름
		var b = context.Request["b"].EmptyIfNull();     // 생년월일 yyyy-MM-dd
		new JoinAction().SendEmail(c, n, b).Write(context);
	}

	// success : true = 인증완료
	void CheckEmail(HttpContext context ) {

		var c = context.Request["c"].EmptyIfNull();
		new JoinAction().CheckEmail(c).Write(context);
	}

	// success : true = 이미 가입됨
	void CheckUserId(HttpContext context ) {

		var c = context.Request["c"].EmptyIfNull();
		new JoinAction().CheckUserId(c).Write(context);
	}


}