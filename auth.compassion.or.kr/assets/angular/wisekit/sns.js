(function () {
	var app = angular.module('wisekit.fn.sns', []);

	app.factory('facebook_share',['$window',function($window){
		return {
			init: function (fbId) {

				if(fbId){
					this.fbId = fbId;
					$window.fbAsyncInit = function() {
						FB.init({ 
							appId: fbId, 
							channelUrl: 'app/channel.html', 
							status: true, 
							xfbml: true 
						});
					};
					(function(d){
						var js,           
						id = 'facebook-jssdk', 
						ref = d.getElementsByTagName('script')[0];
						if (d.getElementById(id)) {
							return;
						}

						js = d.createElement('script'); 
						js.id = id; 
						js.async = true;
						js.src = "//connect.facebook.net/ko_KR/all.js";

						ref.parentNode.insertBefore(js, ref);

					}(document));

				}
				else{
					throw("FB App Id Cannot be blank");
				}
			}
		};
	}]);


	app.factory('kakao_share', ['$window', function ($window) {
		return {
			init: function (kakaoId) {

				if (kakaoId) {
					this.kakaoId = kakaoId;
					
					(function (d) {
						var js,
						id = 'kakao-jssdk',
						ref = d.getElementsByTagName('script')[0];
						if (d.getElementById(id)) {
							return;
						}

						js = d.createElement('script');
						js.id = id;
						js.async = false;
						js.src = "https://developers.kakao.com/sdk/js/kakao.min.js";

						ref.parentNode.insertBefore(js, ref);

					}(document));

					Kakao.init(kakaoId);	// kakao 
				}
				else {
					throw ("kakao App Id Cannot be blank");
				}
			}
		};
	}]);


	app.directive("wisekitShare", function (shareImpl) {
		return function (scope, element, attr) {

			if (!element.attr("data-link") || element.attr("data-link") == "") {
				element.attr("data-link", location.origin + location.pathname);
			}
			
			element.on('click', function (event) {
				var provider = element.attr("data-provider");
				
				if (provider == "facebook" ) {
					shareImpl.facebook(element);
				} else if (provider == "twitter" ) {
					shareImpl.twitter(element);
				} else if (provider == "kakaoStory") {
					shareImpl.kakaoStory(element);
				} else if (provider == "kakaoTalk") {
					shareImpl.kakaoTalk(element);
				}

				return false;

			});

		}
	})

})();


