﻿
var inputValidation = {
	msg: {
		ko: {
			numeric_comma: "숫자와 . 만 입력가능합니다.", 
			yyyymmdd_comma: "년(4자리).월(2자리).일(2자리) 포맷으로 입력해주세요",
			yyyymm_comma: "년(4자리).월(2자리) 포맷으로 입력해주세요",
			yyyymmdd_invalid: "유효하지 않은 날짜입니다.",
			date_future: "현재보다 과거 날짜입니다.",
			date_past: "현재보다 미래 날짜입니다.",
		},
		en: {
			numeric_comma: "You can enter only numbers or '.'",
			yyyymmdd_comma: "invalid yyyy.MM.dd foramt",
			yyyymm_comma: "invalid yyyy.MM foramt",
			yyyymmdd_invalid: "date provided is not valid, please enter a valid date.",
			date_future: "The date must come afterthe current date.",
			date_past: "The date must come before  the current date.",
		}
	},

	lang : "ko" , 
	init: function (lang) {
		if (lang) 
			this.lang = lang;

		this.initYyyymmComma();

		this.initYyyymmddComma();
	},
	
	initYyyymmComma: function () {
		var me = inputValidation;

		$(".yyyymm_comma").attr("MaxLength", "7");
		$(".yyyymm_comma").unbind("keyup").unbind("keydown").unbind("focusout");

		$(".yyyymm_comma").keyup(function () {

			var obj = $(this);
			var val = obj.val().match(/[0-9\.]+/g);
			if (obj.val() != val && obj.val() != "") {
				obj.addClass("alerted");
				alert(eval('inputValidation.msg.' + me.lang + '.numeric_comma'));
				obj.focus();
				return false;
			}

			if (obj.val().length == 4)
				obj.val(obj.val() + ".");

		}).keydown(function (e) {
			var obj = $(this);
			var target = obj.val().substr(obj.val().length - 1, 1);

			if (e.keyCode == 8) {	// del	
				if (target == ".") {
					obj.val(obj.val().substr(0, obj.val().length - 1));
				}
			} else if (e.keyCode == 190) {	// .
				if (target == ".")
					return false;
			}
		}).focusin(function(){
			var obj = $(this);
			obj.removeClass("alerted");

		}).focusout(function () {

			var obj = $(this);
			if (obj.hasClass("alerted")) return false;

			if (!obj.hasClass("required") || obj.val() == "")
				return false;

			if (!/^[0-9]{4}\.[0-9]{2}$/.test(obj.val())) {
				alert(eval('inputValidation.msg.' + me.lang + '.yyyymm_comma'));
				obj.val("");
				obj.focus();
				return false;
			}

			var s = obj.val().split('.');
			if (parseInt(s[1]) > 12) {
				alert(eval('inputValidation.msg.' + me.lang + '.yyyymmdd_invalid'));
				obj.val("");
				obj.focus();
				return false;
			}


		})


	} , 

	initYyyymmddComma: function () {
		var me = inputValidation;

		$(".yyyymmdd_comma").attr("MaxLength", "10");
		$(".yyyymmdd_comma").unbind("keyup").unbind("keydown").unbind("focusout");
		$(".yyyymmdd_comma").keyup(function () {

			var obj = $(this);
			var val = obj.val().match(/[0-9\.]+/g);
			if (obj.val() != val && obj.val() != "") {
				obj.addClass("alerted");
				alert(eval('inputValidation.msg.' + me.lang + '.numeric_comma'));
				obj.focus();
				return false;
			}

			if (obj.val().length == 4)
				obj.val(obj.val() + ".");
			else if (obj.val().length == 7)
				obj.val(obj.val() + ".");

		}).keydown(function (e) {
			var obj = $(this);
			var target = obj.val().substr(obj.val().length - 1, 1);

			if (e.keyCode == 8) {	// del	
				if (target == ".") {
					obj.val(obj.val().substr(0, obj.val().length - 1));
				}
			} else if (e.keyCode == 190) {	// .
				if (target == ".")
					return false;

			}
		
		}).focusin(function () {
			var obj = $(this);
			obj.removeClass("alerted");

		}).focusout(function () {

			var obj = $(this);

			if (obj.hasClass("alerted")) return false;

			if (!obj.hasClass("required") || obj.val() == "")
				return;

			if (!/^[0-9]{4}\.[0-9]{2}\.[0-9]{2}$/.test(obj.val())) {
				alert(eval('inputValidation.msg.' + me.lang + '.yyyymmdd_comma'));
				obj.val("");
				obj.focus();
				return;
			}

			var s = obj.val().split('.');
			var mmddyyyy = s[1] + "/" + s[2] + "/" + s[0];
			var d = Date.parse(mmddyyyy);
			var cur = new Date();
			if (isNaN(d) || parseInt(s[2]) > 31 || parseInt(s[1]) > 12) {
				alert(eval('inputValidation.msg.' + me.lang + '.yyyymmdd_invalid'));
				obj.val("");
				obj.focus();
				return;
			}

			var difference = (cur - d) / (86400000 * 7);
			if (obj.hasClass("future") && difference > 0) {
				alert(eval('inputValidation.msg.' + me.lang + '.date_future'));
				obj.val("");
				obj.focus();
				return;
			}
			
			if (obj.hasClass("past") && difference <= 1) {
				alert(eval('inputValidation.msg.' + me.lang + '.date_past'));
				obj.val("");
				obj.focus();
				return;
			}
			
		})
	}
}