﻿var emailValidation = {

	providers: {

		"직접입력": "free" , 
		"naver.com": "naver.com",
		"daum.net": "daum.net",
		"hanmail.net": "hanmail.net",
		"hotmail.net": "hotmail.net",
		"nate.com": "nate.com",
		"yahoo.co.kr": "yahoo.co.kr",
		"empas.com": "empas.com",
		"empal.com": "empal.com",
		"dreamwiz.com": "dreamwiz.com",
		"freechal.com": "freechal.com",
		"lycos.co.kr": "lycos.co.kr",
		"korea.com": "korea.com",
		"gmail.com": "gmail.com",
		"hanmir.com": "hanmir.com"
	},

	init: function ( email1, email2, email_provider) {
		console.log(email_provider);

		//email2.attr("readonly", "readonly");
		email_provider.bind("change", function () {
			email2.attr("readonly", "readonly");
			if ($(this).val() == "") {
				email2.val("");
			}else{
				if ($(this).val() == "free") {
					email2.removeAttr("readonly");
					email2.val("");
					email2.focus();
				}else{
					email2.val($(this).val());
				}
			}
		});

		var selected = false;
		$.each(eval("emailValidation.providers"), function (i, v) {
			var opt = $('<option value="' + v + '">' + i + '</option>');
			if (email2.val() == v) {
				selected = true;
				opt.attr("selected", "selected");
			}
			email_provider.append(opt);
		});

		// 직접입력의 경우
		if (email2.val() != "" && !selected) {
			email_provider.val("free").attr("selected", "selected");
			email2.removeAttr("readonly");
		}

	} , 

	checkWithAlert: function (obj1, obj2) {

		var val = obj2 ? obj1.val() + "@" + obj2.val() : obj1.val();
		var result = emailValidation.check(val);

		if (!result.result) {
			alert(result.msg);
			obj1.focus();
			return false;
		}

		return true;
	},


	check: function (val) {

		var exp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		
		if (val == "") {
			return { result: false, msg: "이메일은 공백없이 입력해주세요." };
		}

		if ((mval = val.match(exp))) {
			if (mval[0].length != val.length) {
				return { result: false, msg: "이메일주소가 올바르지 않습니다. \n다시 입력해주세요." };
			}
		} else {
			return { result: false, msg: "이메일주소가 올바르지 않습니다. \n다시 입력해주세요." };
		}

		return { result: true, msg: "" };
	}

}
