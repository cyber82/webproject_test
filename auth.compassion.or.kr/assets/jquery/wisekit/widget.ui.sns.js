﻿/*
사용법
<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.sns.js"></script>

0. el 정의
<a href="#"  data-role="sns" data-provider="tw" data-url="">트위터공유</a>
: data-url 이 없는 경우는 property="og:url" 의 값으로 대체

1. 생성
$("body").sns({
	kakaoKey: kakaoKey,
	facebookKey: facebookKey,
	onSuccess: function (provider, response) {
		if (provider == "ks" || provider == "fb") {
			alert("공유되었습니다.");
		}
		console.log("success", provider, response);
	}
});

2. UI변경으로 새로운 공유항목 추가시
var sns = $("body").data("ui-sns");
console.log(sns);
sns.reload();


*/
(function ($) {
	$.widget("ui.sns", {
		
		instance: null,
		options: {
			kakaoKey: '',
			facebookKey: '',
			onSuccess: function (provider , response) { }
		},

		reload: function () {
			var self = this.instance;

			self._setEvents(self);
		} , 

		_init: function () {

			this.instance = this;
			var self = this, o = this.options;
		
			$.getScript("https://developers.kakao.com/sdk/js/kakao.min.js", function (data, textStatus, jqxhr) {
				//console.log(data); // Data returned
			//	console.log(textStatus); // Success
			//	console.log(jqxhr.status); // 200

				Kakao.init(o.kakaoKey);	// kakao 

			});

			window.fbAsyncInit = function () {
				FB.init({
					appId: o.facebookKey,
					xfbml: true,
					version: 'v2.6'
				});
			};

			(function (d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) { return; }
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/ko_KR/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));


			self._setEvents(self);
		},

		_setEvents: function (self) {
			var o = self.options;
			
			var targets = self.element.find("[data-role='sns']");
			$.each(targets, function () {
				var provider = $(this).data("provider");
				switch (provider) {
					case "ks":
						self._bindKakaoStory(self , $(this));
						break;
					case "fb":
						self._bindFacebook(self, $(this));
						break;
					case "tw":
						self._bindTwitter(self, $(this));
						break;
					case "copy":		// copy to clipboard 
						self._bindClipboard(self, $(this));
						break;
				}
			})
		},

		_bindClipboard: function (self, el) {
			var o = self.options;
			
			el.unbind("click");
			el.click(function () {
				var url = el.data("url");
				if (!url)
					url = $("[property='og:url']").attr("content");
				window.prompt("Ctrl+C, Enter키를 차례로 입력하여 아래 주소를 복사해 주세요",url);
				
				return false;
			})
		},

		_bindTwitter: function (self, el) {
			var o = self.options;

			el.unbind("click");
			el.click(function () {
				var url = el.data("url");
				if (!url)
					url = $("[property='og:url']").attr("content");

				var title = el.data("title");
				if (!title)
					title = $("[property='og:title']").attr("content");

				var url = "https://twitter.com/intent/tweet?text=" + encodeURIComponent(title + ' ' + url);
				window.open(url, "_blank", "width=550,height=350,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no");

				if (o.onSuccess) {
					o.onSuccess("tw", null);
				}

				return false;
			})
		} ,

		_bindFacebook: function (self , el) {
			var o = self.options;

			el.unbind("click");
			el.click(function () {
				var url = el.data("url");
				if (!url) 
					url = $("[property='og:url']").attr("content");

				FB.ui({
					method: 'feed',
					link: url,
					//caption: $("#og_description").attr("content") ,
				}, function (response) {
					if (response) {
					
						if (o.onSuccess) {
							o.onSuccess("fb", response);
						}

					}
					
				});

				return false;
			})
		} , 

		_bindKakaoStory: function (self , el) {
			var o = self.options;

			el.unbind("click");
			el.click(function () {
				var url = el.data("url");
				if (!url)
					url = $("[property='og:url']").attr("content");

				Kakao.Auth.login({
					success: function () {
						// 로그인 성공시, API를 호출합니다.
						Kakao.API.request({
							url: '/v1/api/story/linkinfo',
							data: {
								url: url
							}
						}).then(function (res) {
							return Kakao.API.request({
								url: '/v1/api/story/post/link',
								data: {
									link_info: res
								}
							});
						}).then(function (res) {
							return Kakao.API.request({
								url: '/v1/api/story/mystory',
								data: { id: res.id }
							});
						}).then(function (res) {

							if (o.onSuccess) {
								o.onSuccess("ks", res);
							}

						}, function (err) {
							alert(JSON.stringify(err));
						});

					}
				});

				return false;

			});

		}

    });
}(jQuery));