
(function ($) {

	$.widget("ui.search", {

		_init: function () {
			var self = this, o = this.options;

			if (o.checkDefault) {
				o.checkDefault = o.checkDefault == "yes" ? true : false;
			} else {
				o.checkDefault = true;
			}

			o.autoCompleteMinLength = o.autoCompleteMinLength ? o.autoCompleteMinLength : 2;

			if (this.element.val() == "")
				this.element.val(o.empty);

			this.element.focus(function (e) {

				if ($(this).val() == o.empty) {
					$(this).val("");
				}
			});

			this.element.keyup(function (e) {
				clearTimeout($.data(self, "autoComplete"));

				var val = $(this).val();
				if (o.onAutoComplete && val.length >= o.autoCompleteMinLength) {
					$.data(self, "autoComplete", setTimeout(function () {
						o.onAutoComplete(val, self.element);
					}, 500));
				}

			});

			this.element.blur(function (e) {
				if ($(this).val() == "") {
					$(this).val(o.empty);
				}
			});
			this.element.keypress(function (e) {
				if (e.which == 13 && o.onSearch) {
					return self.search(self);

				}
			});

			if (o.button) {
				o.button.click(function () {
					return self.search(self);
				});
			}
		},



		search: function (self) {
			var o = self.options;
			if (self.element.val() == o.empty && o.checkDefault)
				self.element.val("");

			if (self.element.val() == "" && o.msg) {
				if (o.msg) {
					alert(o.msg);
				}
				self.element.focus();
				return false;
			}

			if (o.minimum_length && o.minimum_length.digit > 0) {
				if (self.element.val().length < o.minimum_length.digit && o.minimum_length.msg) {
					alert(o.minimum_length.msg.replace("#len", o.minimum_length.digit));
					self.element.focus();
					return false;
				}
			}

			if (o.onSearch)
				return o.onSearch(self.element);
			else
				return false;
		},
		setEmpty: function (msg) {
			var self = this, o = this.options;
			o.empty = msg;

			if (this.element.val() == "")
				this.element.val(o.empty);

		}
	});


})(jQuery);



