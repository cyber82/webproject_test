String.prototype.string = function (len) { var s = '', i = 0; while (i++ < len) { s += this; } return s; };
String.prototype.zf = function (len) { return "0".string(len - this.length) + this; };
Number.prototype.zf = function (len) { return this.toString().zf(len); };

String.prototype.endsWith = function (suffix) {
	return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

String.prototype.startsWith = function (str) {
	return this.indexOf(str) == 0;
};

String.prototype.contains = function (it) {
	return this.indexOf(it) != -1;
};

function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	} else {
		window.onload = function () {
			oldonload();
			func();
		}
	}
};

String.prototype.htmlEscape = function () {
	return this.replace(/&/g, '&amp;')
		.replace(/"/g, '&quot;')
		.replace(/'/g, '&#39;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;');
};

// 숫자 타입에서 쓸 수 있도록 format() 함수 추가
Number.prototype.format = function () {
	if (this == 0) return 0;

	var reg = /(^[+-]?\d+)(\d{3})/;
	var n = (this + '');

	while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');

	return n;
};

// 문자열 타입에서 쓸 수 있도록 format() 함수 추가
String.prototype.format = function () {
	var num = parseFloat(this);
	if (isNaN(num)) return "0";

	return num.format();
};

String.prototype.toDate = function () {

	var yyyy = this.substr(0, 4)
	var mm = this.substr(5, 2)
	var dd = this.substr(8, 2)

	if (this.length == 10) {
		return new Date((mm + "/" + dd + "/" + yyyy));
	} else {
		return new Date((mm + "/" + dd + "/" + yyyy + " " + this.substr(11, 5) + ":00"));
	}

};


String.prototype.toBase64 = function () {
	return Base64.encode(this);
};

var Base64 = { _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) { var t = ""; var n, r, i, s, o, u, a; var f = 0; e = Base64._utf8_encode(e); while (f < e.length) { n = e.charCodeAt(f++); r = e.charCodeAt(f++); i = e.charCodeAt(f++); s = n >> 2; o = (n & 3) << 4 | r >> 4; u = (r & 15) << 2 | i >> 6; a = i & 63; if (isNaN(r)) { u = a = 64 } else if (isNaN(i)) { a = 64 } t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a) } return t }, decode: function (e) { var t = ""; var n, r, i; var s, o, u, a; var f = 0; e = e.replace(/[^A-Za-z0-9\+\/\=]/g, ""); while (f < e.length) { s = this._keyStr.indexOf(e.charAt(f++)); o = this._keyStr.indexOf(e.charAt(f++)); u = this._keyStr.indexOf(e.charAt(f++)); a = this._keyStr.indexOf(e.charAt(f++)); n = s << 2 | o >> 4; r = (o & 15) << 4 | u >> 2; i = (u & 3) << 6 | a; t = t + String.fromCharCode(n); if (u != 64) { t = t + String.fromCharCode(r) } if (a != 64) { t = t + String.fromCharCode(i) } } t = Base64._utf8_decode(t); return t }, _utf8_encode: function (e) { e = e.replace(/\r\n/g, "\n"); var t = ""; for (var n = 0; n < e.length; n++) { var r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r) } else if (r > 127 && r < 2048) { t += String.fromCharCode(r >> 6 | 192); t += String.fromCharCode(r & 63 | 128) } else { t += String.fromCharCode(r >> 12 | 224); t += String.fromCharCode(r >> 6 & 63 | 128); t += String.fromCharCode(r & 63 | 128) } } return t }, _utf8_decode: function (e) { var t = ""; var n = 0; var r = c1 = c2 = 0; while (n < e.length) { r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r); n++ } else if (r > 191 && r < 224) { c2 = e.charCodeAt(n + 1); t += String.fromCharCode((r & 31) << 6 | c2 & 63); n += 2 } else { c2 = e.charCodeAt(n + 1); c3 = e.charCodeAt(n + 2); t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63); n += 3 } } return t } }

String.prototype.appendQuery = function (val) {
	var result = this;
	result += (result.indexOf("?") > -1 ? "&" : "?") + val;
	return result;

};

if (!Array.prototype.indexOf) {// Check And Fix a Array indexOf Error in ie7
	Array.prototype.indexOf = function (elt /*, from*/) {
		var len = this.length >>> 0;
		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
		: Math.floor(from);
		if (from < 0) {
			from += len;
		};
		for (; from < len; from++) {
			if (from in this && this[from] === elt) {
				return from;
			};
		};
		return -1;
	};
};

if (!Array.prototype.lastIndexOf) {
	Array.prototype.lastIndexOf = function (obj, start) {
		var index = -1;
		for (var i = (start || 0), j = this.length; i < j; i++) {
			if (this[i] === obj) { index = i; }
		}
		return index;
	}
};


// alert(versionCompare("1.1", "1.0.1", { zeroExtend  : true}));
var versionCompare = function (v1, v2, options) {
	var lexicographical = options && options.lexicographical,
		zeroExtend = options && options.zeroExtend,
		v1parts = v1.split('.'),
		v2parts = v2.split('.');

	function isValidPart(x) {
		return (lexicographical ? /^\d+[A-Za-z]*$/ : /^\d+$/).test(x);
	}

	if (!v1parts.every(isValidPart) || !v2parts.every(isValidPart)) {
		return NaN;
	}

	if (zeroExtend) {
		while (v1parts.length < v2parts.length) v1parts.push("0");
		while (v2parts.length < v1parts.length) v2parts.push("0");
	}

	if (!lexicographical) {
		v1parts = v1parts.map(Number);
		v2parts = v2parts.map(Number);
	}

	for (var i = 0; i < v1parts.length; ++i) {
		if (v2parts.length == i) {
			return 1;
		}

		if (v1parts[i] == v2parts[i]) {
			continue;
		}
		else if (v1parts[i] > v2parts[i]) {
			return 1;
		}
		else {
			return -1;
		}
	}

	if (v1parts.length != v2parts.length) {
		return -1;
	}

	return 0;
};



var getParameterByName = function (name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.href);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

var getHashByName = function (name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\#]" + name + "=([^&#]*)"),
		results = regex.exec(location.hash);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};


(function ($) {

	// 폼 유효성
	// options { match: true, message: 오류시 메세지, pattern: 정규식 }
	// $(input).formatValidate({ match: true, message: "숫자만 입력 가능합니다.", pattern: /[0-9\-]+/g });
	$.fn.formatValidate = function (options) {
		return this.each(function () {

			$(this).focusout(function () {
				if ($(this).val() != "") {
					var fail = false;
					if (options.match) {
						var val = $(this).val().match(options.pattern);
						if ($(this).val() != val) fail = true;
					} else {
						if (!options.pattern.test($(this).val())) fail = true;
					}

					if (fail) {
						alert(options.message);
						$(this).val("");
						$(this).focus();
					}
				}
			});
		});
	};


	// 글자 카운트 
	// options { limit: 5000 }
	// $(textarea).textCount($(카운트표시엘리먼트), { limit: 제한될 글자수 });
	$.fn.textCount = function (countViewer, options) {
		countViewer.text($(this).val().length);
		return $(this).keyup(function () {

			var length = $(this).val().length;
			var value = $(this).val();
			if (options.limit && $(this).val().length > options.limit) {
				length = options.limit;
				value = value.substr(0, options.limit);
			}
			countViewer.text(length);
			$(this).val(value);
		}).focus(function () {
			var length = $(this).val().length;
			var value = $(this).val();
			if (options.limit && $(this).val().length > options.limit) {
				length = options.limit;
				value = value.substr(0, options.limit);
			}
			countViewer.text(length);
			$(this).val(value);

		})
	};


	// 날자에 . 추가
	// options "yyyy.mm" or "yyyy.mm.dd"
	// allow_string 문자도 허용
	// $(input).dateFormat("yyyy.mm"); 
	// 숫자 이외에 허용 안함
	$.fn.dateFormat = function (options, allow_string) {
		return $(this).keyup(function (e) {


			var $this = $(this);
			var val = "";


			if (allow_string) {
				val = $this.val().split(".").join("");
			} else {
				val = $this.val().replace(/[^0-9]/gi, "")
			}


			// 정상적인 위치에 점 찍은경우
			if ((val.length == 4 || val.length == 6) && e.which == 190) return;

			var array = [val.substr(0, 4), val.substr(4, 2), val.substr(6, 2)];


			// yyyy.mm 이면 dd제거
			for (var i = 0; i < array.length; i++) {
				if (array[i] == "") {
					array.splice(i, 1);
					i--;
				}
			}
			$(this).val(array.join("."));
		});
	};



	// 금액에 ',' 추가
	// $(input).currencyFormat();
	$.fn.currencyFormat = function () {

		return this.each(function () {
			var $this = $(this);
			$(this).keyup(function (e) {
				var val = $(this).val().replace(/[^0-9]/gi, "");
				if (val == "") return;

				if (isNaN(val) && val != 0) {
					$(this).val("");
					alert("숫자만 입력가능합니다.");
					return;
				}

				$this.val(Number(val).toLocaleString('en'));
			});
		});
	}

	// 전화번호에 '-' 추가
	// $(input).mobileFormat();
	$.fn.mobileFormat = function () {
		this.keyup(function (e) {
			var value = $(this).val().replace(/[^0-9]/gi, "")
			if (value.length < 4) {
				$(this).val(value);
			} else if (value.length < 7) {
				$(this).val(value.substr(0, 3) + "-" + value.substr(3, 3));
			} else if (value.length < 11) {
				value = value.substr(0, 3) + "-" + value.substr(3, 3) + "-" + value.substr(6, 4);
				$(this).val(value);
			} else if (value.length < 12) {
				value = value.substr(0, 3) + '-' + value.substr(3, 4) + '-' + value.substr(7, 4);
				$(this).val(value);
			} else {
				value = value.substr(0, 12);
				value = value.substr(0, 3) + '-' + value.substr(3, 4) + '-' + value.substr(7, 4);
				$(this).val(value);
			}
		});
	}

	// 카운트 버튼
	// $(input).buttonCounter($(minus), $(plus), {max:5, min:0});
	$.fn.buttonCounter = function (minus, plus, option) {

		var count = parseInt($(this).val()) || 0;
		var $this = $(this);

		minus.click(function () {
			if (count - 1 >= option.min) $this.val(--count);
		});

		plus.click(function () {
			if (count + 1 <= option.max) $this.val(++count);

		})

		return this;
	}


}(jQuery));
