﻿
// datepicker 필수
(function ($) {

	$.widget("ui.dateValidate", {

		_init: function () {
			
			var self = this, o = this.options;
			
			if (o.hasTime) {
				var end_val = $(o.end).val();
				$(o.end).datetimepicker("option", "onSelect", function () {
					self.check(self);
				})
				$(o.end).val(end_val);

				var begin_val = self.element.val();
				self.element.datetimepicker("option", "onSelect", function () {
					self.check(self);
				});
				self.element.val(begin_val);

				$(o.end).focusout(function () {
					if (o.invalid) {
						$(this).val("");
						$(this).blur();
					}
				})

			} else {
				$(o.end).datepicker("option", "onSelect", function () {
					self.check(self);
				})

				self.element.datepicker("option", "onSelect", function () {
					self.check(self);
				})
			}
		},

		check: function (self) {
	
			var o = self.options;

			var begin = self.element;
			var end = $(o.end);
			
			if (begin.val() != "" && end.val() != "") {

				if (self.getDate(begin.val()) > self.getDate(end.val())) {
					o.invalid = true;
					
					alert("시작일이 종료일보다 클 수 없습니다.");
					end.val("");

					if (o.onSelect) o.onSelect();
					return false;
				} else {
					o.invalid = false;
				}
				
			}

			if (o.onSelect) o.onSelect();
			return true;

		} , 
		
		getDate: function (str) {

			var yyyy = str.substr(0, 4)
			var mm = str.substr(5, 2)
			var dd = str.substr(8, 2)

			if (str.length == 10) {
				return new Date((mm + "/" + dd + "/" + yyyy));
			} else {
				return new Date((mm + "/" + dd + "/" + yyyy + " " + str.substr(11,5) + ":00" ));
			}

		}

	});


})(jQuery);



