﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;

public partial class pcc_V3_result_seed : System.Web.UI.Page
{

    public String  retInfo;        //복호화 전 결과정보
    public String  decStr;         //복호화 후 결과정보

	public String  name;			//성명
	public String  sex;				//성별
	public String  birYMD;			//생년월일
	public String  fgnGbn;			//내외국인 구분값	
	public String  di;				//DI
	public String  ci1;				//CI1
	public String  ci2;				//CI2
	public String  civersion;		//CI Version

	public String  reqNum;			//요청번호
	public String  result;			//본인확인 결과 (Y/N)
	public String  certDate;		//검증시간
	public String  certGb;			//인증수단
	public String  cellNo;			//핸드폰 번호
	public String  cellCorp;		//이동통신사
	public String  addVar;			//추가 파라메터
    	
	//예약 필드
	public String  ext1;
	public String  ext2;
	public String  ext3;
	public String  ext4;
	public String  ext5;
	
	public String  decStr1;
	public String  decStr2;
	public String  hashStr;
	public String  encPara;
	public String  encMsg;
	public String  msgChk;      //위조/변조 검증 결과

    protected void Page_Load(object sender, EventArgs e){

		//01. 쿠키값 확인
		if(Request.Cookies["UserSettings"]  != null){
			reqNum = Request.Cookies["UserSettings"]["reqNum"];

			//02. 쿠키 정보 획득후 삭제
			Response.Cookies["UserSettings"].Expires = DateTime.Now.AddDays(0d);
			
			Response.CacheControl = "no-cache";
			Response.AddHeader("Pragma", "no-cache");
			Response.Expires = 0;
			Response.Buffer = true;

			retInfo = Request.QueryString["retInfo"];

			SCISecurityLib.SEED secux = new SCISecurityLib.SEED();
			//03. 복호화시 reqNum를 복호화 Key로 사용
			decStr = secux.PccSeedDecript(retInfo, reqNum);

			//04. 복호화 짜르기 "/"
			//데이터 조합 : "본인확인1차암호화값/위변조검증값/암복화확장변수"
			String[] strArr1 = decStr.Split('^');
			encPara   = strArr1[0];	//본인확인 1차 암호화값
			encMsg    = strArr1[1];	//위변조검증값

			//05. 위변조값 생성
			SCISecurityLib.AES hmac = new SCISecurityLib.AES();
			hashStr = hmac.PccHMacEncript(encPara);

			//06. 서버에서 내려준 위변조값과 클라이언트에서 만든 위변조값 비교
			if(hashStr == encMsg){
				msgChk = "T";
			}
			
			if(msgChk == "T"){
				//07. 위변조가 정상일때 "본인확인 1차 암호화값"을 복호화
				//복호화시 reqNum를 Key로 사용

				decStr = secux.PccSeedDecript(encPara, reqNum);
				String[] strArr = decStr.Split('^');

				name		= strArr[0];
				birYMD		= strArr[1];
				sex			= strArr[2];       
				fgnGbn		= strArr[3];				
				di			= strArr[4];
				ci1			= strArr[5];
				ci2			= strArr[6];
				civersion	= strArr[7];
				reqNum		= strArr[8];
				result		= strArr[9];
				certGb		= strArr[10];
				cellNo		= strArr[11];
				cellCorp	= strArr[12];
				certDate	= strArr[13];
				addVar		= strArr[14];
				
				//예약 필드
				ext1		= strArr[15];
				ext2		= strArr[16];
				ext3		= strArr[17];
				ext4		= strArr[18];
				ext5		= strArr[19];
			}

			Marshal.ReleaseComObject(secux);
			secux = null;

			Marshal.ReleaseComObject(hmac);
			hmac = null;

		}
    }
}
