﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pcc_V3_result_seed.aspx.cs" Inherits="pcc_V3_result_seed" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
    <head>
        <title>SCI평가정보 본인확인서비스  테스트</title>
        <meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
        <style>
            <!--
            body,p,ol,ul,td
            {
                font-family: 굴림;
                font-size: 12px;
            }

            a:link { size:9px;color:#000000;text-decoration: none; line-height: 12px}
            a:visited { size:9px;color:#555555;text-decoration: none; line-height: 12px}
            a:hover { color:#ff9900;text-decoration: none; line-height: 12px}

            .style1 {
                color: #6b902a;
                font-weight: bold;
            }
            .style2 {
                color: #666666
            }
            .style3 {
                color: #3b5d00;
                font-weight: bold;
            }
            -->
        </style>
    </head>
	<body>
            [복호화 후 수신값] <br>
            <br>
            <table cellpadding="1" cellspacing="1" border="1">
				
				<tr>
                    <td align="left">성명</td>
                    <td align="left"><%=name%></td>
                </tr>
				<tr>
                    <td align="left">성별</td>
                    <td align="left"><%=sex%></td>
                </tr>
				<tr>
                    <td align="left">생년월일</td>
                    <td align="left"><%=birYMD%></td>
                </tr>
				<tr>
                    <td align="left">내외국인 구분값(1:내국인, 2:외국인)</td>
                    <td align="left"><%=fgnGbn%></td>
                </tr>				
				<tr>
                    <td align="left">중복가입자정보</td>
                    <td align="left"><%=di%></td>
                </tr>
				<tr>
                    <td align="left">연계정보1</td>
                    <td align="left"><%=ci1%></td>
                </tr>
				<tr>
                    <td align="left">연계정보2</td>
                    <td align="left"><%=ci2%></td>
                </tr>
				<tr>
                    <td align="left">연계정보버전</td>
                    <td align="left"><%=civersion%></td>
                </tr>
                <tr>
                    <td align="left">요청번호</td>
                    <td align="left"><%=reqNum%></td>
                </tr>
				<tr>
                    <td align="left">인증성공여부</td>
                    <td align="left"><%=result%></td>
                </tr>
				<tr>
                    <td align="left">인증수단</td>
                    <td align="left"><%=certGb%></td>
                </tr>
				<tr>
                    <td align="left">핸드폰번호</td>
                    <td align="left"><%=cellNo%>&nbsp;</td>                
                </tr>
				<tr>
                    <td align="left">이동통신사</td>
                    <td align="left"><%=cellCorp%>&nbsp;</td>                
                </tr>
                <tr>
                    <td align="left">요청시간</td>
                    <td align="left"><%=certDate%></td>
                </tr>				
				<tr>
                    <td align="left">추가파라미터</td>
                    <td align="left"><%=addVar%>&nbsp;</td>
                </tr>
				
            </table>            
            <br>
            <br>
            <a href="http://.../pcc_V3_input_seed.aspx">[Back]</a>
</body>
</html>
