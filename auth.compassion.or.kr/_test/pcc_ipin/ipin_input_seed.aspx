<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ipin_input_seed.aspx.cs" Inherits="ipin_input_seed" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title>아이핀 서비스 Sample 화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<style>
<!--
   body,p,ol,ul,td
   {
	 font-family: 굴림;
	 font-size: 12px;   
   }
   
   a:link { size:9px;color:#000000;text-decoration: none; line-height: 12px}
   a:visited { size:9px;color:#555555;text-decoration: none; line-height: 12px}
   a:hover { color:#ff9900;text-decoration: none; line-height: 12px}

   .style1 {
		color: #6b902a;
		font-weight: bold;
	}
	.style2 {
	    color: #666666
	}
	.style3 {
		color: #3b5d00;
		font-weight: bold;
	}
-->
</style>
</head>
<body onload="document.reqCBAForm.jumin.focus();" bgcolor="#FFFFFF" topmargin=0 leftmargin=0 marginheight=0 marginwidth=0>
<center>
<br><br><br>
<span class="style1">아이핀 서비스 요청화면 Sample입니다.</span><br>

<form name="reqCBAForm" method="post" action="ipin_request_seed">
<table cellpadding=1 cellspacing=1>
    <tr>
        <td align=center>회원사아이디</td>
        <td align=left><input type="text" name="id" size='41' maxlength ='8' value = "HCF002"></td>
    </tr>
    <tr>
        <td align=center>요청번호</td>
        <td align=left><input type="text" name="reqNum" size='41' maxlength ='30' value='<%=reqNum%>'></td>
    </tr>
    <tr>
        <td align=center>서비스번호</td>
        <td align=left><input type="text" name="srvNo" size='41' maxlength ='6' value="002001"></td>
    </tr>
    <tr>
        <td align=center>결과수신URL</td>
        <td align=left><input type="text" name="retUrl" size="41" value="23http://www.compassionko.org/_test/pcc_ipin/ipin_popup_seed.aspx"></td>
    </tr>
</table>
<br><br>
<input type="submit" value="아이핀서비스 요청">
</form>
<br>
<br>
  이 Sample화면은 i-PIN 서비스 요청화면 개발시 참고가 되도록 제공하고 있는 화면입니다.<br>
<br>
</center>
</body>
</html>