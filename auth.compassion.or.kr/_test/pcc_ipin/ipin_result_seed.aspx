﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ipin_result_seed.aspx.cs" Inherits="ipin_result_seed" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title>SCI평가정보 아이핀서비스  테스트</title>
</head>
<body>
    [아이핀 결과 수신 Sample - ASP.NET ] <br> <br>

	[복호화 하기전 수신값] <br><br>
    retInfo : <%=retInfo%> <br><br>
            
    [복호화 후 수신값] <br>
    <br>

    <table cellpadding=1 cellspacing=1>
        <tr>
            <td align=left>요청번호</td>
            <td align=left><%=reqNum%></td>
        </tr>
        <tr>
            <td align=left>아이핀</td>
            <td align=left><%=vDiscrNo%></td>
        </tr>
        <tr>
            <td align=left>성명</td>
            <td align=left><%=name%></td>
        </tr>
        <tr>
            <td align=left>결과값</td>
            <td align=left><%=result%></td>
        </tr>
        <tr>
            <td align=left>연령대</td>
            <td align=left><%=age%></td>
        </tr>
        <tr>
            <td align=left>성별</td>
            <td align=left><%=sex%></td>
        </tr>
         <tr>
            <td align=left>ip</td>
            <td align=left><%=ip%></td>
        </tr>
        <tr>
            <td align=left>발급수단정보</td>
            <td align=left><%=authInfo%></td>
        </tr>
        <tr>
            <td align=left>생년월일</td>
            <td align=left><%=birth%></td>
        </tr>
        <tr>
            <td align=left>내/외국인구분</td>
            <td align=left><%=fgn%></td>
        </tr>
        <tr>
            <td align=left>중복가입확인정보</td>
            <td align=left><%=discrHash%></td>
        </tr>
		<tr>
            <td align=left>연계정보 버젼</td>
            <td align=left><%=ciVersion%></td>
        </tr>
		<tr>
            <td align=left>연계정보</td>
            <td align=left><%=ciscrHash%></td>
        </tr>
    </table>

    <br>
    retInfo : <%=decStr%> <br>
    <br>
    <br>
    <a href="http://.../ipin_input_seed.aspx">[Back]</a>
</body>
</html>
