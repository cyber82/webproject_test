﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ipin_input_seed : System.Web.UI.Page
{
    public String curDate;
    public String reqNum;

    protected void Page_Load(object sender, EventArgs e){

		//요청시간
        curDate = System.DateTime.Now.ToString("yyyyMMddHHmmss");
		
		//요청 번호 생성
		reqNum = curDate + getRandom(6);		
    }

	//Random 문자 생성
	static String getRandom( int cnt ){
		String ranData = "";
		Random ran = new Random();
		for( int j = 0; j < cnt; j++ ){
			ranData = ranData + ran.Next(10);
		}

		return ranData;
    }
}
