<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pcc_name_sample_seed.aspx.cs" Inherits="pcc_name_sample_seed" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>본인확인서비스 서비스 Sample 화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<style>
<!--
   body,p,ol,ul,td
   {
	 font-family: 굴림;
	 font-size: 12px;   
   } 
   
   a:link { size:9px;color:#000000;text-decoration: none; line-height: 12px}
   a:visited { size:9px;color:#555555;text-decoration: none; line-height: 12px}
   a:hover { color:#ff9900;text-decoration: none; line-height: 12px}

   .style1 {
		color: #6b902a;
		font-weight: bold;
	}
	.style2 {
	    color: #666666
	}
	.style3 {
		color: #3b5d00;
		font-weight: bold;
	}
-->
</style>

<script language=javascript>  
<!--
    var CBA_window; 

    function openPCCWindow(){ 
        var CBA_window = window.open('', 'PCCWindow', 'width=430, height=560, resizable=1, scrollbars=no, status=0, titlebar=0, toolbar=0, left=300, top=200' );

        if(CBA_window == null){ 
			 alert(" ※ 윈도우 XP SP2 또는 인터넷 익스플로러 7 사용자일 경우에는 \n    화면 상단에 있는 팝업 차단 알림줄을 클릭하여 팝업을 허용해 주시기 바랍니다. \n\n※ MSN,야후,구글 팝업 차단 툴바가 설치된 경우 팝업허용을 해주시기 바랍니다.");
        }

        document.reqCBAForm.action = '<%:actionUrl%>';
        document.reqCBAForm.target = 'PCCWindow';

		return true;
    }	

//-->
</script>

</head>

<body bgcolor="#FFFFFF" topmargin=0 leftmargin=0 marginheight=0 marginwidth=0 >

<center>
<br><br><br><br><br><br>
<span class="style1">본인확인서비스 요청화면 Sample입니다.</span><br>
<br><br>
<table cellpadding=1 cellspacing=1>    
    <tr>
        <td align=center>회원사아이디</td>
        <td align=left><%=id%></td>
    </tr>
    <tr>
        <td align=center>서비스번호</td>
        <td align=left><%=srvNo%></td>
    </tr>
    <tr>
        <td align=center>요청번호</td>
        <td align=left><%=reqNum%></td>
    </tr>

	<tr>
        <td align=center>요청시간</td>
        <td align=left><%=certDate%></td>
    </tr>
	<tr>
        <td align=center>추가파라메터</td>
        <td align=left><%=addVar%></td>
    </tr>    
    <tr>
        <td align=center>&nbsp</td>
        <td align=left>&nbsp</td>
    </tr>
    <tr width=100>
        <td align=center>요청정보(암호화)</td>
        <td align=left>
            <%=encStr.Substring(0,40)%>...
        </td>
    </tr>
    <tr>
        <td align=center>결과수신URL</td>
        <td align=left><%=retUrl%></td>
    </tr>
</table>

<!-- 본인확인서비스 요청 form --------------------------->
<form name="reqCBAForm" method="post" action = "#">
	
    <input type="hidden" name="reqInfo"     value = "<%=encStr%>">
    <input type="hidden" name="ok_url"      value = "<%=retUrl%>">
    <input type="submit" value="본인확인서비스 요청" onclick="javascript:openPCCWindow();" >	
</form>
<BR>
<BR>
<!--End 본인확인서비스 요청 form ----------------------->

<br>
<br>
	<table width="450" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="130"><a href=http://www.siren24.com/v2alimi/comm/jsp/v2alimiAuth.jsp?id=SIR005&svc_seq=01 target=blank><img src="/name/images/logo01.gif" width="122" height="41" border=0></a></td>
        <td width="320"><span class="style2">본 사이트는 SCI평가정보(주)의 <span class="style3">명의도용방지서비스</span> 협약사이트 입니다. 타인의 명의를 도용하실 경우 관련법령에 따라 처벌 받으실 수 있습니다.</span></td>
      </tr>
    </table>
      <br>
      <br>
    <br>
  이 Sample화면은 본인확인서비스 요청화면 개발시 참고가 되도록 제공하고 있는 화면입니다.<br>
  <br>
</center>
</BODY>
</HTML>