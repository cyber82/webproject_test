﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pcc_name_result_seed.aspx.cs" Inherits="pcc_name_result_seed" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
    <head>
        <title>SCI평가정보 본인확인서비스  테스트</title>
        <meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
        <style>
            <!--
            body,p,ol,ul,td
            {
                font-family: 굴림;
                font-size: 12px;
            }

            a:link { size:9px;color:#000000;text-decoration: none; line-height: 12px}
            a:visited { size:9px;color:#555555;text-decoration: none; line-height: 12px}
            a:hover { color:#ff9900;text-decoration: none; line-height: 12px}

            .style1 {
                color: #6b902a;
                font-weight: bold;
            }
            .style2 {
                color: #666666
            }
            .style3 {
                color: #3b5d00;
                font-weight: bold;
            }
            -->
        </style>
    </head>
	<body>
            [복호화 후 수신값] <br>
            <br>
            <table cellpadding="1" cellspacing="1" border="1">
				
				<tr>
                    <td align="left">성명</td>
                    <td align="left"><%=name%></td>
                </tr>
				<tr>
                    <td align="left">주민번호</td>
                    <td align="left"><%=registerNo1%><%=registerNo2%></td>
                </tr>
						
				<tr>
                    <td align="left">중복가입자정보</td>
                    <td align="left"><%=di%></td>
                </tr>
				<tr>
                    <td align="left">연계정보1</td>
                    <td align="left"><%=ci1%></td>
                </tr>
				
				<tr>
                    <td align="left">연계정보버전</td>
                    <td align="left"><%=civersion%></td>
                </tr>
                <tr>
                    <td align="left">요청번호</td>
                    <td align="left"><%=reqNum%></td>
                </tr>
				<tr>
                    <td align="left">인증성공여부</td>
                    <td align="left"><%=result%></td>
                </tr>
			
				
            </table>            
            <br>
            <br>
            <a href="http://.../pcc_name_input_seed.aspx">[Back]</a>
</body>
</html>
