﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class m_MainMaster : System.Web.UI.MasterPage {

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			
			this.OnBeforePostBack();
		}

	}
	public string domain_mobile = ConfigurationManager.AppSettings["domain_mobile"];
	protected virtual void OnBeforePostBack() {
		
		if(AppSession.HasCookie(this.Context)) {
			ph_app_bottom.Visible = true;
		}
	}

	public virtual string Title
	{
		get
		{
			return Master.Title;
		}
		set
		{
			Master.Title = value;
		}
	}

}
