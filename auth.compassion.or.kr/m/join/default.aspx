﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="m_join_default" MasterPageFile="~/m/main.Master" %>

<%@ MasterType VirtualPath="~/m/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/id.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/email.js"></script>
    <script type="text/javascript" src="/cert/cert.js"></script>
    <script type="text/javascript" src="/m/join/default.js?v=1.2"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" runat="server" id="user_id" value="" />
    <input type="hidden" runat="server" id="user_identification_method" value="" />
    <input type="hidden" runat="server" id="user_email" value="" />
    <input type="hidden" runat="server" id="user_mobile" value="" />

    <input type="hidden" runat="server" id="user_class" value="14세이상" />
    <input type="hidden" runat="server" id="parent_cert" value="" />
    <input type="hidden" runat="server" id="parent_name" value="" />
    <input type="hidden" runat="server" id="parent_juminId" value="" />
    <input type="hidden" runat="server" id="parent_mobile" value="" />
    <input type="hidden" runat="server" id="parent_email" value="" />


    <div class="wrap-sectionsub">
        <!---->

        <div class="member-join sponsor-payment">

			<asp:PlaceHolder runat="server" ID="ph_normal">
            <div class="top-linkgo">
                <a href="/m/login/"><span></span>로그인</a>
                <a href="/m/find/pwd/"><span></span>비밀번호 찾기</a>
            </div>

            <!--메뉴 활성화 상태  :  "selected"-->
            <div class="tab-jointype">
                <ul>
                    <li class="btn_user_group selected" data-user-group="개인"><span>개인회원<br />(간편가입)</span></li>
                    <%--<li class="btn_user_group" data-user-group="기존"><span>기존후원자<br />(웹 회원전환)</span></li>--%>
                    <li class="btn_user_group" data-user-group="기업"><span>기업/단체<br />회원</span></li>
                </ul>
            </div>
			</asp:PlaceHolder>
			<asp:PlaceHolder runat="server" ID="ph_sponsor" Visible="false">
				<p class="txt2 sectionsub-margin1" style="margin:-20px 0">정기후원의 경우 회원만 후원이 가능합니다.  아래의 간편회원 가입정보를 작성하신 뒤, 바로 후원을 진행해보세요.</p>
				<a runat="server" id="btnList"></a>
			</asp:PlaceHolder>

            <p class="txt-title">기본정보 / 본인확인</p>

            <fieldset class="frm-input">
                <legend>기본정보 / 본인확인 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                             <asp:TextBox runat="server" ID="user_name" class="input_type1" placeholder="이름" style="width: 100%" MaxLength="40"></asp:TextBox>
                            </span>
                        <span class="txt-error" data-id="check_name" style="display: none"></span>

                        <span class="row">
                            <span class="column" style="width: 48%">
                                     <asp:DropDownList runat="server" ID="user_birth_yyyy" style="width: 100%" Class="birth_sel"></asp:DropDownList>
                            </span>
                            <span class="column" style="width: 26%">
                                <asp:DropDownList runat="server" ID="user_birth_mm" style="width: 100%" Class="birth_sel"></asp:DropDownList>
                            </span>
                            <span class="column" style="width: 26%">
                                <asp:DropDownList runat="server" ID="user_birth_dd" style="width: 100%" Class="birth_sel"></asp:DropDownList>
                            </span>
                        </span>

                         <span class="txt-error" data-id="birth_check" style="display: none"></span>
                        <span class="txt-result" data-id="check_cert" style="display: none"></span>

                        <span class="row">
                            <span class="checkbox-ui">
                                <input type="checkbox" class="css-checkbox" id="chk1" />
                                <label for="chk1" class="css-label" style="color: #444">휴대폰 없음</label>
                            </span>
                        </span>

                        <!--휴대폰 없음(기본)-->
                        <span class="row pos-relative1 have_phone">
                            <input type="text" id="phone" data-id="input_phone_no1" class="number_only" maxlength="11" placeholder="휴대폰번호(-없이 입력)" style="width: 100%" />
                            <a class="bt-type9 pos-btn hide_phone_cert" style="width: 50px" data-id="btn_phone_no1">인증</a>
                        </span>
                        <span class="txt-error" data-id="msg_phone_cert" style="display: none"></span>
                        <span class="row pos-relative1 have_phone hide_phone_cert" id="cert_no" style="display:none">
                            <input type="text" placeholder="인증번호" style="width: 100%" id="cert" data-id="input_phone_confirm" class="number_only" />
                            <a class="bt-type9 pos-btn" style="width: 50px" data-id="btn_phone_confirm">확인</a>
                        </span>
                        <span class="row have_phone">
                            <input type="text" id="email" data-id="input_email1" placeholder="이메일" style="width: 100%" />
                            <span class="txt-error" data-id="msg_email1_cert" style="display:none"></span>
                        </span>
                        <!--//휴대폰 없음(기본)-->
                        
                        <!--휴대폰 없음(체크상태)-->
                        <span class="row pos-relative3 no_phone" style="display:none" >
                            <input type="text" value="" placeholder="이메일" style="width: 100%" id="email_2" data-id="input_email2"/>
                            <a class="bt-type9 pos-btn hide_mail_cert" style="width: 50px" href="#" data-id="btn_email2">인증</a>
                            <a class="bt-type9 pos-btn2 hide_mail_cert" data-id="btn_email_confirm" style="width: 50px" href="#">확인</a>
                        </span>
                        <span class="txt-result no_phone" data-id="check_user_email" style="display: none"></span>
                        <p class="txt-bottom hide_mail_cert no_phone" style="margin-top: 9px;display:none">입력하신 이메일에서 인증코드를 클릭하셨으면 [확인] 버튼을 클릭해주세요.</p>
                        <span class="row no_phone" style="display:none">
                            <input type="text" id="user_phone" runat="server" class="number_only" placeholder="전화번호(-없이 입력)" maxlength="11" style="width: 100%" />
                        </span>
                        <span class="txt-result" data-id="check_user_phone" style="display: none"></span>
                        <!--//휴대폰 없음(체크상태)-->
                        
                        <span class="row">
                            <span class="checkbox-ui">
                                <input type="checkbox" runat="server" id="agree_receive" class="css-checkbox" />
                                <label for="agree_receive" class="css-label">이메일/SMS를 수신하지 않겠습니다.</label>
                            </span>
                        </span>

                    </div>
                </div>
            </fieldset>

            <div class="box-gray">
                <strong>이메일/SMS 수신여부에 동의하신 분에게는  </strong>
                <br />
                <ul class="list-bullet">
                    <li>컴패션 후원과 관련한 필수 안내사항과, 매월 발행되는 ‘  이메일 뉴스레터', 연 1회 발행되는 '오프라인 뉴스레터'의   PDF파일을 메일로 보내드립니다.</li>
                    <li>휴대폰으로 이벤트 및 행사 알림 메시지를 보내드립니다.  (일반 전화번호 제외, 휴대폰번호로만 보내드립니다.)</li>
                </ul>
            </div>

            <!--휴대폰 없음(체크상태) : 보호자 동의-->
            
            <div class="protector-agree" data-id="pn_parent" style="display: none" >
            <p class="txt-title">보호자 동의</p>
                <span class="checkbox-ui">
                    <input type="checkbox" class="css-checkbox" id="agree_parent" />
                    <label for="agree_parent" class="css-label">보호자 동의</label>
                </span>
                <span class="txt-error" data-id="parent_check" style="display: none"></span>
                <p class="txt">만 14세 미만은 법률에 의거하여 보호자(법적대리인)의 동의가 필요합니다.</p>
                <div class="tab-column-2 hide_parent_cert">
                    <a class="selected" href="#" data-id="btn_parent_identification_by_phone">휴대폰 인증</a>
                    <a href="#" data-id="btn_parent_identification_by_ipin">아이핀 인증</a>
                </div>
                <p class="txt-result" data-id="msg_parent_identification" style="display: none"></p>
            </div>
            
            <!--//휴대폰 없음(체크상태) : 보호자 동의-->

            <p class="txt-title">ID/PW</p>
            <fieldset class="frm-input">
                <legend>ID/PW 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                            <input type="text" data-id="input_user_id" maxlength="12" placeholder="아이디" style="width: 100%" />
                            <a class="bt-type10 pos-btn" style="width: 70px;display:none" href="#" data-id="btn_check_id">중복검사</a></span>
                        <span class="txt-error" data-id="msg_user_id" style="display: none"></span>
                        <!--에러메시지 경우에만 출력-->

                        <span class="row">
                            <asp:TextBox runat="server" ID="user_pwd" TextMode="Password" MaxLength="15" placeholder="비밀번호" Style="width: 100%"></asp:TextBox>
                        </span>
                        <span class="row">
                            <asp:TextBox runat="server" ID="re_user_pwd" TextMode="Password" MaxLength="15" Style="width: 100%" placeholder="비밀번호 재확인"></asp:TextBox>
                        </span>
                        <span class="txt-error" data-id="msg_user_pwd" style="display: none"></span>
                    </div>
                </div>
            </fieldset>

            <p class="txt-title topline-title">약관동의</p>
            <div class="box-agree">
                <div class="row">이용약관<a class="bt-type8" target="_blank" href="/m/etc/terms/?app_ex=1">전문 보기</a></div>
                <div class="row">개인정보 수집 및 이용<a class="bt-type8" target="_blank" href="/m/etc/privacy/?app_ex=1">전문 보기</a></div>
            </div>

            <p class="bottomtxt-agree">이용 약관 및 개인 정보 이용 안내를 확인하였으며,<br />
                위 내용에 동의합니다.</p>

            <div class="wrap-bt">
                <asp:LinkButton runat="server" ID="btn_submit" style="width: 100%" OnClick="btn_submit_Click" class="bt-type6">동의하고 회원가입</asp:LinkButton>
            </div>

            <p class="txt-bottom">
                회원가입이 어려우시거나 본인확인/인증이 안되시는 경우 한국컴패션으로 연락 주시기 바랍니다.<br />
				후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_gray">info@compassion.or.kr</a>
            </p>


        </div>

        <!--//-->
    </div>




</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
    <asp:PlaceHolder runat="server" ID="ph_ex_frm" Visible="false">
        <form id="exfrm" method="post" action="/m/login/?action=REFER">
            <input type="hidden" id="userId" name="userId" value="<%:this.ViewState["userId"].ToString() %>" />
            <input type="hidden" id="userPwd" name="userPwd" value="<%:this.ViewState["userPwd"].ToString() %>" />
            <input type="hidden" id="r" name="r" value="<%:this.ViewState["returnUrl"].ToString() %>" />
        </form>

        <script type="text/javascript">
            $(function () {
                $("#exfrm").submit();
            })
        </script>

    </asp:PlaceHolder>
</asp:Content>


