﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
public partial class m_join_default : FrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

 

    protected override void OnBeforePostBack() {

        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();


        ErrorLog.Write(HttpContext.Current, 0, "eventIDParam=====" + eventID);

        if (eventID == null || eventID == "")
        {
            //ErrorLog.Write(HttpContext.Current, 0, "eventIDParam===null or empty ==" + eventID);

            if (Request.Cookies["e_id"] != null)
            {
                //ErrorLog.Write(HttpContext.Current, 0, "e_id cookie is not null");

                eventID = Request.Cookies["e_id"].Value;
                eventSrc = Request.Cookies["e_src"].Value;
            }

        }
        else
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        /*
        ErrorLog.Write(HttpContext.Current, 0, "Request.Cookies[e_id]=====" + Request.Cookies["e_id"].Value);
        ErrorLog.Write(HttpContext.Current, 0, "eventID=====" + eventID);
        ErrorLog.Write(HttpContext.Current, 0, "Session[eventID]=====" + Session["eventID"]); 
        */

        var action = Request["action"].EmptyIfNull();
		if (action == "SPONSOR") {      // 후원중에 가입하는 경우

			if(Session["SPONSOR_RETURN_URL"] == null && Request.UrlReferrer != null) {      // 후원중에 가입하는 경우
				Session["SPONSOR_RETURN_URL"] = Request.UrlReferrer.AbsoluteUri;
			}

			btn_submit.Text = "동의하고 후원진행";
			ph_normal.Visible = false;
			ph_sponsor.Visible = true;

			btnList.HRef = "https://" + ConfigurationManager.AppSettings["domain_mobile"] + "/sponsor/info/";
		}

		for(int i = DateTime.Now.Year ; i >= DateTime.Now.Year - 100; i--) {
			user_birth_yyyy.Items.Add(new ListItem(i.ToString() , i.ToString()));
		}
		user_birth_yyyy.Items.Insert(0,new ListItem("생년", ""));

		for(int i = 1; i < 13; i++) {
			user_birth_mm.Items.Add(new ListItem(i.ToString(), i.ToString().PadLeft(2 , '0')));
		}
		user_birth_mm.Items.Insert(0, new ListItem("월", ""));

		for(int i = 1; i < 32; i++) {
			user_birth_dd.Items.Add(new ListItem(i.ToString(), i.ToString().PadLeft(2, '0')));
		}
		user_birth_dd.Items.Insert(0, new ListItem("일", ""));

	}
	
	protected void btn_submit_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}
		
		var userid = user_id.Value; 
		var sponsor_name = user_name.Text;
		var mobile = user_mobile.Value;
		var email = user_email.Value;
		var phone = user_phone.Value;
		var user_birth_date = DateTime.Parse(string.Format("{0}-{1}-{2}", user_birth_yyyy.SelectedValue, user_birth_mm.SelectedValue.PadLeft(2, '0'), user_birth_dd.SelectedValue.PadLeft(2, '0')));

		var SponsorName = sponsor_name;//후원자명
		var BirthDate = user_birth_date.ToString("yyyyMMdd"); //생년월일
		var CampaignID = ""; //캠페인ID
		var CampaignTitle = "";  //캠페인제목	  

		var Remark = "";  //비고
		var UserID = userid;  //LoginID
		var UserDate = DateTime.Now;//웹가입일
		var UserClass = user_class.Value; //후원자구분(14세이상,미만,국내외국인,해외내국인,기업)
		DateTime? CertifyDate = null; //승인일
		if(!string.IsNullOrEmpty(parent_name.Value))
			CertifyDate = DateTime.Now;
		
		var CertifyOrgan = parent_cert.Value; //승인기관	

		var ParentName = parent_name.Value; //14세미만일때 부모님정보
		var ParentJuminID = parent_juminId.Value; //14세미만일때 부모님정보

		var ParentMobile = parent_mobile.Value; //14세미만일때 부모님정보
		var ParentEmail = parent_email.Value; //14세미만일때 부모님정보
		
		var UserPW = user_pwd.Text; //패스워드
		

		if(string.IsNullOrEmpty(UserID)) {
			base.AlertWithJavascript("아이디를 입력해주세요1");
			return;
		}

        #region 1. 웹회원 등록

        using (AuthDataContext dao = new AuthDataContext())
        {

            var sponsorId = new SponsorAction().GetNewSponsorID();
            if (string.IsNullOrEmpty(sponsorId))
            {
                base.AlertWithJavascript("시스템 장애발생 잠시 후 다시 시도해주세요");
                return;
            }

            //var result = dao.sp_tSponsorMaster_insert_f(CodeAction.ChannelType, "", sponsorId, "", "", "", "", "", SponsorName, "", BirthDate, "", "", CampaignID, CampaignTitle, Remark, UserID, UserClass, CertifyDate, CertifyOrgan,
            //    ParentName, Roamer.Config.Cryptography.Encrypt(ParentJuminID), ParentMobile, UserPW, "", "", user_identification_method.Value, mobile, email, phone, "",
            //    !agree_receive.Checked, !agree_receive.Checked, null, null, null).First();

            Object[] op1 = new Object[] { "ChannelType", "SponsorID", "tempSponsorID", "ConID", "ConIDCheck", "JuminID", "FirstName", "LastName", "SponsorName", "ComRegistration", "BirthDate", "BirthDateClass", "GenderCode", "CampaignID", "CampaignTitle", "Remark", "UserID", "UserClass", "CertifyDate", "CertifyOrgan", "ParentName", "ParentJuminID", "ParentMobile", "UserPW", "ManagerName", "FilePath", "AuthMethod", "Mobile", "Email", "Phone", "UserGroup", "AgreePhone", "AgreeEmail", "ZipCode", "Addr1", "Addr2" };
            Object[] op2 = new Object[] { CodeAction.ChannelType, "", sponsorId, "", "", "", "", "", SponsorName, "", BirthDate, "", "", CampaignID, CampaignTitle, Remark, UserID, UserClass, CertifyDate, CertifyOrgan, ParentName, Roamer.Config.Cryptography.Encrypt(ParentJuminID), ParentMobile, UserPW, "", "", user_identification_method.Value, mobile, email, phone, "", !agree_receive.Checked, !agree_receive.Checked, null, null, null };
            var list = www6.selectSPAuth("sp_tSponsorMaster_insert_f", op1, op2).DataTableToList<sp_tSponsorMaster_insert_fResult>();
            var result = list.First();

            if (result.Result != 'Y')
            {

                base.AlertWithJavascript(result.Result_String);
                return;

            }

            #region 이벤트 정보 등록
            /*         이벤트 정보 등록  -- 20170327 이정길       */

            var eventID = Session["eventID"];
            var eventSrc = Session["eventSrc"];

            if (eventID == null)
            {
                if (Request.Cookies["e_id"] != null)
                {
                    eventID = Request.Cookies["e_id"].Value;
                    eventSrc = Request.Cookies["e_src"].Value;
                }
            }

            try
            {
                /*
                ErrorLog.Write(HttpContext.Current, 0, "회원가입 :: 이벤트 정보 등록 UserId == " + UserID);
                ErrorLog.Write(HttpContext.Current, 0, "회원가입 :: 이벤트 정보 등록 SponsorID == " + sponsorId);
                ErrorLog.Write(HttpContext.Current, 0, "회원가입 :: 이벤트 정보 등록 eventID == " + eventID);
                */

                using (FrontDataContext dao1 = new FrontDataContext())
                {
                    if (eventID != null && !eventID.Equals(""))
                    {
                        //ErrorLog.Write(HttpContext.Current, 0, "회원가입 :: 이벤트 정보 등록 eventSrc == " + eventSrc);

                        //dao1.sp_tEventApply_insert_f(int.Parse(eventID.ToString()),
                        //                            UserID,
                        //                            sponsorId,
                        //                            "",
                        //                            eventSrc.ToString(),
                        //                            "MEMBER",
                        //                            "JOIN",
                        //                            0,
                        //                            "");

                        //Object[] op3 = new Object[] { "page", "rowsPerPage", "display", "keyword", "startdate", "enddate" };
                        //Object[] op4 = new Object[] { int.Parse(eventID.ToString()), UserID, sponsorId, "", eventSrc.ToString(), "MEMBER", "JOIN", 0, "" };
                        //www6.selectSP("sp_tEventApply_insert_f", op3, op4);

                        Object[] op3 = new Object[] { "evt_id", "UserID", "SponsorID", "ConID", "evt_route", "SponsorType", "SponsorTypeEng", "SponsorAmount", "CommitmentID" };
                        Object[] op4 = new Object[] { int.Parse(eventID.ToString()), UserID, sponsorId, "", eventSrc.ToString(), "MEMBER", "JOIN", 0, "" };
                        www6.selectSP("sp_tEventApply_insert_f", op3, op4);


                        //dao1.SubmitChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 error :: " + ex.Message + ":::" + UserID + ":" + sponsorId + ":" + eventID + ":" + eventSrc);
            }
            #endregion

        }
        #endregion

        if (Session["SPONSOR_RETURN_URL"] != null) {
			var returnUrl = Session["SPONSOR_RETURN_URL"].ToString();
			Session["SPONSOR_RETURN_URL"] = null;

			this.ViewState["userId"] = UserID;
			this.ViewState["userPwd"] = UserPW;
			this.ViewState["returnUrl"] = returnUrl;
			ph_ex_frm.Visible = true;
			return;
			
		}
        this.SendMail(UserID, email);
        //Response.Redirect("complete");

        if (Request.UrlReferrer.AbsoluteUri.Contains("r="))
        {
            string url = Request.UrlReferrer.AbsoluteUri.Replace("r=", "|").Split('|')[1];
            url = System.Uri.UnescapeDataString(url);
            Response.Redirect(url);
        }
        else
            Response.Redirect("complete");
    }

    void SendMail( string UserID, string email ) {

        var args = new Dictionary<string, string> {
                    {"{userId}" , UserID}
        };

        Email.Send(HttpContext.Current, Email.SystemSender, new List<string>() { email },
            "[한국컴패션] 회원가입을 축하드립니다.", " /mail/join.html",
            args, null);


    }
}