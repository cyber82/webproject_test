﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class m_join_complete : FrontBasePage {

    public override bool RequireSSL
    {
        get
        {
            return true;
        }
    }

    public override bool NoCache
    {
        get
        {
            return true;
        }
    }

    protected override void OnBeforePostBack() {
        
        btn_main.HRef = ConfigurationManager.AppSettings["default_return_url_m"];

		btn_commitment.HRef = ConfigurationManager.AppSettings["default_return_url_m"] + "my/sponsor/commitment/";

	}
   
}