﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
public partial class m_join_offline_form : FrontBasePage {

    public override bool RequireSSL
    {
        get
        {
            return true;
        }
    }

    public override bool NoCache
    {
        get
        {
            return true;
        }
    }

    WWWService.Service _wwwService = new WWWService.Service();
    WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

    protected override void OnBeforePostBack() {

        var SponsorID = Session["offline.join.SponsorID"];
        if(SponsorID == null) {
            //Response.Redirect("/m/join/offline/");
            Response.Redirect("/m/join/");
            return;
        }

        SponsorID = SponsorID.ToString();
        this.ViewState["SponsorID"] = SponsorID;
        //		Response.Write(SponsorID);

        for(int i = DateTime.Now.Year; i >= DateTime.Now.Year - 100; i--) {
            user_birth_yyyy.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        user_birth_yyyy.Items.Insert(0, new ListItem("생년(4자)", ""));

        for(int i = 1; i < 13; i++) {
            user_birth_mm.Items.Add(new ListItem(i.ToString(), i.ToString().PadLeft(2, '0')));
        }
        user_birth_mm.Items.Insert(0, new ListItem("월", ""));

        for(int i = 1; i < 32; i++) {
            user_birth_dd.Items.Add(new ListItem(i.ToString(), i.ToString().PadLeft(2, '0')));
        }
        user_birth_dd.Items.Insert(0, new ListItem("일", ""));

        this.SetUserInfo();
    }

    void SetUserInfo() {

        var SponsorID = this.ViewState["SponsorID"].ToString();

        DataSet ds = _wwwService.listSponsor(SponsorID, "", "", "", "");

        if(ds.Tables[0].Rows.Count > 0) {

            //Response.Write(ds.Tables[0].Rows[0].ToJson());

            var row = ds.Tables[0].Rows[0];

            user_name.Text = row["SponsorName"].ToString();

            this.ViewState["data"] = ds.Tables[0].ToJson();
            /*
			this.ViewState["UserClass"] = row["UserClass"].ToString();
			this.ViewState["LocationType"] = row["LocationType"].ToString();
			this.ViewState["LanguageType"] = row["LanguageType"].ToString();
			this.ViewState["TranslationFlag"] = row["TranslationFlag"].ToString();
			this.ViewState["ChannelType"] = row["ChannelType"].ToString();
			this.ViewState["comRegistration"] = row["comRegistration"].ToString();
			*/

            try {
                var birthDate = Convert.ToDateTime(row["BirthDate"]);
                user_birth_yyyy.SelectedValue = birthDate.ToString("yyyy");
                user_birth_mm.SelectedValue = birthDate.ToString("MM");
                user_birth_dd.SelectedValue = birthDate.ToString("dd");
            } catch { }

        } else {
            ErrorLog.Write(this.Context, 0, "후원자 정보가 없습니다. SponsorID = " + SponsorID);
            //Response.Redirect("/m/join/offline/");
            Response.Redirect("/m/join/");
            return;
        }

        DataSet dsMobile = _wwwService.getSponsorCommunication(SponsorID, "", "휴대번호", "Y");
        if(dsMobile.Tables[0].Rows.Count > 0) {
            user_phone.Text = dsMobile.Tables[0].Rows[0]["CommunicationContents"].ToString();
        }

        DataSet dsEmail = _wwwService.getSponsorCommunication(SponsorID, "", "이메일", "Y");
        if(dsEmail.Tables[0].Rows.Count > 0) {
            user_email.Text = dsEmail.Tables[0].Rows[0]["CommunicationContents"].ToString();
        }


    }

    protected void btn_submit_Click( object sender, EventArgs e ) {

        if(base.IsRefresh) {
            return;
        }

        var userid = user_id.Value;
        var SponsorName = user_name.Text;
        var phone = user_phone.Text;
        var email = user_email.Text;

        var user_birth_date = DateTime.Parse(string.Format("{0}-{1}-{2}", user_birth_yyyy.SelectedValue, user_birth_mm.SelectedValue.PadLeft(2, '0'), user_birth_dd.SelectedValue.PadLeft(2, '0')));

        DataRow dr = ((DataTable)this.ViewState["data"].ToString().ToObject<DataTable>()).Rows[0];
        var comRegistration = dr["comRegistration"].ToString();
        var user_class = dr["UserClass"].ToString();
        var SponsorID = dr["SponsorID"].ToString(); //후원자ID
        var LocationType = dr["LocationType"].ToString(); //거주구분 : 국내,국외,미주
        var LanguageType = dr["LanguageType"].ToString();//사용언어 : 한글,영어,일어
        var TranslationFlag = dr["TranslationFlag"].ToString();//번역여부플래그 : YN
        var ChannelType = dr["ChannelType"].ToString(); //가입채널
        var ConID = dr["ConID"].ToString();
        var ConIDCheck = dr["ConIDCheck"].ToString();
        var JuminID = dr["JuminID"].ToString();
        if(!string.IsNullOrEmpty(JuminID))
            JuminID = Roamer.Config.Cryptography.Encrypt(JuminID);

        var FirstName = dr["FirstName"].ToString();
        var LastName = dr["LastName"].ToString();
        var BirthDateClass = dr["BirthDateClass"].ToString();
        var GenderCode = dr["GenderCode"].ToString();
        var BirthDate = user_birth_date.ToString("yyyyMMdd"); //생년월일
        var CampaignID = "";
        var CampaignTitle = "";

        if(string.IsNullOrEmpty(userid)) {
            base.AlertWithJavascript("아이디를 입력해주세요");
            return;
        }

        using (AuthDataContext dao = new AuthDataContext())
        {
            //var result = dao.sp_tSponsorMaster_insert_f(CodeAction.ChannelType, SponsorID, SponsorID, ConID, ConIDCheck, JuminID, FirstName, LastName, SponsorName, comRegistration, BirthDate, BirthDateClass, GenderCode, CampaignID, CampaignTitle, "", userid, user_class, DateTime.Now, "서울신용평가",
            //    "", "", "", user_pwd.Text, "", "", "", phone, email, "", "", !agree_receive.Checked, !agree_receive.Checked, null, null, null).First();

            Object[] op1 = new Object[] { "ChannelType", "SponsorID", "tempSponsorID", "ConID", "ConIDCheck", "JuminID", "FirstName", "LastName", "SponsorName", "ComRegistration", "BirthDate", "BirthDateClass", "GenderCode", "CampaignID", "CampaignTitle", "Remark", "UserID", "UserClass", "CertifyDate", "CertifyOrgan", "ParentName", "ParentJuminID", "ParentMobile", "UserPW", "ManagerName", "FilePath", "AuthMethod", "Mobile", "Email", "Phone", "UserGroup", "AgreePhone", "AgreeEmail", "ZipCode", "Addr1", "Addr2" };
            Object[] op2 = new Object[] { CodeAction.ChannelType, SponsorID, SponsorID, ConID, ConIDCheck, JuminID, FirstName, LastName, SponsorName, comRegistration, BirthDate, BirthDateClass, GenderCode, CampaignID, CampaignTitle, "", userid, user_class, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "서울신용평가", "", "", "", user_pwd.Text, "", "", "", phone, email, "", "", !agree_receive.Checked, !agree_receive.Checked, null, null, null };
            var list = www6.selectSPAuth("sp_tSponsorMaster_insert_f", op1, op2).DataTableToList<sp_tSponsorMaster_insert_fResult>();
            var result = list.First();

            if (result.Result != 'Y')
            {     // 등록실패

                base.AlertWithJavascript(result.Result_String);
                return;

            }

        }

        #region 2. DAT 회원 수정
        WWWService.Service _wwwService = new WWWService.Service(); //WWWService
        DataSet dsResult = new DataSet();
        Int64 iTimeStamp = Convert.ToInt64(_wwwService.GetDate().ToString("yyyyMMddHHmmssff2")); //ID : TimeStamp

        string sResult = null;
        bool is_regist_dat_sponsor = true;

        try {

            sResult = _wwwService.modifyDATSponsor_NEW(SponsorID, SponsorName, null, null, null, null, null,
                                                      null, null, null, BirthDate.ToDateFormat("-"), null, userid,
                                                      null, null, null, null,
                                                      null, null, null,
                                                      null, null, null, null, null);

            if(sResult.Substring(0, 2) == "30") {
                is_regist_dat_sponsor = false;
            }

        } catch(Exception ex) {

            ErrorLog.Write(this.Context, 0, ex.ToString());
            is_regist_dat_sponsor = false;
        }

        if(!is_regist_dat_sponsor)
        {
            // DAT 등록오류시 웹디비 tSponsorMaster 삭제
            if(!string.IsNullOrEmpty(SponsorID))
            {
                using (AuthDataContext dao = new AuthDataContext())
                {
                    var exist = www6.selectQAuth<tSponsorMaster>("SponsorID", SponsorID);
                    //if (dao.tSponsorMaster.Any(p => p.SponsorID == SponsorID))
                    if(exist.Any())
                    {
                        //dao.tSponsorMaster.DeleteOnSubmit(dao.tSponsorMaster.First(p => p.SponsorID == SponsorID));
                        www6.deleteAuth(exist.First());
                        //dao.SubmitChanges();
                    }

                }
            }
            base.AlertWithJavascript("회원님의 정보를 등록하는 중 오류가 발생했습니다");
            return;
        }

        #endregion


        var sponsorAction = new SponsorAction();

        sponsorAction.UpdateCommunications(true, SponsorID, email, phone);

        sponsorAction.UpdateAgreeCommunications(SponsorID, SponsorName, !agree_receive.Checked, !agree_receive.Checked, email, phone);

        if(Session["SPONSOR_RETURN_URL"] != null) {
            var returnUrl = Session["SPONSOR_RETURN_URL"].ToString();
            Session["SPONSOR_RETURN_URL"] = null;

            this.ViewState["userId"] = userid;
            this.ViewState["userPwd"] = user_pwd.Text;
            this.ViewState["returnUrl"] = returnUrl;
            ph_ex_frm.Visible = true;
            return;

        }
        this.SendMail(userid, email);
        Response.Redirect("complete");
    }

    void SendMail( string userid, string email ) {

        var args = new Dictionary<string, string> {
                    {"{userId}" , userid}
        };

        Email.Send(HttpContext.Current, Email.SystemSender, new List<string>() { email },
            "[한국컴패션] 회원가입을 축하드립니다.", " /mail/join.html",
            args, null);


    }
}