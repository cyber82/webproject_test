﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="form.aspx.cs" Inherits="m_join_offline_form" MasterPageFile="~/m/main.Master" %>

<%@ MasterType VirtualPath="~/m/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/id.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/email.js"></script>
    <script type="text/javascript" src="/m/join/offline/form.js"></script>

    <script type="text/javascript">
        $(function () {


        });

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="user_id" value="" />

    <div class="wrap-sectionsub">
        <!---->

        <div class="member-join">

            <div class="top-linkgo">
                <a href="/m/login/"><span></span>로그인</a>
                <a href="/m/find/pwd/"><span></span>비밀번호 찾기</a>
            </div>

            <!--메뉴 활성화 상태  :  "selected"-->
            <div class="tab-jointype">
                <ul>
                    <li class="btn_user_group selected" data-user-group="개인"><span>개인회원<br />
                        (간편가입)</span></li>
                    <%--<li class="btn_user_group selected" data-user-group="기존"><span>기존후원자<br />
                        (웹 회원전환)</span></li>--%>
                    <li class="btn_user_group" data-user-group="기업"><span>기업/단체<br />
                        회원</span></li>
                </ul>
            </div>

            <p class="txt-title">기본정보 / 본인확인</p>

            <fieldset class="frm-input">
                <legend>기본정보 / 본인확인 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                            <asp:TextBox runat="server" ID="user_name" placeholder="이름" Style="width: 100%"></asp:TextBox></span>
                        <span class="txt-error" data-id="check_name" style="display: none"></span>

                        <span class="row">
                            <span class="column" style="width: 48%">
                                <asp:DropDownList runat="server" ID="user_birth_yyyy" Style="width: 100%"></asp:DropDownList>
                            </span>
                            <span class="column" style="width: 26%">
                                <asp:DropDownList runat="server" ID="user_birth_mm" Style="width: 100%"></asp:DropDownList>
                            </span>
                            <span class="column" style="width: 26%">
                                <asp:DropDownList runat="server" ID="user_birth_dd" Style="width: 100%"></asp:DropDownList>
                            </span>
                        </span>
                            <span class="txt-error" data-id="birth_check" style="display: none"></span>

                        <span class="row">
                            <asp:TextBox runat="server" data-id="input_phone_no1" ID="user_phone" MaxLength="11" Style="width: 100%" class="number_only" placeholder="휴대폰번호(-없이 입력)"></asp:TextBox>
                        </span>
                        <span class="txt-error" data-id="msg_phone_cert" style="display: none"></span>

                        <span class="row">
                            <asp:TextBox runat="server" ID="user_email" data-id="input_email" Style="width: 100%" MaxLength="100" class="input_type1" placeholder="이메일"></asp:TextBox>
                        </span>
                        <span class="txt-error" data-id="check_user_email" style="display: none"></span>

                        <span class="row">
                            <span class="checkbox-ui">
                                <input type="checkbox" class="css-checkbox" runat="server" id="agree_receive" />
                                <label for="agree_receive" class="css-label">이메일/SMS를 수신하지 않겠습니다.</label>
                            </span>
                        </span>

                    </div>
                </div>
            </fieldset>

            <div class="box-gray">
                <strong>이메일/SMS 수신여부에 동의하신 분에게는  </strong>
                <br />
                <ul class="list-bullet">
                    <li>컴패션 후원과 관련한 필수 안내사항과, 매월 발행되는 ‘  이메일 뉴스레터', 연 1회 발행되는 '오프라인 뉴스레터'의   PDF파일을 메일로 보내드립니다.</li>
                    <li>휴대폰으로 이벤트 및 행사 알림 메시지를 보내드립니다.  (일반 전화번호 제외, 휴대폰번호로만 보내드립니다.)</li>
                </ul>
            </div>

            <p class="txt-title">ID/PW</p>
            <fieldset class="frm-input">
                <legend>ID/PW 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                            <input type="text" id="input_user_id" data-id="input_user_id" maxlength="12" placeholder="아이디" style="width: 100%" />
                            <a class="bt-type10 pos-btn" style="width: 70px;display:none" data-id="btn_check_id">중복검사</a>
                        </span>
                        <span class="txt-error" data-id="msg_user_id" style="display: none"></span>

                        <!--에러메시지 경우에만 출력-->

                        <span class="row">
                            <asp:TextBox runat="server" ID="user_pwd" TextMode="Password" MaxLength="15" placeholder="비밀번호" Style="width: 100%"></asp:TextBox>
                        </span>
                        <span class="row">
                            <asp:TextBox runat="server" ID="re_user_pwd" TextMode="Password" MaxLength="15" placeholder="비밀번호 재확인" Style="width: 100%"></asp:TextBox>
                        </span>
                        <span class="txt-error" data-id="msg_user_pwd" style="display: none"></span>
                    </div>
                </div>
            </fieldset>

            <p class="txt-title topline-title">약관동의</p>
            <div class="box-agree">
                <div class="row">이용약관<a class="bt-type8" target="_blank" href="/m/etc/terms/?app_ex=1">약관 전문 보기</a></div>
                <div class="row">개인정보 수집 및 이용<a class="bt-type8" target="_blank" href="/m/etc/privacy/?app_ex=1">약관 전문 보기</a></div>
            </div>
            <p class="bottomtxt-agree">
                이용 약관 및 개인 정보 이용 안내를 확인하였으며,<br />
                위 내용에 동의합니다.
            </p>

            <div class="wrap-bt">
                <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="bt-type6" Style="width: 100%">동의하고 회원가입</asp:LinkButton>

            </div>

            <p class="txt-bottom">
                회원가입이 어려우시거나 본인확인/인증이 안되시는 경우 한국컴패션으로 연락 주시기 바랍니다.<br />
				후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_gray">info@compassion.or.kr</a>
            </p>

        </div>


        <!--//-->
    </div>


</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
    <asp:PlaceHolder runat="server" ID="ph_ex_frm" Visible="false">
        <form id="exfrm" method="post" action="/login/?action=REFER">
            <input type="hidden" id="userId" name="userId" value="<%:this.ViewState["userId"].ToString() %>" />
            <input type="hidden" id="userPwd" name="userPwd" value="<%:this.ViewState["userPwd"].ToString() %>" />
            <input type="hidden" id="r" name="r" value="<%:this.ViewState["returnUrl"].ToString() %>" />
        </form>

        <script type="text/javascript">
            $(function () {
                $("#exfrm").submit();
            })
        </script>

    </asp:PlaceHolder>
</asp:Content>
