﻿$(function () {
    // 탭
    $(".btn_user_group").click(function () {
        var self = $(this);
        var user_group = self.data("user-group");
        $(".btn_user_group").removeClass("selected");
        self.addClass("selected");

        if (user_group == "기업") {
            location.href = "/m/join/company/";
        } else if (user_group == "개인") {
            location.href = "/m/join/";
        } else if (user_group == "기존") {
            location.href = "/m/join/offline/";
        };

        return false;
    })

    // 휴대폰 인증
    $("[data-id=btn_cert_by_phone]").click(function () {
    	
        cert_openPopup("phone");

        return false;
    })

    // 아이핀 인증
    $("[data-id=btn_cert_by_ipin]").click(function () {
        cert_openPopup("ipin");

        return false;
    })

    $("#ic-close").click(function () {
        $(".modal_alert").hide();
    })

});



// 본인인증 결과 응답
// result = Y or N , birth = yyyyMMdd
// sex = M or F
// method = ipin or phone
var cert_setCertResult = function (method, result, ci, di, name, birth, sex) {

	if (result != "Y") {
		alert("인증에 실패했습니다.");
		return;
	}
	checkCI(method, ci);
}

var checkCI = function (method, ci) {
	console.log(ci);
	$.get("/api/join.ashx", { t: "find-offuser", c: ci }, function (r) {

		if (r.success) {
			location.href = "form";
		} else {

			if (r.action == "exist") {
			    if (confirm(r.message + "\n" + "아이디 : " + r.data.user_id)) {
			        location.href = "/m/login/";
			    } else {
			        return false;
			    }
			} else if (r.action == "notfound") {
				$(".modal_alert").show();
			} else {
				alert(r.message);
			}
		}

	});

	return false;

}
