﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="m_join_offline_default" MasterPageFile="~/m/main.Master"%>
<%@ MasterType virtualpath="~/m/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/cert/cert.js"></script>
    <script type="text/javascript" src="/m/join/offline/default.js"></script>
	<script type="text/javascript">


	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">


    <div class="wrap-sectionsub">
<!---->
    
    <div class="member-join">

        <div class="top-linkgo">
            <a href="/m/login/"><span></span>로그인</a>
            <a href="/m/find/pwd/"><span></span>비밀번호 찾기</a>
        </div>

        <!--메뉴 활성화 상태  :  "selected"-->
        <div class="tab-jointype">
            <ul>
             <li class="btn_user_group" data-user-group="개인"><span>개인회원<br />(간편가입)</span></li>
             <li class="btn_user_group selected" data-user-group="기존"><span>기존후원자<br />(웹 회원전환)</span></li>
             <li class="btn_user_group" data-user-group="기업"><span>기업/단체<br />회원</span></li>
            </ul>
        </div>
        
        <p class="txt-title">본인인증</p>
		
		<div class="self-account">
        	<p class="txt1">
            	<strong>컴패션 후원자님!</strong><br />
            	웹회원으로 전환하시면 후원내역관리 및
                후원 어린이에게 편지쓰기, 기부금 영수증 출력 등의
                서비스를 이용하실 수 있습니다.
            </p>
            <div class="link-account">
                <a class="btn_s_type2" data-id="btn_cert_by_phone"><span>휴대폰 인증</span></a>
                <a class="btn_s_type2" data-id="btn_cert_by_ipin"><span>아이핀(i-PIN) 인증</span></a>
            </div>
            <div class="box-gray">
            	<ul class="list-bullet2">
                	<li>본인명의의 휴대폰으로만 본인인증이 가능합니다.<br />
					아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다. </li>
				</ul>
            </div>
        </div>

		<p class="txt-bottom">회원가입이 어려우시거나 본인확인/인증이 안되시는 경우 한국컴패션으로 연락 주시기 바랍니다.<br />
			후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_gray">info@compassion.or.kr</a>
		</p>
        
	</div>
        
    
<!--//-->
</div>

    <!--레이어 안내 팝업-->
<div style="position:absolute;width:80%;left:10%;top:60px;z-index:991;display:none" class="modal_alert">  <!--페이지 임시좌표-->
    
    <div class="wrap-layerpop">
        <div class="header">
        	<p class="txt-title">컴패션 후원자로 일치하는 정보가 없습니다.</p>
        </div>
        <div class="contents">
        	<strong>일반 개인회원으로 가입됩니다.</strong><br />
            
            <div class="wrap-bt"><a href="/m/join/" class="bt-type6">회원가입 계속하기</a></div>
            
			<p class="mb10 color1">1) 최근 결연서를 통해 후원신청을 하셨나요?</p>
			후원자님의 후원신청이 등록 중에 있습니다. 먼저 웹회원으로 가입하시면 1~2주 안에 로그인 후 마이컴패션에서 후원내역을 확인하실 수 있습니다.<br /><br />

			<p class="mb10 color1">2) 현재 후원 중이신데 일치하는 정보가 없다고 확인되시나요?</p>
			먼저 웹회원으로 가입하신 후 후원지원팀으로 연락주시면 후원정보를 등록해드리도록 하겠습니다.<br /><br /><br />

			후원지원팀 (<a href="tel:02-740-1000" class="color3">02-740-1000</a> 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="color3">info@compassion.or.kr</a>
			
        </div>
        <div class="close"><span class="ic-close" id="ic-close" onClick="">레이어 닫기</span></div>
    </div>
    
</div>    
    <div class="modal_alert" style="width:100%;height:100%;position:fixed;left:0;top:0;z-index:990;opacity:0.8;background:#000;display:none">  </div>
<!--//레이어 안내 팝업-->




</asp:Content>
