﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="m_join_complete" MasterPageFile="~/m/main.Master"%>
<%@ MasterType virtualpath="~/m/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript">
	    $(function () {

		});
	    
	    history.pushState("complete", "complete", "complete");

	    window.onpopstate = function (event) {
	        location.href = "/m/login";
	    };
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub">
<!---->
    
    <div class="member-result">
    	<strong class="txt-commit">감사합니다.<br />
            한국컴패션 웹사이트 회원으로<br />
            가입이 완료되었습니다.
		</strong>
        
        <div class="wrap-bt">
            <a runat="server" id="btn_main" class="bt-type6" style="width:100%">메인 페이지로 이동</a>

        </div>
        <div class="linebar"></div>
        
        <div class="box-gray">
        	기존에 한국컴패션 후원하신 내역이 있으신가요?<br />
			후원내역을 연결하여 회원님의 후원정보를 확인해보세요.
			<div class="wrap-bt"><a runat="server" id="btn_commitment" class="bt-type5" style="width:50%" href="#">기존 후원내역 확인</a></div>
        </div>
        
        
    </div>
    
<!--//-->
</div>



    
  
</asp:Content>
