﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="m_join_company_default" MasterPageFile="~/m/main.Master" %>

<%@ MasterType VirtualPath="~/m/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript" src="/assets/jquery/wisekit/validation/id.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/email.js"></script>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript" src="/m/join/company/default.js"></script>

    <script type="text/javascript">
        $(function () {
            var uploader = attachUploader("btn_file_path");
            uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_company)%>";
        	uploader._settings.data.fileType = "image";
	        uploader._settings.data.rename = "y";
	        uploader._settings.data.limit = 2048;

        });




    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" runat="server" id="user_id" value="" />
    <input type="hidden" runat="server" id="user_identification_method" value="" />
    <input type="hidden" runat="server" id="user_email" value="" />
    <input type="hidden" runat="server" id="user_phone" value="" />
    <input type="hidden" runat="server" id="user_mobile" value="" />

    <input type="hidden" runat="server" id="comp_register_no" value="" />
    <input type="hidden" runat="server" id="file_path" value="" />
    <input type="hidden" runat="server" id="user_group" value="" />

    <div class="wrap-sectionsub">
        <!---->

        <div class="member-join">

            <div class="top-linkgo">
                <a href="/m/login/"><span></span>로그인</a>
                <a href="/m/find/pwd/"><span></span>비밀번호 찾기</a>
            </div>

            <!--메뉴 활성화 상태  :  "selected"-->
            <div class="tab-jointype">
                <ul>
                    <li class="btn_tap_group" data-tap-group="개인"><span>개인회원<br />
                        (간편가입)</span></li>
                    <%--<li class="btn_tap_group" data-tap-group="기존"><span>기존후원자<br />
                        (웹 회원전환)</span></li>--%>
                    <li class="btn_tap_group selected" data-tap-group="기업"><span>기업/단체<br />
                        회원</span></li>
                </ul>
            </div>

            <p class="txt-title">기업/단체정보</p>
            <div class="tab-column-3">
                <a class="btn_user_group selected" data-placeholder="기업명" data-user-group="기업"><span>기업</span></a>
                <a class="btn_user_group" data-placeholder="단체명" data-user-group="단체"><span>단체</span></a>
                <a class="btn_user_group" data-placeholder="교회명" data-user-group="교회"><span>교회</span></a>
            </div>



            <fieldset class="frm-input">
                <legend>기업/단체정보 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                            <asp:TextBox runat="server" ID="user_name" Style="width: 100%" placeholder="기업명"></asp:TextBox>
                        </span>
                        <span class="txt-error" data-id="check_company_name" style="display: none"></span>
                        <span class="row pos-relative2">
                            <input type="text" id="company_num" data-id="comp_register_no" maxlength="10" class="number_only" placeholder="사업자번호(-없이 입력)" style="width: 100%" />
                            <a class="bt-type10 pos-btn" style="width: 70px" data-id="btn_check_comp_register_no">중복검사</a>

                        </span>
                        <span class="txt-error" data-id="msg_comp_register_no" style="display: none"></span>
                    </div>
                </div>
            </fieldset>

            <p class="txt-title">담당자 정보 및 본인확인</p>
            <fieldset class="frm-input">
                <legend>담당자 정보 및 본인확인 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                              <asp:TextBox runat="server" ID="manager_name" placeholder="담당자 이름" style="width: 100%"></asp:TextBox>
                        </span>
                                <span class="txt-error" data-id="check_manager_name" style="display: none"></span>
                                <span class="txt-result" data-id="check_cert" style="display: none"></span>

                        <span class="row">
                            <span class="checkbox-ui">
                                <input type="checkbox" class="css-checkbox" id="chk1" />
                                <label for="chk1" class="css-label" style="color: #444">휴대폰 없음</label>
                            </span>
                        </span>

                        <!--휴대폰 없음(기본)-->
                        <span class="row pos-relative1 have_phone">
                            <input type="text" id="phone" data-id="input_phone_no1" class="number_only" maxlength="11" placeholder="휴대폰번호(-없이 입력)" style="width: 100%" />
                            <a class="bt-type9 pos-btn hide_phone_cert" style="width: 50px" data-id="btn_phone_no1">인증</a>
                        </span>
                        <span class="txt-result" data-id="msg_phone_cert" style="display: none"></span>
                        <span class="row pos-relative1 have_phone hide_phone_cert" id="cert_no" style="display:none">
                            <input type="text" id="cert" data-id="input_phone_confirm" placeholder="인증번호" style="width: 100%" class="number_only"/>
                            <a class="bt-type9 pos-btn" style="width: 50px" data-id="btn_phone_confirm">확인</a>
                        </span>
                        <span class="row have_phone">
                            <input type="text" id="email" data-id="input_email1" placeholder="이메일" style="width: 100%" />
                            <span class="txt-error" data-id="msg_email1_cert" style="display: none"></span>
                        </span>
                        <!--//휴대폰 없음(기본)-->

                        <!--휴대폰 없음(체크상태)-->
                        <span class="row pos-relative3 no_phone" style="display: none">
                            <input type="text" id="email_2" data-id="input_email2" placeholder="이메일" style="width: 100%" /><br />
                            <a class="bt-type9 pos-btn hide_mail_cert" style="width: 50px" data-id="btn_email2">인증</a>
                            <a class="bt-type9 pos-btn2 hide_mail_cert" style="width: 50px" data-id="btn_email_confirm">확인</a>
                        </span>
                        <span class="txt-result" data-id="check_user_email" style="display: none"></span>
                        <p class="txt-bottom no_phone" style="display: none" style="margin-top: 9px">입력하신 이메일에서 인증코드를 클릭하셨으면 [확인] 버튼을 클릭해주세요.</p>
                        <span class="row no_phone" style="display: none">
                            <input type="text" id="phone_2" data-id="input_phone_no2" maxlength="11" class="number_only" placeholder="전화번호(-없이 입력)" style="width: 100%" />
                        </span>
                        <span class="txt-result" data-id="check_user_phone" style="display: none"></span>
                        <!--//휴대폰 없음(체크상태)-->

                        <span class="row">
                            <span class="checkbox-ui">
                                <input type="checkbox" class="css-checkbox" runat="server" id="agree_receive" />
                                <label for="agree_receive" class="css-label">이메일/SMS를 수신하지 않겠습니다.</label>
                            </span>
                        </span>

                    </div>
                </div>
            </fieldset>

            <div class="box-gray">
                <strong>이메일/SMS 수신여부에 동의하신 분에게는  </strong>
                <br />
                <ul class="list-bullet">
                    <li>컴패션 후원과 관련한 필수 안내사항과, 매월 발행되는 ‘  이메일 뉴스레터', 연 1회 발행되는 '오프라인 뉴스레터'의   PDF파일을 메일로 보내드립니다.</li>
                    <li>휴대폰으로 이벤트 및 행사 알림 메시지를 보내드립니다.  (일반 전화번호 제외, 휴대폰번호로만 보내드립니다.)</li>
                </ul>
            </div>

            <p class="txt-title nobullet">첨부파일</p>
            <fieldset class="frm-input">
                <legend>첨부파일 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row pos-relative2">
                              <input type="text" runat="server" data-id="file_path" placeholder="사업자등록증을 첨부해주세요." style="width: 100%" readonly="readonly" />
                                    <a href="#" class="bt-type10 pos-btn" style="width: 70px" id="btn_file_path">파일찾기</a>
                        </span>
                    </div>
                </div>
            </fieldset>
            <ul class="list-bullet2 lineheight1">
                <li>사업자등록증을 첨부해주세요.</li>
                <li>첨부파일 용량을 2MB이하로 해주세요.</li>
                <li>첨부파일은 이미지파일(jpg , jpeg , gif , png) 만 가능합니다.</li>
            </ul>


            <p class="txt-title">ID/PW</p>
            <fieldset class="frm-input">
                <legend>ID/PW 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                                 <input type="text" data-id="input_user_id" placeholder="아이디" style="width: 100%" maxlength="12" />
                            <a class="bt-type10 pos-btn" style="width: 70px;display:none" data-id="btn_check_id">중복검사</a></span>
                                <span class="txt-error" data-id="msg_user_id" style="display: none"></span>
                        <!--에러메시지 경우에만 출력-->

                        <span class="row">
                            <asp:TextBox runat="server" ID="user_pwd" TextMode="Password" MaxLength="15" placeholder="비밀번호" style="width: 100%"></asp:TextBox>
                        </span>
                        <span class="row">
                             <asp:TextBox runat="server" ID="re_user_pwd" TextMode="Password" MaxLength="15" placeholder="비밀번호 재확인" style="width: 100%"></asp:TextBox>
                        </span>
                                <span class="txt-error" data-id="msg_user_pwd" style="display: none"></span>
                    </div>
                </div>
            </fieldset>

            <p class="txt-title topline-title">약관동의</p>
            <div class="box-agree">
                <div class="row">이용약관<a class="bt-type8" target="_blank" href="/m/etc/terms/?app_ex=1">약관 전문 보기</a></div>
                <div class="row">개인정보 수집 및 이용<a class="bt-type8" target="_blank" href="/m/etc/privacy/?app_ex=1">약관 전문 보기</a></div>
            </div>
            <p class="bottomtxt-agree">이용 약관 및 개인 정보 이용 안내를 확인하였으며,<br />
                위 내용에 동의합니다.</p>

            <div class="wrap-bt">
                <asp:LinkButton runat="server" class="bt-type6" style="width: 100%" ID="btn_submit" OnClick="btn_submit_Click">동의하고 회원가입</asp:LinkButton>
            </div>

			<p class="txt-bottom">
                회원가입이 어려우시거나 본인확인/인증이 안되시는 경우 한국컴패션으로 연락 주시기 바랍니다.<br />
				후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_gray">info@compassion.or.kr</a>
            </p>

		</div>

            


	</div>

<!--//-->



</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
    <asp:PlaceHolder runat="server" ID="ph_ex_frm" Visible="false">
        <form id="exfrm" method="post" action="/login/?action=REFER">
            <input type="hidden" id="userId" name="userId" value="<%:this.ViewState["userId"].ToString() %>" />
            <input type="hidden" id="userPwd" name="userPwd" value="<%:this.ViewState["userPwd"].ToString() %>" />
            <input type="hidden" id="r" name="r" value="<%:this.ViewState["returnUrl"].ToString() %>" />
        </form>

        <script type="text/javascript">
            $(function () {
                $("#exfrm").submit();
            })
        </script>

    </asp:PlaceHolder>
</asp:Content>
