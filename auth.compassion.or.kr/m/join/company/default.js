﻿$(function () {
    $page.init();

    //기업명 초기화
    $("#user_name").keyup(function () {
        if ($("#user_name").val() != "") {
            $("[data-id=check_company_name]").hide();
        }
    });

    //사업자번호 초기화
    $("#company_num").keyup(function () {
        if ($("#company_num").val() != "") {
            $("[data-id=msg_comp_register_no]").hide();
        }
    });

    //담당자이름 초기화
    $("#manager_name").keyup(function () {
        if ($("#manager_name").val() != "") {
            $("[data-id=check_manager_name]").hide();
        }
    });

    //생년월일 초기화
    $(".sel_type1").change(function () {
        if ($("#user_birth_yyyy").val() != "" && $("#user_birth_mm").val() != "" && $("#user_birth_dd").val() != "") {
            $("[data-id=birth_check]").hide();
        }
    });

    //휴대폰 초기화 

    $("#phone").keyup(function () {
        if ($("#phone").val() != "") {
            $("[data-id=msg_phone_cert]").hide();
        }
    });


    //이메일 초기화 

    $("#email").keyup(function () {
        if ($("#email").val() != "") {
            $("[data-id=msg_email1_cert]").hide();
        }
    });

    //이메일인증시 초기화 

    $("#email_2").keyup(function () {
        if ($("#email_2").val() != "") {
            $("[data-id=check_user_email]").hide();
        }
    });

    $("[data-id=input_phone_no2]").keyup(function () {
        if ($("[data-id=input_phone_no2]").val() != "") {
            $("[data-id=check_user_phone]").hide();
        }
    });


    //아이디 초기화

    $("[data-id=input_user_id]").keyup(function () {
        if ($("[data-id=input_user_id]").val() != "") {
            $("[data-id=msg_user_id]").hide();
        }
    });

    //비밀번호 초기화

    $("#user_pwd").keyup(function () {
        if ($("user_pwd").val() != "") {
            $("[data-id=msg_user_pwd]").hide();
        }
    });

    $("#re_user_pwd").keyup(function () {
        if ($("re_user_pwd").val() != "") {
            $("[data-id=msg_user_pwd]").hide();
        }
    });


    setTimeout(function () {
        $("[name=userfile]").attr("accept", ".jpg, .gif, .png, .jpeg")
    }, 300)

});

var attachUploader = function (button) {
    return new AjaxUpload(button, {
        action: '/common/handler/upload.ashx',
        responseType: 'json',
        onChange: function (file) {
            var fileName = file.toLowerCase();

            if (fileName.indexOf(".jpg") != -1 || fileName.indexOf(".jpeg") != -1 || fileName.indexOf(".gif") != -1 || fileName.indexOf(".png") != -1) {
                return true;
            } else {
                alert("첨부파일은 이미지파일(jpg, jpeg, gif, png) 만 가능합니다.");
                return false;
            }
        },
        onSubmit: function (file, ext) {
            this.disable();
        },
        onComplete: function (file, response) {

            this.enable();
            console.log(file, response);
            if (response.success) {
                $("#file_path").val(response.name);
                $("[data-id=file_path]").val(response.name.replace(/^.*[\\\/]/, ''));
                //	$(".temp_file_size").val(response.size);

            } else
                alert(response.msg);
        }
    });
}

var $page = {

	timer: null,

	init: function () {

		// 사업자등록번호 중복검사
		this.setCompRegistrationNoEvent();

		// 회원아이디 체크 이벤트
		this.setUserIdEvent();
		
		// 본인인증-휴대폰 이벤트
		this.setIdentificationByPhoneEvent();

		// 본인인증-이메일 이벤트
		this.setIdentificationByEmailEvent();
		
		// 가입하기 버튼
		$("#btn_submit").click(function () {

			return $page.onSubmit();

		});
		$("button.open").click(function () {
		    $(".tooltip").fadeIn();
		    return false;
		});
		$("button.close").click(function () {
		    $(".tooltip").fadeOut();
		    return false;
		});

		// 탭1
		$(".btn_user_group").click(function () {
			var self = $(this);
			var placeholder = self.data("placeholder");
			var user_group = self.data("user-group");

			$(".btn_user_group").removeClass("selected");
			self.addClass("selected");
			$("input").val("");
			$("#user_name").attr("placeholder" , placeholder);
			$("#user_group").val(user_group);
			return false;
		})

	    // 탭
		$(".btn_tap_group").click(function () {
		    var self = $(this);
		    var user_group = self.data("tap-group");
		    $(".btn_tap_group").removeClass("selected");
		    self.addClass("selected");

		    if (user_group == "기업") {
		        location.href = "/m/join/company/";
		    } else if (user_group == "개인") {
		        location.href = "/m/join/";
		    } else if (user_group == "기존") {
		        location.href = "/m/join/offline/";
		    };
		    //console.log(user_group)
		    //console.log(self)

		    return false;
		})

		//this.selectType();

		$("#chk1").click(function () {
		    if ($("#chk1").is(":checked")) {
		        $(".no_phone").show();
		        $(".no_phone input").val("");
		        $(".have_phone").hide();
		        $("[data-id=msg_phone_cert]").hide();
		    } else {
		        $(".no_phone").hide();
		        $(".have_phone input").val("");
		        $(".have_phone").show();
		        $("[data-id=check_user_email]").hide();
		    }
		});

		
	},

	// 확인
	onSubmit: function () {

	    if ($("#user_name").val() == "") {
	        $("[data-id=check_company_name]").html("기업/단체/교회명을 입력해주세요").addClass("txt-error").show();
	        $("#user_name").focus();
	        return false;
	    }
	
		if ($("[data-id=comp_register_no]").val() == "") {
		    $("[data-id=msg_comp_register_no]").html("사업자등록번호를 입력해 주세요").addClass("txt-error").show();
			$("[data-id=comp_register_no]").focus();
			return false;
		}

		if ($("#comp_register_no").val() == "") {
		    $("[data-id=msg_comp_register_no]").html("사업자등록번호 중복확인을 해주세요").addClass("txt-error").show();
		    
		    $("#company_num").focus();
			return false;
		}

		if ($("#manager_name").val() == "") {
		    $("[data-id=check_manager_name]").html("담당자 이름을 입력해 해주세요").addClass("txt-error").show();
		    $("#manager_name").focus();
		    return false;
		}


		// 본인인증
		if ($("#user_identification_method").val() == "") {
		    $("[data-id=check_cert]").removeClass("txt-result").show();
		    $("[data-id=check_cert]").html("본인확인을 해주세요.").addClass("txt-error").show();
		    scrollTo($("[data-id=check_cert]"));
			return false;
		}

		if ($("#user_identification_method").val() == "phone") {

		    var check_email1 = emailValidation.check($("[data-id=input_email1]").val());
		    if (!check_email1.result) {
		        $("[data-id=msg_email1_cert]").html(check_email1.msg).show();
		        $("[data-id=input_email1]").focus()
		        return false;
		    }

			$("#user_email").val($("[data-id=input_email1]").val());
		} else if ($("#user_identification_method").val() == "email") {

		    if (!/^[0-9.]{10,11}$/.test($("[data-id=input_phone_no2]").val())) {
		        $("[data-id=check_user_phone]").removeClass("txt-result");
			    $("[data-id=check_user_phone]").html("전화번호를 정확히 입력해주세요.").addClass("txt-error").show();
				$("[data-id=input_phone_no2]").focus();
				return false;
			}

			$("#user_phone").val($("[data-id=input_phone_no2]").val());
		}

	    // 아이디
	    // todo : 중복검사 , 아이디유효성 검사
		if ($("#user_id").val() == "") {
		    $("[data-id=msg_user_id]").html("6~12자 이내의 영문 소문자,숫자 조합으로 입력해주세요").addClass("txt-error").show();
		    $("[data-id=input_user_id]").focus();
		    return false;
		}
		var id_result = idValidation.check($("[data-id=input_user_id]"), 6, 12);
		if (!id_result.result) {
		    $("[data-id=msg_user_id]").removeClass("txt-result");
		    $("[data-id=msg_user_id]").html(id_result.msg).addClass("txt-error").show();
		    return false;
		}
		$("#user_id").val($("[data-id=input_user_id]").val());

	    // 비번
		var pwd_result = passwordValidation.check($("#user_pwd"), $("#re_user_pwd"), 5, 15, $("[data-id=input_user_id]"));
		if (!pwd_result.result) {
		    $("#user_pwd").focus();
		    $("[data-id=msg_user_pwd]").html(pwd_result.msg).show();

		    if (pwd_result.msg.indexOf("확인") == -1) {
		        $("#user_pwd").focus();
		    } else {
		        $("#re_user_pwd").focus();
		    }

		    return false;
		}

		return true;
	},

    // 사업자등록번호 중복검사
	setCompRegistrationNoEvent: function () {

	    $("[data-id=btn_check_comp_register_no]").click(function () {
            
	        var comp_name = $("#user_name").val();
	        var val = $("[data-id=comp_register_no]").val();
	        var msg = $("[data-id=msg_comp_register_no]");
	        if (comp_name == "") {
	            $("[data-id=check_company_name]").removeClass("txt-result");
	            $("[data-id=check_company_name]").html("기업/단체/교회명을 입력해주세요").addClass("txt-error").show();
	            $("#user_name").focus();
	            return false;
	        }

	        if ($("#company_num").val() == "") {
	            $("[data-id=msg_comp_register_no]").removeClass("txt-result");
	            $("[data-id=msg_comp_register_no]").html("사업자등록번호를 입력해 주세요").addClass("txt-error").show();
	            $("#company_num").focus();
	            return false;
	        }

	        if ($("#company_num").val().length < 10) {
	            $("[data-id=msg_comp_register_no]").removeClass("txt-result");
	            $("[data-id=msg_comp_register_no]").html("사업자등록번호를 정확히 입력해 주세요").addClass("txt-error").show();
	            $("#company_num").focus();
	            return false;
	        }
	        $.get("/api/join.ashx", { t: "exist-comp-register-no", c: val, n: comp_name }, function (r) {
	            
	            if (r.success) {
	                $("[data-id=msg_comp_register_no]").removeClass("txt-error").removeClass("txt-result");
	                if (r.data.exist) {

	                	if (r.data.confirm) {

	                	    $("[data-id=msg_comp_register_no]").addClass("txt-error").show();
	                		msg.html("이미 가입된 사업자등록번호 입니다.");
	                	} else {

	                	    $("[data-id=msg_comp_register_no]").addClass("txt-error").show();
	                		msg.html("승인대기중인 기업회원 입니다.");
	                	}
	                } else {
	                    $("[data-id=msg_comp_register_no]").addClass("txt-result").show();
	                    $("#comp_register_no").val(r.data.comp_register_no);
	                    msg.html("등록가능한 사업자 등록번호 입니다.");
	                }
	                
	            } else {
	                alert(r.message);
	            }

	        });

	        return false;
	    })
	},

	// 본인인증-휴대폰 이벤트
	setIdentificationByPhoneEvent: function () {

	    $("[data-id=input_phone_no1]").keyup(function () {
	        $("[data-id=msg_phone_cert]").hide();
	        $(".hide_phone_cert").show();
	        $("#user_identification_method").val("");
	    })

		$("[data-id=btn_identification_by_phone]").click(function () {

			$("[data-id=pn_identification_by_phone]").show();
			$("[data-id=pn_identification_by_email]").hide();

			return false;
		})

		// 인증요청
		$("[data-id=btn_phone_no1]").click(function () {

			var phone = $("[data-id=input_phone_no1]").val();
			var name = $("#manager_name").val();
			if (name == "") {
				$("#manager_name").focus();
				$("[data-id=check_manager_name]").html("담당자 이름을 입력해 해주세요").addClass("txt-error").show();
				return false;
			}
			
			if (!/^[0-9.]{10,11}$/.test(phone)) {
			    $("[data-id=msg_phone_cert]").removeClass("txt-result");
			    $("[data-id=msg_phone_cert]").html("휴대폰번호를 정확히 입력해주세요.").addClass("txt-error").show();
				$("[data-id=input_phone_no1]").focus();
				return false;
			}
			$("#cert_no").show();
			$("#user_mobile").val(phone);

			if ($page.timer != null)
				window.clearTimeout($page.timer);

			// 연속클릭방지
			$page.timer = window.setTimeout(function () {

				$.get("/api/find.ashx", { t: "send-phone", c: phone, n: name }, function (r) {
				    $("[data-id=msg_phone_cert]").removeClass("txt-result").removeClass("txt-error").show();
					$page.timer = null;
					if (r.success) {
					    $("[data-id=msg_phone_cert]").html("문자메세지를 발송하였습니다.").addClass("txt-result").show();
					} else {
					    $("[data-id=msg_phone_cert]").html("문자메세지 발송에 실패했습니다.").addClass("txt-result").show();;
					}

				});

			}, 1000);

			return false;
		});

		// 인증확인
		$("[data-id=btn_phone_confirm]").click(function () {
		    $("[data-id=msg_phone_cert]").removeClass("txt-result").removeClass("txt-error");
			var code = $("[data-id=input_phone_confirm]").val();

			$.get("/api/find.ashx", { t: "check-phone", c: code }, function (r) {

			    if (r.success) {
			        $("#user_identification_method").val("phone");		// 인증이 완료되면 인증수단 저장
			        $("#cert_no , .hide_phone_cert").hide();
			        $("[data-id=msg_phone_cert]").html("인증이 완료되었습니다");
			        $("[data-id=msg_phone_cert]").addClass("txt-result");
			        $("[data-id=check_cert]").hide();
			        $("#chk1").attr("disabled", true);
			    } else {
			        $("[data-id=msg_phone_cert]").html("인증이 완료되지 않았습니다");
			        $("[data-id=msg_phone_cert]").addClass("txt-error");
			    }

			});

			return false;
		});

	},

	// 보인인증-이메일 이벤트
	setIdentificationByEmailEvent : function(){
		$("[data-id=btn_identification_by_email]").click(function () {

			$("[data-id=pn_identification_by_phone]").hide();
			$("[data-id=pn_identification_by_email]").show();

			return false;
		});

		// 인증요청
		$("[data-id=btn_email2]").click(function () {

			var email = $("[data-id=input_email2]").val();

			var check_email2 = emailValidation.check($("[data-id=input_email2]").val());
			if (!check_email2.result) {
			    $("[data-id=check_user_email]").removeClass("txt-result");
			    $("[data-id=check_user_email]").html(check_email2.msg).addClass("txt-error").show();
			    $("[data-id=input_email2]").focus();
			    return false;
			}
			
			$("#user_email").val(email);

			if ($page.timer != null)
				window.clearTimeout($page.timer);

			// 연속클릭방지
			$page.timer = window.setTimeout(function () {

				$.get("/api/find.ashx", { t: "send-email", c: email }, function (r) {

					$page.timer = null;
					if (r.success) {
					    $("[data-id=check_user_email]").removeClass("txt-error")
					    $("[data-id=check_user_email]").html("메일을 발송하였습니다.").addClass("txt-result").show();
						
					} else {
					    $("[data-id=check_user_email]").html().addClass("txt-result").show();
					}

				});

			}, 1000);

			return false;
		});

		// 인증확인
		$("[data-id=btn_email_confirm]").click(function () {
		    $("[data-id=check_user_email]").removeClass("txt-result").removeClass("txt-error");
			var email = $("#user_email").val();
			
			$.get("/api/find.ashx", { t: "check-email", c: email }, function (r) {

				if (r.success) {
					$("#user_identification_method").val("email");		// 인증이 완료되면 인증수단 저장
					$(".hide_mail_cert").hide();
					$("[data-id=check_user_email]").html("인증이 완료되었습니다").addClass("txt-result").show();
					$("[data-id=check_cert]").hide();
					$("#chk1").attr("disabled", true);
                } else {
                    $("[data-id=check_user_email]").html("인증이 완료되지 않았습니다").addClass("txt-error").show();
                }

			});

			return false;
		});
		
	} , 

	// 회원 아이디 체크
	setUserIdEvent: function () {

		var input = $("input[data-id=input_user_id]");
		var msg = $("[data-id=msg_user_id]");

		$("[data-id=input_user_id]").focusout(function () {

		    $("[data-id=msg_user_id]").css("display", "none")
		    msg.html("");
		    check();
		});
        /*
		input.bind("keyup", function () {
		    $("[data-id=msg_user_id]").css("display", "none")
			msg.html("");
		});

		input.search({
			autoCompleteMinLength: 6,
			checkDefault: "no",
			empty: "",
			msg: "",
			onSearch: function (sender) {
				check();
				return false;
			},

			onAutoComplete: function (arg, sender) {
				check();
			},

			button: $("[data-id=btn_check_id]")
		});
        */
		var check = function () {

			$("#user_id").val("");

			var id_result = idValidation.check(input, 6, 12);
			    $("[data-id=msg_user_id]").removeClass("txt-result");
			if (!id_result.result) {
			    $("[data-id=msg_user_id]").addClass("txt-error").show();
				msg.html(id_result.msg);
				return;
			}
			var user_id = input.val();
			$.get("/api/join.ashx", { t: "exist-id", c: user_id }, function (r) {
				
			    if (r.success) {
			        $("[data-id=msg_user_id]").removeClass("txt-error").removeClass("txt-result");
				    if (r.data.exist) {
				        $("[data-id=msg_user_id]").addClass("txt-error").show();
						msg.html("이미 사용중입니다.");
					} else {
				        $("#user_id").val(r.data.user_id);
				        $("[data-id=msg_user_id]").addClass("txt-result").show();
						msg.html("사용 가능합니다.");
					}
					
				} 

			});

		}

	},

	selectType: function () {
	    
	}
}