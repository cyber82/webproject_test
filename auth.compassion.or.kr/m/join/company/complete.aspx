﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="m_join_company_complete" MasterPageFile="~/m/main.Master"%>
<%@ MasterType virtualpath="~/m/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript">
	    $(function () {

		});
	    history.pushState("complete", "complete", "complete");

	    window.onpopstate = function (event) {
	        location.href = "/m/login";
	    };
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub">
<!---->
    
    <div class="member-result">
    	<strong class="txt-commit">감사합니다.<br />
            한국컴패션 웹사이트 회원가입<br />
            신청접수가 완료되었습니다.
		</strong>
        
        <div class="linebar"></div>
       <p class="txt1">기업/단체회원은 가입정보 확인이 필요하여,<br />
		별도로 한국컴패션의 승인 후에 로그인이 가능합니다.<br />
		승인이 완료되면 등록해주신 이메일로 안내해드리겠습니다.
		</p>
        <div class="wrap-bt">
            <a runat="server" id="btn_main" class="bt-type6" style="width:100%">메인 페이지로 이동</a>

        </div>
        <dl class="list-dic">
        	<dt>기업/단체 회원가입 문의</dt>
            <dd>후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_gray">info@compassion.or.kr</a></dd>
        </dl>
        
        
    </div>
    
<!--//-->
</div>


</asp:Content>
