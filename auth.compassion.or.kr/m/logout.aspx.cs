﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.UI.HtmlControls;
using CommonLib;
public partial class m_logout : FrontBasePage {
	protected override void OnBeforePostBack() {

		var return_url = Request.QueryString["r"].ValueIfNull(ConfigurationManager.AppSettings["default_return_url_m"]);
		this.ViewState["r"] = return_url;

		new AuthUser().Logout();
		
		foreach(string k in new AuthenticationAction().sites.Select(p => p.logout_url)) {
			HtmlImage img = new HtmlImage();
			img.Attributes["src"] = k;
			img.Style["display"] = "none";
			ph_setcookie.Controls.Add(img);
		}

		ph_setcookie.Visible = true;

	}
	
	
}