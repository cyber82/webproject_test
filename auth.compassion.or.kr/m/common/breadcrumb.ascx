﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="breadcrumb.ascx.cs" Inherits="Common.breadcrumb" %>
<%@OutputCache Duration="36000" varybyparam="none" varybycustom="url"  %>
<asp:PlaceHolder runat="server" ID="mobileweb" Visible="false">
	<script type="text/javascript">
	
		$(function () {
			
			$("div.title-depth .backgo").click(function () {
				if ($("#btnList").length > 0) {
				
					location.href = $("#btnList").attr("href");
					return false;
				} else {
					return true;
				}
			
			})
		
			var isShowMenu = location.pathname.startsWith("/my/");
			if (isShowMenu) {
				$(".mymenu").show();
			}

			$("div.title-depth .mymenu").click(function () {
				$(".sub-mymenu").toggle();
				return false;
			})

			if (location.pathname.startsWith("/my/") ) {

				var tpl = $("#mymenu_tpl").text();
				$(".wrap-sectionsub-my").prepend(tpl);
			}
		})
	
	</script>
	 <div class="title-depth">
		<h2 runat="server" id="title"></h2>
		 <a class="mymenu" style="display:none" href="#">마이메뉴</a>
		<a class="backgo" runat="server" id="link" href="#">뒤로가기</a>
	</div>

	<textarea id="mymenu_tpl" style="display:none">
		<div class="sub-mymenu">
    		<ul>
        		<li><a href="/my/">마이컴패션 홈</a></li>
				<li><a href="/my/children/">나의 어린이</a></li>
				<li><a href="/my/sponsor/commitment/">후원 관리</a></li>
				<li><a href="/my/user-funding/create">나의 펀딩 관리</a></li>
				<li><a href="/my/activity/CAD">나의 참여/활동</a></li>
				<li><a href="/my/store/order/">스토어 주문내역</a></li>
				<li><a href="/my/qna/">문의내역</a></li>
			</ul>
		</div>
	</textarea>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="app" Visible="false">
	<script type="text/javascript">
	
		$(function () {
			
			$("div.appPageNav .back").click(function () {
				if ($("#btnList").length > 0) {
					location.href = $("#btnList").attr("href");
					return false;
				} else {
					return true;
				}
			})
		
		
		})
	
	</script>
	
	<section class="appPageNav" >
		<div class="left"><a class="back backgo" runat="server" id="link2"  href="#"></a></div>
		<div class="center"><span class="txt" runat="server" id="title2"></span></div>
	</section>
</asp:PlaceHolder>

<input type="hidden" runat="server" id="sitemap_resourcekey" />
