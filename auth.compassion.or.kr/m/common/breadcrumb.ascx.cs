﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace Common {
	public partial class breadcrumb : System.Web.UI.UserControl {

		public override bool Visible {
			get {
				return base.Visible;
			}
			set {
				if(value) {

					ShowMenu();

				}
				base.Visible = value;
			}
		}

		void ShowMenu() {
			SiteMapNode currentNode;
			if(AppSession.HasCookie(this.Context)) {
				app.Visible = true;
				currentNode = SiteMap.Providers["app"].CurrentNode;
			} else {
				mobileweb.Visible = true;
				currentNode = SiteMap.Providers["mobileweb"].CurrentNode;
			}

			if(currentNode != null) {

				sitemap_resourcekey.Value = currentNode.ResourceKey + "|";
				title.InnerText = title2.InnerText = currentNode.Title;

				if(currentNode.ParentNode == null) {
					app.Visible = mobileweb.Visible = false;
				} else {

					currentNode = currentNode.ParentNode;
					sitemap_resourcekey.Value = currentNode.ResourceKey + "|" + sitemap_resourcekey.Value;

					var url = currentNode.Url;
					if (currentNode["domain"] == "m") {
						url = ConfigurationManager.AppSettings["default_return_url_m"] + url.Substring(1);
					}
					link.HRef = link2.HRef = url;

					if(currentNode.ParentNode != null) {
						currentNode = currentNode.ParentNode;
						sitemap_resourcekey.Value = currentNode.ResourceKey + "|" + sitemap_resourcekey.Value;

						if(currentNode.ParentNode != null) {
							currentNode = currentNode.ParentNode;
							sitemap_resourcekey.Value = currentNode.ResourceKey + "|" + sitemap_resourcekey.Value;

						}
					}
				}
			}
			
		//	Response.Write("link > " + link.HRef);
	}

		protected override void OnLoad(EventArgs e) {
	
			base.OnLoad(e);

			
		}

		

	}
}