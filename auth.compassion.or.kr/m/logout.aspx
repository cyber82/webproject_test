﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="logout.aspx.cs" Inherits="m_logout" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
	<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
	
<script type="text/javascript">
		$(function(){
			location.href = "<%:this.ViewState["r"] %>";
		})

	</script>

</head>
<body>

	<asp:PlaceHolder runat="server" ID="ph_setcookie" Visible="false">
	</asp:PlaceHolder>

</body>
</html>
