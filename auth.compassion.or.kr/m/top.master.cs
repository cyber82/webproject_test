﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class m_TopMaster : System.Web.UI.MasterPage
{

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            this.OnBeforePostBack();
        }

        var isApp = AppSession.HasCookie(this.Context);
        //mobileweb_gnb.Visible = ph_mobileweb_content1.Visible = ph_mobileweb_content2.Visible = !isApp;
        //app_gnb.Visible = ph_app_header.Visible = isApp;
        ph_mobileweb_content1.Visible = ph_mobileweb_content2.Visible = !isApp;
        ph_app_header.Visible = isApp;

        #region GA Tag Manager Data
        string dimension1 = "", dimension2 = "", dimension3 = "", dimension4 = "X", dimension5 = "X", dimension6 = "X",
            dimension7 = "X", dimension8 = "X", dimension9 = "X", dimension10 = "X", dimension11 = "X", dimension12 = "X";

        dimension1 = UserInfo.IsLogin ? "Y" : "N";  //고객_로그인여부    Y / N

        //var isApp = AppSession.HasCookie(this.Context);
        var WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0)) { WebMode = "M"; }
        else { WebMode = "WWW"; }
        dimension3 = isApp ? "APP" : WebMode;   //채널_액세스유형    WWW / M / APP

        if (UserInfo.IsLogin)
        {
            UserInfo sess = new UserInfo();

            dimension2 = sess.SponsorID.EmptyIfNull();  //고객_ID   20030701000450900
            dimension4 = sess.Birth.EmptyIfNull().Length >= 4 ? sess.Birth.Substring(0, 4) : "X";  //고객_생년   1979 / X

            var genderCode = "";
            switch (sess.GenderCode.EmptyIfNull())
            {
                case "M": genderCode = "남"; break;
                case "F": genderCode = "여"; break;
                case "C": genderCode = "단체"; break;
                case "E": genderCode = "기타"; break;
                case "": genderCode = "X"; break;
                default: genderCode = "X"; break;
            }
            dimension5 = genderCode;    //dimension5    고객_성별   남 / 여 / 단체 / 기타 / X

            dimension6 = sess.ReligionType.EmptyIfNull().Equals("") ? "X" : sess.ReligionType;   //고객_종교   기독교, 무교, 불교, 천주교, 기타, X
            dimension7 = sess.ChannelType.EmptyIfNull().Equals("") ? "X" : sess.ChannelType;   //고객_가입경로 APP, EMAIL, FAX, IVR, mapp, mWeb, Web 기타, 방문, 우편, 이메일, 전화, 행사집계, X - 최초 가입 경로 (ChannelType)
            dimension8 = sess.AgreeEmail.EmptyIfNull().Equals("") ? "X" : sess.AgreeEmail;   //고객_메일 수신여부  Y / N / X
            dimension9 = sess.UserDate.EmptyIfNull().Equals("") ? "X" : sess.UserDate;   //고객_웹가입일     2017 - 11 - 20 / X
            dimension10 = sess.RegisterDate.EmptyIfNull().Equals("") ? "X" : sess.RegisterDate;   //고객_컴파스등록일   2017 - 11 - 20 / X - 최초 후원계정데이터 생성일(RegisterDate)
            dimension11 = sess.SponsorType.EmptyIfNull().Equals("") ? "X" : sess.SponsorType;   //고객_후원유형     결연 , 결연 / 머니 , 결연 / 머니 / 일반 , 결연 / 일반 , 결연 / 편지 , 결연 / 편지 / 일반 , 머니 , 머니 / 일반 , 일반 , 편지 , 편지 / 일반 , X
            dimension12 = sess.LastPaymentDate.EmptyIfNull().Equals("") ? "X" : sess.LastPaymentDate;   //고객_마지막납부일   2017 - 11 - 20 / X
        }

        this.ViewState["dimension1"] = dimension1;
        this.ViewState["dimension2"] = dimension2;
        this.ViewState["dimension3"] = dimension3;
        this.ViewState["dimension4"] = dimension4;
        this.ViewState["dimension5"] = dimension5;
        this.ViewState["dimension6"] = dimension6;
        this.ViewState["dimension7"] = dimension7;
        this.ViewState["dimension8"] = dimension8;
        this.ViewState["dimension9"] = dimension9;
        this.ViewState["dimension10"] = dimension10;
        this.ViewState["dimension11"] = dimension11;
        this.ViewState["dimension12"] = dimension12;
        #endregion
    }

    public string domain_mobile = ConfigurationManager.AppSettings["domain_mobile"];
    protected virtual void OnBeforePostBack()
    {

        is_login.Value = "N";
        _userid.Value = "";

        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();

            is_login.Value = "Y";
            _userid.Value = sess.UserId;

            userName.Text = sess.UserName;
            ph_after_login.Visible = ph_after_login2.Visible = true;
        }
        else
        {
            ph_before_login.Visible = true;
        }

        /*20170417 트래킹 소스 삽입*/
        var isApp = AppSession.HasCookie(this.Context);
        if (isApp)
        {
            app_tracking.Visible = true;
        }
        else
        {
            mw_tracking.Visible = true;
        }


    }

    public virtual ContentPlaceHolder Content
    {
        get
        {
            return this.body;
        }
    }

    public virtual string Title
    {
        get
        {
            return title.InnerText;
        }
        set
        {
            title.InnerText = value;
        }
    }

}
