﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class m_login : MobileFrontBasePage {
	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}
 
    protected override void OnBeforePostBack() {
        user_id.Focus();
		
		if(AppSession.HasCookie(this.Context)) {
			ph_app.Visible = true;
		}
        //	ph_app.Visible = true;
        var return_url = Request["r"];
		if(Session["SPONSOR_RETURN_URL"] != null) {
			return_url = Session["SPONSOR_RETURN_URL"].ToString();
		} else {
            //return_url = return_url.ValueIfNull(ConfigurationManager.AppSettings["default_return_url_m"]);
            return_url = HttpUtility.UrlDecode(return_url.ValueIfNull(ConfigurationManager.AppSettings["default_return_url_m"])).Replace("&amp;","&");
        }

	//	if(return_url.IndexOf("/my/") > -1) {
	//		return_url = ConfigurationManager.AppSettings["default_return_url_m"];
	//	}

		return_url += return_url.IndexOf("?") < 0 ? "?" : "&";
		return_url += "auth-callback=Y";

		this.ViewState["r"] = return_url;
		btnList.HRef = Request["r"].ValueIfNull(ConfigurationManager.AppSettings["default_return_url_m"]);

		// SSO 사이트의 세션만료등을 다시 로그인 시도를 위해 페이지에 접근했을때 로그인상태가 활성 상태면 자동 로그인 시킨다.
		// action=LOGIN 과 같이 값을 넘기면 자동로그인 체크를 하지 않는다.
		var action = Request.QueryString["action"].EmptyIfNull();
		if (action == "") {
			this.CheckStatus();

			var autoLogin = Request.Cookies[Constants.AUTOLOGIN_COOKIE_NAME];
			if(autoLogin != null && autoLogin.Value == "Y") {
				auto_login.Checked = true;
			}

			// 
		} else if (action == "REFER") {

			var userId = Request.Form["userId"];
			var userPwd = Request.Form["userPwd"];
			user_id.Text = userId;
			user_pwd.Text = userPwd;

			if(user_id.Text == "" || user_pwd.Text == "") {
				chk_info.Visible = true;

				return;
			}

            //[이종진] sessionId추가
            var sessionId = Request.Form["sessionId"];
            this.Login(userId , userPwd , auto_login.Checked, sessionId);
		}

		
    }


    protected void login_Click( object sender, EventArgs e ) {

		if(user_id.Text == "" || user_pwd.Text == "") {
            chk_info.Visible = true;

            return;
		}

		var userId = user_id.Text;
		var userPwd = user_pwd.Text;

		this.Login(userId , userPwd , auto_login.Checked);

	}

	void Login(string userId , string userPwd , bool autoLogin, string sessionId="") {
		
		var auth = new AuthUser();
		JsonWriter result = auth.Login(userId, userPwd , autoLogin, sessionId);

		if(!result.success) {
			chk_info.Visible = true;
			//base.AlertWithJavascript(result.message);
			return;
		}

		this.SendToken(result.data.ToString());
	}

	void CheckStatus() {
		//Response.Write(FrontLoginSession.GetDomain(HttpContext.Current));
		HttpCookie cookie = Request.Cookies[Constants.TOKEN_COOKIE_NAME];
		if(cookie == null)
			return;

		var token = cookie.Value;
	   // HttpContext.Current.Response.Write("enc : " + HttpUtility.UrlEncode(token) + "<br>");
		var result = new AuthenticationAction().Validate(token, Request.Url.Host , Request.UserHostAddress);
		if(result.success) {
			
			this.SendToken(token);
		} else {
			
		//	Response.Write(result.ToJson());
		}
		
	}

	void SendToken(string token) {
		// SSO 대상 사이트에 token 전달 및 세션 생성 요청
		
		foreach(string k in new AuthenticationAction().sites.Select(p=>p.login_url)) {
			HtmlImage img = new HtmlImage();
			img.Attributes["src"] = k + HttpUtility.UrlEncode(token);
			img.Style["display"] = "none";
			ph_setcookie.Controls.Add(img);
		}
		
		ph_setcookie.Visible = ph_setcookie2.Visible = true;
	}

	

	
	

}