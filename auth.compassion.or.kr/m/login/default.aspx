﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="m_login" MasterPageFile="~/m/main.Master"%>
<%@ MasterType virtualpath="~/m/main.master" %>



<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<asp:PlaceHolder runat="server" ID="ph_setcookie2" Visible="false">
		<script type="text/javascript">
			$(function () {
				$("header , footer , body").hide();
				location.href = String("<%:this.ViewState["r"] %>").replace(/&amp;/g,'&');
			})
		</script>

	</asp:PlaceHolder>

	<script type="text/javascript">

	    $(document).ready(function () {
	        if (location.href.split('?').length > 1)
	            $("#btn_join").attr('href', "/m/join/?" + location.href.split('?')[1]);
	        else
	            $("#btn_join").attr('href', "/m/join/");
	    });

		$(function(){
			
		    $("#user_id").keypress(function (e) {
		        if (e.keyCode == 13) {
		            $("#user_pwd").focus();
		        }
		    });

		    $("#user_pwd").keypress(function (e) {
		        if (e.keyCode == 13) {
		            location.href = $("#login").attr("href");
		            return false;

		        }
		    });


		    var main_img_path = "/m/common/img/page/member/bg-login";
		    var random = Math.floor((Math.random() * 3) + 1);

		    $("#main_img").attr("style", "background: url('" + main_img_path + random + ".jpg') no-repeat;background-size:cover;");

		    setFullHeight();
		})
	</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

<div class="wrap-sectionsub">
<!---->
    
    <!--백그라운드이미지 : bg-login1.jpg ~ bg-login3.jpg 랜덤-->
    <div class="member-login sectionsub-margin1 fullHeight" id="main_img" style="background:url('/m/common/img/page/member/bg-login1.jpg') no-repeat;background-size:cover">
    	
        <p class="logo">꿈을 잃은 어린이들에게 그리스도의 사랑을 Compassion</p>
        
        <fieldset class="frm-login">
        	<legend>아이디,비밀번호 입력</legend>
			<asp:TextBox runat="server" ID="user_id" placeholder="아이디 입력" ></asp:TextBox>
			<asp:TextBox runat="server" TextMode="Password" ID="user_pwd" placeholder="비밀번호 입력" ></asp:TextBox>
        </fieldset>
        <p class="txt-error" runat="server" id="chk_info" visible="false">아이디 또는 비밀번호를 다시 확인하세요.</p>   <!--에러메시지 경우에만 출력-->

		<!-- app 인경우 임시 -->
		<asp:PlaceHolder runat="server" ID="ph_app" Visible="false">

		<div class="align-center">
			<input type="checkbox" runat="server" id="auto_login" style="display:none" />
			<span class="auto-login">자동 로그인</span>	<!-- 자동로그인 클릭시 "on" 클래스 추가 -->
			<script>
				if ($("#auto_login").prop("checked")) {
					$(".auto-login").addClass("on");

				}

				$(".auto-login").click(function () {
					if ($(".auto-login").hasClass("on")) {
						$(".auto-login").removeClass("on");
						$("#auto_login").prop("checked", false);
					} else {
						$(".auto-login").addClass("on");
						$("#auto_login").prop("checked", true);
					}
				})
			</script>
		</div>
		</asp:PlaceHolder>
		<!-- //app 인경우 임시 -->

        <div class="wrap-bt"><asp:LinkButton runat="server" ID="login" OnClick="login_Click" style="width:100%" class="bt-type6" >로그인</asp:LinkButton></div>
        
        <div class="etc-link">
        	<a id="btn_join" >회원가입</a><span class="bar">|</span>
            <a href="/m/find/id/">아이디찾기</a><span class="bar">|</span>
            <a href="/m/find/pwd/">비밀번호찾기</a>
        </div>
        
        
    </div>
    
<!--//-->
</div>

	<a href="#" runat="server" id="btnList"></a>
<asp:PlaceHolder runat="server" ID="ph_setcookie" Visible="false"></asp:PlaceHolder>

</asp:Content>

