﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="m_find_id_complete" MasterPageFile="~/m/main.Master"%>
<%@ MasterType virtualpath="~/m/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript">
		$(function () {

		});

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub">
<!---->
    
    <div class="member-result">
    	
        <p class="txt1">회원님이 입력하신 정보와 일치하는 아이디입니다.<br />
            개인정보보호를 위해 개인정보의<br />
			일부는 ***로 표시하였습니다.  
		</p>
        <div class="box2-gray"><asp:Literal runat="server" ID="user_id"/></div>
        <p class="txt2">가입일 : <asp:Literal runat="server" ID="reg_date"/></p>
        
        <div class="wrap-bt">
        	<a class="bt-type6 fl" style="width:49%" href="/m/login/">로그인하기</a>
            <a class="bt-type7 fr" style="width:49%" href="/m/find/pwd/">비밀번호 찾기</a>
		</div> 
        
    </div>
    
<!--//-->
</div>

	
</asp:Content>
