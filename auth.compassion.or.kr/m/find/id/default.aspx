﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="m_find_id_default" MasterPageFile="~/m/main.Master" %>

<%@ MasterType VirtualPath="~/m/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/id.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/email.js"></script>
    <script type="text/javascript" src="/cert/cert.js"></script>
    <script type="text/javascript" src="/m/find/id/default.js?v=1.0"></script>

    <script type="text/javascript">
        $(function () {


        });
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="cur_type_index" value="0" />
    <input type="hidden" runat="server" id="s1_phone" value="" />
    <input type="hidden" runat="server" id="s2_email" value="" />
    <input type="hidden" id="s3_ci" runat="server" />

    <input type="hidden" runat="server" id="birthyyyy" value="" />
    <input type="hidden" runat="server" id="birthmm" value="" />
    <input type="hidden" runat="server" id="birthdd" value="" />

    <div class="wrap-sectionsub">
        <!---->

        <div class="member-join">

            <!--메뉴 활성화 상태  :  "selected"-->
            <div class="tab-jointype">
                <ul>
                    <li data-role="tab" class="btn_member_type selected"><span>휴대폰</span></li>
                    <li data-role="tab" class="btn_member_type"><span>이메일</span></li>
                    <%--<li data-role="tab" class="btn_member_type"><span>본인인증</span></li>--%>
                </ul>
            </div>

            <p class="txt-blue">회원가입 시 사용하신 회원정보로 아이디를 찾을 수 있습니다.</p>
            <div class="linebar"></div>
            <!-- 휴대폰 -->
            <fieldset class="frm-input" data-role="container">
                <legend>이름,휴대폰번호, 인증번호 입력</legend>
                <div class="row-table">
                    <div class="col-td" style="width: 100%">
                        <span class="row">
                            <input type="text" id="s1_name" runat="server" placeholder="이름" style="width: 100%" maxlength="30" />
                            <p class="txt-error" id="msg_s1_name" style="display: none"></p>
                        </span>

                        <span class="row">
                            <span class="column" style="width: 48%">
                                     <asp:DropDownList runat="server" ID="s1_user_birth_yyyy" style="width: 100%" Class="birth_sel"></asp:DropDownList>
                            </span>
                            <span class="column" style="width: 26%">
                                <asp:DropDownList runat="server" ID="s1_user_birth_mm" style="width: 100%" Class="birth_sel"></asp:DropDownList>
                            </span>
                            <span class="column" style="width: 26%">
                                <asp:DropDownList runat="server" ID="s1_user_birth_dd" style="width: 100%" Class="birth_sel"></asp:DropDownList>
                            </span>
                            <p class="txt-error" id="msg_s1_birth" style="display: none"></p>
                        </span>

                        <span class="row pos-relative1">
                            <input type="text" data-id="s1_phone" runat="server" id="temp_s1_phone" class="number_only" placeholder="휴대폰번호(-없이 입력)" style="width: 100%" maxlength="11" />
                            <a class="bt-type9 pos-btn" style="width: 50px" id="s1_btn_auth">인증</a>
                            <p class="txt-error" id="msg_s1_phone" style="display: none"></p>
                        </span>

                        <span class="row pos-relative1">
                            <input type="text" id="s1_code" runat="server" maxlength="30" placeholder="인증번호" style="width: 100%" class="number_only"/>
                            <a class="bt-type9 pos-btn" style="width: 50px" id="s1_btn_confirm">확인</a>
                            
                        </span>

                        <p class="txt-result" id="s1_msg" style="display: none"></p>
                    </div>
                </div>
            </fieldset>

            <!-- 이메일 -->
            <fieldset class="frm-input" data-role="container" style="display: none">
            <legend>이름,휴대폰번호, 인증번호 입력</legend>
            <div class="row-table">
                <div class="col-td" style="width:100%">
                    <span class="row">
                        <input type="text" id="s2_name" runat="server" placeholder="이름" style="width:100%" maxlength="30" />
                        <p class="txt-error" id="msg_s2_name" style="display:none "></p>
                    </span>

                    <span class="row">
                            <span class="column" style="width: 48%">
                                <asp:DropDownList runat="server" ID="s2_user_birth_yyyy" style="width: 100%" Class="birth_sel"></asp:DropDownList>
                            </span>
                            <span class="column" style="width: 26%">
                                <asp:DropDownList runat="server" ID="s2_user_birth_mm" style="width: 100%" Class="birth_sel"></asp:DropDownList>
                            </span>
                            <span class="column" style="width: 26%">
                                <asp:DropDownList runat="server" ID="s2_user_birth_dd" style="width: 100%" Class="birth_sel"></asp:DropDownList>
                            </span>
                        <p class="txt-error" id="msg_s2_birth" style="display:none "></p>
                        </span>
                    
                    <span class="row pos-relative3">
                        <input type="text" data-id="s2_email" runat="server" id="temp_s2_email" placeholder="이메일" style="width:100%" maxlength="100" />
                        <a class="bt-type9 pos-btn" style="width:50px" id="s2_btn_auth">인증</a>
                        <a class="bt-type9 pos-btn2" style="width:50px" id="s2_btn_confirm">확인</a>
                        
                    </span>
                    
                    <p class="txt-result" id="s2_msg" style="display: none"></p>
                    <p class="txt-bottom" style="margin-top:9px">입력하신 이메일에서 인증코드를 클릭하셨으면 [확인] 버튼을 클릭해주세요.</p>
                </div>
            </div>
        </fieldset>

             <!-- 본인 인증 -->

            <div class="self-account" data-role="container" style="display: none">
                <div class="link-account">
                    <a id="s3_btn_cert_by_phone"><span>휴대폰 인증</span></a>
                    <a id="s3_btn_cert_by_ipin"><span>아이핀(i-PIN) 인증</span></a>
                </div>
                    <p class="txt-error" id="s3_msg" style="display:none"></p>
                <div class="box-gray">
                    <ul class="list-bullet2">
                        <li>본인명의의 휴대폰으로만 본인인증이 가능합니다.<br />
                            아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다. </li>
                    </ul>
                </div>
            </div>


            <div class="wrap-bt">
                <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="bt-type6" style="width: 100%">아이디 찾기</asp:LinkButton>
            </div>
            <!-- 
                <p class="txt-error">입력하신 정보와 일치하는 회원이 없습니다. 다시 입력해주세요.</p> 
                -->
            <!--에러메시지 경우에만 출력-->

            <p class="txt-bottom">
                위 방법으로 아이디/비밀번호를 찾을 수 없는 경우<br />
				한국컴패션으로 연락 주시기 바랍니다.<br />
				후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_gray">info@compassion.or.kr</a>
            </p>

        </div>

        <!--//-->
    </div>



</asp:Content>
