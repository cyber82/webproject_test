﻿$(function () {

    $page.init();

    $("#user_pwd").keyup(function () {
        if ($("#user_pwd").val() != "") {
            $("#msg_user_pwd").hide();
        }
    });

    $("#re_user_pwd").keyup(function () {
        if ($("#re_user_pwd").val() != "") {
            $("#msg_re_user_pwd").hide();
        }
    });

});


var $page = { 

	init: function () {

		// 변경 버튼
		$("#btn_submit").click(function () {

			return $page.onSubmit();

		});


	},

	// 확인
	onSubmit : function(){

		// 비번
		var pwd_result = passwordValidation.check($("#user_pwd"), $("#re_user_pwd"), 5, 15);
		if (!pwd_result.result) {
		    $("#user_pwd").focus();

		    if (pwd_result.msg.indexOf("확인") == -1) {
		        $("#user_pwd").focus();
		        $("#msg_user_pwd").html(pwd_result.msg).show();
		    } else {
		        $("#re_user_pwd").focus();
		        $("#msg_re_user_pwd").html(pwd_result.msg).show();
		    }

		    return false;
		}
		alert("비밀번호가 변경되었습니다.")
		return true;
	}
}