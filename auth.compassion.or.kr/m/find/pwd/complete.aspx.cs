﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class m_find_pwd_complete : MobileFrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {

		var pwdUserId = Session["pwdUserId"];
		if (pwdUserId == null) {
			Response.Redirect("/m/find/pwd/");
			return;
		}

		this.ViewState["pwdUserId"] = pwdUserId;
		
	}


	protected void btn_submit_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

        using (AuthDataContext dao = new AuthDataContext())
        {
            //dao.sp_tSponsorMaster_change_pwd_f(this.ViewState["pwdUserId"].ToString(), user_pwd.Value);
            Object[] op1 = new Object[] { "UserID", "UserPW" };
            Object[] op2 = new Object[] { this.ViewState["pwdUserId"].ToString(), user_pwd.Value };
            www6.selectSPAuth("sp_tSponsorMaster_change_pwd_f", op1, op2);

            Response.Redirect("/m/login");

        }

	}
}