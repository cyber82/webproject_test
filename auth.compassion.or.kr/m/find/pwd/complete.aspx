﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="m_find_pwd_complete" MasterPageFile="~/m/main.Master"%>
<%@ MasterType virtualpath="~/m/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js"></script>
	<script type="text/javascript" src="/m/find/pwd/complete.js"></script>

	<script type="text/javascript">
		$(function () {

			
		});
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub">
<!---->
    
    <div class="member-result">
    	
        <p class="txt3">
        	<strong>본인확인이 완료되었습니다. </strong><br />
			새로운 비밀번호를 등록 후 사용해주세요
		</p>
        
        <fieldset class="frm-input">
            <legend>새로운 비밀번호 입력</legend>
            <div class="row-table">
                <div class="col-td" style="width:100%">
                    <span class="row">
                        <input type="password" id="user_pwd" runat="server" placeholder="새 비밀번호" style="width:100%" />
                        <p class="txt-error" id="msg_user_pwd" style="display: none"></p>
                    </span>
                    
                    <span class="row">
                        <input type="password" id="re_user_pwd" runat="server" placeholder="새 비밀번호 확인" style="width:100%" />
                        <p class="txt-error" id="msg_re_user_pwd" style="display: none"></p>
                    </span>
                </div>
            </div>
        </fieldset>
        
        <div class="wrap-bt">
            <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="bt-type6 fl" style="width:100%">비밀번호 변경 완료</asp:LinkButton>
		</div> 
        
    </div>
    
<!--//-->
</div>


</asp:Content>
