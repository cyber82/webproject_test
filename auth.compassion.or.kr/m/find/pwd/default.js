﻿$(function () {

    $page.init();

    $("#btn_email_submit").click(function () {
        $page.onSubmit();
        return false;
    });
    if ($page.cur_type_index == 2) {
    	$("#btn_submit").hide();
    }

    $("#s1_id").keyup(function () {
        if ($("#s1_id").val() != "") {
            $("#msg_s1_id").hide();
        }
    });

    $("#s1_name").keyup(function () {
        if ($("#s1_name").val() != "") {
            $("#msg_s1_name").hide();
        }
    });

    $("#temp_s1_phone").keyup(function () {
        if ($("#temp_s1_phone").val() != "") {
            $("#msg_s1_phone").hide();
        }
    });

    $("#s2_id").keyup(function () {
        if ($("#s2_id").val() != "") {
            $("#msg_s2_id").hide();
        }
    });

    $("#s2_name").keyup(function () {
        if ($("#s2_name").val() != "") {
            $("#msg_s2_name").hide();
        }
    });

    $("#temp_s2_email").keyup(function () {
        if ($("#temp_s2_email").val() != "") {
            $("#s2_msg").hide();
        }
    });

    $("#s3_id").keyup(function () {
        if ($("#s3_id").val() != "") {
            $("#s3_msg").hide();
        }
    });

});


var $page = { 

	timer: null,

	cur_type_index : -1 , 

	init: function () {

	    $page.cur_type_index = parseInt($("#cur_type_index").val());
	    $($("[data-role=tab]").removeClass("selected"));
	    $($("[data-role=tab]")[$page.cur_type_index]).addClass("selected");

		$("[data-role=container]").hide();
		$($("[data-role=container]")[$page.cur_type_index]).show();

		$("[data-role='tab']").each(function (i) {

		    $(this).click(function () {
		        $("input").val("");
		        $(".btn_member_type").removeClass("selected");
		        $page.cur_type_index = i;
		        $("#cur_type_index").val(i); // 0 : 휴대폰 , 1 : 이메일 , 2 : 본인인증

		        if (i == 2) $("#btn_submit").hide();
		        else $("#btn_submit").show();
		        
		        $("[data-role=container]").hide();
		        $($("[data-role=container]")[i]).show();
		        $($("[data-role=tab]")[i]).addClass("selected").show();

		        return false;
		    });
		});

		// 휴대폰
		$page.setType0Event();

		// 이메일
		$page.setType1Event();

		// 본인인증
		$page.setType2Event();

		// 비밀번호찾기
		$("#btn_submit").click(function () {

			return $page.onSubmit();

		});

	},

	// 휴대폰
	setType0Event: function () {

		// 인증요청
	    $("#s1_btn_auth").click(function () {
	        if ($("#s1_id").val() == "") {
	            $("#s1_id").focus();
	            $("#msg_s1_id").html("아이디를 입력해주세요").addClass("txt-error").show();
	            return false;
	        }
			
			var phone = $("[data-id=s1_phone]").val();
			var name = $("#s1_name").val();
			if (name == "") {
				$("#s1_name").focus();
				$("#s1_msg").html("이름을 입력해 주세요").addClass("txt-error").show();
				return false;
			}

			if (!/^[0-9.]{10,11}$/.test(phone)) {
			    $("#s1_msg").html("휴대폰번호를 정확히 입력해주세요.").addClass("txt-error").show();
				$("[data-id=s1_phone]").focus();
				return false;
			}

			if ($page.timer != null)
				window.clearTimeout($page.timer);

			// 연속클릭방지
			$page.timer = window.setTimeout(function () {

				$.get("/api/find.ashx", { t: "send-phone", c: phone, n: name }, function (r) {
				    $("#s1_msg").removeClass("txt-result").removeClass("txt-error");
					$page.timer = null;
					if (r.success) {
					    $("#s1_msg").html("문자메세지를 발송하였습니다.").addClass("txt-result").show();
					} else {
					    $("#s1_msg").html("문자메세지 발송에 실패했습니다.").addClass("txt-error").show();
					}

				});

			}, 300);

			return false;
		});

		// 인증확인
		$("#s1_btn_confirm").click(function () {

			var code = $("#s1_code").val();
			$("#s1_msg").removeClass("txt-result").removeClass("txt-error");
			$.get("/api/find.ashx", { t: "check-phone", c: code }, function (r) {

				if (r.success) {
					
					$("#s1_phone").val(r.data.phone);
					$("#s1_msg").html("인증이 완료되었습니다.").addClass("txt-result").show();
				} else {
				    $("#s1_msg").html("인증이 완료되지 않았습니다.").addClass("txt-error").show();
				}

			});

			return false;
		});

	} , 

	onSubmitType0: function () {
		if ($("#s1_id").val() == "") {
			$("#s1_id").focus();
			$("#msg_s1_id").html("아이디를 입력해주세요").addClass("txt-error").show();
			return false;
		}

		if ($("#s1_name").val() == "") {
			$("#s1_name").focus();
			$("#msg_s1_name").html("이름을 입력해주세요").addClass("txt-error").show();
			return false;
		}

		if ($("#temp_s1_phone").val() == "") {
		    $("#temp_s1_phone").focus();
		    $("#msg_s1_phone").removeClass("txt-result");
		    $("#msg_s1_phone").html("휴대폰번호를 인증해주세요").addClass("txt-error").show();
			return false;
		}

		if ($("#s1_code").val() == "") {
		    $("#s1_code").focus();
		    $("#s1_msg").html("인증번호를 입력해 주세요").addClass("txt-error").show();
		    return false;
		}
		
		return true;
	} ,

	// 이메일
	setType1Event : function(){
	
		// 인증요청
	    $("#s2_btn_auth").click(function () {
	        if ($("#s2_id").val() == "") {
	            $("#s2_id").focus();
	            $("#msg_s2_id").html("아이디를 입력해주세요").addClass("txt-error").show();
	            return false;
	        }

	        if ($("#s2_name").val() == "") {
	            $("#s2_name").focus();
	            $("#msg_s2_name").html("이름을 입력해주세요").addClass("txt-error").show();
	            return false;
	        }

			var check_email = emailValidation.check($("[data-id=s2_email]").val());
			if (!check_email.result) {
			    $("#s2_msg").html(check_email.msg).addClass("txt-error").show();
			    $("#temp_s2_email").focus();
                
			    return false;

			}

			var email = $("[data-id=s2_email]").val();

			if ($page.timer != null)
				window.clearTimeout($page.timer);

			// 연속클릭방지
			$page.timer = window.setTimeout(function () {

				$.get("/api/find.ashx", { t: "send-email", c: email }, function (r) {
				    $("#s2_msg").addClass("txt-result").addClass("text-error");
					$page.timer = null;
					if (r.success) {
					    $("#s2_msg").html("메일을 발송하였습니다.").addClass("txt-result").show();
					} else {
					    $("#s2_msg").html("메일 발송에 실패했습니다.").addClass("txt-error").show();
					}

				});

			}, 1000);

			return false;
		});

		// 인증확인
		$("#s2_btn_confirm").click(function () {
			var email = $("[data-id=s2_email]").val();

			$.get("/api/find.ashx", { t: "check-email", c: email }, function (r) {
			    $("#s2_msg").removeClass("txt-error").addClass("txt-result");
				if (r.success && r.data.email == email) {
				    $("#s2_email").val(r.data.email);
				    $("#s2_msg").html("인증이 완료 되었습니다.").addClass("txt-result").show();
				} else {
				    $("#s2_msg").removeClass("txt-result");
				    $("#s2_msg").html("인증이 완료되지 않았습니다.").addClass("txt-error").show();
				}

			});

			return false;
		});

	} , 

	onSubmitType1: function () {

		if ($("#s2_id").val() == "") {
			$("#s2_id").focus();
			$("#msg_s2_id").html("아이디를 입력해주세요").addClass("txt-error").show();
			return false;
		}

		if ($("#s2_name").val() == "") {
			$("#s2_name").focus();
			$("#msg_s2_name").html("이름을 입력해주세요").addClass("txt-error").show();
			return false;
		}

		if ($("#s2_email").val() == "") {
		    $("#temp_s2_email").focus();
		    $("#s2_msg").html("이메일을 인증해주세요").addClass("txt-error").show();
			return false;
		}

		return true;
	} ,

	// 본인인증
	setType2Event : function(){
		
		// 휴대폰 인증
		$("#s3_btn_cert_by_phone").click(function () {

			if ($("#s3_id").val() == "") {
				$("#s3_id").focus();
				$("#s3_msg").html("아이디를 입력해주세요").addClass("txt-error").show();
				return false;
			}

			cert_openPopup("phone");

			return false;
		})

		// 아이핀 인증
		$("#s3_btn_cert_by_ipin").click(function () {

			if ($("#s3_id").val() == "") {
			    $("#s3_id").focus();
			    $("#s3_msg").html("아이디를 입력해주세요.").addClass("txt-error").show();
				return false;
			}

			cert_openPopup("ipin");
			return false;
		})

	} ,

	onSubmitType2: function () {

		return true;
	} ,

	// 확인
	onSubmit : function(){
		
		if ($page.cur_type_index == 0) {
			return $page.onSubmitType0();
		}else if ($page.cur_type_index == 1) {
			return $page.onSubmitType1();
		} else if ($page.cur_type_index == 2) {
			return $page.onSubmitType2();
		}

		return false;
	}

}

// method = ipin or phone
var cert_setCertResult = function (method, result, ci, di, name, birth, sex) {
    if (result != "Y") {
        
        $("#s2_msg").html("인증에 실패했습니다.").addClass("txt-error").show();
		return;
	}

	$("#s3_ci").val(ci);
	eval($("#btn_submit").attr("href").replace("javascript:", ""));

}