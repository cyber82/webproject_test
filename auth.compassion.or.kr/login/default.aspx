﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="_login" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<asp:PlaceHolder runat="server" ID="ph_setcookie2" Visible="false">
		<script type="text/javascript">
			$(function () {

			    $("header , footer , body").hide();
                location.href = decodeURIComponent("<%:this.ViewState["r"] %>");
			})

			</script>

	</asp:PlaceHolder>

	<script type="text/javascript">
		$(function(){
			
		    $("#user_id").keypress(function (e) {
		        if (e.keyCode == 13) {
		            $("#user_pwd").focus();
		        }
		    });

		    $("#user_pwd").keypress(function (e) {
				
		    	if (e.keyCode == 13) {
		           location.href = $("#login").attr("href");
		            return false;

		        }
		    });

			 var main_img_path = "/common/img/page/member/member_main_bg";
			 var random = Math.floor((Math.random() * 3) + 1);
	
			 $("#main_img").attr("style", "background: url('" + main_img_path + random + ".jpg') no-repeat center top;");
			
		})

		$(document).ready(function () {

		    if (location.href.split('?').length > 1)
		        $("#btn_join").attr('href', "/join/?" + location.href.split('?')[1]);
		    else
		        $("#btn_join").attr('href', "/join/");

		});
	</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    
    <!-- sub body -->
	<section class="sub_body">

		<!-- s: sub contents -->
		<!---------- member_main_bg1 ~ 3 까지 랜덤으로 노출 ----------->
		<div class="subContents member login" id="main_img" >
			<div class="w980">
				<div class="wrap">
					<div class="logo"><a href="<%: ConfigurationManager.AppSettings["default_return_url"] %>"><img src="/common/img/common/logo_1.png" alt="Compassion 로고" /></a></div>
					<div class="login_box">
						<div class="field">
							<label for="user_id">아이디</label>
                            <asp:TextBox runat="server" ID="user_id" class="input_id"></asp:TextBox>
						</div>
						<div class="field">
							<label for="user_pwd">비밀번호</label>
                            <asp:TextBox runat="server" TextMode="Password" ID="user_pwd" class="input_pw"></asp:TextBox>
						</div>
						<p class="tac mb40"><span class="guide_comment2" runat="server" id="chk_info" visible="false">아이디 또는 비밀번호를 다시 확인하세요.</span></p>
                        <asp:LinkButton runat="server" ID="login" OnClick="login_Click" class="btn_b_type4">로그인</asp:LinkButton>
					</div>
					<div class="btn_group">
						<a id="btn_join" class="btn_join">회원가입</a>
						<span class="bar"></span>
						<a href="/find/id/" class="btn_id">아이디찾기</a>
						<span class="bar"></span>
						<a href="/find/pwd/" class="btn_pw">비밀번호찾기</a>
					</div>
				</div>
			</div>
		</div>
		<!--// e: sub contents -->

    </section>
    <!--// sub body -->


		<asp:PlaceHolder runat="server" ID="ph_setcookie" Visible="false"></asp:PlaceHolder>

	
</asp:Content>

