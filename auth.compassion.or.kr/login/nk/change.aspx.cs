﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class login_nk_change : FrontBasePage {
	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		Master.HideDetailNavigation();

		if (Session["NK_ACTION"] == null) {

			Response.Redirect("/login/NK/");

		}
		
		this.ViewState["action"] = Session["NK_ACTION"]; //NK_USABLE(기존NK아이디 사용가능) , NEWID(신규아이디 생성대상)
		this.ViewState["userId"] = Session["NK_USERID"];
		this.ViewState["r"] = Session["NK_RETURN"];
		
		if (this.ViewState["action"].ToString() == "NK_USABLE") {
			ph_existid.Visible = ph_existid2.Visible = true;
			newUserId.Value = txt_userId.Text = this.ViewState["userId"].ToString();

		} else {
			ph_newid.Visible = ph_newid2.Visible = true;
		}


	}


	protected void btn_submit_Click( object sender, EventArgs e ) {
		if(base.IsRefresh)
			return;

		
		if(newUserId.Value == "") {

			return;
		}

		var userId = this.ViewState["userId"].ToString();
		var userPwd = pwd.Value;
		var newID = newUserId.Value.EscapeSqlInjection();
		var auth = new AuthUser();

		JsonWriter result = auth.ChangeNK(userId, newID, userPwd);

		if(!result.success) {
			AlertWithJavascript(result.message);
			return;
		}

		base.AlertWithJavascript("ID통합이 완료되었습니다." , "goLogin('"+ this.ViewState["r"].ToString() + "');");
		

	}
}