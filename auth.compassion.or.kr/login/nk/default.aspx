﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="login_nk" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>



<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        $(function () {
            $("#header").remove();
            $("#btn_login").click(function () {

                if ($("#user_id").val() == "") {
                    alert("아이디를 입력하세요");
                    $("#user_id").focus();
                    return false;
                }

                if ($("#user_pwd").val() == "") {
                    alert("비밀번호를 입력하세요");
                    $("#user_pwd").focus();
                    return false;
                }

                return true;
            }),

            $("#user_id").keypress(function (e) {
                if (e.keyCode == 13) {
                    $("#user_pwd").focus();
                }
            })

            $("#user_pwd").keypress(function (e) {
                if (e.keyCode == 13) {
                    location.href = $("#btn_login").attr("href");
                    return false;

                }
            })

            $("#user_id").keyup(function (e) {
                if ($("#user_id").val() != "") {
                    $("#chk_info").hide();
                }
            })

            $("#user_pwd").keyup(function (e) {
                if ($("#user_pwd").val() != "") {
                    $("#chk_info").hide();
                }
            })


        })

    </script>


	<asp:PlaceHolder runat="server" ID="ph_setcookie2" Visible="false">
		<script type="text/javascript">
			$(function () {

				$("header , footer , body").hide();
				location.href = "<%:this.ViewState["r"] %>";
		    })
		</script>
	</asp:PlaceHolder>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <asp:PlaceHolder runat="server" ID="ph_login"></asp:PlaceHolder>

    <asp:PlaceHolder runat="server" ID="ph_setcookie" Visible="false"></asp:PlaceHolder>

    <header class="topLogo">
        <div class="w980">
            <a href="<%: ConfigurationManager.AppSettings["domain_file"] %>"><img src="/common/img/common/logo_4.png" class="logo" alt="compassion 로고" /></a>
        </div>
    </header>

    <section class="sub_body">

        <!-- s: sub contents -->
        <div class="subContents padding0 integration">

            <div class="bgContent">
                <div class="w980">
                    <h1 class="tit">LOGIN</h1>
                    <span class="bar"></span>
                    <p class="con">
                        한국컴패션 ID와 통합되지 않은 회원은 비밀번호 입력란에 가입하신 "이메일"을 입력해주세요<br />
                        통합하신 회원은 "비밀번호"로 입력해주시면 됩니다.
                    </p>
                </div>
            </div>

            <div class="w980 idpw">

                <h2 class="int_tit">ID/PW</h2>

                <!-- 입력박스 -->
                <div class="inputBox">
                    <div class="item clear2">
                        <label class="field id" for="id"><span class="icon"></span><span class="vam">아이디</span></label>
                        <span class="input">

                            <asp:TextBox runat="server" ID="user_id"></asp:TextBox>
                        </span>
                    </div>
                    <div class="item clear2">
                        <label class="field pw" for="pw_1"><span class="icon"></span><span class="vam">비밀번호</span></label>
                        <span class="input">

                            <asp:TextBox runat="server" TextMode="Password" ID="user_pwd"></asp:TextBox>
                        </span>
                    </div>
                    <p class="guide_wrap2">
                        <span class="guide_comment2" runat="server" id="chk_info" visible="false">아이디 또는 비밀번호를 다시 확인하세요.<br />
                            한국컴패션 ID와 통합되지 않은 회원은 비밀번호 입력란에 이메일을 입력해주세요.<br />
                            통합하신 회원은 비밀번호로 입력해주시면 됩니다.
                        </span>
                    </p>
                </div>
                <!--// 입력박스 -->

                <div class="tac">
                    <asp:LinkButton runat="server" ID="btn_login" OnClick="btn_login_Click" class="btn_type1 mb50">로그인</asp:LinkButton>
                </div>

                <div class="txt_btns">
                    <div class="btn">
                        <a href="/join/">회원가입</a><span></span>
                        <a href="/find/id/">아이디 찾기</a><span></span>
                        <a href="/find/pwd/">비밀번호 찾기</a>
                    </div>
                    <p class="desc">한국컴패션과 통합되지 않은 회원은 아이디/비밀번호 찾기 시, 후원지원팀으로 연락 주시기 바랍니다.</p>
                </div>

                <div class="box_type4 padding1 tac">
                    <span class="s_con1">회원통합관련 문의 : <span class="fc_blue">후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_blue">info@compassion.or.kr</a></span></span>
                </div>
            </div>
            <div class="h100"></div>
        </div>
        <!--// e: sub contents -->

    </section>



</asp:Content>

