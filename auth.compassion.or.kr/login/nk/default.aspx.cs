﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class login_nk : FrontBasePage {
	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
        user_id.Focus();
        Master.HideDetailNavigation();
        var return_url = Request.QueryString["r"].ValueIfNull(ConfigurationManager.AppSettings["default_return_url"]);
		this.ViewState["r"] = return_url;

		// SSO 사이트의 세션만료등을 다시 로그인 시도를 위해 페이지에 접근했을때 로그인상태가 활성 상태면 자동 로그인 시킨다.
		// action=LOGIN 과 같이 값을 넘기면 자동로그인 체크를 하지 않는다.
		var action = Request.QueryString["action"].EmptyIfNull();
		if (action == "") {

			this.CheckStatus();

		}
		
	}
	
	protected void btn_login_Click( object sender, EventArgs e ) {
		if(base.IsRefresh)
			return;

		if(user_id.Text == "" || user_pwd.Text == "") {
			return;
		}
        /* 20170308 인젝션 처리 부분 삭제  - 이석호K 요청*/
        //var userId = user_id.Text.EscapeSqlInjection();
        //var userPwd = user_pwd.Text.EscapeSqlInjection();
        var userId = user_id.Text;
        var userPwd = user_pwd.Text;

        var auth = new AuthUser();

		JsonWriter result = auth.LoginNK(userId , userPwd);

		if(!result.success) {
			if (result.action == "NK_USABLE" || result.action == "NEWID") {     // 미전환계정
				Session["NK_ACTION"] = result.action;
				Session["NK_USERID"] = userId;
				Session["NK_RETURN"] = this.ViewState["r"].ToString();

				Response.Redirect("/login/nk/agree");
				
			} else {
				AlertWithJavascript(result.message);
			}
			return;
		}

		

		result = auth.Login(userId , userPwd);
        if(!result.success) {
            chk_info.Visible = true;
            //base.AlertWithJavascript(result.message);
            return;
        }
   
		this.SendToken(result.data.ToString());


    }


	void CheckStatus() {
		
		HttpCookie cookie = Request.Cookies[Constants.TOKEN_COOKIE_NAME];
		if(cookie == null)
			return;

		var token = cookie.Value;
		
		var result = new AuthenticationAction().Validate(token, Request.Url.Host , Request.UserHostAddress);

		//Response.Write(result.ToJson());

		if(result.success) {
			
			this.SendToken(token);
		} else {
			
			
		}
		
	}

	void SendToken(string token) {
		// SSO 대상 사이트에 token 전달 및 세션 생성 요청
	//	Response.Write("token>" + token);
		foreach(string k in new AuthenticationAction().sites.Select(p=>p.login_url)) {
			HtmlImage img = new HtmlImage();
			img.Attributes["src"] = k + HttpUtility.UrlEncode(token);
			img.Style["display"] = "none";
			ph_setcookie.Controls.Add(img);
		}
		
		ph_setcookie.Visible = ph_setcookie2.Visible = true;
	}
	
}