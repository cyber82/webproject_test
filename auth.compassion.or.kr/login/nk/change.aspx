﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="change.aspx.cs" Inherits="login_nk_change" MasterPageFile="~/top.Master" %>
<%@ MasterType VirtualPath="~/top.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/validation/id.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#header").remove();

            $("[data-id=input_user_id]").bind("keyup", function () {
                $("[data-id=msg_user_id]").css("display", "none")
                $("[data-id=msg_user_id2]").hide();

            });

            $("#pwd").bind("keyup", function () {
                $("[data-id=msg_user_pwd").css("display", "none")
                $("[data-id=msg_user_pwd2]").hide();

            });

            $("#pwd_re").bind("keyup", function () {
                $("[data-id=msg_user_pwd").css("display", "none")
                $("[data-id=msg_user_pwd2]").hide();

            });

            $("#btn_submit").click(function () {
                /*
                if ($("#newUserId").val() == "") {
                    $("[data-id=msg_user_id]").html("아이디 중복검사를 해주세요");
                    $("[data-id=msg_user_id2]").show();
                    $("[data-id=btn_check_id]").focus();
                    return false;
                }
                */
                // 아이디 변경 안하는 경우 있음
                if ($("[data-id=input_user_id]").length > 0) {
                    var id_result = idValidation.check($("[data-id=input_user_id]"), 6, 12);
                    $("[data-id=msg_user_id").removeClass("guide_comment2")
                    $("[data-id=msg_user_id2]").hide();
                    if (!id_result.result) {
                        $("[data-id=msg_user_id]").html(id_result.msg).addClass("guide_comment2").show();
                        $("[data-id=msg_user_id2]").show();
                        return false;
                    }
                    $("#newUserId").val($("[data-id=input_user_id]").val());
                }

                // 비번
                var pwd_result = passwordValidation.check($("#pwd"), $("#pwd_re"), 5, 15);
                if (!pwd_result.result) {

                    $("#pwd").focus();
                    $("[data-id=msg_user_pwd]").html(pwd_result.msg).addClass("guide_comment2").show();
                    $("[data-id=msg_user_pwd2]").show();

                    return false;
                }


            });

            setUserIdEvent();

        })
        
        var setUserIdEvent= function () {

            var input = $("input[data-id=input_user_id]");
            var msg = $("[data-id=msg_user_id]");
            //console.log(input.val().length);
		
            $("[data-id=input_user_id]").focusout(function () {
        
                $("[data-id=msg_user_id]").css("display", "none")
                msg.html("");
                check();
            });

            var check = function () {

                $("#user_id").val("");

            
                var id_result = idValidation.check(input, 6, 12);
                $("[data-id=msg_user_id]").removeClass("guide_comment2");
                if (!id_result.result) {
                    $("[data-id=msg_user_id").addClass("guide_comment2").show();
                    $("[data-id=msg_user_id2]").show();
                    msg.html(id_result.msg);
                    return;
                }


                var user_id = input.val();
                $.get("/api/join.ashx", { t: "exist-id", c: user_id }, function (r) {
                    $("[data-id=msg_user_id2]").removeClass("guide_comment2");
                    if (r.success) {
                        if (r.data.exist) {
                            $("[data-id=msg_user_id").addClass("guide_comment2").show();
                            $("[data-id=msg_user_id2]").show();
                            msg.html("이미 사용중입니다.");
                        } else {
                            $("[data-id=msg_user_id").addClass("guide_comment1").show();
                            $("[data-id=msg_user_id2]").show();
                            $("#newUserId").val(r.data.user_id);
                            msg.html("사용 가능한 아이디입니다.");
                        }

                    }

                });

            }

        }

    	var goLogin = function (r) {
    		location.href = "/login/NK/?r=" + escape(r);
    	}
    </script>

    </asp:PlaceHolder>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="newUserId"  />
  <!-- logo -->
	<header class="topLogo">
		<div class="w980">
			<a href="<%: ConfigurationManager.AppSettings["default_return_url"] %>"><img src="/common/img/common/logo_4.png" class="logo" alt="compassion 로고" /></a>
		</div>
	</header>
    <!--// logo -->

    <!-- sub body -->
	<section class="sub_body">

		<!-- s: sub contents -->
		<div class="subContents padding0 integration">

			<div class="bgContent">
				<div class="w980">
					<h1 class="tit">한국컴패션 회원 아이디 통합</h1>
					<span class="bar"></span>
					<asp:PlaceHolder runat="server" ID="ph_newid2" Visible="false" >
					<p class="con">하나의 통합 아이디로 변경이 필요합니다.<br />변경하실 아이디/비밀번호를 입력해 주세요.</p>
					</asp:PlaceHolder>
					<asp:PlaceHolder runat="server" ID="ph_existid2" Visible="false" >
					<p class="con">기존 아이디의 사용이 가능합니다.<br />변경하실 비밀번호를 입력해 주세요.</p>
					</asp:PlaceHolder>
				</div>
			</div>

			<div class="w980 idpw">
				
				<h2 class="int_tit">ID/PW</h2>

				<!-- 입력박스 -->
				<div class="inputBox">
					<asp:PlaceHolder runat="server" ID="ph_newid" Visible="false" >
					<div class="item clear2">
						<label class="field id" for="id"><span class="icon"></span><span class="vam">아이디</span></label>
						<span class="input"><input type="text" data-id="input_user_id" placeholder="아이디 (6~12자 이내의 영문 소문자+숫자)" /></span>
                        <!--
						<a href="#" class="btn_type8" data-id="btn_check_id">중복검사</a>
                            -->
						<p class="guide_wrap" data-id="msg_user_id2" style="display:none" ><span  data-id="msg_user_id" class="guide_comment2"></span></p>
					</div>
					</asp:PlaceHolder>

					<asp:PlaceHolder runat="server" ID="ph_existid" Visible="false" >
					<div class="item clear2">
						<label class="field id" for="id"><span class="icon"></span><span class="vam">아이디</span></label>
						<span class="input"><asp:Literal runat="server" ID="txt_userId" /></span>
					</div>
					</asp:PlaceHolder>

					<div class="item clear2">
						<label class="field pw" for="pwd"><span class="icon"></span><span class="vam">비밀번호</span></label>
						<span class="input"><input type="password" runat="server" id="pwd" placeholder="비밀번호 (띄어쓰기 없이 영문, 숫자, 특수문자 3가지 조합으로 5~15자)" /></span>
					</div>
					<div class="item clear2">
						<label class="field pw" for="pwd_re"><span class="icon"></span><span class="vam">비밀번호 재확인</span></label>
						<span class="input"><input type="password" id="pwd_re" placeholder="" /></span>
                        <p class="guide_wrap" data-id="msg_user_pwd2" style="display:none" ><span data-id="msg_user_pwd" class="guide_comment2"></span></p>
					</div>
				</div>
				<!--// 입력박스 -->

				<div class="tac">
					<asp:LinkButton runat="server" ID="btn_submit" class="btn_type1 mb60" OnClick="btn_submit_Click">아이디 통합</asp:LinkButton>
				</div>

				<div class="box_type4 padding1 tac">
					<span class="s_con1">회원통합관련 문의 : <span class="fc_blue">후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_blue">info@compassion.or.kr</a></span></span>
				</div>
			</div>
			<div class="h100"></div>
		</div>
		<!--// e: sub contents -->

    </section>
    <!--// sub body -->

</asp:Content>

