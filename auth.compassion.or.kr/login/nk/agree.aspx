﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="agree.aspx.cs" Inherits="login_nk_agree" MasterPageFile="~/top.Master" %>

<%@ MasterType VirtualPath="~/top.master" %>



<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        $(function () {
            $("#header").remove();
            $("#btn_login").click(function () {

                return true;
            })

         
        })

    </script>

    </asp:PlaceHolder>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    
	 <!-- logo -->
	<header class="topLogo">
		<div class="w980">
			<a href="<%: ConfigurationManager.AppSettings["default_return_url"] %>"><img src="/common/img/common/logo_4.png" class="logo" alt="compassion 로고" /></a>
		</div>
	</header>
    <!--// logo -->

    <!-- sub body -->
	<section class="sub_body">

		<!-- s: sub contents -->
		<div class="subContents padding0 integration">

			<div class="bgContent">
				<div class="w980">
					<h1 class="tit">한국컴패션 회원 통합 및 약관 동의 안내</h1>
					<span class="bar"></span>
					<p class="con">
						컴패션 사역훈련 홈페이지를 이용해주셔서 감사합니다.<br />
						한국컴패션 ID와 컴패션 사역훈련 ID가 하나의 한국컴패션 ID로 통합(전환)하였습니다.<br />
						하나의 ID로 통합(전환)하여 한국컴패션 웹사이트와 컴패션 사역훈련 웹사이트를 더욱 편리하게 이용하세요.
					</p>
				</div>
			</div>

			<div class="w980 agree">
				
				<h2 class="int_tit">약관동의</h2>

				<div class="agreeBox clear2">
					<div class="box1">
						<p class="con">이용약관을 확인하였으며,<br />한국컴패션 서비스 이용을 위해 이용약관에 동의합니다.</p>
						<a href="/etc/terms" class="btn_s_type2" target="_blank">약관 전문 보기</a>
					</div>
					<div class="box2">
						<p class="con">개인정보 수집 및 이용에 대한 안내를 확인하였으며,<br />한국컴패션 서비스 이용을 위해 동의합니다.</p>
						<a href="/etc/privacy" class="btn_s_type2" target="_blank">약관 전문 보기</a>
					</div>
				</div>

				<p class="txt1">이용 약관 및 개인 정보 이용 안내를 확인하였으며, 위 내용에 동의합니다.</p>

				<a href="/login/NK/change" class="btn_type1 mb60">동의하고 회원정보 통합</a>


				<div class="box_type4 padding1 tac">
					<span class="s_con1">한국컴패션과 사역훈련 웹사이트 아이디가 다른 경우, 하나의 한국컴패션 아이디로 통합됩니다.</span>
				</div>
			</div>
			<div class="h100"></div>
		</div>
		<!--// e: sub contents -->

    </section>
    <!--// sub body -->

</asp:Content>

