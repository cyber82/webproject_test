﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class _login : FrontBasePage {
	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}
    protected override void OnBeforePostBack() {
        user_id.Focus();
        Master.HideDetailNavigation();
        var return_url = HttpUtility.UrlEncode(Request["r"].EmptyIfNull()).ValueIfNull(ConfigurationManager.AppSettings["default_return_url"]);
	//	if (return_url.IndexOf("/my/") > -1) {
	//		return_url = ConfigurationManager.AppSettings["default_return_url"];
	//	}

        var adminflag = Request.QueryString["f"].EmptyIfNull();
        // 백오피스 기능 : 관리자 페이지에서 UserID만 가지고 로그인 후 세션 생성
        if (adminflag == "ADMINCHECK")
        {
            this.ViewState["r"] = return_url + "?f="+ adminflag;
            var userId = Request.QueryString["i"].EmptyIfNull();

            //Response.Write(userId + " | " + adminflag + " | " + return_url);
            if (string.IsNullOrEmpty(userId))
            {
                // 창 닫기
                //Response.Write("<script language='javascript'>alert(잘못된 접근입니다.); window.close();</script>");
                this.CheckStatus();

            }
            else
            {
                this.Login(userId, "ADMINCHECK");
            }

        }
        else
        {
            this.ViewState["r"] = return_url;
            // SSO 사이트의 세션만료등을 다시 로그인 시도를 위해 페이지에 접근했을때 로그인상태가 활성 상태면 자동 로그인 시킨다.
            // action=LOGIN 과 같이 값을 넘기면 자동로그인 체크를 하지 않는다.
            var action = Request.QueryString["action"].EmptyIfNull();
            if (action == "")
            {
                this.CheckStatus();

                // 
            }
            else if (action == "REFER")
            {

                var userId = Request.Form["userId"];
                //var userPwd = Request.Form["userPwd"].EscapeSqlInjection();
                var userPwd = Request.Form["userPwd"];
                user_id.Text = userId;
                user_pwd.Text = userPwd;

                if (user_id.Text == "" || user_pwd.Text == "")
                {
                    chk_info.Visible = true;

                    return;
                }

                //[이종진] sessionId추가
                var sessionId = Request.Form["sessionId"];
                this.Login(userId, userPwd, sessionId);

            }
        }

		
    }


    protected void login_Click( object sender, EventArgs e ) {

		if(user_id.Text == "" || user_pwd.Text == "") {
            chk_info.Visible = true;

            return;
		}

		var userId = user_id.Text;
		var userPwd = user_pwd.Text;

		this.Login(userId , userPwd);

	}

	void Login(string userId , string userPwd, string sessionId="") {

		var auth = new AuthUser();
        JsonWriter result = auth.Login(userId, userPwd, sessionId);
        if (!result.success) {
            string data = result.data == null ? "null" : result.data.ToString();
            ErrorLog.Write(HttpContext.Current, 9987, "auth.login.result " + result.message + " result.data :" + data);
            chk_info.Visible = true;
			//base.AlertWithJavascript(result.message);
			return;
		}

		this.SendToken(result.data.ToString());
	}

	void CheckStatus() {
		//Response.Write(FrontLoginSession.GetDomain(HttpContext.Current));
		HttpCookie cookie = Request.Cookies[Constants.TOKEN_COOKIE_NAME];
        if (cookie == null)
        {
            ErrorLog.Write(HttpContext.Current, 9987, "cookie is null ");
            return;
        }

		var token = cookie.Value;
	   // HttpContext.Current.Response.Write("enc : " + HttpUtility.UrlEncode(token) + "<br>");
		var result = new AuthenticationAction().Validate(token, Request.Url.Host , Request.UserHostAddress);
        ErrorLog.Write(HttpContext.Current, 9999, result.success.ToString());
        if (result.success) {
			
			this.SendToken(token);
		} else {
            ErrorLog.Write(HttpContext.Current, 9987, "token validation failed : " + result.message);
            //	Response.Write(result.ToJson());
        }
		
	}

	void SendToken(string token) {
        // SSO 대상 사이트에 token 전달 및 세션 생성 요청
        foreach (string k in new AuthenticationAction().sites.Select(p=>p.login_url)) {
			HtmlImage img = new HtmlImage();
			img.Attributes["src"] = k + HttpUtility.UrlEncode(token);
			img.Style["display"] = "none";
			ph_setcookie.Controls.Add(img);
		}
        ph_setcookie.Visible = ph_setcookie2.Visible = true;
	}

	

	
	

}