﻿
var cert_host = "";
// 본인인증
function cert_openPopup(method, host) {
	cert_host = host;
	cert_setDomain();
	if (!host)
		host = location.protocol + "//" + location.host;
	
	/*
	if (location.hostname.startsWith("m.")) {

		var path = (method == "phone") ? host+'/cert/by-phone/' : host+'/cert/by-ipin/';

		$.get("/common/popup/cert-template", {path : path} ,  function (r) {

			$("body").append(r);

		});
		return;
	}
	*/


	var pop = null;
	if (method == "phone") {
		pop = window.open(host+'/cert/by-phone/', 'PCCWindow', 'width=430, height=560, resizable=1, scrollbars=no, status=0, titlebar=0, toolbar=0, left=300, top=200');
		
	} else if (method == "ipin") {
		
		pop = window.open(host+'/cert/by-ipin/', 'IPINWindow', 'width=450, height=500, resizable=0, scrollbars=no, status=0, titlebar=0, toolbar=0, left=300, top=200');
	}

	if (pop == null) {
		alert(" ※ 윈도우 XP SP2 또는 인터넷 익스플로러 7 사용자일 경우에는 \n    화면 상단에 있는 팝업 차단 알림줄을 클릭하여 팝업을 허용해 주시기 바랍니다. \n\n※ MSN,야후,구글 팝업 차단 툴바가 설치된 경우 팝업허용을 해주시기 바랍니다.");
	}

}

// virtual , 본인인증 결과
function cert_setCertResult(method, result, ci, di, name, birth, sex, phone) {

}

// 실명인증
function cert_nameCheck( name , jumin1 , jumin2 , host ) {

	cert_host = host;
	if (name.val() == "") {
		alert("이름을 입력해주세요.");
		name.focus();
		return false;
	}

	if (jumin2 == null) {

		if (jumin1.val().length != 13) {
			alert("주민등록번호를 정확히 입력해주세요");
			jumin1.focus();
			return false;
		}
		strJumin1 = jumin1.val().substr(0, 6);
		strJumin2 = jumin1.val().substr(6, 7);


	}else{
		if (jumin1.val() == "") {
			alert("주민번호 앞6자리를 입력해주세요.");
			jumin1.focus();
			return false;
		}

		if (jumin2.val() == "") {
			alert("주민번호 뒤7자리를 입력해주세요.");
			jumin2.focus();
			return false;
		}

		strJumin1 = jumin1.val();
		strJumin2 = jumin2.val();
	}

	if ($("#_name_check_iframe").length > 0) {
		$("#_name_check_iframe").remove();
	}

	cert_setDomain();
	if (!host)
		host = "";
	var url = host + "/cert/name-check/?name=" + encodeURIComponent(name.val()) + "&jumin1=" + strJumin1 + "&jumin2=" + strJumin2;
	var el = $("<iframe id='_name_check_iframe' src='" + url + "' style='display:none'/>");
	$("body").append(el);

}

// virtual , 실명인증 결과 
function cert_setNameCheckResult(result , ci , di , name , jumin , msg) {

} 

function cert_setDomain() {
	//return;
	//if (location.hostname.startsWith("auth")) return;
	if (location.hostname.indexOf("compassionko.org") > -1)
		document.domain = "compassionko.org";
	else if (location.hostname.indexOf("compassionkr.com") > -1)
		document.domain = "compassionkr.com";
	else
		document.domain = "compassion.or.kr";

}

