﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;

public partial class cert_by_name_check_result : FrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	public String retInfo;        //복호화 전 결과정보
	public String decStr;         //복호화 후 결과정보

	public String name;         //성명
	public String sex;              //성별
	public String birYMD;           //생년월일
	public String fgnGbn;           //내외국인 구분값	
	public String di;               //DI
	public String ci1;              //CI1
	public String ci2;              //CI2
	public String civersion;        //CI Version

	public String reqNum;           //요청번호
	public String result;           //본인확인 결과 (Y/N)
	
	//예약 필드

	public String decStr1;
	public String decStr2;
	public String hashStr;
	public String encPara;
	public String encMsg;
	public String msgChk;      //위조/변조 검증 결과
	public string registerNo1, registerNo2;
	public string Result;
	public string MemId;
	public string Mmdd;
	public string msg;

	string GetResultMsg(string code ) {
		if(code == "1") {
			return "실명인증 성공";
		} else if(code == "2") {
			return "요청한 주민등록번호와 성명이 불일치합니다.";
		} else if(code == "3") {
			return "미등록번호 입니다. ";
		} else if(code == "4") {
			return "후원자님의 정보와 주민등록번호가 일치하지 않습니다.";
		} else if(code == "5") {
			return "시스템장애입니다.관리자에게 문의해주세요";
		} else if(code == "7") {
			return "명의도용방지 주민번호입니다";
		} else if(code == "8") {
			return "14세미만 실명인증 불가";
		} else if(code == "0") {
			return "실명인증실패입니다";
		} else if(code == "9") {
			return "이미 등록된 후원자입니다";
		} else if(code == "E") {
			return "기타에러_에러 및 실명인증정보 받기 실패";
		}

		return code;
		//- 실명인증 ResultCode
		//- 1 : 실명인증 성공                               (후원자등록가능/실명인증정보넣기)
		//- 2 : 요청한 주민등록번호와 성명이 불일치합니다.  (후원자등록가능)
		//- 3 : 미등록번호 입니다.                          (후원자등록가능)
		//- 4 : 주민번호 형식이 올바르지 않습니다.          (후원자등록가능)
		//- 5 : 시스템장애입니다.관리자에게 문의해주세요.   (후원자등록가능)
		//- 7 : 명의도용방지 주민번호입니다.                (후원자등록가능)
		//- 8 : 14세미만 실명인증 불가                      (후원자등록가능)
		//- 0 : 실명인증실패입니다.                         (후원자등록가능)    
		//- 9 : 이미 등록된 후원자입니다.                   (후원자등록불가능)
		//- E : 기타에러_에러 및 실명인증정보 받기 실패     (후원자등록불가능)

	}

	protected override void OnBeforePostBack() {

		//01. 쿠키값 확인
		if(Request.Cookies["UserSettings"] != null) {
			reqNum = Request.Cookies["UserSettings"]["reqNum"];

			//02. 쿠키 정보 획득후 삭제
			Response.Cookies["UserSettings"].Expires = DateTime.Now.AddDays(0d);

			Response.CacheControl = "no-cache";
			Response.AddHeader("Pragma", "no-cache");
			Response.Expires = 0;
			Response.Buffer = true;

			retInfo = Request["retInfo"];

			SCISecurityLib.SEED secux = new SCISecurityLib.SEED();
			//03. 복호화시 reqNum를 복호화 Key로 사용
			decStr = secux.PccSeedDecript(retInfo, reqNum);


			//04. 복호화 짜르기 "/"
			//데이터 조합 : "실명확인1차암호화값/위변조검증값/암복화확장변수"
			String[] strArr1 = decStr.Split('/');
			encPara = strArr1[0];   //실명확인 1차 암호화값
			encMsg = strArr1[1];    //위변조검증값

			//05. 위변조값 생성
			SCISecurityLib.AES hmac = new SCISecurityLib.AES();
			hashStr = hmac.PccHMacEncript(encPara);

			//06. 서버에서 내려준 위변조값과 클라이언트에서 만든 위변조값 비교
			if(hashStr == encMsg) {
				msgChk = "T";
			}

			if(msgChk == "T") {
				//07. 위변조가 정상일때 "실명확인 1차 암호화값"을 복호화
				//복호화시 reqNum를 Key로 사용
				decStr = secux.PccSeedDecript(encPara, reqNum);
				//	Response.Write(decStr + "<br>");
				String[] strArr = decStr.Split('/');

				reqNum = strArr[0];
				registerNo1 = strArr[1];
				registerNo2 = strArr[2];
				name = strArr[3];
				result = strArr[4] == "1" ? "Y" : "N";
				msg = GetResultMsg(strArr[4]);
				di = strArr[5];
				ci1 = strArr[6];
				civersion = strArr[7];
				MemId = strArr[8];
				//EncKey = strArr[9];
				Mmdd = strArr[10];

				di = secux.PccSeedDecript(di, reqNum);
				ci1 = secux.PccSeedDecript(ci1, reqNum);



			}

			System.Runtime.InteropServices.Marshal.ReleaseComObject(secux);
			secux = null;

			System.Runtime.InteropServices.Marshal.ReleaseComObject(hmac);
			hmac = null;


		}

	}

}