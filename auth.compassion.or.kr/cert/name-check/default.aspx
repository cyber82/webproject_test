﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="cert_by_name_check" %>

<!DOCTYPE html>
<html lang="ko">

<head>
	<title runat="server" id="title">compassion</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/function.js"></script>
	<script type="text/javascript">
		$(function () {
			$("#frm").submit();
		});

	</script>
</head>
<body>

	
<!-- 본인확인서비스 요청 form --------------------------->
<form id="frm" name="reqCBAForm" runat="server" method="post">
    <input type="hidden" name="reqInfo"     value = "<%=encStr%>">
    <input type="hidden" name="ok_url"      value = "<%=retUrl%>">
</form>

</body>

</html>




