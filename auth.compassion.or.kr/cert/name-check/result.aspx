﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="result.aspx.cs" Inherits="cert_by_name_check_result" %>

<!DOCTYPE html>
<html lang="ko">

<head>
	<title runat="server" id="title">compassion</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/function.js"></script>
	<script type="text/javascript" src="/cert/cert.js"></script>
	<script type="text/javascript">
		$(function () {
			cert_setDomain();
			parent.cert_setNameCheckResult( '<%:result%>' ,'<%:ci1%>' ,'<%:di%>' ,'<%:name%>' ,'<%:registerNo1 + registerNo2%>' , '<%:msg%>'  );
		});

	</script>

</head>
<body>

	<div style="display:block">
  <table cellpadding="1" cellspacing="1" border="1">
				
				<tr>
                    <td align="left">성명</td>
                    <td align="left"><%=name%></td>
                </tr>
				<tr>
                    <td align="left">주민번호</td>
                    <td align="left"><%=registerNo1%><%=registerNo2%></td>
                </tr>
						
				<tr>
                    <td align="left">중복가입자정보</td>
                    <td align="left"><%=di%></td>
                </tr>
				<tr>
                    <td align="left">연계정보1</td>
                    <td align="left"><%=ci1%></td>
                </tr>
				
				<tr>
                    <td align="left">연계정보버전</td>
                    <td align="left"><%=civersion%></td>
                </tr>
                <tr>
                    <td align="left">요청번호</td>
                    <td align="left"><%=reqNum%></td>
                </tr>
				<tr>
                    <td align="left">인증성공여부</td>
                    <td align="left"><%=result%></td>
                </tr>
			
				
            </table>      
	</div>

    <asp:HiddenField ID="hdResult" value=<%=result%> />
    <asp:HiddenField ID="hdDI" value=<%=di%> />
    <asp:HiddenField ID="hdCI" value=<%=ci1%> />
</body>

</html>


