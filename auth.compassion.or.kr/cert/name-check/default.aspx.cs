﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;

public partial class cert_by_name_check : FrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	public string encStr;
	public string retUrl;

	string GetReqNum() {

		//요청시간
		string curDate = DateTime.Now.ToString("yyyyMMddHHmmss");

		String ranData = "";
		Random ran = new Random();
		for(int j = 0; j < 6; j++) {
			ranData = ranData + ran.Next(10);
		}

		//요청 번호 생성
		return curDate + ranData;
		
	}

	protected override void OnBeforePostBack() {
		
		//01. Seed 암호화 변수 선언
		SCISecurityLib.SEED secux = new SCISecurityLib.SEED();

		Response.CacheControl = "no-cache";
		Response.AddHeader("Pragma", "no-cache");
		Response.Expires = 0;
		Response.Buffer = true;

		string id = "HCF001";
		string srvNo = ConfigurationManager.AppSettings["cert_srvNo_namecheck"];
		string reqNum = GetReqNum();
		string name = Request["name"];
		string jumin1 = Request["jumin1"];
		string jumin2 = Request["jumin2"];
		retUrl = string.Format("https://{0}/cert/name-check/result", Request.Url.Host);
		
		string exVar = "0000000000000000";

		//02. 암호화 파라미터 조합
		//   reqInfo  = id + "^" + srvNo + "^" + reqNum + "^" + certDate + "^" + certGb + "^" + addVar + "^" + exVar;

		string retInfo = string.Concat(id, "/", jumin1, "/", jumin2, "/", name, "/", reqNum, "/", srvNo, "////", exVar);

		//03. 요청 정보 1차 암호화
		string encStr1 = secux.PccSeedEncript(retInfo, "");

		//04. 위변조 검증값 생성
		SCISecurityLib.AES hmac = new SCISecurityLib.AES();
		string hashStr = hmac.PccHMacEncript(encStr1);

		//05. 요청정보 2차 암호화값
		//데이터 생성 규칙 : "요청정보 1차 암호화/위변조검증값/암복화 확장 변수"
		encStr = secux.PccSeedEncript(encStr1 + "/" + hashStr + "/0000000000000000", "");


		string strRegFirst = jumin2.Substring(0, 1);
		if(strRegFirst.Equals("5") || strRegFirst.Equals("6") || strRegFirst.Equals("7") || strRegFirst.Equals("8"))
			frm.Attributes["action"] = "https://name.siren24.com/servlet/foreign_name_check_seed"; //(외국인실명확인)

		else if(strRegFirst.Equals("1") || strRegFirst.Equals("2") || strRegFirst.Equals("3") || strRegFirst.Equals("4"))
			frm.Attributes["action"] = "https://name.siren24.com/servlet/name_check_seed"; //(내국인실명확인)

		//06. reqNum 값을 쿠키로 생성 하기
		/**
		*
		* reqNum 값은 최종 결과값 복호화를 위한 SecuKey로 활용 되므로 중요합니다.
		* reqNum 은 본인 확인 요청시 항상 새로운 값으로 중복 되지 않게 생성 해야 합니다.
		* 쿠키 또는 Session및 기타 방법을 사용해서 reqNum 값을 
		* vname_result_seed.aspx에서 가져 올 수 있도록 해야 함.
		* 샘플을 위해서 쿠키를 사용한 것이므로 참고 하시길 바랍니다.
		* 
		*/
		Response.Cookies["UserSettings"]["reqNum"] = reqNum;
		Response.Cookies["UserSettings"].Expires = DateTime.Now.AddDays(1d);

		Marshal.ReleaseComObject(secux);
		secux = null;

		Marshal.ReleaseComObject(hmac);
		hmac = null;

	}

}