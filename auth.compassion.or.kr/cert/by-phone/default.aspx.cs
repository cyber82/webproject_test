﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;

public partial class cert_by_phone : FrontBasePage {

	public string result = "";
    public string ci1 = "";
	public string di = "";
	public string name = "";
	public string birYMD = "";
	public string sex = "";
	public string cellNo = "";
	
	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	public string encStr;
	public string retUrl;

	string GetReqNum() {

		//요청시간
		string curDate = DateTime.Now.ToString("yyyyMMddHHmmss");

		String ranData = "";
		Random ran = new Random();
		for(int j = 0; j < 6; j++) {
			ranData = ranData + ran.Next(10);
		}

		//요청 번호 생성
		return curDate + ranData;
		
	}

	protected override void OnBeforePostBack() {
		
		//01. Seed 암호화 변수 선언
		SCISecurityLib.SEED secux = new SCISecurityLib.SEED();

		Response.CacheControl = "no-cache";
		Response.AddHeader("Pragma", "no-cache");
		Response.Expires = 0;
		Response.Buffer = true;

		string id = "SHCF001";
		string srvNo = ConfigurationManager.AppSettings["cert_srvNo_phone"];
		
		string reqNum = GetReqNum();
		string certDate = DateTime.Now.ToString("yyyyMMddHHmmss");
		string certGb = "H";
		string addVar = "";
		retUrl = string.Format("22https://{0}/cert/by-phone/result" , Request.Url.Host);
		string exVar = "0000000000000000";

		//02. 암호화 파라미터 조합
		string reqInfo = id + "^" + srvNo + "^" + reqNum + "^" + certDate + "^" + certGb + "^" + addVar + "^" + exVar;

		//03. 요청 정보 1차 암호화
		string encStr1 = secux.PccSeedEncript(reqInfo, "");

		//04. 위변조 검증값 생성
		SCISecurityLib.AES hmac = new SCISecurityLib.AES();
		string hashStr = hmac.PccHMacEncript(encStr1);

		//05. 요청정보 2차 암호화값
		//데이터 생성 규칙 : "요청정보 1차 암호화^위변조검증값^암복화 확장 변수"
		encStr = secux.PccSeedEncript(encStr1 + "^" + hashStr + "^" + "0000000000000000", "");

		//06. reqNum 값을 쿠키로 생성 하기
		/**
		*
		* reqNum 값은 최종 결과값 복호화를 위한 SecuKey로 활용 되므로 중요합니다.
		* reqNum 은 본인 확인 요청시 항상 새로운 값으로 중복 되지 않게 생성 해야 합니다.
		* 쿠키 또는 Session및 기타 방법을 사용해서 reqNum 값을 
		* vname_result_seed.aspx에서 가져 올 수 있도록 해야 함.
		* 샘플을 위해서 쿠키를 사용한 것이므로 참고 하시길 바랍니다.
		* 
		*/
		Response.Cookies["UserSettings"]["reqNum"] = reqNum;
		Response.Cookies["UserSettings"].Expires = DateTime.Now.AddDays(1d);

		Marshal.ReleaseComObject(secux);
		secux = null;

		Marshal.ReleaseComObject(hmac);
		hmac = null;

	}

}