﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="result.aspx.cs" Inherits="cert_by_phone_result" %>

<!DOCTYPE html>
<html lang="ko">

<head>
	<title runat="server" id="title">compassion</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="/assets/jquery/jquery.json-2.4.min.js"></script>
	<script type="text/javascript" src="/assets/jquery/jquery.json-3.3.2.min.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/cookie.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/function.js"></script>
	
	<script type="text/javascript" src="/cert/cert.js"></script>
	<script type="text/javascript">
		$(function () {
			
			<%if (AppSession.HasCookie(this.Context)){%>
				
				var entity = $.parseJSON(cookie.get("cps.app"));
				var appDevice = entity.device;
				
				if (appDevice == "android") {

					setTimeout(function () {
						JSInterface.setCert('phone' , '<%:result%>' , '<%:ci1%>' , '<%:di%>' , '<%:name%>' , '<%:birYMD%>' , '<%:sex%>' , '<%:cellNo%>');
					}, 100);

				} else if (appDevice == "iphone") {

					setTimeout(function () {

						window.location = "app://setCert?method=phone&result=<%:result%>&ci=<%:HttpUtility.UrlEncode(ci1)%>&di=<%:HttpUtility.UrlEncode(di)%>&name=<%:name%>&birYMD=<%:birYMD%>&sex=<%:sex%>&cellNo=<%:cellNo%>";
					} , 1000)
				}
			
			<%} else {%>
				cert_setDomain();
				opener.cert_setCertResult('phone', '<%:result%>', '<%:ci1%>', '<%:di%>', '<%:name%>', '<%:birYMD%>', '<%:sex%>', '<%:cellNo%>');
				self.close();
			<%} %>


		});

	</script>

</head>
<body>

	<div style="display:none">
	<table cellpadding="1" cellspacing="1" border="1">

		<tr>
			<td align="left">성명</td>
			<td align="left"><%=name%></td>
		</tr>
		<tr>
			<td align="left">성별</td>
			<td align="left"><%=sex%></td>
		</tr>
		<tr>
			<td align="left">생년월일</td>
			<td align="left"><%=birYMD%></td>
		</tr>
		<tr>
			<td align="left">내외국인 구분값(1:내국인, 2:외국인)</td>
			<td align="left"><%=fgnGbn%></td>
		</tr>
		<tr>
			<td align="left">중복가입자정보</td>
			<td align="left"><%=di%></td>
		</tr>
		<tr>
			<td align="left">연계정보1</td>
			<td align="left"><%=ci1%></td>
		</tr>
		<tr>
			<td align="left">연계정보2</td>
			<td align="left"><%=ci2%></td>
		</tr>
		<tr>
			<td align="left">연계정보버전</td>
			<td align="left"><%=civersion%></td>
		</tr>
		<tr>
			<td align="left">요청번호</td>
			<td align="left"><%=reqNum%></td>
		</tr>
		<tr>
			<td align="left">인증성공여부</td>
			<td align="left"><%=result%></td>
		</tr>
		<tr>
			<td align="left">인증수단</td>
			<td align="left"><%=certGb%></td>
		</tr>
		<tr>
			<td align="left">핸드폰번호</td>
			<td align="left"><%=cellNo%>&nbsp;</td>
		</tr>
		<tr>
			<td align="left">이동통신사</td>
			<td align="left"><%=cellCorp%>&nbsp;</td>
		</tr>
		<tr>
			<td align="left">요청시간</td>
			<td align="left"><%=certDate%></td>
		</tr>
		<tr>
			<td align="left">추가파라미터</td>
			<td align="left"><%=addVar%>&nbsp;</td>
		</tr>

	</table>
	</div>


</body>

</html>


