﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="result.aspx.cs" Inherits="cert_by_ipin_result" %>

<!DOCTYPE html>
<html lang="ko">

<head>
	<title runat="server" id="title">compassion</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="/assets/jquery/jquery.json-2.4.min.js"></script>
	<script type="text/javascript" src="/assets/jquery/jquery.json-3.3.2.min.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/cookie.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/function.js"></script>
	
	<script type="text/javascript" src="/cert/cert.js"></script>

	<script type="text/javascript">
		$(function () {
			
			<%if (AppSession.HasCookie(this.Context)){%>
				
				var entity = $.parseJSON(cookie.get("cps.app"));
				var appDevice = entity.device;
				
				if (appDevice == "android") {

					setTimeout(function () {
						JSInterface.setCert('ipin', '<%:result%>', '<%:ciscrHash%>', '<%:discrHash%>', '<%:name%>', '<%:birth%>', '<%:sex%>', '');
					}, 100);

				} else if (appDevice == "iphone") {

					setTimeout(function () {

						window.location = "app://setCert?method=ipin&result=<%:result%>&ci=<%:HttpUtility.UrlEncode(ciscrHash)%>&di=<%:HttpUtility.UrlEncode(discrHash)%>&name=<%:name%>&birYMD=<%:birth%>&sex=<%:sex%>";
					} , 1000)
				}
			
			<%} else {%>
				cert_setDomain();
				opener.cert_setCertResult('ipin', '<%:result%>', '<%:ciscrHash%>', '<%:discrHash%>', '<%:name%>', '<%:birth%>', '<%:sex%>');
				self.close();
			<%} %>

			
		});

	</script>

</head>
<body>

		<div style="display:none">
	 <table cellpadding=1 cellspacing=1>
        <tr>
            <td align=left>요청번호</td>
            <td align=left><%=reqNum%></td>
        </tr>
        <tr>
            <td align=left>아이핀</td>
            <td align=left><%=vDiscrNo%></td>
        </tr>
        <tr>
            <td align=left>성명</td>
            <td align=left><%=name%></td>
        </tr>
        <tr>
            <td align=left>결과값</td>
            <td align=left><%=result%></td>
        </tr>
        <tr>
            <td align=left>연령대</td>
            <td align=left><%=age%></td>
        </tr>
        <tr>
            <td align=left>성별</td>
            <td align=left><%=sex%></td>
        </tr>
         <tr>
            <td align=left>ip</td>
            <td align=left><%=ip%></td>
        </tr>
        <tr>
            <td align=left>발급수단정보</td>
            <td align=left><%=authInfo%></td>
        </tr>
        <tr>
            <td align=left>생년월일</td>
            <td align=left><%=birth%></td>
        </tr>
        <tr>
            <td align=left>내/외국인구분</td>
            <td align=left><%=fgn%></td>
        </tr>
        <tr>
            <td align=left>중복가입확인정보</td>
            <td align=left><%=discrHash%></td>
        </tr>
		<tr>
            <td align=left>연계정보 버젼</td>
            <td align=left><%=ciVersion%></td>
        </tr>
		<tr>
            <td align=left>연계정보</td>
            <td align=left><%=ciscrHash%></td>
        </tr>
    </table>
</div>


</body>

</html>


