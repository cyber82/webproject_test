﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;

public partial class cert_by_ipin_result : FrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}


	public String retInfo;        // 복호화 전 결과정보
	public String decStr;         // 복호화 후 결과정보
	public String encPara;        // 1차 복호화값
	public String encMsg;         // 위변조값
	public String hashStr;        // 위변조값
	public String msgChk;         // 위변조 결과

	public String reqNum;         // 본인확인 요청번호
	public String vDiscrNo;       // 아이핀번호
	public String name;           // 성명
	public String age;            // 연령대
	public String sex;            // 성별
	public String result;         // 인증결과
	public String ip;             // User IP
	public String authInfo;       // 발급수단정보
	public String birth;          // 생년월일
	public String fgn;            // 내/외국인구분
	public String discrHash;      // 중복가입확인정보
	public String ciVersion;      // 연계정보 버젼
	public String ciscrHash;      // 연계정보


	protected override void OnBeforePostBack() {

		//01. 쿠키값 확인
		if(Request.Cookies["UserSettings"] == null) {
			return;
		}

		reqNum = Request.Cookies["UserSettings"]["reqNum"];
		//02. 쿠키 정보 획득후 삭제
		Response.Cookies["UserSettings"].Expires = DateTime.Now.AddDays(0d);

		Response.CacheControl = "no-cache";
		Response.AddHeader("Pragma", "no-cache");
		Response.Expires = 0;
		Response.Buffer = true;

		retInfo = Request["retInfo"];

		SCISecurityIPINLib.IPINSEED secux = new SCISecurityIPINLib.IPINSEED();

		//03. 복호화시 reqNum를 복호화 Key로 사용
		decStr = secux.SeedDecript(retInfo, reqNum);

		//04. 복호화 짜르기 "/"
		//데이터 조합 : "본인확인1차암호화값/위변조검증값/암복화확장변수"
		String[] strArr1 = decStr.Split('/');
		encPara = strArr1[0];   //본인확인 1차 암호화값
		encMsg = strArr1[1];    //위변조검증값

		//05. 위변조값 생성
		SCISecurityIPINLib.IPINAES hmac = new SCISecurityIPINLib.IPINAES();
		hashStr = hmac.HMacEncript(encPara);

		//06. 서버에서 내려준 위변조값과 클라이언트에서 만든 위변조값 비교
		if(hashStr == encMsg) {
			msgChk = "T";
		}

		if(msgChk == "T") {
			//07. 위변조가 정상일때 "본인확인 1차 암호화값"을 복호화
			//복호화시 reqNum를 Key로 사용
			decStr = secux.SeedDecript(encPara, reqNum);
			String[] strArr = decStr.Split('/');
			reqNum = strArr[0];
			vDiscrNo = strArr[1];
			name = strArr[2];
			result = strArr[3] == "1" ? "Y" : "N";
			age = strArr[4];
			sex = strArr[5];
			ip = strArr[6];
			authInfo = strArr[7];
			birth = strArr[8];
			fgn = strArr[9];
			discrHash = strArr[10];
			ciVersion = strArr[11];
			ciscrHash = strArr[12];

			//08. 중복가입확인정보 암호화값 복호화
			discrHash = secux.SeedDecript(discrHash, reqNum);

			//09. 연계정보 암호화값 복호화
			ciscrHash = secux.SeedDecript(ciscrHash, reqNum);

		}

		Marshal.ReleaseComObject(secux);
		secux = null;

		Marshal.ReleaseComObject(hmac);
		hmac = null;



	}

}