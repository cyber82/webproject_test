﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="find_id_complete" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript">
		$(function () {

		});

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
     <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>아이디 찾기</h1>
				<span class="desc">아이디/비밀번호를 잊으셨나요?</span>

			<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->
    <!-- s: sub contents -->
		<div class="subContents member">

			<div class="w980">

			
				<div class="find">

					<p class="info_txt1">
						회원님이 입력하신 정보와 일치하는 아이디입니다.<br />
						개인정보보호를 위해 개인정보의 일부는 ***로 표시하였습니다.
					</p>

					<div class="complete">
						<div class="id"><asp:Literal runat="server" ID="user_id"/></div>
						<div class="date">가입일: <asp:Literal runat="server" ID="reg_date"/></div>
					</div>

					<div class="btn_ac mb50">
						<div>
							<a href="/login/" class="btn_type1 fl mr10">로그인하기</a>
							<a href="/find/pwd/" class="btn_type4 fl">비밀번호 찾기</a>
						</div>
					</div>

				</div>

			</div>

		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->
	
</asp:Content>
