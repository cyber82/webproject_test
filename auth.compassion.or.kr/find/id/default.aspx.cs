﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class find_id_default : FrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack()
    {
        resetBirth();
    }
    protected override void OnAfterPostBack()
    {
        resetBirth();
    }

    void CheckByPhone() {
		var name = s1_name.Value;
		var phone = s1_phone.Value;
		var code = s1_code.Value;
        var birth = birthyyyy.Value + "-" + birthmm.Value + "-" + birthdd.Value;

        ErrorLog.Write(HttpContext.Current, 99, "폰ID찾기 web name : " + name + ", phone : " + phone + ", birth" + birth);

        if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(phone) || birth == "--")
        {
            base.AlertWithJavascript("조회정보를 입력해주세요");
            return;
        }

        if (Session["findSendPhoneCode"] == null || Session["findSendPhoneCode"].ToString() != code) {
			base.AlertWithJavascript("인증코드가 일치하지 않습니다.");
			return;
		}

        // 웹디비에서 먼저검색
        using (AuthDataContext dao = new AuthDataContext())
        {
            //var list = dao.sp_tSponsorMaster_get_f("", "", "Y", "", name.EscapeSqlInjection(), "", phone.EscapeSqlInjection()).ToList();
            Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
            Object[] op2 = new Object[] { "", "", "Y", "", name.EscapeSqlInjection(), "", phone.EscapeSqlInjection() };
            var list = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

            //2018-04-26 이종진 - 요청에의해 생년월일까지 비교하도록 함.
            //회원가입 시, 기존회원인지 체크하는것과 동일조건으로 하려함. top 1으로 1개만 조회되도록 되어있음. 
            if (list.Count > 0 && list[0].BirthDate == birth.Replace("-", ""))
            {
                var entity = list.First();
                var user_id = entity.UserId;
                var reg_date = ((DateTime)entity.RegisterDate).ToString("yyyy-MM-dd");
                Response.Redirect("complete/?user-id=" + user_id.Mask("*", 3) + "&reg_date=" + reg_date);
                return;
            }
        }

		WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
		Object[] objSql = new object[1] { string.Format(@"select top 1 sponsorId from tSponsorMaster with(nolock) where CurrentUse = 'Y' and sponsorId in (
select sponsorId from tSponsorCommunication where CommunicationType = '휴대번호' and CurrentUse = 'Y' and CommunicationContents='{0}') and sponsorName = '{1}' and birthdate = '{2}' and sponsorid not in (select sponsorid from tsponsordouble)", phone.EscapeSqlInjection(), name.EscapeSqlInjection(), birth) };
		DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

		this.Check(ds);

	}

    void CheckByEmail() {

        var name = s2_name.Value;
        var email = s2_email.Value;
        var birth = birthyyyy.Value + "-" + birthmm.Value + "-" + birthdd.Value;

        ErrorLog.Write(HttpContext.Current, 99, "이메일ID찾기 web name : " + name + ", email : " + email + ", birth" + birth);

        if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(email) || birth == "--")
        {
            base.AlertWithJavascript("조회정보를 입력해주세요");
            return;
        }

        if (Session["sendEmailCode"] == null) {
			base.AlertWithJavascript("인증코드가 정확하지 않습니다.");
			return;
		}

        using (AuthDataContext dao = new AuthDataContext())
        {
            //var entity = dao.user_email_auth.First(p => p.ue_email == email && p.ue_id == Guid.Parse(Session["sendEmailCode"].ToString());
            var entity = www6.selectQFAuth<user_email_auth>("ue_email", email, "ue_id", Guid.Parse(Session["sendEmailCode"].ToString()));

            if (!entity.ue_active)
            {
                base.AlertWithJavascript("인증되지 않았습니다.");
                return;
            }
        }

        // 웹디비에서 먼저검색
        using (AuthDataContext dao = new AuthDataContext())
        {
            //var list = dao.sp_tSponsorMaster_get_f("", "", "Y", "", name.EscapeSqlInjection(), email.EscapeSqlInjection(), "").ToList();
            Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
            Object[] op2 = new Object[] { "", "", "Y", "", name.EscapeSqlInjection(), email.EscapeSqlInjection(), "" };
            var list = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

            if (list.Count > 0 && list[0].BirthDate == birth.Replace("-",""))
            {
                var entity = list.First();
                var user_id = entity.UserId;
                var reg_date = ((DateTime)entity.RegisterDate).ToString("yyyy-MM-dd");
                Response.Redirect("complete/?user-id=" + user_id.Mask("*", 3) + "&reg_date=" + reg_date);
                return;
            }
        }

		WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
		Object[] objSql = new object[1] { string.Format(@"select top 1 sponsorId from tSponsorMaster with(nolock) where CurrentUse = 'Y' and sponsorId in (
select sponsorId from tSponsorCommunication where CommunicationType = '이메일' and CurrentUse = 'Y' and CommunicationContents='{0}') and sponsorName = '{1}' and birthdate = '{2}' and sponsorid not in (select sponsorid from tsponsordouble)", email.EscapeSqlInjection(), name.EscapeSqlInjection(), birth) };
		DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

		this.Check(ds);


	}

	// 본인인증
	void CheckByCert() {

		var ci = s3_ci.Value;

		if(string.IsNullOrEmpty(ci)) {
			base.AlertWithJavascript("븐인인증을 해주세요");
			return;
		}
		
		WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
		Object[] objSql = new object[1] { string.Format(@"select top 1 sponsorId from tSponsorMaster with(nolock) where CurrentUse = 'Y' and ci ='{0}' and sponsorid not in (select sponsorid from tsponsordouble)", ci) };
		DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

		this.Check(ds);

	}

	void Check(DataSet ds ) {

		if(ds.Tables[0].Rows.Count > 0) {

			var sponsorId = ds.Tables[0].Rows[0]["sponsorId"].ToString().Trim();

            using (AuthDataContext dao = new AuthDataContext())
            {
                //var list = dao.sp_tSponsorMaster_get_f("", "", "Y", sponsorId, "", "", "").ToList();
                Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                Object[] op2 = new Object[] { "", "", "Y", sponsorId, "", "", "" };
                var list = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

                if (list.Count > 0)
                {
                    var entity = list.First();
                    var user_id = entity.UserId;
                    var reg_date = ((DateTime)entity.RegisterDate).ToString("yyyy-MM-dd");
                    Response.Redirect("complete/?user-id=" + user_id.Mask("*", 3) + "&reg_date=" + reg_date);


                }
                else
                {
                    base.AlertWithJavascript("일치하는 회원정보가 없습니다.(웹회원정보 없음)");
                    return;
                }

            }


		} else {
			base.AlertWithJavascript("일치하는 회원정보가 없습니다.");
			return;
		}

	}

	protected void btn_submit_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			base.AlertWithJavascript("");
			return;
		}

		if(cur_type_index.Value == "0") {      // 휴대폰

			this.CheckByPhone();

		} else if(cur_type_index.Value == "1") {      // 이메일

			this.CheckByEmail();
		} else if(cur_type_index.Value == "2") {      // 본인인증

			this.CheckByCert();
		}


	}

    private void resetBirth()
    {
        if (s1_user_birth_yyyy.Items.Count > 0
            || s1_user_birth_mm.Items.Count > 0
            || s1_user_birth_dd.Items.Count > 0
            || s2_user_birth_yyyy.Items.Count > 0
            || s2_user_birth_mm.Items.Count > 0
            || s2_user_birth_dd.Items.Count > 0)
        {
            return;
        }

        for (int i = DateTime.Now.Year; i >= DateTime.Now.Year - 100; i--)
        {
            s1_user_birth_yyyy.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        s1_user_birth_yyyy.Items.Insert(0, new ListItem("생년", ""));

        for (int i = 1; i < 13; i++)
        {
            s1_user_birth_mm.Items.Add(new ListItem(i.ToString(), i.ToString().PadLeft(2, '0')));
        }
        s1_user_birth_mm.Items.Insert(0, new ListItem("월", ""));

        for (int i = 1; i < 32; i++)
        {
            s1_user_birth_dd.Items.Add(new ListItem(i.ToString(), i.ToString().PadLeft(2, '0')));
        }
        s1_user_birth_dd.Items.Insert(0, new ListItem("일", ""));

        for (int i = DateTime.Now.Year; i >= DateTime.Now.Year - 100; i--)
        {
            s2_user_birth_yyyy.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        s2_user_birth_yyyy.Items.Insert(0, new ListItem("생년", ""));

        for (int i = 1; i < 13; i++)
        {
            s2_user_birth_mm.Items.Add(new ListItem(i.ToString(), i.ToString().PadLeft(2, '0')));
        }
        s2_user_birth_mm.Items.Insert(0, new ListItem("월", ""));

        for (int i = 1; i < 32; i++)
        {
            s2_user_birth_dd.Items.Add(new ListItem(i.ToString(), i.ToString().PadLeft(2, '0')));
        }
        s2_user_birth_dd.Items.Insert(0, new ListItem("일", ""));
    }


}