﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="find_id_default" MasterPageFile="~/main.Master" %>

<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ MasterType VirtualPath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/id.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/email.js"></script>
    <script type="text/javascript" src="/cert/cert.js"></script>
    <script type="text/javascript" src="/find/id/default.js?v=1.0"></script>

    <script type="text/javascript">
        $(function () {


        });
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="cur_type_index" value="0" />
    <input type="hidden" runat="server" id="s1_phone" value="" />
    <input type="hidden" runat="server" id="s2_email" value="" />
     <input type="hidden" id="s3_ci" runat="server" />

    <input type="hidden" runat="server" id="birthyyyy" value="" />
    <input type="hidden" runat="server" id="birthmm" value="" />
    <input type="hidden" runat="server" id="birthdd" value="" />

    <!-- sub body -->
    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>아이디 찾기</h1>
                <span class="desc">아이디/비밀번호를 잊으셨나요?</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->
        <!-- s: sub contents -->
        <div class="subContents member">

            <div class="w980">

                <div class="member_type">
                    <a href="#" data-role="tab" class="on btn_member_type"><span></span>휴대폰</a>
                    <a href="#" data-role="tab" class="btn_member_type"><span></span>이메일</a>
                    <%--<a href="#" data-role="tab" class="btn_member_type"><span></span>본인인증</a>--%>
                </div>

                <div class="find">

                    <p class="info_txt1">회원가입 시 사용하신 회원정보로 아이디를 찾을 수 있습니다.</p>
                    <!-- 휴대폰 -->
                    <div class="find_box1" id="pn_s1" data-role="container">
                        <div class="regist">
                            <label for="s1_name" class="hidden">이름</label>
                            <input type="text" id="s1_name" runat="server" class="input_type1" placeholder="이름" maxlength="30" />
                        </div>
                        <div class="mb10"><span class="guide_comment2" id="msg_s1_name" style="display:none">이름를 입력해 주세요</span></div>
                        <div class="regist">
                            <label for="s1_user_birth_yyyy" class="hidden">생년월일(년)</label>
                            <span class="sel_type1 fl mr10" style="width: 180px">
                                <asp:DropDownList runat="server" ID="s1_user_birth_yyyy" class="custom_sel">
                                </asp:DropDownList>

                            </span>
                            <label for="s1_user_birth_mm" class="hidden">생년월일(월)</label>
                            <span class="sel_type1 fl mr10" style="width: 100px">
                                <asp:DropDownList runat="server" ID="s1_user_birth_mm" class="custom_sel"></asp:DropDownList>

                            </span>
                            <label for="s1_user_birth_dd" class="hidden">생년월일(일)</label>
                            <span class="sel_type1 fl" style="width: 100px">
                                <asp:DropDownList runat="server" ID="s1_user_birth_dd" class="custom_sel"></asp:DropDownList>

                            </span>
                        </div>
                        <div class="mb10"><span class="guide_comment2" id="msg_s1_birth" style="display:none">생년월일을 입력해 주세요</span></div>  
                        <div class="regist">
                            <label for="temp_s1_phone" class="hidden">휴대폰번호</label>
                            <input type="text" data-id="s1_phone" runat="server" id="temp_s1_phone" class="input_type1 number_only" placeholder="휴대폰 번호 (-없이 입력)" maxlength="11" />
                            <button class="btn_type7" id="s1_btn_auth">인증</button>
                        </div>
                        <div class="mb10"><span class="guide_comment2" id="msg_s1_phone" style="display:none">휴대폰번호를 입력해 주세요</span></div>
                        <div class="regist">
                            <label for="s1_code" class="hidden">인증번호</label>
                            <input type="text" id="s1_code" runat="server" placeholder="인증번호" maxlength="30" class="input_type1"/>
                            <button class="btn_type7" id="s1_btn_confirm">확인</button>
                        </div>
                        <span class="guide_comment2" id="s1_msg" style="display:none"></span>
                    </div>

                    <!-- 이메일 -->
                    <div class="find_box1 email" id="pn_s2" data-role="container" style="display: none">
                        <div class="regist">
                            <label for="s2_name" class="hidden">이름</label>
                            <input type="text" id="s2_name" runat="server" class="input_type1" placeholder="이름" maxlength="30" />
                            
                        </div>
                        <div class="mb10"><span class="guide_comment2" id="msg_s2_name" style="display:none">이름를 입력해 주세요</span></div>
                        <div class="regist">
                            <label for="s2_user_birth_yyyy" class="hidden">생년월일(년)</label>
                            <span class="sel_type1 fl mr10" style="width: 180px">
                                <asp:DropDownList runat="server" ID="s2_user_birth_yyyy" class="custom_sel"></asp:DropDownList>

                            </span>
                            <label for="s2_user_birth_mm" class="hidden">생년월일(월)</label>
                            <span class="sel_type1 fl mr10" style="width: 100px">
                                <asp:DropDownList runat="server" ID="s2_user_birth_mm" class="custom_sel"></asp:DropDownList>

                            </span>
                            <label for="s2_user_birth_dd" class="hidden">생년월일(일)</label>
                            <span class="sel_type1 fl" style="width: 100px">
                                <asp:DropDownList runat="server" ID="s2_user_birth_dd" class="custom_sel"></asp:DropDownList>

                            </span>
                        </div>
                        <div class="mb10"><span class="guide_comment2" id="msg_s2_birth" style="display:none">생년월일을 입력해 주세요</span></div> 
                        
                        <div class="regist">
                            <label for="temp_s2_email" class="hidden">이메일</label>
                            <input type="text" data-id="s2_email" runat="server" id="temp_s2_email" class="input_type1" placeholder="이메일" maxlength="100" />
                            <button class="btn_type7" id="s2_btn_auth">인증</button>
                            <button class="btn_type7" id="s2_btn_confirm">확인</button>
                        </div>
                        <div class="mb10"><span class="guide_comment2" id="s2_msg" style="display:none">이메일을 입력해 주세요</span></div>
                        <span class="s_con1">입력하신 이메일에서 인증코드를 클릭하셨으면 [확인]버튼을 클릭해주세요.</span><br />
                        
                    </div>

                    <!-- 본인 인증 -->


                    <div class="find existing" id="pn_s3" data-role="container" style="display: none">
                        <div class="cert clear2 mb50">
                            <!-- 휴대폰인증 -->
                            <div class="fl">
                                <div class="box phone">
                                    <p class="tit">휴대폰 인증</p>
                                    <p class="con">
                                        본인 명의의 휴대전화번호로 본인인증을<br />
                                        원하시는 분은 아래 버튼을 클릭해주세요
                                    </p>

                                    <a id="s3_btn_cert_by_phone" class="btn_s_type2">휴대폰 인증하기</a>
                                </div>
                                <p class="s_con1">본인명의의 휴대폰으로만 본인인증이 가능합니다.</p>
                            </div>
                            <!--// -->

                            <!-- 아이핀인증 -->
                            <div class="fr">
                                <div class="box ipin">
                                    <p class="tit">아이핀(i-PIN) 인증</p>
                                    <p class="con">
                                        가상 주민등록번호 아이핀으로 본인인증을<br />
                                        원하시는 분은 아래 버튼을 클릭해주세요
                                    </p>

                                    <a id="s3_btn_cert_by_ipin" class="btn_s_type2">아이핀 인증하기</a>
                                </div>
                                <p class="s_con1">아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다.</p>
                            </div>
                            <!--// -->
                        
                        </div>

                    </div>
                    <div class="btn_line">
                        <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="btn_type1 mb50">아이디 찾기</asp:LinkButton>
                    </div>


                    <div class="contact"><span>위 방법으로 아이디/비밀번호를 찾을 수 없는 경우 한국컴패션으로 연락 주시기 바랍니다. <em>후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</em></span></div>

                </div>

            </div>

        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>
    <!--// sub body -->


</asp:Content>
