﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class find_pwd_default : FrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	WWWService.Service _wwwService = new WWWService.Service();
	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

	protected override void OnBeforePostBack() {
		
	}

	void CheckByPhone() {
		var id = s1_id.Value;
		var name = s1_name.Value;
		var phone = s1_phone.Value;
		var code = s1_code.Value;

		if(Session["findSendPhoneCode"] == null || Session["findSendPhoneCode"].ToString() != code) {
			base.AlertWithJavascript("인증코드가 일치하지 않습니다.");
			return;
		}

		if(string.IsNullOrEmpty(name) || string.IsNullOrEmpty(phone)) {
			base.AlertWithJavascript("조회정보를 입력해주세요");
			return;
		}

        using (AuthDataContext dao = new AuthDataContext())
        {
            //var list = dao.sp_tSponsorMaster_get_f(id.EscapeSqlInjection(), "", "Y", "", name.EscapeSqlInjection(), "", phone.EscapeSqlInjection()).ToList();
            Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
            Object[] op2 = new Object[] { id.EscapeSqlInjection(), "", "Y", "", name.EscapeSqlInjection(), "", phone.EscapeSqlInjection() };
            var list = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

            if (list.Count > 0)
            {
                Session["pwdUserId"] = id;
                Response.Redirect("complete");
                return;
            }
        }

		Object[] objSql = new object[1] { string.Format(@"select top 1 sponsorId from tSponsorMaster with(nolock) where CurrentUse = 'Y' and sponsorId in (
select sponsorId from tSponsorCommunication where CommunicationType = '휴대번호' and CurrentUse = 'Y' and CommunicationContents='{0}') and sponsorName = '{1}' and userId='{2}' and sponsorid not in (select sponsorid from tsponsordouble)", 
			phone.EscapeSqlInjection(), 
			name.EscapeSqlInjection() , 
			id.EscapeSqlInjection()
			)};
		DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

		this.Check(id , ds);

	}

	void CheckByEmail() {
		var id = s2_id.Value;
		var name = s2_name.Value;
		var email = s2_email.Value;

		if(Session["sendEmailCode"] == null) {
			base.AlertWithJavascript("인증코드가 정확하지 않습니다.");
			return;
		}
		
		if(string.IsNullOrEmpty(name) || string.IsNullOrEmpty(email)) {
			base.AlertWithJavascript("조회정보를 입력해주세요");
			return;
		}

        using (AuthDataContext dao = new AuthDataContext())
        {
            //var entity = dao.user_email_auth.First(p => p.ue_email == email && p.ue_id == Guid.Parse(Session["sendEmailCode"].ToString()));
            var entity = www6.selectQFAuth<user_email_auth>("ue_email", email, "ue_id", Guid.Parse(Session["sendEmailCode"].ToString()));

            if (!entity.ue_active)
            {
                base.AlertWithJavascript("인증되지 않았습니다.");
                return;
            }

            //var list = dao.sp_tSponsorMaster_get_f(id.EscapeSqlInjection(), "", "Y", "", name.EscapeSqlInjection(), email.EscapeSqlInjection(), "").ToList();
            Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
            Object[] op2 = new Object[] { id.EscapeSqlInjection(), "", "Y", "", name.EscapeSqlInjection(), email.EscapeSqlInjection(), "" };
            var list = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

            if (list.Count > 0)
            {

                Session["pwdUserId"] = id;
                Response.Redirect("complete");
                return;
            }

        }

		Object[] objSql = new object[1] { string.Format(@"select top 1 sponsorId from tSponsorMaster with(nolock) where CurrentUse = 'Y' and sponsorId in (
select sponsorId from tSponsorCommunication where CommunicationType = '이메일' and CurrentUse = 'Y' and CommunicationContents='{0}') and sponsorName = '{1}' and userId='{2}' and sponsorid not in (select sponsorid from tsponsordouble)", 
				email.EscapeSqlInjection(), 
				name.EscapeSqlInjection() , 
				id.EscapeSqlInjection()
				) };
		DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

		this.Check(id , ds);
		
	}


	// 본인인증
	void CheckByCert() {
		var id = s3_id.Value;
		var ci = s3_ci.Value;

		if(string.IsNullOrEmpty(ci)) {
			base.AlertWithJavascript("본인인증을 해주세요");
			return;
		}

		WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
		Object[] objSql = new object[1] { string.Format(@"select top 1 sponsorId from tSponsorMaster with(nolock) where CurrentUse = 'Y' and ci ='{0}' and userId='{1}' and sponsorid not in (select sponsorid from tsponsordouble)", ci , id ) };
		DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

		this.Check(id , ds);


	}

	void Check( string userId , DataSet ds ) {

		if(ds.Tables[0].Rows.Count > 0) {

			var sponsorId = ds.Tables[0].Rows[0]["sponsorId"].ToString().Trim();

            using (AuthDataContext dao = new AuthDataContext())
            {
                //var list = dao.sp_tSponsorMaster_get_f("", "", "Y", sponsorId, "", "", "").ToList();
                Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                Object[] op2 = new Object[] { "", "", "Y", sponsorId, "", "", "" };
                var list = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

                if (list.Count > 0)
                {
                    Session["pwdUserId"] = userId;
                    Response.Redirect("complete");

                }
                else
                {
                    base.AlertWithJavascript("일치하는 회원정보가 없습니다.(웹회원정보 없음)");
                    return;
                }

            }


			
		} else {
			base.AlertWithJavascript("일치하는 회원정보가 없습니다.");
			return;
		}

	}

	protected void btn_submit_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

		if(cur_type_index.Value == "0") {      // 휴대폰

			this.CheckByPhone();

		} else if(cur_type_index.Value == "1") {      // 이메일

			this.CheckByEmail();
		} else if(cur_type_index.Value == "2") {      // 본인인증

			this.CheckByCert();
		}


	}
}