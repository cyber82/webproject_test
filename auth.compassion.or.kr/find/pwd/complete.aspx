﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="find_pwd_complete" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js"></script>
	<script type="text/javascript" src="/find/pwd/complete.js"></script>

	<script type="text/javascript">
		$(function () {

			
		});
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

        <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>비밀번호 찾기</h1>
				<span class="desc">아이디/비밀번호를 잊으셨나요?</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents member">

			<div class="w980">

				<div class="find">

					<p class="info_txt1">본인확인이 완료되었습니다.<br />새로운 비밀번호를 등록 후 사용해주세요.</p>

					<div class="find_box1">
						<div class="tac mb10">
							<label for="user_pwd" class="hidden">새 비밀번호</label>
                            <input type="password" id="user_pwd" runat="server" class="input_type1" style="width:480px" placeholder="새 비밀번호" maxlength="15"/>
						</div>
                        <div class="tac mt10 mb10">
							<span class="guide_comment2" id="msg_user_pwd" style="display:none">비밀 번호를 입력해 주세요.</span>
						</div>
						<div class="tac">
							<label for="re_user_pwd" class="hidden">새 비밀번호 확인</label>
                            <input type="password" id="re_user_pwd" runat="server" class="input_type1" style="width:480px" placeholder="새 비밀번호 확인" maxlength="15"/>
						</div>

						<!-- 경고문구 -->
						<div class="tac mt10 mb10">
							<span class="guide_comment2" id="msg_re_user_pwd" style="display:none">비밀 번호를 입력해 주세요.</span>
						</div>
					</div>

					<div class="btn_line">
                        <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="btn_type1 mb50">비밀번호 변경 완료</asp:LinkButton>
					</div>
					
				</div>

			</div>

		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

</asp:Content>
