﻿<%@ WebHandler Language="C#" Class="common_handler_upload_fileserver" %>

using System;
using System.Web;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using drawing = System.Drawing;
using NLog;

public class common_handler_upload_fileserver : IHttpHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();
	public void ProcessRequest(HttpContext context) {

		Hashtable result = new Hashtable();
		try {

			HttpPostedFile imageFile = context.Request.Files[0];
			if (imageFile != null && imageFile.ContentLength > 0) {

				bool rename = string.IsNullOrEmpty(context.Request["rename"]) ? false : context.Request["rename"].ToLower() == "y";
				string fileDir = context.Request["fileDir"];
				string delete_file = context.Request["dfile"];      // 삭제대상 파일
				string fname = imageFile.FileName.Split('\\')[imageFile.FileName.Split('\\').Length - 1];
				if (context.Request["fname"].EmptyIfNull() != "") {
					fname = context.Request["fname"];
				}

				if(!string.IsNullOrEmpty(delete_file)) {
					if(File.Exists(context.Server.MapPath(delete_file))) {
						File.Delete(context.Server.MapPath(delete_file));
					} else {
						//ErrorLog.Write(context , 0 , "file없음 > " + delete_file);
					}
				}

				if (!string.IsNullOrEmpty(fileDir)) {

					fileDir += DateTime.Now.ToString("yyMM") + "/";
					if (!Directory.Exists(context.Server.MapPath(fileDir))) {
						Directory.CreateDirectory(context.Server.MapPath(fileDir));
					}

					string fileName = rename ? fname.GetUniqueName(context , fileDir) : fname;
					string path = fileDir + fileName;
					string savePath = context.Server.MapPath(path);

					imageFile.SaveAs(savePath);

					result.Add("success", true);
					result.Add("name", path);
					result.Add("size", imageFile.ContentLength / 1024);
				} else {

					result.Add("success", false);
					result.Add("msg", "fileDir not found");
				}
			}

		} catch (Exception ex) {

			result.Add("success", false);
			result.Add("msg", ex.Message);
		}


		JsonWriter.Write(result , context );

	}


	public bool IsReusable {
		get {
			return false;
		}
	}
}