﻿<%@ WebHandler Language="C#" Class="common_handler_upload" %>

using System;
using System.Web;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;

public class common_handler_upload : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {
        HttpPostedFile file = context.Request.Files[0];

        if(file != null && file.ContentLength > 0) {

            bool rename = context.Request["rename"].ValueIfNull("y") == "y";
            string path = context.Request["fileDir"];
            string tempDir = ConfigurationManager.AppSettings["temp"];
            int limit = Convert.ToInt32(context.Request["limit"].ValueIfNull("2048"));


            Uploader.UploadResult result = new Uploader.UploadResult();

            if (file.ContentLength / 1024 > limit) {
                result.success = false;
                result.msg = string.Format("최대 {0}kb까지 업로드 가능합니다. 업로드 파일 {1}kb" , limit , file.ContentLength / 1024);
                JsonWriter.Write(result, context);
                context.Response.ContentType = "text/html";
                return;
            }

            try {

                result = new Uploader().Upload(context, rename, path, file);
                JsonWriter.Write(result, context);
            } catch(Exception ex) {
                ErrorLog.Write(context, 0, ex.ToString());
            }

            context.Response.ContentType = "text/html";

        }
    }


}