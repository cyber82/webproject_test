﻿(function () {
	var app = angular.module('site.fn.sns', []);

	app.factory('shareImpl', function ($analytics) {

		var self = this;

		return {
			facebook: function (element) {

				var self = this;

				var title = element.attr("data-title");
				var caption = element.attr("data-caption");
				var link = self.getDataLink(element);
				var picture = element.attr("data-picture");

				var obj = {
					method: 'feed',
					link: link,
					/*caption: "http://" + location.host,*/
					description: title + "<center></center>" + caption
				};

				if (picture) {
					obj.picture = picture;
				}

				FB.ui(obj, function (response) {
					if (response && !response.error_code) {

						// SNS 카운트갱신
						self.update("페이스북");
					}
				});
			},

			twitter: function (element) {

				var self = this;

				var title = element.attr("data-title");
				var caption = element.attr("data-caption");
				var link = self.getDataLink(element);
				var picture = element.attr("data-picture");

				var msg = "#해쉬태그 " + title.replace(/(?:\r\n|\r|\n)/g, '') + " \n" + link;

				window.open("https://twitter.com/intent/tweet?source=webclient&text=" + encodeURIComponent(msg), "tweet", "toolbar=0,status=0,width=626,height=436");
				self.update("트위터");

			},

			kakaoStory: function (element) {

				var self = this;

				var title = element.attr("data-title");
				var caption = element.attr("data-caption");
				var link = self.getDataLink(element);
				var picture = element.attr("data-picture");

				Kakao.Auth.login({
					success: function () {
						// 로그인 성공시, API를 호출합니다.
						Kakao.API.request({
							url: '/v1/api/story/linkinfo',
							data: {
								url: link
							}
						}).then(function (res) {
							return Kakao.API.request({
								url: '/v1/api/story/post/link',
								data: {
									link_info: res
								}
							});
						}).then(function (res) {
							return Kakao.API.request({
								url: '/v1/api/story/mystory',
								data: { id: res.id }
							});
						}).then(function (res) {

							alert("공유되었습니다.");
							// SNS 카운트갱신
							self.update("카카오스토리");

						}, function (err) {
							alert(JSON.stringify(err));
						});

					}
				});

			},

			kakaoTalk: function (element) {

				var self = this;

				var title = element.attr("data-title");
				var caption = element.attr("data-caption");
				var link = self.getDataLink(element);
				var picture = element.attr("data-picture");

				var msg = title.replace(/(?:\r\n|\r|\n)/g, '') + " \n" + link;

				Kakao.Link.sendTalkLink({
					label: msg
				});

				self.update("카카오톡");
			},

			update : function(provider) {
				//$analytics.eventTrack('SNS공유', { category: track_title, label: provider });

			},

			getDataLink: function (element) {
				var result = element.attr("data-link");
				
				if (!result || result.length < 1) {
					result = location.href.substr(0, location.href.lastIndexOf("#"));
				}
				return result;
			}

		};
	});


})();


