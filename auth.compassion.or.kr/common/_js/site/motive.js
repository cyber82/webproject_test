﻿// 후원계기
var $motive = {

	code : null , 
	data : null , 
	init: function (hd_motive_code , hd_motive_name , sel_motive1 , sel_motive2) {

		code = hd_motive_code.val();
		sel_motive1.append($("<option value=''>선택하세요</option>"));
		sel_motive2.append($("<option value=''>선택하세요</option>"));

		sel_motive1.change(function () {
			
			var val = $(this).val();
			if (val == "") {
				sel_motive2.empty();
			}else{

				if ($motive.data) {

					$.each($motive.data, function (i, v) {

						if (i == val) {
							
							sel_motive2.empty();
							sel_motive2.append($("<option value=''>선택하세요</option>"));

							$.each(v, function () {
								var opt = $("<option value='" + this.codeid + "'>" + this.codename + "</option>");
								sel_motive2.append(opt);

								if (this.codeid == code) {
									sel_motive2.val(this.codeid);
									sel_motive2.trigger("change")
									code = "";
								}
							})
						}

					});


				}

			}
		})

		sel_motive2.change(function () {

			hd_motive_code.val($(this).val());
			
			hd_motive_name.val(sel_motive2.find("option:selected").text().replace("\n" , ""));

		});

		$.get("/api/motive.ashx", { t: "list" }, function (r) {

			$motive.data = r;
		//	console.log(r);

			$.each($motive.data, function (i,v) {

				//console.log(v);
				var opt = $("<option value='" + i + "'>" + i + "</option>")
				sel_motive1.append(opt);
			})

			if (hd_motive_code.val() != "") {

				$.each($motive.data, function (i, v) {

					$.each(v, function () {
						if (this.codeid == hd_motive_code.val()) {
							sel_motive1.val(i);
						}
					})
					
				});

				sel_motive1.trigger("change");

			}

		});

	}

}