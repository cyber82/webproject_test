﻿$.ajaxSetup({ cache: false });

var kakaoId = "82c81a4e85c73c39204345775d3c36cb";

var debugging = true;
if (!debugging || typeof console == "undefined" || typeof console.log == "undefined") var console = { log: function () { } };

$(function () {

	// 앱화면에서 컨텐츠 길이 짧을경우 컨텐츠 height값 full size로 조정.
	if ($("section").hasClass("appPageNav")) {
		console.log(1);
		var pageH = $(".fullHeight").height();
		var deviceH = $(window).height() - 62;

		if (pageH < deviceH) {
			$(".fullHeight").css({ "height": deviceH });
		}
	}
	/**/


	$.each($("button"), function () {
		if (!$(this).attr("type")) {
			$(this).attr("type", "button");
		}
	})

	if (navigator.userAgent.match(/Firefox/)) {
		$("head").append($('<link rel="stylesheet" type="text/css" href="/common/css/firefox.css" />'));
	}
	else if (navigator.userAgent.match(/Chrome/)) {
		$("head").append($('<link rel="stylesheet" type="text/css" href="/common/css/chrome.css" />'));
	}
	else if (navigator.userAgent.match(/Safari/)) {
		$("head").append($('<link rel="stylesheet" type="text/css" href="/common/css/safari.css" />'));
	}

	// gnb 마우스오버
	$(".depth1").mouseenter(function () {
		//$(".depth2").animate({ "top": "130px" });
	});


    // 마우스오버 공통
	$(".img_over").mouseenter(function () {
	    var img_num = $(this).find("img");
	    img_num.attr("src", img_num.attr("src").replace(".png", "_on.png"))
	}).mouseleave(function () {
	    var img_num = $(this).find("img");
	    img_num.attr("src", img_num.attr("src").replace("_on.png", ".png"))
	});

	// sns 공유버튼 열림 : left direction
	var timeout = null;
	$(".sns_ani").mouseenter(function () {
		if (timeout != null)
			clearTimeout(timeout);
		$(this).find(".common_sns_group").stop().animate({ "width": "185px", "opacity": "1" }, 400);
		$(this).parent().find(".day").animate({ "opacity": "0" }, 400);
	}).mouseleave(function () {
		if (timeout != null)
			clearTimeout(timeout);
		timeout = setTimeout(function () {
			$(".common_sns_group").stop().animate({ "width": "0px", "opacity": "0" }, 400);
			$(".snsWrap .day").animate({ "opacity": "1" }, 400);
		}, 100);

	});


	$(".number_only").keydown(function (e) {

		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
			// Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
			// Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
			// Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}

		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	$(".custom_sel").selectbox({

		onOpen: function (inst) {
		}
	});

})

var common = {

	isLogin : function () {
		return $("#is_login").val() == "Y";	// top.master 
	},

	getUserId: function () {
		return $("#_userid").val();
	},

	checkLogin: function () {
		if (!common.isLogin()) {
			if (confirm("로그인이 필요합니다. \n로그인 페이지로 이동하시겠습니까?")) {
				location.href = "/login";
			}
			return false;
		}

		return true;
	},

}

var goBack = function () {
	history.back();
}

var setDatePicker = function (obj) {

	obj.datepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: '',
		numberOfMonths: 1,
		monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		showTime: false,
		showHour: false,
		showMinute: false,
		closeText: '닫기',
		currentText: '오늘',

		buttonImageOnly: false,
		changeYear: true,
		onSelect: function (text, e) {
			$(this).datepicker("hide");
			
		}
	});

}

var setPlaceholder = function (obj, str) {

	obj.focus(function () {
		if ($(this).val() == str) {
			$(this).val("")
		}
	}).focusout(function () {
		if ($(this).val() == "") {
			$(this).val(str)
		}
	})

};

var scrollTo = function (obj , offset) {
	
	offset = offset || 0;
	if (obj && obj.offset) {
		var top = obj.offset().top - $(".header_bottom").height() - offset;
		//$("html,body").scrollTop(top);
		$("html,body").animate({"scrollTop" : top} , 300)

		//console.log(top);
	}
}