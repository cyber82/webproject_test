﻿var m1, m2, m3, m4, m5, m6;	// 후원,참여,공감,소개,애드보킷

$(function () {

	var getMenu = function (key) {
		return $.grep(_menu.sub, function (r) {
			return r.key == key;
		})[0];
	}

	m1 = getMenu("sponsor");			// 후원
	m2 = getMenu("participation");		// 참여
	m3 = getMenu("sympathy");			// 공감
	m4 = getMenu("about-us");			// 소개
	m5 = getMenu("activity");			// 애드보킷
	m6 = getMenu("my");					// 마이컴패션

	gnb.setCurrent();

	gnb.init();

	footer.init();

	totalMenu.init();

});

var footer = {

	list: null,

	init: function () {
		this.list = [m1, m2, m3, m4, m5];

		var container = $("ul.ft_map");
		$.each(this.list, function (i) {
			
			var d2 = $("<li/>");
			container.append(d2);
			d2.append('<p class="fmap_tit">' + this.name + '</p>');

			var d3 = $('<ul class="fmap_lst"></ul>');
			d2.append(d3);
			$.each(this.sub, function () {

				d3.append($('<li><a href="'+ this.url +'">'+this.name+'</a></li>'));

			});

		});

	}

}

var totalMenu = {

	list: null,
	isShow : false , 

	init: function () {

		this.list = [m1, m2, m3, m4, m5,m6];

		$.each(this.list, function (i) {
			var container = $("div.menuList ul.m" + (i + 1));
			totalMenu.bindEvent(this, container, 2);

		});

		// scroll to

		$(".gnb_totalmenu .menuNav li").each(function (i) {
			$(this).click(function () {

				$(".gnb_totalmenu .menuAllBtn").removeClass("on");
				$($(".gnb_totalmenu .menuAllBtn")[i]).addClass("on");
				var target = $($(".gnb_totalmenu .menuList ul.Ad1")[i]);

				//console.log(target.offset().top, (target.offset().top - $(".gnb_totalmenu").offset().top));
				$(".gnb_totalmenu .menuList").animate({ "scrollTop": (target.offset().top - $(".gnb_totalmenu").offset().top + $(".gnb_totalmenu .menuList").scrollTop() - 10) + "px" }, 500);
				
				return false;
			})
		})

	},

	bindEvent: function (obj, container, depth) {

		var root = $('<ul class="Ad' + depth + '"/>');
		container.append(root);

		if (obj.selected && obj.sub) {
			root.addClass("gnb_selected");
		}

		$.each(obj.sub, function (i) {
			var item = $('<li><a href="' + this.url + '" class="tit' + depth + ' a' + depth + '">' + ((depth == 4) ? "- " : "") + this.name + '</a></li>');
			
			if (depth == 2 && i > 0 && i % 4 == 0) {
				
				root = $('<ul class="Ad' + depth + '"/>');
				container.append(root);
			}

			root.append(item);

			if (this.selected) {
				item.find("a").addClass("gnb_selected");
			}

			if (this.sub) {
				totalMenu.bindEvent(this, item, depth + 1);
			}
		})

	},


	toggle : function(){
		
		totalMenu.isShow = $("#mask").length > 0;
		if (totalMenu.isShow) {
			totalMenu.hide();
		} else {
			totalMenu.show();
		}
	} , 

	show: function () {
		
		$(".gnbsub_wrap").trigger("mouseleave");

		modalShow($(".gnb_totalmenu"), null, true, false);

	},

	hide: function () {
		
		closeModal(function () {
			$("body").css({ "overflow": "auto" })
		});
	}

}

var gnb = {

	enable : true , 

	timeout : null , 

	list : null , 

	bindEvent: function (obj, container, depth) {
	
		$(window).scroll(function () {
			var top = $(this).scrollTop();
			if (top >= 35) {
				$(".header_bottom").css({ "position": "fixed", "top": "0px" });
				$(".gnbsub_wrap , .gnb_totalmenu").css({ "top": "95px" })
			} else {
				$(".header_bottom").css({ "position": "relative" });
				$(".gnbsub_wrap , .gnb_totalmenu").css({ "top": "130px" })
			}
		})

		var root = $('<ul class="d'+depth+'"/>');
		container.append(root);

		if (obj.selected && obj.sub) {
			root.addClass("gnb_selected");
		}

		$.each(obj.sub, function () {
			var item = $('<li><a href="' + this.url + '" class="a'+depth+'">' + this.name + '</a></li>');
			root.append(item);

			if (this.selected) {
				item.find("a").addClass("gnb_selected");
			}

			item.mouseenter(function () {
				$("a.a" + (depth)).removeClass("on");
				$(this).find("a.a" + (depth)).addClass("on")
				clearTimeout(gnb.timeout);
				
				var target = $(this).find("ul.d" + (depth + 1));
				if (target.length > 0)
					target.show();
				else {
					gnb.timeout = setTimeout((function () {
						root.find("ul").hide();
					}), 200);
				}

			}).mouseleave(function () {
				clearTimeout(gnb.timeout);
				$(this).find("a").removeClass("on")
				gnb.timeout = setTimeout((function () {
					root.find("ul").hide();
				}), 200);

			})

			if (this.sub) {
				gnb.bindEvent(this , item , depth+1);
			}
		})

	} , 

	init: function(){


		// 현재 메뉴를 포함하여 상위뎁스까지 selected 표시를 한다.
		this.list = [m1,m2,m3,m4,m5];

		$.each(this.list, function (i) {
			$("a.gnb_top.m" + (i + 1)).attr("href", this.url);
			var container = $("div.gnb.m" + (i + 1));
			gnb.bindEvent(this , container , 2);

		});

		gnb.setDefault();

		$("a.gnb_top").each(function (i) {
			$(this).mouseenter(function () {

				if (!gnb.enable) return;
				if (totalMenu.isShow) return;

				$("a.gnb_top").removeClass("on");
				$(this).addClass("on");
				clearTimeout(gnb.timeout);
				$("div.gnb").hide();
				$("div.gnb.m" + (i + 1)).show();
				$("div.gnb.m" + (i + 1) + " ul.d3").hide();
				$("div.gnb.m" + (i + 1) + " ul.d4").hide();

				if ($(this).hasClass("gnb_selected")) {
					$(".gnb_selected").show();
				}

				$(".gnbsub_wrap").show();

			}).mouseleave(function () {

				/*
				clearTimeout(gnb.timeout);
				gnb.timeout = setTimeout((function () {
					$(".gnbsub_wrap").hide();
					$("div.gnb").hide();
				}), 500);
				*/
			})
		})

		$(".gnbsub_wrap , .header_bottom").mouseenter(function () {

			clearTimeout(gnb.timeout);
			
		}).mouseleave(function () {

			clearTimeout(gnb.timeout);
			gnb.timeout = setTimeout((function () {
				$("a.gnb_top").removeClass("on");
				$(".gnbsub_wrap").hide();
				$("div.gnb").hide();
				gnb.setDefault();
				

			}), 500);
		})
		
		$(".gnb_wrap .menu").click(function () {
			totalMenu.toggle();
			if ($(this).hasClass("on")) {
				$(this).removeClass("on");
			}
			else { $(this).addClass("on"); }
			return false;
		})

		//totalMenu.toggle();

	},

	setDefault : function(){
		$.each(gnb.list, function (i) {
			if (this.selected) {
				$("a.gnb_top.m" + (i + 1)).addClass("on").addClass("gnb_selected");
			}
		});

		
		var targets = $(".gnbsub_wrap .gnb_selected");
		targets.addClass("on");
		targets.show();
	} , 

	setCurrent: function () {

		if ($("#sitemap_resourcekey").length < 1) return;

		var cur_key = $("#sitemap_resourcekey").val();
		
		var setSelected = function (obj) {

			if (cur_key.indexOf(obj.key) > -1) {
				obj.selected = true;
				if (obj.sub) {

					$.each(obj.sub, function () {
						setSelected(this);

					});
				}
			}
		}

		$.each(_menu.sub, function () {
			setSelected(this);
		});
	}

}
