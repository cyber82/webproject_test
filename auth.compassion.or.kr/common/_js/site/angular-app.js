﻿
(function () {
	var app = angular.module('cps', ['bw.paging', 'cps.page', 'ngSanitize', 'angularMoment'], function ($httpProvider) {
		// http://stackoverflow.com/questions/19254029/angularjs-http-post-does-not-send-data
		// Use x-www-form-urlencoded Content-Type
		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

		/**
		 * The workhorse; converts an object to x-www-form-urlencoded serialization.
		 * @param {Object} obj
		 * @return {String}
		 */
		var param = function (obj) {
			var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

			for (name in obj) {
				value = obj[name];

				if (value instanceof Array) {
					for (i = 0; i < value.length; ++i) {
						subValue = value[i];
						fullSubName = name + '[' + i + ']';
						innerObj = {};
						innerObj[fullSubName] = subValue;
						query += param(innerObj) + '&';
					}
				}
				else if (value instanceof Object) {
					for (subName in value) {
						subValue = value[subName];
						fullSubName = name + '[' + subName + ']';
						innerObj = {};
						innerObj[fullSubName] = subValue;
						query += param(innerObj) + '&';
					}
				}
				else if (value !== undefined && value !== null)
					query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
			}

			return query.length ? query.substr(0, query.length - 1) : query;
		};

		// Override $http service's default transformRequest
		$httpProvider.defaults.transformRequest = [function (data) {
			return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
		}];

	});


	app.factory("utils", function () {
		return {
			getPageRoot: function () {
				return $("[ng-app]");
			},

			getImage: function (src, callback) {
				var img = new Image();
				img.onload = function () {
					callback(this);
					img = null;
				};
				img.src = src;

			},

			windowHeight: function () {
				return window.innerHeight ? window.innerHeight : $(window).height();
			}
		}
	});

	app.factory("popup", function ($compile, $http, utils) {
		return {

			init: function ($scope, path, callback, options) {
		
				var self = this;
				options = options || {};
				var opacity = options.opacity || 0.8;
				var backgroundClick = options.backgroundClick || "y";
				var init_animate = options.animate || true;
				//if (init_animate == undefined) init_animate = true;
				var background;
				$http.get(path).success(function (data, status, headers, config) {

					var content = $("<div/>").html(data);
					content.width($(data).width());
					var pop = $("<div class='loading-container'/>");
					pop.css({
						position: "fixed",
						zIndex: 100000,
						width: "100%",
						height: "100%",
						left: 0,
						top: 0,
						opacity: 0.001

					});

					background = $("<div/>");
					background.css({
						position: "absolute",
						"backgroundColor": "#000",
						opacity: opacity,
						width: $(document).width() + "px",
						height: utils.windowHeight() + "px",
						top: 0,
						left: 0
					})
					pop.append(background);
					pop.append(content);

					$(window).resize(function () {
						background.css({ 'width': $(document).width(), 'height': $(document).height() });
						content.css({ 'left': ($(document).width() - content.width()) / 2 });
						
						if (options.iscroll) {
							var iscroll_container = pop.find(".fn_pop_container");
							iscroll_container.width($(window).width());

							var iscroll_content = pop.find(".fn_pop_content");
							iscroll_content.css({ 'left': ($(document).width() - iscroll_content.width()) / 2 });

						} else {
							content.css({ 'left': ($(document).width() - content.width()) / 2 });
						}
					});

					utils.getPageRoot().append(pop);

					self.loadAll(pop, function () {

						// 리소스 전부 로드되면,
						var left = ($(window).width() - content.width()) / 2;
						var top = (utils.windowHeight() - content.height()) / 2;
						var contentHeight = content.height();
						
						if (options.iscroll) {

						} else {
							content.css({ left: left + "px", top: utils.windowHeight() + "px", position: "absolute" });
						}

						pop.hide();

						var modal = {
							obj: pop,

							show: function (animate) {
								$("body").css({ "overflow": "hidden" })
								
								background.css({ 'width': $(document).width(), 'height': $(document).height() });
								if (animate == undefined) animate = false;

								pop.bind("touchmove", function (e) {
									e.preventDefault();
								});

								pop.css({ opacity: 1 });
								
								content.css({ top: utils.windowHeight() + "px" });
								background.css({ height: utils.windowHeight() + "px" });

								// 주소창의 유무에 따라 높이가 변하기때문에, 노출될때마다 계산
								top = (utils.windowHeight() - contentHeight) / 2;

								if (options.top != undefined)
									top = options.top;
								
								// iscroll.js 사용인경우
								if (options.iscroll) {

									this._applyIScroll(pop);
									
								}

								var is_animation = false;
								if (animate)
									is_animation = true;
								else
									is_animation = init_animate;

								if (is_animation) {
									background.css({ opacity: 0.0 });
									content.css({ opacity: 0.0, top: top });
									pop.show();
									
									background.stop().animate({
										opacity: opacity
									}, 'fast', function () {
										
									//	content.css({ top: utils.windowHeight() + "px" });
										content.stop().animate({
											//top: top,
											opacity: 1.0
										}, 'fast', function () {
											pop.show();
										});
									});

									

								} else {
									background.css({ opacity: opacity });
									content.css({ top: top + "px" });
									pop.show();
								}
							},

							_applyIScroll: function (pop) {
								var container = pop.find(".fn_pop_container");
								var content = pop.find(".fn_pop_content");
								
								container.css({
									"position": "absolute", "z-index": 300000, "overflow-y": "auto",
									width: $(window).width(), height: $(window).height()
									
								});

								content.css({
									"position": "absolute",
									"left": ($(window).width() - content.width()) / 2
								})
								
								content.bind("click", function (e) {
									e.preventDefault();
									return false;
								})

								container.bind("click", function () {
									if (backgroundClick == "y")
										modal.hide();
								})

								setTimeout(function () {

									new IScroll("#" + container.attr("id"), {
									});
								}, 200);
							} , 

							hide: function (force_animate) {
								
								$("body").css({ "overflow": "auto" })
								hide_animate = init_animate;
								if (force_animate != undefined)
									hide_animate = force_animate;

								pop.unbind("touchmove");

								if (hide_animate) {
									
									content.stop().animate({
										//top: utils.windowHeight() + "px"
										opacity: 0.0
									}, 'fast', function () {

									});

									background.stop().animate({
										opacity: 0.0
									}, 'fast', function () {
										pop.hide();
									});

								} else {
									pop.hide();
								}

								modal.whenHide();
							},

							whenHide: function () { }
						}

						background.bind("click", function () {
							if (backgroundClick == "y")
								modal.hide();
						})

						callback(modal);

					});
					$compile(pop)($scope);

				})
			},

			loadAll: function (el, callback) {
				var images = el.find("img");
				var image_count = images.length;

				if (image_count < 1)
					callback();

				$.each(images, function () {
					
					if (!$(this).attr("src") || $(this).attr("src") == "") {
						image_count--;
						if (image_count < 1) {
							callback();
						}

					} else {
						utils.getImage($(this).attr("src"), function () {

							image_count--;
							if (image_count < 1) {
								callback();
							}
						})
					}
				})


			}

		}
	})

	app.factory("loading", function ($rootScope, $compile, utils) {
		return {

			obj: null,

			show: function ($scope, msg, options) {

				var self = this;
				options = options || {};
				var opacity = options.opacity || 0.3;

				var content = $("<div class='loading'/>").html("<span>" + msg + "</span>");

				var pop = $("<div class='loading-container loading-layer'/>");
				pop.css({
					position: "fixed",
					zIndex: 100000,
					width: "100%",
					height: "100%",
					left: 0,
					top: 0

				});

				var background = $("<div/>");
				background.css({
					position: "absolute",
					"backgroundColor": "#000",
					opacity: opacity,
					width: $(document).width() + "px",
					height: utils.windowHeight() + "px",
					top: 0,
					left: 0
				})

				pop.append(background);
				pop.append(content);

				utils.getPageRoot().append(pop);
				//$("body").append(pop);

				// 리소스 전부 로드되면,
				var left = ($(document).width() - content.width()) / 2;
				var top = (utils.windowHeight() - content.height()) / 2;

				$compile(pop)($scope);

				obj = pop;

				pop.bind("touchmove", function (e) {
					e.preventDefault();
				});
			},

			hide: function () {

				$(".loading-layer").remove().unbind("touchmove");

			}

		}
	})

	app.directive('checkImage', function ($http) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				attrs.$observe('ngSrc', function (ngSrc) {
					$http.get(ngSrc).success(function () {
					}).error(function () {
						element.attr('src', '/common/img/common/logo_1.png'); // set default image
					});
				});
			}
		};
	});

	app.directive('ngEnter', function () {
	    return function (scope, element, attrs) {
	        element.bind("keydown keypress", function (event) {
	            if (event.which === 13) {
	                scope.$apply(function () {
	                    scope.$eval(attrs.ngEnter);
	                });

	                event.preventDefault();
	            }
	        });
	    };
	});

	app.factory('paramService', function () {
		return {
			getParameter: function (sParam) {
				var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

				for (i = 0; i < sURLVariables.length; i++) {
					sParameterName = sURLVariables[i].split('=');

					if (sParameterName[0] === sParam) {
						return sParameterName[1] === undefined ? true : sParameterName[1];
					}
				}
			},

			getParameterValues: function () {
				
				var sPageURL = decodeURIComponent(window.location.search.substring(1));
				//var sPageURL = window.location.search.substring(1);
				var sURLVariables = sPageURL.split('&')
				var sParameterName = "";
				var result = {};

				for (var i = 0; i < sURLVariables.length; i++) {
					sParameterName = sURLVariables[i].split('=');

					if(sParameterName[0] !="" && sParameterName[1] !== undefined ){
						result[sParameterName[0]] = sParameterName[1] === undefined ? true : sParameterName[1];
					}
				}

				return result;
			}
		};
	});

	app.filter('percentage', ['$filter', function ($filter) {
		return function (input, decimals) {
			return $filter('number')(input * 100, decimals) + '%';
		};
	}]);
	
})();