
$(".carousel").carousel();
$('.carousel').find("[data-slide='prev']").hide();
$('.carousel')
	.bind('slide.bs.carousel', function (e) {

		var isLast = $(e.relatedTarget).hasClass("last");
		var isFirst = $(e.relatedTarget).hasClass("first");

		var container = $(this);
		container.find("[data-slide='prev']").show();
		container.find("[data-slide='next']").show();

		if (isFirst) {
			container.find("[data-slide='prev']").hide();
		}

		if (isLast) {
			container.find("[data-slide='next']").hide();
		}

	});