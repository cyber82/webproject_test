﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <remarks>
/// page에서 사용할때 주의할점!
/// 페이지 렌더링 순서때문에, LoadComplete 이벤트에서 처리해야 함. 
/// if (!Page.IsPostBack) this.LoadComplete += new EventHandler(list_LoadComplete);
/// </remarks>
/// 
namespace Common {
	public partial class breadcrumb : System.Web.UI.UserControl {

		protected override void OnLoad(EventArgs e) {
	
			base.OnLoad(e);
			
			var currentNode = SiteMap.Providers["web"].CurrentNode;
			if(currentNode != null) {
				sitemap_resourcekey.Value = currentNode.ResourceKey + "|";

				if(currentNode.ParentNode != null) {
					currentNode = currentNode.ParentNode;
					sitemap_resourcekey.Value = currentNode.ResourceKey + "|" + sitemap_resourcekey.Value;
					if(currentNode.ParentNode != null) {
						currentNode = currentNode.ParentNode;
						sitemap_resourcekey.Value = currentNode.ResourceKey + "|" + sitemap_resourcekey.Value;
						if(currentNode.ParentNode != null) {
							currentNode = currentNode.ParentNode;
							sitemap_resourcekey.Value = currentNode.ResourceKey + "|" + sitemap_resourcekey.Value;
						}
					}
				}
			}
		}

		

	}
}