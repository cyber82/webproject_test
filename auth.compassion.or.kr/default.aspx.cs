﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class _default : FrontBasePage {
	
	protected override void OnBeforePostBack() {

		if(AppSession.HasCookie(this.Context) || Request.Url.Host.StartsWith("m.")) {
			Response.Redirect(ConfigurationManager.AppSettings["default_return_url_m"]);
			return;
		}
		
		Response.Redirect(ConfigurationManager.AppSettings["default_return_url"]);

	}

}