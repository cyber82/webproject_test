﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransIntro.aspx.cs" Inherits="Mate_Intro_TransIntro" %>--%>
<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="~/Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="~/Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="~/Controls/IntroRight.ascx" tagname="IntroRight" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="apply translation-mate">
 
		<!-- sidebar -->
		<uc4:IntroRight ID="IntroRight1" runat="server" />
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>번역 MATE</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/Intro/MateIntro.aspx">MATE 소개</a><strong>MATE 신청</strong>
			</div>
			<ul class="tab2 tab-mate">
				<li class="on"><a href="TransIntro.aspx" class="t1">번역 MATE</a></li>
				<li><a href="officemate.aspx" class="t2">사무 MATE</a></li>
				<li><a href="eventmate.aspx" class="t3">행사 MATE</a></li>
				<li><a href="specialitymate.aspx" class="t4">전문 MATE</a></li>
			</ul>
			<div class="sec">
				<div class="text txt-intro">
					<h4>번역 메이트 소개</h4>
					<p><strong>매 주 약 8,000통 이상의 편지가 한국컴패션 사무실로 도착하고 있으며, 이 중 번역이 필요한 편지는 약 4,000통 정도입니다.</strong>어린이 편지 약 6,000통, 후원자 편지 약 2,000통 (2010년 기준)</p>
					<p>한국컴패션은 보다 신속하고 정확한 편지 발송을 위하여 편지 번역 mate를 선발하고 있습니다.<strong> 선발 과정을 통해 편지 번역 mate가 되면 어린이와 후원자가 주고 받는 편지를 각각 한국어와 영어로 번역합니다.</strong> (어린이편지 : 영-한 번역 / 후원자 편지 : 한-영 번역)온라인 번역 프로그램(imate)를 사용하여 편지를 번역하므로 컴퓨터와 인터넷만 사용할 수 있다면 언제 어디서든지 봉사활동이 가능합니다.</p>
				</div>
			</div>
			<div class="sec">
				<div class="text apply-qualification">
					<h4>번역 메이트 신청자격</h4>
					<ol>
						<li>컴패션 사역을 이해하고, 자원 봉사 활동에 관심 있는 만 16세 (현재 고등학교 1학년 재학생) 이상의 남, 녀</li>
						<li>한-영 / 영-한 번역 능력 보유하신 분</li>
						<li>컴패션에서 실시하는 오리엔테이션에 참석 가능하신 분 (장소 및 일시 사전 공고)</li>
						<li>6개월 이상 성실하게 봉사 활동 가능하신 분</li>
					</ol>
				</div>
			</div>
			<div class="text apply-process">
				<h4>번역 메이트 신청절차</h4>
				<ol>
					<li>컴패션 홈페이지 회원가입</li>
					<li>메이트 신청 페이지에서 ‘신청하기’클릭</li>
					<li>TEST 통과 및 편지 변역을 위한 필수자료 ‘번역유의사항’정독</li>
					<li>‘번역유의사항’에 관한 Mini Quiz</li>
					<li>편지 번역 TEST 편지 번역문 및 지원동기 작성</li>
					<li>편지 번역 MATE 활동 동의서에 서명</li>
					<li>오리엔테이션 참석 차후 별도 공지 확인</li>
					<li>활동 시작</li>
				</ol>
			</div>
			<div class="contact-wrap">
				<p class="text-contact">MATE 신청 관련 자세한 문의는 MATE 홈페이지 FAQ, 온라인상담 또는 전화 02-3668-3400를 이용해 주시기 바랍니다.</p>
			</div>
			<div class="btn-m">
                <%--<asp:Button ID="btnNext" runat="server" Text="" CssClass="btn btn-apply" 
                    onclick="btnNext_Click" />--%>
			</div>
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>


		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    </form>
</body>
</html>
