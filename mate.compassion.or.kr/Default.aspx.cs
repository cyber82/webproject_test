﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data;

public partial class Mate_Default : System.Web.UI.Page
{
    string sjs = string.Empty;
    DataSet ds = new DataSet();
    string sSponsorID = string.Empty;
    string sSponsorName = string.Empty;

    string redirectPage = string.Empty;
    bool bLoginOK = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (UserInfo.IsLogin)
            {
                Login.Visible = true;
                Logout.Visible = false;
                labUserName.Text = new UserInfo().UserName;
            }

            else
            {
                Login.Visible = false;
                Logout.Visible = true;
            }

            WWWService.Service _wwwService = new WWWService.Service();
            DataSet notiData = _wwwService.Mate_BoardList(1001, "all", "", 0, 4);
            //DataSet ourData = _wwwService.Mate_BoardList(1222, "all", "", 0, 4);
            //함께하는 이야기
            DataSet ourData = _wwwService.Mate_BoardList(1223, "all", "", 0, 4);

            repOurData.DataSource = ourData.Tables[0];
            repNotiData.DataSource = notiData.Tables[0];
            repNotiData.DataBind();
            repOurData.DataBind();

            GetCurrentMateCount();
            ToWordRecent();

            //메이트 메인 업데이트 2012-11-22  추가
            MateSSB();

            //롤링배너 불러오기 2012-12-03  추가
            RollingBanner("mate");

            //팝업창 불러오기 2013-06-03  추가
            PopupDataLoad("mate");


        }
    }

    private void ToWordRecent()
    {
        WWWService.Service _wwwService = new WWWService.Service();
        DataSet data = _wwwService.Mate_BoardList(1011, "all", "", 0, 2);

        if (data.Tables[0].Rows.Count > 0)
        {
            toAWordList.NavigateUrl = String.Format("/nanum/todaywordview.aspx?iTableIndex={0}&iNoIndex={1}&iBoardIdx={2}&b_num={3}&re_level={4}&ref={5}&re_step={6}&pageIdx=0&searchOption=all&searchTxt="
                , data.Tables[0].Rows[0]["table_idx"]
                , data.Tables[0].Rows[0]["no_idx"]
                , data.Tables[0].Rows[0]["board_idx"]
                , data.Tables[0].Rows[0]["b_num"]
                , data.Tables[0].Rows[0]["re_level"]
                , data.Tables[0].Rows[0]["ref"]
                , data.Tables[0].Rows[0]["re_step"]);
            toAWordList.Text = data.Tables[0].Rows[0]["b_content"].ToString();
        }
    }

    protected string GetDate(object obj)
    {
        DateTime val = Convert.ToDateTime(obj);
        return val.Year.ToString().Substring(2) + "." + val.Month.ToString("00") + "." + val.Day.ToString("00");
    }

    private void GetCurrentMateCount()
    {
        int count;
        DateTime today = DateTime.Today;

        if (today.DayOfWeek.ToString().ToLower() == "monday")
            count = 1;

        else if (today.DayOfWeek.ToString().ToLower() == "tuesday")
            count = 2;

        else if (today.DayOfWeek.ToString().ToLower() == "wednesday")
            count = 3;

        else if (today.DayOfWeek.ToString().ToLower() == "thursday")
            count = 4;

        else if (today.DayOfWeek.ToString().ToLower() == "friday")
            count = 5;

        else if (today.DayOfWeek.ToString().ToLower() == "saturday")
            count = 6;

        else
            count = 7;

        //일
        DateTime sunday = today.AddDays(count * -1);

        //월
        DateTime monday = today.AddDays(count * -1 + -6);

        WWWService.Service _wwwService = new WWWService.Service();

        DataSet data = _wwwService.MateCurrentCount(monday.ToString("yyyyMMdd"), sunday.ToString("yyyyMMdd"));
        //Label1.Text = Convert.ToDecimal (data.Tables[1].Rows[0][0]).ToString ("N0");
        //Label2.Text = Convert.ToDecimal(data.Tables[0].Rows[0][0]).ToString("N0");
        //Label3.Text = Convert.ToDecimal(data.Tables[2].Rows[0][0]).ToString("N0");
        //Label4.Text = Convert.ToDecimal(data.Tables[2].Rows[0][1]).ToString("N0");

        if (data != null && data.Tables[0].Rows.Count > 0)
        {
            Label1.Text = Convert.ToDecimal(data.Tables[0].Rows[0][0]).ToString("N0");
            Label2.Text = Convert.ToDecimal(data.Tables[0].Rows[0][1]).ToString("N0");
            Label3.Text = Convert.ToDecimal(data.Tables[0].Rows[0][2]).ToString("N0");
            Label4.Text = Convert.ToDecimal(data.Tables[0].Rows[0][3]).ToString("N0");
        }

        else
        {
            Label1.Text = "";
            Label2.Text = "";
            Label3.Text = "";
            Label4.Text = "";
        }
    }

    #region 로그인 버튼
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        //string sjs;

        //if (txtUserID.Value.Trim().Length < 1)
        //{
        //    sjs = "";
        //    sjs = JavaScript.HeaderScript.ToString();
        //    sjs += JavaScript.GetAlertScript("아이디가 잘못 입력되었습니다.  ");
        //    sjs += JavaScript.FooterScript.ToString();

        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //    return;
        //}

        ////***************2009-06-26 관리자로그인을 위한 수정*******************************************************
        //if (hidTempSpnsorID.Value.ToString().Trim().Length == 0 || hidTempSpnsorID.Value.ToString().Trim().Length != 17 && hidTempSpnsorID.Value.ToString().Trim() != "99999999999999999")
        //{
        //    if (txtPwd.Value.Trim().Length < 1)
        //    {
        //        sjs = "";
        //        sjs = JavaScript.HeaderScript.ToString();
        //        sjs += JavaScript.GetAlertScript("비밀번호가 잘못 입력되었습니다.");
        //        sjs += JavaScript.FooterScript.ToString();

        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //        return;
        //    }
        //}

        ////******************************************************************************************************
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////***************************************************************************
        ////아이디,비번이 존재하는지 체크한후 기존 회원여부를 체크해서 실명인증과정을 거친다.(CertifyDate체크)  
        ////****************************************************************************
        //LoginCheck();
    }
    #endregion

    #region 로그인 체크 - LoginCheck()
    private void LoginCheck()
    {
        //UserInfo userInfo = new UserInfo();

        //DataSet ds = null;

        //string sUserID = txtUserID.Value.Trim();//아이디   
        //string sUserPw = txtPwd.Value.Trim();   //비밀번호  

        //try
        //{
        //    WWWService.Service _wwwService = new WWWService.Service();
        //    //*******2009-06-26 추가(관리자로그인을 위한 부분)**********************************************************
        //    //*******************************************************************************************************
        //    if (hidTempSpnsorID.Value.ToString().Trim() == "99999999999999999")
        //    {
        //        ds = _wwwService.listupWebManagerLogin(sUserID.ToString().Trim());
        //        hidTempSpnsorID.Value = "";
        //    }

        //    else
        //    {
        //        //원래있었던부분/////////////////////////////////////////////////////////////////////////////////////
        //        ds = _wwwService.getUserInfo(sUserID.ToString().Trim(), sUserPw.ToString().Trim());
        //        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    }

        //    if (ds != null && ds.Tables["InfoT"].Rows.Count == 1 && ds.Tables["InfoT"].Rows[0]["SponsorID"].ToString().Trim() == "99999999999999999")
        //    {

        //        userInfo.sTempSponsorId = "99999999999999999";
        //        userInfo.UpdateCookies();

        //        sjs = "";
        //        sjs = JavaScript.HeaderScript.ToString();
        //        sjs += JavaScript.GetPageMoveScript("/Member/Login.aspx?ManagerLog=Y");
        //        sjs += JavaScript.FooterScript.ToString();
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //        return;
        //    }

        //    ////////////////////////여기까지 관리자로그인을 위해서 수정됨/////////////////////////////////////////////////////////////////////////////////////
        //    //********************************************************************************************************
        //    //********************************************************************************************************
        //    ///----------회원정보가 없는경우(비번불일치,회원가입정보없음의 경우로 나뉜다.)=====================
        //    # region 회원정보가 없는경우(비번불일치,회원가입정보없음의 경우로 나뉜다.)
        //    if (ds.Tables["InfoT"].Rows.Count == 0)
        //    {
        //        string sUserYN = string.Empty;
        //        //////////아이디,비번이 잘못입력되었는지 회원정보가 없는지 구분해 준다.(WWWService호출)

        //        try
        //        {
        //            sUserYN = _wwwService.getUserId(sUserID.ToString().Trim());//아이디일치여부 
        //            if (sUserYN == "10")
        //            {
        //                sjs = "";
        //                sjs += JavaScript.HeaderScript.ToString();
        //                sjs += JavaScript.GetAlertScript("비밀번호가 일치하지 않습니다.\\r\\n비밀번호를 분실하셨으면 아이디비밀번호찾기를 이용하세요.");
        //                sjs += JavaScript.FooterScript.ToString();

        //                Page.ClientScript.RegisterStartupScript(this.GetType(), "error", sjs.ToString());
        //                return;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            //Exception Error Insert
        //            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

        //            sjs = "";
        //            sjs += JavaScript.HeaderScript.ToString();
        //            sjs += JavaScript.GetAlertScript("로그인을 위한 서비스로딩에 실패했습니다.\\r\\n 다시입력해 주세요.");
        //            sjs += JavaScript.FooterScript.ToString();

        //            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "error", sjs.ToString());
        //            return;
        //        }
        //        txtUserID.Value = "";
        //        sjs = "";
        //        sjs = JavaScript.HeaderScript.ToString();
        //        sjs += JavaScript.GetAlertScript("아이디가 일치하지 않습니다.다시 입력해 주세요.");
        //        sjs += JavaScript.FooterScript.ToString();

        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "error", sjs.ToString());
        //        return;
        //    }
        //    # endregion

        //    # region 아이디,비번일치시
        //    else
        //    {
        //        string sSponsorID = ds.Tables[0].Rows[0]["SponsorID"].ToString();
        //        DataSet dsDouble = _wwwService.getSponsorDouble(sSponsorID);

        //        //tSponsorDouble에 등록된 후원자일경우 return
        //        if (dsDouble.Tables[0].Rows.Count > 0)
        //        {
        //            sjs = "";
        //            sjs = JavaScript.HeaderScript.ToString();
        //            sjs += JavaScript.GetAlertScript("해당아이디가 잘못되었습니다. \\r\\n다시 입력해 주세요.");
        //            sjs += JavaScript.FooterScript.ToString();
        //            Page.ClientScript.RegisterStartupScript(this.GetType(), "error", sjs.ToString());
        //            return;
        //        }
        //        //로그인처리
        //        else
        //        {
        //            //**************************************************************************************
        //            //웹db의 아이디,비번이 일치하면 기업,미주는 실명인증에 상관없이 로그인 시키고 국내인,국내거주외국인은 실명인증
        //            //여부에 따라서 로그인 또는 실명인증 페이지로 보낸다.
        //            //**************************************************************************************
        //            DivisionCustomerType(ds, _wwwService);


        //        }

        //    }
        //    # endregion

        //    ////----------회원정보 있는 경우 끝===========================================================================
        //}

        //catch (Exception ex)
        //{
        //    //Exception Error Insert
        //    WWWService.Service _wwwService = new WWWService.Service();
        //    _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

        //    sjs = "";
        //    sjs = JavaScript.HeaderScript.ToString();
        //    sjs += JavaScript.GetAlertScript("로그인과정에서 서비스에 문제가 생겼습니다.\\r\\n 관리자에게 문의해 주세요.");
        //    sjs += JavaScript.FooterScript.ToString();

        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //    return;
        //}
    }
    #endregion

    # region 회원정보가 있는 경우
    private void DivisionCustomerType(DataSet ds, WWWService.Service _wwwService)
    {
        //UserInfo sess = new UserInfo();

        //# region  Web디비의 아이디,비번 일치할 경우
        //if (ds.Tables["InfoT"].Rows[0]["SponsorId"] != null)
        //{
        //    //스폰서 아이디 
        //    sSponsorID = ds.Tables["InfoT"].Rows[0]["SponsorId"].ToString().Trim();

        //    //스폰서 이름 
        //    if (ds.Tables["InfoT"].Rows[0]["SponsorName"] != null && ds.Tables["InfoT"].Rows[0]["SponsorName"].ToString().Trim().Length > 0)
        //    {
        //        sSponsorName = ds.Tables["InfoT"].Rows[0]["SponsorName"].ToString().Trim();
        //    }

        //    else
        //    {
        //        sSponsorName = "***";
        //    }
        //    //////***************************************************************************////////
        //    //웹db의 아이디,비번이 일치하면 기업,미주는 실명인증에 상관없이 로그인 시키고 국내인,국내거주외국인은 실명인증
        //    //여부에 따라서 로그인 또는 실명인증 페이지로 보낸다.

        //    # region 1.기업,미주 로그인
        //    //기업과 미주 로그인세션
        //    if (ds.Tables["InfoT"].Rows[0]["ComRegistration"].ToString().Trim().Length == 10 && ds.Tables["InfoT"].Rows[0]["GenderCode"].ToString().Trim() == "C")
        //    {
        //        SetAuthorityCookies(ds);

        //        if (ds.Tables["InfoT"].Rows[0]["CertifyOrgan"] == null || ds.Tables["InfoT"].Rows[0]["CertifyOrgan"].ToString().Trim().Length == 0)
        //        {
        //            //인증이 안된 경우 
        //            sjs = "";
        //            sjs = JavaScript.HeaderScript.ToString();
        //            sjs += JavaScript.GetPageMoveScript("/membership/GroupToCallcenter.aspx");
        //            sjs += JavaScript.FooterScript.ToString();

        //            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //            return;
        //        }
        //        else
        //        {
        //            ////인증이 된 경우 
        //            //sjs = "";
        //            //sjs = JavaScript.HeaderScript.ToString();
        //            //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //            //sjs += JavaScript.FooterScript.ToString();

        //            //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //            //return;    

        //            if (sess.Url == null || sess.Url == "")
        //            {
        //                sjs = "";
        //                sjs = JavaScript.HeaderScript.ToString();
        //                //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //                sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //                sjs += JavaScript.FooterScript.ToString();
        //                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //                return;
        //            }
        //            else if (sess.Url.IndexOf("/membership/") > 0)
        //            {
        //                sjs = "";
        //                sjs = JavaScript.HeaderScript.ToString();
        //                //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //                sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //                sjs += JavaScript.FooterScript.ToString();
        //                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //                return;
        //            }
        //            else
        //            {
        //                sjs = "";
        //                sjs = JavaScript.HeaderScript.ToString();
        //                //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //                sjs += JavaScript.GetPageMoveScript(sess.Url);
        //                sjs += JavaScript.FooterScript.ToString();
        //                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //                return;
        //            }
        //        }
        //    }
        //    //기업로그인 
        //    if (ds.Tables["InfoT"].Rows[0]["LocationType"] != null &&
        //        (ds.Tables["InfoT"].Rows[0]["LocationType"].ToString().Trim() == "미주" ||
        //         ds.Tables["InfoT"].Rows[0]["LocationType"].ToString().Trim() == "국외")
        //       )
        //    {
        //        SetAuthorityCookies(ds);
        //        if (ds.Tables["InfoT"].Rows[0]["CertifyOrgan"] == null || ds.Tables["InfoT"].Rows[0]["CertifyOrgan"].ToString().Trim().Length == 0)
        //        {
        //            //인증이 안된 경우 
        //            sjs = "";
        //            sjs = JavaScript.HeaderScript.ToString();
        //            sjs += JavaScript.GetPageMoveScript("/membership/GroupToCallcenter.aspx");
        //            sjs += JavaScript.FooterScript.ToString();

        //            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //            return;
        //        }
        //        else
        //        {
        //            ////인증이 된 경우 
        //            //sjs = "";
        //            //sjs = JavaScript.HeaderScript.ToString();
        //            //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //            //sjs += JavaScript.FooterScript.ToString();

        //            //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //            //return;

        //            if (sess.Url == null || sess.Url == "")
        //            {
        //                sjs = "";
        //                sjs = JavaScript.HeaderScript.ToString();
        //                //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //                sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //                sjs += JavaScript.FooterScript.ToString();
        //                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //                return;
        //            }
        //            else if (sess.Url.IndexOf("/membership/") > 0)
        //            {
        //                sjs = "";
        //                sjs = JavaScript.HeaderScript.ToString();
        //                //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //                sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //                sjs += JavaScript.FooterScript.ToString();
        //                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //                return;
        //            }
        //            else
        //            {
        //                sjs = "";
        //                sjs = JavaScript.HeaderScript.ToString();
        //                //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
        //                sjs += JavaScript.GetPageMoveScript(sess.Url);
        //                sjs += JavaScript.FooterScript.ToString();
        //                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //                return;
        //            }
        //        }
        //    } //미주로그인 
        //    # endregion

        //    # region 2.실명인증후 사용할 임시세션 설정
        //    //실명인증후 사용할 임시세션 
        //    if (sess.sTempSponsorId != null)
        //    {
        //        sess.sTempSponsorId.Remove(0);
        //    }

        //    sess.sTempSponsorId = ds.Tables["InfoT"].Rows[0]["SponsorID"].ToString().Trim();//후원자번호 
        //    sess.UpdateCookies();
        //    # endregion

        //    # region 3.국내인,국내거주외국인의 실명인증 여부판단(실명인증이 안된 경우 실명인증 페이지로)

        //    //실명인증이 안되어있는 경우 
        //    if (ds.Tables["InfoT"].Rows[0]["CertifyOrgan"] == null || ds.Tables["InfoT"].Rows[0]["CertifyOrgan"].ToString().Trim().Length == 0)
        //    {
        //        # region 국내회원과 국내거주외국인으로 나눈후 국내회원(14세이상,미만구분),국내거주외국인은 외국인등록번호 인증페이지로 넘긴다.

        //        if (ds.Tables["InfoT"].Rows[0]["juminId"] != null && ds.Tables["InfoT"].Rows[0]["juminId"].ToString().Trim().Length == 13)
        //        {
        //            string sJumin = ds.Tables["InfoT"].Rows[0]["juminId"].ToString().Trim();//주민번호
        //            string sJuminDiv = string.Empty;    //국내인과 외국인구분
        //            string sJuminAC = string.Empty;      //국내중14세이상,미만구분 
        //            sJuminDiv = sJumin.Substring(6, 1).ToString();
        //            //주민번호로 국내회원과 국내거주외국인을 구분한다.
        //            //국내회원
        //            if (sJuminDiv == "1" || sJuminDiv == "2" || sJuminDiv == "3" || sJuminDiv == "4")
        //            {
        //                DateTime dtCurrentTime = DateTime.Now;
        //                int iDiv = 0;

        //                if (sJuminDiv == "1" || sJuminDiv == "2")
        //                {
        //                    sJuminAC = "19" + sJumin.Substring(0, 2);
        //                }

        //                else
        //                {
        //                    sJuminAC = "20" + sJumin.Substring(0, 2);
        //                }

        //                try
        //                {
        //                    dtCurrentTime = _wwwService.GetDate();
        //                }

        //                catch (Exception e)
        //                {
        //                    sjs = "";
        //                    sjs = JavaScript.HeaderScript.ToString();
        //                    sjs += JavaScript.GetAlertScript("현재시간을 가져오던중 오류입니다.\\r\\n 오류메시지" + e.Message);
        //                    sjs += JavaScript.FooterScript.ToString();
        //                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //                    return;
        //                }

        //                iDiv = (int.Parse(dtCurrentTime.Year.ToString().Substring(0, 4)) - int.Parse(sJuminAC)) + 1;

        //                //14세이상실명인증페이지로 
        //                if (iDiv >= 14)
        //                {
        //                    sjs = "";
        //                    sjs += JavaScript.HeaderScript.ToString();
        //                    sjs += "Move('adult','" + sSponsorName + "')";
        //                    sjs += JavaScript.FooterScript.ToString();

        //                    Page.ClientScript.RegisterStartupScript(this.GetType(), "confirm", sjs.ToString());
        //                    return;
        //                }

        //                //14세미만 부모님휴대폰인증페이지로 
        //                else
        //                {
        //                    sjs = "";
        //                    sjs += JavaScript.HeaderScript.ToString();
        //                    sjs += "Move('child','" + sSponsorName + "')";
        //                    sjs += JavaScript.FooterScript.ToString();

        //                    Page.ClientScript.RegisterStartupScript(this.GetType(), "confirm", sjs.ToString());
        //                    return;

        //                }
        //            }

        //            //국내거주외국인
        //            else if (sJuminDiv == "5" || sJuminDiv == "6" || sJuminDiv == "7" || sJuminDiv == "8")
        //            {
        //                sjs = "";
        //                sjs += JavaScript.HeaderScript.ToString();
        //                sjs += "Move('foreign','" + sSponsorName + "')";
        //                sjs += JavaScript.FooterScript.ToString();

        //                Page.ClientScript.RegisterStartupScript(this.GetType(), "confirm", sjs.ToString());

        //                return;
        //            }
        //        }

        //        else
        //        {
        //            sjs = "";
        //            sjs = JavaScript.HeaderScript.ToString();
        //            sjs += JavaScript.GetAlertScript(@"한국 컴패션에 있는 회원님의 정보가 올바르지 않습니다.");
        //            sjs += JavaScript.FooterScript.ToString();
        //            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //            return;
        //        }
        //        # endregion
        //    }

        //    else
        //    {
        //        //실명인증이 되어있는경우로 로그인세션을 만들어 준다.

        //        # region 실명인증이 되어있는 경우 로그인
        //        SetAuthorityCookies(ds);
        //        # endregion
        //    }
        //    # endregion
        //}
        //# endregion
    }
    # endregion

    # region 로그인쿠키만들기
    private void SetAuthorityCookies(DataSet ds)
    {
        //UserInfo sess = new UserInfo();
        //string sSponsorNm = string.Empty;
        //string sSponsorID = string.Empty;

        //redirectPage = string.Empty;
        //bLoginOK = false;

        //if (ds != null)
        //{
        //    if (ds.Tables["InfoT"].Rows[0]["SponsorId"] != null && ds.Tables["InfoT"].Rows[0]["SponsorId"].ToString().Trim().Length > 0)
        //    {
        //        sSponsorID = ds.Tables["InfoT"].Rows[0]["SponsorId"].ToString().Trim();  //SponsorId
        //        //SponsorName설정
        //        if (ds.Tables["InfoT"].Rows[0]["SponsorName"] != null && ds.Tables["InfoT"].Rows[0]["SponsorName"].ToString().Trim().Length > 0)
        //            sSponsorNm = ds.Tables["InfoT"].Rows[0]["SponsorName"].ToString().Trim();

        //        else
        //            sSponsorNm = "***";

        //        # region 세션의 SponsorID를 가지고 Compass4의 후원자정보를 검색해서 다른 세션을 만든다.

        //        ds = null;
        //        ds = GetSponsorInfomation(sSponsorID.ToString().Trim());
        //        //Compass4의 후원자정보가 1건일때  
        //        if (ds != null && ds.Tables["tSponsorMaster"] != null && ds.Tables["tSponsorMaster"].Rows.Count == 1)
        //        {
        //            string sUserId = ds.Tables["tSponsorMaster"].Rows[0]["UserID"].ToString().Trim();  //아이디
        //            string sSponsorName = ds.Tables["tSponsorMaster"].Rows[0]["SponsorName"].ToString().Trim();//후원자이름
        //            string sJuminId = ds.Tables["tSponsorMaster"].Rows[0]["JuminID"].ToString().Trim();//주민번호
        //            string sCertifyDate = ds.Tables["tSponsorMaster"].Rows[0]["CertifyDate"].ToString().Trim();//인증날짜
        //            string sUserClass = ds.Tables["tSponsorMaster"].Rows[0]["UserClass"].ToString().Trim();//회원종류 (14세미만,14세이상,외국인국내거주,기업,해외회원)  
        //            string sComRegistration = ds.Tables["tSponsorMaster"].Rows[0]["ComRegistration"].ToString().Trim();//사업자등록번호
        //            string sConId = ds.Tables["tSponsorMaster"].Rows[0]["ConID"].ToString().Trim();//미국후원자번호
        //            string sGenderCode = ds.Tables["tSponsorMaster"].Rows[0]["GenderCode"].ToString().Trim(); //성별 
        //            string sLocationType = ds.Tables["tSponsorMaster"].Rows[0]["LocationType"].ToString().Trim();
        //            string sTranslationFlag = ds.Tables["tSponsorMaster"].Rows[0]["TranslationFlag"].ToString().Trim();//편지번역여부Y/N


        //            ////////세션////////////////////////////////////////////////////////////////////////////////////////////////////
        //            sess.SponsorID = sSponsorID;
        //            sess.UserId = sUserId;
        //            sess.UserName = sSponsorName;
        //            sess.Jumin = sJuminId;
        //            sess.CertifyDate = sCertifyDate;
        //            sess.UserClass = sUserClass;
        //            sess.GroupNo = sComRegistration;
        //            sess.ConId = sConId;
        //            sess.GenderCode = sGenderCode;
        //            sess.LocationType = sLocationType;
        //            sess.TranslationFlag = sTranslationFlag;

        //            sess.UpdateCookies();

        //            redirectPage = sess.Url;
        //            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //        }
        //        else
        //        {
        //            //로그인을 하되 Compass4의 정보가 올바르지 않을때는 콜센터 문의 페이지로 
        //            sjs = "";
        //            sjs = JavaScript.HeaderScript.ToString();
        //            sjs += JavaScript.GetAlertScript(@"한국 컴패션에 있는 회원님의 정보가 올바르지 않습니다.");
        //            sjs += JavaScript.FooterScript.ToString();
        //            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
        //            return;
        //        }

        //        # endregion
        //    }

        //    # region Mate세션
        //    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    //Mate Session
        //    WWWService.Service _wwwService = new WWWService.Service();
        //    try
        //    {
        //        ds = _wwwService.loginMate(sess.UserId.ToString().Trim());
        //    }
        //    catch (Exception ex)
        //    {
        //        //Exception Error Insert
        //        _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);
        //    }
        //    if (ds != null && ds.Tables["loginCorrespondence"] != null && ds.Tables["loginCorrespondence"].Rows.Count > 0)
        //    {
        //        if (ds.Tables["loginCorrespondence"].Rows.Count > 0 && ds.Tables["loginCorrespondence"].Rows[0]["CurrentUse"].ToString() == "Y")
        //        {
        //            if (ds.Tables["loginCorrespondence"].Rows[0]["OfficeMate"].ToString() == "Y")
        //            {
        //                sess.Mate += ",사무";
        //            }
        //            if (ds.Tables["loginCorrespondence"].Rows[0]["TranslationMate"].ToString() == "Y")
        //            {
        //                if (ds.Tables["loginCorrespondence"].Rows[0]["TranslationLevel"].ToString() != "영작")
        //                {
        //                    sess.Mate += ",번역";
        //                    //sess.Mate += "(" + ds.Tables["loginCorrespondence"].Rows[0]["TranslationLevel"].ToString() + ")";
        //                }
        //                else
        //                {
        //                    sess.Mate += ",영작";
        //                    //sess.Mate += "(" + ds.Tables["loginCorrespondence"].Rows[0]["WritingLevel"].ToString() + ")";
        //                }
        //            }
        //            if (ds.Tables["loginCorrespondence"].Rows[0]["ProfessionMate"].ToString() == "Y")
        //            {
        //                sess.Mate += ",전문";
        //            }
        //        }

        //        //,자르기
        //        if (!string.IsNullOrEmpty(sess.Mate))
        //        {
        //            if (sess.Mate.Length > 0)
        //            {
        //                sess.Mate = "Mate  " + "<font color='#0054a6'>" + sess.Mate.Substring(1) + "</div>";
        //            }
        //        }
        //    }
        //    else
        //    {
        //        sess.Mate = "";
        //    }

        //    sess.UpdateCookies();
        //    # endregion

        //    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    //결연,일반후원정보에 대한 세션설정 
        //    if (sess.CertifyDate.ToString().Trim().Length > 0)
        //    {
        //        SponsorType();
        //    }

        //    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    //로그인한 클라이언트아이피주소 

        //    GetLoginIP(sess.UserId.ToString(), sess.UserName.ToString(), sess.Jumin.ToString());

        //    if (!string.IsNullOrEmpty(redirectPage))
        //    {
        //        bLoginOK = true;
        //    }

        //    //추가됨(2011/09/06) - 1:1결연 장바구니 SponsorID로 이관하기
        //    //추가됨(2011/09/15) - CIV, CSP DB Change
        //    if (Request.Cookies["CooBasket"] != null)
        //    {
        //        WWWService.Service wwwSer = new WWWService.Service();
        //        wwwSer.UpdateSponsorEnsureWeb(Request.Cookies["CooBasket"].Value, sess.SponsorID);

        //        wwwSer.SponCartUserIDChange(sess.UserId, Request.Cookies["CooBasket"].Value);

        //        //쿠키삭제
        //        HttpCookie cookie = Request.Cookies["CooBasket"];
        //        cookie.Expires = DateTime.Now.AddDays(-25);
        //        HttpContext.Current.Response.Cookies.Add(cookie);
        //    }

        //    //추가됨(2011/08/25)
        //    string goRedirect;
        //    if (String.IsNullOrEmpty(Request.QueryString["returnUrl"]))
        //        goRedirect = "/Default.aspx";
        //    else
        //        goRedirect = Request.QueryString["returnUrl"];

        //    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "goReturnUrl", String.Format("<script>location.href='{0}';</script>", goRedirect));

        //}
    }
    # endregion

    # region 로그인한 클라이언트IP주소(tLoginAddress에 넣기)
    private void GetLoginIP(string UserID, string UserName, string JuminID)
    {
        //string sResult = string.Empty;
        //string ClientIP = string.Empty; //아이피
        //ClientIP = Request.UserHostAddress.ToString().Trim();
        //try
        //{
        //    WWWService.Service _wwwwService = new WWWService.Service();
        //    sResult = _wwwwService.LoginAddressInsert(ClientIP, UserID, UserName, "");

        //}
        //catch (Exception ex)
        //{
        //    //Exception Error Insert
        //    WWWService.Service _wwwService = new WWWService.Service();
        //    _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);
        //}
    }
    #endregion

    #region VOC여부,결연정보,일반후원정보
    /*
    /// <summary>
    /// VOC여부,결연정보,일반후원정보를 로그인패널에 보여준다. 
    /// </summary>
    public void SponsorType()
    {
        UserInfo sess = new UserInfo();
        try
        {
            WWWService.Service _wwwService = new WWWService.Service();

            //VOC여부       
            ds = null;
            ds = _wwwService.getVOCYN(sess.SponsorID.ToString().Trim());

            if (ds.Tables["GroupT"].Rows.Count > 0)
            {
                if (ds.Tables["GroupT"].Rows[0]["GroupType"].ToString().Trim() == "VOC")
                {
                    sess.VOC = ds.Tables["GroupT"].Rows[0]["GroupType"].ToString().Trim();
                }
            }
            //결연,일반후원건수
            ds = null;
            ds = _wwwService.getApplyCount(sess.SponsorID.ToString().Trim());

            if (ds.Tables["ApplyT"].Rows.Count > 0)
            {
                if (ds.Tables["ApplyT"].Rows[0]["apply"].ToString().Trim() != "0")
                {
                    sess.CommitCount = ds.Tables["ApplyT"].Rows[0]["apply"].ToString().Trim();

                }
            }
            if (ds.Tables["ApplyT"].Rows.Count > 1)
            {

                if (ds.Tables["ApplyT"].Rows[1]["apply"].ToString().Trim() != "0")
                {
                    sess.ApplyCount = ds.Tables["ApplyT"].Rows[1]["apply"].ToString().Trim();
                }
            }

            sess.UpdateCookies();
        }
        catch (Exception ex)
        {
            //Exception Error Insert
            WWWService.Service _wwwService = new WWWService.Service();
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            sjs = "";
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("서비스로딩에 실패했습니다.\\r\\n관리자에게 문의해주세요.");
            sjs += JavaScript.FooterScript.ToString();

            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
            return;
        }
    }
	*/
    #endregion

    #region Compass4에서후원자정보가져오기
    private DataSet GetSponsorInfomation(string sSponsorID)
    {
        if (sSponsorID != null && sSponsorID.ToString().Trim().Length > 0)
        {
            try
            {
                WWWService.Service _wwwService = new WWWService.Service();
                ds = null;
                ds = _wwwService.getSponsorInformation(sSponsorID.ToString().Trim());
            }
            catch (Exception ex)
            {
                //Exception Error Insert
                WWWService.Service _wwwService = new WWWService.Service();
                _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

                ds = null;
                //sjs = "";
                //sjs = JavaScript.HeaderScript.ToString();
                //sjs += JavaScript.GetAlertScript("회원정보수집을 위한 서비스 로딩에 실패했습니다.\\r\\n 다시 로그인해 주세요.");
                //sjs += JavaScript.GetPageMoveScript("/membership/MemberLogin.aspx");
                //sjs += JavaScript.FooterScript.ToString();

                //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
                //return;      
            }
        }
        return ds;
    }
    # endregion

    #region 메이트 메인 업데이트 2012-11-22 
    private void MateSSB()
    {
        WWWService.Service _wwwService = new WWWService.Service();
        string sjs = "";

        DataSet ssb_data = new DataSet();
        PagedDataSource pds = new PagedDataSource();

        try
        {
            ssb_data = _wwwService.MateSsbList();
        }
        catch (Exception ex)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("데이터 불러오기를 실패하였습니다.\r\n" + ex.ToString());
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
            return;
        }

        if (ssb_data != null)
        {
            if (ssb_data.Tables[0].Rows.Count > 0)
            {
                pds.DataSource = ssb_data.Tables[0].DefaultView;
                pds.AllowPaging = true;
                pds.PageSize = 3;
                pds.CurrentPageIndex = 0;
                ssbData.DataSource = pds;
                ssbData.DataBind();
            }
        }
    }
    #endregion

    #region 메이트 메인 커뮤니케이션 3개 데이터 바인딩 2012-11-22
    /// <summary>
    /// 메이트 메인 커뮤니케이션 3개 데이터 바인딩
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ssbData_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        {
            return;
        }
        DataRowView drv = (DataRowView)e.Item.DataItem;

        string code = drv["code"].ToString();

        HtmlAnchor more = (HtmlAnchor)e.Item.FindControl("a_more");
        HtmlInputHidden gubun = (HtmlInputHidden)e.Item.FindControl("code_gubun");

        switch (gubun.Value.ToString())
        {
            case "month_mate":
                more.HRef = "/community/monthlymatelist.aspx";
                break;
            case "bundal_defy":
                more.HRef = "/nanum/MasterChallengeList.aspx";
                break;
            case "benefit_letter":
                more.HRef = "/nanum/BenefitDefault.aspx";
                break;
        }

    }
    #endregion

    #region 롤링배너 불러오기 2012-12-03
    private void RollingBanner(string code)
    {
        WWWService.Service _wwwService = new WWWService.Service();
        string sjs = "";

        DataSet data = new DataSet();

        try
        {
            data = _wwwService.RollingSsbList(code);
        }
        catch (Exception ex)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("데이터 불러오기를 실패하였습니다.\r\n" + ex.ToString());
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
            return;
        }

        if (data != null)
        {
            if (data.Tables[0].Rows.Count > 0)
            {
                RollingData.DataSource = data;
                RollingData.DataBind();
            }
        }
    }
    #endregion

    #region 팝업창 불러오기 2013-06-03
    private void PopupDataLoad(string sType)
    {
        WWWService.Service _wwwService = new WWWService.Service();
        string sjs = "";

        DataSet pdata = new DataSet();

        try
        {
            pdata = _wwwService.GetDataPopupList("", sType);
        }
        catch (Exception ex)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("데이터 불러오기를 실패하였습니다.\r\n" + ex.ToString());
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
            return;
        }

        //테스트 2013-05-31
        //Response.Write("<script> alert('" + pdata.Tables[0].Rows.Count.ToString() + "'); </script>");

        if (pdata != null)
        {
            if (pdata.Tables[0].Rows.Count > 0)
            {
                PopupData.DataSource = pdata;
                PopupData.DataBind();
            }
        }
    }

    /// <summary>
    /// 팝업창 데이터 바인딩
    /// </summary>
    protected void PopupData_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        {
            return;
        }
        DataRowView drv = (DataRowView)e.Item.DataItem;

        string idx = drv["idx"].ToString();
        string pname = drv["p_name"].ToString();
        string ptop = drv["p_top"].ToString();
        string pleft = drv["p_left"].ToString();
        string pwidth = drv["p_width"].ToString();
        string pheight = drv["p_height"].ToString();
        string pstatus = drv["p_status"].ToString();
        string pscrollbar = drv["p_scrollbar"].ToString();

        string str = string.Empty;

        if (pwidth != "")
        {
            if (int.Parse(pwidth) > 0)
            {
                pwidth = (int.Parse(pwidth) - 4).ToString();
            }
        }

        if (pheight != "")
        {
            if (int.Parse(pheight) > 0)
            {
                pheight = (int.Parse(pheight) + 20).ToString();
            }
        }

        str = "<script type='text/javascript'> popupLoad('" + pname + "', '"
                                                            + idx + "', '"
                                                            + pleft + "', '"
                                                            + ptop + "', '"
                                                            + pwidth + "', '"
                                                            + pheight + "', '"
                                                            + pstatus + "', '"
                                                            + pscrollbar + "'); </script>";

        HtmlGenericControl popup_div = (HtmlGenericControl)e.Item.FindControl("popup_div");

        if (int.Parse(idx) > 0)
        {
            popup_div.InnerHtml = str;
        }

    }
    #endregion

    protected void repData_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label labNew = (Label)e.Item.FindControl("labNew");

        HiddenField hidb_writeday = (HiddenField)e.Item.FindControl("hidb_writeday");
        DateTime dt = DateTime.Parse(hidb_writeday.Value);
        TimeSpan ts = DateTime.Now - dt;

        //HtmlGenericControl comment_num = (HtmlGenericControl)e.Item.FindControl("comment_num");
        //if (_tableIdx == 1001)
        //{
        //    comment_num.Visible = false;
        //}
        if (ts.TotalMinutes >= 10080)
            labNew.Visible = false;
    }

    protected void repOurData_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label labNew = (Label)e.Item.FindControl("labNew");

        HiddenField hidb_writeday = (HiddenField)e.Item.FindControl("hidb_writeday");
        DateTime dt = DateTime.Parse(hidb_writeday.Value);
        TimeSpan ts = DateTime.Now - dt;

        //HtmlGenericControl comment_num = (HtmlGenericControl)e.Item.FindControl("comment_num");
        //if (_tableIdx == 1001)
        //{
        //    comment_num.Visible = false;
        //}
        //if (ts.TotalMinutes >= 1440)
        if (ts.TotalMinutes >= 10080)
            labNew.Visible = false;
    }
}