﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>한국컴패션</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="javascript">
    function setCookie(name, value, expiredays) {
        var todayDate = new Date();
        var domain = ".compassion.or.kr";
        todayDate.setDate(todayDate.getDate() + expiredays);
        document.cookie = name + "=" + escape(value) + "; path=/; domain=" + domain + "; expires=" + todayDate.toGMTString() + ";"
        //alert('쿠키정보 저장완료'); 
    }

    function CloseWin() {
        if (document.getElementById ("Notice").checked) {
            setCookie("notice", "done", 1);
            self.close();
        } else {
            self.close();
        }
    }
</script> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF">
<table width="320" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td height="12">    
        <script>
            function getHost() {
                var host = '';
                var loc = String(location.href);
                if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
                else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
                else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
                return host;
            }

            function goLink() {
                var host = getHost();
                if (host != '') location.href = 'http://' + host + '/community/noticeview.aspx?iTableIndex=1001&iNoIndex=7&iBoardIdx=75001&b_num=77376&re_level=0&ref=77376&re_step=0&pageIdx=0&searchOption=all&searchTxt=';
            }
        </script>
        <a href="javascript:goLink()" target="_blank"><img src="/image/notice/matepopup.jpg" border="0"></a>
    </td>
  </tr>
  <tr>     
  <form name="checkClose">
      <td height="15" style="padding-right:15"> 
        <div align="right" style="font-size:8pt">        
        <input type="checkbox" name="Notice" id="Notice"  >
          오늘하루 이창을 열지않습니다.[<a href="javascript:CloseWin();">닫기]</a>&nbsp;&nbsp;&nbsp; </div>
       </td>
 </form>
  </tr>
</table>
</body>
</html>