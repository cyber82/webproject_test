﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;


    public class AESCipher
    {

       // private System.Text.UTF8Encoding utf8Encoding = null;
       // private RijndaelManaged rijndael = null;

        public String AES_encrypt(String Input, String key)
        {
            RijndaelManaged aes = new RijndaelManaged();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = Encoding.UTF8.GetBytes(key);
            aes.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            var encrypt = aes.CreateEncryptor(aes.Key, aes.IV);
            byte[] xBuff = null;
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write))
                {
                    byte[] xXml = Encoding.UTF8.GetBytes(Input);
                    cs.Write(xXml, 0, xXml.Length);
                }

                xBuff = ms.ToArray();
            }

            String Output = Convert.ToBase64String(xBuff);
            return Output;
        }

        public String AES_decrypt(String Input, String key)
        {
            RijndaelManaged aes = new RijndaelManaged();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = Encoding.UTF8.GetBytes(key);
            aes.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            var decrypt = aes.CreateDecryptor();
            byte[] xBuff = null;
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write))
                {
                    byte[] xXml = Convert.FromBase64String(Input);
                    cs.Write(xXml, 0, xXml.Length);
                }

                xBuff = ms.ToArray();
            }

            String Output = Encoding.UTF8.GetString(xBuff);
            // String Output = HttpUtility.UrlEncode(xBuff);
            return Output;
        }

        //public AESCipher(string key, string iv)
        //{
        //    if (key == null || key == "")
        //        throw new ArgumentException("The key is not null.", "key");
        //    if (iv == null || iv == "")
        //        throw new ArgumentException("The initial vector cis not null.", "initialVector");
        //    this.utf8Encoding = new System.Text.UTF8Encoding();
        //    this.rijndael = new RijndaelManaged();
        //    this.rijndael.Mode = CipherMode.CBC;
        //    this.rijndael.Padding = PaddingMode.PKCS7;
        //    this.rijndael.KeySize = 128;
        //    this.rijndael.BlockSize = 128;
        //    this.rijndael.Key = hex2Byte(key);
        //    this.rijndael.IV = hex2Byte(iv);
        //}

        //public string Encrypt(string text)
        //{
        //    byte[] cipherBytes = null;
        //    ICryptoTransform transform = null;
        //    if (text == null)
        //        text = "";
        //    try
        //    {
        //        cipherBytes = new byte[] { };
        //        transform = this.rijndael.CreateEncryptor();
        //        byte[] plainText = this.utf8Encoding.GetBytes(text);
        //        cipherBytes = transform.TransformFinalBlock(plainText, 0, plainText.Length);
        //    }
        //    catch
        //    {                //System.Console.WriteLine(e.StackTrace); 
        //        throw new ArgumentException("text is not a valid string!(Encrypt)", "text");
        //    }
        //    finally
        //    {
        //        //if (this.rijndael != null)     
        //        //this.rijndael.Clear();        
        //    }
        //    return Convert.ToBase64String(cipherBytes);
        //}

        //public string Decrypt(string text)
        //{
        //    byte[] plainText = null;
        //    ICryptoTransform transform = null;
        //    if (text == null || text == "")
        //        throw new ArgumentException("text is not null.");
        //    try
        //    {
        //        plainText = new byte[] { };
        //        transform = rijndael.CreateDecryptor();
        //        byte[] encryptedValue = Convert.FromBase64String(text);
        //        plainText = transform.TransformFinalBlock(encryptedValue, 0, encryptedValue.Length);
        //    }
        //    catch
        //    {
        //        throw new ArgumentException("text is not a valid string!(Decrypt)", "text");
        //        //System.Console.WriteLine(e.StackTrace);         
        //    }
        //    finally
        //    {
        //        //if (this.rijndael != null)                  
        //        //this.rijndael.Clear();           
        //    }
        //    return this.utf8Encoding.GetString(plainText);
        //}

        //public byte[] hex2Byte(string hex)
        //{
        //    byte[] bytes = new byte[hex.Length / 2];
        //    for (int i = 0; i < bytes.Length; i++)
        //    {
        //        try
        //        {
        //            bytes[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
        //        }
        //        catch
        //        {
        //            throw new ArgumentException("hex is not a valid hex number!", "hex");
        //        }
        //    }
        //    return bytes;
        //}

        //public string byte2Hex(byte[] bytes)
        //{
        //    string hex = "";
        //    if (bytes != null)
        //    {
        //        for (int i = 0; i < bytes.Length; i++)
        //        {
        //            hex += bytes[i].ToString("X2");
        //        }
        //    }
        //    return hex;
        //}
    }
