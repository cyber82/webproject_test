﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.IO; 

public class WebCommon
{
    private HttpContext _httpContext;

    public WebCommon()
    {
        _httpContext = HttpContext.Current;
    }

    public static string GetImageName(object idxProduct, object imgExt)
    {
        if (String.IsNullOrEmpty(imgExt.ToString()))
            return "no-image.gif";
        else
            return idxProduct.ToString() + imgExt.ToString();

    }

    public static string CommentNewLine(object comment)
    {
        if (!String.IsNullOrEmpty(comment.ToString()))
            return comment.ToString().Replace(System.Environment.NewLine, "<br/>");
        else
            return String.Empty;
    }

    public static void LogWrite(string path, string msg)
    {
        try
        {
            StreamWriter writer =
                new StreamWriter(
                File.Open(path,
                FileMode.Append,
                FileAccess.Write,
                FileShare.Read), System.Text.Encoding.Default);

            //writer.WriteLine(DateTime.Now.ToString("yyyyMMdd HH:mm:ss"));
            writer.WriteLine(msg);
            writer.WriteLine();

            writer.Close();
            writer.Dispose();
        }
        catch { }
    }

    public static string GetNamePadding(object name, int length)
    {
        if (name.ToString().Length > length)
            return name.ToString().Substring(0, length - 1) + ". . .";
        else
            return name.ToString();
    }

    public static string GetNamePadding2(object name, int length)
    {
        if (name.ToString().Length > length)
            return name.ToString().Substring(0, length - 1) + ". .";
        else
            return name.ToString();
    }

    public static string Decrypt(string value)
    {
        return Roamer.Config.Cryptography.Decrypt(value); 
    }

    public static string Encrypt(string value)
    {
        return Roamer.Config.Cryptography.Encrypt(value);
    }
}
