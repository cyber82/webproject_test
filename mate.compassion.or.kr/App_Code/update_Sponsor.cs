﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Roamer.Config;


public class update_Sponsor
{
    private HttpContext _httpContext;
    public update_Sponsor()
    {
        _httpContext = HttpContext.Current;

        if (_httpContext.Request.Cookies["CompassionToken_2"] == null)
        {
            HttpCookie ticketCookie = new HttpCookie("CompassionToken_2");
            _httpContext.Response.AppendCookie(ticketCookie);
        }
    }
    
    public void UpdateCookies()
    {
        _httpContext.Request.Cookies.Set(_httpContext.Response.Cookies["CompassionToken_2"]);
    }

    public void ClearCookie()
    {
        HttpCookie cookie = HttpContext.Current.Request.Cookies["CompassionToken_2"];
        cookie.Expires = DateTime.Now.AddDays(-1);

        HttpContext.Current.Response.Cookies.Add(cookie);
    }

    public string update_Jumin
    {
        get
        {
            return Cryptography.Decrypt(_httpContext.Request.Cookies["CompassionToken_2"]["update_Jumin"]);
        }
        set
        {
            _httpContext.Response.Cookies["CompassionToken_2"]["update_Jumin"] = Cryptography.Encrypt(value);
        }
    }

    public string update_TranslationFlag
    {
        get
        {
            return Cryptography.Decrypt(_httpContext.Request.Cookies["CompassionToken_2"]["update_TranslationFlag"]);
        }
        set
        {
            _httpContext.Response.Cookies["CompassionToken_2"]["update_TranslationFlag"] = Cryptography.Encrypt(value);
        }
    }
}
