﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

/// <summary>
/// Receipt의 요약 설명입니다.
/// </summary>
public class Receipt
{
	public Receipt()
	{
		//
		// TODO: 생성자 논리를 여기에 추가합니다.
		//
	}

    private string _ConID;
    public string ConID
    {
        get { return _ConID; }
        set { _ConID = value; }
    }

    private string _ConName;
    public string ConName
    {
        get { return _ConName; }
        set { _ConName = value; }
    }

    private string _ConSSN;
    public string ConSSN
    {
        get { return _ConSSN; }
        set { _ConSSN = value; }
    }

    private string _ConAddress;
    public string ConAddress
    {
        get { return _ConAddress; }
        set { _ConAddress = value; }
    }

    private string _ConType;
    public string ConType
    {
        get { return _ConType; }
        set { _ConType = value; }
    }

    public string CreateReceipt(DataSet dsListData)
    {
        string sFilePath = string.Empty;

        sFilePath = CreateReceiptFile(dsListData);
        return sFilePath;
    }

    private string CreateReceiptFile(DataSet dsReceiptContent)
    {    
        string sResult = string.Empty;

        //TextWriter tw;
        Encoding encUTF8;
        Encoding encKor;
        byte[] byteUTF8;
        byte[] byteKor;
        string sFileContent = string.Empty;
        string sFilePath = string.Empty;
        string sFileFullName = string.Empty;
        DateTime dtAppDate = DateTime.Now;

        string sConID = this._ConID;
        string sConName = this._ConName;
        string sConSSN = this._ConSSN;
        string sConAddress = this._ConAddress;

        //-- HTML 내용 만들기
        sFileContent = CreateReceiptHtml(sConID, sConName, sConSSN, sConAddress, dsReceiptContent, dtAppDate);

        //-- Encoding
        encUTF8 = System.Text.Encoding.UTF8;
        encKor = System.Text.Encoding.GetEncoding("euc-kr");

        // Convert Encoding From "UTF8" To "euc-kr"
        byteUTF8 = Encoding.UTF8.GetBytes(sFileContent);
        byteKor = Encoding.Convert(encUTF8, encKor, byteUTF8);

        encKor.GetString(byteKor);

        return encKor.GetString(byteKor); ;
    }

    /// <summary>
    /// 기부금영수증 HTML 내용 만들기
    /// </summary>
    /// <param name="sConID">후원자 ID</param>
    /// <param name="sConName">후원자 이름</param>
    /// <param name="sConSSN">후원자 주민번호</param>
    /// <param name="sConAddress">후원자 주소</param>
    /// <param name="dsReceiptContent">기부내용</param>
    /// <param name="dtAppTime">신청일</param>
    /// <returns></returns>
    private string CreateReceiptHtml(string sConID, string sConName, string sConSSN, string sConAddress, DataSet dsReceiptContent, DateTime dtAppTime)
    {
        WWWService.Service _wwwService = new WWWService.Service();
        DateTime dtNow = _wwwService.GetDate(); //현재시간

        string sHtml = string.Empty;
        Int64 iTotalAmount = 0;
        string sMyCompanyName = "사회복지법인 한국컴패션";
        string sMyCompanySSN = "108-82-05789";
        string sMyCompanyAddress = "서울시 용산구 한남동 723-41 석전빌딩";

        //-- 기부금 영수증 형태 : 개인(person), 법인(company)
        string sConType = this._ConType.ToLower();

        #region //-- 상단, 스크립트, 스타일
        sHtml = " <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>    ".TrimEnd()

        + " <html>                                                                  ".TrimEnd()
        + " <head>                                                                  ".TrimEnd()
        + " <title>한국컴패션 기부금영수증</title>                                  ".TrimEnd()

        #region 미사용
            ////Print시 필요한 파일 설치여부
            //+ " <SCRIPT>                                                                ".TrimEnd()        
            //+ " function Installed()                                                    ".TrimEnd()
            //+ " {                                                                       ".TrimEnd()
            //+ "   if (typeof(document.all('IEPageSetupX'))!='undefined' && document.all('IEPageSetupX')!=null) ".TrimEnd()
            //+ " 	return true;                                                        ".TrimEnd()
            //+ "   else                                                                  ".TrimEnd()
            //+ " 	return false;                                                       ".TrimEnd()
            //+ " }                                                                       ".TrimEnd()

        //+ " function PrintTest()                                                    ".TrimEnd()
            //+ " {                                                                       ".TrimEnd()
            //+ " 	if (!Installed())                                                   ".TrimEnd()
            //+ " 	{                                                                   ".TrimEnd()
            //+ " 		alert('컨트롤이 설치되지 않았습니다. 정상적으로 인쇄되지 않을 수 있습니다.'); ".TrimEnd()
            //+ " 	    return false;                                                   ".TrimEnd()
            //+ " 	}                                                                   ".TrimEnd()
            //+ " 	return true;                                                        ".TrimEnd()
            //+ " }                                                                       ".TrimEnd()
            ////Print시 필요한 파일 설치여부 끝

        ////프린트설정
            //+ " function setPrint()                                                     ".TrimEnd()
            //+ " {                                                                       ".TrimEnd()
            //+ "     if (!PrintTest()){ return; }                                        ".TrimEnd()
            //+ "     IEPageSetupX.header          = '';                                  ".TrimEnd()
            //+ "     IEPageSetupX.footer          = '';                                  ".TrimEnd()
            //+ "     IEPageSetupX.Orientation     = 1;                                   ".TrimEnd()
            //+ "     IEPageSetupX.leftMargin      = '30';                                ".TrimEnd()
            //+ "     IEPageSetupX.rightMargin     = '30';                                ".TrimEnd()
            //+ "     IEPageSetupX.topMargin       = '30';                                ".TrimEnd()
            //+ "     IEPageSetupX.bottomMargin    = '30';                                ".TrimEnd()
            //+ "     IEPageSetupX.PrintBackground = false;                                ".TrimEnd()
            //+ "     agree = confirm('현재 페이지를 출력하시겠습니까?');                 ".TrimEnd()
            ////+ "     if (agree)IEPageSetupX.Print(true);                                 ".TrimEnd()
            //+ "     if (agree)window.print();                                           ".TrimEnd()
            //+ " }                                                                       ".TrimEnd()
            //+ " </script>                                                               ".TrimEnd()
            //프린트설정 끝

        //+ " <SCRIPT language='JavaScript' for='IEPageSetupX' event='OnError(ErrCode, ErrMsg)'> ".TrimEnd()
            //+ " alert('에러 코드: ' + ErrCode + '에러 메시지: ' + ErrMsg);".TrimEnd()
            //+ " </SCRIPT>".TrimEnd() 
        #endregion

        //ScriptX프린트 설정
        //프린트설정

        + " <script language='javascript' type='text/javascript'>                   ".TrimEnd()

        //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조       ".TrimEnd()

        // 마우스 우클릭 제한
        + " document.oncontextmenu = function(){return false;};                     ".TrimEnd()
        // 키 입력 제한
        + " document.onkeydown = function(){return false;};                         ".TrimEnd()
        + " function setPrint()                                                     ".TrimEnd()
        + " {                                                                       ".TrimEnd()
        + "     if(factory.printing == null)                                        ".TrimEnd()
        + "     {                                                                   ".TrimEnd()
        + "         alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.'); ".TrimEnd()
        + "         return;                                                         ".TrimEnd()
        + "     }                                                                   ".TrimEnd()
        + "     factory.printing.header       = '';                                 ".TrimEnd()
        + "     factory.printing.footer       = '';                                 ".TrimEnd()
        + "     factory.printing.portrait     = true;                               ".TrimEnd() //true 세로출력,false가로출력
        + "     factory.printing.leftMargin   = 10;                                 ".TrimEnd()
        + "     factory.printing.topMargin    = 10;                                 ".TrimEnd()
        + "     factory.printing.rightMargin  = 10;                                 ".TrimEnd()
        + "     factory.printing.bottomMargin = 10;                                 ".TrimEnd()

        //+ "     factory.printing.printBackground = true;                            ".TrimEnd()

        + "     agree = confirm('현재 페이지를 출력하시겠습니까?');                   ".TrimEnd()
        + "     if (agree) factory.printing.Print(false, window)                    ".TrimEnd()
        + " }                                                                       ".TrimEnd()

        + " function fnPrint()                                                      ".TrimEnd()
        + " {                                                                       ".TrimEnd()
        + "     document.getElementById('printID').style.display = 'none';          ".TrimEnd()
        + "     window.print();                                                     ".TrimEnd()
        + "     document.getElementById('printID').style.display = 'block';         ".TrimEnd()
        + " }                                                                       ".TrimEnd()
        + " </script>                                                               ".TrimEnd()

        //프린트설정 끝

        #region 미사용
            //프린트함수
            //+ " <script language='javascript' type='text/javascript'>                   ".TrimEnd()
            //+ " window.onload=printPage;                                                ".TrimEnd()
            //+ " function printPage() {                                                  ".TrimEnd()
            //+ "     if (window.print) {                                                 ".TrimEnd()
            //+ "         agree = confirm('현재 페이지를 출력하시겠습니까?');             ".TrimEnd()
            ////+ "   if (agree) window.print();                                          ".TrimEnd()
            ////+ "   if (agree) setPrint();                                           ".TrimEnd()
            //+ "     if (agree) factory.printing.Print(false, window)                    ".TrimEnd()
            //+ "    }                                                                    ".TrimEnd()
            //+ " }                                                                       ".TrimEnd()
            //+ " </script>                                                               ".TrimEnd()
            //프린트함수 끝

        #endregion

        #region style sheet (css)
        + " <style type='text/css'> ".TrimEnd()
        + " /*body { margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;}*/ ".TrimEnd()
        + " img {border:none;} ".TrimEnd()
        + " body, td {  font-family: '돋움'; font-size:8pt; color: #000000;} ".TrimEnd()

        //+ " body, td {  font-family: 'Yoon 윤명조 530_TT'; font-size:10pt; color: #000000;} ".TrimEnd()

        + "  ".TrimEnd()
        + " /* 소득세법 */ ".TrimEnd()
        + " td.CodeTitle{ font-size:6pt; padding-top:5px; padding-bottom:5px; padding-left:5px; padding-right:5px;} ".TrimEnd()

        // 2013-11-29 수정 (유원준)
        + " td.div {font-size:8pt; padding-bottom:5px; padding-left:5px; padding-right:5px; border-top:gray 1px solid; height:19px; width:205px;} ".TrimEnd()
        + " td.type {font-size:8pt; padding-bottom:5px; padding-left:5px; padding-right:5px; border-top:gray 1px solid; border-right:gray 1px solid; border-left:gray 1px solid; height:19px; width:50px; text-align:center;} ".TrimEnd()
        + " td.Code {font-size:8pt; padding-bottom:5px; padding-left:5px; padding-right:5px; border-top:gray 1px solid; height:19px; width:20px; text-align:center;} ".TrimEnd()
        + " td.title{ font-size:8pt; padding-bottom:5px; padding-left:5px; padding-right:5px;} ".TrimEnd()
        + " td.pCode{ font-size:8pt; padding-bottom:5px; padding-left:5px; padding-right:5px;} ".TrimEnd()

        + "  ".TrimEnd()
        + " /* 기부내용 HEAD */ ".TrimEnd()
        + " td.ContentHeadType{width: 90px; height:25px; text-align: center; background-color: silver; border-right:gray 1px solid;} ".TrimEnd()
        + " td.ContentHeadCode{width: 45px; text-align: center; background-color: silver; border-right:gray 1px solid;} ".TrimEnd()
        + " td.ContentHeadDate{width: 90px; text-align: center; background-color: silver; border-right:gray 1px solid;} ".TrimEnd()
        + " td.ContentHeadDesc{width: 80px; text-align: center; background-color: silver; border-right:gray 1px solid;} ".TrimEnd()
        + " td.ContentHeadAmount{width: 100px; text-align: center; background-color: silver; border-right:gray 1px solid;} ".TrimEnd()
        + "  ".TrimEnd()
        + " /* 기부내용 CONTENT */ ".TrimEnd()
        + " td.ContentItem{ ".TrimEnd()
        + " border-top:gray 0px dotted; ".TrimEnd()
        + " border-left:gray 0px dotted; ".TrimEnd()
        + " border-right:gray 1px solid; ".TrimEnd()
        + " border-bottom:gray 1px dotted; ".TrimEnd()
        + " } ".TrimEnd()
        + " td.ContentItemBottom{ ".TrimEnd()
        + " border-top:gray 0px dotted; ".TrimEnd()
        + " border-left:gray 0px dotted; ".TrimEnd()
        + " border-right:gray 1px solid; ".TrimEnd()
        + " border-bottom:gray 1px solid; ".TrimEnd()
        + " } ".TrimEnd()
        + "  ".TrimEnd()
        + " /* Title */ ".TrimEnd()
        + " .Title{ ".TrimEnd()
        + " font-size: 12pt; ".TrimEnd()
        + " font-weight: bold; ".TrimEnd()
        + " border-bottom: gray 1px solid; ".TrimEnd()
        + " } ".TrimEnd()
        + " /*  */ ".TrimEnd()
        + " .Gray_Table{ ".TrimEnd()
        + " border-color:gray; ".TrimEnd()
        + " border-style:solid; ".TrimEnd()
        + " border-top-width:1px; ".TrimEnd()
        + " border-left-width:2px; ".TrimEnd()
        + " border-right-width:2px; ".TrimEnd()
        + " border-bottom-width:1px; ".TrimEnd()
        + " } ".TrimEnd()
        + " .Gray_Head{ ".TrimEnd()
        + " text-align: center; ".TrimEnd()
        + " border-color:gray; ".TrimEnd()
        + " border-style:solid; ".TrimEnd()
        + " border-top-width:0px; ".TrimEnd()
        + " border-left-width:0px; ".TrimEnd()
        + " border-right-width:0px; ".TrimEnd()
        + " border-bottom-width:1px; ".TrimEnd()
        + " } ".TrimEnd()
        + " .Gray_Item{ ".TrimEnd()
        + " border-color:gray; ".TrimEnd()
        + " border-style:solid; ".TrimEnd()
        + " border-top-width:0px; ".TrimEnd()
        + " border-left-width:0px; ".TrimEnd()
        + " border-right-width:0px; ".TrimEnd()
        + " border-bottom-width:1px; ".TrimEnd()
        + " } ".TrimEnd()
        + " </style> ".TrimEnd()
        + " </head> ".TrimEnd();
        #endregion

        #endregion


        #region //-- 기부자, 기부처, 모집처 정보
        sHtml += " <body onload='setPrint()' style='text-align: left;'>	".TrimEnd()

        //+" <object id=factory style='display:none' classid='clsid:1663ed61-23eb-11d2-b92f-008048fdd814'      ".TrimEnd()
            //+" odebase='D:/Files/IEPageSetupX.cab#Version=6,1,429,14'>".TrimEnd()
            //+" </object>".TrimEnd()

        #region 엑티브엑스 잠금.
            //+ " <div> ".TrimEnd()
            ////ScriptX다운로드 경로 (프린트 설정시 필요)
            //+ " <object id=factory style='display:none' ".TrimEnd()
            //+ " classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ".TrimEnd()
            ////+ " codebase=http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,2,433,14 VIEWASTEXT> ".TrimEnd()
            //+ " codebase=../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ".TrimEnd()
            ////+ " <PARAM Name='DebugEnable' VALUE='True'> ".TrimEnd()
            ////+ " codebase='D:/Files/ScriptX.cab#Version=6,4,438,06'> ".TrimEnd()
            //+ " </object> ".TrimEnd()
            ////ScriptX다운로드 경로 끝 
            //+ " </div> ".TrimEnd()
        #endregion

        #region 엑티브엑스 잠금.
            //+ " <div> ".TrimEnd()
            ////ScriptX다운로드 경로 (프린트 설정시 필요)
            //+ " <object id=factory style='display:none' classid='clsid:1663ed61-23eb-11d2-b92f-008048fdd814' codebase='http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,429,14'> </object> ".TrimEnd()

            ////ScriptX다운로드 경로 끝 
            // + " </div> ".TrimEnd()
        #endregion

        #region 미사용
            //////IEPageSetupX다운로드 경로 (프린트 설정시 필요)
            ////+ " <OBJECT id=IEPageSetupX classid='clsid:41C5BC45-1BE8-42C5-AD9F-495D6C8D7586' codebase='./IEPageSetupX.cab#version=1,3,0,2' width=0 height=0> ".TrimEnd()
            //+ " <OBJECT id=IEPageSetupX classid='clsid:41C5BC45-1BE8-42C5-AD9F-495D6C8D7586' codebase='D:/Files/IEPageSetupX.cab#version=1,3,0,2' width=0 height=0> ".TrimEnd()
            //+ "     <param name='copyright' value='http://isulnara.com'> ".TrimEnd()
            //+ "     <div style='position:absolute;top:290;left:250;width:330;height:68;border:solid 1 #99B3A0;background:#B4D0E9;overflow:hidden;z-index:1;visibility:visible;'> ".TrimEnd()
            //+ "         <FONT style='font-family: '굴림', 'Verdana'; font-size: 9pt; font-style: normal;'> ".TrimEnd()
            //+ "         <BR>  인쇄 제어 컨트롤이 설치되지 않았습니다.<BR> ".TrimEnd()
            //+ "         <a href='D:/Files/IEPageSetupX.exe' target='_blank'><font color=red>이곳</font></a>을 클릭하여 수동으로 설치 후 다시 시도해주세요.</FONT> ".TrimEnd()
            ////+ "         <a href='./IEPageSetupX.exe' target='_blank'><font color=red>이곳</font></a>을 클릭하여 수동으로 설치 후 다시 시도해주세요.</FONT> ".TrimEnd()
            //+ "     </div>".TrimEnd()
            //+ " </OBJECT>".TrimEnd()
            //////IEPageSetupX다운로드 경로 끝 
        #endregion

        + " <table style='width:700px; text-align:center;' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()
        + " <tr> ".TrimEnd()
        + " <td> ".TrimEnd()
        + " <table style='width:680px;' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()
        + "     <tr> ".TrimEnd()
        + "         <td> ".TrimEnd()

        //+ "         <img src='https://compassion.benecorp.kr/Files/Images/compassion_logo.gif' width='200px' height='75px' alt=''/></td> ".TrimEnd()

        //수정 2013-06-25
        + "         <table width='100%' cellpadding='0' cellspacing='0' border='0'>".TrimEnd()
        + "         <tr><td align='left'><img src='../images/compassion_logo.gif' width='200px' height='75px' alt=''/></td> ".TrimEnd()
        + "             <td id='printID' align='right'><a onclick='javascript:fnPrint();' style='cursor:pointer;'><span style='font-size:16px;font-weight:bold;'><u>인쇄하기</u></span></a></td>".TrimEnd()
        + "         </tr></table></td>".TrimEnd()
        + "     </tr> ".TrimEnd()
        + "     <tr> ".TrimEnd()
        + "     <td style='text-align: center; height:50px;'> ".TrimEnd()
        + "         <span style='font-size: 20pt; font-weight:bold;'>기부금 영수증</span></td>  ".TrimEnd()
        + " </tr> ".TrimEnd()
        + " <tr> ".TrimEnd()
        + "     <td style='height:10px; text-align: right;'> ".TrimEnd();

        //+ "         일련번호 : KRPC07-1-26</td>  ".TrimEnd()  

        string syyMM = string.Empty;
        if (dsReceiptContent.Tables["tPaymentMonth"].Rows[0][0].ToString().Substring(2, 4) == dtNow.ToString("yyyy"))
        {
            syyMM = dsReceiptContent.Tables["tPaymentMonth"].Rows[0][0].ToString().Substring(2, 2) + dtNow.ToString("MM");
        }
        else
        {
            syyMM = dsReceiptContent.Tables["tPaymentMonth"].Rows[0][0].ToString().Substring(2, 2) + "12";
        }


        if (sConType == "person")
        {
            //sHtml += "     일련번호 : KRPC" + DateTime.Now.ToString("yyMM") + "-01-" + sConID + "</td> ".TrimEnd();
            sHtml += " 일련번호 : KRPC" + syyMM + "-01-" + sConID + "</td> ".TrimEnd();
        }
        else if (sConType == "company")
        {
            //sHtml += "     일련번호 : KRPC" + DateTime.Now.ToString("yyMM") + "-02-" + sConID + "</td> ".TrimEnd();
            sHtml += " 일련번호 : KRPC" + syyMM + "-02-" + sConID + "</td> ".TrimEnd();
        }


        sHtml += " </tr>  ".TrimEnd()
        + " </table> ".TrimEnd()
        + " <table style='width: 680px' cellpadding='0' cellspacing='0' border='0'>  ".TrimEnd()
        + " <tr> ".TrimEnd()
        + "     <td colspan='4' class='Title' align='left'> ".TrimEnd()
        + "         <span style='font-size: 12pt; border-bottom: gray 2px solid;'>⊙기부자</span></td> ".TrimEnd()
        + " </tr> ".TrimEnd()
        + " </table> ".TrimEnd()
        + " <table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()
        + " <tr > ".TrimEnd()
        + "     <td class='Gray_Head' style='width: 70px; height: 18px; background-color: silver;'> ".TrimEnd();


        if (sConType == "person")
        {
            sHtml += " 성명</td> ".TrimEnd();
        }
        else if (sConType == "company")
        {
            sHtml += " 법인명</td> ".TrimEnd();
        }


        sHtml += "     <td class='Gray_Item' align='left' style='width: 250px; padding-left:18px;'> ".TrimEnd()
        + "          " + sConName + "</td>".TrimEnd()
        + "     <td class='Gray_Head' style='width: 110px; background-color: silver;'> ".TrimEnd();


        if (sConType == "person")
        {
            sHtml += "     주민등록번호</td> ".TrimEnd();
        }
        else if (sConType == "company")
        {
            sHtml += "     사업자등록번호</td> ".TrimEnd();
        }


        sHtml += "     <td class='Gray_Item' style='width: 250px; text-align:center;'>&nbsp; ".TrimEnd();


        //주민번호나 사업자등록번호 공백이면 문자열을 나누지 못하여 에러가 나므로 예외처리 해주어야 함
        //2013-01-22
        if (sConType == "person")
        {
            //20101214정성영 *****수정
            //sHtml += "         " + sConSSN.Substring(0, 6) + "-" + sConSSN.Substring(6, 7) + "</td>".TrimEnd();

            //수정 2013-01-22
            if (sConSSN != "")
            {
                sHtml += "         " + sConSSN.Substring(0, 6) + "-" + sConSSN.Substring(6, 1) + "******" + "</td> ".TrimEnd();
            }
        }
        else if (sConType == "company")
        {
            //20101214정성영 *****수정

            //수정 2013-01-22
            if (sConSSN != "")
            {
                sHtml += "         " + sConSSN.Substring(0, 3) + "-" + sConSSN.Substring(3, 2) + "-" + sConSSN.Substring(5, 5) + "</td> ".TrimEnd();
            }
            //sHtml += "         " + sConSSN.Substring(0, 3) + "-" + sConSSN.Substring(3, 2) + "-" + "*****" + "</td>".TrimEnd(); ;
        }

        sHtml += " </tr> ".TrimEnd()
        + " <tr> ".TrimEnd()
        + "     <td class='Gray_Head' style='width: 70px; height: 18px; background-color: silver;'>주소</td>  ".TrimEnd()
        + "     <td class='Gray_Item' align='left' colspan='3' style='padding-left:10px;'>&nbsp; ".TrimEnd()
        + "         " + sConAddress + "</td>".TrimEnd()
        + " </tr> ".TrimEnd()
        + " </table>  ".TrimEnd()
        + " <br />  ".TrimEnd()
        + " <br />  ".TrimEnd()
        + " <table style='width: 680px' id='TABLE1' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()
        + " <tr> ".TrimEnd()
        + "     <td colspan='4' class='Title' align='left'>  ".TrimEnd()
        + "         <span style='font-size: 12pt'>⊙기부금단체</span></td> ".TrimEnd()
        + " </tr> ".TrimEnd()
        + " </table> ".TrimEnd()
        + " <table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()
        + " <tr> ".TrimEnd()
        + "     <td class='Gray_Head' style='width: 70px; height: 18px; background-color: silver;'>단체명</td> ".TrimEnd()
        + "     <td class='Gray_Item' align='left' style='width: 250px; padding-left:10px;'>&nbsp; ".TrimEnd()
        + "         " + sMyCompanyName + "</td>".TrimEnd()
        + "     <td class='Gray_Head' style='width: 110px; background-color: silver;'>사업자등록번호</td>  ".TrimEnd()
        + "     <td class='Gray_Item' style='width: 250px; text-align:center;'>&nbsp; ".TrimEnd()
        + "         " + sMyCompanySSN + "</td>".TrimEnd()
        + " </tr> ".TrimEnd()
        + " <tr> ".TrimEnd()
        + "     <td class='Gray_Head' style='width: 70px; height: 18px; background-color: silver;'>소재지</td> ".TrimEnd()
        + "     <td class='Gray_Item' align='left' colspan='3' style='padding-left:10px;'>&nbsp; ".TrimEnd()
        + "         " + sMyCompanyAddress + "</td>".TrimEnd()
        + " </tr> ".TrimEnd()
        + " </table> ".TrimEnd()
        + " <br /> ".TrimEnd()
        + " <br /> ".TrimEnd()
        + " <table style='width: 680px' cellpadding='0' cellspacing='0' border='0'>  ".TrimEnd()
        + " <tr> ".TrimEnd()
        + "     <td colspan='4' class='Title' align='left'>  ".TrimEnd()
        + "         <span style='font-size: 12pt'>⊙기부금 모집처(언론 기관 등)</span></td> ".TrimEnd()
        + " </tr> ".TrimEnd()
        + " </table> ".TrimEnd()
        + " <table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()
        + " <tr> ".TrimEnd()
        + "     <td class='Gray_Head' style='width: 70px; height: 18px; background-color: silver;'>단체명</td> ".TrimEnd()
        + "     <td class='Gray_Item' style='width: 250px; padding-left:10px;'>&nbsp; ".TrimEnd()
        + "         </td> ".TrimEnd()
        + "     <td class='Gray_Head' style='width: 110px; background-color: silver;'>사업자등록번호</td> ".TrimEnd()
        + "     <td class='Gray_Item' style='width: 250px; text-align:center;'>&nbsp; ".TrimEnd()
        + "         </td> ".TrimEnd()
        + " </tr> ".TrimEnd()
        + " <tr> ".TrimEnd()
        + "     <td class='Gray_Head' style='width: 70px; height: 18px; background-color: silver;'>소재지</td> ".TrimEnd()
        + "     <td class='Gray_Item' colspan='3' style='padding-left:10px;'>&nbsp; ".TrimEnd()
        + "         </td> ".TrimEnd()
        + " </tr> ".TrimEnd()
        + " </table> ".TrimEnd()
        + " <br /> ".TrimEnd()
        + " <br /> ".TrimEnd()
        + " <table style='width: 680px' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()
        + " <tr> ".TrimEnd()
        + "     <td colspan='6' class='Title' align='left'> ".TrimEnd()
        + "         <span style='font-size: 12pt'>⊙기부내용</span></td> ".TrimEnd()
        + " </tr> ".TrimEnd()
        + " </table> ".TrimEnd()
        + " <table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()
        + " <tr> ".TrimEnd()
        + "     <td class='ContentHeadType Gray_Item'>유형</td> ".TrimEnd()
        + "     <td class='ContentHeadCode Gray_Item'>코드</td> ".TrimEnd();


        if (sConType == "person")
        {
            sHtml += "     <td class='ContentHeadDate Gray_Item'>년월</td> ".TrimEnd();

        }
        else if (sConType == "company")
        {
            sHtml += "     <td class='ContentHeadDate Gray_Item'>년월</td> ".TrimEnd();

        }

        sHtml += "     <td class='ContentHeadDesc Gray_Item'>내용</td> ".TrimEnd()
        + "     <td class='ContentHeadAmount Gray_Item'>금액</td> ".TrimEnd()
        + "     <td rowspan='13' style='width: 280px'> ".TrimEnd();

        #endregion


        #region //-- 법률 내용
        if (sConType == "person") // person
        {
            sHtml += "         <table style='width: 280px;' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()


            + "             <tr> ".TrimEnd()

            //+ "                 <td colspan='3' class='CodeTitle'>소득세법 제 34조, 조세특례제한법 ".TrimEnd()
            //+ " 제 73조 및 동법 제 88조의 4의 규정에 의한 ".TrimEnd()
            //+ " 기부금을 이와같이 기부하였음을 증명하여 주시기 바랍니다.<br /> ".TrimEnd()

            + "                 <td colspan='3' class='CodeTitle'>「소득세법」 제34조, 「조세특례제한법」 제76조ㆍ제 ".TrimEnd()
            + "&nbsp;88조의4 및 「법인세법」 제24조에 따른 기부금을 이와 ".TrimEnd()
            + "&nbsp;같이 기부하였음을 증명하여 주시기 바랍니다 ".TrimEnd()
            + "                 </td> ".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td colspan='3' style='text-align: center; font-size:8pt;  height: 30px;'> ".TrimEnd()
            + "                     " + dtAppTime.ToString("   yyyy 년   MM 월   dd 일") + "</td>".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td style='text-align: center; width:80px; font-size:8pt;  height: 30px;'>신청인</td> ".TrimEnd()
            + "                 <td style='text-align: right; width:120px; font-size:8pt;  height: 30px;'>" + sConName + "</td> ".TrimEnd()
            + "                 <td style='text-align: right; width:80px; font-size:8pt;  height: 30px;'>(서명 또는 인)</td>".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td style='height:10px;'></td> ".TrimEnd()
            + "             </tr> ".TrimEnd()
            + "         </table> ".TrimEnd()

            + "         <table style='width: 285px;' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()
            + "             <tr> ".TrimEnd()
            + "                 <td colspan='3' class='title'><br/>※ 유형 및 코드 구분<br /> </td> ".TrimEnd()
            + "             </tr> ".TrimEnd()

            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'>1. 기부금 모집처(언론기관 등)는 방송사, 신문사, 통신회사 등 기부금을</br>&nbsp;&nbsp;&nbsp;&nbsp;대신 접수하여 기부금 단체에 전달하는 기관을 말합니다<br /></td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()

            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'>2. 기부내용란에 적는 유형ㆍ코드는 다음과 같습니다.<br /> </td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' style:'text-align:center;'> 기부금 구분".TrimEnd()
            + "                 <td class='type'> 유형".TrimEnd()
            + "                 <td class='code'> 코드".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 「소득세법」 제34조제2항, 「법인세법」 제24조제2항에 따른 기부금".TrimEnd()
            + "                 <td class='type'> 법정".TrimEnd()
            + "                 <td class='code'> 10".TrimEnd() 
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 「조세특례제한법」 제76조에 따른 기부금".TrimEnd()
            + "                 <td class='type'> 정치자금".TrimEnd()
            + "                 <td class='code'> 20".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 「소득세법」 제34조제1항(종교단체 기부금 제외), 「법인세법」 제24조제1항에 따른 기부금".TrimEnd()
            + "                 <td class='type'> 지정".TrimEnd()
            + "                 <td class='code'> 40".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 「소득세법」 제34조제1항에 따른 기부금 중 종교단체기부금".TrimEnd()
            + "                 <td class='type'> 종교단체".TrimEnd()
            + "                 <td class='code'> 41".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 「조세특례제한법」 제88조의4에 따른 기부금".TrimEnd()
            + "                 <td class='type'> 우리사주".TrimEnd()
            + "                 <td class='code'> 42".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 필요경비(손금) 및 소득공제금액대상에 해당되지 아니하는 기부금".TrimEnd()
            + "                 <td class='type'> 공제제외".TrimEnd()
            + "                 <td class='code'> 50".TrimEnd()
            + "             </tr> ".TrimEnd()

            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'> 가. 소득세법 제34조제2항에 따른 기부금 : 법정, 코드 10 </td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()

            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'> 나. 조세특례제한법 제76조에 따른 기부금 : 정치자금, 코드 20 </td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()


            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'> 다. 조세특례제한법 제73조제1항(제1호 및 제11호 제외)에 따른 기부금 </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 조특법 73, 코드 30 </td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()


            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'> 라. 조세특례제한법 제73조제1항제11호에 따른 공익법인신탁기부금 </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 조특법 73 ① 11, 코드 31 </td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()

            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'> 마. 소득세법 제34조제1항(종교단체 기부금 제외)에 따른 기부금</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 지정 ,코드 40</td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()

            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'> 바. 소득세법 제34조제1항에 따른 기부금 중 종교단체기부금</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 종교단체, 코드 41</td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()


            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'> 사. 조세특례제한법 제88조의4에 따른 기부금 : 우리사주, 코드 42 </td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()

            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'> 아. 필요경비 및 소득공제금액대상에 해당되지 아니하는 기부금)</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 공제제외, 코드 50 </td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' colspan='3' style='border-bottom:gray 1px solid'>&nbsp;".TrimEnd()
            + "             </tr> ".TrimEnd()

            //+ "             <tr> ".TrimEnd()
            //+ "                 <td style='height:30px; border-right:gray 1px solid' colspan='3'></td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()

            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'>코드 10:소득세법 제34조 제2항 기부금<br /> ".TrimEnd()
            //+ "                     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (법정기부금)</td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()
            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'>코드 30:조세특례제한법 제73조 기부금<br />".TrimEnd()
            //+ "                     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (조특법 73)</td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()
            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'>코드 40:소득세법 제34조 제1항 기부금<br /> ".TrimEnd()
            //+ "                     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (지정기부금)</td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()
            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'>코드 41:소득세법 제34조 제1항 기부금<br /> ".TrimEnd()
            //+ "                     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (종교단체기부금)</td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()
            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'>코드 42:조세특례제한법 제88조의 4 기부금<br /> ".TrimEnd()
            //+ "                     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (우리사주조합기부금)</td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()
            //+ "             <tr> ".TrimEnd()
            //+ "                 <td colspan='3' class='code'>코드 50:기타기부금</td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()
            //+ "             <tr> ".TrimEnd()
            //+ "                 <td style='height:50px;'></td> ".TrimEnd()
            //+ "             </tr> ".TrimEnd()

            + "         </table> ".TrimEnd();
        }
        else if (sConType == "company") // company
        {
            sHtml += "         <table style='width: 280px;' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()


            + "             <tr> ".TrimEnd()

            + "                 <td colspan='3' class='CodeTitle'>「소득세법」 제34조, 「조세특례제한법」 제76조ㆍ제 ".TrimEnd()
            + "&nbsp;88조의4 및 「법인세법」 제24조에 따른 기부금을 이와 ".TrimEnd()
            + "&nbsp;같이 기부하였음을 증명하여 주시기 바랍니다 ".TrimEnd()
            + "                 </td> ".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td colspan='3' style='text-align: center; font-size:8pt;  height: 30px;'> ".TrimEnd()
            + "                     " + dtAppTime.ToString("   yyyy 년   MM 월   dd 일") + "</td>".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td style='text-align: center; width:80px; font-size:8pt;  height: 30px;'>신청인</td> ".TrimEnd()
            + "                 <td style='text-align: right; width:120px; font-size:8pt;  height: 30px;'>" + sConName + "</td> ".TrimEnd()
            + "                 <td style='text-align: right; width:80px; font-size:8pt;  height: 30px;'>(서명 또는 인)</td>".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td style='height:10px;'></td> ".TrimEnd()
            + "             </tr> ".TrimEnd()
            + "         </table> ".TrimEnd()

            + "         <table style='width: 285px;' cellpadding='0' cellspacing='0' border='0'> ".TrimEnd()
            + "             <tr> ".TrimEnd()
            + "                 <td colspan='3' class='title'><br/>※ 유형 및 코드 구분<br /> </td> ".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' style:'text-align:center;'> 기부금 구분".TrimEnd()
            + "                 <td class='type'> 유형".TrimEnd()
            + "                 <td class='code'> 코드".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 「소득세법」 제34조제2항, 「법인세법」 제24조제2항에 따른 기부금".TrimEnd()
            + "                 <td class='type'> 법정".TrimEnd()
            + "                 <td class='code'> 10".TrimEnd() 
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 「조세특례제한법」 제76조에 따른 기부금".TrimEnd()
            + "                 <td class='type'> 정치자금".TrimEnd()
            + "                 <td class='code'> 20".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 「소득세법」 제34조제1항(종교단체 기부금 제외), 「법인세법」 제24조제1항에 따른 기부금".TrimEnd()
            + "                 <td class='type'> 지정".TrimEnd()
            + "                 <td class='code'> 40".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 「소득세법」 제34조제1항에 따른 기부금 중 종교단체기부금".TrimEnd()
            + "                 <td class='type'> 종교단체".TrimEnd()
            + "                 <td class='code'> 41".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 「조세특례제한법」 제88조의4에 따른 기부금".TrimEnd()
            + "                 <td class='type'> 우리사주".TrimEnd()
            + "                 <td class='code'> 42".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' align='left'> 필요경비(손금) 및 소득공제금액대상에 해당되지 아니하는 기부금".TrimEnd()
            + "                 <td class='type'> 공제제외".TrimEnd()
            + "                 <td class='code'> 50".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "             <tr> ".TrimEnd()
            + "                 <td class='div' colspan='3' style='border-bottom:gray 1px solid'>&nbsp;".TrimEnd()
            + "             </tr> ".TrimEnd()

            + "         </table> ".TrimEnd();

            //sHtml += "         <table style='width: 230px;' cellpadding='0' cellspacing='0' border='0'>				        ".TrimEnd()
            //+ "             <tr>                                                                                            ".TrimEnd()
            //+ "             <td colspan='3' class='CodeTitle'>                                                              ".TrimEnd()
            //+ "                 법인세법 제 24조, 조세특례제한법                                                            ".TrimEnd()
            //+ "                 제 73조에 따른 기부금을 이와같이                                                            ".TrimEnd()
            //+ "                 기부하였음을 증명하여 주시기 바랍니다.<br />                                                ".TrimEnd()
            //+ "             </td>                                                                                           ".TrimEnd()
            //+ "         </tr>                                                                                               ".TrimEnd()
            //+ "         <tr>                                                                                                ".TrimEnd()
            //+ "             <td colspan='3' class='code'>코드 10:법인세법 제 24조 제2항 기부금<br />                        ".TrimEnd()
            //+ "                 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (법정기부금)</td>                                        ".TrimEnd()
            //+ "         </tr>                                                                                               ".TrimEnd()
            //+ "         <tr>                                                                                                ".TrimEnd()
            //+ "             <td colspan='3' class='code'>코드 30:조세특례제한법 제73조 기부금<br />                         ".TrimEnd()
            //+ "                 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (조특법 73)</td>                                         ".TrimEnd()
            //+ "         </tr>                                                                                               ".TrimEnd()
            //+ "         <tr>                                                                                                ".TrimEnd()
            //+ "             <td colspan='3' class='code'>코드 40:법인세법 제24조 제1항 기부금<br />                         ".TrimEnd()
            //+ "                 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (지정기부금)</td>                                        ".TrimEnd()
            //+ "         </tr>                                                                                               ".TrimEnd()
            //+ "         <tr>                                                                                                ".TrimEnd()
            //+ "             <td colspan='3' class='code'>코드 50:기타기부금<br /><br />                                     ".TrimEnd()
            //+ "             </td>                                                                                           ".TrimEnd()
            //+ "         </tr>                                                                                               ".TrimEnd()
            //+ "             <tr>                                                                                            ".TrimEnd()
            //+ "                 <td style='height:50px;'></td>                                                              ".TrimEnd()
            //+ "             </tr>                                                                                           ".TrimEnd()
            //+ "             <tr>                                                                                            ".TrimEnd()
            //+ "                 <td style='height:50px;'></td>                                                              ".TrimEnd()
            //+ "             </tr>                                                                                           ".TrimEnd()
            //+ "             <tr>                                                                                            ".TrimEnd()
            //+ "                 <td colspan='3' style='text-align: center'>                                                 ".TrimEnd()
            //+ "                     " + dtAppTime.ToString("   yyyy 년   MM 월   dd 일") + "</td>".TrimEnd()
            //+ "             </tr>                                                                                           ".TrimEnd()
            //+ "             <tr>                                                                                            ".TrimEnd()
            //+ "                 <td style='text-align: center; width:50px;'>신청인</td> ".TrimEnd()
            //+ "                 <td style='text-align: right; width:130px;'>" + sConName + "</td> ".TrimEnd()
            //+ "                 <td style='text-align: right; width:50px;'>(인)</td> ".TrimEnd()
            //+ "             </tr>                                                                                           ".TrimEnd()
            //+ "         </table>                                                                                            ".TrimEnd();


            //문구수정 20101221 정성영 (정수진 법인 개인 동일하다고함)

            //sHtml += "         <table style='width: 275px;' cellpadding='0' cellspacing='0' border='0'>                                                   ".TrimEnd()
            //+ "             <tr>                                                                                                                          ".TrimEnd()
            //+ "                 <td colspan='3' class='CodeTitle'>                                                                                        ".TrimEnd()
            //+ "                 법인세법 제24조, 조세특례제한법 제73조에 따른<br />                                                                            ".TrimEnd()
            //+ "                 기부금을 이와같이 기부하였음을 증명하여 주시기<br />                                                                            ".TrimEnd()
            //+ "                 바랍니다.<br />                                                                                                           ".TrimEnd()
            //+ "                 </td>                                                                                                                     ".TrimEnd()
            //+ "             </tr>                                                                                                                         ".TrimEnd()
            //+ "             <tr>                                                                                                                          ".TrimEnd()
            //+ "                 <td colspan='3' style='text-align: center; font-size:8pt;'>                                                               ".TrimEnd()
            //+ "                     " + dtAppTime.ToString("   yyyy 년   MM 월   dd 일") + "</td>".TrimEnd()
            //+ "             </tr>                                                                                                                         ".TrimEnd()
            //+ "             <tr>                                                                                                                          ".TrimEnd()
            //+ "                 <td style='text-align: right; width:65px; font-size:8pt;  height: 30px;'>신청인</td> ".TrimEnd()
            //+ "                 <td style='text-align: right; width:130px; font-size:8pt;  height: 30px;'>" + sConName + "</td> ".TrimEnd()
            //+ "                 <td style='text-align: right; width:80px; font-size:8pt;  height: 30px;'>(서명 또는 인) </td> ".TrimEnd()
            //+ "             </tr>                                                                                                                         ".TrimEnd()
            //+ "             <tr>                                                                                                                          ".TrimEnd()
            //+ "                 <td colspan='3' class='pCode'><br/>※ 작성방법<br /> </td>                                                                      ".TrimEnd()
            //+ "             </tr>                                                                                                                         ".TrimEnd()
            //+ "             <tr>                                                                                                                          ".TrimEnd()
            //+ "                 <td colspan='3' class='pCode'>1. 유형 및 코드는 다음 구분에 따라 기재합니다.<br /></td>                                                ".TrimEnd()
            //+ "             </tr>                                                                                                                         ".TrimEnd()
            //+ "             <tr>                                                                                                                          ".TrimEnd()
            //+ "                 <td colspan='3' class='pCode'> 가. 법인세법 제24조제2항에 따른 기부금(유형 1, 코드 10)<br /></td>                          ".TrimEnd()
            //+ "             </tr>                                                                                                                         ".TrimEnd()
            //+ "             <tr>                                                                                                                          ".TrimEnd()
            //+ "                 <td colspan='3' class='pCode'> 나. 조세특례제한법 제73조제1항에 따른 기부금(유형 2, 코드 30)<br /></td>                     ".TrimEnd()
            //+ "             </tr>                                                                                                                         ".TrimEnd()
            //+ "             <tr>                                                                                                                          ".TrimEnd()
            //+ "                 <td colspan='3' class='pCode'> 다. 법인세법 제24조제1항에 따른 기부금(유형 5, 코드 40)<br /></td>                          ".TrimEnd()
            //+ "             </tr>                                                                                                                         ".TrimEnd()
            //+ "             <tr>                                                                                                                          ".TrimEnd()
            //+ "                 <td colspan='3' class='pCode'> 라. 그 밖의 기부금(유형 6, 코드 50)<br /></td>                          ".TrimEnd()
            //+ "             </tr>                                                                                                                         ".TrimEnd()
            //+ "             <tr>                                                                                                                          ".TrimEnd()
            //+ "                 <td style='height:180px;'></td>                                                                                            ".TrimEnd()
            //+ "             </tr>                                                                                                                         ".TrimEnd()
            //+ "         </table>                                                                                                                          ".TrimEnd();
        }
        #endregion


        #region //-- 기부 내용
        sHtml += "     </td> ".TrimEnd()
        + " </tr> ".TrimEnd();
        for (int i = 0; i <= 11; i++)
        {
            if (i != 12)
            {
                //2008.09.22 수정전
                //if (dsReceiptContent.Tables["FundInfo"].Rows.Count >= i + 1)
                //{
                //    Int64 iAmount = 0;
                //    iAmount = Convert.ToInt64(dsReceiptContent.Tables["FundInfo"].Rows[i][1]);

                //    sHtml += " <tr>"
                //    + "     <td class='ContentItem' style='width:96px; height:25px; text-align:center;'>"
                //    //+ dsReceiptContent.Tables["FundInfo"].Rows[i]["Type"].ToString() + "</td>"
                //    + "지정기부금" + "</td>"
                //    + "     <td class='ContentItem' style='width:40px; text-align:right;'>"
                //    //+ dsReceiptContent.Tables["FundInfo"].Rows[i]["Code"].ToString() + "</td>"
                //    + 40 + "</td>"
                //    + "     <td class='ContentItem' style='width:100px; text-align:right;'>"
                //    + dsReceiptContent.Tables["FundInfo"].Rows[i][0].ToString().Substring(0, 4) + "년"
                //    + dsReceiptContent.Tables["FundInfo"].Rows[i][0].ToString().Substring(5, 2) + "월" 
                //    + "</td>"
                //    + "     <td class='ContentItem' style='width:100px; text-align:right;'>"
                //    //+ dsReceiptContent.Tables["FundInfo"].Rows[i]["Desc"].ToString() + "</td>"
                //    + "&nbsp;" + "</td>"
                //    + "     <td class='ContentItem' style='width:93px; text-align:right;'>"
                //    + iAmount.ToString("###,#") + "원</td>"
                //    + " </tr>";

                //    iTotalAmount += iAmount;
                //}
                //2008.09.22 김보영 수정
                if (dsReceiptContent.Tables["tPaymentMonth"].Rows.Count >= i + 1)
                {
                    Int64 iAmount = 0;
                    iAmount = Convert.ToInt64(dsReceiptContent.Tables["tPaymentMonth"].Rows[i][1]);

                    if (iAmount.ToString() != "0")
                    {
                        sHtml += " <tr>"
                        + "     <td class='ContentItem' style='width:90px; height:25px; text-align:center;'>"
                            //+ dsReceiptContent.Tables["FundInfo"].Rows[i]["Type"].ToString() + "</td>"
                        + "지정기부금" + "</td>"
                        + "     <td class='ContentItem' style='width:45px; text-align:center;'>"
                            //+ dsReceiptContent.Tables["FundInfo"].Rows[i]["Code"].ToString() + "</td>"
                        + 40 + "</td>"
                        + "     <td class='ContentItem' style='width:90px; text-align:center;'>"
                        + dsReceiptContent.Tables["tPaymentMonth"].Rows[i][0].ToString().Substring(0, 4) + "년"
                        + dsReceiptContent.Tables["tPaymentMonth"].Rows[i][0].ToString().Substring(5, 2) + "월"
                        + "</td>"
                        + "     <td class='ContentItem' style='width:80px; text-align:center;'>"
                            //+ dsReceiptContent.Tables["FundInfo"].Rows[i]["Desc"].ToString() + "</td>"
                        + "후원금" + "</td>"
                        + "     <td class='ContentItem' style='width:100px; text-align:right;'>"
                        + iAmount.ToString("###,#") + "원</td>"
                        + " </tr>";

                        iTotalAmount += iAmount;
                    }
                    else
                    {
                        sHtml += " <tr>"
                        + "     <td class='ContentItem' style='width:90px; height:25px; text-align:center;'>"
                            //+ dsReceiptContent.Tables["FundInfo"].Rows[i]["Type"].ToString() + "</td>"
                        + "지정기부금" + "</td>"
                        + "     <td class='ContentItem' style='width:45px; text-align:right;'>"
                            //+ dsReceiptContent.Tables["FundInfo"].Rows[i]["Code"].ToString() + "</td>"
                        + 40 + "</td>"
                        + "     <td class='ContentItem' style='width:90px; text-align:right;'>"
                        + dsReceiptContent.Tables["tPaymentMonth"].Rows[i][0].ToString().Substring(0, 4) + "년"
                        + dsReceiptContent.Tables["tPaymentMonth"].Rows[i][0].ToString().Substring(5, 2) + "월"
                        + "</td>"
                        + "     <td class='ContentItem' style='width:80px; text-align:center;'>"
                            //+ dsReceiptContent.Tables["FundInfo"].Rows[i]["Desc"].ToString() + "</td>"
                        + "후원금" + "</td>"
                        + "     <td class='ContentItem' style='width:100px; text-align:right;'>"
                        + iAmount.ToString() + "원</td>"
                        + " </tr>";

                        iTotalAmount += iAmount;
                    }
                }
                else
                {
                    sHtml += "<tr>"
                    + "    <td class='ContentItem' style='width:90px; height:25px; text-align:center;'>&nbsp</td>"
                    + "    <td class='ContentItem' style='width:45px; text-align:right;'>&nbsp</td>"
                    + "    <td class='ContentItem' style='width:90px; text-align:right;'>&nbsp</td>"
                    + "    <td class='ContentItem' style='width:80px; text-align:right;'>&nbsp;</td>"
                    + "    <td class='ContentItem' style='width:100px; text-align:right;'>&nbsp</td>"
                    + "</tr>";
                }

            }
            else
            {
                if (dsReceiptContent.Tables["tPaymentMonth"].Rows.Count < i + 1)
                {
                    sHtml += " <tr>"
                    + "     <td class='ContentItemBottom' style='width:90px; height:25px; text-align:center;'>&nbsp;</td>"
                    + "     <td class='ContentItemBottom' style='width:45px; text-align:right;'>&nbsp;</td>"
                    + "     <td class='ContentItemBottom' style='width:90px; text-align:right;'>&nbsp;</td>"
                    + "     <td class='ContentItemBottom' style='width:80px; text-align:right;'>&nbsp;</td>"
                    + "     <td class='ContentItemBottom' style='width:100px; text-align:right;'>&nbsp;</td>"
                    + " </tr>";
                }
                else
                {
                    sHtml += "<tr>"
                    + "    <td class='ContentItemBottom' style='width:90px; height:25px; text-align:center;'>&nbsp</td>"
                    + "    <td class='ContentItemBottom' style='width:45px; text-align:right;'>&nbsp</td>"
                    + "    <td class='ContentItemBottom' style='width:90px; text-align:right;'>&nbsp</td>"
                    + "    <td class='ContentItemBottom' style='width:80px; text-align:right;'>&nbsp;</td>"
                    + "    <td class='ContentItemBottom' style='width:100px; text-align:right;'>&nbsp</td>"
                    + "</tr>";
                }
            }

        }
        #endregion


        #region //-- 하단 내용
        // 2013-11-29 수정 (유원준)
        sHtml += " <tr> ".TrimEnd()
        + "     <td style='text-align: right; height:30px;' colspan='3'> ".TrimEnd()
        + "     기부하신 금액은 총 ".TrimEnd()
        + "     </td> ".TrimEnd()

        + "     <td class='ContentTotalSum' style='text-align:center; background-color:Silver;' colspan='2'> ".TrimEnd()
        + "         " + iTotalAmount.ToString("#,###").TrimEnd()
        + "     </td> ".TrimEnd()

        + "     <td style='text-align: left; height:30px;'> ".TrimEnd()
        + "     원 입니다.".TrimEnd()
        + "     </td> ".TrimEnd()

        //+ "     <td style='text-align: right; height:30px;' colspan='4'> ".TrimEnd()
        //+ "         합 계&nbsp;&nbsp;</td> ".TrimEnd()
        //+ "     <td class='ContentTotalSum' style='text-align:right; background-color:Silver;'> ".TrimEnd()
        //+ "         " + iTotalAmount.ToString("#,###") + "원&nbsp;".TrimEnd()
        //+ "     </td> ".TrimEnd()
        //+ "     <td style='text-align: right; height:30px;'>  11111  </td> ".TrimEnd()
        + " </tr> ".TrimEnd()
        + " </table> ".TrimEnd()
        //+ " <tr> ".TrimEnd()
        //+ "     <td colspan='3' style='text-align: center; height: 20px; font-size: 12pt; font-weight:bold; border:1px solid gray margin-left:10px;'> ".TrimEnd()
        //+ "         이와 같이 기부금을 기부받았음을 증명합니다.</td> ".TrimEnd()
        //+ " </tr> ".TrimEnd()

        + " <br /> ".TrimEnd()
            //+ " <table style='width: 680px; background: url(https://compassion.benecorp.kr/Files/Images/stamp2.gif) fixed no-repeat right top;'            ".TrimEnd()

        //+ " <table style='width: 680px; background: url(../images/stamp2.gif) fixed no-repeat right top;'            ".TrimEnd()
            //+ "         cellpadding='0' cellspacing='0' border='0'>  ".TrimEnd()

        + " <tr> ".TrimEnd()
        + "     <td colspan='3' style='text-align: center; height: 20px; font-size: 12pt; font-weight:bold;'> ".TrimEnd()
        + "         이와 같이 기부금을 기부받았음을 증명합니다.</td> ".TrimEnd()
        + " </tr> ".TrimEnd()

        + "             <tr> ".TrimEnd()
        + "                 <td colspan='3' style='text-align: right; font-size:8pt;  height: 30px;'> ".TrimEnd()
        + "                     " + dtAppTime.ToString("   yyyy 년   MM 월   dd 일") + "</td>".TrimEnd()
        + "             </tr> ".TrimEnd()

        + " <tr> ".TrimEnd()
        + "     <td td align = 'right'><img src='../images/stamp3.gif' width='529px' height='78px' alt=''/> ".TrimEnd()
        + " </tr> ".TrimEnd()

        //+ " <tr> ".TrimEnd()
        //+ "     <td colspan='3' style='height: 30px; text-align: right'> ".TrimEnd()
        //+ "         " + DateTime.Now.ToString("   yyyy 년   MM 월   dd 일") + "</td>".TrimEnd()
        //+ "         " + dtNow.ToString("   yyyy 년   MM 월   dd 일") + "</td>".TrimEnd()
        + " </tr> ".TrimEnd()

        + " <tr><td style='height:10px;'></td></tr> ".TrimEnd()     //수정 2013-06-25
        + " <tr> ".TrimEnd()
        + "<table style='font-family: 굴림'>".TrimEnd()
        + "<tr>".TrimEnd()
        + "<td height='10'><span style='font-size: 9pt;'><font color='#0033FF'>&nbsp;&nbsp;&nbsp;한국컴패션</font> (140-894) 서울시 용산구 한남동 723-41 석전빌딩</span></td>".TrimEnd()
        + "</tr>".TrimEnd()
        + "<tr>".TrimEnd()
        + "<td height='10'><span style='font-size: 9pt;'><font color='#0033FF'>&nbsp;&nbsp;&nbsp;후원상담/안내 :</font> 02 3668 3400 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;팩스 :</font> 02 3668 3456 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;이메일 :</font> info@compassion.or.kr &nbsp;&nbsp;&nbsp;<font color='#0033FF'>www.</font>compassion.or.kr</span></td>".TrimEnd()
        + "</tr>".TrimEnd()
        // + "<tr>".TrimEnd()
        // + "<td>&nbsp;</td>".TrimEnd()
        // + "</tr>".TrimEnd()
        + "<tr>".TrimEnd()
        + "<td height='10'><span style='font-size: 9pt;'><font color='#0033FF'>&nbsp;&nbsp;&nbsp;COMPASSION KOREA U.S. BRANCH&nbsp;&nbsp;</font> 14515 Valley View Ave. Ste. #C Santa Fe Springs, CA90670</span></td>".TrimEnd()
        + "</tr>".TrimEnd()
        + "<tr>".TrimEnd()
        + "<td height='10'><span style='font-size: 9pt;'><font color='#0033FF'>&nbsp;&nbsp;&nbsp;TEL :</font> 562 483 4300 &nbsp;&nbsp;&nbsp;<font color='#0033FF'>FAX :</font> 562 407 2112 &nbsp;&nbsp;&nbsp;<font color='#0033FF'>EMAIL :</font> info_us@compassion.or.kr</span></td>"
        + "</tr>".TrimEnd()
        + "</table>".TrimEnd()
        //+ "     <td colspan='3' style='height: 30px; text-align: left'> ".TrimEnd()
        //+ "         <strong>Compassion Korea</strong> | <strong>한국컴패션</strong> 서울시 용산구 한남동 723-41 석전빌딩 (140-894)<br> ".TrimEnd()
        //+ "         TEL: 02-740-1000 &nbsp; FAX: 02-740-1001 &nbsp; EMAIL: info@compassion.or.kr</td> ".TrimEnd()
        + " </tr> ".TrimEnd()
        + " </table> ".TrimEnd()
        + " </td> ".TrimEnd()
        + " </tr> ".TrimEnd()
        + " </table> ".TrimEnd()

        //+ " <div style='left: 530px; width: 150px; position: absolute; top: 870px; height: 100px'> " + dtNow.ToString("   yyyy 년   MM 월   dd 일") + " </div> ".TrimEnd()

        + " </body> ".TrimEnd()
        + " </html> ".TrimEnd();

        #endregion

        return sHtml;
    }
}
