﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// CartCICSVGIFT의 요약 설명입니다.
/// </summary>
public class CartCICSVGIFT
{
	public CartCICSVGIFT()
	{
		//
		// TODO: 여기에 생성자 논리를 추가합니다.
		//
	}

    public static void SaveRelationCookie(string groupID, string codeID, string gubunID,string campaignID, string codeName, string frequency, decimal money)
    {
        WWWService.Service service = new WWWService.Service();

        if (UserInfo.IsLogin)
        {
            UserInfo userInfo = new UserInfo();
            int ret = service.InsertSponCart(userInfo.UserId, groupID, codeID, gubunID, campaignID,codeName, frequency, money);
        }
        else
        {
            if (HttpContext.Current.Request.Cookies["CooBasket"] == null)
            {
                WWWService.Service wwwService = new WWWService.Service();
                DateTime dtNow = wwwService.GetDate();

                HttpContext.Current.Response.Cookies["CooBasket"].Value = dtNow.ToString("yyyyMMddHHmmssff2");
                HttpContext.Current.Response.Cookies["CooBasket"].Expires = dtNow.AddHours(24);
            }

            int ret = service.InsertSponCart(HttpContext.Current.Request.Cookies["CooBasket"].Value, groupID, codeID, gubunID,campaignID, codeName, frequency, money);
        }        
    }

    public static void DelRelationCookie(string keyID, string groupID, string codeID, string gubunID)
    {
        WWWService.Service service = new WWWService.Service();

        if (UserInfo.IsLogin)
        {
            UserInfo userInfo = new UserInfo();
            int ret = service.SponCartCancel(keyID, groupID, codeID, gubunID);
        }
        else
        {
            int ret = service.SponCartCancel(keyID, groupID, codeID, gubunID);
        }
    }
}