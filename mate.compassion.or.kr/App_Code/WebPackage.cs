﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Web.SessionState;

using System.Security.Cryptography;
using System.Net.Mail;

namespace Benecorp.Web
{
    /// <summary>
    /// WebValidation에 대한 요약 설명입니다.
    /// </summary>
    public class WebValidation
    {
        Page page;

        public WebValidation(Page page)
        {
            //
            // TODO: 여기에 생성자 논리를 추가합니다.
            //

            this.page = page;
        }

        public static void NonMouseRight(TextBox textBox)
        {
            textBox.Attributes.Add("oncontextmenu", "return false;");
        }

        public static void NonPaste(TextBox textBox)
        {
            textBox.Attributes.Add("onkeydown", "if (event.keyCode == 86 && event.ctrlKey) { event.keyCode = 0; event.cancelBubble = true; } ");
        }

        public static void NonIME(TextBox textBox)
        {
            textBox.Attributes.CssStyle.Add("IME-MODE", "disabled");
        }

        /// <summary>
        /// TextBox 컨트롤의 입력값이 숫자만 허용된다. NonPaste와 NonIME도 같이 적용된다.
        /// </summary>
        /// <param name="textBox">TextBox 컨트롤</param>
        public static void OnlyNumberWithNonPaste(TextBox textBox)
        {
            textBox.Attributes.Add("onkeypress", "if((event.keyCode<48)||(event.keyCode>57)) event.returnValue=false; ");
            NonIME(textBox);
            NonPaste(textBox);
            NonMouseRight(textBox);
        }

        /// <summary>
        /// TextBox 컨트롤의 입력값이 숫자만 허용된다. Paste로 한글과 영어를 붙여넣을수 있으니 주의!
        /// </summary>
        /// <param name="textBox">TextBox 컨트롤</param>
        public static void OnlyNumber(TextBox textBox)
        {
            textBox.Attributes.Add("onkeypress", "if((event.keyCode<48)||(event.keyCode>57)) event.returnValue=false; ");
            NonIME(textBox);
        }

        /// <summary>
        /// 한글을 제외한 숫자와 영문 입력이 허용된다. NonPaste와 NonIME도 같이 적용된다.
        /// </summary>
        /// <param name="textBox">TextBox 컨트롤</param>
        public static void NonHangulWithNonPaste(TextBox textBox)
        {
            NonIME(textBox);
            NonPaste(textBox);
            NonMouseRight(textBox);
        }

        /// <summary>
        /// 현재 서버 날짜
        /// </summary>
        /// <returns></returns>
        public static DateTime GetDate()
        {
            //SqlService DataService = new SqlService();
            //string sSQL = string.Empty;
            //sSQL = "SELECT GETDATE() AS NowTime ";

            //DateTime NowTime = (DateTime)DataService.ExecuteSqlScalr(sSQL.ToString());

            //return NowTime;

            return DateTime.Now;  

        }
    }



    public class JavaScript
    {
        public JavaScript()
        {
        }

        /// <summary>
        /// 자바 스크립트 시작구문인 script language="javascript" type="text/javascript" 문자열을 얻습니다.
        /// </summary>
        public static string HeaderScript
        {
            get { return @"<script language=""javascript"" type=""text/javascript"">"; }
        }

        public static string FooterScript
        {
            get { return "</script>"; }
        }

        public static string GetNewWindowScript(string PageUrl, string PageName, int Width, int Height, bool IsScroll)
        {
            StringBuilder jscript = new StringBuilder();

            jscript.Append("function NewWindow(mypage,myname,w,h,scroll) ");
            jscript.Append("{ ");
            jscript.Append("LeftPosition = (screen.width) ? (screen.width-w)/2 : 0; ");
            jscript.Append("TopPosition = (screen.height) ? (screen.height-h)/2 : 0; ");
            jscript.Append("settings = 'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll; ");
            jscript.Append("window.open(mypage,myname,settings).focus(); ");
            jscript.Append("}  ");
            jscript.Append("NewWindow('" + PageUrl + "', '" + PageName + "', " + Width + ", " + Height + ", '" + (IsScroll ? "yes" : "no") + "');");

            return jscript.ToString();
        }

        public static string GetFormCloseScript()
        {
            return "self.close();";
        }

        public static string GetAlertScript(string Msg)
        {
            return "alert('" + Msg + "');";
        }

        public static string GetHistoryBackScript()
        {
            return "history.back();";
        }

        public static String GetPageMoveScript(string Location)
        {
            return @"document.location.href= """ + Location + @""";";
        }

        public static String GetOpenerPageMoveScript(string Location)
        {
            return @"opener.location.href= """ + Location + @""";";
        }

        public static string GetNewWindowScript(string p, int p_2, int p_3, bool p_4)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }


    #region 전화번호 자르기
    public class TelNumSubString
    {
        public TelNumSubString()
        {
        }

        //=======================================================================================================
        // 전화번호 일정 크기에 맞게 잘라서 TextBox에 넣기
        //=======================================================================================================
        public static void TelSubstringText(string sNumber, TextBox txt1, TextBox txt2, TextBox txt3)
        {

            if (sNumber == "" || sNumber == "&nbsp;")
            {
            }
            else
            {
                if (sNumber.Length <= 3)
                {
                    txt1.Text = sNumber;
                }
                else
                {
                    try
                    {
                        //----- 서울일경우 (앞자리가 2자리)
                        if (sNumber.Substring(0, 2) == "02")
                        {
                            txt1.Text = sNumber.Substring(0, 2);
                            if (sNumber.Length <= 9)
                            {
                                txt2.Text = sNumber.Substring(2, 3);
                                txt3.Text += sNumber.Substring(5);
                            }
                            else
                            {
                                txt2.Text += sNumber.Substring(2, 4);
                                txt3.Text += sNumber.Substring(6);
                            }
                        }
                        //----- 광고전화일경우 (앞자리가 4자리일경우)
                        else if (sNumber.Substring(0, 2) == "15" || sNumber.Substring(0, 2) == "16")
                        {
                            if (sNumber.Length > 4)
                            {
                                txt1.Text = sNumber.Substring(0, 4);
                                txt2.Text += sNumber.Substring(4);

                            }
                            else
                            {
                                txt1.Text = sNumber;
                            }
                        }
                        //----- 그외
                        else
                        {
                            if (sNumber.Length == 8)
                            {
                                txt2.Text = sNumber.Substring(0, 4);
                                txt3.Text += sNumber.Substring(4);
                            }
                            else if (sNumber.Length == 7)
                            {
                                txt2.Text = sNumber.Substring(0, 3);
                                txt3.Text += sNumber.Substring(3);
                            }
                            else
                            {
                                txt1.Text = sNumber.Substring(0, 3);

                                if (sNumber.Length < 7)
                                {
                                    txt2.Text += sNumber.Substring(3);
                                }
                                else if (sNumber.Length == 7 || sNumber.Length < 11)
                                {
                                    txt2.Text += sNumber.Substring(3, 3);
                                    txt3.Text += sNumber.Substring(6);
                                }
                                else if (sNumber.Length == 11 || sNumber.Length < 13)
                                {
                                    txt2.Text += sNumber.Substring(3, 4);
                                    txt3.Text += sNumber.Substring(7);
                                }
                                else
                                {

                                    //string sTemp = sNumber.Substring(3);

                                    //if (sTemp.Substring(0, 2) == "02")
                                    //{
                                    //    sReturnNumber += sTemp.Substring(0, 2) + sHyphen;
                                    //    if (sTemp.Length <= 9)
                                    //    {
                                    //        sReturnNumber += sTemp.Substring(2, 3) + sHyphen;
                                    //        sReturnNumber += sTemp.Substring(5);
                                    //    }
                                    //    else
                                    //    {
                                    //        sReturnNumber += sTemp.Substring(2, 4) + sHyphen;
                                    //        sReturnNumber += sTemp.Substring(6);
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    if (sTemp.Length <= 12)
                                    //    {
                                    //        sReturnNumber += sTemp.Substring(0, 4) + sHyphen;
                                    //        sReturnNumber += sTemp.Substring(4, 4) + sHyphen;
                                    //        sReturnNumber += sTemp.Substring(8);

                                    //    }
                                    //    else
                                    //    {
                                    //        sReturnNumber += sTemp.Substring(0, 3) + sHyphen;

                                    //        if (sTemp.Length == 10)
                                    //        {
                                    //            sReturnNumber += sTemp.Substring(3, 3) + sHyphen;
                                    //            sReturnNumber += sTemp.Substring(6);
                                    //        }
                                    //        else if (sTemp.Length > 10)
                                    //        {
                                    //            sReturnNumber += sTemp.Substring(3, 4) + sHyphen;
                                    //            sReturnNumber += sTemp.Substring(7);
                                    //        }
                                    //        else
                                    //        {
                                    //            sReturnNumber += sTemp.Substring(3);
                                    //        }
                                    //    }
                                    //}
                                }
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        public static void TelSubstringText2(string sNumber, DropDownList txt1, TextBox txt2, TextBox txt3)
        {

            if (sNumber == "" || sNumber == "&nbsp;")
            {
            }
            else
            {
                if (sNumber.Length <= 3)
                {
                    txt1.Text = sNumber;
                }
                else
                {
                    try
                    {
                        //----- 서울일경우 (앞자리가 2자리)
                        if (sNumber.Substring(0, 2) == "02")
                        {
                            txt1.Text = sNumber.Substring(0, 2);
                            if (sNumber.Length <= 9)
                            {
                                txt2.Text = sNumber.Substring(2, 3);
                                txt3.Text += sNumber.Substring(5);
                            }
                            else
                            {
                                txt2.Text += sNumber.Substring(2, 4);
                                txt3.Text += sNumber.Substring(6);
                            }
                        }
                        //----- 광고전화일경우 (앞자리가 4자리일경우)
                        else if (sNumber.Substring(0, 2) == "15" || sNumber.Substring(0, 2) == "16")
                        {
                            if (sNumber.Length > 4)
                            {
                                txt1.Text = sNumber.Substring(0, 4);
                                txt2.Text += sNumber.Substring(4);

                            }
                            else
                            {
                                txt1.Text = sNumber;
                            }
                        }
                        //----- 그외
                        else
                        {
                            if (sNumber.Length == 8)
                            {
                                txt2.Text = sNumber.Substring(0, 4);
                                txt3.Text += sNumber.Substring(4);
                            }
                            else if (sNumber.Length == 7)
                            {
                                txt2.Text = sNumber.Substring(0, 3);
                                txt3.Text += sNumber.Substring(3);
                            }
                            else
                            {
                                txt1.Text = sNumber.Substring(0, 3);

                                if (sNumber.Length < 7)
                                {
                                    txt2.Text += sNumber.Substring(3);
                                }
                                else if (sNumber.Length == 7 || sNumber.Length < 11)
                                {
                                    txt2.Text += sNumber.Substring(3, 3);
                                    txt3.Text += sNumber.Substring(6);
                                }
                                else if (sNumber.Length == 11 || sNumber.Length < 13)
                                {
                                    txt2.Text += sNumber.Substring(3, 4);
                                    txt3.Text += sNumber.Substring(7);
                                }
                                else
                                {

                                    //string sTemp = sNumber.Substring(3);

                                    //if (sTemp.Substring(0, 2) == "02")
                                    //{
                                    //    sReturnNumber += sTemp.Substring(0, 2) + sHyphen;
                                    //    if (sTemp.Length <= 9)
                                    //    {
                                    //        sReturnNumber += sTemp.Substring(2, 3) + sHyphen;
                                    //        sReturnNumber += sTemp.Substring(5);
                                    //    }
                                    //    else
                                    //    {
                                    //        sReturnNumber += sTemp.Substring(2, 4) + sHyphen;
                                    //        sReturnNumber += sTemp.Substring(6);
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    if (sTemp.Length <= 12)
                                    //    {
                                    //        sReturnNumber += sTemp.Substring(0, 4) + sHyphen;
                                    //        sReturnNumber += sTemp.Substring(4, 4) + sHyphen;
                                    //        sReturnNumber += sTemp.Substring(8);

                                    //    }
                                    //    else
                                    //    {
                                    //        sReturnNumber += sTemp.Substring(0, 3) + sHyphen;

                                    //        if (sTemp.Length == 10)
                                    //        {
                                    //            sReturnNumber += sTemp.Substring(3, 3) + sHyphen;
                                    //            sReturnNumber += sTemp.Substring(6);
                                    //        }
                                    //        else if (sTemp.Length > 10)
                                    //        {
                                    //            sReturnNumber += sTemp.Substring(3, 4) + sHyphen;
                                    //            sReturnNumber += sTemp.Substring(7);
                                    //        }
                                    //        else
                                    //        {
                                    //            sReturnNumber += sTemp.Substring(3);
                                    //        }
                                    //    }
                                    //}
                                }
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        public static string TelSubstring(string Number)
        {
            //---- Define -----------------------------------------------------
            string sNumber = Number.Trim();
            string sReturnNumber = "";
            string sHyphen = "-";									//- 번호 사이에 넣어줄값.
            //----- Validation Check ------------------------------------------

            //----- Do something ----------------------------------------------
            //- 받아온 번호의 값이 공백과 같지 않다면 
            if (sNumber == "" || sNumber == "&nbsp;")
            {
                sReturnNumber = "";
            }
            else
            {
                if (sNumber.Length <= 3)
                {
                    sReturnNumber = sNumber;
                }
                else
                {
                    try
                    {
                        //----- 서울일경우 (앞자리가 2자리)
                        if (sNumber.Substring(0, 2) == "02")
                        {
                            sReturnNumber = sNumber.Substring(0, 2) + sHyphen;
                            if (sNumber.Length <= 9)
                            {
                                sReturnNumber += sNumber.Substring(2, 3) + sHyphen;
                                sReturnNumber += sNumber.Substring(5);
                            }
                            else
                            {
                                sReturnNumber += sNumber.Substring(2, 4) + sHyphen;
                                sReturnNumber += sNumber.Substring(6);
                            }
                        }
                        //----- 광고전화일경우 (앞자리가 4자리일경우)
                        else if (sNumber.Substring(0, 2) == "15" || sNumber.Substring(0, 2) == "16")
                        {
                            if (sNumber.Length > 4)
                            {
                                sReturnNumber = sNumber.Substring(0, 4) + sHyphen;
                                sReturnNumber += sNumber.Substring(4);

                            }
                            else
                            {
                                sReturnNumber = sNumber;
                            }
                        }
                        //----- 그외
                        else
                        {
                            if (sNumber.Length == 8)
                            {
                                sReturnNumber = sNumber.Substring(0, 4) + sHyphen;
                                sReturnNumber += sNumber.Substring(4);
                            }
                            else if (sNumber.Length == 7)
                            {
                                sReturnNumber = sNumber.Substring(0, 3) + sHyphen;
                                sReturnNumber += sNumber.Substring(3);
                            }
                            else
                            {
                                sReturnNumber = sNumber.Substring(0, 3) + sHyphen;

                                if (sNumber.Length < 7)
                                {
                                    sReturnNumber += sNumber.Substring(3);
                                }
                                else if (sNumber.Length == 7 || sNumber.Length < 11)
                                {
                                    sReturnNumber += sNumber.Substring(3, 3) + sHyphen;
                                    sReturnNumber += sNumber.Substring(6);
                                }
                                else if (sNumber.Length == 11 || sNumber.Length < 13)
                                {
                                    sReturnNumber += sNumber.Substring(3, 4) + sHyphen;
                                    sReturnNumber += sNumber.Substring(7);
                                }
                                else
                                {

                                    string sTemp = sNumber.Substring(3);

                                    if (sTemp.Substring(0, 2) == "02")
                                    {
                                        sReturnNumber += sTemp.Substring(0, 2) + sHyphen;
                                        if (sTemp.Length <= 9)
                                        {
                                            sReturnNumber += sTemp.Substring(2, 3) + sHyphen;
                                            sReturnNumber += sTemp.Substring(5);
                                        }
                                        else
                                        {
                                            sReturnNumber += sTemp.Substring(2, 4) + sHyphen;
                                            sReturnNumber += sTemp.Substring(6);
                                        }
                                    }
                                    else
                                    {
                                        if (sTemp.Length <= 12)
                                        {
                                            sReturnNumber += sTemp.Substring(0, 4) + sHyphen;
                                            sReturnNumber += sTemp.Substring(4, 4) + sHyphen;
                                            sReturnNumber += sTemp.Substring(8);

                                        }
                                        else
                                        {
                                            sReturnNumber += sTemp.Substring(0, 3) + sHyphen;

                                            if (sTemp.Length == 10)
                                            {
                                                sReturnNumber += sTemp.Substring(3, 3) + sHyphen;
                                                sReturnNumber += sTemp.Substring(6);
                                            }
                                            else if (sTemp.Length > 10)
                                            {
                                                sReturnNumber += sTemp.Substring(3, 4) + sHyphen;
                                                sReturnNumber += sTemp.Substring(7);
                                            }
                                            else
                                            {
                                                sReturnNumber += sTemp.Substring(3);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        sReturnNumber = "";
                    }
                }
            }


            return sReturnNumber;
        }
    }
    #endregion

    #region 글자수체크
    public class TextLenCheck
    {
        public TextLenCheck()
        {
        }

        public static bool TextLen(int iTotalLenByte, string sTexts)
        {
            bool bResult = false;


            Encoding ecustr = Encoding.GetEncoding(949);
            byte[] ByteMsg = ecustr.GetBytes(sTexts.ToString());


            if (ByteMsg.Length > iTotalLenByte)
            {
                bResult = false;

            }
            else
            {
                bResult = true;
            }

            return bResult;
        }
    }
    #endregion

    public class BlankRow
    {
        public BlankRow()
        {
        }

        private static DataTable GetDT()
        {
            DataTable dt = new DataTable("T");
            dt.Columns.Add("1", typeof(System.String));
            return dt;
        }

        public static void BlankRowAdd(GridView gv)
        {
            //빈데이터 넣기
            DataSet BlankDataSet = new DataSet();
            DataTable dt = BlankRow.GetDT();
            BlankDataSet.Tables.Add(dt);

            //----- Bind ----------------------------------------------------------------------------------------
            gv.DataSource = BlankDataSet.Tables["T"];
            gv.DataBind();
        }

        public static DataSet BlankRowAdd()
        {
            //빈데이터 넣기
            DataSet BlankDataSet = new DataSet();
            DataTable dt = BlankRow.GetDT();
            BlankDataSet.Tables.Add(dt);

            return BlankDataSet;
        }
    }

    #region 이메일보내기

    public class SendEmail
    {
        public SendEmail()
        {
        }

        public static bool EmailSend(string mailTo, string subject, string body)
        {
            string sjs = string.Empty;
            bool result = true;

            if (mailTo != null && subject != null && body != null)
            {

                string Server = ConfigurationManager.AppSettings["SmtpServer"];

                //Attachment attach1 = new Attachment(FileNameEn);   // 영문 이름의 파일 첨부

                //Attachment attach2 = new Attachment(FileNameKo);   // 한글 이름의 파일 첨부

                MailMessage message = new MailMessage();

                // old : 10.180.10.34 
                // 수정 2012-07-23 : 10.180.10.47 
				// 수정 2013-10-31 : Info2011 -> info2011
                SmtpClient client = new SmtpClient("10.180.10.47");        // 메일 서버 설정 포함
                client.Credentials = new System.Net.NetworkCredential("info", "info2011");  

                message.IsBodyHtml = true;

                message.From = new MailAddress("info@compassion.or.kr");

                message.To.Add(new MailAddress(mailTo.ToString().Trim()));

                message.Subject = subject.ToString().Trim();

                message.Body = SendEmail.EmailDecoration(body).ToString();
                try
                {
                    client.Send(message); // 메일 발송

                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    result = false;

                }

            }
            return result;
        }

        public static bool SearchEmailSend(string mailTo, string subject, string body)
        {
            string sjs = string.Empty;
            bool result = true;

            if (mailTo != null && subject != null && body != null)
            {

                string Server = ConfigurationManager.AppSettings["SmtpServer"];

                //Attachment attach1 = new Attachment(FileNameEn);   // 영문 이름의 파일 첨부

                //Attachment attach2 = new Attachment(FileNameKo);   // 한글 이름의 파일 첨부

                MailMessage message = new MailMessage();

                // old : 10.180.10.34 
                // 수정 2012-07-23 : 10.180.10.47 
				// 수정 2013-10-31 : Info2011 -> info2011
                SmtpClient client = new SmtpClient("10.180.10.47");        // 메일 서버 설정 포함
                client.Credentials = new System.Net.NetworkCredential("info", "info2011");  


                message.IsBodyHtml = true;

                message.From = new MailAddress("info@compassion.or.kr");

                message.To.Add(new MailAddress(mailTo.ToString().Trim()));

                message.Subject = subject.ToString().Trim();

                message.Body = SendEmail.SearchEmailDecoration(body).ToString();
                try
                {
                    client.Send(message); // 메일 발송

                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    result = false;

                }

            }
            return result;
        }

        public static bool EnterpriseEmail(string mailFrom, string mailTo, string mailSubject, string mailBody)
        {
            string sjs = string.Empty;
            bool result = true;

            if (mailFrom != null && mailTo != null && mailSubject != null && mailBody != null)
            {

                string Server = ConfigurationManager.AppSettings["SmtpServer"];

                //Attachment attach1 = new Attachment(FileNameEn);   // 영문 이름의 파일 첨부

                //Attachment attach2 = new Attachment(FileNameKo);   // 한글 이름의 파일 첨부

                MailMessage message = new MailMessage();

                // old : 10.180.10.34 
                // 수정 2012-07-23 : 10.180.10.47 
				// 수정 2013-10-31 : Info2011 -> info2011
                SmtpClient client = new SmtpClient("10.180.10.47");        // 메일 서버 설정 포함
                client.Credentials = new System.Net.NetworkCredential("info", "info2011");  



                message.From = new MailAddress(mailFrom.ToString().Trim());

                message.To.Add(new MailAddress(mailTo.ToString().Trim()));

                message.Subject = mailSubject.ToString();

                message.IsBodyHtml = true;

                //message.Body = mailBody.ToString();   
                message.Body = SendEmail.EmailDecoration(mailBody).ToString();
                try
                {
                    client.Send(message); // 메일 발송

                }
                catch (Exception ex)
                {
                    string sMsg = ex.Message;
                    result = false;

                }

            }
            return result;
        }

        //추가 2013-01-31  폼없이 내용만 보낼때
        public static bool ContentEmailSend(string mailTo, string subject, string body)
        {
            string sjs = string.Empty;
            bool result = true;

            if (mailTo != null && subject != null && body != null)
            {

                string Server = ConfigurationManager.AppSettings["SmtpServer"];

                //Attachment attach1 = new Attachment(FileNameEn);   // 영문 이름의 파일 첨부

                //Attachment attach2 = new Attachment(FileNameKo);   // 한글 이름의 파일 첨부

                MailMessage message = new MailMessage();

                // old : 10.180.10.34 
                // 수정 2012-07-23 : 10.180.10.47
								// 수정 2013-10-31 : Info2011 -> info2011
                SmtpClient client = new SmtpClient("10.180.10.47");        // 메일 서버 설정 포함
                client.Credentials = new System.Net.NetworkCredential("info", "info2011");


                message.IsBodyHtml = true;

                message.From = new MailAddress("info@compassion.or.kr");

                message.To.Add(new MailAddress(mailTo.ToString().Trim()));

                message.Subject = subject.ToString().Trim();

                message.Body = body;
                try
                {
                    client.Send(message); // 메일 발송

                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    result = false;

                }

            }
            return result;
        }


        private static StringBuilder EmailDecoration(string sMailBody)
        {
            StringBuilder sDecoration = new StringBuilder();
            //sDecoration.Append(@"<table width=""600"" border=""0"" cellpadding=""0"" cellspacing=""0"">");
            //sDecoration.Append(@"<tr>");
            //sDecoration.Append(@"<td align=""center""><img src=""http://www.compassion.or.kr/formmail/20071206/mailhead.jpg"" border=0>");
            //sDecoration.Append(@"</td>");
            //sDecoration.Append(@"</tr>");
            //sDecoration.Append(@"<tr>");
            //sDecoration.Append(@"<td align=""center"">&nbsp;");
            //sDecoration.Append(@"</td>");
            //sDecoration.Append(@"</tr>");
            //sDecoration.Append(@"<tr>");
            ////sDecoration.Append(@"<td><span style=""font-size:12px;color:#0058AA;"">컴패션 지도자 양성 프로그램은 수혜 학생 개인적으로는 더 나은 변화와 성장을 할 수 있는 기회가 되며, 또한 그를 통해 세상을 변화시킬 수 있는 프로그램입니다. 컴패션에서 고등학교까지 후원받은 어린이 중 선발된 탁월한 학생을 대상으로 대학 교육의 혜택을 제공합니다. 또한 학위 취득 후에도 이들이 가진 재능을 사회에서 발휘하고 훌륭한 전문가로서 살아갈 수 있도록 다양한 기회를 제공합니다.</span>");
            ////sDecoration.Append(@"<td>컴패션 지도자 양성 프로그램은 수혜 학생 개인적으로는 더 나은 변화와 성장을 할 수 있는 기회가 되며, 또한 그를 통해 세상을 변화시킬 수 있는 프로그램입니다. 컴패션에서 고등학교까지 후원받은 어린이 중 선발된 탁월한 학생을 대상으로 대학 교육의 혜택을 제공합니다. 또한 학위 취득 후에도 이들이 가진 재능을 사회에서 발휘하고 훌륭한 전문가로서 살아갈 수 있도록 다양한 기회를 제공합니다.");
            //sDecoration.Append(@"</td>");
            //sDecoration.Append(@"</tr>");
            ///*추가*/
            //sDecoration.Append(@"<tr>");
            //sDecoration.Append(@"<td height=""15""></td>");
            //sDecoration.Append(@"</tr>");
            //sDecoration.Append(@"<tr>");
            //sDecoration.Append(@"<td>" + sMailBody + "</td>");
            //sDecoration.Append(@"</tr>");
            ///*추가 끝*/
            //sDecoration.Append(@"<tr>");
            //sDecoration.Append(@"<td height=""15""></td>");
            //sDecoration.Append(@"</tr>");
            //sDecoration.Append(@"<tr>");
            //sDecoration.Append(@"<td bgColor=#d8d8d8 height=1>");
            //sDecoration.Append(@"</td>");
            //sDecoration.Append(@"</tr>");
            //sDecoration.Append(@"<tr>");
            //sDecoration.Append(@"<td align=""center""><img src=""http://www.compassion.or.kr/formmail/20080710/mail_c1.gif"" usemap=#map77 border=0>    </td>");
            //sDecoration.Append(@"</tr>");
            //sDecoration.Append(@"</table>");
            //sDecoration.Append(@"<map name=map77>");
            //sDecoration.Append(@"<area shape=RECT target=_blank coords=12,12,176,74 href=""http://www.compassion.or.kr"">");
            //sDecoration.Append(@"<area shape=RECT coords=372,97,477,110 href=""mailto:info@compassion.or.kr"">");
            //sDecoration.Append(@"</map>");

            //sDecoration.Append(@"<html>");
            //sDecoration.Append(@"<head>");
            //sDecoration.Append(@"<meta http-equiv=Content-Type content=text/html; charset=euc-kr>");
            //sDecoration.Append(@"<title>안녕하세요.한국 컴패션입니다.</title>");
            //sDecoration.Append(@"<body>");
            sDecoration.Append(@"<style type=text/css>");
            sDecoration.Append(@"<!--");
            sDecoration.Append(@"body {");
            sDecoration.Append(@"	margin-left: 0px;");
            sDecoration.Append(@"	margin-top: 0px;");
            sDecoration.Append(@"	margin-right: 0px;");
            sDecoration.Append(@"	margin-bottom: 0px;");
            sDecoration.Append(@"}");
            sDecoration.Append(@".style1 {color: #003366}");
            sDecoration.Append(@".style2 {color: #005AB5; font-size: 12px; }");
            sDecoration.Append(@".style5 {");
            sDecoration.Append(@"	color: #0066CC;");
            sDecoration.Append(@"	font-size: 13px;");
            sDecoration.Append(@"	font-weight: bold;");
            sDecoration.Append(@"}");
            sDecoration.Append(@".style6 {font-size: 11px; color: #999999;}");
            sDecoration.Append(@"-->");
            sDecoration.Append(@"</style>");
            sDecoration.Append(@"<table width=700 border=0 align=center cellpadding=0 cellspacing=0>");
            sDecoration.Append(@"  <tr>");
            sDecoration.Append(@"    <td><img src=http://www.compassion.or.kr/formmail/mail_top01.jpg width=700 height=185 border=0 usemap=#Map2></td>");
            sDecoration.Append(@"  </tr>");
            sDecoration.Append(@"  <tr>");
            sDecoration.Append(@"    <td background=http://www.compassion.or.kr/formmail/mid_back.JPG><table width=700 border=0 cellspacing=0 cellpadding=0>");
            sDecoration.Append(@"      <tr>");
            sDecoration.Append(@"        <td width=50 align=right valign=top><img src=http://www.compassion.or.kr/formmail/mid_left.jpg width=10 height=101></td>");
            sDecoration.Append(@"        <td valign=top><table width=520 border=0 align=center cellpadding=9 cellspacing=0>");
            sDecoration.Append(@"          <tr>");
            sDecoration.Append(@"            <td align=center><p class=style2 size=4>" + sMailBody + "<br>");
            sDecoration.Append(@"</span></p>");
            sDecoration.Append(@"              <p class=style1><span class=style2>              </span></p></td>");
            sDecoration.Append(@"            </tr>");
            sDecoration.Append(@"          <tr>");
            sDecoration.Append(@"            <td></td>");
            sDecoration.Append(@"            </tr>");
            sDecoration.Append(@"        </table></td>");
            sDecoration.Append(@"        <td width=50 align=left valign=bottom><img src=http://www.compassion.or.kr/formmail/mid_right.jpg width=40 height=188></td>");
            sDecoration.Append(@"      </tr>");
            sDecoration.Append(@"    </table></td>");
            sDecoration.Append(@"  </tr>");
            sDecoration.Append(@"  <tr>");
            sDecoration.Append(@"    <td><img src=http://www.compassion.or.kr/formmail/mail_bottom.JPG width=700 height=94></td>");
            sDecoration.Append(@"  </tr>");
            sDecoration.Append(@"</table>");
            sDecoration.Append(@"<map name=Map2>");
            sDecoration.Append(@"  <area shape=rect coords=16,30,265,166 href=http://www.compassion.or.kr/ target=_blank>");
            sDecoration.Append(@"</map>");
            //sDecoration.Append(@"</body>");
            //sDecoration.Append(@"</html>");

            return sDecoration;
        }

        private static StringBuilder SearchEmailDecoration(string sMailBody)
        {
            StringBuilder sDecoration = new StringBuilder();
            sDecoration.Append(@"<table width=700 border=0 align=center cellpadding=0 cellspacing=0>");
            sDecoration.Append(@"  <tr>");
            sDecoration.Append(@"    <td><img src=http://www.compassion.or.kr/formmail/mail_top01.jpg width=700 height=185 border=0 usemap=#Map2></td>");
            sDecoration.Append(@"  </tr>");
            sDecoration.Append(@"  <tr>");
            sDecoration.Append(@"    <td background=http://www.compassion.or.kr/formmail/mid_back.JPG><table width=700 border=0 cellspacing=0 cellpadding=0>");
            sDecoration.Append(@"      <tr>");
            sDecoration.Append(@"       <td width=50 align=right valign=top><img src=http://www.compassion.or.kr/formmail/mid_left.jpg width=10 height=101></td>");
            sDecoration.Append(@"        <td valign=top><table width=520 border=0 align=center cellpadding=9 cellspacing=0>");
            sDecoration.Append(@"          <tr>");
            sDecoration.Append(@"            <td align=left><font size=3>안녕하세요. 후원자님.<br>" + sMailBody + "</font>");
            sDecoration.Append(@"			<br><br>회원정보 변경은 홈페이지 > 로그인 > 마이페이지 > <a href='http://www.compassion.or.kr/membership/MemberLogin.aspx' target='blank'>회원정보 변경</a>에서 하실 수 있습니다.<br>또, 비밀번호 변경은 홈페이지 > 로그인 > 마이페이지 > 회원정보 변경 > <a href='http://www.compassion.or.kr/membership/MemberLogin.aspx' target='blank'>비밀번호 변경</a>에서 하실 수 있습니다.<br><br><br>후원자님의 가정에 하나님의 축복과 평화가 가득하기를 기도합니다.<br><br>감사합니다.<br>");
            sDecoration.Append(@"			</td>");
            sDecoration.Append(@"            </tr>");
            sDecoration.Append(@"          <tr>");
            sDecoration.Append(@"            <td></td>");
            sDecoration.Append(@"            </tr>");
            sDecoration.Append(@"        </table></td>");
            sDecoration.Append(@"        <td width=50 align=left valign=bottom><img src=http://www.compassion.or.kr/formmail/mid_right.jpg width=40 height=188></td>");
            sDecoration.Append(@"      </tr>");
            sDecoration.Append(@"    </table></td>");
            sDecoration.Append(@"  </tr>");
            sDecoration.Append(@"  <tr>");
            sDecoration.Append(@"    <td><img src=http://www.compassion.or.kr/formmail/mail_bottom.JPG width=700 height=94></td>");
            sDecoration.Append(@"  </tr>");
            sDecoration.Append(@"</table>");
            sDecoration.Append(@"<map name=Map2>");
            sDecoration.Append(@"  <area shape=rect coords=16,30,265,166 href=http://www.compassion.or.kr/ target=_blank>");
            sDecoration.Append(@"</map>");
            return sDecoration;
        }
    }

    #endregion


    #region SMS전송하기

    public class SendSMS
    {
        public SendSMS()
        {
        }

        public static bool SMSSend(string sSMS, string sSponsorName, string sContents)
        {
            string sjs = string.Empty;
            WWWService.Service _wwwService = new WWWService.Service();

            //SMS발송내용
            string[] strSMSContent = new string[4];
            strSMSContent[0] = sSMS; //수신자 전화번호
            strSMSContent[1] = sSponsorName.ToString().Trim();//수신자 이름
            //strSMSContent[2] = "0236683477";//발신자 전화번호
            strSMSContent[2] = ConfigurationManager.AppSettings["SMSSendNo"].ToString().Trim(); ;//발신자 전화번호
            strSMSContent[3] = sContents;

            //발송   
            string sResult = "";
            try
            {
                sResult = _wwwService.SendSMS(strSMSContent);
            }
            catch (Exception ex)
            {
                return false;
            }
            if (sResult.Substring(0, 2) != "OK")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 아이디,비번찾기시 사용할 SMS전송    
        /// </summary>
        /// <param name="sMobile"></param>
        /// <param name="sSponsorName"></param>
        /// <param name="sRandom"></param>   
        /// <returns></returns>
        public static string SMSInfoSearch(string sMobile, string sSponsorName, string sRandom)
        {
            WWWService.Service _wwwService = new WWWService.Service();
            string sRendom = sRandom;
            string[] strSMSContent = new string[4];
            //SMS발송내용
            string[] sSMSContent = new string[4];
            sSMSContent[0] = sMobile; //수신자 전화번호
            sSMSContent[1] = sSponsorName.ToString().Trim();//수신자 이름
            //sSMSContent[2] = "0236683477";//발신자 전화번호
            sSMSContent[2] = ConfigurationManager.AppSettings["SMSSendNo"].ToString().Trim(); //발신자 전화번호
            sSMSContent[3] = "[한국컴패션 인증번호] " + sRandom;

            string sResult = "";

            try
            {

                sResult = _wwwService.SendSMS(sSMSContent);
            }
            catch (Exception ex)
            {
                sRendom = "30";
                return sRendom;
            }

            if (sResult.Substring(0, 2) != "OK")
            {
                sRendom = "30";
            }

            return sRendom;
        }
    }

    #endregion


    #region 실명인증결과값확인
    public class NameCertify
    {
        public NameCertify() { }

        public static string GetNameResult(string sResult)
        {
            string sMsg = "";
            if (sResult == null)
            {
                sResult = "5";
            }
            switch (sResult)
            {
                case "2":
                    sMsg = "요청한 주민등록번호와 성명이 불일치합니다.";
                    break;
                case "3":
                    sMsg = "미등록번호 입니다.";
                    break;
                case "4":
                    sMsg = "주민번호 형식이 올바르지 않습니다.";
                    break;
                case "5":
                    sMsg = "시스템장애입니다.관리자에게 문의해주세요.";
                    break;
                case "7":
                    sMsg = "명의도용방지 주민번호입니다.";
                    break;
                default:
                    sMsg = "실명인증실패입니다.";
                    break;

            }
            return sMsg;
        }
    }
    #endregion
}
