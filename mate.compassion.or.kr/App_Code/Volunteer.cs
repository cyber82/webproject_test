﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

/// <summary>
/// Receipt의 요약 설명입니다.
/// </summary>
public class Volunteer
{
    public Volunteer()
	{
		//
		// TODO: 생성자 논리를 여기에 추가합니다.
		//
	}

    private string _SponsorName;
    public string SponsorName
    {
        get { return _SponsorName; }
        set { _SponsorName = value; }
    }

    private string _SponsorBirthday;
    public string SponsorBirthday
    {
        get { return _SponsorBirthday; }
        set { _SponsorBirthday = value; }
    }

    private string _SponsorAddress;
    public string SponsorAddress
    {
        get { return _SponsorAddress; }
        set { _SponsorAddress = value; }
    }

    private string _SponsorCommunication;
    public string SponsorCommunication
    {
        get { return _SponsorCommunication; }
        set { _SponsorCommunication = value; }
    }

    private string _VolunteerTime;
    public string VolunteerTime
    {
        get { return _VolunteerTime; }
        set { _VolunteerTime = value; }
    }

    private string _VolunteerContents;
    public string VolunteerContents
    {
        get { return _VolunteerContents; }
        set { _VolunteerContents = value; }
    }

    private string _Charge;
    public string Charge
    {
        get { return _Charge; }
        set { _Charge = value; }
    }

    public string CreateReceipt()
    {
        string sFilePath = string.Empty;

        sFilePath = CreateReceiptFile();
        return sFilePath;
    }

    //private string CreateReceiptFile(DataSet dsReceiptContent)
    private string CreateReceiptFile()
    {
        string sResult = string.Empty;

        //TextWriter tw;
        Encoding encUTF8;
        Encoding encKor;
        byte[] byteUTF8;
        byte[] byteKor;
        string sFileContent = string.Empty;
        string sFilePath = string.Empty;
        string sFileFullName = string.Empty;
        DateTime dtAppDate = DateTime.Now;

        //-- getData
        string sSponsorName = this._SponsorName;
        string sSponsorBirthday = this._SponsorBirthday;
        string sSponsorAddress = this._SponsorAddress;
        string sSponsorCommunication  = this._SponsorCommunication;
        string sVolunteerTime = this._VolunteerTime;
        string sVolunteerContents = "<br/>" + this._VolunteerContents + "<br/>";
        string sCharge = this._Charge;

        //-- 봉사내용 자르기
        //int i=0;
        //while(i < sVolunteerContents.Length)
        //{
        //    if ((i + 77) < sVolunteerContents.Length)
        //    {
        //        sVolunteerContentsResult += sVolunteerContents.Substring(i, 77) + "<br/>";
        //        i = i + 77;
        //    }
        //    else
        //    {
        //        sVolunteerContentsResult += sVolunteerContents.Substring(i);
        //        break;
        //    }
        //}
        //sVolunteerContentsResult = "<br/>" + sVolunteerContentsResult + "<br/>";

        //-- HTML 내용 만들기
        sFileContent = CreateReceiptHtml(sSponsorName, sSponsorBirthday, sSponsorAddress, sSponsorCommunication, sVolunteerTime, sVolunteerContents, sCharge);

        //-- Encoding
        encUTF8 = System.Text.Encoding.UTF8;
        encKor = System.Text.Encoding.GetEncoding("euc-kr");

        //-- Convert Encoding From "UTF8" To "euc-kr"
        byteUTF8 = Encoding.UTF8.GetBytes(sFileContent);
        byteKor = Encoding.Convert(encUTF8, encKor, byteUTF8);

        encKor.GetString(byteKor);

        return encKor.GetString(byteKor); ;
    }

    /// <summary>
    /// 기부금영수증 HTML 내용 만들기
    /// </summary>
    /// <param name="sConID">후원자 ID</param>
    /// <param name="sConName">후원자 이름</param>
    /// <param name="sConSSN">후원자 주민번호</param>
    /// <param name="sConAddress">후원자 주소</param>
    /// <param name="dsReceiptContent">기부내용</param>
    /// <param name="dtAppTime">신청일</param>
    /// <returns></returns>
    private string CreateReceiptHtml( string sSponsorName, string sSponsorBirthday, string sSponsorAddress
                                    , string sSponsorCommunication, string sVolunteerTime, string sVolunteerContents, string sCharge)
    {
        string sHtml = string.Empty;
        
        #region //-- 상단, 스크립트, 스타일
        sHtml = " <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>                                                                      ".TrimEnd()
        + " <html>                                                                                                                                    ".TrimEnd()
        + " <head>                                                                                                                                    ".TrimEnd()
        + " <title>한국컴패션 사회봉사확인서</title>                                                                                                  ".TrimEnd()

        //ScriptX프린트 설정
        //프린트설정
        + " <script language='javascript' type='text/javascript'>                   ".TrimEnd()
        //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        + " function setPrint()                                                     ".TrimEnd()
        + " {                                                                       ".TrimEnd()
        + "     if(factory.printing == null)                                        ".TrimEnd()
        + "     {                                                                   ".TrimEnd()
        + "         alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.'); ".TrimEnd()
        + "         return;                                                         ".TrimEnd()
        + "     }                                                                   ".TrimEnd()
        + "     factory.printing.header       = '';                                 ".TrimEnd()
        + "     factory.printing.footer       = '';                                 ".TrimEnd()
        + "     factory.printing.portrait     = true;                               ".TrimEnd() //true 세로출력,false가로출력
        + "     factory.printing.leftMargin   = 10;                                 ".TrimEnd()
        + "     factory.printing.topMargin    = 10;                                 ".TrimEnd()
        + "     factory.printing.rightMargin  = 10;                                 ".TrimEnd()
        + "     factory.printing.bottomMargin = 10;                                 ".TrimEnd()
        //+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        + "     agree = confirm('현재 페이지를 출력하시겠습니까?');                 ".TrimEnd()
        + "     if (agree) factory.printing.Print(false, window)                    ".TrimEnd()
        + " }                                                                       ".TrimEnd()
        + " </script>                                                               ".TrimEnd()
        //프린트설정 끝

        + " <style type='text/css'>                                                                                                                   ".TrimEnd()
        + " /*body { margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;}*/                                                     ".TrimEnd()
        + " img {border:none;}                                                                                                                        ".TrimEnd()
        + " body, td {  font-family: '돋움', '바탕', '굴림'; font-size:10pt; color: #000000;}                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 소득세법 */                                                                                                                            ".TrimEnd()
        + " td.CodeTitle{ font-size:10pt; padding-bottom:5px;}                                                                                        ".TrimEnd()
        + " td.Code{ font-size:8pt; padding-bottom:5px;}                                                                                              ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 HEAD */                                                                                                                       ".TrimEnd()
        + " td.ContentHeadType{width: 96px; height:25px; text-align: center; background-color: silver;}                                               ".TrimEnd()
        + " td.ContentHeadCode{width: 46px; text-align: center; background-color: silver;}                                                            ".TrimEnd()
        + " td.ContentHeadDate{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadDesc{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadAmount{width: 98px; text-align: center; background-color: silver;}                                                          ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 CONTENT */                                                                                                                    ".TrimEnd()
        + " td.ContentItem{                                                                                                                           ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px dotted;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " td.ContentItemBottom{                                                                                                                     ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px solid;                                                                                                             ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* Title */                                                                                                                               ".TrimEnd()
        + " .Title{                                                                                                                                   ".TrimEnd()
        + " font-size: 12pt;                                                                                                                          ".TrimEnd()
        + " font-weight: bold;                                                                                                                        ".TrimEnd()
        + " border-bottom: gray 1px solid;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " /*  */                                                                                                                                    ".TrimEnd()
        + " .Gray_Table{                                                                                                                              ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:1px;                                                                                                                     ".TrimEnd()
        + " border-left-width:2px;                                                                                                                    ".TrimEnd()
        + " border-right-width:2px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head{                                                                                                                               ".TrimEnd()
        + " text-align: center;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Item{                                                                                                                               ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " </style>                                                                                                                                  ".TrimEnd()
        + " </head>                                                                                                                                   ".TrimEnd();
        #endregion

        #region //-- 인적사항
        sHtml += "<body onload = 'setPrint()'; style='text-align: left;'>                                                                               ".TrimEnd()
        
        //+ " <object id=factory style='display:none' classid='clsid:1663ed61-23eb-11d2-b92f-008048fdd814' odebase='D:/Files/IEPageSetupX.cab#Version=6,1,429,14'></object>".TrimEnd()

        //ScriptX다운로드 경로 (프린트 설정시 필요)
        + " <object id=factory style='display:none' ".TrimEnd()
        + " classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ".TrimEnd()
            //+ " codebase=http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,2,433,14 VIEWASTEXT> ".TrimEnd()
        + " codebase=../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ".TrimEnd()
            //+ " <PARAM Name='DebugEnable' VALUE='True'> ".TrimEnd()
            //+ " codebase='D:/Files/ScriptX.cab#Version=6,4,438,06'> ".TrimEnd()
        + " </object> ".TrimEnd()
            //ScriptX다운로드 경로 끝

        + " <table style='width:700px; text-align:center;' cellpadding='0' cellspacing='0' border='0'>                                                  ".TrimEnd()
        +"	<tr>                                                                                                                                        ".TrimEnd()
        +"		<td>                                                                                                                                    ".TrimEnd()
        +"			<br/>                                                                                                                               ".TrimEnd()
        +"			<table style='width:680px;' cellpadding='0' cellspacing='0' border='0'>                                                             ".TrimEnd()
        +"				<tr>                                                                                                                            ".TrimEnd()
        +"					<td>                                                                                                                        ".TrimEnd()
        + "						<img src='../images/compassion_logo.gif' width='200px' height='75px' alt=''/>                                           ".TrimEnd()
        +"					</td>                                                                                                                       ".TrimEnd()
        +"				</tr>                                                                                                                           ".TrimEnd()
        +"				<tr>                                                                                                                            ".TrimEnd()
        +"					<td height='50px'></td>                                                                                                     ".TrimEnd()
        +"				</tr>                                                                                                                           ".TrimEnd()
        +"				<tr>                                                                                                                            ".TrimEnd()
        +"					<td style='text-align: center; height:50px;'>                                                                               ".TrimEnd()
        +"						<span style='font-size: 20pt; font-weight:bold;'>사회봉사 확인서</span>                                                 ".TrimEnd()
        +"					</td>                                                                                                                       ".TrimEnd()
        +"				</tr>                                                                                                                           ".TrimEnd()
        +"				<tr>                                                                                                                            ".TrimEnd()
        +"					<td height='50px'></td>                                                                                                     ".TrimEnd()
        +"				</tr>                                                                                                                           ".TrimEnd()
        +"			</table>                                                                                                                            ".TrimEnd()
        +"			<table style='width: 680px' cellpadding='0' cellspacing='0' border='0'>                                                             ".TrimEnd()
        +"				<tr>                                                                                                                            ".TrimEnd()
        +"					<td colspan='4' class='Title'>                                                                                              ".TrimEnd()
        +"						<span style='font-size: 12pt; border-bottom: gray 2px solid;'>⊙인적사항</span>                                         ".TrimEnd()
        +"					</td>                                                                                                                       ".TrimEnd()
        +"				</tr>                                                                                                                           ".TrimEnd()
        +"			</table>                                                                                                                            ".TrimEnd()
        +"			<table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'>                                          ".TrimEnd()
        +"				<tr >                                                                                                                           ".TrimEnd()
        +"					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>성&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;명</td> ".TrimEnd()
        +"					<td class='Gray_Item' style='width: 230px; padding-left:10px;'>&nbsp;" + sSponsorName + "</td>                              ".TrimEnd()
        +"					<td class='Gray_Head' style='width: 110px; background-color: silver;'>생 년 월 일</td>                                      ".TrimEnd()
        +"					<td class='Gray_Item' style='width: 230px; text-align:center;'>&nbsp;" + sSponsorBirthday + "</td>                          ".TrimEnd()
        +"				</tr>                                                                                                                           ".TrimEnd()
        +"				<tr>                                                                                                                            ".TrimEnd()
        +"					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>주&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;소</td> ".TrimEnd()
        //+"					<td class='Gray_Item' colspan='3' style='padding-left:10px;'>&nbsp;서울 광진구 광장동 장로회신학대학교 (김도일 교수)</td>   ".TrimEnd()
        + "					<td class='Gray_Item' colspan='3' style='padding-left:10px;'>&nbsp;" + sSponsorAddress + "</td>                             ".TrimEnd()
        +"				</tr>                                                                                                                           ".TrimEnd()
        +"				<tr>                                                                                                                            ".TrimEnd()
        +"					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>연 락 처</td>                           ".TrimEnd()
        +"					<td class='Gray_Item' colspan='3' style='padding-left:10px;'>&nbsp;" + sSponsorCommunication + "</td>                       ".TrimEnd()
        +"				</tr>                                                                                                                           ".TrimEnd()
        +"			</table>                                                                                                                            ".TrimEnd()
        +"			                                                                                                                                    ".TrimEnd()
        +"			<br />                                                                                                                              ".TrimEnd()
        +"			<br />                                                                                                                              ".TrimEnd()
        +"			<br />                                                                                                                              ".TrimEnd();
        #endregion
        
        #region //-- 기부 내용
        sHtml += "  <table style='width: 680px' id='TABLE1' cellpadding='0' cellspacing='0' border='0'>                                                                                 ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='2' class='Title'><span style='font-size: 12pt'>⊙봉사활동내역</span></td>                                                                      ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                                                            ".TrimEnd()
        + "			<table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'>                                                                          ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>봉사기관명</td>                                                         ".TrimEnd()
        + "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;사회복지법인 한국컴패션</td>                                                                         ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>봉&nbsp;사&nbsp;시&nbsp;간</td>                                         ".TrimEnd()
        //+ "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;2007년06월14일 - 2007년06월14일(1회) 30분</td>                                                       ".TrimEnd()
        + "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;" + sVolunteerTime + "</td>                                                                          ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 200px; background-color: silver;'>봉&nbsp;사&nbsp;내&nbsp;용</td>                                        ".TrimEnd()
        + "					<td class='Gray_Item' style='padding-left:10px;' valign='top'>&nbsp;" + sVolunteerContents + "</td>                                                                      ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>담&nbsp;&nbsp;&nbsp;당&nbsp;&nbsp;&nbsp;자</td>                         ".TrimEnd()
        + "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;" + sCharge + "</td>                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "			</table>	                                                                                                                                                        ".TrimEnd()
        + "			                                                                                                                                                                    ".TrimEnd()
        + "			<br />                                                                                                                                                              ".TrimEnd()
        + "			<br />                                                                                                                                                              ".TrimEnd()
        + "			<br />                                                                                                                                                              ".TrimEnd()
        + "                                                                                                                                                                             ".TrimEnd();
        #endregion

        #region //-- 끝
        sHtml += "  <table style='width: 680px; background: url(../images/stamp2.gif) fixed no-repeat right middle;' cellpadding='0' cellspacing='0' border='0'>                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='text-align: center; height: 30px;'>                                                                                                  ".TrimEnd()
        + "						<span style='font-size: 12pt; font-family: 바탕체; font-weight:bold;'>상기 학생은 본 기관에서 성실히 봉사활동을 수행했음을 위와 같이 증명합니다.</span> ".TrimEnd()
        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 90px;'></td>                                                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='text-align: right; height: 30px;'>                                                                                                   ".TrimEnd()
        + "						사회복지법인 <span style='font-size: 16pt; font-weight:bold;'>한국컴패션</span>&nbsp;                                                                   ".TrimEnd()
        + "						대표 <span style='font-size: 16pt; font-weight:bold;'>서정인</span>                                                                                     ".TrimEnd()
        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 30px; text-align: right'>" + DateTime.Now.ToString("yyyy년MM월dd일") + "</td>                                                ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td style='height:50px;'></td>                                                                                                                              ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 30px; text-align: left'>                                                                                                     ".TrimEnd()
        + "						<strong>Compassion Korea</strong> |                                                                                                                     ".TrimEnd()
        + "						<strong>한국컴패션</strong>                                                                                                                             ".TrimEnd()
        + "						140-894 서울시 용산구 한남동 석전빌딩 한국컴패션                                                                                              ".TrimEnd()
        + "						<br />                                                                                                                                                  ".TrimEnd()
        + "						TEL: 02-3668-3400 &nbsp; FAX: 02-3668-3456 &nbsp; EMAIL: info@compassion.or.kr                                                                          ".TrimEnd()
        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                                                            ".TrimEnd()
        + "		</td>                                                                                                                                                                   ".TrimEnd()
        + "	</tr>                                                                                                                                                                       ".TrimEnd()
        + "</table>                                                                                                                                                                     ".TrimEnd()
        + "                                                                                                                                                                             ".TrimEnd()
        + "</body>                                                                                                                                                                      ".TrimEnd()
        + "</html>                                                                                                                                                                      ".TrimEnd();
	    #endregion

        return sHtml;
    }
}
