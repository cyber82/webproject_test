﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text; 

public class JavaScript
{
    public JavaScript()
    {
    }

    /// <summary>
    /// 자바 스크립트 시작구문인 script language="javascript" type="text/javascript" 문자열을 얻습니다.
    /// </summary>
    public static string HeaderScript
    {
        get { return @"<script language=""javascript"" type=""text/javascript"">"; }
    }

    public static string FooterScript
    {
        get { return "</script>"; }
    }

    public static string GetNewWindowScript(string PageUrl, string PageName, int Width, int Height, bool IsScroll)
    {
        StringBuilder jscript = new StringBuilder();

        jscript.Append("function NewWindow(mypage,myname,w,h,scroll) ");
        jscript.Append("{ ");
        jscript.Append("LeftPosition = (screen.width) ? (screen.width-w)/2 : 0; ");
        jscript.Append("TopPosition = (screen.height) ? (screen.height-h)/2 : 0; ");
        jscript.Append("settings = 'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll; ");
        jscript.Append("window.open(mypage,myname,settings).focus(); ");
        jscript.Append("}  ");
        jscript.Append("NewWindow('" + PageUrl + "', '" + PageName + "', " + Width + ", " + Height + ", '" + (IsScroll ? "yes" : "no") + "');");

        return jscript.ToString();
    }

    public static string GetFormCloseScript()
    {
        return "self.close();";
    }

    public static string GetAlertScript(string Msg)
    {
        return "alert('" + Msg + "');";
    }

    public static string GetHistoryBackScript()
    {
        return "history.back();";
    }

    public static String GetPageMoveScript(string Location)
    {
        return @"document.location.href= """ + Location + @""";";
    }

    public static String GetOpenerPageMoveScript(string Location)
    {
        return @"opener.location.href= """ + Location + @""";";
    }

    public static string GetNewWindowScript(string p, int p_2, int p_3, bool p_4)
    {
        throw new Exception("The method or operation is not implemented.");
    }
}