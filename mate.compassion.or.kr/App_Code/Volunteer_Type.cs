﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

/// <summary>
/// Receipt의 요약 설명입니다.
/// </summary>
public class Volunteer_Type
{
    int countOrderNo = 1;
    int payment_total = 0;

    #region 생성자

    public Volunteer_Type()
    {
        //
        // TODO: 생성자 논리를 여기에 추가합니다.
        //
    }


    private string _Type;
    public string Type
    {
        get { return _Type; }
        set { _Type = value; }
    }

    private string _ConID;
    public string ConID
    {
        get { return _ConID; }
        set { _ConID = value; }
    }

    private string _SponsorName;
    public string SponsorName
    {
        get { return _SponsorName; }
        set { _SponsorName = value; }
    }

    private string _SponsorAddress;
    public string SponsorAddress
    {
        get { return _SponsorAddress; }
        set { _SponsorAddress = value; }
    }

    private string _JuminID;
    public string JuminID
    {
        get { return _JuminID; }
        set { _JuminID = value; }
    }

    private string _BirthDate;
    public string BirthDate
    {
        get { return _BirthDate; }
        set { _BirthDate = value; }
    }

    private string _VolunteerTerm;
    public string VolunteerTerm
    {
        get { return _VolunteerTerm; }
        set { _VolunteerTerm = value; }
    }

    private string _VolunteerTime;
    public string VolunteerTime
    {
        get { return _VolunteerTime; }
        set { _VolunteerTime = value; }
    }

    private string _VolunteerContents;
    public string VolunteerContents
    {
        get { return _VolunteerContents; }
        set { _VolunteerContents = value; }
    }

    private string _PrintDate;
    public string PrintDate
    {
        get { return _PrintDate; }
        set { _PrintDate = value; }
    }

    private string _Charge;
    public string Charge
    {
        get { return _Charge; }
        set { _Charge = value; }
    }

    private string _ChargeTelNo;
    public string ChargeTelNo
    {
        get { return _ChargeTelNo; }
        set { _ChargeTelNo = value; }
    }

    private string _SponsorNameENG;
    public string SponsorNameENG
    {
        get { return _SponsorNameENG; }
        set { _SponsorNameENG = value; }
    }

    private string _VolunteerTermENG;
    public string VolunteerTermENG
    {
        get { return _VolunteerTermENG; }
        set { _VolunteerTermENG = value; }
    }

    private string _VolunteerTimeENG;
    public string VolunteerTimeENG
    {
        get { return _VolunteerTimeENG; }
        set { _VolunteerTimeENG = value; }
    }

    private string _PrintDateENG;
    public string PrintDateENG
    {
        get { return _PrintDateENG; }
        set { _PrintDateENG = value; }
    }

    private string _SearchTime;
    public string SearchTime
    {
        get { return _SearchTime; }
        set { _SearchTime = value; }
    }

    private DataSet _dsMateActivity;
    public DataSet dsMateActivity
    {
        get { return _dsMateActivity; }
        set { _dsMateActivity = value; }
    }

    private DataSet _dsPayment;
    public DataSet dsPayment
    {
        get { return _dsPayment; }
        set { _dsPayment = value; }
    }

    public string CreateReceipt()
    {
        string sFilePath = string.Empty;

        sFilePath = CreateReceiptFile();
        return sFilePath;
    }

    StringBuilder sHtml_New = new StringBuilder();


    #endregion

    //private string CreateReceiptFile(DataSet dsReceiptContent)
    private string CreateReceiptFile()
    {
        string sResult = string.Empty;

        //TextWriter tw;
        Encoding encUTF8;
        Encoding encKor;
        byte[] byteUTF8;
        byte[] byteKor;
        string sFileContent = string.Empty;
        string sFilePath = string.Empty;
        string sFileFullName = string.Empty;
        DateTime dtAppDate = DateTime.Now;

        //-- getData
        //string sSponsorName = this._SponsorName;
        //string sSponsorBirthday = this._SponsorBirthday;
        //string sSponsorAddress = this._SponsorAddress;
        //string sSponsorCommunication  = this._SponsorCommunication;
        //string sVolunteerTime = this._VolunteerTime;
        //string sVolunteerContents = "<br/>" + this._VolunteerContents + "<br/>";
        //string sCharge = this._Charge;

        //-- HTML 내용 만들기

        if (this._Type == "한글확인서")
            sFileContent = CreateReceiptHtml_1();

        else if (this._Type == "영문확인서")
            sFileContent = CreateReceiptHtml_2();

        else if (this._Type == "한글내역")
            sFileContent = CreateReceiptHtml_3();

        else if (this._Type == "영문내역")
            sFileContent = CreateReceiptHtml_4();

        else if (this._Type == "사무메이트확인서")
            sFileContent = CreateReceiptHtml_5();

        else if (this._Type == "사무메이트영문확인서")
            sFileContent = CreateReceiptHtml_6();

        else if (this._Type == "납부내역서")
            sFileContent = CreateReceiptHtml_7();

        //-- Encoding
        encUTF8 = System.Text.Encoding.UTF8;
        encKor = System.Text.Encoding.GetEncoding("euc-kr");

        //-- Convert Encoding From "UTF8" To "euc-kr"
        byteUTF8 = Encoding.UTF8.GetBytes(sFileContent);
        byteKor = Encoding.Convert(encUTF8, encKor, byteUTF8);

        encKor.GetString(byteKor);

        return encKor.GetString(byteKor);
    }

    /// <summary>
    /// 봉사확인서 HTML 내용 만들기 (한글 확인서)
    /// </summary>
    /// <returns></returns>
    private string CreateReceiptHtml_1()
    {
        string sHtml = string.Empty;

        #region //-- 상단, 스크립트, 스타일
        sHtml = " <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>                                                                      ".TrimEnd()
        + " <html>                                                                                                                                    ".TrimEnd()
        + " <head>                                                                                                                                    ".TrimEnd()
        + " <title>한국컴패션 사회봉사확인서</title>                                                                                                  ".TrimEnd()

        //ScriptX프린트 설정
            //프린트설정
        + " <script language='javascript' type='text/javascript'>                   ".TrimEnd()
            //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        + " function setPrint()                                                     ".TrimEnd()
        + " {                                                                       ".TrimEnd()
        + "     if(factory.printing == null)                                        ".TrimEnd()
        + "     {                                                                   ".TrimEnd()
        + "         alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.'); ".TrimEnd()
        + "         return;                                                         ".TrimEnd()
        + "     }                                                                   ".TrimEnd()
        + "     factory.printing.header       = '';                                 ".TrimEnd()
        + "     factory.printing.footer       = '';                                 ".TrimEnd()
        + "     factory.printing.portrait     = true;                               ".TrimEnd() //true 세로출력,false가로출력
        + "     factory.printing.leftMargin   = 10;                                 ".TrimEnd()
        + "     factory.printing.topMargin    = 10;                                 ".TrimEnd()
        + "     factory.printing.rightMargin  = 10;                                 ".TrimEnd()
        + "     factory.printing.bottomMargin = 10;                                 ".TrimEnd()
            //+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        + "     agree = confirm('현재 페이지를 출력하시겠습니까?');                 ".TrimEnd()
        + "     if (agree) factory.printing.Print(false, window)                    ".TrimEnd()
        + " }                                                                       ".TrimEnd()
        + " </script>                                                               ".TrimEnd()
            //프린트설정 끝

        + " <style type='text/css'>                                                                                                                   ".TrimEnd()
        + " /*body { margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;}*/                                                     ".TrimEnd()
        + " img {border:none;}                                                                                                                        ".TrimEnd()
        + " body, td {  font-family: '돋움', '바탕', '굴림'; font-size:10pt; color: #000000;}                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 소득세법 */                                                                                                                            ".TrimEnd()
        + " td.CodeTitle{ font-size:10pt; padding-bottom:5px;}                                                                                        ".TrimEnd()
        + " td.Code{ font-size:8pt; padding-bottom:5px;}                                                                                              ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 HEAD */                                                                                                                       ".TrimEnd()
        + " td.ContentHeadType{width: 96px; height:25px; text-align: center; background-color: silver;}                                               ".TrimEnd()
        + " td.ContentHeadCode{width: 46px; text-align: center; background-color: silver;}                                                            ".TrimEnd()
        + " td.ContentHeadDate{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadDesc{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadAmount{width: 98px; text-align: center; background-color: silver;}                                                          ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 CONTENT */                                                                                                                    ".TrimEnd()
        + " td.ContentItem{                                                                                                                           ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px dotted;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " td.ContentItemBottom{                                                                                                                     ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px solid;                                                                                                             ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* Title */                                                                                                                               ".TrimEnd()
        + " .Title{                                                                                                                                   ".TrimEnd()
        + " font-size: 12pt;                                                                                                                          ".TrimEnd()
        + " font-weight: bold;                                                                                                                        ".TrimEnd()
        + " border-bottom: gray 1px solid;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " /*  */                                                                                                                                    ".TrimEnd()
        + " .Gray_Table{                                                                                                                              ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:1px;                                                                                                                     ".TrimEnd()
        + " border-left-width:2px;                                                                                                                    ".TrimEnd()
        + " border-right-width:2px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head{                                                                                                                               ".TrimEnd()
        + " text-align: center;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head2{                                                                                                                               ".TrimEnd()
        + " text-align: left;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()

        + " .Gray_Item{                                                                                                                               ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " </style>                                                                                                                                  ".TrimEnd()
        + " </head>                                                                                                                                   ".TrimEnd();
        #endregion

        #region //-- 인적사항
        sHtml += "<body onload = 'setPrint()'; style='text-align: left;'>                                                                               ".TrimEnd()

        //ScriptX다운로드 경로 (프린트 설정시 필요)
        + " <object id=factory style='display:none' ".TrimEnd()
        + " classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ".TrimEnd()
        + " codebase=../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ".TrimEnd()
        + " </object> ".TrimEnd()
            //ScriptX다운로드 경로 끝

        + " <table style='width:700px; text-align:center;' cellpadding='0' cellspacing='0' border='0'>                                                  ".TrimEnd()
        + "	<tr>                                                                                                                                        ".TrimEnd()
        + "		<td>                                                                                                                                    ".TrimEnd()
        + "			<br/>                                                                                                                               ".TrimEnd()
        + "			<table style='width:680px;' cellpadding='0' cellspacing='0' border='0'>                                                             ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td>                                                                                                                        ".TrimEnd()
        + "						<img src='../images/compassion_logo.gif' width='200px' height='75px' alt=''/>                                           ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='20px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td style='text-align:left; height:30px;'>                                                                                  ".TrimEnd()
        + "						<span style='font-size: 8pt;'>발급번호 : " + DateTime.Now.ToString("yyyyMMddhhmmssfff") + "    </span>                 ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td style='text-align: center; height:50px;'>                                                                               ".TrimEnd()
        + "						<span style='font-size: 18pt; font-weight:bold;'>봉 사 활 동 확 인 서</span>                                            ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='50px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                            ".TrimEnd();

        #endregion

        #region //-- 기부 내용

        sHtml += "	 <table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'>                                         ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td colspan='4' class='Gray_Head2' style='font-size: 10pt;height: 30px; background-color: silver;font-weight:bold;'>1. 봉사자 인적사항</td>                                                                   ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr >                                                                                                                           ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>성&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;명</td> ".TrimEnd()
        + "					<td class='Gray_Item' style='width: 230px; padding-left:10px;'>&nbsp;" + this._SponsorName + "</td>                         ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; background-color: silver;'>주민등록번호</td>                                     ".TrimEnd()
        + "					<td class='Gray_Item' style='width: 230px; text-align:center;'>&nbsp;" + this.JuminID + "</td>                              ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>주&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;소</td> ".TrimEnd()
        + "					<td class='Gray_Item' colspan='3' style='padding-left:10px;'>&nbsp;" + this._SponsorAddress + "</td>                        ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                            ".TrimEnd()
        + "			<br />                                                                                                                              ".TrimEnd()
        + "			<br />                                                                                                                              ".TrimEnd()
        + "			<br />                                                                                                                              ".TrimEnd()
        + "			<table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'>                                                                          ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td colspan='4' class='Gray_Head2' style='font-size: 10pt;height: 30px; background-color: silver;font-weight:bold;' >2. 봉사기관 및 봉사활동내역</td>                                                         ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>봉사기관명</td>                                                         ".TrimEnd()
        + "					<td colspan='3' class='Gray_Item' style='padding-left:10px;'>&nbsp;사회복지법인 한국컴패션</td>                                                                         ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>봉&nbsp;사&nbsp;기&nbsp;간</td>                                         ".TrimEnd()
        + "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;" + this._VolunteerTerm + "</td>                                                                          ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>봉&nbsp;사&nbsp;시&nbsp;간</td>                                         ".TrimEnd()
        + "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;" + this._VolunteerTime + "</td>                                                                          ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>봉&nbsp;사&nbsp;내&nbsp;용</td>                                        ".TrimEnd()
        + "					<td colspan='3' class='Gray_Item' style='padding-left:10px;' >&nbsp;" + this._VolunteerContents + "</td>                                                         ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>담&nbsp;&nbsp;&nbsp;당&nbsp;&nbsp;&nbsp;자</td>                         ".TrimEnd()
        + "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;" + this.Charge + "</td>                                                                                 ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>연&nbsp;&nbsp;&nbsp;락&nbsp;&nbsp;&nbsp;처</td>                         ".TrimEnd()
        + "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;" + this.ChargeTelNo + "</td>                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "			</table>	                                                                                                                                                        ".TrimEnd()
        + "			                                                                                                                                                                    ".TrimEnd()
        + "			<br />                                                                                                                                                              ".TrimEnd()
        + "			<br />                                                                                                                                                              ".TrimEnd()
        + "			<br />                                                                                                                                                              ".TrimEnd()
        + "                                                                                                                                                                             ".TrimEnd();
        #endregion

        #region //-- 끝
        sHtml += "  <table style='width: 680px;' cellpadding='0' cellspacing='0' border='0'>                        ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='text-align: center; height: 30px;'>                                                                                                  ".TrimEnd()
        + "						<span style='font-size: 10pt; font-family: 바탕체; font-weight:bold;'>위와 같이 봉사활동 사실을 확인합니다.</span>                                      ".TrimEnd()
        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 70px;'></td>                                                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='text-align: Right; height: 20px;font-size: 10pt;'> " + this.PrintDate + "&nbsp;&nbsp;</td>                                                                          ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 70px;'></td>                                                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()

        + "					<td colspan='3' style='text-align: right; height: 30px;'>                                                                                                   ".TrimEnd()
        + "						<img src='../images/stamp3.gif' width='529px' height='78px' alt=''/>                                           ".TrimEnd()

        //+ "						사회복지법인 <span style='font-size: 16pt; font-weight:bold;'>한국컴패션</span>&nbsp;                                                                   ".TrimEnd()
            //+ "						대표 <span style='font-size: 16pt; font-weight:bold;'>서정인</span>                                                                                     ".TrimEnd()

        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td style='height:50px;'></td>                                                                                                                              ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 30px; text-align: left'>                                                                                                     ".TrimEnd()
        + "						<strong>Compassion Korea</strong> |                                                                                                                     ".TrimEnd()
        + "						<strong>한국컴패션</strong>                                                                                                                             ".TrimEnd()
        + "						110-717 서울시 종로구 인의동 112-1 동원빌딩 7층 한국컴패션                                                                                                  ".TrimEnd()
        + "						<br />                                                                                                                                                  ".TrimEnd()
        + "						TEL: 02-3668-3400 &nbsp; FAX: 02-3668-3456 &nbsp; EMAIL: info@compassion.or.kr                                                                          ".TrimEnd()
        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                                                            ".TrimEnd()
        + "		</td>                                                                                                                                                                   ".TrimEnd()
        + "	</tr>                                                                                                                                                                       ".TrimEnd()
        + "</table>                                                                                                                                                                     ".TrimEnd()
        + "                                                                                                                                                                             ".TrimEnd()
        + "</body>                                                                                                                                                                      ".TrimEnd()
        + "</html>                                                                                                                                                                      ".TrimEnd();
        #endregion


        #region  한글확인서
        sHtml_New.Remove(0, sHtml_New.Length);
        sHtml_New.AppendLine("  <html>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <head>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <title>봉사활동확인서</title>                                                                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("  <meta http-equiv='Content-Type' content='text/html; charset=euc-kr'>                                                                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");

        //ScriptX다운로드 경로 (프린트 설정시 필요)
        //sHtml_New.AppendLine(" <object id=factory style='display:none' ");
        //sHtml_New.AppendLine(" classid='clsid:1663ed61-23eb-11d2-b92f-008048fdd814' ");
        //sHtml_New.AppendLine(" codebase='http://www.compassion.or.kr/Files/ScriptX.cab#Version=6,1,431,8'> ");
        //sHtml_New.AppendLine(" </object> ");
        //ScriptX다운로드 경로 끝

        //ScriptX프린트 설정
        //프린트설정
        sHtml_New.AppendLine("  <script language='javascript' type='text/javascript'>                    ");
        //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        sHtml_New.AppendLine("   function setPrint()                                                      ");
        sHtml_New.AppendLine("   {                                                                       ");
        sHtml_New.AppendLine("      agree = confirm('현재 페이지를 출력하시겠습니까?');                                ");
        sHtml_New.AppendLine("      if (agree) window.print();                                                        ");
        //sHtml_New.AppendLine("      if(factory.printing == null)                                         ");
        //sHtml_New.AppendLine("       {                                                                  ");
        //sHtml_New.AppendLine("        alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.');   ");
        //sHtml_New.AppendLine("         return;                                                           ");
        //sHtml_New.AppendLine("       }                                                                    ");
        //sHtml_New.AppendLine("      factory.printing.header       = '';                                  ");
        //sHtml_New.AppendLine("       factory.printing.footer       = '';                                 ");
        //sHtml_New.AppendLine("     factory.printing.portrait     = true;                               ");//true 세로출력,false가로출력
        //sHtml_New.AppendLine("      factory.printing.leftMargin   = 10;                                  ");
        //sHtml_New.AppendLine("       factory.printing.topMargin    = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.rightMargin  = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.bottomMargin = 10;                                 ");
        //sHtml_New.AppendLine("       factory.printing.Print(false);                   ");
        sHtml_New.AppendLine("   }                                                                        ");
        sHtml_New.AppendLine("   </script>                                                               ");
        //프린트설정 끝

        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("  <body onload = 'setPrint()' style='font-family: 맑은 고딕;'>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");

        sHtml_New.AppendLine("  <table width='800' border='0' align='center' cellpadding='0' cellspacing='0'>                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td><img src='../../images/img001.jpg' width='800' height='100' /></td>                                                                                                                                                                                                                                                                                                               ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='60'><table width='750' border='0' align='center' cellpadding='0' cellspacing='0'>                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td><span style='font-size: 9pt; font-weight:bold;'>발급번호 : " + DateTime.Now.ToString("yyyyMMddhhmmssfff") + " </span></td>                                                                                                                                                                                                                                                                               ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("      </table></td>                                                                                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='80' align='center'><span style='font-size: 19pt; font-weight:bold;'>봉 사 활 동 확 인 서</span></td>                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='60' align='center'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                      ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td><table width='800' border='1' cellpadding='0' cellspacing='0' bordercolor='#000000' style='border-collapse: collapse;'>                                                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td height='40' colspan='4' bgcolor='#CCCCCC'><span style='font-size: 11pt; font-weight:bold;'>&nbsp;&nbsp;1. 봉사자 인적사항</span></td>                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("          </tr>                                                                                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td width='160' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>성 명</span></td>                                                                                                                                                                                                                                                               ");
        sHtml_New.AppendLine("          <td width='200' height='40' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;" + this._SponsorName + "</span></td>                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("          <td width='160' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>생년월일</span></td>                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("          <td width='275' height='40' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;" + this._BirthDate + "</span></td>                                                                                                                                                                                                                                                               ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>주 소</span></td>                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("          <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;" + this._SponsorAddress + "</span></td>                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("          </tr>                                                                                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("      </table></td>                                                                                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='50'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                     ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td><table width='800' border='1' cellpadding='0' cellspacing='0' bordercolor='#000000' style='border-collapse: collapse;'>                                                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td height='40' colspan='4' bgcolor='#CCCCCC'><span style='font-size: 11pt; font-weight:bold;'>&nbsp;&nbsp;2. 봉사기관 및 봉사활동내역</span></td>                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>봉사기관명</span></td>                                                                                                                                                                                                                                                                      ");
        sHtml_New.AppendLine("          <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;한국컴패션</span></td>                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td width='160' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>봉사기간</span></td>                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("          <td width='305' height='40' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;" + this._VolunteerTerm + "</span></td>                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td width='141' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>봉사시간</span></td>                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("          <td width='189' height='40' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;" + this._VolunteerTime + "</span></td>                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>봉사내용</span></td>                                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("          <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;컴패션 어린이/후원자 편지 번역</span></td>                                                                                                                                                                                                                                               ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td width='160' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>담당자</span></td>                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("          <td width='305' height='40' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;" + this.Charge + "</span></td>                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("          <td width='141' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>연락처</span></td>                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("          <td width='189' height='40' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;" + this.ChargeTelNo + "</span></td>                                                                                                                                                                                                                                                                 ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");
        sHtml_New.AppendLine("      </table></td>                                                                                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='80'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                     ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td align='center'><span style='font-size: 11pt;'>위와 같이 봉사활동 사실을 확인합니다.</span></td>                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='80'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                    ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td align='center'><span style='font-size: 11pt;'>" + this.PrintDate + "</span></td>                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                     ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        //sHtml_New.AppendLine("      <td align='center'><span style='font-size: 10pt;'>사회복지법인</span><span style='font-size: 12pt;'> 한국컴패션 </span><span style='font-size: 10pt;'>대표 서정인<img src='../../images/img002.jpg' width='118' height='114'></td>                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("      <td align='center'><div style='display: inline-block; position: relative;'><div style='position: relative; top: -50; left: 38px; font-size:13pt; font-weight: bold; float:left;'>사회복지법인 한국컴패션 대표 서정인 (인)</div></div><img src='../../images/img002.jpg' width='118' height='114'></td>                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20' style='padding-top:20px;'><span style='font-size: 9pt;'><font color='#0033FF'><b>한국컴패션</b></font> (04418) 서울시 용산구 한남대로 102-5 (한남동 723-41) 석전빌딩</span></td>                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'><span style='font-size: 9pt;'><font color='#0033FF'><b>후원상담/안내 :</b></font> 02-740-1000 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>팩스 :</b></font> 02-740-1001 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>이메일 :</b></font> info@compassion.or.kr &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>www.</b></font>compassion.or.kr</span></td>           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                                 ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        /*
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>Compassion USA Korea Office&nbsp;&nbsp;</b></font> 14515 Valley View Ave. Ste. #C Santa Fe Springs, CA90670</span></td>                                                                                                                                                                              ");
        sHtml_New.AppendLine("      </tr>                                                                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("      <tr>                                                                                                                                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>TEL :</b></font> 562-483-4300 &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>EMAIL :</b></font> info_us@compassion.or.kr</span></td>                                                                                                                                                                      ");
        sHtml_New.AppendLine("      </tr>                                                                                                                                                                                                                                                                                                                                                                           ");
        */
        sHtml_New.AppendLine("    </table>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("    </body>                                                                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("    </html>																																																																																																																																																																																						");


        #endregion


        return sHtml_New.ToString();
    }


    /// <summary>
    /// 봉사확인서 HTML 내용 만들기 (영문 확인서)
    /// </summary>
    /// <returns></returns>
    private string CreateReceiptHtml_2()
    {
        string sHtml = string.Empty;

        #region //-- 상단, 스크립트, 스타일
        sHtml = " <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>                                                                      ".TrimEnd()
        + " <html>                                                                                                                                    ".TrimEnd()
        + " <head>                                                                                                                                    ".TrimEnd()
        + " <title>한국컴패션 사회봉사확인서</title>                                                                                                  ".TrimEnd()

        //ScriptX프린트 설정
            //프린트설정
        + " <script language='javascript' type='text/javascript'>                   ".TrimEnd()
            //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        + " function setPrint()                                                     ".TrimEnd()
        + " {                                                                       ".TrimEnd()
        + "     if(factory.printing == null)                                        ".TrimEnd()
        + "     {                                                                   ".TrimEnd()
        + "         alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.'); ".TrimEnd()
        + "         return;                                                         ".TrimEnd()
        + "     }                                                                   ".TrimEnd()
        + "     factory.printing.header       = '';                                 ".TrimEnd()
        + "     factory.printing.footer       = '';                                 ".TrimEnd()
        + "     factory.printing.portrait     = true;                               ".TrimEnd() //true 세로출력,false가로출력
        + "     factory.printing.leftMargin   = 10;                                 ".TrimEnd()
        + "     factory.printing.topMargin    = 10;                                 ".TrimEnd()
        + "     factory.printing.rightMargin  = 10;                                 ".TrimEnd()
        + "     factory.printing.bottomMargin = 10;                                 ".TrimEnd()
            //+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        + "     agree = confirm('현재 페이지를 출력하시겠습니까?');                 ".TrimEnd()
        + "     if (agree) factory.printing.Print(false, window)                    ".TrimEnd()
        + " }                                                                       ".TrimEnd()
        + " </script>                                                               ".TrimEnd()
            //프린트설정 끝

        + " <style type='text/css'>                                                                                                                   ".TrimEnd()
        + " /*body { margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;}*/                                                     ".TrimEnd()
        + " img {border:none;}                                                                                                                        ".TrimEnd()
        + " body, td {  font-family: '돋움', '바탕', '굴림'; font-size:10pt; color: #000000;}                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 소득세법 */                                                                                                                            ".TrimEnd()
        + " td.CodeTitle{ font-size:10pt; padding-bottom:5px;}                                                                                        ".TrimEnd()
        + " td.Code{ font-size:8pt; padding-bottom:5px;}                                                                                              ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 HEAD */                                                                                                                       ".TrimEnd()
        + " td.ContentHeadType{width: 96px; height:25px; text-align: center; background-color: silver;}                                               ".TrimEnd()
        + " td.ContentHeadCode{width: 46px; text-align: center; background-color: silver;}                                                            ".TrimEnd()
        + " td.ContentHeadDate{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadDesc{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadAmount{width: 98px; text-align: center; background-color: silver;}                                                          ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 CONTENT */                                                                                                                    ".TrimEnd()
        + " td.ContentItem{                                                                                                                           ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px dotted;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " td.ContentItemBottom{                                                                                                                     ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px solid;                                                                                                             ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* Title */                                                                                                                               ".TrimEnd()
        + " .Title{                                                                                                                                   ".TrimEnd()
        + " font-size: 12pt;                                                                                                                          ".TrimEnd()
        + " font-weight: bold;                                                                                                                        ".TrimEnd()
        + " border-bottom: gray 1px solid;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " /*  */                                                                                                                                    ".TrimEnd()
        + " .Gray_Table{                                                                                                                              ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:1px;                                                                                                                     ".TrimEnd()
        + " border-left-width:2px;                                                                                                                    ".TrimEnd()
        + " border-right-width:2px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head{                                                                                                                               ".TrimEnd()
        + " text-align: center;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head2{                                                                                                                               ".TrimEnd()
        + " text-align: left;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()

        + " .Gray_Item{                                                                                                                               ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " </style>                                                                                                                                  ".TrimEnd()
        + " </head>                                                                                                                                   ".TrimEnd();
        #endregion

        #region //-- 인적사항
        sHtml += "<body onload = 'setPrint()'; style='text-align: left;'>                                                                               ".TrimEnd()

        //ScriptX다운로드 경로 (프린트 설정시 필요)
        + " <object id=factory style='display:none' ".TrimEnd()
        + " classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ".TrimEnd()
        + " codebase=../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ".TrimEnd()
        + " </object> ".TrimEnd()
            //ScriptX다운로드 경로 끝

        + " <table style='width:700px; text-align:center;' cellpadding='0' cellspacing='0' border='0'>                                                  ".TrimEnd()
        + "	<tr>                                                                                                                                        ".TrimEnd()
        + "		<td>                                                                                                                                    ".TrimEnd()
        + "			<br/>                                                                                                                               ".TrimEnd()
        + "			<table style='width:680px;' cellpadding='0' cellspacing='0' border='0'>                                                             ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td>                                                                                                                        ".TrimEnd()
        + "						<img src='../images/compassion_logo.gif' width='200px' height='75px' alt=''/>                                           ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='20px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td style='text-align:left; height:30px;'>                                                                                  ".TrimEnd()
        + "						<span style='font-size: 8pt;'>Issue No. " + DateTime.Now.ToString("yyyyMMddhhmmssfff") + "    </span>                 ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td style='text-align: center; height:50px;'>                                                                               ".TrimEnd()
        + "						<span style='font-size: 18pt; font-weight:bold;'>certificate of volunteer WORK</span>                                            ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='50px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                            ".TrimEnd();

        #endregion

        #region //-- 기부 내용

        sHtml += "	 <table style='width: 600px' cellpadding='0' cellspacing='0' border='0'>                                          ".TrimEnd()
        + "				<tr>                                                                                                                             ".TrimEnd()
        + "					<td  style='font-size: 10pt;height: 30px;'>".TrimEnd()
        + "This is to certify that " + this.SponsorNameENG + "  has served devotedly as a volunteer in Compassion Korea,  </br>  ".TrimEnd()
        + "a Non-governmental Christian organization working for child advocacy and ministry in poverty   </br>  ".TrimEnd()
        + "stricken places of the World. The above mentioned person has volunteered for " + this.VolunteerTimeENG + "</br>".TrimEnd()
        + "in total from " + this.VolunteerTermENG + " at our organization translating child/sponsor letters. </br>".TrimEnd()
        + "The volunteer’s sincere heart, devotion, and the promptness in finishing </br>".TrimEnd()
        + "the works given, is highly appreciated and is of great help to the neediest children in the world.</br>".TrimEnd()
        + "</td>".TrimEnd()
        + "</tr>".TrimEnd()
        + "			</table>".TrimEnd()
        + "".TrimEnd();
        #endregion

        #region //-- 끝
        sHtml += "  <table style='width: 680px; ' cellpadding='0' cellspacing='0' border='0'>                        ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 250px;'></td>                                                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='text-align: Right; height: 20px;font-size: 10pt;'> " + this.PrintDateENG + "</td>                                                                          ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 70px;'></td>                                                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='text-align: right; height: 30px;'>                                                                                                   ".TrimEnd()

        + "						<img src='../images/stamp3.gif' width='529px' height='78px' alt=''/>                                           ".TrimEnd()

        //+ "						사회복지법인 <span style='font-size: 16pt; font-weight:bold;'>한국컴패션</span>&nbsp;                                                                   ".TrimEnd()
            //+ "						대표 <span style='font-size: 16pt; font-weight:bold;'>서정인</span>                                                                                     ".TrimEnd()


        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td style='height:50px;'></td>                                                                                                                              ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 30px; text-align: left;font-size: 8pt;'>                                                                                                     ".TrimEnd()
        + "						<strong>Compassion Korea</strong> |                                                                                                                     ".TrimEnd()
        + "						<strong>한국컴패션</strong>                                                                                                                             ".TrimEnd()
        + "						110-717 서울시 종로구 인의동 112-1 동원빌딩 7층 한국컴패션                                                                                              ".TrimEnd()
        + "						<br />                                                                                                                                                  ".TrimEnd()
        + "						TEL: 02-3668-3400 &nbsp; FAX: 02-3668-3456 &nbsp; EMAIL: info@compassion.or.kr                                                                          ".TrimEnd()
        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                                                            ".TrimEnd()
        + "		</td>                                                                                                                                                                   ".TrimEnd()
        + "	</tr>                                                                                                                                                                       ".TrimEnd()
        + "</table>                                                                                                                                                                     ".TrimEnd()
        + "                                                                                                                                                                             ".TrimEnd()
        + "</body>                                                                                                                                                                      ".TrimEnd()
        + "</html>                                                                                                                                                                      ".TrimEnd();
        #endregion

        #region 영문확인서
        sHtml_New.Remove(0, sHtml_New.Length);

        sHtml_New.AppendLine("  <html>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <head>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <title>봉사활동확인서</title>                                                                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("  <meta http-equiv='Content-Type' content='text/html; charset=euc-kr'>                                                                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");

        //ScriptX프린트 설정
        //프린트설정
        sHtml_New.AppendLine("  <script language='javascript' type='text/javascript'>                    ");
        //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        sHtml_New.AppendLine("   function setPrint()                                                      ");
        sHtml_New.AppendLine("   {                                                                       ");
        sHtml_New.AppendLine("      agree = confirm('현재 페이지를 출력하시겠습니까?');                                ");
        sHtml_New.AppendLine("      if (agree) window.print();                                                        ");
        //sHtml_New.AppendLine("      if(factory.printing == null)                                         ");
        //sHtml_New.AppendLine("       {                                                                  ");
        //sHtml_New.AppendLine("        alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.');   ");
        //sHtml_New.AppendLine("         return;                                                           ");
        //sHtml_New.AppendLine("       }                                                                    ");
        //sHtml_New.AppendLine("      factory.printing.header       = '';                                  ");
        //sHtml_New.AppendLine("       factory.printing.footer       = '';                                 ");
        //sHtml_New.AppendLine("     factory.printing.portrait     = true;                               ");//true 세로출력,false가로출력
        //sHtml_New.AppendLine("      factory.printing.leftMargin   = 10;                                  ");
        //sHtml_New.AppendLine("       factory.printing.topMargin    = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.rightMargin  = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.bottomMargin = 10;                                 ");
        ////+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        //sHtml_New.AppendLine("       agree = confirm('현재 페이지를 출력하시겠습니까?');                  ");
        //sHtml_New.AppendLine("       if (agree) factory.printing.Print(false, window)                   ");
        sHtml_New.AppendLine("   }                                                                        ");
        sHtml_New.AppendLine("   </script>                                                               ");
        //프린트설정 끝



        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("  <body onload = 'setPrint()' style='font-family:Times New Roman'>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");
        //ScriptX다운로드 경로 (프린트 설정시 필요)
        //sHtml_New.AppendLine("  <object id=factory style='display:none' ");
        //sHtml_New.AppendLine(" classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ");
        //sHtml_New.AppendLine(" codebase=../../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ");
        //sHtml_New.AppendLine(" </object> ");
        //ScriptX다운로드 경로 끝

        sHtml_New.AppendLine("                                                                                                                                                                  ");
        sHtml_New.AppendLine("  <table width='800' border='0' align='center' cellpadding='0' cellspacing='0'>                                                                                   ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td><img src='../../images/img001.jpg' width='800' height='100' /></td>                                                                                           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='70' align='center'>&nbsp;</td>                                                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='60'><table width='750' border='0' align='center' cellpadding='0' cellspacing='0'>                                                               ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                      ");
        sHtml_New.AppendLine("          <td><span style='font-size: 10pt; font-weight:bold;'>Issue No.  " + DateTime.Now.ToString("yyyyMMddhhmmssfff") + " </span></td>                                                                  ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                     ");
        sHtml_New.AppendLine("      </table></td>                                                                                                                                               ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='50' align='center'><span style='font-size: 20pt; font-weight:bold;'>CERTIFICATE<strong> OF VOLUNTEER  WORK</strong></span></td>                 ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='80' align='center'>&nbsp;</td>                                                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td style='padding-left:40px;padding-right:40px'><span style='font-size: 12pt;text-align:justify; text-justify:inter-cluster;'> ");

        //sHtml_New.AppendLine("    This is to certify that " + this.SponsorNameENG + "  has served devotedly as a volunteer in Compassion Korea,  </br>     ");
        //sHtml_New.AppendLine("    a Non-governmental Christian organization working for child advocacy and ministry in poverty   </br>     ");
        //sHtml_New.AppendLine("    stricken places of the World. The above mentioned person has volunteered for " + this.VolunteerTimeENG + "</br>   ");
        //sHtml_New.AppendLine("    in total from " + this.VolunteerTermENG + " at our organization translating child letters to sponsors or</br>   ");
        //sHtml_New.AppendLine("    vice versa. The volunteer’s sincere heart, devotion, and the promptness in finishing the works given</br>   ");
        //sHtml_New.AppendLine("    , is highly appreciated and is of great help to the neediest children in the world</br>   ");

        sHtml_New.AppendLine("This is to certify that " + this.SponsorNameENG + "  has served devotedly as a volunteer in Compassion Korea, ");
        sHtml_New.AppendLine("a Non-governmental Christian organization working for child advocacy and ministry in poverty ");
        sHtml_New.AppendLine("stricken places of the World. The above mentioned person has volunteered for " + this.VolunteerTimeENG + " ");
        sHtml_New.AppendLine("in total from " + this.VolunteerTermENG + " at our organization translating child/sponsor letters. ");
        sHtml_New.AppendLine("The volunteer’s sincere heart, devotion, and the promptness in finishing the works given ");
        sHtml_New.AppendLine(", is highly appreciated and is of great help to the neediest children in the world. ");



        sHtml_New.AppendLine("     </span></td>                                                                                                        ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='250'>&nbsp;</td>                                                                                                                                ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td>&nbsp;</td>                                                                                                                                             ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td align='right' style='padding-right:45px'><p align='right'><strong>" + this.PrintDateENG + "</strong></p></td>                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='20'>&nbsp;</td>                                                                                                                                 ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td align='right'><img src='../../images/img005.jpg' width='184' height='49' style='display:block;'><div style='display:inline-block; text-align:center; padding-right:20px;'><span style='font-size: 12pt; display:block; border-bottom:1px solid #000000;'>Justin Suh</span><span style='font-size: 14pt; display:block;'>CEO/President</span><span style='font-size: 15pt; display:block;'>Compassion Korea </span></div></td>                                                                               ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");

        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='50'>&nbsp;</td>                                                                                                                                ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");


        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20' style='padding-top:20px;'><span style='font-size: 9pt; font-family: 맑은 고딕;'><font color='#0033FF'><b>한국컴패션</b></font> (04418) 서울시 용산구 한남대로 102-5 (한남동 723-41) 석전빌딩</span></td>                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'><span style='font-size: 9pt; font-family: 맑은 고딕;'><font color='#0033FF'><b>후원상담/안내 :</b></font> 02-740-1000 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>팩스 :</b></font> 02-740-1001 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>이메일 :</b></font> info@compassion.or.kr &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>www.</b></font>compassion.or.kr</span></td>           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                                 ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        /*
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>Compassion USA Korea Office&nbsp;&nbsp;</b></font> 14515 Valley View Ave. Ste. #C Santa Fe Springs, CA90670</span></td>                                                                                                                                                                              ");
        sHtml_New.AppendLine("      </tr>                                                                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("      <tr>                                                                                                                                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>TEL :</b></font> 562-483-4300 &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>EMAIL :</b></font> info_us@compassion.or.kr</span></td>                                                                                             ");
        sHtml_New.AppendLine("      </tr>                                                                                                                                                                                                                                                                                                                                                                           ");
        */
        sHtml_New.AppendLine("  </table>                                                                                                                                                        ");
        sHtml_New.AppendLine("                                                                                                                                                                  ");
        sHtml_New.AppendLine("  </body>                                                                                                                                                         ");
        sHtml_New.AppendLine("  </html>																																																																													");

        #endregion

        return sHtml_New.ToString();


    }


    /// <summary>
    /// 봉사확인서 HTML 내용 만들기 (한글 내역)
    /// </summary>
    /// <returns></returns>
    private string CreateReceiptHtml_3()
    {
        string sHtml = string.Empty;

        #region //-- 상단, 스크립트, 스타일
        sHtml = " <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>                                                                      ".TrimEnd()
        + " <html>                                                                                                                                    ".TrimEnd()
        + " <head>                                                                                                                                    ".TrimEnd()
        + " <title>한국컴패션 사회봉사확인서</title>                                                                                                  ".TrimEnd()

        //ScriptX프린트 설정
            //프린트설정
        + " <script language='javascript' type='text/javascript'>                   ".TrimEnd()
            //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        + " function setPrint()                                                     ".TrimEnd()
        + " {                                                                       ".TrimEnd()
        + "     if(factory.printing == null)                                        ".TrimEnd()
        + "     {                                                                   ".TrimEnd()
        + "         alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.'); ".TrimEnd()
        + "         return;                                                         ".TrimEnd()
        + "     }                                                                   ".TrimEnd()
        + "     factory.printing.header       = '';                                 ".TrimEnd()
        + "     factory.printing.footer       = '';                                 ".TrimEnd()
        + "     factory.printing.portrait     = true;                               ".TrimEnd() //true 세로출력,false가로출력
        + "     factory.printing.leftMargin   = 10;                                 ".TrimEnd()
        + "     factory.printing.topMargin    = 10;                                 ".TrimEnd()
        + "     factory.printing.rightMargin  = 10;                                 ".TrimEnd()
        + "     factory.printing.bottomMargin = 10;                                 ".TrimEnd()
            //+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        + "     agree = confirm('현재 페이지를 출력하시겠습니까?');                 ".TrimEnd()
        + "     if (agree) factory.printing.Print(false, window)                    ".TrimEnd()
        + " }                                                                       ".TrimEnd()
        + " </script>                                                               ".TrimEnd()
            //프린트설정 끝

        + " <style type='text/css'>                                                                                                                   ".TrimEnd()
        + " /*body { margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;}*/                                                     ".TrimEnd()
        + " img {border:none;}                                                                                                                        ".TrimEnd()
        + " body, td {  font-family: '돋움', '바탕', '굴림'; font-size:10pt; color: #000000;}                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 소득세법 */                                                                                                                            ".TrimEnd()
        + " td.CodeTitle{ font-size:10pt; padding-bottom:5px;}                                                                                        ".TrimEnd()
        + " td.Code{ font-size:8pt; padding-bottom:5px;}                                                                                              ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 HEAD */                                                                                                                       ".TrimEnd()
        + " td.ContentHeadType{width: 96px; height:25px; text-align: center; background-color: silver;}                                               ".TrimEnd()
        + " td.ContentHeadCode{width: 46px; text-align: center; background-color: silver;}                                                            ".TrimEnd()
        + " td.ContentHeadDate{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadDesc{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadAmount{width: 98px; text-align: center; background-color: silver;}                                                          ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 CONTENT */                                                                                                                    ".TrimEnd()
        + " td.ContentItem{                                                                                                                           ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px dotted;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " td.ContentItemBottom{                                                                                                                     ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px solid;                                                                                                             ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* Title */                                                                                                                               ".TrimEnd()
        + " .Title{                                                                                                                                   ".TrimEnd()
        + " font-size: 12pt;                                                                                                                          ".TrimEnd()
        + " font-weight: bold;                                                                                                                        ".TrimEnd()
        + " border-bottom: gray 1px solid;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " /*  */                                                                                                                                    ".TrimEnd()
        + " .Gray_Table{                                                                                                                              ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:1px;                                                                                                                     ".TrimEnd()
        + " border-left-width:2px;                                                                                                                    ".TrimEnd()
        + " border-right-width:2px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head{                                                                                                                               ".TrimEnd()
        + " text-align: center;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head2{                                                                                                                               ".TrimEnd()
        + " text-align: left;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()

        + " .Gray_Item{                                                                                                                               ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " </style>                                                                                                                                  ".TrimEnd()
        + " </head>                                                                                                                                   ".TrimEnd();
        #endregion

        #region //-- 인적사항
        sHtml += "<body onload = 'setPrint()'; style='text-align: left;'>                                                                               ".TrimEnd()

        //ScriptX다운로드 경로 (프린트 설정시 필요)
        + " <object id=factory style='display:none' ".TrimEnd()
        + " classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ".TrimEnd()
        + " codebase=../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ".TrimEnd()
        + " </object> ".TrimEnd()
            //ScriptX다운로드 경로 끝

        + " <table style='width:700px; text-align:center;' cellpadding='0' cellspacing='0' border='0'>                                                  ".TrimEnd()
        + "	<tr>                                                                                                                                        ".TrimEnd()
        + "		<td>                                                                                                                                    ".TrimEnd()
        + "			<br/>                                                                                                                               ".TrimEnd()
        + "			<table style='width:680px;' cellpadding='0' cellspacing='0' border='0'>                                                             ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td>                                                                                                                        ".TrimEnd()
        + "						<img src='../images/compassion_logo.gif' width='200px' height='75px' alt=''/>                                           ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='20px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td style='text-align: center; height:50px;'>                                                                               ".TrimEnd()
        + "						<span style='font-size: 18pt; font-weight:bold;'>봉사활동상세내역</span>                                            ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='50px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                            ".TrimEnd();

        #endregion

        #region //-- 기부 내용

        sHtml += "	 <table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'>                                          ".TrimEnd()
        + "				<tr>                                                                                                                             ".TrimEnd()
        + "					<td class='Gray_Head' style='font-size: 13pt;height: 30px; width: 100px; background-color: silver;'>번호</td>                                                      ".TrimEnd()
        + "					<td class='Gray_Head' style='font-size: 13pt;height: 30px; width: 360px; background-color: silver;'>제목</td>                                                      ".TrimEnd()
        + "					<td class='Gray_Head' style='font-size: 13pt;height: 30px; width: 120px; background-color: silver;'>봉사일</td>                                                      ".TrimEnd()
        + "					<td class='Gray_Head' style='font-size: 13pt;height: 30px; width: 100px; background-color: silver;'>시간(분)</td>                                                      ".TrimEnd()
        + "			    </tr>                                                                                                                            ".TrimEnd();

        countOrderNo = 1;
        foreach (DataRow drRow in dsMateActivity.Tables[0].Rows)
        {

            sHtml += "				<tr style='text-align: center;'>                                                                                                                             ".TrimEnd()

             //+ "					<td class='Gray_Item' style='font-size: 10pt;height: 20px;'>" + drRow["OrderNo"].ToString() + "     </td>                                                      ".TrimEnd()

             + "					<td class='Gray_Item' style='font-size: 10pt;height: 20px;'>" + countOrderNo.ToString() + "     </td>                                                      ".TrimEnd()

             + "					<td class='Gray_Item' style='font-size: 10pt;height: 20px;'>" + drRow["MateTransKorType"].ToString() + "      </td>                                                      ".TrimEnd()

             + "					<td class='Gray_Item' style='font-size: 10pt;height: 20px;'>" + drRow["WorkingEnd"].ToString() + "     </td>                                                      ".TrimEnd()

             + "					<td class='Gray_Item' style='font-size: 10pt;height: 20px;'>" + drRow["mm"].ToString() + "      </td>                                                      ".TrimEnd()

             + "			    </tr>                                                                                                                            ".TrimEnd();

            countOrderNo++;
        }
        sHtml += "			</table>                                                                                                                            ".TrimEnd()
       + "                                                                                                                                                                             ".TrimEnd();
        #endregion

        #region //-- 끝
        sHtml += " 	</td>                                                                                                                                                                   ".TrimEnd()
        + "	</tr>                                                                                                                                                                       ".TrimEnd()
        + "</table>                                                                                                                                                                     ".TrimEnd()
        + "                                                                                                                                                                             ".TrimEnd()
        + "</body>                                                                                                                                                                      ".TrimEnd()
        + "</html>                                                                                                                                                                      ".TrimEnd();
        #endregion

        #region 한글 내역

        sHtml_New.Remove(0, sHtml_New.Length);


        sHtml_New.AppendLine("  <html>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <head>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <title>봉사활동확인서</title>                                                                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("  <meta http-equiv='Content-Type' content='text/html; charset=euc-kr'>                                                                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");

        //ScriptX프린트 설정
        //프린트설정
        sHtml_New.AppendLine("  <script language='javascript' type='text/javascript'>                    ");
        //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        sHtml_New.AppendLine("   function setPrint()                                                      ");
        sHtml_New.AppendLine("   {                                                                       ");
        sHtml_New.AppendLine("      agree = confirm('현재 페이지를 출력하시겠습니까?');                                ");
        sHtml_New.AppendLine("      if (agree) window.print();                                                        ");
        //sHtml_New.AppendLine("      if(factory.printing == null)                                         ");
        //sHtml_New.AppendLine("       {                                                                  ");
        //sHtml_New.AppendLine("        alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.');   ");
        //sHtml_New.AppendLine("         return;                                                           ");
        //sHtml_New.AppendLine("       }                                                                    ");
        //sHtml_New.AppendLine("      factory.printing.header       = '';                                  ");
        //sHtml_New.AppendLine("       factory.printing.footer       = '';                                 ");
        //sHtml_New.AppendLine("     factory.printing.portrait     = true;                               ");//true 세로출력,false가로출력
        //sHtml_New.AppendLine("      factory.printing.leftMargin   = 10;                                  ");
        //sHtml_New.AppendLine("       factory.printing.topMargin    = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.rightMargin  = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.bottomMargin = 10;                                 ");
        ////+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        //sHtml_New.AppendLine("       agree = confirm('현재 페이지를 출력하시겠습니까?');                  ");
        //sHtml_New.AppendLine("       if (agree) factory.printing.Print(false, window)                   ");
        sHtml_New.AppendLine("   }                                                                        ");
        sHtml_New.AppendLine("   </script>                                                               ");
        //프린트설정 끝



        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("  <body onload = 'setPrint()' style='font-family: 맑은 고딕;'>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");
        //ScriptX다운로드 경로 (프린트 설정시 필요)
        //sHtml_New.AppendLine("  <object id=factory style='display:none' ");
        //sHtml_New.AppendLine(" classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ");
        //sHtml_New.AppendLine(" codebase=../../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ");
        //sHtml_New.AppendLine(" </object> ");
        //ScriptX다운로드 경로 끝
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("  <table width='800' border='0' align='center' cellpadding='0' cellspacing='0'>                                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td><img src='../../images/img001.jpg' width='800' height='100' /></td>                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='50' align='center'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");


        sHtml_New.AppendLine(" 	<tr>                                                                                                                         ");
        sHtml_New.AppendLine(" 		<td style='text-align: center; height:50px;'>                                                                                 ");
        sHtml_New.AppendLine(" 				<span style='font-size: 21pt; font-weight:bold;'>봉사활동상세내역</span>                                              ");
        sHtml_New.AppendLine(" 			</td>                                                                                                                       ");
        sHtml_New.AppendLine(" 		</tr>                                                                                                                             ");

        sHtml_New.AppendLine(" 	<tr>                                                                                                                         ");
        sHtml_New.AppendLine(" 		<td style='text-align: right; height:30px;'>                                                                                 ");
        sHtml_New.AppendLine("      <table width='800' border='0' cellpadding='0' cellspacing='0' bordercolor='#000000'><tr><td width='650px'></td><td width='150px' style='text-align: left;'>                                                                                                                                                                                                                                                                                                    ");
        sHtml_New.AppendLine(" 				<span style='font-size: 11pt; '>성    명 : "+ this._SponsorName +"</span></br>                                              ");
        sHtml_New.AppendLine(" 				<span style='font-size: 11pt; '>생년월일 : " + this._BirthDate + "</span>                                              ");
        sHtml_New.AppendLine(" 			</td></tr></table>                                                                                                                       ");
        sHtml_New.AppendLine(" 			</td>                                                                                                                       ");
        sHtml_New.AppendLine(" 		</tr>                                                                                                                             ");


        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='30' align='center'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");

        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td><table width='800' border='1' cellpadding='0' cellspacing='0' bordercolor='#000000' style='border-collapse: collapse;'>                                                                                                                                                                                                                                                                                                    ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                                      ");
        sHtml_New.AppendLine("          <td width='121' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'><b>번호</b></span></td>                                                                                                                                                                                                                                                                 ");
        sHtml_New.AppendLine("          <td width='367' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'><b>봉사내용</b></span></td>                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("          <td width='138' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'><b>봉사일</b></span></td>                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("          <td width='82' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'><b>시간(분)</b></span></td>                                                                                                                                                                                                                                                                      ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                                     ");

        countOrderNo = 1;

        foreach (DataRow drRow in dsMateActivity.Tables[0].Rows)
        {
            sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                                      ");
            //sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'>" + drRow["OrderNo"].ToString() + "</span></td>                                                                                                                                                                                                                                                                                        ");
            sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>" + countOrderNo.ToString() + "</span></td>                                                                                                                                                                                                                                                                                        ");
            sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>" + drRow["MateTransKorType"].ToString() + " </span></td>                                                                                                                                                                                                                                                                                      ");
            sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>" + drRow["WorkingEnd"].ToString() + "</span></td>                                                                                                                                                                                                                                                                     ");
            sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>" + drRow["mm"].ToString() + " </span></td>                                                                                                                                                                                                                                                                                           ");
            sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                                     ");

            countOrderNo++;
        }

        sHtml_New.AppendLine("         </table></td>                                                                                                                                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='100'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        //sHtml_New.AppendLine("      <td align='right'><span style='font-size: 10pt;'>사회복지법인</span><span style='font-size: 12pt;'> 한국컴패션 </span><span style='font-size: 10pt;'>대표 서정인<img src='../../images/img002.jpg' width='118' height='114'></td>                                                                                                                                                                                                                                                                                                               ");
        sHtml_New.AppendLine("      <td align='center'><div style='display: inline-block; position: relative;'><div style='position: relative; top: -50; left: 38px; font-size:13pt; font-weight: bold; float:left;'>사회복지법인 한국컴패션 대표 서정인 (인)</div></div><img src='../../images/img002.jpg' width='118' height='114'></td>                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20' style='padding-top:20px;'><span style='font-size: 9pt;'><font color='#0033FF'><b>한국컴패션</b></font> (04418) 서울시 용산구 한남대로 102-5 (한남동 723-4) 석전빌딩</span></td>                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'><span style='font-size: 9pt;'><font color='#0033FF'><b>후원상담/안내 :</b></font> 02-740-1000 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>팩스 :</b></font> 02-740-1001 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>이메일 :</b></font> info@compassion.or.kr &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>www.</b></font>compassion.or.kr</span></td>           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        /*
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                                 ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>Compassion USA Korea Office&nbsp;&nbsp;</b></font> 14515 Valley View Ave. Ste. #C Santa Fe Springs, CA90670</span></td>                                                                                                                                                                              ");
        sHtml_New.AppendLine("      </tr>                                                                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("      <tr>                                                                                                                                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>TEL :</b></font> 562-483-4300 &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>EMAIL :</b></font> info_us@compassion.or.kr</span></td>                                                                                             ");
        sHtml_New.AppendLine("      </tr>      ");       
        */
        sHtml_New.AppendLine("  </table>                                                                                                                                                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("  </body>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("  </html>                                                                                                                                                          																																																																																																																");

        #endregion

        return sHtml_New.ToString();
    }

    /// <summary>
    /// 봉사확인서 HTML 내용 만들기 (영문 내역)
    /// </summary>
    /// <returns></returns>
    private string CreateReceiptHtml_4()
    {
        string sHtml = string.Empty;

        #region //-- 상단, 스크립트, 스타일
        sHtml = " <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>                                                                      ".TrimEnd()
        + " <html>                                                                                                                                    ".TrimEnd()
        + " <head>                                                                                                                                    ".TrimEnd()
        + " <title>한국컴패션 사회봉사확인서</title>                                                                                                  ".TrimEnd()

        //ScriptX프린트 설정
            //프린트설정
        + " <script language='javascript' type='text/javascript'>                   ".TrimEnd()
            //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        + " function setPrint()                                                     ".TrimEnd()
        + " {                                                                       ".TrimEnd()
        + "     if(factory.printing == null)                                        ".TrimEnd()
        + "     {                                                                   ".TrimEnd()
        + "         alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.'); ".TrimEnd()
        + "         return;                                                         ".TrimEnd()
        + "     }                                                                   ".TrimEnd()
        + "     factory.printing.header       = '';                                 ".TrimEnd()
        + "     factory.printing.footer       = '';                                 ".TrimEnd()
        + "     factory.printing.portrait     = true;                               ".TrimEnd() //true 세로출력,false가로출력
        + "     factory.printing.leftMargin   = 10;                                 ".TrimEnd()
        + "     factory.printing.topMargin    = 10;                                 ".TrimEnd()
        + "     factory.printing.rightMargin  = 10;                                 ".TrimEnd()
        + "     factory.printing.bottomMargin = 10;                                 ".TrimEnd()
            //+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        + "     agree = confirm('현재 페이지를 출력하시겠습니까?');                 ".TrimEnd()
        + "     if (agree) factory.printing.Print(false, window)                    ".TrimEnd()
        + " }                                                                       ".TrimEnd()
        + " </script>                                                               ".TrimEnd()
            //프린트설정 끝

        + " <style type='text/css'>                                                                                                                   ".TrimEnd()
        + " /*body { margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;}*/                                                     ".TrimEnd()
        + " img {border:none;}                                                                                                                        ".TrimEnd()
        + " body, td {  font-family: '돋움', '바탕', '굴림'; font-size:10pt; color: #000000;}                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 소득세법 */                                                                                                                            ".TrimEnd()
        + " td.CodeTitle{ font-size:10pt; padding-bottom:5px;}                                                                                        ".TrimEnd()
        + " td.Code{ font-size:8pt; padding-bottom:5px;}                                                                                              ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 HEAD */                                                                                                                       ".TrimEnd()
        + " td.ContentHeadType{width: 96px; height:25px; text-align: center; background-color: silver;}                                               ".TrimEnd()
        + " td.ContentHeadCode{width: 46px; text-align: center; background-color: silver;}                                                            ".TrimEnd()
        + " td.ContentHeadDate{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadDesc{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadAmount{width: 98px; text-align: center; background-color: silver;}                                                          ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 CONTENT */                                                                                                                    ".TrimEnd()
        + " td.ContentItem{                                                                                                                           ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px dotted;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " td.ContentItemBottom{                                                                                                                     ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px solid;                                                                                                             ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* Title */                                                                                                                               ".TrimEnd()
        + " .Title{                                                                                                                                   ".TrimEnd()
        + " font-size: 12pt;                                                                                                                          ".TrimEnd()
        + " font-weight: bold;                                                                                                                        ".TrimEnd()
        + " border-bottom: gray 1px solid;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " /*  */                                                                                                                                    ".TrimEnd()
        + " .Gray_Table{                                                                                                                              ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:1px;                                                                                                                     ".TrimEnd()
        + " border-left-width:2px;                                                                                                                    ".TrimEnd()
        + " border-right-width:2px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head{                                                                                                                               ".TrimEnd()
        + " text-align: center;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head2{                                                                                                                               ".TrimEnd()
        + " text-align: left;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()

        + " .Gray_Item{                                                                                                                               ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " </style>                                                                                                                                  ".TrimEnd()
        + " </head>                                                                                                                                   ".TrimEnd();
        #endregion

        #region //-- 인적사항
        sHtml += "<body onload = 'setPrint()'; style='text-align: left;'>                                                                               ".TrimEnd()

        //ScriptX다운로드 경로 (프린트 설정시 필요)
        + " <object id=factory style='display:none' ".TrimEnd()
        + " classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ".TrimEnd()
        + " codebase=../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ".TrimEnd()
        + " </object> ".TrimEnd()
            //ScriptX다운로드 경로 끝

        + " <table style='width:700px; text-align:center;' cellpadding='0' cellspacing='0' border='0'>                                                  ".TrimEnd()
        + "	<tr>                                                                                                                                        ".TrimEnd()
        + "		<td>                                                                                                                                    ".TrimEnd()
        + "			<br/>                                                                                                                               ".TrimEnd()
        + "			<table style='width:680px;' cellpadding='0' cellspacing='0' border='0'>                                                             ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td>                                                                                                                        ".TrimEnd()
        + "						<img src='../../images/compassion_logo.gif' width='200px' height='75px' alt=''/>                                           ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='20px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td style='text-align: center; height:50px;'>                                                                               ".TrimEnd()
        + "						<span style='font-size: 20pt; font-weight:bold;'>W o r k S h e e t</span>                                            ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='50px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                            ".TrimEnd();

        #endregion

        #region //-- 기부 내용

        sHtml += "	 <table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'>                                          ".TrimEnd()
        + "				<tr>                                                                                                                             ".TrimEnd()
        + "					<td class='Gray_Head' style='font-size: 13pt;height: 30px; width: 100px; background-color: silver;'>No.</td>                                                      ".TrimEnd()
        + "					<td class='Gray_Head' style='font-size: 13pt;height: 30px; width: 360px; background-color: silver;'>Type of work</td>                                                      ".TrimEnd()
        + "					<td class='Gray_Head' style='font-size: 13pt;height: 30px; width: 120px; background-color: silver;'>Date</td>                                                      ".TrimEnd()
        + "					<td class='Gray_Head' style='font-size: 13pt;height: 30px; width: 100px; background-color: silver;'>Hours</td>                                                      ".TrimEnd()
        + "			    </tr>                                                                                                                            ".TrimEnd();

        countOrderNo = 1;

        foreach (DataRow drRow in dsMateActivity.Tables[0].Rows)
        {

            sHtml += "				<tr style='text-align: center;'>                                                                                                                             ".TrimEnd()

             //+ "					<td class='Gray_Item' style='font-size: 10pt;height: 20px;'>" + drRow["OrderNo"].ToString() + "     </td>                                                      ".TrimEnd()

              + "					<td class='Gray_Item' style='font-size: 10pt;height: 20px;'>" + countOrderNo.ToString() + "     </td>                                                      ".TrimEnd()

             + "					<td class='Gray_Item' style='font-size: 10pt;height: 20px;'>" + drRow["MateTransKorType"].ToString() + "      </td>                                                      ".TrimEnd()

             + "					<td class='Gray_Item' style='font-size: 10pt;height: 20px;'>" + drRow["WorkingEnd"].ToString() + "     </td>                                                      ".TrimEnd()

             + "					<td class='Gray_Item' style='font-size: 10pt;height: 20px;'>" + drRow["mm"].ToString() + "      </td>                                                      ".TrimEnd()

             + "			    </tr>                                                                                                                            ".TrimEnd();

            countOrderNo++;
        }
        sHtml += "			</table>                                                                                                                            ".TrimEnd()
       + "                                                                                                                                                                             ".TrimEnd();
        #endregion

        #region //-- 끝
        sHtml += " 	</td>                                                                                                                                                                   ".TrimEnd()
        + "	</tr>                                                                                                                                                                       ".TrimEnd()
        + "</table>                                                                                                                                                                     ".TrimEnd()
        + "                                                                                                                                                                             ".TrimEnd()
        + "</body>                                                                                                                                                                      ".TrimEnd()
        + "</html>                                                                                                                                                                      ".TrimEnd();
        #endregion

        #region 영문 내역

        sHtml_New.Remove(0, sHtml_New.Length);

        sHtml_New.AppendLine("  <html>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <head>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <title>봉사활동확인서</title>                                                                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("  <meta http-equiv='Content-Type' content='text/html; charset=euc-kr'>                                                                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");

        //ScriptX프린트 설정
        //프린트설정
        sHtml_New.AppendLine("  <script language='javascript' type='text/javascript'>                    ");
        //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        sHtml_New.AppendLine("   function setPrint()                                                      ");
        sHtml_New.AppendLine("   {                                                                       ");
        sHtml_New.AppendLine("      agree = confirm('현재 페이지를 출력하시겠습니까?');                                ");
        sHtml_New.AppendLine("      if (agree) window.print();                                                        ");
        //sHtml_New.AppendLine("      if(factory.printing == null)                                         ");
        //sHtml_New.AppendLine("       {                                                                  ");
        //sHtml_New.AppendLine("        alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.');   ");
        //sHtml_New.AppendLine("         return;                                                           ");
        //sHtml_New.AppendLine("       }                                                                    ");
        //sHtml_New.AppendLine("      factory.printing.header       = '';                                  ");
        //sHtml_New.AppendLine("       factory.printing.footer       = '';                                 ");
        //sHtml_New.AppendLine("     factory.printing.portrait     = true;                               ");//true 세로출력,false가로출력
        //sHtml_New.AppendLine("      factory.printing.leftMargin   = 10;                                  ");
        //sHtml_New.AppendLine("       factory.printing.topMargin    = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.rightMargin  = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.bottomMargin = 10;                                 ");
        ////+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        //sHtml_New.AppendLine("       agree = confirm('현재 페이지를 출력하시겠습니까?');                  ");
        //sHtml_New.AppendLine("       if (agree) factory.printing.Print(false, window)                   ");
        sHtml_New.AppendLine("   }                                                                        ");
        sHtml_New.AppendLine("   </script>                                                               ");
        //프린트설정 끝

        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("  <body onload = 'setPrint()' style='font-family:Times New Roman'>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");
        //ScriptX다운로드 경로 (프린트 설정시 필요)
        //sHtml_New.AppendLine("  <object id=factory style='display:none' ");
        //sHtml_New.AppendLine(" classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ");
        //sHtml_New.AppendLine(" codebase=../../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ");
        //sHtml_New.AppendLine(" </object> ");
        //ScriptX다운로드 경로 끝
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("  <table width='800' border='0' align='center' cellpadding='0' cellspacing='0'>                                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td><img src='../../images/img001.jpg' width='800' height='100' /></td>                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='50' align='center'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");

        sHtml_New.AppendLine(" 	<tr>                                                                                                                         ");
        sHtml_New.AppendLine(" 		<td style='text-align: center; height:50px;'>                                                                                 ");
        sHtml_New.AppendLine(" 				<span style='font-size: 22pt; font-weight:bold;'><strong>Volunteer Work Description</strong></span>                                              ");
        sHtml_New.AppendLine(" 			</td>                                                                                                                       ");
        sHtml_New.AppendLine(" 		</tr>                                                                                                                             ");

        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='30' align='center'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td><table width='800' border='1' cellpadding='0' cellspacing='0' bordercolor='#000000' style='border-collapse: collapse;'>                                                                                                                                                                                                                                                                                                    ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                                      ");
        sHtml_New.AppendLine("          <td width='121' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'><b>No.</b></span></td>                                                                                                                                                                                                                                                                 ");
        sHtml_New.AppendLine("          <td width='367' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'><b>Type of work</b></span></td>                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("          <td width='138' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'><b>Date</b></span></td>                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("          <td width='82' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'><b>Hours(Min)</b></span></td>                                                                                                                                                                                                                                                                      ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                                     ");

        countOrderNo = 1;

        foreach (DataRow drRow in dsMateActivity.Tables[0].Rows)
        {
            sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                                      ");
            sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'>" + countOrderNo.ToString() + "</span></td>                                                                                                                                                                                                                                                                                        ");
            sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'>" + drRow["MateTransType"].ToString() + " </span></td>                                                                                                                                                                                                                                                                                      ");
            sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'>" + drRow["WorkingEnd"].ToString() + "</span></td>                                                                                                                                                                                                                                                                     ");
            sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'>" + drRow["mm"].ToString() + " </span></td>                                                                                                                                                                                                                                                                                           ");
            sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                                     ");
            countOrderNo++;
        }

        sHtml_New.AppendLine("         </table></td>                                                                                                                                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='100'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        //sHtml_New.AppendLine("      <td align='right'><span style='font-size: 12pt;'>Compassion Korea </span><span style='font-size: 10pt;'>CEO/President Justin Suh</span><img src='../../images/img004.jpg' width='184' height='49'></td>                                                                                                                                                                                                                                                                                                               ");
        sHtml_New.AppendLine("      <td align='right'><img src='../../images/img005.jpg' width='184' height='49' style='display:block;'><div style='display:inline-block; text-align:center; padding-right:20px;'><span style='font-size: 12pt; display:block; border-bottom:1px solid #000000;'>Justin Suh</span><span style='font-size: 14pt; display:block;'>CEO/President</span><span style='font-size: 14pt; display:block;'>Compassion Korea </span></div></td>                                                                               ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");

        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='50' align='center'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");

        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20' style='padding-top:20px;'><span style='font-size: 9pt; font-family: 맑은 고딕;'><font color='#0033FF'><b>한국컴패션</b></font> (04418) 서울시 용산구 한남대로 102-5 (한남동 723-4) 석전빌딩</span></td>                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'><span style='font-size: 9pt; font-family: 맑은 고딕;'><font color='#0033FF'><b>후원상담/안내 :</b></font> 02-740-1000 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>팩스 :</b></font> 02-740-1001 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>이메일 :</b></font> info@compassion.or.kr &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>www.</b></font>compassion.or.kr</span></td>           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        /*
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                                 ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>Compassion USA Korea Office&nbsp;&nbsp;</b></font> 14515 Valley View Ave. Ste. #C Santa Fe Springs, CA90670</span></td>                                                                                                                                                                              ");
        sHtml_New.AppendLine("      </tr>                                                                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("      <tr>                                                                                                                                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>TEL :</b></font> 562-483-4300 &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>EMAIL :</b></font> info_us@compassion.or.kr</span></td>                                                                                             ");
        sHtml_New.AppendLine("      </tr>     "); 
        */
        sHtml_New.AppendLine("  </table>                                                                                                                                                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("  </body>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("  </html>                                                                                                                                                          																																																																																																																");

        #endregion

        return sHtml_New.ToString();

    }

    /// <summary>
    /// 사무메이트 봉사활동확인서 HTML 내용 만들기 (한글 확인서)
    /// </summary>
    /// <returns></returns>
    private string CreateReceiptHtml_5()
    {
        string sHtml = string.Empty;

        #region //-- 상단, 스크립트, 스타일
        sHtml = " <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>                                                                      ".TrimEnd()
        + " <html>                                                                                                                                    ".TrimEnd()
        + " <head>                                                                                                                                    ".TrimEnd()
        + " <title>한국컴패션 사회봉사확인서</title>                                                                                                  ".TrimEnd()

        //ScriptX프린트 설정
            //프린트설정
        + " <script language='javascript' type='text/javascript'>                   ".TrimEnd()
            //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        + " function setPrint()                                                     ".TrimEnd()
        + " {                                                                       ".TrimEnd()
        + "     if(factory.printing == null)                                        ".TrimEnd()
        + "     {                                                                   ".TrimEnd()
        + "         alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.'); ".TrimEnd()
        + "         return;                                                         ".TrimEnd()
        + "     }                                                                   ".TrimEnd()
        + "     factory.printing.header       = '';                                 ".TrimEnd()
        + "     factory.printing.footer       = '';                                 ".TrimEnd()
        + "     factory.printing.portrait     = true;                               ".TrimEnd() //true 세로출력,false가로출력
        + "     factory.printing.leftMargin   = 10;                                 ".TrimEnd()
        + "     factory.printing.topMargin    = 10;                                 ".TrimEnd()
        + "     factory.printing.rightMargin  = 10;                                 ".TrimEnd()
        + "     factory.printing.bottomMargin = 10;                                 ".TrimEnd()
            //+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        + "     agree = confirm('현재 페이지를 출력하시겠습니까?');                 ".TrimEnd()
        + "     if (agree) factory.printing.Print(false, window)                    ".TrimEnd()
        + " }                                                                       ".TrimEnd()
        + " </script>                                                               ".TrimEnd()
            //프린트설정 끝

        + " <style type='text/css'>                                                                                                                   ".TrimEnd()
        + " /*body { margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;}*/                                                     ".TrimEnd()
        + " img {border:none;}                                                                                                                        ".TrimEnd()
        + " body, td {  font-family: '맑은 고딕', '돋움', '바탕', '굴림'; font-size:10pt; color: #000000;}                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 소득세법 */                                                                                                                            ".TrimEnd()
        + " td.CodeTitle{ font-size:10pt; padding-bottom:5px;}                                                                                        ".TrimEnd()
        + " td.Code{ font-size:8pt; padding-bottom:5px;}                                                                                              ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 HEAD */                                                                                                                       ".TrimEnd()
        + " td.ContentHeadType{width: 96px; height:25px; text-align: center; background-color: silver;}                                               ".TrimEnd()
        + " td.ContentHeadCode{width: 46px; text-align: center; background-color: silver;}                                                            ".TrimEnd()
        + " td.ContentHeadDate{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadDesc{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadAmount{width: 98px; text-align: center; background-color: silver;}                                                          ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 CONTENT */                                                                                                                    ".TrimEnd()
        + " td.ContentItem{                                                                                                                           ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px dotted;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " td.ContentItemBottom{                                                                                                                     ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px solid;                                                                                                             ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* Title */                                                                                                                               ".TrimEnd()
        + " .Title{                                                                                                                                   ".TrimEnd()
        + " font-size: 12pt;                                                                                                                          ".TrimEnd()
        + " font-weight: bold;                                                                                                                        ".TrimEnd()
        + " border-bottom: gray 1px solid;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " /*  */                                                                                                                                    ".TrimEnd()
        + " .Gray_Table{                                                                                                                              ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:1px;                                                                                                                     ".TrimEnd()
        + " border-left-width:2px;                                                                                                                    ".TrimEnd()
        + " border-right-width:2px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head{                                                                                                                               ".TrimEnd()
        + " text-align: center;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head2{                                                                                                                               ".TrimEnd()
        + " text-align: left;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()

        + " .Gray_Item{                                                                                                                               ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " </style>                                                                                                                                  ".TrimEnd()
        + " </head>                                                                                                                                   ".TrimEnd();
        #endregion

        #region //-- 인적사항
        sHtml += "<body onload = 'setPrint()'; style='text-align: left;'>                                                                               ".TrimEnd()

        //ScriptX다운로드 경로 (프린트 설정시 필요)
        + " <object id=factory style='display:none' ".TrimEnd()
        + " classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ".TrimEnd()
        + " codebase=../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ".TrimEnd()
        + " </object> ".TrimEnd()
            //ScriptX다운로드 경로 끝

        + " <table style='width:700px; text-align:center;' cellpadding='0' cellspacing='0' border='0'>                                                  ".TrimEnd()
        + "	<tr>                                                                                                                                        ".TrimEnd()
        + "		<td>                                                                                                                                    ".TrimEnd()
        + "			<br/>                                                                                                                               ".TrimEnd()
        + "			<table style='width:680px;' cellpadding='0' cellspacing='0' border='0'>                                                             ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td>                                                                                                                        ".TrimEnd()
        + "						<img src='../images/compassion_logo.gif' width='200px' height='75px' alt=''/>                                           ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='10px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td style='text-align: center; height:50px;'>                                                                               ".TrimEnd()
        + "						<span style='font-size: 20pt; font-weight:bold;'>봉사활동확인서</span>                                            ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='10px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                            ".TrimEnd();

        #endregion

        #region //-- 기부 내용

        sHtml += "	 <table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'>                                         ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td colspan='2' class='Gray_Head2' style='font-size: 13pt;height: 30px; background-color: #083995; font-weight:bold; color: #FFFFFF;'>인적사항</td>                                                                   ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr >                                                                                                                           ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; font-weight:bold;'>성&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;명</td> ".TrimEnd()
        + "					<td class='Gray_Item' style='width: 230px; padding-left:10px;'>&nbsp;" + this._SponsorName + "</td>                         ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px;'>주민등록번호</td>                                      ".TrimEnd()
        + "					<td class='Gray_Item' style='width: 230px; text-align:center;'>&nbsp;" + this.JuminID + "</td>                            ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                            ".TrimEnd()
        + "			<br />                                                                                                                              ".TrimEnd()
        + "			<br />                                                                                                                              ".TrimEnd()
        + "			<br />                                                                                                                              ".TrimEnd()
        + "			<table class='Gray_Table' style='width: 680px' cellpadding='0' cellspacing='0' border='0'>                                                                          ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td colspan='2' class='Gray_Head2' style='font-size: 13pt;height: 30px; background-color: #083995; font-weight:bold; color: #FFFFFF;' >봉사활동내역</td>                                                         ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; font-weight:bold;'>봉사기관명</td>                                                         ".TrimEnd()
        + "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;한국컴패션</td>                                                                         ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd();

        foreach (DataRow drRow in dsMateActivity.Tables[0].Rows)
        {
            string _WorkTerm = drRow["WorkingStart"].ToString();

            sHtml += "				<tr>                                                                                                                                                            ".TrimEnd()
            + "					<td rowspan=" + dsMateActivity.Tables[0].Rows + " class='Gray_Head' style='width: 110px; height: 30px; font-weight:bold;'>봉&nbsp;사&nbsp;시&nbsp;간</td>                                         ".TrimEnd()
            + "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;" + this._VolunteerTerm + "</td>                                                                          ".TrimEnd()
            + "				</tr>                                                                                                                                                           ".TrimEnd();
        }


        sHtml += "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>봉&nbsp;사&nbsp;내&nbsp;용</td>                                        ".TrimEnd()
        + "					<td colspan='3' class='Gray_Item' style='padding-left:10px;' >&nbsp;" + this._VolunteerContents + "</td>                                                         ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>담&nbsp;&nbsp;&nbsp;당&nbsp;&nbsp;&nbsp;자</td>                         ".TrimEnd()
        + "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;" + this.Charge + "</td>                                                                                 ".TrimEnd()
        + "					<td class='Gray_Head' style='width: 110px; height: 30px; background-color: silver;'>연&nbsp;&nbsp;&nbsp;락&nbsp;&nbsp;&nbsp;처</td>                         ".TrimEnd()
        + "					<td class='Gray_Item' style='padding-left:10px;'>&nbsp;" + this.ChargeTelNo + "</td>                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "			</table>	                                                                                                                                                        ".TrimEnd()
        + "			                                                                                                                                                                    ".TrimEnd()
        + "			<br />                                                                                                                                                              ".TrimEnd()
        + "			<br />                                                                                                                                                              ".TrimEnd()
        + "			<br />                                                                                                                                                              ".TrimEnd()
        + "                                                                                                                                                                             ".TrimEnd();
        #endregion

        #region //-- 끝
        sHtml += "  <table style='width: 680px;' cellpadding='0' cellspacing='0' border='0'>                        ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='text-align: center; height: 30px;'>                                                                                                  ".TrimEnd()
        + "						<span style='font-size: 12pt; font-family: 바탕체; font-weight:bold;'>위와 같이 봉사활동 사실을 확인합니다.</span>                                      ".TrimEnd()
        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 70px;'></td>                                                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='text-align: Right; height: 20px;'> " + this.PrintDate + "&nbsp;&nbsp;</td>                                                                          ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 70px;'></td>                                                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()

        + "					<td colspan='3' style='text-align: right; height: 30px;'>                                                                                                   ".TrimEnd()
        + "						<img src='../images/stamp3.gif' width='529px' height='78px' alt=''/>                                           ".TrimEnd()

        //+ "						사회복지법인 <span style='font-size: 16pt; font-weight:bold;'>한국컴패션</span>&nbsp;                                                                   ".TrimEnd()
            //+ "						대표 <span style='font-size: 16pt; font-weight:bold;'>서정인</span>                                                                                     ".TrimEnd()

        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td style='height:50px;'></td>                                                                                                                              ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 30px; text-align: left'>                                                                                                     ".TrimEnd()
        + "						<strong>Compassion Korea</strong> |                                                                                                                     ".TrimEnd()
        + "						<strong>한국컴패션</strong>                                                                                                                             ".TrimEnd()
        + "						110-717 서울시 종로구 인의동 112-1 동원빌딩 7층 한국컴패션                                                                                              ".TrimEnd()
        + "						<br />                                                                                                                                                  ".TrimEnd()
        + "						TEL: 02-3668-3400 &nbsp; FAX: 02-3668-3456 &nbsp; EMAIL: info@compassion.or.kr                                                                          ".TrimEnd()
        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                                                            ".TrimEnd()
        + "		</td>                                                                                                                                                                   ".TrimEnd()
        + "	</tr>                                                                                                                                                                       ".TrimEnd()
        + "</table>                                                                                                                                                                     ".TrimEnd()
        + "                                                                                                                                                                             ".TrimEnd()
        + "</body>                                                                                                                                                                      ".TrimEnd()
        + "</html>                                                                                                                                                                      ".TrimEnd();
        #endregion


        #region  사무메이트 봉사활동확인서
        sHtml_New.Remove(0, sHtml_New.Length);
        sHtml_New.AppendLine("  <html>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <head>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <title>봉사활동확인서</title>                                                                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("  <meta http-equiv='Content-Type' content='text/html; charset=euc-kr'>                                                                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");

        //ScriptX프린트 설정
        //프린트설정
        sHtml_New.AppendLine("  <script language='javascript' type='text/javascript'>                    ");
        //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        sHtml_New.AppendLine("   function setPrint()                                                      ");
        sHtml_New.AppendLine("   {                                                                       ");
        sHtml_New.AppendLine("      agree = confirm('현재 페이지를 출력하시겠습니까?');                                ");
        sHtml_New.AppendLine("      if (agree) window.print();                                                        ");
        //sHtml_New.AppendLine("      if(factory.printing == null)                                         ");
        //sHtml_New.AppendLine("       {                                                                  ");
        //sHtml_New.AppendLine("        alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.');   ");
        //sHtml_New.AppendLine("         return;                                                           ");
        //sHtml_New.AppendLine("       }                                                                    ");
        //sHtml_New.AppendLine("      factory.printing.header       = '';                                  ");
        //sHtml_New.AppendLine("       factory.printing.footer       = '';                                 ");
        //sHtml_New.AppendLine("     factory.printing.portrait     = true;                               ");//true 세로출력,false가로출력
        //sHtml_New.AppendLine("      factory.printing.leftMargin   = 10;                                  ");
        //sHtml_New.AppendLine("       factory.printing.topMargin    = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.rightMargin  = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.bottomMargin = 10;                                 ");
        ////+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        //sHtml_New.AppendLine("       agree = confirm('현재 페이지를 출력하시겠습니까?');                  ");
        //sHtml_New.AppendLine("       if (agree) factory.printing.Print(false, window)                   ");
        sHtml_New.AppendLine("   }                                                                        ");
        sHtml_New.AppendLine("   </script>                                                               ");
        //프린트설정 끝



        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("  <body onload = 'setPrint()';>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");
        //ScriptX다운로드 경로 (프린트 설정시 필요)
        //sHtml_New.AppendLine("  <object id=factory style='display:none' ");
        //sHtml_New.AppendLine(" classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ");
        //sHtml_New.AppendLine(" codebase=../../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ");
        //sHtml_New.AppendLine(" </object> ");
        //ScriptX다운로드 경로 끝



        sHtml_New.AppendLine("  <table width='800' border='0' align='center' cellpadding='0' cellspacing='0'>                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td><img src='../../images/img001.jpg' width='800' height='100' /></td>                                                                                                                                                                                                                                                                                                               ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='5'><table width='750' border='0' align='center' cellpadding='0' cellspacing='0'>                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("      </table></td>                                                                                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='50' align='center'><span style='font-size: 20pt; font-weight:bold;'>봉사활동확인서</span></td>                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='10' align='center'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                      ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td><table width='800' border='1' cellpadding='0' cellspacing='0' bordercolor='#000000'>                                                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td height='40' colspan='2' bgcolor='#083995'><span style='font-size: 12pt; font-weight:bold; color: #FFFFFF;'>&nbsp;인적사항</span></td>                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("          </tr>                                                                                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td width='160' height='40' align='center'><span style='font-size: 12pt;font-weight:bold;'>성 명</span></td>                                                                                                                                                                                                                                                               ");
        sHtml_New.AppendLine("          <td width='640' height='40'><span style='font-size: 12pt;'>&nbsp;" + this._SponsorName + "</span></td>                                                                                                                                                                                                                                                                       ");

        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td width='160' height='40' align='center'><span style='font-size: 12pt;font-weight:bold;'>생년월일</span></td>                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("          <td width='640' height='40' bgcolor='#FFFFFF'><span style='font-size: 12pt;'>&nbsp;" + this._BirthDate + "</span></td>                                                                                                                                                                                                                                                               ");
        sHtml_New.AppendLine("          </tr>                                                                                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("      </table></td>                                                                                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='40'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                     ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td><table width='800' border='1' cellpadding='0' cellspacing='0' bordercolor='#000000'>                                                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td height='40' colspan='2' bgcolor='#083995'><span style='font-size: 12pt; font-weight:bold; color: #FFFFFF;'>&nbsp;봉사활동내역</span></td>                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td width='160' height='40' align='center'><span style='font-size: 12pt; font-weight:bold;'>봉사기관명</span></td>                                                                                                                                                                                                                                                                      ");
        sHtml_New.AppendLine("          <td width='640' height='40'><span style='font-size: 12pt;'>&nbsp;한국컴패션</span></td>                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        int nRow = dsMateActivity.Tables[0].Rows.Count + 1;

        int iCnt = 0;

        foreach (DataRow drRow in dsMateActivity.Tables[0].Rows)
        {
            sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
            if (iCnt == 0)
            {
                sHtml_New.AppendLine("          <td rowspan=" + nRow.ToString() + " width='160' height='40' align='center' font-weight:bold;><span style='font-size: 12pt;font-weight:bold;'>봉 사 시 간</span></td>                                                                                                                                                                                                                                                            ");
            }

            iCnt++;

            //string sWorkTerm = Convert.ToDateTime(drRow["WorkingStart"]).ToString("yyyy년 MM월 dd일 hh:mm");

            string sWorkTerm = Convert.ToDateTime(drRow["WorkingStart"]).ToString("yyyy년 MM월 dd일");

            if (drRow["WorkingStart"].ToString().IndexOf("오후") > 0)
            {
                if (Convert.ToInt32(Convert.ToDateTime(drRow["WorkingStart"]).ToString("hh")) != 12)
                    sWorkTerm += " " + Convert.ToString(Convert.ToInt32(Convert.ToDateTime(drRow["WorkingStart"]).ToString("hh")) + 12);

                else
                    sWorkTerm += Convert.ToString(Convert.ToDateTime(drRow["WorkingEnd"]).ToString("hh"));
            }

            else
                sWorkTerm += " " + Convert.ToString(Convert.ToDateTime(drRow["WorkingStart"]).ToString("hh"));

            sWorkTerm += ":" + Convert.ToString(Convert.ToDateTime(drRow["WorkingStart"]).ToString("mm"));

            //sWorkTerm += " - " + Convert.ToDateTime(drRow["WorkingEnd"]).ToString("hh:mm") + " (";

            sWorkTerm += " - ";

            if (drRow["WorkingEnd"].ToString().IndexOf("오후") > 0)
            {
                if (Convert.ToInt32(Convert.ToDateTime(drRow["WorkingEnd"]).ToString("hh")) != 12)
                    sWorkTerm += Convert.ToString(Convert.ToInt32(Convert.ToDateTime(drRow["WorkingEnd"]).ToString("hh")) + 12);

                else
                    sWorkTerm += Convert.ToString(Convert.ToDateTime(drRow["WorkingEnd"]).ToString("hh"));
            }

            else
                sWorkTerm += Convert.ToString(Convert.ToDateTime(drRow["WorkingEnd"]).ToString("hh"));

            sWorkTerm += ":" + Convert.ToString(Convert.ToDateTime(drRow["WorkingEnd"]).ToString("mm")) + " (";

            if (drRow["Minute"].ToString() == "0")
            {
                sWorkTerm += drRow["Hour"].ToString() + "시간)";
            }
            else
            {
                sWorkTerm += drRow["Hour"].ToString() + "시간 " + drRow["Minute"].ToString() + "분)";
            }

            //sWorkTerm += drRow["WorkingStart"].ToString().Substring(0,4) + "년 ";
            //sWorkTerm += drRow["WorkingStart"].ToString().Substring(5, 2) + "월 ";
            //sWorkTerm += drRow["WorkingStart"].ToString().Substring(8, 2) + "일 ";
            //sWorkTerm += drRow["WorkingStart"].ToString().Substring(11, 5) + " - ";
            //sWorkTerm += drRow["WorkingEnd"].ToString().Substring(11, 5) + " (";
            //sWorkTerm += drRow["Hour"].ToString() + "시간 " + drRow["Minute"].ToString() + "분)";



            sHtml_New.AppendLine("          <td width='640' height='40' bgcolor='#FFFFFF'><span style='font-size: 12pt;'>&nbsp;" + sWorkTerm + "</span></td>                                                                                                                                                                                                                                     ");


            sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                              ");
        }
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td height='40'><span style='font-size: 12pt;'>&nbsp;" + this._VolunteerTime + "</span></td>                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td height='40' align='center'><span style='font-size: 12pt; font-weight:bold;'>봉사내용</span></td>                                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("          <td height='40' ><span style='font-size: 12pt;'>&nbsp;" + this.VolunteerContents + "</span></td>                                                                                                                                                                                                                                               ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("          <td width='160' height='40' align='center'><span style='font-size: 12pt;font-weight:bold;'>담당자</span></td>                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("          <td width='640' height='40'><span style='font-size: 12pt;'>&nbsp;" + this.Charge + "</span></td>                                                                                                                                                                                                                                                                       ");

        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");
        sHtml_New.AppendLine("      </table></td>                                                                                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='10'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                     ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td align='center'><span style='font-size: 12pt;'>위 사람이 본 기관에서 성실히 봉사활동을 수행했음을 위와 같이 증명합니다.</span></td>                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='15'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                    ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td align='center'><span style='font-size: 12pt;'>" + this.PrintDate + "</span></td>                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                     ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        //sHtml_New.AppendLine("      <td align='center'><span style='font-size: 10pt;'>사회복지법인</span><span style='font-size: 12pt;'> 한국컴패션 </span><span style='font-size: 10pt;'>대표 서정인<img src='../../images/img002.jpg' width='118' height='114'></td>                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("      <td align='center'><span style='font-size: 12pt;'>사회복지법인 한국컴패션 대표 서정인 (인)</span><img src='../../images/img002.jpg' width='118' height='114'></td>                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");

        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='40'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                     ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");

        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>한국컴패션</b></font> (140-893) 서울시 용산구 한남대로 102-5 석전빌딩</span></td>                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>후원상담/안내 :</b></font> 02-740-1000 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>팩스 :</b></font> 02-740-1001 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>이메일 :</b></font> info@compassion.or.kr &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>www.</b></font>compassion.or.kr</span></td>           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                                 ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>Compassion USA Korea Office&nbsp;&nbsp;</b></font> 14515 Valley View Ave. Ste. #C Santa Fe Springs, CA90670</span></td>                                                                                                                                                                              ");
        sHtml_New.AppendLine("      </tr>                                                                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("      <tr>                                                                                                                                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>TEL :</b></font> 562-483-4300 &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>EMAIL :</b></font> info_us@compassion.or.kr</span></td>                                                                                             ");
        sHtml_New.AppendLine("      </tr>      ");
        sHtml_New.AppendLine("    </table>                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("    </body>                                                                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("    </html>																																																																																																																																																																																						");


        #endregion

        //sHtml_New = new StringBuilder();

        //sHtml_New.Append("<html>test</html>");
        return sHtml_New.ToString();
    }

    /// <summary>
    /// 봉사확인서 HTML 내용 만들기 (영문 확인서)
    /// </summary>
    /// <returns></returns>
    private string CreateReceiptHtml_6()
    {
        string sHtml = string.Empty;

        #region //-- 상단, 스크립트, 스타일
        sHtml = " <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>                                                                      ".TrimEnd()
        + " <html>                                                                                                                                    ".TrimEnd()
        + " <head>                                                                                                                                    ".TrimEnd()
        + " <title>한국컴패션 사회봉사확인서</title>                                                                                                  ".TrimEnd()

        //ScriptX프린트 설정
            //프린트설정
        + " <script language='javascript' type='text/javascript'>                   ".TrimEnd()
            //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        + " function setPrint()                                                     ".TrimEnd()
        + " agree = confirm('현재 페이지를 출력하시겠습니까? ".TrimEnd()
        + " if (agree) window.print();  ".TrimEnd()
        //+ "     if(factory.printing == null)                                        ".TrimEnd()
        //+ "     {                                                                   ".TrimEnd()
        //+ "         alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.'); ".TrimEnd()
        //+ "         return;                                                         ".TrimEnd()
        //+ "     }                                                                   ".TrimEnd()
        //+ "     factory.printing.header       = '';                                 ".TrimEnd()
        //+ "     factory.printing.footer       = '';                                 ".TrimEnd()
        //+ "     factory.printing.portrait     = true;                               ".TrimEnd() //true 세로출력,false가로출력
        //+ "     factory.printing.leftMargin   = 10;                                 ".TrimEnd()
        //+ "     factory.printing.topMargin    = 10;                                 ".TrimEnd()
        //+ "     factory.printing.rightMargin  = 10;                                 ".TrimEnd()
        //+ "     factory.printing.bottomMargin = 10;                                 ".TrimEnd()
        //    //+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        //+ "     agree = confirm('현재 페이지를 출력하시겠습니까?');                 ".TrimEnd()
        //+ "     if (agree) factory.printing.Print(false, window)                    ".TrimEnd()
        + " }                                                                       ".TrimEnd()
        + " </script>                                                               ".TrimEnd()
            //프린트설정 끝

        + " <style type='text/css'>                                                                                                                   ".TrimEnd()
        + " /*body { margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;}*/                                                     ".TrimEnd()
        + " img {border:none;}                                                                                                                        ".TrimEnd()
        + " body, td {  font-family: '돋움', '바탕', '굴림'; font-size:10pt; color: #000000;}                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 소득세법 */                                                                                                                            ".TrimEnd()
        + " td.CodeTitle{ font-size:10pt; padding-bottom:5px;}                                                                                        ".TrimEnd()
        + " td.Code{ font-size:8pt; padding-bottom:5px;}                                                                                              ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 HEAD */                                                                                                                       ".TrimEnd()
        + " td.ContentHeadType{width: 96px; height:25px; text-align: center; background-color: silver;}                                               ".TrimEnd()
        + " td.ContentHeadCode{width: 46px; text-align: center; background-color: silver;}                                                            ".TrimEnd()
        + " td.ContentHeadDate{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadDesc{width: 105px; text-align: center; background-color: silver;}                                                           ".TrimEnd()
        + " td.ContentHeadAmount{width: 98px; text-align: center; background-color: silver;}                                                          ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* 기부내용 CONTENT */                                                                                                                    ".TrimEnd()
        + " td.ContentItem{                                                                                                                           ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px dotted;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " td.ContentItemBottom{                                                                                                                     ".TrimEnd()
        + " border-top:gray 0px dotted;                                                                                                               ".TrimEnd()
        + " border-left:gray 0px dotted;                                                                                                              ".TrimEnd()
        + " border-right:gray 1px solid;                                                                                                              ".TrimEnd()
        + " border-bottom:gray 1px solid;                                                                                                             ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + "                                                                                                                                           ".TrimEnd()
        + " /* Title */                                                                                                                               ".TrimEnd()
        + " .Title{                                                                                                                                   ".TrimEnd()
        + " font-size: 12pt;                                                                                                                          ".TrimEnd()
        + " font-weight: bold;                                                                                                                        ".TrimEnd()
        + " border-bottom: gray 1px solid;                                                                                                            ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " /*  */                                                                                                                                    ".TrimEnd()
        + " .Gray_Table{                                                                                                                              ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:1px;                                                                                                                     ".TrimEnd()
        + " border-left-width:2px;                                                                                                                    ".TrimEnd()
        + " border-right-width:2px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head{                                                                                                                               ".TrimEnd()
        + " text-align: center;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " .Gray_Head2{                                                                                                                               ".TrimEnd()
        + " text-align: left;                                                                                                                       ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;	                                                                                                              ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()

        + " .Gray_Item{                                                                                                                               ".TrimEnd()
        + " border-color:gray;                                                                                                                        ".TrimEnd()
        + " border-style:solid;                                                                                                                       ".TrimEnd()
        + " border-top-width:0px;                                                                                                                     ".TrimEnd()
        + " border-left-width:0px;                                                                                                                    ".TrimEnd()
        + " border-right-width:0px;                                                                                                                   ".TrimEnd()
        + " border-bottom-width:1px;                                                                                                                  ".TrimEnd()
        + " }                                                                                                                                         ".TrimEnd()
        + " </style>                                                                                                                                  ".TrimEnd()
        + " </head>                                                                                                                                   ".TrimEnd();
        #endregion

        #region //-- 인적사항
        sHtml += "<body onload = 'setPrint()'; style='text-align: left;'>                                                                               ".TrimEnd()

        //ScriptX다운로드 경로 (프린트 설정시 필요)
        + " <object id=factory style='display:none' ".TrimEnd()
        + " classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ".TrimEnd()
        + " codebase=../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ".TrimEnd()
        + " </object> ".TrimEnd()
            //ScriptX다운로드 경로 끝

        + " <table style='width:700px; text-align:center;' cellpadding='0' cellspacing='0' border='0'>                                                  ".TrimEnd()
        + "	<tr>                                                                                                                                        ".TrimEnd()
        + "		<td>                                                                                                                                    ".TrimEnd()
        + "			<br/>                                                                                                                               ".TrimEnd()
        + "			<table style='width:680px;' cellpadding='0' cellspacing='0' border='0'>                                                             ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td>                                                                                                                        ".TrimEnd()
        + "						<img src='../images/compassion_logo.gif' width='200px' height='75px' alt=''/>                                           ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='20px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td style='text-align:left; height:30px;'>                                                                                  ".TrimEnd()
        + "						<span style='font-size: 9pt;'>Issue No. " + DateTime.Now.ToString("yyyyMMddhhmmssfff") + "    </span>                 ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td style='text-align: center; height:50px;'>                                                                               ".TrimEnd()
        + "						<span style='font-size: 20pt; font-weight:bold;'>certificate of volunteer WORK</span>                                            ".TrimEnd()
        + "					</td>                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                            ".TrimEnd()
        + "					<td height='50px'></td>                                                                                                     ".TrimEnd()
        + "				</tr>                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                            ".TrimEnd();

        #endregion

        #region //-- 기부 내용

        sHtml += "	 <table style='width: 600px' cellpadding='0' cellspacing='0' border='0'>                                          ".TrimEnd()
        + "				<tr>                                                                                                                             ".TrimEnd()
        + "					<td  style='font-size: 10pt;height: 30px;'>".TrimEnd()
        + "This is to certify that " + this.SponsorNameENG + "  has served devotedly as a volunteer in Compassion Korea,  </br>  ".TrimEnd()
        + "a Non-governmental Christian organization working for child advocacy and ministry in poverty   </br>  ".TrimEnd()
        + "stricken places of the World. The above mentioned person has volunteered for " + this.VolunteerTimeENG + "</br>".TrimEnd()
        + "in total from " + this.VolunteerTermENG + " at our organization translating child letters to    </br>".TrimEnd()
        + "sponsors or vice versa. The volunteer’s sincere heart, devotion, and the promptness in finishing </br>".TrimEnd()
        + "the works given, is highly appreciated and is of great help to the neediest children in the world</br>".TrimEnd()
        + "</td>".TrimEnd()
        + "</tr>".TrimEnd()
        + "			</table>".TrimEnd()
        + "".TrimEnd();
        #endregion

        #region //-- 끝
        sHtml += "  <table style='width: 680px; ' cellpadding='0' cellspacing='0' border='0'>                        ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 250px;'></td>                                                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='text-align: Right; height: 20px;'> " + this.PrintDateENG + "</td>                                                                          ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 70px;'></td>                                                                                                                 ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='text-align: right; height: 30px;'>                                                                                                   ".TrimEnd()

        + "						<img src='../images/stamp3.gif' width='529px' height='78px' alt=''/>                                           ".TrimEnd()

        //+ "						사회복지법인 <span style='font-size: 16pt; font-weight:bold;'>한국컴패션</span>&nbsp;                                                                   ".TrimEnd()
            //+ "						대표 <span style='font-size: 16pt; font-weight:bold;'>서정인</span>                                                                                     ".TrimEnd()


        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td style='height:50px;'></td>                                                                                                                              ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "				<tr>                                                                                                                                                            ".TrimEnd()
        + "					<td colspan='3' style='height: 30px; text-align: left'>                                                                                                     ".TrimEnd()
        + "						<strong>Compassion Korea</strong> |                                                                                                                     ".TrimEnd()
        + "						<strong>한국컴패션</strong>                                                                                                                             ".TrimEnd()
        + "						110-717 서울시 종로구 인의동 112-1 동원빌딩 7층 한국컴패션                                                                                              ".TrimEnd()
        + "						<br />                                                                                                                                                  ".TrimEnd()
        + "						TEL: 02-3668-3400 &nbsp; FAX: 02-3668-3456 &nbsp; EMAIL: info@compassion.or.kr                                                                          ".TrimEnd()
        + "					</td>                                                                                                                                                       ".TrimEnd()
        + "				</tr>                                                                                                                                                           ".TrimEnd()
        + "			</table>                                                                                                                                                            ".TrimEnd()
        + "		</td>                                                                                                                                                                   ".TrimEnd()
        + "	</tr>                                                                                                                                                                       ".TrimEnd()
        + "</table>                                                                                                                                                                     ".TrimEnd()
        + "                                                                                                                                                                             ".TrimEnd()
        + "</body>                                                                                                                                                                      ".TrimEnd()
        + "</html>                                                                                                                                                                      ".TrimEnd();
        #endregion

        #region 영문확인서
        sHtml_New.Remove(0, sHtml_New.Length);

        sHtml_New.AppendLine("  <html>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <head>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <title>봉사활동확인서</title>                                                                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("  <meta http-equiv='Content-Type' content='text/html; charset=euc-kr'>                                                                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");

        //ScriptX프린트 설정
        //프린트설정
        sHtml_New.AppendLine("  <script language='javascript' type='text/javascript'>                    ");
        //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        sHtml_New.AppendLine("   function setPrint()                                                      ");
        sHtml_New.AppendLine("   {                                                                       ");
        sHtml_New.AppendLine("      agree = confirm('현재 페이지를 출력하시겠습니까?');                                ");
        sHtml_New.AppendLine("      if (agree) window.print();                                                        ");
        //sHtml_New.AppendLine("      if(factory.printing == null)                                         ");
        //sHtml_New.AppendLine("       {                                                                  ");
        //sHtml_New.AppendLine("        alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.');   ");
        //sHtml_New.AppendLine("         return;                                                           ");
        //sHtml_New.AppendLine("       }                                                                    ");
        //sHtml_New.AppendLine("      factory.printing.header       = '';                                  ");
        //sHtml_New.AppendLine("       factory.printing.footer       = '';                                 ");
        //sHtml_New.AppendLine("     factory.printing.portrait     = true;                               ");//true 세로출력,false가로출력
        //sHtml_New.AppendLine("      factory.printing.leftMargin   = 10;                                  ");
        //sHtml_New.AppendLine("       factory.printing.topMargin    = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.rightMargin  = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.bottomMargin = 10;                                 ");
        ////+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        //sHtml_New.AppendLine("       agree = confirm('현재 페이지를 출력하시겠습니까?');                  ");
        //sHtml_New.AppendLine("       if (agree) factory.printing.Print(false, window)                   ");
        sHtml_New.AppendLine("   }                                                                        ");
        sHtml_New.AppendLine("   </script>                                                               ");
        //프린트설정 끝



        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("  <body onload = 'setPrint()';>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");
        //ScriptX다운로드 경로 (프린트 설정시 필요)
        //sHtml_New.AppendLine("  <object id=factory style='display:none' ");
        //sHtml_New.AppendLine(" classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ");
        //sHtml_New.AppendLine(" codebase=../../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ");
        //sHtml_New.AppendLine(" </object> ");
        //ScriptX다운로드 경로 끝

        sHtml_New.AppendLine("                                                                                                                                                                  ");
        sHtml_New.AppendLine("  <table width='800' border='0' align='center' cellpadding='0' cellspacing='0'>                                                                                   ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td><img src='../../images/img001.jpg' width='800' height='100' /></td>                                                                                           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='70' align='center'>&nbsp;</td>                                                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='60'><table width='750' border='0' align='center' cellpadding='0' cellspacing='0'>                                                               ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                      ");
        sHtml_New.AppendLine("          <td><span style='font-size: 10pt; font-weight:bold;'>Issue No.  " + DateTime.Now.ToString("yyyyMMddhhmmssfff") + " </span></td>                                                                  ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                     ");
        sHtml_New.AppendLine("      </table></td>                                                                                                                                               ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='50' align='center'><span style='font-size: 20pt; font-weight:bold;'>CERTIFICATE<strong> OF VOLUNTEER  WORK</strong></span></td>                 ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='80' align='center'>&nbsp;</td>                                                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td style='padding-left:40px;padding-right:40px'><span style='font-size: 14pt;text-align:justify; text-justify:inter-cluster;'> ");

        //sHtml_New.AppendLine("    This is to certify that " + this.SponsorNameENG + "  has served devotedly as a volunteer in Compassion Korea,  </br>     ");
        //sHtml_New.AppendLine("    a Non-governmental Christian organization working for child advocacy and ministry in poverty   </br>     ");
        //sHtml_New.AppendLine("    stricken places of the World. The above mentioned person has volunteered for " + this.VolunteerTimeENG + "</br>   ");
        //sHtml_New.AppendLine("    in total from " + this.VolunteerTermENG + " at our organization translating child letters to sponsors or</br>   ");
        //sHtml_New.AppendLine("    vice versa. The volunteer’s sincere heart, devotion, and the promptness in finishing the works given</br>   ");
        //sHtml_New.AppendLine("    , is highly appreciated and is of great help to the neediest children in the world</br>   ");

        sHtml_New.AppendLine("This is to certify that " + this.SponsorNameENG + "  has served devotedly as a volunteer in Compassion Korea, ");
        sHtml_New.AppendLine("a Non-governmental Christian organization working for child advocacy and ministry in poverty ");
        sHtml_New.AppendLine("stricken places of the World. The above mentioned person has volunteered for " + this.VolunteerTimeENG + " ");
        sHtml_New.AppendLine("in total from " + this.VolunteerTermENG + " at our organization doing child/sponsor letters filing  work. ");
        sHtml_New.AppendLine("The volunteer’s sincere heart, devotion, and the promptness in finishing the works given, ");
        sHtml_New.AppendLine("is highly appreciated and is of great help to the neediest children in the world. ");



        sHtml_New.AppendLine("     </span></td>                                                                                                        ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='300'>&nbsp;</td>                                                                                                                                ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td>&nbsp;</td>                                                                                                                                             ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td align='right' style='padding-right:45px'><p align='right'><strong>" + this.PrintDateENG + "</strong></p></td>                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='20'>&nbsp;</td>                                                                                                                                 ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td align='right'><span style='font-size: 12pt;'>Compassion Korea </span><span style='font-size: 10pt;'>CEO/President Justin Suh</span><img src='../../images/img004.jpg' width='184' height='49'></td>                                                                               ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                         ");

        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>한국컴패션</b></font> (140-893) 서울시 용산구 한남대로 102-5 석전빌딩</span></td>                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>후원상담/안내 :</b></font> 02-740-1000 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>팩스 :</b></font> 02-740-1001 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>이메일 :</b></font> info@compassion.or.kr &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>www.</b></font>compassion.or.kr</span></td>           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                                 ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>Compassion USA Korea Office&nbsp;&nbsp;</b></font> 14515 Valley View Ave. Ste. #C Santa Fe Springs, CA90670</span></td>                                                                                                                                                                              ");
        sHtml_New.AppendLine("      </tr>                                                                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("      <tr>                                                                                                                                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>TEL :</b></font> 562-483-4300 &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>EMAIL :</b></font> info_us@compassion.or.kr</span></td>                                                                                             ");
        sHtml_New.AppendLine("      </tr>                                                                                                                                                                                                                                                                                                                                                                           ");

        sHtml_New.AppendLine("  </table>                                                                                                                                                        ");
        sHtml_New.AppendLine("                                                                                                                                                                  ");
        sHtml_New.AppendLine("  </body>                                                                                                                                                         ");
        sHtml_New.AppendLine("  </html>																																																																													");

        #endregion

        return sHtml_New.ToString();
    }

    /// <summary>
    /// 후원금 납부내역 출력
    /// </summary>
    /// <returns></returns>
    private string CreateReceiptHtml_7()
    {
        #region 한글 내역
        sHtml_New.Remove(0, sHtml_New.Length);

        sHtml_New.AppendLine("  <html>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <head>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("  <title>후원금 납부내역서</title>                                                                                                                                                                                                                                                                                                                                                       ");
        sHtml_New.AppendLine("  <meta http-equiv='Content-Type' content='text/html; charset=euc-kr'>                                                                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");

        //ScriptX프린트 설정
        //프린트설정
        sHtml_New.AppendLine("  <script language='javascript' type='text/javascript'>                    ");
        //+ " //인쇄 : http://www.meadroid.com/scriptx/docs/printdoc.htm 참조         ".TrimEnd()
        sHtml_New.AppendLine("   function setPrint()                                                      ");
        sHtml_New.AppendLine("   {                                                                       ");
        sHtml_New.AppendLine("      agree = confirm('현재 페이지를 출력하시겠습니까?');                                ");
        sHtml_New.AppendLine("      if (agree) window.print();                                                        ");
        //sHtml_New.AppendLine("      if(factory.printing == null)                                         ");
        //sHtml_New.AppendLine("       {                                                                  ");
        //sHtml_New.AppendLine("        alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.');   ");
        //sHtml_New.AppendLine("         return;                                                           ");
        //sHtml_New.AppendLine("       }                                                                    ");
        //sHtml_New.AppendLine("      factory.printing.header       = '';                                  ");
        //sHtml_New.AppendLine("       factory.printing.footer       = '';                                 ");
        //sHtml_New.AppendLine("     factory.printing.portrait     = true;                               ");//true 세로출력,false가로출력
        //sHtml_New.AppendLine("      factory.printing.leftMargin   = 10;                                  ");
        //sHtml_New.AppendLine("       factory.printing.topMargin    = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.rightMargin  = 10;                                  ");
        //sHtml_New.AppendLine("      factory.printing.bottomMargin = 10;                                 ");
        ////+ "     factory.printing.printBackground = true;                            ".TrimEnd()
        //sHtml_New.AppendLine("       agree = confirm('현재 페이지를 출력하시겠습니까?');                  ");
        //sHtml_New.AppendLine("       if (agree) factory.printing.Print(false, window)                   ");
        sHtml_New.AppendLine("   }                                                                        ");
        sHtml_New.AppendLine("   </script>                                                               ");
        //프린트설정 끝

        sHtml_New.AppendLine("  </head>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("  <body onload = 'setPrint()';>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                      ");
        //ScriptX다운로드 경로 (프린트 설정시 필요)
        //sHtml_New.AppendLine("  <object id=factory style='display:none' ");
        //sHtml_New.AppendLine(" classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 ");
        //sHtml_New.AppendLine(" codebase=../../Files/ScriptX.cab#Version=6,1,431,8 VIEWASTEXT> ");
        //sHtml_New.AppendLine(" </object> ");
        //ScriptX다운로드 경로 끝
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("  <table width='800' border='0' align='center' cellpadding='0' cellspacing='0'>                                                                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td><img src='../../images/img001.jpg' width='800' height='100' /></td>                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='50' align='center'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine(" 	<tr>                                                                                                                         ");
        sHtml_New.AppendLine(" 		<td style='text-align: center; height:50px;'>                                                                                 ");
        sHtml_New.AppendLine(" 		    <span style='font-size: 20pt; font-weight:bold;'>후원금 납부내역</span>                                              ");
        sHtml_New.AppendLine(" 		</td>                                                                                                                       ");
        sHtml_New.AppendLine("  </tr>                                                                                                                             ");

        sHtml_New.AppendLine("  <tr> ");
        sHtml_New.AppendLine("      <td height='60'>&nbsp;</td> ");
        sHtml_New.AppendLine("  </tr> ");
        sHtml_New.AppendLine("  <tr> ");
        sHtml_New.AppendLine("      <td>후원자번호 : " + this.ConID + " </td> ");
        sHtml_New.AppendLine("  </tr> ");
        sHtml_New.AppendLine("  <tr> ");
        sHtml_New.AppendLine("      <td height='5'>&nbsp;</td> ");
        sHtml_New.AppendLine("  </tr> ");
        sHtml_New.AppendLine("  <tr> ");
        sHtml_New.AppendLine("      <td>후원자이름 : " + this.SponsorName +  " </td> ");
        sHtml_New.AppendLine("  </tr ");
        sHtml_New.AppendLine("  <tr> ");
        sHtml_New.AppendLine("      <td height='40'>&nbsp;</td> ");
        sHtml_New.AppendLine("  </tr> ");
        sHtml_New.AppendLine("  <tr> ");
        sHtml_New.AppendLine("      <td style='text-align: right;'>조회기간 : " + this.SearchTime + " </td> ");
        sHtml_New.AppendLine("  </tr> ");

        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='30' align='center'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td><table width='800' border='1' cellpadding='0' cellspacing='0' bordercolor='#000000'>                                                                                                                                                                                                                                                                                                    ");
        sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                                      ");
        sHtml_New.AppendLine("          <td width='140' height='40' align='center' bgcolor='#E1E1E1' style='width:280px;'><span style='font-size: 12pt;'><b>납부일자</b></span></td>                                                                                                                                                                                                                                                                 ");
        sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#E1E1E1'><span style='font-size: 12pt;'><b>납부방법</b></span></td>                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("          <td width='140' height='40' align='center' bgcolor='#E1E1E1' style='width:220px;'><span style='font-size: 12pt;'><b>후원금액</b></span></td>                                                                                                                                                                                                                                                                   ");
        sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                                     ");

        countOrderNo = 1;
        payment_total = 0;

        foreach (DataRow drRow in dsPayment.Tables["PaymentlistT"].Rows)
        {
            sHtml_New.AppendLine("        <tr>                                                                                                                                                                                                                                                                                                                                                                                      ");
            sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'>" + drRow["PaymentDate"].ToString() + " </span></td>                                                                                                                                                                                                                                                                                      ");
            sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'>" + drRow["PaymentName"].ToString() + "</span></td>                                                                                                                                                                                                                                                                     ");
            //sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'>" + drRow["Amount"].ToString() + "원 </span></td>                                                                                                                                                                                                                                                                                           ");
            sHtml_New.AppendLine("          <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 12pt;'>" + String.Format("{0:#,###}", drRow["Amount"]) + "원 </span></td>                                                                                                                                                                                                                                                                                           ");
            sHtml_New.AppendLine("        </tr>                                                                                                                                                                                                                                                                                                                                                                                     ");

            payment_total += Convert.ToInt32(drRow["Amount"]);

            countOrderNo++;
        }

        sHtml_New.AppendLine("          <tr> ");
        sHtml_New.AppendLine("              <td height='40' colspan='2' align='center' bgcolor='#E1E1E1'><b>총 후원금액</b></td> ");
        sHtml_New.AppendLine("              <td height='40' align='center' bgcolor='#E1E1E1'><b>" + String.Format("{0:#,###}", payment_total) + "원</b></td> ");
        sHtml_New.AppendLine("          </tr> ");
        sHtml_New.AppendLine("         </table></td>                                                                                                                                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("    <tr> ");
        sHtml_New.AppendLine("      <td height='40'></td> ");
        sHtml_New.AppendLine("    </tr> ");
        sHtml_New.AppendLine("    <tr> ");
        sHtml_New.AppendLine("      <td>후원금 납부내역을 위와 같이 확인합니다.</td> ");
        sHtml_New.AppendLine("    </tr> ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                                          ");
        sHtml_New.AppendLine("      <td height='100'>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                                ");
        sHtml_New.AppendLine("    </tr> ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>한국컴패션</b></font> (140-893) 서울시 용산구 한남대로 102-5 석전빌딩</span></td>                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>후원상담/안내 :</b></font> 02-740-1000 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>팩스 :</b></font> 02-740-1001 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>이메일 :</b></font> info@compassion.or.kr &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>www.</b></font>compassion.or.kr</span></td>           ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("      <td>&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                                 ");
        sHtml_New.AppendLine("    </tr>                                                                                                                                                                                                                                                                                                                                                                             ");
        sHtml_New.AppendLine("    <tr>                                                                                                                                                                                                                                                                                                                                                                              ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>Compassion USA Korea Office&nbsp;&nbsp;</b></font> 14515 Valley View Ave. Ste. #C Santa Fe Springs, CA90670</span></td>                                                                                                                                                                              ");
        sHtml_New.AppendLine("      </tr>                                                                                                                                                                                                                                                                                                                                                                           ");
        sHtml_New.AppendLine("      <tr>                                                                                                                                                                                                                                                                                                                                                                            ");
        sHtml_New.AppendLine("        <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>TEL :</b></font> 562-483-4300 &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>EMAIL :</b></font> info_us@compassion.or.kr</span></td>                                                                                             ");
        sHtml_New.AppendLine("      </tr>      ");
        sHtml_New.AppendLine("  </table>                                                                                                                                                                                                                                                                                                                                                                                        ");
        sHtml_New.AppendLine("                                                                                                                                                                                                                                                                                                                                                                                                  ");
        sHtml_New.AppendLine("  </body>                                                                                                                                                                                                                                                                                                                                                                                         ");
        sHtml_New.AppendLine("  </html>                                                                                                                                                          																																																																																																																");
        #endregion


        return sHtml_New.ToString();
    }
}
