﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Roamer.Config;

/// <summary>
/// TranslateInfo의 요약 설명입니다.
/// </summary>
public class TranslateInfo
{
    private HttpContext _httpContext;

	public TranslateInfo()
	{
        _httpContext = HttpContext.Current;

        if (_httpContext.Request.Cookies["MATETranslateToken"] == null)
        {
            HttpCookie ticketCookie = new HttpCookie("MATETranslateToken");
            _httpContext.Response.AppendCookie(ticketCookie);
        }
	}

    //번역 중 글 유지
    public string TranslateText
    {
        get
        {
            return Cryptography.Decrypt(_httpContext.Request.Cookies["MATETranslateToken"]["TranslateText"]);
        }
        set
        {
            _httpContext.Response.Cookies["MATETranslateToken"]["TranslateText"] = Cryptography.Encrypt(value);
        }
    }
    //스크리닝 중 글 유지
    public string ScreeningText
    {
        get
        {
            return Cryptography.Decrypt(_httpContext.Request.Cookies["MATETranslateToken"]["ScreeningText"]);
        }
        set
        {
            _httpContext.Response.Cookies["MATETranslateToken"]["ScreeningText"] = Cryptography.Encrypt(value);
        }
    }
    //탭 버튼 유지
    public string  TabSelectValue
    {
        get
        {
            return Cryptography.Decrypt(_httpContext.Request.Cookies["MATETranslateToken"]["TabSelectValue"]);
        }
        set
        {
            _httpContext.Response.Cookies["MATETranslateToken"]["TabSelectValue"] = Cryptography.Encrypt(value);
        }
    }
    //번역 및 스크리닝 분류
    public string MateYearValue
    {
        get
        {
            return Cryptography.Decrypt(_httpContext.Request.Cookies["MATETranslateToken"]["MateYearValue"]);
        }
        set
        {
            _httpContext.Response.Cookies["MATETranslateToken"]["MateYearValue"] = Cryptography.Encrypt(value);
        }
    }
    //번역 상세로 들어온 페이지 분류
    public string MateMoveType
    {
        get
        {
            return Cryptography.Decrypt(_httpContext.Request.Cookies["MATETranslateToken"]["MateMoveType"]);
        }
        set
        {
            _httpContext.Response.Cookies["MATETranslateToken"]["MateMoveType"] = Cryptography.Encrypt(value);
        }
    }
    //경유한 페이지 상세 분류
    public string MatePageType
    {
        get
        {
            return Cryptography.Decrypt(_httpContext.Request.Cookies["MATETranslateToken"]["MatePageType"]);
        }
        set
        {
            _httpContext.Response.Cookies["MATETranslateToken"]["MatePageType"] = Cryptography.Encrypt(value);
        }
    }
    //3PL 권한체크
    public string ThirdPLCheck
    {
        get
        {
            return Cryptography.Decrypt(_httpContext.Request.Cookies["MATETranslateToken"]["ThirdPLCheck"]);
        }
        set
        {
            _httpContext.Response.Cookies["MATETranslateToken"]["ThirdPLCheck"] = Cryptography.Encrypt(value);
        }
    }
    //스크리닝 권한체크
    public string ScreeningCheck
    {
        get
        {
            return Cryptography.Decrypt(_httpContext.Request.Cookies["MATETranslateToken"]["ScreeningCheck"]);
        }
        set
        {
            _httpContext.Response.Cookies["MATETranslateToken"]["ScreeningCheck"] = Cryptography.Encrypt(value);
        }
    }
    //번역 권한체크
    public string TranslateCheck
    {
        get
        {
            return Cryptography.Decrypt(_httpContext.Request.Cookies["MATETranslateToken"]["TranslateCheck"]);
        }
        set
        {
            _httpContext.Response.Cookies["MATETranslateToken"]["TranslateCheck"] = Cryptography.Encrypt(value);
        }
    }

}