﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
 
/// <summary>
/// Relation의 요약 설명입니다.
/// </summary>

[Serializable] 
public class RelationData : DataTable
{
    public RelationData()
        : base()
    {
        base.Columns.Add("GroupType", typeof(String));
        base.Columns.Add("CodeID", typeof(String));
        base.Columns.Add("CodeName", typeof(String));
        base.Columns.Add("Frequency", typeof(String));
        base.Columns.Add("Money", typeof(decimal));
    }
}
