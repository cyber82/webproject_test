﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Text;



/// <summary>
/// pushService의 요약 설명입니다.
/// </summary>
public class pushService
{
	public pushService()
	{
		//
		// TODO: 여기에 생성자 논리를 추가합니다.
		//
	}
    //GCM
    public string GCMPush(string postData)
    {
        string apiKey = "AIzaSyANct1fVztBs0KO581OdMiQ6mjMAOSlmck";
        string postDataContentType = "application/json";
        ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateServerCertificate);

        byte[] byteArray = Encoding.UTF8.GetBytes(postData);
        HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://android.googleapis.com/gcm/send");
        Request.Method = "POST";
        Request.KeepAlive = false;
        Request.ContentType = postDataContentType;
        Request.Headers.Add(string.Format("Authorization: key={0}", apiKey));
        //Request.Headers.Add(string.Format("Sender: id={0}", "749338113577"));
        Request.ContentLength = byteArray.Length;
        Stream dataStream = Request.GetRequestStream();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();

        string result = string.Empty;

        try
        {
            WebResponse Response = Request.GetResponse();
            HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;

            if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
            {
                result = "Unauthorized - need new token";
            }
            else if (!ResponseCode.Equals(HttpStatusCode.OK))
            {
                result = "Response from web service isn't OK";
            }
            StreamReader Reader = new StreamReader(Response.GetResponseStream());
            string responseLine = Reader.ReadToEnd();
            Reader.Close();
            return responseLine + "==" + result;
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }


    //GCM && APNS 
    public  bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        return true;
    }

    //APNS
    public void APNSPush(string dviceid, string sendMsg)
    {
        int port = 2195;
        String hostname = "gateway.push.apple.com";
        String certificatePath = "D:/apns_cert/apns_prod.p12";


        X509Certificate2 clientCertificate = new X509Certificate2(certificatePath, "C0mpassion", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
        X509Certificate2Collection certificatesCollection = new X509Certificate2Collection(clientCertificate);
        TcpClient client = new TcpClient(hostname, port);
        SslStream sslStream = new SslStream(client.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificate), null);

        try
        {
            sslStream.AuthenticateAsClient(hostname, certificatesCollection, SslProtocols.Tls, true);
        }
        catch (Exception e)
        {
            throw (e);
            client.Close();

            //return e.Message;
        }

        //return "test2";
        MemoryStream memoryStream = new MemoryStream();
        BinaryWriter writer = new BinaryWriter(memoryStream);
        writer.Write((byte)0);  //The command 
        writer.Write((byte)0);  //The first byte of the deviceId length (big-endian first byte) 
        writer.Write((byte)32); //The deviceId length (big-endian second byte)   



        writer.Write(HexStringToByteArray(dviceid.ToUpper()));

        ////후원어린이에게 편지가 도착했어요. 바로 확인하실래요? 	해당 편지 상세 페이지로 이동 (매일 오후 1시)	msgID=1, ChildMasterID, LetterID,PersonalNameKr
        ////어린이날  3개월 전 입니다. 아이들에게 선물과 편지로 마음을 전하세요 	편지 메뉴로 이동 (2월 10일 오후 1시)	msgID=2
        ////크리스마스 3개월 전 입니다. 아이들에게 선물과 편지로 마음을 전하세요	편지 메뉴로 이동(9월 10일 오후 1시)	msgID=3
        ////어린이가 최근 1년 동안 편지를 받지 못해 울고 있어요. 편지로 마음을 전해보세요 	편지 메뉴로 이동 (매월 마지막 주 금요일 오후 1시)	msgID=4
        ////어린이 생일 3개월 전 입니다. 아이들에게 선물과 편지로 마음을 전하세요 	메인으로 이동 	msgID=5
        ////어린이 정보가 업데이트 되었습니다. 확인하러 가실래요?? 	메인으로 이동 	msgID=6
        ////신규 캠페인이 등록되었습니다. 	해당 캠페인 상세 페이지로 이동 (있을시 10시에 발송)	msgID=7, CampaignID
        ////신규 이벤트가 등록되었습니다. 	해당 이벤트 상세 페이지로 이동 (있을시 10시에 발송) 	msgID=8, EventID

        //SendMsg1 = "{\"aps\":{\"alert\":\"후원어린이에게 편지가 도착했어요. 바로 확인하실래요?\",\"badge\":0,\"sound\":\"default\"}, \"msgID\":1, \"ChildMasterID\":1000, \"LetterID\":9999, \"PersonalNameKr\":\"홍길동\"   }";
        //SendMsg2 = "{\"aps\":   {\"alert\":\"어린이날  3개월 전 입니다. 아이들에게 선물과 편지로 마음을 전하세요\",\"badge\":0,\"sound\":\"default\"}, \"msgID\":2 }";
        //SendMsg3 = "{\"aps\":   {\"alert\":\"크리스마스 3개월 전 입니다. 아이들에게 선물과 편지로 마음을 전하세요\",\"badge\":0,\"sound\":\"default\"}, \"msgID\":3 }";
        //SendMsg4 = "{\"aps\":   {\"alert\":\"어린이가 최근 1년 동안 편지를 받지 못해 울고 있어요. 편지로 마음을 전해보세요\",\"badge\":0,\"sound\":\"default\"}, \"msgID\":4}";
        //SendMsg5 = "{\"aps\":   {\"alert\":\"어린이 생일 3개월 전 입니다. 아이들에게 선물과 편지로 마음을 전하세요\",\"badge\":0,\"sound\":\"default\"}, \"msgID\":5}";
        //SendMsg6 = "{\"aps\":   {\"alert\":\"어린이 정보가 업데이트 되었습니다. 확인하러 가실래요?? \",\"badge\":0,\"sound\":\"default\"}, \"msgID\":6}";
        //SendMsg7 = "{\"aps\":   {\"alert\":\"신규 캠페인이 등록되었습니다. \",\"badge\":0,\"sound\":\"default\"}, \"msgID\":7,\"CampaignID\":1}";
        //SendMsg8 = "{\"aps\":   {\"alert\":\"신규 이벤트가 등록되었습니다.\",\"badge\":0,\"sound\":\"default\"}, \"msgID\":8,\"EventID\":2}";

        UTF8Encoding utf8 = new UTF8Encoding();
        writer.Write((byte)0);
        writer.Write((byte)utf8.GetBytes(sendMsg).Length);
        byte[] b1 = System.Text.Encoding.UTF8.GetBytes(sendMsg);
        writer.Write(b1); writer.Flush();
        byte[] array = memoryStream.ToArray();
        sslStream.Write(array);
        sslStream.Flush();

        //return "true";
        client.Close();

    }

    //APNS
    public static byte[] HexStringToByteArray(String s)
    {
        s = s.Replace(" ", "");

        byte[] buffer = new byte[s.Length / 2];

        for (int i = 0; i < s.Length; i += 2)
        {

            buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);

        }
        return buffer;
    }


}