﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransStep3.aspx.cs" Inherits="Mate_Intro_TransStep3" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/IntroRight.ascx" tagname="IntroRight" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />

    <script type="text/javascript">
    <!--
        //Backspace, F5, Ctrl+N, Ctrl+R, Ctrl+C, Ctrl+V 막기
        document.onkeydown = NewEdit;
        function NewEdit() {
            var keyval = event.keyCode;
            if (keyval == 116) {
                EventSet();
                alert("F5 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 78)) {
                EventSet();
                alert("Ctrl + N 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 82)) {
                EventSet();
                alert("Ctrl + R 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 86)) {
                EventSet();
                alert("Ctrl + V 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 67)) {
                EventSet();
                alert("Ctrl + C 키를 금지합니다.");
            }

            //Backspace
            if (keyval == 8) {
                if (event.srcElement.tagName == "INPUT" || event.srcElement.tagName == "TEXTAREA") {
                } else {
                    EventSet();
                }
            }
        }

        function EventSet() {
            event.keyCode = 0;
            event.cancelBubble = true;
            event.returnValue = false;
        }
    //-->
    </script>
       
    <style type="text/css">
        .q9{width:500px;height:70px;background:url('/Mate/mateTransferImage/Q9_image.jpg') no-repeat;margin-left:25px;margin-top:10px;}
    </style>

    <script type="text/javascript">
        //수정 2012-09-12
        function Validate() {
//            if (!document.getElementById("question1").checked &&
//                !document.getElementById("question2").checked &&
//                !document.getElementById("question3").checked &&
//                !document.getElementById("question4").checked) {
//                alert('첫번째 질의에 대한 답을 선택해 주세요.');
//                document.getElementById("question1").focus();
//                return false;
//            }

//            if (!document.getElementById("question5").checked &&
//                !document.getElementById("question6").checked &&
//                !document.getElementById("question7").checked &&
//                !document.getElementById("question8").checked) {
//                alert('두번째 질의에 대한 답을 선택해 주세요.');
//                document.getElementById("question5").focus();
//                return false;
//            }

//            if (!document.getElementById("question9").checked &&
//                !document.getElementById("question10").checked &&
//                !document.getElementById("question11").checked &&
//                !document.getElementById("question12").checked) {
//                alert('세번째 질의에 대한 답을 선택해 주세요.');
//                document.getElementById("question9").focus();
//                return false;
//            }

//            if (!document.getElementById("question13").checked &&
//                !document.getElementById("question14").checked &&
//                !document.getElementById("question15").checked &&
//                !document.getElementById("question16").checked) {
//                alert('네번째 질의에 대한 답을 선택해 주세요.');
//                document.getElementById("question13").focus();
//                return false;
//            }

//            if (!document.getElementById("question17").checked &&
//                !document.getElementById("question18").checked &&
//                !document.getElementById("question19").checked &&
//                !document.getElementById("question20").checked) {
//                alert('다섯번째 질의에 대한 답을 선택해 주세요.');
//                document.getElementById("question17").focus();
//                return false;
//            }

//            if (!document.getElementById("question21").checked &&
//                !document.getElementById("question22").checked &&
//                !document.getElementById("question23").checked) {
//                alert('여섯번째 질의에 대한 답을 선택해 주세요.');
//                document.getElementById("question20").focus();
//                return false;
//            }

//            if (!document.getElementById("question24").checked &&
//                !document.getElementById("question25").checked &&
//                !document.getElementById("question26").checked &&
//                !document.getElementById("question27").checked) {
//                alert('일곱번째 질의에 대한 답을 선택해 주세요.');
//                document.getElementById("question24").focus();
//                return false;
//            }

//            if (!document.getElementById("question28").checked &&
//                !document.getElementById("question29").checked &&
//                !document.getElementById("question30").checked &&
//                !document.getElementById("question31").checked) {
//                alert('여덟번째 질의에 대한 답을 선택해 주세요.');
//                document.getElementById("question28").focus();
//                return false;
//            }

//            if (!document.getElementById("question32").checked &&
//                !document.getElementById("question33").checked &&
//                !document.getElementById("question34").checked) {
//                alert('아홉번째 질의에 대한 답을 선택해 주세요.');
//                document.getElementById("question32").focus();
//                return false;
//            }

//            if (!document.getElementById("question35").checked &&
//                !document.getElementById("question36").checked) {
//                alert('열번째 질의에 대한 답을 선택해 주세요.');
//                document.getElementById("question35").focus();
//                return false;
//            }            
            return true;
        }

        function alertfail() {
            var msg1 = "MINI TEST를 통과하지 못하셨습니다.\n번역유의사항을 읽고 다시 도전해 주시기 바랍니다.";

            alert(msg1)
        }

        function alertSuccess() {
            var msg1 = "축하드립니다!\nMINI TEST를 통과하셨습니다.\n편지 번역 SAMPLE TEST로 이동합니다.";

            var answer = alert(msg1)

            //alert(document.getElementById('hdAgeDivision').value);

            var age = document.getElementById('hdAgeDivision').value;
            var school = document.getElementById('hdSchool').value;
            if (age == "성인" || age == "고등학생") {
                age = encodeURI(age);
                school = encodeURI(school);
            }

            top.window.location.href = "TransStep4.aspx?AgeDIvision=" + age + "&School=" + school;
        }
    </script>
</head>
<%--<body>--%>
<body oncontextmenu='return false' ondragstart='return false'>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="apply mini-test">
 
		<!-- sidebar -->
		<uc4:IntroRight ID="IntroRight1" runat="server" />
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>번역 MATE란</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/Intro/MateIntro.aspx">MATE 소개</a><strong>MATE 신청</strong>
			</div>
			<ul class="tab2 tab-mate">
				<li class="on"><a href="TransIntro.aspx" class="t1">번역 MATE</a></li>
				<li><a href="officemate.aspx" class="t2">사무 MATE</a></li>
				<li><a href="eventmate.aspx" class="t3">행사 MATE</a></li>
				<li><a href="specialitymate.aspx" class="t4">전문 MATE</a></li>
			</ul>
			<div class="txt-procedure">
				<h4>편지 번역 메이트 신청 절차</h4>
			</div>
			<ul class="tab3 tab-step">
				<li><a href="javascript:void(0);" class="t1">STEP01 메이트 정보 입력</a></li>
				<li><a href="javascript:void(0);" class="t2">STEP02 번역유의사항 PDF DOWN</a></li>
				<li class="on"><a href="javascript:void(0);" class="t3">STEP03 번역유의사항 MINI TEST</a></li>
				<li><a href="javascript:void(0);" class="t4">STEP04 편지번역 SAMPLE TEST</a></li>
				<li><a href="javascript:void(0);" class="t5">STEP05 번역 메이트 활동 동의서 서명</a></li>
				<li><a href="javascript:void(0);" class="t6">STEP06 지원완료</a></li>
			</ul>
			<div class="step-wrap">
				<%--<p class="text txt-notice">번역유의사항 mini test 번역유의사항을 다운로드 받고 숙지하신 후 TEST에 응해주시기 바랍니다.번역유의사항 숙지도를 평가하기 위한 TEST입니다. 이 TEST를 통과하셔야만 다음 단계로 넘어갈 수 있습니다.</p>--%>
				<p class="text txt-notice"></p>
				<fieldset class="questionnaire">
					<legend class="hide">번역유의사항 mini test</legend>
					<ol>
						<li style="margin-bottom:15px;">
							<div class="q-list" style="margin-bottom:10px;">
								<%--<p class="text txt-num1">Q1</p>--%>
								<p style="margin-left:5px; font-size:13px;"><b>Q1&nbsp;&nbsp;어린이편지를 번역을 할 때, 다음 중 꼭 번역해야 하는 항목은 무엇일까요?</b></p>
							</div>
                            <table>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question1" name="issue1" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question1">Dear OOO</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question2" name="issue1" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question2">From OOO</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question3" name="issue1" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question3">어린이의 학교 성적표</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question4" name="issue1" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question4">편지지에 인쇄되어 있는 국가 소개</label></td>
                                </tr>
                            </table>
							<%--<ul>
								<li>
									<input type="radio" id="question1" name="issue1" runat ="server"/>
									<label for="question1"><span>Dear OOO</span></label>
								</li>
								<li>
									<input type="radio" id="question2" name="issue1" class="rad" runat ="server"/>
									<label class="text txt-ex2" for="question2"><span>존경하는 후원자님께</span></label>
								</li>
								<li>
									<input type="radio" id="question3" name="issue1" class="rad" runat ="server"/>
									<label class="text txt-ex3" for="question3"><span>사랑하는 후원자님께</span></label>
								</li>
								<li>
									<input type="radio" id="question4" name="issue1" class="rad" runat ="server"/>
									<label class="text txt-ex4" for="question4"><span>Dear OOO는 따로 번역하지 않습니다. 번역된 편지를 출력할 때 자동으로 사랑하는 OOO 후원자님께가 인쇄되어 나오기 때문입니다.</span></label>
								</li>
							</ul>--%>
						</li>
						<li style="margin-bottom:15px;">
							<div class="q-list" style="margin-bottom:10px;">
								<%--<p class="text txt-num2">Q2</p>--%>
								<p style="margin-left:5px; font-size:13px;"><b>Q2&nbsp;&nbsp;어린이편지에서 날짜 번역 시, 올바른 표기법은 무엇일까요?<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(예: March 21st, 2014)</b></p>
							</div>
                            <table>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question5" name="issue2" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question5">21-03-2014</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question6" name="issue2" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question6">2014.03.21</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question7" name="issue2" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question7">2014년 3월 21일</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question8" name="issue2" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question8">날짜는 번역하지 않습니다.</label></td>
                                </tr>
                            </table>
							<%--<ul>
								<li>
									<input type="radio" id="question5" name="issue2" class="rad" runat ="server"/>
									<label class="text txt-ex5" for="question5"><span>신</span></label>
								</li>
								<li>
									<input type="radio" id="question6" name="issue2" class="rad" runat ="server"/>
									<label class="text txt-ex6" for="question6"><span>하나님</span></label>
								</li>
								<li>
									<input type="radio" id="question7" name="issue2" class="rad" runat ="server"/>
									<label class="text txt-ex7" for="question7"><span>하느님</span></label>
								</li>
								<li>
									<input type="radio" id="question8" name="issue2" class="rad" runat ="server"/>
									<label class="text txt-ex8" for="question8"><span>신, 하나님, 하느님 중에 한 가지를 택하여 번역 (어떤 단어를 택하더라도 의미 전달에 무리가 없으면 문제 없음)</span></label>
								</li>
							</ul>--%>
						</li>
						<li style="margin-bottom:15px;">
							<div class="q-list" style="margin-bottom:10px;">
								<%--<p class="text txt-num3">Q3</p>--%>
								<p style="margin-left:5px; font-size:13px"><b>Q3&nbsp;&nbsp;어린이편지를 번역할 때, God은 어떻게 번역할까요?</b></p>
							</div>
                            <table>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question9" name="issue3" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question9">신</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question10" name="issue3" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question10">하나님</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question11" name="issue3" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question11">하느님</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question12" name="issue3" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question12">신, 하나님, 하느님 중에 한 가지를 택하여 번역</br>(어떤 단어를 택하더라도 의미 전달에 무리가 없으면 문제 없음)</label></td>
                                </tr>
                            </table>
							<%--<ul>
								<li>
									<input type="radio" id="question9" name="issue3" class="rad" runat ="server"/>
									<label class="text txt-ex9" for="question9"><span>당신</span></label>
								</li>
								<li>
									<input type="radio" id="question10" name="issue3" class="rad" runat ="server"/>
									<label class="text txt-ex10" for="question10"><span>너</span></label>
								</li>
								<li>
									<input type="radio" id="question11" name="issue3" class="rad" runat ="server"/>
									<label class="text txt-ex11" for="question11"><span>후원자님</span></label>
								</li>
								<li>
									<input type="radio" id="question12" name="issue3" class="rad" runat ="server"/>
									<label class="text txt-ex12" for="question12"><span>아줌마, 아저씨</span></label>
								</li>
							</ul>--%>
						</li>
						<li style="margin-bottom:15px;">
							<div class="q-list" style="margin-bottom:10px;">
								<%--<p class="text txt-num4" style="padding-right:5px;">Q4</p>--%>
								<p style="margin-left:5px; font-size:13px"><b>Q4&nbsp;&nbsp;어린이편지에 등장하는 Sponsor를 지칭하는 'You'는 어떻게 번역할까요?</b></p>
							</div>
                            <table>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question13" name="issue4" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question13">당신</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question14" name="issue4" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question14">너</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question15" name="issue4" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question15">후원자님</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question16" name="issue4" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question16">아줌마, 아저씨</label></td>
                                </tr>
                            </table>
							<%--<ul>
								<li>
									<input type="radio" id="question13" name="issue4" class="rad" runat ="server"/>
									<label class="text txt-ex13" for="question13"><span>멘토</span></label>
								</li>
								<li>
									<input type="radio" id="question14" name="issue4" class="rad" runat ="server"/>
									<label class="text txt-ex14" for="question14"><span>개인교사</span></label>
								</li>
								<li>
									<input type="radio" id="question15" name="issue4" class="rad" runat ="server"/>
									<label class="text txt-ex15" for="question15"><span>자원봉사자</span></label>
								</li>
								<li>
									<input type="radio" id="question16" name="issue4" class="rad" runat ="server"/>
									<label class="text txt-ex16" for="question16"><span>컴패션 어린이센터 교사 (또는) 선생님</span></label>
								</li>
							</ul>--%>
						</li>
						<li style="margin-bottom:15px;">
							<div class="q-list" style="margin-bottom:10px;">
								<%--<p class="text txt-num5">Q5</p>--%>
								<p style="margin-left:5px; font-size:13px"><b>Q5&nbsp;&nbsp;어린이편지를 번역할 때, Social Worker, Mentor, Tutor 등은 한 단어로 통일하<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;여 번역합니다. 다음 중 올바른 표현은 무엇일까요?</b></p>
							</div>
                            <table>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question17" name="issue5" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question17">멘토</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question18" name="issue5" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question18">개인교사</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question19" name="issue5" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question19">자원봉사자</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question20" name="issue5" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question20">컴패션 어린이센터 교사 (또는) 선생님</label></td>
                                </tr>
                            </table>
							<%--<ul>
								<li>
									<input type="radio" id="question17" name="issue5" class="rad" runat ="server"/>
									<label class="text txt-ex17" for="question17"><span>Payasam (번역하지 않고 알파벳 표기 그대로 사용)</span></label>
								</li>
								<li>
									<input type="radio" id="question18" name="issue5" class="rad" runat ="server"/>
									<label class="text txt-ex18" for="question18"><span>빠야삼 (한국말로 음역, 소리나는대로 표기)</span></label>
								</li>
								<li>
									<input type="radio" id="question19" name="issue5" class="rad" runat ="server"/>
									<label class="text txt-ex19" for="question19"><span>빠야삼 (Payasam) (음역 표기와 알파벳 표기를 모두 사용)</span></label>
								</li>
							</ul>--%>
						</li>
						<li style="margin-bottom:15px;">
							<div class="q-list" style="margin-bottom:10px;">
								<%--<p class="text txt-num6">Q6</p>--%>
								<p style="margin-left:5px; font-size:13px;"><b>Q6&nbsp;&nbsp;어린이편지를 번역할 때, 고유명사를 번역하는 올바른 방법은 무엇일까요?<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(예 : 편지에 Payasam이라는 음식명이 등장했을 때)</b></p>
							</div>
                            <table>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question21" name="issue6" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question21">Payasam (번역하지 않고 알파벳 표기 그대로 사용)</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question22" name="issue6" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question22">빠야삼 (한국어로 음역, 소리나는대로 표기)</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question23" name="issue6" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question23">빠야삼(Payasam) (음역 표기와 알파벳 표기를 모두 사용)</label></td>
                                </tr>
                            </table>
							<%--<ul>
								<li>
									<input type="radio" id="question20" name="issue6" class="rad" runat ="server"/>
									<label class="text txt-ex20" for="question20"><span>프로젝트</span></label>
								</li>
								<li>
									<input type="radio" id="question21" name="issue6" class="rad" runat ="server"/>
									<label class="text txt-ex21" for="question21"><span>어린이집</span></label>
								</li>
								<li>
									<input type="radio" id="question22" name="issue6" class="rad" runat ="server"/>
									<label class="text txt-ex22" for="question22"><span>학생 센터</span></label>
								</li>
								<li>
									<input type="radio" id="question23" name="issue6" class="rad" runat ="server"/>
									<label class="text txt-ex23" for="question23"><span>컴패션 어린이센터</span></label>
								</li>
							</ul>--%>
						</li>
						<li style="margin-bottom:15px;">
							<div class="q-list" style="margin-bottom:10px;">
								<%--<p class="text txt-num7">Q7</p>--%>
								<p style="margin-left:5px; font-size:13px;"><b>Q7&nbsp;&nbsp;어린이편지에 자주 등장하는 Child Development Center, Project, <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Student Center, Day Care의 올바른 번역법은 무엇일까요?</b></p>
							</div>
                            <table>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question24" name="issue7" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question24">프로젝트</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question25" name="issue7" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question25">어린이 집</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question26" name="issue7" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question26">학생 센터</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question27" name="issue7" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question27">컴패션 어린이센터</label></td>
                                </tr>
                            </table>
							<%--<ul>
								<li>
									<input type="radio" id="question24" name="issue7" class="rad" runat ="server"/>
									<label class="text txt-ex24" for="question24"><span>10-22 2010</span></label>
								</li>
								<li>
									<input type="radio" id="question25" name="issue7" class="rad" runat ="server"/>
									<label class="text txt-ex25" for="question25"><span>2010.10.22</span></label>
								</li>
								<li>
									<input type="radio" id="question26" name="issue7" class="rad" runat ="server"/>
									<label class="text txt-ex26" for="question26"><span>October 22nd 2010</span></label>
								</li>
								<li>
									<input type="radio" id="question27" name="issue7" class="rad" runat ="server"/>
									<label class="text txt-ex27" for="question27"><span>날짜는 번역하지 않습니다</span></label>
								</li>
							</ul>--%>
						</li>
                        <li style="margin-bottom:15px;">
							<div class="q-list" style="margin-bottom:10px;">
								<p style="margin-left:5px; font-size:13px;"><b>Q8&nbsp;&nbsp;후원자편지에서 날짜 번역 시, 올바른 영문 표기법은 무엇일까요? <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (예 : 2014년 7월 1일)</b></p>
							</div>
                            <table>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question28" name="issue8" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question28">07-01-2014</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question29" name="issue8" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question29">2014. 7. 1</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question30" name="issue8" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question30">July 1st, 2014</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question31" name="issue8" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question31">날짜는 번역하지 않습니다.</label></td>
                                </tr>
                            </table>
                        </li>
                        <li style="margin-bottom:15px;">
							<div class="q-list" style="margin-bottom:10px;">
								<p style="margin-left:5px; font-size:13px;"><b>Q9&nbsp;&nbsp;후원자편지를 번역할 때, 편지지에 나와있는 Sponsor/Writer's Infomation 부분<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;은 어떻게 번역할까요?</b></p>
                            </div>
                           
                            <table>
                                <tr style="height:37px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question32" name="issue9" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question32">[Sponsor/Writer's Infomation]<br />&nbsp;Gender : Female / Age : 34</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question33" name="issue9" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question33">Female, 34 (직접 표기한 부분만 번역)</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question34" name="issue9" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question34">번역하지 않습니다.</label></td>
                                </tr>
                            </table>
                        </li>
                        <li style="margin-bottom:15px;">
							<div class="q-list" style="margin-bottom:10px;">
								<p style="margin-left:5px; font-size:13px;"><b>Q10&nbsp;&nbsp;후원자편지를 번역할 때, 후원자님께서 영어로 작성하신 내용은 어떻게 번역할까<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;요?</b></p>
							</div>
                            <table>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question35" name="issue10" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question35">직접 영어로 작성하신 부분이기 때문에 따로 번역하지 않습니다.</label></td>
                                </tr>
                                <tr style="height:22px;" valign="top">
									<td align="right" style="width:50px;"><input type="radio" id="question36" name="issue10" runat ="server"/></td>
									<td align="left" style="padding-left:4px;"><label for="question36">영어로 작성하신 부분도 그대로 번역란에 옮겨 적습니다.</label></td>
                                </tr>
                            </table>
                        </li>
					</ol>
				</fieldset >
			</div>
            <div>
                <asp:Literal ID="ltScript" runat="server"></asp:Literal>
            </div>            
			<div class="btn-r">
				<a href="TransStep1.aspx" class="btn btn-prestep"><span>이전단계</span></a>
                <asp:Button ID="btnNext" runat="server" Text="" CssClass="btn btn-submit2" 
                    runat ="server" OnClientClick = "return Validate();" onclick="btnNext_Click" />
			</div>
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />     
       <asp:HiddenField ID="hdAgeDivision" runat="server" />
       <asp:HiddenField ID="hdSchool" runat='server' /> 
        <asp:HiddenField ID="hdMatePlus" runat='server' /> 
    </div>    
    </form>
</body>
</html>
