﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransStep1.aspx.cs" Inherits="Mate_Intro_TransStep1" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/IntroRight.ascx" tagname="IntroRight" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <style>
        hr{display:inherit;}
        th{width:60px;}
        #btn_addr{display:inline-block;width:70px;height:25px;background:#e1e1e1;text-decoration:none;vertical-align:middle;text-align:center;font-size:12px;border:1px solid #d5d5d5}
    </style>

    <script type="text/javascript">
    <!--
        //Backspace, F5, Ctrl+N, Ctrl+R, Ctrl+C, Ctrl+V 막기
        document.onkeydown = NewEdit;
        function NewEdit() {
            return;
            var keyval = event.keyCode;
            if (keyval == 116) {
                EventSet();
                alert("F5 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 78)) {
                EventSet();
                alert("Ctrl + N 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 82)) {
                EventSet();
                alert("Ctrl + R 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 86)) {
                EventSet();
                alert("Ctrl + V 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 67)) {
                EventSet();
                alert("Ctrl + C 키를 금지합니다.");
            }

            //Backspace
            if (keyval == 8) {
                if (event.srcElement.tagName == "INPUT" || event.srcElement.tagName == "TEXTAREA") {
                } else {
                    EventSet();
                }
            }
        }

        function EventSet() {
            event.keyCode = 0;
            event.cancelBubble = true;
            event.returnValue = false;
        }



        var $page = {
            init: function () {

                $page.setAddressEvent($("[name=area]:checked").val());
                // 국가 선택
                $("[name=area]").click(function () {
                    $("#addr1").val("");
                    $("#addr2").val("");
                    $("#zipcode").val("");


                    $("#nation_zipcode").val("");
                    $("#nation_addr1").val("");
                    $("#nation_addr2").val("");

                    $page.setAddressEvent($(this).val());
                });


                // 고등학생, 성인
                $(":input:radio[name=rd]").click(function () {
                    if ($(this).val() == "adult") {
                        $("#schoolName").hide();
                    } else {
                        $("#schoolName").show();
                    }
                });


                $("#nation_zipcode").val($("#zipcode").val());
                $("#nation_addr1").val($("#addr1").val());
                $("#nation_addr2").val($("#addr2").val());



                if ($("#dspAddrDoro").val() != "") {
                	$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
                	if ($("#dspAddrJibun").val() != "") {
                		$("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
                	}

                } else if ($("#addr1").val() != "") {

                	addr_array = $("#addr1").val().split("//");
                	$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + addr_array[0] + " " + $("#addr2").val());
                	if (addr_array[1]) {
                		$("#addr_jibun").text("[지번주소] (" + $("#zipcode").val() + ") " + addr_array[1] + " " + $("#addr2").val());
                	}
                }

            },

            setAddressEvent: function (area) {
                if (area == "area_internal") {

                    $("#btn_addr").show();
                    $(".nation_tr").hide();
                    $("#un_nation_tr").show();
                    $("#nation").val("");

                    $("#btn_addr").click(function () {
                        var pop = window.open("address.aspx?callback=jusoCallback", "pop", "width=570,height=420, scrollbars=yes, resizable=yes");
                        return false;
                    });
                } else {

                    $("#btn_addr").hide();
                    $(".nation_tr").show();
                    $("#un_nation_tr").hide();

                    $("#btn_addr").unbind("click");
                }

            }
        }

        //-->


        $(function () {
            $page.init();
        });

        function jusoCallback(zipNo, addr1, addr2, roadFullAddr, roadAddrPart1, addrDetail, roadAddrPart2, engAddr, jibunAddr, admCd, rnMgtSn, bdMgtSn) {
        	$("#zipcode").val(zipNo);
        	$("#addr1").val(addr1);
            $("#addr2").val(addr2);
			
            $("#addr_road").text("[도로명주소] (" + zipNo + ") " + roadFullAddr + " " + addr2);
            $("#addr_jibun").text("[지번주소] (" + zipNo + ") " + jibunAddr + " " + addr2);
        }


        function onSubmit() {
            if ($("#name").val() == "" || $("#birth").val() == "" || $("#email").val() == "") {
                alert("'마이컴패션>개인정보 수정'으로 이동하여 누락된 개인정보 필수 항목을 입력해주세요.\r(필수 항목: 생년월일, 이메일, 주소)");
                return false;
            }
            if ($("[name=area]:checked").val() == "foreign" && $("#nation").val() == "") {
                alert("국가를 선택해주세요.");
                $("#nation").focus();
                return false;
            }

            if ($("[name=area]:checked").val() == "foreign") {
            	$("#zipcode").val($("#nation_zipcode").val());
            	$("#addr1").val($("#nation_addr1").val());
            	$("#addr2").val($("#nation_addr2").val());
            }

            if ($("#zipcode").val() == "") {
                alert("우편번호를 입력해주세요.");
                $("#zipcode").focus();
                return false;
            }
            if ($("#addr1").val() == "") {
                alert("주소를 입력해주세요.");
                $("#addr1").focus();
                return false;
            }
            if ($("#addr2").val() == "") {
                alert("상세주소를 입력해주세요.");
                $("#addr2").focus();
                return false;
            }

            
            if ($("input[name=rd]:checked").val() == "student" && $("#txtSchoolName").val() == "") {
                alert("고등학생인 경우,\n학교 정보를 입력해주세요.");
                $("#txtSchoolName").focus();
                return false;
            }


            return true;
        }
        

    </script>
</head>
<%--<body>--%>
<body oncontextmenu='return false' ondragstart='return false'>
    <form id="form1" runat="server">     
	<input type="hidden" runat="server" id="hd_ExistAddress" value="" />
    <input type="hidden" runat="server" id="hfAddressType" value="" />
	<input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />

    <div>
        <%if(string.IsNullOrEmpty(sExtend)){  %>
        <uc2:Top ID="Top1" runat="server" />  
        <%} %>        
        <!-- container -->
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="apply pdf-down">
 
        <%if(string.IsNullOrEmpty(sExtend)){  %>
		<!-- sidebar -->
		<uc4:IntroRight ID="IntroRight1" runat="server" />
		<!-- //sidebar -->
        <%} %>  
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>번역 MATE</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/Intro/MateIntro.aspx">MATE 소개</a><strong>MATE 신청</strong>
			</div>
            <%if(string.IsNullOrEmpty(sExtend)){  %>
			<ul class="tab2 tab-mate">
				<li class="on"><a href="TransIntro.aspx" class="t1">번역 MATE</a></li>
				<li><a href="officemate.aspx" class="t2">사무 MATE</a></li>
				<li><a href="eventmate.aspx" class="t3">행사 MATE</a></li>
				<li><a href="specialitymate.aspx" class="t4">전문 MATE</a></li>
			</ul>
            <%} %> 
            <%if(string.IsNullOrEmpty(sExtend)){  %> 
			<%--<div class="txt-procedure">
				<h4>편지 번역 메이트 신청 절차</h4>
			</div>--%>
            <%} %>
            <div class="txt-procedure" style="margin-bottom:10px;">
				<h4>편지 번역 메이트 신청 절차</h4>
			</div>
			<ul id="tap1" runat="server" class="tab3 tab-step">
				<li class="on"><a href="javascript:void(0);" class="t1">STEP01 메이트 정보 입력</a></li>
				<li><a href="javascript:void(0);" class="t2">STEP02 번역유의사항 PDF DOWN</a></li>
				<li><a href="javascript:void(0);" class="t3">STEP03 편지번역 SAMPLE TEST</a></li>
				<li><a href="javascript:void(0);" class="t4">STEP04 번역 메이트 활동 동의서 서명</a></li>
				<li><a href="javascript:void(0);" class="t5">STEP05 지원완료</a></li>
			</ul>
			<div class="step-wrap" style="font-family:굴림">

                <div style="width:489px;margin:0 auto;padding-top:60px;">
                    <hr style="border: solid 2px #a5a5a5;margin-bottom:20px;">
                    <h4 style="font-size:15px;color:#505050;margin-bottom:10px;">1. 봉사활동 인증 관련 안내</h4>

                    <div style="border:1px solid #d5d5d5;padding:15px;color:#757575;line-height:20px;">
                        교육부 <2017학년도 학생 봉사활동 운영계획>과 서울특별시교육청 <2017학년도 학생 봉사활동 운영계획>에 의해 학생들은 1) 학교 교육과정에 의한 봉사활동 절차와, 2) 개인 계획에 의한 봉사활동 절차를 통해 봉사활동 시간을 인정받을 수 있습니다. 두 절차 중, 2) 개인 계획에 의한 봉사활동 절차를 따를 경우, 학생이 제출한 봉사활동확인서를 학교에서 평가하고 학교생활기록부에 기재할 수 있다고 명시되어 있습니다.<br />

                        교육부에서도 "봉사활동 인정 기관은 학교장이 인정하는 여러 기관에서 가능하며, VMS, 1365등 자원봉사포털을 통해 발급된 봉사활동확인서만 인정할 수 있는 법적근거는 없습니다. 또한 봉사활동은 창의적체험활동 교육과정의 일환으로써 학교장의 판단하에 자율적으로 시행하게 되며, 학교생활기록부는 교내활동으로 업격히 제한하나 학생 봉사활동은 외부 활동 기재를 폭넓게 인정하고 있습니다."라고 답변하였습니다.<br />

                        따라서 학생 메이트님께서 1) 현재 재학중이신 학교에 사전 봉사활동 계획서를 제출한 후 2) 메이트 홈페이지에서 발급받은 봉사활동확인서를 제출하면, 봉사내역이 VMS, 1365 등의 자원봉사포털에 등록되어 있지 않더라도 번역메이트 봉사활동을 인정받을 수 있음을 확인하였습니다.<br />
                        
                        한국컴패션 번역메이트 봉사활동은 VMS, 1365를 통한 봉사활동확인서 발급이 불가능하므로, 학생 메이트님께서는 활동연장을 신청하시기 전, 재학 중이신 학교의 한국컴패션 번역메이트 활동 인정 여부를 꼭 확인하신 후에 신청해주시기 바랍니다.

                    </div>
                    
                    <h4 style="font-size:15px;color:#505050;margin-top:20px;margin-bottom:10px;">2. 메이트 개인정보</h4>
                    <div style="border:1px solid #d5d5d5;padding:15px;color:#757575;line-height:27px;">
                        <table>
                            <tr>
                                <th>이름</th>
                                <td>
                                    <input type="hidden" id="name" runat="server" />
                                    <asp:Literal ID="lbName" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <th>생년월일</th>
                                <td>
                                    <input type="hidden" id="birth" runat="server" />
                                    <asp:Literal ID="lbBirth" runat="server" ></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <th>휴대폰</th>
                                <td>
                                    <input type="hidden" id="phone" runat="server" />
                                    <asp:Literal ID="lbPhone" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <th>이메일</th>
                                <td>
                                    <input type="hidden" id="email" runat="server" />
                                    <asp:Literal ID="lbEmail" runat="server" ></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <th>국내/외</th>
                                <td>
                                    <label><asp:RadioButton ID="area_internal" GroupName="area" runat="server" CssClass="area"/>국내</label>&nbsp;
                                    <label><asp:RadioButton ID="area_foreign" GroupName="area" runat="server" CssClass="area" value="foreign"/>해외</label>
                                </td>
                            </tr>
                            <tr class="nation_tr">
                                <th>국가</th>
                                <td>
                                    <asp:DropDownList runat="server" ID="nation"></asp:DropDownList>&nbsp;
                                </td>
                            </tr>
							<tr class="nation_tr">
								<th>우편번호</th>
								<td>
									<asp:TextBox ID="nation_zipcode" runat="server" BorderWidth="1px" Height="20px" Width="80px" style="" MaxLength="20"></asp:TextBox><br />
								</td>
							</tr>
							<tr class="nation_tr">
								<th>주소</th>
								<td>
									<asp:TextBox ID="nation_addr1" runat="server" BorderWidth="1px" Height="20px" Width="300px" style="" MaxLength="50"></asp:TextBox><br />
								</td>
							</tr>
							<tr class="nation_tr">
								<th>상세주소</th>
								<td>
									<asp:TextBox ID="nation_addr2" runat="server" BorderWidth="1px" Height="20px" Width="300px" style="" MaxLength="50"></asp:TextBox><br />
								</td>
							</tr>
                            <tr id="un_nation_tr">
                                <th>주소</th>
                                <td>
                                    <a href="#" id="btn_addr" runat="server">주소검색</a>
									<input type="hidden" id="zipcode" runat="server" value=""/>
									<input type="hidden" id="addr1" runat="server"  value=""/>
									<input type="hidden" id="addr2" runat="server"  value=""/>
									
									<p id="addr_road" class="mt15"></p>
									<p id="addr_jibun" class="mt10"></p>

                                </td>
                            </tr>
                            
                        </table>

                        <div style="line-height:1.5;padding-top:20px;">
                            * 메이트님의 개인정보를 다시 한 번 확인해 주세요.<br />
                            필수 정보가 잘못 입력되었거나, 미 입력된 경우 <a runat="server" id="link_my" target="_blank" style="color:#005dab">'마이컴패션</a><a runat="server" id="link_modify" target="_blank" style="color:#005dab">>개인정보 수정'</a> 수정으로 이동하여 변경 후 다시 지원해 주세요.
                        </div>
                    </div>
                    <div style="border:1px solid #d5d5d5;padding:15px;color:#757575;line-height:27px;margin-top:20px;margin-bottom:20px;">
                        <input type="radio" runat="server" id="rdstudent" name="rd" checked value="student" class="grade"/>고등학생&nbsp;&nbsp;&nbsp;
                        <input type="radio" runat="server" id="rdAdult" name="rd" value="adult" class="grade"/>성인(대학생 이상)<br />
                        <div id="schoolName">
                            <asp:TextBox ID="txtSchoolName" runat="server" BorderWidth="1px" Height="20px" Width="160px" Enabled="true" style=""></asp:TextBox> 예)OO고등학교 / 홈스쿨링 등<br />
                        </div>
                        메이트님의 정보를 수정 또는 확인하신 후 [다음]을 클릭해주세요.

                    </div>

                    <h4 style="font-size:15px;color:#505050;margin-bottom:10px;">3. MATE+ 신청</h4>

                    <div style="border:1px solid #d5d5d5;padding:15px;color:#757575;line-height:20px;">
                        <span style="color:#00BFFF;font-weight: bold;"> MATE+ 란?</span><br />
                        번역활동 이외에 더 많은 활동으로 어린이들에게 사랑을 전하고 주변 사람들에게 따뜻한 감동을 전하길 원하는 컴패션 번역메이트들의 모임입니다.<br /><br />
                        <span style="color:#00BFFF;font-weight: bold;">MATE+와 함께 하시면,</span><br />
                        번역 활동 외 특별히 진행되는 컴패션 행사, 온라인 감동 나눔, 봉사 활동, 소액 어린이 후원, 강연/공연 참여 등 다양한 행사 소식을 가장 먼저 접하실 수 있습니다. (관련 소식은 MATE+메이트님들께만 공지 드립니다.)<br /><br />
                        연간 행사 계획도 안내 드릴 예정이니, 관심 있는 MATE+ 이벤트에 미리 체크해둘 수도 있어요!<br /><br />
                        번역메이트 그 이상의 커뮤니티, MATE+!!<br />
                        지금 신청하세요. ^^<br /><br />
                        <asp:RadioButtonList ID="rdMatePlus" runat="server" RepeatLayout="Flow">
                            <asp:ListItem Value="False" Selected="True">활동연장, 수료증, MATE DAY, 컴패션 행사 등 번역메이트 활동 관련 주요 공지사항만 안내 받고 싶습니다.</asp:ListItem>
                            <asp:ListItem Value="True">[MATE+] MATE 주요 공지사항은 물론 특별한 메이트 행사 및 활동에 대한 정보도 받고 싶습니다.</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>

                <div style="background:url('/Intro/image/transstep1.jpg') no-repeat; height:730px; width:545px; margin:0 auto 0 auto;display:none;">
                    <a id="submission" runat="server" href="/Files/school_submission.jpg" target="_blank" style="position:absolute;width:84px;height:25px;top:340px;left:533px;border:0px solid red;"></a>
                    
                </div>
			</div>
			<div class="btn-r">				
                <asp:Button ID="btnNext" runat="server" Text="" CssClass="btn btn-nextstep" onclick="btnNext_Click" OnClientClick="return onSubmit();" />
			</div>
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

       <%if(string.IsNullOrEmpty(sExtend)){  %>
		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" /> 
       <%} %>       
    </div>    
    </form>
</body>
</html>
