﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MateExtension_Notice.aspx.cs" Inherits="Mate_Intro_MateExtension_Notice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <style type="text/css">
        body{margin:auto;}
        
        .trans{width:800px;margin:0px auto 0px auto;list-style:none;padding-left:0px;background:url('/Intro/image/ExtensionNotice.jpg') no-repeat;}
        .trans .notice{position:relative;width:800px;height:628px;background-position:0 0;}
        
        .btn-notice_1{position:absolute;width:65px;height:20px;top:36px;margin-left:688px;border:0px solid red;}
        .btn-notice_2{position:absolute;width:65px;height:20px;top:216px;margin-left:688px;border:0px solid red;}
        .btn-notice_3{position:absolute;width:65px;height:20px;top:366px;margin-left:688px;border:0px solid red;}
    </style>

    <script type="text/javascript">
    <!--
        //Backspace, F5, Ctrl+N, Ctrl+R, Ctrl+C, Ctrl+V 막기
        document.onkeydown = NewEdit;
        function NewEdit() {
            var keyval = event.keyCode;
            if (keyval == 116) {
                EventSet();
                alert("F5 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 78)) {
                EventSet();
                alert("Ctrl + N 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 82)) {
                EventSet();
                alert("Ctrl + R 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 86)) {
                EventSet();
                alert("Ctrl + V 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 67)) {
                EventSet();
                alert("Ctrl + C 키를 금지합니다.");
            }

            //Backspace
            if (keyval == 8) {
                if (event.srcElement.tagName == "INPUT" || event.srcElement.tagName == "TEXTAREA") {
                } else {
                    EventSet();
                }
            }
        }

        function EventSet() {
            event.keyCode = 0;
            event.cancelBubble = true;
            event.returnValue = false;
        }

        //TransStep4.aspx 이동
        function ShowExtendClause() {

            sWidth = "780";
            sHeight = "800";

            if (screen.width < 1025) {
                LeftPosition = 0;
                TopPosition = 0;
            } else {
                LeftPosition = (screen.width) ? (screen.width - sWidth) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - sHeight) / 2 : 100;
            }

            window.open("TransStep5.aspx?Extend=2", "ExtendClause", "width=" + sWidth + ",height=" + sHeight + ",top=" + TopPosition + ",left=" + LeftPosition + ", scrollbars=yes, toolbar=no menubar=no, location=no");
        }
    //-->
    </script>
</head>

<%--<body>--%>
<body oncontextmenu='return false' ondragstart='return false'>
    <form id="form2" runat="server">
    <script>
        function getHost() {
            var host = '';
            var loc = String(location.href);
            if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
            else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
            else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
            return host;
        }

        function goLink(linkType) {
            var host = getHost();
            var param = '';
            if (linkType == 1) param = '/community/noticeview.aspx?iTableIndex=1001&iNoIndex=113&iBoardIdx=107496&b_num=108979&re_level=0&ref=108979&re_step=0&pageIdx=0&searchOption=all&searchTxt=1365';
            else if (linkType == 2) param = '/community/noticeview.aspx?iTableIndex=1001&iNoIndex=125&iBoardIdx=110039&b_num=111402&re_level=0&ref=111402&re_step=0&pageIdx=0&searchOption=all&searchTxt=1365';
            else if (linkType == 3) param = '/community/noticeview.aspx?iTableIndex=1001&iNoIndex=136&iBoardIdx=117227&b_num=118299&re_level=0&ref=118299&re_step=0&pageIdx=0&searchOption=all&searchTxt=1365';
            if (host != '') location.href = 'http://' + host + param;
        }
    </script>
    <div>
        <!-- conts -->
        <div class="trans">
            <div class="notice"></div>
            <a href="javascript:goLink(1)" target="_blank" class="btn-notice_1"></a>
            <a href="javascript:goLink(2)" target="_blank" class="btn-notice_2"></a>
            <a href="javascript:goLink(3)" target="_blank" class="btn-notice_3"></a>
        </div>
		<!-- // conts -->     
    </div>
    </form>
</body>
</html>
