﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Text;
using CommonLib;

public partial class Mate_Intro_TransStep5 : System.Web.UI.Page
{
    string sTranslationType;
    string sMateYearCode;
    string sMateWorkSDate;
    string sMateWorkEDate;
    string sMateWorkMonth;


    public string sExtend;
    public string AgeDivision;
    public string School = String.Empty;
    public String sMatePlus;
    public Boolean bMatePlus;

    protected void Page_Load(object sender, EventArgs e)
    {
        sTranslationType = Request.QueryString["TranslationType"];
        sExtend = Request.QueryString["Extend"];

        if (Request.QueryString["AgeDivision"] != null && Request.QueryString["AgeDivision"] != "")
            AgeDivision = Request.QueryString["AgeDivision"];

        if (Request.QueryString["School"] != null && Request.QueryString["School"] != "")
            School = Request.QueryString["School"];

        if (Request.QueryString["MatePlus"] != null && Request.QueryString["MatePlus"] != "")
            sMatePlus = Request.QueryString["MatePlus"];

        //모집 체크
        Object[] objSql = new object[1] { "MATE_RecruitCheck" };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", null, null);

        sMateYearCode = ds.Tables[0].Rows[0][2].ToString();
        //Response.Write("<script>alert('" + sMateYearCode + "');</script>");

        if (sExtend == "2")
            tap.Attributes.Add("style", "display:none;");

        if (!IsPostBack)
        {
            txtUserName.Text = new UserInfo().UserName;

            if (Request.QueryString["isPop"] == "1")
            {
                objSql = new object[1] { "SELECT * FROM MATE_Joining WHERE userid = '" + new UserInfo().UserId + "' AND MateYearCode = '" + sMateYearCode + "'" };

                DataSet dsCheck = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

                if (dsCheck.Tables[0].Rows.Count > 0)
                    Label1.Text = Convert.ToDateTime(dsCheck.Tables[0].Rows[0]["RegisterDT"]).ToString("yyyy-MM-dd");

                else
                    Label1.Text = DateTime.Now.ToString("yyyy-MM-dd");

                txtUserName.ReadOnly = true;
            }

            else
                Label1.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        GetWorkDate();
    }

    /// <summary>
    /// 번역 기간
    /// </summary>
    private void GetWorkDate()
    {
        //일반
        if (string.IsNullOrEmpty(sExtend))
        {
            //old : #13313 신영섭 선생님 요청으로 mate.compassion.or.kr\Web_Backoffice\backoffice\mate\mate_sample_test_info.asp 의 내용으로 변경
            //Object[] objSql = new object[1] { "MATE_MateWorkDate" };
            //Object[] objParam = new object[1] { "MateYearCode " };
            //Object[] objValue = new object[1] { sMateYearCode };

            //WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            //DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //if (data.Tables[0].Rows.Count > 0)
            //{
            //    sMateWorkSDate = data.Tables[0].Rows[0][0].ToString();//시작
            //    sMateWorkEDate = data.Tables[0].Rows[0][1].ToString();//종료
            //    //sMateWorkMonth = data.Tables[0].Rows[0][2].ToString();//기간(달)
            //    sMateWorkMonth = "6";

            //    sMateWorkSDate = sMateWorkSDate.Substring(0, 4).ToString() + "년 " + int.Parse(sMateWorkSDate.Substring(5, 2).ToString()).ToString() + "월 " + sMateWorkSDate.Substring(8, 2) + "일";

            //    sMateWorkEDate = sMateWorkEDate.Substring(0, 4).ToString() + "년 " + int.Parse(sMateWorkEDate.Substring(5, 2).ToString()).ToString() + "월 " + sMateWorkEDate.Substring(8, 2) + "일";
            //}

            //else if (data.Tables[0].Rows.Count == 0)
            //{
            //    string sjs = JavaScript.HeaderScript.ToString();
            //    sjs += JavaScript.GetAlertScript("마감시간을 초과하여 신청이 불가능합니다.");
            //    sjs += JavaScript.GetPageMoveScript("/Default.aspx");
            //    sjs += JavaScript.FooterScript.ToString();
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            //    return;
            //}

            //new : #13313 신영섭 선생님 요청으로 mate.compassion.or.kr\Web_Backoffice\backoffice\mate\mate_sample_test_info.asp 의 내용으로 변경
            //Object[] objSql = new object[1] { "select * from mate_sample_test_info" };
            //WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
            //DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);
            //Label2.Text = ds.Tables[0].Rows[0]["play_date"].ToString();



            //using (FrontDataContext dao = new FrontDataContext())
            //{
                //var playDate = dao.mate_sample_test_info.FirstOrDefault().play_date;
                var playDate = www6.selectQF<mate_sample_test_info>().play_date;

                Label2.Text = playDate.ToString();
            //}
        }

        else//연정신청
        {
            //하반기
            if (DateTime.Now.Month >= 7 && DateTime.Now.Month <= 12)
            {
                sMateWorkSDate = DateTime.Now.Year + Convert.ToInt32(1) + "년 3월 2일";
                sMateWorkEDate = DateTime.Now.Year + Convert.ToInt32(1) + "년 8월 31일";
            }

            //상반기
            else if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 6)
            {
                sMateWorkSDate = DateTime.Now.Year + "년 7월 1일";
                sMateWorkEDate = DateTime.Now.Year + "년 12월 31일";
            }

            sMateWorkSDate = DateTime.Now.Year + "년 2월 26일";
            sMateWorkEDate = DateTime.Now.Year + "년 8월 26일";
            sMateWorkMonth = "6";

            Label2.Text = sMateWorkSDate + " ~ " + sMateWorkEDate + "까지 " + sMateWorkMonth + "개월 간";
        }

        //Label2.Text = sMateWorkSDate + " ~ " + sMateWorkEDate + "까지 " + sMateWorkMonth + "개월 간";
        //var mateData = new MateAction().GetSampleTestInfo();
        //if (mateData.success) {
        //	var data = (mate_sample_test_info)mateData.data;
        //	Label2.Text = data.play_date;
        //}
    }


    protected void btnNext_Click(object sender, EventArgs e)
    {

        if (!UserInfo.IsLogin)
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("로그인을 하신후 메이트 신청이 가능 합니다.");
            //sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/Intro/transstep1.aspx");
            sjs += JavaScript.GetPageMoveScript("/login.aspx");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        //테스트 2013-11-04
        //string sjs1 = JavaScript.HeaderScript.ToString();
        //sjs1 += JavaScript.GetAlertScript("sExtend: " + sExtend);
        //sjs1 += JavaScript.FooterScript.ToString();
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "Error1", sjs1);

        //일반
        if (string.IsNullOrEmpty(sExtend))
        {
            MateApply();
        }

        else//연장신청
        {
            //테스트 2013-11-04
            //sjs1 = JavaScript.HeaderScript.ToString();
            //sjs1 += JavaScript.GetAlertScript("연장신청");
            //sjs1 += JavaScript.FooterScript.ToString();
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Error2", sjs1);


            UserInfo sess = new UserInfo();
            //연장신청 체크[1]:연장체크 기간 체크 [2]:이미등록되있는 메이트 체크
            Object[] objSql = new object[2] { "SELECT * FROM MATE_Recruit WHERE MateYearCode = dbo.uf_GetMateYearCode(1) AND CONVERT(DATE, GETDATE()) BETWEEN ExtendStart AND ExtendEnd",
            "SELECT * FROM MATE_Master WHERE MateID = '" + sess.UserId + "' AND MateYearCode = dbo.uf_GetMateYearCode(1) AND Status = 'ACTIVE'" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            //연장신청 버튼 보이기/숨기기
            if (ds.Tables[0].Rows.Count == 0)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("활동연장 신청기간이 아닙니다");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs);
            }
            else
            {
                //연장신청 버튼 보이기/숨기기
                if (ds.Tables[0].Rows.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    objSql = new object[1] { "SELECT * FROM MATE_Master WHERE MateYearCode = (SELECT MateYearCode FROM MATE_Recruit WHERE CONVERT(DATE, GETDATE()) BETWEEN WorkingStart AND WorkingEnd AND(RIGHT(MateYearCode,2) = '11' OR RIGHT(MateYearCode,2) = '12')) AND MateID = '" + sess.UserId + "'" };
                    ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

                    bool bExtentionApply = Convert.ToBoolean(ds.Tables[0].Rows[0]["ExtentionApply"]);//연장신청유무

                    if (bExtentionApply)
                    {
                        string sjs = JavaScript.HeaderScript.ToString();
                        sjs += JavaScript.GetAlertScript("이미 연장 완료되었습니다");
                        sjs += JavaScript.FooterScript.ToString();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
                        return;
                    }
                    else
                    {
                        MateExtendApply();
                    }

                    ds.Dispose();
                    _WWW6Service.Dispose();
                }

                else
                {
                    string sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("현재 활동중인 번역메이트만 활동 연장이 가능합니다.");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                }
            }










        }
    }

    /// <summary>
    /// 메이트 신청
    /// </summary>
    private void MateApply()
    {
        //WWWService.Service _wwwService = new WWWService.Service();
        //DataSet dsCheck = new DataSet();
        //UserInfo user = new UserInfo();

        //Object[] objSql = new object[1] { "SELECT * FROM MATE_Joining WHERE MateYearCode = '" + sMateYearCode + "' AND SponsorID = '" + user.SponsorID + "'" };

        //WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        //try
        //{
        //    //기존연결
        //    //dsCheck = _wwwService.checkDATMateJoining(user.SponsorID //SponsorID
        //    //                                         , "번역" //Mate타입
        //    //                                         );

        //    //신청내역
        //    dsCheck = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

        //}
        ////Exception Error
        //catch (Exception ex)
        //{
        //    //Exception Error Insert           
        //    _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

        //    string sjs = JavaScript.HeaderScript.ToString();
        //    sjs += JavaScript.GetAlertScript("Mate신청여부를 확인 중 오류가 발생했습니다. \\r\\nException Error Message : " +
        //                                      ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
        //    sjs += JavaScript.FooterScript.ToString();
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
        //    return;
        //}
        ////DB Error
        //if (dsCheck == null)
        //{
        //    string sjs = JavaScript.HeaderScript.ToString();
        //    sjs += JavaScript.GetAlertScript("Mate신청여부를 확인 중 오류가 발생했습니다.\\r\\n관리자에게 문의해주세요.");
        //    sjs += JavaScript.FooterScript.ToString();
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
        //    return;
        //}
        ////
        //else
        //{
        //    //신청내역이 있으면 return false
        //    if (dsCheck.Tables[0].Rows.Count > 0)
        //    {
        //        string sjs = JavaScript.HeaderScript.ToString();
        //        sjs += JavaScript.GetAlertScript("해당 Mate는 이미 신청하신 상태로 승인처리 중 입니다.");
        //        sjs += JavaScript.FooterScript.ToString();
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

        //        return;
        //    }
        //}

        WWWService.Service _wwwService = new WWWService.Service();
        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        WWWService.Service wwwService = new WWWService.Service();
        //DataSet data = wwwService.MateMyActive(new UserInfo().UserId);
        Object[] objSql = new object[1] { "SELECT * FROM MATE_Master WHERE MateID = '" + new UserInfo().UserId + "' AND MateYearCode = '" + sMateYearCode + "' AND Status <> '" + "STOP'" };
        DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

        if (data.Tables[0].Rows.Count == 0)
        {
        }

        else
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("이미 번역메이트 활동중에 계신 회원님이십니다.");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        UserInfo sess = new UserInfo();
        string sResult = string.Empty;
        //MATE_Joining 등록 성공 유무
        int iResult;

        #region //getData
        string sApplicationReason = Session["cause"].ToString();

        //봉사경험 여부
        string sExperienceYN = "";

        //봉사경험
        string sExperienceDescription = "";

        //번역
        string sTranslation = Session["trans"].ToString();

        // 연령구분 , 학교 정보 우선 세션에서 체크

        string AgeDiv = "";
        string School = "";
        if (Session["AgeDivision"] != null)
        {
            AgeDiv = Session["AgeDivision"].ToString();
            School = Session["School"].ToString();
            sMatePlus = Session["MatePlus"].ToString();
        }
        else
        {
            AgeDiv = Server.UrlDecode(Request.QueryString["AgeDIvision"].ToString());
            School = Server.UrlDecode(Request.QueryString["School"].ToString());
        }

        if (AgeDiv != "성인" && AgeDiv != "고등학생")
        {
            AgeDiv = HttpUtility.UrlDecode(AgeDiv);
            School = HttpUtility.UrlDecode(School);
        }


        #endregion

        try
        {
            string sponsorId = "";
            using (AuthDataContext dao = new AuthDataContext())
            {
                //sponsorId = dao.tSponsorMaster.First(p => p.UserID == sess.UserId).tempSponsorID;
                sponsorId = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId).tempSponsorID;
            }

            if (string.IsNullOrEmpty(sponsorId))
                sponsorId = sess.SponsorID;

            if (sMatePlus != null && !sMatePlus.Equals(""))
            {
                bMatePlus = Convert.ToBoolean(sMatePlus);
            }

            objSql = new object[1] { "DELETE MATE_Joining WHERE MateYearCode = '" + sMateYearCode + "' AND SponsorID = '" + sponsorId + "'" };

            data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            objSql = new object[1] { "MATE_WebJoiningSave" };

            Object[] objParam = new object[14] { "rowState",//쿼리 선택 값	
                                                 "MateYearCode",//기수
                                                 "SponsorID",//SponsorID
                                                 "SponsorName",//SponsorName
                                                 "UserID",//WebID
                                                 "ApplicationMotive",//신청 사유
                                                 "ApplicationDT",//신청 날짜
                                                 "SmsReceive",//수신여부
                                                 "Experience",//봉사활동내역
                                                 "TranslationType",//(번역,작문) 타입
                                                 "SampleTest",//샘플번역
                                                 "School",//학교명
                                                 "AgeDiv",//연령구분
                                                 "Mate_plus"
                                            };
            Object[] objValue = new object[14] { "NEW",
                                                sMateYearCode,
                                                sponsorId,
                                                sess.UserName,
                                                sess.UserId,
                                                sApplicationReason,
                                                "",
                                                "",
                                                sExperienceDescription,
                                                sTranslationType,
                                                sTranslation,
                                                School,
                                                AgeDiv,
                                                bMatePlus
                                            };
            //MATE_Joining 등록
            iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);
        }

        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate신청 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        //DB Error
        if (iResult < 1)
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate신청 중 오류가 발생했습니다.");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        //신청 등록 완료
        else
        {
            Session["AgeDivision"] = null;
            Session["School"] = null;

            //완료페이지로 이동
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetPageMoveScript("TransStep6.aspx?TranslationType=" + sTranslationType + "&MateYearCode=" + sMateYearCode);
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
            return;
        }
    }
    /// <summary>
    /// 메이트 연장신청
    /// </summary>
    private void MateExtendApply()
    {
        UserInfo sess = new UserInfo();

        string sjs = JavaScript.HeaderScript.ToString();

        Object[] objSql = new object[1] { "UPDATE MATE_Master SET ExtentionApply = 1, ExtentionDate = GETDATE() WHERE MateYearCode = '" + sMateYearCode + "' AND MateID = '" + new UserInfo().UserId + "'" };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        try
        {
            if (sMatePlus != null && !sMatePlus.Equals(""))
            {
                bMatePlus = Convert.ToBoolean(sMatePlus);
            }

            string sponsorId = "";
            using (AuthDataContext dao = new AuthDataContext())
            {
                //sponsorId = dao.tSponsorMaster.First(p => p.UserID == sess.UserId).tempSponsorID.EmptyIfNull();
                sponsorId = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId).tempSponsorID.EmptyIfNull();
            }
            if (sponsorId == "")
            {
                sponsorId = sess.SponsorID;
            }

            _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            objSql = new object[1] { "MATE_WebJoiningSave" };

            Object[] objParam = new object[7] { "rowState",//쿼리 선택 값	
                                                "MateYearCode",//기수
                                                "SponsorID",//SponsorID
                                                "UserID",//WebID
                                                "School",//학교명
                                                "AgeDiv",//연령구분
                                                "Mate_plus"
                                              };
            Object[] objValue = new object[7] { "EXTENSION",
                                                sMateYearCode,
                                                sponsorId,
                                                sess.UserId,
                                                School,
                                                AgeDivision,
                                                bMatePlus
                                              };

            //MATE_Joining 업데이트
            int iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //StringBuilder sb = new StringBuilder();
            //sb.Append("<script type='text/javascript' language ='javascript' >\n ");
            //sb.Append("FinishPopUp();");
            //sb.Append("</script>");
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "ExtendPage", sb.ToString());

            sjs = JavaScript.HeaderScript.ToString();
            sjs += "FinishPopUp()";
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "func0", sjs.ToString());

            return;
        }

        catch (Exception ex)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("연장신청을 실패 했습니다\\r\\n관리자에게 문의 하세요" + ex.Message.ToString());
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
        }
    }
}
