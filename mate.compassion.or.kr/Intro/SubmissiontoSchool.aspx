﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SubmissiontoSchool.aspx.cs" Inherits="Mate_Intro_SubmissiontoSchool" %>

<%--<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="/Controls/Header.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="/Controls/Top.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>--%>

<%--<%@ Register src="/Controls/MemberShipRight.ascx" tagname="MemberShipRight" tagprefix="sideMenu" %>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <%--<uc1:Header ID="Header1" runat="server" />--%>
    
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("webfont", "1");
        google.setOnLoadCallback(function () {
            WebFont.load({ custom: {
                families: ["NanumGothic"],
                urls: ["http://fontface.kr/NanumGothic/css"]
            }
            });
        });
    </script>
    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, button, select, input, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, menu, textarea{margin:0;padding:0;border:0;font-size:9pt;font-family:'NanumGothic'}
        body{background:#fff;color:#333;line-height:1.4;}
        html, body{height:100%;}
        li{list-style:none}
        a{color:#333;text-decoration:none;cursor:pointer}
        a:hover{text-decoration:underline}
                
        .etc .faq{margin:0px;}
        .etc .faq .invite-img{width:900px;height:1273px;background:url('/mate/Files/school_submission.jpg') no-repeat;}
        .etc .faq .invite-btn{width:900px;border:0px solid #eee;padding:15px;}
    </style>

    <script type="text/javascript" src="/common/js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript">
    <!--
        function fnPrint() {
            //document.body.innerHTML = inviteImg.innerHTML;
            window.print();
        }
    //-->
    </script>
</head>

<%--<body>--%>
<body oncontextmenu='return false' ondragstart='return false'>
    <form id="form1" runat="server">     
    <div>
        <%--<uc2:Top ID="Top1" runat="server" /> --%>       

        <!-- container -->
        <div id="container" class="etc">

            <center>
	        <!-- wrapper -->
	        <div id="wrapper" class="faq">

		        <!-- contents -->
		        <div id="contents" id="inviteImg">
 
                    <div class="invite-img">
                        <img src="/Files/school_submission.jpg" width="900px" height="1273px" border="0" />
                    </div> 
  
			        <%--<div class="invite-btn">
			        </div>--%>
 
		        </div>
		        <!-- // contents -->
	        </div>
	        <!-- // wrapper -->
            </center>

        </div>
	    <!-- // container -->    

        <%--<uc3:Footer ID="Footer1" runat="server" />--%>      
    </div>    
    </form>

</body>
</html>
