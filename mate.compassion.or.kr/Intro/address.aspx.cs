﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class _common_popup_address : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e) {
        roadFullAddr.Value = Request["roadFullAddr"].EmptyIfNull();
        roadAddrPart1.Value = Request["roadAddrPart1"].EmptyIfNull();
        addrDetail.Value = Request["addrDetail"].EmptyIfNull();
        roadAddrPart2.Value = Request["roadAddrPart2"].EmptyIfNull();
        engAddr.Value = Request["engAddr"].EmptyIfNull();
        jibunAddr.Value = Request["jibunAddr"].EmptyIfNull();
        zipNo.Value = Request["zipNo"].EmptyIfNull();
        admCd.Value = Request["admCd"].EmptyIfNull();
        rnMgtSn.Value = Request["rnMgtSn"].EmptyIfNull();
        bdMgtSn.Value = Request["bdMgtSn"].EmptyIfNull();
        inputYn.Value = Request["inputYn"].EmptyIfNull();
		callback.Value = Request["callback"].EmptyIfNull();
		store_session.Value = Request["store_session"].ValueIfNull("Y");        // session 에 지번,도로명 주소를 보관하여, update 시에 tSponsorAddressAll 에 업데이트 , N이면 tSponsorAddressAll에 저장안함
		confmKey.Value = ConfigurationManager.AppSettings["addressApiKey"];
		// callback
		if (inputYn.Value == "Y") {

			DataSet dsAddress = new DataSet();

			dsAddress.Tables.Add();

			dsAddress.Tables[0].Columns.Add("inputZip");
			dsAddress.Tables[0].Columns.Add("inputAddr1");
			dsAddress.Tables[0].Columns.Add("inputAddr2");
			dsAddress.Tables[0].Columns.Add("inputCode");
			dsAddress.Tables[0].Columns.Add("jibunZip");
			dsAddress.Tables[0].Columns.Add("jibunAddr1");
			dsAddress.Tables[0].Columns.Add("jibunAddr2");
			dsAddress.Tables[0].Columns.Add("jibunCode");
			dsAddress.Tables[0].Columns.Add("roadZip");
			dsAddress.Tables[0].Columns.Add("roadAddr1");
			dsAddress.Tables[0].Columns.Add("roadAddr2");
			dsAddress.Tables[0].Columns.Add("roadCode");
			dsAddress.Tables[0].Columns.Add("AddressUsage");
			dsAddress.Tables[0].Columns.Add("Reference");
			dsAddress.Tables[0].Columns.Add("NNMZ");
			dsAddress.Tables[0].Columns.Add("MSG");

			var addr2 = addrDetail.Value + " " + roadAddrPart2.Value;
			dsAddress.Tables[0].Rows.Add(zipNo.Value.ToString(), roadAddrPart1.Value , addr2, "1",
										 zipNo.Value.ToString(), jibunAddr.Value, addr2, "2",
										 zipNo.Value.ToString(), roadAddrPart1.Value, addr2, "3",
										 "3", "", "", "");
			Session["dsHouseAddress"] = dsAddress;

		}

	}
}