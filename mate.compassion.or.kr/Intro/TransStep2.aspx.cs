﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Mate_Intro_TransStep2 : System.Web.UI.Page {
	string sjs = string.Empty;
	String AgeDivision;
    String School;
    String bMatePlus;

    protected void Page_Load(object sender, EventArgs e)
    {

		if (!UserInfo.IsLogin) {
			sjs = JavaScript.HeaderScript.ToString();
			sjs += JavaScript.GetAlertScript("로그인이 필요합니다.");
			sjs += JavaScript.GetPageMoveScript("/login.aspx");
			sjs += JavaScript.FooterScript.ToString();
			Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
			return;
		}

		if(Session["AgeDIvision"] == null || Session["School"] == null) {
			//Response.Redirect("TransStep1.aspx");
		}


		AgeDivision = Session["AgeDIvision"].ToString();
        School = Session["School"].ToString();
        bMatePlus = Session["MatePlus"].ToString();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["AgeDivision"] != null && Request.QueryString["AgeDivision"] != "")
            AgeDivision = Request.QueryString["AgeDivision"];

        if (Request.QueryString["School"] != null && Request.QueryString["School"] != "")
            School = Request.QueryString["School"];

        if (Request.QueryString["MatePlus"] != null && Request.QueryString["MatePlus"] != "")
            bMatePlus = Request.QueryString["MatePlus"];


        Response.Redirect("TransStep4.aspx?AgeDivision=" + AgeDivision + "&School=" + School + "&MatePlus=" + bMatePlus);
    }

    protected void btn_download_Click(object sender, EventArgs e)
    {
        string sBaseDir = Server.MapPath("/");

        FileInfo fi = new FileInfo(Path.Combine(sBaseDir, "matemanual.pdf"));

        if (fi.Exists == false)
        {
            //돌아갈페이지 스크립트
            string sJs = string.Empty;
            sJs = JavaScript.HeaderScript.ToString();
            sJs += JavaScript.GetAlertScript("파일이 존재하지않아 다운로드를 중지합니다.");
            sJs += JavaScript.GetHistoryBackScript();
            sJs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sJs);
            return;
        }

        //강제 다운로드
        Response.Clear();//버퍼 비우기
        Response.ContentType = //바이너리 파일
            "application/octet-stream";
        Response.AddHeader(//파일 다운로드명 설정
            "Content-Disposition",
            "attachment;filename=" +
            Server.UrlPathEncode("matemanual.pdf"));//한글처리
        Response.WriteFile(	//다운로드
            Path.Combine(sBaseDir, "matemanual.pdf"));
        //[!] 다운로드 횟수 증가
        Response.End();//버퍼 비우고, 종료.
    }
}