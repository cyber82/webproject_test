﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Collections;

public partial class Mate_Intro_TransIntro : System.Web.UI.Page
{
    DateTime dt;
    string sjs = string.Empty;
    string sgMateYearCode;

    protected static string dateStr, dateEndStr;
    protected static string dateFlag, dateEndFlag;
    protected static string dateYN, dateEndYN;

    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //신청하기 시간에 따른 조건을 위한 변수 선언 2012-05-31
        if (!IsPostBack)
        {
            //번역 타입 선택
            //ArrayList aList = new ArrayList();
            //aList.Add("선택");
            //aList.Add("어린이편지");
            //aList.Add("후원자편지");
            //this.ddlTranslationType.DataSource = aList;
            //this.ddlTranslationType.DataBind();
        }

        //연장신청 체크[1]:연장체크 기간 체크 [2]:이미등록되있는 메이트 체크
        Object[] objSql = new object[2] { "SELECT * FROM MATE_Recruit WHERE MateYearCode = dbo.uf_GetMateYearCode(1) AND CONVERT(DATE, GETDATE()) BETWEEN ExtendStart AND ExtendEnd", 
            "SELECT * FROM MATE_Master WHERE MateID = '" + new UserInfo().UserId + "' AND MateYearCode = dbo.uf_GetMateYearCode(1) AND Status != 'STOP'" };

        //Object[] objSql = new object[2] { "SELECT * FROM MATE_Recruit WHERE MateYearCode = '" + "201411" + "' AND CONVERT(DATE, GETDATE()) BETWEEN ExtendStart AND ExtendEnd", 
        //    "SELECT * FROM MATE_Master WHERE MateID = '" + new UserInfo().UserId + "' AND MateYearCode = '" + "201411" + "' AND Status = 'ACTIVE'" };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

        //연장신청 버튼 보이기/숨기기
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[1].Rows.Count > 0)
        {
            //this.imgBtnExtendApply.Visible = true;
            //SetImgBtnExtendApply();//연장신청버튼 스크립트 설정
        }

        else
        {
            //this.imgBtnExtendApply.Visible = false;
        }

        ds.Dispose();
        _WWW6Service.Dispose();
    }


    //연장신청버튼 스크립트 설정
    private void SetImgBtnExtendApply()
    {
        Object[] objSql = new object[1] { "SELECT * FROM MATE_Master WHERE MateYearCode = (SELECT MateYearCode FROM MATE_Recruit WHERE CONVERT(DATE, GETDATE()) BETWEEN WorkingStart AND WorkingEnd AND(RIGHT(MateYearCode,2) = '11' OR RIGHT(MateYearCode,2) = '12')) AND MateID = '" + new UserInfo().UserId + "'" };
        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

        bool bExtentionApply = Convert.ToBoolean(ds.Tables[0].Rows[0]["ExtentionApply"]);//연장신청유무
        sgMateYearCode = ds.Tables[0].Rows[0]["MateYearCode"].ToString();//기수

        if (bExtentionApply)
        {
            //수정 2013-11-04
            //this.imgBtnExtendApply.Attributes.Add("onClick", "alert('이미 연장 신청을 했습니다.'); return false;");
            //this.imgBtnExtendApply.Attributes.Add("onClick", "alert('활동연장 신청이 정상 접수되었습니다.'); return false;");
        }

        else
        {  
            //this.imgBtnExtendApply.Attributes.Add("onClick", "return confirm('연장신청을 하시겠습니까?');");
            //this.imgBtnExtendApply.Attributes.Add("onClick", "ShowExtendClause(); return false;");

            //Response.End();
        }

        ds.Dispose();
        _WWW6Service.Dispose();
    }
    
    #region 신청하기 버튼
    protected void btnNext_Click(object sender, EventArgs e)
    {
        ////2017-08-14 14:00:00
        //var nowDate = DateTime.Now;
        //var no3 = new DateTime(2017, 8, 14, 14, 0, 0);
        //int ret3 = DateTime.Compare(nowDate, no3);
        //if (ret3 >= 0)
        //{
        //    //신청 마감시에 처리
        //    sjs = JavaScript.HeaderScript.ToString();
        //    sjs += JavaScript.GetAlertScript("모집기간이 아닙니다.");
        //    sjs += JavaScript.FooterScript.ToString();
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
        //    return;
        //}

        ////2차 : 2017-08-11 14:00:00
        //var no2 = new DateTime(2017, 8, 11, 14, 0, 0);
        //int ret2 = DateTime.Compare(nowDate, no2);
        //if (ret2 >= 0)
        //{
        //    //Response.Redirect("/Intro/TransStep1.aspx");
        //}
        //else
        //{
        //    //1차 : 2017-08-09 17:00:00
        //    var no1 = new DateTime(2017, 8, 9, 17, 0, 0);
        //    int ret1 = DateTime.Compare(nowDate, no1);
        //    if (ret1 >= 0)
        //    {
        //        Response.Redirect("/community/noticeview.aspx?iTableIndex=1001&iNoIndex=858&iBoardIdx=181968&b_num=179784&re_level=0&ref=179784&re_step=0&pageIdx=0&searchOption=all&searchTxt=");
        //        return;
        //    }
        //}


        //모집 체크
        Object[] objSql = new object[1] { "MATE_RecruitCheck" };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", null, null);

        if (ds.Tables[0].Rows[0][0].ToString() == "2")
        {
            //신청 마감시에 처리
            sjs = JavaScript.HeaderScript.ToString();
            //sjs += JavaScript.GetAlertScript("모집기간이 아닙니다.");
            //sjs += JavaScript.GetAlertScript("2차 전형 지원 기간: 2/9(금) 오전 10시 ~2/11(일) 오후 2시");
            sjs += JavaScript.GetAlertScript("모집 기간이 아닙니다.\\n14기 번역메이트 신규모집은 8월에 진행되니 참고해주세요!");
            sjs += JavaScript.FooterScript.ToString();

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            return;
        }

        ds.Dispose();

        if (!UserInfo.IsLogin)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("로그인이 필요합니다.");
			//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/Intro/transintro.aspx");
			sjs += JavaScript.GetPageMoveScript("/login.aspx");
			sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        WWWService.Service _wwwService = new WWWService.Service();

        DataSet dsCheck = new DataSet();
        UserInfo user = new UserInfo();

        try
        {
            //신청내역
            objSql = new object[2] { "SELECT * FROM MATE_Joining WHERE MateYearCode = '" + ds.Tables[0].Rows[0][2].ToString() + "' AND UserID = '" + new UserInfo().UserId + "'",
                                     "SELECT * FROM MATE_Master WHERE  MateYearCode = '" + ds.Tables[0].Rows[0][2].ToString() + "' AND MateID = '" + new UserInfo().UserId + "'"};

            dsCheck = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);
        }

        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert           
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate신청여부를 확인 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        //DB Error
        if (dsCheck == null)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate신청여부를 확인 중 오류가 발생했습니다.\\r\\n관리자에게 문의해주세요.");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        else
        {
            sjs = JavaScript.HeaderScript.ToString();

            //신청내역이 있으면 return false
            if (dsCheck.Tables[1].Rows.Count == 0 && dsCheck.Tables[0].Rows.Count > 0)
            {
                bool isApply = false;

                if (ds.Tables[0].Rows[0][0].ToString() == "0")
                {
                    if (dsCheck.Tables[0].Rows[0]["RecruitType"].ToString() == "NORMAL")
                        isApply = true;
                }

                else if (ds.Tables[0].Rows[0][0].ToString() == "1")
                {
                    if (dsCheck.Tables[0].Rows[0]["RecruitType"].ToString() == "ADD")
                        isApply = true;
                }


                if (isApply)
                {
                    sjs += JavaScript.GetAlertScript("이미 신청하셨습니다.");
                    sjs += JavaScript.FooterScript.ToString();

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                    dsCheck.Dispose();

                    return;
                }
            }

            else if (dsCheck.Tables[1].Rows.Count > 0 && dsCheck.Tables[1].Rows[0]["Status"].ToString() != "STOP" && dsCheck.Tables[1].Rows[0]["Status"].ToString() != "PAUSE")
            {
                sjs += JavaScript.GetAlertScript("이미 등록된 메이트 입니다. ");
                sjs += JavaScript.FooterScript.ToString();

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                dsCheck.Dispose();

                return;
            }
        }

        string sTranslationType = "";

        WWWService.Service wwwService = new WWWService.Service();

        // 신규 모집하는 메이트의 년도와 코드를 저장
        int Year = Convert.ToInt32(ds.Tables[0].Rows[0][2].ToString().Substring(0, 4));
        int Code = Convert.ToInt32(ds.Tables[0].Rows[0][2].ToString().Substring(4, 2));

        if (ds.Tables[0].Rows[0][0].ToString() == "0")
        {
            // 전 기수의 MateYearCode를 구하기 위한 if문
            if (Code == 11)
            {
                Year--;
                Code = 12;
            }

            else if (Code == 12)
                Code = 11;
        }

        String MateYearCode = Year.ToString() + Code.ToString();

        //[1]:번역 메이트 정보
        objSql = new object[1] { "SELECT * FROM MATE_Master WHERE MateID = '" + new UserInfo().UserId + "' AND MateYearCode = '" + MateYearCode + "'" + " AND Status != '" + "STOP'" };

        DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

        if (data.Tables[0].Rows.Count == 0) {
            //Response.Redirect("TransStep1.aspx");
            
            //2차 : 2017-08-11 14:00:00
            //var no2 = new DateTime(2017, 8, 11, 14, 0, 0);
            //int ret2 = DateTime.Compare(nowDate, no2);
            //if (ret2 >= 0) {
                Response.Redirect("/Intro/TransStep1.aspx");
            //}
        }
        else
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("현재 활동 중인 번역 메이트님께서는 신청하실 수 없습니다.\\r\\n감사합니다.");

            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
        }
    }
    #endregion

    #region 연장하기 버튼
    protected void imgBtnExtendApply_Click(object sender, ImageClickEventArgs e)
    {
        Object[] objSql = new object[1] { "UPDATE MATE_Master SET ExtentionApply = 1, ExtentionDate = GETDATE() WHERE MateYearCode = '" + sgMateYearCode + "' AND MateID = '" + new UserInfo().UserId + "'" };
        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        try
        {
            DataSet dsResult = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("연장신청을 했습니다");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Success", sjs);
        }

        catch (Exception ex)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("연장신청을 실패 했습니다\\r\\n관리자에게 문의 하세요");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
        }

        //연장신청버튼 스크립트 설정
        SetImgBtnExtendApply();
    }
    #endregion
}
