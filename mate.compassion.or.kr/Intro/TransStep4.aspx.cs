﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class Mate_Intro_TransStep4 : System.Web.UI.Page {
	string sjs = string.Empty;
	string sTranslationType;
    string AgeDivision;
    string School;
    String bMatePlus;

    protected void Page_Load(object sender, EventArgs e) {
		if (!UserInfo.IsLogin) {
			sjs = JavaScript.HeaderScript.ToString();
			sjs += JavaScript.GetAlertScript("로그인이 필요합니다.");
			sjs += JavaScript.GetPageMoveScript("/login.aspx");
			sjs += JavaScript.FooterScript.ToString();
			Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
			return;
		}

		if (Session["AgeDIvision"] == null || Session["School"] == null || Session["MatePlus"] == null) {
			//Response.Redirect("TransStep1.aspx");
		}

		if (!IsPostBack)
        {
            hdAgeDivision.Value = Request.QueryString["AgeDIvision"].ToString();
            hdSchool.Value = Request.QueryString["School"].ToString();
            hdMatePlus.Value = Request.QueryString["MatePlus"].ToString();

            RandomImgSet();
        }
    }

    #region 번역 샘플이미지 설정 2012-11-30
    //수정 2012-11-30, 2013-09-13
    private void RandomImgSet()
    {
        if (Request.QueryString["num"] == null)
        {
            cont1.Attributes.Add("style", "display:block;");
            cont2.Attributes.Add("style", "display:none;");

			SetImage("1");

		}

        else if (Request.QueryString["num"] == "1")
        {
            cont1.Attributes.Add("style", "display:block;");
            cont2.Attributes.Add("style", "display:none;");

			SetImage("1");
		}

        else if (Request.QueryString["num"] == "2")
        {
            cont1.Attributes.Add("style", "display:none;");
            cont2.Attributes.Add("style", "display:block;");

			SetImage("2");
		}
    }
    #endregion

    #region 다음버튼
    protected void btnNext_Click(object sender, EventArgs e)
    {
        int transLen, causeLen; //문자열 길이

        string trans, cause;
        string num = Request.QueryString["num"];
        string sMateYearCode = Request["txtTranslationType"];

        if (String.IsNullOrEmpty(num) || num == "1")
        {
            transLen = sampleTrans1.Value.ToString().Length;
            causeLen = applyCause1.Value.ToString().Length;

            trans = sampleTrans1.Value;
            cause = applyCause1.Value;

            sTranslationType = "CHILDLETTER";
        }

        else
        {
            //주석처리 2012-09-12
            //주석품 2012-11-27
            transLen = sampleTrans2.Value.ToString().Length;
            causeLen = applyCause2.Value.ToString().Length;

            trans = sampleTrans2.Value;
            cause = applyCause2.Value;

            sTranslationType = "SPONSORLETTER";
        }

        //추가 2012-06-07 특수문자 변환
        for (int i = 0; i < transLen; i++)
        {
            trans.ToString().Replace("<", "&lt;");
            trans.ToString().Replace(">", "&gt;");
        }

        for (int i = 0; i < causeLen; i++)
        {
            cause.ToString().Replace("<", "&lt;");
            cause.ToString().Replace(">", "&gt;");
        }

        if (String.IsNullOrEmpty(trans))
        {
            string sjs = JavaScript.HeaderScript.ToString();

            sjs += JavaScript.GetAlertScript("샘플번역란에 번역 하셔야 합니다.");            
            sjs += JavaScript.FooterScript.ToString();

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sjs);
            return;
        }

        if (String.IsNullOrEmpty(cause))
        {
            string sjs = JavaScript.HeaderScript.ToString();

            sjs += JavaScript.GetAlertScript("지원동기를 작성해 주세요.");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sjs);
            return;
        }

        Session.Remove("trans");
        Session.Remove("cause");

        Session.Add("trans",trans);
        Session.Add("cause",cause);

        String AgeDivision = Request.QueryString["AgeDIvision"].ToString();
        String School = Request.QueryString["School"].ToString();
        String MatePlus = Request.QueryString["MatePlus"].ToString();

        Response.Redirect("TransStep5.aspx?TranslationType=" + sTranslationType + "&AgeDivision=" + AgeDivision + "&School=" + School+"&MatePlus=" + MatePlus);
    }
    #endregion


	private void SetImage(string type){
		var mateData = new MateAction().GetSampleTestList(type);
		if (mateData.success) {

            List<mate_sample_test> data = (List<mate_sample_test>)mateData.data;
            int max = data.Count;
            Random r = new Random();
            int selected = r.Next(0, max);
            List<mate_sample_test> selectedList = new List<mate_sample_test>();
            selectedList.Add(data[selected]);
            Session.Add("selectedtran", selected);

            hdSelectedTran.Value = selectedList[0].st_id.ToString();

            if(type == "1") {
                repeater.DataSource = selectedList;
                repeater.DataBind();
            } else {
                repeater1.DataSource = selectedList;
                repeater1.DataBind();
            }
		}
	}
}