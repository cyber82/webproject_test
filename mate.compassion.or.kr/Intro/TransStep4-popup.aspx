﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransStep4-popup.aspx.cs" Inherits="Mate_Intro_TransStep4" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <style>
        body{margin:0;}
    </style>
    <script type="text/javascript" src="/common/js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript">
        $(function () {
            var img = $("img").eq(0);
            var width = $(img).width();
            window.resizeTo(width, 700);
            
        });

    </script>
</head>
<body oncontextmenu='return false' ondragstart='return false'>
    <asp:Repeater runat=server ID=repeater>
		<ItemTemplate>
            <asp:Image ID="Image" runat="server" ImageUrl='<%#Eval("st_image") %>' />
		</ItemTemplate>
	</asp:Repeater>	
</body>
</html>
