﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="specialitymate.aspx.cs" Inherits="Mate_Intro_specialitymate" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/IntroRight.ascx" tagname="IntroRight" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="apply speciality-mate">
 
		<!-- sidebar -->
		<uc4:IntroRight ID="IntroRight1" runat="server" />
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>전문 MATE</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/Intro/MateIntro.aspx">MATE 소개</a><strong>MATE 신청</strong>
			</div>
			<ul class="tab2 tab-mate">
				<li><a href="TransIntro.aspx" class="t1">번역 MATE</a></li>
				<li><a href="officemate.aspx" class="t2">사무 MATE</a></li>
				<li><a href="eventmate.aspx" class="t3">행사 MATE</a></li>
				<li class="on"><a href="specialitymate.aspx" class="t4">전문 MATE</a></li>
			</ul>
			<div class="sec">
				<div class="text txt-intro">
					<h4>전문 메이트 소개</h4>
					<p><strong>각자에게 주어진 특별한 재능과 열정을 나누면 두배, 세배로 커진 사랑으로 아이들에게 전해집니다.</strong> - 디자인, 웹디자인, 사진 촬영, 영상 촬영 및 편집 등 전문 기술이 필요한 각 분야에서 탁월한 재능을 컴패션에 기부하는 MATE입니다. </p>
				</div>
			</div>
			<div class="sec">
				<div class="text txt-recruit">
					<h4>전문 메이트 모집 및 활동 시기</h4>
					<p>전문 MATE 모집 시, 컴패션 홈페이지 www.compassion.or.kr 를 통해 공지합니다.</p>
				</div>
			</div>
			<div class="text txt-apply">
				<h4>전문 메이트 신청방법</h4>
				<p>컴패션 홈페이지 www.compassion.or.kr 회원 가입 후 전문 mate 모집 공고 시 신청</p>
			</div>
			<div class="contact-wrap">
				<p class="text-contact">MATE 신청 관련 자세한 문의는 MATE 홈페이지 FAQ, 온라인상담 또는 전화 02-740-1000[구.02-3668-3400]를 이용해 주시기 바랍니다.</p>
			</div>
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    </form>
</body>
</html>
