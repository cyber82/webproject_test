﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MateIntro.aspx.cs" Inherits="Mate_Intro_MateIntro" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>

<%@ Register src="../Controls/IntroRight.ascx" tagname="IntroRight" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="mate-intro">
 
		<!-- sidebar -->
		<uc4:IntroRight ID="IntroRight1" runat="server" />
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>MATE란?</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/Intro/MateIntro.aspx">MATE 소개</a><strong>MATE란?</strong>			    
			</div>
			<div class="sec1">
				<p class="text txt-cover"><span>&quot;컴패션 메이트란?&quot; 컴패션 MATE는 컴패션의 비전에 공감하여 자발적으로 자신의 재능과 시간, 열정을 전 세계의 가난한 어린아이들을 위해 나누는 헌신된 자원봉사자들을 말합니다. 각각 은사를 받은 대로 하나님의 여러 가지 은혜를 맡은 선한 청지기 같이 서로 봉사하라 누가 봉사하려면 하나님이 공급하시는 힘으로 하는 것 같이 하라 이는 범사에 예수 그리스도로 말미암아 하나님이 영광을 받으시게 하려 함이니(베드로전서 4장 10-11절)</span></p>
                <div class="video">
					<object width="389" height="292"><param name="movie" value="http://www.youtube.com/v/5oa6oN6_TII?version=3&amp;hl=ko_KRversion=3&autohide=1&autoplay=1&rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/5oa6oN6_TII?version=3&amp;hl=ko_KRversion=3&autohide=1&autoplay=1&rel=0" type="application/x-shockwave-flash" width="389" height="292" allowscriptaccess="always" allowfullscreen="true"></embed></object>
				</div>
			</div>
			<div class="sec2">
				<div class="text txt-pursuit-worth">
					<h4>컴패션 메이트가 추구하는 가치</h4>
					<dl>
						<dt>Child-centered</dt>
						<dd>컴패션 Mate의 <strong>모든 활동의 중심에는 어린이가 있습니다.</strong>전세계의 가난한 어린이들이 내일의 소망을 품고 살아갈 수 있도록 그들에게 사랑과 희망을 나누어 줍니다.</dd>
						<dt>Excellence</dt>
						<dd>컴패션은 <strong>정직성, 투명성, 효율성을 바탕으로 일합니다.</strong>컴패션을 이끌어가는 또 하나의 축으로 컴패션 Mate 는어느 분야에서나 최상의 결실을 낼 수 있도록 노력합니다.</dd>
						<dt>Active</dt>
						<dd>컴패션 Mate는 모험을 두려워 하지 않으며오늘보다 더 나은 내일을 기대하는 마음으로 <strong>창의적인활동을 창출하는데 주의를 기울이는 역동적인 주체입니다.</strong></dd>
						<dt>Family</dt>
						<dd>컴패션은 하나님 안에서 새로운 가족입니다.컴패션 Mate는 컴패션의 가족으로서 <strong>서로 교제하며지속적인 관계를 가지고 사랑을 나누는 법을 배워갑니다.</strong></dd>
					</dl>
				</div>
			</div>
			<div class="text txt-do">
				<h4>컴패션 메이트가 하는 일</h4>
				<p>한국컴패션 활동과 관련한 모든 분야에서 활동하실 수 있습니다. 또 전세계의 어린이들을 빈곤에서 벗어나게 하기 위한 목적에 적합한 활동을 제안, 실행하는 것을 적극 권고합니다. 더 자세한 사항은 각분야의 메뉴를 참조해주세요.</p>
			</div>
			<div class="text txt-detail-view">
				<ul>
					<li class="fir-field">
						<strong>번역 MATE</strong> - 번역 MATE가 되시면 컴패션의 연중 다양한 결연 행사들의 진행을 돕는 활동을 합니다.<a href="TransIntro.aspx" class="btn btn-detail2">자세히 보기</a>
					</li>
					<li>
						<strong>사무 MATE</strong> - 사무 MATE가 되시면 한국컴패션 전 부서에 걸쳐 다양한 사무 업무 활동을 합니다.<a href="OfficeMate.aspx" class="btn btn-detail2">자세히 보기</a>
					</li>
					<li>
						<strong>행사 MATE</strong> - 행사 MATE가 되시면 한국컴패션의 연중 다양한 결연 행사들의 진행 활동을 합니다.<a href="EventMate.aspx" class="btn btn-detail2">자세히 보기</a>
					</li>
					<li>
						<strong>전문 MATE</strong> - 전문 MATE가 되시면 디자인, 웹디자인, 사진 촬영, 영상 촬영 및 편집 등 전문 활동을 합니다.<a href="specialitymate.aspx" class="btn btn-detail2">자세히 보기</a>
					</li>
				</ul>
			</div>
 
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>


		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    </form>
</body>
</html>
