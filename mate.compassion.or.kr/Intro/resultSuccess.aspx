﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="resultSuccess.aspx.cs" Inherits="Mate_Intro_resultSuccess" %>

<!DOCTYPE html>
<html lang="ko">
<head> 
	<meta charset="UTF-8">
    <meta property="og:image" content="http://www.compassion.or.kr/common/img/sns_default_image.jpg">
	<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title> 
	<link rel="stylesheet" href="/common/css/mate.css" />
	<script type="text/javascript" src="/common/js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="/common/js/common.js"></script>
    <script language="javascript">
        function ParentMove() {
            top.window.location.href = "TransStep4.aspx?";   
        }
    </script>
</head>
<body>
<div class="layer1 test-result-success">
	<div class="tit"><strong>번역유의사항 MINI TEST 결과</strong></div>
	<div class="lay-cont">
		<p class="txt txt1"><strong>축하드립니다!MINI TEST를 통과하셨습니다.편지 번역SAMPLE TEST로 이동합니다.</strong></p>
		<div class="btn-m">
			<a href="javascript:ParentMove();" class="btn btn-confirm"><span>확인</span></a>
		</div>
	</div>
	<button type="button" onclick="_close()" class="btn btn-close"><span>닫기</span></button>
</div>
</body>
</html>