﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Data;
using System.Configuration;

public partial class Mate_Intro_TransStep1 : System.Web.UI.Page
{
    string sjs = string.Empty;
    public String sExtend = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        ////2017-08-14 14:00:00
        //var nowDate = DateTime.Now;
        //var no3 = new DateTime(2017, 8, 14, 14, 0, 0);
        //int ret3 = DateTime.Compare(nowDate, no3);
        //if (ret3 >= 0)
        //{
        //    //신청 마감시에 처리
        //    sjs = JavaScript.HeaderScript.ToString();
        //    sjs += JavaScript.GetAlertScript("모집기간이 아닙니다.");
        //    sjs += JavaScript.GetPageMoveScript("/Intro/TransIntro.aspx");
        //    sjs += JavaScript.FooterScript.ToString();
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
        //    return;
        //}

        ////2차 : 2017-08-11 14:00:00
        //var no2 = new DateTime(2017, 8, 11, 14, 0, 0);
        //int ret2 = DateTime.Compare(nowDate, no2);
        //if (ret2 >= 0)
        //{
        //    //Response.Redirect("/Intro/TransStep1.aspx");
        //}
        //else
        //{
        //    //1차 : 2017-08-09 17:00:00
        //    var no1 = new DateTime(2017, 8, 9, 17, 0, 0);
        //    int ret1 = DateTime.Compare(nowDate, no1);
        //    if (ret1 >= 0)
        //    {
        //        sjs = JavaScript.HeaderScript.ToString();
        //        sjs += JavaScript.GetAlertScript("모집기간이 아닙니다.");
        //        sjs += JavaScript.GetPageMoveScript("/Intro/TransIntro.aspx");
        //        sjs += JavaScript.FooterScript.ToString();
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
        //        return;
        //    }
        //}


        if (!UserInfo.IsLogin)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("로그인이 필요합니다.");
            sjs += JavaScript.GetPageMoveScript("/login.aspx");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }





        var domain_www = ConfigurationManager.AppSettings["domain_www"];
        link_my.HRef = domain_www + "/my/";
        link_modify.HRef = domain_www + "/my/account/";

        if (Request.QueryString["Extend"] != null && Request.QueryString["Extend"] != "")
        {
            sExtend = Request.QueryString["Extend"];

            tap1.Attributes.Add("style", "display:none;");
        }

        else
        {
            submission.Attributes.Add("style", "position:absolute;width:84px;height:25px;top:505px;left:533px;border:0px solid red;");
            //rdstudent.Attributes.Add("style", "position:absolute;top:1045px;left:153px;");
            //rdAdult.Attributes.Add("style", "position:absolute;top:1045px;left:235px;");
            //txtSchoolName.Attributes.Add("style", "position:absolute;top:1072px;left:153px;");
        }


        if (!IsPostBack)
        {
            // 개인정보
            UserInfo sess = new UserInfo();
            SetUserInfo(sess);
            SetUserAddress(sess);
        }

    }


    protected void SetUserAddress(UserInfo sess)
    {
        var countryResult = new CodeAction().Countries();
        if (countryResult.success)
        {
            nation.Items.Add(new ListItem("국가", ""));
            DataTable dt = (DataTable)countryResult.data;
            foreach (DataRow dr in dt.Rows)
            {
                nation.Items.Add(new ListItem(dr["CodeName"].ToString(), dr["CodeID"].ToString()));
            }
        }
        if (sess.LocationType.EmptyIfNull() != "")
        {

            if (sess.LocationType == "국내")
            {
                area_internal.Checked = true;
            }
            else
            {
                area_foreign.Checked = true;
                nation_addr1.Enabled = false;
                nation_addr2.Enabled = false;
                nation_zipcode.Enabled = false;
            }
        }
        var addr_result = new SponsorAction().GetAddress();
        if (addr_result.success)
        {
            SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;
            hfAddressType.Value = addr_data.AddressType;
            dspAddrDoro.Value = addr_data.DspAddrDoro;
            dspAddrJibun.Value = addr_data.DspAddrJibun;
            hd_ExistAddress.Value = addr_data.ExistAddress ? "1" : "0";

            addr1.Value = addr_data.Addr1;
            addr2.Value = addr_data.Addr2;
            zipcode.Value = addr_data.Zipcode;

            if (addr_data.Zipcode.EmptyIfNull() != "")
            {
                btn_addr.Visible = false;
                area_internal.Enabled = false;
                area_foreign.Enabled = false;
                nation.Enabled = false;
            }

            if (addr_data.Country.EmptyIfNull() != "")
            {
                nation.SelectedValue = addr_data.Country;
            }
        }


    }


    protected void SetUserInfo(UserInfo sess)
    {

        if (sess.UserName.EmptyIfNull() != "")
        {
            name.Value = sess.UserName;
            lbName.Text = sess.UserName;
        }
        if (sess.Birth.EmptyIfNull() != "")
        {
            birth.Value = sess.Birth.EmptyIfNull().Length > 9 ? sess.Birth.Substring(0, 10) : "1900-01-01";
            lbBirth.Text = sess.Birth.EmptyIfNull().Length > 9 ? sess.Birth.Substring(0, 10) : "1900-01-01";
        }

        var comm_result = new SponsorAction().GetCommunications();
        if (comm_result.success)
        {
            SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
            phone.Value = comm_data.Mobile;
            email.Value = comm_data.Email;

            lbPhone.Text = comm_data.Mobile;
            lbEmail.Text = comm_data.Email;
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        // 개인정보 업데이트
        UserInfo sess = new UserInfo();

        if (sExtend == "2")
        {

        }
        else
        {
            //모집 체크
            Object[] objSql2 = new object[1] { "MATE_RecruitCheck" };

            WWW6Service.SoaHelper _WWW6Service2 = new WWW6Service.SoaHelper();

            DataSet ds2 = _WWW6Service2.NTx_ExecuteQuery("SqlCompass5", objSql2, "SP", null, null);

            if (ds2.Tables[0].Rows[0][0].ToString() == "2")
            {
                //신청 마감시에 처리
                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("모집기간이 아닙니다.");
                sjs += JavaScript.FooterScript.ToString();

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                return;
            }

            ds2.Dispose();
        }



        #region 활동연장을 신청할 경우, 재신청 여부 체크
        if (sExtend == "2" && rdMatePlus.SelectedValue.ToString().Equals("False"))
        {
            //연장신청 체크[1]:연장체크 기간 체크 [2]:이미등록되있는 메이트 체크
            Object[] objSql = new object[2] { "SELECT * FROM MATE_Recruit WHERE MateYearCode = dbo.uf_GetMateYearCode(1) AND CONVERT(DATE, GETDATE()) BETWEEN ExtendStart AND ExtendEnd",
            "SELECT * FROM MATE_Master WHERE MateID = '" + sess.UserId + "' AND MateYearCode = dbo.uf_GetMateYearCode(1) AND Status = 'ACTIVE'" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            //연장신청 버튼 보이기/숨기기
            if (ds.Tables[0].Rows.Count == 0)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("활동연장 신청기간이 아닙니다");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs);
            }
            else
            {
                //연장신청 버튼 보이기/숨기기
                if (ds.Tables[0].Rows.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    objSql = new object[1] { "SELECT * FROM MATE_Master WHERE MateYearCode = (SELECT MateYearCode FROM MATE_Recruit WHERE CONVERT(DATE, GETDATE()) BETWEEN WorkingStart AND WorkingEnd AND(RIGHT(MateYearCode,2) = '11' OR RIGHT(MateYearCode,2) = '12')) AND MateID = '" + sess.UserId + "'" };
                    ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

                    bool bExtentionApply = Convert.ToBoolean(ds.Tables[0].Rows[0]["ExtentionApply"]);//연장신청유무

                    if (bExtentionApply)
                    {
                        string sjs = JavaScript.HeaderScript.ToString();
                        sjs += JavaScript.GetAlertScript("이미 연장 완료되었습니다");
                        sjs += JavaScript.FooterScript.ToString();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
                        return;
                    }
                    else
                    {
                        btnNext_Click_DO();
                    }

                    ds.Dispose();
                    _WWW6Service.Dispose();
                }

                else
                {
                    string sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("현재 활동중인 번역메이트만 활동 연장이 가능합니다.");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                }
            }

        }
        #endregion 활동연장을 신청할 경우, 재신청 여부 체크
        else
        {
            btnNext_Click_DO();
        }



    }

    protected void btnNext_Click_DO()
    {
        // 개인정보 업데이트
        UserInfo sess = new UserInfo();

        // 기존에 주소가 없는경우
        if (hd_ExistAddress.Value == "0")
        {

            Response.Write(addr1.Value + " : " + addr2.Value);

            using (AuthDataContext dao = new AuthDataContext())
            {

                //var user = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
                var user = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);
                user.LocationType = area_internal.Checked ? "국내" : "국외";
                user.Addr1 = (addr1.Value + "$" + (area_internal.Checked ? "한국" : nation.SelectedValue)).Encrypt();
                user.Addr2 = addr2.Value.Encrypt();
                user.ZipCode = zipcode.Value.Encrypt();
                //dao.SubmitChanges();
                www6.updateAuth(user);
            }

            //JsonWriter result = new SponsorAction().UpdateAddress(false, hfAddressType.Value, area_internal.Checked ? "국내" : "국외", nation.SelectedValue, zipcode.Value, addr1.Value, addr2.Value);

        }

        /*20160206 SPONSOR 정보 COMPASS4 에 저장 - START*/

        var sSponsorID = sess.SponsorID;
        JsonWriter actionResult;
        SponsorAction sponsorAction = new SponsorAction();

        ErrorLog.Write(HttpContext.Current, 0, "test");

        if (string.IsNullOrEmpty(sSponsorID))
        {
            // 실명인증,본인인증은 ajax 로 처리되기때문에 ci ,di , jumin 값은 넘기지 않는다.
            actionResult = sponsorAction.AddSponsor(area_internal.Checked ? "국내" : "국외", nation.SelectedValue, zipcode.Value, addr1.Value, addr2.Value, phone.Value, email.Value, null, birth.Value);
            if (actionResult.success)
            {
                sSponsorID = (string)actionResult.data;
                this.ViewState["sponsorId"] = sSponsorID;
            }

        }
        else
        {

            actionResult = sponsorAction.UpdateSponsor(area_internal.Checked ? "국내" : "국외", nation.SelectedValue, zipcode.Value, addr1.Value, addr2.Value, phone.Value, email.Value, null, birth.Value);
        }

        /*20160206 SPONSOR 정보 COMPASS4 에 저장 - END*/

        if (rdstudent.Checked)
        {
            if (txtSchoolName.Text != "")
            {
                Session["AgeDivision"] = "고등학생";
                Session["School"] = txtSchoolName.Text;
                Session["MatePlus"] = rdMatePlus.SelectedValue.ToString();

                string School = HttpUtility.UrlEncode(txtSchoolName.Text);
                string AgeDivision = HttpUtility.UrlEncode("고등학생");
                string MatePlus = rdMatePlus.SelectedValue.ToString();
                if (sExtend == "2")
                    Response.Redirect("TransStep5.aspx?Extend=2&AgeDivision=" + AgeDivision + "&School=" + School + "&MatePlus=" + MatePlus);

                else
                    Response.Redirect("TransStep2.aspx?AgeDivision=" + AgeDivision + "&School=" + School + "&MatePlus=" + MatePlus);
            }

            else
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("고등학생인 경우,\\r\\n학교 정보를 입력해주세요.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
                return;
            }
        }

        else
        {
            Session["AgeDivision"] = "성인";
            Session["School"] = "";
            Session["MatePlus"] = rdMatePlus.SelectedValue.ToString();

            string AgeDivision = HttpUtility.UrlEncode("성인");
            string MatePlus = rdMatePlus.SelectedValue.ToString();

            if (sExtend == "2")
                Response.Redirect("TransStep5.aspx?Extend=2&AgeDivision=" + AgeDivision + "&School=" + "&MatePlus=" + MatePlus);

            else
                Response.Redirect("TransStep2.aspx?AgeDivision=" + AgeDivision + "&School=" + "&MatePlus=" + MatePlus);
        }
    }

    protected void rd_onCheckedChanged(object sender, EventArgs e)
    {
        if (rdstudent.Checked)
            txtSchoolName.Enabled = true;

        else
            txtSchoolName.Enabled = false;
    }
}