﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mate_Intro_TransStep3 : System.Web.UI.Page {
	string sjs = string.Empty;
	string sMateYearCode;
    String AgeDivision;
    String School;
    String bMatePlus;

    protected void Page_Load(object sender, EventArgs e) {
		if (!UserInfo.IsLogin) {
			sjs = JavaScript.HeaderScript.ToString();
			sjs += JavaScript.GetAlertScript("로그인이 필요합니다.");
			sjs += JavaScript.GetPageMoveScript("/login.aspx");
			sjs += JavaScript.FooterScript.ToString();
			Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
			return;
		}

		if (Session["AgeDIvision"] == null || Session["School"] == null || Session["MatePlus"] == null) {
			Response.Redirect("TransStep1.aspx");
		}

		sMateYearCode = Request.QueryString["MateYearCode"];
        hdAgeDivision.Value = Request["AgeDIvision"].ToString();
        hdSchool.Value = Request["School"].ToString();
        hdMatePlus.Value = Request["MatePlus"].ToString();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        int sum = 0;

        if (question3.Checked)
            sum += 10;

        if (question7.Checked)
            sum += 10;

        if (question10.Checked)
            sum += 10;

        if (question15.Checked)
            sum += 10;

        if (question20.Checked)
            sum += 10;

        if (question23.Checked)
            sum += 10;

        if (question27.Checked)
            sum += 10;

        if (question30.Checked)
            sum += 10;

        if (question32.Checked)
            sum += 10;

        if (question36.Checked)
            sum += 10;

        if (sum == 100)
        {
            String sjs = JavaScript.HeaderScript.ToString();
            sjs += "alertSuccess()";
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CetifyPageMove", sjs.ToString());

            return;

            //this.ClientScript.RegisterStartupScript(this.GetType(), "layerPopup", "<script>layer('resultSuccess.aspx?MateYearCode=" + sMateYearCode+ "','415','332');</script>");
        }

        else
        {
            String sjs = JavaScript.HeaderScript.ToString();
            sjs += "alertfail()";
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CetifyPageMove", sjs.ToString());

            return;
            //this.ClientScript.RegisterClientScriptBlock(this.GetType(), "layerPopup", "<script>layer('resultFail.aspx?MateYearCode=" + sMateYearCode + "','415','332');</script>");        
        }

        //if (sum == 70)        
        //    ltScript.Text = "<script>layer('resultSuccess.aspx','415','332');</script>";
        //else
        //    ltScript.Text = "<script>layer('resultFail.aspx','415','332');</script>";

        //Response.Redirect("TransStep3.aspx");
    }
}