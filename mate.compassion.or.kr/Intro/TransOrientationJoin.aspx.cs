﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Data;

public partial class Mate_Intro_TransOrientationJoin : System.Web.UI.Page
{
    UserInfo sess;
    public string mateYN;

    DateTime dt;
    string dateStr, dateEndStr;
    string dateFlag, dateEndFlag;
    string dateYN, dateEndYN;
    
    public string sgShowList;
    public DataSet _dsOrientation = new DataSet();

    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        string sjs = "";

        sess = new UserInfo();

        dt = DateTime.Now;

        //테스트를 위해 주석처리
        /**/
        //오리엔테이션 신청기간 
        //수정 2013-03-08, 수정 2013-05-28
        //dateStr = "2017-02-22 14:00:00";
        //dateEndStr = "2017-02-23 11:00:00";
        dateStr = "2018-02-19 14:00:00";
        dateEndStr = "2018-02-23 11:00:00";

        dateFlag = dt.CompareTo(Convert.ToDateTime(dateStr)).ToString();
        dateEndFlag = dt.CompareTo(Convert.ToDateTime(dateEndStr)).ToString();

        if (int.Parse(dateFlag) > -1)
            dateYN = "Y";

        else
            dateYN = "N";

        if (int.Parse(dateEndFlag) > -1)
            dateEndYN = "Y";

        else
            dateEndYN = "N";

        //수정 2013-03-07, 수정 2013-05-28
        if (dateYN == "N" && dateEndYN == "N")
        {
            //신청 이전시에 처리
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("오리엔테이션 신청은 2월 19일(월) 오후 2시부터 시작됩니다.\\r\\n감사합니다.");
            sjs += JavaScript.GetPageMoveScript("/Default.aspx");
            sjs += JavaScript.FooterScript.ToString();

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            return;
        }

        //추가 2013-03-07
        else if (dateYN == "Y" && dateEndYN == "Y")
        {
            //신청 마감시에 처리
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("오리엔테이션 신청이 마감되었습니다. 감사합니다.");
            sjs += JavaScript.GetPageMoveScript("/Default.aspx");
            sjs += JavaScript.FooterScript.ToString();

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            return;
        }
        /**/
        //신청하기 시간에 따른 조건을 위한 변수 선언 2012-05-31
        if (!IsPostBack)
        {
            if (!UserInfo.IsLogin)
            {
                sjs = JavaScript.HeaderScript.ToString();
                //수정 2013-03-07
                //sjs += JavaScript.GetAlertScript("로그인을 하신후 메이트 신청이 가능 합니다.");
                sjs += JavaScript.GetAlertScript("로그인이 필요합니다.");
				//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/Intro/TransOrientationJoin.aspx");
				sjs += JavaScript.GetPageMoveScript("/login.aspx");
				sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }
        }



        //후원자 신청 확인
        mateYN = MateYN();

        //mateYN = "Y";

        if (mateYN == "Y")
        {
            Object[] objSql = new object[1] { "SELECT * FROM MATE_Orientation WHERE isOpen = '1' AND MateYearCode = '201811' " };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            _dsOrientation = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

            if (_dsOrientation != null)
            {
                PassCheck();

                sgShowList = "";
                sgShowList += "<div style='margin-top:15px;'>";

                WWWService.Service _wwwService = new WWWService.Service();

                for (int i = 0; i < _dsOrientation.Tables[0].Rows.Count; i++)
                {
                    string attendDate = Convert.ToDateTime(_dsOrientation.Tables[0].Rows[i]["StartDate"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string check = "";

                    try
                    {
                        Object[] objSql2 = new object[1] { "MATE_OrientationInquiry" };
                        Object[] objParam2 = new object[] { "DIVIS", "MateYearCode", "RecruitType", "OrientationOrder" };
                        Object[] objValue2 = new object[] { CodeAction.ChannelType, _dsOrientation.Tables[0].Rows[i]["MateYearCode"], _dsOrientation.Tables[0].Rows[i]["RecruitType"], _dsOrientation.Tables[0].Rows[i]["OrientationOrder"] };

                        DataSet dsCnt = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "SP", objParam2, objValue2);

                        if (dsCnt == null || dsCnt.Tables[0].Rows.Count == 0)
                            check = "E";

                        else if (Convert.ToInt32(dsCnt.Tables[0].Rows[0]["Cnt"]) < Convert.ToInt32(_dsOrientation.Tables[0].Rows[i]["RecruitCnt"]))
                            check = "N";

                        else
                            check = "Y";

                        //check = _wwwService.MateMagamDateYN("번역", attendDate, 120);

                        if (check == "E")
                        {
                            sjs = JavaScript.HeaderScript.ToString();
                            sjs += JavaScript.GetAlertScript("Mate 합격자 오리엔테이션 마감날짜 확인 중 오류가 발생했습니다.\\r\\n관리자에게 문의해주세요.");
                            sjs += JavaScript.FooterScript.ToString();

                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                            return;
                        }

                        else if (check == "Y")
                        {
                            //sgShowList += "<input runat='server' id='rd" + i.ToString() + "' name='joinDate' type='radio' disabled='false' style='margin-left:240px;'>   " + _dsOrientation.Tables[0].Rows[i]["Remark"].ToString() + "</input>";
                            sgShowList += "<input runat='server' id='rd" + i.ToString() + "' name='joinDate' type='radio' disabled='false' style='margin-left:240px;' /><label for='rd" + i.ToString() + "'>" + _dsOrientation.Tables[0].Rows[i]["Remark"].ToString() + "</label>";
                        }


                        else {
                            sgShowList += "<input runat='server' id='rd" + i.ToString() + "' name='joinDate' type='radio' value='" + i.ToString() + "' onclick='rb_Selected(this.value);' style='margin-left:240px;' /><label for='rd" + i.ToString() + "'>" + _dsOrientation.Tables[0].Rows[i]["Remark"].ToString() + "</label>";
                        }


                        if (i < (_dsOrientation.Tables[0].Rows.Count - 1))
                            sgShowList += "<br /><br />";
                    }

                    //Exception Error
                    catch (Exception ex)
                    {
                        //Exception Error Insert           
                        _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

                        sjs = JavaScript.HeaderScript.ToString();
                        sjs += JavaScript.GetAlertScript("Mate 합격자 오리엔테이션 마감날짜 확인 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                                          ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
                        sjs += JavaScript.FooterScript.ToString();

                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                        return;
                    }
                }

                sgShowList += "</div>";
            }

            else
            {
                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("오리엔테이션 신청정보를 가져오던 도중 오류가 발생했습니다.");
                sjs += JavaScript.GetPageMoveScript("/Default.aspx");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }
        }

        else
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("번역메이트 신규모집 합격자를 대상으로 하는 오리엔테이션입니다.");
            sjs += JavaScript.GetPageMoveScript("/Default.aspx");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }

    #region Mate승인된 후원자 확인
    /// <summary>
    /// Mate승인된 후원자 확인
    /// </summary>
    /// <returns></returns>
    private string MateYN()
    {
        WWWService.Service _wwwService = new WWWService.Service();
        string result = "N";

        string sponsorID = sess.SponsorID.ToString();

        Object[] objSql = new object[1] { "SELECT * FROM MATE_Joining WHERE MateYearCode = '" + "201811" + "' AND UserID = '" + new UserInfo().UserId + "'" + 
                                          " AND Status = '" + "APPROVAL" + "'"  };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        DataSet ds = new DataSet();

        try
        {
            ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            //result = _wwwService.MatePassJoinYN(sponsorID, dateFrom, dateTo, mateType, acceptFlag);
        }

        catch (Exception ex)
        {
            //Exception Error Insert           
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate승인여부를 확인 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            result = null;
        }

        if (ds.Tables[0].Rows.Count > 0)
        {
            result = "Y";
        }

        return result;
    }
    #endregion

    #region 메이트 합격자 오리엔테이션 마감날짜 확인여부
    /// <summary>
    /// 메이트 합격자 오리엔테이션 마감날짜 확인여부 2012-06-15
    /// </summary>
    private void MagamSetting()
    {
        #region 주석 141124
        //int cnt = 6;

        //for (int i = 0; i < cnt; i++)
        //{
        //    string attendDate = "";
        //    string check = "";

        //    switch (i)
        //    {
        //        //수정 2013-05-28
        //        case 0:
        //            attendDate = "2014-06-20 19:00:00";
        //            break;
        //        case 1:
        //            attendDate = "2014-06-21 15:00:00";
        //            break;
        //        case 2:
        //            attendDate = "2014-06-21 17:30:00";
        //            break;
        //        case 3:
        //            attendDate = "2014-06-26 19:00:00";
        //            break;
        //    }

        //    try
        //    {
        //        //수정 2012-09-12 기존:185명씩
        //        //수정 2012-11-27 기존:150명씩
        //        //수정 2013-03-07 기존:120명씩
        //        //수정 2013-05-28 기존:150명씩 그대로 사용
        //        check = _wwwService.MateMagamDateYN("번역", attendDate, 120);
        //    }

        //    //Exception Error
        //    catch (Exception ex)
        //    {
        //        //Exception Error Insert           
        //        _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

        //        string sjs = JavaScript.HeaderScript.ToString();
        //        sjs += JavaScript.GetAlertScript("Mate 합격자 오리엔테이션 마감날짜 확인 중 오류가 발생했습니다. \\r\\nException Error Message : " +
        //                                          ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
        //        sjs += JavaScript.FooterScript.ToString();
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
        //        return;
        //    }

        //    //DB Error
        //    if (check == "E")
        //    {
        //        string sjs = JavaScript.HeaderScript.ToString();
        //        sjs += JavaScript.GetAlertScript("Mate 합격자 오리엔테이션 마감날짜 확인 중 오류가 발생했습니다.\\r\\n관리자에게 문의해주세요.");
        //        sjs += JavaScript.FooterScript.ToString();
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
        //        return;
        //    }

        //    if (check == "Y")
        //    {
        //        switch (i)
        //        {
        //            //수정 2013-05-28
        //            case 0:
        //                rb1.Checked = false;
        //                rb1.Disabled = true;
        //                break;

        //            case 1:
        //                rb2.Checked = false;
        //                rb2.Disabled = true;
        //                break;

        //            case 2:
        //                rb3.Checked = false;
        //                rb3.Disabled = true;
        //                break;

        //            case 3:
        //                rb4.Checked = false;
        //                rb4.Disabled = true;
        //                break;
        //        }
        //    }

        //    else
        //    {
        //        switch (i)
        //        {
        //            //수정 2013-05-28
        //            case 0:
        //                rb1.Checked = false;
        //                rb1.Disabled = false;
        //                break;
        //            case 1:
        //                rb2.Checked = false;
        //                rb2.Disabled = false;
        //                break;
        //            case 2:
        //                rb3.Checked = false;
        //                rb3.Disabled = false;
        //                break;
        //            case 3:
        //                rb4.Checked = false;
        //                rb4.Disabled = false;
        //                break;
        //        }
        //    }
        //}
        #endregion
    }
    #endregion

    #region 오리엔테이션 Mate 신청한 후원자 확인
    /// <summary>
    /// 오리엔테이션 Mate 신청한 후원자 확인 2013-12-09
    /// </summary>
    private void PassCheck()
    {
        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
        WWWService.Service _wwwService = new WWWService.Service();

        DataSet dsCheck = new DataSet();

        string sponsorID = sess.SponsorID.ToString();
        string userID = sess.UserId.ToString();

        try
        {
            Object[] objSql = new object[1] { "MATE_OrientationInquiry" };
            Object[] objParam = new object[] { "DIVIS", "MateYearCode", "RecruitType", "MateID" };
            Object[] objValue = new object[] { "CHECK", _dsOrientation.Tables[0].Rows[0]["MateYearCode"], _dsOrientation.Tables[0].Rows[0]["RecruitType"], userID };

            dsCheck = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            if (dsCheck.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToInt32(dsCheck.Tables[0].Rows[0]["Cnt"]) > 0)
                {
                    string sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("이미 오리엔테이션 참석을 신청해 주셨습니다. 감사합니다. \\r\\n(신청하신 참석날짜 : " + dsCheck.Tables[0].Rows[0]["Remark"].ToString() + ")");
                    sjs += JavaScript.GetPageMoveScript("/Default.aspx");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                    return;
                }
            }

            //dsCheck = _wwwService.CheckMateOrientationJoinProc(mateYN, sponsorID, userID, "번역");
        }

        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert           
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate신청 확인하는 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }
    #endregion

    #region 신청버튼
    /// <summary>
    /// 신청버튼
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNext_Click(object sender, EventArgs e)
    {
        WWWService.Service _wwwService = new WWWService.Service();

        DataSet dsCheck = new DataSet();
        UserInfo user = new UserInfo();

        string sponsorID = user.SponsorID.ToString();
        string sponsorName = user.UserName.ToString();
        string userID = user.UserId.ToString();
        string attendDate = "";
        string attendDateStr = "";

        int tmp;

        if (hdSelectValue.Value == "")
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("신청날짜가 선택되지 않았습니다.\\r\\n선택하여 주세요.");
            sjs += JavaScript.FooterScript.ToString();

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            return;
        }

        else
        {
            tmp = Convert.ToInt32(hdSelectValue.Value);

            attendDate = Convert.ToDateTime(_dsOrientation.Tables[0].Rows[tmp]["StartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
            attendDateStr = _dsOrientation.Tables[0].Rows[tmp]["Remark"].ToString();
        }

        try
        {
            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            Object[] objSql = new object[1] { "MATE_OrientationSave" };
            Object[] objParam = new object[] { "DIVIS", "MateYearCode", "RecruitType", "OrientationOrder", "MateID" };
            Object[] objValue = new object[] { CodeAction.ChannelType, _dsOrientation.Tables[0].Rows[tmp]["MateYearCode"], _dsOrientation.Tables[0].Rows[tmp]["RecruitType"], _dsOrientation.Tables[0].Rows[tmp]["OrientationOrder"], user.UserId };

            int iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            if (iResult > 0)
            {
                Object[] objSql2 = new object[1] { "MATE_OrientationInquiry" };
                Object[] objParam2 = new object[] { "DIVIS", "MateYearCode", "RecruitType", "OrientationOrder" };
                Object[] objValue2 = new object[] { CodeAction.ChannelType, _dsOrientation.Tables[0].Rows[tmp]["MateYearCode"], _dsOrientation.Tables[0].Rows[tmp]["RecruitType"], _dsOrientation.Tables[0].Rows[tmp]["OrientationOrder"] };

                DataSet dsCnt = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "SP", objParam2, objValue2);

                if (Convert.ToInt32(dsCnt.Tables[0].Rows[0]["Cnt"]) > Convert.ToInt32(_dsOrientation.Tables[0].Rows[tmp]["RecruitCnt"]))
                {
                    Object[] objSql3 = new object[1] { "MATE_OrientationSave" };
                    Object[] objParam3 = new object[] { "DIVIS", "MateYearCode", "RecruitType", "OrientationOrder", "MateID" };
                    Object[] objValue3 = new object[] { "FAIL", _dsOrientation.Tables[0].Rows[tmp]["MateYearCode"], _dsOrientation.Tables[0].Rows[tmp]["RecruitType"], _dsOrientation.Tables[0].Rows[tmp]["OrientationOrder"], user.UserId };

                    iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql3, "SP", objParam3, objValue3);

                    string sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("죄송합니다. 신청하신 회차의 정원이 마감되었습니다.");
                    sjs += JavaScript.GetPageMoveScript("/Intro/TransOrientationJoin.aspx");
                    sjs += JavaScript.FooterScript.ToString();

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                    return;
                }

                else
                {
                    dsCheck = _wwwService.InsertMateOrientationJoinProc(mateYN, sponsorID, sponsorName, userID, "번역",
                                                                        "신청", attendDate, "", "", "", sponsorID, sponsorName);
                }
            }

            else
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("Mate신청 하는 중 오류가 발생했습니다.");
                sjs += JavaScript.FooterScript.ToString();

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                return;
            }
        }

        //Exception Error
        catch (Exception ex)
        {
            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            Object[] objSql = new object[1] { "MATE_OrientationSave" };
            Object[] objParam = new object[] { "DIVIS", "MateYearCode", "RecruitType", "OrientationOrder", "MateID" };
            Object[] objValue = new object[] { "FAIL", _dsOrientation.Tables[0].Rows[tmp]["MateYearCode"], _dsOrientation.Tables[0].Rows[tmp]["RecruitType"], _dsOrientation.Tables[0].Rows[tmp]["OrientationOrder"], user.UserId };

            int iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //Exception Error Insert  
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate신청 하는 중 오류가 발생했습니다.\\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        //DB Error
        if (dsCheck == null)
        {
            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            Object[] objSql = new object[1] { "MATE_OrientationSave" };
            Object[] objParam = new object[] { "DIVIS", "MateYearCode", "RecruitType", "OrientationOrder", "MateID" };
            Object[] objValue = new object[] { "FAIL", _dsOrientation.Tables[0].Rows[tmp]["MateYearCode"], _dsOrientation.Tables[0].Rows[tmp]["RecruitType"], _dsOrientation.Tables[0].Rows[tmp]["OrientationOrder"], user.UserId };

            int iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            string  sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate신청 하는 중 오류가 발생했습니다.\\r\\n관리자에게 문의해주세요.");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        else
        {
            if (dsCheck.Tables[0].Rows.Count > 0)
            {
                //신청내역이 있으면 return false
                if (dsCheck.Tables[0].Rows[0]["Result"].ToString() != "Y")
                {
                    WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

                    Object[] objSql = new object[1] { "MATE_OrientationSave" };
                    Object[] objParam = new object[] { "DIVIS", "MateYearCode", "RecruitType", "OrientationOrder", "MateID" };
                    Object[] objValue = new object[] { "FAIL", _dsOrientation.Tables[0].Rows[tmp]["MateYearCode"], _dsOrientation.Tables[0].Rows[tmp]["RecruitType"], _dsOrientation.Tables[0].Rows[tmp]["OrientationOrder"], user.UserId };

                    int iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

                    string sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript(dsCheck.Tables[0].Rows[0]["Result_String"].ToString());
                    sjs += JavaScript.GetPageMoveScript("/Default.aspx");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                    return;
                }

                //신청내역이 없으면 return
                else
                {
                    string sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("오리엔테이션 신청이 완료되었습니다."
                                                    + "\\r\\n" + sponsorName + " 메이트님께서"
                                                    + "\\r\\n신청하신 오리엔테이션 일시는"
                                                    + "\\r\\n" + attendDateStr + " 입니다."
                                                    + "\\r\\n신청하신 날짜에 꼭 참석해 주세요.");
                    sjs += JavaScript.GetPageMoveScript("/Default.aspx");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "OK", sjs);
                    return;
                }
            }
        }
    }
    #endregion
}
