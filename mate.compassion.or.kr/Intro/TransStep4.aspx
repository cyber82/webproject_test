﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransStep4.aspx.cs" Inherits="Mate_Intro_TransStep4" ValidateRequest="false" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/IntroRight.ascx" tagname="IntroRight" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <style>
        #btn_popup1{position:relative;left:387px;top:10px;display:inline-block;width:100px;height:25px;line-height:25px;background:#e1e1e1;text-decoration:none;text-align:center;font-size:12px;border:1px solid #d5d5d5}
        #btn_popup2{position:relative;left:490px;top:-30px;display:inline-block;width:100px;height:25px;line-height:25px;background:#e1e1e1;text-decoration:none;text-align:center;font-size:12px;border:1px solid #d5d5d5}
    </style>

    <script type="text/javascript">
    <!--

        $(function () {
            $("#btn_popup1").click(function () {
                window.open("TransStep4-popup.aspx?type=1&SelectedTran=<%= hdSelectedTran.Value%>", "popup", "width=500,height=700,scrollbars=yes");

                return false;
            });

            $("#btn_popup2").click(function () {
                window.open("TransStep4-popup.aspx?type=2&SelectedTran=<%= hdSelectedTran.Value%>", "popup", "width=500,height=700,scrollbars=yes");

                return false;
            });
        });





        //Backspace, F5, Ctrl+N, Ctrl+R, Ctrl+C, Ctrl+V 막기
        document.onkeydown = NewEdit;
        function NewEdit() {
            var keyval = event.keyCode;
            if (keyval == 116) {
                EventSet();
                alert("F5 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 78)) {
                EventSet();
                alert("Ctrl + N 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 82)) {
                EventSet();
                alert("Ctrl + R 키를 금지합니다.");
            }
            /* 주석처리 2012-09-13
            if (event.ctrlKey == true && (event.keyCode == 86)) {
            EventSet();
            alert("Ctrl + V 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 67)) {
            EventSet();
            alert("Ctrl + C 키를 금지합니다.");
            }
            */

            //Backspace
            if (keyval == 8) {
                if (event.srcElement.tagName == "INPUT" || event.srcElement.tagName == "TEXTAREA") {
                } else {
                    EventSet();
                }
            }
        }

        function EventSet() {
            event.keyCode = 0;
            event.cancelBubble = true;
            event.returnValue = false;
        }

        function ChangeTrans(num) {
            //top.window.location.href = "TransStep4.aspx?num=" + num;
            top.window.location.href = "TransStep4.aspx?AgeDIvision=" + document.getElementById('hdAgeDivision').value + "&School=" + document.getElementById('hdSchool').value + "&MatePlus=" + document.getElementById('hdMatePlus').value + "&num=" + num;
        }
    //-->
    </script>
</head>
<%--<body>--%>
<body oncontextmenu='return false' ondragstart='return false'>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="apply sample-test">
 
		<!-- sidebar -->
		<uc4:IntroRight ID="IntroRight1" runat="server" />
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>번역 MATE</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/Intro/MateIntro.aspx">MATE 소개</a><strong>MATE 신청</strong>
			</div>
			<ul class="tab2 tab-mate">
				<li class="on"><a href="TransIntro.aspx" class="t1">번역 MATE</a></li>
				<li><a href="officemate.aspx" class="t2">사무 MATE</a></li>
				<li><a href="eventmate.aspx" class="t3">행사 MATE</a></li>
				<li><a href="specialitymate.aspx" class="t4">전문 MATE</a></li>
			</ul>
			<div class="txt-procedure">
				<h4>편지 번역 메이트 신청 절차</h4>
			</div>
			<ul class="tab3 tab-step">
				<li><a href="javascript:void(0);" class="t1">STEP01 메이트 정보 입력</a></li>
				<li><a href="javascript:void(0);" class="t2">STEP02 번역유의사항 PDF DOWN</a></li>
				<li  class="on"><a href="javascript:void(0);" class="t3">STEP03 편지번역 SAMPLE TEST</a></li>
				<li><a href="javascript:void(0);" class="t4">STEP04 번역 메이트 활동 동의서 서명</a></li>
				<li><a href="javascript:void(0);" class="t5">STEP05 지원완료</a></li>
			</ul>


			<div class="cont" id="cont1" runat="server">
				<div class="step-wrap">

                    <% //수정 2012-09-13  영한,한영번역 메시지 보기 %>
                    <% //수정 2013-05-29 %>
					<p class="text txt-choice"></p>

					<div class="choice-wrap">
						<ul class="tab2 tab-step03" id="tab-step03" >
                            <% //수정 2012-09-12  영한,한영번역 버튼 보기 %>
                            <% //주석처리 2013-03-07 %>
                            <% //수정 2013-05-29 %>
							<li class="on"><a onclick="javascript:ChangeTrans('1');" class="t1" id="tab1">영한 번역 (어린이 편지)</a></li>
                            <li><a href="javascript:ChangeTrans('2');" class="t2" id="tab2">한영 번역 (후원자 편지)</a></li>
                            
                            <% //영한번역 버튼 보기 %>
                            <%--<li class="on"><a class="t1" id="tab1" style="margin-left:80px;">영한 번역 (어린이 편지)</a></li>--%>
						</ul>
					</div>
					<ul class="text txt-description">
						<li>아래의 편지 이미지를 확인 후 번역해 주시고, 지원동기를 작성해 주시기 바랍니다.</li>
						<li>작성 후에는 [제출하기] 버튼을 눌러 주세요. </li>
					</ul>
					<ol class="text txt-notice">
						<li>번역유의사항의 모든 내용을 빠짐없이 적용하여서 편지를 번역합니다.  </li>
						<li>맞춤법(띄어쓰기, 문장부호 포함)을 지켜서 편지를 번역합니다.</li>
						<li>편지 원문에 있는 내용을 빠짐 없이 정확하게 번역합니다.</li>
						<li>편지 원문의 형식과 관계 없이 인사말, 본문, 맺음말로 번역본을 작성하고, 문장과  문장사이에 한줄 띄기 및 새로운 문장 시작 시 들여쓰기는 하지 않습니다.</li>
					</ol>
					
                    <div>
					    <p class="text tit-image" style="margin-top:20px;float:left"><strong>편지이미지</strong></p>
                        <a href="#" id="btn_popup1">새창으로 보기</a>
                        
                    </div>
					<div class="img-area">
                        <asp:Repeater runat=server ID=repeater>
						    <ItemTemplate>
                                <asp:Image ID="Image" runat="server" ImageUrl='<%#Eval("st_image") %>' />
						    </ItemTemplate>
					    </asp:Repeater>	
					</div>

                    <p class="text tit-etc"></p>

					<p class="text tit-sample"><strong>샘플번역란</strong></p>

					<div class="cont-area">
						<textarea rows="10" cols="65" runat="server" id="sampleTrans1"></textarea>
					</div>
					<p class="text tit-motive"><strong>지원동기</strong></p>
					<div class="cont-area">
						<textarea rows="10" cols="65" runat="server" id="applyCause1"></textarea>
					</div>
				</div>
			</div>

			<div class="cont" id="cont2" runat="server">
				<div class="step-wrap">
					<p class="text txt-choice">편지번역 sample test Sample Test에 응시해주시기 전에 아래 번역 타입을 선택 해주세요.</p>
					<div class="choice-wrap">
						<ul id="tab" class="tab2 tab-step03">
							<li><a onclick="javascript:ChangeTrans('1');" id="tab1" class="t1">영한 번역 (어린이 편지)</a></li>
							<li class="on"><a onclick="javascript:ChangeTrans('2');" id="tab2" class="t2">한영 번역 (후원자 편지)</a></li>
						</ul>
					</div>
					<ul class="text txt-description">
						<li>아래의 편지 이미지를 확인 후 번역해 주시고, 지원동기를 작성해 주시기 바랍니다.</li>
						<li>작성 후에는 [제출하기] 버튼을 눌러 주세요. </li>
					</ul>
					<ol class="text txt-notice">
						<li>번역유의사항의 모든 내용을 빠짐없이 적용하여서 편지를 번역합니다.  </li>
						<li>맞춤법(띄어쓰기, 문장부호 포함)을 지켜서 편지를 번역합니다.</li>
						<li>편지 원문에 있는 내용을 빠짐 없이 정확하게 번역합니다.</li>
						<li>편지 원문의 형식과 관계 없이 인사말, 본문, 맺음말로 번역본을 작성하고, 문장과  문장사이에 한줄 띄기 및 새로운 문장 시작 시 들여쓰기는 하지 않습니다.</li>
					</ol>
                    
                    <div style="height:28px;">
					    <p class="text tit-image" style="margin-top:20px;"><strong>편지이미지</strong></p>
                        <a href="#" id="btn_popup2">새창으로 보기</a>
                    </div>
					<div class="img-area">
                        <asp:Repeater runat=server ID=repeater1>
						    <ItemTemplate>
                                <asp:Image ID="Image" runat="server" ImageUrl='<%#Eval("st_image") %>' />
						    </ItemTemplate>
					    </asp:Repeater>	
					</div>

                    <p class="text tit-etc"></p>

					<p class="text tit-sample"><strong>샘플번역란(* 주의사항: "<"또는 ">"를 사용하시면 오류가 발생할 수 있으니, 입력하지 말아 주세요.)</strong></p>
					<div class="cont-area">
						<textarea rows="10" cols="65" runat="server" id="sampleTrans2"></textarea>
					</div>
					<p class="text tit-motive"><strong>지원동기</strong></p>
					<div class="cont-area">
						<textarea rows="10" cols="65" runat="server" id="applyCause2"></textarea>
					</div>
				</div>
			</div>
            <div class="btn-r">
                <% //수정 20130311  btn-nextstep: 다음단계,  btn-submit2: 제출하기  %>
                <asp:Button ID="btnNext" runat="server" Text="" CssClass="btn btn-submit2" OnClick="btnNext_Click" />
			</div>

			<script type="text/javascript">
            <!--
			    //pageSplit('tab-step03', 'cont', 'tab');
            //-->
            </script>
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />   
       <asp:HiddenField ID="hdAgeDivision" runat="server" />
       <asp:HiddenField ID="hdSchool" runat='server' />    
       <asp:HiddenField ID="hdMatePlus" runat='server' />    
       <asp:HiddenField ID="hdSelectedTran" runat='server' />    
    </div>    
    </form>
</body>
</html>
