﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OfficeMate.aspx.cs" Inherits="Mate_Intro_OfficeMate" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/IntroRight.ascx" tagname="IntroRight" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script type="text/javascript">
        function GoOffice() {
            alert('현재 진행 중인 모집이 없습니다'); return false;

            <% //수정 20120730 주석취소 %>
            <%if (UserInfo.IsLogin) {%>
                <%if (ngMateYearList > 0) {%>

                    <%if(ngMateJoinList == 0) {%>
                        NewWindow("PopOfficeMateSubmint.aspx?MateYearCode=<%=sgMateYearCode%>","popoffice","540", "500", "auto");
                    <%}else{ %>
                    alert('이미 등록된 사무 메이트입니다.\n활동 재개 문의는 전화(02-3668-3540) 또는 이메일(jchoi@compassion.or.kr)로 연락 주시기 바랍니다.');
                    <%} %>
                <%}else{ %>
                    alert('사무메이트 신청기간이 아닙니다.');
                <%} %>
            <%}else{ %>
                alert('로그인 하신후 신청이 가능 합니다.');
                location.href="/login.aspx" ;
            <%} %>
            

            //주석처리 20120730 신청중지할때만 사용
            /*
            alert("사무메이트에 지원해 주셔서 감사합니다."
                + "\n\r최근 사무메이트 지원자가 급증함에 따라, 메이트님들의 원활한 봉사활동 운영을 위하여 잠정적으로 사무메이트 신청을 중단하게 되었습니다."
                + "\n\r"
                + "\n\r이 점 양해 부탁드리며, "
                + "\n\r가급적 빠른 시일 내에 신청을 재개 하도록 하겠습니다."
                + "\n\r컴패션과 사무메이트에 지속적인 관심과 사랑 부탁 드립니다."
                + "\n\r감사합니다."
                + "\n\r"
                + "\n\r문의: 02) 3668 – 3465 / ykim@compassion.or.kr");
            return false;
            */
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="apply business-mate">
 
		<!-- sidebar -->
		<uc4:IntroRight ID="IntroRight1" runat="server" />
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>사무 MATE</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/Intro/MateIntro.aspx">MATE 소개</a><strong>MATE 신청</strong>
			</div>
			<ul class="tab2 tab-mate">
				<li><a href="TransIntro.aspx" class="t1">번역 MATE</a></li>
				<li class="on"><a href="officemate.aspx" class="t2">사무 MATE</a></li>
				<li><a href="eventmate.aspx" class="t3">행사 MATE</a></li>
				<li><a href="specialitymate.aspx" class="t4">전문 MATE</a></li>
			</ul>
			<div class="sec">
				<div class="text txt-intro">
					<h4>사무 메이트 소개</h4>
					<p><strong>신속하고 원활한 서비스를 제공하기 위해서다양한 사무 업무가 이루어지고 있습니다.</strong> - 빠르게 늘어나고 있는 후원자의 규모에 맞게 신속하고 원할한 서비스를 제공하기 위해 한국컴패션 전 부서에 걸쳐 다양한사무 업무가 이루어지고 있습니다. </p>
				</div>
			</div>
			<div class="sec qualification-wrap">
				<div class="text apply-qualification">
					<h4>사무 메이트 공통 신청자격</h4>
					<ol>
						<li>컴패션 사역을 이해하고, 자원 봉사 활동에 관심 있는 만 16세 이상의 남, 녀</li>
						<li>3개월 이상 꾸준히 봉사활동 가능하신분 </li>
						<li>봉사 활동시간 : 월요일 ~ 금요일, 10:00 ~ 18:00 (12:00부터 13:00까지 점심시간) 원하는 봉사활동 날짜 및 시간은 담당자와 협의하여 선택 및 조절 할 수 있습니다.</li>
					</ol>
				</div>
				<table class="list1" border="1">
					<thead>
						<tr>
							<th scope="col" class="td-department"><img src="/image/mate/text/td-department.gif" alt="부서"></th>
							<th scope="col" class="td-field"><img src="/image/mate/text/td-field.gif" alt="분야"></th>
							<th scope="col" class="td-activity"><img src="/image/mate/text/td-activity.gif" alt="활동내역"></th>
							<th scope="col" class="td-qualification"><img src="/image/mate/text/td-qualification.gif" alt="신청자격"></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="7" class="td-department">후원자 서비스팀</td>
							<%--<td rowspan="2" class="td-field">어린이 자료</td>
							<td rowspan="2" class="td-activity">어린이 정보 번역</td>
							<td class="td-qualification">컴퓨터 활용 능력이 있으신 분</td>--%>
						</tr>
						<%--<tr>
							<td class="td-qualification">영문 번역 가능하신 분</td>
						</tr>--%>
						<tr>
							<td rowspan="3" class="td-field">어린이 편지 <br>및 후원자 편지</td>
							<td class="td-activity">편지 원본과 번역본 매칭</td>
							<td rowspan="3" class="td-qualification">세심한 손놀림을 가지신 분</td>
						</tr>
						<tr>
							<td class="td-activity">편지 접기</td>
						</tr>
						<tr>
							<td class="td-activity">편지 봉투에 넣기</td>
						</tr>
						<tr>
							<td rowspan="2" class="td-field">발송</td>
							<td class="td-activity">어린이 자료 만들기</td>
							<td rowspan="2">세심한 손놀림을 가지신 분</td>
						</tr>
						<tr>
							<td class="td-activity">우편발송 작업</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="text apply-process">
				<h4>사무 메이트 신청절차</h4>
				<p>사무MATE 활동 및 신청에 대한 구체적인 시안은 내부적으로 논의한 후 전달해 드리겠습니다.</p>
				<ol>
					<li>컴패션 홈페이지 회원가입</li>
					<li>메이트 신청 페이지에서 ‘신청하기’클릭</li>
					<li>지원동기 작성</li>
					<li>오리엔테이션 참석 (장소 및 일시 사전 공고)</li>
					<li>mate현황에 따라 필요시 담당자가 직접 연락 드림</li>
				</ol>
			</div>
			<div class="contact-wrap">
				<p class="text-contact">MATE 신청 관련 자세한 문의는 MATE 홈페이지 FAQ, 온라인상담 또는 전화 02-740-1000[구.02-3668-3400]를 이용해 주시기 바랍니다.</p>
			</div>
			<div class="btn-m">
                <asp:Button ID="btnSave" CssClass="btn btn-apply" runat="server" Text="" 
                     OnClientClick="javascript:GoOffice();return false;" />
			</div>
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    </form>
</body>
</html>
