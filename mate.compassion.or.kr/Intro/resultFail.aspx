﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="resultFail.aspx.cs" Inherits="Mate_Intro_resultFail" %>

<!DOCTYPE html>
<html lang="ko">
<head> 
	<meta charset="UTF-8">
    <meta property="og:image" content="http://www.compassion.or.kr/common/img/sns_default_image.jpg">
	<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title> 
	<link rel="stylesheet" href="/common/css/mate.css" />
	<script type="text/javascript" src="/common/js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="/common/js/common.js"></script>

    <script language="javascript" type="text/javascript">
        function Close() {
            document.getElementById('test').style.display = 'none';
        }
    </script>
</head>
<body>
    <div id="test" runat="server">
        <div class="layer1 test-result-fail" style="height:230px;width:300px;">
	        <div class="tit"><strong>번역유의사항 MINI TEST 결과</strong></div>
	        <div class="lay-cont">
		        <p class="txt txt1" id="txt1" runat="server"><strong><span>MINI TEST를 통과하지 못하셨습니다.번역유의사항을 읽고 다시 도전해 주시기 바랍니다.</span></strong></p>
		        <div class="btn-m">
			        <%--<a href="javascript:void(0);" onclick ="javascript:location.href='TransStep1.aspx'" class="btn btn-confirm"><span>확인</span></a>--%>
                    <button type="button" ID="btnCofirm" runat="server" class="btn btn-confirm" onclick="javascript:Close();" />
		        </div>
	        </div>
	        <button type="button" onclick="javascript:Close();" class="btn btn-close"><span>닫기</span></button>
        </div>
    </div>
</body>
</html>