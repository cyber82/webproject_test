﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class Mate_Intro_TransStep6 : System.Web.UI.Page
{
    public string sgRecruitEnd;
    public string sTranslationType;

    protected void Page_Load(object sender, EventArgs e)
    {


		var mateData = new MateAction().GetSampleTestInfo();
		if (mateData.success) {
			var data = (mate_sample_test_info)mateData.data;
			announce_date.Text = data.announce_date;
			tel.Text = data.tel;
			email.Text = data.email;
		}


		sTranslationType = Request.QueryString["TranslationType"];
        string sMateYearCode = Request.QueryString["MateYearCode"];

		/*
        //일반
        if (sTranslationType != "Extend")
        {
            Object[] objSql = new object[1] { "SELECT RecruitEnd FROM MATE_Recruit WHERE (CONVERT(DATE, GETDATE()) BETWEEN RecruitStart AND RecruitEnd OR CONVERT(DATE, GETDATE()) BETWEEN AdditionalStart AND AdditionalEnd) AND MateYearCode = '" + sMateYearCode + "'" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            sgRecruitEnd = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null).Tables[0].Rows[0][0].ToString();

            sgRecruitEnd = sgRecruitEnd.Substring(0, 4).ToString() + "년 " + int.Parse(sgRecruitEnd.Substring(5, 2).ToString()).ToString() + "월 " + sgRecruitEnd.Substring(8, 2) + "일";
        }

        else//연정신청
        {
            //하반기
            if (DateTime.Now.Month >= 7 && DateTime.Now.Month <= 12)
            {
                sgRecruitEnd = DateTime.Now.Year + "년 12월 31일";
            }
            //상반기
            else if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 6)
            {
                sgRecruitEnd = DateTime.Now.Year + "년 6월 30일";
            }
        }
		*/
    }
}