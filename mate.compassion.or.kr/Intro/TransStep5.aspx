﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransStep5.aspx.cs" Inherits="Mate_Intro_TransStep5" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/IntroRight.ascx" tagname="IntroRight" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />

    <script type="text/javascript">
        function Validate() {
            if ('<%=sExtend%>' == '1') {
                return confirm("연장신청 하시겠습니까?");
            }
            else {
                return confirm("번역 메이트에 지원 하시겠습니까?");
            }
        }
    </script>
    <script type="text/javascript">
    <!--
        //Backspace, F5, Ctrl+N, Ctrl+R, Ctrl+C, Ctrl+V 막기
        document.onkeydown = NewEdit;
        function NewEdit() {
            var keyval = event.keyCode;
            if (keyval == 116) {
                EventSet();
                alert("F5 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 78)) {
                EventSet();
                alert("Ctrl + N 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 82)) {
                EventSet();
                alert("Ctrl + R 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 86)) {
                EventSet();
                alert("Ctrl + V 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 67)) {
                EventSet();
                alert("Ctrl + C 키를 금지합니다.");
            }

            //Backspace
            if (keyval == 8) {
                if (event.srcElement.tagName == "INPUT" || event.srcElement.tagName == "TEXTAREA") {
                } else {
                    EventSet();
                }
            }
        }

        function EventSet() {
            event.keyCode = 0;
            event.cancelBubble = true;
            event.returnValue = false;
        }

        function FinishPopUp() {
            
            alert('활동연장을 신청해주셔서 감사합니다.\n\r메이트님께서는 2018년 2월 25일까지\n\r제11기 번역메이트로 활동하시게 됩니다.');
            if ('<%=sExtend%>' == '1') {
                window.opener.document.location.reload();
            }
            window.close();
        }
        
    //-->
    </script>
</head>
<%--<body>--%>
<body style="overflow-X:hidden" oncontextmenu='return false' ondragstart='return false'>
    <form id="form1" runat="server">     
    <div>
    <%if(string.IsNullOrEmpty(sExtend)){  %>
        <uc2:Top ID="Top1" runat="server" />  
        <%} %>      
        <!-- container -->
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="apply consent-sign">
 
		<!-- sidebar -->
        <%if(string.IsNullOrEmpty(sExtend)){  %>
		<uc4:IntroRight ID="IntroRight1" runat="server" />
        <%} %>
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>번역 MATE</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/Intro/MateIntro.aspx">MATE 소개</a><strong>MATE 신청</strong>
			</div>
            <%if(string.IsNullOrEmpty(sExtend)){  %>
			<ul class="tab2 tab-mate">
				<li class="on"><a href="TransIntro.aspx" class="t1">번역 MATE</a></li>
				<li><a href="officemate.aspx" class="t2">사무 MATE</a></li>
				<li><a href="eventmate.aspx" class="t3">행사 MATE</a></li>
				<li><a href="specialitymate.aspx" class="t4">전문 MATE</a></li>
			</ul>
            <%} %>
			<div class="txt-procedure">
				<h4>편지 번역 메이트 신청 절차</h4>
			</div>
			<ul id="tap" runat="server" class="tab3 tab-step">
				<li><a href="javascript:void(0);" class="t1">STEP01 메이트 정보 입력</a></li>
				<li><a href="javascript:void(0);" class="t2">STEP02 번역유의사항 PDF DOWN</a></li>
				<li><a href="javascript:void(0);" class="t3">STEP03 편지번역 SAMPLE TEST</a></li>
				<li class="on"><a href="javascript:void(0);" class="t4">STEP04 번역 메이트 활동 동의서 서명</a></li>
				<li><a href="javascript:void(0);" class="t5">STEP05 지원완료</a></li>
			</ul>
			<div class="step-wrap">
 
				<div class="text txt-consent-form">
					<p><strong>번역 메이트 활동 동의서</strong> 동의서를 확인하시고 마지막에 서명해 주시면 번역 메이트 지원이 완료 됩니다.</p>
					<dl>
						<dt>컴패션 편지 번역 메이트의 정체성 (Identity) 에 대한 동의</dt>
						<dd>
							<ul>
								<li>1. 컴패션 편지번역 메이트는 후원자와 어린이 사이의 사랑과 희망이 온전히 전달될 수 있도록 보이지 않는 곳에서 섬기는 아름다운 사람입니다.</li>
								<li>2. 컴패션 편지번역 메이트는 그리스도 안에서 컴패션과 후원자가 함께, 꿈을 잃은 어린이들의 삶에 기적을 만들어가는 동역자입니다.</li>
								<li>3. 컴패션 편지번역 메이트는 하나님의 영광과 그 선한 뜻을 위하여 하나님께서 주신 재능을 기꺼이 쓰는 신실한 주의 자녀입니다.</li>
							</ul>
						</dd>
						<dt>컴패션 편지 번역 메이트의 핵심가치 (Core Value) 에 대한 동의</dt>
						<dd>
							<ul>
								<li>4. 정직(honesty)을 최우선으로 하여 어린이와 후원자 편지를 번역합니다. 편지번역의 모든 프로세스(메이트 신청, 번역, 오류편지신고, 스크리닝 과정 등) 에 정직함으로 임합니다.</li>
								<li>5. 탁월한 번역(Excellence)을 향한 끊임없는 시도와 노력을 추구합니다. 한국컴패션에서 제작한 번역 관련 자료(번역유의사항,iMate메뉴얼,홈페이지에 연재중인 번역의 달인)를 꼼꼼히 읽고 숙지한 후 편지번역을 시작합니다.</li>
								<li>6. 하나님의 청지기(Steward)로서 하나님께서 주신 재능에 감사하는 마음과 겸손한 자세로 성실하게 번역합니다</li>
								<li>7. 어린이를 비롯한 모든 사람들을 존귀(Dignity)하게 여기며 그 동일한 마음으로 어린이와 후원자의 개인정보 및 편지 내용을 소중히 다루고 외부에 유출하지 않습니다.</li>
							</ul>
						</dd>
						<dt>컴패션 편지 번역 메이트의 활동규칙에 대한 동의</dt>
						<dd>
							<ul>
								<li>8. 정일주일에 (월요일 오전 00:00시부터 다음주 월요일 오전 00:00까지)  3회 (편지세통)이상 번역합니다. 단, 번역할 편지가 없는경우에는 해당되지 않습니다.</li>
								<li>9. 편지이미지와 장수를 꼼꼼히 확인한 후, 수기로 기록된모든 내용 (편지내용, 성경, 구절, 추신, 날짜 등)을 빠짐없이 번역합니다. 편지내용 누락으로 편지가 반송되면 5일이내에 수정 및 추가 번역하여 재전송합니다.</li>
								<li>10. 편지 원문의 내용과 다르게 번역되거나 또는 제5항에 언급된 번역유의사항의 내용이 반영되지 않았을 경우 오역으로 간주되며 오역으로 편지가 반송되면 5일 이내에 수정 및 추가 번역하여 재전송합니다.</li>
								<li>11. 번역 시 글자 판독이 어렵거나 모르는 단어, 문장이 나올 경우 홈페이지의 번역 Q&amp;A 게시판을 통하여 컴패션직원 또는 다른 메이트들의 도움을 받아 번역을 완료합니다.</li>
								<li>12. 제8항,9항,10항이 준수되지 않았을 경우 별도의 안내를 받게되며, 5회 이상 안내를 받았을 경우에는 내부관리 방침에 따라 활동이 중단될 수 있습니다.</li>
								<li>13. 번역 메이트 활동이 중단될 경우, 차기 기수 모집 시 재 신청할 수 있습니다.</li>
								<li>14. 어린이와 후원자가 받는 모든 편지번역본에는 번역 메이asdfasdfasdfasdf니다.</li>
							</ul>
						</dd>
					</dl>
				</div>
				<div class="txt-activity-term">
					<strong class="text txt-activity-term1"><span>상기내용에 동의하며</span></strong>
					<strong class="txt-activity-term2">
                        <% //기간 수정 2013-03-07 %>
                        <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                    </strong>
					<strong class="text txt-activity-term3"><span>컴패션 편지 번역 메이트로서 성실히 활동하겠습니다.</span></strong>
				</div>				
                <dl class="signature-wrap">
					<dt class="text txt-sign-date1"><span>서명일</span></dt>
					<dd class="txt-sign-date2"><asp:Label ID="Label1" runat="server" Text=""></asp:Label></dd>
					<dt class="text txt-signing1"><span>서명</span></dt>
					<dd>
                        <asp:TextBox ID="txtUserName" CssClass="txt-signing2" runat="server"></asp:TextBox>
					</dd>
                    <dt></dt>
                    <dd class="btn-r" style="padding-top:7px">
                        <asp:Button ID="btnNext" runat="server" Text="" CssClass="btn btn-submit2" OnClick="btnNext_Click" OnClientClick="return Validate();"/>
                    </dd>
				</dl>       
			</div>
 
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>


		<!-- // container -->   
        <%if(string.IsNullOrEmpty(sExtend)){  %> 
       <uc3:Footer ID="Footer1" runat="server" />   
       <%} %>   
    </div>    
    </form>
</body>
</html>

