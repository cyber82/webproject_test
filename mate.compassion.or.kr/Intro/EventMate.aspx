﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventMate.aspx.cs" Inherits="Mate_Intro_EventMate" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/IntroRight.ascx" tagname="IntroRight" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="apply event-mate">
 
		<!-- sidebar -->
		<uc4:IntroRight ID="IntroRight1" runat="server" />
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>행사 MATE</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/Intro/MateIntro.aspx">MATE 소개</a><strong>MATE 신청</strong>
			</div>
			<ul class="tab2 tab-mate">
				<li><a href="TransIntro.aspx" class="t1">번역 MATE</a></li>
				<li><a href="officemate.aspx" class="t2">사무 MATE</a></li>
				<li class="on"><a href="eventmate.aspx" class="t3">행사 MATE</a></li>
				<li><a href="specialitymate.aspx" class="t4">전문 MATE</a></li>
			</ul>
			<div class="sec">
				<div class="text txt-intro">
					<h4>행사 메이트 소개</h4>
					<p><strong>전세계 어린이들을 대신하여 세상에 목소리를 높이기 위해 연중 다양한 결연 행사들을 진행합니다.</strong> - 한국컴패션은 전세계 어린이들을 대신하여 세상에 목소리를 높이기 위해 다양한 결연 행사들을 진행합니다. 이 때 MATE분들은 행사 성격에 따라 다양한 역할을 감당하시는 귀중한 손길이 됩니다.</p>
				</div>
			</div>
			<div class="sec">
				<div class="text txt-recruit">
					<h4>행사 메이트 모집 및 활동 시기</h4>
					<p>행사 시 MATE님들의 도움이 필요할 경우, 컴패션 홈페이지 www.compassion.or.kr 를 통해 공지합니다.</p>
				</div>
			</div>
			<div class="text txt-apply">
				<h4>행사 메이트 신청방법</h4>
				<p>컴패션 홈페이지 www.compassion.or.kr회원 가입 후 행사 mate 모집 공고 시 신청</p>
			</div>
			<div class="contact-wrap">
				<p class="text-contact">MATE 신청 관련 자세한 문의는 MATE 홈페이지 FAQ, 온라인상담 또는 전화 02-740-1000[구.02-3668-3400]를 이용해 주시기 바랍니다.</p>
			</div>
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    </form>
</body>
</html>
