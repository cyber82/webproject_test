﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Collections;

public partial class Mate_Intro_MateExtension_6 : System.Web.UI.Page
{
    DateTime dt;
    string sjs = string.Empty;
    string sgMateYearCode;


    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //UserInfo sess = new UserInfo();
        if (!IsPostBack)
        { 
           
        }
    }

    //연장신청 버튼
    protected void SetImgBtnExtendApply_Click(object sender, EventArgs e)
    {
        if (!UserInfo.IsLogin)
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("로그인 후, 신청 가능합니다.");
            //sjs += JavaScript.GetOpenerPageMoveScript("/membership/Login.aspx?returnUrl=/community/noticeview.aspx?iTableIndex=1001&iNoIndex=118&iBoardIdx=108808&b_num=110206&re_level=0&ref=110206&re_step=0&pageIdx=0&searchOption=all&searchTxt=");
            //sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/community/noticeview.aspx?iTableIndex=1001&iNoIndex=118&iBoardIdx=108808&b_num=110206&re_level=0&ref=110206&re_step=0&pageIdx=0&searchOption=all&searchTxt=");
            sjs += " parent.document.location.href = '/membership/Login.aspx?returnUrl=/community/noticeview.aspx?iTableIndex=1001^iNoIndex=183^iBoardIdx=156362^ref=155415^re_step=0';";
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        else
        {
            //연장신청 체크[1]:연장체크 기간 체크 [2]:이미등록되있는 메이트 체크
            Object[] objSql = new object[2] { "SELECT * FROM MATE_Recruit WHERE MateYearCode = dbo.uf_GetMateYearCode(1) AND CONVERT(DATE, GETDATE()) BETWEEN ExtendStart AND ExtendEnd", 
            "SELECT * FROM MATE_Master WHERE MateID = '" + new UserInfo().UserId + "' AND MateYearCode = dbo.uf_GetMateYearCode(1) AND Status = 'ACTIVE'" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            //연장신청 버튼 보이기/숨기기
            if (ds.Tables[0].Rows.Count > 0 && ds.Tables[1].Rows.Count > 0)
            {
                //this.imgBtnExtendApply.Attributes.Add("style", "display:block;");
                
                //연장신청버튼 스크립트 설정
                objSql = new object[1] { "SELECT * FROM MATE_Master WHERE MateYearCode = (SELECT MateYearCode FROM MATE_Recruit WHERE CONVERT(DATE, GETDATE()) BETWEEN WorkingStart AND WorkingEnd AND(RIGHT(MateYearCode,2) = '11' OR RIGHT(MateYearCode,2) = '12')) AND MateID = '" + new UserInfo().UserId + "'" };
                ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

                bool bExtentionApply = Convert.ToBoolean(ds.Tables[0].Rows[0]["ExtentionApply"]);//연장신청유무
                sgMateYearCode = ds.Tables[0].Rows[0]["MateYearCode"].ToString();//기수

                if (bExtentionApply)
                {
                    //this.imgBtnExtendApply.Attributes.Add("onClick", "alert('이미 연장 신청을 했습니다.'); return false;");

                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("활동연장 신청이 정상 접수되었습니다.");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                }

                else
                {
                    //this.imgBtnExtendApply.Attributes.Add("onClick", "return confirm('연장신청을 하시겠습니까?');");
                    //this.imgBtnExtendApply.Attributes.Add("onClick", "ShowExtendClause(); return false;");
                    //Response.End();

                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += "ShowExtendClause();";
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                }
            }

            else if (ds.Tables[0].Rows.Count == 0)
            {
                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("지금은 활동연장 신청기간이 아닙니다. 02-3668-3404로 문의해주세요.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            }

            else
            {
                //this.imgBtnExtendApply.Attributes.Add("style", "display:none;");

                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("현재 활동중인 번역메이트만 신청 가능합니다.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            }          
        }
    }
}