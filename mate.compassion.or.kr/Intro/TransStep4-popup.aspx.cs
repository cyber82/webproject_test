﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class Mate_Intro_TransStep4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		string type = Request["type"];
        string selected = Request["selectedTran"];
		var mateData = new MateAction().GetSampleTestList(type);
		if (mateData.success) {
            List<mate_sample_test> data = (List<mate_sample_test>)mateData.data;
            List<mate_sample_test> item = data.Where<mate_sample_test>(_ => _.st_id.ToString() == selected).ToList();
			repeater.DataSource = item;
			repeater.DataBind();
		}
	}
}