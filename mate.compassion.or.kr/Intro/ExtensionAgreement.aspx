﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExtensionAgreement.aspx.cs" Inherits="Mate_Intro_ExtensionAgreement" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <style type="text/css">
        body{margin:auto;}
        
        .trans{width:800px;margin:0px auto 0px auto;list-style:none;padding-left:0px;background:url('/Intro/image/agreement.jpg') no-repeat;}
        .trans .agreement{position:relative;width:800px;height:700px;background-position:0 0;}
        
        .btn-notice{position:absolute;width:103px;height:21px;top:188px;margin-left:650px;border:0px solid red;}
        .btn-agreement{position:absolute;width:133px;height:28px;top:648px;margin-left:330px;border:0px solid red;}
        .btn-school{position:absolute;width:102px;height:23px;top:515px;margin-left:650px;border:0px solid red;}
    </style>

    <script type="text/javascript">
    <!--
        //Backspace, F5, Ctrl+N, Ctrl+R, Ctrl+C, Ctrl+V 막기
        document.onkeydown = NewEdit;
        function NewEdit() {
            var keyval = event.keyCode;
            if (keyval == 116) {
                EventSet();
                alert("F5 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 78)) {
                EventSet();
                alert("Ctrl + N 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 82)) {
                EventSet();
                alert("Ctrl + R 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 86)) {
                EventSet();
                alert("Ctrl + V 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 67)) {
                EventSet();
                alert("Ctrl + C 키를 금지합니다.");
            }

            //Backspace
            if (keyval == 8) {
                if (event.srcElement.tagName == "INPUT" || event.srcElement.tagName == "TEXTAREA") {
                } else {
                    EventSet();
                }
            }
        }

        function EventSet() {
            event.keyCode = 0;
            event.cancelBubble = true;
            event.returnValue = false;
        }

        //TransStep4.aspx 이동
        function ShowExtendClause() {

            sWidth = "780";
            sHeight = "800";

            if (screen.width < 1025) {
                LeftPosition = 0;
                TopPosition = 0;
            } else {
                LeftPosition = (screen.width) ? (screen.width - sWidth) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - sHeight) / 2 : 100;
            }

            window.open("TransStep5.aspx?Extend=2", "ExtendClause", "width=" + sWidth + ",height=" + sHeight + ",top=" + TopPosition + ",left=" + LeftPosition + ", scrollbars=yes, toolbar=no menubar=no, location=no");
        }
    //-->
    </script>
</head>

<%--<body>--%>
<body oncontextmenu='return false' ondragstart='return false'>
    <form id="form2" runat="server">
    <div>
        <!-- conts -->
        <div class="trans">
            <div class="agreement"></div>
            <%--<a href="http://www.compassion.or.kr/community/noticeview.aspx?iTableIndex=1001&iNoIndex=113&iBoardIdx=107496&b_num=108979&re_level=0&ref=108979&re_step=0&pageIdx=1&searchOption=&searchTxt=" target="_blank" class="btn-notice"></a>--%>
            <a href="MateExtension_Notice.aspx" target="_blank" class="btn-notice"></a>
            <a href="SubmissiontoSchool.aspx" target="_blank" class="btn-school"></a>
            <a href="TransStep5.aspx?Extend=2" class="btn-agreement"></a>
        </div>
		<!-- // conts -->
        
    </div>
    </form>
</body>
</html>
