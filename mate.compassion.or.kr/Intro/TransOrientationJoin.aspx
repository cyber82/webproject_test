﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransOrientationJoin.aspx.cs" Inherits="Mate_Intro_TransOrientationJoin" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/IntroRight.ascx" tagname="IntroRight" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />

    <style type="text/css">
        /*body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, fieldset, input, textarea, p, blockquote, th, td {margin:0;padding:0;}*/
        /*ol, ul,li{list-style:none; display:inline;}*/
        .btn-request{position:absolute;width:176px;height:37px;top:1600px;left:100px;border:1px solid red;}
        .tbn-counsel 
        {
        	position:absolute;
        	bottom:212px;
        	width:155px;
        	height:45px;
        	border:0px solid red;cursor:pointer;
        	left:80px;
        }
    </style>
    <script type="text/javascript">
        //Backspace, F5, Ctrl+N, Ctrl+R 막기
        document.onkeydown = NewEdit;
        function NewEdit() {
            var keyval = event.keyCode;
            if ((keyval == 116) || (event.ctrlKey == true && (event.keyCode == 78 || event.keyCode == 82))) {
                //if (event.srcElement.tagName == "INPUT") {
                //} else {
                event.keyCode = 0;
                event.cancelBubble = true;
                event.returnValue = false;
                //}
            }

            if (keyval == 8) {
                if (event.srcElement.tagName == "INPUT") {
                } else {
                    event.keyCode = 0;
                    event.cancelBubble = true;
                    event.returnValue = false;
                }
            }
        }

        function rb_Selected(val) {
            document.getElementById('hdSelectValue').value = val;

            //alert(document.getElementById('hdSelectValue').value);
        }

        function nextClick() {

            __doPostBack("<%= btnNext.UniqueID %>", "");
        }
    </script>
</head>
<%--<body>--%>
<body oncontextmenu='return false' ondragstart='return false'>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />    
            
        <!-- container -->
        <div id="container" class="mate">
 
	        <!-- wrapper -->
	        <div id="wrapper" class="apply translation-mate">
 
		        <!-- sidebar -->
		        <uc4:IntroRight ID="IntroRight1" runat="server" />
		        <!-- //sidebar -->
 
		        <!-- contents -->
		        <div id="contents">
			        <div class="headline headline-dep3">
				        <div class="txt">
					        <h3>번역 MATE</h3>
					        <p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				        </div>
			        </div>
			        <div class="breadcrumbs">
				        <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/Intro/MateIntro.aspx">MATE 소개</a><strong>MATE 신청</strong>
			        </div>

                    <% //수정 2012-12-17 %>
                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="650px">
                        <tr>
                            <% //수정 2013-06-14  이미지 교체 %>
                            <%--<td><img src="/Intro/image/trans_mate01.jpg" border="0" /></td>--%>
                            <%--<td><img src="/Intro/image/trans_top_20121217.jpg" border="0" /></td>--%>
                            <td style="width:25px;"></td>
                            <td style="background:url(/Intro/image/trans_content_2.jpg) no-repeat; width:600px; height:1540px;">

                                <%--<input runat="server" id="rb1" name="joinDate" type="radio" value="6월 20일 (금) 저녁 7시" onclick="test(this.value);" style="margin-top:1505px; margin-left:71px;" />
                                <input runat="server" id="rb2" name="joinDate" type="radio" value="6월 21일 (토) 오후 3시" style="margin-top:1505px; margin-left:112px;" />
                                <input runat="server" id="rb3" name="joinDate" type="radio" value="6월 21일 (토) 오후 5시 30분" style="margin-top:1505px; margin-left:113px;" />
                                <input runat="server" id="rb4" name="joinDate" type="radio" value="6월 26일 (목) 저녁 7시" style="margin-top:1505px; margin-left:134px;" />
                                <asp:ImageButton ID="btnNext" runat="server" OnClick="btnNext_Click" ImageUrl="~/Intro/image/btn_receipt.png" style="margin-top:55px; margin-left:210px;" />--%>
                           </td>
                        </tr>
                    </table>
                    <%=sgShowList %>
                    <%--ImageUrl="~/Intro/image/mate_request.png"--%>
                    <%--<a class="tbn-counsel" style="margin-top:15px; margin-left:220px;" onclick="nextClick();"></a>--%>
                    <p style="text-align:center;">
                        <asp:ImageButton ID="ImageButton1" runat="server" OnClick="btnNext_Click" ImageUrl="~/Intro/image/btn_receipt.png" style="margin-top:55px;" />
                    </p>
                    
		        </div>
		        <!-- // contents -->
	        </div>
	        <!-- // wrapper -->
        </div>
		<!-- // container -->          
        <uc3:Footer ID="Footer1" runat="server" />      
    </div>
       
    <input type="hidden" id="hdSelectValue" name="hdSelectValue" runat="server" />
    <asp:LinkButton ID="btnNext" runat="server" OnClick="btnNext_Click"  />

    </form>
</body>
</html>
