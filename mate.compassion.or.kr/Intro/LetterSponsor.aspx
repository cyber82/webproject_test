﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LetterSponsor.aspx.cs" Inherits="Mate_Intro_LetterSponsor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <style type="text/css">
        body{margin:auto;}
        
        .letter {width:600px;margin:0px auto 0px auto;list-style:none;padding-left:0px;}
        .letter .txt{background:url('/Intro/image/lettersponsor_wobtn.jpg') no-repeat}
        .letter .txt-desc{position:relative;width:600px;height:1290px;background-position:0 0;} 
        .letter .txt-cont{position:relative;width:600px;height:70px;background-position:0 -1290px;}

        .SelectCount{padding:7px 0 0 115px;width:178px;height:20px;}
        .SelectCount .list{width:178px;height:25px;font-size:12px;border:2px solid blue;}

   
        .btn-request-spon{position:absolute;width:180px;height:37px;top:0px;left:300px;border:0px solid red;}
    </style>

    <script type="text/javascript">
    <!--
        //Backspace, F5, Ctrl+N, Ctrl+R, Ctrl+C, Ctrl+V 막기
        document.onkeydown = NewEdit;
        function NewEdit() {
            var keyval = event.keyCode;
            if (keyval == 116) {
                EventSet();
                alert("F5 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 78)) {
                EventSet();
                alert("Ctrl + N 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 82)) {
                EventSet();
                alert("Ctrl + R 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 86)) {
                EventSet();
                alert("Ctrl + V 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 67)) {
                EventSet();
                alert("Ctrl + C 키를 금지합니다.");
            }

            //Backspace
            if (keyval == 8) {
                if (event.srcElement.tagName == "INPUT" || event.srcElement.tagName == "TEXTAREA") {
                } else {
                    EventSet();
                }
            }
        }

        function EventSet() {
            event.keyCode = 0;
            event.cancelBubble = true;
            event.returnValue = false;
        }
    //-->
    </script>
</head>

<%--<body>--%>
<body oncontextmenu='return false' ondragstart='return false'>
    <form id="form2" runat="server">
    <div>
        <!-- conts -->
		<div id="conts">
            <ul class="letter">
                <li class="txt txt-desc">
                </li>
                <li class="txt txt-cont">
                    <div class="SelectCount">
                        <select id="ddlCountSelect" class="list" runat="server">
                            <option value="none" selected="selected">희망 편지후원수</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                    <asp:ImageButton runat="server" ImageUrl="/Intro/image/btn_lettersponsor.png" CssClass="btn-request-spon" OnClick="SponRequest_click" />
                </li>
            </ul>
		</div>
		<!-- // conts -->    
    </div>
    </form>
</body>
</html>
