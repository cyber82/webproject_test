﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PopOfficeMateSubmint.aspx.cs" Inherits="Mate_Intro_PopOfficeMateSubmint" %>

<!DOCTYPE html>
<html lang="ko">
<head> 
	<meta charset="UTF-8">
    <meta property="og:image" content="http://www.compassion.or.kr/common/img/sns_default_image.jpg">
	<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title> 
	<link rel="stylesheet" href="/common/css/mate.css" />
 
	<script type="text/javascript" src="/common/js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="/common/js/common.js"></script>
    <script type="text/javascript" src="/common/js/util.js"></script>
    <script language="javascript">
        function Validate() {
            if (document.getElementById('comment').value.trim() == "") {
                alert('지원동기 및 봉사활동 가능시간을 입력해 주세요.');
                return false;
            }
            return true;
        }
    </script>
</head>
<body>
<form runat="server" id="form1">
<div class="popup1 business-mate-motive">
	<div class="tit"><strong>사무 메이트 지원동기</strong></div>
	<div class="cont">
		<p class="txt txt1"></p>
		
		<textarea rows="5" cols="50" runat="server" id="comment">1) 사무 메이트 지원 동기:
2) 봉사활동 가능 시간: 
        </textarea>
 
		<p class="txt txt2"><span>작성예시) 금요일 14:00~17:00</span></p>
	</div>
 
	<div class="btn-m">
        <asp:Button ID="Button1" runat="server" Text="" CssClass="btn btn-submit3" 
            onclick="Button1_Click" OnClientClick = "return Validate();" />
	</div>
</div>
</form>
</body>
</html>
