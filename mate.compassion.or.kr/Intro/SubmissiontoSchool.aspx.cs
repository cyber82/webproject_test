﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;

public partial class Mate_Intro_SubmissiontoSchool : System.Web.UI.Page
{
    string sjs = string.Empty;

    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //------ 로그인체크 -----------------------------------------------
            if (!UserInfo.IsLogin)
            {
                //로그인전이면 
                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("로그인 후에 출력할 수 있습니다.");
				//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/Intro/SubmissiontoSchool.aspx");
				sjs += JavaScript.GetPageMoveScript("/login.aspx");
				sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Move", sjs);
                return;
            }
            else
            {
                sjs = JavaScript.HeaderScript.ToString();
                sjs += "fnPrint();";
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "btnPrint", sjs);
                return;
            }
        }
    }
}