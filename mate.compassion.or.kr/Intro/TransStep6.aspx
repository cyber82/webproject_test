﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransStep6.aspx.cs" Inherits="Mate_Intro_TransStep6" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/IntroRight.ascx" tagname="IntroRight" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />

    <script type="text/javascript">
    <!--
            //Backspace, F5, Ctrl+N, Ctrl+R, Ctrl+C, Ctrl+V 막기
            document.onkeydown = NewEdit;
            function NewEdit() {
                var keyval = event.keyCode;
                if (keyval == 116) {
                    EventSet();
                    alert("F5 키를 금지합니다.");
                }
                if (event.ctrlKey == true && (event.keyCode == 78)) {
                    EventSet();
                    alert("Ctrl + N 키를 금지합니다.");
                }
                if (event.ctrlKey == true && (event.keyCode == 82)) {
                    EventSet();
                    alert("Ctrl + R 키를 금지합니다.");
                }
                if (event.ctrlKey == true && (event.keyCode == 86)) {
                    EventSet();
                    alert("Ctrl + V 키를 금지합니다.");
                }
                if (event.ctrlKey == true && (event.keyCode == 67)) {
                    EventSet();
                    alert("Ctrl + C 키를 금지합니다.");
                }

                //Backspace
                if (keyval == 8) {
                    if (event.srcElement.tagName == "INPUT" || event.srcElement.tagName == "TEXTAREA") {
                    } else {
                        EventSet();
                    }
                }
            }

            function EventSet() {
                event.keyCode = 0;
                event.cancelBubble = true;
                event.returnValue = false;
            }
    //-->
    </script>
</head>
<%--<body>--%>
<body style="overflow-X:hidden" oncontextmenu='return false' ondragstart='return false'>
    <form id="form1" runat="server">     
    <div>
        <%if (sTranslationType != "Extend")
          {  %>
        <uc2:Top ID="Top1" runat="server" />   
        <%} %>     
        <!-- container -->
        
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="apply apply-complete">
 
		<!-- sidebar -->
        <%if (sTranslationType != "Extend")
            {  %>
		<uc4:IntroRight ID="IntroRight1" runat="server" />
            <%} %>     
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>번역 MATE</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/Intro/MateIntro.aspx">MATE 소개</a><strong>MATE 신청</strong>
			</div>
            <%if (sTranslationType != "Extend")
                {  %>
			<ul class="tab2 tab-mate">
				<li class="on"><a href="TransIntro.aspx" class="t1">번역 MATE</a></li>
				<li><a href="officemate.aspx" class="t2">사무 MATE</a></li>
				<li><a href="eventmate.aspx" class="t3">행사 MATE</a></li>
				<li><a href="specialitymate.aspx" class="t4">전문 MATE</a></li>
			</ul>
             <%} %>  
			<div class="txt-procedure">
				<h4>편지 번역 메이트 신청 절차</h4>
			</div>
			<ul id="tap" runat="server" class="tab3 tab-step">
				<li><a href="javascript:void(0);" class="t1">STEP01 메이트 정보 입력</a></li>
				<li><a href="javascript:void(0);" class="t2">STEP02 번역유의사항 PDF DOWN</a></li>
				<li><a href="javascript:void(0);" class="t3">STEP03 편지번역 SAMPLE TEST</a></li>
				<li><a href="javascript:void(0);" class="t4">STEP04 번역 메이트 활동 동의서 서명</a></li>
				<li class="on"><a href="javascript:void(0);" class="t5">STEP05 지원완료</a></li>
			</ul>
			<div class="step-wrap">
				<div class="text txt-greeting">
					<p>&quot;Thank you&quot;번역 메이트로 지원해 주셔서 감사합니다. 컴패션 편지 번역 메이트로 지원해 주셔서 감사드립니다. 합격 결과는 공지사항 및 마이페이지에서 확인하실 수 있습니다.</p>                    
				</div>
                <% //수정 2013-03-07, 2013-05-28, 2013-09-23 %>
                <div style="text-align:center; font-weight:bold;" ><p><font color="red"><asp:Literal runat="server" ID="announce_date"></asp:Literal></font></p></div>
                <div>&nbsp;</div>
				<div class="txt-info">
					<p style="font-size:9pt;font-family:돋움;">신청 관련 문의 사항은 <b><asp:Literal runat="server" ID="tel" /></b> 또는 이메일 <b><asp:Literal runat="server" ID="email" /></b>로 연락 주시기 바랍니다.</p>
				</div>
			</div>
 
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

		<!-- // container -->    
        <%if (sTranslationType != "Extend")
               {  %>
       <uc3:Footer ID="Footer1" runat="server" />    
       <%} %>   
    </div>    
    </form>
</body>
</html>
