﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Collections;

public partial class Mate_Intro_LetterSponsor : System.Web.UI.Page
{
    DateTime dt;
    string sjs = string.Empty;

    UserInfo sess = new UserInfo();

    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void SponRequest_click(object sender, EventArgs e)
    {
        if (!UserInfo.IsLogin)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("로그인이 필요합니다.");
			//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/Intro/LetterSponsor.aspx");
			sjs += JavaScript.GetPageMoveScript("/login.aspx");
			sjs += JavaScript.FooterScript.ToString();

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            return;
        }

        else if (ddlCountSelect.Value == "none")
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("희망 편지 후원 수를 선택해 주시기 바랍니다.");
            sjs += JavaScript.FooterScript.ToString();

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            return;
        }

        else
        {
            WWWService.Service _wwwService = new WWWService.Service();
            
            DataSet ds = new DataSet();

            try
            {
                ds = _wwwService.getCorrespondenceSponsor("1", sess.SponsorID);
            }

            catch (Exception ex)
            {
                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("신청 여부를 확인 하는 도중 오류가 발생하였습니다.");
                sjs += JavaScript.FooterScript.ToString();

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                return;
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("이미 편지 후원을 신청하셨습니다.");
                sjs += JavaScript.FooterScript.ToString();

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                ds.Dispose();

                return;
            }

            else
            {
                try
                {
                    ds = _wwwService.getCorrespondenceSponCount("1");
                }

                catch (Exception ex)
                {
                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("신청 수를 가져오는 도중 오류가 발생하였습니다.");
                    sjs += JavaScript.FooterScript.ToString();

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                    return;
                }

                if (ds != null)
                {
                    if (Convert.ToInt32(ds.Tables[0].Rows[0]["Cnt"]) >= 150)
                    {
                        sjs = JavaScript.HeaderScript.ToString();
                        sjs += JavaScript.GetAlertScript("죄송합니다. 신청이 마감되었습니다.");
                        sjs += JavaScript.FooterScript.ToString();

                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                        ds.Dispose();

                        return;
                    }

                    else
                    {
                        try
                        {
                            if (sess.LocationType == "국내")
                            {
                                string result = _wwwService.setCorrespondenceSponRequest("1", sess.SponsorID, sess.ConId, ddlCountSelect.Value, sess.UserId, sess.UserName);

                                if (result == "Y")
                                {
                                    sjs = JavaScript.HeaderScript.ToString();
                                    sjs += JavaScript.GetAlertScript("신청해 주셔서 감사합니다. 편지후원 어린이의 자료는 2주 안에 받으실 수 있습니다.");
                                    sjs += JavaScript.FooterScript.ToString();

                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Success", sjs);

                                    ds.Dispose();

                                    return;
                                }

                                else if (result == "N")
                                {
                                    sjs = JavaScript.HeaderScript.ToString();
                                    sjs += JavaScript.GetAlertScript("신청 도중 오류가 발생하였습니다.");
                                    sjs += JavaScript.FooterScript.ToString();

                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                                    ds.Dispose();

                                    return;
                                }
                            }

                            else
                            {
                                sjs = JavaScript.HeaderScript.ToString();
                                sjs += JavaScript.GetAlertScript("국내 거주 후원자 분만 신청이 가능합니다.");
                                sjs += JavaScript.FooterScript.ToString();

                                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                                ds.Dispose();

                                return;
                            }
                        }

                        catch
                        {
                            sjs = JavaScript.HeaderScript.ToString();
                            sjs += JavaScript.GetAlertScript("신청 도중 오류가 발생하였습니다.");
                            sjs += JavaScript.FooterScript.ToString();

                            ds.Dispose();

                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                            return;
                        }
                    }
                }
            }
        }
    }
}