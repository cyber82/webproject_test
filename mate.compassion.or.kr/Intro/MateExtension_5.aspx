﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MateExtension_5.aspx.cs" Inherits="Mate_Intro_MateExtension_5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <style type="text/css">
        .trans{width:600px;margin:0px auto 0px auto;list-style:none;padding-left:0px;}
        .trans .txt{background:url('/Intro/image/transmate_main_5.jpg') no-repeat} /* 600 * 1273 */
        .trans .txt-desc{position:relative;width:600px;height:500px;background-position:0 0;} 
        .trans .txt-cont{position:relative;width:600px;height:430px;background-position:0 -820px;}
                
        .trans .video{width:389px;height:292px;margin:10px auto 20px auto;}
        .trans .txt-cont .btn-apply{position:absolute;float:left;width:90px;height:28px;top:395px;left:65px;border:0px solid red;
                                    background:url('/Intro/image/btn_mate.png') no-repeat;cursor:pointer;}
           
        .trans .btn-request{position:relative;width:107px;height:27px;background-position:-250px -1200px;top:378px;left:246px;}                 
    </style>

    <script type="text/javascript">
    <!--
        //Backspace, F5, Ctrl+N, Ctrl+R, Ctrl+C, Ctrl+V 막기
        document.onkeydown = NewEdit;
        function NewEdit() {
            var keyval = event.keyCode;
            if (keyval == 116) {
                EventSet();
                alert("F5 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 78)) {
                EventSet();
                alert("Ctrl + N 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 82)) {
                EventSet();
                alert("Ctrl + R 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 86)) {
                EventSet();
                alert("Ctrl + V 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 67)) {
                EventSet();
                alert("Ctrl + C 키를 금지합니다.");
            }

            //Backspace
            if (keyval == 8) {
                if (event.srcElement.tagName == "INPUT" || event.srcElement.tagName == "TEXTAREA") {
                } else {
                    EventSet();
                }
            }
        }

        function EventSet() {
            event.keyCode = 0;
            event.cancelBubble = true;
            event.returnValue = false;
        }

        //TransStep4.aspx 이동
        function ShowExtendClause() {

            sWidth = "780";
            sHeight = "520";

            if (screen.width < 1025) {
                LeftPosition = 0;
                TopPosition = 0;
            } else {
                LeftPosition = (screen.width) ? (screen.width - sWidth) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - sHeight) / 2 : 100;
            }

            window.open("ExtensionAgreement.aspx", "ExtendClause", "width=" + sWidth + ",height=" + sHeight + ",top=" + TopPosition + ",left=" + LeftPosition + ", scrollbars=yes, toolbar=no menubar=no, location=no");
        }
    //-->
    </script>
</head>

<%--<body>--%>
<body oncontextmenu='return false' ondragstart='return false'>
    <form id="form2" runat="server">
    <div>
        <!-- conts -->
		<div id="conts">

            <ul class="trans">
                <li class="txt txt-desc">
                </li>

                <li class="video">
                    <iframe width='389' height='292' src='http://www.youtube.com/embed/5oa6oN6_TII?rel=0&autoplay=1' frameborder='0' allowfullscreen></iframe>
                </li>

                <li class="txt txt-cont">
                    <asp:Button ID="imgBtnExtendApply" runat="server" Text="" OnClick="SetImgBtnExtendApply_Click" CssClass="txt btn-request" />
                </li>
            </ul>
            
		</div>
		<!-- // conts -->
        
    </div>
    </form>
</body>
</html>
