﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using CommonLib;

public partial class Mate_Intro_PopOfficeMateSubmint : System.Web.UI.Page
{
    public string sgMateYearCode;

    protected void Page_Load(object sender, EventArgs e)
    {
        sgMateYearCode = Request.QueryString["MateYearCode"];
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (!UserInfo.IsLogin)
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("로그인을 하신후 메이트 신청이 가능 합니다.");             
            sjs += JavaScript.GetFormCloseScript(); 
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            Response.End();
            return;
        }

        WWWService.Service _wwwService = new WWWService.Service();
        DataSet dsCheck = new DataSet();
        UserInfo user = new UserInfo();

        String status = "APPLY";
        string sponsorId = "";
        using (AuthDataContext dao = new AuthDataContext())
        {
            //sponsorId = dao.tSponsorMaster.First(p => p.UserID == user.UserId).tempSponsorID;
            sponsorId = www6.selectQFAuth<tSponsorMaster>("UserID", user.UserId).tempSponsorID;
        }

        Object[] objSql = new object[1] {"SELECT * FROM MATE_Joining WHERE MateYearCode = '" + sgMateYearCode + "' AND SponsorID = '" + sponsorId + "' AND Status = '" + status + "'"};

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        try
        {
            //dsCheck = _wwwService.checkDATMateJoining(user.SponsorID //SponsorID
            //                                         , "사무" //Mate타입
            //                                         );
            //신청내역
            

            dsCheck = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);     
        }

        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert           
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate신청여부를 확인 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.GetFormCloseScript(); 
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        //DB Error
        if (dsCheck == null)
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate신청여부를 확인 중 오류가 발생했습니다.\\r\\n관리자에게 문의해주세요.");
            sjs += JavaScript.GetFormCloseScript(); 
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        //
        else
        {
            //신청내역이 있으면 return false
            if (dsCheck.Tables[0].Rows.Count > 0)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("해당 Mate는 이미 신청하신 상태로 승인처리 중 입니다.");
                sjs += JavaScript.GetFormCloseScript(); 
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                return;
            }
        }

        dsCheck.Dispose();

        //WWWService.Service wwwService = new WWWService.Service();

        //기존연결
        //DataSet data = wwwService.MateMyActive(new UserInfo().UserId);

        objSql = new object[1] { "SELECT * FROM MATE_Master WHERE MateID = '" + new UserInfo().UserId + "' AND MateYearCode='" +sgMateYearCode + "'" };

        DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

        if (data.Tables[0].Rows.Count == 0)
        {
        }

        else
        {       
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("이미 사무메이트 등록 된 회원님이십니다.");
            sjs += JavaScript.GetFormCloseScript(); 
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;    
        }

        data.Dispose();

        UserInfo sess = new UserInfo();
        string sResult = string.Empty;

        #region //getData

        string sApplicationReason = comment.Value; 

        //봉사경험 여부
        string sExperienceYN = string.Empty;
        //봉사경험
        string sExperienceDescription = string.Empty;
        //번역
        string sTranslation = String.Empty;
        //MATE_Joining 등록 성공 유무
        int iResult;

        #endregion

        try
        {
            objSql = new object[1] { "DELETE MATE_Joining WHERE MateYearCode = '" + sgMateYearCode + "' AND SponsorID = '" + sponsorId + "'" };
    
            data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            //sResult = wwwService.registerDATMateJoining(sess.SponsorID //SponsorID
            //                                            , sess.UserName //SponsorName
            //                                            , sess.UserId //WebID
            //                                            , sMateType //Mate타입
            //                                            , sApplicationReason //신청사유
            //                                            , sExperienceYN //봉사경험여부
            //                                            , sExperienceDescription //봉사경험
            //                                            , sTranslation //번역예문
            //                                            );

            objSql = new object[1] { "MATE_WebJoiningSave" };
            Object[] objParam = new object[11] { "rowState",//쿼리 선택 값	
                                                    "MateYearCode",//기수
                                                    "SponsorID",//SponsorID
                                                    "SponsorName",//SponsorName
                                                    "UserID",//WebID
                                                    "ApplicationMotive",//신청 사유
                                                    "ApplicationDT",//신청 날짜
                                                    "SmsReceive",//수신여부
                                                    "Experience",//봉사활동내역
                                                    "TranslationType",//(번역,작문) 타입
                                                    "SampleTest",//샘플번역
                                            };
            Object[] objValue = new object[11] { "NEW",
                                                sgMateYearCode,
                                                sponsorId,
                                                sess.UserName,
                                                sess.UserId,
                                                sApplicationReason,
                                                "",
                                                "",
                                                sExperienceDescription,
                                                "",
                                                sTranslation
                                            };


            //MATE_Joining 등록
            iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            _WWW6Service.Dispose();
        }

        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate신청 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.GetFormCloseScript(); 
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        //DB Error
        if (iResult < 1)
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate신청 중 오류가 발생했습니다");
            sjs += JavaScript.GetFormCloseScript(); 
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }

        //신청 등록 완료
        else
        {
            //완료페이지로 이동
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("사무 메이트 신청이 완료 되었습니다.");
            sjs += JavaScript.GetFormCloseScript(); 
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
            return;
        }
    }
}