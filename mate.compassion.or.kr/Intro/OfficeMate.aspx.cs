﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CommonLib;

public partial class Mate_Intro_OfficeMate : System.Web.UI.Page
{
    public int ngMateYearList;
    public int ngMateJoinList;
    public string sgMateYearCode;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        string sJs;

        UserInfo user = new UserInfo();
        if (UserInfo.IsLogin)
        {

            string sponsorId = "";
            using (AuthDataContext dao = new AuthDataContext())
            {
                //sponsorId = dao.tSponsorMaster.First(p => p.UserID == user.UserId).tempSponsorID;
                sponsorId = www6.selectQFAuth<tSponsorMaster>("UserID", user.UserId).tempSponsorID;
            }
            Object[] objSql = new object[2] { "SELECT MateYearCode FROM MATE_Recruit WHERE CONVERT(DATE, GETDATE()) BETWEEN RecruitStart AND RecruitEnd AND MateYearCode = dbo.uf_GetMateYearCode(0)"
        ,"SELECT * FROM MATE_Joining WHERE MateYearCode = dbo.uf_GetMateYearCode(0) AND SponsorID = '" + sponsorId + "'" + " AND Status = '" + "APPROVAL" + "'"};

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
            DataSet dsCheck = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            ngMateYearList = dsCheck.Tables[0].Rows.Count;
            ngMateJoinList = dsCheck.Tables[1].Rows.Count;

            if (ngMateYearList > 0)
            {
                sgMateYearCode = dsCheck.Tables[0].Rows[0][0].ToString();
            }

            dsCheck.Dispose();
            _WWW6Service.Dispose();
        }else
        {
            //string sReturnUrl = Request.Url.AbsoluteUri.Replace("http://" + Request.Url.Authority, "");
            //sReturnUrl = sReturnUrl.Replace("https://" + Request.Url.Authority, "");

            //sJs = JavaScript.HeaderScript;
            //sJs += JavaScript.GetAlertScript("로그인 후 사용 가능합니다.");
            //sJs += JavaScript.GetPageMoveScript("/login.aspx");
            //sJs += JavaScript.GetHistoryBackScript();
            //sJs += JavaScript.FooterScript;
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }
    }  
}