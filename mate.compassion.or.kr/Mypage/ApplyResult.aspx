﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ApplyResult.aspx.cs" Inherits="Mate_Mypage_ApplyResult" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/MypageRight.ascx" tagname="MypageRight" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript">
        function Openwin() {
            NewWindow("/intro/TransStep5.aspx?isPop=1", "popupaaa", "1024", "600", "yes");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->        
        <div id="container" class="mate">
 
	        <!-- wrapper -->
	        <div id="wrapper" class="mypage apply-result">
 
		        <!-- sidebar -->
		        <uc4:MypageRight ID="MypageRight1" runat="server" />
		        <!-- //sidebar -->
 
		        <!-- contents -->
		        <div id="contents">
			        <div class="headline headline-dep3">
				        <div class="txt">
					        <h3>MATE 신청 결과</h3>
					        <p>전 세계 가난한 어린이들을 위해 자발적 헌신하는 든든한 자원봉사자 MATE로 선정이 되었는지 확인할 수 있는 공간입니다.</p>
				        </div>
			        </div>
			        <div class="breadcrumbs">
				        <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/mypage/ServiceBreakDown.aspx">마이페이지</a><strong>MATE 신청 결과</strong>
			        </div>


                    <% //추가 2013-06-10 %>
                    <div class="wrap_bg" runat="server" id="mateID0">
					    <div id="a0" runat="server" style="margin-top:30px;">
                            메이트 신청 내역이 없습니다.
					    </div>
                    </div>

                    <div class="wrap_bg" runat="server" id="mateID1">
					    <div id="a1" runat="server">
						    <span><asp:Label ID="lbUserName1" runat="server" Text="" class="text-breakdown"></asp:Label>님께서는</span>

						    <span><asp:Label ID="lbAppDate1" runat="server" Text="" class="text-breakdown"></asp:Label>에</span>

                            <span><asp:Label ID="lbMateType_A1" runat="server" Text="" class="text-breakdown"></asp:Label>로 신청하셨습니다.</span><br />

						    <span><asp:Label ID="lbMateType_A2" runat="server" Text=""></asp:Label>로 지원해 주셔서 감사드립니다.</span><br /><br />

						    <strong><span>신청 결과: <asp:Label ID="lbResult1" runat="server" Text="" class="text-breakdown"></asp:Label></span></strong>
					    </div>
                    </div>

                    <div class="wrap_bg" runat="server" id="mateID2">
					    <div id="a2" runat="server">
						    <span><asp:Label ID="lbUserName2" runat="server" Text="" class="text-breakdown"></asp:Label>님께서는</span>

						    <span><asp:Label ID="lbAppDate2" runat="server" Text="" class="text-breakdown"></asp:Label>에</span>

                            <span><asp:Label ID="lbMateType_B1" runat="server" Text="" class="text-breakdown"></asp:Label>로 신청하셨습니다.</span><br />

						    <span><asp:Label ID="lbMateType_B2" runat="server" Text=""></asp:Label>로 지원해 주셔서 감사드립니다.</span><br /><br />

						    <strong><span>신청 결과: <asp:Label ID="lbResult2" runat="server" Text="" class="text-breakdown"></asp:Label></span></strong>
					    </div>
                    </div>


                    <% //주석처리 2013-06-10 %>
			        <%--<div class="cont-wrap" runat="server" id="translation" visible ="false">
				        <strong class="txt txt-celebration1"><span>축하드립니다!</span></strong>
				        <strong class="txt txt-celebration2"><span>편지번역 메이트로 선정되셨습니다.</span></strong>
				        <div class="list-wrap">
					        <div class="butterfly"></div>
					        <p>
						        <strong class="text-breakdown"><asp:Label ID="labName1" runat="server" Text=""></asp:Label></strong>
						        <strong class="txt txt-pass1"><span>님께서는</span></strong>
						        <strong class="text-breakdown"><asp:Label ID="labDate1" runat="server" Text=""></asp:Label></strong>
						        <strong class="txt txt-pass2"><span>일 부터</span></strong><br>
						        <strong class="txt txt-pass3"><span>컴패션</span></strong>
						        <strong class="text-breakdown">편지번역 메이트</strong>
						        <strong class="txt txt-pass4"><span>로 활동하게 되었습니다.</span></strong>
					        </p>
					        <a href="javascript:Openwin();" class="btn btn-consent"><span>번역메이트 활동 동의서 보기</span></a>
				        </div>
			        </div>
        
			        <div class="cont-wrap" runat="server" id="office" visible ="false">
				        <strong class="txt txt-celebration1"><span>축하드립니다!</span></strong>
				        <strong class="txt txt-celebration3"><span>사무 메이트로 선정되셨습니다.</span></strong>
				        <div class="list-wrap cont">
					        <div class="butterfly"></div>
					        <p>
						        <strong class="text-breakdown"><asp:Label ID="labName2" runat="server" Text=""></asp:Label></strong>
						        <strong class="txt txt-pass1"><span>님께서는</span></strong>
						        <strong class="text-breakdown"><asp:Label ID="labDate2" runat="server" Text=""></asp:Label></strong>
						        <strong class="txt txt-pass2"><span>일 부터</span></strong><br>
						        <strong class="txt txt-pass3"><span>컴패션</span></strong>
						        <strong class="text-breakdown">사무 메이트</strong>
						        <strong class="txt txt-pass4"><span>로 활동하게 되었습니다.</span></strong>
					        </p>
				        </div>
			        </div>
        
			        <div class="cont-wrap" runat="server" id="eventmate" visible ="false">
				        <strong class="txt txt-celebration1"><span>축하드립니다!</span></strong>
				        <strong class="txt txt-celebration4"><span>행사 메이트로 선정되셨습니다.</span></strong>
				        <div class="list-wrap cont">
					        <div class="butterfly"></div>
					        <p>
						        <strong class="text-breakdown"><asp:Label ID="labName3" runat="server" Text=""></asp:Label></strong>
						        <strong class="txt txt-pass1"><span>님께서는</span></strong>
						        <strong class="text-breakdown"><asp:Label ID="labDate3" runat="server" Text=""></asp:Label></strong>
						        <strong class="txt txt-pass2"><span>일 부터</span></strong><br>
						        <strong class="txt txt-pass3"><span>컴패션</span></strong>
						        <strong class="text-breakdown">행사 메이트</strong>
						        <strong class="txt txt-pass4"><span>로 활동하게 되었습니다.</span></strong>
					        </p>
				        </div>
			        </div>
        
			        <div class="cont-wrap" runat="server" id="profession" visible ="false">
				        <strong class="txt txt-celebration1"><span>축하드립니다!</span></strong>
				        <strong class="txt txt-celebration5"><span>전문 메이트로 선정되셨습니다.</span></strong>
				        <div class="list-wrap cont">
					        <div class="butterfly"></div>
					        <p>
						        <strong class="text-breakdown"><asp:Label ID="labName4" runat="server" Text=""></asp:Label></strong>
						        <strong class="txt txt-pass1"><span>님께서는</span></strong>
						        <strong class="text-breakdown"><asp:Label ID="labDate4" runat="server" Text=""></asp:Label></strong>
						        <strong class="txt txt-pass2"><span>일 부터</span></strong><br>
						        <strong class="txt txt-pass3"><span>컴패션</span></strong>
						        <strong class="text-breakdown">전문 메이트</strong>
						        <strong class="txt txt-pass4"><span>로 활동하게 되었습니다.</span></strong>
					        </p>
				        </div>
			        </div>--%>
		        </div>
		        <!-- // contents -->
 
	        </div>
	        <!-- // wrapper -->
 
        </div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    
    </form>
</body>
</html>
