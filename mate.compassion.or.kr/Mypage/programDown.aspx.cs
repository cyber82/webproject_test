﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mate_Mypage_programDown : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!UserInfo.IsLogin)
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("로그인을 하신 후 확인하실 수 있습니다.");
			//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/mate/mypage/programDown.aspx");
			sjs += JavaScript.GetPageMoveScript("/login.aspx");
			sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            return;
        }
    }

    protected void btnDown_Click(object sender, EventArgs e)
    {
        //세션체크
        UserInfo sess = new UserInfo();

        if (sess.Mate != null && (sess.Mate.IndexOf("번역") > -1 || sess.Mate.IndexOf("영작") > 0))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Msg", "<script>window.open('http://ws.compassion.or.kr/ltr4setup/');</script>");
        }

        else
        {
            //번역메이트인 사람외에는 클릭불가능
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("번역메이트이신분만 이용하실 수 있습니다.");
            sjs += JavaScript.FooterScript.ToString();

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Msg", sjs);
            return;
        }
    }
}