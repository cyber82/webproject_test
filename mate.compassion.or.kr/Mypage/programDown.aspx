﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="programDown.aspx.cs" Inherits="Mate_Mypage_programDown" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/MypageRight.ascx" tagname="MypageRight" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="mypage program-down">
 
		<!-- sidebar -->
		<uc4:MypageRight ID="MypageRight1" runat="server" />
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>번역프로그램 DOWN</h3>
					<p>다양한 언어로 이루어진 어린이들의 편지번역에 필요한 프로그램을 다운받을 수 있는 공간입니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/mypage/ServiceBreakDown.aspx">마이페이지</a><strong>번역프로그램 DOWN</strong>
			</div>
			<strong class="txt txt-install"><span>&quot;안내에 따라 설치하세요&quot;</span></strong>
			<div class="install-wrap">
				<ul class="txt txt-order">
					<li><strong>iMate사용메뉴얼</strong> 과 <strong>번역유의사항</strong> 다운로드 및 숙지</li>
					<li>하단에 있는 <strong>iMate설치하기</strong> 버튼을 클릭합니다.</li>
					<li>설치페이지로 이동 한 후 <strong>설치</strong> 버튼을 클릭합니다.</li>
					<li><strong>실행</strong> 버튼을 누르고, iMate 설치를 완료합니다.</li>
					<li>컴패션 홈페이지 아이디/비밀번호를 사용하여 로그인 합니다.</li>
				</ul>
			</div>
			<div class="btn-m">
				<a href="/Files/iMate_manual.pdf" class="btn btn-imate"><span>imate사용메뉴얼</span></a>
				<a href="/Files/Trans_careful.pdf" class="btn btn-attention"><span>번역 유의사항</span></a>
                <asp:Button ID="btnDown" runat="server" Text="" CssClass="btn btn-install" 
                    onclick="btnDown_Click" />
			</div>
			<div class="contact-wrap">
				<strong class="txt txt-contact"><span>iMATE 설치 관련 자세한 문의는 hyunkim@compassion.or.kr 또는 전화 02-3668-3404를 이용해 주시기 바랍니다.</span></strong>
			</div>
 
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    </form>
</body>
</html>
