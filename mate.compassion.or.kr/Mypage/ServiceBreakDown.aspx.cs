﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;

public partial class Mate_Mypage_ServiceBreakDown : System.Web.UI.Page
{
    private bool _bDataCheck;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!UserInfo.IsLogin)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("로그인을 하신 후 확인하실 수 있습니다.");
				//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/mate/mypage/ServiceBreakDown.aspx");
				sjs += JavaScript.GetPageMoveScript("/login.aspx");
				sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                return;
            }

            //UserInfo user = new UserInfo ();

            //if (string.IsNullOrEmpty (user.Mate) || (user.Mate.IndexOf("번역") == -1 && user.Mate.IndexOf("영작") == -1))
            //{
            //    string sjs = JavaScript.HeaderScript.ToString();
            //    sjs += JavaScript.GetAlertScript("메이트이신 분만 확인하실 수 있습니다.");
            //    sjs += JavaScript.GetPageMoveScript("/default.aspx");
            //    sjs += JavaScript.FooterScript.ToString();
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            //    return;
            //}

            txtDateFrom.Text = System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");
            txtDateTo.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
        }

        //btnSearch.Attributes.Add("onclick", "ShowPrint()");

        //if (!_bDataCheck)
        //{
        //    btnPrint.Attributes.Add("onclick", "alert('검색된 결과가 없습니다.'); return false");
        //}
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        WWWService.Service _wwwService = new WWWService.Service();
        UserInfo sess = new UserInfo();

        try
        {
            if (!UserInfo.IsLogin)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("로그인을 하신후 조회가 가능 합니다.");
				//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/mate/mypage/ServiceBreakDown.aspx");
				sjs += JavaScript.GetPageMoveScript("/login.aspx");
				sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                _wwwService.Dispose();

                return;
            }

            DataSet dsMateActivity = null;

            string sFrom = Convert.ToDateTime(txtDateFrom.Text).ToString("yyyyMMdd");
            string sTo = Convert.ToDateTime(txtDateTo.Text).ToString("yyyyMMdd");
            string sDIVIS = "";

            if (this.rboTran.Checked)
                sDIVIS = "TRANS";

            else
                sDIVIS = "BIZWEB";

            Object[] objSql = new object[1] { "MATE_BreakDownList" };
            Object[] objParam = new object[] { "MateID", "DateFrom", "DateTo", "DIVIS", "DETAIL" };
            Object[] objValue = new object[] { new UserInfo().UserId, sFrom, sTo, sDIVIS, CodeAction.ChannelType, };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            dsMateActivity = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);
            //dsMateActivity = _wwwService.MateActivity_List(sess.SponsorID, sFrom, sTo);

            DataSet ds = new DataSet();

            if (dsMateActivity.Tables[0].Rows.Count > 0)
            {
                #region 주석
                //_bDataCheck = true;

                //int iNo = 1;

                //foreach (DataRow drRow in dsMateActivity.Tables[0].Rows)
                //{
                //    drRow["OrderNo"] = iNo.ToString();
                //    iNo++;
                //}
                #endregion

                if (sDIVIS == "TRANS")
                {
                    DataTable dt = new DataTable();

                    dt = dsMateActivity.Tables[0].Clone();

                    ds.Tables.Add(dt);

                    int order = 1;

                    String WorkingType = dsMateActivity.Tables[0].Rows[0]["MateTransType"].ToString();
                    String WorkingEnd = String.Empty;

                    bool isChild = true;
                    bool isSponsor = true;

                    foreach (DataRow dr in dsMateActivity.Tables[0].Rows)
                    {
                        if (WorkingEnd != dr["WorkingEnd"].ToString() || (WorkingEnd == dr["WorkingEnd"].ToString() && WorkingType != dr["MateTransType"].ToString()))
                        {
                            if (WorkingEnd != dr["WorkingEnd"].ToString())
                            {
                                isChild = true;
                                isSponsor = true;
                            }

                            if (WorkingType != dr["MateTransType"].ToString())
                            {
                                if (dr["MateTransType"].ToString() == "CHILDLETTER" && !isChild)
                                    continue;

                                else if (dr["MateTransType"].ToString() == "SPONSORLETTER" && !isSponsor)
                                    continue;

                                else
                                    WorkingType = dr["MateTransType"].ToString();
                            }

                            if (WorkingType == "CHILDLETTER" && isChild)
                                isChild = false;

                            else if (WorkingType == "SPONSORLETTER" && isSponsor)
                                isSponsor = false;

                            int mm = 0;

                            WorkingEnd = dr["WorkingEnd"].ToString();

                            for (int j = 0; j < dsMateActivity.Tables[0].Rows.Count; j++)
                            {
                                if (WorkingEnd == dsMateActivity.Tables[0].Rows[j]["WorkingEnd"].ToString() && WorkingType == dsMateActivity.Tables[0].Rows[j]["MateTransType"].ToString())
                                    mm += Convert.ToInt32(dsMateActivity.Tables[0].Rows[j]["mm"]);
                            }

                            DataRow drNew = ds.Tables[0].NewRow();

                            drNew["OrderNo"] = order;
                            drNew["Expr1"] = dr["Expr1"];
                            drNew["MateTransKorType"] = dr["MateTransKorType"];
                            drNew["MateTransType"] = dr["MateTransType"];
                            drNew["TranslationID"] = dr["TranslationID"];
                            drNew["TranslationName"] = dr["TranslationName"];
                            drNew["WorkingEnd"] = dr["WorkingEnd"];

                            if ((mm / 60) == 0)
                                drNew["mm"] = mm.ToString() + "분";

                            else if ((mm % 60) != 0)
                                drNew["mm"] = (mm / 60).ToString() + "시간 " + (mm % 60).ToString() + "분";

                            else if ((mm % 60) == 0)
                                drNew["mm"] = (mm / 60).ToString() + "시간";

                            drNew["Charge"] = dr["Charge"];
                            drNew["ChargeTelNo"] = dr["ChargeTelNo"];

                            ds.Tables[0].Rows.Add(drNew);

                            order++;
                        }
                    }
                }

                else if (sDIVIS == "BIZWEB")
                {
                    DataTable dt2 = dsMateActivity.Tables[0].Copy();

                    ds.Tables.Add(dt2);
                }
            }

            else
            {
                _bDataCheck = false;

                string sjs = "";
                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("검색된 결과가 없습니다.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
            }

            repData.DataSource = ds.Tables[0];
            repData.DataBind();

            ds.Dispose();
            dsMateActivity.Dispose();
        }

        catch (Exception ex)
        {
            string sMsg = ex.Message;
            ////Exception Error Insert
            //WWWService.Service _wwwService = new WWWService.Service();
            //_wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);
            Response.Write(sMsg);
            string sjs = "";
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("검색기간 형식이 잘못되었습니다.");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());

            txtDateFrom.Text = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");
            txtDateTo.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        UserInfo sess = new UserInfo();
        WWWService.Service _wwwService = new WWWService.Service();

        try
        {
            if (!UserInfo.IsLogin)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("로그인을 하신후 출력 가능 합니다.");
				//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/mate/mypage/ServiceBreakDown.aspx");
				sjs += JavaScript.GetPageMoveScript("/login.aspx");
				sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                return;
            }

            DataSet dsMateActivity = null;
            string sFrom = Convert.ToDateTime(txtDateFrom.Text).ToString("yyyyMMdd");
            string sTo = Convert.ToDateTime(txtDateTo.Text).ToString("yyyyMMdd");

            Object[] objSql = new object[1] { "MATE_BreakDownList" };
            Object[] objParam = new object[4] { "MateID", "DateFrom", "DateTo", "DETAIL" };
            Object[] objValue = new object[4] { new UserInfo().UserId, sFrom, sTo, CodeAction.ChannelType };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            dsMateActivity = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //dsMateActivity = _wwwService.MateActivity_List(sess.SponsorID, sFrom, sTo);

            if (dsMateActivity.Tables[0].Rows.Count <= 0)
            {
                string sjs = "";
                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("검색된 결과가 없습니다.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());

            }

            // 영문 일 시 영문명 체크
            string strFullName = dsMateActivity.Tables[0].Rows[0]["FirstName"].ToString() + " " + dsMateActivity.Tables[0].Rows[0]["LastName"].ToString();
            if (outputform2.Checked)
            {
                if (string.IsNullOrEmpty(strFullName.Trim()))
                {
                    string sjs = "";
                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += "goUserInfo();";
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "goUserInfo", sjs.ToString());
                    return;
                }
            }

            //Response.Write(dsMateActivity.Tables[0].Rows[0]["Address1"].ToString());
            //Response.End();
            //----- Define
            string sSponsorName = sess.UserName; //후원자명
            string sSponsorAddress = dsMateActivity.Tables[0].Rows[0]["Address1"].ToString() + " " + dsMateActivity.Tables[0].Rows[0]["Address2"].ToString(); ; //주소
            string sJuminID = dsMateActivity.Tables[0].Rows[0]["JuminID"].ToString(); //주민등록번호
            string sVolunteerTerm = string.Empty; //봉사기간
            string sVolunteerTime = string.Empty; //봉사시간
            string sVolunteerContents = dsMateActivity.Tables[0].Rows[0]["MateTransKorType"].ToString(); //봉사내용
            string sPrintDate = string.Empty; //출력일
            string sBirthDate = DateTime.Parse(dsMateActivity.Tables[0].Rows[0]["BirthDate"].ToString()).ToString("yyyy-MM-dd");

            string sSponsorNameENG = dsMateActivity.Tables[0].Rows[0]["FirstName"].ToString() + " " + dsMateActivity.Tables[0].Rows[0]["LastName"].ToString(); //후원자명ENG
            string sVolunteerTermENG = string.Empty; //봉사기간ENG
            string sVolunteerTimeENG = string.Empty; //봉사시간ENG
            string sPrintDateENG = string.Empty; //출력일ENG

            string sCharge = dsMateActivity.Tables[0].Rows[0]["Charge"].ToString(); //담당자
            string sChargeTelNo = dsMateActivity.Tables[0].Rows[0]["ChargeTelNo"].ToString(); //담당자연락처

            //	번역	2	어린이편지번역	20030701000197024	김현수	2010-03-10	30	20100302175714181	Byung Kwon	Jin	서울 서대문구 연희동	344-137 4통 5반 진병관앞	750901-1******

            #region 시간계산

            int SumMin = 0;
            decimal iHour = 0;
            int iMin = 0;

            string sHour = string.Empty;
            string sMin = string.Empty;

            string sHourENG = string.Empty;
            string sMinENG = string.Empty;

            foreach (DataRow drRow in dsMateActivity.Tables[0].Rows)
            {
                SumMin = SumMin + int.Parse(drRow["mm"].ToString());
            }

            iHour = decimal.Truncate(SumMin / 60);
            iMin = SumMin % 60;

            if (iHour > 0)
            {
                sHour = iHour.ToString() + " 시간 ";
                sHourENG = iHour.ToString() + " hours ";
            }

            if (iMin > 0)
            {
                sMin = iMin.ToString() + " 분 ";
                sMinENG = iMin.ToString() + " minutes ";
            }

            sVolunteerTime = "총 " + sHour + sMin;
            sVolunteerTimeENG = sHourENG + sMinENG;

            #endregion

            #region 시작,끝 날짜.

            int iRowCount = dsMateActivity.Tables[0].Rows.Count;
            string sStartDate = string.Empty;
            string sEndDate = string.Empty;
            sStartDate = dsMateActivity.Tables[0].Rows[0]["WorkingEnd"].ToString();
            sEndDate = dsMateActivity.Tables[0].Rows[iRowCount - 1]["WorkingEnd"].ToString();
            sVolunteerTerm = sStartDate + " ~ " + sEndDate;

            string sStartDateENG = string.Empty;
            string sEndDateENG = string.Empty;
            sStartDateENG = DateTime.Parse(dsMateActivity.Tables[0].Rows[0]["WorkingEnd"].ToString()).ToString("MMM dd, yyyy", new System.Globalization.CultureInfo("en-US"));
            sEndDateENG = DateTime.Parse(dsMateActivity.Tables[0].Rows[iRowCount - 1]["WorkingEnd"].ToString()).ToString("MMM dd, yyyy", new System.Globalization.CultureInfo("en-US"));
            sVolunteerTermENG = sStartDateENG + " to " + sEndDateENG;
            #endregion

            #region 출력일
            DateTime dPaintDate = DateTime.Today;
            sPrintDate = dPaintDate.ToString("yyyy년 MM월 dd일"); //출력일
            sPrintDateENG = dPaintDate.ToString("MMM dd,yyyy", DateTimeFormatInfo.InvariantInfo); //출력일ENG 
            #endregion

            #region 출력
            Volunteer_Type myVolunteer = new Volunteer_Type();

            if (outputform1.Checked)
            {
                if (outputtype1.Checked)
                {
                    ////----- setData
                    myVolunteer.Type = "한글확인서";
                    myVolunteer.SponsorName = sSponsorName;
                    myVolunteer.SponsorAddress = sSponsorAddress;
                    myVolunteer.JuminID = sJuminID;
                    myVolunteer.BirthDate = sBirthDate;
                    myVolunteer.VolunteerTerm = sVolunteerTerm;
                    myVolunteer.VolunteerTime = sVolunteerTime;
                    myVolunteer.VolunteerContents = sVolunteerContents;
                    myVolunteer.PrintDate = sPrintDate;
                    myVolunteer.Charge = sCharge;
                    myVolunteer.ChargeTelNo = sChargeTelNo;
                }

                else
                {
                    DataSet ds = new DataSet();

                    DataTable dt = new DataTable();

                    dt = dsMateActivity.Tables[0].Clone();

                    ds.Tables.Add(dt);

                    int order = 1;

                    String WorkingType = dsMateActivity.Tables[0].Rows[0]["MateTransType"].ToString();
                    String WorkingEnd = String.Empty;

                    bool isChild = true;
                    bool isSponsor = true;

                    foreach (DataRow dr in dsMateActivity.Tables[0].Rows)
                    {
                        if (WorkingEnd != dr["WorkingEnd"].ToString() || (WorkingEnd == dr["WorkingEnd"].ToString() && WorkingType != dr["MateTransType"].ToString()))
                        {
                            if (WorkingEnd != dr["WorkingEnd"].ToString())
                            {
                                isChild = true;
                                isSponsor = true;
                            }

                            if (WorkingType != dr["MateTransType"].ToString())
                            {
                                if (dr["MateTransType"].ToString() == "CHILDLETTER" && !isChild)
                                    continue;

                                else if (dr["MateTransType"].ToString() == "SPONSORLETTER" && !isSponsor)
                                    continue;

                                else
                                    WorkingType = dr["MateTransType"].ToString();
                            }

                            if (WorkingType == "CHILDLETTER" && isChild)
                                isChild = false;

                            else if (WorkingType == "SPONSORLETTER" && isSponsor)
                                isSponsor = false;

                            int mm = 0;

                            WorkingEnd = dr["WorkingEnd"].ToString();

                            for (int j = 0; j < dsMateActivity.Tables[0].Rows.Count; j++)
                            {
                                if (WorkingEnd == dsMateActivity.Tables[0].Rows[j]["WorkingEnd"].ToString() && WorkingType == dsMateActivity.Tables[0].Rows[j]["MateTransType"].ToString())
                                    mm += Convert.ToInt32(dsMateActivity.Tables[0].Rows[j]["mm"]);
                            }

                            DataRow drNew = ds.Tables[0].NewRow();

                            drNew["OrderNo"] = order;
                            drNew["Expr1"] = dr["Expr1"];
                            drNew["MateTransKorType"] = dr["MateTransKorType"];
                            drNew["MateTransType"] = dr["MateTransType"];
                            drNew["TranslationID"] = dr["TranslationID"];
                            drNew["TranslationName"] = dr["TranslationName"];
                            drNew["WorkingEnd"] = dr["WorkingEnd"];

                            if ((mm / 60) == 0)
                                drNew["mm"] = mm.ToString() + "분";

                            else if ((mm % 60) != 0)
                                drNew["mm"] = (mm / 60).ToString() + "시간 " + (mm % 60).ToString() + "분";

                            else if ((mm % 60) == 0)
                                drNew["mm"] = (mm / 60).ToString() + "시간";

                            drNew["Charge"] = dr["Charge"];
                            drNew["ChargeTelNo"] = dr["ChargeTelNo"];

                            ds.Tables[0].Rows.Add(drNew);

                            order++;
                        }
                    }
                    myVolunteer.SponsorName = sSponsorName;
                    myVolunteer.BirthDate = sBirthDate;
                    myVolunteer.Type = "한글내역";
                    myVolunteer.dsMateActivity = ds;

                    ds.Dispose();
                    dsMateActivity.Dispose();
                }
            }

            else
            {
                if (outputtype1.Checked)
                {
                    ////----- setData
                    myVolunteer.Type = "영문확인서";
                    myVolunteer.SponsorNameENG = sSponsorNameENG;
                    myVolunteer.VolunteerTermENG = sVolunteerTermENG;
                    myVolunteer.VolunteerTimeENG = sVolunteerTimeENG;
                    myVolunteer.PrintDateENG = sPrintDateENG;
                }

                else
                {
                    DataSet ds = new DataSet();

                    DataTable dt = new DataTable();

                    dt = dsMateActivity.Tables[0].Clone();

                    ds.Tables.Add(dt);

                    int order = 1;

                    String WorkingType = dsMateActivity.Tables[0].Rows[0]["MateTransType"].ToString();
                    String WorkingEnd = String.Empty;

                    bool isChild = true;
                    bool isSponsor = true;

                    foreach (DataRow dr in dsMateActivity.Tables[0].Rows)
                    {
                        if (WorkingEnd != dr["WorkingEnd"].ToString() || (WorkingEnd == dr["WorkingEnd"].ToString() && WorkingType != dr["MateTransType"].ToString()))
                        {
                            if (WorkingEnd != dr["WorkingEnd"].ToString())
                            {
                                isChild = true;
                                isSponsor = true;
                            }

                            if (WorkingType != dr["MateTransType"].ToString())
                            {
                                if (dr["MateTransType"].ToString() == "CHILDLETTER" && !isChild)
                                    continue;

                                else if (dr["MateTransType"].ToString() == "SPONSORLETTER" && !isSponsor)
                                    continue;

                                else
                                    WorkingType = dr["MateTransType"].ToString();
                            }

                            if (WorkingType == "CHILDLETTER" && isChild)
                                isChild = false;

                            else if (WorkingType == "SPONSORLETTER" && isSponsor)
                                isSponsor = false;

                            int mm = 0;

                            WorkingEnd = dr["WorkingEnd"].ToString();

                            for (int j = 0; j < dsMateActivity.Tables[0].Rows.Count; j++)
                            {
                                if (WorkingEnd == dsMateActivity.Tables[0].Rows[j]["WorkingEnd"].ToString() && WorkingType == dsMateActivity.Tables[0].Rows[j]["MateTransType"].ToString())
                                    mm += Convert.ToInt32(dsMateActivity.Tables[0].Rows[j]["mm"]);
                            }

                            DataRow drNew = ds.Tables[0].NewRow();

                            drNew["OrderNo"] = order;
                            drNew["Expr1"] = dr["Expr1"];
                            drNew["MateTransKorType"] = dr["MateTransKorType"];

                            if (dr["MateTransType"].ToString() == "CHILDLETTER")
                                drNew["MateTransType"] = "Child Letter Translation (English→Korean)";

                            else if (dr["MateTransType"].ToString() == "SPONSORLETTER")
                                drNew["MateTransType"] = "Sponsor Letter Translation (Korean→English)";

                            else
                                drNew["MateTransType"] = "Offline Orientation";

                            drNew["TranslationID"] = dr["TranslationID"];
                            drNew["TranslationName"] = dr["TranslationName"];
                            drNew["WorkingEnd"] = dr["WorkingEnd"];

                            if ((mm / 60) == 0)
                                drNew["mm"] = mm.ToString() + "min";

                            else if ((mm % 60) != 0)
                                drNew["mm"] = (mm / 60).ToString() + "hour " + (mm % 60).ToString() + "min";

                            else if ((mm % 60) == 0)
                                drNew["mm"] = (mm / 60).ToString() + "hour";

                            drNew["Charge"] = dr["Charge"];
                            drNew["ChargeTelNo"] = dr["ChargeTelNo"];

                            ds.Tables[0].Rows.Add(drNew);

                            order++;
                        }
                    }

                    myVolunteer.Type = "영문내역";
                    myVolunteer.dsMateActivity = ds;

                    ds.Dispose();
                    dsMateActivity.Dispose();
                }
            }

            hdVolunteerContents.Value = myVolunteer.CreateReceipt();

            _WWW6Service.Dispose();

            #endregion
            //Response.Write(myVolunteer.CreateReceipt());
            //----- OpenPopup
            string sJs = string.Empty;
            sJs = JavaScript.HeaderScript.ToString();
            sJs += "printWindowOpen()";
            sJs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sJs);
        }

        catch (Exception ex)
        {
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);
            string sMsg = ex.Message;
            return;
        }
    }

    protected void btnBizPrint_Click(object sender, EventArgs e)
    {
        UserInfo sess = new UserInfo();
        WWWService.Service _wwwService = new WWWService.Service();

        //try
        //{
        if (!UserInfo.IsLogin)
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("로그인을 하신후 출력 가능 합니다.");
			//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/mate/mypage/ServiceBreakDown.aspx");
			sjs += JavaScript.GetPageMoveScript("/login.aspx");
			sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            _wwwService.Dispose();

            return;
        }

        DataSet dsMateActivity = null;
        string sFrom = Convert.ToDateTime(txtDateFrom.Text).ToString("yyyyMMdd");
        string sTo = Convert.ToDateTime(txtDateTo.Text).ToString("yyyyMMdd");

        Object[] objSql = new object[1] { "MATE_BreakDownList" };
        Object[] objParam = new object[4] { "MateID", "DateFrom", "DateTo", "DIVIS", };
        Object[] objValue = new object[4] { new UserInfo().UserId, sFrom, sTo, "BIZWEB" };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        dsMateActivity = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

        //dsMateActivity = _wwwService.MateActivity_List(sess.SponsorID, sFrom, sTo);

        if (dsMateActivity.Tables[0].Rows.Count <= 0)
        {
            string sjs = "";
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("검색된 결과가 없습니다.");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
            return;
        }

        // 영문 일 시 영문명 체크
        string strFullName = dsMateActivity.Tables[0].Rows[0]["FirstName"].ToString() + " " + dsMateActivity.Tables[0].Rows[0]["LastName"].ToString();
        if (outputform2.Checked)
        {
            if (string.IsNullOrEmpty(strFullName.Trim()))
            {
                string sjs = "";
                sjs = JavaScript.HeaderScript.ToString();
                sjs += "goUserInfo();";
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "goUserInfo", sjs.ToString());
                return;
            }
        }
        
        //Response.Write(dsMateActivity.Tables[0].Rows[0]["Address1"].ToString());
        //Response.End();
        //----- Define
        string sSponsorName = sess.UserName; //후원자명
        string sJuminID = dsMateActivity.Tables[0].Rows[0]["JuminID"].ToString(); //주민등록번호
        string sVolunteerTime = "총 " + dsMateActivity.Tables[0].Rows[0]["TotalHour"].ToString() + "시간 "; //총 봉사시간
        string sBirthDate = DateTime.Parse(dsMateActivity.Tables[0].Rows[0]["BirthDate"].ToString()).ToString("yyyy-MM-dd");

        if (dsMateActivity.Tables[0].Rows[0]["TotalMinute"].ToString() != "0")
            sVolunteerTime += dsMateActivity.Tables[0].Rows[0]["TotalMinute"].ToString() + "분"; //총 봉사시간

        string sVolunteerContents = dsMateActivity.Tables[0].Rows[0]["VolunteerContents"].ToString(); //봉사내용
        string sPrintDate = DateTime.Now.ToString("yyyy-MM-dd"); //출력일
        string sCharge = dsMateActivity.Tables[0].Rows[0]["Charge"].ToString(); //담당자

        Volunteer_Type myVolunteer = new Volunteer_Type();

        if (outputform3.Checked)
        {
            ////----- setData
            myVolunteer.Type = "사무메이트확인서";
            myVolunteer.SponsorName = sSponsorName;
            myVolunteer.JuminID = sJuminID;
            myVolunteer.BirthDate = sBirthDate;
            myVolunteer.VolunteerTime = sVolunteerTime;
            myVolunteer.VolunteerContents = sVolunteerContents;
            myVolunteer.PrintDate = sPrintDate;
            myVolunteer.Charge = sCharge;
        }

        else if (outputform4.Checked)
        {
            int Hour = 0;
            int Minute = 0;


            DateTime WorkingStart = new DateTime();
            DateTime WorkingEnd = new DateTime();

            int Cnt = 0;

            for (int i = 0; i < dsMateActivity.Tables[0].Rows.Count; i++)
            {
                Hour += Convert.ToInt32(dsMateActivity.Tables[0].Rows[i]["Hour"]);
                Minute += Convert.ToInt32(dsMateActivity.Tables[0].Rows[i]["Minute"]);

                if (Cnt == 0)
                {
                    WorkingStart = (DateTime)dsMateActivity.Tables[0].Rows[i]["WorkingStart"];
                    WorkingEnd = (DateTime)dsMateActivity.Tables[0].Rows[i]["WorkingEnd"];
                    Cnt++;
                }

                if (Cnt != 0 && WorkingStart > (DateTime)dsMateActivity.Tables[0].Rows[i]["WorkingStart"])
                    WorkingStart = (DateTime)dsMateActivity.Tables[0].Rows[i]["WorkingStart"];

                if (Cnt != 0 && WorkingEnd < (DateTime)dsMateActivity.Tables[0].Rows[i]["WorkingEnd"])
                    WorkingEnd = (DateTime)dsMateActivity.Tables[0].Rows[i]["WorkingEnd"];
            }

            if (Minute > 60)
                Hour += Minute / 60;

            string strTime = Hour + " hours " + Minute + " minutes";
            string strTerm = WorkingStart.ToString("MMM dd, yyyy", new System.Globalization.CultureInfo("en-US")) + " to " + WorkingEnd.ToString("MMM dd, yyyy", new System.Globalization.CultureInfo("en-US"));

            DateTime dPaintDate = DateTime.Today;
            String sPrintDateENG = dPaintDate.ToString("MMM dd,yyyy", DateTimeFormatInfo.InvariantInfo); //출력일ENG 

            myVolunteer.Type = "사무메이트영문확인서";
            myVolunteer.SponsorNameENG = strFullName;
            myVolunteer.VolunteerTimeENG = strTime;
            myVolunteer.VolunteerTermENG = strTerm;
            myVolunteer.PrintDateENG = sPrintDateENG;
        }

        myVolunteer.dsMateActivity = dsMateActivity;

        hdVolunteerContents.Value = myVolunteer.CreateReceipt();

        dsMateActivity.Dispose();
        _WWW6Service.Dispose();

        //Response.Write(myVolunteer.CreateReceipt());
        //----- OpenPopup
        string sJs = string.Empty;
        sJs = JavaScript.HeaderScript.ToString();
        sJs += "printWindowOpen()";
        sJs += JavaScript.FooterScript.ToString();
        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sJs);

        //}
        //catch (Exception ex)
        //{
        //    _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);
        //    string sMsg = ex.Message;
        //    return;
        //}
    }
}