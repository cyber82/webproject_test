﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data; 

public partial class Mate_Mypage_ApplyResult : System.Web.UI.Page
{
    string sjs;

    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!UserInfo.IsLogin)
            {
                //수정 2013-06-10
                //office.Visible = false;
                //translation.Visible = false;
                //eventmate.Visible = false;
                //profession.Visible = false;

                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("로그인을 하신후 신청결과 확인이 가능 합니다.");
				//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/mate/mypage/applyresult.aspx");
				sjs += JavaScript.GetPageMoveScript("/login.aspx");
				sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }

            //메이트 신청결과 조회
            GetMateState();
        }
    }

    #region 메이트 신청결과 조회
    private void GetMateState()
    {
        UserInfo user = new UserInfo();

        //WWWService.Service wwwService = new WWWService.Service();
        //DataSet dataJoin = wwwService.MateMyJoining(user.UserId);
        //DataSet data = wwwService.MateMyActive(user.UserId);

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        Object[] objSql = new object[1] { "MATE_MyInfoResult" };
        Object[] objParam = new object[2] { "DIVIS", "MateID" };
        Object[] objValue = new object[2] { "MYINFO", user.UserId };

        DataSet dataJoin = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

        if (dataJoin.Tables[0].Rows.Count == 0)
        {
            //주석처리 2013-06-10
            //office.Visible = false;
            //translation.Visible = false;
            //eventmate.Visible = false;
            //profession.Visible = false;

            //sjs = JavaScript.HeaderScript.ToString();
            //sjs += JavaScript.GetAlertScript("회원님은 메이트신청을 하시지 않았습니다.");
            //sjs += JavaScript.FooterScript.ToString();
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            //return;

            //추가 2013-06-10
            mateID0.Visible = true;
            mateID1.Visible = false;
            mateID2.Visible = false;
        }

        else
        {
            #region //주석처리 2013-06-10
            //if (data.Tables[0].Rows.Count == 0)
            //{
            //    office.Visible = false;
            //    translation.Visible = false;
            //    eventmate.Visible = false;
            //    profession.Visible = false;

            //    sjs = JavaScript.HeaderScript.ToString();
            //    sjs += JavaScript.GetAlertScript("회원님은 현재 활동중인 메이트가 아닙니다.");
            //    sjs += JavaScript.FooterScript.ToString();
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            //    return;
            //}
            //else if (data.Tables[0].Select ("currentUse = 'Y'").Length == 0)
            //{
            //    office.Visible = false;
            //    translation.Visible = false;
            //    eventmate.Visible = false;
            //    profession.Visible = false;

            //    sjs = JavaScript.HeaderScript.ToString();
            //    sjs += JavaScript.GetAlertScript("회원님은 현재 메이트 활동중이시지 않습니다.");
            //    sjs += JavaScript.FooterScript.ToString();
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            //    return;
            //}
            //else
            //{
            //    bool flag = false;
            //    if (data.Tables[0].Select("officemate = 'Y' and currentUse = 'Y'").Length > 0)
            //    {
            //        labName2.Text = user.UserName;
            //        labDate2.Text = Convert.ToDateTime(data.Tables[0].Rows[0]["approvaldate"]).ToString("yyyy-MM-dd");
            //        office.Visible = true;
            //        flag = true;
            //    }
            //    if (data.Tables[0].Select("translationmate = 'Y' and currentUse = 'Y'").Length > 0)                
            //    {
            //        labName1.Text = user.UserName;
            //        labDate1.Text = Convert.ToDateTime(data.Tables[0].Rows[0]["approvaldate"]).ToString("yyyy-MM-dd");
            //        translation.Visible = true;
            //        flag = true;
            //    }
            //    if (data.Tables[0].Select("professionmate = 'Y' and currentUse = 'Y'").Length > 0)
            //    {
            //        labName4.Text = user.UserName;
            //        labDate4.Text = Convert.ToDateTime(data.Tables[0].Rows[0]["approvaldate"]).ToString("yyyy-MM-dd");
            //        profession.Visible = true;
            //        flag = true;
            //    }

            //    if(!flag)
            //    {
            //        office.Visible = false;
            //        translation.Visible = false;
            //        eventmate.Visible = false;
            //        profession.Visible = false;

            //        sjs = JavaScript.HeaderScript.ToString();
            //        sjs += JavaScript.GetAlertScript("회원님의 메이트 신청을 심사중에 있습니다.");
            //        sjs += JavaScript.FooterScript.ToString();
            //        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            //    }
            //}
            #endregion

            //추가 2013-06-10
            mateID0.Visible = false;

            int row1 = 0;
            string mate1 = string.Empty;
            
            int row2 = 0;
            string mate2 = string.Empty;

            for (int i = 0; i < dataJoin.Tables[0].Rows.Count; i++)
            {
                if (dataJoin.Tables[0].Rows[i]["MateType"].ToString() == "번역")
                {
                    row1 = i;
                    mate1 = i.ToString();
                    break;
                }
            }

            for (int i = 0; i < dataJoin.Tables[0].Rows.Count; i++)
            {
                if (dataJoin.Tables[0].Rows[i]["MateType"].ToString() == "사무")
                {
                    row2 = i;
                    mate2 = i.ToString();
                    break;
                }
            }


            //번역
            if (mate1 != string.Empty || mate1 != "")
            {
                mateID1.Visible = true;

                DateTime appDate = Convert.ToDateTime(dataJoin.Tables[0].Rows[row1]["ApplicationDate"].ToString());
                string sUserName = dataJoin.Tables[0].Rows[row1]["SponsorName"].ToString();
                string sMateType = dataJoin.Tables[0].Rows[row1]["MateType"].ToString();
                string sAccFlag = dataJoin.Tables[0].Rows[row1]["AcceptFlag"].ToString();
                string sAppDate = appDate.ToShortDateString();

                lbUserName1.Text = sUserName;
                lbAppDate1.Text = sAppDate;
                lbMateType_A1.Text = sMateType + " 메이트";
                lbMateType_A2.Text = sMateType + " 메이트";

                switch (sAccFlag)
                {
                    case "신청":
                        lbResult1.Text = "신청 내역이 정상적으로 접수되었습니다.";
                        break;

                    case "승인":
                        lbResult1.Text = sMateType + " 메이트가 되신 것을 축하드립니다.";
                        break;

                    case "거절":
                        lbResult1.Text = "아쉽지만 다음 기회에 다시 지원해주시길 바랍니다.";
                        break;
                }
            }

            else
            {
                mateID1.Visible = false;
            }

            //사무
            if (mate2 != string.Empty || mate2 != "")
            {
                mateID2.Visible = true;

                DateTime appDate = Convert.ToDateTime(dataJoin.Tables[0].Rows[row2]["ApplicationDate"].ToString());
                string sUserName = dataJoin.Tables[0].Rows[row2]["SponsorName"].ToString();
                string sMateType = dataJoin.Tables[0].Rows[row2]["MateType"].ToString();
                string sAccFlag = dataJoin.Tables[0].Rows[row2]["AcceptFlag"].ToString();
                string sAppDate = appDate.ToShortDateString();

                lbUserName2.Text = sUserName;
                lbAppDate2.Text = sAppDate;
                lbMateType_B1.Text = sMateType + " 메이트";
                lbMateType_B2.Text = sMateType + " 메이트";

                switch (sAccFlag)
                {
                    case "신청":
                        lbResult2.Text = "신청 내역이 정상적으로 접수되었습니다.";
                        break;

                    case "승인":
                        lbResult2.Text = sMateType + " 메이트가 되신 것을 축하드립니다.";
                        break;

                    case "거절":
                        lbResult2.Text = "아쉽지만 다음 기회에 다시 지원해주시길 바랍니다.";
                        break;
                }
            }

            else
            {
                mateID2.Visible = false;
            }
        }

        dataJoin.Dispose();
        _WWW6Service.Dispose();
    }
    #endregion
}