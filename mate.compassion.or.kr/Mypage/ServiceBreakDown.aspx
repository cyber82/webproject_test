﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ServiceBreakDown.aspx.cs" Inherits="Mate_Mypage_ServiceBreakDown"  enableEventValidation="false" EnableViewStateMac="false" ValidateRequest ="false"%>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>

<%@ Register src="../Controls/MypageRight.ascx" tagname="MypageRight" tagprefix="uc4" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript" type="text/javascript">
        function Validate() {
            if (!document.getElementById("outputform1").checked && !document.getElementById("outputform2").checked) {
                alert('출력형식을 선택해 주세요.');
                return false;
            }
            if (!document.getElementById("outputtype1").checked && !document.getElementById("outputtype2").checked) {
                alert('출력유형을 선택해 주세요.');
                return false;
            }
            return true;
        }

        function printWindowOpen() {
            var wopt = 'menubar=no,toolbar=no,location=no,scrollbars=yes,status=yes,resizable=yes,width=750,height=860';
            winResult = window.open('about:blank', '', wopt);
            winResult.document.open('text/html', 'replace');
            winResult.document.write(document.getElementById("hdVolunteerContents").value);
            winResult.document.close();
            return false;
        }

        function CalendarExtenderShowing(dateControl, emptyEventArgs) {
            dateControl._popupDiv.style.zIndex = 999;
        }

        function ShowPrint() {


            //alert(document.getElementById('rboTran').checked);

            if (document.getElementById('rboTran').checked) {
                document.getElementById('BizPrnt').style.display = "none"
                document.getElementById('TranPrint').style.display = ""
            }
            else {
                document.getElementById('TranPrint').style.display = "none"
                document.getElementById('BizPrnt').style.display = ""
            }
        }

        function goUserInfo() {
            if (confirm("영문 이름이 없습니다. 개인정보를 수정해주세요.\n\r개인정보 수정 페이지로 이동 하시겠습니까?")) {

                var host = '';
                var loc = String(location.href);
                if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
                else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
                else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
                
                location.href = "http://" + host + "/my/account/";
            }
        }
</script> 
</head>
<body onload="ShowPrint()">
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        
<div id="container" class="mate">
	<!-- wrapper -->
	<div id="wrapper" class="mypage service-breakdown">
 
		<!-- sidebar -->
		<uc4:MypageRight ID="MypageRight1" runat="server" />
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>봉사확인서 출력</h3>
					<p>MATE활동을 하는 동안의 활동내역 조회와 출력이 가능한 공간입니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/mypage/ServiceBreakDown.aspx">마이페이지</a><strong>봉사확인서 출력</strong>
			</div>	
            
			<fieldset class="search-service-breakdown">
                <table>
                <tr>
                <td style="text-align:right; vertical-align:middle">
                <div>
                    <strong><span>메이트 타입</span></strong>&nbsp;
					<input type="radio" id="rboTran" runat="server" class="rad" name="search-form" checked/>
					<label for="output-form1" class="left-sel">번역</label>&nbsp;
					<input type="radio" id="rboBiz" runat="server" class="rad" name="search-form"/>
					<label for="output-form2">사무</label>
                </div>
                </td>
                <td>
                <legend class="hide">봉사내역확인 조회</legend>	
                <asp:TextBox ID="txtDateFrom" runat="server" CssClass="date"></asp:TextBox>					
                <a href="" class="btn btn-calendar" id="imgFrom"><span>날짜 선택</span></a>
				<strong class="bul-wave"><span>~</span></strong>
				<asp:TextBox ID="txtDateTo" runat="server" CssClass="date"></asp:TextBox>
				<a href="" class="btn btn-calendar" id="imgTo"><span>날짜 선택</span></a>
				<asp:Button ID="btnSearch" runat="server" Text=""  CssClass="btn btn-inquiry" 
                            onclick="btnSearch_Click"/>
                </td>
                </tr>
                </table>
                
				
			</fieldset>
            
			<div class="result-wrap">				
				<strong class="txt txt-breakdown"><span>봉사 내역 확인서</span></strong>
				<table class="confirmation" border="1">
					<thead>
						<th class="td-num"><img src="/image/mate/text/td-num.gif" alt="NO"></th>
						<th class="td-title"><img src="/image/mate/text/td-title.gif" alt="제목"></th>
						<th class="td-date"><img src="/image/mate/text/td-date.gif" alt="봉사일자"></th>
						<th class="td-hour"><img src="/image/mate/text/td-hour.gif" alt="봉사시간(분)"></th>
					</thead>
					<tbody>
                    <asp:Repeater ID="repData" runat="server">
                        <ItemTemplate>                            
						<tr>
							<td class="td-num"><%#DataBinder.Eval(Container.DataItem, "OrderNo")%></td>
							<td class="td-title"><%#DataBinder.Eval(Container.DataItem, "MateTransKorType")%></td>
							<td class="td-date"><%#DataBinder.Eval(Container.DataItem, "WorkingEnd", "{0:yyyy-MM-dd}")%></td>
							<td class="td-hour"><%#DataBinder.Eval(Container.DataItem, "mm")%></td>
						</tr>
                        </ItemTemplate>         
                    </asp:Repeater>
					</tbody>
				</table>
			</div>
            
			<div id="TranPrint" class="result-wrap">
				<div class="sort">
					<strong class="txt txt-form"><span>출력형식</span></strong>
					<input type="radio" id="outputform1" runat="server" class="rad" name="output-form"/>
					<label for="output-form1" class="left-sel">한글</label>
					<input type="radio" id="outputform2" runat="server" class="rad" name="output-form"/>
					<label for="output-form2">영어</label>
					<strong class="txt txt-type"><span>출력유형</span></strong>
					<input type="radio" id="outputtype1" runat="server" class="rad" name="output-type"/>
					<label for="output-type1" class="left-sel">확인서</label>
					<input type="radio" id="outputtype2" runat="server" class="rad" name="output-type"/>
					<label for="output-type2">상세내역</label>
                    <asp:Button ID="btnPrint" runat="server" Text=""  CssClass="btn-print2" 
                        onclick="btnPrint_Click" OnClientClick ="return Validate();" />
				</div>
			</div>		
            <div id="BizPrnt" class="result-wrap">
				<div class="sort">
               <label for="output-form1" class="left-sel"></label>
					<strong class="txt txt-form"><span>출력형식</span></strong>
					<input type="radio" id="outputform3" runat="server" class="rad" name="output-form"/>
					<label for="output-form1" class="left-sel">한글</label>
					<input type="radio" id="outputform4" runat="server" class="rad" name="output-form"/>
					<label for="output-form2">영어</label>
                    <asp:Button ID="btnBizPrint" runat="server" Text=""  CssClass="btn-print2" 
                        onclick="btnBizPrint_Click" />
				</div>
			</div>			
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    <asp:HiddenField ID="hdVolunteerContents" runat="server" />
    <asp:ScriptManager id="ScriptManager1" runat="server" enablescriptglobalization="True">
    </asp:ScriptManager>
    <cc1:CalendarExtender id="calDateFrom" runat="server" TargetControlID="txtDateFrom" PopupButtonID="imgFrom" OnClientShowing="CalendarExtenderShowing" Format="yyyy-MM-dd" FirstDayOfWeek="Sunday">
    </cc1:CalendarExtender>
     <cc2:CalendarExtender id="calDateTo" runat="server" TargetControlID="txtDateTo" PopupButtonID="imgTo" OnClientShowing="CalendarExtenderShowing" Format="yyyy-MM-dd" FirstDayOfWeek="Sunday">
    </cc2:CalendarExtender>  
    </form>
</body>
</html>
