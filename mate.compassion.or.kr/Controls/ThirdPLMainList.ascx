﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThirdPLMainList.ascx.cs" Inherits="Mate_Controls_ThirdPLMainList" %>
<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>

<script language="javascript">
    function ShowDetail(sThirdYear, sProjectNumber) {

        location.href = '/translation/TranslationDetailMain.aspx?ThirdYear=' + sThirdYear + '&ProjectNumber=' + sProjectNumber;
        
    }

    function GetLetterCheck(nListCnt, nEndCnt) {

        if (nListCnt > 0) {
            alert("이미 편지를 가져 왔습니다");
            return false;
        }
        else {
            if (nEndCnt > 0) {
                alert("이번주 번역이 완료 되었습니다");
                return false;
            }
            return true;
        }

    }

</script>
<table style="width:100%">
<tr>
<td>
<div style="text-align:left">
<asp:Label ID="lblTotalCount" runat="server" Text="Label"></asp:Label>
</div>
</td>
<td>
<div style="text-align:right" >
                <%--편지개수:&nbsp--%>
    <asp:Label ID="lblNullLetterCount" runat="server" Text="Label" Visible="false"></asp:Label>&nbsp
    <asp:TextBox ID="txtGetLetterCount" runat="server" CssClass="text" Width="20px" MaxLength="3"></asp:TextBox>&nbsp통&nbsp
    <asp:ImageButton ID="imgBtnGetLetter" runat="server" 
                    onclick="imgBtnGetLetter_Click" ToolTip="편지를 한 통씩 가져옵니다." ImageUrl="~/image/imate/btn/btn_email.jpg" />&nbsp
    <asp:ImageButton ID="imgBtnSearch" runat="server" Visible="false"/>
</div>
</td>
</tr>
</table>
</br>
<table class="mate-list">
	<thead>
		<tr>
			<th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    편지ID
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    FO
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    프로젝트
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    프로젝트명칭
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    작성자
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    작성자명
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    번역상태
            </th>
		</tr>
	</thead>
	<tbody>
        <asp:Repeater ID="repData" runat="server">
            <ItemTemplate>
		    <tr style="cursor:pointer;" onclick="ShowDetail('<%#DataBinder.Eval(Container.DataItem, "ThirdYear")%>','<%#DataBinder.Eval(Container.DataItem, "ProjectNumber")%>'); return false;" >
			    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "LetterID")%></center></td>
			    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "FieldOffice")%></center></td>
			    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ProjectNumber")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ProjectName")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ActualAuthorRole")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "AuthorName")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "TranslateStatusName")%></center></td>
		    </tr>      
            </ItemTemplate> 
        </asp:Repeater>		
	</tbody>
</table>
<!-- paginator -->
<table style="width:100%">
<div class="paginator">
    <taeyo:PagingHelper ID="PagingHelper1" runat="server" OnOnPageIndexChanged="PagingHelper1_OnPageIndexChanged"
        CurrnetNumberColor="Silver" ForeColor="#404040"></taeyo:PagingHelper>
</div>
</table>