﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThirdPLCompleteList.ascx.cs" Inherits="Mate_Controls_ThirdPLCompleteList" %>
<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<link type="text/css" href="../../common/js/inc/js/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../common/js/inc/js/ui/demos.css" rel="stylesheet" />
<style type="text/css">
    .sbox
    {
        border: 1px solid #bbb;
    }
</style>
<script type="text/javascript" src="../../common/js/inc/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.core.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.datepicker-ko.js"></script>
<script type="text/javascript">
<!--
    $(function () {
        $('#start_date').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    $(function () {
        $('#end_date').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

     function ShowDetail(sThirdYear, sProjectNumber) {

        location.href = '/translation/TranslationDetailMain.aspx?ThirdYear=' + sThirdYear + '&ProjectNumber=' + sProjectNumber;

    }

    function trColor(sw, idx) {
        var id = document.getElementById("dataTr_" + idx);

        if (sw == "1") {
            id.style.backgroundColor = "#ddd";
        }
        else if (sw == "0") {
            id.style.backgroundColor = "#fff";
        }
    }

    function SetDate(sSDate, sEDate) {


        document.getElementById("start_date").value = sSDate;
        document.getElementById("end_date").value = sEDate;

    }

    
//-->
</script>
<table>
<tr>
<td>
<div style="text-align: left">
<asp:Label ID="lblTotalCount" runat="server" Text="Label"></asp:Label>
</div>
</td>
<td>
<div style="text-align: right">
    <%--편지개수:&nbsp--%>
    기간 : &nbsp
    <input type="text" id="start_date" name="start_date" size="10" maxlength="10" value=""
        style="ime-mode: disabled; height: 20px; line-height: 20px;" class="sbox" onkeydown="javascript:OnlyNumber();" />
    &nbsp; ~ &nbsp;
    <input type="text" id="end_date" name="end_date" size="10" maxlength="10" value=""
        style="ime-mode: disabled; height: 20px; line-height: 20px;" class="sbox" onkeydown="javascript:OnlyNumber();" />
    &nbsp;&nbsp;
    <asp:ImageButton ID="imgBtnSearch" runat="server" OnClick="imgBtnSearch_Click" ImageUrl="~/image/imate/btn/btn_search.jpg" ToolTip="기간 설정 후, 조회합니다." />
</div>
</td>
</tr>
</table>
</br>
<table class="mate-list">
    <thead>
        <tr>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    편지ID
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    FO
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    프로젝트
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    프로젝트명칭
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    작성자
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    작성자명
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    번역상태
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    번역완료일
            </th>
        </tr>
    </thead>
    <tbody>
        <asp:Repeater ID="repData" runat="server">
            <ItemTemplate>
               <tr style="cursor:pointer;" onclick="ShowDetail('<%#DataBinder.Eval(Container.DataItem, "ThirdYear")%>','<%#DataBinder.Eval(Container.DataItem, "ProjectNumber")%>'); return false;" >
			    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "LetterID")%></center></td>
			    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "FieldOffice")%></center></td>
			    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ProjectNumber")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ProjectName")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ActualAuthorRole")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "AuthorName")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "TranslateStatusName")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "TranslateDate", "{0:yyyy-MM-dd}")%></center></td>
		    
              </tr>
            </ItemTemplate>
        </asp:Repeater>
    </tbody>
</table>
<!-- paginator -->
<table style="width:100%">
<div class="paginator">
    <taeyo:PagingHelper ID="PagingHelper1" runat="server" OnOnPageIndexChanged="PagingHelper1_OnPageIndexChanged"
        CurrnetNumberColor="Silver" ForeColor="#404040"></taeyo:PagingHelper>
</div>
</table>
