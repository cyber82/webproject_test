﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TranslationLetterSearchList.ascx.cs" Inherits="Mate_Controls_TranslationLetterSearchList" %>
<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>

<link type="text/css" href="../../common/js/inc/js/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../common/js/inc/js/ui/demos.css" rel="stylesheet" />
<style type="text/css">
    .sbox
    {
        border: 1px solid #bbb;
    }
</style>
<script type="text/javascript" src="../../common/js/inc/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.core.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.datepicker-ko.js"></script>

<script type="text/javascript">
<!--

    function ShowDetail(sCorrID, sPackageID, sPageCount, sSearchType) {
        

//        var drpFilterType = document.getElementById("ddlSearchType");
//        var selectedFilterType = drpFilterType.options[drpFilterType.selectedIndex].value;
                //alert(document.getElementById("ddlSearchType").selectedIndex);
                //alert(document.getElementById("ddlSearchType").options[document.getElementById("ddlSearchType").selectedIndex].value)
        sSearchKind = document.getElementById("txtSearchKind").value
        sSearchText = document.getElementById("txtSearch").value

        //수정 2013-08-28
        //location.href = '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount + '&TabSelectValue=2&SearchKind=' + sSearchKind + '&SearchText=' + sSearchText;
        /*
        var f = top.document.form1;

        f.action = '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount + '&TabSelectValue=2&SearchKind=' + sSearchKind + '&SearchText=' + sSearchText;
        f.method = "post";
        f.target = "_top";
        f.submit();
        */

        var f = $("#form1");
        //f.attr("action", '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount + '&TabSelectValue=2&SearchKind=' + sSearchKind + '&SearchText=' + sSearchText);
        f.attr("action", '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount + '&SearchKind=' + sSearchKind + '&SearchText=' + sSearchText);
        f.attr("method", "post");
        f.attr("target", "_top");
        f.submit();
    }


    function SetTextValue(sSearch, sSearchType, sSearchKind) {

        //수정 2013-08-28
        if (sSearch != "") {
            document.getElementById("txtSearch").value = sSearch;
        }

        document.getElementById("txtSearchKind").value = sSearchKind;

    }

    function handlerNum(obj) {
        
        //숫자만 입력 받게끔 하는 함수.
        e = window.event; //윈도우의 event를 잡는것입니다.

        //입력 허용 키
        if ((e.keyCode >= 48 && e.keyCode <= 57) ||   //숫자열 0 ~ 9 : 48 ~ 57
    (e.keyCode >= 96 && e.keyCode <= 105) ||   //키패드 0 ~ 9 : 96 ~ 105
     e.keyCode == 8 ||    //BackSpace
     e.keyCode == 46 ||    //Delete
        //e.keyCode == 110 ||    //소수점(.) : 문자키배열
        //e.keyCode == 190 ||    //소수점(.) : 키패드
           e.keyCode == 37 ||  //좌 화살표
           e.keyCode == 39 ||  //우 화살표
           e.keyCode == 35 ||  //End 키
           e.keyCode == 36 ||  //Home 키
           e.keyCode == 9 ||  //Tab 키
           e.keyCode == 17 || //Ctrl 키
           e.keyCode == 86 || //v 키
           e.keyCode == 67  //c 키
       ) {
            if (e.keyCode == 48 || e.keyCode == 96) { //0을 눌렀을경우
                if (obj.value == "" || obj.value == '0') //아무것도 없거나 현재 값이 0일 경우에서 0을 눌렀을경우
                    e.returnValue = false; //-->입력되지않는다.
                else //다른숫자뒤에오는 0은
                    return; //-->입력시킨다.
            }
            else //0이 아닌숫자
                return; //-->입력시킨다.
        }
        else //숫자가 아니면 넣을수 없다.
        {
            e.returnValue = false;
        }
    }

    function handlerEnter() {
        e = window.event;
        if (e.keyCode == 13) {            
        }
    }

    //추가 2013-09-03
    function GetTextValue() {
        document.getElementById("txtSearch").value = document.getElementById("<%=txtSearchText.ClientID %>").value;
        document.getElementById("txtSearchKind").value = document.getElementById("<%=ddlSearchType.ClientID %>").value;
        document.getElementById("txtButton").value = "Y";

        //var f = top.document.form1;
        //f.action = "/translation/TranslationLetterSearch.aspx";
        //f.method = "post";
        //f.target = "_self";
        //f.submit();
    }

    //추가 2013-09-03
    function SetButtonEmpty() {
        document.getElementById("txtButton").value = "";
    }
//-->
</script>

<input type="hidden" name="TabSelectValue" value="2" />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <div style="text-align: left">
                <asp:Label ID="lblTotalCount" runat="server" Text="Label"></asp:Label>
            </div>
        </td>
        <td>
            <div style="text-align: right">
                <%--편지개수:&nbsp--%>
                &nbsp조회 : 
                <input type="hidden" id="txtButton" />
                <input type="hidden" id="txtSearchKind" />
                <input type="hidden" id="txtSearch" name="txtSearch" size="11" maxlength="11" value=""
                    style="ime-mode: disabled; height: 20px; line-height: 20px;" class="sbox" onkeydown="handlerEnter()" />
               

                <asp:DropDownList ID="ddlSearchType" runat="server">
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchText" runat="server" class="sbox" height="20" width="90" MaxLength="11"></asp:TextBox>
                <asp:ImageButton ID="imgBtnSearch" runat="server" OnClick="imgBtnSearch_Click" OnClientClick="return GetTextValue();" 
                ImageUrl="~/image/imate/btn/btn_search.jpg" ToolTip="편지ID 또는 어린이키로 편지를 조회합니다." />
            </div>
        </td>
    </tr>
</table>
<br/>

<table border="0" cellpadding="0" cellspacing="0" class="mate-list">
    <thead>
        <tr>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                스캔일자
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                편지ID
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                패키지ID
            </th>
            
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                편지타입
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                어린이키
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                어린이명
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                후원자키
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                후원자명
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                쪽수
            </th>
        </tr>
    </thead>
    <tbody>
        <asp:Repeater ID="repData" runat="server">
            <ItemTemplate>
                <tr>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ScanningDate", "{0:yyyy-MM-dd}")%></center>
                    </td>
                     <td style='border: 1px solid #CCCCCC; height: 27px; cursor: pointer;' bgcolor="#C6DFFF" onclick="ShowDetail('<%#DataBinder.Eval(Container.DataItem, "CorrID")%>','<%#DataBinder.Eval(Container.DataItem, "PackageID")%>','<%#DataBinder.Eval(Container.DataItem, "CorrPage")%>'); return false;">
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "CorrID")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "PackageID")%></center>
                    </td>
                   
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "CorrType")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ChildKey")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ChildName")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ConID")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "SponsorName")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "CorrPage")%></center>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </tbody>
</table>

<% //추가 2013-08-28 %>
<input type="hidden" id="hdIdxPage" name="hdIdxPage" runat="server" />

<!-- paginator -->
<div class="paginator" id="paginator" runat="server">
    <taeyo:PagingHelper ID="PagingHelper1" runat="server" OnOnPageIndexChanged="PagingHelper1_OnPageIndexChanged"
        CurrnetNumberColor="Silver" ForeColor="#404040"></taeyo:PagingHelper>
</div>
<!-- // paginator -->