﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MateTop.ascx.cs" Inherits="Controls_MateTop" %>
<!-- header -->
<script language="javascript">

    var time;

    function MenuShowOver(sDiv) {
        clearTimeout(time)

        if (sDiv == "1") {
            document.getElementById('divIntro').style.display = "";
        }

        else if (sDiv == "2") {
            document.getElementById('divNotice').style.display = "";
        }

        else if (sDiv == "3") {
            document.getElementById('divOurStory').style.display = "";
        }

        else if (sDiv == "4") {
            document.getElementById('divIMate').style.display = "";
        }

        else {
            document.getElementById('divIMate').style.display = "none";
        }     
    }
    function MenuShowOut(sDiv) {
        if (sDiv == "1") {
            time = setTimeout("document.getElementById('divIntro').style.display = 'none'", 1000)
        }

        else if (sDiv == "2") {
            time = setTimeout("document.getElementById('divNotice').style.display = 'none'", 1000)
        }

        else if (sDiv == "3") {
            time = setTimeout("document.getElementById('divOurStory').style.display = 'none'", 1000)
        }

        else if (sDiv == "4") {
            time = setTimeout("document.getElementById('divIMate').style.display = 'none'", 1000)
        }

        else {
            time = setTimeout("document.getElementById('divIMate').style.display = 'none'", 1000)
        }
    }
</script>
<style>

</style>



    <header class="header">
		<div class="wrap" style="position:absolute">
			<div class="header_top">
				<div class="w980">
					<ul>
						<asp:PlaceHolder runat="server" ID="ph_before_login" Visible="false">
                            <li><a runat="server" id="link_compassion_not_login" target="_top">한국컴패션</a></li>
						    <li><a href="/login.aspx">로그인</a></li>
						    <li><a href="/join.aspx">회원가입</a></li>
						    <li><a runat="server" id="link_recruit_not_login" target="_top">인재채용</a></li>
                            <li><a id="link_faq1" runat="server" target="_top">FAQ</a></li>
						</asp:PlaceHolder>

						<asp:PlaceHolder runat="server" ID="ph_after_login" Visible="false">
                            <li><a runat="server" id="link_compassion_login" target="_top">한국컴패션</a></li>
						    <li><a href="/logout.aspx"><!--<asp:Literal ID="labName2" runat="server"></asp:Literal>님&nbsp; -->로그아웃</a></li>
						    <li><a runat="server" id="link_my" target="_top">마이컴패션</a></li>
						    <li><a runat="server" id="link_recruit_login" target="_top">인재채용</a></li>
                            <li><a id="link_faq2" runat="server" target="_top">FAQ</a></li>
						    <li class="post"><a runat="server" id="link_letter" target="_top">편지함<span runat="server" id="letter_count"></span></a></li>
						</asp:PlaceHolder>
                        <script type="text/javascript">
                            $(function () {
                                $(".header_top .store").mouseenter(function () {
                                    $(this).find(".storeSub").show();
                                }).mouseleave(function () {
                                    $(this).find(".storeSub").hide();
                                });
                            })
                        </script>
                        <li class="store">
                            <a runat="server" id="link_store" target="_top">스토어</a>
                            <ul class="storeSub">
                                <li><a runat="server" id="link_store2" target="_top">컴패션 스토어</a></li>
                                <li><a runat="server" id="link_cart" target="_top">장바구니</a></li>
                                <li><a runat="server" id="link_review" target="_top">후기/문의</a></li>
                                <li><a runat="server" id="link_notice" target="_top">공지사항</a></li>
                            </ul>
                        </li>
					</ul>
				</div>
			</div>
		</div>
    </header>
    <!--// header -->

<div id="header">
	<div class="wrap">
		<h1 class="logo1"><a href="/default.aspx">한국컴패션 - MATE</a></h1>		
        <div class="gnb">
			<ul class="type2">
				<li id="gnb1">
					<a href="/Intro/MateIntro.aspx" class="l-introduce" onmouseover="MenuShowOver('1');" onmouseout="MenuShowOut('1');">MATE 소개</a>
					<div id="divIntro" onmouseover="MenuShowOver('1');" onmouseout="MenuShowOut('1');">
                    <script language="javascript">
                        document.all.divIntro.style.display = 'none'
                    </script>
						<ul>
							<li><a href="/Intro/MateIntro.aspx" class="dep1-1">MATE란?</a></li>
							<li><a href="/Intro/TransIntro.aspx" class="dep1-2">MATE 신청</a></li>
						</ul>
					</div>
				</li>
				<li id="gnb2">
					<a href="/community/noticelist.aspx" class="l-announce" onmouseover="MenuShowOver('2');" onmouseout="MenuShowOut('2');">MATE 알림</a>
					<div id="divNotice" onmouseover="MenuShowOver('2');" onmouseout="MenuShowOut('2');" >
                    <script language="javascript">
                        document.all.divNotice.style.display = 'none'
                    </script>
						<ul>
							<li><a href="/community/noticelist.aspx" class="dep2-1">공지사항</a></li>
							<li><a href="/community/monthlymatelist.aspx" class="dep2-2">이 달의 메이트</a></li>
							<li><a href="/community/faq.aspx" class="dep2-3" >FAQ</a></li>
                            <!--
                            <li><a href="/community/ChristmasLoveLetterList.aspx" class="dep2-4" style="width:300px;">러브레터 콘테스트 </a></li>
                            -->
						</ul>
					</div>
				</li>
				<li id="gnb3">
					<a href="/nanum/TogetherStoryList.aspx" class="l-sharing" onmouseover="MenuShowOver('3');" onmouseout="MenuShowOut('3');">MATE 나눔</a>
					<div id="divOurStory" onmouseover="MenuShowOver('3');" onmouseout="MenuShowOut('3');">
                    	<ul>
                            <li><a href="/nanum/TogetherStoryList.aspx" class="dep3-7">함께하는 이야기</a></li>
                            <li><a href="/nanum/OurStoryList.aspx" class="dep3-1">우리들의 이야기</a></li>
							<li><a href="/nanum/TodayWordList.aspx" class="dep3-2">오늘의 말씀</a></li>
							<li><a href="/nanum/MasterChallengeList.aspx" class="dep3-3">번달이 도전기</a></li>
							<li><a href="/nanum/TranslationQnAList.aspx" class="dep3-4">번역 Q &AMP; A</a></li>
							<li><a href="/nanum/OnlineList.aspx" class="dep3-5">온라인상담</a></li>
                            <li><a href="/nanum/BenefitDefault.aspx" class="dep3-6">수혜국 편지 용어</a></li>
						</ul>
					</div>
				</li>
                <li id="gnb4">
					<%=sgHtml%>
				</li>
                <li id="gnb3">
					<a href="/nanum/TogetherStoryList.aspx" class="l-mateplus" onmouseover="MenuShowOver('3');" onmouseout="MenuShowOut('3');"> MATE+ </a>
                </li>
                
			</ul>
            <ul>

            </ul>
		</div>
		<%--<div class="area1">
			<ul class="related1" id="Logout" runat ="server">
				<li><a href="/membership/login.aspx" class="l-login">로그인</a></li>
				<li><a href="/membership/registeradult.aspx" class="l-join">회원가입</a></li>
				<li><a href="/mate/mypage/ServiceBreakDown.aspx" class="l-mypage">마이페이지</a></li>
				<!--<li><a href="#" class="l-event">MATE이벤트</a></li>-->
                <% //수정 2013-08-16 %>
				<li><a href="/mate/mypage/programDown.aspx" class="l-translation">번역프로그램</a></li>
			</ul>
            <ul class="related1" id="Login" runat ="server">
				<li class="greeting"><strong><asp:Label ID="labUserName" runat="server" Text=""></asp:Label></strong> <strong class="l-greeting"><span>님 환영합니다</span></strong></li>
				<li><a href="/Logoff.aspx?mate=on" class="l-logout">로그아웃</a></li>
				<!--<li><a href="" class="l-join">회원가입</a></li>-->
				<li><a href="/mate/mypage/ServiceBreakDown.aspx" class="l-mypage">마이페이지</a></li>
				<% //수정 2013-08-16 %>
                <%--<li><a href="/mate/mypage/programDown.aspx" class="l-translation">번역프로그램</a></li>
			</ul>
            <!--
			<fieldset class="gnb-search">
				<legend class="hide">검색</legend>				
				<input type="text" class="text" /><input type="submit" class="btn btn-search1" value="" title="검색" />
			</fieldset>
		</div>--%>
	</div>
</div>
<!-- // header -->
