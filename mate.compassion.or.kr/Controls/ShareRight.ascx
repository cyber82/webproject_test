﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShareRight.ascx.cs" Inherits="Controls_ShareRight" %>

<%@ Register src="/Controls/RightBanner.ascx" tagname="Banner" tagprefix="bc1" %>

<div id="sidebar" class="sec-get-involved">
	<div class="tit">적극적 참여 GET INVOLVED</div>
	<ul id="lnb" class="lnb">		
        <li id="sub1"><a href="/Share/SponsorEnterprise.aspx" class="dep1"><span>함께 하는 기업/단체</span></a>			
			<ul id="subEl1" runat ="server" enableviewstate = "true" >
				<li id="sub1_1"><a href="/Share/SponsorEnterprise.aspx" class="dep1-1"><span>함께하는 기업</span></a></li>
				<li id="sub1_2"><a href="/Share/SponsorGroup.aspx" class="dep1-2"><span>함께하는 단체</span></a></li>
				<li id="sub1_3"><a href="/Share/Supporters.aspx" class="dep1-3"><span>기업 및 단체후원</span></a></li>
				<!--<li id="sub1_4"><a href="" class="dep1-4"><span>제휴문의</span></a></li>-->
			</ul>
		</li>
		<li id="sub2">
            <% //수정 2013-01-07, 수정 2013-06-28 링크수정 %>
			<a href="/Share/CompassionSunday.aspx" class="dep2"><span>교회협력</span></a>
			<ul id="subEl2" runat ="server" enableviewstate = "true" >
				<li id="sub2_1"><a href="/Share/CompassionSunday.aspx" class="dep2-1"><span>컴패션선데이</span></a></li>
				<li id="sub2_2"><a href="/Share/CompassionDay.aspx" class="dep2-2"><span>컴패션데이</span></a></li>
				<li id="sub2_3"><a href="/Share/C2C.aspx" class="dep2-3"><span>C2C+</span></a></li>
				<li id="sub2_4"><a href="/Share/ChurchVisionTrip.aspx" class="dep2-4"><span>교회비전트립</span></a></li>
			</ul>
		</li>
		<li id="sub3"><a href="/Share/VocAct.aspx" class="dep3"><span>일반인홍보대사 VOC</span></a></li>
		<li id="sub4"><a href="/Share/YVocAct.aspx" class="dep4_etc"><span>청소년홍보대사 VOC</span></a></li>
		<li id="sub5"><a href="/Share/Foc.aspx" class="dep5"><span>FOC</span></a></li>
		<li id="sub6"><a href="/Share/Mate.aspx" class="dep6"><span>메이트</span></a></li>
		<li id="sub7">
			<a href="/Share/VisionTripAbout.aspx" class="dep7"><span>비전트립</span></a>
			<ul id="subEl7" runat ="server" enableviewstate = "true" >
				<li id="sub7_1"><a href="/Share/VisionTripAbout.aspx" class="dep7-1"><span>비전트립 소개</span></a></li>
				<li id="sub7_2"><a href="/Share/VisionTripGuide.aspx" class="dep7-2"><span>비전트립 안내</span></a></li>
				<li id="sub7_3"><a href="/Share/VisionTripApply.aspx" class="dep7-3"><span>비전트립 신청</span></a></li>
				<li id="sub7_4"><a href="/Share/VisionTripQna.aspx" class="dep7-4"><span>비전트립 Q&amp;A</span></a></li>
			</ul>
		</li>
        <li id="sub11">
			<a href="/Share/PersonalVisitAbout.aspx" class="dep11"><span>개인방문</span></a>
			<ul id="subEl11" runat="server" enableviewstate="true">
				<li id="sub11_1"><a href="/Share/PersonalVisitAbout.aspx" class="dep11-1"><span>개인방문 소개</span></a></li>
			    <li id="sub11_2"><a href="/Share/PersonalVisitGuide.aspx" class="dep11-2"><span>개인방문 안내</span></a></li>
				<li id="sub11_3"><a href="/Share/PersonalVisitApply.aspx" class="dep11-3"><span>개인방문 신청</span></a></li>
			    <li id="sub11_4"><a href="/Share/PersonalVisitQna.aspx" class="dep11-4"><span>개인방문 Q&amp;A</span></a></li>
			</ul>
		</li>
        <li id="sub8"><a href="/Share/CompassionBand.aspx" class="dep8"><span>컴패션 밴드</span></a></li>		
        <li id="sub9"><a href="/Share/StarProject.aspx" class="dep9"><span>나눔별 프로젝트</span></a></li>
        <%--<li id="sub12"><a href="/Share/Legacy.aspx" class="dep12"><span>컴패션 유산기부</span></a></li>--%>
        <li id="sub12">
			<a href="/Birthday" class="dep12"><span>틀별한 나눔</span></a>
			<ul id="subEl12" runat="server" enableviewstate="true">
				<li id="sub12_1"><a href="/Birthday" class="dep12-1"><span>첫 생일 첫 나눔</span></a></li>
			    <li id="sub12_2"><a href="/Wedding" class="dep12-2"><span>결혼 첫 나눔</span></a></li>
				<li id="sub12_3"><a href="/Share/Legacy.aspx" class="dep12-3"><span>믿음의 유산</span></a></li>
			</ul>
		</li>
        <li id="sub10"><a href="/Share/CpsPicture.aspx" class="dep10"><span>컴패션 자료실</span></a></li>
	</ul>
	<div class="div">
		<dl>
			<dt class="txt tit-vision-trip">컴패션 비전트립</dt> <% //수정 20120327 %>
			<dd>
				<ul class="list-lnb">
					<asp:Repeater ID="repLoved" runat="server">
                    <ItemTemplate>
					<li>
						<a href="http://www.iamcompassion.or.kr/story/view.aspx?Idx=<%#DataBinder.Eval(Container.DataItem, "idx")%>&boardid=<%#DataBinder.Eval(Container.DataItem, "board_id")%>&Mode=read" target="_blank">
							<img src="http://www.iamcompassion.or.kr/ssBoard/thumbnail/<%#DataBinder.Eval(Container.DataItem, "idx")%>.jpg" width="54" height="54" class="thumb" alt="아이들을 부모님만큼 사랑해줄 거예요" />
							<strong><%#WebCommon.GetNamePadding (DataBinder.Eval(Container.DataItem, "title"),40)%></strong>
							<!--<span>저는 어린이들이 사는...</span>-->
						</a>
					</li>
                    </ItemTemplate>
                    </asp:Repeater>					
				</ul>
			</dd>
		</dl>
		<a href="http://www.iamcompassion.or.kr/Story/Default.aspx" class="btn btn-more" target="_blank"><span>more</span></a>
	</div>
	<div class="div">
        <p class="txt txt-support-children"><span>이 어린이를 후원해 주세요.</span></p>
		<div id="support-wrap3" class="support-wrap">
			<ul id="support-inner3">
                <% //수정 20120425 어린이 테이블 막기 %>
				<%--<asp:Repeater ID="repChild" runat="server" onitemcommand="repChild_ItemCommand">
                    <ItemTemplate>
				    <li>
					    <img src="<%#GetImgPath(DataBinder.Eval(Container.DataItem,"ChildKey"))%>" width="98" height="98" class="thumb" alt="까리나" />
					    <dl>
						    <dt><strong><%#DataBinder.Eval(Container.DataItem, "ChildName")%></strong></dt>
						    <dd class="name"><strong><%#DataBinder.Eval(Container.DataItem, "ChildCountry")%>/<%#DataBinder.Eval(Container.DataItem, "ChildAge")%>살</strong></dd>
						    <dd class="btn">
                                <asp:Button ID="btnSupport" CssClass="btn btn-relationship3" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "ChildMasterID")%>' CommandName ="Add" runat="server" Text="" />							    
						    </dd>
					    </dl>
				    </li>
                    </ItemTemplate>
                </asp:Repeater>--%>

                <% //수정 20120425 배너 열기 %>
                <div style="text-align:center;">
			        <a href="../Sponsor/CDSPList.aspx"><img src="/image/btn/btn-relationship3.png" board="0" alt="결연하러 가기" /></a>
		        </div>
			</ul>
		</div>
        <% //수정 20120425 어린이 목록의 이전 다음 막기 %>
		<%--<div class="btn-move2">
			<a href="#support-wrap3" id="btn-next1" class="pre">이전</a>
			<a href="#support-wrap3" id="btn-prev1" class="next">다음</a>
		</div>--%>
		<script type="text/javascript">
			//<![CDATA[
			var objRolling = new ImageRotation();
			objRolling.objName = 'objRolling';
			objRolling.scrollDirection = 'direction'; // direction: 좌-우, 상-하. indirection: 우-좌, 하-상.
			objRolling.setScrollType('horizontal'); 	// 'horizontal', 'vertical', 'none';;
			objRolling.autoScroll = "none"; 			// 'none' 자동 동작 없습
			objRolling.scrollGap = 1000; 			//스크롤 시간 (1초: 1000)
			objRolling.listNum = 1; 					// 보여줄 li갯수
			objRolling.wrapId = "support-wrap3"; 	//ul을 감싸고있는 box id
			objRolling.listId = "support-inner3"; // ul의 id
			objRolling.btnNext = "btn-next1"; 		//next 버튼 id
			objRolling.btnPrev = "btn-prev1"; 		//prev 버튼 id
			objRolling.initialize();
			//]]>
		</script>
	</div>

	<ul class="banner1">
		<% //수정 2013-05-28 %>
        <bc1:Banner ID="banner1" runat="server" />
		
        <% //주석처리 2013-05-28 %>
        <%--<li><a href="/AboutUs/noticeview.aspx?iTableIndex=1001&iNoIndex=318&iBoardIdx=67376&ref=47200&re_step=0&pageIdx=0&searchOption=&searchTxt="><img src="/image/banner/charity-navigator.png" width="240" height="100" alt="" /></a></li>
		<li><a href="/Intro/MateIntro.aspx"><img src="/image/banner/mate.png" width="240" height="90" alt="" /></a></li>
        <li><a href="http://www.hanscake.co.kr/2011/story/responsibility.php" target="_blank"><img src="/image/banner/hanscake.png" width="240" height="92" alt="" /></a></li>
		<li><a href="/Share/VisionTripApply.aspx"><img src="/image/banner/vision-trip2012.png" width="240" height="90" alt="" /></a></li>--%>
	</ul>
</div>