﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data; 

public partial class Mate_Controls_BoardWrite2 : System.Web.UI.UserControl
{
    int _tableIdx;

    protected void Page_Load(object sender, EventArgs e)
    {
        _tableIdx = Convert.ToInt32(((HiddenField)Page.FindControl("hidTableIdx")).Value);

        if (!IsPostBack)
        {
            if (!UserInfo.IsLogin)
            {
                string pageName = Context.Handler.GetType().Name;
                int findit = pageName.LastIndexOf("_aspx");
                pageName = pageName.Substring(0, findit);
                findit = pageName.LastIndexOf("_");
                pageName = pageName.Substring(findit + 1);

                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("로그인을 하신후 글쓰기 하셔야 합니다.");
				//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/community/" + pageName + ".aspx");
				sjs += JavaScript.GetPageMoveScript("/login.aspx");
				sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                return;
            }
            string noIndex = Request.QueryString["iNoIndex"];
            string boardIdx = Request.QueryString["iBoardIdx"];
            string mod = Request.QueryString["mod"];

            if (mod=="m")
            {
                btnSave.CssClass = "btn btn-modify";
                DisplayInfo(noIndex, boardIdx);
            }
            else
                btnSave.CssClass = "btn btn-registration";
        }
    }

    private void DisplayInfo(string iNoIndex, string iBoardIdx)
    {
        string sJs = string.Empty;

        //-----1. 데이타를 가지고 온다. -----------------------------------------------------------               
        DataSet ds = new DataSet();
        try
        {
            WWWService.Service wwwService = new WWWService.Service();
            ds = wwwService.MateGetBoardContents(_tableIdx, Convert.ToInt32(iNoIndex), Convert.ToInt32(iBoardIdx));
        }
        catch (Exception ex)
        {            
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("게시판 글을 가져오던 중 에러가 발생했습니다. 관리자에게 문의하세요. 원인 : " + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sJs += JavaScript.GetHistoryBackScript ();
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);            
            return;
        }

        UserInfo user = new UserInfo ();
        if (user.UserId.ToLower() != ds.Tables["T"].Rows[0]["user_id"].ToString().ToLower ())
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("게시판 글을 수정할 수 있는 권한이 없습니다.");
            sJs += JavaScript.GetHistoryBackScript();
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
            return;
        }

        txtTitle.Value = ds.Tables["T"].Rows[0]["b_title"].ToString();
        txtComment.Value = ds.Tables["T"].Rows[0]["b_content"].ToString();
        hidNoIndex.Value = iNoIndex;
        hidBoardIndex.Value = iBoardIdx;
        //for (int i = 1; i <= 5; i++)
        //{
        //    litFiles.Text += i.ToString () + "번파일 : " + FileView(ds.Tables["T"].Rows[0]["b_filename" + i.ToString()].ToString());
        //}

    }

    private string FileView(string sFileNM)
    {
        string sUrl = String.Empty;
        if (sFileNM.Trim().Length > 0)
        {
            sUrl = "<a href='/Controls/FileDown.aspx?filename=" + sFileNM
                     + "'>" + sFileNM + "</a>&nbsp;";
        }
        return sUrl;
    }

    protected string UserName
    {
        get
        {
            return new UserInfo().UserName;
        }
    }

    private bool InsertBoard()
    {        
        WWWService.Service wwwService = new WWWService.Service();
        string sJs = string.Empty;

        //1. 파일업로드부분
        //Files/Board 폴더에 파일을 저장한다.

        //파일이 존재하지않는다면.
        //bool bFileDivision;

        //파일이 존재한다면 원하는 정보를 변수에 넣는다.
        string sFileName = string.Empty;
        //int iFileSize = 0;

        string category = "";

        int no_idx = String.IsNullOrEmpty(Request.QueryString["iNoIndex"]) ? 0 : Convert.ToInt32(Request.QueryString["iNoIndex"]);
        int b_num = String.IsNullOrEmpty(Request.QueryString["b_num"]) ? 0 : Convert.ToInt32(Request.QueryString["b_num"]);
        int reff = String.IsNullOrEmpty(Request.QueryString["ref"]) ? 0 : Convert.ToInt32(Request.QueryString["ref"]);
        int re_step = String.IsNullOrEmpty(Request.QueryString["re_step"]) ? 0 : Convert.ToInt32(Request.QueryString["re_step"]);
        int re_level = String.IsNullOrEmpty(Request.QueryString["re_level"]) ? 0 : Convert.ToInt32(Request.QueryString["re_level"]);                                              

        if(String.IsNullOrEmpty (Request.QueryString["mod"]))
            no_idx = 0;

        UserInfo user = new UserInfo();

        WWWService.Service service = new WWWService.Service();

        string sPublic = string.Empty;

        if (rdpublic_Y.Checked)
            sPublic = "Y";

        else if (rdpublic_N.Checked)
            sPublic = "N";

        service.MateTestSetBoardContents_Christmas(_tableIdx
                                           , no_idx
                                           , b_num
                                           , reff
                                           , re_step
                                           , re_level
                                           , user.UserName
                                           , txtTitle.Value
                                           , txtComment.Value
                                           , Request.UserHostAddress
                                           , "compassion_zoppura"
                                           , false
                                           , ""
                                           , user.UserId
                                           , category
                                           , sPublic);

        return true;
    }

    private bool UpdateBoard()
    {
        WWWService.Service wwwService = new WWWService.Service();
        string sJs = string.Empty;

        string sFiles = string.Empty;

        string sPublic = string.Empty;

        if (rdpublic_Y.Checked)
            sPublic = "Y";

        else if (rdpublic_N.Checked)
            sPublic = "N";

        UserInfo user = new UserInfo();
        WWWService.Service service = new WWWService.Service();

        wwwService.MateModifyBoardContents_Christmas(_tableIdx
                                              , int.Parse(hidNoIndex.Value)
                                              , int.Parse(hidBoardIndex.Value)
                                              , UserName
                                              , txtTitle.Value
                                              , txtComment.Value
                                              , ""
                                              , sPublic);

        return true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool retVal;

        if (String.IsNullOrEmpty(Request.QueryString["mod"]) || Request.QueryString["mod"] == "r")
            retVal = InsertBoard();
        else
            retVal = UpdateBoard();

        if (retVal)
        {
            string pageName = Context.Handler.GetType().Name;
            int findit = pageName.LastIndexOf("_aspx");
            pageName = pageName.Substring(0, findit);
            findit = pageName.LastIndexOf("_");
            pageName = pageName.Substring(findit + 1);

            if (pageName == "masterchallengewrite")
                Response.Redirect("masterchallengelist.aspx");
            else if (pageName == "ourstorywrite")
                Response.Redirect("ourstorylist.aspx");
            else if (pageName == "translationqnawrite")
                Response.Redirect("translationqnalist.aspx");
            else if (pageName == "onlinewrite")
                Response.Redirect("onlinelist.aspx");
            else if (pageName == "christmasloveletterwrite")
                Response.Redirect("ChristmasLoveLetterList.aspx");
            else
                Response.Redirect("empty.aspx");
        }
    }
}