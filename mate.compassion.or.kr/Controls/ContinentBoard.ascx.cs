﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data; 

public partial class Controls_ContinentBoard : System.Web.UI.UserControl
{
    WWWService.Service _WWWService = new WWWService.Service();
    string sjs;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            GetDataBind();
    }

    private void GetDataBind()
    {
        string[] nationStr = ((HiddenField)Page.FindControl("hidNation")).Value.Split(',');        
        DataSet data = _WWWService.NoticeNationBoardList("2",1006,nationStr);
        repData.DataSource = data.Tables[0];
        repData.DataBind();
    }

    protected string Ahref(object TABLE_IDX)
    {
        if (TABLE_IDX.ToString () == "1227")
            return "prayersDetail";
        else
            return "DisasterDetail";
    }
}