﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;

public partial class Controls_MateTop : System.Web.UI.UserControl
{
    public bool bThirdPL;
    public bool bScreening;
    public bool bTranslate;
    public string sgHtml;
    
    protected void Page_Load(object sender, EventArgs e)
    {

		SetTopLink();

		StringBuilder sb = new StringBuilder();
        if (UserInfo.IsLogin){
            
            ph_after_login.Visible = true;

			var actionResult = new LetterAction().GetUnreadCount();
			if (actionResult.success) {
				var count = ((LetterAction.UnreadEntity)actionResult.data).cnt;
				letter_count.Visible = count > 0;
				letter_count.InnerHtml = count > 99 ? "99+" : count.ToString();
			} else {
				letter_count.Visible = false;
			}


            UserInfo user = new UserInfo();

			labName2.Text = user.UserName;
			/*
            if (user.ConId.Length > 6)
                labName2.Text = user.UserId + " 님";

            else
                labName2.Text = user.UserId + "(" + user.ConId + ") 님";
			*/

			//권한 체크
			Object[] objSql = new object[1] { "MATE_MateYearCheck" };
            Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
            Object[] objThirdPLValue = new object[2] { new UserInfo().UserId, "31" };
            Object[] objScreeningValue = new object[2] { new UserInfo().UserId, "21" };
            Object[] objTranslateValue = new object[2] { new UserInfo().UserId, "11" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            //3PL 권한체크
            if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objThirdPLValue).Tables[0].Rows.Count > 0)
            {
                //new TranslateInfo().ThirdPLCheck = "true";
                bThirdPL = true;
            }

            //스크리닝 권한체크
            if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objScreeningValue).Tables[0].Rows.Count > 0)
            {
                //new TranslateInfo().ScreeningCheck = "true";
                bScreening = true;
            }

            //번역 권한체크
            if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objTranslateValue).Tables[0].Rows.Count > 0)
            {
                //new TranslateInfo().TranslateCheck = "true";
                bTranslate = true;
            }

            _WWW6Service.Dispose();

            if (bThirdPL || bScreening || bTranslate)
            {
                sb.Append("<a href=\"/translation/TranslationHome.aspx\" class=\"l-imate\" onmouseover=\"MenuShowOver('4');\" onmouseout=\"MenuShowOut('4');\">iMATE</a>");
                sb.Append("<div id=\"divIMate\" onmouseover=\"MenuShowOver('4');\" onmouseout=\"MenuShowOut('4');\">");
                sb.Append("<script language=\"javascript\">");
                sb.Append("document.all.divIMate.style.display = 'none'");
                sb.Append("</script>");

                sb.Append("<ul>");
                sb.Append("	<li><a href=\"/translation/TranslationHome.aspx\" class=\"dep4-1\">홈</a></li>");

                if (bTranslate)
                {
                    sb.Append("<li><a href=\"/translation/TranslationMain.aspx\" class=\"dep4-2\">번역</a></li>");
                    sb.Append("<li><a href=\"/translation/TranslationComplete.aspx\" class=\"dep4-3\">번역 완료</a></li>");
                }

                if (bScreening)
                {
                    sb.Append("<li><a href=\"/translation/ScreeningMain.aspx\" class=\"dep4-4\">스크리닝</a></li>");
                    sb.Append("<li><a href=\"/translation/ScreeningComplete.aspx\" class=\"dep4-5\">스크리닝 완료</a></;li>");
                }

                if (bThirdPL)
                {
                    sb.Append("<li><a href=\"/translation/ThirdPLMain.aspx\" class=\"dep4-6\">3PL</a></li>");
                    sb.Append("<li><a href=\"/translation/ThirdPLComplete.aspx\" class=\"dep4-7\">3PL 완료</a></li>");
                }

                if (bTranslate || bScreening)
                {
                    sb.Append("<li><a href=\"/translation/TranslationLetterSearch.aspx\" class=\"dep4-8\">편지 조회</a></li>");
                }

                sb.Append("</ul>");
                sb.Append("</div>");
            }

            else
            {
                sb.Append("<a href=\"#\" class=\"l-imate\" onmouseover=\"MenuShowOver('');\" onmouseout=\"MenuShowOut('');\" title=\"활동 중인 번역메이트만 사용 가능합니다.\">iMATE</a>");
                sb.Append("<div id=\"divIMate\" onmouseover=\"MenuShowOver('');\" onmouseout=\"MenuShowOut('');\">");
                sb.Append("<script language=\"javascript\">");
                sb.Append("document.all.divIMate.style.display = 'none'");
                sb.Append("</script>");
                //sb.Append("&nbsp<b>활동 중지</b>");
                sb.Append("</div>");
            }
        }

        else{

			ph_before_login.Visible = true;



            sb.Append("<a href=\"/translation/TranslationHome.aspx\" class=\"l-imate\" onmouseover=\"MenuShowOver('');\" onmouseout=\"MenuShowOut('');\" title=\"로그인 후 사용 가능합니다.\">iMATE</a>");
            sb.Append("<div id=\"divIMate\" onmouseover=\"MenuShowOver('');\" onmouseout=\"MenuShowOut('');\">");
            sb.Append("<script language=\"javascript\">");
            sb.Append("document.all.divIMate.style.display = 'none'");
            sb.Append("</script>");
            //sb.Append("&nbsp<b>로그인 필요</b>");
            sb.Append("</div>");

            //Login.Visible = false;
            //Logout.Visible = true;
        }

        sgHtml = sb.ToString();
    }


	void SetTopLink() {
		
		var domain_www = ConfigurationManager.AppSettings["domain_www"];
		// 한국컴패션
		link_compassion_not_login.HRef = domain_www;
		link_compassion_login.HRef = domain_www;
		// 인재채용 
		link_recruit_not_login.HRef = domain_www + "/recruit/";
		// 마이페이지
		link_my.HRef = domain_www + "/my/";
		// 인재채용
		link_recruit_login.HRef = domain_www + "/recruit/";
		// 편지함
		link_letter.HRef = domain_www + "/my/letter/";
		// 스토어
		link_store.HRef = domain_www + "/store/";
		// 컴패션 스토어
		link_store2.HRef = domain_www + "/store/";
		// 장바구니
		link_cart.HRef = domain_www + "/store/cart/";
		// 후기/문의
		link_review.HRef = domain_www + "/store/review";
		// 공지사항
		link_notice.HRef = domain_www + "/store/notice/";
		// faq
		link_faq1.HRef = domain_www + "/customer/faq/";
		link_faq2.HRef = domain_www + "/customer/faq/";


		// 메일
		var actionResult = new LetterAction().GetUnreadCount();
		if (actionResult.success) {
			var count = ((LetterAction.UnreadEntity)actionResult.data).cnt;
			letter_count.Visible = count > 0;
			letter_count.InnerHtml = count > 99 ? "99+" : count.ToString();
		} else {
			letter_count.Visible = false;
		}

	}
}