﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data; 

public partial class Mate_Controls_BoardDetail2 : System.Web.UI.UserControl
{
    WWWService.Service _wwwService = new WWWService.Service();
    string sJs;    

    protected void Page_Load(object sender, EventArgs e)
    {
        int iTableIndex = Convert.ToInt32(Request.QueryString["iTableIndex"]);
        int iNoIndex = Convert.ToInt32(Request.QueryString["iNoIndex"]);
        int iBoardIdx = Convert.ToInt32(Request.QueryString["iBoardIdx"]);

        string userid = string.Empty;
        string PublicYN = string.Empty;

        if (Request.QueryString["user_id"] != null)
            userid = Request.QueryString["user_id"].ToString();

        if (Request.QueryString["PublicYN"] != null)
            PublicYN = Request.QueryString["PublicYN"].ToString();

        if (PublicYN == "N")
        {
            UserInfo userInfo = new UserInfo();

            if (userInfo.UserId != userid)
            {
                litContent.Text = "비공개 글입니다.";
            }

            else
            {
                if (!IsPostBack)
                {
                    try
                    {
                        DisplayInfo(iTableIndex, iNoIndex, iBoardIdx);
                        GetCommentData();
                    }
                    catch (Exception ex)
                    {
                        sJs = JavaScript.HeaderScript;
                        sJs += JavaScript.GetAlertScript("게시판 글을 가져오던 중 에러가 발생했습니다. 관리자에게 문의하세요. 원인 : " + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
                        sJs += JavaScript.FooterScript;
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
                        return;
                    }

                    //string pageName = GetParentName();
                    //if (pageName == "noticeview" || pageName == "monthlymateview" || pageName == "todaywordview")
                    //    btnAnswer.Visible = false;
                }
            }
        }

        else
        {
            if (!IsPostBack)
            {
                DisplayInfo(iTableIndex, iNoIndex, iBoardIdx);
                GetCommentData();

                //string pageName = GetParentName();
                //if (pageName == "noticeview" || pageName == "monthlymateview" || pageName == "todaywordview")
                //    btnAnswer.Visible = false;
            }
        }
    }

    private void DisplayInfo(int iTableIndex, int iNoIndex, int iBoardIdx)
    {
        string sJs = string.Empty;

        //-----1. 데이타를 가지고 온다. -----------------------------------------------------------               
        DataSet ds = new DataSet();

        try
        {
            ds = _wwwService.MateGetBoardContents(iTableIndex, iNoIndex, iBoardIdx);
        }

        catch (Exception ex)
        {            
            //메세지 띄우기
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("게시판 글을 가져오던 중 에러가 발생했습니다. 관리자에게 문의하세요. 원인 : " + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
            return;
        }

        labNo.Text = ds.Tables["T"].Rows[0]["no_idx"].ToString();         
        labTitle.Text = ds.Tables["T"].Rows[0]["b_title"].ToString();
        labWriter.Text = ds.Tables["T"].Rows[0]["b_name"].ToString();
        labWriteDay.Text = Convert.ToDateTime (ds.Tables["T"].Rows[0]["b_WriteDay"].ToString()).ToString ("yyyy-MM-dd");
        labReadNum.Text = ds.Tables["T"].Rows[0]["b_readnum"].ToString();
                
        DateTime dt = DateTime.Parse(ds.Tables["T"].Rows[0]["b_WriteDay"].ToString());
        TimeSpan ts = DateTime.Now - dt;

        if (ts.TotalMinutes >= 10080)
            labNew.Visible = false;

        if (UserInfo.IsLogin && new UserInfo().UserId.ToLower() == ds.Tables["T"].Rows[0]["user_id"].ToString().ToLower())
        {
            btnModify.Visible = true;
            btnDel.Visible = true; 
        }

        string sContents = ds.Tables["T"].Rows[0]["b_content"].ToString();
        sContents = sContents.Replace("''", "'");
        //sContents = sContents.Replace("<", "&lt;");
        //sContents = sContents.Replace(">", "&gt;");
        sContents = sContents.Replace("\r\n", "<br/>");
        sContents = sContents.Replace("\n", "<br/>");       
         
        litContent.Text = sContents;

        //첨부파일관련
        for (int i = 1; i <= 5; i++)
        {
            litFiles.Text += FileView(ds.Tables["T"].Rows[0]["b_filename" + i.ToString()].ToString());
        }

        //리플수를 올려준다.
        _wwwService.mateReplyCountUpdate (int.Parse(ds.Tables["T"].Rows[0]["board_idx"].ToString()));

        // Mate 속도 관련
        ds.Dispose();
    }

    private string FileView(string sFileNM)
    {
        string sUrl = String.Empty;
        if (sFileNM.Trim().Length > 0)
        {
            sUrl = "<a href='/Controls/FileDown.aspx?filename=" + sFileNM
                     + "'>" + sFileNM + "</a>&nbsp;";
        }
        return sUrl;
    }

    private void GetCommentData()
    {
        int iTableIndex = Convert.ToInt32(Request.QueryString["iTableIndex"]);
        int iNoIndex = Convert.ToInt32(Request.QueryString["iNoIndex"]);
        int iref = Convert.ToInt32(Request.QueryString["ref"]);
        int ire_step = Convert.ToInt32(Request.QueryString["re_step"]);

        DataSet data = _wwwService.MateCommentList(iTableIndex, iNoIndex, iref, ire_step);
        repCommentData.DataSource = data.Tables[0];
        repCommentData.DataBind();

        // Mate 속도 관련
        data.Dispose();
    }

    private string GetParentName()
    {
        string pageName = Context.Handler.GetType().Name;
        int findit = pageName.LastIndexOf("_aspx");
        pageName = pageName.Substring(0, findit);
        findit = pageName.LastIndexOf("_");
        pageName = pageName.Substring(findit + 1);
        return pageName;
    }

    protected string ParentPageName
    {
        get
        {
            string pageName = GetParentName();
            if (pageName == "noticeview")
                return "noticelist.aspx";
            else if (pageName == "monthlymateview")
                return "MonthlyMateList.aspx";
            else if (pageName == "ourstoryview")
                return "OurStoryList.aspx";
            else if (pageName == "masterchallengeview")
                return "MasterChallengeList.aspx";
            else if (pageName == "translationqnaview")
                return "TranslationQnAList.aspx";
            else if (pageName == "todaywordview")
                return "todaywordlist.aspx";
            else if (pageName == "onlineview")
                return "onlinelist.aspx";
            else if (pageName == "christmasloveletterview")
                return "ChristmasLoveLetterList.aspx";
            else
                return "empty.aspx";
        }
    }

    protected string ParentPageWriteName
    {
        get
        {
            string pageName = GetParentName();
            if (pageName == "noticeview")
                return "noticeWrite.aspx";
            else if (pageName == "monthlymateview")
                return "MonthlyMateWrite.aspx";
            else if (pageName == "ourstoryview")
                return "OurStoryWrite.aspx";
            else if (pageName == "masterchallengeview")
                return "MasterChallengeWrite.aspx";
            else if (pageName == "translationqnaview")
                return "TranslationQnAWrite.aspx";
            else if (pageName == "onlineview")
                return "onlineWrite.aspx";
            else if (pageName == "christmasloveletterview")
                return "ChristmasLoveLetterWrite.aspx";
            else
                return "empty.aspx";
        }
    }

    protected void repCommentData_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
        HiddenField hidUserID = (HiddenField)e.Item.FindControl("hidUserID");
        if (UserInfo.IsLogin)
        {
            UserInfo userInfo = new UserInfo();
            if (userInfo.UserId == hidUserID.Value)
                btnDelete.Visible = true;
            else
                btnDelete.Visible = false;
        }
        else
            btnDelete.Visible = false;
    }


    protected void btnCommentSave_Click(object sender, EventArgs e)
    {
        if (UserInfo.IsLogin)
        {
            try
            {
                UserInfo userInfo = new UserInfo();

                int iTableIndex = Convert.ToInt32(Request.QueryString["iTableIndex"]);
                int iNoIndex = Convert.ToInt32(Request.QueryString["iNoIndex"]);
                int iref = Convert.ToInt32(Request.QueryString["ref"]);
                int ire_step = Convert.ToInt32(Request.QueryString["re_step"]);

                _wwwService.MateCommentInsert(iTableIndex
                                         , iNoIndex
                                         , iref
                                         , ire_step
                                         , userInfo.UserName
                                         , userInfo.UserId
                                         , ""
                                         , Request.UserHostAddress);

                GetCommentData();

                sJs = JavaScript.HeaderScript;
                sJs += JavaScript.GetPageMoveScript(this.Page.Request.RawUrl);    
                sJs += JavaScript.FooterScript;
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);

            }
            catch (Exception ex)
            {
                string sJs = string.Empty;
                //메세지 띄우기
                sJs = JavaScript.HeaderScript;
                sJs += JavaScript.GetAlertScript("코맨트 작성중 에러가 발생했습니다. 관리자에게 문의하세요. 원인 : " + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
                sJs += JavaScript.FooterScript;
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
                return;
            }
        }
        else
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("로그인후 댓글을 작성하셔야 합니다.");
			//sJs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=" + Server.UrlEncode(Request.Url.AbsoluteUri));
			sJs += JavaScript.GetPageMoveScript("/login.aspx");
			sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }
    }
    protected void repCommentData_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            HiddenField hidUserID = (HiddenField)e.Item.FindControl("hidUserID");
            HiddenField hidcref = (HiddenField)e.Item.FindControl("hidcref");
            HiddenField hidtable_idx = (HiddenField)e.Item.FindControl("hidtable_idx");
            HiddenField hidre_step = (HiddenField)e.Item.FindControl("hidre_step");
            HiddenField hidwriteday = (HiddenField)e.Item.FindControl("hidwriteday");
            int seqno = Convert.ToInt32(e.CommandArgument);

            _wwwService.MateCommentDelete(seqno, hidUserID.Value, Convert.ToInt32(hidtable_idx.Value), Convert.ToInt32(hidcref.Value), Convert.ToInt32(hidre_step.Value), hidwriteday.Value);
            GetCommentData();

            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetPageMoveScript(this.Page.Request.RawUrl);
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }
    }
    protected void btnModify_Click(object sender, EventArgs e)
    {
        Response.Redirect(ParentPageWriteName + "?mod=m&inoIndex=" + Request.QueryString["iNoIndex"] + "&iBoardIdx=" + Request.QueryString["iBoardIdx"] + "&b_num=0&re_level=0&re_step=0&ref=0"); 
    }
    protected void btnDel_Click(object sender, EventArgs e)
    {
        _wwwService.MateBoardDelete2 (Convert.ToInt32 (Request.QueryString["iBoardIdx"]));
        Response.Redirect(ParentPageName); 
    }
}