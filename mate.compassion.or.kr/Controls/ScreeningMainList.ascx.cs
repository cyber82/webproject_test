﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Text;

using System.Web.UI.HtmlControls;

using CommonLib;

public partial class Mate_Controls_ScreeningMainList : System.Web.UI.UserControl
{
    int _PAGESIZE = 10;
    int _tableIdx;
    bool bgGetLetter;

    protected string _pageIdx = "0";    

    protected void Page_Load(object sender, EventArgs e)
    {
        string sJs;

        if (UserInfo.IsLogin)
        {
            string sUserId = new UserInfo().UserId;

            //권한 체크
            Object[] objSql = new object[1] { "MATE_MateYearCheck" };
            Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
            Object[] objValue = new object[2] { sUserId, "21" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //권한체크
            if (data.Tables[0].Rows.Count > 0)
            {
                DataBind(Convert.ToInt32(_pageIdx));

                //페이지 타입 쿠키 저장
                new TranslateInfo().MatePageType = "20";
            }

            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                sb.Append("alert('해당 권한이 없습니다.'); location.href='/';");
                sb.Append("</script>");

                Response.Write(sb.ToString());     
            }

            data.Dispose();
            _WWW6Service.Dispose();
        }

        else
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("로그인 후 사용 가능합니다.");
			//sJs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=" + Server.UrlEncode(Request.Url.AbsoluteUri));
			sJs += JavaScript.GetPageMoveScript("/login.aspx");
			sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }

		// 편지 타입
		var codeResult = new LetterAction().GetCodeList();
		if (codeResult.success) {

			HtmlGenericControl all = new HtmlGenericControl("li");
			all.InnerText = "전체";
			all.Attributes["data-id"] = "";
			letter_type.Controls.Add(all);

			DataTable codeList = (DataTable)codeResult.data;
			foreach (DataRow dr in codeList.Rows) {
				HtmlGenericControl li = new HtmlGenericControl("li");
				var codeId = dr["codeId"].ToString();

				if (rdoChild.Checked) {
					if (codeId.StartsWith("CHI")) {
						li.InnerText = dr["codeId"].ToString();
						li.Attributes["data-id"] = dr["codeId"].ToString();
						letter_type.Controls.Add(li);
					}
				} else {
					if (!codeId.StartsWith("CHI")) {
						li.InnerText = dr["codeId"].ToString();
						li.Attributes["data-id"] = dr["codeId"].ToString();
						letter_type.Controls.Add(li);
					}
				}
				

			}
		}


		// 어린이 키
		// 국외인경우 국가목록
		var countryResult = new CountryAction().GetList("c_id");
		if (countryResult.success) {

			HtmlGenericControl all = new HtmlGenericControl("li");
			all.InnerText = "전체";
			all.Attributes["data-id"] = "";
			ul_chile_key.Controls.Add(all);

			var countryList = (List<sp_country_list_fResult>)countryResult.data;
			foreach (sp_country_list_fResult entity in countryList) {
				HtmlGenericControl li = new HtmlGenericControl("li");
				li.InnerText = entity.c_id;
				li.Attributes["data-id"] = entity.c_id; 
				ul_chile_key.Controls.Add(li);
			}
		}



	}

    private void DataBind(int idxPage)
    {
        string sUserId = new UserInfo().UserId;
        //sUserId = "aeii";//테스트용 임시

        //기본 가져올 편지 개수 "1"
        if (string.IsNullOrEmpty(this.txtGetLetterCount.Text))
        {
            this.txtGetLetterCount.Text = "1";
        }

		//리스트 출력
		//시작 시간에 해당 한 주가 지난 목록은 가져 오지 않는다
		Object[] objSql = new object[1] { "MATE_ScreeningMainList2016" };
        Object[] objParam = new object[7] { "DIVIS", "TranslationID ", "CorrID", "CorrType", "ChildKeyPrefix", "order_name", "order_type" };
		Object[] objValue = new object[7] { "LIST", sUserId, CorrID.Text == "" ? "-1" : CorrID.Text.Trim(), CorrType.Value, ChildKey.Value, order_name.Value, order_type.Value };

		WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

        // 정렬
        //DataView dv = data.Tables[0].DefaultView;
        //dv.Sort = "ScanningDate";

        //DataTable dt = dv.Table.Clone();

        //foreach (DataRowView drv in dv)
        //    dt.ImportRow(drv.Row);

        //data = new DataSet();
        //data.Tables.Add(dt);

        //for (int i = 0; i < data.Tables[0].Rows.Count; i++)
        //    data.Tables[0].Rows[i]["Idx"] = i + 1;

        //리스트 카운터
        this.lblTotalCount.Text = "Total : " + data.Tables[0].Rows.Count;

        bgGetLetter = true;//편지 가져 올지 유무 

        if (data.Tables[0].Rows.Count > 0)
        {
            if (data.Tables[0].Select("", "ScreeningStart")[0]["ScreeningStart"] != DBNull.Value)
            {
                DateTime dtMin = Convert.ToDateTime(data.Tables[0].Select("", "ScreeningStart")[0]["ScreeningStart"]);

                //편지를 가져온지 7일이 지난 편지가 있을 경우 편지를 더 이상 가져오지 못 한다
                if (dtMin.AddDays(7).Subtract(Convert.ToDateTime(DateTime.Now.ToShortDateString())).Days <= 0)
                {
                    bgGetLetter = false;
                }
            }
        }

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = data.Tables[0].DefaultView;
        pds.AllowPaging = true;
        pds.PageSize = _PAGESIZE;
        pds.CurrentPageIndex = idxPage;
        repData.DataSource = pds;
        repData.DataBind();

        PagingSetting(data.Tables[0].Rows.Count, idxPage);

        data.Dispose();
        _WWW6Service.Dispose();
    }

    private void PagingSetting(int totalCount, int curPage)
    {
        PagingHelper1.VirtualItemCount = totalCount;
        PagingHelper1.PageSize = _PAGESIZE;
        PagingHelper1.CurrentPageIndex = curPage;
    }

    protected void PagingHelper1_OnPageIndexChanged(object sender, TaeyoNetLib.PagingEventArgs e)
    {
        DataBind(e.PageIndex);
        _pageIdx = e.PageIndex.ToString();
    }

    protected void imgBtnGetLetter_Click(object sender, ImageClickEventArgs e)
    {
		int iResult = 0;
		string sjs = "";

        DateTime reqTime = DateTime.Now;
        if (CheckFreezingTime(reqTime) == false)
        {
            Response.Write("<script language='javascript'>alert('매주 월요일 00:00 ~ 00:10은 데이터 조정 시간입니다.\\r\\n잠시 후 시도해주세요.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");
            return;
        }

        try {
			if (bgGetLetter) {
				string sUserId = new UserInfo().UserId;
				//sUserId = "aeii";//테스트용 임시

				//가져올 편지 개수
				if (string.IsNullOrEmpty(this.txtGetLetterCount.Text)) {
					this.txtGetLetterCount.Text = "1";
				}

				string sCorrType = "";

				if (this.rdoChild.Checked)//어린이 편지 가져오기 코드
				{
					sCorrType = "CHI";//Default
				} else if (this.rdoSponsor.Checked)//후원자 편지 가져오기 코드
				  {
					sCorrType = "SPN";
				}

				WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

				//편지 가져오기
				Object[] objSql = new object[1] { "MATE_ScreeningSave" };
				Object[] objParam = new object[3] { "MateID", "GetCount", "CorrType" };
				Object[] objValue = new object[3] { sUserId, this.txtGetLetterCount.Text, sCorrType };//번역

				iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

				_WWW6Service.Dispose();

				if (iResult > 0) {
					DataBind(Convert.ToInt32(_pageIdx));
					return;
				} else {
					sjs = JavaScript.HeaderScript.ToString();
					sjs += JavaScript.GetAlertScript("가져올 편지가 없습니다.");
					sjs += JavaScript.FooterScript.ToString();
					Page.ClientScript.RegisterStartupScript(this.GetType(), "Alter", sjs);
					return;
				}
			} else {
				sjs = JavaScript.HeaderScript.ToString();
				sjs += JavaScript.GetAlertScript("현재 가지고 있는 편지 중에 선점한 지 7일이 지난 편지가 있습니다.\\n가지고 있는 편지를 먼저 스크리닝 해주세요.");
				sjs += JavaScript.FooterScript.ToString();
				Page.ClientScript.RegisterStartupScript(this.GetType(), "Alter", sjs);
				return;
			}
		} catch (Exception ex) {
			//Exception Error Insert     
			WWWService.Service _wwwService = new WWWService.Service();
			_wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

			sjs = JavaScript.HeaderScript.ToString();
			sjs += JavaScript.GetAlertScript("편지를 가져오는 중 오류가 발생했습니다. \\r\\nException Error Message : " +
											  ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
			sjs += JavaScript.FooterScript.ToString();
			Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
			return;
		}

	}

	protected void btn_submit_Click(object sender, EventArgs e) {
		//DataBind(Convert.ToInt32(_pageIdx));
	}

    //2018-05-17 이종진 - WO-171
    //월요일 00:00 스케쥴러가 처리안된 편지를 EXCEED 처리시킴 데이터의 충돌우려로 인해,
    //월요일 00:00 ~ 00:10 분까지는 '임시저장', '전송', '편지가져오기', '신고하기' 기능을 정지시킴
    //*시간은 서버시간으로 체크해야하므로 script로 처리하지않고, 로직에서 처리함
    bool CheckFreezingTime(DateTime now)
    {
        bool result = true;
        if (now.DayOfWeek == DayOfWeek.Monday)  //월요일
        {
            if (now.Hour == 0)  //0시
            {
                if (0 <= now.Minute && now.Minute <= 10)    //0~10분
                {
                    result = false;
                }
            }
        }

        return result;
    }

}