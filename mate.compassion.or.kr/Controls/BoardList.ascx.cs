﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data;
using System.Text;

public partial class Mate_Controls_BoardList : System.Web.UI.UserControl
{
    int _PAGESIZE = 10;
    int _tableIdx;
    protected string _pageIdx = "0";

    protected void Page_Load(object sender, EventArgs e)
    {
        _tableIdx = Convert.ToInt32(((HiddenField)Page.FindControl("hidTableIdx")).Value);

        if (!IsPostBack)
        {
            txtSearch.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSearch.ClientID + "').click();return false;}} else {return true}; ");
            _pageIdx = Request.QueryString["pageIdx"];
            if (String.IsNullOrEmpty(_pageIdx))
                _pageIdx = "0";

            string searchOption = Request.QueryString["searchOption"];
            string searchTxt = Request.QueryString["searchTxt"];

            if (!String.IsNullOrEmpty(searchOption))
                _searchOption = searchOption;
            else
            {
                _searchOption = "all";
                searchOption = "all";
            }

            if (!String.IsNullOrEmpty(searchTxt))
                _searchTxt = searchTxt;

            HeadDataBind();
            DataBind(Convert.ToInt32(_pageIdx), searchOption, searchTxt);
        }
        else
        {
            _pageIdx = Request.QueryString["pageIdx"];
            if (String.IsNullOrEmpty(_pageIdx))
                _pageIdx = "0";
        }
    }

    private void PagingSetting(int totalCount, int curPage)
    {
        PagingHelper1.VirtualItemCount = totalCount;
        PagingHelper1.PageSize = _PAGESIZE;
        PagingHelper1.CurrentPageIndex = curPage;
    }

    protected string IsReply( object re_level)
    {
        string ret=String.Empty;  
        //int step = Convert.ToInt32 (re_step); 
        int level = Convert.ToInt32 (re_level);
        if (level > 0)
        {            
            for (int i = 0; i < level; i++)
                ret += "&nbsp;&nbsp;";

            ret += "┗Re:";
        }
        return ret;
    }

    protected void PagingHelper1_OnPageIndexChanged(object sender, TaeyoNetLib.PagingEventArgs e)
    {
        DataBind(e.PageIndex, dropOption.SelectedValue, txtSearch.Text);
        _pageIdx = e.PageIndex.ToString();
    }

    protected string CntString(object obj)
    {
        if (Convert.ToInt32(obj) > 0)
            return " (" + obj.ToString() + ")";
        else
            return String.Empty; 
    }

    private void HeadDataBind()
    {
        WWWService.Service _wwwService = new WWWService.Service();
        DataSet data = _wwwService.Mate_NoticeBoardList(_tableIdx);

        for (int i = data.Tables[0].Rows.Count - 1; i > 2; i--)
            data.Tables[0].Rows.RemoveAt(i); 

        repHeadData.DataSource = data.Tables [0];
        repHeadData.DataBind();

        // Mate 속도 관련
        data.Dispose();
    }
    

    private void DataBind(int idxPage, string searchOption, string searchStr)
    {
        WWWService.Service _wwwService = new WWWService.Service();
        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
        DataSet data;

        // 2018-06-04 junheo : Paging 관련 수정
        if (searchOption == "b_recontent")
        {
            data = _wwwService.Mate_BoardCommentSearchList(_tableIdx, searchStr);
        }
        else
        {
            //data = _wwwService.Mate_BoardList(_tableIdx, searchOption, searchStr, 0, 500);

            // Board 별 List 조회 
            StringBuilder sbList = new StringBuilder();

            sbList.Append("SELECT * FROM ( ");

            sbList.Append("SELECT Row_Number() OVER (order by MU.ref desc) as rownum , COUNT(*) OVER () AS totalCNT, CONVERT(VARCHAR, b_writeday, 111) AS WriteDay, MU.*, ");
            sbList.Append("(SELECT COUNT(*) FROM mate_upboard_comment uc WHERE uc.table_idx = " + _tableIdx + " and MU.no_idx = uc.seqno and MU.b_num = uc.ref and MU.re_step = uc.re_step ) AS CNT ");    // 댓글 갯수 조회
            sbList.Append("FROM Mate_UpBoard MU ");
            sbList.Append("WHERE table_idx = " + _tableIdx + " ");  // 조회대상 Table
            sbList.Append("AND notice_idx <> 'Y' ");    // 공지 제외
            sbList.Append("AND re_level = 0 ");  // 답변이 아닌 것만 조회

            // 검색 조건 처리
            if(searchOption == "all")
            {
                sbList.Append("AND (MU.b_title LIKE '%" + searchStr + "%' OR MU.b_content LIKE '%" + searchStr + "%' ) ");  // 전체 조회인 경우
            }
            else
            {
                sbList.Append("AND (MU." + searchOption + " LIKE '%" + searchStr + "%' ) ");  // 선택 조회인 경우
            }

            sbList.Append(") AS A  WHERE A.rownum BETWEEN (" + (idxPage + 1).ToString() + "-1) * " + _PAGESIZE.ToString() + " + 1 AND " + (idxPage + 1).ToString() + " * " + _PAGESIZE.ToString() + " ");  // 페이징 처리

            sbList.Append("ORDER BY ref DESC "); // 정렬

            Object[] objSql = new object[1] { sbList.ToString() };
            
            data = _WWW6Service.NTx_ExecuteQuery("SqlCompassWeb4", objSql, "TEXT", null, null); // 쿼리 실행
        }

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = data.Tables[0].DefaultView;
        pds.AllowPaging = true;
        pds.PageSize = _PAGESIZE;
        pds.CurrentPageIndex = idxPage;

        repData.DataSource = data;
        repData.DataBind();

        //PagingSetting(data.Tables[0].Rows.Count, idxPage);
        int totalCNT = 0;
        if (data.Tables[0].Rows.Count > 0)
        {
            totalCNT = Convert.ToInt32(data.Tables[0].Rows[0]["totalCNT"].ToString());
        }
        PagingSetting(totalCNT, idxPage);

        // Mate 속도 관련
        data.Dispose();
    }

    protected void repData_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label labNew = (Label)e.Item.FindControl("labNew");

        HiddenField hidb_writeday = (HiddenField)e.Item.FindControl("hidb_writeday");
        DateTime dt = DateTime.Parse(hidb_writeday.Value);
        TimeSpan ts = DateTime.Now - dt;

		HtmlGenericControl comment_num = (HtmlGenericControl)e.Item.FindControl("comment_num");
		if(_tableIdx == 1001) {
			comment_num.Visible = false;
		}
		if (ts.TotalMinutes >= 10080)
            labNew.Visible = false;
    }

    protected string IsBold(object title,object boardIdx)
    {
        string iBoardIdx = Request.QueryString["iBoardIdx"];
        if (iBoardIdx == boardIdx.ToString ())
            return "<b>" + title.ToString() + "</b>";
        else
            return title.ToString(); 
    }

    protected void repHeadData_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label labNew = (Label)e.Item.FindControl("labNew");

        HiddenField hidb_writeday = (HiddenField)e.Item.FindControl("hidb_writeday");
        DateTime dt = DateTime.Parse(hidb_writeday.Value);
        TimeSpan ts = DateTime.Now - dt;

        if (ts.TotalMinutes >= 10080)
            labNew.Visible = false;
    }

    protected string _searchOption;
    protected string _searchTxt;

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        _searchOption = dropOption.SelectedValue;
        _searchTxt = txtSearch.Text;
        DataBind(0, dropOption.SelectedValue, txtSearch.Text);
    }

    protected string ParentPageName
    {
        get
        {
            string pageName = Context.Handler.GetType().Name;
            int findit = pageName.LastIndexOf("_aspx");
            pageName = pageName.Substring(0, findit);
            findit = pageName.LastIndexOf("_");
            pageName = pageName.Substring(findit + 1);

            if (pageName == "noticelist" || pageName == "noticeview")
                return "noticeview.aspx";
            else if (pageName == "monthlymatelist" || pageName == "monthlymateview")
                return "MonthlyMateView.aspx";
            else if (pageName == "ourstorylist" || pageName == "ourstoryview")
                return "ourstoryview.aspx";
            else if (pageName == "masterchallengelist" || pageName == "masterchallengeview")
                return "MasterChallengeView.aspx";
            else if (pageName == "translationqnalist" || pageName == "translationqnaview")
                return "TranslationQnAView.aspx";
            else if (pageName == "todaywordlist" || pageName == "todaywordview")
                return "todaywordview.aspx";
            else if (pageName == "onlinelist" || pageName == "onlineview")
                return "onlineview.aspx";
            else if (pageName == "christmasloveletterlist" || pageName == "christmasloveletterview")
                return "ChristmasLoveLetterView.aspx";
            else if (pageName == "togetherstorylist" || pageName == "togetherstoryview")
                return "togetherstoryview.aspx";
            else
                return "empty.aspx";
        }
    }
}
