﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NanumRight.ascx.cs" Inherits="Mate_Controls_NanumRight" %>
<script language="javascript" type="text/javascript">
    function ValidateBeneInsert() {
        <%
            if (!UserInfo.IsLogin)
            {
         %>
            alert('로그인 하신후 이용하실 수 있습니다.');
            return false;
         <%
            }
          %>
        if (document.getElementById('<%=eword.ClientID%>').value.trim() == "") {
            alert('용어를 입력 하세요.');
            return false;
        }
        if (document.getElementById('<%=comment.ClientID%>').value.trim() == "") {
            alert('내용을 입력 하세요.');
            return false;
        }
        return true;
    }

    
    <%--function checkPermission(url) {
        if ("<%=bLogin%>" == "True") {
            if ("<%=bThirdPL%>" == "True" || "<%=bScreening%>" == "True" || "<%=bTranslate%>" == "True") {
                location.href = url;
            }
            else {
                alert("활동 중인 번역메이트만 사용 가능합니다.");
            }
        }
        else {
            alert("로그인 후 사용 가능합니다.");
        }
    }--%>
</script>
<div id="sidebar" class="sec-community">
	<div class="tit">
		<h2>메이트 나눔</h2>
	</div>
	<ul id="lnb" class="lnb">
		<li id="sub7" runat="server" ><a href="TogetherStoryList.aspx" class="dep7"><span>함께하는 이야기</span></a></li>
        <li id="sub1" runat="server" ><a href="OurStoryList.aspx" class="dep1"><span>우리들의 이야기</span></a></li>
		<li id="sub2" runat="server" ><a href="TodayWordList.aspx" class="dep2"><span>오늘의 말씀</span></a></li>
		<li id="sub3" runat="server" ><a href="MasterChallengeList.aspx" class="dep3"><span>번달이 도전기</span></a></li>
		<li id="sub4" runat="server" ><a href="TranslationQnAList.aspx" class="dep4"><span>번역 Q&amp;A</span></a></li>
        <li id="sub5" runat="server" ><a href="OnlineList.aspx" class="dep5"><span>온라인 상담</span></a></li>
		<li id="sub6" runat="server" ><a href="BenefitDefault.aspx" class="dep6"><span>수혜국 편지 정보/용어</span></a></li>
	</ul>
	<ul class="banner1">
        <%--<li><a href="/community/ChristmasLoveLetterList.aspx"><img src="/image/mate/mate-xmasletter.jpg" width="199" height="105" alt="" /></a></li>--%>
		<li><a href="/Intro/MateIntro.aspx"><img src="/image/mate/mate-apply.jpg" width="199" height="105" alt="" /></a></li>
		<!--<li><a href="/mate/mypage/programDown.aspx"><img src="/image/mate/imate-down.jpg" width="199" height="53" alt="" /></a></li>-->
	</ul>
    <fieldset class="upload-wording">
		<legend class="hide">용어 올리기 제출 양식</legend>
		<div class="txt tit-upload-wording">용어 올리기</div>
		<p class="txt txt-upload-wording">메이트 님께서 편지를 번역하시면서 발견하신 유용한 정보를 다른 메이트님들과 공유해 주세요.</p>
		<dl class="field">
			<dt><strong class="txt txt-wording"><span>용어</span></strong></dt>
			<dd><input type="text" class="text" style="width:119px;" id="eword" runat="server" /></dd>
			<dt><strong class="txt txt-nation"><span>국가</span></strong></dt>
			<dd>				
                <asp:DropDownList ID="dropNation" runat="server" wh="128">
                </asp:DropDownList>		
			</dd>
			<dt><strong class="txt txt-initial"><span>초성</span></strong></dt>
			<dd>
				<select style="width:128px;" id="dropInitial" runat="server">
					<option value ="A">A</option>
                    <option value ="B">B</option>
                    <option value ="C">C</option>
                    <option value ="D">D</option>
                    <option value ="E">E</option>
                    <option value ="F">F</option>
                    <option value ="G">G</option>
                    <option value ="H">H</option>
                    <option value ="I">I</option>
                    <option value ="J">J</option>
                    <option value ="K">K</option>
                    <option value ="L">L</option>
                    <option value ="M">M</option>
                    <option value ="N">N</option>
                    <option value ="O">O</option>
                    <option value  ="P">P</option>
                    <option value ="Q">Q</option>
                    <option value ="R">R</option>
                    <option value ="S">S</option>
                    <option value ="T">T</option>
                    <option value ="U">U</option>
                    <option value ="V">V</option>
                    <option value ="W">W</option>
                    <option value ="X">X</option>
                    <option value ="Y">Y</option>
                    <option value ="Z">Z</option>
				</select>
			</dd>
			<dt><strong class="txt txt-context"><span>내용</span></strong></dt>
			<dd>
				<textarea rows="5" cols="13" id="comment" runat="server"></textarea>
			</dd>
		</dl>
		<div class="btn-m">
            <asp:Button ID="btnSave" runat="server" Text="" CssClass="btn btn-submit1" 
                OnClientClick ="return ValidateBeneInsert();" onclick="btnSave_Click" />
		</div>
	</fieldset>
</div>
