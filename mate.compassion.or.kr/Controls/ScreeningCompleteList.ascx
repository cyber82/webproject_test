﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ScreeningCompleteList.ascx.cs" Inherits="Mate_Controls_ScreeningCompleteList" %>
<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>

<link type="text/css" href="../../common/js/inc/js/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../common/js/inc/js/ui/demos.css" rel="stylesheet" />
<style type="text/css">
    .sbox
    {
        border: 1px solid #bbb;
    }
    .dropdown{position:absolute;z-index:100000;background-color:#fff;width:100px;}
    .dropdown ul{border:1px solid #CCCCCC;}
    .dropdown li{padding:5px 0;}
    .dropdown li:last-child{padding-bottom:5px;}
    .dropdown .on{background-color:#E6E6E6}
</style>
<script type="text/javascript" src="../../common/js/inc/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.core.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.datepicker-ko.js"></script>

<script type="text/javascript">
<!--
    $(function () {
        $('#start_date').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    $(function () {
        $('#end_date').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    function ShowDetail(sCorrID, sPackageID, sPageCount) {
        //수정 2013-08-28
        //location.href = '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount;
        var f = $("#form1");
        f.attr("action", '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount);
        f.attr("method", "post");
        f.attr("target", "_top");
        f.submit();

        /*
        var f = top.document.form1;
        f.action = '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount;
        f.method = "post";
        f.target = "_top";
        f.submit();
        */
    }

    function trColor(sw, idx) {
        var id = document.getElementById("dataTr_" + idx);

        if (sw == "1") {
            id.style.backgroundColor = "#ddd";
        }
        else if (sw == "0") {
            id.style.backgroundColor = "#fff";
        }
    }

    function SetDate(sSDate, sEDate) {
        document.getElementById("start_date").value = sSDate;
        document.getElementById("end_date").value = sEDate;
    }

    $(function () {

        $(".letter_type li").mouseover(function () {
            var selectedId = $(".CorrType").val();
            $(".letter_type li:not([data-id=" + selectedId + "])").removeClass("on");
            $(this).addClass("on");
        });
        $(".letter_type li").mouseout(function () {
            var selectedId = $(".CorrType").val();
            $(".letter_type li:not([data-id=" + selectedId + "])").removeClass("on");
        })

        $(".letter_type li").click(function () {
            $(".CorrType").val($(this).attr("data-id"));
            $(".dropdown_type").toggle();

            eval($(".btn_submit").attr("href").replace("javascript:", ""));
            return false;
        });

        $("#btn_curr_type").click(function () {
            $(".dropdown_type").toggle();
            $(".dropdown_key").hide();
            return false;
        });



        $(".ul_chile_key li").mouseover(function () {
            var selectedId = $(".ChildKey").val();
            $(".ul_chile_key li:not([data-id=" + selectedId + "])").removeClass("on");
            $(this).addClass("on");
        });
        $(".ul_chile_key li").mouseout(function () {
            var selectedId = $(".ChildKey").val();
            $(".ul_chile_key li:not([data-id=" + selectedId + "])").removeClass("on");
        })

        $(".ul_chile_key li").click(function () {
            $(".ChildKey").val($(this).attr("data-id"));
            $(".dropdown_key").toggle();

            eval($(".btn_submit").attr("href").replace("javascript:", ""));
            return false;
        });


        $("#btn_child_key").click(function () {
            $(".dropdown_key").toggle();
            $(".dropdown_type").hide();
            return false;
        });


        // 정령
        $(".order").click(function () {
            var order_name = $(this).attr("data-name");

            if ($(".order_name").val() == order_name) {
                $(".order_type").val($(".order_type").val() == "asc" ? "desc" : "asc");
            } else {
                $(".order_type").val("desc");
            }
            $(".order_name").val(order_name);
            eval($(".btn_submit").attr("href").replace("javascript:", ""));
            return false;
        });

        $("body").click(function () {
            $(".dropdown_key").hide();
            $(".dropdown_type").hide();
        });

        $page.init();
    });


    var $page = {
        init: function () {

            var order_name = $(".order_name").val();
            var order_type = $(".order_type").val();

            $(".order span").text("▽");
            var orderMark = "▼";
            if (order_type == "asc") {
                orderMark = "▲";
            }

            $(".order[data-name=" + order_name + "] > span").text(orderMark);


            $(".letter_type li[data-id=" + $(".CorrType").val() + "]").addClass("on");
            $(".ul_chile_key li[data-id=" + $(".ChildKey").val() + "]").addClass("on");
        }


    }
//-->
</script>

<input type="hidden" id="CorrType" class="CorrType" runat="server"/>
<input type="hidden" id="ChildKey" class="ChildKey" runat="server"/>
<input type="hidden" id="order_type" class="order_type" runat="server"/>
<input type="hidden" id="order_name" class="order_name" runat="server"/>
<asp:LinkButton runat=server ID=btn_submit CssClass=btn_submit onclick="btn_submit_Click"></asp:LinkButton>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td colspan="2">
            <div style="text-align: left">
            <asp:Label ID="lblTotalCount" runat="server" Text="Label"></asp:Label>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div style="text-align:left">
            편지ID : <asp:TextBox runat="server" ID="CorrID" class="text CorrID"></asp:TextBox>
            <asp:ImageButton ID="btn_search" runat="server" title="편지ID로 편지를 조회합니다." ImageUrl="../image/imate/btn/btn_search.jpg" onclick="btn_submit_Click"/>&nbsp    
            </div>
        </td>
        <td>
            <div style="text-align: right">
                <asp:RadioButton ID="rdoChild" GroupName="RadioCorrType" Text="어린이편지" runat="server" Checked="true" AutoPostBack="true"/>&nbsp&nbsp
                <asp:RadioButton ID="rdoSponsor" GroupName="RadioCorrType" Text="후원자편지" runat="server"  AutoPostBack="true"/>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                <%--편지개수:&nbsp--%>
                기간 : &nbsp<input type="text" id="start_date" name="start_date" size="10" maxlength="10" value="" style="ime-mode: disabled; height: 20px; line-height: 20px;" class="sbox" onkeydown="javascript:OnlyNumber();" /> &nbsp; ~ &nbsp;
                <input type="text" id="end_date" name="end_date" size="10" maxlength="10" value="" style="ime-mode: disabled; height: 20px; line-height: 20px;" class="sbox" onkeydown="javascript:OnlyNumber();" />&nbsp;&nbsp;
                <asp:ImageButton ID="imgBtnSearch" runat="server" OnClick="imgBtnSearch_Click" ImageUrl="~/image/imate/btn/btn_search.jpg" ToolTip="기간 설정 후, 조회합니다." />
            </div>
        </td>
    </tr>
</table>
<br/>
    
<table border="0" cellpadding="0" cellspacing="0" class="mate-list">
    <thead>
        <tr>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                번역완료
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                번역자ID
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                <a href="#" class="order" data-name="TranslationName">번역자<span>▽</span></a>
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                스크리닝완료
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                    <a href="#" class="order" data-name="ScanningDate">스캔일자<span>▽</span></a>
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                    <a href="#" class="order" data-name="CorrID">편지ID<span>▽</span></a>
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                패키지ID
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;' >
                <a href="#" id="btn_curr_type">편지타입▼</a>
                <div class="dropdown dropdown_type" style="display:none;">
                    <ul runat="server" id="letter_type" class="letter_type">
                    </ul>
                </div>
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                <a href="#" id="btn_child_key">어린이키▼</a>
                <div class="dropdown dropdown_key" style="display:none;">
                    <ul runat="server" id="ul_chile_key" class="ul_chile_key">
                    </ul>
                </div>

            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    어린이명
            </th>
                <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    후원자키
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    후원자명
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    쪽수
            </th>
        </tr>
    </thead>
    <tbody>
        <asp:Repeater ID="repData" runat="server">
            <ItemTemplate>
                <tr>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "TranslationDate", "{0:yyyy-MM-dd}")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "TranslationID")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "TranslationName")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ScreeningEnd", "{0:yyyy-MM-dd}")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ScanningDate", "{0:yyyy-MM-dd}")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px; cursor: pointer;'  bgcolor="#C6DFFF" onclick="ShowDetail('<%#DataBinder.Eval(Container.DataItem, "CorrID")%>','<%#DataBinder.Eval(Container.DataItem, "PackageID")%>','<%#DataBinder.Eval(Container.DataItem, "CorrPage")%>'); return false;">
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "CorrID")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "PackageID")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "CorrType")%></center>
                    </td>
                    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ChildKey")%></center></td>
                    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ChildName")%></center></td>
                    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ConID")%></center></td>
                    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "SponsorName")%></center></td>
                    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "CorrPage")%></center></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </tbody>
</table>

<% //추가 2013-08-28 %>
<input type="hidden" id="hdIdxPage" name="hdIdxPage" runat="server" />

<!-- paginator -->
<div class="paginator">
    <taeyo:PagingHelper ID="PagingHelper1" runat="server" OnOnPageIndexChanged="PagingHelper1_OnPageIndexChanged"
        CurrnetNumberColor="Silver" ForeColor="#404040"></taeyo:PagingHelper>
</div>
<!-- // paginator -->
