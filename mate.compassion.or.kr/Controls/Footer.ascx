﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="Controls_Footer" %>

<style>
    


</style>
<!-- footer -->
    <footer class="footer">
		<div class="w980">
			<div class="fb_area1">
				<ul class="util_menu">
					<li><a runat="server" id="footer_link_terms" target="_blank" style="color:#767676;">이용약관</a></li>
					<li><a runat="server" id="footer_link_privacy" target="_blank">개인정보 처리방침</a></li>
					<li><a runat="server" id="footer_link_recruit" target="_blank" style="color:#767676;">인재채용</a></li>
					<li class="noline"><a href="#" runat="server" id="footer_link_faq" target="_blank" style="color:#767676;">FAQ</a></li>
                    <li><a target="_blank" href="http://www.huheye.com" class="gallery">허호아이갤러리</a></li>
				</ul>
				<ul class="ft_sns">
                    <li><a target="_blank" href="https://www.facebook.com/compassion.Korea" class="facebook">fackbook</a></li>
                    <li><a target="_blank" href="https://www.youtube.com/user/CompassionKR" class="youtube">youtube</a></li>
                    <li><a target="_blank" href="https://www.instagram.com/compassionkorea/" class="instargram">instargram</a></li>
                    <li><a target="_blank" href="https://twitter.com/compassionkorea" class="twitter">twitter</a></li>
                    <li><a target="_blank" href="http://plus.kakao.com/home/jsd0z8yd" class="yellow">yellow</a></li>
                    <li><a target="_blank" href="http://happylog.naver.com/compassion.do" class="naver_cafe">naver_cafe</a></li>
				</ul>
			</div>
			<div class="fb_area2">
				<ul class="ft_map">
				</ul>
			</div>
			<div class="fb_area3">
				<p class="ft_logo">
					<img src="/common/img/common/ft_logo.png" alt="푸터 컴패션 로고" />
				</p>
				<div class="address">
                    <ul>
                        <li><em>한국컴패션</em> 사업자등록번호 : 108-82-05789</li>
                        <li>대표자 : 서정인 </li>
                    </ul>
                    <ul>
                        <li><!--em>서울본사</em-->(04418) 서울시 용산구 한남대로 102-5 (한남동 723-41) 석전빌딩</li>

                    </ul>
                    <ul>
                        <li>후원상담/안내 : 02-740-1000(평일 09:00 ~ 18:00)</li>
                        <li>팩스 : 02-740-1001</li>
                        <li>이메일 : info@compassion.or.kr</li>
                    </ul>
                    <!--p><em>부산지사</em> (48733) 부산시 동구 중앙대로 216 (초량 3동 1199-9번지) 교원빌딩 17층</p-->
                    <p class="copyright">&copy; COMPASSION KOREA All Rights Reserved. Contact us for more information</p>
                </div>

			</div>
		</div>

	</footer>
    <!--// footer -->




<!--로거적용-->
<script type="text/javascript">    var _TRK_LID = "13269"; var _L_TD = "trk1.logger.co.kr"; var _FC_TRK_CN = "10051";</script>
<script type="text/javascript">    var _CDN_DOMAIN = location.protocol == "https:" ? "https://fs.bizspring.net" : "http://fs.bizspring.net"; document.write(unescape("%3Cscript src='" + _CDN_DOMAIN + "/fs4/bstrk.js' type='text/javascript'%3E%3C/script%3E"));</script>
<noscript><img alt="Logger Script" width="1" height="1" src="http://trk1.logger.co.kr/tracker.tsp?u=13269&amp;js=N" /></noscript>

<% if (Request.Url.Host.Equals("mate.compassion.or.kr")) { %>
<!--Google Analytics 적용-->
<%--<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39495914-18']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>--%>
<% } %>

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-39495914-18"></script> 
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());
    gtag('config', 'UA-39495914-18', { 'send_page_view': false });
</script> 

<!-- Google Tag Manager dataLayer for global header --> 
<script type='text/javascript'> 
    dataLayer = [{   
        'dimension1':'<%=this.ViewState["dimension1"].ToString()%>',
        'dimension2':'<%=this.ViewState["dimension2"].ToString()%>',
        'dimension3':'<%=this.ViewState["dimension3"].ToString()%>',
        'dimension4':'<%=this.ViewState["dimension4"].ToString()%>',
        'dimension5':'<%=this.ViewState["dimension5"].ToString()%>',
        'dimension6':'<%=this.ViewState["dimension6"].ToString()%>',
        'dimension7':'<%=this.ViewState["dimension7"].ToString()%>',
        'dimension8':'<%=this.ViewState["dimension8"].ToString()%>',
        'dimension9':'<%=this.ViewState["dimension9"].ToString()%>',
        'dimension10':'<%=this.ViewState["dimension10"].ToString()%>',
        'dimension11':'<%=this.ViewState["dimension11"].ToString()%>',
        'dimension12':'<%=this.ViewState["dimension12"].ToString()%>',
    }];

    console.log(dataLayer);
</script>
<!-- End Google Tag Manager Data Layer -->
    
<!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) { w[l] = w[l] || []; w[l].push({ 'gtm.start': 
new Date().getTime(), event: 'gtm.js' }); var f = d.getElementsByTagName(s)[0], 
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src = 
'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-MW8PTXT');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) --> 
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MW8PTXT" 
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> 
<!-- End Google Tag Manager (noscript) -->

<!-- // footer -->
