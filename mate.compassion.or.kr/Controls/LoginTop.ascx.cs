﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_LoginTop : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (UserInfo.IsLogin)
            {
                UserInfo user = new UserInfo();

                login.Attributes.Add("style", "display:none;");
                logout.Attributes.Add("style", "display:inline-block; *display:inline; zoom:1;");

                if (user.ConId.Length > 6)
                    labName2.Text = user.UserId + " 님";

                else
                    labName2.Text = user.UserId + "(" + user.ConId + ") 님";

                //aRegister.CssClass = "l-mypage";
                //aRegister.Text = "MY PAGE";

                //aLogin.CssClass = "l-logout";
                //aLogin.Text = "로그아웃";
                //aLogin.NavigateUrl = "/LogOff.aspx"; 
       
                //if ((String.IsNullOrEmpty(user.CommitCount) || user.CommitCount == "0")
                //&& (String.IsNullOrEmpty(user.ApplyCount) || user.ApplyCount == "0"))
                //{
                //    labName1.Text = user.UserName;
                //    sMember.Visible = false;
                //    gMember.Visible = true;
                //    aRegister.NavigateUrl = "/Mypage/OneToOneList.aspx";
                //}

                //else
                //{
                //    sMember.Visible = true;
                //    gMember.Visible = false;
                //    labName2.Text = user.UserName + "(" + user.ConId + ")";
                //    //labSponID.Text = "(" + user.SponsorID + ")";
                //    aRegister.NavigateUrl = "/Mypage/ApplyInfo.aspx";
                //}
            }

            else
            {
                login.Attributes.Add("style", "display:inline-block; *display:inline; zoom:1;");
                logout.Attributes.Add("style", "display:none;");

                //aRegister.CssClass = "l-join";
                //aRegister.Text = "회원가입";
                //aRegister.NavigateUrl = "/Membership/RegisterAdult.aspx";

                //aLogin.CssClass = "l-login";
                //aLogin.Text = "로그인";
                //aLogin.NavigateUrl = "/MemberShip/Login.aspx";

                //sMember.Visible = false;
                //gMember.Visible = false;
            }
        }
    }
}