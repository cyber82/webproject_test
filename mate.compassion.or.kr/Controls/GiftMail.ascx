﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GiftMail.ascx.cs" Inherits="Controls_GiftMail" %>
<table class="list1" border="1">
	<tbody>
		<tr>
			<th><img src="/image/text/td-mail-title.gif" alt="메일 제목" /></th>
			<td><input type="text" class="text" runat="server" id="subject"/></td>
		</tr>
		<tr>
			<th><img src="/image/text/td-email-sender.gif" alt="보내는 사람 이메일" /></th>
			<td><input type="text" class="text" runat="server" id="sendEmail" /></td>
		</tr>
		<tr>
			<th><img src="/image/text/td-email-receiver.gif" alt="받는 사람 이메일" /></th>
			<td><input type="text" class="text text-sender" runat="server" id="receiveEmail" /></td>
		</tr>
		<tr>
			<th><img src="/image/text/td-message1.gif" alt="전달할 메세지" /></th>
			<td><textarea rows="10" cols="50" runat="server" id="comment">
어린이를 위한 가치 있는 선물, Gift of Compassion
컴패션의 Gift of Compassion은 도움이 필요한 나라의 어린이들에게 특별한
선물을 보내는 또 다른 사랑의 표현이자 소중한 후원입니다.
예방주사, 영양제, 과학교실 등 다양한 분야를 지원해 온전한 어린이 양육이
이루어지도록 작은 선물을 보내주시지 않으실래요?

한국컴패션은?
1952년 한국전쟁의 폐허 속에서 한국 고아들을 돕기 위해
시작된 국제어린이양육기구 컴패션은 1993년까지 10만 명의 한국 어린이들을
길러냈습니다.
2003년 수혜국에서 후원국이 된 한국을 포함한 12개 후원국가와 함께 현재,
전 세계 25개국 130만 여명의 가난한 어린이들이 밝은 미래를 꿈꿀 수
있도록 1:1 결연을 통해 양육하고 있습니다.
</textarea></td>
		</tr>
	</tbody>
</table>
<script language="javascript" type="text/javascript">
    function ValidateEmail() {
        if (document.getElementById("<%=subject.ClientID%>").value == "") {
            alert("메일 제목을 입력 하세요.");
            return false;
        }
        if (document.getElementById("<%=sendEmail.ClientID%>").value == "") {
            alert("보내는 사람 이메일을 입력 하세요.");
            return false;
        }
        if (document.getElementById("<%=receiveEmail.ClientID%>").value == "") {
            alert("받는 사람 이메일을 입력 하세요.");
            return false;
        }
        if (document.getElementById("<%=comment.ClientID%>").value == "") {
            alert("전달할 메시지를 입력 하세요.");
            return false;
        }
        return true;
    }
</script>
<div class="btn-m">
    <asp:Button ID="btnMail" runat="server" Text="" CssClass="btn btn-ask-friend1" 
        onclick="btnMail_Click" OnClientClick ="return ValidateEmail();" />
</div>