﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberShipRight.ascx.cs" Inherits="Controls_MemberShipRight" %>

<%@ Register src="/Controls/RightBanner.ascx" tagname="Banner" tagprefix="bc1" %>

<div id="sidebar" class="sec-etc">
	<div class="tit">부가정보 ETC</div>

	<ul id="lnb" class="lnb">
		<li id="sub1" runat ="server" ><a href="/MemberShip/Login.aspx" class="dep1"><span>로그인</span></a></li>
		<li id="sub2" runat ="server" ><a href="/MemberShip/RegisterAdult.aspx" class="dep2"><span>회원가입</span></a></li>
		<li id="sub3" runat ="server" ><a href="/MemberShip/FindIDLocal.aspx" class="dep3"><span>아이디/비밀번호 찾기</span></a></li>
		<%--<li id="sub4"><a href="/MemberShip/Sitemap.aspx" class="dep4"><span>사이트맵</span></a></li>--%>
		<li id="sub5"><a href="/MemberShip/GuideCompassion.aspx" class="dep5"><span>후원자 가이드</span></a>
			<ul id="subEl5" runat="server">
				<li id="sub5_1"><a href="/MemberShip/GuideCompassion.aspx" class="dep5-1"><span>컴패션과 함께 하는 행복한 세상</span></a></li>
				<li id="sub5_2"><a href="/MemberShip/GuideCsp.aspx" class="dep5-2"><span>태아영아 생존 프로그램</span></a></li>
				<li id="sub5_3"><a href="/MemberShip/GuideCdsp.aspx" class="dep5-3"><span>1:1 어린이 양육프로그램</span></a></li>
				<%--<li id="sub5_4"><a href="/MemberShip/GuideLdp.aspx" class="dep5-4"><span>1:1 리더쉽 결연프로그램</span></a></li>--%>
				<li id="sub5_5"><a href="/MemberShip/GuideCiv.aspx" class="dep5-5"><span>양육을 돕는 후원</span></a></li>
				<li id="sub5_6"><a href="/MemberShip/GuideGetInvolved.aspx" class="dep5-6"><span>컴패션 백배 즐기기</span></a></li>
				<li id="sub5_7"><a href="/MemberShip/GuidePayment.aspx" class="dep5-7"><span>후원금 납부 안내</span></a></li>
			</ul>
		</li>
		<li id="sub6"><a href="/MemberShip/Faq.aspx" class="dep6"><span>자주 묻는 질문</span></a></li>
		<li id="sub7"><a href="/MemberShip/Terms.aspx" class="dep7"><span>이용약관</span></a></li>
		<li id="sub8"><a href="/MemberShip/Privacy.aspx" class="dep8"><span>개인정보 취급방침</span></a></li>
	</ul>

	<ul class="banner1">
		<% //수정 2013-05-28 %>
        <bc1:Banner ID="banner1" runat="server" />
		
        <% //주석처리 2013-05-28 %>
        <%--<li><a href="/AboutUs/noticeview.aspx?iTableIndex=1001&iNoIndex=318&iBoardIdx=67376&ref=47200&re_step=0&pageIdx=0&searchOption=&searchTxt="><img src="/image/banner/charity-navigator.png" width="240" height="100" alt="" /></a></li>
		<li><a href="/Intro/MateIntro.aspx"><img src="/image/banner/mate.png" width="240" height="90" alt="" /></a></li>
        <li><a href="http://www.hanscake.co.kr/2011/story/responsibility.php" target="_blank"><img src="/image/banner/hanscake.png" width="240" height="92" alt="" /></a></li>
		<li><a href="/Share/VisionTripApply.aspx"><img src="/image/banner/vision-trip2012.png" width="240" height="90" alt="" /></a></li>--%>
	</ul>
</div>