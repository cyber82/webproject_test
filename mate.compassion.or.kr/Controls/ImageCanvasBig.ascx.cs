﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Data;

public partial class Controls_ImageCanvasBig : System.Web.UI.UserControl
{
    public Bitmap bitmap;
    public ContentType ImageType = ContentType.Jpeg;



    void Page_Load(Object sender, EventArgs e)
    {
        //이미지컨트롤에 Attribute등록
        String Url = Request.Url.ToString();

        if (!IsPostBack)
        {
            try
            {
                if (Request.QueryString["ChildMasterID"] != null && Request.QueryString["ImageID"] != null)
                {
                    string sChildMasterID = Request.QueryString["ChildMasterID"].ToString();
                    int iRowCount = Convert.ToInt32(Request.QueryString["ImageID"].ToString());

                    WWWService.Service _wwwService = new WWWService.Service();

                    //- 어린이이미지정보 가져오기
                    //DataSet dsImg = _wwwService.GetChildImg(sChildMasterID);
                    DataSet dsImg = _wwwService.getChildImageList_New(sChildMasterID);

                    //- ImageSort
                    //dsImg.Tables[0].DefaultView.Sort = "ImageDate desc";

                    DataTable dtImg = dsImg.Tables[0].DefaultView.ToTable();

                    if (dtImg != null && dtImg.Rows[iRowCount] != null)
                    {
                        byte[] info = new byte[0];

                        info = (byte[])dtImg.Rows[iRowCount]["ChildImage"]; //어린이 이미지

                        Stream ss = new MemoryStream(info);
                        System.Drawing.Image img = System.Drawing.Image.FromStream(ss);
                        Bitmap bImg = new Bitmap(img, 400, 600);
                        Graphics g = Graphics.FromImage(bImg);
                        g.DrawImage(bImg, 0, 0, 200, 300);
                        bitmap = bImg;
                    }
                }
            }

            catch
            {
                string sjs = string.Empty;
                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("이미지 로드 중 에러가 발생했습니다.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());

                return;
            }

            try
            {
                if (Request.Params[ID] != null)
                {
                    Response.Clear();
                    Response.ContentType = "Image/" + ImageType.ToString();
                    bitmap.Save(Response.OutputStream, getFormat((int)ImageType));
                    Response.End();
                }

                if (Url.IndexOf("?") == -1)
                {
                    DyImageBig.ImageUrl = Url + "?" + ID + "=Show";
                }

                else
                {
                    DyImageBig.ImageUrl = Url + "&" + ID + "=Show";
                }
            }

            catch (Exception)
            {
                DyImageBig.ImageUrl = "../images/NoImage.gif";
            }
        }
    }

    ImageFormat getFormat(int i)
    {
        ImageFormat imgfmt = ImageFormat.Jpeg;
        switch (i)
        {
            case 0:
                imgfmt = ImageFormat.Gif;
                break;

            case 1:
                imgfmt = ImageFormat.Jpeg;
                break;

            case 2:
                imgfmt = ImageFormat.Png;
                break;
        }

        return imgfmt;
    }

    public enum ContentType
    {
        Gif = 0, Jpeg = 1, Png = 2
    }
}