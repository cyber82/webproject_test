﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data; 

public partial class Controls_PaymentRight : System.Web.UI.UserControl
{
    IamWeb.IamCompassionWeb _iamWeb = new IamWeb.IamCompassionWeb();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BeLovedChildBind();
        }
    }
    private void BeLovedChildBind()
    {
        WWWService.Service _wwwService = new WWWService.Service();

        string code = _wwwService.ConfigCode("main_ssb");

        DataSet data = _wwwService.MainSsbList(code);
        //DataSet data = _iamWeb.GetBeLovedChildList("prayer");
        repLoved.DataSource = data.Tables[0];
        repLoved.DataBind();
    }
}