﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Text;

public partial class Mate_Controls_ThirdPLMainList : System.Web.UI.UserControl
{
    int _PAGESIZE = 10;
    int _tableIdx;
    bool bgGetLetter = true;

    protected string _pageIdx = "0";

    public string sgScreeningStatusName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        string sJs;

        if (UserInfo.IsLogin)
        {
            string sUserId = new UserInfo().UserId;

            //권한 체크
            Object[] objSql = new object[1] { "MATE_MateYearCheck" };
            Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
            Object[] objValue = new object[2] { sUserId, "31" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //권한체크
            if (data.Tables[0].Rows.Count > 0)
            {
                DataBind(Convert.ToInt32(_pageIdx));
                //페이지 타입 쿠키 저장
                new TranslateInfo().MatePageType = "40";
            }

            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                sb.Append("alert('해당 권한이 없습니다.'); history.back();");
                sb.Append("</script>");

                Response.Write(sb.ToString());
            }

            data.Dispose();
            _WWW6Service.Dispose();
        }

        else
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("로그인 후 사용 가능합니다.");
			//sJs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=" + Server.UrlEncode(Request.Url.AbsoluteUri));
			sJs += JavaScript.GetPageMoveScript("/login.aspx");
			sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }
    }

    private void DataBind(int idxPage)
    {
        string sUserId = new UserInfo().UserId;
        //sUserId = "aeii";//테스트용 임시

        //기본 가져올 편지 개수 "1"
        if (string.IsNullOrEmpty(this.txtGetLetterCount.Text))
        {
            this.txtGetLetterCount.Text = "1";
        }

        StringBuilder sbList = new StringBuilder();

        sbList.Append("SELECT (ThirdYear + ProjectNumber) AS LetterID, ");
        sbList.Append("FieldOffice,ProjectNumber, ProjectName, ActualAuthorRole, AuthorName, ThirdYear, ");
        sbList.Append("dbo.uf_GetCommonName('THIRD_TRANS_STATUS', TranslateStatus) AS TranslateStatusName ");
        sbList.Append("FROM ThirdPL_Master ");
        sbList.Append("WHERE TranslateID = '" + sUserId + "' AND TranslateStatus = 'START'");

        //Response.Write(sbList.ToString());
        //Response.End();

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        //번역 대기 중인 편지
        Object[] objSql = new object[1] { sbList.ToString() };

        DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

        //리스트 카운터
        this.lblTotalCount.Text = "Total : " + data.Tables[0].Rows.Count;

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = data.Tables[0].DefaultView;
        pds.AllowPaging = true;
        pds.PageSize = _PAGESIZE;
        pds.CurrentPageIndex = idxPage;
        repData.DataSource = pds;
        repData.DataBind();

        PagingSetting(data.Tables[0].Rows.Count, idxPage);

        data.Dispose();
        _WWW6Service.Dispose();
    }

    private void PagingSetting(int totalCount, int curPage)
    {
        PagingHelper1.VirtualItemCount = totalCount;
        PagingHelper1.PageSize = _PAGESIZE;
        PagingHelper1.CurrentPageIndex = curPage;
    }

    protected void PagingHelper1_OnPageIndexChanged(object sender, TaeyoNetLib.PagingEventArgs e)
    {
        DataBind(e.PageIndex);
        _pageIdx = e.PageIndex.ToString();
    }

    protected void imgBtnGetLetter_Click(object sender, ImageClickEventArgs e)
    {
        int iResult = 0;
        string sjs = "";

        try
        {
            if (bgGetLetter)
            {
                string sUserId = new UserInfo().UserId;
                //sUserId = "aeii";//테스트용 임시
                string sUserName = new UserInfo().UserName;

                WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

                //편지 가져오기
                Object[] objSql = new object[1] { "MATE_ThirdPLSave" };
                Object[] objParam = new object[4] { "TranslateID", "LetterCnt", "ModifyID", "ModifyName" };
                Object[] objValue = new object[4] { sUserId, txtGetLetterCount.Text, sUserId, sUserName };//3PL

                iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

                _WWW6Service.Dispose();

                if (iResult > 0)
                {
                    //sjs = JavaScript.HeaderScript.ToString();
                    //sjs += JavaScript.GetAlertScript("편지를 가져 왔습니다.");
                    //sjs += JavaScript.FooterScript.ToString();
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "Alter", sjs);
                    DataBind(Convert.ToInt32(_pageIdx));
                    return;
                }

                else
                {
                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("가져올 편지가 없습니다.");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alter", sjs);
                    return;
                }
            }
        }

        catch (Exception ex)
        {
            //Exception Error Insert     
            WWWService.Service _wwwService = new WWWService.Service();
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("편지를 가져오는 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }
}