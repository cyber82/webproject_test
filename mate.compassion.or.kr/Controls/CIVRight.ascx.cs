﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data; 

public partial class Controls_CIVRight : System.Web.UI.UserControl
{
    IamWeb.IamCompassionWeb _iamWeb = new IamWeb.IamCompassionWeb(); 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BeLovedChildBind();
            btnRadio_Click(this, null);
        }
    }
    protected void btnSupport_Click(object sender, EventArgs e)
    {
        string money = "";
        if (dropMoney.Visible == true)
        {
            money = dropMoney.SelectedValue.ToString();
        }
        else
        {
            money = txtPrice.Text;
        }

        CartCICSVGIFT.SaveRelationCookie("CIV"
           , dropSponItem.SelectedValue
           , dropSponItem.SelectedValue
           , "90000000000000000"
           , dropSponItem.Text
           , dropFrequency.SelectedValue
           , Convert.ToDecimal(money));

        //수정 20120426 로그인 하지 않을때 로그인페이지에서 로그인 한 후에도 바로 장바구니페이지로 이동 할 수 있도록 링크
        //Response.Redirect("/Payment/CartProgram.aspx");
        Response.Redirect("/membership/login.aspx?returnUrl=/Payment/CartProgram.aspx");
    }

    private void BeLovedChildBind()
    {
        WWWService.Service _wwwService = new WWWService.Service();

        string code = _wwwService.ConfigCode("main_ssb");

        DataSet data = _wwwService.MainSsbList(code);
        //DataSet data = _iamWeb.GetBeLovedChildList("child");
        repLoved.DataSource = data.Tables[0];
        repLoved.DataBind();
    }

    //추가 20120409 라디오버튼 후원금
    protected void btnRadio_Click(object sender, EventArgs e)
    {
        if (dropFrequency.SelectedValue == "정기후원")
        {
            dropMoney.Visible = true;
            dropMoney.Items.Clear();
            ListItem[] li = new ListItem[3];
            for (int i = 0; i < li.Length; i++)
            {
                li[i] = new ListItem();
                switch (i)
                {
                    //수정 20120410
                    //case 0:
                    //    li[0].Text = "직접입력";
                    //    li[0].Value = "input";
                    //    break;
                    case 0:
                        li[0].Text = "10,000";
                        li[0].Value = "10000";
                        break;
                    case 1:
                        li[1].Text = "20,000";
                        li[1].Value = "20000";
                        break;
                    case 2:
                        li[2].Text = "30,000";
                        li[2].Value = "30000";
                        break;
                }
            }

            dropMoney.Items.AddRange(li);
            dropMoney.DataBind();
            txtPrice.Visible = false;
            spanPrice.Visible = false;
        }
        else
        {
            dropMoney.Visible = false;
            txtPrice.Visible = true;
            spanPrice.Visible = true;
            txtPrice.Text = "";
        }

    }
}