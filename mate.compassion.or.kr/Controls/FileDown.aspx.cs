﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using DEXTUpload.NET;
using Benecorp.Web;

public partial class Controls_FileDown : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string FileName = Request.QueryString["filename"];
        //string FilePath = Request.QueryString["filepath"];

        //using (DEXTUpload.NET.FileDownload fileDownload = new DEXTUpload.NET.FileDownload())
        //{		
        //    //objDownload.Download(filepath, 파일이름, 강제저장유무(True:강제 저장), 이어받기 유무(True:이어받기))
        //    fileDownload.Download(FilePath, FileName, true, true);
        //}

        string sBaseDir = Server.MapPath("/Files") + "/Board/";

        FileInfo fi = new FileInfo(Path.Combine(sBaseDir, FileName));

        if (fi.Exists == false)
        {
            //돌아갈페이지 스크립트
            string sJs = string.Empty;
            sJs = JavaScript.HeaderScript.ToString();
            sJs += JavaScript.GetAlertScript("파일이 존재하지않아 다운로드를 중지합니다.");
            sJs += JavaScript.GetHistoryBackScript();
            sJs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sJs);
            return;
        }

        //강제 다운로드
        Response.Clear();//버퍼 비우기
        Response.ContentType = //바이너리 파일
            "application/octet-stream";
        Response.AddHeader(//파일 다운로드명 설정
            "Content-Disposition",
            "attachment;filename=" +
            Server.UrlPathEncode(FileName));//한글처리
        Response.WriteFile(	//다운로드
            Path.Combine(sBaseDir, FileName));
        //[!] 다운로드 횟수 증가
        Response.End();//버퍼 비우고, 종료.
    }
}