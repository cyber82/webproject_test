﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data; 

public partial class Controls_GIFTRight : System.Web.UI.UserControl
{
    string sjs = String.Empty;
    WWWService.Service _wwwService = new WWWService.Service();
    IamWeb.IamCompassionWeb _iamWeb = new IamWeb.IamCompassionWeb(); 

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ChildBind();
            BeLovedChildBind();
        }
    }

    private void ChildBind()
    {
        DataSet data = _wwwService.listSponsorshipApplyOrderBy("4", "", "");
        repChild.DataSource = data.Tables[0];
        repChild.DataBind();
    }

    private void BeLovedChildBind()
    {
        WWWService.Service _wwwService = new WWWService.Service();

        string code = _wwwService.ConfigCode("main_ssb");

        DataSet data = _wwwService.MainSsbList(code);
        //DataSet data = _iamWeb.GetBeLovedChildList("child");
        repLoved.DataSource = data.Tables[0];
        repLoved.DataBind();
    }

    protected string GetImgPath(object childKey)
    {
        return "http://ws.compassion.or.kr/Files/Child/" + childKey.ToString().Substring(0, 2) + "/" + childKey.ToString().Trim() + ".jpg";
    }
    protected void repChild_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Add")
        {
            UserInfo userInfo = new UserInfo();

            if (!String.IsNullOrEmpty(userInfo.SponsorID))
            {
                //인증받지 않은 기업일때    
                if (userInfo.GenderCode == "C" && String.IsNullOrEmpty(userInfo.CertifyDate))
                {
                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetPageMoveScript("/membership/GroupToCallcenter.aspx");
                    sjs += JavaScript.FooterScript.ToString();

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sjs.ToString());
                    return;
                }
                //인증받지 않은 미주일때 
                if (userInfo.LocationType == "미주" && String.IsNullOrEmpty(userInfo.CertifyDate))
                {
                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetPageMoveScript("/membership/GroupToCallcenter.aspx");
                    sjs += JavaScript.FooterScript.ToString();

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sjs.ToString());
                    return;
                }
            }

            string masterID = e.CommandArgument.ToString();

            //쿠키가 없으면 쿠키만들기 수정됨(로그인시에는 쿠키생성 안함)
            if (Request.Cookies["CooBasket"] == null && !UserInfo.IsLogin)
            {
                WWWService.Service wwwService = new WWWService.Service();
                DateTime dtNow = wwwService.GetDate();

                Response.Cookies["CooBasket"].Value = dtNow.ToString("yyyyMMddHHmmssff2");
                Response.Cookies["CooBasket"].Expires = dtNow.AddHours(24);
                //Request.Cookies.Set(Response.Cookies["CooBasket"]);
            }
            //장바구니테이블에 어린이 등록
            if (UserInfo.IsLogin)
                registerChildEnsure("", masterID, userInfo.SponsorID);
            else
                registerChildEnsure(Request.Cookies["CooBasket"].Value, masterID, "");
        }
    }

    protected bool registerChildEnsure(string cookieValue, string masterID, string sponsorID)
    {
        //세션선언
        UserInfo sess = new UserInfo();
        DataSet ds = new DataSet();
        string sResult = "";
        WWWService.Service wwwService = new WWWService.Service();

        try
        {
            //쿠키가 있으면 장바구니 테이블 Data 등록
            ds = wwwService.getDATChildEnsure(cookieValue
                                               , masterID
                                               , sponsorID);
        }
        catch (Exception ex)
        {
            WWWService.Service _wwwService = new WWWService.Service();
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sMsg = ex.Message;

            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("선택한 어린이 정보를 체크 중 오류가 발생했습니다. \\r\\n" +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sjs);

            return false;
        }
        if (ds == null) //DB Error
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("선택한 어린이 정보를 체크 중 오류가 발생했습니다.\\r\\n관리자에게 문의해주세요.");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sjs);

            return false;
        }
        if (ds.Tables[0].Rows.Count > 0)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("이미 선택하신 어린이입니다.");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sjs);

            return true;
        }

        // Insert Data
        try
        {
            //쿠키가 있으면 장바구니 테이블 Data 등록
            sResult = wwwService.registerDATChildEnsure(cookieValue,
                                                         sponsorID, //후원자ID
                                                         masterID,
                                                         DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") //등록일
                                                         , "0"
                                                        );
        }
        catch (Exception ex)
        {
            wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sMsg = ex.Message;

            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("선택하신 어린이를 등록 중 오류가 발생했습니다.. \\r\\n" +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sjs);

            return false;
        }
        if (sResult.Substring(0, 2) == "30") //DB Error
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("선택하신 어린이를 등록하지 못했습니다. \\r\\n" +
                                              sResult.Substring(2).ToString().Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sjs);

            return false;
        }
        else
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetPageMoveScript("/Payment/CartProgram.aspx");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GoUrl", sjs);
        }

        return true;
    }
}