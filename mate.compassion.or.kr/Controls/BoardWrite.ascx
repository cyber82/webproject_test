﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BoardWrite.ascx.cs" Inherits="Mate_Controls_BoardWrite" %>
<table class="write1">
	<tbody>
		<tr>
			<th class="td-writer"><img src="/image/mate/text/td-writer.gif" alt="작성자" /></th>
			<td><%=UserName%></td>
		</tr>
		<tr>
			<th><img src="/image/text/td-title.gif" alt="제목" /></th>
			<td><input type="text" class="text" style="width:344px;height:20px;" id="txtTitle" runat ="server"/></td>
		</tr>		
	</tbody>
</table>
<div>
    <%--<textarea rows="23" cols="105" id="txtComment" runat ="server" style="border-width:2px"></textarea>--%>
    <textarea name="txtComment" id="txtComment" runat ="server" style="width:656px;height:320px;display:none;" class="sbox"></textarea>
</div>
 <table class="write1">
	<tbody>		
    <tr>
		<th><img src="/image/text/td-attached-file.gif" alt="첨부파일" /></th>
		<td>
			<div style="height:22px"><input style ="width:550px; height:18px "  type="file" title="파일선택" name="files"/></div>
            <div style="height:22px"><input style ="width:550px; height:18px "  type="file" title="파일선택" name="files"/></div>
            <div style="height:22px"><input style ="width:550px; height:18px "  type="file" title="파일선택" name="files"/></div>
            <div style="height:22px"><input style ="width:550px; height:18px "  type="file" title="파일선택" name="files"/></div>
            <div style="height:22px"><input style ="width:550px; height:18px "  type="file" title="파일선택" name="files"/></div>
			<p class="txt-bold1">첨부파일은 최대 5개까지 가능하며, 개당 파일의 용량은 2MB로 제한됩니다.</p>								
		</td>
	</tr>
     <tr>
        <th>기존첨부파일</th>
        <td><asp:Literal ID="litFiles" runat="server"></asp:Literal></td>
    </tr>
</tbody>
</table>
<div class="btn-list">
	<div class="btn-r">
         <asp:Button ID="btnSave" runat="server" Text="" CssClass ="btn btn-modify" 
                        onclick="btnSave_Click" OnClientClick ="var ret = WriteValidate(); updateEditorContent(); return ret;" />
		<a href="javascript:history.back(-1);" class="btn btn-cancel"><span>취소하기</span></a>
	</div>
</div>
<asp:HiddenField ID="hidNoIndex" runat="server" />
<asp:HiddenField ID="hidBoardIndex" runat="server" />

<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/common/smartEditor/initEditor.js"></script>
<script type="text/javascript">
    //tinymce_config("b_content");
    $(function () {
        image_path = "/images/board/";
        initEditor(oEditors, "BoardWrite1_txtComment");
    });

    function updateEditorContent() {
        oEditors.getById["BoardWrite1_txtComment"].exec("UPDATE_CONTENTS_FIELD", []);
    }
</script>
