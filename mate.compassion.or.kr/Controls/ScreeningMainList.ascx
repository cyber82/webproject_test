﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ScreeningMainList.ascx.cs" Inherits="Mate_Controls_ScreeningMainList" %>
<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<style>
    .dropdown{position:absolute;z-index:100000;background-color:#fff;width:100px;}
    .dropdown ul{border:1px solid #CCCCCC;}
    .dropdown li{padding:5px 0;}
    .dropdown li:last-child{padding-bottom:5px;}
    .dropdown .on{background-color:#E6E6E6}
</style>
<script language="javascript">
    function ShowDetail(sCorrID, sPackageID, sPageCount, sTranslationStatus) {
        if (sTranslationStatus == '') {
            alert("초기화 된 편지입니다\n관리자에게 문의 하세요");
        }
        
        else {
            location.href = '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount;
        }
    }

    function GetLetterCheck(nListCnt, nEndCnt) {
        if (nListCnt > 0) {
            alert("이미 편지를 가져 왔습니다");
            return false;
        }

        else {
            if (nEndCnt > 0) {
                alert("이번주 번역이 완료 되었습니다");
                return false;
            }
            return true;
        }
    }

    function Test(ConID) {
        document.getElementById('td1').bgColor = '#AACE55';
    }

    $(function () {

        $(".letter_type li").mouseover(function () {
            var selectedId = $(".CorrType").val();
            $(".letter_type li:not([data-id=" + selectedId + "])").removeClass("on");
            $(this).addClass("on");
        });
        $(".letter_type li").mouseout(function () {
            var selectedId = $(".CorrType").val();
            $(".letter_type li:not([data-id=" + selectedId + "])").removeClass("on");
        })

        $(".letter_type li").click(function () {
            $(".CorrType").val($(this).attr("data-id"));
            $(".dropdown_type").toggle();

            eval($(".btn_submit").attr("href").replace("javascript:", ""));
            return false;
        });

        $("#btn_curr_type").click(function () {
            $(".dropdown_type").toggle();
            $(".dropdown_key").hide();
            return false;
        });



        $(".ul_chile_key li").mouseover(function () {
            var selectedId = $(".ChildKey").val();
            $(".ul_chile_key li:not([data-id=" + selectedId + "])").removeClass("on");
            $(this).addClass("on");
        });
        $(".ul_chile_key li").mouseout(function () {
            var selectedId = $(".ChildKey").val();
            $(".ul_chile_key li:not([data-id=" + selectedId + "])").removeClass("on");
        })

        $(".ul_chile_key li").click(function () {
            $(".ChildKey").val($(this).attr("data-id"));
            $(".dropdown_key").toggle();

            eval($(".btn_submit").attr("href").replace("javascript:", ""));
            return false;
        });


        $("#btn_child_key").click(function () {
            $(".dropdown_key").toggle();
            $(".dropdown_type").hide();
            return false;
        });


        // 정령
        $(".order").click(function () {
            var order_name = $(this).attr("data-name");
            if ($(".order_name").val() == order_name) {
                $(".order_type").val($(".order_type").val() == "asc" ? "desc" : "asc");
            } else {
                $(".order_type").val("desc");
            }
            $(".order_name").val(order_name);
            eval($(".btn_submit").attr("href").replace("javascript:", ""));
            return false;
        });

        $("body").click(function () {
            $(".dropdown_key").hide();
            $(".dropdown_type").hide();
        });

        $page.init();
    });


    var $page = {
        init: function () {

            var order_name = $(".order_name").val();
            var order_type = $(".order_type").val();

            $(".order span").text("▽");
            var orderMark = "▼";
            if (order_type == "asc") {
                orderMark = "▲";
            }

            $(".order[data-name=" + order_name + "] > span").text(orderMark);


            $(".letter_type li[data-id=" + $(".CorrType").val() + "]").addClass("on");
            $(".ul_chile_key li[data-id=" + $(".ChildKey").val() + "]").addClass("on");
        }

        
    }
</script>
<table style="width:100%">
<tr>
<td>
<div>
    <asp:Label ID="lblTotalCount" runat="server" Text="Label"></asp:Label>
</div>
<div style="text-align:left">
편지ID : <asp:TextBox runat="server" ID="CorrID" class="text CorrID"></asp:TextBox>
<asp:ImageButton ID="btn_search" runat="server" title="편지ID로 편지를 조회합니다." ImageUrl="../image/imate/btn/btn_search.jpg" onclick="btn_submit_Click"/>&nbsp    
</div>
</td>
<td>
<div style="text-align:right" >
    <%--편지개수:&nbsp--%>
    <asp:RadioButton ID="rdoChild" GroupName="RadioCorrType" Text="어린이편지" runat="server" Checked="true" AutoPostBack="true"/>&nbsp&nbsp
    <asp:RadioButton ID="rdoSponsor" GroupName="RadioCorrType" Text="후원자편지" runat="server"  AutoPostBack="true"/>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    <asp:TextBox ID="txtGetLetterCount" runat="server" CssClass="text" Width="20px" MaxLength="3"></asp:TextBox>&nbsp통&nbsp
    <asp:ImageButton ID="imgBtnGetLetter" runat="server" ImageUrl="~/image/imate/btn/btn_email.jpg" onclick="imgBtnGetLetter_Click"/>&nbsp    

    <input type="hidden" id="CorrType" class="CorrType" runat="server"/>
    <input type="hidden" id="ChildKey" class="ChildKey" runat="server"/>
    <input type="hidden" id="order_type" class="order_type" runat="server"/>
    <input type="hidden" id="order_name" class="order_name" runat="server"/>


    <asp:LinkButton runat=server ID=btn_submit CssClass=btn_submit onclick="btn_submit_Click"></asp:LinkButton>
</div>
</td>
</tr>
</table>
</br>
<table class="mate-list">
	<thead>
		<tr>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    번역완료
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    번역자ID
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                <a href="#" class="order" data-name="TranslationName">번역자<span>▽</span></a>
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    스크리닝시작
            </th>
			<th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    <a href="#" class="order" data-name="ScanningDate">스캔일자<span>▽</span></a>
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    <a href="#" class="order" data-name="CorrID">편지ID<span>▽</span></a>
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    패키지ID
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;' >
                <a href="#" id="btn_curr_type">편지타입▼</a>
                <div class="dropdown dropdown_type" style="display:none;">
                    <ul runat="server" id="letter_type" class="letter_type">
                    </ul>
                </div>
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                <a href="#" id="btn_child_key">어린이키▼</a>
                <div class="dropdown dropdown_key" style="display:none;">
                    <ul runat="server" id="ul_chile_key" class="ul_chile_key">
                    </ul>
                </div>

            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    어린이명
            </th>
             <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    후원자키
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    후원자명
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    쪽수
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    상태
            </th>
		</tr>
	</thead>
	<tbody>
        <asp:Repeater ID="repData" runat="server">
            <ItemTemplate>
		    <tr id="tr<%#DataBinder.Eval(Container.DataItem, "Idx")%>" title='<%#DataBinder.Eval(Container.DataItem, "CommentMsg")%>' >
                <script language="javascript">
                    function Toggle() {
                        //번역이 시작 이고 (스크리너가 반송 또는 번역자가 지정 됬을 때 또는 오신고 및 기타 관리자가 반송 했을 때)
                        if ('<%#DataBinder.Eval(Container.DataItem, "AssignType")%>' != "") {
                            var qa = document.getElementById("tr" + '<%#DataBinder.Eval(Container.DataItem, "Idx")%>');

                            if (qa.style.color != 'gray') {
                                qa.style.color = "gray";
                            }

                            else {
                                qa.style.color = ''
                            }

                            //alert(qa.style.color);
                        }

                        if ('<%#DataBinder.Eval(Container.DataItem, "HoldYN")%>' == "Y") {
                            var qa = document.getElementById("tr" + '<%#DataBinder.Eval(Container.DataItem, "Idx")%>');

                            qa.style.backgroundColor = "#FFDF24";
                        }
                    }

                    setInterval(Toggle, 300);

                    if ('<%#DataBinder.Eval(Container.DataItem, "AssignType")%>' != "") {
                        var qa = document.getElementById("tr" + '<%#DataBinder.Eval(Container.DataItem, "Idx")%>');
                        qa.style.backgroundColor = '#DEEAFC'
                       //alert(qa.style.color);
                    }
                </script>
                <td id="td1" runat="server" style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "TranslationDate", "{0:yyyy-MM-dd}")%></center></td>
                <td id="test1" runat="server" style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "TranslationID")%></center></td>
			    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "TranslationName")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ScreeningStart", "{0:yyyy-MM-dd}")%></center></td>
			    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ScanningDate", "{0:yyyy-MM-dd}")%></center></td>
			    <td style='border:1px solid #CCCCCC; height:27px; cursor:pointer;' bgcolor="#C6DFFF" onclick="ShowDetail('<%#DataBinder.Eval(Container.DataItem, "CorrID")%>','<%#DataBinder.Eval(Container.DataItem, "PackageID")%>','<%#DataBinder.Eval(Container.DataItem, "CorrPage")%>','<%#DataBinder.Eval(Container.DataItem, "ScreeningStatus")%>'); return false;"><center><%#DataBinder.Eval(Container.DataItem, "CorrID")%></center></td>
			    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "PackageID")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "CorrType")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ChildKey")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ChildName")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ConID")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "SponsorName")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "CorrPage")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ScreeningStatusName").ToString() == "" ? "초기화" : DataBinder.Eval(Container.DataItem, "ScreeningStatusName")%></center></td>
		    </tr>    
            </ItemTemplate> 
        </asp:Repeater>		
	</tbody>
</table>
<!-- paginator -->
<table style="width:100%">
<div class="paginator">
    <taeyo:PagingHelper ID="PagingHelper1" runat="server" OnOnPageIndexChanged="PagingHelper1_OnPageIndexChanged"
        CurrnetNumberColor="Silver" ForeColor="#404040"></taeyo:PagingHelper>
</div>
</table>
<!-- // paginator -->