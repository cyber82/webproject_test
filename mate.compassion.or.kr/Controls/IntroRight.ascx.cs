﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mate_Controls_IntroRight : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PageSubBind();
        }
    }

    private void PageSubBind()
    {
        string pageName = Context.Handler.GetType().Name;
        int findit = pageName.LastIndexOf("_aspx");
        pageName = pageName.Substring(0, findit);
        findit = pageName.LastIndexOf("_");
        pageName = pageName.Substring(findit + 1);
        if (pageName == "mateintro")
            sub1.Attributes.Add("class", "on");
        else if (pageName.StartsWith("trans"))
            sub2.Attributes.Add("class", "on");
        else if (pageName.StartsWith("office"))
            sub2.Attributes.Add("class", "on");
        else if (pageName.StartsWith("eventmate"))
            sub2.Attributes.Add("class", "on");
        else if (pageName.StartsWith("speciality"))
            sub2.Attributes.Add("class", "on");
        
    }
}