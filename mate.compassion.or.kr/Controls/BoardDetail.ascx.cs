﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Text; 

public partial class Mate_Controls_BoardDetail : System.Web.UI.UserControl
{
    WWWService.Service _wwwService = new WWWService.Service();
    string sJs;
    int m_iTableIndex = 0;   

    protected void Page_Load(object sender, EventArgs e)
    {
        int iTableIndex = Convert.ToInt32(Request.QueryString["iTableIndex"]);
        int iNoIndex = Convert.ToInt32(Request.QueryString["iNoIndex"]);
        int iBoardIdx = Convert.ToInt32(Request.QueryString["iBoardIdx"]);

        m_iTableIndex = iTableIndex;

        if (!IsPostBack)
        {
            DisplayInfo(iTableIndex, iNoIndex, iBoardIdx);
            GetCommentData();

            string pageName= GetParentName();

            // 답변 버튼 무조건 숨김
            //if (pageName == "noticeview" || pageName == "monthlymateview" || pageName == "todaywordview")
            btnAnswer.Visible = false;


			if (pageName == "noticeview") {
				writeComment2.Visible = false;
				listComment2.Visible = false;
			}

            

            
        }
    }

    private void DisplayInfo(int iTableIndex, int iNoIndex, int iBoardIdx)
    {
        string sJs = string.Empty;

        //-----1. 데이타를 가지고 온다. -----------------------------------------------------------               
        DataSet ds = new DataSet();

        try
        {
            ds = _wwwService.MateGetBoardContents(iTableIndex, iNoIndex, iBoardIdx);
        }

        catch (Exception ex)
        {            
            //메세지 띄우기
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("게시판 글을 가져오던 중 에러가 발생했습니다. 관리자에게 문의하세요. 원인 : " + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
            return;
        }

        labNo.Text = ds.Tables["T"].Rows[0]["no_idx"].ToString();         
        labTitle.Text = ds.Tables["T"].Rows[0]["b_title"].ToString();
        labWriter.Text = ds.Tables["T"].Rows[0]["b_name"].ToString();
        labWriteDay.Text = Convert.ToDateTime (ds.Tables["T"].Rows[0]["b_WriteDay"].ToString()).ToString ("yyyy-MM-dd");
        labReadNum.Text = ds.Tables["T"].Rows[0]["b_readnum"].ToString();
                
        DateTime dt = DateTime.Parse(ds.Tables["T"].Rows[0]["b_WriteDay"].ToString());
        TimeSpan ts = DateTime.Now - dt;

        if (ts.TotalMinutes >= 10080)
            labNew.Visible = false;

        if (UserInfo.IsLogin && new UserInfo().UserId.ToLower() == ds.Tables["T"].Rows[0]["user_id"].ToString().ToLower())
        {
            btnModify.Visible = true;
            btnDel.Visible = true; 
        }

        string sContents = ds.Tables["T"].Rows[0]["b_content"].ToString();

        /*
        if (sContents.Contains("\n"))
        {
            sContents = sContents.Replace("\r\n", "<br/>" );
            sContents = sContents.Replace("\n", "<br/>" );
        }

        if (sContents.Contains("<br/><br/><br/><br/><br/>"))
        {
            sContents = sContents.Replace("<br/><br/><br/><br/><br/>", "");
        }
		*/

        if (sContents.Contains("<strong>"))
        {
            sContents = sContents.Replace("<strong>", "<strong style='font-size: inherit;'>");
        }
        
        litContent.InnerHtml = sContents.ToHtml();

        //첨부파일관련
        for (int i = 1; i <= 5; i++)
        {
            litFiles.Text += FileView(ds.Tables["T"].Rows[0]["b_filename" + i.ToString()].ToString());
        }

        //리플수를 올려준다.
        _wwwService.mateReplyCountUpdate(int.Parse(ds.Tables["T"].Rows[0]["board_idx"].ToString()));

        // Mate 속도 관련
        ds.Dispose();
    }

    private string FileView(string sFileNM)
    {
        string sUrl = String.Empty;

        if (sFileNM.Trim().Length > 0)
        {
            sUrl = "<a href='/Controls/FileDown.aspx?filename=" + sFileNM
                     + "'>" + sFileNM + "</a>&nbsp;";
        }

        return sUrl;
    }

    private void GetCommentData()
    {
        int iTableIndex = Convert.ToInt32(Request.QueryString["iTableIndex"]);
        int iNoIndex = Convert.ToInt32(Request.QueryString["iNoIndex"]);
        int iref = Convert.ToInt32(Request.QueryString["b_num"]);
        int ire_step = Convert.ToInt32(Request.QueryString["re_step"]);

        DataSet data = _wwwService.MateCommentList(iTableIndex, iNoIndex, iref, ire_step);
        repCommentData.DataSource = data.Tables[0];
        repCommentData.DataBind();

        // Mate 속도 관련
        data.Dispose();
    }

    private string GetParentName()
    {
        string pageName = Context.Handler.GetType().Name;
        int findit = pageName.LastIndexOf("_aspx");
        pageName = pageName.Substring(0, findit);
        findit = pageName.LastIndexOf("_");
        pageName = pageName.Substring(findit + 1);
        return pageName;
    }

    protected string ParentPageName
    {
        get
        {
            string pageName = GetParentName();
            if (pageName == "noticeview")
                return "noticelist.aspx";
            else if(pageName == "monthlymateview")
                return "MonthlyMateList.aspx";
            else if (pageName == "ourstoryview")
                return "OurStoryList.aspx";
            else if (pageName == "masterchallengeview")
                return "MasterChallengeList.aspx";
            else if (pageName == "translationqnaview")
                return "TranslationQnAList.aspx";
            else if (pageName == "todaywordview")
                return "todaywordlist.aspx";
            else if (pageName == "onlineview")
                return "onlinelist.aspx";
            else if (pageName == "togetherstoryview")
                return "togetherstorylist.aspx";
            else
                return "empty.aspx";
        }
    }

    protected string ParentPageWriteName
    {
        get
        {
            string pageName = GetParentName();
            if (pageName == "noticeview")
                return "noticeWrite.aspx";
            else if (pageName == "monthlymateview")
                return "MonthlyMateWrite.aspx";
            else if (pageName == "ourstoryview")
                return "OurStoryWrite.aspx";
            else if (pageName == "masterchallengeview")
                return "MasterChallengeWrite.aspx";
            else if (pageName == "translationqnaview")
                return "TranslationQnAWrite.aspx";
            else if (pageName == "onlineview")
                return "onlineWrite.aspx";
            else if (pageName == "togetherstoryview")
                return "togetherstorywrite.aspx";
            else
                return "empty.aspx";
        }
    }

    protected void repCommentData_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
        HiddenField hidUserID = (HiddenField)e.Item.FindControl("hidUserID");
        if (UserInfo.IsLogin)
        {
            UserInfo userInfo = new UserInfo();
            if (userInfo.UserId == hidUserID.Value)
                btnDelete.Visible = true;
            else
                btnDelete.Visible = false;
        }
        else
            btnDelete.Visible = false;
    }


    protected void btnCommentSave_Click(object sender, EventArgs e)
    {
        if (UserInfo.IsLogin)
        {
            try
            {

                //번역QnA, 온라인상담 권한 체크 (현재 활동중(일시정지포함))
                if (m_iTableIndex == 1010 || m_iTableIndex == 1002)
                {
                    bool bTranslate = false;

                    string sUserId = new UserInfo().UserId;

                    //권한 체크
                    Object[] objSql = new object[1] { "MATE_MateYearCheck" };
                    Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
                    Object[] objValue = new object[2] { sUserId, "" };

                    WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

                    DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

                    //권한체크
                    if (data.Tables[0].Rows.Count > 0)
                    {
                        //권한 체크
                        objSql = new object[1] { "MATE_MateYearCheckIncludePause" };
                        objParam = new object[2] { "MateID", "MateYearTypeCode" };
                        Object[] objTranslateValue = new object[2] { new UserInfo().UserId, "11" };

                        //번역 권한체크
                        if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objTranslateValue).Tables[0].Rows.Count > 0)
                        {
                            bTranslate = true;
                        }

                        _WWW6Service.Dispose();
                    }

                    if (!bTranslate)
                    {
                        string url = this.Page.Request.Url.ToString();
                        //string loc = url.Contains("TranslationQnAWrite") ? "/nanum/TranslationQnAList.aspx" : url.Contains("OnlineWrite") ? "/nanum/OnlineList.aspx" : "";

                        string sjs = JavaScript.HeaderScript.ToString();
                        sjs += JavaScript.GetAlertScript("번역 메이트만 글쓰기가 가능합니다.");
                        //sjs += JavaScript.GetPageMoveScript(loc);
                        sjs += JavaScript.FooterScript.ToString();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                        return;
                    }
                }
                



                //////////////
                UserInfo userInfo = new UserInfo();


                string value = comment.Value;
                //byte[] s = Encoding.UTF8.GetBytes(value);
                // Byte 체크 수정 문희원
                int len = Encoding.Default.GetByteCount(value);
                if (len > 1000)
                {
                    sJs = JavaScript.HeaderScript;
                    sJs += JavaScript.GetAlertScript("영문 1000자리, 한글 500자리를 초과할 수 없습니다.");
                    sJs += JavaScript.FooterScript;
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
                    return;
                }


                int iTableIndex = Convert.ToInt32(Request.QueryString["iTableIndex"]);
                int iNoIndex = Convert.ToInt32(Request.QueryString["iNoIndex"]);
                int iref = Convert.ToInt32(Request.QueryString["b_num"]);
                int ire_step = Convert.ToInt32(Request.QueryString["re_step"]);

                _wwwService.MateCommentInsert(iTableIndex
                                         , iNoIndex
                                         , iref
                                         , ire_step
                                         , userInfo.UserName
                                         , userInfo.UserId
                                         , comment.Value
                                         , Request.UserHostAddress);
                comment.Value = "";
                GetCommentData();

                sJs = JavaScript.HeaderScript;
                sJs += JavaScript.GetPageMoveScript(this.Page.Request.RawUrl);    
                sJs += JavaScript.FooterScript;
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);

            }
            catch (Exception ex)
            {
                string sJs = string.Empty;
                //메세지 띄우기
                sJs = JavaScript.HeaderScript;
                sJs += JavaScript.GetAlertScript("코맨트 작성중 에러가 발생했습니다. 관리자에게 문의하세요. 원인 : " + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
                sJs += JavaScript.FooterScript;
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
                return;
            }
        }
        else
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("로그인후 댓글을 작성하셔야 합니다.");
			//sJs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=" + Server.UrlEncode(Request.Url.PathAndQuery));
			sJs += JavaScript.GetPageMoveScript("/login.aspx");
			//sJs += JavaScript.GetPageMoveScript(string.Format(System.Web.Configuration.WebConfigurationManager.AppSettings["loginPage"] , Server.UrlEncode(Request.Url.AbsoluteUri)));
			sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }
    }
    protected void repCommentData_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            HiddenField hidUserID = (HiddenField)e.Item.FindControl("hidUserID");
            HiddenField hidcref = (HiddenField)e.Item.FindControl("hidcref");
            HiddenField hidtable_idx = (HiddenField)e.Item.FindControl("hidtable_idx");
            HiddenField hidre_step = (HiddenField)e.Item.FindControl("hidre_step");
            HiddenField hidwriteday = (HiddenField)e.Item.FindControl("hidwriteday");
            int seqno = Convert.ToInt32(e.CommandArgument);

            _wwwService.MateCommentDelete(seqno, hidUserID.Value, Convert.ToInt32(hidtable_idx.Value), Convert.ToInt32(hidcref.Value), Convert.ToInt32(hidre_step.Value), hidwriteday.Value);
            GetCommentData();

            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetPageMoveScript(this.Page.Request.RawUrl);
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }
    }
    protected void btnModify_Click(object sender, EventArgs e)
    {
        Response.Redirect(ParentPageWriteName + "?mod=m&inoIndex=" + Request.QueryString["iNoIndex"] + "&iBoardIdx=" + Request.QueryString["iBoardIdx"] + "&b_num=0&re_level=0&re_step=0&ref=0"); 
    }
    protected void btnDel_Click(object sender, EventArgs e)
    {
        _wwwService.MateBoardDelete2 (Convert.ToInt32 (Request.QueryString["iBoardIdx"]));
        Response.Redirect(ParentPageName); 
    }
}