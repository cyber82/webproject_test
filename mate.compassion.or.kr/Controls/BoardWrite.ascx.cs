﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data; 

public partial class Mate_Controls_BoardWrite : System.Web.UI.UserControl
{
    int _tableIdx;

    protected void Page_Load(object sender, EventArgs e)
    {
        _tableIdx = Convert.ToInt32(((HiddenField)Page.FindControl("hidTableIdx")).Value);

        if (!IsPostBack)
        {
            
            if (!UserInfo.IsLogin)
            {
                string pageName = Context.Handler.GetType().Name;
                int findit = pageName.LastIndexOf("_aspx");
                pageName = pageName.Substring(0, findit);
                findit = pageName.LastIndexOf("_");
                pageName = pageName.Substring(findit + 1);

                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("로그인을 하신후 글쓰기 하셔야 합니다.");
                //sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/nanum/" + pageName + ".aspx");
                sjs += JavaScript.GetPageMoveScript("/login.aspx");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                return;
            }

            //번역QnA, 온라인상담 권한 체크 (현재 활동중(일시정지포함))
            if (_tableIdx == 1010 || _tableIdx == 1002)
            {
                bool bTranslate = false;

                string sUserId = new UserInfo().UserId;

                //권한 체크
                Object[] objSql = new object[1] { "MATE_MateYearCheck" };
                Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
                Object[] objValue = new object[2] { sUserId, "" };

                WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

                DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

                //권한체크
                if (data.Tables[0].Rows.Count > 0)
                {
                    //권한 체크
                    objSql = new object[1] { "MATE_MateYearCheckIncludePause" };
                    objParam = new object[2] { "MateID", "MateYearTypeCode" };
                    Object[] objTranslateValue = new object[2] { new UserInfo().UserId, "11" };

                    //번역 권한체크
                    if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objTranslateValue).Tables[0].Rows.Count > 0)
                    {
                        bTranslate = true;
                    }

                    _WWW6Service.Dispose();
                }

                if (!bTranslate)
                {
                    string url = this.Page.Request.Url.ToString();
                    string loc = url.Contains("TranslationQnAWrite") ? "/nanum/TranslationQnAList.aspx" : url.Contains("OnlineWrite") ? "/nanum/OnlineList.aspx" : "";

                    string sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("번역 메이트만 글쓰기가 가능합니다.");
                    sjs += JavaScript.GetPageMoveScript(loc);
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                    return;
                }
            }



            string noIndex = Request.QueryString["iNoIndex"];
            string boardIdx = Request.QueryString["iBoardIdx"];
            string mod = Request.QueryString["mod"];

            if (mod=="m")
            {
                btnSave.CssClass = "btn btn-modify";
                DisplayInfo(noIndex, boardIdx);
            }
            else
                btnSave.CssClass = "btn btn-registration";
        }
    }

    private void DisplayInfo(string iNoIndex, string iBoardIdx)
    {
        string sJs = string.Empty;

        //-----1. 데이타를 가지고 온다. -----------------------------------------------------------               
        DataSet ds = new DataSet();
        try
        {
            WWWService.Service wwwService = new WWWService.Service();
            ds = wwwService.MateGetBoardContents(_tableIdx, Convert.ToInt32(iNoIndex), Convert.ToInt32(iBoardIdx));
        }
        catch (Exception ex)
        {            
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("게시판 글을 가져오던 중 에러가 발생했습니다. 관리자에게 문의하세요. 원인 : " + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sJs += JavaScript.GetHistoryBackScript ();
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);            
            return;
        }

        UserInfo user = new UserInfo ();
        if (user.UserId.ToLower() != ds.Tables["T"].Rows[0]["user_id"].ToString().ToLower ())
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("게시판 글을 수정할 수 있는 권한이 없습니다.");
            sJs += JavaScript.GetHistoryBackScript();
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
            return;
        }

        txtTitle.Value = ds.Tables["T"].Rows[0]["b_title"].ToString();
        txtComment.Value = ds.Tables["T"].Rows[0]["b_content"].ToString();
        hidNoIndex.Value = iNoIndex;
        hidBoardIndex.Value = iBoardIdx;

        for (int i = 1; i <= 5; i++)
        {
            litFiles.Text += i.ToString () + "번파일 : " + FileView(ds.Tables["T"].Rows[0]["b_filename" + i.ToString()].ToString());
        }
    }

    private string FileView(string sFileNM)
    {
        string sUrl = String.Empty;
        if (sFileNM.Trim().Length > 0)
        {
            sUrl = "<a href='/Controls/FileDown.aspx?filename=" + sFileNM
                     + "'>" + sFileNM + "</a>&nbsp;";
        }
        return sUrl;
    }

    protected string UserName
    {
        get
        {
            return new UserInfo().UserName;
        }
    }

    private bool InsertBoard()
    {
        UserInfo user = new UserInfo();
        
        ////번역QnA쓰기 로그생성
        //if (_tableIdx == 1010) {
        //    ErrorLog.Write(HttpContext.Current, 610, "1.QnA쓰기시작 - userID:" + user.UserId);
        //}
        
        DEXTUpload.NET.FileUpload fileUpload = new DEXTUpload.NET.FileUpload();
        WWWService.Service wwwService = new WWWService.Service();
        string sJs = string.Empty;

        //1. 파일업로드부분
        //Files/Board 폴더에 파일을 저장한다.
        string UploadedPath;

        string sFiles = string.Empty;
        bool isFileMax = false;

        for (int i = 0; i < fileUpload["files"].Count; i++)
        {
            if (fileUpload["files"][i].Value != "" && fileUpload["files"][i].FileLength > 1024 * 1024 * 2)
            {
                isFileMax = true;                
                break;
            }
        }

        if (isFileMax)
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("첨부파일은 2MB를 초과할수 없습니다.");
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
            return false;
        }


        for (int i = 0; i < fileUpload["files"].Count; i++)
        {
            if (fileUpload["files"][i].IsFileElement && fileUpload["files"][i].Value != "")
            {
                UploadedPath = fileUpload["files"][i].SaveVirtual("/Files/Board", false);

                if (sFiles.Length == 0)
                {
                    sFiles = fileUpload["files"][i].LastSavedFileName.ToString();
                }
                else
                {
                    sFiles += "|" + fileUpload["files"][i].LastSavedFileName.ToString();
                }
            }
        }

        ////번역QnA쓰기 로그생성
        //if (_tableIdx == 1010)
        //{
        //    ErrorLog.Write(HttpContext.Current, 610, "2.파일업로드완료 - userID:" + user.UserId);
        //}

        //파일이 존재하지않는다면.
        bool bFileDivision;
        if (sFiles.Length == 0)
        {
            bFileDivision = false;
        }
        else
        {
            bFileDivision = true;
        }

        //파일이 존재한다면 원하는 정보를 변수에 넣는다.
        string sFileName = string.Empty;
        int iFileSize = 0;
        //if (bFileDivision == true)
        //{
        //    ////파일이 존재하는데 지정된 용량을 초과할경우.
        //    //fileUpload.MaxFileLength = 1024 * 1024 * 2;  //2MB             

        //    //if (fileUpload.FileLength > fileUpload.MaxFileLength)
        //    //{
        //    //    sJs = JavaScript.HeaderScript.ToString();
        //    //    sJs += JavaScript.GetAlertScript("파일의 크기는 2M이하이여야합니다.");
        //    //    sJs += JavaScript.FooterScript.ToString();

        //    //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sJs.ToString());
        //    //    return;
        //    //}


        //    //용량또한 초과하지않을경우.
        //    sFileName = fileUpload.LastSavedFileName.ToString();
        //    iFileSize = fileUpload.FileLength;
        //}

        //string category = ddlCategory.Text;
        string category = "";

        int no_idx = String.IsNullOrEmpty(Request.QueryString["iNoIndex"]) ? 0 : Convert.ToInt32(Request.QueryString["iNoIndex"]);
        int b_num = String.IsNullOrEmpty(Request.QueryString["b_num"]) ? 0 : Convert.ToInt32(Request.QueryString["b_num"]);
        int reff = String.IsNullOrEmpty(Request.QueryString["ref"]) ? 0 : Convert.ToInt32(Request.QueryString["ref"]);
        int re_step = String.IsNullOrEmpty(Request.QueryString["re_step"]) ? 0 : Convert.ToInt32(Request.QueryString["re_step"]);
        int re_level = String.IsNullOrEmpty(Request.QueryString["re_level"]) ? 0 : Convert.ToInt32(Request.QueryString["re_level"]);                                              

        if(String.IsNullOrEmpty (Request.QueryString["mod"]))
            no_idx = 0;

        ////번역QnA쓰기 로그생성
        //if (_tableIdx == 1010)
        //{
        //    ErrorLog.Write(HttpContext.Current, 610, "3.insert시작 - userID:" + user.UserId + ", title:" + txtTitle.Value + ", comment:" + txtComment.Value);
        //}

        WWWService.Service service = new WWWService.Service();
        service.MateTestSetBoardContents(_tableIdx
                                           , no_idx
                                           , b_num
                                           , reff
                                           , re_step
                                           , re_level
                                           , user.UserName
                                           , txtTitle.Value
                                           , txtComment.Value
                                           , Request.UserHostAddress
                                           , "compassion_zoppura"
                                           , bFileDivision
                                           , sFiles
                                           , user.UserId
                                           , category);

        ////번역QnA쓰기 로그생성
        //if (_tableIdx == 1010)
        //{
        //    ErrorLog.Write(HttpContext.Current, 610, "4.insert완료 - userID:" + user.UserId);
        //}

        return true;
    }

    private bool UpdateBoard()
    {
        DEXTUpload.NET.FileUpload fileUpload = new DEXTUpload.NET.FileUpload();
        WWWService.Service wwwService = new WWWService.Service();
        string sJs = string.Empty;

        //1. 파일업로드부분
        //Files/Board 폴더에 파일을 저장한다.
        string UploadedPath ="";

        string sFiles = string.Empty;
        bool isFileMax = false;

        for (int i = 0; i < fileUpload["files"].Count; i++)
        {
            if (fileUpload["files"][i].Value != "" && fileUpload["files"][i].FileLength > 1024 * 1024 * 2)
            {
                isFileMax = true;
                break;
            }
        }

        if (isFileMax)
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("첨부파일은 2MB를 초과할수 없습니다.");
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
            return false;
        }

        for (int i = 0; i < fileUpload["files"].Count; i++)
        {
            if (fileUpload["files"][i].Value != "")
            {
                UploadedPath = fileUpload["files"][i].SaveVirtual("/Files/Board", false);
                //fileUpload["files"][i].SaveAs(@"D:\NewCompassion\Files\Board\",false);

                if (i == 0)
                    sFiles = i.ToString() + ":" + fileUpload["files"][i].LastSavedFileName;
                else
                    sFiles += "|" + i.ToString() + ":" + fileUpload["files"][i].LastSavedFileName;          
            }
            else
            {
                if (i == 0)
                    sFiles = i.ToString() + ":No";
                else
                    sFiles += "|" + i.ToString() + ":No";
            }
        }

        //파일이 존재하지않는다면.
        bool bFileDivision;
        if (sFiles.Length == 0)
        {
            bFileDivision = false;
        }
        else
        {
            bFileDivision = true;
        }

        //파일이 존재한다면 원하는 정보를 변수에 넣는다.
        string sFileName = string.Empty;
        int iFileSize = 0;
        //if (bFileDivision == true)
        //{
        //    ////파일이 존재하는데 지정된 용량을 초과할경우.
        //    //fileUpload.MaxFileLength = 1024 * 1024 * 2;  //2MB             

        //    //if (fileUpload.FileLength > fileUpload.MaxFileLength)
        //    //{
        //    //    sJs = JavaScript.HeaderScript.ToString();
        //    //    sJs += JavaScript.GetAlertScript("파일의 크기는 2M이하이여야합니다.");
        //    //    sJs += JavaScript.FooterScript.ToString();

        //    //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sJs.ToString());
        //    //    return;
        //    //}


        //    //용량또한 초과하지않을경우.
        //    sFileName = fileUpload.LastSavedFileName.ToString();
        //    iFileSize = fileUpload.FileLength;
        //}
  
        UserInfo user = new UserInfo();
        WWWService.Service service = new WWWService.Service();
        wwwService.MateModifyBoardContents(_tableIdx 
                                              , int.Parse(hidNoIndex.Value)
                                              , int.Parse(hidBoardIndex.Value)
                                              , UserName
                                              , txtTitle.Value 
                                              , txtComment.Value 
                                              , sFiles);

        return true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool retVal;

        if (String.IsNullOrEmpty(Request.QueryString["mod"]) || Request.QueryString["mod"] == "r")
            retVal = InsertBoard();
        else
            retVal = UpdateBoard();

        if (retVal)
        {
            string pageName = Context.Handler.GetType().Name;
            int findit = pageName.LastIndexOf("_aspx");
            pageName = pageName.Substring(0, findit);
            findit = pageName.LastIndexOf("_");
            pageName = pageName.Substring(findit + 1);

            if (pageName == "masterchallengewrite")
                Response.Redirect("masterchallengelist.aspx");
            else if (pageName == "ourstorywrite")
                Response.Redirect("ourstorylist.aspx");
            else if (pageName == "translationqnawrite")
                Response.Redirect("translationqnalist.aspx");
            else if (pageName == "onlinewrite")
                Response.Redirect("onlinelist.aspx");
            else if (pageName == "togetherstorywrite")
                Response.Redirect("togetherstorylist.aspx");
            else
                Response.Redirect("empty.aspx");
        }
    }
}