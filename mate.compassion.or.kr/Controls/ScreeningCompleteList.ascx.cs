﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Text;
using System.Web.Configuration;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class Mate_Controls_ScreeningCompleteList : System.Web.UI.UserControl
{
    int _PAGESIZE = 10;
    int _tableIdx;
    bool bgGetLetter;

    protected string _pageIdx = "0";

    //추가 2013-08-28
    protected string IdxPage
    {
        get { return _pageIdx; }
    }


    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        string sJs;
        string sgSDate;
        string sgEDate;

        if (UserInfo.IsLogin)
        {
            string sUserId = new UserInfo().UserId;

            //권한 체크
            Object[] objSql = new object[1] { "MATE_MateYearCheck" };
            Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
            Object[] objValue = new object[2] { sUserId, "21" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //권한체크
            if (data.Tables[0].Rows.Count > 0)
            {
                sgSDate = Request["start_date"];
                sgEDate = Request["end_date"];

                if (string.IsNullOrEmpty(sgSDate))
                {
                    sgSDate = DateTime.Today.AddMonths(-1).ToString("yyy-MM-dd");
                }

                if (string.IsNullOrEmpty(sgEDate))
                {
                    sgEDate = DateTime.Today.ToString("yyy-MM-dd");
                }

                //수정 2013-08-28
                DataBind(Convert.ToInt32(_pageIdx), sgSDate, sgEDate);

                //검색창 날짜 셋팅
                StringBuilder sb = new StringBuilder();
                sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                sb.Append("SetDate('" + sgSDate + "','" + sgEDate + "');");
                sb.Append("</script>");

                Page.RegisterStartupScript("TranslationComplete", sb.ToString());

                //페이지 타입 쿠키 저장
                new TranslateInfo().MatePageType = "21";
            }

            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                sb.Append("alert('해당 권한이 없습니다.'); history.back();");
                sb.Append("</script>");

                Response.Write(sb.ToString());
            }

            data.Dispose();
            _WWW6Service.Dispose();
        }

        else
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("로그인 후 사용 가능합니다.");
			//sJs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=" + Server.UrlEncode(Request.Url.AbsoluteUri));
			sJs += JavaScript.GetPageMoveScript("/login.aspx");
			sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }

		// 편지 타입
		var codeResult = new LetterAction().GetCodeList();
		if (codeResult.success) {

			HtmlGenericControl all = new HtmlGenericControl("li");
			all.InnerText = "전체";
			all.Attributes["data-id"] = "";
			letter_type.Controls.Add(all);

			DataTable codeList = (DataTable)codeResult.data;
			foreach (DataRow dr in codeList.Rows) {
				HtmlGenericControl li = new HtmlGenericControl("li");
				var codeId = dr["codeId"].ToString();

				if (rdoChild.Checked) {
					if (codeId.StartsWith("CHI")) {
						li.InnerText = dr["codeId"].ToString();
						li.Attributes["data-id"] = dr["codeId"].ToString();
						letter_type.Controls.Add(li);
					}
				} else {
					if (!codeId.StartsWith("CHI")) {
						li.InnerText = dr["codeId"].ToString();
						li.Attributes["data-id"] = dr["codeId"].ToString();
						letter_type.Controls.Add(li);
					}
				}


			}
		}


		// 어린이 키
		// 국외인경우 국가목록
		var countryResult = new CountryAction().GetList();
		if (countryResult.success) {

			HtmlGenericControl all = new HtmlGenericControl("li");
			all.InnerText = "전체";
			all.Attributes["data-id"] = "";
			ul_chile_key.Controls.Add(all);

			var countryList = (List<sp_country_list_fResult>)countryResult.data;
			foreach (sp_country_list_fResult entity in countryList) {
				HtmlGenericControl li = new HtmlGenericControl("li");
				li.InnerText = entity.c_id;
				li.Attributes["data-id"] = entity.c_id;
				ul_chile_key.Controls.Add(li);
			}
		}

	}

	#region 데이터 바인딩
	private void DataBind(int idxPage, string sgSDate, string sgEDate)
    {
		DataSet data;

        string sUserId = new UserInfo().UserId;

        Object[] objSql = new object[1] { "MATE_ScreeningCompleteList2016" };
		Object[] objParam = new object[8] { "ScreeningID", "ScreeningSDate", "ScreeningEDate", "CorrID", "CorrType", "ChildKeyPrefix", "order_name", "order_type" };
		Object[] objValue = new object[8] { sUserId, sgSDate, sgEDate, CorrID.Text == "" ? "-1" : CorrID.Text.Trim(), CorrType.Value, ChildKey.Value, order_name.Value, order_type.Value };

		WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

        //리스트 카운터
        this.lblTotalCount.Text = "Total : " + data.Tables[0].Rows.Count;

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = data.Tables[0].DefaultView;
        pds.AllowPaging = true;
        pds.PageSize = _PAGESIZE;
        pds.CurrentPageIndex = idxPage;
        repData.DataSource = pds;
        repData.DataBind();

        PagingSetting(data.Tables[0].Rows.Count, idxPage);

        data.Dispose();
        _WWW6Service.Dispose();
    }
    #endregion

    #region 하단목록
    private void PagingSetting(int totalCount, int curPage)
    {
        PagingHelper1.VirtualItemCount = totalCount;
        PagingHelper1.PageSize = _PAGESIZE;
        PagingHelper1.CurrentPageIndex = curPage;
    }

    //하단목록 페이지검색
    protected void PagingHelper1_OnPageIndexChanged(object sender, TaeyoNetLib.PagingEventArgs e)
    {
        //수정 2013-08-28
        string sgSDate = Request["start_date"];
        string sgEDate = Request["end_date"];

        DataBind(e.PageIndex, sgSDate, sgEDate);
        _pageIdx = e.PageIndex.ToString();
        hdIdxPage.Value = _pageIdx;
    }
    #endregion

    #region 검색버튼
    protected void imgBtnSearch_Click(object sender, ImageClickEventArgs e)
    {
        //수정 2013-08-28
        string sgSDate = Request["start_date"];
        string sgEDate = Request["end_date"];

        DataBind(0, sgSDate, sgEDate);
        _pageIdx = "0";
        hdIdxPage.Value = _pageIdx;
    }
	#endregion


	protected void btn_submit_Click(object sender, EventArgs e) {
		//DataBind(Convert.ToInt32(_pageIdx));
	}
}