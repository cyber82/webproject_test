﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using System.Text;

public partial class Mate_Controls_NanumRight : System.Web.UI.UserControl
{
    //public bool bThirdPL;
    //public bool bScreening;
    //public bool bTranslate;
    //public bool bLogin;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PageSubBind();

            WWWService.Service service = new WWWService.Service();
            DataSet data = service.GetMateNationList(57);
            dropNation.DataTextField = "title";
            dropNation.DataValueField = "idx";
            dropNation.DataSource = data.Tables[0];
            dropNation.DataBind();

            //if (UserInfo.IsLogin)
            //{
            //    bLogin = true;

            //    string sUserId = new UserInfo().UserId;

            //    //권한 체크
            //    Object[] objSql = new object[1] { "MATE_MateYearCheck" };
            //    Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
            //    Object[] objValue = new object[2] { sUserId, "" };

            //    WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
            //    DataSet data2 = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //    //권한체크
            //    if (data2.Tables[0].Rows.Count > 0)
            //    {
            //        //권한 체크
            //        objSql = new object[1] { "MATE_MateYearCheck" };
            //        objParam = new object[2] { "MateID", "MateYearTypeCode" };
            //        Object[] objThirdPLValue = new object[2] { new UserInfo().UserId, "31" };
            //        Object[] objScreeningValue = new object[2] { new UserInfo().UserId, "21" };
            //        Object[] objTranslateValue = new object[2] { new UserInfo().UserId, "11" };

            //        //3PL 권한체크
            //        if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objThirdPLValue).Tables[0].Rows.Count > 0)
            //        {
            //            bThirdPL = true;
            //        }

            //        //스크리닝 권한체크
            //        if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objScreeningValue).Tables[0].Rows.Count > 0)
            //        {
            //            bScreening = true;
            //        }

            //        //번역 권한체크
            //        if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objTranslateValue).Tables[0].Rows.Count > 0)
            //        {
            //            bTranslate = true;
            //        }

            //        _WWW6Service.Dispose();
                    
            //    }
                
            //}
        }
    }

    private void PageSubBind()
    {
        string pageName = Context.Handler.GetType().Name;
        int findit = pageName.LastIndexOf("_aspx");
        pageName = pageName.Substring(0, findit);
        findit = pageName.LastIndexOf("_");
        pageName = pageName.Substring(findit + 1);
        if (pageName == "masterchallengelist" || pageName == "masterchallengeview" || pageName == "masterchallengewrite")        
            sub3.Attributes.Add("class", "on");         
        else if (pageName == "ourstorylist" || pageName == "ourstoryview" || pageName == "ourstorywrite")
            sub1.Attributes.Add("class", "on"); 
        else if (pageName == "todaywordlist" || pageName == "todaywordview")
            sub2.Attributes.Add("class", "on"); 
        else if (pageName == "translationqnalist" || pageName == "translationqnaview" || pageName == "translationqnawrite")
            sub4.Attributes.Add("class", "on");
        else if (pageName == "onlinelist" || pageName == "onlineview" || pageName == "onlinewrite")
            sub5.Attributes.Add("class", "on");
        else if (pageName == "benefitdefault" || pageName == "benefitdetail" || pageName == "benefitlist" || pageName == "childletterview")
            sub6.Attributes.Add("class", "on");
        else if (pageName.Contains("togetherstory"))
            sub7.Attributes.Add("class", "on"); 
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!UserInfo.IsLogin)
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("로그인을 하신후 글쓰기 하셔야 합니다.");
			//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/mate/default.aspx");
			sjs += JavaScript.GetPageMoveScript("/login.aspx");
			sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            return;
        }

        UserInfo user = new UserInfo ();

        WWWService.Service service = new WWWService.Service();
        service.InsertBenefit("", eword.Value, dropInitial.Value, comment.Value
            , Convert.ToInt32 (dropNation.SelectedValue), user.UserId, user.UserName);

        eword.Value = "";
        comment.Value = "";
        dropInitial.SelectedIndex = 0;
        dropNation.SelectedIndex = 0;


        string str = JavaScript.HeaderScript.ToString();
        str += JavaScript.GetAlertScript("수혜국 관련 용어가 저장 되었습니다");
        str += JavaScript.FooterScript.ToString();
        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", str);

        return;
    }
}