﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_Top : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (UserInfo.IsLogin)
            {
                UserInfo user = new UserInfo();

                login.Attributes.Add("style", "display:none;");
                logout.Attributes.Add("style", "display:inline-block; *display:inline; zoom:1;");

                if (user.ConId.Length > 6)
                    labName2.Text = user.UserId + " 님";

                else
                    labName2.Text = user.UserId + "(" + user.ConId + ") 님";
            }

            else
            {
                login.Attributes.Add("style", "display:inline-block; *display:inline; zoom:1;");
                logout.Attributes.Add("style", "display:none;");
            }
        }
    }
}