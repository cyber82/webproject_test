﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Text;
using System.Web.Configuration;

public partial class Mate_Controls_ThirdPLCompleteList : System.Web.UI.UserControl
{
    int _PAGESIZE = 10;
    int _tableIdx;
    bool bgGetLetter;

    string sgSDate;
    string sgEDate;

    protected string _pageIdx = "0";


    protected void Page_Load(object sender, EventArgs e)
    {
        string sJs;

        if (UserInfo.IsLogin)
        {
            string sUserId = new UserInfo().UserId;

            //권한 체크
            Object[] objSql = new object[1] { "MATE_MateYearCheck" };
            Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
            Object[] objValue = new object[2] { sUserId, "31" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //권한체크
            if (data.Tables[0].Rows.Count > 0)
            {
                sgSDate = Request["start_date"];
                sgEDate = Request["end_date"];

                if (string.IsNullOrEmpty(sgSDate))
                {
                    sgSDate = DateTime.Today.AddMonths(-1).ToString("yyy-MM-dd");
                }

                if (string.IsNullOrEmpty(sgEDate))
                {
                    sgEDate = DateTime.Today.ToString("yyy-MM-dd");
                }


                DataBind(Convert.ToInt32(_pageIdx));
                //검색창 날짜 셋팅
                StringBuilder sb = new StringBuilder();
                sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                sb.Append("SetDate('" + sgSDate + "','" + sgEDate + "');");
                sb.Append("</script>");

                Page.RegisterStartupScript("TranslationComplete", sb.ToString());
                //페이지 타입 쿠키 저장
                new TranslateInfo().MatePageType = "41";
            }

            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                sb.Append("alert('해당 권한이 없습니다.'); history.back();");
                sb.Append("</script>");

                Response.Write(sb.ToString());
            }

            data.Dispose();
            _WWW6Service.Dispose();
        }

        else
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("로그인 후 사용 가능합니다.");
			//sJs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=" + Server.UrlEncode(Request.Url.AbsoluteUri));
			sJs += JavaScript.GetPageMoveScript("/login.aspx");
			sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }
        
    }

    private void DataBind(int idxPage)
    {
        DataSet data;

        string sUserId = new UserInfo().UserId;

        StringBuilder sbList = new StringBuilder();

        sbList.Append("SELECT (ThirdYear + ProjectNumber) AS LetterID, ");
        sbList.Append("FieldOffice,ProjectNumber, ProjectName, ActualAuthorRole, AuthorName, ThirdYear, ");
        sbList.Append("dbo.uf_GetCommonName('THIRD_TRANS_STATUS',TranslateStatus) AS TranslateStatusName, TranslateDate ");
        sbList.Append("FROM ThirdPL_Master ");
        sbList.Append("WHERE TranslateID = '" + sUserId + "' AND TranslateStatus = 'END' ");
        sbList.Append("AND TranslateDate BETWEEN '" + sgSDate + "' AND '" + sgEDate + "'  ");
        sbList.Append("ORDER BY TranslateDate DESC ");

        Object[] objSql = new object[1] { sbList.ToString() };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

        //리스트 카운터
        this.lblTotalCount.Text = "Total : " + data.Tables[0].Rows.Count;

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = data.Tables[0].DefaultView;
        pds.AllowPaging = true;
        pds.PageSize = _PAGESIZE;
        pds.CurrentPageIndex = idxPage;
        repData.DataSource = pds;
        repData.DataBind();

        PagingSetting(data.Tables[0].Rows.Count, idxPage);

        data.Dispose();
        _WWW6Service.Dispose();
    }

    private void PagingSetting(int totalCount, int curPage)
    {
        PagingHelper1.VirtualItemCount = totalCount;
        PagingHelper1.PageSize = _PAGESIZE;
        PagingHelper1.CurrentPageIndex = curPage;
    }

    protected void PagingHelper1_OnPageIndexChanged(object sender, TaeyoNetLib.PagingEventArgs e)
    {
        DataBind(e.PageIndex);
        _pageIdx = e.PageIndex.ToString();
    }

    protected void imgBtnSearch_Click(object sender, ImageClickEventArgs e)
    {

    }
}