﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecruitRight.ascx.cs" Inherits="Controls_RecruitRight" %>

<%@ Register src="/Controls/RightBanner.ascx" tagname="Banner" tagprefix="bc1" %>

<div id="sidebar" class="sec-recruit">
	<div class="tit">인재채용 RECRUITMENT</div>

    <% //수정 2013-07-08 %>
	<ul id="lnb" class="lnb">
		<li id="sub1"><a href="/recruit/Process.aspx" class="dep1"><span>채용프로세스</span></a></li>
		<li id="sub2"><a href="/recruit/Ability.aspx" class="dep2"><span>핵심역량</span></a></li>
		<%--<li id="sub3"><a href="/recruit/Duty.aspx" class="dep3"><span>직무소개</span></a></li>--%>
		<li id="sub4"><a href="/recruit/ApplyList.aspx" class="dep4"><span>온라인 입사지원</span></a></li>
		<li id="sub5"><a href="/recruit/ApplyMasterWrite.aspx?isMy=5" class="dep5"><span>인재풀 지원</span></a></li>
		<li id="sub6"><a href="/recruit/ApplyMasterWrite.aspx?isMy=6" class="dep6"><span>이력서 관리</span></a></li>
		<li id="sub7"><a href="/recruit/ResultDirect.aspx" class="dep7"><span>지원결과 조회</span></a></li>
		<li id="sub8"><a href="/recruit/Faq.aspx" class="dep8"><span>자주 묻는 질문</span></a></li>
	</ul>

    <% //수정 2013-07-10 %>
	<ul class="banner1">
		<li><a href="/recruit/ApplyList.aspx"><img src="/image/banner/recruitment.png" width="240" height="94" alt="" /></a></li>
		<li><a href="/recruit/ApplyMasterWrite.aspx?isMy=5"><img src="/image/banner/hrpool-support.png" width="242" height="100" alt="" /></a></li>
	</ul>
</div>