﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BoardDetail.ascx.cs" Inherits="Mate_Controls_BoardDetail" %>
<table class="mate-list">
	<thead>
		<tr>
			<th class="td-num"><img src="/image/mate/text/td-num2.gif" alt="번호" /></th>
			<th class="td-title"><img src="/image/mate/text/td-title2.gif" alt="제목" /></th>
            <th class="td-writer"><img src="/image/mate/text/td-writer.gif" alt="작성자" /></th>
			<th class="td-date"><img src="/image/mate/text/td-date-reg2.gif" alt="등록일" /></th>
            <th class="td-hits"><img src="/image/mate/text/td-hits.gif" alt="조회수" /></th>
		</tr>
	</thead>
	<tbody>
		<tr class="first-tr">
			<td class="td-num"><asp:Label ID="labNo" runat="server" Text=""></asp:Label></td>
			<td class="td-title"><asp:Label ID="labTitle" runat="server" Text=""></asp:Label><asp:Label ID="labNew" CssClass="ico-new" runat="server" Text="new"></asp:Label></td>
			<td class="td-writer"><asp:Label ID="labWriter" runat="server" Text=""></asp:Label></td>
            <td class="td-date"><asp:Label ID="labWriteDay" runat="server" Text=""></asp:Label></td>
            <td class="td-hits"><asp:Label ID="labReadNum" runat="server" Text=""></asp:Label></td>
		</tr>
	</tbody>
</table>
<div class="view-context2">
	<div class="txt-attach2">
		<asp:Literal ID="litFiles" runat="server"></asp:Literal>
	</div>

    <% //수정 2013-05-29 %>
    <div>
        <%--<asp:Literal ID="litContent" runat="server"></asp:Literal>--%>
        <div runat="server" id="litContent"></div>
    </div>
</div>
<div class="btn-list">	
	<div class="btn-r">	
        <asp:Button ID="btnModify" runat="server" Text="" CssClass="btn btn-modify" onclick="btnModify_Click" Visible="False" />
        <asp:Button ID="btnDel" runat="server" Text="" CssClass="btn btn-delete" onclick="btnDel_Click" Visible="False" OnClientClick ="return confirm('삭제 하시겠습니까?');" />
        
        <script>
            //hidTableIdx
            function checkPermission(url) {
                if ($('#hidTableIdx').val() == "1002" || $('#hidTableIdx').val() == "1010") {
                    //온라인상담, 번역Q&A : 활동중인 메이트만 글쓰기 및 댓글 가능
                    var bLogin = $('#hdnbLogin').val();
                    var bThirdPL = $('#hdnbThirdPL').val();
                    var bScreening = $('#hdnbScreening').val();
                    var bTranslate = $('#hdnbTranslate').val();

                    if (bLogin == "true") {
                        if (bThirdPL == 'true' || bScreening == 'true' || bTranslate == 'true') {
                            location.href = url;
                        }
                        else {
                            alert('활동중인 메이트만 가능합니다.');
                        }
                    }
                    else {
                        alert('로그인 후 사용 가능합니다.');
                    }
                }
            }
        </script>
        
        
        <a href="javascript:checkPermission('<%=ParentPageWriteName%>?mod=r&inoIndex=<%=Request.QueryString["iNoIndex"]%>&iBoardIdx=<%=Request.QueryString["iBoardIdx"]%>&b_num=<%=Request.QueryString["b_num"]%>&re_level=<%=Request.QueryString["re_level"]%>&re_step=<%=Request.QueryString["re_step"]%>&ref=<%=Request.QueryString["ref"]%>')" class="btn btn-answer"><img src="/image/btn/btn-answer.png" runat="server" id="btnAnswer" /></a>
		<a href="<%=ParentPageName%>" class="btn btn-list3" id="aList"><span>목록보기</span></a>
	</div>
</div>

<!-- 댓글쓰기 -->
<div class="write-comment2" runat="server" id="writeComment2">
<fieldset>
	<legend><strong class="tit-reply2"><span class="hide">댓글을 남겨주세요</span></strong>현재 <span id="countComment">0</span>/1000byte(최대 한글 500자, 영문 1000자)</legend>
	<textarea rows="4" cols="85" runat="server" id="comment"  onkeyup="checkContentLength( this,1000 );"></textarea>
        <asp:Button ID="btnCommentSave" runat="server" Text="" 
            CssClass ="btn btn-reply2" onclick="btnCommentSave_Click" OnClientClick ="return Validate();" />
</fieldset>
</div>

<div class="list-comment2" runat="server" id="listComment2">
<ul>
<asp:Repeater ID="repCommentData" runat="server" 
            onitemdatabound="repCommentData_ItemDataBound" 
            onitemcommand="repCommentData_ItemCommand" >
            <ItemTemplate>
	<li>
		<strong class="writer2"><%#DataBinder.Eval(Container.DataItem, "writer")%></strong> 
		<div class="text">
			<p><%#DataBinder.Eval(Container.DataItem, "comment").ToString ().Replace ("\n","<BR/>") %>
				<span class="add-info">
					<span><%#Convert.ToDateTime (DataBinder.Eval(Container.DataItem, "writeday")).ToString ("yyyy-MM-dd HH:mm")%></span>
					<!-- 본인 작성 글일 때 -->
                    <asp:ImageButton ID="btnDelete" runat="server" CommandName="delete" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "seqno")%>' ImageUrl ="/image/mate/btn/btn-delete3.png" />                    
                    <asp:HiddenField ID="hidUserID" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "user_id")%>'/>
                    <asp:HiddenField ID="hidcref" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "cref")%>'/>
                    <asp:HiddenField ID="hidtable_idx" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "table_idx")%>'/>
                    <asp:HiddenField ID="hidre_step" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "re_step")%>'/>
                    <asp:HiddenField ID="hidwriteday" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "writeday")%>'/>    
				</span>
			</p> <!-- 수정 삭제는 본인 일때만 -->
		</div>
	</li>   
    </ItemTemplate>      
</asp:Repeater>
</ul>
</div>
<br/>