﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Text;

public partial class Mate_Controls_TranslationRight : System.Web.UI.UserControl
{

    public string sgBibleList;//성경목차
    public string sgCountryList;//국가약칭
    public string sgCurrencyList;//수혜국통화

    public bool bThirdPL;
    public bool bScreening;
    public bool bTranslate;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (UserInfo.IsLogin)
        {

            if (!IsPostBack)
            {
                PageSubBind();

                WWWService.Service service = new WWWService.Service();
                DataSet data = service.GetMateNationList(57);
                dropNation.DataTextField = "title";
                dropNation.DataValueField = "idx";
                dropNation.DataSource = data.Tables[0];
                dropNation.DataBind();
            }

            //3PL 권한 체크
            Object[] objSql = new object[1] { "MATE_MateYearCheck" };
            Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
            Object[] objThirdPLValue = new object[2] { new UserInfo().UserId, "31" };

            Object[] objScreeningValue = new object[2] { new UserInfo().UserId, "21" };
            Object[] objTranslateValue = new object[2] { new UserInfo().UserId, "11" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            //3PL 권한체크
            //3PL 권한체크
            if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objThirdPLValue).Tables[0].Rows.Count > 0)
            {
                bThirdPL = true;
            }

            //스크리닝 권한체크
            if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objScreeningValue).Tables[0].Rows.Count > 0)
            {
                bScreening = true;
            }

            //번역 권한체크
            if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objTranslateValue).Tables[0].Rows.Count > 0)
            {
                bTranslate = true;
            }

            _WWW6Service.Dispose();

            //Response.Write(Request.Url.Host.ToString() + "/" + Request.Url.Port.ToString());
            //성경목차 리스트 숨기기/펼치기
            imgBtnBibleList.Attributes.Add("onClick", "DivShow('1'); return false;");
            //국가약칭 리스트 숨기기/펼치기
            imgBtnCountryList.Attributes.Add("onClick", "DivShow('2'); return false;");
            //수혜국통화 리스트 숨기기/펼치기
            imgBtnCurrencyList.Attributes.Add("onClick", "DivShow('3'); return false;");
            //편지용어검색 창 팝업
            imgBtnBenefit.Attributes.Add("onClick", "openBrWindow('/translation/BenefitDefault.aspx','BenefitDefault','780','800','0'); return false;");
            //질문게시판 창 팝업
            imgBtnQnA.Attributes.Add("onClick", "openBrWindow('/translation/TranslationQnAList.aspx','BenefitDefault','780','800','0'); return false;");
            //HolyBible 조회 창 팝업
            imgHolyBible.Attributes.Add("onClick", "openBrWindow('http://www.holybible.or.kr/','HolyBible','780','800','1'); return false;");
            //iMate사용메뉴얼 창 팝업
            imgManual.Attributes.Add("onClick", "openBrWindow('/Files/iMate_manual.pdf','Manual','780','800','1'); return false;");
            //번역 유의사항 창 팝업
            imgCareful.Attributes.Add("onClick", "openBrWindow('/Files/Trans_careful.pdf','Careful','780','800','1'); return false;");

            //GetGuidelist();//성경목차
        }

    }

    private void InitSet()
    {
    }

    private void PageSubBind()
    {
        string pageName = Context.Handler.GetType().Name;
        int findit = pageName.LastIndexOf("_aspx");
        pageName = pageName.Substring(0, findit);
        findit = pageName.LastIndexOf("_");
        pageName = pageName.Substring(findit + 1);
        
        if (pageName == "translationhome")
            sub1.Attributes.Add("class", "on");
        else if (pageName == "translationmain" || (pageName == "translationdetailmain" &&  new TranslateInfo().MatePageType == "10"))
            sub2.Attributes.Add("class", "on");
        else if (pageName == "translationcomplete" || (pageName == "translationdetailmain" &&  new TranslateInfo().MatePageType == "11"))
            sub3.Attributes.Add("class", "on");
        else if (pageName == "screeningmain" || (pageName == "translationdetailmain" &&  new TranslateInfo().MatePageType == "20"))
            sub4.Attributes.Add("class", "on");
        else if (pageName == "screeningcomplete" || (pageName == "translationdetailmain" &&  new TranslateInfo().MatePageType == "21"))
            sub5.Attributes.Add("class", "on");
        else if (pageName == "thirdplmain" || (pageName == "translationdetailmain" &&  new TranslateInfo().MatePageType == "40"))
            sub6.Attributes.Add("class", "on");
        else if (pageName == "thirdplcomplete" || (pageName == "translationdetailmain" &&  new TranslateInfo().MatePageType == "41"))
            sub7.Attributes.Add("class", "on");
        else if (pageName == "translationlettersearch" || (pageName == "translationdetailmain" &&  new TranslateInfo().MatePageType == "30"))
            sub8.Attributes.Add("class", "on");


        //sub9.Attributes.Add("class", "on");
    }

    //용어저장
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!UserInfo.IsLogin)
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("로그인을 하신후 글쓰기 하셔야 합니다.");
			//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/mate/default.aspx");
			sjs += JavaScript.GetPageMoveScript("/login.aspx");
			sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            return;
        }

        UserInfo user = new UserInfo();

        WWWService.Service service = new WWWService.Service();
        service.InsertBenefit("", eword.Value, dropInitial.Value, comment.Value
            , Convert.ToInt32(dropNation.SelectedValue), user.UserId, user.UserName);

        eword.Value = "";
        comment.Value = "";
        dropInitial.SelectedIndex = 0;
        dropNation.SelectedIndex = 0;


        string str = JavaScript.HeaderScript.ToString();
        str += JavaScript.GetAlertScript("수혜국 관련 용어가 저장 되었습니다");
        str += JavaScript.FooterScript.ToString();
        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", str);

        service.Dispose();

        return;
    }

    private void GetGuidelist()
    {
        int iCnt = 1;
        DataSet ds = new DataSet();
        //가이드 쿼리 (구약성경, 신약성경, 국가약칭, 수혜국 통화)
        Object[] objSql = new object[4] { "SELECT * FROM MATE_Help WHERE HelpType = 'BIBLE' AND Description = 'OLD'", "SELECT * FROM MATE_Help WHERE HelpType = 'BIBLE' AND Description = 'NEW'", "SELECT * FROM MATE_Help WHERE HelpType = 'COUNTRY' ORDER BY KorCode", "SELECT * FROM MATE_Help WHERE HelpType = 'CURRENCY' ORDER BY KorCode" };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

        StringBuilder sb = new StringBuilder();

        //구약성경
        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td>");
        sb.Append("                &nbsp 구약");
        sb.Append("           </td>");
        sb.Append("        </tr>");
        sb.Append("</table>");
        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td style='width:15%; border-top:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                약자");
        sb.Append("            </td>");
        sb.Append("            <td style='width:25%; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                제목");
        sb.Append("            </td>");
        sb.Append("             <td style='width:30%; border-top:1px solid #CCCCCC; border-left:1px solid #E6E6E6; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                약자(영문)");
        sb.Append("           </td>");
        sb.Append("            <td style='width:30%; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                제목(영문)");
        sb.Append("            </td>");
        sb.Append("        </tr>");

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            if (iCnt == ds.Tables[0].Rows.Count)
            {

                sb.Append("        <tr>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }
            else
            {
                sb.Append("        <tr>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }

            iCnt++;
        }

        sb.Append("    </table>");

        iCnt = 1;

        //신약성경
        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td>");
        sb.Append("                &nbsp 신약");
        sb.Append("           </td>");
        sb.Append("        </tr>");
        sb.Append("</table>");
        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td style='width:15%; border-top:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                약자");
        sb.Append("            </td>");
        sb.Append("            <td style='width:25%; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                제목");
        sb.Append("            </td>");
        sb.Append("             <td style='width:30%; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                약자(영문)");
        sb.Append("           </td>");
        sb.Append("            <td style='width:30%; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                제목(영문)");
        sb.Append("            </td>");
        sb.Append("        </tr>");

        foreach (DataRow dr in ds.Tables[1].Rows)
        {
            if (iCnt == ds.Tables[1].Rows.Count)
            {

                sb.Append("        <tr>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }
            else
            {
                sb.Append("        <tr>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }

            iCnt++;
        }

        sb.Append("    </table>");

        sgBibleList = sb.ToString();

        iCnt = 1;
        sb = new StringBuilder();

        //국가약칭
        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td style='border-top:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                국가코드");
        sb.Append("            </td>");
        sb.Append("            <td style='border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                국가명");
        sb.Append("            </td>");
        sb.Append("        </tr>");

        foreach (DataRow dr in ds.Tables[2].Rows)
        {
            if (iCnt == ds.Tables[2].Rows.Count)
            {

                sb.Append("        <tr>");
                sb.Append("            <td style='border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }
            else
            {
                sb.Append("        <tr>");
                sb.Append("            <td style='border-top:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }

            iCnt++;
        }
        sb.Append("    </table>");

        sgCountryList = sb.ToString();

        iCnt = 1;
        sb = new StringBuilder();

        //수혜국통화
        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td style='width:25%; border-top:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                국가코드");
        sb.Append("            </td>");
        sb.Append("            <td style='width:20%; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                국가명");
        sb.Append("            </td>");
        sb.Append("             <td style='width:25%; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                화폐단위");
        sb.Append("           </td>");
        sb.Append("            <td style='width:30%; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                화폐단위명");
        sb.Append("            </td>");
        sb.Append("        </tr>");

        foreach (DataRow dr in ds.Tables[3].Rows)
        {
            if (iCnt == ds.Tables[3].Rows.Count)
            {

                sb.Append("        <tr>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }
            else
            {
                sb.Append("        <tr>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; word-break:break-all; border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC;'>");
                sb.Append(dr["EngName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }

            iCnt++;
        }
        sb.Append("    </table>");

        sgCurrencyList = sb.ToString();

        ds.Dispose();
        _WWW6Service.Dispose();
    }
}