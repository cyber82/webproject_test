﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TranslationCompleteList.ascx.cs" Inherits="Mate_Controls_TranslationCompleteList" %>
<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>

<link type="text/css" href="../../common/js/inc/js/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../common/js/inc/js/ui/demos.css" rel="stylesheet" />
<style type="text/css">
    .sbox
    {
        border: 1px solid #bbb;
    }
</style>
<script type="text/javascript" src="../../common/js/inc/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.core.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../common/js/inc/js/ui/ui.datepicker-ko.js"></script>

<script type="text/javascript">
<!--
    $(function () {
        $('#start_date').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    $(function () {
        $('#end_date').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    function ShowDetail(sCorrID, sPackageID, sPageCount) {
        //수정 2013-08-28
        //location.href = '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount;
        /*
        var f = top.document.form1;

        f.action = '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount;
        f.method = "post";
        f.target = "_top";
        f.submit();
        */
        var f = $("#form1");
        f.attr("action", '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount);
        f.attr("method", "post");
        f.attr("target", "_top");
        f.submit();
    }

    function trColor(sw, idx) {
        var id = document.getElementById("dataTr_" + idx);

        if (sw == "1") {
            id.style.backgroundColor = "#ddd";
        }
        else if (sw == "0") {
            id.style.backgroundColor = "#fff";
        }
    }

    function SetDate(sSDate, sEDate) {
        document.getElementById("start_date").value = sSDate;
        document.getElementById("end_date").value = sEDate;
    }
//-->
</script>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <div style="text-align: left">
            <asp:Label ID="lblTotalCount" runat="server" Text="Label"></asp:Label>
            </div>
        </td>
    <td>
        <div style="text-align: right">
        <%--편지개수:&nbsp--%>
            기간 : &nbsp
            <input type="text" id="start_date" name="start_date" size="10" maxlength="10" value=""
                style="ime-mode: disabled; height: 20px; line-height: 20px;" class="sbox" onkeydown="javascript:OnlyNumber();" />
            &nbsp; ~ &nbsp;
            <input type="text" id="end_date" name="end_date" size="10" maxlength="10" value=""
                style="ime-mode: disabled; height: 20px; line-height: 20px;" class="sbox" onkeydown="javascript:OnlyNumber();" />
            &nbsp;&nbsp;
            <asp:ImageButton ID="imgBtnSearch" runat="server" OnClick="imgBtnSearch_Click" ImageUrl="~/image/imate/btn/btn_search.jpg" ToolTip="기간 설정 후, 조회합니다." />
        </div>
    </td>
</tr>
</table>
<br/>

<table border="0" cellpadding="0" cellspacing="0" class="mate-list">
    <thead>
        <tr>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                번역완료
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                스크리너
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                스크리닝완료
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                스캔일자
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                편지ID
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                패키지ID
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                편지타입
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                어린이키
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                어린이명
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                후원자키
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                후원자명
            </th>
            <th style='background-color: #E6E6E6; height: 25px; border: 1px solid #CCCCCC;'>
                쪽수
            </th>
        </tr>
    </thead>
    <tbody>
        <asp:Repeater ID="repData" runat="server">
            <ItemTemplate>
                <tr>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "TranslationDate", "{0:yyyy-MM-dd}")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ScreeningName")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ScreeningDate", "{0:yyyy-MM-dd}")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ScanningDate", "{0:yyyy-MM-dd}")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px; cursor:pointer;' bgcolor="#C6DFFF" onclick="ShowDetail('<%#DataBinder.Eval(Container.DataItem, "CorrID")%>','<%#DataBinder.Eval(Container.DataItem, "PackageID")%>','<%#DataBinder.Eval(Container.DataItem, "CorrPage")%>'); return false;">
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "CorrID")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "PackageID")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "CorrType")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ChildKey")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ChildName")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "ConID")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "SponsorName")%></center>
                    </td>
                    <td style='border: 1px solid #CCCCCC; height: 27px;'>
                        <center>
                            <%#DataBinder.Eval(Container.DataItem, "CorrPage")%></center>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </tbody>
</table>

<% //추가 2013-08-28 %>
<input type="hidden" id="hdIdxPage" name="hdIdxPage" runat="server" />

<!-- paginator -->
<div class="paginator">
    <taeyo:PagingHelper ID="PagingHelper1" runat="server" OnOnPageIndexChanged="PagingHelper1_OnPageIndexChanged"
        CurrnetNumberColor="Silver" ForeColor="#404040"></taeyo:PagingHelper>
</div>
<!-- // paginator -->
