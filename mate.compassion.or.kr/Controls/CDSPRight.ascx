﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CDSPRight.ascx.cs" Inherits="Controls_CDSPRight" %>

<%@ Register src="/Controls/RightBanner.ascx" tagname="Banner" tagprefix="bc1" %>

<!-- sidebar -->
<div id="sidebar" class="sec-cdsp">
	<div class="tit">1:1 어린이 양육</div>

	<ul id="lnb" class="lnb">
		<li id="sub1"><strong class="dep1"><span>1:1 어린이 양육 프로그램</span></strong></li>
	</ul>

    <% //추가 2012-08-31 %>
    <div class=""><a href="/Payment/CartProgram.aspx"><img src="/image/banner/lnb-cart.jpg" alt="" border="0"></a></div>
	
    <% //수정 2013-03-29, 2013-09-23 %>
    <ul class="banner-relation">
		<%--<li><a class="bn-ldp" href="/Sponsor/Ldp/ldpList.aspx">1:1 리더십 양육</a></li>--%>
		<li><a class="bn-csp" href="/Sponsor/CSPDetail.aspx">태아/영아 살리기</a></li>
		<li><a class="bn-civ" href="/Sponsor/CIVList.aspx">양육을 돕는 후원</a></li>
	</ul>
	<div class="banner1"><a href="/Sponsor/GiftList.aspx"><img src="/image/banner/banner-goc.jpg" alt=""></a></div>

	<div class="div">
		<dl>
			<dt><p style="font-size:10pt;font-weight:bold;">컴패션 스토리</p></dt>
			<dd>
				<ul class="list-lnb">
                    <asp:Repeater ID="repLoved" runat="server">
                    <ItemTemplate>
					<li>
						<a href="http://www.iamcompassion.or.kr/story/view.aspx?Idx=<%#DataBinder.Eval(Container.DataItem, "idx")%>&boardid=<%#DataBinder.Eval(Container.DataItem, "board_id")%>&Mode=read" target="_blank">
							<img src="http://www.iamcompassion.or.kr/ssBoard/thumbnail/<%#DataBinder.Eval(Container.DataItem, "idx")%>.jpg" width="54" height="54" class="thumb" alt="아이들을 부모님만큼 사랑해줄 거예요" />
							<strong><%#WebCommon.GetNamePadding (DataBinder.Eval(Container.DataItem, "title"),40)%></strong>
                            <dd class="cont">
                                <asp:Label ID="lb_content" runat="server" Text=""></asp:Label>
                            </dd>
						</a>
					</li>
                    </ItemTemplate>
                    </asp:Repeater>
				</ul>
			</dd>
		</dl>
		<a href="http://www.iamcompassion.or.kr/Story/Default.aspx" target ="_blank"  class="btn btn-more"><span>more</span></a>
	</div>
	<div class="div">
		<dl>
			<dt class="txt tit-faq">자주 묻는 질문</dt>
			<dd>
				<ul class="list-article">
                <asp:Repeater ID="repFaq" runat="server">
                    <ItemTemplate>
					<li><a href="/MemberShip/FaqList.aspx?faqType=일대일결연"><%#DataBinder.Eval(Container.DataItem, "b_title")%></a></li>
					</ItemTemplate>
                </asp:Repeater>
				</ul>
			</dd>
		</dl>
		<a href="/MemberShip/FaqList.aspx?faqType=일대일결연" class="btn btn-more"><span>more</span></a>
	</div>
	<div class="banner2">
		<a href="/MemberShip/GuideCompassion.aspx"><img src="/image/banner/supporter-guide.png" width="240" height="139" alt="" /></a>
	</div>

	<ul class="banner1">
		<% //수정 2013-05-28 %>
        <bc1:Banner ID="banner1" runat="server" />
		
        <% //주석처리 2013-05-28 %>
        <%--<li><a href="/AboutUs/noticeview.aspx?iTableIndex=1001&iNoIndex=318&iBoardIdx=67376&ref=47200&re_step=0&pageIdx=0&searchOption=&searchTxt="><img src="/image/banner/charity-navigator.png" width="240" height="100" alt="" /></a></li>
		<li><a href="/Intro/MateIntro.aspx"><img src="/image/banner/mate.png" width="240" height="90" alt="" /></a></li>
        <li><a href="http://www.hanscake.co.kr/2011/story/responsibility.php" target="_blank"><img src="/image/banner/hanscake.png" width="240" height="92" alt="" /></a></li>
		<li><a href="/Share/VisionTripApply.aspx"><img src="/image/banner/vision-trip2012.png" width="240" height="90" alt="" /></a></li>--%>
	</ul>
</div>
<!-- //sidebar -->