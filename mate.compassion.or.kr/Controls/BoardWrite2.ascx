﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BoardWrite2.ascx.cs" Inherits="Mate_Controls_BoardWrite2" %>
<table class="write1">
	<tbody>
		<tr>
			<th class="td-writer"><img src="/image/mate/text/td-writer.gif" alt="작성자" /></th>
			<td colspan="2"><%=UserName%></td>
		</tr>
		<tr>
			<th><img src="/image/text/td-title.gif" alt="제목" /></th>
			<td colspan="2"><input type="text" class="text" style="width:344px;height:20px;" id="txtTitle" runat ="server"/></td>
		</tr>
        <tr>
			<th><img src="/image/text/td-sort3.gif" alt="구분" /></th>
			<td style="width:8%"><input type="radio" name="rdpublic" id="rdpublic_Y" runat="server" /> 공개</td>
            <td><input type="radio" id="rdpublic_N" name="rdpublic" runat="server" /> 비공개</td>
		</tr>	
	</tbody>
</table>
<div>
	<textarea rows="23" cols="90" id="txtComment" runat ="server" style="border-width:2px"></textarea>
</div>
<div class="btn-list">
	<div class="btn-r">
         <asp:Button ID="btnSave" runat="server" Text="" CssClass ="btn btn-modify" 
                        onclick="btnSave_Click" OnClientClick ="return WriteValidate();" />
		<a href="javascript:history.back(-1);" class="btn btn-cancel"><span>취소하기</span></a>
	</div>
</div>
<asp:HiddenField ID="hidNoIndex" runat="server" />
<asp:HiddenField ID="hidBoardIndex" runat="server" />
