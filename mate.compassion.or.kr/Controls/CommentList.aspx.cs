﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;

public partial class Controls_CommentList : System.Web.UI.Page
{
    WWWService.Service _wwwService = new WWWService.Service();
    string sJs;


    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        int iTableIndex = Convert.ToInt32(Request.QueryString["iTableIndex"]);
        int iNoIndex = Convert.ToInt32(Request.QueryString["iNoIndex"]);
        int iBoardIdx = Convert.ToInt32(Request.QueryString["iBoardIdx"]);
        
        if (!IsPostBack)
        {
            GetCommentData();
        }
    }


    private void GetCommentData()
    {
        int iTableIndex = Convert.ToInt32(Request.QueryString["iTableIndex"]);
        int iNoIndex = Convert.ToInt32(Request.QueryString["iNoIndex"]);
        int iref = Convert.ToInt32(Request.QueryString["ref"]);
        int ire_step = Convert.ToInt32(Request.QueryString["re_step"]);

        DataSet data = _wwwService.CommentList(iTableIndex, iNoIndex, iref, ire_step);
        repCommentData.DataSource = data.Tables[0];
        repCommentData.DataBind();
    }

    protected string ParentPageName
    {
        get
        {
            string pageName = Context.Handler.GetType().Name;
            int findit = pageName.LastIndexOf("_aspx");
            pageName = pageName.Substring(0, findit);
            findit = pageName.IndexOf("_");
            pageName = pageName.Substring(findit + 1);
            if (pageName == "disasterdetail")
                return "disasterList.aspx";
            else if (pageName == "prayersdetail")
                return "prayersList.aspx";
            else if (pageName == "voccommunityview")
                return "VocCommunityList.aspx";
            else if (pageName == "yvoccommunityview")
                return "YVocCommunityList.aspx";
            else if (pageName == "noticeview")
                return "noticelist.aspx";
            else if (pageName == "pressview")
                return "presslist.aspx";
            else if (pageName == "stararchiveview")
                return "stararchivelist.aspx";
            else
                return "empty.aspx";
        }
    }

    protected void repCommentData_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Button btnDelete = (Button)e.Item.FindControl("btnDelete");
        HiddenField hidUserID = (HiddenField)e.Item.FindControl("hidUserID");
        if (UserInfo.IsLogin)
        {
            UserInfo userInfo = new UserInfo();
            if (userInfo.UserId == hidUserID.Value)
                btnDelete.Visible = true;
            else
                btnDelete.Visible = false;
        }
        else
            btnDelete.Visible = false;
    }



    protected void repCommentData_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            HiddenField hidUserID = (HiddenField)e.Item.FindControl("hidUserID");
            HiddenField hidcref = (HiddenField)e.Item.FindControl("hidcref");
            HiddenField hidtable_idx = (HiddenField)e.Item.FindControl("hidtable_idx");
            HiddenField hidre_step = (HiddenField)e.Item.FindControl("hidre_step");
            HiddenField hidwriteday = (HiddenField)e.Item.FindControl("hidwriteday");
            int seqno = Convert.ToInt32(e.CommandArgument);

            _wwwService.CommentDelete(seqno, hidUserID.Value, Convert.ToInt32(hidtable_idx.Value), Convert.ToInt32(hidcref.Value), Convert.ToInt32(hidre_step.Value), hidwriteday.Value);
            GetCommentData();
        }
    }
}