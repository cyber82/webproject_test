﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CSPRight.ascx.cs" Inherits="Controls_CSPRight" %>

<%@ Register src="/Controls/RightBanner.ascx" tagname="Banner" tagprefix="bc1" %>

<div id="sidebar" class="sec-csp">
	<div class="tit">태아/영아 살리기</div>
	<ul id="lnb" class="lnb">
		<li id="sub1"><strong class="dep1"><span>태아/영아 살리기</span></strong></li>
	</ul>

    <% //추가 2012-08-31 %>
    <div class=""><a href="/Payment/CartProgram.aspx"><img src="/image/banner/lnb-cart.jpg" alt="" border="0"></a></div>
	
    <% //수정 2013-03-29, 2013-09-23 %>
    <ul class="banner-relation">
		<li><a class="bn-cdsp" href="/Sponsor/CDSPList.aspx">1:1 어린이 양육</a></li>
		<%--<li><a class="bn-ldp" href="/Sponsor/Ldp/ldpList.aspx">1:1 리더십 양육</a></li>--%>
		<li><a class="bn-civ" href="/Sponsor/CIVList.aspx">양육을 돕는 후원</a></li>
	</ul>
    <div class="banner1"><a href="/Sponsor/GiftList.aspx"><img src="/image/banner/banner-goc.jpg" alt=""></a></div>

	<div class="div">
		<p class="txt txt-support-children"><span>이 어린이를 후원해 주세요.</span></p>
		<div id="support-wrap" class="support-wrap">
			<ul id="support-inner">
                <% //수정 20120425 어린이 테이블 막기 %>
                <%--<asp:Repeater ID="repChild" runat="server" onitemcommand="repChild_ItemCommand">
                    <ItemTemplate>
				    <li>
					    <img src="<%#GetImgPath(DataBinder.Eval(Container.DataItem,"ChildKey"))%>" width="98" height="98" class="thumb" alt="까리나" />
					    <dl>
						    <dt><strong><%#DataBinder.Eval(Container.DataItem, "ChildName")%></strong></dt>
						    <dd class="name"><strong><%#DataBinder.Eval(Container.DataItem, "ChildCountry")%>/<%#DataBinder.Eval(Container.DataItem, "ChildAge")%>살</strong></dd>
						    <dd class="btn">
                                <asp:Button ID="btnSupport" CssClass="btn btn-relationship3" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "ChildMasterID")%>' CommandName ="Add" runat="server" Text="" />							    
						    </dd>
					    </dl>
				    </li>
                    </ItemTemplate>
                </asp:Repeater>--%>

                <% //수정 20120425 배너 열기 %>
                <div style="text-align:center;">
			        <a href="CDSPList.aspx"><img src="/image/btn/btn-relationship3.png" board="0" alt="결연하러 가기" /></a>
		        </div>
			</ul>
		</div>
        <% //수정 20120425 어린이 목록의 이전 다음 막기 %>
		<%--<div class="btn-move2">
			<a href="#support-wrap" id="btn-next" class="pre">이전</a>
			<a href="#support-wrap" id="btn-prev" class="next">다음</a>
		</div>--%>
		<script type="text/javascript">
			//<![CDATA[
			var objRolling = new ImageRotation();
			objRolling.objName = 'objRolling';
			objRolling.scrollDirection = 'direction'; // direction: 좌-우, 상-하. indirection: 우-좌, 하-상.
			objRolling.setScrollType('horizontal'); 	// 'horizontal', 'vertical', 'none';;
			objRolling.autoScroll = "none"; 			// 'none' 자동 동작 없습
			objRolling.scrollGap = 1000; 			//스크롤 시간 (1초: 1000)
			objRolling.listNum = 1; 					// 보여줄 li갯수
			objRolling.wrapId = "support-wrap"; 	//ul을 감싸고있는 box id
			objRolling.listId = "support-inner"; // ul의 id
			objRolling.btnNext = "btn-next"; 		//next 버튼 id
			objRolling.btnPrev = "btn-prev"; 		//prev 버튼 id
			objRolling.initialize();
			//]]>
		</script>
	</div>
	<div class="div">
		<dl>
			<dt class="txt tit-lovely-children">사랑하는 어린이</dt>
			<dd>
				<ul class="list-lnb">
                    <asp:Repeater ID="repLoved" runat="server">
                    <ItemTemplate>
					<li>
						<a href="http://www.iamcompassion.or.kr/story/view.aspx?Idx=<%#DataBinder.Eval(Container.DataItem, "idx")%>&boardid=<%#DataBinder.Eval(Container.DataItem, "board_id")%>&Mode=read" target="_blank">
							<img src="http://www.iamcompassion.or.kr/ssBoard/thumbnail/<%#DataBinder.Eval(Container.DataItem, "idx")%>.jpg" width="54" height="54" class="thumb" alt="아이들을 부모님만큼 사랑해줄 거예요" />
							<strong><%#WebCommon.GetNamePadding (DataBinder.Eval(Container.DataItem, "title"),40)%></strong>
							<!--<span>저는 어린이들이 사는...</span>-->
						</a>
					</li>		
                    </ItemTemplate>
                    </asp:Repeater>
				</ul>
			</dd>
		</dl>
		<a href="http://www.iamcompassion.or.kr/Story/Default.aspx" target ="_blank"  class="btn btn-more"><span>more</span></a>
	</div>
	<div class="banner2">
		<a href="/MemberShip/GuideCompassion.aspx"><img src="/image/banner/supporter-guide.png" width="240" height="139" alt="" /></a>
	</div>

	<ul class="banner1">
		<% //수정 2013-05-28 %>
        <bc1:Banner ID="banner1" runat="server" />
		
        <% //주석처리 2013-05-28 %>
        <%--<li><a href="/AboutUs/noticeview.aspx?iTableIndex=1001&iNoIndex=318&iBoardIdx=67376&ref=47200&re_step=0&pageIdx=0&searchOption=&searchTxt="><img src="/image/banner/charity-navigator.png" width="240" height="100" alt="" /></a></li>
		<li><a href="/Intro/MateIntro.aspx"><img src="/image/banner/mate.png" width="240" height="90" alt="" /></a></li>
        <li><a href="http://www.hanscake.co.kr/2011/story/responsibility.php" target="_blank"><img src="/image/banner/hanscake.png" width="240" height="92" alt="" /></a></li>
		<li><a href="/Share/VisionTripApply.aspx"><img src="/image/banner/vision-trip2012.png" width="240" height="90" alt="" /></a></li>--%>
	</ul>
</div>