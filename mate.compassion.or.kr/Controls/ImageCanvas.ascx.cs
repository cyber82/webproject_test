﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;


public partial class Controls_ImageCanvas : System.Web.UI.UserControl
{
    public Bitmap bitmap;
    public ContentType ImageType = ContentType.Jpeg;

    void Page_Load(Object sender, EventArgs e)
    {
        //이미지컨트롤에 Attribute등록   
        String Url = Request.Url.ToString();
        DyImage.Attributes.Add("onclick", "OpenImg('" + Url + "','" + hidImageID.Value.ToString().Trim() + "');");
        try
        {
            if (Request.Params[ID] != null)
            {
                Response.Clear();
                Response.ContentType = "Image/" + ImageType.ToString();
                bitmap.Save(Response.OutputStream, getFormat((int)ImageType));
                Response.End();
            }

            if (Url.IndexOf("?") == -1)
            {
                DyImage.ImageUrl = Url + "?" + ID + "=Show";

            }
            else
            {
                DyImage.ImageUrl = Url + "&" + ID + "=Show";

            }
        }
        catch (Exception)
        {
            DyImage.ImageUrl = "../images/NoImage.gif";

        }
    }

    ImageFormat getFormat(int i)
    {
        ImageFormat imgfmt = ImageFormat.Jpeg;
        switch (i)
        {
            case 0:
                imgfmt = ImageFormat.Gif;
                break;
            case 1:
                imgfmt = ImageFormat.Jpeg;
                break;
            case 2:
                imgfmt = ImageFormat.Png;
                break;
        }
        return imgfmt;
    }

    public enum ContentType
    {
        Gif = 0, Jpeg = 1, Png = 2
    } 
}