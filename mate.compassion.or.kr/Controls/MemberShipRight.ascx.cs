﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_MemberShipRight : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RightMenuSet();

            if (UserInfo.IsLogin)
            {
                sub1.Visible = false;
                sub2.Visible = false;
                sub3.Visible = false;
            }
            else
            {
                sub1.Visible = true;
                sub2.Visible = true;
                sub3.Visible = true;
            }
        }
    }

    private void RightMenuSet()
    {
        string pageName = Context.Handler.GetType().Name;
        int findit = pageName.LastIndexOf("_aspx");
        pageName = pageName.Substring(0, findit);
        findit = pageName.IndexOf("_");
        pageName = pageName.Substring(findit + 1);

        subEl5.Style.Clear();
        if (pageName.StartsWith ("guide"))
            subEl5.Style.Add("display", "block");
        else
            subEl5.Style.Add("display", "none");

    }
}