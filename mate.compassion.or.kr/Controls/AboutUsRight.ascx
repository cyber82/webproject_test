﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AboutUsRight.ascx.cs" Inherits="Controls_AboutUsRight" %>

<%@ Register src="/Controls/RightBanner.ascx" tagname="Banner" tagprefix="bc1" %>

<div id="sidebar" class="sec-about-us">
	<div class="tit">한국컴패션 ABOUT US</div>
	<ul id="lnb" class="lnb">
		<li id="sub1">
			<a href="/AboutUs/Introduce.aspx" class="dep1"><span>컴패션소개</span></a>
			<ul id="subEl1" runat ="server">
				<li id="sub1_1"><a href="/AboutUs/Introduce.aspx" class="dep1-1"><span>컴패션 철학</span></a></li>
				<li id="sub1_2"><a href="/AboutUs/CeoGreeting.aspx" class="dep1-2"><span>CEO 인사말</span></a></li>
				<li id="sub1_3"><a href="/AboutUs/History.aspx" class="dep1-3"><span>컴패션 역사관</span></a></li>
                <li id="sub1_4"><a href="/AboutUs/BrandingGuide.aspx" class="dep1-4"><span>브랜딩 가이드</span></a></li>
				<li id="sub1_5"><a href="/AboutUs/Organization.aspx" class="dep1-5"><span>부서안내</span></a></li>
			</ul>
		</li>
		<li id="sub2">
			<a href="/AboutUs/PerformanceReporting.aspx" class="dep2"><span>연례보고서</span></a>
			<ul id="subEl2" runat ="server">
				<li id="sub2_1"><a href="/AboutUs/PerformanceReporting.aspx" class="dep2-1"><span>사업성과보고서</span></a></li>
				<li id="sub2_2"><a href="/AboutUs/RevenueCosts.aspx" class="dep2-2"><span>재무보고</span></a></li>
			</ul>
		</li>
		<li id="sub3">
			<a href="/AboutUs/SustainabilityAbout.aspx" class="dep3"><span>한국컴패션의 지속가능성</span></a>
			<ul id="subEl3" runat ="server">
				<li id="sub3_1"><a href="/AboutUs/SustainabilityAbout.aspx" class="dep3-1"><span>지속가능성이란?</span></a></li>
				<li id="sub3_2"><a href="/AboutUs/SustainabilityReport.aspx" class="dep3-2"><span>지속가능성/COP 보고서</span></a></li>
			</ul>
		</li>
		<li id="sub4"><a href="/AboutUs/HonoraryAmbassador.aspx" class="dep4"><span>홍보대사</span></a></li>
        <li id="sub5"><a href="/recruit/Process.aspx" class="dep5"><span>인재채용</span></a></li>
		<li id="sub6"><a href="/AboutUs/NoticeList.aspx" class="dep6"><span>공지사항</span></a></li>
		<li id="sub7"><a href="/AboutUs/PressList.aspx" class="dep7"><span>보도자료</span></a></li>
		<li id="sub8"><a href="/AboutUs/Magazine.aspx?num=4" class="dep8"><span>소식지</span></a></li>
		<li id="sub9"><a href="/AboutUs/Map.aspx" class="dep9"><span>찾아오시는길</span></a></li>
	</ul>
	<div class="div">
        <p class="txt txt-support-children"><span>이 어린이를 후원해 주세요.</span></p>
		<div id="support-wrap2" class="support-wrap">
			<ul id="support-inner2">
                <% //수정 20120425 어린이 테이블 막기 %>
                <%--<asp:Repeater ID="repChild" runat="server" onitemcommand="repChild_ItemCommand">
                    <ItemTemplate>
				    <li>
					        <img src="<%#GetImgPath(DataBinder.Eval(Container.DataItem,"ChildKey"))%>" width="98" height="98" class="thumb" alt="까리나" />
					        <dl>
						        <dt><strong><%#DataBinder.Eval(Container.DataItem, "ChildName")%></strong></dt>
						        <dd class="name"><strong><%#DataBinder.Eval(Container.DataItem, "ChildCountry")%>/<%#DataBinder.Eval(Container.DataItem, "ChildAge")%>살</strong></dd>
						        <dd class="btn">
                                    <asp:Button ID="btnSupport" CssClass="btn btn-relationship3" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "ChildMasterID")%>' CommandName ="Add" runat="server" Text="" />							    
						        </dd>
					        </dl>
				        </li>
                    </ItemTemplate>
                </asp:Repeater>--%>

                <% //수정 20120425 배너 열기 %>
                <div style="text-align:center;">
			        <a href="../Sponsor/CDSPList.aspx"><img src="/image/btn/btn-relationship3.png" board="0" alt="결연하러 가기" /></a>
		        </div>
			</ul>
		</div>
        <% //수정 20120425 어린이 목록의 이전 다음 막기 %>
		<%--<div class="btn-move2">
			<a href="#support-wrap2" id="btn-next1" class="pre">이전</a>
			<a href="#support-wrap2" id="btn-prev1" class="next">다음</a>
		</div>--%>
		<script type="text/javascript">
			//<![CDATA[
			var objRolling = new ImageRotation();
			objRolling.objName = 'objRolling';
			objRolling.scrollDirection = 'direction'; // direction: 좌-우, 상-하. indirection: 우-좌, 하-상.
			objRolling.setScrollType('horizontal'); 	// 'horizontal', 'vertical', 'none';;
			objRolling.autoScroll = "none"; 			// 'none' 자동 동작 없습
			objRolling.scrollGap = 1000; 			//스크롤 시간 (1초: 1000)
			objRolling.listNum = 1; 					// 보여줄 li갯수
			objRolling.wrapId = "support-wrap2"; 	//ul을 감싸고있는 box id
			objRolling.listId = "support-inner2"; // ul의 id
			objRolling.btnNext = "btn-next1"; 		//next 버튼 id
			objRolling.btnPrev = "btn-prev1"; 		//prev 버튼 id
			objRolling.initialize();
			//]]>
		</script>
	</div>

	<ul class="banner1">
        <% //수정 2013-05-28 %>
        <bc1:Banner ID="banner1" runat="server" />
		
        <% //주석처리 2013-05-28 %>
        <%--<li><a href="/AboutUs/noticeview.aspx?iTableIndex=1001&iNoIndex=318&iBoardIdx=67376&ref=47200&re_step=0&pageIdx=0&searchOption=&searchTxt="><img src="/image/banner/charity-navigator.png" width="240" height="100" alt="" /></a></li>
		<li><a href="/Intro/MateIntro.aspx"><img src="/image/banner/mate.png" width="240" height="90" alt="" /></a></li>
        <li><a href="http://www.hanscake.co.kr/2011/story/responsibility.php" target="_blank"><img src="/image/banner/hanscake.png" width="240" height="92" alt="" /></a></li>
		<li><a href="/Share/VisionTripApply.aspx"><img src="/image/banner/vision-trip2012.png" width="240" height="90" alt="" /></a></li>--%>
	</ul>
</div>
