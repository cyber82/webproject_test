﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Text;
using System.Web.Configuration;
using System.Collections;

public partial class Mate_Controls_TranslationLetterSearchList : System.Web.UI.UserControl
{
    int _PAGESIZE = 10;
    int _tableIdx;
    bool bgGetLetter;

    protected string _pageIdx = "0";


    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        string sJs = string.Empty;
        string sgSearch = string.Empty;
        string sSearchKind = string.Empty;
        string sButton = string.Empty;

        if (UserInfo.IsLogin)
        {
            //수정 2013-09-03
            txtSearchText.Attributes["onkeypress"] = "e = window.event; if (e.keyCode == 13) {GetTextValue(); "
                                                + Page.GetPostBackEventReference(imgBtnSearch) + "; return false;}";

            string sUserId = new UserInfo().UserId;

            sgSearch = Request["txtSearch"];
            sSearchKind = Request["txtSearchKind"];
            sButton = Request["txtButton"];


            //수정 2013-08-28
            if (string.IsNullOrEmpty(sgSearch))
            {
                sgSearch = txtSearchText.Text.ToString();
            }

            else
            {
                txtSearchText.Text = sgSearch;
            }

            //주석처리 2013-08-28
            //sgSearch = sgSearch.Trim();

            //수정 2013-08-28
            if (string.IsNullOrEmpty(sSearchKind))
            {
                sSearchKind = this.ddlSearchType.SelectedIndex.ToString();
            }

            else
            {
                if (sSearchKind.ToString() != "")
                {
                    ddlSearchType.SelectedIndex = int.Parse(sSearchKind.ToString());
                }

                else
                {
                    ddlSearchType.SelectedIndex = 0;
                }
            }
                     
            //추가 2013-09-03
            //int pidx = 0;
            //if (hdIdxPage.Value != "")
            //{
            //    pidx = Convert.ToInt32(hdIdxPage.Value.ToString().Trim());
            //}
            
            //수정 2013-08-28, 2013-09-03
            DataBind(Convert.ToInt32(_pageIdx), sgSearch.Trim(), sSearchKind, "");
            //DataBind(pidx, sgSearch.Trim(), sSearchKind);

            //검색타입
            if (!IsPostBack)
            {
                ArrayList aList = new ArrayList();
                aList.Add("편지ID");
                aList.Add("어린이키");
                this.ddlSearchType.DataSource = aList;
                this.ddlSearchType.DataBind();
            }

            //검색창 셋팅
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type='text/javascript' language ='javascript' >\n ");
            sb.Append("SetTextValue('" + sgSearch + "','" + ddlSearchType.SelectedIndex.ToString() + "','" + sSearchKind + "');");
            if (sButton == "Y") sb.Append("SetButtonEmpty();");
            sb.Append("</script>");
            Page.RegisterStartupScript("TranslationLetterSearch", sb.ToString());

            //if (sButton != "Y")
            //{
                //페이지 타입 쿠키 저장
                new TranslateInfo().MatePageType = "30";
            //}
        }
        else
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("로그인 후 사용 가능합니다.");
			//sJs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=" + Server.UrlEncode(Request.Url.AbsoluteUri));
			sJs += JavaScript.GetPageMoveScript("/login.aspx");
			sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }
    }

    #region 데이터 바인딩
    private void DataBind(int idxPage, string sSearch, string sSearchKind, string sButton)
    {
        DataSet data = new DataSet();

        string sUserId = new UserInfo().UserId;

        StringBuilder sbList = new StringBuilder();

        //추가 2013-09-03
        //TaeyoNetLib.PagingHelper PagingHelper1 = new TaeyoNetLib.PagingHelper();
        //PagingHelper1.Attributes.Add("OnOnPageIndexChanged", "PagingHelper1_OnPageIndexChanged");
        //PagingHelper1.Attributes.Add("CurrnetNumberColor", "Silver");
        //PagingHelper1.Attributes.Add("ForeColor", "#404040");
        //if (sButton == "Y")
        //{
        //    paginator.Controls.Remove(PagingHelper1);
        //}
        //paginator.Controls.Add(PagingHelper1);

        //수정 2013-08-28
        if (sSearch != "")
        {
            try
            {
                sbList.Append("SELECT  ");
                sbList.Append("TranslationName, ");
                sbList.Append("( CASE SUBSTRING(a.CorrType, 1, 3) WHEN 'CHI' THEN B.ScanningDate ELSE A.ScanningDate END ) AS ScanningDate, ");
                sbList.Append("PackageID, ");
                sbList.Append("A.CorrID, ");
                sbList.Append("CorrType, ");
                sbList.Append("SponsorName, ");
                sbList.Append("ChildKey, ");
                sbList.Append("ChildName, ");
                sbList.Append("CorrPage, ");
                sbList.Append("ConID ");
                sbList.Append("FROM CORR_Master A ");
                sbList.Append("LEFT OUTER JOIN Corr_FirstDate B ON A.CorrID = B.CorrID ");

                sbList.Append("left outer join ( ");
                sbList.Append(" select CorrID, max(ResultDT) ResultDT from MATE_TranslateReport group by CorrID ");
                sbList.Append(") C on A.CorrID = C.CorrID ");
                sbList.Append("left outer join MATE_TranslateReport D on C.CorrID = D.CorrID and C.ResultDT = D.ResultDT ");
                
                sbList.Append("WHERE 1=1 ");

                //sbList.Append(" AND ISNULL(A.TranslationStatus,'') != 'report' ");
                //sbList.Append(" AND ISNULL(A.ScreeningStatus,'') != 'report' ");
                sbList.Append(" AND ISNULL(D.ProcessStatus, '') not in ('followup', 'process') ");

                sbList.Append(" AND ISNULL(A.IsDelete, 0) = 0 ");

                //주석처리 2013-08-28
                //sbList.Append(" AND TranslationStatus IS NOT NULL ");

                if (ddlSearchType.SelectedIndex == 0)//편지ID
                {
                    sbList.Append(" AND A.CorrID = '" + sSearch + "' ");
                }
                else//어린이키
                {
                    sbList.Append(" AND ChildKey = '" + sSearch + "' ");
                }
                sbList.Append(" ORDER BY ScanningDate DESC ");

                Object[] objSql = new object[1] { sbList.ToString() };

                WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

                data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

                //리스트 카운터
                this.lblTotalCount.Text = "Total : " + data.Tables[0].Rows.Count;

                PagedDataSource pds = new PagedDataSource();
                pds.DataSource = data.Tables[0].DefaultView;
                pds.AllowPaging = true;
                pds.PageSize = _PAGESIZE;
                pds.CurrentPageIndex = idxPage;
                repData.DataSource = pds;
                repData.DataBind();
                
                PagingSetting(data.Tables[0].Rows.Count, idxPage, PagingHelper1);
            }
            catch (Exception ex)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("검색 문자를 확인해주세요");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }
        }
        else
        {
            //리스트 카운터
            this.lblTotalCount.Text = "Total : " + 0;

            DataTable dt = new DataTable();

            PagedDataSource pds = new PagedDataSource();
            pds.DataSource = dt.DefaultView;
            pds.AllowPaging = true;
            pds.PageSize = _PAGESIZE;
            pds.CurrentPageIndex = idxPage;
            repData.DataSource = pds;
            repData.DataBind();

            PagingSetting(0, 0, PagingHelper1);
        }

    }
    #endregion
    
    #region 하단목록
    private void PagingSetting(int totalCount, int curPage, TaeyoNetLib.PagingHelper PagingHelper1)
    {
        PagingHelper1.VirtualItemCount = totalCount;
        PagingHelper1.PageSize = _PAGESIZE;
        PagingHelper1.CurrentPageIndex = curPage;
    }

    protected void PagingHelper1_OnPageIndexChanged(object sender, TaeyoNetLib.PagingEventArgs e)
    {
        //수정 2013-08-28
        string sgSearch = string.Empty;
        string sSearchKind = string.Empty;

        if (txtSearchText.Text.Trim() != "")
        {
            sgSearch = Request["txtSearch"];
            sSearchKind = Request["txtSearchKind"];

            _pageIdx = e.PageIndex.ToString();
            hdIdxPage.Value = _pageIdx;
            DataBind(e.PageIndex, sgSearch, sSearchKind, "");
        }
    }
    #endregion

    #region 검색버튼
    protected void imgBtnSearch_Click(object sender, ImageClickEventArgs e)
    {
        //수정 2013-08-28, 2013-09-03
        string sgSearch = string.Empty;
        string sSearchKind = string.Empty;

        if (txtSearchText.Text.Trim() != "")
        {
            sgSearch = Request["txtSearch"];
            sSearchKind = Request["txtSearchKind"];

            _pageIdx = "0";
            hdIdxPage.Value = _pageIdx;
            DataBind(0, sgSearch, sSearchKind, "Y");
        }
        else
        {
            //Response.Redirect("/translation/TranslationLetterSearch.aspx");
        }
    }
    #endregion
}