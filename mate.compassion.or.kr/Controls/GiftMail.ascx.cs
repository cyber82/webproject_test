﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Web.SessionState;

using System.Security.Cryptography;
using System.Net.Mail;


public partial class Controls_GiftMail : System.Web.UI.UserControl
{

    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void btnMail_Click(object sender, EventArgs e)
    {
        //수정 2013-03-09
        string mailFrom = sendEmail.Value.ToString().Trim();
        string mailTo = receiveEmail.Value.ToString().Trim();
        string title = subject.Value.ToString();
        string body = comment.Value.ToString();
        string page = Request.QueryString["page"].ToString();
        string sJs = string.Empty;

        if (mailFrom != null && mailTo != null && title != null && body != null)
        {
            string Server = ConfigurationManager.AppSettings["SmtpServer"];

            MailMessage message = new MailMessage();

            // old : 10.180.10.34 
            // 수정 2013-03-09 : 10.180.10.47 
            SmtpClient client = new SmtpClient("10.180.10.47");        // 메일 서버 설정 포함
            //client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new System.Net.NetworkCredential("info", "Info2011");

            message.IsBodyHtml = true;

            message.From = new MailAddress(mailFrom);
            message.To.Add(new MailAddress(mailTo));

            message.Subject = title;
            message.Body = body + System.Environment.NewLine + System.Environment.NewLine + page;

            //Response.Write(message.Body.ToString());
            //Response.End();
            
            try
            {
                client.Send(message);

                sJs = JavaScript.HeaderScript;
                sJs += JavaScript.GetAlertScript("메일발송이 완료 되었습니다.");
                sJs += JavaScript.FooterScript;
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "success", sJs);
            }
            catch (Exception ex)
            {
                sJs = JavaScript.HeaderScript;
                sJs += JavaScript.GetAlertScript(ex.Message.Replace("\n", "\\n"));
                sJs += JavaScript.FooterScript;
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "error", sJs);
            }
        }
        else
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("입력한 값 중에 빈값이 들어 있습니다.");
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "error1", sJs);
        }
    }

}