﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Text;

public partial class Mate_Controls_TranslationMainList : System.Web.UI.UserControl
{
    int _PAGESIZE = 10;
    int _tableIdx;
    bool bgGetLetter = true;
    int ngLetterCount = 0;

    protected string _pageIdx = "0";

    public string sgScreeningStatusName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        string sJs;

        if (UserInfo.IsLogin)
        {
            string sUserId = new UserInfo().UserId;

            //권한 체크
            Object[] objSql = new object[1] { "MATE_MateYearCheck" };
            Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
            Object[] objValue = new object[2] { sUserId, "11" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //권한체크
            if (data.Tables[0].Rows.Count > 0)
            {
                DataBind(Convert.ToInt32(_pageIdx));
                //페이지 타입 쿠키 저장
                new TranslateInfo().MatePageType = "10";
            }

            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                sb.Append("alert('해당 권한이 없습니다.'); history.back();");
                sb.Append("</script>");

                Response.Write(sb.ToString());
            }

            data.Dispose();
            _WWW6Service.Dispose();
        }

        else
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("로그인 후 사용 가능합니다.");
			//sJs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=" + Server.UrlEncode(Request.Url.AbsoluteUri));
			sJs += JavaScript.GetPageMoveScript("/login.aspx");
			sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }   
    }

    private void DataBind(int idxPage)
    {
        string sUserId = new UserInfo().UserId;

        //리스트 출력
        //시작 시간에 해당 한 주가 지난 목록은 가져 오지 않는다
        Object[] objSql = new object[1] { "MATE_TranslateMainList" };
        Object[] objParam = new object[2] { "DIVIS", "TranslationID " };
        Object[] objValue = new object[2] { "MAINLIST", sUserId };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

        // 정렬
        DataView dv = data.Tables[0].DefaultView;
        dv.Sort = "ScanningDate";

        DataTable dt = dv.Table.Clone();

        foreach (DataRowView drv in dv)
            dt.ImportRow(drv.Row);

        data = new DataSet();
        data.Tables.Add(dt);

        for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            data.Tables[0].Rows[i]["Idx"] = i + 1;

        //리스트 카운터
        this.lblTotalCount.Text = "Total : " + data.Tables[0].Rows.Count;

        //번역 대기 중인 편지
        objSql = new object[1] { "MATE_TranslateMainList" };
        objParam = new object[2] { "DIVIS", "TranslationID" };
        objValue = new object[2] { "TRANSCOUNT", sUserId };

        ngLetterCount = Int32.Parse( _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0].Rows[0][0].ToString());

        this.lblNullLetterCount.Text = "번역이 필요한 편지 수: " + ngLetterCount.ToString() + "통";

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = data.Tables[0].DefaultView;
        pds.AllowPaging = true;
        pds.PageSize = _PAGESIZE;
        pds.CurrentPageIndex = idxPage;
        repData.DataSource = pds;
        repData.DataBind();

        PagingSetting(data.Tables[0].Rows.Count, idxPage);

        data.Dispose();
        _WWW6Service.Dispose();
    }

    private void PagingSetting(int totalCount, int curPage)
    {
        PagingHelper1.VirtualItemCount = totalCount;
        PagingHelper1.PageSize = _PAGESIZE;
        PagingHelper1.CurrentPageIndex = curPage;
    }

    protected void PagingHelper1_OnPageIndexChanged(object sender, TaeyoNetLib.PagingEventArgs e)
    {
        DataBind(e.PageIndex);
        _pageIdx = e.PageIndex.ToString();
    }

    protected void imgBtnGetLetter_Click(object sender, ImageClickEventArgs e)
    {
        int iResult = 0;
        string sjs = "";

        DateTime reqTime = DateTime.Now;
        if (CheckFreezingTime(reqTime) == false)
        {
            Response.Write("<script language='javascript'>alert('매주 월요일 00:00 ~ 00:10은 데이터 조정 시간입니다.\\r\\n잠시후 시도해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");
            return;
        }

        try
        {
            if (bgGetLetter)
            {
                if (ngLetterCount > 0)
                {
                    string sUserId = new UserInfo().UserId;

                    WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

                    //편지 가져오기
                    Object[] objSql = new object[1] { "MATE_TranslateSave" };
                    Object[] objParam = new object[1] { "MateID" };
                    Object[] objValue = new object[1] { sUserId };//번역

                    iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

                    _WWW6Service.Dispose();

                    if (iResult > 0)
                    {
                        DataBind(Convert.ToInt32(_pageIdx));
                        return;
                    }

                    else
                    {
                        objSql = new object[1] { "MATE_TranslateMainList" };
                        objParam = new object[2] { "DIVIS", "TranslationID" };
                        objValue = new object[2] { "MYTRANSCOUNT", sUserId };

                        sjs = JavaScript.HeaderScript.ToString();
                        sjs += JavaScript.GetAlertScript("메이트님의 일주일 번역가능 편지수(" + _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0].Rows[0][0].ToString() + ")를 초과하였습니다.");
                        sjs += JavaScript.FooterScript.ToString();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Alter", sjs);
                        return;
                    }
                }

                else
                {
                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("번역할 편지가 없습니다.\\n새로운 편지를 등록할 예정이오니\\n주중에 다시 [편지 가져오기]를 시도해주세요.");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alter", sjs);
                }
            }
        }

        catch (Exception ex)
        {
            //Exception Error Insert     
            WWWService.Service _wwwService = new WWWService.Service();
            _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("편지를 가져오는 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }

    //2018-05-17 이종진 - WO-171
    //월요일 00:00 스케쥴러가 처리안된 편지를 EXCEED 처리시킴 데이터의 충돌우려로 인해,
    //월요일 00:00 ~ 00:10 분까지는 '임시저장', '전송', '편지가져오기', '신고하기' 기능을 정지시킴
    //*시간은 서버시간으로 체크해야하므로 script로 처리하지않고, 로직에서 처리함
    bool CheckFreezingTime(DateTime now)
    {
        bool result = true;
        if (now.DayOfWeek == DayOfWeek.Monday)  //월요일
        {
            if (now.Hour == 0)  //0시
            {
                if (0 <= now.Minute && now.Minute <= 10)    //0~10분
                {
                    result = false;
                }
            }
        }

        return result;
    }

}