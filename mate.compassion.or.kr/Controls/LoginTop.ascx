﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LoginTop.ascx.cs" Inherits="Controls_LoginTop" %>
<div id="header_2" style="margin-bottom:1px;">
    <div class="quickNavArea"><div class="quickNav">
        <script>
            function getHost() {
                var host = '';
                var loc = String(location.href);
                if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
                else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
                else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
                return host;
            }

            function goLink(linkType) {
                var host = getHost();
                var param = '';
                if (linkType == 1) param = '/';
                else if (linkType == 2) param = '/Mate/Default.aspx';
                else if (linkType == 3) param = '/Membership/Login.aspx';
                else if (linkType == 4) param = '/Membership/RegisterAdult.aspx';
                else if (linkType == 5) param = '/event/CurrentEventList.aspx';
                else if (linkType == 6) param = '/recruit/Process.aspx';
                else if (linkType == 7) param = '/Mypage/ApplyInfo.aspx';
                else if (linkType == 8) param = '/event/CurrentEventList.aspx';
                else if (linkType == 9) param = '/recruit/Process.aspx';

                if (host != '') location.href = 'http://' + host + param;
            }
        </script>
	    <ul class="site">
		    <li class="s1 on"><a href="javascript:goLink(1)">한국컴패션</a></li>
		    <li class="s2"><a href="http://www.iamcompassion.or.kr/">I AM COMPASSION</a></li>
		    <li class="s3"><a href="javascript:goLink(2)">MATE</a></li>
	    </ul>
	    <div class="utilArea">
		    <ul class="util" id="login" runat="server">
			    <li style="padding-top:6px;"><a href="javascript:goLink(3)"><img src="../image/common/login.png" alt="로그인" /></a></li>
			    <li style="padding-top:6px;"><a href="javascript:goLink(4)"><img src="../image/common/join.png" alt="회원가입" /></a></li>
			    <li style="padding-top:6px;"><a href="javascript:goLink(5)"><img src="../image/common/event.png" alt="캠페인/이벤트" /></a></li>
			    <li style="padding-top:6px;"><a href="javascript:goLink(6)"><img src="../image/common/recruit.png" alt="인재채용" /></a></li>
			    <li style="padding-top:6px;"><a href="http://www.iamcompassion.or.kr/SiteMap.aspx?site=compassion"><img src="../image/common/sitemap.png" alt="사이트맵" /></a></li>
		    </ul>
		    <ul class="util" id="logout" runat="server" style="display:none;">
			    <li style="margin-right:2px; padding-top:10px;color:White;line-height:1.0;padding-right:14px;background:none;"><asp:Literal ID="labName2" runat="server"></asp:Literal></li>
                <li style="padding-top:6px;"><a href="javascript:goLink(7)"><img src="../image/common/mypage.png" alt="마이페이지" /></a></li> 
			    <li style="padding-top:6px;"><a href="/LogOff.aspx"><img src="../image/common/logout.png" alt="로그아웃" /></a></li>
                <li style="padding-top:6px;"><a href="javascript:goLink(8)"><img src="../image/common/event.png" alt="캠페인/이벤트" /></a></li>
			    <li style="padding-top:6px;"><a href="javascript:goLink(9)"><img src="../image/common/recruit.png" alt="인재채용" /></a></li>
			    <li style="padding-top:6px;"><a href="http://www.iamcompassion.or.kr/SiteMap.aspx?site=compassion"><img src="../image/common/sitemap.png" alt="사이트맵" /></a></li>
		    </ul>
		    <ul class="sns" id="sns" runat="server">
			    <li><a href="https://www.facebook.com/compassion.Korea" target="_blank"><img src="../image/common/sns_facebook.png" alt="facebook" /></a></li>
			    <li><a href="https://twitter.com/compassionkorea" target="_blank"><img src="../image/common/sns_twitter.png" alt="twitter" /></a></li>
			    <li><a href="http://www.youtube.com/user/CompassionKR" target="_blank"><img src="../image/common/sns_youyube.png" alt="youyube" /></a></li>
                <li><a href="https://www.instagram.com/compassionkorea" target="_blank"><img src="/image/common/sns_insta.jpg" alt="instagram" /></a></li>
		    </ul>
	    </div>
    </div></div>
</div>
<!-- // header -->

<script type="text/javascript">
<!--
    function MenuBlock() {
        $(".type2 > li div, .type3 > li div").css('display', 'block');
        $(".type2 > li div, .type3 > li div").css('visibility', 'hidden');
    }
-->
</script>
