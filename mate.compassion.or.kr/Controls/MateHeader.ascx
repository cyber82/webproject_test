﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MateHeader.ascx.cs" Inherits="Mate_Controls_MateHeader" %>
<meta charset="UTF-8">
<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title> 
<link rel="stylesheet" href="/common/css/mate.css?v=1.0" />
<link rel="stylesheet" href="/common/css/layout.css" />
<link rel="shortcut icon" href="/common/img/common/favicon.ico" />
<script type="text/javascript" src="/common/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/jquery.simplyscroll-1.0.4.min.js"></script>
<script type="text/javascript" src="/common/js/util.js"></script>
<script language="javascript" type="text/javascript">

    function ShareFaceBook() {
        window.open("http://www.facebook.com/sharer.php?u=" + encodeURIComponent('<%=this.Page.Request.Url.AbsoluteUri.Replace("'","")%>'));
    }

    function ShareFaceBookSummary(summary) {
        window.open("http://www.facebook.com/sharer.php?u=" + encodeURIComponent('<%=this.Page.Request.Url.AbsoluteUri.Replace("'","")%>') + "&t=test");
    }

    function ShareTwitter() {
        window.open("http://twitter.com/home?status=" + encodeURIComponent('<%=this.Page.Request.Url.AbsoluteUri.Replace("'","")%>'));
    }

    function CountryMove() {        
        var con = document.all.selCountry.value;                   
        if (con != "") {            
            window.open("http://" + con, "_blank");            
        }
    }

    function setClip() {
        window.clipboardData.setData('Text', '<%=this.Page.Request.Url.AbsoluteUri.Replace("'","")%>');
        alert('클립보드에 복사 되었습니다.\ncrtl+v(붙여넣기) 하세요.');
    }

</script>
