﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BoardList2.ascx.cs" Inherits="Mate_Controls_BoardList" %>
<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<table class="mate-list">
	<thead>
		<tr>
			<th class="td-num"><img src="/image/mate/text/td-num2.gif" alt="번호" /></th>
			<th class="td-title"><img src="/image/mate/text/td-title2.gif" alt="제목" /></th>
            <th class="td-writer"><img src="/image/mate/text/td-writer.gif" alt="작성자" /></th>
			<th class="td-date"><img src="/image/mate/text/td-date-reg2.gif" alt="등록일" /></th>
            <th class="td-hits"><img src="/image/mate/text/td-hits.gif" alt="조회수" /></th>
		</tr>
	</thead>
	<tbody>
        <asp:Repeater ID="repHeadData" runat="server" onitemdatabound="repHeadData_ItemDataBound">
            <ItemTemplate>
		    <tr class="first-tr">
			    <td class="td-num"><strong class="no-notice">[공지]</strong></td>
			    <td class="td-title"><a href="<%=ParentPageName%>?iTableIndex=<%#DataBinder.Eval(Container.DataItem,"table_idx")%>&iNoIndex=<%#DataBinder.Eval(Container.DataItem,"no_idx")%>&iBoardIdx=<%#DataBinder.Eval(Container.DataItem,"board_idx")%>&b_num=<%#DataBinder.Eval(Container.DataItem,"b_num")%>&re_level=<%#DataBinder.Eval(Container.DataItem,"re_level")%>&ref=<%#DataBinder.Eval(Container.DataItem,"ref")%>&re_step=<%#DataBinder.Eval(Container.DataItem,"re_step")%>&pageIdx=<%=_pageIdx%>&searchOption=<%=_searchOption%>&searchTxt=<%=_searchTxt%>"><%#DataBinder.Eval(Container.DataItem,"b_title")%><%#CntString(DataBinder.Eval(Container.DataItem, "cnt"))%> </a><asp:Label ID="labNew" runat="server" Text="new" CssClass="ico-new"></asp:Label></td>
			    <td class="td-writer"><%#DataBinder.Eval(Container.DataItem,"b_name")%></td>
                <td class="td-date"><%#DataBinder.Eval(Container.DataItem, "WriteDay")%></td>
                <td class="td-hits"><%#DataBinder.Eval(Container.DataItem, "b_readnum")%></td>
		    </tr>
            <asp:HiddenField ID="hidb_writeday" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "b_writeday")%>' />            
            </ItemTemplate> 
        </asp:Repeater>		
        <asp:Repeater ID="repData" runat="server" onitemdatabound="repData_ItemDataBound" >
            <ItemTemplate>
		    <tr class="first-tr">
			    <td class="td-num"><%#DataBinder.Eval(Container.DataItem,"no_idx")%></td>
			    <td class="td-title"><%#IsReply(DataBinder.Eval(Container.DataItem,"re_level"))%><a href="<%=ParentPageName%>?iTableIndex=<%#DataBinder.Eval(Container.DataItem,"table_idx")%>&iNoIndex=<%#DataBinder.Eval(Container.DataItem,"no_idx")%>&iBoardIdx=<%#DataBinder.Eval(Container.DataItem,"board_idx")%>&b_num=<%#DataBinder.Eval(Container.DataItem,"b_num")%>&re_level=<%#DataBinder.Eval(Container.DataItem,"re_level")%>&ref=<%#DataBinder.Eval(Container.DataItem,"ref")%>&re_step=<%#DataBinder.Eval(Container.DataItem,"re_step")%>&pageIdx=<%=_pageIdx%>&searchOption=<%=_searchOption%>&searchTxt=<%=_searchTxt%>&user_id=<%#DataBinder.Eval(Container.DataItem,"user_id")%>&PublicYN=<%#DataBinder.Eval(Container.DataItem,"PublicYN")%>"><%#IsBold(DataBinder.Eval(Container.DataItem,"b_title"),DataBinder.Eval(Container.DataItem,"board_idx"))%></a> <span class="comment-num"><%#CntString(DataBinder.Eval(Container.DataItem, "cnt"))%></span><asp:Label ID="labNew" runat="server" Text="new" CssClass="ico-new"></asp:Label></td>
                <td class="td-writer"><%#DataBinder.Eval(Container.DataItem,"b_name")%></td>
			    <td class="td-date"><%#DataBinder.Eval(Container.DataItem, "WriteDay")%></td>
                <td class="td-hits"><%#DataBinder.Eval(Container.DataItem, "b_readnum")%></td>
		    </tr>
            <asp:HiddenField ID="hidb_writeday" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "b_writeday")%>' />            
                           
            </ItemTemplate> 
        </asp:Repeater>        
	</tbody>
</table>
<!-- paginator -->
<div class="paginator">
		<taeyo:paginghelper id="PagingHelper1" runat="server"  ononpageindexchanged="PagingHelper1_OnPageIndexChanged" CurrnetNumberColor="Silver" ForeColor="#404040"></taeyo:paginghelper>                 
	</div>
<!-- // paginator -->
<!-- article search -->
<div class="article-search">
    <asp:DropDownList ID="dropOption" runat="server">
        <asp:ListItem  Value ="all">전체</asp:ListItem>
        <asp:ListItem  Value ="b_title">제목</asp:ListItem>
        <asp:ListItem  Value ="b_content">본문내용</asp:ListItem>
        <asp:ListItem  Value ="b_recontent">댓글내용</asp:ListItem>
        <asp:ListItem  Value ="b_name">작성자</asp:ListItem>                
    </asp:DropDownList>
    <asp:TextBox ID="txtSearch" CssClass ="text" runat="server" ></asp:TextBox>
    <asp:Button ID="btnSearch" runat="server" CssClass ="btn-search" Text="" 
        onclick="btnSearch_Click" OnClientClick ="return SearchValidate();" />
</div>
<!-- // article search -->
