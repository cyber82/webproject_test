﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventRight.ascx.cs" Inherits="Controls_EventRight" %>

<%@ Register src="/Controls/RightBanner.ascx" tagname="Banner" tagprefix="bc1" %>

<div id="sidebar" class="sec-event">
	<div class="tit">캠페인/이벤트 CAMPAIGN & EVENT</div>

	<ul id="lnb" class="lnb">
		<li id="sub1"><a href="/Event/CurrentEventList.aspx" class="dep1"><span>진행중인 캠페인/이벤트</span></a></li>
		<li id="sub2"><a href="/Event/PastEventList.aspx" class="dep2"><span>지난 캠페인/이벤트</span></a></li>
	</ul>

	<ul class="banner1">
		<% //수정 2013-05-28 %>
        <bc1:Banner ID="banner1" runat="server" />
		
        <% //주석처리 2013-05-28 %>
        <%--<li><a href="/AboutUs/noticeview.aspx?iTableIndex=1001&iNoIndex=318&iBoardIdx=67376&ref=47200&re_step=0&pageIdx=0&searchOption=&searchTxt="><img src="/image/banner/charity-navigator.png" width="240" height="100" alt="" /></a></li>
		<li><a href="/Intro/MateIntro.aspx"><img src="/image/banner/mate.png" width="240" height="90" alt="" /></a></li>
        <li><a href="http://www.hanscake.co.kr/2011/story/responsibility.php" target="_blank"><img src="/image/banner/hanscake.png" width="240" height="92" alt="" /></a></li>
		<li><a href="/Share/VisionTripApply.aspx"><img src="/image/banner/vision-trip2012.png" width="240" height="90" alt="" /></a></li>--%>
	</ul>
</div>