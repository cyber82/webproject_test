﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContinentBoard.ascx.cs" Inherits="Controls_ContinentBoard" %>
<div class="personal-details">
<asp:Repeater ID="repData" runat="server" >
<ItemTemplate>
	<a href="<%#Ahref(DataBinder.Eval(Container.DataItem,"table_idx"))%>.aspx?iTableIndex=<%#DataBinder.Eval(Container.DataItem,"table_idx")%>&iNoIndex=<%#DataBinder.Eval(Container.DataItem,"no_idx")%>&iBoardIdx=<%#DataBinder.Eval(Container.DataItem,"board_idx")%>" class="part">
		<dl>
			<dt class="title"><strong class="title"><%#DataBinder.Eval(Container.DataItem, "b_title")%></strong></dt>
			<dd class="desc"><%#WebCommon.GetNamePadding(DataBinder.Eval(Container.DataItem, "b_content"),40)%></dd>
		</dl>
	</a>
</ItemTemplate> 
</asp:Repeater>
</div>