﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_MateCommunityRight : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string pName = ParentPageName;
            if (pName.StartsWith("notice"))
                sub1.Attributes.Add("class", "on");
            else if (pName.StartsWith("month"))
                sub2.Attributes.Add("class", "on");
            else if (pName.StartsWith("faq"))
                sub3.Attributes.Add("class", "on");
            //else if (pName.StartsWith("christmas"))
            //    sub5.Attributes.Add("class", "on");
        }
    }

    protected string ParentPageName
    {
        get
        {
            string pageName = Context.Handler.GetType().Name;
            int findit = pageName.LastIndexOf("_aspx");
            pageName = pageName.Substring(0, findit);
            findit = pageName.LastIndexOf("_");
            pageName = pageName.Substring(findit + 1);
            return pageName;
        }
    }
}