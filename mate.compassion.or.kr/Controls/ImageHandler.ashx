﻿<%@ WebHandler Language="C#" Class="ImageHandler" %>

using System;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using Benecorp.Web;

public class ImageHandler : IHttpHandler
{    
    iamCompassionService.Service _service = new iamCompassionService.Service();
    Hashtable _var;

    public void ProcessRequest(HttpContext context)
    {
        /*
        string id = context.Request["sChildMasterID"].ToString();
        string width = context.Request["width"].ToString();
        string height = context.Request["height"].ToString();
         */
        _var = getWebVar();

        Bitmap bitmap;

        if (isN(_var["id"]) || isN(_var["width"]) || isN(_var["height"]))
        {
            bitmap = printFalse();
        }
        else
        {
            bitmap = GenerateImage(_var["id"].ToString());
        }


        context.Response.ContentType = "image/jpeg";

        bitmap.Save(context.Response.OutputStream, ImageFormat.Jpeg);

        bitmap.Dispose();
    }

    public bool IsReusable
    {
        get { return true; }
    }


    // 서비스에서 테이터를 못불러온경우
    protected Bitmap printFalse()
    {
        int width = 50;
        int height = 50;
        if (isN(_var["width"]) || isN(_var["height"]))
        {
        }
        else
        {
            if (isNumeric(_var["width"].ToString()))
            {
                width = int.Parse(_var["width"].ToString());
            }
            if (isNumeric(_var["height"].ToString()))
            {
                height = int.Parse(_var["height"].ToString());
            }
        }
        string noPicPath = HttpContext.Current.Server.MapPath("/") + "\\image\\NoImage.gif";
        Bitmap bitmap = new Bitmap(noPicPath);

        /*
        Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);

        Font font = new Font("Verdana", 12);
        Brush brush = new SolidBrush(Color.Gainsboro);

        Graphics g = Graphics.FromImage(bitmap);

        StringFormat format = new StringFormat();
        format.Alignment = StringAlignment.Center;
        format.LineAlignment = StringAlignment.Center;

        Rectangle rect = new Rectangle(0, 0, width, height);

        g.DrawString("noPic", font, brush, rect, format);

        font.Dispose();
        brush.Dispose();
        g.Dispose();
            */
        return bitmap;
    }


    public Bitmap GenerateImage(string id)
    {
        //string sChildMasterID = "0701420618";
        //string sChildMasterID = "2566833";

        DataSet dsImg = new DataSet();

        try
        {
            dsImg = _service.GetChildImgUseChildKey(id);
        }
        catch
        {
            return printFalse();
        }

        if (dsImg == null)
        {
            return printFalse();
        }

        if (dsImg.Tables[0].Rows.Count == 0)
        {
            return printFalse();
        }


        DataTable dtImg = dsImg.Tables[0];

        try
        {
            byte[] info = new byte[0];
            byte[] blob = (byte[])dtImg.Rows[0]["image_file"]; //어린이 이미지

            string image_head_left = dtImg.Rows[0]["image_head_left"].ToString();
            string image_head_top = dtImg.Rows[0]["image_head_top"].ToString();
            string image_head_width = dtImg.Rows[0]["image_head_width"].ToString();
            string image_head_height = dtImg.Rows[0]["image_head_height"].ToString();

            int[] pic = { 0, 0, 0, 0 };
            if (image_head_left != "")
            {
                pic[0] = int.Parse(image_head_left);
            }
            if (image_head_top != "")
            {
                pic[1] = int.Parse(image_head_top);
            }
            if (image_head_width != "")
            {
                pic[2] = int.Parse(image_head_width);
            }
            if (image_head_height != "")
            {
                pic[3] = int.Parse(image_head_height);
            }


            MemoryStream stream = new MemoryStream();
            stream.Write(blob, 0, blob.Length);

            // 원하는 이미지 파일이 데이터베이스에 있을 경우 
            Bitmap bitmap = new Bitmap(stream, true);

            int imgWidth = int.Parse(_var["width"].ToString()); ;
            int imgHeight = int.Parse(_var["height"].ToString()); ;

            Bitmap tmpBitmap = new Bitmap(imgWidth, imgHeight, PixelFormat.Format32bppArgb);

            Rectangle rect = new Rectangle(pic[0], pic[1], pic[2], pic[3]);
            Rectangle rct = new Rectangle(0, 0, imgWidth, imgHeight);


            Graphics gr = Graphics.FromImage(tmpBitmap);

            gr.DrawImage(bitmap, rct, rect, GraphicsUnit.Pixel);


            gr.Dispose();
            return tmpBitmap;
        }
        catch
        {
            return printFalse();
        }

    }

    private bool isNumeric(string arg)
    {
        return System.Text.RegularExpressions.Regex.IsMatch(arg, "^\\d+$");
    }

    private Hashtable getWebVar()
    {
        Hashtable rtn = new Hashtable();
        string tmpString;
        NameValueCollection formColl;

        formColl = HttpContext.Current.Request.Form;
        String[] arrFormKeys = formColl.AllKeys;


        for (int i = 0; i < arrFormKeys.Length; i++)
        {
            String[] arrVals = formColl.GetValues(arrFormKeys[i]);
            tmpString = "";
            for (int j = 0; j < arrVals.Length; j++)
            {
                if (j != 0)
                {
                    tmpString += ", ";
                }
                if (arrVals[j] != null)
                {
                    tmpString += arrVals[j];
                }
                else
                {
                    tmpString += "";
                }
            }
            rtn.Add(arrFormKeys[i], tmpString);
        }

        NameValueCollection queryColl;
        queryColl = HttpContext.Current.Request.QueryString;
        String[] arrQueryKeys = queryColl.AllKeys;


        for (int i = 0; i < arrQueryKeys.Length; i++)
        {
            String[] arrVals = queryColl.GetValues(arrQueryKeys[i]);
            tmpString = "";

            for (int j = 0; j < arrVals.Length; j++)
            {
                if (j != 0)
                {
                    tmpString += ", ";
                }
                if (arrVals[j] != null)
                {
                    tmpString += arrVals[j];
                }
                else
                {
                    tmpString += "";
                }
            }

            rtn.Add(arrQueryKeys[i], tmpString);
        }
        return rtn;
    }

    private Boolean isN(object arg)
    {
        if (arg == null)
        {
            return true;
        }

        string str = string.Empty;
        try
        {
            str = arg.ToString();
        }
        catch
        {
            return true;
        }

        str = str.Trim();

        if (str == "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
