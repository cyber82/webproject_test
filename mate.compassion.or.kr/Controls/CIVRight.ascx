﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CIVRight.ascx.cs" Inherits="Controls_CIVRight"%>

<%@ Register src="/Controls/RightBanner.ascx" tagname="Banner" tagprefix="bc1" %>

<!-- sidebar -->
<div id="sidebar" class="sec-civ">
	<div class="tit">양육보완 프로그램</div>
    <ul id="lnb" class="lnb">
		<li id="sub1"><strong class="dep1"><span>양육을 돕는 후원</span></strong></li>
	</ul>

    <% //추가 2012-08-31 %>
    <div class=""><a href="/Payment/CartProgram.aspx"><img src="/image/banner/lnb-cart.jpg" alt="" border="0"></a></div>

    <% //수정 2013-03-29, 2013-09-23 %>
	<ul class="banner-relation">
		<li><a class="bn-cdsp" href="/Sponsor/CDSPList.aspx">1:1 어린이 양육</a></li>
		<%--<li><a class="bn-ldp" href="/Sponsor/Ldp/ldpList.aspx">1:1 리더십 양육</a></li>--%>
		<li><a class="bn-csp" href="/Sponsor/CSPDetail.aspx">태아/영아 살리기</a></li>
	</ul>
    <div class="banner1"><a href="/Sponsor/GiftList.aspx"><img src="/image/banner/banner-goc.jpg" alt=""></a></div>

	<div class="div">
		<p class="txt txt-support-instant"><span>즉시 후원하기</span></p>
        <asp:DropDownList ID="dropSponItem" runat="server" Width ="188px">
            <asp:ListItem Value = "MDA">의료지원 후원금</asp:ListItem>
            <asp:ListItem Value = "AIDS">에이즈예방 및 퇴치후원금</asp:ListItem>
            <asp:ListItem Value = "RF">재난구호 후원금</asp:ListItem>
            <asp:ListItem Value = "OVC">어린이 SOS 후원금</asp:ListItem>
            <asp:ListItem Value = "EDU">교육지원 후원금</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="dropFrequency" runat="server" Width ="76px" OnSelectedIndexChanged="btnRadio_Click" AutoPostBack="true">
            <asp:ListItem Value = "정기후원">정기</asp:ListItem>
            <asp:ListItem Value = "일시후원">일시</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="dropMoney" runat="server" Width ="108px">
            <asp:ListItem Value = "10000">10,000</asp:ListItem>
            <asp:ListItem Value = "20000">20,000</asp:ListItem>
            <asp:ListItem Value = "30000">30,000</asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="txtPrice" runat="server" Width="95px" Height="15px" BorderColor="#e3e3e3" BorderWidth="1" Visible="false">
        </asp:TextBox><span id="spanPrice" runat="server" visible="false">원</span>
		<div class="btn-list">
            <asp:Button ID="btnSupport" CssClass="btn btn-support3" runat="server" Text="" 
                onclick="btnSupport_Click" />			
		</div>
	</div>
	<div class="div">
		<dl>
			<dt class="txt tit-lovely-children">사랑하는 어린이</dt>
			<dd>
				<ul class="list-lnb">
                    <asp:Repeater ID="repLoved" runat="server">
                    <ItemTemplate>
					<li>
						<a href="http://www.iamcompassion.or.kr/story/view.aspx?Idx=<%#DataBinder.Eval(Container.DataItem, "idx")%>&boardid=<%#DataBinder.Eval(Container.DataItem, "board_id")%>&Mode=read" target="_blank">
							<img src="http://www.iamcompassion.or.kr/ssBoard/thumbnail/<%#DataBinder.Eval(Container.DataItem, "idx")%>.jpg" width="54" height="54" class="thumb" alt="아이들을 부모님만큼 사랑해줄 거예요" />
							<strong><%#WebCommon.GetNamePadding (DataBinder.Eval(Container.DataItem, "title"),40)%></strong>
							<!--<span>저는 어린이들이 사는...</span>-->
						</a>
					</li>		
                    </ItemTemplate>
                    </asp:Repeater>	
				</ul>
			</dd>
		</dl>
		<a href="http://www.iamcompassion.or.kr/Story/Default.aspx" target ="_blank"  class="btn btn-more"><span>more</span></a>
	</div>
	<div class="banner2">
		<a href="/MemberShip/GuideCompassion.aspx"><img src="/image/banner/supporter-guide.png" width="240" height="139" alt="" /></a>
	</div>

	<ul class="banner1">
		<% //수정 2013-05-28 %>
        <bc1:Banner ID="banner1" runat="server" />
		
        <% //주석처리 2013-05-28 %>
        <%--<li><a href="/AboutUs/noticeview.aspx?iTableIndex=1001&iNoIndex=318&iBoardIdx=67376&ref=47200&re_step=0&pageIdx=0&searchOption=&searchTxt="><img src="/image/banner/charity-navigator.png" width="240" height="100" alt="" /></a></li>
		<li><a href="/Intro/MateIntro.aspx"><img src="/image/banner/mate.png" width="240" height="90" alt="" /></a></li>
        <li><a href="http://www.hanscake.co.kr/2011/story/responsibility.php" target="_blank"><img src="/image/banner/hanscake.png" width="240" height="92" alt="" /></a></li>
		<li><a href="/Share/VisionTripApply.aspx"><img src="/image/banner/vision-trip2012.png" width="240" height="90" alt="" /></a></li>--%>
	</ul>
</div>
<!-- //sidebar -->