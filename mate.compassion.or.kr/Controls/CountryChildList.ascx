﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CountryChildList.ascx.cs" Inherits="Controls_CountryChildList" %>

<!--
<asp:Repeater ID="repData" runat="server" onitemcommand="repData_ItemCommand" 
    onitemdatabound="repData_ItemDataBound" >
<ItemTemplate>
<li>
	<img src="<%#GetImgPath(DataBinder.Eval(Container.DataItem,"ChildKey"))%>" width="100" height="150" alt="까리나" />
	<dl>
		<dt>
            <asp:Image ID="imgBirth" runat="server" Visible = "false" ImageUrl ="/image/ico/ico-birth.gif" width="12" height="11" ></asp:Image>
            <asp:Image ID="imgOver6" runat="server" Visible = "false" ImageUrl ="/image/ico/ico-heart.gif" width="12" height="11" ></asp:Image>
            <strong><%#DataBinder.Eval(Container.DataItem, "ChildName")%></strong>
         </dt>
		<dd class="name"><%#DataBinder.Eval(Container.DataItem, "ChildCountry")%><br>
        <%#DataBinder.Eval(Container.DataItem, "ChildAge")%>살</dd>
		<dd class="btn">									        
            <asp:Button ID="btnRelation" CssClass ="btn btn-relationship" runat="server" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "ChildMasterID")%>' CommandName = "relation"/>
		</dd>
	</dl>
</li>
<asp:HiddenField ID="hidBirthDay" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "birthDay")%>' />
<asp:HiddenField ID="hidStartDate" runat="server"  Value ='<%#DataBinder.Eval(Container.DataItem, "startDate")%>'/>
</ItemTemplate>
</asp:Repeater>


<div class="no-child" runat="server" id="nochildCountry" visible="false">
	<p class="txt related-child"><strong>후원자의 사랑을 기다리는 어린이와 결연하세요</strong></p>
	<div class="btn-m">
		<a href="/Sponsor/CDSPList.aspx" class="btn btn-cdsp2"><span>1:1 어린이 양육하기</span></a>
	</div>
</div>
-->

<div class="cdsp-index">
    <div class="relation-act1">
	    <div class="txt-relationship1">
		    <p class="hide">오랫동안 기다려온 어린이와 결연하세요! 내 후원 어린이와의 더욱 소중한 만남</p>
		    <a href="../sponsor/cdspAssign.aspx" class="btn btn-relationship5"><span>결연하러 가기</span></a>
	    </div>
    </div>
</div>
