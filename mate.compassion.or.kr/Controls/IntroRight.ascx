﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IntroRight.ascx.cs" Inherits="Mate_Controls_IntroRight" %>
<div id="sidebar" class="sec-about">
	<div class="tit">
		<h2>메이트 소개</h2>
	</div>
	<ul id="lnb" class="lnb">
		<li id="sub1" runat="server"><a href="MateIntro.aspx" class="dep1"><span>MATE 란?</span></a></li>
		<li id="sub2" runat="server">
			<a href="TransIntro.aspx" class="dep2"><span>MATE 신청</span></a>
			<div class="sub-wrap">
				<ul id="subEl2">
					<li id="sub2_1" runat="server"><a href="TransIntro.aspx" class="dep2-1"><span>번역 메이트</span></a></li>
					<li id="sub2_2" runat="server"><a href="Officemate.aspx" class="dep2-2"><span>사무 메이트</span></a></li>
					<li id="sub2_3" runat="server"><a href="eventMate.aspx" class="dep2-3"><span>행사 메이트</span></a></li>
					<li id="sub2_4" runat="server"><a href="specialitymate.aspx" class="dep2-4"><span>전문 메이트</span></a></li>
				</ul>
			</div>
		</li>
	</ul>
	<ul class="banner1">
        <%--<li><a href="/community/ChristmasLoveLetterList.aspx"><img src="/image/mate/mate-xmasletter.jpg" width="199" height="105" alt="" /></a></li>--%>
		<li><a href="/Intro/MateIntro.aspx"><img src="/image/mate/mate-apply.jpg" width="199" height="105" alt="" /></a></li>
<!--		<li><a href="/mate/mypage/programDown.aspx"><img src="/image/mate/imate-down.jpg" width="199" height="53" alt="" /></a></li> -->
	</ul>
</div>
