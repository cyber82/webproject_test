﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TranslationRight.ascx.cs" Inherits="Mate_Controls_TranslationRight" %>
<script language="javascript" type="text/javascript">

    function ValidateBeneInsert() {
        <%
            if (!UserInfo.IsLogin)
            {
         %>
            alert('로그인 하신후 이용하실 수 있습니다.');
            return false;
         <%
            }
          %>
        if (document.getElementById('<%=eword.ClientID%>').value.trim() == "") {
            alert('용어를 입력 하세요.');
            return false;
        }
        if (document.getElementById('<%=comment.ClientID%>').value.trim() == "") {
            alert('내용을 입력 하세요.');
            return false;
        }
        return true;
    }

    function DivShow(sDivKey) {

        //성경목차
        if (sDivKey == "1")
        {

            if ( document.getElementById('BibleList').style.display != "none") {
                    
                    document.getElementById('BibleList').style.display = "none"// 혹은 "block" 
                   
                }
                else {
                    
                    document.getElementById('BibleList').style.display = ""
                   
                }
        }
        //국가약칭
        else if (sDivKey == "2")
        {

            if (document.getElementById('CountryList').style.display != "none") {
                    
                    document.getElementById('CountryList').style.display = "none"// 혹은 "block" 
                    
                }
                else {
                    
                    document.getElementById('CountryList').style.display = ""
                    
                }
        }
        //수혜국 통화
        else if (sDivKey == "3")
        {

            if (document.getElementById('CurrencyList').style.display != "none") {
                    
                    document.getElementById('CurrencyList').style.display = "none"// 혹은 "block" 
                    
                }
                else {
                    
                    document.getElementById('CurrencyList').style.display = ""
                    
                }
        }

        
    }

    function openBrWindow(sAddr,sPopName,sWidth,sHeight,sResizable) { 
        
        //window.open("http://eschyles.mireene.com/", "", "");  //속성 지정하지 않은 기본창

                if (screen.width < 1025){
                   LeftPosition=0;
                   TopPosition=0;
                 } else {
                   LeftPosition=(screen.width)?(screen.width-sWidth)/2:100;
                   TopPosition=(screen.height)?(screen.height-sHeight)/2:100;
                 }

                 //window.open(sAddr, "", "");
                 if(sResizable == "0")
                 {
                    window.open(sAddr, sPopName, "width="+sWidth+",height="+sHeight+",top="+TopPosition+",left="+LeftPosition+", scrollbars=yes, toolbar=no menubar=no, location=no");
                 }
                 else
                 {
                    window.open(sAddr, sPopName, "width="+sWidth+",height="+sHeight+",top="+TopPosition+",left="+LeftPosition+", scrollbars=yes, toolbar=no menubar=no, location=no, resizable=yes");
                 }
    }
</script>
<input type="text" id="test1"/>
<div id="sidebar" class="sec-imate">
    <div style="border-left:1px solid #dddddd">
	<div class="tit">
		<h2>메이트 번역</h2>
	</div>
	<ul id="lnb" class="lnb">
		<li id="sub1" runat="server" ><a href="TranslationHome.aspx" class="dep1"><span>홈</span></a></li>
        <%if (bTranslate)
          { %>
		<li id="sub2" runat="server" ><a href="TranslationMain.aspx" class="dep2"><span>번역</span></a></li>
		<li id="sub3" runat="server" ><a href="TranslationComplete.aspx" class="dep3"><span>번역 완료</span></a></li>
        <%} %>
        <%if (bScreening)
          { %>
		<li id="sub4" runat="server" ><a href="ScreeningMain.aspx" class="dep4"><span>스크리닝</span></a></li>
        <li id="sub5" runat="server" ><a href="ScreeningComplete.aspx" class="dep5"><span>스크리닝 완료</span></a></li>
        <%} %>
        <%if (bThirdPL)
          { %>
        <li id="sub6" runat="server" ><a href="ThirdPLMain.aspx" class="dep6"><span>3PL</span></a></li>
        <li id="sub7" runat="server" ><a href="ThirdPLComplete.aspx" class="dep7"><span>3PL 완료</span></a></li>
        <%} %>
        <%if (bTranslate || bScreening)
          { %>
        <li id="sub8" runat="server" ><a href="TranslationLetterSearch.aspx" class="dep8"><span>편지 조회</span></a></li>
        <%} %>
        
	</ul>
    <div>
    <table>
    <tr>
    <td>
    <img scr="" src="../../image/imate/community-guide.gif" />
    </td>
    </tr>
    </table>	
	</div>
    



    <div style="border-top:1px solid #d5d5d5;position:relative;z-index:10;overflow:auto; overflow-y:hidden;">
    <ul id="quide" class="quide">
    
		<li id="sub9" runat="server" style="cursor:pointer;" onclick="openBrWindow('/translation/BibleIndexList.aspx','BibleIndexList','800','800','1'); return false;">
        <%--<input type='image' src="../../image/imate/icon/icon_ext.png" onclick="DivShow('1')"/>--%>
        <a class="quide1"><span>1</span></a></li><!---->
            <div id="BibleList" style="width:100%; overflow:auto; overflow-y:hidden; display:none">
                <%=sgBibleList %>
            </div>
        

            
		<li id="sub10" runat="server" style="cursor:pointer" onclick="openBrWindow('/translation/CountryAbbreviationList.aspx','CountryAbbreviationList','390','800','1'); return false;"><a class="quide2"><span>1</span></a></li>
            <div id="CountryList" style="width:100%; overflow:auto; overflow-y:hidden; display:none">
                <%=sgCountryList %>
            </div>
		<li id="sub11" runat="server" style="cursor:pointer" onclick="openBrWindow('/translation/CountryMoneyList.aspx','CountryMoneyList','780','800','1'); return false;"><a class="quide3"><span>1</span></a></li>
		    <div id="CurrencyList" style="width:100%; overflow:auto; overflow-y:hidden; display:none">
                <%=sgCurrencyList %>
            </div>
        <li id="sub12" runat="server" style="cursor:pointer" onclick="openBrWindow('/translation/BenefitDefault.aspx','BenefitDefault','780','800','0');"><a class="quide4"><span>1</span></a></li>
        
        
        <li id="sub17" runat="server" style="cursor:pointer" onclick="openBrWindow('/translation/MasterChallengeList.aspx','MasterChallengeList','780','800','0');"><a class="quide5"><span>1</span></a></li>
        
        <li id="sub13" runat="server" style="cursor:pointer" onclick="openBrWindow('/translation/TranslationQnAList.aspx','BenefitDefault','780','800','0');"><a class="quide6"><span>1</span></a></li>
        <%--<li id="sub14" runat="server" style="cursor:pointer" onclick="openBrWindow('http://newbible.godpia.com/Main.asp#byMenuItem=red_010_01m&personid=&BIBLE_KD=gae||niv&BIBLE_VOL=gen&chapter=50&CHAPTER_NO=1&FLAG=T&target=1','HolyBible','1000','800','1');"><a class="quide6"><span>1</span></a></li>--%>
        <li id="sub14" runat="server" style="cursor:pointer" onclick="openBrWindow('http://www.holybible.or.kr','HolyBible','1000','800','1');"><a class="quide7"><span>1</span></a></li>
        <li id="sub15" runat="server" style="cursor:pointer" onclick="openBrWindow('/Files/iMate_manual.pdf','Manual','780','800','1');"><a class="quide8"><span>1</span></a></li>
        <li id="sub16" runat="server" style="cursor:pointer" onclick="openBrWindow('/Files/Trans_careful.pdf','Careful','780','800','1');"><a class="quide9"><span>1</span></a></li>      
	</ul>

    <div class="mypage">
        <ul class="lnb">
            <li runat="server"><a href="/Mypage/ServiceBreakDown.aspx" class="mypage1"><span>봉사 내역 확인</span></a></li>
        </ul>
    </div> 
    
    <div style="display:none">
    <table>
        <tr>
            <td>
                <asp:ImageButton ID="imgBtnBibleList" runat="server" ImageUrl="~/image/imate/btn/btn_quide1.jpg" Height="40px" />
                
            </td>
        </tr>
        <tr style="border-top:1px solid #d5d5d5;">
            <td>
                <asp:ImageButton ID="imgBtnCountryList" runat="server" ImageUrl="~/image/imate/btn/btn_quide2.jpg" Height="40px"  />
                
            </td>
        </tr>
        <tr style="border-top:1px solid #d5d5d5;">
            <td>
                <asp:ImageButton ID="imgBtnCurrencyList" runat="server" ImageUrl="~/image/imate/btn/btn_quide3.jpg" Height="40px" />
                
            </td>
        </tr>
        <tr style="border-top:1px solid #d5d5d5;">
            <td>
                <asp:ImageButton ID="imgBtnBenefit" runat="server" ImageUrl="~/image/imate/btn/btn_quide4.jpg" Height="40px" />
            </td>
        </tr>
        <tr style="border-top:1px solid #d5d5d5;">
            <td>
                <asp:ImageButton ID="imgBtnQnA" runat="server" ImageUrl="~/image/imate/btn/btn_quide5.jpg" Height="40px" />
            </td>
        </tr>
        <tr style="border-top:1px solid #d5d5d5;">
            <td>
                <asp:ImageButton ID="imgHolyBible" runat="server" ImageUrl="~/image/imate/btn/btn_quide6.jpg" Height="40px" />
            </td>
        </tr>
        <tr style="border-top:1px solid #d5d5d5;">
            <td>
                <asp:ImageButton ID="imgManual" runat="server" ImageUrl="~/image/imate/btn/btn_quide7.jpg" Height="40px" />
            </td>
        </tr>
        <tr style="border-top:1px solid #d5d5d5;">
            <td>
                <asp:ImageButton ID="imgCareful" runat="server" ImageUrl="~/image/imate/btn/btn_quide8.jpg" Height="40px" />
            </td>
        </tr>
    </table>
     </div>   
        
    </div>
    <fieldset class="upload-wording">
		<legend class="hide">용어 올리기 제출 양식</legend>
		<div class="txt tit-upload-wording">용어 올리기</div>
		<p class="txt txt-upload-wording">메이트 님께서 편지를 번역하시면서 발견하신 유용한 정보를 다른 메이트님들과 공유해 주세요.</p>
		<dl class="field">
			<dt><strong class="txt txt-wording"><span>용어</span></strong></dt>
			<dd><input type="text" class="text" style="width:119px;" id="eword" runat="server" /></dd>
			<dt><strong class="txt txt-nation"><span>국가</span></strong></dt>
			<dd>				
                <asp:DropDownList ID="dropNation" runat="server" wh="128">
                </asp:DropDownList>		
			</dd>
			<dt><strong class="txt txt-initial"><span>초성</span></strong></dt>
			<dd>
				<select style="width:128px;" id="dropInitial" runat="server">
					<option value ="A">A</option>
                    <option value ="B">B</option>
                    <option value ="C">C</option>
                    <option value ="D">D</option>
                    <option value ="E">E</option>
                    <option value ="F">F</option>
                    <option value ="G">G</option>
                    <option value ="H">H</option>
                    <option value ="I">I</option>
                    <option value ="J">J</option>
                    <option value ="K">K</option>
                    <option value ="L">L</option>
                    <option value ="M">M</option>
                    <option value ="N">N</option>
                    <option value ="O">O</option>
                    <option value ="P">P</option>
                    <option value ="Q">Q</option>
                    <option value ="R">R</option>
                    <option value ="S">S</option>
                    <option value ="T">T</option>
                    <option value ="U">U</option>
                    <option value ="V">V</option>
                    <option value ="W">W</option>
                    <option value ="X">X</option>
                    <option value ="Y">Y</option>
                    <option value ="Z">Z</option>
				</select>
			</dd>
			<dt><strong class="txt txt-context"><span>내용</span></strong></dt>
			<dd>
				<textarea rows="5" cols="13" id="comment" runat="server"></textarea>
			</dd>
		</dl>
		<div class="btn-m">
            <asp:Button ID="btnSave" runat="server" Text="" CssClass="btn btn-submit1" 
                OnClientClick ="return ValidateBeneInsert();" onclick="btnSave_Click" />
		</div>
	</fieldset>
    </div>
</div>