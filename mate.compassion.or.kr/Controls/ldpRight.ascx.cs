﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data; 

public partial class Controls_ldpRight : System.Web.UI.UserControl
{
    IamWeb.IamCompassionWeb _iamWeb = new IamWeb.IamCompassionWeb();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BeLovedChildBind();
            FaqBind();
        }
    }
    private void BeLovedChildBind()
    {
        WWWService.Service _wwwService = new WWWService.Service();

        string code = _wwwService.ConfigCode("main_ssb");

        DataSet data = _wwwService.MainSsbList(code);
        //DataSet data = _iamWeb.GetBeLovedChildList("sponsor");
        repLoved.DataSource = data.Tables[0];
        repLoved.DataBind();
    }

    private void FaqBind()
    {
        WWWService.Service _wwwService = new WWWService.Service();
        DataSet data = _wwwService.CounselBoardList(1015, "전체", "", 0, 4);
        repFaq.DataSource = data.Tables[0];
        repFaq.DataBind();
    }
}