﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="Controls_Header" %>
<meta charset="UTF-8">
<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title> 
<link rel="stylesheet" href="/common/css/common.css" />
<link rel="stylesheet" href="/common/css/common_low.css" />
<link rel="stylesheet" href="/common/css/popup.css" />
<script type="text/javascript" src="/common/js/jquery-1.6.2.min.js"></script>
<!--<script type="text/javascript" src="/common/js/prototype.js"></script>-->
<script type="text/javascript" src="/common/js/common.js"></script>
<script type="text/javascript" src="/common/js/util.js"></script>
<script type="text/javascript" src="/common/js/Default.js"></script>
<script language="javascript" type="text/javascript">
    function ShareFaceBook() {
        window.open("http://www.facebook.com/sharer.php?u=" + encodeURIComponent('<%=this.Page.Request.Url.AbsoluteUri.Replace("'","")%>'));
    }

    function ShareFaceBookSummary(summary) {
        window.open("http://www.facebook.com/sharer.php?u=" + encodeURIComponent('<%=this.Page.Request.Url.AbsoluteUri.Replace("'","")%>') + "&t=test");
    }

    function ShareTwitter() {
        window.open("http://twitter.com/home?status=" + encodeURIComponent('<%=this.Page.Request.Url.AbsoluteUri.Replace("'","")%>'));
    }

    function CountryMove() {        
        var con = document.all.selCountry.value;                   
        if (con != "") {            
            window.open("http://" + con, "_blank");            
        }
    }

    function setClip() {
        window.clipboardData.setData('Text', '<%=this.Page.Request.Url.AbsoluteUri.Replace("'","")%>');
        alert('클립보드에 복사 되었습니다.\ncrtl+v(붙여넣기) 하세요.');
    }

</script>