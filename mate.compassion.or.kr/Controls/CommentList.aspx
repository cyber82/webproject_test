﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CommentList.aspx.cs" Inherits="Controls_CommentList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/common/css/common.css" />
    <style type="text/css">
    body 
    {
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        overflow-y: auto;
        overflow-x: hidden;
    }
        
    html 
    {
        scrollbar-face-color: #FFF;
        scrollbar-highlight-color: #000;
        scrollbar-3dlight-color: #999;
        scrollbar-shadow-color: #000;
        scrollbar-darkshadow-color: #999;
        scrollbar-track-color: #FFF;
        scrollbar-arrow-color: #999;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <!-- 댓글보기 -->
        <div class="list-comment">    
	        <ul>
                <asp:Repeater ID="repCommentData" runat="server" 
                    onitemdatabound="repCommentData_ItemDataBound" 
                    onitemcommand="repCommentData_ItemCommand" >
                    <ItemTemplate> 
		            <li>
			            <strong class="writer"><%#DataBinder.Eval(Container.DataItem, "writer")%></strong> 
			            <div class="cont">
				            <p><%#DataBinder.Eval(Container.DataItem, "comment").ToString ().Replace ("\r\n","<BR/>") %></p>
				            <span class="date"><%#DataBinder.Eval(Container.DataItem, "writerDay")%></span>
			            </div>
			            <!-- 본인 작성 글일 때 -->
                        <asp:Button ID="btnDelete" CommandName="delete" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "seqno")%>' runat="server" Text="" CssClass ="btn btn-delete1" />			    
                        <asp:HiddenField ID="hidUserID" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "user_id")%>'/>
                        <asp:HiddenField ID="hidcref" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "cref")%>'/>
                        <asp:HiddenField ID="hidtable_idx" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "table_idx")%>'/>
                        <asp:HiddenField ID="hidre_step" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "re_step")%>'/>
                        <asp:HiddenField ID="hidwriteday" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "writeday")%>'/>
		            </li>            
                    </ItemTemplate> 
                </asp:Repeater>
	        </ul>	
        </div>   
    </div>
    </form>
</body>
</html>
