﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SupportCountryRight.ascx.cs" Inherits="Controls_SupportCountryRight" %>

<%@ Register src="/Controls/RightBanner.ascx" tagname="Banner" tagprefix="bc1" %>

<div id="sidebar" class="sec-where">
	<div class="tit">양육현장</div>
	<ul id="lnb" class="lnb">
		<li id="sub1">
			<a href="asia.aspx" class="dep1"><span>아시아</span></a>
			<ul id="subEl1" runat="server" >
				<li id="sub1_1"><a href="bangladesh.aspx" class="dep1-1"><span>방글라데시</span></a></li>
				<li id="sub1_2"><a href="india.aspx" class="dep1-2"><span>인도</span></a></li>
				<li id="sub1_3"><a href="indonesia.aspx" class="dep1-3"><span>인도네시아</span></a></li>
				<li id="sub1_4"><a href="philippines.aspx" class="dep1-4"><span>필리핀</span></a></li>
				<li id="sub1_5"><a href="thailand.aspx" class="dep1-5"><span>태국</span></a></li>
				<li id="sub1_6"><a href="srilanka.aspx" class="dep1-6"><span>스리랑카</span></a></li>
			</ul>
		</li>
		<li id="sub2">
			<a href="africa.aspx" class="dep2"><span>아프리카</span></a>
			<ul id="subEl2" runat="server" >
				<li id="sub2_1"><a href="burkinafaso.aspx" class="dep2-1"><span>부르키나파소</span></a></li>
				<li id="sub2_2"><a href="ethiopia.aspx" class="dep2-2"><span>에티오피아</span></a></li>
				<li id="sub2_3"><a href="ghana.aspx" class="dep2-3"><span>가나</span></a></li>
				<li id="sub2_4"><a href="kenya.aspx" class="dep2-4"><span>케냐</span></a></li>
				<li id="sub2_5"><a href="rwanda.aspx" class="dep2-5"><span>르완다</span></a></li>
				<li id="sub2_6"><a href="tanzania.aspx" class="dep2-6"><span>탄자니아</span></a></li>
				<li id="sub2_7"><a href="uganda.aspx" class="dep2-7"><span>우간다</span></a></li>
				<li id="sub2_8"><a href="togo.aspx" class="dep2-8"><span>토고</span></a></li>
			</ul>
		</li>
		<li id="sub3">
			<a href="southAmerica.aspx" class="dep3"><span>남미</span></a>
			<ul id="subEl3" runat="server" >
				<li id="sub3_1"><a href="bolivia.aspx" class="dep3-1"><span>볼리비아</span></a></li>
				<li id="sub3_2"><a href="brazil.aspx" class="dep3-2"><span>브라질</span></a></li>
				<li id="sub3_3"><a href="colombia.aspx" class="dep3-3"><span>콜롬비아</span></a></li>
				<li id="sub3_4"><a href="ecuador.aspx" class="dep3-4"><span>에콰도르</span></a></li>
				<li id="sub3_5"><a href="peru.aspx" class="dep3-5"><span>페루</span></a></li>
			</ul>
		</li>
		<li id="sub4">
			<a href="caribbean.aspx" class="dep4"><span>중남미와 카리브연안</span></a>
			<ul id="subEl4" runat="server" >
				<li id="sub4_1"><a href="dominicanrepublic.aspx" class="dep4-1"><span>도미니카 공화국</span></a></li>
				<li id="sub4_2"><a href="elsalvador.aspx" class="dep4-2"><span>엘살바도르</span></a></li>
				<li id="sub4_3"><a href="guatemala.aspx" class="dep4-3"><span>과테말라</span></a></li>
				<li id="sub4_4"><a href="haiti.aspx" class="dep4-4"><span>아이티</span></a></li>
				<li id="sub4_5"><a href="honduras.aspx" class="dep4-5"><span>온두라스</span></a></li>
				<li id="sub4_6"><a href="mexico.aspx" class="dep4-6"><span>멕시코</span></a></li>
				<li id="sub4_7"><a href="nicaragua.aspx" class="dep4-7"><span>니카라과</span></a></li>
			</ul>
		</li>
		<li id="sub5"><a href="DisasterList.aspx" class="dep5"><span>재해소식</span></a></li>
		<li id="sub6"><a href="NewPrayersList.aspx" class="dep6"><span>기도나눔</span></a></li>
	</ul>

	<ul class="banner1">
		<% //수정 2013-05-28 %>
        <bc1:Banner ID="banner1" runat="server" />
		
        <% //주석처리 2013-05-28 %>
        <%--<li><a href="/AboutUs/noticeview.aspx?iTableIndex=1001&iNoIndex=318&iBoardIdx=67376&ref=47200&re_step=0&pageIdx=0&searchOption=&searchTxt="><img src="/image/banner/charity-navigator.png" width="240" height="100" alt="" /></a></li>
		<li><a href="/Intro/MateIntro.aspx"><img src="/image/banner/mate.png" width="240" height="90" alt="" /></a></li>
        <li><a href="http://www.hanscake.co.kr/2011/story/responsibility.php" target="_blank"><img src="/image/banner/hanscake.png" width="240" height="92" alt="" /></a></li>
		<li><a href="/Share/VisionTripApply.aspx"><img src="/image/banner/vision-trip2012.png" width="240" height="90" alt="" /></a></li>--%>
	</ul>
</div>