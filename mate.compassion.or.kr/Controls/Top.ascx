﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Top.ascx.cs" Inherits="Controls_Top" %>
<div id="header_2" style="margin-bottom:1px;">
    <div class="quickNavArea"><div class="quickNav">
	    <script>
            function getHost() {
                var host = '';
                var loc = String(location.href);
                if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
                else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
                else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
                return host;
            }

            function goLink(linkType) {
                var host = getHost();
                var param = '';
                if (linkType == 1) param = '/';
                else if (linkType == 2) param = '/Mate/Default.aspx';
                if (host != '') location.href = 'http://' + host + param;
            }
        </script>
        <ul class="site">
		    <li class="s1 on"><a href="javascript:goLink(1)">한국컴패션</a></li>
		    <li class="s2"><a href="http://www.iamcompassion.or.kr/">I AM COMPASSION</a></li>
		    <li class="s3"><a href="javascript:goLink(2)">MATE</a></li>
        </ul>
	    <div class="utilArea">
		    <ul class="util" id="login" runat="server">
			    <li style="padding-top:6px;"><a href="/Membership/Login.aspx"><img src="/image/common/login.png" alt="로그인" /></a></li>
			    <li style="padding-top:6px;"><a href="/Membership/RegisterAdult.aspx"><img src="/image/common/join.png" alt="회원가입" /></a></li>
			    <li style="padding-top:6px;"><a href="/event/CurrentEventList.aspx"><img src="/image/common/event.png" alt="캠페인/이벤트" /></a></li>
			    <li style="padding-top:6px;"><a href="/recruit/Process.aspx"><img src="/image/common/recruit.png" alt="인재채용" /></a></li>
			    <li style="padding-top:6px;"><a href="http://www.iamcompassion.or.kr/SiteMap.aspx?site=compassion"><img src="/image/common/sitemap.png" alt="사이트맵" /></a></li>
		    </ul>
		    <ul class="util" id="logout" runat="server" style="display:none;">
                <li style="margin-right:2px;padding-top:6px;color:White;line-height:1.7;padding-right:14px;background:none;"><asp:Literal ID="labName2" runat="server"></asp:Literal></li>
			    <li style="padding-top:6px;"><a href="/Mypage/ApplyInfo.aspx"><img src="/image/common/mypage.png" alt="마이페이지" /></a></li> 
                <li style="padding-top:6px;"><a href="/LogOff.aspx"><img src="/image/common/logout.png" alt="로그아웃" /></a></li>
			    <li style="padding-top:6px;"><a href="/event/CurrentEventList.aspx"><img src="/image/common/event.png" alt="캠페인/이벤트" /></a></li>
			    <li style="padding-top:6px;"><a href="/recruit/Process.aspx"><img src="/image/common/recruit.png" alt="인재채용" /></a></li>
			    <li style="padding-top:6px;"><a href="http://www.iamcompassion.or.kr/SiteMap.aspx?site=compassion"><img src="/image/common/sitemap.png" alt="사이트맵" /></a></li>
		    </ul>
		    <ul class="sns">
			    <li><a href="https://www.facebook.com/compassion.Korea" target="_blank"><img src="/image/common/sns_facebook.png" alt="facebook" /></a></li>
			    <li><a href="https://twitter.com/compassionkorea" target="_blank"><img src="/image/common/sns_twitter.png" alt="twitter" /></a></li>
			    <li><a href="http://www.youtube.com/user/CompassionKR" target="_blank"><img src="/image/common/sns_youyube.png" alt="youtube" /></a></li>
                <li><a href="https://www.instagram.com/compassionkorea" target="_blank"><img src="/image/common/sns_insta.jpg" alt="instagram" /></a></li>
		    </ul>
	    </div>
    </div></div>
</div>
<!-- header -->
<div id="header">
	<div class="wrap">
		<a class="logo1" href="/Default.aspx">한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</a>
		<ul class="related1">
            <% //수정 2013-06-04  컴패션블로그 주석처리 %>
			<%--<li><a href="http://blog.iamcompassion.or.kr/blog/BlogPersonalMain.aspx?blogid=COMmaster3" class="l-blog" target="_blank">컴패션 블로그</a></li>--%>
			<li><a href="/MemberShip/GuideCompassion.aspx" class="l-guide">후원자 가이드</a></li>
			<li><a href="/MemberShip/Faq.aspx" class="l-faq">자주하는 질문</a></li>			
            <li><a href="/Sponsor/GiftList.aspx" class="l-gifts">GIFTS COMPASSION</a></li>
            <%--<li><a href="/Mate/Default.aspx" class="l-mate">Mate</a></li>--%>
            <!--<li><a href="/eventAmount/raising_main.aspx" class="l-finance" target = "_blank">컴패션 재정투명성</a></li>-->
            <!--<li><a href="/eventAmount/transparency.html" class="l-finance" target = "_blank">컴패션 재정투명성</a></li>-->
            <li><a href="/eventAmount/raising_main.aspx" class="l-raising" target = "_blank">후원금은 양육비</a></li>
		</ul>
		<%--<ul class="related2">            
            <li class="message" id="gMember" runat="server" ><strong><asp:Label ID="labName1" runat="server" Text=""></asp:Label></strong><span class="txt txt-member"><span>회원님 안녕하세요</span></span></li>           
			<li class="message" id="sMember" runat="server"><strong><asp:Literal ID="labName2" runat="server"></asp:Literal></strong><span class="txt txt-supporter"><span>후원자님 안녕하세요</span></span></li>
            <li><asp:HyperLink ID="aLogin" runat="server"  CssClass="l-login">로그인</asp:HyperLink></li>
			<li><asp:HyperLink ID="aRegister" runat="server" CssClass ="l-join">회원가입</asp:HyperLink></li>
			
            <% //수정 2013-05-27 %>
            <li><a href="/event/CurrentEventList.aspx" class="l-event">캠페인/이벤트</a></li>
			<li><a href="/recruit/Process.aspx" class="l-recruit">인재채용</a></li>
			<li><a href="/Membership/Sitemap.aspx" class="l-sitemap">사이트맵</a></li>
		</ul>--%>
		<div id="gnb" class="gnb">
			<ul class="type1">
				<li id="gnb1"><a href="/Sponsor/CDSPList.aspx" class="dep1">1:1 어린이 양육</a></li>
				<%--<li id="gnb2"><a href="/Sponsor/Ldp/ldpList.aspx" class="dep2">1:1 리더쉽 양육</a></li>--%>  <% //수정 2013-03-29 %>
				<li id="gnb3"><a href="/Sponsor/CSPDetail.aspx" class="dep3">태아영아 살리기</a></li>
				<li id="gnb4"><a href="/Sponsor/CIVList.aspx" class="dep4">양육을 돕는 후원</a></li>
			</ul> 
			<ul class="type2">
				<li id="gnb5" onmouseover="MenuBlock()">
					<a href="/AboutUs/Introduce.aspx" class="dep5">한국컴패션</a>
					<div style="display:none">
						<ul>
							<li><a href="/AboutUs/Introduce.aspx" class="dep5-1">컴패션 소개</a></li>
							<li><a href="/AboutUs/PerformanceReporting.aspx" class="dep5-2">연례보고</a></li>
							<li><a href="/AboutUs/SustainabilityAbout.aspx" class="dep5-3">한국컴패션의 지속가능성</a></li>
							<li><a href="/AboutUs/HonoraryAmbassador.aspx" class="dep5-4">홍보대사</a></li>   
                            <li><a href="/recruit/Process.aspx" class="dep5-5">인재채용</a></li>              
							<li><a href="/AboutUs/Noticelist.aspx" class="dep5-6">공지사항</a></li>
							<li><a href="/AboutUs/PressList.aspx" class="dep5-7">보도자료</a></li>
							<li><a href="/AboutUs/Magazine.aspx?num=4" class="dep5-8">소식지</a></li>
							<li><a href="/AboutUs/Map.aspx" class="dep5-9">찾아오시는 길</a></li>
						</ul>
					</div>
				</li>
				<li id="gnb6" onmouseover="MenuBlock()">
					<a href="/Share/SponsorEnterprise.aspx" class="dep6">적극적 참여</a>
					<div style="display:none">
						<ul>
							<li><a href="/Share/SponsorEnterprise.aspx" class="dep6-1">함께하는 기업 단체</a></li>
							<li><a href="/Share/CompassionSunday.aspx" class="dep6-2">교회협력</a></li>
							<li><a href="/Share/VocAct.aspx" class="dep6-3">일반인 홍보대사 VOC</a></li>
                            <li><a href="/Share/YVocAct.aspx" class="dep6-4">청소년 홍보대사 VOC</a></li>
							<li><a href="/Share/Foc.aspx" class="dep6-5">FOC</a></li>
							<li><a href="/Share/Mate.aspx" class="dep6-6">메이트</a></li>
							<li><a href="/Share/VisionTripAbout.aspx" class="dep6-7">비전트립</a></li> <% //수정 2013-01-07, 수정 2013-07-19 %>	
                            <li><a href="/Share/PersonalVisitAbout.aspx" class="dep6-11">개인방문</a></li>		
                            <li><a href="/Share/CompassionBand.aspx" class="dep6-8">컴패션 밴드</a></li>
							<li><a href="/Share/StarProject.aspx" class="dep6-9">나눔별프로젝트</a></li>
							<%--<li><a href="/Share/Legacy.aspx" class="dep6-12">믿음의 유산</a></li>--%>
                            <li><a href="/Birthday" class="dep6-13">특별한 나눔</a></li>
							<li><a href="/Share/CpsPicture.aspx" class="dep6-10">컴패션 자료실</a></li>
						</ul>
                    </div>
				</li>
				<li id="gnb7" onmouseover="MenuBlock()">
					<a href="/SupportCountry/asia.aspx" class="dep7">양육현장</a>
					<div style="display:none">
						<ul>
							<li><a href="/SupportCountry/asia.aspx" class="dep7-1">아시아</a></li>
							<li><a href="/SupportCountry/africa.aspx" class="dep7-2">아프리카</a></li>
							<li><a href="/SupportCountry/southAmerica.aspx" class="dep7-3">남미</a></li>
							<li><a href="/SupportCountry/caribbean.aspx" class="dep7-4">중미와 카리브 연안</a></li>
							<li><a href="/SupportCountry/DisasterList.aspx" class="dep7-5">재해소식</a></li>
							<li><a href="/SupportCountry/NewPrayersList.aspx" class="dep7-6">기도나눔</a></li>
						</ul>
					</div>
				</li>
			</ul>
 
			<ul class="type3">
				<li onmouseover="MenuBlock()">
					<a href="http://www.iamcompassion.or.kr" class="dep8" target="_blank">I AM COMPASSION</a>
					<%--<div style="display:none">
						<ul>
                            <% //수정 2012-11-28 %>
							<li><a href="http://story.iamcompassion.or.kr/sympathyMain.aspx" class="dep8-1" target="_blank">공감</a></li>
							<li><a href="http://club.iamcompassion.or.kr/partMain.aspx" class="dep8-2" target="_blank">참여</a></li>
							<li><a href="http://blog.iamcompassion.or.kr/commMain.aspx" class="dep8-3" target="_blank">소통</a></li>
                            <li><a href="http://store.iamcompassion.or.kr/happyGift.aspx" class="dep8-4" target="_blank">행복한 선물</a></li>
							<li><a href="http://store.iamcompassion.or.kr/friendsShop.aspx" class="dep8-5" target="_blank">프렌즈샵</a></li>
							<li><a href="http://store.iamcompassion.or.kr/shareCompassion.aspx" class="dep8-6" target="_blank">후원자와 나눠요</a></li>
							<li><a href="http://story.iamcompassion.or.kr/loveLetter.aspx" class="dep8-7" target="_blank">러브레터</a></li>
							<li><a href="http://www.iamcompassion.or.kr/openPaper.aspx" class="dep8-8" target="_blank">블로그 집합소</a></li>
						</ul>
					</div>--%>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- // header -->

<script type="text/javascript">
<!--
    function MenuBlock() {
        $(".type2 > li div, .type3 > li div").css('display', 'block');
        $(".type2 > li div, .type3 > li div").css('visibility', 'hidden');
    }
-->
</script>
