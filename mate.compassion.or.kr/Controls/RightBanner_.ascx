﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RightBanner.ascx.cs" Inherits="Controls_RightBanner" %>

<!-- RightBanner -->
<li>
    <script>
        function getHost() {
            var host = '';
            var loc = String(location.href);
            if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
            else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
            else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
            return host;
        }

        function goLink() {
            var host = getHost();
            if (host != '') location.href = 'http://' + host + '/aboutus/noticeview.aspx?iTableIndex=1001&iNoIndex=348&iBoardIdx=85481&ref=64519&re_step=0';
        }
    </script>
    <a href="javascript:goLink()">
    <img src="/image/banner/charity-navigator.png" width="240" height="100" alt="" /></a>
</li>

<li>
    <a href="/Intro/MateIntro.aspx"><img src="/image/banner/mate.png" width="240" height="90" alt="" /></a>
</li>

<li>
    <a href="http://www.hanscake.co.kr/2011/story/responsibility.php" target="_blank"><img src="/image/banner/hanscake.png" width="240" height="92" alt="" /></a>
</li>

<li>
    <a href="/Share/VisionTripApply.aspx"><img src="/image/banner/vision-trip2012.png" width="240" height="90" alt="" /></a>
</li>
