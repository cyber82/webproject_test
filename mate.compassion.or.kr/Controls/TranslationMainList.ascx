﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TranslationMainList.ascx.cs" Inherits="Mate_Controls_TranslationMainList" %>
<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>



<script language="javascript">
    function ShowDetail(sCorrID, sPackageID, sPageCount, sTranslationStatus) {
        if (sTranslationStatus == '') {
            alert("초기화 된 편지입니다\n관리자에게 문의 하세요");
        } else {
            location.href = '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount;
        }
    }

    function GetLetterCheck(nListCnt, nEndCnt) {

        if (nListCnt > 0) {
            alert("이미 편지를 가져 왔습니다");
            return false;
        }
        else {
            if (nEndCnt > 0) {
                alert("이번주 번역이 완료 되었습니다");
                return false;
            }
            return true;
        }
       
    }

</script>
<table style="width:100%">
<tr>
<td>
<div style="text-align:left">
<asp:Label ID="lblTotalCount" runat="server" Text="Label"></asp:Label>
</div>
</td>
<td>
<div style="text-align:right" >
                <%--편지개수:&nbsp--%>
                <asp:TextBox ID="txtGetLetterCount" runat="server" CssClass="text" Width="20px" MaxLength="3" Visible="false"></asp:TextBox>&nbsp
    <asp:Label ID="lblNullLetterCount" runat="server" Text="Label"></asp:Label>&nbsp
    <asp:ImageButton ID="imgBtnGetLetter" runat="server" 
                    onclick="imgBtnGetLetter_Click" ToolTip="편지를 한 통씩 가져옵니다." ImageUrl="~/image/imate/btn/btn_email.jpg" />&nbsp
                <asp:ImageButton ID="imgBtnSearch" runat="server" Visible="false"/>
</div>
</td>
</tr>
</table>
</br>
<table class="mate-list">
	<thead>
		<tr>
			<th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    스캔일자
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    편지ID
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    패키지ID
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    편지타입
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    어린이키
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    어린이명
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    후원자키
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    후원자명
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    쪽수
            </th>
            <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    상태
            </th>
		</tr>
	</thead>
	<tbody>
        <asp:Repeater ID="repData" runat="server">
            <ItemTemplate>
		    <tr id="tr<%#DataBinder.Eval(Container.DataItem, "Idx")%>" title='<%#DataBinder.Eval(Container.DataItem, "CommentMsg")%>' >
            <script language="javascript">


                function Toggle() {
                    //번역이 시작 이고 (스크리너가 반송 또는 번역자가 지정 됬을 때 또는 오신고 및 기타 관리자가 반송 했을 때)
                    if ('<%#DataBinder.Eval(Container.DataItem, "CommentMsg")%>' != "") {
                        var qa = document.getElementById("tr" + '<%#DataBinder.Eval(Container.DataItem, "Idx")%>');

                        if (qa.style.color != 'gray') {
                            qa.style.color = "gray";
                        }
                        else {
                            qa.style.color = ''
                        }
                        //alert(qa.style.color);
                    }
                }
                //setInterval(Toggle, 500);

                //번역이 시작 이고 (스크리너가 반송 또는 번역자가 지정 됬을 때 또는 오신고 및 기타 관리자가 반송 했을 때)
                if ('<%#DataBinder.Eval(Container.DataItem, "CommentMsg")%>' != "") {
                    var qa = document.getElementById("tr" + '<%#DataBinder.Eval(Container.DataItem, "Idx")%>');
                    qa.style.backgroundColor = '#DEEAFC'
                    //alert(qa.style.color);
                }
                
                  
            </script>
			    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ScanningDate", "{0:yyyy-MM-dd}")%></center></td>
			    <td style='border:1px solid #CCCCCC; height:27px; cursor:pointer;' bgcolor="#C6DFFF" onclick="ShowDetail('<%#DataBinder.Eval(Container.DataItem, "CorrID")%>','<%#DataBinder.Eval(Container.DataItem, "PackageID")%>','<%#DataBinder.Eval(Container.DataItem, "CorrPage")%>','<%#DataBinder.Eval(Container.DataItem, "TranslationStatus")%>'); return false;"><center><%#DataBinder.Eval(Container.DataItem, "CorrID")%></center></td>
			    <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "PackageID")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "CorrType")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ChildKey")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ChildName")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "ConID")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "SponsorName")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#DataBinder.Eval(Container.DataItem, "CorrPage")%></center></td>
                <td style='border:1px solid #CCCCCC; height:27px;'><center><%#(DataBinder.Eval(Container.DataItem, "ScrReturn").ToString() != "" || DataBinder.Eval(Container.DataItem, "CautionReturn").ToString() != "") ? "반송" : DataBinder.Eval(Container.DataItem, "TranslationStatusName")%></center></td>
		    </tr>      
            </ItemTemplate> 
        </asp:Repeater>		
	</tbody>
</table>
<!-- paginator -->
<table style="width:100%">
<div class="paginator">
    <taeyo:PagingHelper ID="PagingHelper1" runat="server" OnOnPageIndexChanged="PagingHelper1_OnPageIndexChanged"
        CurrnetNumberColor="Silver" ForeColor="#404040"></taeyo:PagingHelper>
</div>
</table>
<!-- // paginator -->