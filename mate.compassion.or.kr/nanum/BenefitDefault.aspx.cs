﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
 
public partial class Mate_nanum_BenefitDefault : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtSearch.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSeach.ClientID + "').click();return false;}} else {return true}; ");
            InitPage();
        }       
    }

    private void InitPage()
    {
        WWWService.Service service = new WWWService.Service();
        DataSet data = service.GetMateNationList(57);

        //DataRow row = data.Tables[0].NewRow();
        //row["title"] = "전체";
        //row["idx"] = 0;
        //data.Tables[0].Rows.InsertAt(row, 0); 

        dropNation.DataTextField = "title";
        dropNation.DataValueField = "idx";
        dropNation.DataSource = data.Tables[0];
        dropNation.DataBind();

        data.Dispose();

        DataSet benefitData = service.GetInitBenefitList();
        DataTable A = GetDataBenefit(benefitData.Tables[0], "A");        
        RepeaterA.DataSource = A ;
        RepeaterA.DataBind();

        DataTable B = GetDataBenefit(benefitData.Tables[0], "B");
        RepeaterB.DataSource = B;
        RepeaterB.DataBind();

        DataTable C = GetDataBenefit(benefitData.Tables[0], "C");
        RepeaterC.DataSource = C;
        RepeaterC.DataBind();

        DataTable D = GetDataBenefit(benefitData.Tables[0], "D");
        RepeaterD.DataSource = D;
        RepeaterD.DataBind();

        DataTable E = GetDataBenefit(benefitData.Tables[0], "E");
        RepeaterE.DataSource = E;
        RepeaterE.DataBind();

        DataTable F = GetDataBenefit(benefitData.Tables[0], "F");
        RepeaterF.DataSource = F;
        RepeaterF.DataBind();

        DataTable G = GetDataBenefit(benefitData.Tables[0], "G");
        RepeaterG.DataSource = G;
        RepeaterG.DataBind();

        DataTable H = GetDataBenefit(benefitData.Tables[0], "H");
        RepeaterH.DataSource = H;
        RepeaterH.DataBind();

        DataTable I = GetDataBenefit(benefitData.Tables[0], "I");
        RepeaterI.DataSource = I;
        RepeaterI.DataBind();

        DataTable J = GetDataBenefit(benefitData.Tables[0], "J");
        RepeaterJ.DataSource = J;
        RepeaterJ.DataBind();

        DataTable K = GetDataBenefit(benefitData.Tables[0], "K");
        RepeaterK.DataSource = K;
        RepeaterK.DataBind();

        DataTable L = GetDataBenefit(benefitData.Tables[0], "L");
        RepeaterL.DataSource = L;
        RepeaterL.DataBind();

        DataTable M = GetDataBenefit(benefitData.Tables[0], "M");
        RepeaterM.DataSource = M;
        RepeaterM.DataBind();

        DataTable N = GetDataBenefit(benefitData.Tables[0], "N");
        RepeaterN.DataSource = N;
        RepeaterN.DataBind();

        DataTable O = GetDataBenefit(benefitData.Tables[0], "O");
        RepeaterO.DataSource = O;
        RepeaterO.DataBind();

        DataTable P = GetDataBenefit(benefitData.Tables[0], "P");
        RepeaterP.DataSource = P;
        RepeaterP.DataBind();

        DataTable Q = GetDataBenefit(benefitData.Tables[0], "Q");
        RepeaterQ.DataSource = Q;
        RepeaterQ.DataBind();

        DataTable R = GetDataBenefit(benefitData.Tables[0], "R");
        RepeaterR.DataSource = R;
        RepeaterR.DataBind();

        DataTable S = GetDataBenefit(benefitData.Tables[0], "S");
        RepeaterS.DataSource = S;
        RepeaterS.DataBind();

        DataTable T = GetDataBenefit(benefitData.Tables[0], "T");
        RepeaterT.DataSource = T;
        RepeaterT.DataBind();

        DataTable U = GetDataBenefit(benefitData.Tables[0], "U");
        RepeaterU.DataSource = U;
        RepeaterU.DataBind();

        DataTable V = GetDataBenefit(benefitData.Tables[0], "V");
        RepeaterV.DataSource = V;
        RepeaterV.DataBind();

        DataTable W = GetDataBenefit(benefitData.Tables[0], "W");
        RepeaterW.DataSource = W;
        RepeaterW.DataBind();

        DataTable X = GetDataBenefit(benefitData.Tables[0], "X");
        RepeaterX.DataSource = X;
        RepeaterX.DataBind();

        DataTable Y = GetDataBenefit(benefitData.Tables[0], "Y");
        RepeaterY.DataSource = Y;
        RepeaterY.DataBind();

        DataTable Z = GetDataBenefit(benefitData.Tables[0], "Z");
        RepeaterZ.DataSource = Z;
        RepeaterZ.DataBind();

        A.Dispose();
        B.Dispose();
        C.Dispose();
        D.Dispose();
        E.Dispose();
        F.Dispose();
        G.Dispose();
        H.Dispose();
        I.Dispose();
        J.Dispose();
        K.Dispose();
        L.Dispose();
        M.Dispose();
        N.Dispose();
        O.Dispose();
        P.Dispose();
        Q.Dispose();
        R.Dispose();
        S.Dispose();
        T.Dispose();
        U.Dispose();
        V.Dispose();
        W.Dispose();
        X.Dispose();
        Y.Dispose();
        Z.Dispose();
    }

    private DataTable GetDataBenefit(DataTable table, string initial)
    {
        int i = 0;

        DataRow[] arrRow = table.Select("initial='" + initial + "'");
        DataRow row;
        DataTable data = new DataTable();

        data.Columns.Add("eword");
        data.Columns.Add("idx");

        foreach (DataRow dr in arrRow)
        {
            if (i > 2)
                break;

            row = data.NewRow();

            row["idx"] = dr["idx"];
            row["eword"] = dr["eword"];

            data.Rows.Add(row);

            i++;
        }

        return data;
    }

    protected string GetPadding(object name, int length)
    {
        if (name.ToString().Length > length)
            return name.ToString().Substring(0, length - 1) + "..";

        else
            return name.ToString();
    }
}