﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChildLetterView.aspx.cs" Inherits="Mate_nanum_ChildLetterView" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/NanumRight.ascx" tagname="NanumRight" tagprefix="uc5" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript">
        function WinChildEtc(page) {
            NewWindow("../benefitNation/" + page + ".html", "bedetail", "573", "750", "auto");
        }
        function WinChild(page) {
            NewWindow("../benefitNation/" + page + ".html", "bedetail", "593", "750", "auto");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="benefit-letter">
 
		<!-- sidebar -->
		<uc5:NanumRight ID="NanumRight1" runat="server" />
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>수혜국 편지 정보</h3>
					<p>수혜국 어린이들의 편지와 관련된 정보 및 용어를 검색하여 해결하는 공간입니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/nanum/OurStoryList.aspx">MATE 나눔</a><strong>수혜국 편지 정보</strong>
			</div>
			<ul class="tab2 tab-benefit-letter">
				<li><a href="BenefitDefault.aspx" class="t1">수혜국 편지 용어</a></li>
				<li class="on"><a href="ChildLetterView.aspx" class="t2">수혜국 어린이 편지 보기</a></li>
			</ul>
 
			<div class="cont">
				<div class="child-letters">
					<div class=" txt txt-child-letters">
						<h4>25개국의 수혜국에서 오는 어린이 편지들</h4>
						<strong>어린이를 소개하는 첫 편지, 크리스마스 카드, 선물에 대한 감사 편지, 부활절 편지 등 내용도 다양합니다.</strong>
						<ul>
							<li>수혜국의 현지어(Local Language)로 쓰여진 내용은 편지의 윗 부분, 왼 쪽 또는 앞 장에 있습니다. </li>
							<li>영문 번역본은 편지의 아래 부분, 오른 쪽, 또는 뒷 장에 있습니다.</li>
						</ul>
					</div>
					<a href="javascript:void(0);" onclick="WinChildEtc('translation-location');" class="btn btn-position-search">영문 번역본 위치 알아보기</a>
				</div>
				<div class="area">
					 <!-- 이 영역은 차후 작업 예정입니다. -->
					<img src="/image/mate/temp/area.jpg" alt="" />
				</div>
 
				<h4 class="txt tit-letter-view"><span>수혜국 어린이 편지보기</span></h4>
 
				<ul class="letter-view">
					<li><a href="javascript:void(0);" onclick="WinChild('bf');"  class="nation nation-bf">부르키나파소(BF)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('ug');"   class="nation nation-ug">우간다(UG)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('th');"   class="nation nation-th">태국(TH)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('es');"   class="nation nation-es">엘살바도르(ES)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('bo');"   class="nation nation-bo">볼리비아(BO)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('gh');"   class="nation nation-gh">가나(GH)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('ke');"   class="nation nation-ke">케냐(KE)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('ph');"   class="nation nation-ph">필리핀(PH)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('ni');"   class="nation nation-ni">니키라과(NI)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('ha');"   class="nation nation-ha">아이티(HA)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('tg');"   class="nation nation-tg">토고(TG)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('tz');"   class="nation nation-tz">탄자니아(TZ)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('io-id');"   class="nation nation-io">인도네시아(IO/ID)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('co');"   class="nation nation-co">콜럼비아(CO)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('dr');"   class="nation nation-dr">도미니카공화국(DR)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('rw');"   class="nation nation-pw">르완다(RW)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('in-ei');"   class="nation nation-in">인도(IN/EL)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('me');"   class="nation nation-me">멕시코(ME)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('ec');"   class="nation nation-ec">에콰도르(EC)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('ho');"   class="nation nation-hq">온두라스(HQ)</a></li>
					<li><a href="javascript:void(0);" onclick="javascript:alert('스리랑카 편지는 준비중 입니다.');"   class="nation nation-sl">스리랑카</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('bd');"   class="nation nation-bd">방글라데시(BD)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('gu');"   class="nation nation-gu">과테말라(GU)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('pe');"   class="nation nation-pe">페루(PE)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('br');"   class="nation nation-br">브라질(BR)</a></li>
					<li><a href="javascript:void(0);" onclick="WinChild('et');"   class="nation nation-et">에티오피아(ET)</a></li>
				</ul>
			</div>
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    </form>
</body>
</html>
