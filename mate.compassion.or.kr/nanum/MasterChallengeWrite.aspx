﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MasterChallengeWrite.aspx.cs" Inherits="Mate_nanum_MasterChallengeWrite"  validateRequest=false %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/NanumRight.ascx" tagname="NanumRight" tagprefix="uc5" %>
<%@ Register src="../Controls/BoardWrite.ascx" tagname="BoardWrite" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript">
        function WriteValidate() {
            if (document.getElementById("BoardWrite1_txtTitle").value.trim() == "") {
                alert("제목을 입력 하세요.");
                return false;
            }
            //if (document.getElementById("BoardWrite1_txtComment").value.trim() == "") {
            //    alert("내용을 입력 하세요.");
            //    return false;
            //}
            return true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype ="multipart/form-data" method ="post">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
<div id="container" class="mate">

	<!-- wrapper -->
	<div id="wrapper" class="share master-challenge">

		<!-- sidebar -->
		<uc5:NanumRight ID="NanumRight1" runat="server" />
		<!-- //sidebar -->

		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>번달이 도전기</h3>
					<p>번역초보에서 번달이(번역의 달인)이 되기까지 이야기가 담겨있는 공간입니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/nanum/OurStoryList.aspx">MATE 나눔</a><strong>번달이 도전기</strong>
			</div>
			<uc4:BoardWrite ID="BoardWrite1" runat="server" />
		</div>
		<!-- // contents -->

	</div>
	<!-- // wrapper -->

</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    <asp:hiddenfield ID="hidTableIdx" runat="server" Value ="1005"></asp:hiddenfield>   
    </form>
</body>
</html>
