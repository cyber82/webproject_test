﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OurStoryList.aspx.cs" Inherits="Mate_nanum_OurStoryList" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>

<%@ Register src="../Controls/NanumRight.ascx" tagname="NanumRight" tagprefix="uc5" %>
<%@ Register src="../Controls/BoardList.ascx" tagname="BoardList" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript">
        function SearchValidate() {
            if (document.getElementById("BoardList1_txtSearch").value.trim() == "") {
                alert("검색어를 입력 하세요.");
                return false;
            }
            return true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
<div id="container" class="mate">

	<!-- wrapper -->
	<div id="wrapper" class="share our-story">

		<!-- sidebar -->
        <uc5:NanumRight ID="NanumRight1" runat="server" />
		<!-- //sidebar -->

		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>우리들의 이야기</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/nanum/OurStoryList.aspx">MATE 나눔</a><strong>우리들의 이야기</strong>
			</div>
			<div class="our-story-cover">
				<strong>&quot;감동과 공감이 있는 우리들의 이야기, 우리는 컴패션 메이트입니다!&quot;</strong>                
			</div>
            <div style="text-align:right" >
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="/image/btn/btn-write.png" onclick="ImageButton1_Click"/>
            </div>
			<uc4:BoardList ID="BoardList1" runat="server" />	
		</div>
		<!-- // contents -->

	</div>
	<!-- // wrapper -->

</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    <asp:hiddenfield ID="hidTableIdx" runat="server" Value ="1222"></asp:hiddenfield>    
    </form>
</body>
</html>
