﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BenefitDetail.aspx.cs" Inherits="Mate_nanum_BenefitDetail" %>

<!DOCTYPE html>
<html lang="ko">
<head> 
	<meta charset="UTF-8">
    <meta property="og:image" content="http://www.compassion.or.kr/common/img/sns_default_image.jpg">
	<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title> 
	<link rel="stylesheet" href="/common/css/mate.css" />
 
	<script type="text/javascript" src="/common/js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="/common/js/common.js"></script>
    <script language="javascript">
        function RConfirm() {
            return confirm('삭제 하시겠습니까?');
        }
    </script>
</head>
<body>
<form id="form1" runat="server">
<div class="layer1 benefit-letter-pop">
	<div class="tit"><strong>수혜국 편지 정보/용어/</strong></div>
	<div class="lay-cont">
		<div class="tit-count">
			<p class="txt-l"><strong><asp:Label ID="labEword1" runat="server" Text=""></asp:Label></strong></p>
			<p class="txt-r">조회수 : <asp:Label ID="labReadCount" runat="server" Text=""></asp:Label></p>
		</div>
		<table>
			<tbody>
				<tr>
					<th class="td-term-english"><img src="/image/mate/text/td-term-english.gif" alt="편지용어 (Alphabet)" /></th>
					<td class="td-term-english"><asp:Label ID="labEword2" runat="server" Text=""></asp:Label></td>
				</tr>
                <!--
				<tr>
					<th class="td-term-korean"><img src="/image/mate/text/td-term-korean.gif" alt="편지용어 (한글)" /></th>
					<td class="td-term-korean"><asp:Label ID="labEword" runat="server" Text=""></asp:Label></td>
				</tr>
                -->
				<tr>
					<th class="td-nation"><img src="/image/mate/text/td-nation.gif" alt="국가" /></th>
					<td class="td-nation"><asp:Label ID="labNationStr" runat="server" Text=""></asp:Label></td>
				</tr>
				<tr>
					<th class="td-mean"><img src="/image/mate/text/td-mean.gif" alt="용어 뜻" /></th>
					<td class="td-mean2"><div class="scrollwrap"><asp:Label ID="labComment" runat="server" Text=""></asp:Label></div></td>
				</tr>
			</tbody>
		</table>
		<div class="write-info">
		<div class="btn-l">
            <asp:Button ID="btnDel" runat="server" Text="" CssClass="btn btn-delete2" 
                onclick="btnDel_Click" OnClientClick ="return RConfirm()" 
                Visible="False" />
			<!--<a href="" class="btn btn-modify3"><span>수정</span></a>-->
		</div>
		<p><asp:Label ID="labRegisterDate" runat="server" Text=""></asp:Label>&nbsp;&nbsp;<strong><asp:Label ID="labRegUserName" runat="server" Text=""></asp:Label></strong></p>
		</div>
	</div>
	<button type="button" onclick="javascript:window.close();" class="btn btn-close"><span>닫기</span></button>
</div>
</form>
</body>
</html>
