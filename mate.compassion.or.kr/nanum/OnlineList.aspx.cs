﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class Mate_nanum_OnlineList : System.Web.UI.Page
{
    public bool bThirdPL;
    public bool bScreening;
    public bool bTranslate;

    protected void Page_Load(object sender, EventArgs e)
    {
        string sJs;

        if (UserInfo.IsLogin)
        {
            string sUserId = new UserInfo().UserId;

            //권한 체크
            Object[] objSql = new object[1] { "MATE_MateYearCheck" };
            Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
            Object[] objValue = new object[2] { sUserId, "" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
            DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //권한체크
            if (data.Tables[0].Rows.Count > 0)
            {
                //권한 체크
                objSql = new object[1] { "MATE_MateYearCheck" };
                objParam = new object[2] { "MateID", "MateYearTypeCode" };
                Object[] objThirdPLValue = new object[2] { new UserInfo().UserId, "31" };
                Object[] objScreeningValue = new object[2] { new UserInfo().UserId, "21" };
                Object[] objTranslateValue = new object[2] { new UserInfo().UserId, "11" };

                //3PL 권한체크
                if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objThirdPLValue).Tables[0].Rows.Count > 0)
                {
                    bThirdPL = true;
                }

                //스크리닝 권한체크
                if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objScreeningValue).Tables[0].Rows.Count > 0)
                {
                    bScreening = true;
                }

                //번역 권한체크
                if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objTranslateValue).Tables[0].Rows.Count > 0)
                {
                    bTranslate = true;
                }

                _WWW6Service.Dispose();

                //if (bThirdPL || bScreening || bTranslate)
                //{

                //}
                //else
                //{
                //    Response.Redirect("/default.aspx");
                //}
            }

            else
            {
                //StringBuilder sb = new StringBuilder();
                //sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                //sb.Append("alert('해당 권한이 없습니다.'); location.href='http://" + Request.Url.Authority + "/';");
                //sb.Append("</script>");

                //Response.Write(sb.ToString());
            }
        }
        else
        {
            //string sReturnUrl = Request.Url.AbsoluteUri.Replace("http://" + Request.Url.Authority, "");
            //sReturnUrl = sReturnUrl.Replace("https://" + Request.Url.Authority, "");

            //sJs = JavaScript.HeaderScript;
            //sJs += JavaScript.GetAlertScript("로그인 후 사용 가능합니다.");
            //sJs += JavaScript.GetPageMoveScript("/login.aspx");
            //sJs += JavaScript.GetHistoryBackScript();
            //sJs += JavaScript.FooterScript;
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        string sJs;

        if (UserInfo.IsLogin)
        {
            if (bThirdPL || bScreening || bTranslate)
            {
                Response.Redirect("OnlineWrite.aspx");
            }
            else
            {
                sJs = JavaScript.HeaderScript;
                sJs += JavaScript.GetAlertScript("활동 중인 번역메이트만 사용 가능합니다.");
                sJs += JavaScript.GetPageMoveScript("/login.aspx");
                sJs += JavaScript.GetHistoryBackScript();
                sJs += JavaScript.FooterScript;
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
            }
        }
        else
        {
            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("로그인 후 사용 가능합니다.");
            sJs += JavaScript.GetPageMoveScript("/login.aspx");
            sJs += JavaScript.GetHistoryBackScript();
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }



        
    }
}