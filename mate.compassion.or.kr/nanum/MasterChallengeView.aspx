﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MasterChallengeView.aspx.cs" Inherits="Mate_nanum_MasterChallengeView" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>

<%@ Register src="../Controls/NanumRight.ascx" tagname="NanumRight" tagprefix="uc6" %>
<%@ Register src="../Controls/BoardDetail.ascx" tagname="BoardDetail" tagprefix="uc4" %>
<%@ Register src="../Controls/BoardList.ascx" tagname="BoardList" tagprefix="uc5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript">
        function Validate() {
            if (document.getElementById("BoardDetail1_comment").value.trim() == "") {
                alert("댓글을 입력하세요.");
                return false;
            }

            if (BytesCheck(document.getElementById('BoardDetail1_comment').value.trim()) > 1000) {
                alert("댓글은 영문 1000자리 한글 500자리이상을 초과할수없습니다.");
                document.getElementById('BoardDetail1_comment').focus();
                return false;
            }
            return true;
        }

        function SearchValidate() {
            if (document.getElementById("BoardList1_txtSearch").value.trim() == "") {
                alert("검색어를 입력 하세요.");
                return false;
            }
            return true;
        }

        //-- 바이트수체크
        function BytesCheck(sValue) {
            var tcount = 0;

            var tmpStr = new String(sValue);
            var temp = tmpStr.length;

            var onechar;
            for (k = 0; k < temp; k++) {
                onechar = tmpStr.charAt(k);
                if (escape(onechar).length > 4) {
                    tcount += 2;
                }
                else {
                    tcount += 1;
                }
            }

            return tcount;
        }        
        
    </script>
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
<div id="container" class="mate">

	<!-- wrapper -->
	<div id="wrapper" class="share master-challenge">

		<!-- sidebar -->
		<uc6:NanumRight ID="NanumRight1" runat="server" />
		<!-- //sidebar -->

		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>번달이 도전기</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/nanum/OurStoryList.aspx">MATE 나눔</a><strong>번달이 도전기</strong>
			</div>
			<uc4:BoardDetail ID="BoardDetail1" runat="server" />
			<uc5:BoardList ID="BoardList1" runat="server" />
		</div>
		<!-- // contents -->

	</div>
	<!-- // wrapper -->

</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    <asp:hiddenfield ID="hidTableIdx" runat="server" Value ="1005"></asp:hiddenfield> 
    </form>
</body>
</html>
