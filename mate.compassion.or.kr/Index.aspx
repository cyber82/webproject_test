﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Mate_Index" %>

<%@ Register src="~/Controls/MateTranslationHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="~/Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="UTF-8">
    <title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title> 
    <link rel="stylesheet" href="/common/css/mate.css?v=1.1" />
    <link rel="stylesheet" href="/common/css/layout.css" />
    <link rel="shortcut icon" href="/common/img/common/favicon.ico" />
	<script type="text/javascript" src="/common/js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="/common/js/common.js"></script>
	<script type="text/javascript" src="/common/js/jquery.simplyscroll-1.0.4.min.js"></script>
	<script type="text/javascript" src="/common/js/slides.min.jquery.js"></script>
	<script type="text/javascript">
	    $(function () {

	        $("#scroller").simplyScroll({ className: 'vert',
	            horizontal: false,
	            frameRate: 20,
	            speed: 1
	        });

	        $('#slides').slides({
	            play: 5500, effect: 'fade', fadeSpeed: 800
	        });
	    });
	</script>

<script src="http://widgets.twimg.com/j/2/widget.js"></script>


<script language="javascript" type="text/javascript">
    function getCookie(name) {
        var nameOfCookie = name + "=";
        var x = 0;
        while (x <= document.cookie.length) {
            var y = (x + nameOfCookie.length);
            if (document.cookie.substring(x, y) == nameOfCookie) {
                if ((endOfCookie = document.cookie.indexOf(";", y)) == -1)
                    endOfCookie = document.cookie.length;
                return unescape(document.cookie.substring(y, endOfCookie));
            }
            x = document.cookie.indexOf(" ", x) + 1;
            if (x == 0) break;
        }
        return "";
    }

    notice();
    function notice() { //새창을 띄우는 함수 
    }
</script>
</head>

<%--<body>--%>
<body class="index" oncontextmenu='return false' ondragstart='return false'>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        <div id="container">
 
	        <h2 class="hide">메인 페이지</h2>
            
            <% //추가 2013-06-03  팝업창 %>
            <script type="text/javascript">
            <!--
                function popupLoad(pname, idx, pleft, ptop, pwidth, pheight, pstatus, pscrollbar) {
                    if (getCookie(pname) != 'done') {
                        var styn = "";
                        var scyn = "";

                        if (pstatus == "Y") styn = "yes"; else styn = "no";
                        if (pscrollbar == "Y") scyn = "yes"; else scyn = "no";

                        window.open('/popup/popup.aspx?idx=' + idx
                                , ''
                                , 'left=' + pleft + ',top=' + ptop + ',width=' + pwidth + ',height=' + pheight + ',status=' + styn + ',scrollbars=' + scyn
                                + ',toolbar=no,location=no,directories=no,menubar=no,resizable=no');
                    }
                }
            //-->
            </script>
            <div style="display:none;">
                <asp:Repeater ID="PopupData" runat="server" OnItemDataBound="PopupData_ItemDataBound" >
                    <ItemTemplate>
                        <div id="popup_div" runat="server"></div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
 
	        <div class="cover" id="slides">
		        <ul class="slides_container">
                    <asp:Repeater ID="RollingData" runat="server" >                    
                        <ItemTemplate>                    
                            <li>
                                <a href='<%#Eval("link")%>' target='<%#Eval("ssb_target").ToString() == "B" ? "_blank" : "_self" %>'>
                                    <img src='/main_image/<%#Eval("code")%>/<%#Eval("img_filename")%>' alt='<%#Eval("title")%>' />
                                </a>
                            </li>
                        </ItemTemplate>                    
                    </asp:Repeater>
		        </ul>
	        </div>

 
	        <!-- wrapper -->
	        <div id="wrapper">
 
		        <!-- contents -->
		        <div id="contents">
 
			        <dl class="todays-words">
				        <dt class="txt">오늘의 말씀</dt>
				        <dd><ul id="scroller"><li><asp:HyperLink ID="toAWordList" runat="server" CssClass="context"></asp:HyperLink></li></ul></dd>
			        </dl>
 
			        <div class="articles">
				        <div class="wrap">
					        <ul class="tabs1">
						        <li class="on tab-notice" ><a href="#sec-notice" style="cursor:default"  class="txt">공지사항</a></li>
					        </ul>
 
					        <div id="sec-notice" class="sec sec-notice tab-content1">
						        <h2 class="hide">공지사항</h2>
						        <ul>
                                <asp:Repeater ID="repNotiData" runat="server" onitemdatabound="repData_ItemDataBound">
                                <ItemTemplate>
							        <%--<li><a href="./community/noticeview.aspx?iTableIndex=<%#DataBinder.Eval(Container.DataItem,"table_idx")%>&iNoIndex=<%#DataBinder.Eval(Container.DataItem,"no_idx")%>&iBoardIdx=<%#DataBinder.Eval(Container.DataItem,"board_idx")%>&ref=<%#DataBinder.Eval(Container.DataItem,"ref")%>&re_step=<%#DataBinder.Eval(Container.DataItem,"re_step")%>"><div style="width: 200px; text-overflow: ellipsis; display: inline-flex; overflow: hidden; white-space: nowrap; float:left;"><%#WebCommon.GetNamePadding (DataBinder.Eval(Container.DataItem,"b_title"),18)%></div></a><asp:Label ID="labNew" runat="server" Text="new" CssClass="ico-new"></asp:Label> <span class="date"><%#GetDate(DataBinder.Eval(Container.DataItem, "WriteDay"))%></span><asp:HiddenField ID="hidb_writeday" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "b_writeday")%>' /></li>--%>
                                    <li><a href="./community/noticeview.aspx?iTableIndex=<%#DataBinder.Eval(Container.DataItem,"table_idx")%>&iNoIndex=<%#DataBinder.Eval(Container.DataItem,"no_idx")%>&iBoardIdx=<%#DataBinder.Eval(Container.DataItem,"board_idx")%>&ref=<%#DataBinder.Eval(Container.DataItem,"ref")%>&re_step=<%#DataBinder.Eval(Container.DataItem,"re_step")%>"><div style="width: 200px; text-overflow: ellipsis; display: inline-block; overflow: hidden; white-space: nowrap;"><%#DataBinder.Eval(Container.DataItem,"b_title")%></div></a><asp:Label ID="labNew" runat="server" Text="new" CssClass="ico-new" style="position:absolute;"></asp:Label> <span class="date"><%#GetDate(DataBinder.Eval(Container.DataItem, "WriteDay"))%></span><asp:HiddenField ID="hidb_writeday" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "b_writeday")%>' /></li>
                                </ItemTemplate>
                                </asp:Repeater>
						        </ul>
						        <a href="./community/noticelist.aspx" class="btn-more">more</a>
					        </div>
				        </div>
 
				        <div class="sec sec-member">
                            <script language="javascript">
                                function checkPermission(url) {
                                    if ("<%=bLogin%>" == "True") {
                                        if ("<%=bThirdPL%>" == "True" || "<%=bScreening%>" == "True" || "<%=bTranslate%>" == "True") {
                                            location.href = url;
                                        }
                                        else {
                                            alert("활동 중인 번역메이트만 사용 가능합니다.");
                                        }
                                    }
                                    else {
                                        alert("로그인 후 사용 가능합니다.");
                                    }
                                }
                            </script>
					        <h2 class="txt">함께하는 이야기</h2>
					        <ul>
						        <asp:Repeater ID="repOurData" runat="server" onitemdatabound="repOurData_ItemDataBound">
                                <ItemTemplate>
							        <%--<li><a href="./nanum/TogetherStoryList.aspx?iTableIndex=<%#DataBinder.Eval(Container.DataItem,"table_idx")%>&iNoIndex=<%#DataBinder.Eval(Container.DataItem,"no_idx")%>&iBoardIdx=<%#DataBinder.Eval(Container.DataItem,"board_idx")%>&ref=<%#DataBinder.Eval(Container.DataItem,"ref")%>&re_step=<%#DataBinder.Eval(Container.DataItem,"re_step")%>"><div style="width: 200px; text-overflow: ellipsis; display: inline-flex; overflow: hidden; white-space: nowrap; float:left;"><%#WebCommon.GetNamePadding (DataBinder.Eval(Container.DataItem,"b_title"),23)%></div></a><asp:Label ID="labNew" runat="server" Text="new" CssClass="ico-new"></asp:Label> <span class="date"><%#GetDate(DataBinder.Eval(Container.DataItem, "WriteDay"))%></span><asp:HiddenField ID="hidb_writeday" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "b_writeday")%>' /></li>--%>
                                    <li><a href="javascript:checkPermission('./nanum/TogetherStoryList.aspx?iTableIndex=<%#DataBinder.Eval(Container.DataItem,"table_idx")%>&iNoIndex=<%#DataBinder.Eval(Container.DataItem,"no_idx")%>&iBoardIdx=<%#DataBinder.Eval(Container.DataItem,"board_idx")%>&ref=<%#DataBinder.Eval(Container.DataItem,"ref")%>&re_step=<%#DataBinder.Eval(Container.DataItem,"re_step")%>')"><div style="width: 200px; text-overflow: ellipsis; display: inline-block; overflow: hidden; white-space: nowrap;"><%#DataBinder.Eval(Container.DataItem,"b_title")%></div></a><asp:Label ID="labNew" runat="server" Text="new" CssClass="ico-new" style="position:absolute;"></asp:Label> <span class="date"><%#GetDate(DataBinder.Eval(Container.DataItem, "WriteDay"))%></span><asp:HiddenField ID="hidb_writeday" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "b_writeday")%>' /></li>
                                </ItemTemplate>
                                </asp:Repeater>
					        </ul>
					        <a href="javascript:checkPermission('./nanum/TogetherStoryList.aspx')" class="btn-more">more</a>
				        </div>
			        </div>
 
			        <div class="linked">
                        <asp:Repeater ID="ssbData" runat="server" OnItemDataBound="ssbData_ItemDataBound" >                    
                            <ItemTemplate>                    
					            <div class="sec">
					                <h2 class="tit"><%#Eval("code_name") %></h2>
					                <div class="cont">
                                        <a href="<%#Eval("link") %>">
                                            <img src="/image/mate/temp/<%#Eval("code") %>/<%#Eval("img_filename") %>" width="195" height="130" alt="<%#Eval("caption")%>" /></a>
                                        </a>

                                        <div class="caption"><strong><%#Eval("caption")%></strong><div class="bg"></div></div>
                                    </div>

					                <a href="/community/monthlymatelist.aspx" id="a_more" runat="server" class="btn-more">more</a>
                                    <input type="hidden" name="code_gubun" id="code_gubun" runat="server" value='<%#Eval("code") %>' />
				                </div>
                            </ItemTemplate> 
                        </asp:Repeater>
			        </div>
 
			        <div class="applied">
				        <div class="sec-mate-apply">
					        <p class="txt txt-mate-apply"><span>한국컴패션의 모든 분야에서 활동하는 MATE를 신청해 보세요! 자세한 사항은 각 분야의 메뉴를 참조해 주세요.</span></p>
					        <a href="/Intro/MateIntro.aspx" class="btn btn-apply-mate1"><span>MATE 신청하기</span></a>
				        </div>
 
				        <div class="sec-certificate">
					        <p class="txt txt-certificate"><span>봉사활동 확인서 출력 - 봉사 내역확인 &amp; 봉사 확인서 출력하세요.</span></p>
					        <a href="/Mypage/ServiceBreakDown.aspx" class="btn btn-detail1"><span>자세히보기</span></a>
				        </div>
			        </div>
 
		        </div>
		        <!-- // contents -->
 
		        <!-- sidebar -->
		        <div id="sidebar">
 
			        <fieldset class="login-area"  id="Logout" runat ="server">
				        <h2 class="txt txt-login">LOGIN 로그인을 해주세요.</h2>
				        <div class="wrap">
                            <a href="/login.aspx" class="btn btn-login2"></a>
				        </div>
                        <asp:HiddenField ID="hidTempSpnsorID" runat="server" Value=""/>
			        </fieldset>
 
                    <div class="personalize" id="Login" runat ="server">
				        <p><strong><asp:Label ID="labUserName" runat="server" Text=""></asp:Label></strong> <strong class="txt txt-greeting"><span>님 환영합니다! </span></strong></p>
				        <div class="btns">                    
					        <a href="/mypage/ServiceBreakDown.aspx" class="btn btn-conf-voluntary"><span>봉사활동확인</span></a>
                            <!--<a href="/Logoff.aspx?mate=on" class="btn btn-logout"><span>로그아웃</span></a>-->
                            <a href="/logout.aspx" class="btn btn-logout"><span>로그아웃</span></a>
				        </div>
			        </div>

			        <div class="sec-update"> <% //수정 2012-12-05 %>
				        <h2 class="txt">오늘의 편지현황 | 일일 업데이트<!--실시간 업데이트--></h2>
				        <table class="wrap">
					        <tr class="first-tr">
						        <th><span class="txt txt-today-child">번역이 필요한 어린이편지<!--어린이 편지--></span></th>
						        <td><div><strong class="txt-num"><asp:Label ID="Label1" runat="server" Text=""></asp:Label></strong><span class="txt txt-letter">통</span></div></td>
					        </tr>
					        <tr>
						        <th><span class="txt txt-today-supporter">번역이 필요한 후원자편지<!--후원자 편지--></span></th>
						        <td><div><strong class="txt-num"><asp:Label ID="Label2" runat="server" Text=""></asp:Label></strong><span class="txt txt-letter">통</span></div></td>
					        </tr>
					        <tr>
						        <th><span class="txt txt-past-mate">지난 주 참여한 메이트</span></th>
						        <td><div><strong class="txt-num"><asp:Label ID="Label3" runat="server" Text=""></asp:Label></strong><span class="txt txt-person">명</span></div></td>
					        </tr>
					        <tr class="first-tr last-el">
						        <th><span class="txt txt-past-letter">지난 주 번역한 편지</span></th>
						        <td><div><strong class="txt-num"><asp:Label ID="Label4" runat="server" Text=""></asp:Label></strong><span class="txt txt-letter">통</span></div></td>
					        </tr>
				        </table>
			        </div>
 
			        <div class="sec-twitter">				
                        <script>
                            new TWTR.Widget({ version: 2, type: 'profile', rpp: 20, interval: 6000, width: 284, height: 230, theme: {
                                shell: { background: '#cccccc', color: '#ffffff' },
                                tweets: { background: '#ffffff', color: '#666666', links: '#7e7bdb' }
                            },
                                features: { scrollbar: true, loop: false, live: true, hashtags: false, timestamp: true, avatars: false, behavior: 'all' }
                            }).render().setUser('compassionkorea').start();
                        </script>
			        </div>
 
		        </div>
		        <!-- //sidebar -->
 
	        </div>
	        <!-- // wrapper -->
 
        </div>


		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    </form>
</body>
</html>
