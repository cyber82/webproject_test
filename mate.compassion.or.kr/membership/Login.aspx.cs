﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
public partial class membership_Login : System.Web.UI.Page {
	protected void Page_Load( object sender, EventArgs e ) {

		var r = Request["returnUrl"];
		
		if(!r.StartsWith("http://")) {

			r = "http://" + Request.Url.DnsSafeHost + r;
			r = HttpUtility.UrlEncode(r);
			var url = string.Format(ConfigurationManager.AppSettings["loginPage"], r);
			Response.Redirect(url);

		}

	}
}