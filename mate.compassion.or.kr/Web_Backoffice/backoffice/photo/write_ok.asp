<% Response.Buffer=true %>
<%
  table_idx = request("table_idx")
  no_idx = request("no_idx")
  page = request("page")
	keyfield = request("keyfield")	
	key = request("key")
%>
<% 
 set UploadForm = server.CreateObject("SiteGalaxyUpload.Form")
  'sitegalaxyupload 개체를 생성한다....
  ' sitegalaxyupload 를 사용할때는 request 대신에 uploadform을 사용한다..중요...
  'write.asp 에서 넘긴 값들을 받아온다.....

  b_notice = uploadform("b_notice")
  b_name = uploadform("b_name")  
  b_email = UploadForm("b_email")
  b_hompage = UploadForm("b_hompage")
  b_title = UploadForm("b_title")
  b_pwd = UploadForm("b_pwd")
  b_tel = UploadForm("b_tel")
  b_content = UploadForm("b_content")
  b_filename = UploadForm("b_filename")
  idx = uploadform("idx")
  ref = uploadform("ref")
  re_step = uploadform("re_step")
  re_level = uploadform("re_level")
  
  b_title=replace(b_title,"'","''") '쿼리에 '가 들어가면 에러가 나기 때문에 그것을 replace처리 합니다.
  b_content=replace(b_content,"'","''")
 
  
  '**********************************************************
  '                 Upload File 이 있을때의 처리
  '**********************************************************
  
   if len(b_filename) > 0 then '파일의 이름이 존재할경우.....
      b_filesize=UploadForm("b_filename").size '파일의 크기를 저장
      maxsize=Cint(20) * 1024 
      
      set fc = createobject("Scripting.FileSystemObject")  'fs 라는 인스턴스를 생성 

      storedir =  "E:\Compassion\pdsfile2\" '파일이 저장될 절대경로를 설정
      attach_file=UploadForm("b_filename").filepath '파일이 저장될 경로를 지정
      b_filename=mid(attach_file, instrrev(attach_file,"\")+1) '경로명을 제외한 파일명을 추출 
      strname=mid(b_filename, 1, instrrev(b_filename, ".")-1) ' file 명에서 이름과 확장자를 분리
      strext=mid(b_filename, instrrev(b_filename, ".")+1) '확장자를얻는다
      
      strfilename=storedir & b_filename ' 저장할 이름의 전체 path를 만듭니다. ex) c:\inetpub\upload\write.asp
      
      fexist = true
      count = 0
      
      do while fexist ' 파일이 중복될경우 이름을 다시 지정 - 파일이름 뒤에 숫자를 붙여서 업로드
        if(fc.FileExists(strfilename)) then
             count = count+1 '파일이름에 숫자를 붙인 새로운 파일 이름 생성
             b_filename=strname&"-"&count&"."&strext
             strfilename=storedir & b_filename
         else
            fexist = false
         end if
       
       loop
          UploadForm("b_filename").saveas strfilename ' 콤포넌트에 의해 실제 파일을 업로드
    end if
    
   '****************************************************
   '    Upload 할 파일이 없을때 적절한 값으로 대체   
   '****************************************************
   if b_filename = "" then
       b_filesize=0
       b_filename=""
   end if
   
   '*****************************************************
   '           file size변환
   '*****************************************************            
      
     temp = clng(filesize)
        if temp >1024 then   '1024byte(1kbyte) 보다 크면
            temp = temp / 1024
            b_filesize = cstr(cint(temp)) & "k"
         end if 
%>

<!--#include file="../../lib/dbcon.asp"-->
<%       
  
  sql = "select Max(b_num) from upboard"  
  set rs = server.CreateObject("adodb.recordset")
  rs.Open sql,db
  
  if isNull(rs(0)) then ' 글이 없을경우 b_num을 1로 한다
     b_num = 1
  else
    b_num=rs(0) + 1
  end if
  
  '*************답변형 게시판의 추가 부분************************
  if idx <> "" then
  'if uploadform("idx") <> "" then '즉 답변쓰기라면 
  
  ref = cint(ref)
  re_step = cInt(re_step)
  re_level = cint(re_level)
  
  sqlstring = "update upboard set re_step = re_step + 1 where ref=" & ref & " AND re_step > " & re_step &" and table_idx = '"&table_idx&"' and notice_idx = '"&b_notice&"' "
  
  db.Execute(sqlstring)
  
  re_step = re_step + 1
  re_level = re_level + 1
  
  else ' 첨글쓰기 라면
  ref = b_num
  re_step=0
  re_level=0
  end if

if idx <> "" then
  
 '********테이블에 저장한다.**************** 
 sql = "insert into upboard (table_idx,no_idx, notice_idx, b_admin, b_name, b_email, b_hompage, b_title, b_content, b_num,"
  sql = sql & "b_readnum, b_writeday, ref, re_level, re_step, b_pwd,b_filename, b_filesize) values "
  sql = sql & "('" & table_idx & "'"
  sql = sql & ",'" & no_idx & "'"
  sql = sql & ",'" & b_notice & "'"
  sql = sql & ",'A'"
  sql = sql & ",'" & b_name & "'"
  sql = sql & ",'" & b_email & "'"
  sql = sql & ",'" & b_hompage & "'"
  sql = sql & ",'" & b_title & "'"
  sql = sql & ",'" & b_content & "'"
  sql = sql & "," & b_num
  sql = sql & ",0,'"& now() & "'"
  sql = sql & "," & ref
  sql = sql & "," & re_level
  sql = sql & "," & re_step
  sql = sql & ",'" & b_pwd & "'"
  sql = sql & ",'" & b_filename & "'"
  sql = sql & ",'" & b_filesize & "')"
  
  db.Execute sql

else

    sql="select max(no_idx) from upboard where table_idx = '"&table_idx&"' and notice_idx = '"&b_notice&"' "

    set record=db.execute(sql)
    if isnull(record(0)) then titleno="1" else titleno= record(0) +1

 '********테이블에 저장한다.**************** 
 sql = "insert into upboard (table_idx,no_idx, notice_idx, b_admin, b_name, b_email, b_hompage, b_title, b_content, b_num,"
  sql = sql & "b_readnum, b_writeday, ref, re_level, re_step, b_pwd,b_filename, b_filesize) values "
  sql = sql & "('" & table_idx & "'"
  sql = sql & ",'" & titleno & "'"
  sql = sql & ",'" & b_notice & "'"
  sql = sql & ",'A'"
  sql = sql & ",'" & b_name & "'"
  sql = sql & ",'" & b_email & "'"
  sql = sql & ",'" & b_hompage & "'"
  sql = sql & ",'" & b_title & "'"
  sql = sql & ",'" & b_content & "'"
  sql = sql & "," & b_num
  sql = sql & ",0,'"& now() & "'"
  sql = sql & "," & ref
  sql = sql & "," & re_level
  sql = sql & "," & re_step
  sql = sql & ",'" & b_pwd & "'"
  sql = sql & ",'" & b_filename & "'"
  sql = sql & ",'" & b_filesize & "')"

'response.write sql

  db.Execute sql

end if
   
 ' 인스턴스를 소멸......  
  rs.Close
  db.Close
  set rs=nothing
  set db=nothing
  set FSO = Nothing
  set UploadForm = nothing
 
  %>
 <script language="javascript">
  location="list.asp?table_idx=<%=table_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>"
</script>