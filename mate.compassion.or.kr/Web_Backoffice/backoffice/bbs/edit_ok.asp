<% @CODEPAGE = 65001 %>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<% '추가 2013-01-23 %>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
    '추가 2013-01-23
    'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

    '수정 2013-02-27
    provWSDL = session("serviceUrl")


    table_idx = request("table_idx")
    no_idx = request("no_idx")

    page = request("page")
    b_pwd = request("b_pwd")
    ref = request("ref")
    re_step = request("re_step")
    notice_idx = request("notice_idx")

    keyfield = request("keyfield")	
    key = request("key")

%>

<%
    b_notice = request("b_notice")  
    b_name = request("b_name")  
    b_tel = request("b_tel")
    no_idx2 = request("no_idx2")
    b_email = request("b_email")
    b_hompage = request("b_hompage")
    b_title = request("b_title")
    b_content = request("b_content")
    b_filename = request("b_filename")
    idx = request("idx")
    b_writeday = request("b_writeday") 
    b_readnum = request("b_readnum") 
    d_confirm = request("d_confirm") 
    f_confirm = request("f_confirm") 
    f_part = request("f_part") 

    '추가 2013-06-05
    table_id = request("table_id") 
    view_type = request("view_type") 
    
    b_title = replace(b_title, "'", "''") '쿼리에 '가 들어가면 에러가 나기 때문에 그것을 replace 처리 해준다.
    b_content = replace(b_content, "'", "''")
%>

<% 
 
'현재글의 비밀번호를 가져오는 쿼리문을 작성한다.
'수정 2013-01-23
sql = "select b_pwd from upboard where table_idx = '"&table_idx&"' and no_idx = "&no_idx&" and notice_idx = '"&notice_idx&"' and ref = "&ref&" and re_step = "&re_step&" "
'set result= db.Execute(sql)' 받아온 값 쿼리를 실행....
Set result = RecordsetFromWS(provWSDL, sql)
 
'입력받은 비밀번호와 실제 비밀번호르 비교하여 일치한다면
if b_pwd = result("b_pwd") then

    sql = "update upboard set notice_idx = '" & b_notice& "'"
    sql = sql & ", b_name = N'" & b_name & "'"
    sql = sql & ", b_title = N'" & b_title & "'"
    sql = sql & ", b_email = N'" & b_email & "'"
    sql = sql & ", b_content = N'" & b_content & "'"
    sql = sql & ", b_hompage = N'" & b_hompage & "'"
    sql = sql & ", b_filename1 = N'" & b_filename & "'"
    sql = sql & ", b_filesize = N'" & b_filesize & "'"
    sql = sql & ", b_readnum = N'" & b_readnum & "'"  
    sql = sql & ", d_confirm = N'" & d_confirm & "'"    
    sql = sql & ", f_confirm = N'" & f_confirm & "'"  
    sql = sql & ", f_part = N'" & f_part & "'"  
    'sql = sql & ", b_writeday = N'" & b_writeday & "'"
        
    '추가 2013-06-05
    sql = sql & ", table_idx = N'" & table_id & "'"  
    sql = sql & ", ViewYN = N'" & view_type & "'"  
    
    sql = sql & " where no_idx = "&no_idx&" and table_idx = '"&table_idx&"' and notice_idx = '"&notice_idx&"' and ref = "&ref&" and re_step = "&re_step&" "
    
    'Response.Write sql & "<br/><br/>"
    'db.Execute sql

    '추가 2013-01-23  
    upresult = BoardWriteWS(provWSDL, sql)
    'Response.Write(upresult)
    'Response.End()

    '추가 2013-07-17
    if upresult = "10" then
    %>
    <script type="text/javascript" charset='euc-kr'>
    <!--
        alert('수정되었습니다.');
    //-->
    </script>
    <%
    end if 


    if upresult <> "10" AND upresult <> "" then
    %>
    <script type="text/javascript" charset='euc-kr'>
    <!--
        alert('<%=upresult %>');
    //-->
    </script>
    <%
    end if 
%>

<script type="text/javascript" charset='euc-kr'>
<!--
    location="list.asp?table_idx=<%=table_id%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>"
//-->
</script>

<%   '데이터 베이스에 현재의 레코드를 update 하고 리스트로 돌아간다.
else
%>

<script type="text/javascript" charset='euc-kr'>
<!--
    alert("비밀번호가 일치하지 않습니다.");
    history.back();
//-->
</script>

<% end if %>
     
</html>