
<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

<body>
<table width="140" border="1" cellspacing="0" cellpadding="0">

   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../band/list.asp" target="listFrame">컴패션 밴드</a></b></td>
   </tr>

<!-- 주석처리 2013-03-22
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="http://compassionk.cafe24.com/20110711/backoffice/letter_list.php" target="listFrame">스마트레터 신청</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="http://compassionk.cafe24.com/20110630/backoffice/trans_list.php" target="listFrame">번역메이트 동의서</a></b></td>
   </tr>
-->
<!-- 
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="http://compassiontr.cafe24.com/backoffice/mypage/list.asp" target="listFrame">KBS공연 신청</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="http://compassiontr.cafe24.com/backoffice/mate/list.asp" target="listFrame">KBS공연 메이트신청</a></b></td>
   </tr>
-->
<!-- 주석처리 2013-03-22
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="http://compassionk.cafe24.com/banner/bbs_list.php" target="listFrame">지난 이벤트</a></b></td>
   </tr>
-->

   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1001" target="listFrame">공지사항</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1006" target="listFrame">재해소식</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1011" target="listFrame">아이티 재난소식</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1007" target="listFrame">인도네시아 재난소식</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1010" target="listFrame">보도기사</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1014" target="listFrame">자료실</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1050" target="listFrame">나눔별자료실</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=2001" target="listFrame">편지쓰기 FAQ</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1227" target="listFrame">기도파트너</a></b></td>
   </tr>

   <!--수정 2013-05-13 -->
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../faq/list.asp?table_idx=1015" target="listFrame">FAQ</a></b></td>
   </tr>

   <!-- 수정 2013-03-22 -->
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../ldp/list.asp" target="listFrame">LDP학생 등록</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../ldp/01_list.asp" target="listFrame">LDP신청 현황</a></b></td>
   </tr>

   <% '추가 2013-05-21 %>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../event/list.asp" target="listFrame">캠페인&이벤트</a></b></td>
   </tr>

   <% '추가 2013-06-11 %>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../organization/list.asp" target="listFrame">정보공시</a></b></td>
   </tr>

   <% '추가 2013-06-26 %>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../sunday/list.asp" target="listFrame">컴패션 선데이</a></b></td>
   </tr>

   <% '추가 2013-07-15 %>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../event_wydick/list.asp" target="listFrame">크리스채너티 투데이</a></b></td>
   </tr>

   <% '추가 2013-09-04 %>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../donation_receipt/list.asp" target="listFrame">기부금 영수증</a></b></td>
   </tr>

   <% '추가 2013-09-23 %>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../login_record/list.asp" target="listFrame">관리자 로그인정보</a></b></td>
   </tr>

   <% '추가 2013-09-30 %>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../haiti_kuhofund/list.asp" target="listFrame">아이티 후원금<br />전환 동의</a></b></td>
   </tr>


<!-- 주석처리 2013-03-22
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="cf.asp" target="listFrame">크리스마스 금액</a></b></td>
   </tr>

   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="http://compassiontr.cafe24.com/backoffice/photo/list.asp" target="listFrame">LDP학생 등록</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="http://compassiontr.cafe24.com/backoffice/photo/01_list.asp" target="listFrame">LDP신청 현황</a></b></td>
   </tr>
-->
<!-- 
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="cd_list.asp" target="listFrame">후원자 CD신청</a></b></td>
   </tr>
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="ca_list.asp" target="listFrame">CD/칼랜더 주문신청</a></b></td>
   </tr> 
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="plus_list.asp" target="listFrame">더하기 신청</a></b></td>
   </tr>
-->
<!-- 주석처리 2013-03-22
   <tr>
     <td width="140" height="25" align="center" bgcolor="#EBEBEB"><b><a href="http://compassionk.cafe24.com/20110320/backoffice/list.php" target="listFrame">비지정 결연</a></b></td>
   </tr>

</table>
-->

</body>
</html>
