﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<% 
'주석처리 2013-01-18
'<!--#include file="../inc/top_main.asp"//--> 
%>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

<% '추가 2013-01-18 %>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
    'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

    '수정 2013-02-27
    provWSDL = session("serviceUrl")


    table_idx = request("t_id")
    no_idx = request("id")
    page = request("page")
    n_id = request("n_id")
    ref = request("ref")
    re_step = request("re_step")
%>

<%
    sql = "select * from upboard  where table_idx = '" & table_idx & "' and no_idx = "&no_idx&"   and ref = "&ref&" and re_step = "&re_step&" "
    'set rs = server.CreateObject("adodb.recordset")
    'rs.open sql, db  
 
    '추가 2013-01-18
    Set rs = RecordsetFromWS(provWSDL, sql)

    '변수에 값들을 저장
    idx = rs("b_num")
    ref = rs("ref")
    re_level = rs("re_level")
    re_step = rs("re_step")
    b_filename=rs("b_filename1")
    b_filesize=rs("b_filesize")
 
    keyfield = request("keyfield")	
    key = request("key")

    sqlcon = "select b_content from upboard  where table_idx = '" & table_idx & "' and no_idx = "&no_idx&"   and ref = "&ref&" and re_step = "&re_step&" "
    b_content = ContentViewLoadWS(provWSDL, sqlcon)

    'Response.Write(sqlcon)
    'Response.End()

    'b_content = replace(rs("b_content"), chr(13) & chr(10), "<br>")
    b_content = replace(b_content, chr(10), "<br/>")
    b_content = replace(b_content, chr(13), "<br/>")
    b_content = replace(b_content, "&lt;", "<")
    b_content = replace(b_content, "&gt;", ">")
    b_content = replace(b_content, "&amp;", "&")
    b_content = replace(b_content, "&apos;", "'")
    b_content = replace(b_content, "&quot;", """")
%>  


<body leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
	 <!--#include file="../inc/top.asp"//-->
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30">&nbsp;</td>
          <td width="650"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9"><!--#include file="bbs_title.asp"//--></font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>

            <%
	            If left(rs("f_confirm"),1) = "Y" Then
		            f_confirm = "등록"
	            Else
		            f_confirm = "등록안함"	
	            End if
            %>

              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">


            <% if table_idx = "1002" then %>
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="35" align="center" bgcolor="#F3F3F3"><b>FAQ 등록</td>
                      <td bgcolor="#FFFFFF" style="padding:5px;"><%=f_confirm%> </td>
                    </tr>
            <% end if %>

            <%

	            If rs("notice_idx") = "Y" Then
		            b_notice = "공지체크"
	            Else
		            b_notice = "공지안함"	
	            End if

            %>

            <% if b_notice = "공지체크" then %>
					<tr bgcolor="#F3F3F3"> 
                      <td width="100" height="35" align="center" bgcolor="#F3F3F3"><b>공지사항</td>
                      <td bgcolor="#FFFFFF" style="padding:5px;"><%=b_notice%> </td>
                    </tr>
            <% end if %>

            <% if table_idx = "1015" then %>
					<tr bgcolor="#F3F3F3"> 
                      <td width="10%" height="35" align="center" bgcolor="#F3F3F3"><b>분류</td>
                      <td bgcolor="#FFFFFF" style="padding:5px;"><%=rs("f_part")%> </td>
                    </tr>
            <% end if %>

					<tr bgcolor="#F3F3F3"> 
                      <td width="10%" height="35" align="center" bgcolor="#F3F3F3"><b>아이디</td>
                      <td bgcolor="#FFFFFF" style="padding:5px;"><%=rs("user_id")%> </td>
                    </tr>                    

					<tr bgcolor="#F3F3F3"> 
                      <td width="10%" height="35" align="center" bgcolor="#F3F3F3"><b>작성자</td>
                      <td bgcolor="#FFFFFF" style="padding:5px;"><%=rs("b_name")%> </td>
                    </tr>                    
                    <tr bgcolor="#F3F3F3"> 
                      <td width="10%" height="35" align="center" bgcolor="#F3F3F3"><b>제목</td>
                      <td bgcolor="#FFFFFF" style="padding:5px;"><%=rs("b_title")%></td>
                    </tr>
                    <tr bgcolor="#FFFFFF"> 
                      <td width="10%" height="200" align="center" bgcolor="#F3F3F3"><b>내용</td>
                      <td valign="top" style="padding:5px;">
					  <%'=replace(rs("b_content"), chr(13), "<br/>")%>
                      <%=b_content %>
                      </td>
                    </tr>

                    <tr bgcolor="#FFFFFF">
                      <td width="10%" height="35" align="center" bgcolor="#F3F3F3"><b>첨부파일</td>
                      <td height="35" style="padding:5px;">

                      <% If rs("b_filename1") ="" then %>파일이 없습니다.<%else%>
					      <% if right(rs("b_filename1"),3) = "hwp" or right(rs("b_filename1"),3) = "HWP" then %>
						      <a href="hwp.asp?b_filename=<%=rs("b_filename1")%>"><%=rs("b_filename1") %>
					      <% else %>							  
						      <a href="../../pdsfile1/<%=rs("b_filename1")%>"><%=rs("b_filename1")%>
					      <% end if %>	  
					  <% End if %>

					  </td>
                    </tr>

                <% if table_idx = "1008"  Or table_idx = "1043" then %>
                    <tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>확인여부</td>

                <form name="searchForm" action="edit02.asp?table_idx=<%=table_idx%>&no_idx=<%=no_idx%>&n_id=<%=n_id%>&ref=<%=ref%>&re_step=<%=re_step%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>" method="post" onSubmit="return searchSendit();">
                      <td bgcolor="#FFFFFF" style="padding:5px;">

                        <% if rs("d_confirm") = "2" then %>

                            상담완료

                        <% else %>

					        <input type="radio" name="d_confirm" value="0" <%if rs("d_confirm") = "0" then%>checked<%end if%>>상담신청
						    <input type="radio" name="d_confirm" value="1" <%if rs("d_confirm") = "1" then%>checked<%end if%>>확인중
                            &nbsp;&nbsp;
						    <input type="submit" name="Submit" value="수정">

                        <% end if %>

					  </td>
                </form>

                    </tr>
                <% end if %>

                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>

        <script type="text/javascript">
        <!--
            //원래들의 글넘버, ref 등의 정보 폼을 넘겨주는 스크립트
            function send_re()
            {
                document.re.submit();
            }

            function send_comment()
            {
	            if (document.mycomment.name.value=="")
	            {
		            alert("이름을 입력해 주세요")
		            document.mycomment.name.focus();
		            return ;
	            }
	            if (document.mycomment.comment.value=="")
	            {
		            alert("내용을 입력해 주세요.")
		            document.mycomment.comment.focus()
		            return ;
	            }
	            document.mycomment.submit();
            } 
        //-->
        </script>

        <script type="text/javascript">
		<!--
            function delChk(url)
            {
                if (confirm('정말 삭제하시겠습니까?'))
                {
                    location.href = url;
                }
		    }
		//-->
        </script>
              <tr> 
                <td align="right"><table width="10%" border="0" cellspacing="2" cellpadding="0">
                    <tr>
					<% if Session("compassion_level") = "1" then %>
					<td><a href="list.asp?table_idx=<%=table_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>"><img src="../img/btn16.gif" width="42" height="20" border="0"></a></td>
					<%else%>
					
					<% if table_idx = "1002" or table_idx = "1006" or table_idx = "1009" then %>
					  <td><a href="javascript:send_re()"><img src="../img/btn18.gif" width="44" height="20" border="0"></td>
					<% end if %>
					
					<% if table_idx = "1008"  Or table_idx = "1043" then %>
                      <td></td>
					<%else%>
                      <td><a href="edit.asp?table_idx=<%=table_idx%>&no_idx=<%=no_idx%>&n_id=<%=n_id%>&ref=<%=ref%>&re_step=<%=re_step%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"><img src="../img/btn06.gif" width="44" height="20" border="0"></td>
					<%end if%>
					
                      <td><a href="javascript:delChk('delete_ok.asp?table_idx=<%=table_idx%>&no_idx=<%=no_idx%>&notice_idx=<%=rs("notice_idx")%>&n_id=<%=n_id%>&ref=<%=ref%>&b_pwd=<%=rs("b_pwd")%>&re_step=<%=re_step%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>');"><img src="../img/btn02.gif" width="44" height="20" border="0"></td>
                      <td><a href="list.asp?table_idx=<%=table_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>"><img src="../img/btn16.gif" width="42" height="20" border="0"></a></td>
					<%end if%>
                    </tr>
                  </table></td>
              </tr>

        <form name="re" method="post" action="write.asp">
          <input type="hidden" name="t_id" value="<%=table_idx%>">
		  <input type="hidden" name="id" value="<%=no_idx%>">
		  <input type="hidden" name="n_id" value="<%=n_id%>">
		  <input type="hidden" name="left_menu" value="<%=left_menu%>">
		  <input type="hidden" name="page" value="<%=page%>">

          <input type="hidden" name="idx" value="<%=idx%>">
          <input type="hidden" name="re_step" value="<%=re_step%>">
          <input type="hidden" name="re_level" value="<%=re_level%>">
          <input type="hidden" name="ref" value="<%=ref%>">
		  <input type="hidden" name="keyfield" value="<%=keyfield%>">
          <input type="hidden" name="key" value="<%=key%>">
         </form> 

              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>

			

<!-- ########################################## 기타 등등 시작 ##########################################3 -->

<table width="600" border="0" cellspacing="0" cellpadding="0" align="center">

<form name="mycomment" action="comment_ok.asp" method="POST">
<input type="hidden" name="table_idx" value="<%=table_idx%>">
<input type="hidden" name="no_idx" value="<%=rs("no_idx")%>">
<input type="hidden" name="notice_id" value="<%=n_id%>">
<input type="hidden" name="page" value="<%=page%>">
<input type="hidden" name="ref" value="<%=ref%>">
<input type="hidden" name="re_step" value="<%=re_step%>">

<% if Session("compassion_level") = "1" then %><%else%>
        <tr bgcolor="#EBEBEB"> 
          <td height="5" colspan="4"></td>
        </tr>
        <tr> 
          <td height="5" colspan="4"></td>
        </tr>

        <tr> 
          <td height="19" width="39"> 
            <div align="left">이름</div>
          </td>
          <td height="19" width="3"> 
            <div align="left"></div>
          </td>
          <td height="19" width="472"> 
            <div align="left"> 
              <input type="text" name="name" size="15" style="border-width:1px; border-color:rgb(215,215,215); border-style:solid;" maxlength="20">
            </div>
          </td>
        </tr>

        <tr> 
          <td height="19" width="39"> 
            <div align="left">내용</div>
          </td>
          <td height="19" width="3"> 
            <div align="left"></div>
          </td>
          <td height="19" width="472"> 
            <div align="left"> 
              <textarea name="comment" cols="60" rows="15" style="border-width:1px; border-color:rgb(215,215,215); border-style:solid;"></textarea>
            </div>
          </td>
          <td height="19" rowspan="3"> 
            <div align="right"> <a href="javascript:send_comment();" onfocus="this.blur()"><img src="../img/btn11.gif" border="0"></a> </div>
          </td>
        </tr>
<% end if %>
        <tr> 
          <td colspan="4" height="5"></td>
        </tr>
        <tr bgcolor="#EBEBEB"> 
          <td colspan="4" height="1"></td>
        </tr>

</form>

	  </table>
      <table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
<%
    'upboard_comment즉, 한줄 답변 테이블에서 이 글과 해당되는 한줄 답변을 가져와서 테이블에 뿌려주는 부분이다.
 
    query = "select * from upboard_comment where table_idx = '"&table_idx&"' and notice_idx = '"&n_id&"'  and ref = '"&ref&"' and re_step = '"&re_step&"' "
    'set grs = server.CreateObject("ADODB.Recordset")
    'grs.Open query, db, 1

    '추가 2013-01-18
    Set grs = RecordsetFromWS(provWSDL, query)
 
    if grs.EOF or grs.BOF then
    else
 
    i=1
    do until grs.EOF
%> 
		<tr> 
          <td height="35" width="80"> 
            <div align="left"><b><%=grs("writer")%></b></div>
          </td>
          <td height="19" width="5"> 
            <div align="left"></div>
          </td>

        <script type="text/javascript">
	    <!--
		    function delChk<%=i%>(url){
			    if (confirm('정말 삭제하시겠습니까?')){
			    location.href =url;}
		    }
	    //-->
        </script>

          <td height="19" width="469"><%=replace(grs("comment"), chr(13), "<br>")%> (<%=grs("writeday")%>)</td>
          <td height="10" width="46"> 
		  <% if Session("compassion_level") = "1" then %><%else%>
            <div align="right">		
			<a href="javascript:delChk<%=i%>('comment_delete.asp?table_idx=<%=table_idx%>&n_id=<%=n_id%>&ref=<%=grs("ref")%>&seqno=<%=grs("seqno")%>&writeday=<%=grs("writeday")%>&writer=<%=grs("writer")%>&id=<%=no_idx%>&re_step=<%=re_step%>&left_menu=<%=left_menu%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>');"> [삭제]</a></div>
		  <%end if %>
          </td>
        </tr>
        <tr> 
          <td colspan="4" height="1" bgcolor="#EBEBEB"></td>
        </tr>
    <%
    grs.MoveNext 
    i=i+1
    loop
    %>
<%end if%>
      </table>

	  <br/>

      <table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td bgcolor="#EBEBEB" height="20"></td>
        </tr>
      </table>
      <table width="600" border="0" cellspacing="0" cellpadding="0" align="center">

      <% 
        SQL2="SELECT * FROM upboard WHERE no_idx = (SELECT MAX(no_idx) FROM upboard WHERE no_idx < '"&rs("no_idx")&"' and table_idx = '"&table_idx&"' and notice_idx = '"&n_id&"') and table_idx = '"&table_idx&"' and notice_idx = '"&n_id&"'"
        'set rs2=db.execute(SQL2)

        '추가 2013-01-18
        Set rs2 = RecordsetFromWS(provWSDL, SQL2)
      %>
        <tr> 
          <td height="22" width="50">이전 글</td>
          <td height="22" width="30" align="center">
		    <% if rs2.eof then %>
			<% else %>
	          <%=rs2("no_idx")%></a>
	        <% end if %>
		  </td>
          <td width="300">
		    <% if rs2.eof then %>
			<% else %>
		      <a href="content.asp?t_id=<%=rs2("table_idx")%>&id=<%=rs2("no_idx")%>&n_id=<%=rs2("notice_idx")%>&left_menu=<%=left_menu%>&page=<%=page%>&ref=<%=rs2("ref")%>&re_step=<%=rs2("re_step")%>" onfocus="this.blur()">  <%=rs2("b_title")%></a>
	        <% end if %>		  
          </td>
          <td width="90" align="center">
		    <% if rs2.eof then %>
        	<% else %>
				  <%=rs2("b_name")%>
        	<% end if %>
		  </td>
          <td width="90" align="center">
		    <% if rs2.eof then %>
			<% else %>
	          <%=left(rs2("b_writeday"),10)%></a>
	        <% end if %>		  
		  </td>
          <td width="40" align="center">
		    <% if rs2.eof then %>
			<% else %>
	          <%=rs2("b_readnum")%></a>
	        <% end if %>		  
		  </td>
        </tr>
        <tr bgcolor="#EBEBEB"> 
          <td colspan="6" height="1"></td>
        </tr>

      <% 
        SQL3="SELECT * FROM upboard WHERE no_idx = (SELECT MIN(no_idx) FROM upboard WHERE no_idx > '"&rs("no_idx")&"' and table_idx = '"&table_idx&"' and notice_idx = '"&n_id&"') and table_idx = '"&table_idx&"' and notice_idx = '"&n_id&"'"
        'set rs3=db.execute(SQL3)

        '추가 2013-01-18
        Set rs3 = RecordsetFromWS(provWSDL, SQL3)
      %>
        <tr> 
          <td height="22" width="50">다음 글</td>
          <td height="22" width="30" align="center">
		    <% if rs3.eof then %>
			<% else %>
	          <%=rs3("no_idx")%></a>
	        <% end if %>
		  </td>
          <td width="300">
		    <% if rs3.eof then %>
			<% else %>
		       <a href="content.asp?t_id=<%=rs3("table_idx")%>&id=<%=rs3("no_idx")%>&n_id=<%=rs3("notice_idx")%>&left_menu=<%=left_menu%>&page=<%=page%>&ref=<%=rs3("ref")%>&re_step=<%=rs3("re_step")%>" onfocus="this.blur()">  <%=rs3("b_title")%></a>
	        <% end if %>		  
          </td>
          <td width="90" align="center">
		    <% if rs3.eof then %>
        	<% else %>
				    <%=rs3("b_name")%>
        	<% end if %>
		  </td>
          <td width="90" align="center">
		    <% if rs3.eof then %>
			<% else %>
	          <%=left(rs3("b_writeday"),10)%></a>
	        <% end if %>		  
		  </td>
          <td width="40" align="center">
		    <% if rs3.eof then %>
			<% else %>
	          <%=rs3("b_readnum")%></a>
	        <% end if %>		  
		  </td>
        </tr>
        <tr bgcolor="#EBEBEB"> 
          <td colspan="6" height="1"></td>
        </tr>
        <tr> 
          <td colspan="6" height="1" bgcolor="#EBEBEB"></td>
        </tr>
      </table>

<br/><br/><br/><br/><br/><br/>

<!-- ########################################## 기타 등등 끝 ##########################################3 -->


			
			</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>