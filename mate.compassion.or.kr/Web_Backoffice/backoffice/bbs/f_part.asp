<html>
<head>
    <title>관리자화면</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="../index.css">
</head>
<body>
    <select name="f_part">
        <option value="일대일결연" <%If f_part = "일대일결연" then%>selected<%End if%>>일대일결연</option>
        <option value="선물금" <%If f_part = "선물금" then%>selected<%End if%>>선물금</option>
        <option value="양육보완프로그램" <%If f_part = "양육보완프로그램" then%>selected<%End if%>>양육보완프로그램</option>
        <option value="어린이방문" <%If f_part = "어린이방문" then%>selected<%End if%>>어린이방문</option>
        <option value="자원봉사" <%If f_part = "자원봉사" then%>selected<%End if%>>자원봉사</option>
        <option value="컴패션" <%If f_part = "컴패션" then%>selected<%End if%>>컴패션</option>
        <option value="편지" <%If f_part = "편지" then%>selected<%End if%>>편지</option>
        <option value="현지관련" <%If f_part = "현지관련" then%>selected<%End if%>>현지관련</option>
        <option value="후원금" <%If f_part = "후원금" then%>selected<%End if%>>후원금</option>
    </select>
</body>
</html>
