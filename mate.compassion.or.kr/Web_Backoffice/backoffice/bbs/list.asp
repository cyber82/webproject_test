﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

	keyfield = request("keyfield")	
	key = request("key")	
	m_code = request("m_code")

	table_idx = request("table_idx")
%>


<% '추가 2013-01-18 %>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'주석처리 2013-01-10
'<!-- #include virtual="/Web_Backoffice/lib/dbcon.asp"-->

'추가 2013-01-10
'웹서비스 연동
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")


'Dim obj, objXML
'Dim sStr

'soap 정의
'Set oSoap = Server.CreateObject("MSSOAP.SoapClient30")
'oSoap.ClientProperty("ServerHTTPRequest") = True
'Call oSoap.MSSoapInit(provWSDL)

'Set objXML = Server.CreateObject("MSXML.DomDocument")
'Set xmlDoc = Server.CreateObject("MSXML2.DOMDocument.4.0")
'Set xmlDom = Server.CreateObject("Microsoft.XMLDOM")
'xmlDom.async = false

'리턴값이 문자열이면 일반 변수
'리턴값이 데이터셋이면 Set 변수
'함수선언 매개변수도 포함 예: objSoap.GetDate2("param1", "param2", "param3")
'sStr = oSoap.BoardTest()

'sStr = objSoap.BoardList(table_idx, "Y", "", "", "")
'xmlDom.LoadXml(sStr)

'Set rs = Server.CreateObject("ADODB.Recordset")
'Set rs = RecordsetFromWS(provWSDL)
'sStr = RecordsetFromWS(provWSDL)
'Response.Write(sStr)
'Response.End()

'Set rs6 = server.createobject("ADODB.Recordset")
'Set rs = server.CreateObject("ADODB.RecordSet")
'Set grs1 = server.CreateObject("ADODB.Recordset")
'Set grs2 = server.CreateObject("ADODB.Recordset")
%>

<body leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30">&nbsp;</td>
          <td width="850"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9"><!--#include file="bbs_title.asp"//--></font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                      <td width="6%" height="25" align="center">번호</td>
					  <td align="center">제목</td>
                      <td width="10%" align="center">작성자</td>
                      <td width="10%" align="center">등록일</td>
                      <td width="13%" align="center">글번호</td>
                    </tr>

            <!--------- 공지사항 목록 구하기 시작 -------->
            <%
            '수정 2013-01-17
            sql6 = " select " 
            sql6 = sql6 & " table_idx, no_idx, ref, re_step, notice_idx, b_title, b_name, b_filename1, b_writeday "
            sql6 = sql6 & " from upboard where table_idx = '"&table_idx&"' and notice_idx = 'Y' order by ref desc, re_step asc "
            'set rs6=server.createobject("ADODB.Recordset")
            'rs6.Open sql6, db, 3

            '추가 2013-01-17
            Set rs6 = RecordsetFromWS(provWSDL, sql6)
            %>

            <% if rs6.BOF or rs6.EOF then ' 만일 레코드가 없다면 %>
	                <!--<tr> 
		                <td height="22" colspan="5" align="center"><b>글이 없습니다!!!</b></td>
	                </tr>-->
            <% else %>
            <% While NOT rs6.eof  %>

					<tr bgcolor="#FFFFFF"> 
                      <td height="25" align="center"><b>공지</b></td>
					  
                      <% 'if table_idx = "1002" then %>
                      <!--<td height="25" align="center">&nbsp;</td>-->
					  <% 'end if %>
                      
                      <td style="padding:5px;">
                        <a href="content.asp?t_id=<%=table_idx%>&id=<%=rs6("no_idx")%>&ref=<%=rs6("ref")%>&re_step=<%=rs6("re_step")%>&page=<%=page%>&n_id=<%=rs6("notice_idx")%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"> 
                            <% if rs6("b_title") = "" then %>
                                <font color="blue">없는 글입니다.</font>
                            <% else %>
                                <%=rs6("b_title") %>
                            <% end if %>
                        </a>

                    <!--------- 메모글 목록 구하기 시작 ---------->   
			        <%  
                       '수정 2013-01-17
                        query = " select "
                        query = query & " table_idx, seqno, notice_idx, writer, pwd, writeday, ref, re_step, user_ip, user_id "
                        query = query & " from upboard_comment where table_idx = '" & table_idx & "' and notice_idx = '" & rs6("notice_idx") & "'  and ref = '" & rs6("ref") & "' and re_step = '" & rs6("re_step") & "' "
				        'set grs = server.CreateObject("ADODB.Recordset")
                        'grs.Open query, db, 1
                
                        '추가 2013-01-17
                        Set grs1 = RecordsetFromWS(provWSDL, query)
 
				        if grs1.EOF or grs1.BOF then
				        else
				        k=1
				        do until grs1.EOF

				        grs1.MoveNext 
				        k=k+1
				        loop
				        k=k-1
			        %>

 			        <font size="2" color="gray" face="돋움">(<%=k %>)</font> 

			        <% end if %>
		            <!--------- 메모글 목록 구하기 끝 ---------->  

			          <% if datediff("h",rs6("b_writeday"),now()) < 48 then %>
                        <img src="bbs_new.gif">
                      <% end if %>
					  
					  </td>
                      <td align="center"><%=rs6("b_name") %></td>
                      <td align="center"><%=left(rs6("b_writeday"),10) %></td>
                      <td align="center">
					      <% if rs6("b_filename1") = "" then '파일이 없을경우 %>
					      <% else '파일이 있을경우 %>
					        <img src="../img/icon04.gif" width="14" height="14">
					      <% end if %>
					  </td>
                    </tr>
            <% 
	            rs6.Movenext
            %>
            <% wend %> 
            <%
	            rs6.Close
	            set rs6 = nothing                        
            %> 
            <% end if %>

            <% 'Response.End() %>
            <!------- 공지사항 목록 구하기 끝 ----------> 




            <!------- 게시판 목록 구하기 시작 ---------->
            <%
            '수정 2013-01-17  
            sql = " select "
            sql = sql & " table_idx, no_idx, ref, re_step, notice_idx, b_title, b_name, b_filename1, b_writeday, f_part, s_confirm, board_idx " 
            sql = sql & " from upboard "

            if key = "" then
	            sql = sql & " where table_idx = '"&table_idx&"' and notice_idx = 'N' order by ref desc, re_step asc "
            else
	            sql = sql & " where table_idx = '"&table_idx&"' and notice_idx = 'N' and "&keyfield&" like '%"&key&"%' order by ref desc, re_step asc "
            end if


            '수정 2013-01-17
            'set rs = server.CreateObject("adodb.recordset")
            'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
  
            '추가 2013-01-17
            'Response.Write(sql)
            Set rs = RecordsetFromWS(provWSDL, sql)

            '추가 2013-01-17
            sqlCnt = " select "
            sqlCnt = sqlCnt & " count(table_idx) as totalCnt " 
            sqlCnt = sqlCnt & " from upboard "
                  
            if key = "" then
	            sqlCnt = sqlCnt & " where table_idx = '"&table_idx&"' and notice_idx = 'N' "
            else
	            sqlCnt = sqlCnt & " where table_idx = '"&table_idx&"' and notice_idx = 'N' and "&keyfield&" like '%"&key&"%' "
            end if 

            Set rsCnt = RecordsetFromWS(provWSDL, sqlCnt)       
            %>
					
				<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
					<tr> 
					  <td height="22" colspan="5" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
				<% else '데이터가 있다면
                    
                    rs.PageSize = 10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

					totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
                    ''totalpage = rsCnt("totalCnt")

					rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
					endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
					''totalpage = Int(rsCnt("totalCnt") / rs.PageSize) + 1

                    if endpage > totalpage then
						endpage = totalpage
					end If
                    
					if request("page")="" then
						npage = 1
					else
						npage = CInt(request("page"))
					end if
						
					trd = rs.RecordCount
                    ''trd = rsCnt("totalCnt")
					j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)

                    'Response.Write("totalpage : " & totalpage & "<br/>")
                    'Response.Write("startpage : " & startpage & "<br/>")
                    'Response.Write("endpage : " & endpage & "<br/>")
                    'Response.Write("rs.PageSize : " & rs.PageSize & "<br/>")
                    'Response.Write("npage : " & npage & "<br/>")
                    'Response.Write("trd : " & trd & "<br/>")
				%>
				<%
				    i = 1
					do until rs.EOF or i > rs.PageSize
				%>
                    <tr bgcolor="#FFFFFF"> 
                      <td height="25" align="center">
                        <% if rs("re_step") = "0" then %>
                            <%=j %>
                        <% end if %>
                      </td>
					   
					  <% 'if table_idx = "102" then %>
					  <!--<td width="13%" align="center"><%'=rs("m_code") %></td>-->
					  <% 'end if %>

                      <td style="padding:5px;">
                        <% if rs("f_part") <> "" then %>
                            <font color="green">[<%=rs("f_part") %>]</font>
                        <% end if %> 

                        <a href="content.asp?t_id=<%=table_idx%>&id=<%=rs("no_idx")%>&ref=<%=rs("ref")%>&re_step=<%=rs("re_step")%>&page=<%=page%>&n_id=<%=rs("notice_idx")%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
                            <% if rs("b_title") = "" then %>
                                <font color="blue">없는 글입니다.</font>
                            <% else %>
                                <% if rs("s_confirm") = "del" then %>
                                    <font color="red"><%=rs("b_name") %>님의 요청에 의하여 삭제된 글입니다. (답글 달지 마세요)</font>
                                <% else %> 
                                    <%=rs("b_title") %>
                                <% end if %>
                            <% end if %>
                        </a>

                        <% 'Response.End() %>

			        <!--------- 메모글 목록 구하기 시작 ---------->   
			        <% 
                        '수정 2013-01-17
                        query = " select "
                        query = query & " table_idx, seqno, notice_idx, writer, pwd, writeday, ref, re_step, user_ip, user_id "
                        query = query & " from upboard_comment where table_idx = '"&table_idx&"' and notice_idx = '"&rs("notice_idx")&"' and ref = '"&rs("ref")&"' and re_step = '"&rs("re_step")&"' "
				        'set grs = server.CreateObject("ADODB.Recordset")
 				        'grs.Open query, db, 1

                        '추가 2013-01-17
                        'Response.Write(query)
                        Set grs2 = RecordsetFromWS(provWSDL, query)
				        
                        if grs2.EOF or grs2.BOF then
				        else
				        k=1
				        do until grs2.EOF

				        grs2.MoveNext 
				        k=k+1
				        loop
				        k=k-1
			        %>

 			            <% if table_idx <> "1008"  Or table_idx <> "1043" then %>
                            <font size="2" color="gray" face="돋움">(<%=k %>)</font>
                        <% end if %> 

			        <% end if %>
		            <!--------- 메모글 목록 구하기 끝 ---------->   

					  </td>
                      <td align="center"><%=rs("b_name") %></td>
                      <td align="center"><%=left(rs("b_writeday"),10) %></td>

                    <script type="text/javascript">
                    <!--
                        function d_sendit<%=i%>()
                        {
                            document.d_bbs<%=i%>.submit();
                        }
                    //-->
                    </script>

                    <form name="d_bbs<%=i%>" method="post" action="j_dong_ok.asp?table_idx=<%=table_idx%>&board_idx=<%=rs("board_idx")%>&ref=<%=rs("ref")%>&re_step=<%=rs("re_step")%>&page=<%=page%>&notice_idx=<%=rs("notice_idx")%>&keyfield=<%=keyfield%>&key=<%=key%>">
					  <td align="center">
					    <input type="text" name="no_idx" size="5" value="<%=rs("no_idx")%>"> 
						<a href="javascript:d_sendit<%=i%>();" onfocus="this.blur()"><b>[확인]</b></a>
					  </td>
                    </form>

                    </tr>

            <%
                'Response.Write("j : " & j & "<br/>")
                'Response.Write("i : " & i & "<br/>")

				j=j-1
				rs.MoveNext ' 다음 레코드로 이동한다.
			    i=i+1
				loop '레코드의 끝까지 loop를 돈다.
			%> 

            <% 
         	    rs.Close
	            Set rs = nothing 

                end if
            %>
            <!------- 게시판 목록 구하기 시작 ---------->

                  </table></td>
              </tr>

              <tr> 
                <td>&nbsp;</td>
              </tr>

              <tr>
                <td align="right">
                    <% if Session("compassion_level") = "1" then %>
                    <% else %> 
                        <% if table_idx <> "1008" Or table_idx <> "1043" then %>
                            <a href="write.asp?t_id=<%=table_idx%>&page=<%=page%>" onfocus="this.blur()"><img src="../img/btn17.gif" width="52" height="20" border="0"></a>
                        <% end if %>
                    <% end if %>
                </td>
              </tr>
              <tr> 
                <td align="center">
					<% if CInt(startpage) <> CInt(1) then %> 
				         <a href="list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [이전]
                         </a>
			        <% end if %> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then %>
				         [<%=page %>]
		             <% else %>
					     <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"> 
                         <%=i %> 
                         </a>
		             <% end if %>
				   <% next %>

				   <% if cint(endpage) <> cint(totalpage) then %>
			             <a href="list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [다음]
                         </a>
			       <% end if %>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
	
              <tr> 
                <td align="center"><table width="30%" border="0" cellspacing="2" cellpadding="0">
					<form name="searchForm" action="list.asp?table_idx=<%=table_idx%>" method="post" onSubmit="return searchSendit();">	
                    <tr> 
                      <td><select name="keyfield" style="height:19px;">
                                <option value="b_title">제목</option>
                                <option value="b_name">작성자</option>
                                <option value="b_content">내용</option>
                              </select></td>
                      <td><input name="key" type="text" class="text"></td>
                      <td><input type="submit" name="Submit" value="찾기"></td>
                    </tr>
					</form>
                  </table></td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
        
            <% 'if table_idx = "102" then %>
              <!--<tr> 
                <td align="center">문의항목 선택 : 
						<input type="radio" name="m_code" value="컨텐츠 가이드" onclick="location.href='list.asp?m_code=컨텐츠 가이드&table_idx=<%'=table_idx%>'">컨텐츠 가이드 
						<input type="radio" name="m_code" value="엠큐브플레이어" onclick="location.href='list.asp?m_code=엠큐브플레이어&table_idx=<%'=table_idx%>'">엠큐브플레이어
						<input type="radio" name="m_code" value="포인트결제" onclick="location.href='list.asp?m_code=포인트결제&table_idx=<%'=table_idx%>'">포인트결제 
						<input type="radio" name="m_code" value="회원가입" onclick="location.href='list.asp?m_code=회원가입&table_idx=<%'=table_idx%>'">회원가입 
						<input type="radio" name="m_code" value="사이트이용" onclick="location.href='list.asp?m_code=사이트이용&table_idx=<%'=table_idx%>'">사이트이용
				</td>
              </tr>-->
            <% 'end if %>

            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
