﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/freeASPUpload.asp" -->

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

  code=request("code")
  selView=request("selView")
  viewYN=request("viewYN")

  if request("idx") = "" then
    idx = 0
  else
    idx=request("idx")
  end if

  keyfield = request("keyfield")	
  key = request("key")

  link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?code=" & code & "&page=" & page & "&startpage=" & startpage & "';</script>"
%>

<%
if viewYN = "Y" then

    Dim sql_save2
    sql_save2 = ""

    sql_save2 = " update mate_ssb set ssb_view='" & selView & "' where idx = "& idx

    '글수정
    'db.Execute(sql_save2)
    inresult = BoardWriteWS(provWSDL, sql_save2)

    response.write "<script type='text/javascript' charset='euc-kr'>alert('수정되었습니다.');</script>"
    response.write link

else

    sql = "select top 1 idx from mate_ssb where idx = "& idx
    'set rs = db.Execute(sql)
    set rs = RecordsetFromWS(provWSDL, sql)

    if rs.BOF or rs.EOF then ' 만일 레코드가 없다면
        main_idx = 0
    else '데이터가 있다면
        main_idx = rs(0)
    end if


    if CInt(main_idx) > 0 then
    '테이블 mate_ssb
%>

<%
        DIM Data_Folder

        '저장 폴더명
        Data_Folder = Server.MapPath("/") & "\image\mate\temp\" & code & "\"

        SET Upload = NEW FreeASPUpload
        Upload.Save(Data_Folder)

        ks = Upload.UploadedFiles.keys
        if (UBound(ks) <> -1) then
            for each fileKey in Upload.UploadedFiles.keys
                SaveFileName = Upload.UploadedFiles(fileKey).FileName
            next
        end if

        old_filename = Upload.Form("old_filename")
        b_filename = Upload.Form("b_filename")
        b_title = Upload.Form("b_title")
        b_caption = Upload.Form("b_caption")
        b_link = Upload.Form("b_link")
        b_view = Upload.Form("b_view")
        b_ip = Request.ServerVariables("REMOTE_ADDR")
        
        '============= 기존파일 삭제 ============== 수정 2012-12-04
        If old_filename <> SaveFileName AND old_filename <> "" AND SaveFileName <> "" Then
        	strDelPath1 = Data_Folder & old_filename
        
        	Set fso = CreateObject("Scripting.FileSystemObject")
        	'파일 존재시 파일 삭제
        	If fso.FileExists(strDelPath1) Then
        		fso.DeleteFile strDelPath1
        	End If
        	Set fso = Nothing
        End if
        '============= 기존파일 삭제 ==============

        '============= 기존파일 같은파일일때 처리 ============== 추가 2012-12-04
        If old_filename = SaveFileName AND old_filename <> "" AND SaveFileName <> "" Then
            strDelPath1 = Data_Folder & old_filename
        
            Set fso = CreateObject("Scripting.FileSystemObject")
            '파일 존재시 문구 출력
            If fso.FileExists(strDelPath1) Then
                link2 = "location.href='list.asp?code=" & code & "&page=" & page & "&startpage=" & startpage & "';"
                response.write "<script type='text/javascript' charset='euc-kr'>"
                response.write "if(confirm('파일이 존재합니다. 덮어쓰겠습니까?')){"
                response.write "} else {"
                response.write link2          
                response.write "}"
                response.write "</script>"
            End If
            Set fso = Nothing
        End If
        '============= 기존파일 처리 ==============

        'response.Write(old_filename)
        'response.end()
%>

<%
        Dim sql_save
        sql_save = ""

        sql_save = "update mate_ssb set "
        
        if SaveFileName <> "" then
        sql_save = sql_save & " img_filename = N'" & SaveFileName & "', "
        end if

        '수정 2013-07-01
        if b_title <> "" or code = "month_mate" then
        sql_save = sql_save & " title = N'" & b_title & "', "
        end if
        
        sql_save = sql_save & " caption = N'" & b_caption & "', "
        sql_save = sql_save & " link = N'" & b_link & "', "
        sql_save = sql_save & " ssb_view = N'" & b_view & "'"
        sql_save = sql_save & " where idx = " & idx

        '글수정
        'db.Execute(sql_save)
        inresult = BoardWriteWS(provWSDL, sql_save)

        response.write "<script type='text/javascript' charset='euc-kr'>alert('수정되었습니다.');</script>"
        response.write link
    else 

        response.write "<script type='text/javascript' charset='euc-kr'>alert('글이 선택되지 않았습니다.');</script>"
        response.write link
    end if

end if

%>