﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/freeASPUpload.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

  code = request("code")

  link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?code=" & code & "&page=" & page & "&startpage=" & startpage & "';</script>"
%>

<%
DIM Data_Folder
DIM old_filename

'저장 폴더명
Data_Folder = Server.MapPath("/") & "\image\mate\temp\" & code & "\"


fname = request("fname")

If instrRev(fname, "\") > 0 Then

    fstart = instrRev(fname, "\") + 1
    fmax = Len(fname)

    old_filename = Mid(fname, fstart, fmax)

End If

'============= 기존파일 처리 ==============
If old_filename <> "" Then
    strDelPath1 = Data_Folder & old_filename
        
    Set fso = CreateObject("Scripting.FileSystemObject")
    '파일 존재시 문구 출력
    If fso.FileExists(strDelPath1) Then
        link2 = "location.href='list.asp?code=" & code & "&page=" & page & "&startpage=" & startpage & "';"
        response.write "<script type='text/javascript' charset='euc-kr'>"
        response.write "if(confirm('파일이 존재합니다. 덮어쓰겠습니까?')){"
        response.write "} else {"
        response.write link2          
        response.write "}"
        response.write "</script>"
    End If
    Set fso = Nothing
End If
'============= 기존파일 처리 ==============

'response.Write(old_filename + "<br/>")
'response.end()
%>

<%
    '파일 등록
    SET Upload = NEW FreeASPUpload
    Upload.Save(Data_Folder)

    ks = Upload.UploadedFiles.keys
    if (UBound(ks) <> -1) then
        for each fileKey in Upload.UploadedFiles.keys
            SaveFileName = Upload.UploadedFiles(fileKey).FileName
        next
    end if

    b_filename = Upload.Form("b_filename")
    b_title = Upload.Form("b_title")
    b_caption = Upload.Form("b_caption")
    b_link = Upload.Form("b_link")
    b_view = Upload.Form("b_view")
    b_ip = Request.ServerVariables("REMOTE_ADDR")
%>

<%
if code = "" then    
    response.write "<script type='text/javascript' charset='euc-kr'>alert('잘못된 경로입니다.');</script>"
    response.write link
end if

if SaveFileName <> "" AND b_title <> "" then    
%>

<%
    sql = "select count(*) from mate_ssb where code='" & code & "' and img_filename='" & SaveFileName & "' and title='" & b_title & "' and del_flag='N'"
    'set rs = db.Execute(sql)
    set rs = RecordsetFromWS(provWSDL, sql)

    if rs.BOF or rs.EOF then ' 만일 레코드가 없다면
        ssb_idx = 0
    else '데이터가 있다면
        ssb_idx = rs(0)
    end if


    if CInt(ssb_idx) > 0 then

        '============= 기존파일 삭제 ==============
        'If SaveFileName <> "" Then
        '	strDelPath1 = Data_Folder & SaveFileName
        '
        '	Set fso = CreateObject("Scripting.FileSystemObject")
        '	'파일 존재시 파일 삭제
        '	If fso.FileExists(strDelPath1) Then
        '		fso.DeleteFile strDelPath1
        '	End If
        '	Set fso = Nothing
        'End if
        '============= 기존파일 삭제 ==============

        response.write "<script type='text/javascript' charset='euc-kr'>alert('현재 글이 있어 등록되지 않습니다.');</script>"
        response.write link
    else 
%>

<%
        Dim sql_save

        sql_save = " INSERT INTO mate_ssb "
        sql_save = sql_save & " ([code], [img_filename], [title], [caption], [link], [ssb_view], [ip_address], [user_id], [user_pw]) "
        sql_save = sql_save & " VALUES "
        sql_save = sql_save & " (N'" & code & "', N'" & SaveFileName & "', N'" & b_title & "', N'" & b_caption & "', N'" & b_link & "', N'" & b_view & "', N'" & b_ip & "', N'compassion', N'') "

        'response.Write sql_save
        'response.end

        '글등록
        'db.Execute(sql_save)
        inresult = BoardWriteWS(provWSDL, sql_save)
    
        response.write "<script type='text/javascript' charset='euc-kr'>alert('등록되었습니다.');</script>"
        response.write link
    end if
%>

<%
else
    response.write "<script type='text/javascript' charset='euc-kr'>alert('내용이 담겨 있지 않습니다. 다시 확인하시기 바랍니다.');</script>"
    response.write link
end if
%>

</html>