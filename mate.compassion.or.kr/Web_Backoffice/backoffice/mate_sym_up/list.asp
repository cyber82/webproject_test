﻿
<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>


<%

    '전체 문자 치환
    Function eregi_replace(pattern, replace, text)
        Dim eregObj
        Set eregObj = New RegExp
        eregObj.Pattern = pattern '//패턴설정
        eregObj.IgnoreCase = True '//대소문자 구분 여부
        eregObj.Global = True '//전체 문서에서 검색
        eregi_replace = eregObj.Replace(text, replace) '//Replace String
    End Function

    '정규식 문자 치환
	Function strip_tags(str)
		Dim content
        content = str
        content = eregi_replace("[<][a-z|A-Z|/](.|\n)*?[>]", "", content)
        content = eregi_replace("&nbsp;", "", content)
		strip_tags = content
	End Function

  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if 
  
  code = request("code")

  keyfield = request("keyfield")	
  key = request("key")
%>

<body leftmargin="0" topmargin="0">

<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9"><!--#include file="title.asp"//--></font></strong></td>
                    </tr>
                </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                      <td width="5%" height="25" align="center">번호</td>
                      <td width="200px" align="center">파일명</td>
                      <td width="200px" align="center">이미지</td>
                      <td width="10%" align="center">제목</td>
                      <td width="15%" align="center">캡션</td>
                      <td width="10%" align="center">보기여부</td>
                      <td width="10%" align="center">IP</td>
                      <td width="10%" align="center">등록일</td>
                      <td width="10%" align="center">버튼</td>
                    </tr>

<%
  sql = "SELECT ms.idx, ms.code, mc.code_name, ms.img_filename, ms.title, ms.caption, ms.link, "
  sql = sql & " ms.ssb_view, ms.del_flag, ms.ip_address, ms.[user_id], ms.[user_pw], ms.reg_date "
  sql = sql & " FROM mate_ssb ms JOIN mate_config mc ON mc.code = ms.code "
  sql = sql & " WHERE ms.code='" & code & "' AND ms.del_flag='N' "
  sql = sql & " ORDER BY ms.ssb_view DESC, ms.reg_date DESC "

 'set rs = server.CreateObject("adodb.recordset")
  'rs.PageSize=10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.
  'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
  set rs = RecordsetFromWS(provWSDL, sql)

%>					
					
					<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면%>
					<tr> 
					  <td height="22" colspan="10" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
					<% else '데이터가 있다면
   
						totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
						rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
						endpage = startpage + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
						if endpage > rs.PageCount then
							endpage = rs.PageCount
						end If


						if request("page")="" then
							npage=1
						else
							npage=cint(request("page"))
						end if
						
						trd=rs.recordcount
						j=trd-Cint(rs.pagesize)*(Cint(npage)-1)
					%>
					<%
						i=1
						do until rs.EOF or i>rs.pagesize

                        filename = rs("img_filename")
                        imageSrc = "/image/mate/temp/" & code & "/" & filename

                        'if code = "month_mate" then 
                        '    imageSrc = "/image/temp/" & thumbnail  
                        'end if

                        'if code = "bundal_defy" then
                        '    imageSrc = "/image/mate/temp/" & code & "/" & thumbnail  
                        'end if

                        'if code = "benefit_letter" then
                        '    imageSrc = "/image/mate/" & thumbnail  
                        'end if
				    %>

                    
                    <tr bgcolor="#FFFFFF"> 
                      <td height="25" align="center"><%=j %></td>
                      <td align="center">
                        <%=filename %>
                        <input type="hidden" name="file_<%=rs("idx") %>" id="file_<%=rs("idx") %>" value="<%=filename %>" />
                      </td>
                      <td align="center"><img src="<%=imageSrc %>" border="0" width="150" height="100" /></td>
                      <td align="center"><%=rs("title") %></td>
                      <td align="center"><%=rs("caption") %></td>
                      <td align="center">
                        <% if rs("ssb_view") = "Y" then %>Y<% else %>N<% end if %>
                        <select id="sel_view<%=j%>" name="sel_view<%=j%>" style="display:none;height:20px;">
                            <option value="Y" <% if rs("ssb_view") = "Y" then %>selected<% end if %>>Y</option>
                            <option value="N" <% if rs("ssb_view") = "N" then %>selected<% end if %>>N</option>
                        </select>
                        <script type="text/javascript">
                        <!--
                            function selectView<%=j%>()
                            {
                                var selV = document.getElementById("sel_view"+<%=j%>);
                                var sel1 = document.getElementById("btn_or1"+<%=j%>);
                                var sel2 = document.getElementById("btn_or2"+<%=j%>);
                                var sel3 = document.getElementById("btn_or3"+<%=j%>);

                                selV.style.display = "block";
                                sel1.style.display = "none";
                                sel2.style.display = "block";
                                sel3.style.display = "block";
                            }

                            function selectNone<%=j%>()
                            {
                                var selV = document.getElementById("sel_view"+<%=j%>);
                                var sel1 = document.getElementById("btn_or1"+<%=j%>);
                                var sel2 = document.getElementById("btn_or2"+<%=j%>);
                                var sel3 = document.getElementById("btn_or3"+<%=j%>);

                                selV.style.display = "none";
                                sel1.style.display = "block";
                                sel2.style.display = "none";
                                sel3.style.display = "none";
                            }

                            function subjectOrder<%=j%>()
                            {
                                var sel_view = document.getElementById("sel_view"+<%=j%>);
                                var idx = document.getElementById("idx");
                                var selView = document.getElementById("selView");
                                var title = document.getElementById("title");
                                var viewYN = document.getElementById("viewYN");

                                var f = document.form1;

                                idx.value = '<%=rs("idx")%>';
                                selView.value = sel_view.value;
                                viewYN.value = "Y";

                                f.action = 'edit_proc.asp?code=<%=code%>&page=<%=page%>&startpage=<%=startpage%>&idx=<%=rs("idx") %>';
                                f.method = "post";
                                f.submit();
                            }
                        //-->
                        </script>
                        <input type="button" name="btn_or1<%=j%>" id="btn_or1<%=j%>" value="수정" onclick="javascript:selectView<%=j%>();" style="display:block;" />
                        <input type="button" name="btn_or2<%=j%>" id="btn_or2<%=j%>" value="취소" onclick="javascript:selectNone<%=j%>();" style="display:none;" />
                        <input type="button" name="btn_or3<%=j%>" id="btn_or3<%=j%>" value="적용" onclick="javascript:subjectOrder<%=j%>();" style="display:none;"/>
                      </td>
                      <td align="center"><%=rs("ip_address") %></td>
                      <td align="center"><%=rs("reg_date") %></td>                  
                      
                      <td align="center">
                          <a href="edit.asp?code=<%=code%>&page=<%=page%>&startpage=<%=startpage%>&idx=<%=rs("idx")%>" onfocus="this.blur()"><img src="../img/btn06.gif" width="44" height="20" border="0"></a>
                          <a href="javascript:delChk_<%=rs("idx") %>();"><img src="../img/btn02.gif" width="44" height="20" border="0"></a>
                            <script type="text/javascript">
                            <!--
                                function delChk_<%=rs("idx") %>() {
                                    if (confirm('정말 삭제하시겠습니까?')) {
                                        var f = document.form1;
                                        var link = 'delete_proc.asp?code=<%=code%>&page=<%=page%>&startpage=<%=startpage%>&idx=<%=rs("idx")%>';
                                        var filename = document.getElementById('file_' + '<%=rs("idx") %>');
                                        var b_filename = document.getElementById('b_filename');

                                        b_filename.value = filename.value;

                                        //location.href = link;
                                        f.action = link;
                                        f.method = "post";
                                        f.target = "_self";
                                        f.submit();

                                    } else {
                                        alert("취소되었습니다.");
                                    }
                                }
                            //-->
                            </script>
                      </td>
                    </tr>
					<%
					   j=j-1
					   rs.MoveNext ' 다음 레코드로 이동한다.
			           i=i+1
					   loop '레코드의 끝까지 loop를 돈다.
					%> 
					<% end if %>                      
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="right"><a href="write.asp?code=<%=code%>&page=<%=page%>&startpage=<%=startpage%>" onfocus="this.blur()"><img src="../img/btn17.gif" width="52" height="20" border="0"></a></td>
              </tr>
              <tr> 
                <td align="center">
					<% if cint(startpage)<>cint(1) then%> 
				         <a href="list.asp?page=<%=startpage %>&startpage=<%=startpage-10%>" onfocus="this.blur()">
			             [이전]</a>
			        <%end if%> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then%>
				         <% if page > 0 then %>
                         [<%=page%>]
                         <% end if %>
		             <%else%>
                         <% if page > 0 then %>
					     <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>" onfocus="this.blur()"> <%=i%> </a>
                         <% end if %>
		             <%end if%>
				   <% next%>

				   <% if cint(endpage)<>cint(rs.PageCount) then%>
			             <a href="list.asp?page=<%=i%>&startpage=<%=startpage+10%>" onfocus="this.blur()">
			             [다음]</a>
			       <% end if%>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>

<input type="hidden" name="idx" id="idx" />
<input type="hidden" name="selView" id="selView" />
<input type="hidden" name="code" id="code" value="<%=code%>" />
<input type="hidden" name="title" id="title" />
<input type="hidden" name="viewYN" id="viewYN" />
<input type="hidden" name="b_filename" id="b_filename" />

</form>
</body>
</html>
