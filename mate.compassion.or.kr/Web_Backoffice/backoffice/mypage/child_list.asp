<!--#include file="../inc/session.asp"-->

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
</head>

<%
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  
%>

<!--#include file="../../lib/dbcon.asp"-->

<%

  keyfield = request("keyfield")	
  key = request("key")	


  if key = "" then
	  sql = "select * from child_info order by idx desc"
  else
	  sql = "select * from child_info where "&keyfield&" like '%"&key&"%' order by idx desc"
  end if

  set rs = server.CreateObject("adodb.recordset")
  rs.PageSize=10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.
  rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
%>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="0">
		<tr>
          <td align="center" >
		  <br><img src="../img/icon01.gif" width="22" height="18" align="absmiddle"> <strong><font color="#3788D9">결연어린이정보</font>
		  <br><br>
		  </td>
        </tr>
      </table>   
	  <table width="700" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100" align="center" bgcolor="#EBEBEB" height="25"><b>후원자번호</td>
          <td width="100" align="center" bgcolor="#EBEBEB"><b>어린이키</td>
          <td width="300" align="center" bgcolor="#EBEBEB"><b>이름</td>
          <td width="100" align="center" bgcolor="#EBEBEB"><b>성별</td>
          <td width="100" align="center" bgcolor="#EBEBEB"><b>생일</td>
        </tr>
      </table> 
 	  <table width="700" border="0" cellspacing="0" cellpadding="0">
		<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면%>
		<tr> 
          <td height="22" colspan="5" align="center"><b>자료가 없습니다!!!</b></td>
        </tr>
		<% else '데이터가 있다면
   
			totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
			rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
			endpage = startpage + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
			if endpage > rs.PageCount then
				endpage = rs.PageCount
			end if


			if request("page")="" then
				npage=1
			else
				npage=cint(request("page"))
			end if

			trd=rs.recordcount
			j=trd-Cint(rs.pagesize)*(Cint(npage)-1)
		%>
		<%
			 i=1
			 do until rs.EOF or i>rs.pagesize
	    %>

		<a href="child_content.asp?idx=<%=rs("idx")%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"><tr style="cursor:hand"> 
          <td width="100" height="22" align="center">
		  <% if Session("compassion_level") = "1" then %>**<%=left(rs("con_id"),3)%><%else%><%=rs("con_id")%><%end if%></td>
		  <td width="100" height="22" align="center">
		  <% if Session("compassion_level") = "1" then %>**<%=left(rs("child_id"),3)%><%else%><%=rs("child_id")%><%end if%></td>
          <td width="300" align="center"> 
			<% if Session("compassion_level") = "1" then %>**<%=left(rs("name"),2)%><%else%><%=rs("name")%><%end if%>
		  </td>
          <td width="100" align="center">
			  <%if rs("gender") = "M" then%>남<%else%>여<%end if%>
		  </td>
          <td width="100" align="center"><%=rs("birthday")%></td>
        </tr>
        <tr> 
          <td colspan="6" height="1" bgcolor="#EBEBEB"></td>
        </tr>
		<%
	 	   j=j-1
		   rs.MoveNext ' 다음 레코드로 이동한다.
	           i=i+1	
		   loop '레코드의 끝까지 loop를 돈다.
		%> 
		<% end if %>
      </table>
      <table width="600" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td colspan="3"><img src="image/b_bottom.gif" width="600" height="4"></td>
        </tr>
        <tr> 
          <td height="10" colspan="3"></td>
        </tr>
<% if Session("compassion_level") = "1" then %><%else%>
        <tr> 
          <td align="center"colspan="3">
		  <% if cint(startpage)<>cint(1) then%> 
             <a href="child_list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
             <img src="image/ppre.gif" width="15" height="15" border="0" align="absmiddle"></a>
          <%end if%>
			<% for i = startpage to endpage step 1 %>
             <% if cint(i) = cint(page) then%>
                 [<%=page%>]
             <%else%>
                 <a href="child_list.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
                  <%=i%> </a>
                 <!--해당되는 페이지로 이동시킨다.-->
             <%end if%>
            <% next%>
		  <% if cint(endpage)<>cint(rs.PageCount) then%>
             <a href="child_list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
             <img src="image/nnext.gif" width="15" height="15" border="0" align="absmiddle"></a>
          <% end if%>
		  </td>
        </tr>

		<tr> 
                <td align="center"colspan="3">
				<br>
				<table width="30%" border="0" cellspacing="2" cellpadding="0">
					<form name="searchForm" action="child_list.asp" method="post" onSubmit="return searchSendit();">	
                    <tr> 
                      <td><select name="keyfield">
                                <option value="con_id">후원자번호</option>
                                <option value="child_id">어린이키</option>
                                <option value="name">이름</option>
                              </select></td>
                      <td><input name="key" type="text" class="text"></td>
                      <td><input type="submit" name="Submit" value="찾기"></td>
                    </tr>
					</form>
                  </table></td>
         </tr>
<%end if%>
      </table>
<% '사용한 개체드릉 모두 반납한다.
 rs.close
 db.Close
  set rs = nothing
  set db = nothing
%>
</body>
</html>
