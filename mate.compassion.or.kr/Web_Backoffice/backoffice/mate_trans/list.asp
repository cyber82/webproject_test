﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css" href="../index.css">
<link type="text/css" href="../inc/js/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../inc/js/ui/demos.css" rel="stylesheet" />
    <style type="text/css">
    .sbox{border:1px solid #bbb;}
    </style>

<script type="text/javascript" src="../inc/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.core.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.datepicker-ko.js"></script>
</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
    if request("page") = "" then
        page = 1
    else
        page=request("page")
    end if
  
    if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
        startpage=1
    else
        startpage = request("startpage")
    end if  

	keyfield = request("keyfield")	
	key = request("key")	


    nowDate = Now()
    nDate = Left(nowDate, 10)

    sYear = Left(nowDate, 4) - 1
    sMonth = Mid(nowDate, 6, 2)
    sDay = Mid(nowDate, 9, 2)
                                
    if request("start_date") = "" then 
        start_date = sYear & "-" & sMonth & "-" & sDay
    else
        start_date = request("start_date")
    end if

    if request("end_date") = "" then 
        end_date = nDate
    else
        end_date = request("end_date")
    end if
%>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<body leftmargin="0" topmargin="0">

<script type="text/javascript" charset='euc-kr'>
<!--
    $(function () {
        $('#start_date').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    $(function () {
        $('#end_date').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    function trColor(sw, idx)
    {
        var id = document.getElementById("dataTr_" + idx);

        if (sw == "1") {
            id.style.backgroundColor = "#ddd";
        }
        else if (sw == "0") {
            id.style.backgroundColor = "#fff";
        }
    }

    //- 숫자만입력
    function OnlyNumber() {
        key = event.keyCode;

        if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
            event.returnValue = true;
        }
        else {
            event.returnValue = false;
        }
    }

//-->
</script>
<form name="searchForm" action="list.asp" method="post">	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                        <td width="22px"><img src="../img/icon01.gif" width="22" height="18"></td>
                        <td><strong><font color="#3788D9"><!--#include file="title.asp"//--></font></strong></td>
                        <td align="right">
                            <a href="excel.asp?keyfield=<%=keyfield%>&key=<%=key%>&start_date=<%=start_date %>&end_date=<%=end_date %>"><strong><font color="#3788D9">엑셀 다운로드</font></strong></a>
                        </td>
                    </tr>
                </table></td>
              </tr>

              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="80px"><font color="#3788D9">등록기간검색</font></td>
                        <td>
                            <input type="text" id="start_date" name="start_date" size="10" maxlength="10" value="<%=start_date %>" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" /> 
                            &nbsp;&nbsp; ~ &nbsp;&nbsp;
                            <input type="text" id="end_date" name="end_date" size="10" maxlength="10" value="<%=end_date %>" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" />
                            &nbsp;&nbsp;
                            <input type="submit" name="Submit1" value="검색">
                        </td>
                    </tr>
                </table></td>
              </tr>

              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                      <td width="8%" height="25" align="center">번호</td>
					  <td width="15%" align="center">후원자아이디</td>
                      <td width="15%" align="center">후원자명</td>
                      <td width="15%" align="center">웹아이디</td>
                      <td width="10%" align="center">번역종류</td>
                      <td width="15%" align="center">서명인</td>
                      <td width="7%" align="center">기수</td>
                      <td width="15%" align="center">등록일</td>
                    </tr>


            <!------- 게시판 목록 구하기 시작 ----------> 
            <%
                sql = " select "
                sql = sql & " idx, user_name, userid, real_name, mate, conid, flag, " 
                sql = sql & " convert(varchar, writeday, 120) as writeday "
                sql = sql & " from MATE_TRANS "

                if key = "" then
	                sql = sql & " where writeday between '"& start_date &" 00:00:00' and '"& end_date &" 23:59:59' order by writeday desc, flag desc"
                else
	                sql = sql & " where writeday between '"& start_date &" 00:00:00' and '"& end_date &" 23:59:59' and "&keyfield&" like '%"&key&"%' order by writeday desc, flag desc"
                end if

                'set rs = server.CreateObject("adodb.recordset")
                'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
 
                'response.Write sql
                'response.End  
                Set rs = RecordsetFromWS(provWSDL, sql)

                'totalcount
                sqlCnt = " select "
                sqlCnt = sqlCnt & " count(idx) as totalCnt " 
                sqlCnt = sqlCnt & " from MATE_TRANS "
                  
                if key = "" then
	                sqlCnt = sqlCnt & " where writeday between '"& start_date &" 00:00:00' and '"& end_date &" 23:59:59'"
                else
	                sqlCnt = sqlCnt & " where writeday between '"& start_date &" 00:00:00' and '"& end_date &" 23:59:59' and "&keyfield&" like '%"&key&"%' "
                end if

                'response.Write sqlCnt
                'response.End                  
                Set rsCnt = RecordsetFromWS2(provWSDL, sqlCnt)
            %>					
					
				<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
					<tr> 
					  <td height="22" colspan="6" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
				<% else '데이터가 있다면
                    
                    rs.PageSize = 20 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

					totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
                    ''totalpage = rsCnt("totalCnt")

					rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
					endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
					''totalpage = Int(rsCnt("totalCnt") / rs.PageSize) + 1

                    if endpage > totalpage then
						endpage = totalpage
					end If
                    
					if request("page")="" then
						npage = 1
					else
						npage = CInt(request("page"))
					end if
						
					trd = rs.RecordCount
                    ''trd = rsCnt("totalCnt")
					j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)

                    'Response.Write("totalpage : " & totalpage & "<br/>")
                    'Response.Write("startpage : " & startpage & "<br/>")
                    'Response.Write("endpage : " & endpage & "<br/>")
                    'Response.Write("rs.PageSize : " & rs.PageSize & "<br/>")
                    'Response.Write("npage : " & npage & "<br/>")
                    'Response.Write("trd : " & trd & "<br/>")
				%>
				<%
				    i = 1
					do until rs.EOF or i > rs.PageSize
				%>
                    <tr id="dataTr_<%=rs("idx") %>" bgcolor="#FFFFFF" onmouseover='javascript:trColor(1, "<%=rs("idx") %>");' onmouseout='javascript:trColor(0, "<%=rs("idx") %>");'> 
                      <td height="25" align="center"><%=j%></td>
                      <td align="center"><%=rs("conid") %></td>
					  <td align="center"><%=rs("user_name") %></td>
                      <td align="center"><%=rs("userid") %></td>
                      <td align="center"><%=rs("mate") %></td>
                      <td align="center"><%=rs("real_name") %></td>
                      <td align="center"><%=rs("flag") %></td>
                      <!-- left 사용시 변함 --> <% '=left(rs("writeday"),10)%>
                      <td align="center"><%=rs("writeday") %></td>
                    </tr>

            <%
                'Response.Write("j : " & j & "<br/>")
                'Response.Write("i : " & i & "<br/>")

                j=j-1
				rs.MoveNext ' 다음 레코드로 이동한다.
			    i=i+1
				loop '레코드의 끝까지 loop를 돈다.
			%> 
            <% 
         	    rs.Close
	            Set rs = nothing 

                end if
            %>               
            <!------- 게시판 목록 구하기 끝 ---------->  
                                    
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <!--<tr>
                <td align="right"><a href="write.asp?page=<%'=page%>" onfocus="this.blur()"><img src="../img/btn17.gif" width="52" height="20" border="0"></a></td>
              </tr>-->
              <tr> 
                <td align="center">
					<% if cint(startpage)<>cint(1) then%> 
				         <a href="list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>&start_date=<%=start_date %>&end_date=<%=end_date %>" onfocus="this.blur()">
			             [이전]</a>
			        <%end if%> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then%>
				         [<%=page%>]
		             <%else%>
					     <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>&start_date=<%=start_date %>&end_date=<%=end_date %>" onfocus="this.blur()"> <%=i%> </a>
		             <%end if%>
				   <% next%>

				   <% if cint(endpage)<>cint(totalpage) then%>
			             <a href="list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>&start_date=<%=start_date %>&end_date=<%=end_date %>" onfocus="this.blur()">
			             [다음]</a>
			       <% end if%>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
			  
              <tr> 
                <td align="center"><table width="30%" border="0" cellspacing="2" cellpadding="0">
                    <tr> 
                      <td>
                        <select name="keyfield" style="height:19px;">
                            <option value="user_name">후원자명</option>
                            <option value="conid">후원자아이디</option>
                            <option value="userid">웹아이디</option>
                            <option value="mate">번역종류</option>
                            <option value="real_name">서명인</option>
                            <option value="flag">기수</option>
                        </select>
                      </td>
                      <td><input name="key" type="text" class="text"></td>
                      <td><input type="submit" name="Submit2" value="찾기"></td>
                    </tr>
                 </table></td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</form>

</body>
</html>
