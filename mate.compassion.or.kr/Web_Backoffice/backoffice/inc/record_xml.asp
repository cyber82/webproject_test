﻿<%

'웹서비스 링크 주소 krds01.krpc.ci.org
'provWSDL = "http://10.181.111.10/WWW5Service_penta/AdminService.asmx?WSDL"
session("serviceUrl") = "http://krds01.krpc.ci.org/WWW5Service_penta/AdminService.asmx?WSDL"
'session("serviceUrl") = "http://10.181.11.10/WWW5Service_penta/AdminService.asmx?WSDL"



'웹서비스 글등록
Function BoardWriteWS(sWSAddr, sSql)
    
    Dim sStr

    'soap 정의
    Set objSoap = Server.CreateObject("MSSOAP.SoapClient30")
    objSoap.ClientProperty("ServerHTTPRequest") = true
    
    '웹서비스 링크 주소
    objSoap.MSSoapInit(sWSAddr)

    'Set xmlDom = Server.CreateObject("Microsoft.XMLDOM")
    'xmlDom.async = false

    '리턴값이 문자열이면 일반 변수
    '리턴값이 데이터셋이면 Set 변수
    '함수선언 매개변수도 포함 예: objSoap.GetDate2("param1", "param2", "param3")
    
    sStr = objSoap.BoardWriteString(sSql)
    'xmlDom.Loadxml(sStr)

    'If xmlDom.parseError.errorCode <> 0 Then
        'Response.write "handle the error: "& xmlDom.parseError.reason & ":" & xmlDom.parseError.errorCode
        'Response.end
    'End If

    BoardWriteWS = sStr

End Function



' 웹서비스 연동 - 리스트 목록
Function RecordsetFromWS(sWSAddr, sSql)

    'soap 정의
    Set objSoap = Server.CreateObject("MSSOAP.SoapClient30")
    objSoap.ClientProperty("ServerHTTPRequest") = true
    
    '웹서비스 링크 주소
    objSoap.MSSoapInit(sWSAddr)

    Set xmlDom = Server.CreateObject("Microsoft.XMLDOM")
    xmlDom.async = false

    '리턴값이 문자열이면 일반 변수
    '리턴값이 데이터셋이면 Set 변수
    '함수선언 매개변수도 포함 예: objSoap.GetDate2("param1", "param2", "param3")
    'WSXML = objSoap.BoardList(table_idx, "Y", "", "", "")
    'WSXML = objSoap.BoardCommentList(table_idx, "Y", "", "")
    WSXML = objSoap.BoardListXmlString(sSql)

    xmlDom.Loadxml(WSXML)

    If xmlDom.parseError.errorCode <> 0 Then
        Response.write "handle the error: "& xmlDom.parseError.reason & ":" & xmlDom.parseError.errorCode
        Response.end
    End If

    'XML 형식 예제
    'testXml = "<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    'testXml = testXml & "<s:Schema id='RowsetSchema'>"
    'testXml = testXml & "<s:ElementType name='row' content='eltOnly'>"
    'testXml = testXml & "<s:AttributeType name='shares' rs:number='1' />"
    'testXml = testXml & "<s:AttributeType name='symbol' rs:number='2' />"
    'testXml = testXml & "<s:AttributeType name='price' rs:number='3' />"
    'testXml = testXml & "<s:extends type='rs:rowbase'/>"
    'testXml = testXml & "</s:ElementType>"
    'testXml = testXml & "</s:Schema>"
    'testXml = testXml & "<rs:data>"
    'testXml = testXml & "<z:row shares='100' symbol='MSFT' price='$70.00'  />"
    'testXml = testXml & "<z:row shares='100' symbol='AAPL' price='$107.00'  />"
    'testXml = testXml & "<z:row shares='100' symbol='DELL' price='$50.00'  />"
    'testXml = testXml & "</rs:data>"
    'testXml = testXml & "</xml>"

    iForChk = 0

    'Response.Write(xmlDom.documentElement.childNodes.length)
    'Response.End()


    if xmlDom.documentElement.childNodes.length > 0 then

        'XML 형식에 맞게 생성
        sXml = "<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
        sXml = sXml & "<s:Schema id='RowsetSchema'>"
        sXml = sXml & "<s:ElementType name='row' content='eltOnly'>"

        for each elem in xmlDom.documentElement.childNodes

            sFieldValues = ""

            for iCnt=0 to elem.childNodes.length - 1

                '필드 셋팅
                if iForChk = 0 then
                    sXml = sXml & "<s:AttributeType name='" & elem.childNodes.item(iCnt).nodeName & "' rs:number='" & iCnt + 1 & "' />"
                end if
                '필드 값 셋팅
                sFieldValues = sFieldValues & elem.childNodes.item(iCnt).nodeName & "=" & "'" & elem.childNodes.item(iCnt).text & "' "
        
            next

            if iForChk = 0 then
                    sXml = sXml & "<s:extends type='rs:rowbase'/>"
                    sXml = sXml & "</s:ElementType>"
                    sXml = sXml & "</s:Schema>"
                    sXml = sXml & "<rs:data>"
            end if
            'response.Write "test" & sFieldValues
            sXml = sXml & "<z:row " & sFieldValues & "  />"

            iForChk = iForChk + 1
        next

        sXml = sXml & "</rs:data>"
        sXml = sXml & "</xml>"
    else

        'XML 형식에 맞게 생성
        sXml = "<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
        sXml = sXml & "<s:Schema id='RowsetSchema'>"
        sXml = sXml & "<s:ElementType name='row' content='eltOnly'>"    
        
        sXml = sXml & "<s:AttributeType name='' rs:number='1' />"
        sXml = sXml & "<s:extends type='rs:rowbase'/>"
        sXml = sXml & "</s:ElementType>"
        sXml = sXml & "</s:Schema>"
        sXml = sXml & "<rs:data>"

        'response.Write "test" & sFieldValues
        'sXml = sXml & "<z:row />"
        
        sXml = sXml & "</rs:data>"
        sXml = sXml & "</xml>"

    end if 

    'XML -> Recordset
    'convert xml string to ado recordset
    Set oStream = Server.CreateObject("ADODB.Stream")
    oStream.Open
    'oStream.WriteText testXml   'Give the XML string to the ADO Stream
    oStream.WriteText sXml   'Give the XML string to the ADO Stream
    oStream.Position = 0    'Set the stream position to the start
        
    Set oRecordset = Server.CreateObject("ADODB.Recordset")
    oRecordset.Open oStream    'Open a recordset from the stream
        
    oStream.Close
    Set oStream = Nothing
        
    Set RecordsetFromWS = oRecordset  'Return the recordset
    Set oRecordset = Nothing

End Function



' 웹서비스 연동 - 리스트 목록 (특수문자 변환하지 않고 일반적으로 출력)
Function RecordsetFromWS2(sWSAddr, sSql)

    'soap 정의
    Set objSoap = Server.CreateObject("MSSOAP.SoapClient30")
    objSoap.ClientProperty("ServerHTTPRequest") = true
    
    '웹서비스 링크 주소
    objSoap.MSSoapInit(sWSAddr)

    Set xmlDom = Server.CreateObject("Microsoft.XMLDOM")
    xmlDom.async = false

    '리턴값이 문자열이면 일반 변수
    '리턴값이 데이터셋이면 Set 변수
    '함수선언 매개변수도 포함 예: objSoap.GetDate2("param1", "param2", "param3")
    'WSXML = objSoap.BoardList(table_idx, "Y", "", "", "")
    'WSXML = objSoap.BoardCommentList(table_idx, "Y", "", "")
    WSXML = objSoap.BoardListXmlString2(sSql)

    xmlDom.Loadxml(WSXML)

    If xmlDom.parseError.errorCode <> 0 Then
        Response.write "handle the error: "& xmlDom.parseError.reason & ":" & xmlDom.parseError.errorCode
        Response.end
    End If

    'XML 형식 예제
    'testXml = "<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    'testXml = testXml & "<s:Schema id='RowsetSchema'>"
    'testXml = testXml & "<s:ElementType name='row' content='eltOnly'>"
    'testXml = testXml & "<s:AttributeType name='shares' rs:number='1' />"
    'testXml = testXml & "<s:AttributeType name='symbol' rs:number='2' />"
    'testXml = testXml & "<s:AttributeType name='price' rs:number='3' />"
    'testXml = testXml & "<s:extends type='rs:rowbase'/>"
    'testXml = testXml & "</s:ElementType>"
    'testXml = testXml & "</s:Schema>"
    'testXml = testXml & "<rs:data>"
    'testXml = testXml & "<z:row shares='100' symbol='MSFT' price='$70.00'  />"
    'testXml = testXml & "<z:row shares='100' symbol='AAPL' price='$107.00'  />"
    'testXml = testXml & "<z:row shares='100' symbol='DELL' price='$50.00'  />"
    'testXml = testXml & "</rs:data>"
    'testXml = testXml & "</xml>"

    iForChk = 0

    'Response.Write(xmlDom.documentElement.childNodes.length)
    'Response.End()


    if xmlDom.documentElement.childNodes.length > 0 then

        'XML 형식에 맞게 생성
        sXml = "<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
        sXml = sXml & "<s:Schema id='RowsetSchema'>"
        sXml = sXml & "<s:ElementType name='row' content='eltOnly'>"

        for each elem in xmlDom.documentElement.childNodes

            sFieldValues = ""

            for iCnt=0 to elem.childNodes.length - 1

                '필드 셋팅
                if iForChk = 0 then
                    sXml = sXml & "<s:AttributeType name='" & elem.childNodes.item(iCnt).nodeName & "' rs:number='" & iCnt + 1 & "' />"
                end if
                '필드 값 셋팅
                sFieldValues = sFieldValues & elem.childNodes.item(iCnt).nodeName & "=" & "'" & elem.childNodes.item(iCnt).text & "' "
        
            next

            if iForChk = 0 then
                    sXml = sXml & "<s:extends type='rs:rowbase'/>"
                    sXml = sXml & "</s:ElementType>"
                    sXml = sXml & "</s:Schema>"
                    sXml = sXml & "<rs:data>"
            end if
            'response.Write "test" & sFieldValues
            sXml = sXml & "<z:row " & sFieldValues & "  />"

            iForChk = iForChk + 1
        next

        sXml = sXml & "</rs:data>"
        sXml = sXml & "</xml>"
    else

        'XML 형식에 맞게 생성
        sXml = "<xml xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
        sXml = sXml & "<s:Schema id='RowsetSchema'>"
        sXml = sXml & "<s:ElementType name='row' content='eltOnly'>"    
        
        sXml = sXml & "<s:AttributeType name='' rs:number='1' />"
        sXml = sXml & "<s:extends type='rs:rowbase'/>"
        sXml = sXml & "</s:ElementType>"
        sXml = sXml & "</s:Schema>"
        sXml = sXml & "<rs:data>"

        'response.Write "test" & sFieldValues
        'sXml = sXml & "<z:row />"
        
        sXml = sXml & "</rs:data>"
        sXml = sXml & "</xml>"

    end if 

    'XML -> Recordset
    'convert xml string to ado recordset
    Set oStream = Server.CreateObject("ADODB.Stream")
    oStream.Open
    'oStream.WriteText testXml   'Give the XML string to the ADO Stream
    oStream.WriteText sXml   'Give the XML string to the ADO Stream
    oStream.Position = 0    'Set the stream position to the start
        
    Set oRecordset = Server.CreateObject("ADODB.Recordset")
    oRecordset.Open oStream    'Open a recordset from the stream
        
    oStream.Close
    Set oStream = Nothing
        
    Set RecordsetFromWS2 = oRecordset  'Return the recordset
    Set oRecordset = Nothing

End Function



'웹서비스 수정시에 내용보기
Function BoardLoadContentWS(sWSAddr, sSql)
    
    'soap 정의
    Set objSoap = Server.CreateObject("MSSOAP.SoapClient30")
    objSoap.ClientProperty("ServerHTTPRequest") = true
    
    '웹서비스 링크 주소
    objSoap.MSSoapInit(sWSAddr)

    'Set xmlDom = Server.CreateObject("Microsoft.XMLDOM")
    'xmlDom.async = false

    '리턴값이 문자열이면 일반 변수
    '리턴값이 데이터셋이면 Set 변수
    '함수선언 매개변수도 포함 예: objSoap.GetDate2("param1", "param2", "param3")
    WSXML = objSoap.BoardLoadContent(sSql)
    'xmlDom.Loadxml(WSXML)

    'If xmlDom.parseError.errorCode <> 0 Then
        'Response.write "handle the error: "& xmlDom.parseError.reason & ":" & xmlDom.parseError.errorCode
        'Response.end
    'End If

    BoardLoadContentWS = WSXML
End Function


'웹서비스 내용보기
Function ContentViewLoadWS(sWSAddr, sSql)
    
    'soap 정의
    Set objSoap = Server.CreateObject("MSSOAP.SoapClient30")
    objSoap.ClientProperty("ServerHTTPRequest") = true
    
    '웹서비스 링크 주소
    objSoap.MSSoapInit(sWSAddr)

    'Set xmlDom = Server.CreateObject("Microsoft.XMLDOM")
    'xmlDom.async = false

    '리턴값이 문자열이면 일반 변수
    '리턴값이 데이터셋이면 Set 변수
    '함수선언 매개변수도 포함 예: objSoap.GetDate2("param1", "param2", "param3")
    WSXML = objSoap.ContentViewLoad(sSql)

    ContentViewLoadWS = WSXML
End Function
%>