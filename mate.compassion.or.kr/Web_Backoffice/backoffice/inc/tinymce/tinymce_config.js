function tinymce_config(id) {

    tinyMCE.init({
        // General options
        mode: "exact",
        elements: id,
        theme: "advanced",
        //skin: "o2k7",   
        //skin_variant: "silver", 
        
        //��� �÷�����
        //plugins: "inlinepopups,preview,advlink,table",
        plugins: "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

        // Theme options
        theme_advanced_buttons1: "fontselect,fontsizeselect,|,forecolor,backcolor,|,bold,italic,underline,strikethrough,sub,sup,|,undo,redo,link,unlink,media,|,code,preview",
        theme_advanced_buttons2: "tablecontrols,|,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,outdent,indent",
        theme_advanced_buttons3: "",
        theme_advanced_buttons4: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,
        theme_advanced_resize_horizontal: false,
        theme_advanced_resizing_use_cookie: false,
        theme_advanced_fonts: "��������='malgun gothic';����=����;����ü=����ü;�ü�=�ü�;�ü�ü=�ü�ü;����=����;����ü=����ü;����=����;����ü=����ü;Arial=Arial; Comic Sans MS='Comic Sans MS';Courier New='Courier New';Tahoma=Tahoma;Times New Roman='Times New Roman';Verdana=Verdana",

        // Example content CSS (should be your site CSS)
        content_css: "/Web_Backoffice/backoffice/inc/tinymce/jscripts/tiny_mce/themes/advanced/skins/default/tiny_mce.css",
        //popup_css_add: "/Web_Backoffice/backoffice/inc/tinymce/jscripts/tiny_mce/tiny_mce_popup.css",

        // Extended options
        //���� 2013-08-05
        /*extended_valid_elements: "a[name|href|target|title|onclick], "
                                //+ "img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name|usemap|ismap], "
                                + "img[*], "
                                + "map[*], "
                                + "area[*], "
                                + "hr[class|width|size|noshade], font[face|size|color|style], span[class|align|style]",*/
        extended_valid_elements: "*[*]",

        //������ ���� ����
        width: "600px",
        height: "320px",
        plugin_preview_width: "680px",
        plugin_preview_height: "600px",
        tab_focus: ":prev,:next",
        relative_urls: false,
        theme_advanced_path: false,

        //IE���� �ѱ��Է� ���� �ذ��� ���ؼ�
        //forced_root_block: "p",
        forced_root_block: false,
        language: "ko"
    });
}