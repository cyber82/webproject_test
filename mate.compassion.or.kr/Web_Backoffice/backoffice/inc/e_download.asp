<%
'----------------------------------------------------------------------
' *************** ASP 파일 다운로드 소스 ******************
'  파일 링크 다운로드가 아닌 파일을 직접 읽어 스트림으로 보내는 형식
' ****************************************************
'
' [사용방법]
'  1) download.asp?fileName=test.jpg&filePath=경로
'  2) <a href="download/download.asp?fileName=aaa.jpg">aaa.jpg 파일 다운로드</a>
'---------------------------------------------------------------------- 

Response.Buffer = False
Response.Expires = -1440
'Response.Clear


Dim fileName, filePath, fileDir, phyFilePath, fileDirLen

'다운로드할 파일 이름을 얻어온다.(c:\temp\에 해당 파일이 있으면 다운로드 함)
phyFilePath = ""
fileName = request("fileName")
filePath = request("filePath")

phyFilePath = "E:\" & filePath & fileName

'response.write phyFilePath
'response.end

Response.CacheControl = "public"
Response.ContentType = "application/unknown"
'Response.ContentType = "application/octet-stream" '<-- 파일내용 중 한글이 깨질경우? 이거랑 UTF-8설정
Response.AddHeader "Content-Disposition","attachment;filename=" & Server.URLPathEncode(fileName) '<-- 파일명이깨져  Server.URLPathEncode 해줌.. 
'Response.Charset = "UTF-8" 
Response.AddHeader "Content-Transfer-Encoding","binary"


'fileDir = Split(fileName, "/")
'fileDirLen = UBound(fileDir)

'For i=0 To fileDirLen
'    phyFilePath = phyFilePath & "/" & fileDir(i)
'Next

'fileName = fileDir(fileDirLen)
'phyFilePath = Server.MapPath(".") & filePath & fileName

%>

<%
Dim objFS, objStream, strFile
Set objFS = server.CreateObject("scripting.filesystemobject")

If not objFS.FileExists(phyFilePath) then

%>

<script type="text/javascript" charset='euc-kr'>
    alert("파일이 존재하지 않습니다.");
</script>

<% 

Else

    Set objStream = server.CreateObject("ADODB.Stream")
    objStream.Open
    objStream.Type = 1
    objStream.LoadFromFile phyFilePath

    strFile = objStream.Read
    Response.BinaryWrite strFile

    Set strFile = Nothing
    Set objStream = Nothing
End If

Set objFS = Nothing
%>
