﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  'if request("page") = "" then
    'page = 1
  'else
    'page=request("page")
  'end if
  
  'if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   'startpage=1
  'else
   'startpage = request("startpage")
  'end if  

  If request("key") = "" Then 
  Else
	keyfield = request("keyfield")	
	key = request("key")
  End if 
%>


<%
	'If session("admin_id")="" Then
		
		'response.write "<script>alert('로그인 후 이용해주세요.'); top.location.href='/admin/login.asp';</script>"
	    'response.end

	'End If
	
%>

<%
	Call setXls()
	Sub setXls()
		response.write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>")
	    '수정 ) 파일명 깨짐
        xls_filename = Server.URLEncode("크리스채너티_투데이")
        'xls_filename = "크리스채너티_투데이"
		Response.ContentType  = "application/x-excel"
	    Response.CacheControl  = "public" 
		Response.AddHeader  "Content-Disposition" , "attachment; filename="& xls_filename & ".xls" 	

	
	End Sub
%>

<html>
<head>
<title>관리자화면</title>
<!--link rel="stylesheet" type="text/css" href="../index.css"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
	<table cellpadding="1" cellspacing="1" border="0" width="100%">
		<tr align="center" bgcolor="#3399CC" height="30px" style="font-weight:bold; color:#FFFFFF;">
            <td width="10%" height="25" align="center">번호</td>
			<td width="20%" align="center">웹아이디</td>
            <td width="20%" align="center">후원자명</td>
            <td width="15%" align="center">다운수</td>
            <td width="15%" align="center">기사링크</td>
            <td width="20%" align="center">우편신청</td>          
		</tr>
		<%
            '수정 2013-07-23, 2013-09-05
            sql = " select distinct UserID, UserName, ZipRequest, "
            sql = sql & " 	case when a.DownClickCnt is null then 0 "
            sql = sql & " 		else a.DownClickCnt end as DownClickCnt, "
            sql = sql & " 	case when a.LinkClickCnt is null then 0 "
            sql = sql & " 		else a.LinkClickCnt end as LinkClickCnt "
            sql = sql & " from "
            sql = sql & "       (select es.UserID, es.UserName, a.DownClickCnt, b.LinkClickCnt, "
            sql = sql & "           case when exists(select UserID from tEventSummaryZip where UserID=es.UserID and Flag='R') then '신청' " 
            sql = sql & "               when not exists(select UserID from tEventSummaryZip where UserID=es.UserID and Flag='R') then '-' "
            sql = sql & "               else '-' "
            sql = sql & "           end as ZipRequest "
            sql = sql & "           from tEventSummary es "
            sql = sql & "           left outer join (select COUNT(UserID)as DownClickCnt, UserID, UserName from tEventSummary  "
            sql = sql & "                           where ClickType='pdf' group by UserID, UserName) a on es.UserID = a.UserID and es.UserName = a.UserName "
            sql = sql & "           left outer join (select COUNT(UserID) as LinkClickCnt, UserID, UserName from tEventSummary  "
            sql = sql & "                           where ClickType='link' group by UserID, UserName) b on es.UserID = b.UserID and es.UserName = b.UserName "
            sql = sql & "           group by es.UserID, es.UserName, a.DownClickCnt, b.LinkClickCnt "
            sql = sql & "       UNION "
            sql = sql & "  	    select ez.UserID, ez.UserName, a.DownClickCnt, b.LinkClickCnt, "
            sql = sql & "  		    case when exists(select UserID from tEventSummaryZip where UserID=ez.UserID and Flag='R') then '신청' "
            sql = sql & "  		    	when not exists(select UserID from tEventSummaryZip where UserID=ez.UserID and Flag='R') then '-' "
            sql = sql & "  			    else '-' "
            sql = sql & "  		    end as ZipRequest "
            sql = sql & "  	    from tEventSummaryZip ez "
            sql = sql & "  	    left outer join (select COUNT(UserID) as DownClickCnt, UserID from tEventSummary "
            sql = sql & "  		                where ClickType='pdf' group by UserID, UserName) a on ez.UserID = a.UserID "
            sql = sql & "  	    left outer join (select COUNT(UserID) as LinkClickCnt, UserID from tEventSummary "
            sql = sql & "  		                where ClickType='link' group by UserID, UserName) b on ez.UserID = b.UserID "
            sql = sql & "  	    group by ez.UserID, ez.UserName, a.DownClickCnt, b.LinkClickCnt "
            sql = sql & "  	    ) a "
            sql = sql & " where 1=1 "

            if key = "" then
	            sql = sql & " "
            else
	            sql = sql & " and "&keyfield&" like '%"&key&"%' "
            end if

            sql = sql & " order by a.UserName asc "


            'set rs = server.CreateObject("adodb.recordset")
            'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
 
            'response.Write sql
            'response.End  
            Set rs = RecordsetFromWS(provWSDL, sql)
		%>
		<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면%>
		<tr> 
			<td height="22" colspan="6" align="center"><b>글이 없습니다!!!</b></td>
		</tr>
		<% else '데이터가 있다면
			j = rs.RecordCount

			i=1
			do until rs.EOF
		%>
        <tr bgcolor="#FFFFFF" style="border-bottom:1px solid #ccc;" height="30px"> 
            <td align="center"><%=i%></td>
            <!--<td align="center" style="mso-number-format:\@"><% '=rs("sponsor") %></td>-->
            <td align="center"><%=rs("UserID") %></td>
		    <td align="center"><%=rs("UserName") %></td>
            <td align="center"><%=rs("DownClickCnt") %></td>
            <td align="center"><%=rs("LinkClickCnt") %></td>
            <td align="center"><%=rs("ZipRequest") %></td>
        </tr>
		<%									
			    j = j - 1
			    rs.MoveNext
            	i = i + 1
			Loop 				
		%>
		<%	
			End If 
			rs.close
			Set rs=Nothing 
		%>
	</table>
</body>
</html>