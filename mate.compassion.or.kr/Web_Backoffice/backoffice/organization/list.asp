﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css" />
    <style type="text/css">
    .sbox{border:1px solid #bbb;}
    </style>

<link type="text/css" href="../inc/js/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../inc/js/ui/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../inc/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.core.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.datepicker-ko.js"></script>

</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
    if request("page") = "" then
        page = 1
    else
        page=request("page")
    end if
  
    if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
        startpage=1
    else
        startpage = request("startpage")
    end if  

	keyfield = request("keyfield")	
	key = request("key")	


    nowDate = Now()
    nDate = Left(nowDate, 10)
    
    sYear = Left(nowDate, 4) - 1
    sMonth = Mid(nowDate, 6, 2)
    sDay = Mid(nowDate, 9, 2)
                                
    if request("sdate") = "" then 
        sdate = sYear & "-" & sMonth & "-" & sDay
    else
        sdate = request("sdate")
    end if

    if request("edate") = "" then 
        edate = nDate
    else
        edate = request("edate")
    end if
%>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<body leftmargin="0" topmargin="0">

<script type="text/javascript" charset='euc-kr'>

    $(function () {
        $('#sdate').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    $(function () {
        $('#edate').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    function trColor(sw, idx) {
        var id = document.getElementById("dataTr_" + idx);

        if (sw == "1") {
            id.style.backgroundColor = "#ddd";
        }
        else if (sw == "0") {
            id.style.backgroundColor = "#fff";
        }
    }

    //- 숫자만입력
    function OnlyNumber() {
        key = event.keyCode;

        if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
            event.returnValue = true;
        }
        else {
            event.returnValue = false;
        }
    }

    function writePage(num, wtype) {

        var idx = document.getElementById("idx");

        if (Number(num) > 0) {
            idx.value = num;
        }
        else {
            idx.value = "";
        }
        
        document.searchForm.action = "write.asp";
        document.searchForm.method = "post";
        document.searchForm.target = "_self";

        //if (wtype == "edit") {
        //    if (confirm("수정하시겠습니까?")) {
        //        document.searchForm.submit();
        //    } else {
        //        return;
        //    }
        //}
        //else {
            document.searchForm.submit();
        //}
    }

    function delPage(num) {

        var idx = document.getElementById("idx");

        if (Number(num) > 0) {
            idx.value = num;
        }
        else {
            idx.value = "";
        }

        document.searchForm.action = "del_proc.asp";
        document.searchForm.method = "post";
        document.searchForm.target = "_self";

        if (confirm("삭제하시겠습니까?")) {
            document.searchForm.submit();
        } else {
            return;
        }
    }
//-->
</script>
<form name="searchForm" action="list.asp" method="post">	
<input type="hidden" name="idx" id="idx" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30px">&nbsp;</td>
          <td width="900px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                        <td width="22px"><img src="../img/icon01.gif" width="22" height="18"></td>
                        <td><strong><font color="#3788D9"><!--#include file="title.asp"//--></font></strong></td>
                    </tr>
                </table></td>
              </tr>

              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="80px"><font color="#3788D9">등록기간검색</font></td>
                        <td>
                            <input type="text" name="sdate" id="sdate" size="10" value="<%=sdate %>" style="ime-mode:disabled;cursor:pointer;" class="sbox" onkeydown="javascript:OnlyNumber();" /> 
                            &nbsp;&nbsp; ~ &nbsp;&nbsp;
                            <input type="text" name="edate" id="edate" size="10" value="<%=edate %>" style="ime-mode:disabled;cursor:pointer;" class="sbox" onkeydown="javascript:OnlyNumber();" />
                            &nbsp;&nbsp;
                            <input type="submit" name="Submit1" value="검색">
                        </td>
                        <td align="right" width="80">
			                <input type="button" value="신규등록" onclick='javascript:writePage(0, "write");' style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">
		                </td> 
                    </tr>
                </table></td>
              </tr>

              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                        <td width="8%" height="30px" align="center">번호</td>
                        <td width="10%" align="center">이사회<br />개최일</td>
                        <td width="30%" align="center">제목</td>
                        <td width="15%" align="center">시작일</td>
                        <td width="15%" align="center">종료일</td>
                        <td width="10%" align="center">등록일</td>
                        <td width="12%" align="center">버튼</td>
                    </tr>


            <!------- 게시판 목록 구하기 시작 ----------> 
            <%
                sql = " SELECT "
                sql = sql & " idx, mt_date, title, contents " 
                sql = sql & " , p_top, p_left, p_width, p_height " 
                sql = sql & " , p_status, p_scrollbar, view_flag, del_flag "
                sql = sql & " , convert(varchar(20),[start_date],120) as [start_date] "
                sql = sql & " , convert(varchar(20),end_date,120) as end_date "
                sql = sql & " , convert(varchar(20),reg_date,120) as reg_date "
                sql = sql & " FROM [compassweb4].[dbo].[tOrganization] "
                sql = sql & " WHERE 1=1 AND del_flag=0 "
                sql = sql & " AND reg_date between '"& sdate &" 00:00:00' AND '"& edate &" 23:59:59' "

                if key = "" then
	                sql = sql & " order by reg_date desc "
                else
	                sql = sql & " and "& keyfield &" like '%"& key &"%' order by reg_date desc "
                end if

                'set rs = server.CreateObject("adodb.recordset")
                'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
 
                'response.Write sql
                'response.End  
                Set rs = RecordsetFromWS(provWSDL, sql)

                'totalcount
                sqlCnt = " SELECT count(idx) "
                sqlCnt = sqlCnt & " FROM [compassweb4].[dbo].[tOrganization] "
                sqlCnt = sqlCnt & " WHERE 1=1 AND del_flag=0 "
                sqlCnt = sqlCnt & " AND reg_date between '"& sdate &"' AND '"& edate &"' "

                if key <> "" then
	                sqlCnt = sqlCnt & " and "& keyfield &" like '%"& key &"%' "
                end if

                'response.Write sqlCnt
                'response.End
                Set rsCnt = RecordsetFromWS2(provWSDL, sqlCnt)
            %>					
					
				<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
					<tr> 
					  <td height="22" colspan="7" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
				<% else '데이터가 있다면
                    
                    rs.PageSize = 10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

					totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
                    ''totalpage = rsCnt("totalCnt")

					rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
					endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
					''totalpage = Int(rsCnt("totalCnt") / rs.PageSize) + 1

                    if endpage > totalpage then
						endpage = totalpage
					end If
                    
					if request("page")="" then
						npage = 1
					else
						npage = CInt(request("page"))
					end if
						
					trd = rs.RecordCount
                    ''trd = rsCnt("totalCnt")
					j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)

                    'Response.Write("totalpage : " & totalpage & "<br/>")
                    'Response.Write("startpage : " & startpage & "<br/>")
                    'Response.Write("endpage : " & endpage & "<br/>")
                    'Response.Write("rs.PageSize : " & rs.PageSize & "<br/>")
                    'Response.Write("npage : " & npage & "<br/>")
                    'Response.Write("trd : " & trd & "<br/>")
				%>
				<%
                    host = request.ServerVariables("HTTP_HOST")
                    'response.write host
                    'response.end

				    i = 1
					do until rs.EOF or i > rs.PageSize

                    'addr = "http://www.compassion.or.kr"
                    addr = "http://www.compassionko.org"
				%>
                    <tr id="dataTr_<%=rs("idx") %>" bgcolor="#FFFFFF" onmouseover='javascript:trColor(1, "<%=rs("idx") %>");' onmouseout='javascript:trColor(0, "<%=rs("idx") %>");'> 
                      <td height="25" align="center"><%=j%></td>
					  <td align="center"><%=rs("mt_date") %></td>
                      <td align="center">
                        <a href='javascript:;'>
                        <%=rs("title") %></a>
                      </td>

                      <td align="center"><%=rs("start_date") %></td>
                      <td align="center"><%=rs("end_date") %></td>


                      <!-- left 사용시 변함 --> <% '=left(rs("reg_date"),10)%>
                      <td align="center"><%=left(rs("reg_date"),10) %></td>

                      <td align="center">
                        <a href='javascript:writePage("<%=rs("idx") %>", "edit");' target="_self">[수정]</a>
                        <a href='javascript:delPage("<%=rs("idx") %>");' target="_self">[삭제]</a>
                      </td>
                    </tr>

            <%
                'Response.Write("j : " & j & "<br/>")
                'Response.Write("i : " & i & "<br/>")

                j=j-1
				rs.MoveNext ' 다음 레코드로 이동한다.
			    i=i+1
				loop '레코드의 끝까지 loop를 돈다.
			%> 
            <% 
         	    rs.Close
	            Set rs = nothing 

                end if
            %>               
            <!------- 게시판 목록 구하기 끝 ---------->  
                                    
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <!--<tr>
                <td align="right"><a href="write.asp?page=<%'=page%>" onfocus="this.blur()"><img src="../img/btn17.gif" width="52" height="20" border="0"></a></td>
              </tr>-->
              <tr> 
                <td align="center">
					<% if cint(startpage)<>cint(1) then%> 
				         <a href="list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>&sdate=<%=sdate %>&edate=<%=edate %>" onfocus="this.blur()">
			             [이전]</a>
			        <%end if%> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then%>
				         [<%=page%>]
		             <%else%>
					     <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>&sdate=<%=sdate %>&edate=<%=edate %>" onfocus="this.blur()"> <%=i%> </a>
		             <%end if%>
				   <% next%>

				   <% if cint(endpage)<>cint(totalpage) then%>
			             <a href="list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>&sdate=<%=sdate %>&edate=<%=edate %>" onfocus="this.blur()">
			             [다음]</a>
			       <% end if%>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
			  
              <tr> 
                <td align="center"><table width="30%" border="0" cellspacing="2" cellpadding="0">
                    <tr> 
                      <td>
                        <select name="keyfield" style="height:19px;">
                            <option value="title">제목</option>
                        </select>
                      </td>
                      <td><input name="key" type="text" class="text"></td>
                      <td><input type="submit" name="Submit2" value="찾기"></td>
                    </tr>
                 </table></td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</form>

    <% '추가 2013-06-11  팝업창 %>
    <script type="text/javascript">
    <!--
        function popupLoad(idx, pleft, ptop, pwidth, pheight, pstatus, pscrollbar) {
            //if (getCookie(pname) != 'done') {
                var styn = "";
                var scyn = "";

                if (pstatus == "Y") styn = "yes"; else styn = "no";
                if (pscrollbar == "Y") scyn = "yes"; else scyn = "no";

                //var dom = "http://www.compassionko.org";

                window.open('/popup/popup.aspx?idx=' + idx
                        , ''
                        , 'left=' + pleft + ',top=' + ptop + ',width=' + pwidth + ',height=' + pheight + ',status=' + styn + ',scrollbars=' + scyn
                        + ',toolbar=no,location=no,directories=no,menubar=no,resizable=no');
            //}
        }

        function getCookie(name) {
            var nameOfCookie = name + "=";
            var x = 0;
            while (x <= document.cookie.length) {
                var y = (x + nameOfCookie.length);
                if (document.cookie.substring(x, y) == nameOfCookie) {
                    if ((endOfCookie = document.cookie.indexOf(";", y)) == -1)
                        endOfCookie = document.cookie.length;
                    return unescape(document.cookie.substring(y, endOfCookie));
                }
                x = document.cookie.indexOf(" ", x) + 1;
                if (x == 0) break;
            }
            return "";
        }
    //-->
    </script>

</body>
</html>
