<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 이사회 회의록 관리
'작성일      : 2013-06-12
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/freeaspupload.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")

%>

<%
Dim aCnt

saveDefaultPath = "/popup/20_DirectorMeeting/images/"

'저장 폴더명
Dim strDateDir : strDateDir = getDateDivDir(0, "\")
strDefFolder = Server.MapPath(saveDefaultPath) & "\"
strSaveFolder = strDefFolder & strDateDir

Call MkDirAll(strSaveFolder)	'폴더 생성

uploadTempDir = strSaveFolder
Set Upload = New FreeASPUpload


'***************** 이미지 저장 *****************************
'파일 저장
Upload.Save(uploadTempDir)

    wtype = Upload.Form("wtype") '등록, 수정 구분
    page = Upload.Form("page")
    startpage = Upload.Form("startpage")
    idx = Upload.Form("idx")
    fCnt = Upload.Form("inputFileCnt")


    start_date = Upload.Form("sdate")
    end_date = Upload.Form("edate")
    mt_date = Upload.Form("mt_date")

    title = Upload.Form("title")
    summary = Upload.Form("summary")
    contents = Upload.Form("contents")
	
	p_top = Upload.Form("p_top")
	p_left = Upload.Form("p_left")
	p_width = Upload.Form("p_width")
	p_height = Upload.Form("p_height")
	p_status = Upload.Form("p_status")
	p_scrollbar = Upload.Form("p_scrollbar")

    view_flag = Upload.Form("view_flag")
    'fname1 = Upload.Form("fname1")

if IsNumeric(fCnt) = true then
    if Cint(fCnt) > 0 then
        aCnt = Cint(fCnt) - 1
    else
        aCnt = 0
    end if
end if

'Response.Write fCnt & "<br/>"
'Response.Write aCnt
'Response.End

Dim tempFile(), newFilename(), strFilePath(), fname(), cont_img(), f_idx(), file_chk()
Dim strImg(), strDelPath()

Redim tempFile(Cint(aCnt))
Redim newFilename(Cint(aCnt))
Redim strFilePath(Cint(aCnt))

Redim fname(Cint(aCnt))
Redim cont_img(Cint(aCnt))
Redim f_idx(Cint(aCnt))
Redim file_chk(Cint(aCnt))

Redim strImg(Cint(aCnt))
Redim strDelPath(Cint(aCnt))
'Response.Write aCnt
'Response.End

    '파일
    a = 1
    For i = 0 To Cint(aCnt)
        fname(i) = Upload.Form("fname" & a)	'상단 내용 이미지
        cont_img(i) = Upload.Form("cont_img" & a)	'DB저장된 상단 내용 이미지
        f_idx(i) = Upload.Form("f_idx" & a)	'파일 테이블 번호
        file_chk(i) = Upload.Form("file_chk" & a)	'파일input의 값체크
        
        'Response.Write "fname("& i &"): " & fname(i) & "<br/>"
        'Response.Write "cont_img("& i &"): " & cont_img(i) & "<br/>"
        'Response.Write "f_idx("& i &"): " & f_idx(i) & "<br/>"
        'Response.Write "file_chk("& i &"): " & file_chk(i) & "<br/>"

        a = a + 1
    Next
    'Response.End

    '파일명 추출
    t = 0
    b = 1
    For i = 0 To Cint(aCnt)
        if file_chk(i) <> "Y" then
        else
            tempFile(i) = Upload.UploadedFiles("fname" & b).FileName
            'response.write "tempFile("& i &"): " & tempFile(i) & "<br/><br/>"
        end if
        b = b + 1
    Next 

    'For each fileKey in Upload.UploadedFiles.keys
    '    tempFile(t) = Upload.UploadedFiles("fname" & b).FileName
    '    response.write "tempFile("& t &"): " & tempFile(t) & "<br/><br/>"
    '    b = b + 1
    '    t = t + 1
    'Next
    'Response.End

    'For each fileKey in Upload.UploadedFiles.keys
	    'Select Case fileKey
            'Case "fname1" : tempFile1 = Upload.UploadedFiles("fname1").FileName
	    'End select
    'Next
'***************** 이미지 저장 *****************************

For i = 0 to Cint(aCnt)
    if file_chk(i) = "Y" then
        If tempFile(i) <> "" Then
	        vFilename = tempFile(i)						'파일이름
	        strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	        strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	        set fso = CreateObject("Scripting.FileSystemObject")
	        set file = fso.GetFile(uploadTempDir & "\" & tempFile(i))

	        '새이름 생성
	        newFilename(i) = MakeFileName2(strName) & "."&strExt
	        file.name = newFilename(i)

	        set file = nothing
	        set fso = Nothing

	        '디비에 저장될 이미지 경로 (배너이미지)
	        strFilePath(i) = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename(i)
        End If
    end if
Next

'Response.End

    if contents = "" or contents = "<BR>" then
    
        For i = 0 to Cint(aCnt)

            if strFilePath(i) <> "" then
		        strImg(i) = strFilePath(i)
	        else
		        'strImg(i) = cont_img
	        end If
            
            if i > 0 then
                contents = contents & "<br />"
            end if

            contents = contents & "<img src="""& strImg(i) &""" alt=""""  style=""border:0;"" />"
        Next
    end if
    
    title = replace(title, "'", "''")
    contents = replace(contents, "'", "''")
    mt_date = Trim(mt_date)


    link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"

'테스트 2013-06-12
'For i = 0 to Cint(aCnt)
'    response.write "strFilePath("& i &"): " & strFilePath(i) & "<br/>"
'    response.write "tempFile("& i &"): " & tempFile(i) & "<br/>"
'Next
'response.end

'등록
if wtype = "write" then

    sql = " INSERT INTO [compassweb4].[dbo].[tOrganization] ("
	sql = sql & " [start_date],[end_date],[mt_date],[title],[contents] "
	sql = sql & ", [p_top],[p_left],[p_width],[p_height],[p_status],[p_scrollbar] "
	sql = sql & ", [view_flag], [summary] "
    sql = sql & " ) VALUES ( "
    
    if start_date <> "" then   sql = sql & " N'"&start_date&"' "  else   sql = sql & " null "   end if 
    if end_date <> "" then   sql = sql & " , N'"&end_date&"' "  else   sql = sql & " , null "   end if 
        
    sql = sql & " , N'"& mt_date &"', N'"& title &"', N'"& contents &"' "
    sql = sql & " , N'"& p_top &"', N'"& p_left &"', N'"& p_width &"', N'"& p_height &"', N'"& p_status &"', N'"& p_scrollbar &"' "
    sql = sql & " , N'"& view_flag &"', N'"& summary &"' "
    sql = sql & " ) "

    'response.write sql
    'response.end
    inresult = BoardWriteWS(provWSDL, sql)
    
    if inresult = 10 then
        Dim o_idx
        
        sqlcon = "select convert(varchar, idx) as idx from [compassweb4].[dbo].[tOrganization] order by idx desc "
        o_idx = ContentViewLoadWS(provWSDL, sqlcon)
    
        'Response.Write o_idx
        'Response.End

        For i = 0 to Cint(aCnt)
        
            if strFilePath(i) <> "" then
                fsql = " INSERT INTO [compassweb4].[dbo].[tOrganizationFile] ("
	            fsql = fsql & " [o_idx], [o_filename] "
                fsql = fsql & " ) VALUES ( "
                fsql = fsql & " N'"& o_idx &"', N'"& strFilePath(i) &"' "
                fsql = fsql & " ) "

                'response.write sql
                'response.end

                fileresult = BoardWriteWS(provWSDL, fsql)
            end if 

        Next

        response.write "<script type='text/javascript' charset='euc-kr'> alert('등록되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('등록중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if

'수정
Else

'response.end

Dim arr_fname, strDateDir1, strDateDir2, strSaveFolder

    For i = 0 to Cint(aCnt)
    
        if file_chk(i) = "Y" then
            if strFilePath(i) <> "" then
		        '============= 기존파일 삭제 ==============
		        If cont_img(i) <> "" AND cont_img(i) <> newFilename(i) Then
			        arr_fname = Split(cont_img(i), "/")
			        fname(i) = arr_fname(Ubound(arr_fname))
    
			        strDateDir1 = arr_fname(Ubound(arr_fname)-2) & "\"
			        strDateDir2 = arr_fname(Ubound(arr_fname)-1) & "\"
			        strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			        strDelPath(i) = strSaveFolder & fname(i)
    
			        Set fso = CreateObject("Scripting.FileSystemObject")

			        '파일 존재시 파일 삭제
			        If fso.FileExists(strDelPath(i)) Then
			           fso.DeleteFile strDelPath(i)
			        End If
			        Set fso = Nothing
    
		        End if
		        '============= 기존파일 삭제 ==============
		        strImg(i) = strFilePath(i)
	        else
		        strImg(i) = cont_img(i)
	        end If
        else
            strImg(i) = cont_img(i)
        end if
    Next

'response.end
 	

    sql2 = " UPDATE [compassweb4].[dbo].[tOrganization] SET "

    if start_date <> "" then   sql2 = sql2 & " [start_date]=N'"& start_date &"' "  else   sql2 = sql2 & " [start_date]=null "   end if 
    if end_date <> "" then   sql2 = sql2 & " , [end_date]=N'"& end_date &"' "  else   sql2 = sql2 & " , [end_date]=null "   end if 

    sql2 = sql2 & " , mt_date=N'"& mt_date &"', title=N'"& title &"', contents=N'"& contents &"' "
    sql2 = sql2 & " , p_top=N'"& p_top &"', p_left=N'"& p_left &"', p_width=N'"& p_width &"', p_height=N'"& p_height &"' "
    sql2 = sql2 & " , p_status=N'"& p_status &"', p_scrollbar=N'"& p_scrollbar &"' "
    sql2 = sql2 & " , view_flag=N'"& view_flag &"', summary=N'"& summary &"' "
    sql2 = sql2 & " WHERE idx = '"& idx &"' "

    'response.write sql2
    'response.end
    
    upresult = BoardWriteWS(provWSDL, sql2)

    'response.Write upresult
    'response.end

    if upresult = 10 then

        If CInt(idx) > 0 Then

            For i = 0 to Cint(aCnt)

                if f_idx(i) = "" then
                    f_idx(i) = 0
                end if

                '값이 있는지 없는지 확인
                sqlCnt2 = " SELECT "
                sqlCnt2 = sqlCnt2 & " convert(varchar, case when exists( "
                sqlCnt2 = sqlCnt2 & "   select f_idx from [compassweb4].[dbo].[tOrganizationFile] "
                sqlCnt2 = sqlCnt2 & "   where 1=1 and o_idx = " & idx & " and f_idx = " & f_idx(i)
                sqlCnt2 = sqlCnt2 & "   ) then f_idx "
                sqlCnt2 = sqlCnt2 & "   when not exists( "
                sqlCnt2 = sqlCnt2 & "   select f_idx from [compassweb4].[dbo].[tOrganizationFile] "
                sqlCnt2 = sqlCnt2 & "   where 1=1 and o_idx = " & idx & " and f_idx = " & f_idx(i)
                sqlCnt2 = sqlCnt2 & "   ) then 0 "
                sqlCnt2 = sqlCnt2 & " else 0 "
                sqlCnt2 = sqlCnt2 & " end) as f_idx "
                sqlCnt2 = sqlCnt2 & " FROM [compassweb4].[dbo].[tOrganizationFile] "
                sqlCnt2 = sqlCnt2 & " WHERE 1=1 "

                'Response.End

                chk = ContentViewLoadWS(provWSDL, sqlCnt2)
                
                'Response.Write sqlCnt2
                'Response.Write chk
                'Response.End

                if CInt(chk) = 0 then
                    'insert문
                    if strImg(i) <> "" then
                        fsql2 = " INSERT INTO [compassweb4].[dbo].[tOrganizationFile] ("
	                    fsql2 = fsql2 & " [o_idx], [o_filename] "
                        fsql2 = fsql2 & " ) VALUES ( "
                        fsql2 = fsql2 & " N'"& idx &"', N'"& strImg(i) &"' "
                        fsql2 = fsql2 & " ) "

                        'Response.Write "strImg("& i &"): " & strImg(i) & "<br/>"
                        
                        fresult = BoardWriteWS(provWSDL, fsql2)
                    end if 
                else
                    'update문
                    if strImg(i) <> "" then
                        fsql2 = " UPDATE [compassweb4].[dbo].[tOrganizationFile] SET "
	                    fsql2 = fsql2 & " [o_filename] = N'"& strImg(i) &"' "
                        fsql2 = fsql2 & " WHERE 1=1 "
                        fsql2 = fsql2 & " AND o_idx = N'"& idx &"' "
                        fsql2 = fsql2 & " AND f_idx = N'"& f_idx(i) &"' "
                        
                        fresult = BoardWriteWS(provWSDL, fsql2)
                    end if
                end if
            Next
            'Response.End

        End If

        response.write "<script type='text/javascript' charset='euc-kr'> alert('수정되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('수정중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if

End If

%>