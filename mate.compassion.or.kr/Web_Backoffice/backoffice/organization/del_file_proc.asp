﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
'*******************************************************************************
'페이지 설명 : 이사회 회의록 관리 (파일삭제)
'작성일      : 2013-06-14
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
%>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
if Request("idx") = "" then
    idx = 0
else 
    idx = Request("idx")
end if

page = Request("page")
startpage = Request("startpage")
f_idx = Request("f_idx")
cont_img = Request("cont_img")


'기본 저장위치
saveDefaultPath = "/popup/20_DirectorMeeting/images/"
strDefFolder = Server.MapPath(saveDefaultPath) & "\"
strSaveFolder = strDefFolder & strDateDir

'test
'idx = 0

link = "<script type='text/javascript' charset='euc-kr'>location.href='write.asp?idx=" & idx & "';</script>"

if CInt(idx) > 0 then 

    If CInt(f_idx) > 0 Then

        fsql = " SELECT "
        fsql = fsql & " convert(varchar(200), "
        fsql = fsql & "     case when exists(select o_filename from [compassweb4].[dbo].[tOrganizationFile] "
        fsql = fsql & "                     where 1=1 and o_idx = " & idx & " and f_idx = " & f_idx & ")  "
        fsql = fsql & "             then (select o_filename from [compassweb4].[dbo].[tOrganizationFile]  "
        fsql = fsql & "             where 1=1 and o_idx = " & idx & " and f_idx = " & f_idx & ") "
        fsql = fsql & "         when not exists(select o_filename from [compassweb4].[dbo].[tOrganizationFile]  "
        fsql = fsql & "                         where 1=1 and o_idx = " & idx & " and f_idx = " & f_idx & ") "
        fsql = fsql & "         then '' "
        fsql = fsql & "         else '' "
        fsql = fsql & "     end) as o_filename "

        'Response.Write fsql
        'Response.End

        o_filename = ContentViewLoadWS(provWSDL, fsql)

	    '============= 기존파일 삭제 ==============
	    If o_filename <> "" Then
		    arr_fname = Split(o_filename, "/")
		    fname = arr_fname(Ubound(arr_fname))
    
		    strDateDir1 = arr_fname(Ubound(arr_fname)-2) & "\"
		    strDateDir2 = arr_fname(Ubound(arr_fname)-1) & "\"
		    strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
		    strDelPath = strSaveFolder & fname
    
		    Set fso = CreateObject("Scripting.FileSystemObject")

		    '파일 존재시 파일 삭제
		    If fso.FileExists(strDelPath) Then
			    fso.DeleteFile strDelPath
		    End If
		    Set fso = Nothing
    
	    End if
	    '============= 기존파일 삭제 ==============
        'response.End


        '데이터 삭제
        fsql2 = " DELETE FROM [compassweb4].[dbo].[tOrganizationFile] "
        fsql2 = fsql2 & " WHERE 1=1 "
        fsql2 = fsql2 & " AND o_idx = N'"& idx &"' "
        fsql2 = fsql2 & " AND f_idx = N'"& f_idx &"' "

        fresult = BoardWriteWS(provWSDL, fsql2)


        if fresult = 10 then
            response.write "<script type='text/javascript' charset='euc-kr'> alert('파일삭제 되었습니다.'); </script>"
            response.write link
        else
            response.write "<script type='text/javascript' charset='euc-kr'> alert('삭제중 오류가 발생하였습니다.'); </script>"
            response.write link
        end if

    Else 
        response.write "<script type='text/javascript' charset='euc-kr'> alert('파일데이터가 없어 삭제할 수 없습니다.'); </script>"
        response.write link
    End If 
else 
    response.write "<script type='text/javascript' charset='euc-kr'> alert('글이 없어 삭제할 수 없습니다.'); </script>"
    response.write link
end if 
%>

</html>
