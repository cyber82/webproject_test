﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
'*******************************************************************************
'페이지 설명 : 이사회 회의록 관리
'작성일      : 2013-06-13
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
%>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
if request("idx") = "" then
    idx = 0
else 
    idx = request("idx")
end if

page = Request("page")
startpage = Request("startpage")

'기본 저장위치
saveDefaultPath = "/popup/20_DirectorMeeting/images/"
strDefFolder = Server.MapPath(saveDefaultPath) & "\"
strSaveFolder = strDefFolder & strDateDir

'test
'idx = 0

link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"

if CInt(idx) > 0 then 

    sql = " UPDATE [compassweb4].[dbo].[tOrganization] SET "
    sql = sql & " del_flag = 1 "
    sql = sql & " WHERE idx = " & idx

    inresult = BoardWriteWS(provWSDL, sql)

    if inresult = 10 then

        If CInt(idx) > 0 Then

            fsql = " SELECT "
            fsql = fsql & " f_idx, o_idx, o_filename " 
            fsql = fsql & " , convert(varchar(20),reg_date,120) as reg_date " 
            fsql = fsql & " FROM [compassweb4].[dbo].[tOrganizationFile] "
            fsql = fsql & " WHERE 1=1 "
            fsql = fsql & " AND o_idx = " & idx
            fsql = fsql & " ORDER BY f_idx ASC "
            set rs2 = RecordsetFromWS(provWSDL, fsql) 

            'totalcount
            sqlCnt2 = " SELECT convert(varchar, count(f_idx)) as cnt "
            sqlCnt2 = sqlCnt2 & " FROM [compassweb4].[dbo].[tOrganizationFile] "
            sqlCnt2 = sqlCnt2 & " WHERE 1=1 "
            sqlCnt2 = sqlCnt2 & " AND o_idx = " & idx
            aCnt = ContentViewLoadWS(provWSDL, sqlCnt2)

            'totalcount
            sqlCnt = " SELECT count(f_idx) "
            sqlCnt = sqlCnt & " FROM [compassweb4].[dbo].[tOrganizationFile] "
            sqlCnt = sqlCnt & " WHERE 1=1 "
            sqlCnt = sqlCnt & " AND o_idx = " & idx
            Set rsCnt = RecordsetFromWS2(provWSDL, sqlCnt)

            Dim fname(), f_idx(), strDelPath()
            Dim arr_fname, strDateDir1, strDateDir2, strSaveFolder

            Redim fname(Cint(aCnt))
            Redim f_idx(Cint(aCnt))
            Redim strDelPath(Cint(aCnt))

	        if rs2.BOF or rs2.EOF then
            Else
                i = 1
                j = 0
		        do until rs2.EOF or i > rs2.PageSize

                f_idx(j) = rs2("f_idx")
                o_idx = rs2("o_idx")
                o_filename = rs2("o_filename")
                reg_date = rs2("reg_date")

                'response.End

	            '============= 기존파일 삭제 ==============
	            If o_filename <> "" Then
		            arr_fname = Split(o_filename, "/")
		            fname(j) = arr_fname(Ubound(arr_fname))
    
		            strDateDir1 = arr_fname(Ubound(arr_fname)-2) & "\"
		            strDateDir2 = arr_fname(Ubound(arr_fname)-1) & "\"
		            strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
		            strDelPath(j) = strSaveFolder & fname(j)
    
		            Set fso = CreateObject("Scripting.FileSystemObject")

		            '파일 존재시 파일 삭제
		            If fso.FileExists(strDelPath(j)) Then
			            fso.DeleteFile strDelPath(j)
		            End If
		            Set fso = Nothing
    
	            End if
	            '============= 기존파일 삭제 ==============
                'response.End

                rs2.MoveNext ' 다음 레코드로 이동한다.
		        i = i + 1
                j = j + 1
		        Loop '레코드의 끝까지 loop를 돈다.

	        End If

            '데이터 삭제
            For i = 0 to Cint(aCnt)
                fsql2 = " DELETE FROM [compassweb4].[dbo].[tOrganizationFile] "
                fsql2 = fsql2 & " WHERE 1=1 "
                fsql2 = fsql2 & " AND o_idx = N'"& idx &"' "
                fsql2 = fsql2 & " AND f_idx = N'"& f_idx(i) &"' "

                fresult = BoardWriteWS(provWSDL, fsql2)
            Next
            'response.End

        End If


        response.write "<script type='text/javascript' charset='euc-kr'> alert('삭제되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('삭제중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if
else 
    response.write "<script type='text/javascript' charset='euc-kr'> alert('글이 없어 삭제할 수 없습니다.'); </script>"
    response.write link
end if 
%>

</html>
