<%@Language="VBScript" CODEPAGE="65001"%>
<%
'*******************************************************************************
'페이지 설명 : 공지사항 관리 리스트 삭제
'작성일      : 2012-08-17
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<%

idx = Request("idx")
page = Request("page")


With oCmd
	.ActiveConnection = db
	.CommandText = "dbo.[UP_APP_ADMIN_CAMPAIGN]"
	.CommandType = adCmdStoredProc
	.Parameters.Append .CreateParameter("@cmd", adVarChar, adParamInput, 10, "del")
	.Parameters.Append .CreateParameter("@idx", adInteger, adParamInput, 4, idx)
	.Parameters.Append .CreateParameter("@A_PAGE", adInteger, adParamInput, 4, intPage)
	.Parameters.Append .CreateParameter("@A_PAGE_SIZE", adInteger, adParamInput, 4, intPageSize)
	.Parameters.Append .CreateParameter("@st", adVarChar, adParamInput, 50, st)
	.Parameters.Append .CreateParameter("@sw", adVarChar, adParamInput, 50, sw)
	.Parameters.Append .CreateParameter("@title", adVarChar, adParamInput, 100, title)
	.Parameters.Append .CreateParameter("@contents", adVarChar, adParamInput, 3000, contents)
	.Execute,,adExecuteNoRecords
	.Parameters.Delete("@cmd")
	.Parameters.Delete("@idx")
	.Parameters.Delete("@A_PAGE")
	.Parameters.Delete("@A_PAGE_SIZE")
	.Parameters.Delete("@st")
	.Parameters.Delete("@sw")
	.Parameters.Delete("@title")
	.Parameters.Delete("@contents")
End With
Set oCmd = Nothing


Call Alert_Go("삭제되었습니다.", "list.asp?page="& page)
%>