<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 공지사항 관리 리스트
'작성일      : 2012-07-12
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%

idx = Request.Form("idx")
page = Request.Form("page")
title = Request.Form("title")
contents = Request.Form("contents")
useyn = Request.Form("useyn")

Set oCmd = Server.CreateObject("ADODB.Command")

'등록
if idx = "" Then

    sql = "exec UP_APP_ADMIN_NOTICE 'insert', 0, '" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "','" &title& "','" &contents& "','" &useyn& "'"
    inresult = BoardWriteWS(provWSDL, sql)


'수정
Else

        sql = "exec UP_APP_ADMIN_NOTICE 'edit','" &idx& "','" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "','" &title& "','" &contents& "','" &useyn& "'"
        inresult = BoardWriteWS(provWSDL, sql)
End If
Set oCmd = Nothing

Response.Redirect "list.asp?page="& page
%>