﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 공지사항 관리 리스트
'작성일      : 2012-07-12
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%

idx = Request("idx")
page = Request("page")

If idx = "" Then
	stitle = "신규등록"
	useyn = "1"
Else
	stitle = "수정"

	'TITLE, SDATE, EDATE, MEMO, IMG, MAIN_IMG, CONTENT_IMG
    sql = "exec UP_APP_ADMIN_NOTICE 'view', '" &idx& "','" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "'"
    set Rs = RecordsetFromWS(provWSDL, sql)

	If Not Rs.eof Then

		TITLE = Rs(0)
		TITLE = Replace(TITLE,"""","&quot;")
		CONTENTS = Rs(1)
		CONTENTS = Replace(CONTENTS,"""","&quot;")
		app_viewyn = Rs(2)

	End If

End If

%>
<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="../cal.js"></script>
<script type="text/javascript">
function doWrite(){
	var f = document.write_f;
	if (f.title.value == "") {
		alert("제목을 입력해 주세요.");
		f.title.focus();
		return;
	}
	if (f.contents.value == "") {
		alert("내용을 입력해 주세요.");
		f.contents.focus();
		return;
	}
	f.action = "write_proc.asp";
	f.submit();
}
function doDel(){
	if (confirm("[주의]삭제 하시겠습니까?")) {
		var f = document.del_f;
		f.action = "del_proc.asp";
		f.submit();
	}
}
</script>
</head>
<body>

<br>
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">공지사항 관리 <%=stitle%></font></strong>
<br>

<form name="del_f" id="del_f" method="post">
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="page" value="<%=page%>">
</form>


<form name="write_f" id="write_f" method="post">
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="page" value="<%=page%>">

<table width="800" border="0" cellpadding="3" cellspacing="1" bgcolor="#DFDFDF">
  <tr>
    <td bgcolor="#F3F3F3"><b>제목(20자이내)</b></td>
	<td bgcolor="#FFFFFF">
		<%=TITLE%>
		<!-- <input type="text" name="title" size="70" value="<%'=TITLE%>"> -->
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>내용</b></td>
	<td bgcolor="#FFFFFF">
		<textarea name="contents" rows="10" cols="60"><%=CONTENTS%></textarea>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>노출여부</b></td>
	<td bgcolor="#FFFFFF">
		<input type="radio" name="useyn" value="Y" <%=fnSelect(app_viewyn,"Y","checked")%>>노출
		<input type="radio" name="useyn" value="N" <%=fnSelect(app_viewyn,"N","checked")%>>비노출
	</td>
  </tr>
</table>
</form>
<br>
<%
If idx = "" Then
%>

<input type="button" value="등록" onClick="doWrite();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">

<%
Else
%>
<input type="button" value="수정" onClick="doWrite();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">
<!-- <input type="button" value="삭제" onClick="doDel();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;"> -->

<%
End if
%>

<input type="button" value="취소" onClick="location.href='list.asp?page=<%=page%>';" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">

<br><br><br><br>
</body>
</html>