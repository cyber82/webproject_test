﻿
<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 캠페인 관리 리스트
'작성일      : 2012-07-12
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%

intPage = Request("page")
If intPage = "" Then intPage = 1
st = Request("st")
sw = Request("sw")

intPageSize = 20


    sql = "exec UP_APP_ADMIN_CAMPAIGN 'list', 0,'" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "'"
    set Rs = RecordsetFromWS(provWSDL, sql)

If Rs.EOF Then
	blnNewsList = False
	RecordCount = 0
Else
	blnNewsList = True
	RecordCount = Rs(0)
	arrNewsList = Rs.GetRows
End If
Rs.Close
Set Rs = Nothing
'db.Close

intPageCount = RecordCount \ intPageSize
If RecordCount Mod intPageSize > 0 Then intPageCount = intPageCount + 1
If intPageCount = 0 Then intPageCount = 1
'	Response.Write intRecordCount

intNum = RecordCount - (intPage-1) * intPageSize

%>
<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript">
function doSearch(f){
	if (f.sw.value == "") {
		alert("검색어를 입력해 주세요.");
		f.sw.focus();
		return false;
	}
}
</script>
</head>
<body>

<br>
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">캠페인관리 리스트</font></strong>
<br>
<br>


<table width="800" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
  <tr bgcolor="#F3F3F3" align="center">
    <td height="25"><b>No</b></td>
	<td width="120"><b>이미지</b></td>
	<td><b>제목</b></td>
	<td width="70"><b>메인노출</b></td>
	<td width="150"><b>캠페인 기간</b></td>
	<td width="140"><b>등록일</b></td>
  </tr>

<%

If blnNewsList Then

'IDX
',	TITLE
',	SDATE
',	EDATE
',	MAIN_IMG

	For i=0 To UBound(arrNewsList,2)

		idx = arrNewsList(1,i)
		title = arrNewsList(2,i)
		sdate = arrNewsList(3,i)
		edate = arrNewsList(4,i)
		main_img = arrNewsList(5,i)
		regdate = arrNewsList(6,i)
		term = Trim(arrNewsList(7,i))
		mainyn = Trim(arrNewsList(8,i))

		If CStr(term) = "1" Then	'진행중
			bgColor = "#FFFFFF"
		Else						'종료
			bgColor = "#F3F3F3"
		End If

		If CStr(mainyn) = "Y" Then
			strMainyn = "<b>[메인노출]</b>"
		Else
			strMainyn = ""
		End if

		strImg = ""
		If main_img <> "" Then
			strImg = "<img src='"& main_img &"' width='110' height='110' border='0'>"
		End If

		term = sdate & " ~ " & edate
%>
  <tr bgcolor="<%=bgColor%>">
    <td height="25" align="center" width="30"><%=intNum%></td>
	<td>&nbsp;<%=strImg%></td>
	<td><a href="write.asp?idx=<%=idx%>&page=<%=intPage%>"><%=title%></a></td>
	<td>&nbsp;<%=strMainyn%></td>
	<td align="center"><%=term%></td>
	<td align="center"><%=regdate%></td>
  </tr>
<%
	intNum = intNum - 1
	Next
End if
%>
</table>

<table width="800" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" height="50">
<%
	Dim objNavi
	Set objNavi = New PageNavi

	With objNavi
	.CurrentPage = intPage		'필
	.PageCount = intPageCount	'필
	.PrevImage = ""
	.NextImage = ""
	.QueryString = "sw="&sw
	.NaviCount = 10
	.Style = 1
	.LinkClass = ""
	.ParamPageName = "page"
	.GoFirst = True
	.GoLast = True
	.GoFirstImage = "/images/arrow_pre.gif"
	.GoLastImage = "/images/arrow_next.gif"
	.DisplayNoLink = True

	Response.Write .MakeNavi
	End With
	Set objNavi = Nothing
%>
    </td>
  </tr>
  <tr>
    <td align="right" height="40">
	  <table border="0" width="700" cellpadding="0" cellspacing="0">
		<tr>
		  <td align="right">
			<form name="search_f" method="get" onSubmit="return doSearch(this);">
			<select name="st">
			  <option value="title">제목</option>
			</select>
			<input type="text" name="sw" value="<%=sw%>"> <input type="submit" value="검색" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">
			</form>
		  </td>
		  <td align="right" width="80">
			<input type="button" value="신규등록" onClick="location.href='write.asp';" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">
		  </td>
		</tr>
      </table>
	</td>
  </td>
</table>


<br><br><br><br>
</body>
</html>