﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 영상관리 리스트
'작성일      : 2012-07-12
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%

idx = Request("idx")
page = Request("page")

If idx = "" Then
	stitle = "신규등록"
	useyn = "1"
Else
	stitle = "수정"

	'TITLE, SDATE, EDATE, MEMO, IMG, MAIN_IMG, CONTENT_IMG
    sql = "exec UP_APP_ADMIN_CAMPAIGN 'view', '" &idx& "', '" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "'"
    set Rs = RecordsetFromWS(provWSDL, sql)

	If Not Rs.eof Then

		TITLE = Rs(0)
		TITLE = Replace(TITLE,"""","&quot;")
		SDATE = Rs(1)
		EDATE = Rs(2)
		MAIN_IMG = Rs(3)
		THUMB_IMG = Rs(4)
		MAIN_THUMB_IMG = Rs(5)
		IMG = Rs(6)
		CONTENTS = Rs(7)
		CONTENTS = Replace(CONTENTS,"""","&quot;")
		CATE = Rs(8)
		SHARE_URL = Rs(9)
		MAINYN = Rs(10)

		If MAIN_IMG <> "" Then strImg1 = "<br><img src='"& MAIN_IMG &"' border='0'>"
		If THUMB_IMG <> "" Then strImg2 = "<br><img src='"& THUMB_IMG &"' border='0'>"
		If MAIN_THUMB_IMG <> "" Then strImg3 = "<br><img src='"& MAIN_THUMB_IMG &"' border='0'>"
		If IMG <> "" Then strImg4 = "<br><img src='"& IMG &"' border='0'>"

	End If

End If

%>
<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="../cal.js"></script>
<script type="text/javascript">
function doWrite(){
	var f = document.write_f;
	if (f.title.value == "") {
		alert("제목을 입력해 주세요.");
		f.title.focus();
		return;
	}
	if (f.sdate.value == "") {
		alert("캠페인 시작일을 입력해 주세요.");
		f.sdate.focus();
		return;
	}
	if (f.edate.value == "") {
		alert("캠페인 종료일을 입력해 주세요.");
		f.edate.focus();
		return;
	}
	if (f.shareUrl.value == "") {
		alert("공유URL을 입력해 주세요.");
		f.shareUrl.focus();
		return;
	}
	if (f.contents.value == "") {
		alert("내용을 입력해 주세요.");
		f.contents.focus();
		return;
	}
	f.action = "write_proc.asp";
	f.submit();
}
function doDel(){
	if (confirm("[주의]삭제 하시겠습니까?")) {
		var f = document.del_f;
		f.action = "del_proc.asp";
		f.submit();
	}
}
</script>
</head>
<body>

<br>
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">캠페인관리 <%=stitle%></font></strong>
<br>

<form name="del_f" id="del_f" method="post">
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="page" value="<%=page%>">
</form>


<form name="write_f" id="write_f" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="fname1" value="<%=MAIN_IMG%>">
<input type="hidden" name="fname2" value="<%=THUMB_IMG%>">
<input type="hidden" name="fname3" value="<%=MAIN_THUMB_IMG%>">
<input type="hidden" name="fname4" value="<%=IMG%>">
<input type="hidden" name="page" value="<%=page%>">

<table width="800" border="0" cellpadding="3" cellspacing="1" bgcolor="#DFDFDF">
  <tr>
    <td bgcolor="#F3F3F3"><b>제목(10자이내)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="title" size="50" value="<%=TITLE%>">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>캠페인시작일</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="sdate" id="sdate" size="10" value="<%=SDATE%>" style="cursor:pointer;" onclick="popUpCalendar(this, sdate, 'yyyy-mm-dd')">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>캠페인종료일</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="edate" id="edate" size="10" value="<%=EDATE%>" style="cursor:pointer;" onclick="popUpCalendar(this, edate, 'yyyy-mm-dd')">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>공유URL<br>(실제 SNS로 나가는 URL)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="shareUrl" id="shareUrl" size="50" value="<%=SHARE_URL%>">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>메인 이미지 (250 X 310)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file1" size="50">
		<%=strImg1%>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>리스트 이미지(110 X 110)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file2" size="50">
		<%=strImg2%>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>상단 썸네일 이미지(603 X 389)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file3" size="50">
		<%=strImg3%>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>전체 이미지(640 X 960)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file4" size="50">
		<%=strImg4%>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>내용</b></td>
	<td bgcolor="#FFFFFF">
		<textarea name="contents" rows="10" cols="60"><%=contents%></textarea>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>일반후원금 종류</b></td>
	<td bgcolor="#FFFFFF">
		<select name="cate">
			<option value="MDA" <%=fnSelect(CATE,"MDA","selected")%>>의료지원 후원금</option>
			<option value="AIDS" <%=fnSelect(CATE,"AIDS","selected")%>>에이즈 예방 및 퇴치 후원금</option>
			<option value="RF" <%=fnSelect(CATE,"RF","selected")%>>재난구호 후원금</option>
			<option value="OVC" <%=fnSelect(CATE,"OVC","selected")%>>어린이SOS 후원금</option>
			<option value="EDU" <%=fnSelect(CATE,"EDU","selected")%>>교육지원 후원금</option>
			<option value="CSP" <%=fnSelect(CATE,"CSP","selected")%>>CSP</option>
			<option value="CF" <%=fnSelect(CATE,"CF","selected")%>>CF</option>
            <option value="PBCIV" <%=fnSelect(CATE,"PBCIV","selected")%>>PBCIV</option> <% '추가 2013-06-18 %>
		</select>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>메인노출여부</b></td>
	<td bgcolor="#FFFFFF">
		<input type="radio" name="mainyn" value="Y" <%=fnSelect(MAINYN,"Y","checked")%>>노출
		<input type="radio" name="mainyn" value="N" <%=fnSelect(MAINYN,"N","checked")%>>비노출
	</td>
  </tr>
</table>
</form>
<br>
<%
If idx = "" Then
%>

<input type="button" value="등록" onClick="doWrite();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">

<%
Else
%>
<input type="button" value="수정" onClick="doWrite();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">
<input type="button" value="삭제" onClick="doDel();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">

<%
End if
%>

<input type="button" value="취소" onClick="location.href='list.asp?page=<%=page%>';" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">

<br><br><br><br>
</body>
</html>