﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 프렌즈 Shop 관리 리스트 수정
'작성일      : 2012-07-17
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%

idx = Request("idx")
page = Request("page")

Set oCmd = Server.CreateObject("ADODB.Command")

If idx = "" Then
	stitle = "신규등록"
	USEYN = "1"
Else
	stitle = "수정"


    sql = "exec UP_APP_ADMIN_FRIENDS_SHOP 'view', '" &idx& "'"
    set Rs = RecordsetFromWS(provWSDL, sql)

'shop_idx, shop_cateCode, shop_lat, shop_lng, shop_name, shop_tel, shop_addr, shop_info, img1, img2, img3
	If Not Rs.eof Then

		shop_idx = Rs(0)
		shop_cateCode = Rs(1)
		shop_lat = Rs(2)
		shop_lng = Rs(3)
		shop_name = Rs(4)
		shop_name = Replace(shop_name,"""","&quot;")
		shop_tel = Rs(5)
		shop_addr = Rs(6)
		shop_info = Rs(7)	'내용
		shop_info = Replace(shop_info,"""","&quot;")
		shop_app_img1 = Rs(8)
		shop_app_img2 = Rs(9)
		shop_app_img3 = Rs(10)
		delYN = Rs(11)
		shop_pros = Rs(12)	'혜택
		shop_homepage = Rs(13)	'홈페이지
		shop_intro = Trim(Rs(14))	'짧은소개 - 가져와서 내용에 넣는다
		shop_intro = Replace(shop_intro,"""","&quot;")

		'내용이 없을때 짧은소개를 넣는다
		If shop_info = "" Then
			shop_info = shop_intro
		End if

		If shop_app_img1 <> "" Then strImg1 = "<img src='"& shop_app_img1 &"' border='0'><br>"
		If shop_app_img2 <> "" Then strImg2 = "<img src='"& shop_app_img2 &"' border='0'><br>"
		If shop_app_img3 <> "" Then strImg3 = "<img src='"& shop_app_img3 &"' border='0'><br>"
	End If

End If

'카테고리 조회
strOpt = ""

    sql = "exec UP_APP_ADMIN_FRIENDS_SHOP 'get_cate'"
    set Rs2 = RecordsetFromWS(provWSDL, sql)

If Not Rs2.eof Then

	AllRec = Rs2.GetRows
	For j = 0 To UBound(AllRec,2)

		cat_code = Trim(AllRec(0, j))	'001
		cat_desc = Trim(AllRec(1, j))	'레스토랑/카페

		strSelect = ""
		If CStr(cat_code) = CStr(shop_cateCode) Then
			strSelect = "selected"
		End if

		strOpt = strOpt & "<option value='"& cat_code &"' "& strSelect &">"& cat_desc &"</option>"
	Next

End if

Set oCmd = Nothing
%>
<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="../jquery-1.7.2.min.js"></script>
<script type="text/javascript">
function doWrite(){
	var f = document.write_f;
	/*
	if (f.contents.value == "") {
		alert("내용을 입력해 주세요.");
		f.contents.focus();
		return;
	}
	if (f.url.value == "") {
		alert("연결URL을 입력해 주세요.");
		f.url.focus();
		return;
	}*/
	f.action = "write_proc.asp";
	f.submit();
}
function doDel(){
	if (confirm("[주의]삭제 하시겠습니까?")) {
		var f = document.write_f;
		f.action = "del_proc.asp";
		f.submit();
	}
}
function subFriendShopReg(){
	var f = document.sub_write_f;

	if (f.sub_shop_name.value == "") {
		alert("지점이름을 입력해 주세요.");
		f.sub_shop_name.focus();
		return;
	}
	if (f.sub_shop_addr.value == "") {
		alert("지점 주소를 입력해 주세요.");
		f.sub_shop_addr.focus();
		return;
	}
	if (f.sub_latitude.value == "") {
		alert("지점 위도를 입력해 주세요.");
		f.sub_latitude.focus();
		return;
	}
	if (f.sub_longitude.value == "") {
		alert("지점 경도를 입력해 주세요.");
		f.sub_longitude.focus();
		return;
	}
	if (f.sub_shop_tel.value == "") {
		alert("지점 연락처를 입력해 주세요.");
		f.sub_shop_tel.focus();
		return;
    }

var url = "ax_subFriendShop_write_proc.asp";

//location.replace(url);




    var qstring = $("#sub_write_f").serialize();
    $.post(url, qstring, function (data) {
        if (data == "SUC") {
            alert("등록 되었습니다.");
            $("#sub_shop_name").val("");
            $("#sub_shop_addr").val("");
            $("#sub_latitude").val("");
            $("#sub_longitude").val("");
            $("#sub_shop_tel").val("");
            $("#delyn0").attr("checked", true)
            subFriendShopList();
        }
    });
}
function subFriendShopList(){
	var url = "ax_subFriendShop_list.asp";
	var qstring = $("#sub_list_f").serialize();
	$.post(url, qstring,
	function(data){
		$("#sub_fshop_area").html(data);
	});
}
function subFriendShopEdit(idx) {
	var f = eval("sub_edit_f_"+idx);
	if (f.sub_shop_name.value == "") {
		alert("지점이름을 입력해 주세요.");
		f.sub_shop_name.focus();
		return;
	}
	if (f.sub_shop_addr.value == "") {
		alert("지점 주소를 입력해 주세요.");
		f.sub_shop_addr.focus();
		return;
	}
	if (f.sub_latitude.value == "") {
		alert("지점 위도를 입력해 주세요.");
		f.sub_latitude.focus();
		return;
	}
	if (f.sub_longitude.value == "") {
		alert("지점 경도를 입력해 주세요.");
		f.sub_longitude.focus();
		return;
	}
	if (f.sub_shop_tel.value == "") {
		alert("지점 연락처를 입력해 주세요.");
		f.sub_shop_tel.focus();
		return;
	}
	var url = "ax_subFriendShop_write_proc.asp";
	var qstring = $("#sub_edit_f_" + idx).serialize();
    // 2013.01.25 firelog 주석 차후 조사
	//firelog('qstring ',qstring )
	$.post(url, qstring, function (data) {

		if (data == "SUC"){
			alert("수정 되었습니다.");
			subFriendShopList();
		}
	});
}
$(document).ready(function(){
	subFriendShopList();
});
</script>
</head>
<body>

<br>
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">프렌즈 Shop 관리 <%=stitle%></font></strong>
<br>

<form name="del_f" id="del_f" method="post">
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="page" value="<%=page%>">
</form>


<form name="write_f" id="write_f" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="fname1" value="<%=shop_app_img1%>">
<input type="hidden" name="fname2" value="<%=shop_app_img2%>">
<input type="hidden" name="fname3" value="<%=shop_app_img3%>">
<input type="hidden" name="page" value="<%=page%>">

<table width="800" border="0" cellpadding="3" cellspacing="1" bgcolor="#DFDFDF">
  <tr>
    <td bgcolor="#F3F3F3" width="100"><b>프렌즈Shop</b></td>
    <td bgcolor="#FFFFFF" colspan="3" height="25">
		<%=shop_name%>
		<!-- <input type="text" name="shop_name" id="shop_name" size="50" value="<%=shop_name%>" /> -->
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>혜택</b></td>
    <td bgcolor="#FFFFFF" colspan="3" height="25">
		<%=shop_pros%>
		<!-- <input type="text" name="shop_pros" id="shop_pros" size="90" value="<%'=shop_pros%>" /> -->
	</td>
    <!-- <td bgcolor="#F3F3F3" width="100"><b>연락처</b></td>
    <td bgcolor="#FFFFFF">
		<input type="text" name="shop_tel" id="shop_tel" size="15" value="<%=shop_tel%>" />
	</td> -->
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="100"><b>연락처</b></td>
    <td bgcolor="#FFFFFF" colspan="3" height="25">
		<%=shop_tel%>
		<!-- <input type="text" name="shop_tel" id="shop_tel" size="20" value="<%=shop_tel%>" /> -->
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>홈페이지</b></td>
    <td bgcolor="#FFFFFF" colspan="3" height="25">
		<%=shop_homepage%>
		<!-- <input type="text" name="shop_homepage" id="shop_homepage" size="90" value="<%=shop_homepage%>" /> -->
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>주소</b></td>
    <td bgcolor="#FFFFFF" colspan="3" height="25">
		<%=shop_addr%>
		<!-- <input type="text" name="shop_addr" id="shop_addr" size="90" value="<%=shop_addr%>" /> -->
	</td>
  </tr>
  <!-- <tr>
    <td bgcolor="#F3F3F3"><b>짧은소개</b></td>
    <td bgcolor="#FFFFFF" colspan="3" height="25"> -->
		<%'=shop_intro%>
		<!-- <input type="text" name="shop_intro" id="shop_intro" size="90" value="<%=shop_intro%>" /> -->
	<!-- </td>
  </tr> -->
  <tr>
    <td bgcolor="#F3F3F3"><b>위/경도</b></td>
    <td bgcolor="#FFFFFF" colspan="3">
    	<b>위도</b> <input type="text" name="latitude" id="latitude" size="20" value="<%=shop_lat%>" />
      	<b>경도</b> <input type="text" name="longitude" id="longitude" size="20" value="<%=shop_lng%>" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button" value="위/경도 계산하러가기" onClick="window.open('http://dna.daum.net/apis/maps/demo','','');" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:140px;height:23px;cursor:pointer;">
    </td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>카테고리</b></td>
    <td bgcolor="#FFFFFF" colspan="3">
      <select name="shop_cate" id="shop_cate">
		<%=strOpt%>
      </select>
    </td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>이미지등록<br>(434*281)</b></td>
    <td bgcolor="#FFFFFF" colspan="3">
      <input type="file" name="file1" size="30" /><br />
	  <%=strImg1%>
      <input type="file" name="file2" size="30" /><br />
	  <%=strImg2%>
      <input type="file" name="file3" size="30" /><br />
	  <%=strImg3%>
    </td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>내용등록</b></td>
    <td bgcolor="#FFFFFF" colspan="3"><textarea name="shop_info" cols="60" rows="8"><%=shop_info%></textarea></td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>노출여부</b></td>
    <td bgcolor="#FFFFFF" colspan="3">
		<input type="radio" name="delyn" value="N" <%=fnSelect(delYN,"N","checked")%>>노출
		<input type="radio" name="delyn" value="Y" <%=fnSelect(delYN,"Y","checked")%>>비노출
	</td>
  </tr>
</table>
</form>
<br>
<%
If idx = "" Then
%>

<!-- <input type="button" value="등록" onClick="doWrite();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;"> -->

<%
Else
%>
<input type="button" value="수정" onClick="doWrite();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">
<!-- <input type="button" value="삭제" onClick="doDel();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;"> -->

<%
End if
%>

<input type="button" value="취소" onClick="location.href='list.asp?page=<%=page%>';" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">

<br><br><br><br>


<form name="sub_list_f" id="sub_list_f" method="post">
<input type="hidden" name="idx" value="<%=idx%>"><!-- 지점번호 -->
<input type="hidden" name="shop_idx" value="<%=shop_idx%>"><!-- 본점번호 -->
<input type="hidden" name="sub_page" value="<%=page%>">
</form>
<form name="sub_write_f" id="sub_write_f" method="post" accept-charset="utf-8">
<input type="hidden" name="idx" value="<%=idx%>"><!-- 지점번호 -->
<input type="hidden" name="shop_idx" value="<%=shop_idx%>"><!-- 본점번호 -->
<input type="hidden" name="shop_cate" value="<%=shop_cateCode%>"><!-- 본점카테고리 -->
<input type="hidden" name="cmd" value="sub_insert">
<table width="800" border="0" cellpadding="3" cellspacing="1" bgcolor="#DFDFDF">
  <tr>
    <td bgcolor="#F3F3F3" width="100"><b>지점이름</b></td>
    <td bgcolor="#FFFFFF">
		<input type="text" name="sub_shop_name" id="sub_shop_name" size="50" value="" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="100"><b>주소</b></td>
    <td bgcolor="#FFFFFF">
		<input type="text" name="sub_shop_addr" id="sub_shop_addr" size="90" value="" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="100"><b>위/경도</b></td>
    <td bgcolor="#FFFFFF">
    	<b>위도</b> <input type="text" name="sub_latitude" id="sub_latitude" size="20" value="" />
      	<b>경도</b> <input type="text" name="sub_longitude" id="sub_longitude" size="20" value="" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button" value="위/경도 계산하러가기" onClick="window.open('http://dna.daum.net/apis/maps/demo','','');" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1px solid #330099;width:140px;height:23px;cursor:pointer;">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="100"><b>연락처</b></td>
    <td bgcolor="#FFFFFF" colspan="3">
		<input type="text" name="sub_shop_tel" id="sub_shop_tel" size="20" value="" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>노출여부</b></td>
    <td bgcolor="#FFFFFF" colspan="3">
		<input type="radio" name="delyn" id="delyn0" value="N" checked>노출
		<input type="radio" name="delyn" value="Y">비노출
	</td>
  </tr>
</table>
<input type="button" value="지점추가" onClick="subFriendShopReg();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1px solid #330099;width:70px;height:23px;cursor:pointer;">
</form>

<div id="sub_fshop_area"></div>

<br><br><br><br>
</body>
</html>