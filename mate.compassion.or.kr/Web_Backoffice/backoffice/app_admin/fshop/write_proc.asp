<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 영상관리 리스트
'작성일      : 2012-07-12
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
	Server.ScriptTimeout = 600
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/freeaspupload.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%

saveDefaultPath = "/images/mobileapp/friends_shop/"

'저장 폴더명
Dim strDateDir : strDateDir = getDateDivDir(0, "\")
strDefFolder = Server.MapPath(saveDefaultPath) & "\"
strSaveFolder = strDefFolder & strDateDir
Call MkDirAll(strSaveFolder)	'폴더 생성

uploadTempDir = strSaveFolder
Set Upload = New FreeASPUpload


'***************** 이미지 저장 *****************************
Upload.Save(uploadTempDir)

for each fileKey in Upload.UploadedFiles.keys

	Select Case fileKey
		Case "file1" : tempFile1 = Upload.UploadedFiles("file1").FileName
		Case "file2" : tempFile2 = Upload.UploadedFiles("file2").FileName
		Case "file3" : tempFile3 = Upload.UploadedFiles("file3").FileName
	End select

Next
'***************** 이미지 저장 *****************************

If tempFile1 <> "" Then

	vFilename = tempFile1						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile1)

	'새이름 생성
	newFilename = MakeFileName() & "."&strExt
	file.name = newFilename

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로
	strFilePath1 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename

End If

If tempFile2 <> "" Then

	vFilename = tempFile2						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile2)

	'새이름 생성
	newFilename = MakeFileName() & "."&strExt
	file.name = newFilename

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로
	strFilePath2 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename

End If

If tempFile3 <> "" Then

	vFilename = tempFile3						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile3)

	'새이름 생성
	newFilename = MakeFileName() & "."&strExt
	file.name = newFilename

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로
	strFilePath3 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename

End If



idx = Upload.Form("idx")
page = Upload.Form("page")
shop_name = Upload.Form("shop_name")	'샵이름
'shop_pros = Upload.Form("shop_pros")	'혜택
shop_tel = Upload.Form("shop_tel")		'연락처
'shop_homepage = Upload.Form("shop_homepage")	'홈페이지
shop_addr = Upload.Form("shop_addr")			'주소
'shop_intro = Upload.Form("shop_intro")			'짧은소개
latitude = Upload.Form("latitude")			'위도
longitude = Upload.Form("longitude")		'경도
shop_cate = Upload.Form("shop_cate")		'카테고리
shop_info = Upload.Form("shop_info")		'내용


delyn = Upload.Form("delyn")		'노출여부

fname1 = Upload.Form("fname1")	'이미지1
fname2 = Upload.Form("fname2")	'이미지2
fname3 = Upload.Form("fname3")	'이미지3


Set oCmd = Server.CreateObject("ADODB.Command")

if strFilePath1 <> "" then
	'============= 기존파일 삭제 ==============
	If fname1 <> "" Then
		arr_fname1 = Split(fname1, "/")
		fname = arr_fname1(Ubound(arr_fname1))

		strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
		strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
		strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
		strDelPath1 = strSaveFolder & fname

		Set fso = CreateObject("Scripting.FileSystemObject")
		'파일 존재시 파일 삭제
		If fso.FileExists(strDelPath1) Then
		   fso.DeleteFile strDelPath1
		End If
		Set fso = Nothing

	End if
	'============= 기존파일 삭제 ==============
	strImg1 = strFilePath1
else
	strImg1 = fname1
end If

if strFilePath2 <> "" then
	'============= 기존파일 삭제 ==============
	If fname2 <> "" Then
		arr_fname2 = Split(fname2, "/")
		fname = arr_fname2(Ubound(arr_fname2))

		strDateDir1 = arr_fname2(Ubound(arr_fname2)-2) & "\"
		strDateDir2 = arr_fname2(Ubound(arr_fname2)-1) & "\"
		strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
		strDelPath1 = strSaveFolder & fname

		Set fso = CreateObject("Scripting.FileSystemObject")
		'파일 존재시 파일 삭제
		If fso.FileExists(strDelPath1) Then
		   fso.DeleteFile strDelPath1
		End If
		Set fso = Nothing

	End if
	'============= 기존파일 삭제 ==============
	strImg2 = strFilePath2
else
	strImg2 = fname2
end If

if strFilePath3 <> "" then
	'============= 기존파일 삭제 ==============
	If fname3 <> "" Then
		arr_fname3 = Split(fname3, "/")
		fname = arr_fname3(Ubound(arr_fname3))

		strDateDir1 = arr_fname3(Ubound(arr_fname3)-2) & "\"
		strDateDir2 = arr_fname3(Ubound(arr_fname3)-1) & "\"
		strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
		strDelPath1 = strSaveFolder & fname

		Set fso = CreateObject("Scripting.FileSystemObject")
		'파일 존재시 파일 삭제
		If fso.FileExists(strDelPath1) Then
		   fso.DeleteFile strDelPath1
		End If
		Set fso = Nothing

	End if
	'============= 기존파일 삭제 ==============
	strImg3 = strFilePath3
else
	strImg3 = fname3
end If

'@cmd varchar(10)='',
'@idx int=0,
'@A_PAGE AS INT=0,
'@A_PAGE_SIZE AS INT=0,
'@st varchar(50)='',
'@sw varchar(50)='',
'@shop_name varchar(500)='',
'@shop_tel varchar(50)='',
'@shop_addr varchar(500)='',
'@latitude varchar(50)='',
'@longitude varchar(50)='',
'@shop_cate varchar(10)='',
'@shop_info text='',
'@img1 varchar(500)='',
'@img2 varchar(500)='',
'@img3 varchar(500)='',
'@delyn char(1)=''


    sql = "exec UP_APP_ADMIN_FRIENDS_SHOP 'edit', '" &idx& "','" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "','" &shop_name& "','" &shop_tel& "','" &shop_addr& "','" &latitude& "','" &longitude& "','" &shop_cate& "','" &shop_info& "','" &strImg1& "','" &strImg2& "','" &strImg3& "', '" &delyn& "'"
    inresult = BoardWriteWS(provWSDL, sql)

Response.Redirect "list.asp?page="& page
%>