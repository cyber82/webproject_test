<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 프렌즈샵 지점 리스트
'작성일      : 2012-08-20
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%

intPage = Request("sub_page")
If intPage = "" Then intPage = 1
intPageSize = 10


idx = Request("idx")	'지점번호
shop_idx = Request("shop_idx")	'본점번호


sub_shop_name = Request("sub_shop_name")	'샵이름
sub_shop_tel = Request("sub_shop_tel")		'연락처
sub_shop_addr = Request("sub_shop_addr")			'주소
sub_latitude = Request("sub_latitude")			'위도
sub_longitude = Request("sub_longitude")		'경도
shop_cate = Request("shop_cate")				'본점 카테고리
delyn = Request("delyn")		'노출여부
cmd = Request("cmd")

    sql = "exec UP_APP_ADMIN_FRIENDS_SHOP 'sub_list', '" &shop_idx& "','" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "','" &sub_shop_name& "','" &sub_shop_tel& "','" &sub_shop_addr& "','" &sub_latitude& "','" &sub_longitude& "','" &shop_cate& "','" &shop_info& "','" &strImg1& "','" &strImg2& "','" &strImg3& "','" &delyn& "'"
    set Rs = RecordsetFromWS(provWSDL, sql)

'@rcount
',	idx
',	shop_name
',	shop_tel
',	shop_addr
',	shop_lat
',	shop_lng
',	delYN
If Not Rs.eof Then

	AllRec = Rs.GetRows
	For j = 0 To UBound(AllRec,2)

		idx = AllRec(0, j)
		shop_name = AllRec(1, j)
		shop_tel = AllRec(2, j)
		shop_addr = AllRec(3, j)
		shop_lat = AllRec(4, j)
		shop_lng = AllRec(5, j)
		delyn = AllRec(6, j)

%>
<form name="sub_edit_f_<%=idx%>" id="sub_edit_f_<%=idx%>" method="post">
<input type="hidden" name="idx" value="<%=idx%>"><!-- 지점번호 -->
<input type="hidden" name="cmd" value="sub_edit">
<table width="800" border="0" cellpadding="3" cellspacing="1" bgcolor="#DFDFDF">
  <tr>
    <td bgcolor="#F3F3F3" width="100"><b>지점이름</b></td>
    <td bgcolor="#FFFFFF">
		<input type="text" name="sub_shop_name" id="sub_shop_name" size="50" value="<%=shop_name%>" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="100"><b>주소</b></td>
    <td bgcolor="#FFFFFF">
		<input type="text" name="sub_shop_addr" id="sub_shop_addr" size="90" value="<%=shop_addr%>" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="100"><b>위/경도</b></td>
    <td bgcolor="#FFFFFF">
    	<b>위도</b> <input type="text" name="sub_latitude" id="sub_latitude" size="20" value="<%=shop_lat%>" />
      	<b>경도</b> <input type="text" name="sub_longitude" id="sub_longitude" size="20" value="<%=shop_lng%>" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button" value="위/경도 계산하러가기" onClick="window.open('http://dna.daum.net/apis/maps/demo','','');" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1px solid #330099;width:140px;height:23px;cursor:pointer;">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="100"><b>연락처</b></td>
    <td bgcolor="#FFFFFF" colspan="3">
		<input type="text" name="sub_shop_tel" id="sub_shop_tel" size="20" value="<%=shop_tel%>" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>노출여부</b></td>
    <td bgcolor="#FFFFFF" colspan="3">
		<input type="radio" name="delyn" value="N" <%=fnSelect(delyn,"N","checked")%>>노출
		<input type="radio" name="delyn" value="Y" <%=fnSelect(delyn,"Y","checked")%>>비노출
	</td>
  </tr>
  <tr>
    <td colspan="4" bgcolor="#FFFFFF">
		<input type="button" value="수정" onClick="subFriendShopEdit('<%=idx%>');" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1px solid #330099;width:70px;height:23px;cursor:pointer;">
	</td>
  </tr>
</table>
</form>
<br>
<%
	Next
End if
%>