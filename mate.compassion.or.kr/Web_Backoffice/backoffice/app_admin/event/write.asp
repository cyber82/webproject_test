﻿
<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 영상관리 리스트
'작성일      : 2012-07-12
'작성자      :
'참고사항    :
'*******************************************************************************
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%


idx = Request("idx")
page = Request("page")

If idx = "" Then
	stitle = "신규등록"
	useyn = "1"
Else
	stitle = "수정"

	'TITLE, SDATE, EDATE, MEMO, IMG, MAIN_IMG, CONTENT_IMG


    sql = "exec UP_APP_ADMIN_EVENT 'view','" &idx& "','" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "'"
    set Rs = RecordsetFromWS(provWSDL, sql) 

	If Not Rs.eof Then

		TITLE = Rs(0)
		TITLE = Replace(TITLE,"""","&quot;")
		SDATE = Trim(Rs(1))
		EDATE = Trim(Rs(2))
		MEMO = Rs(3)
		MEMO = Replace(MEMO,"""","&quot;")
		IMG = Trim(Rs(4))
		MAIN_IMG = Trim(Rs(5))
		CONTENT_IMG = Trim(Rs(6))
		SHOW_ITEM = Rs(7)
		PERSON_MAX = Rs(8)
		SHARE_URL = Rs(9)
		draw_type = Trim(Rs(10))
		win_type = Rs(11)
		share_date = Rs(12)

		If IMG <> "" Then strImg1 = "<br><img src='"& IMG &"' border='0'>"
		If MAIN_IMG <> "" Then strImg2 = "<br><img src='"& MAIN_IMG &"' border='0'>"
		If CONTENT_IMG <> "" Then strImg3 = "<br><img src='"& CONTENT_IMG &"' border='0'>"

		If CStr(draw_type) = "1" Then
			strDrawType = "모두"
		Else
			strDrawType = "후원자"
		End If

		If CStr(win_type) = "1" Then
			strWinType = "직접선정"
		ElseIf CStr(win_type) = "2" Then
			strWinType = "댓글 중 자동 추첨"
		else
			strWinType = "댓글 중 N번째 선정"
		End If

	End If

End If

%>
<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="../jquery-1.7.2.min.js"></script>
<!-- <script type="text/javascript" src="../cal.js"></script> -->
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript" src="jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript">
function doWrite(){
	var f = document.write_f;
	if (f.title.value == "") {
		alert("제목을 입력해 주세요.");
		f.title.focus();
		return;
	}
	if (f.sdate.value == "") {
		alert("이벤트 시작일을 입력해 주세요.");
		f.sdate.focus();
		return;
	}
	if (f.edate.value == "") {
		alert("이벤트 종료일을 입력해 주세요.");
		f.edate.focus();
		return;
	}
	if (f.shareUrl.value == "") {
		alert("공유URL을 입력해 주세요.");
		f.shareUrl.focus();
		return;
	}
	if (f.memo.value == "") {
		alert("요약내용을 입력해 주세요.");
		f.memo.focus();
		return;
	}
	var isPersonMax = $("#show_item1").is(':checked');	//동반인 선택시
	var personMaxVal="";
	if (isPersonMax) {
		personMaxVal = $("#person_max option:selected").val();
		if (personMaxVal == "")	{
			alert("동반인 MAX값을 선택해 주세요.");
			f.person_max.focus();
			return;
		}
	}
	/*
	var isShareDate = $("#shareDate").is(':checked');	//신청날짜 선택시
	if (isShareDate){
		if ( $("#share_date").val() == ""){
			alert("신청날짜를 입력해 주세요.");
			f.share_date.focus();
			return;
		}
	}*/
	f.action = "write_proc.asp";
	f.submit();
}
function doDel(){
	if (confirm("[주의]삭제 하시겠습니까?")) {
		var f = document.del_f;
		f.action = "del_proc.asp";
		f.submit();
	}
}
function chkPersonMax(){
	var isPersonMax = $("#show_item1").is(':checked');	//동반인 선택시
	if (isPersonMax){
		$("#person_max").attr("disabled", false);
	} else {
		$("#person_max").val("");
		$("#person_max").attr("disabled", true);
	}
}
function chkShareDate(){
	var isShareDate = $("#shareDate").is(':checked');	//신청날짜 선택시
	if (isShareDate){
		$("#shareDateArea").css("display","block");
	} else {
		//$("#share_date").val("");
		$("#shareDateArea").css("display", "none");
	}
}
$(document).ready(function(){
	chkPersonMax();
	//chkShareDate();
});
jQuery(document).ready(function() {
	jQuery.timepicker.setDefaults({
		monthNames: ['년 1월','년 2월','년 3월','년 4월','년 5월','년 6월','년 7월','년 8월','년 9월','년 10월','년 11월','년 12월'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		showMonthAfterYear:true,
		dateFormat: 'yy-mm-dd',
		timeFormat: 'hh:mm:ss',
		duration: '',
		showTime: true,
		showSecond: true,
		constrainInput: false,
		stepMinutes: 1,
		stepHours: 1,
		altTimeField: '',
		time24h: true
	});
	jQuery( "#sdate" ).timepicker();
	jQuery( "#edate" ).timepicker();
	}
);
</script>
</head>
<body>

<br>
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">이벤트관리 <%=stitle%></font></strong>
<br>

<form name="del_f" id="del_f" method="post">
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="fname1" value="<%=IMG%>">
<input type="hidden" name="page" value="<%=page%>">
</form>

<form name="write_f" id="write_f" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="fname1" value="<%=IMG%>">
<input type="hidden" name="fname2" value="<%=MAIN_IMG%>">
<input type="hidden" name="fname3" value="<%=CONTENT_IMG%>">
<input type="hidden" name="page" value="<%=page%>">

<table width="800" border="0" cellpadding="3" cellspacing="1" bgcolor="#DFDFDF">
  <tr>
    <td bgcolor="#F3F3F3"><b>제목(10자이내)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="title" size="50" value="<%=TITLE%>">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>이벤트시작일</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="sdate" id="sdate" size="20" value="<%=SDATE%>" style="cursor:pointer;">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>이벤트종료일</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="edate" id="edate" size="20" value="<%=EDATE%>" style="cursor:pointer;">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>응모대상</b></td>
	<td bgcolor="#FFFFFF" height="23">
		<%=strDrawType%>
		<!-- <select name="draw_type">
		  <option value="1" <%'=fnSelect(draw_type,"1","selected")%>>모두</option>
		  <option value="2" <%'=fnSelect(draw_type,"2","selected")%>>후원자</option>
		</select> -->
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>추첨방법</b></td>
	<td bgcolor="#FFFFFF" height="23">
		<%=strWinType%>
		<!-- <select name="win_type">
		  <option value="1" <%'=fnSelect(win_type,"1","selected")%>>직접선정</option>
		  <option value="2" <%'=fnSelect(win_type,"2","selected")%>>댓글 중 자동 추첨</option>
		  <option value="3" <%'=fnSelect(win_type,"3","selected")%>>댓글 중 N번째 선정</option>
		</select> -->
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>공유URL<br>(실제 SNS로 나가는 URL)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="shareUrl" id="shareUrl" size="70" value="<%=SHARE_URL%>">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>요약내용</b></td>
	<td bgcolor="#FFFFFF">
		<textarea name="memo" id="memo" rows="5" cols="55"><%=MEMO%></textarea>
		<!-- <input type="text" name="memo" id="memo" size="70" value="<%=MEMO%>"> -->
	</td>
  </tr>

  <tr>
    <td bgcolor="#F3F3F3"><b>썸네일 이미지(110 X 110)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file1" size="50">
		<%=strImg1%>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>메인 썸네일 이미지(640*170)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file2" size="50">
		<%=strImg2%>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>내용이미지(640 X free)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file3" size="50">
		<%=strImg3%>
	</td>
  </tr>

  <tr>
    <td bgcolor="#F3F3F3"><b>신청노출여부</b></td>
	<td bgcolor="#FFFFFF">
		<input type="checkbox" name="show_item" value="name" <%=fnSelect("name",show_item,"checked")%>>이름
		<input type="checkbox" name="show_item" id="show_item1" value="person" <%=fnSelect("person",show_item,"checked")%> onClick="chkPersonMax();">동반인
		최대
		<select name="person_max" id="person_max">
		  <option value="">선택</option>
		  <%=MakeNumSelBox(1,10,1,PERSON_MAX," 인")%>
		</select>
		<input type="checkbox" name="show_item" value="tel" <%=fnSelect("tel",show_item,"checked")%>>연락처
		<input type="checkbox" name="show_item" value="date" id="shareDate" <%=fnSelect("date",show_item,"checked")%> >신청날짜
		<input type="checkbox" name="show_item" value="content" <%=fnSelect("content",show_item,"checked")%>>신청내용
		<br>
		<!-- <div id="shareDateArea"><b>신청날짜</b> : <input type="text" name="share_date" id="share_date" value="<%'=share_date%>" size="60"><br>
		<b>ex)2012-08-30 18시 공연 / 2012-08-31 18시 공연</b></div> -->
	</td>
  </tr>
</table>
</form>
<br>
<%
If idx = "" Then
%>

<input type="button" value="등록" onClick="doWrite();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">

<%
Else
%>
<input type="button" value="수정" onClick="doWrite();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">
<!-- <input type="button" value="삭제" onClick="doDel();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;"> -->

<%
End if
%>

<input type="button" value="취소" onClick="location.href='list.asp?page=<%=page%>';" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">

<br><br><br><br><br>
</body>
</html>