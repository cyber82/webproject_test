<%@Language="VBScript" CODEPAGE="65001"%>
<%
'*******************************************************************************
'페이지 설명 : 이벤트관리 리스트 삭제
'작성일      : 2012-07-12
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<%

idx = Request("idx")
page = Request("page")

'기본 저장위치
saveDefaultPath = "/images/mobileapp/event/"

'이미지삭제
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
	.ActiveConnection = db
	.CommandText = "dbo.[UP_APP_ADMIN_EVENT]"
	.CommandType = adCmdStoredProc
	.Parameters.Append .CreateParameter("@cmd", adVarChar, adParamInput, 10, "view")
	.Parameters.Append .CreateParameter("@idx", adInteger, adParamInput, 4, idx)
	.Parameters.Append .CreateParameter("@A_PAGE", adInteger, adParamInput, 4, intPage)
	.Parameters.Append .CreateParameter("@A_PAGE_SIZE", adInteger, adParamInput, 4, intPageSize)
	.Parameters.Append .CreateParameter("@st", adVarChar, adParamInput, 50, st)
	.Parameters.Append .CreateParameter("@sw", adVarChar, adParamInput, 50, sw)
	Set Rs = .Execute
	.Parameters.Delete("@cmd")
	.Parameters.Delete("@idx")
	.Parameters.Delete("@A_PAGE")
	.Parameters.Delete("@A_PAGE_SIZE")
	.Parameters.Delete("@st")
	.Parameters.Delete("@sw")
End With
If Not Rs.eof Then
	strImg = Rs("APP_ThumImg")
	strMainImg = Rs("APP_MainThumImg")
	strContentImg = Rs("APP_ContentImg")
End If
'============= 파일 삭제 ==============
If strImg <> "" Then
	arr_fname1 = Split(strImg, "/")
	fname = arr_fname1(Ubound(arr_fname1))

	strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
	strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
	strDefFolder = Server.MapPath(saveDefaultPath) & "\"
	strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
	strDelPath1 = strSaveFolder & fname

	Set fso = CreateObject("Scripting.FileSystemObject")
	'파일 존재시 파일 삭제
	If fso.FileExists(strDelPath1) Then
	   fso.DeleteFile strDelPath1
	End If
	Set fso = Nothing

End if
'============= 파일 삭제 ==============
'============= 파일 삭제 ==============
If strMainImg <> "" Then
	arr_fname1 = Split(strMainImg, "/")
	fname = arr_fname1(Ubound(arr_fname1))

	strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
	strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
	strDefFolder = Server.MapPath(saveDefaultPath) & "\"
	strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
	strDelPath1 = strSaveFolder & fname

	Set fso = CreateObject("Scripting.FileSystemObject")
	'파일 존재시 파일 삭제
	If fso.FileExists(strDelPath1) Then
	   fso.DeleteFile strDelPath1
	End If
	Set fso = Nothing

End if
'============= 파일 삭제 ==============
'============= 파일 삭제 ==============
If strContentImg <> "" Then
	arr_fname1 = Split(strContentImg, "/")
	fname = arr_fname1(Ubound(arr_fname1))

	strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
	strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
	strDefFolder = Server.MapPath(saveDefaultPath) & "\"
	strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
	strDelPath1 = strSaveFolder & fname

	Set fso = CreateObject("Scripting.FileSystemObject")
	'파일 존재시 파일 삭제
	If fso.FileExists(strDelPath1) Then
	   fso.DeleteFile strDelPath1
	End If
	Set fso = Nothing

End if
'============= 파일 삭제 ==============


With oCmd
	.ActiveConnection = db
	.CommandText = "dbo.[UP_APP_ADMIN_EVENT]"
	.CommandType = adCmdStoredProc
	.Parameters.Append .CreateParameter("@cmd", adVarChar, adParamInput, 10, "del")
	.Parameters.Append .CreateParameter("@idx", adInteger, adParamInput, 4, idx)
	.Parameters.Append .CreateParameter("@A_PAGE", adInteger, adParamInput, 4, intPage)
	.Parameters.Append .CreateParameter("@A_PAGE_SIZE", adInteger, adParamInput, 4, intPageSize)
	.Parameters.Append .CreateParameter("@st", adVarChar, adParamInput, 50, st)
	.Parameters.Append .CreateParameter("@sw", adVarChar, adParamInput, 50, sw)
	.Parameters.Append .CreateParameter("@title", adVarChar, adParamInput, 100, title)
	.Parameters.Append .CreateParameter("@sdate", adVarChar, adParamInput, 20, sdate)
	.Parameters.Append .CreateParameter("@edate", adVarChar, adParamInput, 20, edate)
	.Parameters.Append .CreateParameter("@memo", adVarChar, adParamInput, 8000, memo)
	.Parameters.Append .CreateParameter("@img", adVarChar, adParamInput, 200, strImg1)
	.Parameters.Append .CreateParameter("@main_img", adVarChar, adParamInput, 200, strImg2)
	.Parameters.Append .CreateParameter("@content_img", adVarChar, adParamInput, 200, strImg3)
	.Parameters.Append .CreateParameter("@show_item", adVarChar, adParamInput, 100, show_item)
	.Parameters.Append .CreateParameter("@person_max", adInteger, adParamInput, 4, person_max)
	.Parameters.Append .CreateParameter("@share_url", adVarChar, adParamInput, 100, shareUrl)
	.Execute,,adExecuteNoRecords
	.Parameters.Delete("@cmd")
	.Parameters.Delete("@idx")
	.Parameters.Delete("@A_PAGE")
	.Parameters.Delete("@A_PAGE_SIZE")
	.Parameters.Delete("@st")
	.Parameters.Delete("@sw")
	.Parameters.Delete("@title")
	.Parameters.Delete("@sdate")
	.Parameters.Delete("@edate")
	.Parameters.Delete("@memo")
	.Parameters.Delete("@img")
	.Parameters.Delete("@main_img")
	.Parameters.Delete("@content_img")
	.Parameters.Delete("@show_item")
	.Parameters.Delete("@person_max")
	.Parameters.Delete("@share_url")
End With
Set oCmd = Nothing

Call Alert_Go("삭제되었습니다.", "list.asp?page="& page)
%>