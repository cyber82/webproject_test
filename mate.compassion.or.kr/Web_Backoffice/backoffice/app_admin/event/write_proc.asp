<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 영상관리 리스트
'작성일      : 2012-07-12
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/freeaspupload.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%

saveDefaultPath = "/images/mobileapp/event/"

'저장 폴더명
Dim strDateDir : strDateDir = getDateDivDir(0, "\")
strDefFolder = Server.MapPath(saveDefaultPath) & "\"
strSaveFolder = strDefFolder & strDateDir
Call MkDirAll(strSaveFolder)	'폴더 생성

uploadTempDir = strSaveFolder
Set Upload = New FreeASPUpload


'***************** 이미지 저장 *****************************
Upload.Save(uploadTempDir)

for each fileKey in Upload.UploadedFiles.keys

	Select Case fileKey
		Case "file1" : tempFile1 = Upload.UploadedFiles("file1").FileName
		Case "file2" : tempFile2 = Upload.UploadedFiles("file2").FileName
		Case "file3" : tempFile3 = Upload.UploadedFiles("file3").FileName
	End select

Next
'***************** 이미지 저장 *****************************

If tempFile1 <> "" Then

	vFilename = tempFile1						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile1)

	'새이름 생성
	newFilename = MakeFileName() & "."&strExt
	file.name = newFilename

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로
	strFilePath1 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename

End If

If tempFile2 <> "" Then

	vFilename = tempFile2						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile2)

	'새이름 생성
	newFilename = MakeFileName() & "."&strExt
	file.name = newFilename

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로
	strFilePath2 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename

End If

If tempFile3 <> "" Then

	vFilename = tempFile3						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile3)

	'새이름 생성
	newFilename = MakeFileName() & "."&strExt
	file.name = newFilename

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로
	strFilePath3 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename

End If



idx = Upload.Form("idx")
page = Upload.Form("page")
title = Upload.Form("title")
sdate = Upload.Form("sdate")
edate = Upload.Form("edate")
memo = Upload.Form("memo")
show_item = Upload.Form("show_item")
show_item = Replace(show_item," ","")
person_max = Upload.Form("person_max")	'동반인 MAX
shareUrl = Upload.Form("shareUrl")	'공유URL
draw_type =	Upload.Form("draw_type")	'응모대상
win_type = Upload.Form("win_type")		'추첨방법
share_date = Upload.Form("share_date")	'신청날짜

draw_type = 0
win_type = 0

If InStr(show_item, "date") Then
Else
	share_date = ""		'신청날짜 미선택시 공백으로
End if

If InStr(show_item, "person") Then
Else
	person_max = "0"		'동반인을 미선택시 동반인 MAX값은 ""
End if

fname1 = Upload.Form("fname1")	'썸네일(원본)
fname2 = Upload.Form("fname2")	'메인 썸네일(원본)
fname3 = Upload.Form("fname3")	'내용이미지(원본)


Set oCmd = Server.CreateObject("ADODB.Command")

'등록
if idx = "" Then

        sql = "exec UP_APP_ADMIN_EVENT 'insert', 0, '" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "','" &title& "','" &sdate& "','" &edate& "','" &memo& "','" &strFilePath1& "','" &strFilePath2& "','" &strFilePath3& "','" &show_item& "','" &person_max& "','" &shareUrl& "','" &draw_type& "', '" &win_type& "','" &share_date& "'"
        inresult = BoardWriteWS(provWSDL, sql)

'수정
Else


	if strFilePath1 <> "" then
		'============= 기존파일 삭제 ==============
		If fname1 <> "" Then
			arr_fname1 = Split(fname1, "/")
			fname = arr_fname1(Ubound(arr_fname1))

			strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
			strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============
		strImg1 = strFilePath1
	else
		strImg1 = fname1
	end If

	if strFilePath2 <> "" then
		'============= 기존파일 삭제 ==============
		If fname2 <> "" Then
			arr_fname2 = Split(fname2, "/")
			fname = arr_fname2(Ubound(arr_fname2))

			strDateDir1 = arr_fname2(Ubound(arr_fname2)-2) & "\"
			strDateDir2 = arr_fname2(Ubound(arr_fname2)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============
		strImg2 = strFilePath2
	else
		strImg2 = fname2
	end If

	if strFilePath3 <> "" then
		'============= 기존파일 삭제 ==============
		If fname3 <> "" Then
			arr_fname3 = Split(fname3, "/")
			fname = arr_fname3(Ubound(arr_fname3))

			strDateDir1 = arr_fname3(Ubound(arr_fname3)-2) & "\"
			strDateDir2 = arr_fname3(Ubound(arr_fname3)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============
		strImg3 = strFilePath3
	else
		strImg3 = fname3
	end If

	
		sql = "exec UP_APP_ADMIN_EVENT 'edit','"&idx&"','" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "','" &title& "','" &sdate& "','" &edate& "','" &memo& "','" &strImg1& "','" &strImg2& "','" &strImg3& "','" &show_item& "','" &person_max& "','" &shareUrl& "','" &draw_type& "', '" &win_type& "','" &share_date& "'"
        inresult = BoardWriteWS(provWSDL, sql)
   

End If
Set oCmd = Nothing

Response.Redirect "list.asp?page="& page
%>