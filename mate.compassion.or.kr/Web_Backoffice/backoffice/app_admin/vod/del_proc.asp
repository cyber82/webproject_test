<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 영상관리 리스트 삭제
'작성일      : 2012-07-12
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%

idx = Request("idx")
page = Request("page")


'기본 저장위치
saveDefaultPath = "/images/mobileapp/vod/"


'이미지삭제
    sql = "exec UP_APP_ADMIN_VOD 'view','" &idx& "','" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "','" &title& "','" &strFilePath1& "','" &url& "','" &useyn& "'"
    set Rs = RecordsetFromWS(provWSDL, sql)



If Not Rs.eof Then
	strImg = Rs("IMG")
End If
'============= 파일 삭제 ==============
If strImg <> "" Then

	arr_fname1 = Split(strImg, "/")
	fname = arr_fname1(Ubound(arr_fname1))

	strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
	strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
	strDefFolder = Server.MapPath(saveDefaultPath) & "\"
	strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
	strDelPath1 = strSaveFolder & fname

	Set fso = CreateObject("Scripting.FileSystemObject")
	'파일 존재시 파일 삭제
	If fso.FileExists(strDelPath1) Then
	   fso.DeleteFile strDelPath1
	End If
	Set fso = Nothing

End if
'============= 파일 삭제 ==============

    sql = "exec UP_APP_ADMIN_VOD 'del','" &idx& "','" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "','" &title& "','" &strFilePath1& "','" &url& "','" &useyn& "'"
    inresult = BoardWriteWS(provWSDL, sql)


Call Alert_Go("삭제되었습니다.", "list.asp?page="& page)
%>