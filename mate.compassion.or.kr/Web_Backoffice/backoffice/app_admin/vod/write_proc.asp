<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 영상관리 리스트
'작성일      : 2012-07-12
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/freeaspupload.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%

saveDefaultPath = "/images/mobileapp/vod/"

'저장 폴더명
Dim strDateDir : strDateDir = getDateDivDir(0, "\")
strDefFolder = Server.MapPath(saveDefaultPath) & "\"
strSaveFolder = strDefFolder & strDateDir
Call MkDirAll(strSaveFolder)	'폴더 생성

uploadTempDir = strSaveFolder
Set Upload = New FreeASPUpload



'***************** 이미지 저장 *****************************
Upload.Save(uploadTempDir)

for each fileKey in Upload.UploadedFiles.keys

	Select Case fileKey
		Case "file1" : tempFile1 = Upload.UploadedFiles("file1").FileName
	End select

Next
'***************** 이미지 저장 *****************************



If tempFile1 <> "" Then

		vFilename = tempFile1						'파일이름
		strName = Mid(vFilename, 1 , InstrRev(vFilename,".")-1)	'파일명
		strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

		set fso = CreateObject("Scripting.FileSystemObject")
		set file = fso.GetFile(uploadTempDir & "\" & tempFile1)

		'새이름 생성
		newFilename = MakeFileName() & "."&strExt
		file.name = newFilename

		set file = nothing
		set fso = Nothing


	'디비에 저장될 이미지 경로
	strFilePath1 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename

End If



idx = Upload.Form("idx")
page = Upload.Form("page")
title = Upload.Form("title")
url = Upload.Form("url")
useyn = Upload.Form("useyn")

fname1 = Upload.Form("fname1")	'썸네일(원본)


'등록
if idx = "" Then

    sql = "exec UP_APP_ADMIN_VOD 'insert', 0, '" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "','" &title& "','" &strFilePath1& "','" &url& "','" &useyn& "'"
    inresult = BoardWriteWS(provWSDL, sql)
'수정
Else


	if strFilePath1 <> "" then
		'============= 기존파일 삭제 ==============
		If fname1 <> "" Then
			arr_fname1 = Split(fname1, "/")
			fname = arr_fname1(Ubound(arr_fname1))

			strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
			strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============

		strImg = strFilePath1

	else
		strImg = fname1
	end If

    sql = "exec UP_APP_ADMIN_VOD 'edit','" &idx& "','" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "','" &title& "','" &strImg& "','" &url& "','" &useyn& "'"
    inresult = BoardWriteWS(provWSDL, sql)


End If

'Response.Write "<br>=== END ===<br>"

Response.Redirect "list.asp?page="& page
%>