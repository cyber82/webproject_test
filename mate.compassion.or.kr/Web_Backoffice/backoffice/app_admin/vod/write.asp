﻿
<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 영상관리 리스트
'작성일      : 2012-07-12
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%

idx = Request("idx")
page = Request("page")

If idx = "" Then
	stitle = "신규등록"
	USEYN = "1"
Else
	stitle = "수정"

    sql = "exec UP_APP_ADMIN_VOD 'view', '" &idx& "','" &intPage& "','" &intPageSize& "','" &st& "','" &sw& "','" &title& "','" &strFilePath1& "','" &url& "', '" &useyn& "'"
    set Rs = RecordsetFromWS(provWSDL, sql)

	If Not Rs.eof Then
		TITLE = Rs(0)
		TITLE = Replace(TITLE,"""","&quot;")
		IMG = Rs(1)
		URL = Rs(2)
		USEYN = Rs(3)

		If IMG <> "" Then strImg = "<br><img src='"& IMG &"' border='0'>"
	End If

End If

%>
<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript">
function doWrite(){
	var f = document.write_f;
	if (f.title.value == "") {
		alert("제목을 입력해 주세요.");
		f.title.focus();
		return;
	}
	if (f.url.value == "") {
		alert("연결URL을 입력해 주세요.");
		f.url.focus();
		return;
	}
	f.action = "write_proc.asp";
	f.submit();
}
function doDel(){
	if (confirm("[주의]삭제 하시겠습니까?")) {
		var f = document.del_f;
		f.action = "del_proc.asp";
		f.submit();
	}
}
</script>
</head>
<body>


<br>
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">영상관리 <%=stitle%></font></strong>
<br>


<form name="del_f" id="del_f" method="post">
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="page" value="<%=page%>">
</form>

<form name="write_f" id="write_f" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="fname1" value="<%=IMG%>">
<input type="hidden" name="page" value="<%=page%>">

<table width="800" border="0" cellpadding="3" cellspacing="1" bgcolor="#DFDFDF">
  <tr>
    <td bgcolor="#F3F3F3"><b>제목(10자이내)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="title" size="50" value="<%=TITLE%>">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>썸네일 이미지(185*130)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file1" size="50">
		<%=strImg%>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>연결 URL</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="url" size="50" value="<%=URL%>">
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>노출여부</b></td>
	<td bgcolor="#FFFFFF">
		<input type="radio" name="useyn" value="1" <%=fnSelect(USEYN,"1","checked")%>>노출
		<input type="radio" name="useyn" value="0" <%=fnSelect(USEYN,"0","checked")%>>비노출
	</td>
  </tr>
</table>
</form>
<br>
<%
If idx = "" Then
%>

<input type="button" value="등록" onClick="doWrite();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">

<%
Else
%>
<input type="button" value="수정" onClick="doWrite();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">
<input type="button" value="삭제" onClick="doDel();" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">

<%
End if
%>

<input type="button" value="취소" onClick="location.href='list.asp?page=<%=page%>';" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">


</body>
</html>