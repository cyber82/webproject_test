<%@Language="VBScript" CODEPAGE="65001"%>
<%
'*******************************************************************************
'페이지 설명 : 연결내용 리스트
'작성일      : 2012-07-16
'작성자      :
'참고사항    :
'*******************************************************************************
%>

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'M, S, C, E
menu = Request("menu")
cmd = "main_" & LCase(menu)

SQL = "EXEC dbo.[UP_APP_ADMIN_MAIN] @cmd='"& cmd &"'"

set Rs = RecordsetFromWS(provWSDL, SQL)

if Not Rs.eof Then

	AllRec = Rs.GetRows
	For j = 0 To UBound(AllRec,2)

		idx = AllRec(0, j)
		title = AllRec(1, j)

		data_name = data_name & ", """ & escape(title) & """"
		data_val = data_val & ", """ & idx & """"

	Next

end if

data_name = Mid(data_name, 3)
data_val = Mid(data_val, 3)

%>
{
"opt_name": [ <%=data_name%> ],
"opt_val": [ <%=data_val%> ]
}