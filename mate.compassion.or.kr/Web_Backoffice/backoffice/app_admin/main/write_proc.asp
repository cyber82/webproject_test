<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
'*******************************************************************************
'페이지 설명 : 메인 관리 리스트 등록처리
'작성일      : 2012-07-16
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/freeaspupload.asp" -->
<%
saveDefaultPath = "/images/mobileapp/main/"

'저장 폴더명
Dim strDateDir : strDateDir = getDateDivDir(0, "\")
strDefFolder = Server.MapPath(saveDefaultPath) & "\"
strSaveFolder = strDefFolder & strDateDir
Call MkDirAll(strSaveFolder)	'폴더 생성

uploadTempDir = strSaveFolder
Set Upload = New FreeASPUpload


'***************** 이미지 저장 *****************************
Upload.Save(uploadTempDir)

for each fileKey in Upload.UploadedFiles.keys

	Select Case fileKey
		Case "file1" : tempFile1 = Upload.UploadedFiles("file1").FileName
	End select

Next
'***************** 이미지 저장 *****************************


If tempFile1 <> "" Then

		vFilename = tempFile1						'파일이름
		strName = Mid(vFilename, 1 , InstrRev(vFilename,".")-1)	'파일명
		strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

		set fso = CreateObject("Scripting.FileSystemObject")
		set file = fso.GetFile(uploadTempDir & "\" & tempFile1)

		'새이름 생성
		newFilename = MakeFileName() & "."&strExt
		file.name = newFilename

		set file = nothing
		set fso = Nothing


	'디비에 저장될 이미지 경로
	strFilePath1 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename

End If



main_type = Upload.Form("main_type")	'비회원용, 회원용(결연자), 비회원용(일반후원자), 비회원용(웹회원)
menu = Upload.Form("menu")			'연결메뉴
menu_url = Upload.Form("menu_url")	'연결내용 - 각 게시판 번호

fname1 = Upload.Form("fname1")	'이미지 (640*290)

'스토리 선택시 스토리 보기 url
story_url = ""
If menu = "S" Then
	story_url = "/innerSet/boardview.aspx?artIdx="& menu_url
End if


if strFilePath1 <> "" then
	'============= 기존파일 삭제 ==============
	If fname1 <> "" Then
		arr_fname1 = Split(fname1, "/")
		fname = arr_fname1(Ubound(arr_fname1))

		strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
		strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
		strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
		strDelPath1 = strSaveFolder & fname

		Set fso = CreateObject("Scripting.FileSystemObject")
		'파일 존재시 파일 삭제
		If fso.FileExists(strDelPath1) Then
		   fso.DeleteFile strDelPath1
		End If
		Set fso = Nothing

	End if
	'============= 기존파일 삭제 ==============
	strFilePath1 = strFilePath1
else
	strFilePath1 = fname1
end If


sql = "exec UP_APP_ADMIN_MAIN 'insert', '" &main_type& "', '"&strFilePath1&"', '"&menu&"', '"&menu_url&"', '"&story_url&"'"
inresult = BoardWriteWS(provWSDL, sql)


Call Alert_GO("저장되었습니다.", "list.asp")
%>