﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%
'*******************************************************************************
'페이지 설명 : 메인 관리 리스트
'작성일      : 2012-07-16
'작성자      :
'참고사항    :
'*******************************************************************************
%>
<%

	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/app_admin/inc/func.asp" -->
<%


sql = "exec UP_APP_ADMIN_MAIN 'view'"
set Rs = RecordsetFromWS(provWSDL, sql)

If Rs.EOF Then
	blnNewsList = False
Else
	blnNewsList = True
	AllRec = Rs.GetRows
End If
Rs.Close
Set Rs = Nothing
'db.Close

Dim IMG(3), MENU(3), MENU_URL(3)
If blnNewsList Then

	'MAIN_TYPE, IMG, MENU, MENU_URL
	For j = 0 To UBound(AllRec,2)

		Select Case AllRec(0, j)
			Case "NO_MEMBER"
				IMG(0) = AllRec(1, j)
				MENU(0) = AllRec(2, j)
				MENU_URL(0) = AllRec(3, j)
			Case "MEMBER"
				IMG(1) = AllRec(1, j)
				MENU(1) = AllRec(2, j)
				MENU_URL(1) = AllRec(3, j)
			Case "NO_MEMBER_GENERAL"
				IMG(2) = AllRec(1, j)
				MENU(2) = AllRec(2, j)
				MENU_URL(2) = AllRec(3, j)
			Case "NO_MEMBER_WEB"
				IMG(3) = AllRec(1, j)
				MENU(3) = AllRec(2, j)
				MENU_URL(3) = AllRec(3, j)
		End select

	Next

	If IMG(0) <> "" Then strImgUrl1 = "<br><img src='"& IMG(0) &"' border='0'>"
	If IMG(1) <> "" Then strImgUrl2 = "<br><img src='"& IMG(1) &"' border='0'>"
	If IMG(2) <> "" Then strImgUrl3 = "<br><img src='"& IMG(2) &"' border='0'>"
	If IMG(3) <> "" Then strImgUrl4 = "<br><img src='"& IMG(3) &"' border='0'>"
End if

%>
<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="../jquery-1.7.2.min.js"></script>
<script type="text/javascript">
function callMenuURL(f,menu,menu_url){
    var f = eval("document." + f);

	f.menu_url.value="";
	f.menu_url.options.length = 1;

	var bSelect, k = 0;
	var url = 'ax_menu_url_list.asp?menu='+menu;
	$.getJSON(url,
	function(data){
		for (var i=0; i<data.opt_name.length; i++) {
			if (menu_url.toString() == data.opt_val[i].toString()){
				bSelect = true;
			} else bSelect = false;
			f.menu_url.options[++k] = new Option(unescape(data.opt_name[i]), data.opt_val[i], true, bSelect);
		}
		f.menu_url.options.length = ++k;
	});
}

function isChecked(obj){
	var checked = false;

	if (typeof(obj.length) != 'undefined'){
		for (var i=0; i<obj.length; i++)
			if (obj[i].checked)
				checked = true;
	} else {
		checked = obj.checked;
	}
	return checked;
}
function doWrite(f){
	var f = eval("document."+f);
	var menu_checked = isChecked(f.menu);

	if (!menu_checked) {
		alert("연결메뉴를 선택해 주세요.");
		f.menu[0].focus();
		return;
	}
	if (f.menu_url.value == "") {
		alert("연결내용을 선택해 주세요.");
		f.menu_url.focus();
		return;
	}
	f.action = "write_proc.asp";
	f.submit();
}
</script>
</head>
<body>

<br>
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">비회원용 Main</font></strong> <input type="button" value="저장" onClick="doWrite('main_f');" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:22px;cursor:pointer;">
<br>
<form name="main_f" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="main_type" value="NO_MEMBER">
<input type="hidden" name="fname1" value="<%=IMG(0)%>">
<table width="800" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
  <tr>
    <td bgcolor="#F3F3F3" width="150"><b>이미지등록(580*285)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file1" size="30">
		<%=strImgUrl1%>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>연결메뉴</b></td>
	<td bgcolor="#FFFFFF">
		<input type="radio" name="menu" value="M" onClick="callMenuURL('main_f',this.value,'');" <%=fnSelect(MENU(0),"M","checked")%>>영상
		<input type="radio" name="menu" value="S" onClick="callMenuURL('main_f',this.value,'');" <%=fnSelect(MENU(0),"S","checked")%>>스토리
		<input type="radio" name="menu" value="C" onClick="callMenuURL('main_f',this.value,'');" <%=fnSelect(MENU(0),"C","checked")%>>캠페인
		<input type="radio" name="menu" value="E" onClick="callMenuURL('main_f',this.value,'');" <%=fnSelect(MENU(0),"E","checked")%>>이벤트
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>연결내용</b></td>
	<td bgcolor="#FFFFFF">
		<select name="menu_url" id="menu_url">
			<option value="">선택</option>
		</select>
		<script type="text/javascript">callMenuURL('main_f','<%=MENU(0)%>','<%=MENU_URL(0)%>')</script>
	</td>
  </tr>
</table>
</form>

<br>
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">회원용(결연자) Main</font></strong> <input type="button" value="저장" onClick="doWrite('main_f2');" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:22px;cursor:pointer;">
<br>
<form name="main_f2" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="main_type" value="MEMBER">
<input type="hidden" name="fname1" value="<%=IMG(1)%>">
<table width="800" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
  <tr>
    <td bgcolor="#F3F3F3" width="150"><b>이미지등록(640*150)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file1" size="30">
		<%=strImgUrl2%>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>연결메뉴</b></td>
	<td bgcolor="#FFFFFF">
		<input type="radio" name="menu" value="M" onClick="callMenuURL('main_f2',this.value,'');" <%=fnSelect(MENU(1),"M","checked")%>>영상
		<input type="radio" name="menu" value="S" onClick="callMenuURL('main_f2',this.value,'');" <%=fnSelect(MENU(1),"S","checked")%>>스토리
		<input type="radio" name="menu" value="C" onClick="callMenuURL('main_f2',this.value,'');" <%=fnSelect(MENU(1),"C","checked")%>>캠페인
		<input type="radio" name="menu" value="E" onClick="callMenuURL('main_f2',this.value,'');" <%=fnSelect(MENU(1),"E","checked")%>>이벤트
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>연결내용</b></td>
	<td bgcolor="#FFFFFF">
		<select name="menu_url" id="menu_url">
			<option value="">선택</option>
		</select>
		<script type="text/javascript">callMenuURL('main_f2','<%=MENU(1)%>','<%=MENU_URL(1)%>')</script>
	</td>
  </tr>
</table>
</form>

<br>
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">비회원용(일반후원자) Main</font></strong> <input type="button" value="저장" onClick="doWrite('main_f3');" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:22px;cursor:pointer;">
<br>
<form name="main_f3" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="main_type" value="NO_MEMBER_GENERAL">
<input type="hidden" name="fname1" value="<%=IMG(2)%>">
<table width="800" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
  <tr>
    <td bgcolor="#F3F3F3" width="150"><b>이미지등록(640*150)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file1" size="30">
		<%=strImgUrl3%>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>연결메뉴</b></td>
	<td bgcolor="#FFFFFF">
		<input type="radio" name="menu" value="M" onClick="callMenuURL('main_f3',this.value,'');" <%=fnSelect(MENU(2),"M","checked")%>>영상
		<input type="radio" name="menu" value="S" onClick="callMenuURL('main_f3',this.value,'');" <%=fnSelect(MENU(2),"S","checked")%>>스토리
		<input type="radio" name="menu" value="C" onClick="callMenuURL('main_f3',this.value,'');" <%=fnSelect(MENU(2),"C","checked")%>>캠페인
		<input type="radio" name="menu" value="E" onClick="callMenuURL('main_f3',this.value,'');" <%=fnSelect(MENU(2),"E","checked")%>>이벤트
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>연결내용</b></td>
	<td bgcolor="#FFFFFF">
		<select name="menu_url" id="menu_url">
			<option value="">선택</option>
		</select>
		<script type="text/javascript">callMenuURL('main_f3','<%=MENU(2)%>','<%=MENU_URL(2)%>')</script>
	</td>
  </tr>
</table>
</form>

<br>
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">비회원용(웹회원) Main</font></strong> <input type="button" value="저장" onClick="doWrite('main_f4');" style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:22px;cursor:pointer;">
<br>
<form name="main_f4" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="main_type" value="NO_MEMBER_WEB">
<input type="hidden" name="fname1" value="<%=IMG(3)%>">
<table width="800" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
  <tr>
    <td bgcolor="#F3F3F3" width="150"><b>이미지등록(640*150)</b></td>
	<td bgcolor="#FFFFFF">
		<input type="file" name="file1" size="30">
		<%=strImgUrl4%>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>연결메뉴</b></td>
	<td bgcolor="#FFFFFF">
		<input type="radio" name="menu" value="M" onClick="callMenuURL('main_f4',this.value,'');" <%=fnSelect(MENU(3),"M","checked")%>>영상
		<input type="radio" name="menu" value="S" onClick="callMenuURL('main_f4',this.value,'');" <%=fnSelect(MENU(3),"S","checked")%>>스토리
		<input type="radio" name="menu" value="C" onClick="callMenuURL('main_f4',this.value,'');" <%=fnSelect(MENU(3),"C","checked")%>>캠페인
		<input type="radio" name="menu" value="E" onClick="callMenuURL('main_f4',this.value,'');" <%=fnSelect(MENU(3),"E","checked")%>>이벤트
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>연결내용</b></td>
	<td bgcolor="#FFFFFF">
		<select name="menu_url" id="menu_url">
			<option value="">선택</option>
		</select>
		<script type="text/javascript">callMenuURL('main_f4','<%=MENU(3)%>','<%=MENU_URL(3)%>')</script>
	</td>
  </tr>
</table>
</form>

<br><br><br><br><br>