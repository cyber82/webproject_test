﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

	keyfield = request("keyfield")	
	key = request("key")	
%>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>


<body leftmargin="0" topmargin="0">

<script type="text/javascript" charset='euc-kr'>
<!--
    function trColor(sw, idx)
    {
        var id = document.getElementById("dataTr_" + idx);

        if (sw == "1") {
            id.style.backgroundColor = "#ddd";
        }
        else if (sw == "0") {
            id.style.backgroundColor = "#fff";
        }
    }
//-->
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9"><!--#include file="title.asp"//--></font></strong></td>
                      <td align="right">
                        <a href="excel.asp?keyfield=<%=keyfield%>&key=<%=key%>"><strong><font color="#3788D9">엑셀 다운로드</font></strong></a>
                      </td>
                    </tr>
                </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                      <td width="7%" height="25" align="center">번호</td>
					  <td width="15%" align="center">후원자아이디</td>
                      <td width="10%" align="center">후원자명</td>
                      <td width="15%" align="center">UCC</td>
                      <td width="15%" align="center">지원서 파일</td>
                      <td width="13%" align="center">핸드폰</td>
                      <td width="10%" align="center">연락가능시간</td>
                      <td width="15%" align="center">가입일자</td>
                    </tr>


            <!------- 게시판 목록 구하기 시작 ----------> 
            <%
                sql = " select "
                sql = sql & " idx, up_file2, special, gyegi, hakryuk, name, brithday, sponsor, hand, email, church, zip, [address], content, ucc, tel_call_time, " 
                sql = sql & " convert(varchar, writeday, 120) as writeday "
                sql = sql & " from BAND "

                if key = "" then
	                sql = sql & " where name <> '' order by writeday desc"
                else
	                sql = sql & " where name <> '' and "&keyfield&" like '%"&key&"%' order by writeday desc"
                end if

                'set rs = server.CreateObject("adodb.recordset")
                'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
 
                'response.Write sql
                'response.End  
                Set rs = RecordsetFromWS(provWSDL, sql)

                'totalcount
                sqlCnt = " select "
                sqlCnt = sqlCnt & " count(idx) as totalCnt " 
                sqlCnt = sqlCnt & " from BAND "
                  
                if key = "" then
	                sqlCnt = sqlCnt & " where name <> '' "
                else
	                sqlCnt = sqlCnt & " where name <> '' and "&keyfield&" like '%"&key&"%' "
                end if

                'response.Write sqlCnt
                'response.End                  
                Set rsCnt = RecordsetFromWS2(provWSDL, sqlCnt)
            %>					
					
				<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
					<tr> 
					  <td height="22" colspan="6" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
				<% else '데이터가 있다면
                    
                    rs.PageSize = 20 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

					totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
                    ''totalpage = rsCnt("totalCnt")

					rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
					endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
					''totalpage = Int(rsCnt("totalCnt") / rs.PageSize) + 1

                    'Response.Write rsCnt("totalCnt") & "<br/>"
                    'Response.Write rs.PageSize & "<br/>"
                    'Response.Write rs.PageCount & "<br/>"
                    'Response.End

                    if endpage > totalpage then
						endpage = totalpage
					end If
                    
					if request("page")="" then
						npage = 1
					else
						npage = CInt(request("page"))
					end if
						
					trd = rs.RecordCount
                    ''trd = rsCnt("totalCnt")
					j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)

                    'Response.Write("totalpage : " & totalpage & "<br/>")
                    'Response.Write("startpage : " & startpage & "<br/>")
                    'Response.Write("endpage : " & endpage & "<br/>")
                    'Response.Write("rs.PageSize : " & rs.PageSize & "<br/>")
                    'Response.Write("npage : " & npage & "<br/>")
                    'Response.Write("trd : " & trd & "<br/>")
				%>
				<%
				    i = 1
					do until rs.EOF or i > rs.PageSize
				%>
                    <tr id="dataTr_<%=rs("idx") %>" bgcolor="#FFFFFF" onmouseover='javascript:trColor(1, "<%=rs("idx") %>");' onmouseout='javascript:trColor(0, "<%=rs("idx") %>");'> 
                      <td height="25" align="center"><%=j%></td>
                      <td align="center"><%=rs("sponsor") %></td>
					  <td align="center"><%=rs("name") %></td>
                      <td align="center">
                        <% '수정 2013-07-01 %>
                        <% if Right(rs("ucc"), 3) = "zip" then %>
                            <a href='/upload/band/<%=rs("ucc") %>'><%=rs("ucc") %></a>
                        <% else %>
                            <a href='../inc/download.asp?filePath=/upload/band/&fileName=<%=rs("ucc") %>'><%=rs("ucc") %></a>
                        <% end if %>
                      </td>
                      <td align="center">
                        <% '수정 2013-07-01 %>
                        <% if Right(rs("up_file2"), 3) = "zip" then %>
                            <a href='/upload/band/<%=rs("up_file2") %>'><%=rs("up_file2") %></a>
                        <% else %>
                            <a href='../inc/download.asp?filePath=/upload/band/&fileName=<%=rs("up_file2") %>'><%=rs("up_file2") %></a>
                        <% end if %>
                      </td>
                      <td align="center"><%=rs("hand") %></td>
                      <td align="center"><%=rs("tel_call_time") %></td>
                      <!-- left 사용시 변함 --> <% '=left(rs("writeday"),10)%>
                      <td align="center"><%=rs("writeday") %></td>
                    </tr>

            <%
                'Response.Write("j : " & j & "<br/>")
                'Response.Write("i : " & i & "<br/>")

                j=j-1
				rs.MoveNext ' 다음 레코드로 이동한다.
			    i=i+1
				loop '레코드의 끝까지 loop를 돈다.
			%> 
            <% 
         	    rs.Close
	            Set rs = nothing 

                end if
            %>               
            <!------- 게시판 목록 구하기 끝 ---------->  
                                    
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <!--<tr>
                <td align="right"><a href="write.asp?page=<%'=page%>" onfocus="this.blur()"><img src="../img/btn17.gif" width="52" height="20" border="0"></a></td>
              </tr>-->
              <tr> 
                <td align="center">
					<% if cint(startpage)<>cint(1) then%> 
				         <a href="list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [이전]</a>
			        <%end if%> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then%>
				         [<%=page%>]
		             <%else%>
					     <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"> <%=i%> </a>
		             <%end if%>
				   <% next%>

				   <% if cint(endpage)<>cint(totalpage) then%>
			             <a href="list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [다음]</a>
			       <% end if%>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
			  
              <tr> 
                <td align="center"><table width="30%" border="0" cellspacing="2" cellpadding="0">
					<form name="searchForm" action="list.asp" method="post" onSubmit="return searchSendit();">	
                    <tr> 
                      <td>
                        <select name="keyfield" style="height:19px;">
                            <option value="name">후원자명</option>
                            <option value="sponsor">후원자아이디</option>
                            <option value="hand">핸드폰</option>
                        </select>
                      </td>
                      <td><input name="key" type="text" class="text"></td>
                      <td><input type="submit" name="Submit" value="찾기"></td>
                    </tr>
					</form>
                  </table></td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
