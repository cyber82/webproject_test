<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<% Response.Buffer = true %>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

    idx = request("idx")
	keyfield = request("keyfield")	
	key = request("key")	
%>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%
  
sql = "select * from BAND where idx = " & idx

'set rs = server.CreateObject("adodb.recordset")
'rs.Open sql, db
Set rs = RecordsetFromWS(provWSDL, sql)
 
 
if rs("idx") <> "" AND Session("compassion_id") = "compassion" then '운영자가 일치할 경우
 
   sql = "delete from BAND"
   sql = sql & " where idx = " & idx
   'db.Execute sql
   inresult = BoardWriteWS(provWSDL, sql)


  if inresult <> "10" then

%>

    <script type="text/javascript" charset='euc-kr'>
    <!--
        alert('<%=inresult %>');
        history.back();
    //-->
    </script>

<%
  end if
   
   rs.Close
   set rs = nothing
   'set db = nothing
%>

    <script type="text/javascript" charset='euc-kr'>
        alert("삭제되었습니다.");
        location = "list.asp?page=<%=page%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>"
    </script>

<% 
 else
%>

    <script type="text/javascript" charset='euc-kr'>
        alert("운영자 회원이 일치하지 않습니다.");
        history.back();
    </script>

<%end if%>
