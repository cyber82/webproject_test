<% Response.Buffer=true %>
<%
	page = request("page")
	keyfield = request("keyfield")	
	key = request("key")
%>
<% 
 set UploadForm = server.CreateObject("SiteGalaxyUpload.Form")
  'sitegalaxyupload 개체를 생성한다....
  ' sitegalaxyupload 를 사용할때는 request 대신에 uploadform을 사용한다..중요...
  'write.asp 에서 넘긴 값들을 받아온다.....

  r_max_code = uploadform("r_max_code")
  r_middle_code = uploadform("r_middle_code")
  r_min_code = uploadform("r_min_code")
  title = UploadForm("title")
  bang_year = UploadForm("bang_year")
  bang_month = UploadForm("bang_month")
  bang_day = UploadForm("bang_day")
  bang_am = UploadForm("bang_am")
  bang_hour = UploadForm("bang_hour")
  bang_min = UploadForm("bang_min")
  jangru = uploadform("jangru")
  chulyeon = uploadform("chulyeon")
  jejakjin = uploadform("jejakjin")
  bangsongsa = uploadform("bangsongsa")
  point  = uploadform("point")	
  series  = uploadform("series")	
  deug_kind  = uploadform("deug_kind")	
  movie  = uploadform("movie")	
  content  = uploadform("content")	
  b_filename  = uploadform("b_filename")	
  b_filename2  = uploadform("b_filename2")	
  b_filename3  = uploadform("b_filename3")	
  chu_yeo  = uploadform("chu_yeo")	
  hd_link  = uploadform("hd_link")	
  sd_link  = uploadform("sd_link")	
  
  title=replace(title,"'","''") '쿼리에 '가 들어가면 에러가 나기 때문에 그것을 replace처리 합니다.
  content=replace(content,"'","''")
 
  
  '**********************************************************
  '                 Upload File 이 있을때의 처리
  '**********************************************************
  
   if len(b_filename) > 0 then '파일의 이름이 존재할경우.....
      b_filesize=UploadForm("b_filename").size '파일의 크기를 저장
      maxsize=Cint(20) * 1024 
      
      set fc = createobject("Scripting.FileSystemObject")  'fs 라는 인스턴스를 생성 

      storedir =  "c:\inetpub\wwwroot\pdsfile\" '파일이 저장될 절대경로를 설정
      attach_file=UploadForm("b_filename").filepath '파일이 저장될 경로를 지정
      b_filename=mid(attach_file, instrrev(attach_file,"\")+1) '경로명을 제외한 파일명을 추출 
      strname=mid(b_filename, 1, instrrev(b_filename, ".")-1) ' file 명에서 이름과 확장자를 분리
      strext=mid(b_filename, instrrev(b_filename, ".")+1) '확장자를얻는다
      
      strfilename=storedir & b_filename ' 저장할 이름의 전체 path를 만듭니다. ex) c:\inetpub\upload\write.asp
      
      fexist = true
      count = 0
      
      do while fexist ' 파일이 중복될경우 이름을 다시 지정 - 파일이름 뒤에 숫자를 붙여서 업로드
        if(fc.FileExists(strfilename)) then
             count = count+1 '파일이름에 숫자를 붙인 새로운 파일 이름 생성
             b_filename=strname&"-"&count&"."&strext
             strfilename=storedir & b_filename
         else
            fexist = false
         end if
       
       loop
          UploadForm("b_filename").saveas strfilename ' 콤포넌트에 의해 실제 파일을 업로드
    end if
    
   '****************************************************
   '    Upload 할 파일이 없을때 적절한 값으로 대체   
   '****************************************************
   if b_filename = "" then
       b_filesize=0
       b_filename=""
   end if
   
   '*****************************************************
   '           file size변환
   '*****************************************************            
      
     temp = clng(filesize)
        if temp >1024 then   '1024byte(1kbyte) 보다 크면
            temp = temp / 1024
            b_filesize = cstr(cint(temp)) & "k"
         end if 







  '**********************************************************
  '                 Upload File 이 있을때의 처리 - 2
  '**********************************************************
  
   if len(b_filename2) > 0 then '파일의 이름이 존재할경우.....
      
      set fc = createobject("Scripting.FileSystemObject")  'fs 라는 인스턴스를 생성 
    
      storedir =  "c:\inetpub\wwwroot\pdsfile\" '파일이 저장될 절대경로를 설정
       'ex) storedir = "c:\pdsdata\"
      attach_file=UploadForm("b_filename2").filepath '파일이 저장될 경로를 지정
      b_filename2=mid(attach_file, instrrev(attach_file,"\")+1) '경로명을 제외한 파일명을 추출 
      strname=mid(b_filename2, 1, instrrev(b_filename2, ".")-1) ' file 명에서 이름과 확장자를 분리
      strext=mid(b_filename2, instrrev(b_filename2, ".")+1) '확장자를얻는다
      
      strfilename=storedir & b_filename2 ' 저장할 이름의 전체 path를 만듭니다. ex) c:\inetpub\upload\write.asp
      
      fexist = true
      count = 0
      
      do while fexist ' 파일이 중복될경우 이름을 다시 지정 - 파일이름 뒤에 숫자를 붙여서 업로드
        if(fc.FileExists(strfilename)) then
             count = count+1 '파일이름에 숫자를 붙인 새로운 파일 이름 생성
             b_filename2=strname&"-"&count&"."&strext
             strfilename=storedir & b_filename2
         else
            fexist = false
         end if
       
       loop
          UploadForm("b_filename2").saveas strfilename ' 콤포넌트에 의해 실제 파일을 업로드
    end if
    
   '****************************************************
   '    Upload 할 파일이 없을때 적절한 값으로 대체   
   '****************************************************
   if b_filename2 = "" then
       b_filename2=""
   end if



  '**********************************************************
  '                 Upload File 이 있을때의 처리 - 3
  '**********************************************************
  
   if len(b_filename3) > 0 then '파일의 이름이 존재할경우.....
      
      set fc = createobject("Scripting.FileSystemObject")  'fs 라는 인스턴스를 생성 
    
      storedir =  "c:\inetpub\wwwroot\pdsfile\" '파일이 저장될 절대경로를 설정
       'ex) storedir = "c:\pdsdata\"
      attach_file=UploadForm("b_filename3").filepath '파일이 저장될 경로를 지정
      b_filename3=mid(attach_file, instrrev(attach_file,"\")+1) '경로명을 제외한 파일명을 추출 
      strname=mid(b_filename3, 1, instrrev(b_filename3, ".")-1) ' file 명에서 이름과 확장자를 분리
      strext=mid(b_filename3, instrrev(b_filename3, ".")+1) '확장자를얻는다
      
      strfilename=storedir & b_filename3 ' 저장할 이름의 전체 path를 만듭니다. ex) c:\inetpub\upload\write.asp
      
      fexist = true
      count = 0
      
      do while fexist ' 파일이 중복될경우 이름을 다시 지정 - 파일이름 뒤에 숫자를 붙여서 업로드
        if(fc.FileExists(strfilename)) then
             count = count+1 '파일이름에 숫자를 붙인 새로운 파일 이름 생성
             b_filename3=strname&"-"&count&"."&strext
             strfilename=storedir & b_filename3
         else
            fexist = false
         end if
       
       loop
          UploadForm("b_filename3").saveas strfilename ' 콤포넌트에 의해 실제 파일을 업로드
    end if
    
   '****************************************************
   '    Upload 할 파일이 없을때 적절한 값으로 대체   
   '****************************************************
   if b_filename3 = "" then
       b_filename3=""
   end if





%>

<!--#include file="../../include/dbcon.asp"-->
<%       

	chucheon = "0"


If hd_link = "" then
  hd_link = b_filename2
Else
  hd_link = hd_link
End If

If sd_link = "" then
  sd_link = b_filename3
Else
  sd_link = sd_link
End If

  
 '********테이블에 저장한다.**************** 
 sql = "insert into contents (max_code, middle_code, min_code, chu_yeo, title, bang_year, bang_month, bang_day, bang_am, bang_hour, bang_min, jangru,"
  sql = sql & "chulyeon, jejakjin, bangsongsa, series, movie, content, chucheon, deug_kind, point, hd_link, sd_link, b_filename) values "
  sql = sql & "('" & r_max_code & "'"
  sql = sql & ",'" & r_middle_code & "'"
  sql = sql & ",'" & r_min_code & "'"
  sql = sql & ",'" & chu_yeo & "'"
  sql = sql & ",'" & title & "'"
  sql = sql & ",'" & bang_year & "'"
  sql = sql & ",'" & bang_month & "'"
  sql = sql & ",'" & bang_day & "'"
  sql = sql & ",'" & bang_am & "'"
  sql = sql & ",'" & bang_hour & "'"
  sql = sql & ",'" & bang_min & "'"
  sql = sql & ",'" & jangru & "'"
  sql = sql & ",'" & chulyeon & "'"
  sql = sql & ",'" & jejakjin & "'"
  sql = sql & ",'" & bangsongsa & "'"
  sql = sql & ",'" & series & "'"
  sql = sql & ",'" & movie & "'"
  sql = sql & ",'" & content & "'"
  sql = sql & ",'" & chucheon & "'"
  sql = sql & ",'" & deug_kind & "'"
  sql = sql & ",'" & point & "'"
  sql = sql & ",'" & hd_link & "'"
  sql = sql & ",'" & sd_link & "'"
  sql = sql & ",'" & b_filename & "')"
 
' response.write sql
  
  db.Execute sql

   

  %>

<script language="javascript">
  location="list.asp?page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>"
</script>