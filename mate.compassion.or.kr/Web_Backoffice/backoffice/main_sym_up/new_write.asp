﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"
 
'수정 2013-02-27
provWSDL = session("serviceUrl")
%>


<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  
	
%>

<%
'수정 2013-08-13  스토리 앨범 추가
sql = "select top 1 idx from iamcompassionWeb.dbo.ssb_article where board_id in ('child', 'sponsor', 'vision', 'prayer', 'history', 'movie') and del_flag = 0 order by idx desc"
'set rs = db.Execute(sql)
set rs = RecordsetFromWS(provWSDL, sql)

if rs.BOF or rs.EOF then ' 만일 레코드가 없다면
    ssb_idx = 0
else '데이터가 있다면
    ssb_idx = rs(0)
end if

sql2 = "select top 1 article_idx from compassweb4.dbo.main_ssb where del_flag = 0 order by article_idx desc"
'set rs2 = db.Execute(sql2)
set rs2 = RecordsetFromWS(provWSDL, sql2)

if rs2.BOF or rs2.EOF then ' 만일 레코드가 없다면
    article_idx = 0
else '데이터가 있다면
    article_idx = rs2(0)
end if

'Response.Write(CInt(ssb_idx))
'Response.Write(CInt(article_idx))
'Response.End()

if CInt(ssb_idx) > CInt(article_idx) then
'최신글 가져오기 저장 테이블 main_ssb

Dim sql_save
sql_save = ""

    '수정 2013-08-13
    if CInt(article_idx) > 0 then '글이 하나라도 있을때
        sql_save = " INSERT INTO compassweb4.dbo.main_ssb "
        sql_save = sql_save &" (article_idx, board_id, board_group, view_order, img_thumbnail, title, content, [user_id], [user_name], user_PW "
        sql_save = sql_save &" ,ip_address, del_flag, notice, reg_date) "
        sql_save = sql_save &" SELECT idx, board_id, board_group, 0 as view_order, Cast(idx as nvarchar) + '.jpg' as img_thumbnail, title, body as content, "
        sql_save = sql_save &" [user_id], [user_name], user_PW, ip_address, del_flag, notice, view_date as reg_date FROM "
        sql_save = sql_save &" 	(select top 100 percent ar.idx, ar.board_id, ar.board_group, ar.view_num, ar.title, substring(ar.body, 0, 2000) as body, ar.view_count, ar.ref, "
        sql_save = sql_save &"		ar.step, ar.lev, ar.user_id, ar.user_name, ar.user_PW, ar.publish, ar.status, ar.ans_user_id, ar.ans_user_name, "
        sql_save = sql_save &"		ar.ans_title, ar.ans_body, ar.del_flag, ar.notice, ar.reg_date, ar.ans_reg_date, ar.ip_address, ar.view_date, "
        sql_save = sql_save &"		ar.opt1, ar.opt2, ar.yn1, ar.yn2, at.idx as att_idx, at.article_idx, at.dvn, at.ori_file_name, at.saved_file_name, "
        sql_save = sql_save &"		at.file_ext, at.file_size, at.img_flag, at.width, at.height "
        sql_save = sql_save &"	from [iamcompassionWeb].[dbo].[ssb_article] ar left outer join [iamcompassionWeb].[dbo].[ssb_att] at on at.article_idx = ar.idx  "
        sql_save = sql_save &"	where ar.board_id in ('child', 'sponsor', 'vision', 'prayer', 'history', 'movie') and ar.del_flag = 0 and ar.idx > " & CInt(article_idx)
        sql_save = sql_save &" order by ar.view_date desc, ar.idx desc) A "
        sql_save = sql_save &" ORDER BY VIEW_DATE ASC, IDX ASC "
    else '등록된 글이 없을때
        sql_save = " INSERT INTO compassweb4.dbo.main_ssb "
        sql_save = sql_save &" (article_idx, board_id, board_group, view_order, img_thumbnail, title, content, [user_id], [user_name], user_PW "
        sql_save = sql_save &" ,ip_address, del_flag, notice, reg_date) "
        sql_save = sql_save &" SELECT idx, board_id, board_group, 0 as view_order, Cast(idx as nvarchar) + '.jpg' as img_thumbnail, title, body as content, "
        sql_save = sql_save &" [user_id], [user_name], user_PW, ip_address, del_flag, notice, view_date as reg_date FROM "
        sql_save = sql_save &" 	(select top 3 ar.idx, ar.board_id, ar.board_group, ar.view_num, ar.title, substring(ar.body, 0, 2000) as body, ar.view_count, ar.ref, "
        sql_save = sql_save &"		ar.step, ar.lev, ar.user_id, ar.user_name, ar.user_PW, ar.publish, ar.status, ar.ans_user_id, ar.ans_user_name, "
        sql_save = sql_save &"		ar.ans_title, ar.ans_body, ar.del_flag, ar.notice, ar.reg_date, ar.ans_reg_date, ar.ip_address, ar.view_date, "
        sql_save = sql_save &"		ar.opt1, ar.opt2, ar.yn1, ar.yn2, at.idx as att_idx, at.article_idx, at.dvn, at.ori_file_name, at.saved_file_name, "
        sql_save = sql_save &"		at.file_ext, at.file_size, at.img_flag, at.width, at.height "
        sql_save = sql_save &"	from [iamcompassionWeb].[dbo].[ssb_article] ar left outer join [iamcompassionWeb].[dbo].[ssb_att] at on at.article_idx = ar.idx  "
        sql_save = sql_save &"	where ar.board_id in ('child', 'sponsor', 'vision', 'prayer', 'history', 'movie') and ar.del_flag = 0 and ar.reg_date > dateadd(day, -7, getdate()) "
        sql_save = sql_save &" order by ar.view_date desc, ar.idx desc) A "
        sql_save = sql_save &" ORDER BY VIEW_DATE ASC, IDX ASC "
    end if 

    '글등록
    'db.Execute(sql_save)
    inresult = BoardWriteWS(provWSDL, sql_save)

    response.write "<script>alert('등록되었습니다.');</script>"
    response.write "<script>location.href='list.asp';</script>"
else 

    response.write "<script>alert('최신글이 없습니다. \r\n더이상 등록이 되지 않습니다.');</script>"
    response.write "<script>location.href='list.asp';</script>"
end if

%>