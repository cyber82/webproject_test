﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->
<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%
    '전체 문자 치환
    Function eregi_replace(pattern, replace, text)
        Dim eregObj
        Set eregObj = New RegExp
        eregObj.Pattern = pattern '//패턴설정
        eregObj.IgnoreCase = True '//대소문자 구분 여부
        eregObj.Global = True '//전체 문서에서 검색
        eregi_replace = eregObj.Replace(text, replace) '//Replace String
    End Function

    '정규식 문자 치환
	Function strip_tags(str)
		Dim content
        content = str
        content = eregi_replace("[<][a-z|A-Z|/](.|\n)*?[>]", "", content)
        content = eregi_replace("&nbsp;", "", content)
		strip_tags = content
	End Function

  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  
%>

<body leftmargin="0" topmargin="0">
<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9">메인공감 리스트 (* 순서는 높을수록 메인에 위쪽으로 올라갑니다.)</font></strong></td>
                      <td align="right">
                <%
                    con_sql = "select code from main_config where field='main_ssb'"
  
                    set cs = RecordsetFromWS(provWSDL, con_sql)
                    if cs.EOF OR cs.BOF then
                    code = ""
                    else
                    code = cs(0)
                    end if
                %>
                        <script type="text/javascript">
                        <!--
                            function configBtn() {
                                var code = document.getElementById("code");
                                var sel_radio1 = document.getElementById("sel_radio1");
                                var sel_radio2 = document.getElementById("sel_radio2");
                                var f = document.form1;

                                if (sel_radio1.checked == true) {
                                    code.value = "default";
                                }
                                else if (sel_radio2.checked == true) {
                                    code.value = "order";
                                }
                                else {
                                    code.value = "";
                                }

                                //alert(code.value);

                                f.action = "edit.asp";
                                f.method = "post";
                                f.submit();
                            }
                        //-->
                        </script>
                        <input type="radio" name="sel_radio" id="sel_radio1" value="default" <% if code = "default" then %>checked<% end if %> /> 기본형
                        <input type="radio" name="sel_radio" id="sel_radio2" value="order" <% if code = "order" then %>checked<% end if %> /> 순서형
                        <input type="button" name="Button1" id="Button1" value="적용" onclick="javascript:configBtn();"/>
                        &nbsp;&nbsp;
                        <a href="new_write.asp" target="_self"><strong><font color="#3788D9">최신글 가져오기</font></strong></a>
                      </td>
                    </tr>
                </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                      <td width="5%" height="25" align="center">번호</td>
					  <td width="10%" align="center">썸네일</td>
                      <td width="10%" align="center">공감</td>
                      <td width="15%" align="center">제목</td>
                      <td width="20%" align="center">내용</td>
                      <td width="10%" align="center">순서</td>
                      <td width="10%" align="center">IP</td>
                      <td width="5%" align="center">공지<br />여부</td>
                      <td width="5%" align="center">삭제<br />여부</td>
                      <td width="10%" align="center">등록일</td>
                      
                    </tr>


                <%
                    sql = "select * from main_ssb order by view_order desc, reg_date desc"

                    set rs = RecordsetFromWS(provWSDL, sql)

                    'set rs = server.CreateObject("adodb.recordset")
                    rs.PageSize=10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.
                    'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.

                    '추가 2013-06-05
                    'totalcount
                    sqlCnt = " SELECT count(idx) "
                    sqlCnt = sqlCnt & " FROM main_ssb "
                                        
                    'response.Write sqlCnt
                    'response.End
                    Set rsCnt = RecordsetFromWS2(provWSDL, sqlCnt)
                %>					
					
			    <% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면%>
					<tr> 
					  <td height="22" colspan="10" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
				<% else '데이터가 있다면
   
                    rs.PageSize = 10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

					totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
                    ''totalpage = rsCnt("totalCnt")

					rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
					endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
					''totalpage = Int(rsCnt("totalCnt") / rs.PageSize) + 1

                    if endpage > totalpage then
						endpage = totalpage
					end If
                    
					if request("page")="" then
						npage = 1
					else
						npage = CInt(request("page"))
					end if
						
					trd = rs.RecordCount
                    ''trd = rsCnt("totalCnt")
					j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)

                    'Response.Write("totalpage : " & totalpage & "<br/>")
                    'Response.Write("startpage : " & startpage & "<br/>")
                    'Response.Write("endpage : " & endpage & "<br/>")
                    'Response.Write("rs.PageSize : " & rs.PageSize & "<br/>")
                    'Response.Write("npage : " & npage & "<br/>")
                    'Response.Write("trd : " & trd & "<br/>")
				%>
				<%
					i=1
					do until rs.EOF or i > rs.PageSize

                    if rs("board_id") = "child" then
                    board_name = "컴패션 스토리"
                    end if 

                    if rs("board_id") = "sponsor" then
                    board_name = "행복한 후원자"
                    end if 

                    if rs("board_id") = "vision" then
                    board_name = "비전트립 나눔"
                    end if 

                    if rs("board_id") = "prayer" then
                    board_name = "컴패션 양육일기"
                    end if 

                    if rs("board_id") = "history" then
                    board_name = "컴패션 가이드"
                    end if 

                    '추가 2013-08-13
                    if rs("board_id") = "movie" then
                    board_name = "스토리 앨범"
                    end if

                    thumbnail = rs("img_thumbnail")
                    
                    '수정 2013-08-13
                    host = request.ServerVariables("HTTP_HOST")
                    if host = "admin.compassionko.org" then
                        imageSrc = "http://story.compassionko.org/ssBoard/thumbnail/" & thumbnail
                    else
                        imageSrc = "http://story.iamcompassion.or.kr/ssBoard/thumbnail/" & thumbnail
                    end if
                    
                    'response.write(host)
                         
                    content = rs("content")
                    content = replace(content, chr(10), "<br/>")
                    content = replace(content, chr(13), "<br/>")
                    content = replace(content, "&lt;", "<")
                    content = replace(content, "&gt;", ">")
                    content = replace(content, "&amp;", "&")
                    content = replace(content, "&apos;", "'")
                    content = replace(content, "&quot;", """")


                    content = strip_tags(content)
                    contents = Mid(content, 1, 200)
				%>

                    <tr bgcolor="#FFFFFF"> 
                      <td height="25" align="center"><%=j %></td>
                      <td align="center"><img src="<%=imageSrc %>" border="0" width="68" height="68" /></td>
                      <td align="center"><%=board_name %></td>
                      <td align="center"><%=rs("title") %></td>
                      <td align="center">
                        <script type="text/javascript">
                        <!--
                            function conView<%=j%>()
                            {
                                var span_contents = document.getElementById("span_contents"+<%=j%>);
                                var contents = document.getElementById("contents"+<%=j%>);
                                var sel1 = document.getElementById("btn_con1"+<%=j%>);
                                var sel2 = document.getElementById("btn_con2"+<%=j%>);
                                var sel3 = document.getElementById("btn_con3"+<%=j%>);

                                span_contents.style.display = "none";
                                contents.style.display = "block";
                                sel1.style.display = "none";
                                sel2.style.display = "block";
                                sel3.style.display = "block";
                            }

                            function conNone<%=j%>()
                            {
                                var span_contents = document.getElementById("span_contents"+<%=j%>);
                                var contents = document.getElementById("contents"+<%=j%>);
                                var sel1 = document.getElementById("btn_con1"+<%=j%>);
                                var sel2 = document.getElementById("btn_con2"+<%=j%>);
                                var sel3 = document.getElementById("btn_con3"+<%=j%>);

                                span_contents.style.display = "block";
                                contents.style.display = "none";
                                sel1.style.display = "block";
                                sel2.style.display = "none";
                                sel3.style.display = "none";

                                contents.value = "";
                            }

                            function subjectCon<%=j%>()
                            {
                                var contents = document.getElementById("contents"+<%=j%>);
                                var idx = document.getElementById("idx");
                                var ctype = document.getElementById("ctype");
                                var cont = document.getElementById("content");
                                var f = document.form1;

                                idx.value = '<%=rs("idx")%>';
                                ctype.value = "content";
                                cont.value = contents.value;

                                f.action = "edit.asp";
                                f.method = "post";
                                f.submit();
                            }
                        //-->
                        </script>
                        <span id="span_contents<%=j%>" style="display:block"><%=contents %></span>
                        <input type="text" name="contents<%=j%>" id="contents<%=j%>" value="<%=contents %>" style="display:none" />
                        <input type="button" name="btn_con1<%=j%>" id="btn_con1<%=j%>" value="수정" onclick="javascript:conView<%=j%>();" style="display:block;" />
                        <input type="button" name="btn_con2<%=j%>" id="btn_con2<%=j%>" value="취소" onclick="javascript:conNone<%=j%>();" style="display:none;" />
                        <input type="button" name="btn_con3<%=j%>" id="btn_con3<%=j%>" value="적용" onclick="javascript:subjectCon<%=j%>();" style="display:none;"/>
                      </td>
                      <td align="center">
                        <%=rs("view_order") %>
                        <select id="sel_order<%=j%>" name="sel_order<%=j%>" style="display:none;height:20px">
                            <option value=0 <% if rs("view_order") = 0 then %>selected<% end if %>>0</option>
                            <option value=1 <% if rs("view_order") = 1 then %>selected<% end if %>>1</option>
                            <option value=2 <% if rs("view_order") = 2 then %>selected<% end if %>>2</option>
                            <option value=3 <% if rs("view_order") = 3 then %>selected<% end if %>>3</option>
                            <option value=4 <% if rs("view_order") = 4 then %>selected<% end if %>>4</option>
                            <option value=5 <% if rs("view_order") = 5 then %>selected<% end if %>>5</option>
                            <option value=6 <% if rs("view_order") = 6 then %>selected<% end if %>>6</option>
                            <option value=7 <% if rs("view_order") = 7 then %>selected<% end if %>>7</option>
                            <option value=8 <% if rs("view_order") = 8 then %>selected<% end if %>>8</option>
                            <option value=9 <% if rs("view_order") = 9 then %>selected<% end if %>>9</option>
                        </select>
                        
                        <script type="text/javascript">
                        <!--
                            function selectView<%=j%>()
                            {
                                var selV = document.getElementById("sel_order"+<%=j%>);
                                var sel1 = document.getElementById("btn_or1"+<%=j%>);
                                var sel2 = document.getElementById("btn_or2"+<%=j%>);
                                var sel3 = document.getElementById("btn_or3"+<%=j%>);

                                selV.style.display = "block";
                                sel1.style.display = "none";
                                sel2.style.display = "block";
                                sel3.style.display = "block";
                            }

                            function selectNone<%=j%>()
                            {
                                var selV = document.getElementById("sel_order"+<%=j%>);
                                var sel1 = document.getElementById("btn_or1"+<%=j%>);
                                var sel2 = document.getElementById("btn_or2"+<%=j%>);
                                var sel3 = document.getElementById("btn_or3"+<%=j%>);

                                selV.style.display = "none";
                                sel1.style.display = "block";
                                sel2.style.display = "none";
                                sel3.style.display = "none";
                            }

                            function subjectOrder<%=j%>()
                            {
                                var sel_order = document.getElementById("sel_order"+<%=j%>);
                                var idx = document.getElementById("idx");
                                var ctype = document.getElementById("ctype");
                                var selV = document.getElementById("selV");
                                var f = document.form1;

                                idx.value = '<%=rs("idx")%>';
                                ctype.value = "order";
                                selV.value = sel_order.value;

                                f.action = "edit.asp";
                                f.method = "post";
                                f.submit();

                                //location.href='edit.asp?idx=<%=rs("idx")%>&ctype=order&selV='+selV.value;
                            }
                        //-->
                        </script>
                        <input type="button" name="btn_or1<%=j%>" id="btn_or1<%=j%>" value="수정" onclick="javascript:selectView<%=j%>();" style="display:block;" />
                        <input type="button" name="btn_or2<%=j%>" id="btn_or2<%=j%>" value="취소" onclick="javascript:selectNone<%=j%>();" style="display:none;" />
                        <input type="button" name="btn_or3<%=j%>" id="btn_or3<%=j%>" value="적용" onclick="javascript:subjectOrder<%=j%>();" style="display:none;"/>
                      </td>
                      <td align="center"><%=rs("ip_address") %></td>
                      <td align="center">
                        <%
                            Dim noticeFlag, delFlag

                            if rs("notice") = "False" then
                                noticeFlag = 0
                            else
                                noticeFlag = 1
                            end if

                            if rs("del_flag") = "False" then
                                delFlag = 0
                            else
                                delFlag = 1
                            end if
                         %>
                        <% if noticeFlag > 0 then %>Y<% else %>N<% end if %>
                      </td>
                      <td align="center">
                        <% if delFlag > 0 then %>Y<% else %>N<% end if %>
                      </td>
                      <td align="center"><%=rs("reg_date") %></td>
                    </tr>
					<%
					   j=j-1
					   rs.MoveNext ' 다음 레코드로 이동한다.
			           i=i+1	
					   loop '레코드의 끝까지 loop를 돈다.
					%> 
					<% end if %>                      
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="center">
					<% if cint(startpage)<>cint(1) then%> 
				         <a href="list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [이전]</a>
			        <%end if%> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then%>
				         [<%=page%>]
		             <%else%>
					     <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"> <%=i%> </a>
		             <%end if%>
				   <% next%>

				   <% if cint(endpage)<>cint(totalpage) then%>
			             <a href="list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [다음]</a>
			       <% end if%>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>

<input type="hidden" name="idx" id="idx" />
<input type="hidden" name="ctype" id="ctype" />
<input type="hidden" name="selV" id="selV" />
<input type="hidden" name="content" id="content" />
<input type="hidden" name="code" id="code" />

</form>
</body>
</html>
