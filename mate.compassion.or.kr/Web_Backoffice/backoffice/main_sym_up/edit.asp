﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

  if request("idx") = "" then
    idx = 0
  else
    idx=request("idx")
  end if

  if request("ctype") = "" then
    ctype = ""
  else
    ctype=request("ctype")
  end if

  if request("selV") = "" then
    selV = 0
  else
    selV=request("selV")
  end if

  if request("content") = "" then
    content = 0
  else
    content=request("content")
  end if

  if request("code") = "" then
    code = ""
  else
    code=request("code")
  end if
%>


<%
sql = "select top 1 idx from compassweb4.dbo.main_ssb where idx = "& idx
'set rs = db.Execute(sql)
set rs = RecordsetFromWS(provWSDL, sql)

if rs.BOF or rs.EOF then ' 만일 레코드가 없다면
    main_idx = 0
else '데이터가 있다면
    main_idx = rs(0)
end if


if code <> "" then
'기본형, 순서형 저장 테이블 main_config

    Dim sql_save2
    sql_save2 = ""

    if code = "default" then
        sql_save2 = " update main_config set code='default', code_name='기본형', reg_date=getDate() where field='main_ssb' "
    elseif code = "order" then
        sql_save2 = " update main_config set code='order', code_name='순서형', reg_date=getDate() where field='main_ssb' "
    end if

    '글수정
    'db.Execute(sql_save2)
    inresult = BoardWriteWS(provWSDL, sql_save2)

    response.write "<script>alert('수정되었습니다.');</script>"
    response.write "<script>location.href='list.asp';</script>"
end if 


if CInt(main_idx) = CInt(idx) then
'테이블 main_ssb

    Dim sql_save
    sql_save = ""

    if ctype = "order" then '순서 수정할때
        sql_save = " update compassweb4.dbo.main_ssb set view_order="&selV&" where idx="&idx

        '글수정
        'db.Execute(sql_save)
        inresult = BoardWriteWS(provWSDL, sql_save)

        response.write "<script>alert('수정되었습니다.');</script>"
        response.write "<script>location.href='list.asp';</script>"
    end if

    if ctype = "content" then '내용 수정할때
        sql_save = " update compassweb4.dbo.main_ssb set content=N'"&content&"' where idx="&idx

        '글수정
        'db.Execute(sql_save)
        inresult = BoardWriteWS(provWSDL, sql_save)

        response.write "<script>alert('수정되었습니다.');</script>"
        response.write "<script>location.href='list.asp';</script>"
    end if 
    
    if ctype <> "order" and ctype <> "content" then '내용 수정할때    
        response.write "<script>alert('type이 없어 수정되지 않았습니다.');</script>"
        response.write "<script>location.href='list.asp';</script>"
    end if 
else 

    response.write "<script>alert('수정할 번호가 선택되지 않았습니다.');</script>"
    response.write "<script>location.href='list.asp';</script>"
end if

%>