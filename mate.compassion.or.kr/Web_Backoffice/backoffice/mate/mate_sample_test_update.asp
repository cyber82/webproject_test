﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<% '추가 2013-07-17 %>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/tinymce_config.js" charset='euc-kr'></script>
<script type="text/javascript" src="/common/js/jquery-1.6.2.min.js" ></script> 
</head>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%

provWSDL = session("serviceUrl")

    'dim web_con, grs, rsCnt, connectionString

    
    'set xmlDoc=server.CreateObject("Microsoft.XMLDOM")  
    'set xmlappSettings=server.CreateObject("Microsoft.XMLDOM")  
    'set xmladd=server.CreateObject("Microsoft.XMLDOM")  
    'xmlDoc.async="false"  
    'xmlDoc.load(server.MapPath ("/web.config"))  
    'set xmlappSettings = xmldoc.GetElementsByTagName("appSettings").Item(0)   
    'set xmladd = xmlappSettings.GetElementsByTagName("add")  
    'for each x in xmladd   
    'if x.getAttribute("key") ="mate_connection" then  
    '    connectionString = x.getAttribute("value")
    'end if  
    'next   

    'Set web_con = Server.CreateObject("ADODB.Connection")
    'web_con.open connectionString


st_id = request("st_id")
page = request("page")
keyfield = request("keyfield")
key = request("key")


st_display = true
if st_id <> "" then

	'sql = "select st_id, st_type, st_title, st_image, st_order, st_display, st_regdate from COMPASSION_WEB_2016.dbo.mate_sample_test where st_id = "+st_id ' 개발용
    sql = "select st_id, st_type, st_title, st_image, st_order, st_display, st_regdate from KRDS08.COMPASSION_WEB_2016.dbo.mate_sample_test where st_id = "+st_id  ' 운영용 (DB Link 사용)
    set grs = RecordsetFromWS(provWSDL, sql)
    'Set grs = web_con.Execute(sql)
    st_type = grs("st_type")
    st_title = grs("st_title")
    st_image = grs("st_image")
    st_display = grs("st_display")
    st_regdate = grs("st_regdate")
    st_order = grs("st_order")

else

    'sql = "select max(st_order) + 1 as st_order from COMPASSION_WEB_2016.dbo.mate_sample_test" ' 개발용
    sql = "select max(st_order) + 1 as st_order from KRDS08.COMPASSION_WEB_2016.dbo.mate_sample_test"  ' 운영용 (DB Link 사용)
    set grs = RecordsetFromWS(provWSDL, sql)
    'Set grs = web_con.Execute(sql)
    st_order = grs("st_order")
end if

%>

<script language="javascript">
   <!--

    function b_list() {
        location.href = "mate_sample_test_list.asp?page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>";
    }


    function sendit() {
        if (document.myform.st_title.value == "") {
            alert("제목을 입력해 주십시오.");
            document.myform.st_title.focus();
            return;
        }
        
        if (document.myform.st_type.value == "") {
            alert("타입을 선택해 주십시오.");
            document.myform.st_type.focus();
            return;
        }

        <%if st_id = "" then%>
        if (document.myform.st_image.value == "") {
            document.myform.st_image.focus();
            alert("이미지 파일을 등록해 주십시오.");
            return;
        }
        <%end if %>
            

        if (document.myform.st_order.value == "") {
            alert("순서를 입력해 주십시오.");
            document.myform.st_order.focus();
            return;
        }

        document.myform.delete.value = "false";
        document.myform.submit();
    }

    function delChk(url) {
        if (confirm('정말 삭제하시겠습니까?')) {
            document.myform.delete.value = "true";
            document.myform.submit();
        }
    }
    //-->
</script>


<body leftmargin="0" topmargin="0">
<form name="myform" Enctype="multipart/form-data" method = "post" action="mate_sample_test_update_ex.asp?page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>">
<input type="hidden" name="st_id" value="<%=request("st_id")%>">
<input type="hidden" name="delete" value="false">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	 <!--#include file="../inc/top.asp"//-->
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9">편지 번역 SAMPLE TEST</font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr>
                        <td width="15%" height="25" align="center" bgcolor="#F3F3F3"><b>보기유형 여부</b></td>
	                    <td bgcolor="#FFFFFF">
                            <input type="radio" name="st_display" id="st_display1" value="Y" <% if st_display = "True" then %> checked <% end if %> /> 보이기 &nbsp;&nbsp;
		                    <input type="radio" name="st_display" id="st_display2" value="N" <% if st_display = "False" then %> checked <% end if %> /> 보이지 않기 &nbsp;&nbsp;
	                    </td>
                    </tr>

                    
                    <tr>
                        <td width="15%" height="25" align="center" bgcolor="#F3F3F3"><b>타입</b></td>
	                    <td bgcolor="#FFFFFF">
                            <select id="st_type" name="st_type" style="height:25px;">
                                <option value="">선택</option>
                                <option value="1" <% if st_type = "1" then %>selected<% end if %>>어린이편지</option>
                                <option value="2" <% if st_type = "2" then %>selected<% end if %>>후원자편지</option>
                            </select>
	                    </td>
                    </tr>

                    <tr bgcolor="#F3F3F3"> 
                      <td height="25" align="center" bgcolor="#F3F3F3"><b>제목</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="st_title" size="65" maxlength="150" value="<%=st_title%>"></td>
                    </tr>
                    
                    <tr bgcolor="#FFFFFF"> 
                      <td height="25" align="center" bgcolor="#F3F3F3"><b>이미지</b></td>
                      <td>
                          <%if st_image <> "" then %>
                          <img src="<%=st_image %>" style="max-width:200px;max-height:200px;" /><br />
                          <%end if %>
                          <input type="file" name="st_image" size="40" maxlength="30">

                      </td>
                    </tr>

                    <tr bgcolor="#FFFFFF"> 
                      <td height="25" align="center" bgcolor="#F3F3F3"><b>순서<br />(낮은 숫자일수록 위에 나타남)</b></td>
                      <td>
                         <input type="text" name="st_order" size="20" maxlength="150" value="<%=st_order%>">

                      </td>
                    </tr>
                    
                    
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="right">
                    
                    <table width="10%" border="0" cellspacing="2" cellpadding="0">
                        <tr>
                            <td><a href="javascript:delChk();"><img src="../img/btn02.gif" width="44" height="20" border="0"></a></td>
                            <td><a href="mate_sample_test_list.asp?page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>"><img src="../img/btn16.gif" width="42" height="20" border="0"></a></a></td>
                            <td><a href="javascript:sendit();" onfocus="this.blur()"><img src="../img/btn03.gif" width="44" height="20" border="0"></a></td>
                        </tr>
                    </table>
                    
                </td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</form>

</body>
</html>
