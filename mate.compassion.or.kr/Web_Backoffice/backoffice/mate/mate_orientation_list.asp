﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

	keyfield = request("keyfield")	
	key = request("key")	
%>
<%
	table_idx = request("table_idx")
%>

<body leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9">메이트 오리엔테이션 신청명단</font></strong></td>
                      <td align="right">
                            <a href="mate_orientation_excel.asp?keyfield=<%=keyfield%>&key=<%=key%>"><strong><font color="#3788D9">엑셀 다운로드</font></strong></a>
                      </td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                      <td width="6%" height="25" align="center">번호</td>
                      <td width="15%" align="center">후원자ID</td>
                      <td width="12%" align="center">후원명</td>
                      <td width="12%" align="center">웹ID</td>
                      <td width="10%" align="center">메이트분류</td>
                      <td width="10%" align="center">구분</td>
                      <td width="20%" align="center">시간선택</td>
                      <td width="15%" align="center">등록일</td>
                    </tr>

<!--------- 목록 구하기 시작 -------->

<%
  if key = "" then
	  sql = "select * from tMateOrientationJoin order by RegisterDate desc"
  else
	  sql = "select * from tMateOrientationJoin where "&keyfield&" like '%"&key&"%' order by RegisterDate desc"
  end if

  'set rs = server.CreateObject("adodb.recordset")
  'rs.PageSize=10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.
  'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
  set rs = RecordsetFromWS(provWSDL, sql)

%>					
					
					<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면%>
					<tr> 
					  <td height="25" colspan="8" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
					<% else '데이터가 있다면
   
						totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
						rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
						endpage = startpage + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
						if endpage > rs.PageCount then
							endpage = rs.PageCount
						end If


						if request("page")="" then
							npage=1
						else
							npage=cint(request("page"))
						end if
						
						trd=rs.recordcount
						j=trd-Cint(rs.pagesize)*(Cint(npage)-1)
					%>
					<%
						 i=1
						 do until rs.EOF or i>rs.pagesize
				    %>

                    <tr bgcolor="#FFFFFF"> 
                      <td height="25" align="center"><%=j%></td>
                      <td align="center"><%=rs("SponsorID") %></td>
                      <td align="center"><%=rs("SponsorName") %></td>
                      <td align="center"><%=rs("UserID") %></td>
                      <td align="center"><%=rs("MateType") %></td>
                      <td align="center"><%=rs("AcceptFlag") %></td>
                      <td align="center"><%=rs("AttendDate") %></td>
                      <td align="center"><%=left(rs("RegisterDate"), 10)%></td>
                    </tr>

					<%
					   j=j-1
					   rs.MoveNext ' 다음 레코드로 이동한다.
			           i=i+1	
					   loop '레코드의 끝까지 loop를 돈다.
					%> 
					<% end if %>                      
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>

<!--------- 목록 구하기 끝 ----------> 

              <tr> 
                <td align="center">
					<% if cint(startpage)<>cint(1) then%> 
				         <a href="mate_orientation_list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [이전]</a>
			        <%end if%> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then%>
				         [<%=page%>]
		             <%else%>
					     <a href="mate_orientation_list.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"> <%=i%> </a>
		             <%end if%>
				   <% next%>

				   <% if cint(endpage)<>cint(rs.PageCount) then%>
			             <a href="mate_orientation_list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [다음]</a>
			       <% end if%>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
			  
              <tr> 
                <td align="center"><table width="30%" border="0" cellspacing="2" cellpadding="0">
					<form name="searchForm" action="mate_orientation_list.asp" method="post" onSubmit="return searchSendit();">	
                    <tr> 
                      <td><select name="keyfield" style="height:19px;">
                        <option value="SponsorID">후원자ID</option>
                        <option value="SponsorName">후원자명</option>
                        <option value="UserID">웹ID</option>
                        <option value="MateType">메이트분류</option>
                        <option value="AttendDate">시간선택</option>
                      </select></td>
                      <td><input name="key" type="text" class="text"></td>
                      <td><input type="submit" name="Submit" value="찾기"></td>
                    </tr>
					</form>
                  </table></td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
