﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
'provWSDL = "http://krds01.krpc.ci.org/WWW5Service_penta/Service.asmx?WSDL"
%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

	keyfield = request("keyfield")	
	key = request("key")
%>
<%
	table_idx = request("table_idx")
%>

<body leftmargin="0" topmargin="0">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="70">&nbsp;</td>
                        <td width="900">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="right">&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <table width="40%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                                                <td><strong><font color="#3788D9">편지 번역 SAMPLE TEST</font></strong></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                                            <tr bgcolor="#F3F3F3"> 
                                                <td width="6%" height="25" align="center">번호</td>
                                                <td style="width:10%" align="center">타입</td>
                                                <td align="center">제목</td>
                                                <td width="10%" align="center">순서</td>
                                                <td width="10%" align="center">노출여부</td>
                                                <td width="10%" align="center">등록일</td>
                                            </tr>
<!------- 게시판 목록 구하기 시작 ----------> 
<%
if key = "" then
	'sql = "select * from COMPASSION_WEB_2016.dbo.mate_sample_test order by st_order asc"   ' 개발용
    sql = "select * from KRDS08.COMPASSION_WEB_2016.dbo.mate_sample_test order by st_order asc"    ' 운영용 (DB Link 사용)
else
	'sql = "select * from COMPASSION_WEB_2016.dbo.mate_sample_test where "&keyfield&" like '%"&key&"%' order by st_order asc"   ' 개발용
    sql = "select * from KRDS08.COMPASSION_WEB_2016.dbo.mate_sample_test where "&keyfield&" like '%"&key&"%' order by st_order asc"    ' 운영용 (DB Link 사용)
end if

'sql = "select * from mate_sample_test"

'Response.write(sql)

set rs = RecordsetFromWS(provWSDL, sql)

'Response.write(rs.PageCount)
%>

<%
if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
    <tr><td height="22" colspan="6" align="center"><b>글이 없습니다!!!</b></td></tr>
<% else '데이터가 있다면
   
	totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
	rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
	endpage = startpage + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
	if endpage > rs.PageCount then
		endpage = rs.PageCount
	end If


	if request("page")="" then
		npage=1
	else
		npage=cint(request("page"))
	end if
						
	trd=rs.recordcount
	j=trd-Cint(rs.pagesize)*(Cint(npage)-1)
%>
<%
		i=1
		do until rs.EOF or i>rs.pagesize
%>
        <tr bgcolor="#FFFFFF"> 
            <td height="25" align="center"><%=j %></td>
            <td height="25" align="center"><% if rs("st_type")="1" then Response.write("어린이편지") else Response.write("후원자편지") %></td>
            <td style="padding:5px;">
                <a href="mate_sample_test_update.asp?st_id=<%=rs("st_id")%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"> 
                    <%=rs("st_title")%>
                </a>                        
            </td>
            <td align="center"> <%=rs("st_order")%></td>
            <td align="center"><% if rs("st_display") = "True" then Response.write("O") else Response.write("X") end if%></td>
            <td align="center"> <%=left(rs("st_regdate"), 10)%></td>
        </tr>
<%
	j=j-1
	rs.MoveNext ' 다음 레코드로 이동한다.
	i=i+1	
	loop '레코드의 끝까지 loop를 돈다.
%> 
<% end if %>               
<!------- 게시판 목록 구하기 끝 ---------->  
                                    
                                        </table>
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr>
                                    <td align="right"><a href="mate_sample_test_update.asp?page=<%=page%>" onfocus="this.blur()"><img src="../img/btn17.gif" width="52" height="20" border="0"></a></td>
                                </tr>
                                <tr> 
                                    <td align="center">
					                    <% if cint(startpage)<>cint(1) then%> 
				                             <a href="letterspon_request_list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			                                 [이전]</a>
			                            <%end if%> 

					                    <% for i = startpage to endpage step 1 %>
		                                 <% if cint(i) = cint(page) then%>
				                             [<%=page%>]
		                                 <%else%>
					                         <a href="letterspon_request_list.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"> <%=i%> </a>
		                                 <%end if%>
				                       <% next%>

				                       <% if cint(endpage)<>cint(rs.PageCount) then%>
			                                 <a href="letterspon_request_list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			                                 [다음]</a>
			                           <% end if%>
                                    </td>
                                </tr>
                                <tr><td align="center">&nbsp;</td></tr>
                                <tr> 
                                    <td align="center">
                                        <table width="30%" border="0" cellspacing="2" cellpadding="0">
                                            <form name="searchForm" action="mate_sample_test_list.asp" method="post" onSubmit="return searchSendit();">	
                                                <tr> 
                                                    <td>
                                                        <select name="keyfield" style="height:19px;">
                                                            <option value="b_title">제목</option>
                                                        </select>
                                                    </td>
                                                    <td><input name="key" type="text" class="text"></td>
                                                    <td><input type="submit" name="Submit" value="찾기"></td>
                                                </tr>
                                            </form>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td align="center">&nbsp;</td></tr>
                            </table>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
