﻿<%@Language="VBScript" CODEPAGE="65001"%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
    
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/freeASPUpload.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
    

    provWSDL = session("serviceUrl")
    

    'dim web_con, rs, rsCnt, connectionString

    
    'set xmlDoc=server.CreateObject("Microsoft.XMLDOM")  
    'set xmlappSettings=server.CreateObject("Microsoft.XMLDOM")  
    'set xmladd=server.CreateObject("Microsoft.XMLDOM")  
    'xmlDoc.async="false"  
    'xmlDoc.load(server.MapPath ("/web.config"))  
    'set xmlappSettings = xmldoc.GetElementsByTagName("appSettings").Item(0)   
    'set xmladd = xmlappSettings.GetElementsByTagName("add")  
    'for each x in xmladd   
    'if x.getAttribute("key") ="mate_connection" then  
    '    connectionString = x.getAttribute("value")
    'end if  
    'next


    'Set web_con = Server.CreateObject("ADODB.Connection")
    'web_con.open connectionString
    'set rs=Server.CreateObject("ADODB.recordset")



    SET Upload = NEW FreeASPUpload
    Data_Folder = Server.MapPath("/") & "\upload\mate\test\"
    Upload.Save(Data_Folder)
    

    result = ""
    sql = ""


    st_id = Upload.Form("st_id")

    if Upload.Form("delete") = "true" and st_id <> "" then
    
        'sql = sql & " delete COMPASSION_WEB_2016.dbo.mate_sample_test where st_id = "& st_id   ' 개발용
        sql = sql & " delete KRDS08.COMPASSION_WEB_2016.dbo.mate_sample_test where st_id = "& st_id    ' 운영용 (DB Link 사용)
        inresult = BoardWriteWS(provWSDL, sql)
        'web_con.Execute sql
        result = "삭제되었습니다."
    else

        DIM Data_Folder
        DIM ofilename, ofiledate, ofilesize

        ks = Upload.UploadedFiles.keys
        if (UBound(ks) <> -1) then
            for each fileKey in Upload.UploadedFiles.keys
                SaveFileName = Upload.UploadedFiles(fileKey).FileName


                If SaveFileName <> "" Then

                    vFilename = SaveFileName						'파일이름
                    strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
                    strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

                    set fso = CreateObject("Scripting.FileSystemObject")
                    set file = fso.GetFile(Data_Folder & "\" & SaveFileName)

                    '새이름 생성
                    newFilename = MakeFileName2(strName) & "." & strExt
                    file.name = newFilename

                    set file = nothing
                    set fso = Nothing

                    '디비에 저장될 이미지 경로
                    strFilePath = Data_Folder & Replace(strDateDir,"\","/") & newFilename

                End If
            next
        end if

        if strFilePath <> "" then
	        If b_filename <> "" Then
		        arr_fname = Split(b_filename, "/")
		        fname = arr_fname(Ubound(arr_fname))

		        strDateDir1 = arr_fname(Ubound(arr_fname)-2) & "\"
		        strDateDir2 = arr_fname(Ubound(arr_fname)-1) & "\"
		        strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
		        strDelPath = strSaveFolder & fname

		        Set fso = CreateObject("Scripting.FileSystemObject")
		        '파일 존재시 파일 삭제
		        If fso.FileExists(strDelPath) Then
		            fso.DeleteFile strDelPath
		        End If
		        Set fso = Nothing

	        End if
	        strImgFile = strFilePath
        else
	        strImgFile = b_filename
        end If


        page = Upload.Form("page")
        keyfield = Upload.Form("keyfield")	
        key = Upload.Form("key")


        st_type = Upload.Form("st_type")
        st_title = Upload.Form("st_title")
        st_order = Upload.Form("st_order")  
        st_image = Upload.Form("st_image")
        st_display = Upload.Form("st_display")
        st_regdate = Upload.Form("st_regdate")

        if st_display = "Y" then st_display = "1" else st_display = "0" end if
  

        if st_id = "" then
  
            'sql = sql & " insert into COMPASSION_WEB_2016.dbo.mate_sample_test(st_type, st_title, st_image, st_order, st_display, st_regdate )"    ' 개발용
            sql = sql & " insert into KRDS08.COMPASSION_WEB_2016.dbo.mate_sample_test(st_type, st_title, st_image, st_order, st_display, st_regdate )" ' 운영용 (DB Link 사용)
            sql = sql & " values ('"&st_type&"', '"&st_title&"', '"&"\upload\mate\test\" & newFilename&"', "&st_order&", "&st_display&", getdate()) "

            inresult = BoardWriteWS(provWSDL, sql)
            'web_con.Execute sql

            result = "등록되었습니다."
  
        else
            'sql = sql & " update COMPASSION_WEB_2016.dbo.mate_sample_test "    ' 개발용
            sql = sql & " update KRDS08.COMPASSION_WEB_2016.dbo.mate_sample_test " ' 운영용 (DB Link 사용)
            sql = sql & " set st_type = '"& st_type&"',"
            sql = sql & " st_title = '"& st_title&"',"
            if newFilename <> "" then 
                sql = sql & " st_image = '"& "\upload\mate\test\" & newFilename&"',"
            end if
            sql = sql & " st_order = "& st_order&","
            sql = sql & " st_display = "& st_display
            sql = sql & " where st_id = "&st_id

            inresult = BoardWriteWS(provWSDL, sql)
            'web_con.Execute sql

            result = "수정되었습니다."

        end if
    end if


    if inresult <> "10" AND inresult <> "" then
        result = inresult
    end if
    %>
    <script type="text/javascript" charset='euc-kr'>
    <!--
        alert("<%=result%>");
        location = "mate_sample_test_list.asp?page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>"
    //-->
</script>

</html>