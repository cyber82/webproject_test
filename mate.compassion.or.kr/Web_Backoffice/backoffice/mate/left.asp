<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

<table width="160" border="1" cellspacing="0" cellpadding="0">

    <!-- 수정 2013-03-26  메뉴 정리 -->
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1001" target="listFrame">공지사항</a></b></td>
   </tr>
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1004" target="listFrame">이달의 메이트<!--우수/감동 편지--></a></b></td>
   </tr>
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1009" target="listFrame">FAQ</a></b></td>
   </tr>
    <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1223" target="listFrame">함께하는 이야기</a></b></td>
   </tr>
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1222" target="listFrame">우리들의 이야기</a></b></td>
   </tr>
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1011" target="listFrame">오늘의 말씀</a></b></td>
   </tr>
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1005" target="listFrame">번달이 도전기<!--번역의 달인--></a></b></td>
   </tr>
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1010" target="listFrame">번역 Q&A<!--온라인상담--></a></b></td>
   </tr>
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1002" target="listFrame">온라인 상담<!--자유게시판--></a></b></td>
   </tr>
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1008" target="listFrame">수혜국 편지 정보<!--자료실--></a></b></td>
   </tr>
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=1003" target="listFrame">특별한 그들 엿보기</a></b></td>
   </tr>
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="list.asp?table_idx=9999" target="listFrame">크리스마스 러브레터</a></b></td>
   </tr>

   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="mate_orientation_list.asp" target="listFrame">오리엔테이션 신청명단</a></b></td>
   </tr>
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../mate_trans/list.asp" target="listFrame">번역메이트 연장동의서</a></b></td>
   </tr>

   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="letterspon_request_list.asp" target="listFrame">편지 후원 신청명단</a></b></td>
   </tr>
    
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../mate/mate_sample_test_list.asp" target="listFrame">편지 번역 SAMPLE TEST</a></b></td>
   </tr>
    
   <tr>
     <td width="160" height="25" align="center" bgcolor="#EBEBEB"><b><a href="../mate/mate_sample_test_info.asp" target="listFrame">편지 번역 SAMPLE 노출정보</a></b></td>
   </tr>
</table>

</body>
</html>
