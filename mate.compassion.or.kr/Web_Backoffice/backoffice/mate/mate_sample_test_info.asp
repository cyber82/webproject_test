﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<% '추가 2013-07-17 %>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/tinymce_config.js" charset='euc-kr'></script>
<script type="text/javascript" src="/common/js/jquery-1.6.2.min.js" ></script> 
</head>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%

provWSDL = session("serviceUrl")

    'dim web_con, grs, rsCnt, connectionString

    
    'set xmlDoc=server.CreateObject("Microsoft.XMLDOM")  
    'set xmlappSettings=server.CreateObject("Microsoft.XMLDOM")  
    'set xmladd=server.CreateObject("Microsoft.XMLDOM")  
    'xmlDoc.async="false"  
    'xmlDoc.load(server.MapPath ("/web.config"))  
    'set xmlappSettings = xmldoc.GetElementsByTagName("appSettings").Item(0)   
    'set xmladd = xmlappSettings.GetElementsByTagName("add")  
    'for each x in xmladd   
    'if x.getAttribute("key") ="mate_connection" then  
    '    connectionString = x.getAttribute("value")
    'end if  
    'next   

    'Set web_con = Server.CreateObject("ADODB.Connection")
    'web_con.open connectionString





	'sql = "select * from COMPASSION_WEB_2016.dbo.mate_sample_test_info"    ' 개발용
    sql = "select * from KRDS08.COMPASSION_WEB_2016.dbo.mate_sample_test_info"  ' 운영용(DB Link 사용)
    set grs = RecordsetFromWS(provWSDL, sql)
    'Set grs = web_con.Execute(sql)
    play_date= grs("play_date")
    announce_date= grs("announce_date")
    tel = grs("tel")
    email = grs("email")

%>

<script language="javascript">
   <!--



    function sendit() {

        if (document.myform.play_date.value == "") {
            alert("활동기간을 입력해 주십시오.");
            document.myform.play_date.focus();
            return;
        }

        if (document.myform.announce_date.value == "") {
            alert("합격자 발표일을 입력해 주십시오.");
            document.myform.announce_date.focus();
            return;
        }
        
        if (document.myform.tel.value == "") {
            alert("전화번호를 입력해 주십시오.");
            document.myform.tel.focus();
            return;
        }

        if (document.myform.email.value == "") {
            alert("이메일을 입력해 주십시오.");
            document.myform.email.focus();
            return;
        }

        document.myform.submit();
    }

    //-->
</script>


<body leftmargin="0" topmargin="0">
<form name="myform" method = "post" action="mate_sample_test_info_ex.asp">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	 <!--#include file="../inc/top.asp"//-->
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9">편지 번역 SAMPLE TEST 정보</font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    
                    <tr bgcolor="#F3F3F3"> 
                      <td height="25" align="center" bgcolor="#F3F3F3"><b>활동기간</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="play_date" size="65" maxlength="150" value="<%=play_date%>"></td>
                    </tr>


                    <tr bgcolor="#F3F3F3"> 
                      <td height="25" align="center" bgcolor="#F3F3F3"><b>당첨자 발표일</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="announce_date" size="65" maxlength="150" value="<%=announce_date%>"></td>
                    </tr>
                    
                    
                    <tr bgcolor="#F3F3F3"> 
                      <td height="25" align="center" bgcolor="#F3F3F3"><b>전화번호</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="tel" size="65" maxlength="150" value="<%=tel%>"></td>
                    </tr>
                    
                    <tr bgcolor="#F3F3F3"> 
                      <td height="25" align="center" bgcolor="#F3F3F3"><b>이메일</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="email" size="65" maxlength="150" value="<%=email%>"></td>
                    </tr>
                    
                    
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="right">
                    
                    <table width="10%" border="0" cellspacing="2" cellpadding="0">
                        <tr>
                            <td><a href="javascript:sendit();" onfocus="this.blur()"><img src="../img/btn03.gif" width="44" height="20" border="0"></a></td>
                        </tr>
                    </table>
                    
                </td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</form>

</body>
</html>
