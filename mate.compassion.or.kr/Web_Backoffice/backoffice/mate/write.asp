﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<% 
'주석처리 2013-07-18
'<!--#include file="../inc/top_main.asp"//--> 
%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<% '추가 2013-07-17 %>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/tinymce_config.js" charset='euc-kr'></script>
<script type="text/javascript" src="/common/js/jquery-1.6.2.min.js" ></script> 
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/common/smartEditor/initEditor.js"></script>
<script type="text/javascript">
    //tinymce_config("b_content");
    $(function () {
        image_path = "/images/board/";
        initEditor(oEditors, "b_content");
    });
</script>
</head>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%
 ' 답변일 경우 원래 글의 내용과 제목을 보여준다....
 'content.asp  에서 넘어논 id 값을 가지고 원래 글의 제목과 글내용을 디비에서 가져와 보여준다.

if request("id") <> "" then
%>
<%
	table_idx = request("t_id")
	no_idx = request("id")
	notice_idx = request("n_id")
	page = request("page")
	keyfield = request("keyfield")	
	key = request("key")

	sql = "select b_title, b_content, ViewYN from mate_upboard where table_idx = '"&table_idx&"' and no_idx = "&no_idx&" and notice_idx = 'N' "

    'set grs = server.CreateObject("adodb.recordset")
    'grs.Open sql, db
    set grs = RecordsetFromWS(provWSDL, sql)
    
    b_title = grs("b_title")
    b_content = grs("b_content")
    b_content = ">" & b_content '답변글와 원래글의 구분을 위하여.. 원래글앞에 > 를 붙여준다
    b_title = "Re:" & b_title
    b_content = replace(b_content,chr(13) & chr(10), chr(13) & chr(10) & ">")

    '추가 2013-06-05
    view_type = grs("ViewYN")
    if view_type <> "N" then
        view_type = "Y"
    end if
else 

    view_type = "Y"    
end if
%>
<%
	table_idx = request("t_id")
	no_idx = request("id")
	notice_idx = request("n_id")
	left_menu = request("left_menu")
	page = request("page")
%>

<script language="javascript">
   <!--
    function b_list()
    {
     location.href="list.asp?table_idx=<%=table_idx%>&page=<%=page%>"
    }
    
    
    function sendit()
    {
     //이름
      if(document.myform.b_name.value =="") {
      
       alert("글쓴이를 입력해 주십시오.");
       return;
     }



     //제목
      if (document.myform.b_title.value == "") {
      alert("제목을 입력해 주십시오.");
      return ;
     }   
     
      oEditors.getById["b_content"].exec("UPDATE_CONTENTS_FIELD", []);


     //비밀번호
      if (document.myform.b_pwd.value == "") {
      alert("비밀번호을 입력해 주십시오.");
      return ;
      }

      var content = document.getElementById("b_content").value;
      document.getElementById("b_content").value = content.replace(/\u200B/g, '').replace(/\n/g, '').replace(/\r/g, '');

     document.myform.submit();

     }
      
     //-->
     </script>


<body leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	 <!--#include file="../inc/top.asp"//-->
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9"><!--#include file="bbs_title.asp"//--></font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
<form name="myform" method = "post" action="write_ok.asp?table_idx=<%=table_idx%>&no_idx=<%=no_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>" enctype="multipart/form-data">
<input type="hidden" name="idx" value="<%=request("idx")%>">
<input type="hidden" name="ref" value="<%=request("ref")%>">
<input type="hidden" name="re_step" value="<%=request("re_step")%>">
<input type="hidden" name="re_level" value="<%=request("re_level")%>">
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    
                    <% '추가 2013-06-05 %>
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3"><b>게시판분류</b></td>
                      <td bgcolor="#FFFFFF">
						 <!--#include file="table_idx.asp"//-->
					  </td>
                    </tr>
                    
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3"><b>공지사항</b></td>
                      <td bgcolor="#FFFFFF">
						  <select name="b_notice" style="height:18px;">
				            <option value="Y">공지체크</option>
			                <option value="N" selected>공지안함</option>
						  </select>
					  </td>
                    </tr>
                    
                    <% if table_idx = "1002" then %>
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3"><b>분류</b></td>
                      <td bgcolor="#FFFFFF">
						 <!--#include file="kind.asp"//-->
					  </td>
                    </tr>
                    <%end if%>

                    <% '추가 2013-05-29, 수정 2013-08-01, 수정 2013-08-08 %>
                    <% if table_idx = "1001" then %>
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3"><b>메이트구분</b></td>
                      <td bgcolor="#FFFFFF">
						 <select name="mate_type" style="height:18px;">
				            <option value="" selected>사용안함</option>
			                <option value="imate">아이메이트사용</option>
                            <option value="imate-p">아이메이트전용</option>
						  </select>
					  </td>
                    </tr>
                    <%end if%>

                    <% '추가 2013-06-05 %>
                    <tr>
                        <td width="15%" height="25" align="center" bgcolor="#F3F3F3"><b>보기유형 여부</b></td>
	                    <td bgcolor="#FFFFFF">
                            <input type="radio" name="view_type" id="view_type1" value="Y" <% if view_type = "Y" then %> checked <% end if %> /> 보이기 &nbsp;&nbsp;
		                    <input type="radio" name="view_type" id="view_type2" value="N" <% if view_type = "N" then %> checked <% end if %> /> 보이지 않기 &nbsp;&nbsp;
                            (체크가 안되있을시에는 기본적으로 보이기로 됨)
	                    </td>
                    </tr>

					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3"><b>작성자</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_name" size="40" maxlength="30" value="컴패션"></td>
                    </tr>
                    <tr bgcolor="#F3F3F3"> 
                      <td height="25" align="center" bgcolor="#F3F3F3"><b>제목</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_title" size="65" maxlength="150" value="<%=b_title%>"></td>
                    </tr>
<%  if request("id") <> "" then%>
                    <tr bgcolor="#F3F3F3"> 
                      <td height="25" align="center" bgcolor="#F3F3F3"><b>질문내용</b></td>
                      <td bgcolor="#FFFFFF" valign="top" width="350"><%=replace(grs("b_content"), chr(13), "<br>")%></td>
                    </tr>
<%end if%>
                    <tr bgcolor="#FFFFFF"> 
                      <td height="200" align="center" bgcolor="#F3F3F3"><b>내용</b></td>
                      <td>
                        <!--<textarea name="b_content" cols="60" rows="10"></textarea>-->
                        <% '수정 2013-06-11 %>
                        <div>
	                        <textarea name="b_content" id="b_content" style="width:630px;height:170px;display:none;" class="sbox"></textarea>
                        </div>
                        <!--<div><button onClick="addArea2();">Editor</button> <button onClick="removeArea2();">Html</button></div>-->
                      </td>
                    </tr>
                    <tr bgcolor="#FFFFFF"> 
                      <td height="25" align="center" bgcolor="#F3F3F3"><b>첨부파일</b></td>
                      <td><input type="file" name="b_filename" size="40" maxlength="30"></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                      <td height="25" align="center" bgcolor="#F3F3F3"><b>비밀번호</b></td>
                      <td><input type="password" name="b_pwd" size="40" maxlength="30" value="compassion_admin00"></td>
                    </tr>
<!--
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">작성일</td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_writeday" size="40"></td>
                    </tr>
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">조회수</td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_readnum" size="40" maxlength="30"></td>
                    </tr>
-->
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="right"><a href="javascript:sendit();" onfocus="this.blur()"><img src="../img/btn03.gif" width="44" height="20" border="0"></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>

    <% '추가 2013-06-19 %>
    <% '주석처리 2013-07-17 %>
    <!--<script src="/Web_Backoffice/backoffice/inc/js/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        var area1, area2;
        /*
        function toggleArea1() {
        if (!area1) {
        area1 = new nicEditor({ fullPanel: true }).panelInstance('myArea1', { hasPanel: true });
        } else {
        area1.removeInstance('myArea1');
        area1 = null;
        }
        }
        */

        function addArea2() {
            area2 = new nicEditor({ fullPanel: true }).panelInstance('b_content');
        }
        function removeArea2() {
            area2.removeInstance('b_content');
        }

        bkLib.onDomLoaded(function () { addArea2(); });
    </script>-->

</body>
</html>
