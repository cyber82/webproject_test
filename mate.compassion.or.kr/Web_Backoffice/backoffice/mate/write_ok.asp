﻿<%@Language="VBScript" CODEPAGE="65001"%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/freeASPUpload.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->
<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

provWSDL = session("serviceUrl")


DIM Data_Folder
DIM old_filename

'저장 폴더명
Data_Folder = Server.MapPath("/") & "\Files\Board\"

    SET Upload = NEW FreeASPUpload
    Upload.Save(Data_Folder)
    ks = Upload.UploadedFiles.keys
    if (UBound(ks) <> -1) then
        for each fileKey in Upload.UploadedFiles.keys
            b_filename = Upload.UploadedFiles(fileKey).FileName
        next
    end if
    

    table_idx = Upload.Form("table_idx")
    no_idx = Upload.Form("no_idx")

    page = Upload.Form("page")
    keyfield = Upload.Form("keyfield")	
    key = Upload.Form("key")



    b_notice = Upload.Form("b_notice")
    b_name = Upload.Form("b_name")  
    b_email = Upload.Form("b_email")
    b_hompage = Upload.Form("b_hompage")
    b_title = Upload.Form("b_title")
    b_pwd = Upload.Form("b_pwd")
    b_tel = Upload.Form("b_tel")
    b_content = Upload.Form("b_content")
    'b_filename = Upload.Form("b_filename")
    mate_type = Upload.Form("mate_type")

    '추가 2013-06-05
    table_id = Upload.Form("table_id") 
    view_type = Upload.Form("view_type") 

    idx = Upload.Form("idx")
    ref = Upload.Form("ref")
    re_step = Upload.Form("re_step")
    re_level = Upload.Form("re_level")

    b_writeday = Upload.Form("b_writeday")
    b_readnum = Upload.Form("b_readnum")
    b_kind = Upload.Form("b_kind")

    'response.Write cint(ref)
    'response.End

    b_title=replace(b_title,"'","''") '쿼리에 '가 들어가면 에러가 나기 때문에 그것을 replace처리 합니다.
    b_content=replace(b_content,"'","''")

    b_content = replace(b_content,"&#8203;","")
  
  sql = "select Max(b_num) from mate_upboard"  
  'set rs = server.CreateObject("adodb.recordset")
  'rs.Open sql,db
  set rs = RecordsetFromWS(provWSDL, sql)


  if isNull(rs(0)) then ' 글이 없을경우 b_num을 1로 한다
     b_num = 1
  else
    b_num=rs(0) + 1
  end if
  
  '*************답변형 게시판의 추가 부분************************
  if idx <> "" then
  'if Upload.Form("idx") <> "" then '즉 답변쓰기라면 
  

  ref = clng(ref)
  re_step = cInt(re_step)
  re_level = cint(re_level)

  sqlstring = "update mate_upboard set re_step = re_step + 1 where ref=" & ref & " AND re_step > " & re_step &" and table_idx = '"&table_idx&"' and notice_idx = '"&b_notice&"' "


  'db.Execute(sqlstring)
  inresult = BoardWriteWS(provWSDL, sqlstring)

  re_step = re_step + 1
  re_level = re_level + 1
  
  else ' 첨글쓰기 라면
  ref = b_num
  re_step=0
  re_level=0
  end if

if idx <> "" then
  
    '수정 2013-05-29
    '********테이블에 저장한다.**************** 
    sql = "insert into mate_upboard (table_idx,no_idx, notice_idx, b_admin, b_name, b_kind, b_email, b_hompage, b_title, b_content, b_num,"
    sql = sql & "b_readnum, b_writeday, ref, re_level, re_step, b_pwd, b_filename1, b_filesize, ViewYN "
    
    if mate_type <> "" then
        sql = sql & " , mate_type "
    end if

    sql = sql & " ) values "
    sql = sql & "( N'" & table_id & "'"     '수정 2013-06-05
    sql = sql & ", N'" & no_idx & "'"
    sql = sql & ", N'" & b_notice & "'"
    sql = sql & ", N'A'"
    sql = sql & ", N'" & b_name & "'"
    sql = sql & ", N'" & b_kind & "'"
    sql = sql & ", N'" & b_email & "'"
    sql = sql & ", N'" & b_hompage & "'"
    sql = sql & ", N'" & b_title & "'"
    sql = sql & ", N'" & b_content & "'"
    sql = sql & ", " & b_num
    sql = sql & ", 0, getdate()"
    '  sql = sql & ","& b_readnum & ",'"& b_writeday & "'"
    sql = sql & ", " & ref
    sql = sql & ", " & re_level
    sql = sql & ", " & re_step
    sql = sql & ", N'" & b_pwd & "'"
    sql = sql & ", N'" & b_filename & "'"
    sql = sql & ", N'" & b_filesize & "'"
    sql = sql & ", N'" & view_type & "'"    '추가 2013-06-05

    if mate_type <> "" then
        sql = sql & ", N'" & mate_type & "'"
    end if

    sql = sql & ")"
  
    'db.Execute sql
    inresult = BoardWriteWS(provWSDL, sql)

else

    sql="select isnull(max(no_idx), 0) as max_idx from mate_upboard where table_idx = '"&table_id&"' and notice_idx = '"&b_notice&"' "

    ' 글쓰기 공지체크 부분
    'set record=db.execute(sql)
    Set record = RecordsetFromWS(provWSDL, sql)

    
    
	if record.BOF or record.EOF then
        titleno="1"
    else
        titleno = record(0) + 1
	End If

    '수정 2013-05-29, 2013-09-16
    '********테이블에 저장한다.**************** 
    sql = "insert into mate_upboard (table_idx,no_idx, notice_idx, b_admin, b_name, b_kind, b_email, b_hompage, b_title, b_content, b_num,"
    sql = sql & "b_readnum, b_writeday, ref, re_level, re_step, b_pwd, b_filename1, b_filesize, ViewYN "

    if mate_type <> "" then
        sql = sql & " , mate_type "
    end if

    sql = sql & " ) values "
    sql = sql & "( N'" & table_id & "'"     '수정 2013-06-05
    sql = sql & ", N'" & titleno & "'"
    sql = sql & ", N'" & b_notice & "'"
    sql = sql & ", N'A'"
    sql = sql & ", N'" & b_name & "'"
    sql = sql & ", N'" & b_kind & "'"
    sql = sql & ", N'" & b_email & "'"
    sql = sql & ", N'" & b_hompage & "'"
    sql = sql & ", N'" & b_title & "'"
    sql = sql & ", N'" & b_content & "'"
    sql = sql & ", " & b_num
    sql = sql & ", 0, getdate()"
    '  sql = sql & ","& b_readnum & ",'"& b_writeday & "'"
    sql = sql & ", " & ref
    sql = sql & ", " & re_level
    sql = sql & ", " & re_step
    sql = sql & ", N'" & b_pwd & "'"
    sql = sql & ", N'" & b_filename & "'"
    sql = sql & ", N'" & b_filesize & "'"
    sql = sql & ", N'" & view_type & "'"    '추가 2013-06-05
    
    if mate_type <> "" then
        sql = sql & ", N'" & mate_type & "'"
    end if

    sql = sql & ")"

    'db.Execute sql
    inresult = BoardWriteWS(provWSDL, sql)


    '추가 2013-07-17
    if inresult = "10" then
    %>
    <script type="text/javascript" charset='euc-kr'>
    <!--
        alert('등록되었습니다.');
    //-->
    </script>
    <%
    end if

    if inresult <> "10" AND inresult <> "" then
    %>
    <script type="text/javascript" charset='euc-kr'>
    <!--
        alert('<%=inresult %>');
    //-->
    </script>
    <%
    end if

end if

 
  '인스턴스를 소멸......  
  rs.Close
  'inresult.Close
  set rs=nothing
  'set inresult=nothing

%>
<script type="text/javascript" charset='euc-kr'>
     location = "list.asp?table_idx=<%=table_id%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>"
</script>

</html>