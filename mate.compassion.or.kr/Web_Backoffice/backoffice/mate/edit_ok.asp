﻿<%@Language="VBScript" CODEPAGE="65001"%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/freeASPUpload.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->
<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")

DIM Data_Folder
DIM old_filename

'저장 폴더명
Data_Folder = Server.MapPath("/") & "\Files\Board\"

    SET Upload = NEW FreeASPUpload
    Upload.Save(Data_Folder)
    ks = Upload.UploadedFiles.keys
    if (UBound(ks) <> -1) then
        for each fileKey in Upload.UploadedFiles.keys
            b_filename = Upload.UploadedFiles(fileKey).FileName
        next
    end if
    

    table_idx = Upload.Form("table_idx")
    no_idx = Upload.Form("no_idx")

    page = Upload.Form("page")
    b_pwd = Upload.Form("b_pwd")
    ref = Upload.Form("ref")
    re_step = Upload.Form("re_step")
    notice_idx = Upload.Form("notice_idx")

    keyfield = Upload.Form("keyfield")	
    key = Upload.Form("key")

 
    b_notice = Upload.Form("b_notice")  
    b_name = Upload.Form("b_name")  
    b_tel = Upload.Form("b_tel")
    no_idx2 = Upload.Form("no_idx2")
    b_email = Upload.Form("b_email")
    b_hompage = Upload.Form("b_hompage")
    b_title = Upload.Form("b_title")
    b_content = Upload.Form("b_content")
    'b_filename = Upload.Form("b_filename")
    idx = Upload.Form("idx")
    b_writeday = Upload.Form("b_writeday") 
    b_readnum = Upload.Form("b_readnum") 
    b_kind = Upload.Form("b_kind")
    mate_type = Upload.Form("mate_type")

    '추가 2013-06-05
    table_id = Upload.Form("table_id") 
    view_type = Upload.Form("view_type") 

    '쿼리에 '가 들어가면 에러가 나기 때문에 그것을 replace 처리 해준다.
    b_title = replace(b_title,"'","''")
    b_content = replace(b_content,"'","''")

    b_content = replace(b_content,"&#8203;","")

    '현재글의 비밀번호를 가져오는 쿼리문을 작성한다.

    sql = "select * from mate_upboard where table_idx = '"&table_idx&"' and no_idx = "&no_idx&" and notice_idx = '"&notice_idx&"' and ref = "&ref&" and re_step = "&re_step&" "

    'set result= db.Execute(sql)' 받아온 값 쿼리를 실행....
    '입력받은 비밀번호와 실제 비밀번호르 비교하여 일치한다면
    set result = RecordsetFromWS(provWSDL, sql)

    if b_pwd = result("b_pwd") then
   
        '수정 2013-05-29
        sql = "update mate_upboard set notice_idx = '" & b_notice& "'"
        sql = sql & ", b_name = '" & b_name & "'"
        sql = sql & ", b_title = '" & b_title & "'"
        sql = sql & ", b_email = '" & b_email & "'"
        sql = sql & ", b_kind = '" & b_kind & "'"
        sql = sql & ", b_content = N'" & b_content & "'"
        sql = sql & ", b_hompage = '" & b_hompage & "'"
        sql = sql & ", b_filename1 = '" & b_filename & "'"
        sql = sql & ", b_filesize = '" & b_filesize & "'"
        sql = sql & ", b_readnum = '" & b_readnum & "'"  
        'sql = sql & ", b_writeday = '" & b_writeday & "'"

        '추가 2013-06-05
        sql = sql & ", table_idx = N'" & table_id & "'"  
        sql = sql & ", ViewYN = N'" & view_type & "'"  

        if mate_type <> "" then
            sql = sql & ", mate_type = '" & mate_type & "'"
        else
            sql = sql & ", mate_type = null "
        end if
        sql = sql & " where no_idx = "&no_idx&" and table_idx = '"&table_idx&"' and notice_idx = '"&notice_idx&"' and ref = "&ref&" and re_step = "&re_step&" "



    'db.Execute sql
    upresult = BoardWriteWS(provWSDL, sql)

    '추가 2013-07-17
    if upresult = "10" then
    %>
    <script type="text/javascript" charset='euc-kr'>
    <!--
        alert('수정되었습니다.');
    //-->
    </script>
    <%
    end if

    if upresult <> "10" AND upresult <> "" then
    %>
    <script type="text/javascript" charset='euc-kr'>
    <!--
        alert('<%=inresult %>');
    //-->
    </script>
    <%
    end if
    %>
    <script type="text/javascript" charset='euc-kr'>
        location="list.asp?table_idx=<%=table_id%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>"
    </script>

<%   
'데이터 베이스에 현재의 레코드를 update 하고 리스트로 돌아간다.
else

%>

    <script type="text/javascript" charset='euc-kr'>
        alert("비밀번호가 일치하지 않습니다.");
        history.back();
    </script>

<% 
end if
%>
     
</html>