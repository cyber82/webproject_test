<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
</head>

<%
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  
%>

<!--#include file="../../lib/dbcon.asp"-->

<%

  keyfield = request("keyfield")	
  key = request("key")	

  sql = "select * from event_200611 order by c_cnt desc"

  set rs = server.CreateObject("adodb.recordset")

  rs.PageSize=10 


  rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
%>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="0">
		<tr>
          <td align="center" >
		  <br><img src="../img/icon01.gif" width="22" height="18" align="absmiddle"> <strong><font color="#3788D9">로그인 횟수</font>
		  <br><br>
		  </td>
        </tr>
      </table>   
	  <table width="500" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100" align="center" bgcolor="#EBEBEB" height="25"><b>번호</td>
          <td width="100" align="center" bgcolor="#EBEBEB"><b>아이디</td>
          <td width="100" align="center" bgcolor="#EBEBEB"><b>이름</td>
          <td width="100" align="center" bgcolor="#EBEBEB"><b>마지막접속일</td>
          <td width="100" align="center" bgcolor="#EBEBEB"><b>접속횟수</td>
        </tr>
      </table> 
 	  <table width="500" border="0" cellspacing="0" cellpadding="0">
		<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면%>
		<tr> 
          <td height="22" colspan="5" align="center"><b>자료가 없습니다!!!</b></td>
        </tr>
		<% else '데이터가 있다면
   
			totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
			rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
			endpage = startpage + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
			if endpage > rs.PageCount then
				endpage = rs.PageCount
			end if


			if request("page")="" then
				npage=1
			else
				npage=cint(request("page"))
			end if

			trd=rs.recordcount
			j=trd-Cint(rs.pagesize)*(Cint(npage)-1)
		%>
		<%
			 i=1
			 do until rs.EOF or i>rs.pagesize
	    %>

		
		<tr> 
          <td width="100" height="22" align="center">
		    <%=j%></td>
		  <td width="100" height="22" align="center">
		    <%=rs("user_id")%>
		  </td>
          <td width="100" align="center"> 
		    <%=rs("user_name")%>
		  </td>
          <td width="100" align="center"> 
		    <%=left(rs("writeday"),10)%>
		  </td>
          <td width="100" align="center"> 
		    <%=rs("c_cnt")%>
		  </td>
        </tr>
        <tr> 
          <td colspan="10" height="1" bgcolor="#EBEBEB"></td>
        </tr>
		<%
	 	   j=j-1
		   rs.MoveNext ' 다음 레코드로 이동한다.
	           i=i+1	
		   loop '레코드의 끝까지 loop를 돈다.
		%> 
		<% end if %>
      </table>
      <table width="500" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td colspan="3"><img src="image/b_bottom.gif" width="600" height="4"></td>
        </tr>
        <tr> 
          <td height="10" colspan="3"></td>
        </tr>
        <tr> 
          <td align="center"colspan="3">
		  <% if cint(startpage)<>cint(1) then%> 
             <a href="member_08.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
             <img src="image/ppre.gif" width="15" height="15" border="0" align="absmiddle"></a>
          <%end if%>
			<% for i = startpage to endpage step 1 %>
             <% if cint(i) = cint(page) then%>
                 [<%=page%>]
             <%else%>
                 <a href="member_08.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
                  <%=i%> </a>
                 <!--해당되는 페이지로 이동시킨다.-->
             <%end if%>
            <% next%>
		  <% if cint(endpage)<>cint(rs.PageCount) then%>
             <a href="member_08.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
             <img src="image/nnext.gif" width="15" height="15" border="0" align="absmiddle"></a>
          <% end if%>
		  </td>
        </tr>
      </table>
<% '사용한 개체드릉 모두 반납한다.
 rs.close
 db.Close
  set rs = nothing
  set db = nothing
%>
</body>
</html>
