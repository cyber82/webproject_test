﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
'*******************************************************************************
'페이지 설명 : 팝업창 관리
'작성일      : 2013-05-31
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
%>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%

if request("idx") = "" then
    idx = 0
else 
    idx = request("idx")
end if

page = Request("page")
startpage = Request("startpage")

'기본 저장위치
saveDefaultPath = "/main_image/popup/"


'test
'idx = 0


If CInt(idx) > 0 Then

    sql = " SELECT idx, "
    sql = sql & " cont_img, del_flag " 
    sql = sql & " FROM [compassweb4].[dbo].[tPopup] "
    sql = sql & " WHERE 1=1 AND del_flag=0 "
    sql = sql & " AND idx = " &idx

    'response.Write sql
    'response.End

    Set rs = RecordsetFromWS(provWSDL, sql) 

    If Not rs.eof Then

        idx = rs("idx")
        cont_img = rs("cont_img")

        host = request.ServerVariables("HTTP_HOST")
        addr = "http://www.compassion.or.kr"
        'addr = "http://www.compassionko.org"

        'If cont_img <> "" Then contImg = "<img src='"& addr & cont_img &"' border='0'>" End If
        If cont_img <> "" Then contImg = "<img src='"& cont_img &"' border='0'>" End If

	End If


    '============= 파일 삭제 ==============
    If cont_img <> "" Then
	    arr_fname1 = Split(cont_img, "/")
	    fname = arr_fname1(Ubound(arr_fname1))

	    strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
	    strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
	    strDefFolder = Server.MapPath(saveDefaultPath) & "\"
	    strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
	    strDelPath1 = strSaveFolder & fname

	    Set fso = CreateObject("Scripting.FileSystemObject")
	    '파일 존재시 파일 삭제
	    If fso.FileExists(strDelPath1) Then
	       fso.DeleteFile strDelPath1
	    End If
	    Set fso = Nothing

    End if
    '============= //파일 삭제 ============== 
End If


link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"

if CInt(idx) > 0 then 

    sql = " UPDATE [compassweb4].[dbo].[tPopup] SET "
    sql = sql & " del_flag = 1 "
    sql = sql & " WHERE idx = " & idx

    inresult = BoardWriteWS(provWSDL, sql)

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('삭제되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('삭제중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if
else 
    response.write "<script type='text/javascript' charset='euc-kr'> alert('글이 없어 삭제할 수 없습니다.'); </script>"
    response.write link
end if 
%>

</html>
