<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 팝업창 관리
'작성일      : 2013-05-31
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/freeaspupload.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
Dim strFilePath1, strFilePath2
Dim tempFile1, tempFile2


saveDefaultPath = "/main_image/popup/"

'저장 폴더명
Dim strDateDir : strDateDir = getDateDivDir(0, "\")
strDefFolder = Server.MapPath(saveDefaultPath) & "\"
strSaveFolder = strDefFolder & strDateDir

Call MkDirAll(strSaveFolder)	'폴더 생성

uploadTempDir = strSaveFolder
Set Upload = New FreeASPUpload


'***************** 이미지 저장 *****************************
Upload.Save(uploadTempDir)

for each fileKey in Upload.UploadedFiles.keys

	Select Case fileKey
		Case "fname1" : tempFile1 = Upload.UploadedFiles("fname1").FileName
	End select

Next
'***************** 이미지 저장 *****************************

If tempFile1 <> "" Then
	vFilename = tempFile1						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile1)

	'새이름 생성
	newFilename1 = MakeFileName2(strName) & "."&strExt
	file.name = newFilename1

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로 (배너이미지)
	strFilePath1 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename1
End If


    wtype = Upload.Form("wtype") '등록, 수정 구분
    page = Upload.Form("page")
    startpage = Upload.Form("startpage")
    idx = Upload.Form("idx")

	p_type = Upload.Form("p_type")
	p_name = Upload.Form("p_name")

    start_date = Upload.Form("sdate")
    end_date = Upload.Form("edate")
    close_day = Upload.Form("close_day")

    title = Upload.Form("title")
    contents = Upload.Form("contents")
    cont_img = Upload.Form("cont_img")
	
	p_top = Upload.Form("p_top")
	p_left = Upload.Form("p_left")
	p_width = Upload.Form("p_width")
	p_height = Upload.Form("p_height")
	p_link = Upload.Form("p_link")
	p_target = Upload.Form("p_target")
	p_status = Upload.Form("p_status")
	p_scrollbar = Upload.Form("p_scrollbar")

    view_flag = Upload.Form("view_flag")


    'Response.Write event_num + "<br/>"
    'Response.Write title + "<br/>"
    'Response.Write easy_cont + "<br/>"
    'Response.Write page_type + "<br/>"
    'Response.Write head_flag + "<br/>"
    'Response.Write view_flag + "<br/>"
    
    '테스트 2013-11-05
    'Response.Write "p_link : " + p_link + "<br/>"
    'Response.Write len(p_link)
    'Response.End


    '수정 2013-06-11
    if contents = "" or contents = "<BR>" then
        host = request.ServerVariables("HTTP_HOST")
        addr = "http://www.compassion.or.kr"
        'addr = "http://www.compassionko.org"
    
        
        if strFilePath1 <> "" then
		    strImg1 = strFilePath1
	    else
		    strImg1 = cont_img
	    end If

        '수정 2013-06-03
        if p_target = "B" then
            
            'contents = "<a href="""&p_link&""" target=""_blank""><img src="""& addr & strImg1 &""" alt=""""  style=""border:0;"" /></a>"
            contents = "<a href="""&p_link&""" target=""_blank""><img src="""& strImg1 &""" alt=""""  style=""border:0;"" /></a>"
        
        elseif p_target = "S" then
            
            'contents = "<a href=""javascript:selfLink('"&p_link&"');""><img src="""& addr & strImg1 &""" alt=""""  style=""border:0;"" /></a>"
            contents = "<a href=""javascript:selfLink('"&p_link&"');""><img src="""& strImg1 &""" alt=""""  style=""border:0;"" /></a>"

        end if
    end if
    
    title = replace(title, "'", "''")
    contents = replace(contents, "'", "''")
    p_link = replace(p_link, "'", "''")


    '파일
    fname1 = Upload.Form("fname1")	'상단 내용 이미지
    cont_img = Upload.Form("cont_img") '상단 내용 이미지

    link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"

'response.write "strFilePath1: " & strFilePath1 & "<br/>"
'response.write "tempFile1: " & tempFile1
'response.Write share_desc
'response.end


'등록
if wtype = "write" then

    sql = " INSERT INTO [compassweb4].[dbo].[tPopup] ("
	sql = sql & " [p_type],[p_name],[start_date],[end_date],[close_day] "
	sql = sql & ", [title],[contents],[cont_img] "
	sql = sql & ", [p_top],[p_left],[p_width],[p_height] "
	sql = sql & ", [p_link],[p_target],[p_status],[p_scrollbar] "
	sql = sql & ", [view_flag] "
    sql = sql & " ) VALUES ( "
    sql = sql & " '"& p_type &"','"& p_name &"' "
    
    if start_date <> "" then   sql = sql & " , '"&start_date&"' "  else   sql = sql & " , null "   end if 
    if end_date <> "" then   sql = sql & " , '"&end_date&"' "  else   sql = sql & " , null "   end if 
        
    sql = sql & " , '"& close_day &"','"& title &"','"& contents &"','"& strFilePath1 &"' "
    sql = sql & " , '"& p_top &"','"& p_left &"','"& p_width &"','"& p_height &"' "
    sql = sql & " , '"& p_link &"','"& p_target &"','"& p_status &"','"& p_scrollbar &"' "
    sql = sql & " , '"& view_flag &"' "
    sql = sql & " ) "


    'response.write sql
    'response.end
    inresult = BoardWriteWS(provWSDL, sql)

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('등록되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('등록중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if

'수정
Else

	if strFilePath1 <> "" then
		'============= 기존파일 삭제 ==============
		If cont_img <> "" AND cont_img <> newFilename1 Then
			arr_fname1 = Split(cont_img, "/")
			fname = arr_fname1(Ubound(arr_fname1))

			strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
			strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============
		strImg1 = strFilePath1
	else
		strImg1 = cont_img
	end If
   	

    sql = " UPDATE [compassweb4].[dbo].[tPopup] SET "
    sql = sql & " p_type='"& p_type &"', p_name='"& p_name &"' "

    if start_date <> "" then   sql = sql & " , [start_date]='"& start_date &"' "  else   sql = sql & " , [start_date]=null "   end if 
    if end_date <> "" then   sql = sql & " , [end_date]='"& end_date &"' "  else   sql = sql & " , [end_date]=null "   end if 

    sql = sql & " , close_day='"& close_day &"', title='"& title &"', contents='"& contents &"' "
    sql = sql & " , cont_img='"& strImg1 &"', p_top='"& p_top &"', p_left='"& p_left &"'  "
    sql = sql & " , p_width='"& p_width &"', p_height='"& p_height &"', p_link='"& p_link &"'  "
    sql = sql & " , p_target='"& p_target &"', p_status='"& p_status &"', p_scrollbar='"& p_scrollbar &"'  "
    sql = sql & " , view_flag='"& view_flag &"' "
    sql = sql & " WHERE idx = '"& idx &"' "

    'response.write sql
    'response.end
    inresult = BoardWriteWS(provWSDL, sql)

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('수정되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('수정중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if

End If

%>