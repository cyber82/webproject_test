﻿
<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 팝업창 관리
'작성일      : 2013-05-31
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
    if request("page") = "" then
        page = 1
    else
        page=request("page")
    end if
  
    if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
        startpage = 1
    else
        startpage = request("startpage")
    end if  

    if request("idx") = "" then
        idx = 0
    else 
        idx = request("idx")
    end if

	keyfield = request("keyfield")	
	key = request("key")	

    'test
    'idx = 94

If CInt(idx) > 0 Then
	wtype = "edit"
    
    sql = " SELECT "
    sql = sql & " idx, p_type, p_name, close_day, title, contents, cont_img " 
    sql = sql & " , p_top, p_left, p_width, p_height, p_link, p_target " 
    sql = sql & " , p_status, p_scrollbar, view_flag, del_flag "
    sql = sql & " , convert(varchar(20),[start_date],120) as [start_date] "
    sql = sql & " , convert(varchar(20),end_date,120) as end_date "
    sql = sql & " , convert(varchar(20),reg_date,120) as reg_date "
    sql = sql & " FROM [compassweb4].[dbo].[tPopup] "
    sql = sql & " WHERE 1=1 AND del_flag=0 "
    sql = sql & " AND idx = " &idx

    set rs = RecordsetFromWS(provWSDL, sql) 

	if rs.BOF or rs.EOF then
    else
        idx = rs("idx")

	    p_type = rs("p_type")
	    p_name = rs("p_name")

        start_date = rs("start_date")
        end_date = rs("end_date")
        close_day = rs("close_day")

        title = rs("title")
        contents = rs("contents")
        cont_img = rs("cont_img")
	
	    p_top = rs("p_top")
	    p_left = rs("p_left")
	    p_width = rs("p_width")
	    p_height = rs("p_height")
	    p_link = rs("p_link")
	    p_target = rs("p_target")
	    p_status = rs("p_status")
	    p_scrollbar = rs("p_scrollbar")

        view_flag = rs("view_flag")
        reg_date = rs("reg_date")


        contents = replace(contents, "&lt;", "<")
        contents = replace(contents, "&gt;", ">")
        contents = replace(contents, "&amp;", "&")
        contents = replace(contents, "&apos;", "'")
        contents = replace(contents, "&quot;", """")
                

        'Response.Write event_num + "<br/>"
        'Response.Write title + "<br/>"
        'Response.Write easy_cont + "<br/>"
        'Response.Write page_type + "<br/>"
        'Response.Write head_flag + "<br/>"
        'Response.Write view_flag + "<br/>"
        'Response.End
        
        
        host = request.ServerVariables("HTTP_HOST")
        addr = "http://www.compassion.or.kr"
        'addr = "http://www.compassionko.org"
        
        '이미지 태그
        'If cont_img <> "" Then contImg = "<img src='"& addr & cont_img &"' border='0'>" End If
        If cont_img <> "" Then contImg = "<img src='"& cont_img &"' border='0'>" End If

	End If
    
Else
	wtype = "write"

    view_flag = "False"
End If

%>
<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css" />
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
    .sbox{border:1px solid #bbb;}
    </style>

    <script type="text/javascript" src="cal.js"></script>
    <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery-ui-timepicker-addon.js"></script>

    <% '추가 2013-07-17 %>
    <script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/tinymce_config.js" charset='euc-kr'></script>
    <script type="text/javascript">
        tinymce_config("contents");
    </script>

    <script type="text/javascript">
        <!--

        jQuery(document).ready(function () {
            jQuery.timepicker.setDefaults({
                monthNames: ['년 1월', '년 2월', '년 3월', '년 4월', '년 5월', '년 6월', '년 7월', '년 8월', '년 9월', '년 10월', '년 11월', '년 12월'],
                dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
                showMonthAfterYear: true,
                dateFormat: 'yy-mm-dd',
                timeFormat: 'hh:mm:ss',
                duration: '',
                showTime: true,
                showSecond: true,
                constrainInput: false,
                stepMinutes: 1,
                stepHours: 1,
                altTimeField: '',
                time24h: true
            });
            jQuery("#sdate").timepicker();
            jQuery("#edate").timepicker();
        }
        );

        /*
        $(function () {
            $('#sdate').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '2009:2020'
            });
        });

        $(function () {
            $('#edate').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '2009:2020'
            });
        });
        */

        function doWrite(){
            var f = document.write_f;

            if (f.p_type.value == "") {
                alert("팝업구분을 선택해 주세요.");
                f.p_type.focus();
                return;
            }
            if (f.p_name.value == "") {
                alert("팝업명을 입력해 주세요.");
                f.p_name.focus();
                return;
            }
		    if (f.title.value == "") {
		        alert("제목을 입력해 주세요.");
		        f.title.focus();
		        return;
		    }
            /*
	        if (f.sdate.value == "") {
		        alert("기간 시작일을 입력해 주세요.");
		        f.sdate.focus();
		        return;
	        }
	        if (f.edate.value == "") {
		        alert("기간 종료일을 입력해 주세요.");
		        f.edate.focus();
		        return;
		    }
            */
		    if (f.close_day.value == "") {
		        alert("팝업 창닫기 일정을 입력해 주세요.");
		        f.close_day.focus();
		        return;
		    }
		    if (f.view_flag.value == "") {
		        alert("보기유형 여부를 선택해 주세요.");
		        f.view_flag.focus();
		        return;
		    }
		    if (f.p_top.value == "") {
		        alert("세로위치를 입력해 주세요.");
		        f.p_top.focus();
		        return;
		    }
		    if (f.p_left.value == "") {
		        alert("가로위치를 입력해 주세요.");
		        f.p_left.focus();
		        return;
		    }
		    if (f.p_width.value == "") {
		        alert("WIDTH를 입력해 주세요.");
		        f.p_width.focus();
		        return;
		    }
		    if (f.p_height.value == "") {
		        alert("HEIGHT를 입력해 주세요.");
		        f.p_height.focus();
		        return;
		    }
		    if (f.p_status.value == "") {
		        alert("브라우저 Status를 선택해 주세요.");
		        f.p_status.focus();
		        return;
		    }
		    if (f.p_scrollbar.value == "") {
		        alert("브라우저 Scrollbar를 선택해 주세요.");
		        f.p_scrollbar.focus();
		        return;
		    }
		    if (f.p_link.value == "") {
		        alert("링크를 입력해 주세요.");
		        f.p_link.focus();
		        return;
		    }
		    if (f.p_target.value == "") {
		        alert("링크타겟를 선택해 주세요.");
		        f.p_target.focus();
		        return;
		    }
		    if (f.fname1.value == "" && f.cont_img.value == "") {
		        alert("팝업내용 이미지를 선택해 주세요.");
		        f.fname1.focus();
		        return;
		    }

		    f.action = "write_proc.asp";
		    f.method = "post";
		    f.target = "_self";

		    if ("<%=wtype%>" == "edit") {
		        if (confirm("수정하시겠습니까?")) {
		            f.submit();
		        }
		        else {
		            return;
		        }
		    }
		    else {
		        if (confirm("등록하시겠습니까?")) {
		            f.submit();
		        }
		        else {
		            return;
		        }
		    }
	    }


        function doDel(){
	        if (confirm("[주의]삭제 하시겠습니까?")) {
		        var f = document.del_f;
		        f.action = "del_proc.asp";
		        f.submit();
	        }
		}

	    //- 숫자만입력
	    function OnlyNumber() {
	        key = event.keyCode;

	        if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
	            event.returnValue = true;
	        }
	        else {
	            event.returnValue = false;
	        }
	    }

    //-->
    </script>
</head>

<body>
<!--<body leftmargin="0" topmargin="0" oncontextmenu='return false' ondragstart='return false'>-->

<p style="margin:20px;">
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">캠페인&이벤트</font></strong>
</p>

<form name="del_f" id="del_f" method="post">
<input type="hidden" name="idx" value="<%=idx%>" />
<input type="hidden" name="page" value="<%=page%>" />
<input type="hidden" name="startpage" value="<%=startpage%>" />
</form>

<form name="write_f" id="write_f" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="idx" value="<%=idx%>" />
<input type="hidden" name="page" value="<%=page%>" />
<input type="hidden" name="startpage" value="<%=startpage%>" />
<input type="hidden" name="wtype" value="<%=wtype%>" />

<table width="870px" border="0" cellpadding="3" cellspacing="1" bgcolor="#DFDFDF" style="margin-left:20px;">
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">

    <% If CInt(idx) > 0 Then %>
        <input type="button" value="수정" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
        <input type="button" value="삭제" onclick="doDel();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" /> 
    <% Else %>
        <input type="button" value="등록" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    <% End if %>

        <input type="button" value="취소" onclick="location.href='list.asp?page=<%=page%>';" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    </td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="140px"><b>팝업구분</b></td>
	<td bgcolor="#FFFFFF" width="730px">
 		<select id="p_type" name="p_type" style="height:18px;" tabindex="1">
            <option value="main" <% if p_type = "main"  then %> selected <% end if %>>메인</option>
            <option value="mate" <% if p_type = "mate"  then %> selected <% end if %>>메이트</option>
        </select>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="140px"><b>팝업명</b></td>
	<td bgcolor="#FFFFFF" width="730px">
		<input type="text" name="p_name" id="p_name" size="20" maxlength="20" value="<%=p_name%>" style="ime-mode:disabled;" class="sbox" tabindex="2" />
        (팝업코드를 영어로 예: code1)
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="140px"><b>제목</b></td>
	<td bgcolor="#FFFFFF" width="730px">
		<input type="text" name="title" id="title" size="50" maxlength="50" value="<%=title%>" style="ime-mode:active;" class="sbox" tabindex="3" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>팝업 시작일</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="sdate" id="sdate" size="20" value="<%=start_date%>" style="cursor:pointer;" class="sbox" tabindex="4" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>팝업 종료일</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="edate" id="edate" size="20" value="<%=end_date%>" style="cursor:pointer;" class="sbox" tabindex="5" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>팝업 창닫기 일정</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="close_day" id="close_day" size="5" maxlength="10" value="<% if close_day <> "" then %><%=close_day%><% else %>1<% end if %>" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" tabindex="6" />일
	</td>
  </tr>

  <tr>
    <td bgcolor="#F3F3F3"><b>보기유형 여부</b></td>
	<td bgcolor="#FFFFFF">
        <input type="radio" name="view_flag" id="view_flag1" value="0" <% if view_flag = "False" then %> checked <% end if %> tabindex="7" /> 보이기 &nbsp;&nbsp;
		<input type="radio" name="view_flag" id="view_flag2" value="1" <% if view_flag = "True" then %> checked <% end if %> tabindex="7" /> 보이지 않기 &nbsp;&nbsp;
	</td>
  </tr>

  <tr>
    <td bgcolor="#F3F3F3"><b>팝업 사이즈</b></td>
	<td bgcolor="#FFFFFF">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr height="35px">
            <td>
                세로위치: <input type="text" name="p_top" id="p_top" size="5" maxlength="10" value="<% if p_top <> "" then %><%=p_top%><% else %>50<% end if %>" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" tabindex="8" />px
                &nbsp;&nbsp;
                가로위치: <input type="text" name="p_left" id="p_left" size="5" maxlength="10" value="<% if p_left <> "" then %><%=p_left%><% else %>50<% end if %>" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" tabindex="9" />px
                &nbsp;&nbsp;
                WIDTH: <input type="text" name="p_width" id="p_width" size="5" maxlength="10" value="<% if p_width <> "" then %><%=p_width%><% else %>270<% end if %>" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" tabindex="10" />px
                &nbsp;&nbsp;
                HEIGHT: <input type="text" name="p_height" id="p_height" size="5" maxlength="10" value="<% if p_height <> "" then %><%=p_height%><% else %>430<% end if %>" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" tabindex="11" />px
            </td>
        </tr>
        <tr height="35px">
            <td>
                브라우저 Status: 
                <select id="p_status" name="p_status" style="height:18px;" tabindex="12">
                    <option value="N" <% if p_scrollbar = "N"  then %> selected <% end if %>>사용안함</option>
                    <option value="Y" <% if p_scrollbar = "Y"  then %> selected <% end if %>>사용</option>
                </select>
                &nbsp;&nbsp;
                브라우저 Scrollbar: 
                <select id="p_scrollbar" name="p_scrollbar" style="height:18px;" tabindex="13">
                    <option value="N" <% if p_scrollbar = "N"  then %> selected <% end if %>>사용안함</option>
                    <option value="Y" <% if p_scrollbar = "Y"  then %> selected <% end if %>>사용</option>
                </select>
            </td>
        </tr>
        <tr height="35px">
            <td>
                링크: <input type="text" name="p_link" id="p_link" size="50" maxlength="200" value="<%=p_link%>" style="ime-mode:disabled;" class="sbox" tabindex="14" />
                &nbsp;&nbsp;
                링크타겟: 
                <select id="p_target" name="p_target" style="height:18px;" tabindex="15">
                    <option value="B" <% if p_target = "B"  then %> selected <% end if %>>새창</option>
                    <option value="S" <% if p_target = "S"  then %> selected <% end if %>>부모창</option>
                </select>
            </td>
        </tr>
        </table>
	</td>
  </tr>

  <tr>
    <td bgcolor="#F3F3F3"><b>상세내용</b></td>
	<td bgcolor="#FFFFFF">
        <% '수정 2013-06-07 %>
        <div>
	        <textarea name="contents" id="contents" style="width:700px;height:170px;" class="sbox" tabindex="16"><%=contents %></textarea>
        </div>
        <!--<div><button onClick="addArea2();">Editor</button> <button onClick="removeArea2();">Html</button></div>-->
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>상세화면 내용<br />이미지</b></td>
	<td bgcolor="#FFFFFF">
		<%=contImg%><br />
        <%=cont_img%><br />
        <input type="file" name="fname1" id="fname1" size="50" style="ime-mode:disabled;" class="sbox" tabindex="17" />
        <input type="hidden" name="cont_img" id="cont_img" value="<%=cont_img%>" />
	</td>
  </tr>

  <tr>
    <td colspan="2" bgcolor="#FFFFFF">

    <% If CInt(idx) > 0 Then %>
        <input type="button" value="수정" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" tabindex="18" />
        <input type="button" value="삭제" onclick="doDel();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" /> 
    <% Else %>
        <input type="button" value="등록" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" tabindex="18" />
    <% End if %>

        <input type="button" value="취소" onclick="location.href='list.asp?page=<%=page%>';" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    </td>
  </tr>
</table>
</form>

    <% '추가 2013-06-07 %>
    <% '주석처리 2013-07-17 %>
    <!--<script src="/Web_Backoffice/backoffice/inc/js/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        var area1, area2;
        /*
        function toggleArea1() {
            if (!area1) {
                area1 = new nicEditor({ fullPanel: true }).panelInstance('myArea1', { hasPanel: true });
            } else {
                area1.removeInstance('myArea1');
                area1 = null;
            }
        }
        */

        function addArea2() {
            area2 = new nicEditor({ fullPanel: true }).panelInstance('contents');
        }
        function removeArea2() {
            area2.removeInstance('contents');
        }

        bkLib.onDomLoaded(function () { addArea2(); });
    </script>-->	

    <script type="text/javascript">
    <!--
        document.write_f.p_type.focus();
    //-->
    </script>
</body>
</html>