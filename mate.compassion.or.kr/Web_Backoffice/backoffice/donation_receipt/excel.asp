﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  'if request("page") = "" then
    'page = 1
  'else
    'page=request("page")
  'end if
  
  'if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   'startpage=1
  'else
   'startpage = request("startpage")
  'end if  

  If request("key") = "" Then 
  Else
	keyfield = request("keyfield")	
	key = request("key")
  End if 
%>


<%
	'If session("admin_id")="" Then
		
		'response.write "<script>alert('로그인 후 이용해주세요.'); top.location.href='/admin/login.asp';</script>"
	    'response.end

	'End If
	
%>

<%
	Call setXls()
	Sub setXls()
		response.write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>")
	    '수정 ) 파일명 깨짐
        xls_filename = Server.URLEncode("기부금_영수증_우편발송_신청목록")
        'xls_filename = "기부금_영수증_우편발송_신청목록"
		Response.ContentType  = "application/x-excel"
	    Response.CacheControl  = "public" 
		Response.AddHeader  "Content-Disposition" , "attachment; filename="& xls_filename & ".xls" 	

	
	End Sub
%>

<html>
<head>
<title>관리자화면</title>
<!--link rel="stylesheet" type="text/css" href="../index.css"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
	<table cellpadding="1" cellspacing="1" border="0" width="100%">
		<tr align="center" bgcolor="#3399CC" height="30px" style="font-weight:bold; color:#FFFFFF;">
            <td width="8%" height="25" align="center">번호</td>
			<td width="10%" align="center">conid</td>
            <td width="10%" align="center">이름</td>
            <td width="15%" align="center">등록시간</td>
            <td width="35%" align="center">주소</td>
            <td width="12%" align="center">전화번호</td>
            <td width="10%" align="center">신청</td>      
		</tr>
		<%
            sql = " SELECT Idx, ConID, UserID, UserName, ZipCode, Address1, Address2, Tel, Flag "
            sql = sql & " 	, convert(varchar(20),RegDate,120) as RegDate "
            sql = sql & " FROM [compassweb4].[dbo].[tDonationReceiptZip] "
            sql = sql & " WHERE 1=1 "
            'sql = sql & " AND Flag = 'Y' "


            if key = "" then
	            sql = sql & " "
            else
	            sql = sql & " and "&keyfield&" like '%"&key&"%' "
            end if

            sql = sql & " ORDER BY RegDate DESC "


            'set rs = server.CreateObject("adodb.recordset")
            'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
 
            'response.Write sql
            'response.End  
            Set rs = RecordsetFromWS(provWSDL, sql)
		%>
		<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면%>
		<tr> 
			<td height="22" colspan="6" align="center"><b>글이 없습니다!!!</b></td>
		</tr>
		<% else '데이터가 있다면
			j = rs.RecordCount

			i=1
			do until rs.EOF

                juso = rs("ZipCode") & " " & rs("Address1") & " " & rs("Address2")

                ask = ""
                if rs("Flag") = "Y" then
                    ask = "신청"
                else
                    ask = "취소"
                end if
		%>
        <tr bgcolor="#FFFFFF" style="border-bottom:1px solid #ccc;" height="30px"> 
            <td align="center"><%=j%></td>
            <!--<td align="center" style="mso-number-format:\@"><% '=rs("sponsor") %></td>-->
            <td align="center"><%=rs("ConID") %></td>
			<td align="center"><%=rs("UserName") %></td>
            <td align="center"><%=rs("RegDate") %></td>
            <td align="left"><%=juso %></td>
            <td align="center"><%=rs("Tel") %></td>
            <td align="center"><%=ask %></td>
        </tr>
		<%									
			    j = j - 1
			    rs.MoveNext
            	i = i + 1
			Loop 				
		%>
		<%	
			End If 
			rs.close
			Set rs=Nothing 
		%>
	</table>
</body>
</html>