﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

	keyfield = request("keyfield")	
	key = request("key")	
%>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>


<body leftmargin="0" topmargin="0">

<script type="text/javascript" charset='euc-kr'>
<!--
    function trColor(sw, idx)
    {
        var id = document.getElementById("dataTr_" + idx);

        if (sw == "1") {
            id.style.backgroundColor = "#ddd";
        }
        else if (sw == "0") {
            id.style.backgroundColor = "#fff";
        }
    }

    function cancelEvent(val, state) {
        var str = "";
        if (state == "Y") {
            str = "신청";
        }
        else {
            str = "신청 취소";
        }

        if (confirm(str + "하시겠습니까?")) {
            location.href = "cancel_proc.asp?idx=" + val + "&flag=" + state;
        }
    }
//-->
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9">기부금 영수증 우편발송 신청목록</font></strong></td>
                      <td align="right">
                        <a href="excel.asp?keyfield=<%=keyfield%>&key=<%=key%>"><strong><font color="#3788D9">엑셀 다운로드</font></strong></a>
                      </td>
                    </tr>
                </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                        <td width="8%" height="25" align="center">번호</td>
			            <td width="10%" align="center">conid</td>
                        <td width="10%" align="center">이름</td>
                        <td width="15%" align="center">등록시간</td>
                        <td width="35%" align="center">주소</td>
                        <td width="12%" align="center">전화번호</td>
                        <td width="10%" align="center">신청</td>
                    </tr>


            <!------- 게시판 목록 구하기 시작 ----------> 
            <%
                sql = " SELECT Idx, ConID, UserID, UserName, ZipCode, Address1, Address2, Tel, Flag "
                sql = sql & " 	, convert(varchar(20),RegDate,120) as RegDate "
                sql = sql & " FROM [compassweb4].[dbo].[tDonationReceiptZip] "
                sql = sql & " WHERE 1=1 "
                'sql = sql & " AND Flag = 'Y' "

                if key = "" then
	                sql = sql & " "
                else
	                sql = sql & " and "&keyfield&" like '%"&key&"%' "
                end if
                                
                sql = sql & " ORDER BY RegDate DESC "


                'set rs = server.CreateObject("adodb.recordset")
                'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
 
                'response.Write sql
                'response.End  
                Set rs = RecordsetFromWS(provWSDL, sql)
            %>					
					
				<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
					<tr> 
					  <td height="22" colspan="6" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
				<% else '데이터가 있다면
                    
                    rs.PageSize = 20 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

					totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
                    ''totalpage = rsCnt("totalCnt")

					rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
					endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
					''totalpage = Int(rsCnt("totalCnt") / rs.PageSize) + 1

                    'Response.Write rsCnt("totalCnt") & "<br/>"
                    'Response.Write rs.PageSize & "<br/>"
                    'Response.Write rs.PageCount & "<br/>"
                    'Response.End

                    if endpage > totalpage then
						endpage = totalpage
					end If
                    
					if request("page")="" then
						npage = 1
					else
						npage = CInt(request("page"))
					end if
						
					trd = rs.RecordCount
                    ''trd = rsCnt("totalCnt")
					j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)

                    'Response.Write("totalpage : " & totalpage & "<br/>")
                    'Response.Write("startpage : " & startpage & "<br/>")
                    'Response.Write("endpage : " & endpage & "<br/>")
                    'Response.Write("rs.PageSize : " & rs.PageSize & "<br/>")
                    'Response.Write("npage : " & npage & "<br/>")
                    'Response.Write("trd : " & trd & "<br/>")
				%>
				<%
				    i = 1
					do until rs.EOF or i > rs.PageSize

                        juso = rs("ZipCode") & " " & rs("Address1") & " " & rs("Address2")

                        ask = ""
                        if rs("Flag") = "Y" then
                            ask = "신청"
                        else
                            ask = "취소"
                        end if
				%>
                    <tr id="dataTr_<%=i %>" bgcolor="#FFFFFF" onmouseover='javascript:trColor(1, "<%=i %>");' onmouseout='javascript:trColor(0, "<%=i %>");'> 
                      <td height="25" align="center"><%=j%></td>
                      <td align="center"><%=rs("ConID") %></td>
					  <td align="center"><%=rs("UserName") %></td>
                      <td align="center"><%=rs("RegDate") %></td>
                      <td align="left" style="padding:0px 10px 0px 10px;"><%=juso %></td>
                      <td align="center"><%=rs("Tel") %></td>
                      <td align="center" valign="middle">
                        <%=ask %>
                        <% if rs("Flag") = "Y" then %>
                            <input type="button" id="btn1" value="취소" onclick='cancelEvent("<%=rs("Idx") %>", "N");'
                            style="border:1px solid #ddd;background-color:#999;color:#fff;cursor:pointer;height:18px;" />
                        <% else %>
                            <input type="button" id="btn2" value="신청" onclick='cancelEvent("<%=rs("Idx") %>", "Y");'
                            style="border:1px solid #ddd;background-color:#999;color:#fff;cursor:pointer;height:18px;" />
                        <% end if %> 
                      </td>
                    </tr>

            <%
                'Response.Write("j : " & j & "<br/>")
                'Response.Write("i : " & i & "<br/>")

                j=j-1
				rs.MoveNext ' 다음 레코드로 이동한다.
			    i=i+1
				loop '레코드의 끝까지 loop를 돈다.
			%> 
            <% 
         	    rs.Close
	            Set rs = nothing 

                end if
            %>               
            <!------- 게시판 목록 구하기 끝 ---------->  
                                    
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <!--<tr>
                <td align="right"><a href="write.asp?page=<%'=page%>" onfocus="this.blur()"><img src="../img/btn17.gif" width="52" height="20" border="0"></a></td>
              </tr>-->
              <tr> 
                <td align="center">
					<% if cint(startpage)<>cint(1) then%> 
				         <a href="list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [이전]</a>
			        <%end if%> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then%>
				         [<%=page%>]
		             <%else%>
					     <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"> <%=i%> </a>
		             <%end if%>
				   <% next%>

				   <% if cint(endpage)<>cint(totalpage) then%>
			             <a href="list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [다음]</a>
			       <% end if%>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
			  
              <tr> 
                <td align="center"><table width="30%" border="0" cellspacing="2" cellpadding="0">
					<form name="searchForm" action="list.asp" method="post" onSubmit="return searchSendit();">	
                    <tr> 
                      <td>
                        <select id="keyfield" name="keyfield" style="height:19px;" onchange="keyfieldChange(this.value);">
                            <option value="UserName" <% if keyfield = "UserName" then %> selected <% end if %>>후원자명</option>
                            <option value="ConID" <% if keyfield = "ConID" then %> selected <% end if %>>ConID</option>
                        </select>

                        <script type="text/javascript" charset="euc-kr">
                        <!--
                            function keyfieldChange(val) {

                                var k = document.getElementById("key");
                                k.value = "";

                                if (val == "a.UserID") {
                                    k.style.imeMode = "disabled";
                                } else {
                                    k.style.imeMode = "active";
                                }
                            }
                        //-->
                        </script>
                      </td>
                      <td><input id="key" name="key" type="text" class="text" value="<%=key %>" style="ime-mode:active;"></td>
                      <td><input type="submit" name="Submit" value="찾기"></td>
                    </tr>
					</form>
                  </table></td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
