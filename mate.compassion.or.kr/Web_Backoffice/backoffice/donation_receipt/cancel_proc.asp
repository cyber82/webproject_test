﻿<% @CODEPAGE = 65001 %>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%

if request("idx") = "" then
    idx = 0
else 
    idx = request("idx")
end if

page = Request("page")
startpage = Request("startpage")

if Request("flag") <> "" then
    flag = Request("flag")
else
    flag = "N"
end if


'test
'idx = 0

link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"

if CInt(idx) > 0 then 

    sql = " UPDATE [compassweb4].[dbo].[tDonationReceiptZip] SET "
    sql = sql & " Flag = '" & flag & "' "
    sql = sql & " WHERE idx = " & idx

    inresult = BoardWriteWS(provWSDL, sql)

    str = ""
    if flag = "Y" then
        str = "신청"
    else
        str = "신청 취소"
    end if

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('" & str & "되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('" & str & "중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if
else 
    response.write "<script type='text/javascript' charset='euc-kr'> alert('해당 글이 없습니다.'); </script>"
    response.write link
end if 
%>

</html>
