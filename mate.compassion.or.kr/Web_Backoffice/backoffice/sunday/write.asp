﻿
<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 컴패션 선데이 관리
'작성일      : 2013-06-26
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
    if request("page") = "" then
        page = 1
    else
        page=request("page")
    end if
  
    if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
        startpage = 1
    else
        startpage = request("startpage")
    end if  

    if request("idx") = "" then
        idx = 0
    else 
        idx = request("idx")
    end if

	keyfield = request("keyfield")	
	key = request("key")	


    nowDate = Now()
    nDate = Left(nowDate, 10)
    
    sYear = Left(nowDate, 4) - 1
    sMonth = Mid(nowDate, 6, 2)
    sDay = Mid(nowDate, 9, 2)
                                
    if request("c_date") = "" then 
        sdate = sYear & "-" & sMonth & "-" & sDay
    else
        sdate = request("c_date")
    end if


    'test
    'idx = 94

If CInt(idx) > 0 Then
	wtype = "edit"
    
    sql = " SELECT "
    sql = sql & " idx, c_year, c_date, church_name " 
    sql = sql & " , view_yn, del_yn " 
    sql = sql & " , convert(varchar(20),reg_date,120) as reg_date "
    sql = sql & " FROM [compassweb4].[dbo].[tCompassionSunday] "
    sql = sql & " WHERE 1=1 AND del_yn='N' "
    sql = sql & " AND idx = " &idx

    set rs = RecordsetFromWS(provWSDL, sql) 

	if rs.BOF or rs.EOF then
    else
        idx = rs("idx")

        c_year = rs("c_year")
        c_date = rs("c_date")
        church_name = rs("church_name")

        view_yn = rs("view_yn")
        reg_date = rs("reg_date")
        
	End If
    
Else
	wtype = "write"

    view_yn = "Y"
End If

%>
<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css" />
    <style type="text/css">
        .sbox{border:1px solid #bbb;}
    </style>

    <link type="text/css" href="../inc/js/themes/base/ui.all.css" rel="stylesheet" />
    <link type="text/css" href="../inc/js/ui/demos.css" rel="stylesheet" />

    <script type="text/javascript" src="../inc/js/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="../inc/js/ui/ui.core.js"></script>
    <script type="text/javascript" src="../inc/js/ui/ui.datepicker.js"></script>
    <script type="text/javascript" src="../inc/js/ui/ui.datepicker-ko.js"></script>

    <script type="text/javascript">
    <!--

        $(function () {
            $('#c_date').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '2009:2020'
            });
        });
        
        function doWrite(){
            var f = document.write_f;

            if (f.c_date.value == "") {
                alert("선데이 일정을 입력해 주세요.");
                f.c_date.focus();
                return;
            }
            if (f.church_name.value == "") {
		        alert("교회명을 입력해 주세요.");
		        f.church_name.focus();
		        return;
		    }
		    if (f.view_yn.value == "") {
		        alert("보기유형 여부를 선택해 주세요.");
		        f.view_yn.focus();
		        return;
		    }
 
		    f.action = "write_proc.asp";
		    f.method = "post";
		    f.target = "_self";

		    if ("<%=wtype%>" == "edit") {
		        if (confirm("수정하시겠습니까?")) {
		            f.submit();
		        }
		        else {
		            return;
		        }
		    }
		    else {
		        if (confirm("등록하시겠습니까?")) {
		            f.submit();
		        }
		        else {
		            return;
		        }
		    }
	    }


        function doDel(){
	        if (confirm("[주의]삭제 하시겠습니까?")) {
	            var f = document.write_f;
		        f.action = "del_proc.asp";
		        f.submit();
		    }
		    else {
		        return;
		    }
		}


	    //- 숫자만입력
	    function OnlyNumber() {
	        key = event.keyCode;

	        if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
	            event.returnValue = true;
	        }
	        else {
	            event.returnValue = false;
	        }
	    }
    //-->
    </script>
</head>

<body>
<!--<body leftmargin="0" topmargin="0" oncontextmenu='return false' ondragstart='return false'>-->

<p style="margin:20px;">
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9"><!--#include file="title.asp"//--></font></strong>
</p>

<form name="write_f" id="write_f" method="post">
<input type="hidden" name="idx" value="<%=idx%>" />
<input type="hidden" name="page" value="<%=page%>" />
<input type="hidden" name="startpage" value="<%=startpage%>" />
<input type="hidden" name="wtype" value="<%=wtype%>" />


<table width="870px" border="0" cellpadding="3" cellspacing="1" bgcolor="#DFDFDF" style="margin-left:20px;">
  <tr>
    <td bgcolor="#F3F3F3"><b>선데이 일정</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="c_date" id="c_date" size="20" value="<%=c_date%>" style="ime-mode:disabled;cursor:pointer;" class="sbox" tabindex="1" />
	</td>
  </tr>

  <tr>
    <td bgcolor="#F3F3F3" width="140px"><b>교회명</b></td>
	<td bgcolor="#FFFFFF" width="730px">
		<input type="text" name="church_name" id="church_name" size="50" maxlength="50" value="<%=church_name%>" style="ime-mode:active;" class="sbox" tabindex="2" />
	</td>
  </tr>

  <tr>
    <td bgcolor="#F3F3F3"><b>보기유형 여부</b></td>
	<td bgcolor="#FFFFFF">
        <input type="radio" name="view_yn" id="view_yn1" value="Y" <% if view_yn = "Y" then %> checked <% end if %> tabindex="3" /> 보이기 &nbsp;&nbsp;
		<input type="radio" name="view_yn" id="view_yn2" value="N" <% if view_yn = "N" then %> checked <% end if %> tabindex="3" /> 보이지 않기 &nbsp;&nbsp;
	</td>
  </tr>

  <tr>
    <td colspan="2" bgcolor="#FFFFFF">

    <% If CInt(idx) > 0 Then %>
        <input type="button" value="수정" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" tabindex="18" />
        <input type="button" value="삭제" onclick="doDel();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" /> 
    <% Else %>
        <input type="button" value="등록" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" tabindex="18" />
    <% End if %>

        <input type="button" value="취소" onclick="location.href='list.asp?page=<%=page%>';" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    </td>
  </tr>
</table>
</form>

    <script type="text/javascript">
    <!--
        document.write_f.cdate.focus();
    //-->
    </script>
</body>
</html>