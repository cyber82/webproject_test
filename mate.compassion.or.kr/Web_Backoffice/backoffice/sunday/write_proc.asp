<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 컴패션 선데이 관리
'작성일      : 2013-06-26
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")

%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  
%>

<%
    wtype = Request.Form("wtype")   '등록, 수정 구분
    page = Request.Form("page")
    startpage = Request.Form("startpage")
    idx = Request.Form("idx")
        
    c_date = Request.Form("c_date")
    if c_date <> "" then
        c_year = LEFT(c_date, 4)
    end if
    church_name = Request.Form("church_name")
    view_yn = Request.Form("view_yn")

    church_name = replace(church_name, "'", "''")

    link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "&cyear=" & c_year & "';</script>"

'등록
if wtype = "write" then

    sql = " INSERT INTO [compassweb4].[dbo].[tCompassionSunday] ("
	sql = sql & " c_year, c_date, church_name, view_yn "
    sql = sql & " ) VALUES ( "
    sql = sql & " N'"& c_year &"', N'"& c_date &"', N'"& church_name &"', N'"& view_yn &"' "
    sql = sql & " ) "

    'response.write sql
    'response.end

    inresult = BoardWriteWS(provWSDL, sql)
    
    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('등록되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('등록중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if

'수정
Else

    sql2 = " UPDATE [compassweb4].[dbo].[tCompassionSunday] SET "
    sql2 = sql2 & " c_year=N'"& c_year &"', c_date=N'"& c_date &"', church_name=N'"& church_name &"', view_yn=N'"& view_yn &"' "
    sql2 = sql2 & " WHERE idx = '"& idx &"' "

    upresult = BoardWriteWS(provWSDL, sql2)

    if upresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('수정되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('수정중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if

End If

%>