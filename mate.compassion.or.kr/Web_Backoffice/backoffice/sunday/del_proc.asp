﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
'*******************************************************************************
'페이지 설명 : 컴패션 선데이 관리
'작성일      : 2013-06-27
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
%>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
if request("idx") = "" then
    idx = 0
else 
    idx = request("idx")
end if

page = Request("page")
startpage = Request("startpage")
    
    c_date = request("c_date")
    if c_date <> "" then
        c_year = LEFT(c_date, 4)
    end if

'test
'idx = 0

link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "&cyear=" & c_year & "';</script>"


if CInt(idx) > 0 then

    sql = " UPDATE [compassweb4].[dbo].[tCompassionSunday] SET "
    sql = sql & " del_yn = 'Y' "
    sql = sql & " WHERE idx = " & idx

    inresult = BoardWriteWS(provWSDL, sql)

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('삭제되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('삭제중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if
else
    response.write "<script type='text/javascript' charset='euc-kr'> alert('글이 없어 삭제할 수 없습니다.'); </script>"
    response.write link
end if
%>

</html>
