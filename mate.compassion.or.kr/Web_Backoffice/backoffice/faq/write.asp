﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">

<% '추가 2013-07-17 %>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/tinymce_config.js" charset='euc-kr'></script>
<script type="text/javascript">
    tinymce_config("b_content");
</script>
</head>

<%
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

	table_idx = request("table_idx")
	keyfield = request("keyfield")	
	key = request("key")
    f_part = request("f_part")
%>

<script type="text/javascript">
<!--
    function b_list()
    {
        location.href = "list.asp?table_idx=<%=table_idx%>&page=<%=page%>&f_part=<%=f_part %>"
    }
   
    
    function sendit() {

        //분류
        if (document.myform.f_part.value == "") {
            alert("분류를 선택해 주십시오.");
            f_part.focus();
            return;
        }

        //이름
        if(document.myform.b_name.value =="") {
            alert("글쓴이를 입력해 주십시오.");
            b_name.focus();
            return;
        }

        //제목
        if (document.myform.b_title.value == "") {
            alert("제목을 입력해 주십시오.");
            b_title.focus();
            return;
        }   
     
        //글내용
        if (document.myform.b_content.value =="") {
            alert("글을 작성안하셨습니다. 글을 작성해 주십시요");
            b_content.focus();
            return;
        }    

        //비밀번호
        if (document.myform.b_pwd.value == "") {
            alert("비밀번호을 입력해 주십시오.");
            b_pwd.focus();
            return;
        }

        if (confirm("작성하시겠습니까?")) {
            
            document.myform.method = "post";
            document.myform.action = "write_ok.asp?table_idx=<%=table_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>";
            document.myform.submit();
        } 
        else {
            alert("취소되었습니다.");
        }
     }      
//-->
</script>


<body leftmargin="0" topmargin="0">
<!--<body leftmargin="0" topmargin="0" oncontextmenu='return false' ondragstart='return false'>-->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<form name="myform" method = "post" action="write_ok.asp?table_idx=<%=table_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>">
<input type="hidden" name="b_pwd" size="40" maxlength="30" value="compassion_admin00">
  <tr>
	 <!--#include virtual="/Web_Backoffice/backoffice/inc/top.asp"//-->
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9">FAQ</font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>

              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">

					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="35" align="center" bgcolor="#F3F3F3"><b>Top12</b></td>
                      <td bgcolor="#FFFFFF">
						  <select name="b_notice" style="height:18px;" tabindex="1">
				            <option value="Y">YES</option>
			                <option value="N" selected>NO</option>
						  </select>
					  </td>
                    </tr>

                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="35" align="center" bgcolor="#F3F3F3"><b>분류</b></td>
                      <td bgcolor="#FFFFFF">
                          <select name="f_part" style="height:18px;" tabindex="2">
                            <option value="" selected>선택하세요</option>
                            <option value="일대일결연">일대일결연</option>
                            <option value="선물금">선물금</option>
                            <option value="양육보완프로그램">양육보완프로그램</option>
                            <option value="어린이방문">어린이방문</option>
                            <option value="자원봉사">자원봉사</option>
                            <option value="컴패션">컴패션</option>
                            <option value="편지">편지</option>
                            <option value="현지관련">현지관련</option>
                            <option value="후원금">후원금</option>
                          </select>
					  </td>
                    </tr>

					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="35" align="center" bgcolor="#F3F3F3"><b>작성자</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_name" size="40" maxlength="30" value="컴패션" tabindex="3"></td>
                    </tr>

                    <tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>제목</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_title" size="65" maxlength="150" value="" tabindex="4"></td>
                    </tr>

                    <tr bgcolor="#FFFFFF"> 
                      <td height="200" align="center" bgcolor="#F3F3F3"><b>내용</b></td>
                      <td>
                        <!--<textarea name="b_content" cols="60" rows="10"></textarea>-->
                        <% '수정 2013-06-11 %>
                        <div>
	                        <textarea name="b_content" id="b_content" style="width:600px;height:170px;" class="sbox" tabindex="5"></textarea>
                        </div>
                        <!--<div><button onClick="addArea2();">Editor</button> <button onClick="removeArea2();">Html</button></div>-->
                      </td>
                    </tr>

                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="right">
                    <a href="javascript:sendit();" onfocus="this.blur()">
                    <img src="../img/btn03.gif" width="44" height="20" border="0">
                    </a>
                </td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</form>
</table>

    <% '추가 2013-06-11 %>
    <% '주석처리 2013-07-17 %>
    <!--<script src="/Web_Backoffice/backoffice/inc/js/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        var area1, area2;
        /*
        function toggleArea1() {
        if (!area1) {
        area1 = new nicEditor({ fullPanel: true }).panelInstance('myArea1', { hasPanel: true });
        } else {
        area1.removeInstance('myArea1');
        area1 = null;
        }
        }
        */

        function addArea2() {
            area2 = new nicEditor({ fullPanel: true }).panelInstance('b_content');
        }
        function removeArea2() {
            area2.removeInstance('b_content');
        }

        bkLib.onDomLoaded(function () { addArea2(); });
    </script>-->

</body>
</html>
