<% @CODEPAGE = 65001 %>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
    '웹서비스 연동
    provWSDL = session("serviceUrl")
    
    table_idx = request("table_idx")
    no_idx = request("no_idx")
    page = request("page")
    startpage = request("startpage")
    notice_idx = request("notice_idx")
    ref = request("ref")
    re_step = request("re_step")
    f_part = request("f_part")
  
    '수정 2013-01-23
    sql = "select b_pwd, b_filename1 from upboard where no_idx = "&no_idx&" and table_idx = '"&table_idx&"' and notice_idx = '"&notice_idx&"' and ref = "&ref&" and re_step = "&re_step&" "
    'set rs = server.CreateObject("adodb.recordset")
    'rs.Open sql, db
    Set rs = RecordsetFromWS(provWSDL, sql)

    b_filename = rs("b_filename1")
 
if request("b_pwd") = rs("b_pwd") then '비밀번호가 일치할 경우
 
    sql = "delete from upboard"
    sql = sql & " where no_idx = "&no_idx&" and table_idx = '"&table_idx&"' and notice_idx = '"&notice_idx&"' and ref = "&ref&" and re_step = "&re_step&" "
    
    'db.Execute sql
    delresult = BoardWriteWS(provWSDL, sql)

    '추가 2013-07-17
    if delresult = "10" then
    %>
    <script type="text/javascript" charset='euc-kr'>
    <!--
        alert('삭제되었습니다.');
    //-->
    </script>
    <%
    end if

    if delresult <> "10" AND delresult <> "" then
    %>
    <script type="text/javascript" charset='euc-kr'>
    <!--
        alert('<%=delresult %>');
    //-->
    </script>
    <%
    end if
   
    rs.Close
    set rs = nothing

%>

<script type="text/javascript" charset='euc-kr'>
<!--
    location = "list.asp?table_idx=<%=table_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=<%=f_part %>"
//-->
</script>

<% else %>

<script type="text/javascript" charset='euc-kr'>
<!--
    alert("비밀번호가 일치하지 않습니다.");
    history.back();
//-->
</script>

<% end if %>

</html>