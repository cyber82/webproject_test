﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

	keyfield = request("keyfield")	
	key = request("key")	

	table_idx = request("table_idx")
    f_part = request("f_part")
    notice_idx = "N"

    if f_part = "전체" then
        f_part = ""
    end if
    if f_part = "Top12" then
        notice_idx = "Y"
        f_part = ""
    else 
        notice_idx = "N"
    end if

    '추가 2013-05-30
    menu_part = request("f_part")
    if menu_part = "" then
        menu_part = "전체"
    end if
%>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 연동
provWSDL = session("serviceUrl")
%>

<body leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30">&nbsp;</td>
          <td width="850"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                        <td width="22px"><img src="../img/icon01.gif" width="22" height="18"></td>
                        <td align="left"><strong><font color="#3788D9">FAQ</font></strong></td>
                    </tr>

                    <tr height="30px">
                        <td width="22px"></td>
                        <td>
                        <p style="font-size:9pt;">
                        <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=Top12" onfocus="this.blur()"><span <% if menu_part = "Top12" then %> style="font-weight:bold;color:#6799FF;text-decoration:underline;" <% end if %>>Top12</span></a>&nbsp;&nbsp;
                        <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=전체" onfocus="this.blur()"><span <% if menu_part = "전체" then %> style="font-weight:bold;color:#6799FF;text-decoration:underline;" <% end if %>>전체</span></a>&nbsp;&nbsp;
                        <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=컴패션" onfocus="this.blur()"><span <% if menu_part = "컴패션" then %> style="font-weight:bold;color:#6799FF;text-decoration:underline;" <% end if %>>컴패션</span></a>&nbsp;&nbsp;
                        <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=일대일결연" onfocus="this.blur()"><span <% if menu_part = "일대일결연" then %> style="font-weight:bold;color:#6799FF;text-decoration:underline;" <% end if %>>1:1어린이양육(일대일결연)</span></a>&nbsp;&nbsp;
                        <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=양육보완프로그램" onfocus="this.blur()"><span <% if menu_part = "양육보완프로그램" then %> style="font-weight:bold;color:#6799FF;text-decoration:underline;" <% end if %>>양육보완프로그램</span></a>&nbsp;&nbsp;
                        <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=후원금" onfocus="this.blur()"><span <% if menu_part = "후원금" then %> style="font-weight:bold;color:#6799FF;text-decoration:underline;" <% end if %>>후원금</span></a>&nbsp;&nbsp;
                        <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=편지" onfocus="this.blur()"><span <% if menu_part = "편지" then %> style="font-weight:bold;color:#6799FF;text-decoration:underline;" <% end if %>>편지</span></a>&nbsp;&nbsp;
                        <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=선물금" onfocus="this.blur()"><span <% if menu_part = "선물금" then %> style="font-weight:bold;color:#6799FF;text-decoration:underline;" <% end if %>>선물금</span></a>&nbsp;&nbsp;
                        <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=어린이방문" onfocus="this.blur()"><span <% if menu_part = "어린이방문" then %> style="font-weight:bold;color:#6799FF;text-decoration:underline;" <% end if %>>어린이방문</span></a>&nbsp;&nbsp;
                        <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=현지관련" onfocus="this.blur()"><span <% if menu_part = "현지관련" then %> style="font-weight:bold;color:#6799FF;text-decoration:underline;" <% end if %>>현지관련</span></a>&nbsp;&nbsp;
                        <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=자원봉사" onfocus="this.blur()"><span <% if menu_part = "자원봉사" then %> style="font-weight:bold;color:#6799FF;text-decoration:underline;" <% end if %>>자원봉사</span></a>&nbsp;&nbsp;
                        </p>
                        </td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                      <td width="6%" height="25" align="center">번호</td>
					  <td align="center">제목</td>
                      <td width="10%" align="center">작성자</td>
                      <td width="10%" align="center">등록일</td>
                      
                      <% '수정 2013-05-30 %>
                      <% if f_part = "" AND notice_idx = "Y" then %>
                        <td width="13%" align="center">top번호</td>
                      <% else %>
                        <td width="13%" align="center">글번호</td>
                      <% end if %>
                    </tr>


            <!------- 게시판 목록 구하기 시작 ---------->
            <%
            '수정 2013-01-17, 수정 2013-05-30  
            sql = " select "
            sql = sql & " table_idx, no_idx, ref, re_step, notice_idx, b_title, b_name, b_filename1, b_writeday, f_part, s_confirm, board_idx, top_idx " 
            sql = sql & " from upboard "
            sql = sql & " where table_idx = '"&table_idx&"' "

            if f_part = "" AND notice_idx = "Y" then
                sql = sql & " and notice_idx = '"&notice_idx&"' "
            end if

            if f_part <> "" then
                'sql = sql & " and notice_idx = '"&notice_idx&"' "
                sql = sql & " and f_part = '"&f_part&"' "
            end if

            if key = "" then
	            sql = sql & " order by ref desc, re_step asc "
            else
	            sql = sql & " and "&keyfield&" like '%"&key&"%' order by ref desc, re_step asc "
            end if


            '수정 2013-01-17
            'set rs = server.CreateObject("adodb.recordset")
            'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
  
            '추가 2013-01-17
            'Response.Write(sql)
            Set rs = RecordsetFromWS(provWSDL, sql)

            '추가 2013-01-17
            sqlCnt = " select "
            sqlCnt = sqlCnt & " count(table_idx) as totalCnt " 
            sqlCnt = sqlCnt & " from upboard "
            sqlCnt = sqlCnt & " where table_idx = '"&table_idx&"' "
            
            if f_part = "" AND notice_idx = "Y" then
                sqlCnt = sqlCnt & " and notice_idx = '"&notice_idx&"' "
            end if

            if f_part <> "" then
                sqlCnt = sqlCnt & " and notice_idx = '"&notice_idx&"' "
                sqlCnt = sqlCnt & " and f_part = '"&f_part&"' "
            end if
                  
            if key <> "" then
	            sqlCnt = sqlCnt & " and "&keyfield&" like '%"&key&"%' "
            end if 

            Set rsCnt = RecordsetFromWS(provWSDL, sqlCnt)       
            %>
					
				<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
					<tr> 
					  <td height="22" colspan="5" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
				<% else '데이터가 있다면
                    
                    rs.PageSize = 10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

					totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
                    ''totalpage = rsCnt("totalCnt")

					rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
					endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
					''totalpage = Int(rsCnt("totalCnt") / rs.PageSize) + 1

                    if endpage > totalpage then
						endpage = totalpage
					end If
                    
					if request("page")="" then
						npage = 1
					else
						npage = CInt(request("page"))
					end if
						
					trd = rs.RecordCount
                    ''trd = rsCnt("totalCnt")
					j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)

                    'Response.Write("totalpage : " & totalpage & "<br/>")
                    'Response.Write("startpage : " & startpage & "<br/>")
                    'Response.Write("endpage : " & endpage & "<br/>")
                    'Response.Write("rs.PageSize : " & rs.PageSize & "<br/>")
                    'Response.Write("npage : " & npage & "<br/>")
                    'Response.Write("trd : " & trd & "<br/>")
				%>
				<%
				    i = 1
					do until rs.EOF or i > rs.PageSize
				%>
                    <tr bgcolor="#FFFFFF"> 
                      <td height="25" align="center">
                        <% if rs("re_step") = "0" then %>
                            <%=j %>
                        <% end if %>
                      </td>

                      <td style="padding:5px;">
                        <% if rs("notice_idx") = "Y" then %>
                            <font color="blue">[Top12]</font>
                        <% end if %>
                        <% if rs("f_part") <> "" then %>
                            <font color="green">[<%=rs("f_part") %>]</font>
                        <% end if %> 

                        <a href="content.asp?t_id=<%=table_idx%>&id=<%=rs("no_idx")%>&ref=<%=rs("ref")%>&re_step=<%=rs("re_step")%>&page=<%=page%>&n_id=<%=rs("notice_idx")%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=<%=f_part %>" onfocus="this.blur()">
                            <% if rs("b_title") = "" then %>
                                <font color="blue">없는 글입니다.</font>
                            <% else %>
                                <% if rs("s_confirm") = "del" then %>
                                    <font color="red"><%=rs("b_name") %>님의 요청에 의하여 삭제된 글입니다. (답글 달지 마세요)</font>
                                <% else %> 
                                    <%=rs("b_title") %>
                                <% end if %>
                            <% end if %>
                        </a>
	
					  </td>
                      <td align="center"><%=rs("b_name") %></td>
                      <td align="center"><%=left(rs("b_writeday"),10) %></td>

                    <script type="text/javascript">
                    <!--
                        //수정 2013-05-30
                        function d_sendit<%=i%>()
                        {
                            var f_part = "";
                            if ("<%=f_part %>" == "" && "<%=notice_idx %>" == "Y" ){
                                f_part = "Top12";
                                document.d_bbs<%=i%>.action = 'j_dong_ok.asp?table_idx=<%=table_idx%>&board_idx=<%=rs("board_idx")%>&ref=<%=rs("ref")%>&re_step=<%=rs("re_step")%>&page=<%=page%>&notice_idx=<%=rs("notice_idx")%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=' + f_part + '&top_type=Y';
                            }
                            else{
                                if ("<%=f_part %>" == ""){
                                    f_part = "전체";
                                }
                                document.d_bbs<%=i%>.action = 'j_dong_ok.asp?table_idx=<%=table_idx%>&board_idx=<%=rs("board_idx")%>&ref=<%=rs("ref")%>&re_step=<%=rs("re_step")%>&page=<%=page%>&notice_idx=<%=rs("notice_idx")%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=' + f_part + '&top_type=N';
                            }
                            document.d_bbs<%=i%>.method = "post";
                            document.d_bbs<%=i%>.submit();
                        }
                    //-->
                    </script>

                    <form name="d_bbs<%=i%>" method="post" action="">
					  <td align="center">
                        <% '수정 2013-05-30 %>
                        <% if f_part = "" AND notice_idx = "Y" then %>
					        <input type="text" name="top_idx" size="5" value="<%=rs("top_idx")%>"> 
                        <% else %>
                            <input type="text" name="no_idx" size="5" value="<%=rs("no_idx")%>"> 
                        <% end if %>
						<a href="javascript:d_sendit<%=i%>();" onfocus="this.blur()"><b>[확인]</b></a>
					  </td>
                    </form>

                    </tr>

            <%
                'Response.Write("j : " & j & "<br/>")
                'Response.Write("i : " & i & "<br/>")

				j=j-1
				rs.MoveNext ' 다음 레코드로 이동한다.
			    i=i+1
				loop '레코드의 끝까지 loop를 돈다.
			%> 

            <% 
         	    rs.Close
	            Set rs = nothing 

                end if
            %>
            <!------- 게시판 목록 구하기 시작 ---------->

                  </table></td>
              </tr>

              <tr> 
                <td>&nbsp;</td>
              </tr>

              <tr>
                <td align="right">
                    <% if Session("compassion_level") = "1" then %>
                    <% else %> 
                        <% if table_idx = "1015" then %>
                            <a href="write.asp?table_idx=<%=table_idx%>&page=<%=page%>&f_part=<%=f_part %>" onfocus="this.blur()">
                                <img src="../img/btn17.gif" width="52" height="20" border="0">
                            </a>
                        <% end if %>
                    <% end if %>
                </td>
              </tr>
              <tr> 
                <td align="center">
					<% if CInt(startpage) <> CInt(1) then %> 
				         <a href="list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=<%=f_part %>" onfocus="this.blur()">
			             [이전]
                         </a>
			        <% end if %> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then %>
				         [<%=page %>]
		             <% else %>
					     <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=<%=f_part %>" onfocus="this.blur()"> 
                         <%=i %> 
                         </a>
		             <% end if %>
				   <% next %>

				   <% if cint(endpage) <> cint(totalpage) then %>
			             <a href="list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&table_idx=<%=table_idx%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=<%=f_part %>" onfocus="this.blur()">
			             [다음]
                         </a>
			       <% end if %>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
	
              <tr> 
                <td align="center"><table width="30%" border="0" cellspacing="2" cellpadding="0">
					<form name="searchForm" action="list.asp?table_idx=<%=table_idx%>&f_part=<%=f_part %>" method="post" onSubmit="return searchSendit();">	
                    <tr> 
                      <td><select name="keyfield" style="height:19px;">
                                <option value="b_title">제목</option>
                                <option value="b_name">작성자</option>
                                <option value="b_content">내용</option>
                              </select></td>
                      <td><input name="key" type="text" class="text"></td>
                      <td><input type="submit" name="Submit" value="찾기"></td>
                    </tr>
					</form>
                  </table></td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>

            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
