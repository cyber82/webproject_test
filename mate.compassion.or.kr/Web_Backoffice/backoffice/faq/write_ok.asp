<% @CODEPAGE = 65001 %>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<% '추가 2013-01-18 %>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
    '웹서비스 연동
    provWSDL = session("serviceUrl")

    table_idx = request("table_idx")
    no_idx = request("no_idx")
    page = request("page")
    startpage = request("startpage")

	keyfield = request("keyfield")	
	key = request("key")
%>

<% 
    b_notice = request("b_notice")
    b_name = request("b_name")  
    b_email = request("b_email")
    b_hompage = request("b_hompage")
    b_title = request("b_title")
    b_pwd = request("b_pwd")
    b_tel = request("b_tel")
    b_content = request("b_content")
    b_filename = request("b_filename")
    idx = request("idx")
    ref = request("ref")
    re_step = request("re_step")
    re_level = request("re_level")
    b_writeday = request("b_writeday")
    b_readnum = request("b_readnum")
    f_confirm = request("f_confirm")
    f_part = request("f_part")
 
    b_title=replace(b_title, "'", "''") '쿼리에 '가 들어가면 에러가 나기 때문에 그것을 replace처리 합니다.
    b_content=replace(b_content, "'", "''")
%>

<% 
  
  sql = "select Max(b_num) from upboard"  
  'Set rs = server.CreateObject("ADODB.RecordSet")
  'rs.Open sql,db


  '추가 2013-01-18
  Set rs = RecordsetFromWS(provWSDL, sql)
  
  if isNull(rs(0)) then ' 글이 없을경우 b_num을 1로 한다
    b_num = 1
  else
    b_num=rs(0) + 1
  end if
  
  '*************답변형 게시판의 추가 부분************************
  if idx <> "" then
  'if request("idx") <> "" then '즉 답변쓰기라면 
  
  ref = cint(ref)
  re_step = cInt(re_step)
  re_level = cint(re_level)
  

  sqlstring = "update upboard set re_step = re_step + 1 where ref=" & ref & " AND re_step > " & re_step &" and table_idx = '"&table_idx&"' and notice_idx = '"&b_notice&"' "
  'db.Execute(sqlstring)
  
  '추가 2013-01-18  
  upresult = BoardWriteWS(provWSDL, sqlstring)
  'Response.Write(upresult)


  re_step = re_step + 1
  re_level = re_level + 1

  
  else '첨글쓰기 라면
  ref = b_num
  re_step=0
  re_level=0
  end if


if idx <> "" then
else

    nsql="select max(no_idx) as noCnt from upboard where table_idx = '"&table_idx&"' "
    'Set record = db.execute(sql)
    '추가 2013-01-18
    Set record = RecordsetFromWS(provWSDL, nsql)
    
    '수정 2013-03-08
    'if isnull(record(0)) then titleno="1" else titleno= record(0) +1
    if isnull(record(0)) or record(0) = "" then 
        titleno = "1"
    else
        titleno = record(0) +1
    end if

   
    '추가 2013-05-30
    tsql="select max(top_idx) as topCnt from upboard where table_idx = '"&table_idx&"' and notice_idx = 'Y' "
    Set recordTop = RecordsetFromWS(provWSDL, tsql)
    
    if isnull(recordTop(0)) or recordTop(0) = "" then 
        topno = "1"
    else
        topno = recordTop(0) + 1
    end if


    '수정 2013-05-30
    '********테이블에 저장한다.**************** 
    sql = "insert into upboard (table_idx,no_idx, notice_idx, b_admin, b_name, b_email, b_hompage, b_title, b_content, b_num "
    sql = sql & ", b_readnum, b_writeday, ref, re_level, re_step, b_pwd, f_confirm, f_part, b_filename1, b_filesize "
    
    if isnull(recordTop(0)) or recordTop(0) = "" then 
    else 
        sql = sql & ", top_idx "
    end if

    sql = sql & ") values "
    sql = sql & "( N'" & table_idx & "'"
    sql = sql & ", N'" & titleno & "'"
    sql = sql & ", N'" & b_notice & "'"
    sql = sql & ", N'A'"
    sql = sql & ", N'" & b_name & "'"
    sql = sql & ", N'" & b_email & "'"
    sql = sql & ", N'" & b_hompage & "'"
    sql = sql & ", N'" & b_title & "'"
    sql = sql & ", N'" & b_content & "'"
    sql = sql & ", " & b_num
    sql = sql & ", 0, N'"& date() & "'"
    'sql = sql & ","& b_readnum & ",'"& b_writeday & "'"
    sql = sql & ", " & ref
    sql = sql & ", " & re_level
    sql = sql & ", " & re_step
    sql = sql & ", N'" & b_pwd & "'"
    sql = sql & ", N'" & f_confirm & "'"
    sql = sql & ", N'" & f_part & "'"
    sql = sql & ", N'" & b_filename & "'"
    sql = sql & ", N'" & b_filesize & "'"

    if isnull(recordTop(0)) or recordTop(0) = "" then 
    else 
        sql = sql & ", N'" & topno & "'"
    end if

    sql = sql & ")"


    'response.write sql
    'db.Execute sql

    'Response.Write(sql)
    'Response.End()

    '추가 2013-01-18
    inresult = BoardWriteWS(provWSDL, sql)
    'Response.Write(inresult)

end if
   
 ' 인스턴스를 소멸......  
  rs.Close
  set rs=nothing

  '추가 2013-01-18
  record.Close
  set record=nothing

  '추가 2013-05-30
  recordTop.Close
  set recordTop=nothing


'추가 2013-07-17
if inresult = "10" then
%>
<script type="text/javascript" charset='euc-kr'>
<!--
    alert('등록되었습니다.');
//-->
</script>
<%
end if 

if inresult <> "10" AND inresult <> "" then
%>
<script type="text/javascript" charset='euc-kr'>
<!--
    alert('<%=inresult %>');
//-->
</script>
<%
end if 
%>

<script type="text/javascript" charset='euc-kr'>
<!--
    location.href = "list.asp?table_idx=<%=table_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=<%=f_part %>";
//-->
</script>

</html>