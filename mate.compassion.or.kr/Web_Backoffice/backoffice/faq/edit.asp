﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">

<% '추가 2013-07-17 %>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/tinymce_config.js" charset='euc-kr'></script>
<script type="text/javascript">
    tinymce_config("b_content");
</script>
</head>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->


<%
    '웹서비스 연동
    provWSDL = session("serviceUrl")

	table_idx = request("table_idx")
	no_idx = request("no_idx")
	notice_idx = request("n_id")
	page = request("page")
    startpage = request("startpage")
	ref = request("ref")
	re_step = request("re_step")
	keyfield = request("keyfield")	
	key = request("key")
    f_part = request("f_part")
%>
<%
    '수정 2013-01-23
    sql = " select " 
    sql = sql & " table_idx, no_idx, ref, re_step, notice_idx, b_title, b_name, b_filename1, b_writeday, b_pwd, b_readnum, f_confirm, f_part "
    sql = sql & " from upboard where table_idx = '"&table_idx&"' and no_idx = "&no_idx&" and notice_idx = '"&notice_idx&"' and ref = "&ref&" and re_step = "&re_step&" "
    'set grs = server.CreateObject("adodb.recordset")
    'grs.Open sql, db

    '추가 2013-01-23
    Set grs = RecordsetFromWS(provWSDL, sql)

    b_title = grs("b_title")
    'b_content = grs("b_content")

    'Response.Write left(grs("f_confirm"),1)
    'Response.End


    sqlcon = "select b_content from upboard where table_idx = '"&table_idx&"' and no_idx = "&no_idx&" and notice_idx = '"&notice_idx&"' and ref = "&ref&" and re_step = "&re_step&" "
    'Response.Write sqlcon
    'Response.End
    b_content = BoardLoadContentWS(provWSDL, sqlcon)

    'Response.Write b_content
    'Response.End

    'b_content = replace(rs("b_content"), chr(13) & chr(10), "<br>")
    b_content = replace(b_content, chr(10), "<br/>")
    b_content = replace(b_content, chr(13), "<br/>")
    b_content = replace(b_content, "&lt;", "<")
    b_content = replace(b_content, "&gt;", ">")
    b_content = replace(b_content, "&amp;", "&")
    b_content = replace(b_content, "&apos;", "'")
    b_content = replace(b_content, "&quot;", """")

    'Response.Write b_content
    'Response.End
%>


<script type="text/javascript">
<!--
    function b_list()
    {
        location.href = "list.asp?table_idx=<%=table_idx%>&page=<%=page%>&f_part=<%=f_part %>"
    }
    
    function sendit() {

        //분류
        if (document.myform.f_part.value == "") {
            alert("분류를 선택해 주십시오.");
            f_part.focus();
            return;
        }

        //이름
        if (document.myform.b_name.value == "") {
            alert("글쓴이를 입력해 주십시오.");
            b_name.focus();
            return;
        }

        //제목
        if (document.myform.b_title.value == "") {
            alert("제목을 입력해 주십시오.");
            b_title.focus();
            return;
        }

        //글내용
        if (document.myform.b_content.value == "") {
            alert("글을 작성안하셨습니다. 글을 작성해 주십시요");
            b_content.focus();
            return;
        }

        //비밀번호
        if (document.myform.b_pwd.value == "") {
            alert("비밀번호을 입력해 주십시오.");
            b_pwd.focus();
            return;
        }

        if (confirm("수정하시겠습니까?")) {

            document.myform.method = "post";
            document.myform.action = "edit_ok.asp?table_idx=<%=table_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>";
            document.myform.submit();
        }
        else {
            alert("취소되었습니다.");
        }
    }
      
//-->
</script>


<body leftmargin="0" topmargin="0">
<!--<body leftmargin="0" topmargin="0" oncontextmenu='return false' ondragstart='return false'>-->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<form name="myform" method = "post" action="edit_ok.asp?table_idx=<%=table_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>">
<input type="hidden" name="idx" value="<%=request("idx")%>">
<input type="hidden" name="no_idx" value="<%=grs("no_idx")%>">
<input type="hidden" name="notice_idx" value="<%=grs("notice_idx")%>">
<input type="hidden" name="ref" value="<%=grs("ref")%>">
<input type="hidden" name="re_step" value="<%=grs("re_step")%>">

  <tr>
	 <!--#include virtual="/Web_Backoffice/backoffice/inc/top.asp"//-->
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9">FAQ</font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>

              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">

					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="35" align="center" bgcolor="#F3F3F3"><b>Top12</b></td>
                      <td bgcolor="#FFFFFF">
						  <select name="b_notice" style="height:18px;" tabindex="1">
				            <option value="Y" <%If grs("notice_idx") = "Y" then%>selected<%End if%>>YES</option>
			                <option value="N" <%If grs("notice_idx") = "N" then%>selected<%End if%>>NO</option>
						  </select>
					  </td>
                    </tr>	

                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="35" align="center" bgcolor="#F3F3F3"><b>분류</b></td>
                      <td bgcolor="#FFFFFF">
						  <select name="f_part" style="height:18px;" tabindex="2">
                            <option value="일대일결연" <%If grs("f_part") = "일대일결연" then%>selected<%End if%>>일대일결연</option>
                            <option value="선물금" <%If grs("f_part") = "선물금" then%>selected<%End if%>>선물금</option>
                            <option value="양육보완프로그램" <%If grs("f_part") = "양육보완프로그램" then%>selected<%End if%>>양육보완프로그램</option>
                            <option value="어린이방문" <%If grs("f_part") = "어린이방문" then%>selected<%End if%>>어린이방문</option>
                            <option value="자원봉사" <%If grs("f_part") = "자원봉사" then%>selected<%End if%>>자원봉사</option>
                            <option value="컴패션" <%If grs("f_part") = "컴패션" then%>selected<%End if%>>컴패션</option>
                            <option value="편지" <%If grs("f_part") = "편지" then%>selected<%End if%>>편지</option>
                            <option value="현지관련" <%If grs("f_part") = "현지관련" then%>selected<%End if%>>현지관련</option>
                            <option value="후원금" <%If grs("f_part") = "후원금" then%>selected<%End if%>>후원금</option>
                          </select>
					  </td>
                    </tr>
                
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="35" align="center" bgcolor="#F3F3F3"><b>작성자</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_name" size="40" maxlength="30" value="<%=grs("b_name")%>" tabindex="3"></td>
                    </tr>
                    <tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>제목</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_title" size="65" maxlength="150" value="<%=b_title%>" tabindex="4"></td>
                    </tr>
                    <tr bgcolor="#FFFFFF"> 
                      <td height="200" align="center" bgcolor="#F3F3F3"><b>내용</b></td>
                      <td>
                        <!--<textarea name="b_content" cols="60" rows="10"><%=b_content%></textarea>-->
                        <% '수정 2013-06-11 %>
                        <div>
	                        <textarea name="b_content" id="b_content" style="width:600px;height:170px;" class="sbox" tabindex="5"><%=b_content %></textarea>
                        </div>
                        <!--<div><button onClick="addArea2();">Editor</button> <button onClick="removeArea2();">Html</button></div>-->
                      </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>비밀번호</b></td>
                      <td><input type="password" name="b_pwd" size="40" maxlength="30" value="<%=grs("b_pwd")%>" tabindex="6"></td>
                    </tr>
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="35" align="center" bgcolor="#F3F3F3"><b>조회수</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_readnum" size="40" value="<%=grs("b_readnum")%>" tabindex="7"></td>
                    </tr>

					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="35" align="center" bgcolor="#F3F3F3"><b>작성일</b></td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_writeday" size="40" value="<%=grs("b_writeday")%>" disabled tabindex="8"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="right"><a href="javascript:sendit();" onfocus="this.blur()"><img src="../img/btn03.gif" width="44" height="20" border="0"></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</form>
</table>

    <% '추가 2013-06-11 %>
    <% '주석처리 2013-07-17 %>
    <!--<script src="/Web_Backoffice/backoffice/inc/js/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        var area1, area2;
        /*
        function toggleArea1() {
        if (!area1) {
        area1 = new nicEditor({ fullPanel: true }).panelInstance('myArea1', { hasPanel: true });
        } else {
        area1.removeInstance('myArea1');
        area1 = null;
        }
        }
        */

        function addArea2() {
            area2 = new nicEditor({ fullPanel: true }).panelInstance('b_content');
        }
        function removeArea2() {
            area2.removeInstance('b_content');
        }

        bkLib.onDomLoaded(function () { addArea2(); });
    </script>-->

</body>
</html>
