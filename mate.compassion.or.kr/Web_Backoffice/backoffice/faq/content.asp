﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
    '웹서비스 연동
    provWSDL = session("serviceUrl")
    
    table_idx = request("t_id")
    no_idx = request("id")
    page = request("page")
    startpage = request("startpage")
    n_id = request("n_id")
    ref = request("ref")
    re_step = request("re_step")
    f_part = request("f_part")
%>

<%
    sql = "select * from upboard  where table_idx = '" & table_idx & "' and no_idx = "&no_idx&"   and ref = "&ref&" and re_step = "&re_step&" "
    'set rs = server.CreateObject("adodb.recordset")
    'rs.open sql, db  
 
    '추가 2013-01-18
    Set rs = RecordsetFromWS(provWSDL, sql)

    '변수에 값들을 저장
    idx = rs("b_num")
    ref = rs("ref")
    re_level = rs("re_level")
    re_step = rs("re_step")
    b_filename=rs("b_filename1")
    b_filesize=rs("b_filesize")
 
    keyfield = request("keyfield")	
    key = request("key")

    sqlcon = "select b_content from upboard  where table_idx = '" & table_idx & "' and no_idx = "&no_idx&"   and ref = "&ref&" and re_step = "&re_step&" "
    b_content = ContentViewLoadWS(provWSDL, sqlcon)

    'Response.Write(sqlcon)
    'Response.End()

    'b_content = replace(rs("b_content"), chr(13) & chr(10), "<br>")
    b_content = replace(b_content, chr(10), "<br/>")
    b_content = replace(b_content, chr(13), "<br/>")
    b_content = replace(b_content, "&lt;", "<")
    b_content = replace(b_content, "&gt;", ">")
    b_content = replace(b_content, "&amp;", "&")
    b_content = replace(b_content, "&apos;", "'")
    b_content = replace(b_content, "&quot;", """")
%>  


<body leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
	 <!--#include virtual="/Web_Backoffice/backoffice/inc/top.asp"//-->
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30">&nbsp;</td>
          <td width="650"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9">FAQ</font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>

            <%
	            If left(rs("f_confirm"),1) = "Y" Then
		            f_confirm = "등록"
	            Else
		            f_confirm = "등록안함"	
	            End if
            %>

              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">


            <%

	            If rs("notice_idx") = "Y" Then
		            b_notice = "YES"
	            Else
		            b_notice = "NO"	
	            End if

            %>

			        <tr bgcolor="#F3F3F3"> 
                        <td width="100" height="35" align="center" bgcolor="#F3F3F3"><b>Top12</td>
                        <td bgcolor="#FFFFFF" style="padding:5px;"><%=b_notice%> </td>
                    </tr>

					<tr bgcolor="#F3F3F3"> 
                      <td width="10%" height="35" align="center" bgcolor="#F3F3F3"><b>분류</td>
                      <td bgcolor="#FFFFFF" style="padding:5px;"><%=rs("f_part")%> </td>
                    </tr>

					<tr bgcolor="#F3F3F3"> 
                      <td width="10%" height="35" align="center" bgcolor="#F3F3F3"><b>아이디</td>
                      <td bgcolor="#FFFFFF" style="padding:5px;"><%=rs("user_id")%> </td>
                    </tr>                    

					<tr bgcolor="#F3F3F3"> 
                      <td width="10%" height="35" align="center" bgcolor="#F3F3F3"><b>작성자</td>
                      <td bgcolor="#FFFFFF" style="padding:5px;"><%=rs("b_name")%> </td>
                    </tr>                    
                    <tr bgcolor="#F3F3F3"> 
                      <td width="10%" height="35" align="center" bgcolor="#F3F3F3"><b>제목</td>
                      <td bgcolor="#FFFFFF" style="padding:5px;"><%=rs("b_title")%></td>
                    </tr>
                    <tr bgcolor="#FFFFFF"> 
                      <td width="10%" height="200" align="center" bgcolor="#F3F3F3"><b>내용</td>
                      <td valign="top" style="padding:5px;">
					  <%'=replace(rs("b_content"), chr(13), "<br/>")%>
                      <%=b_content %>
                      </td>
                    </tr>

                    <!--<tr bgcolor="#FFFFFF">
                      <td width="10%" height="35" align="center" bgcolor="#F3F3F3"><b>첨부파일</td>
                      <td height="35" style="padding:5px;">

                      <% If rs("b_filename1") ="" then %>파일이 없습니다.<%else%>
					      <% if right(rs("b_filename1"),3) = "hwp" or right(rs("b_filename1"),3) = "HWP" then %>
						      <a href="hwp.asp?b_filename=<%=rs("b_filename1")%>"><%=rs("b_filename1") %>
					      <% else %>							  
						      <a href="../../pdsfile1/<%=rs("b_filename1")%>"><%=rs("b_filename1")%>
					      <% end if %>	  
					  <% End if %>

					  </td>
                    </tr>-->

                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>

        <script type="text/javascript">
		<!--
            function delChk(url)
            {
                if (confirm('정말 삭제하시겠습니까?'))
                {
                    location.href = url;
                }
		    }
		//-->
        </script>
              <tr> 
                <td align="right"><table width="10%" border="0" cellspacing="2" cellpadding="0">
                    <tr>
					<% if Session("compassion_level") = "1" then %>
					<td><a href="list.asp?table_idx=<%=table_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=<%=f_part %>"><img src="../img/btn16.gif" width="42" height="20" border="0"></a></td>
					<%else%>
					
					<% if table_idx = "1015" then %>
                      <td><a href="edit.asp?table_idx=<%=table_idx%>&no_idx=<%=no_idx%>&n_id=<%=n_id%>&ref=<%=ref%>&re_step=<%=re_step%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=<%=f_part %>" onfocus="this.blur()"><img src="../img/btn06.gif" width="44" height="20" border="0"></td>
                    <%end if%>
					
                      <td><a href="javascript:delChk('delete_ok.asp?table_idx=<%=table_idx%>&no_idx=<%=no_idx%>&notice_idx=<%=rs("notice_idx")%>&n_id=<%=n_id%>&ref=<%=ref%>&b_pwd=<%=rs("b_pwd")%>&re_step=<%=re_step%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=<%=f_part %>');"><img src="../img/btn02.gif" width="44" height="20" border="0"></td>
                      <td><a href="list.asp?table_idx=<%=table_idx%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>&f_part=<%=f_part %>"><img src="../img/btn16.gif" width="42" height="20" border="0"></a></td>
					<%end if%>
                    </tr>
                  </table></td>
              </tr>

        <form name="re" method="post" action="write.asp">
          <input type="hidden" name="t_id" value="<%=table_idx%>">
		  <input type="hidden" name="id" value="<%=no_idx%>">
		  <input type="hidden" name="n_id" value="<%=n_id%>">
		  <input type="hidden" name="left_menu" value="<%=left_menu%>">
		  <input type="hidden" name="page" value="<%=page%>">

          <input type="hidden" name="idx" value="<%=idx%>">
          <input type="hidden" name="re_step" value="<%=re_step%>">
          <input type="hidden" name="re_level" value="<%=re_level%>">
          <input type="hidden" name="ref" value="<%=ref%>">
		  <input type="hidden" name="keyfield" value="<%=keyfield%>">
          <input type="hidden" name="key" value="<%=key%>">
         </form> 

              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>

			

<!-- ########################################## 기타 등등 시작 ##########################################3 -->

	  <br/>

      <table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td bgcolor="#EBEBEB" height="20"></td>
        </tr>
      </table>
      <table width="600" border="0" cellspacing="0" cellpadding="0" align="center">

      <% 
        SQL2="SELECT * FROM upboard WHERE no_idx = (SELECT MAX(no_idx) FROM upboard WHERE no_idx < '"&rs("no_idx")&"' and table_idx = '"&table_idx&"' and notice_idx = '"&n_id&"') and table_idx = '"&table_idx&"' and notice_idx = '"&n_id&"'"
        'set rs2=db.execute(SQL2)

        '추가 2013-01-18
        Set rs2 = RecordsetFromWS(provWSDL, SQL2)
      %>
        <tr> 
          <td height="22" width="50">이전 글</td>
          <td height="22" width="30" align="center">
		    <% if rs2.eof then %>
			<% else %>
	          <%=rs2("no_idx")%></a>
	        <% end if %>
		  </td>
          <td width="300">
		    <% if rs2.eof then %>
			<% else %>
		      <a href="content.asp?t_id=<%=rs2("table_idx")%>&id=<%=rs2("no_idx")%>&n_id=<%=rs2("notice_idx")%>&left_menu=<%=left_menu%>&page=<%=page%>&ref=<%=rs2("ref")%>&re_step=<%=rs2("re_step")%>" onfocus="this.blur()">  <%=rs2("b_title")%></a>
	        <% end if %>		  
          </td>
          <td width="90" align="center">
		    <% if rs2.eof then %>
        	<% else %>
				  <%=rs2("b_name")%>
        	<% end if %>
		  </td>
          <td width="90" align="center">
		    <% if rs2.eof then %>
			<% else %>
	          <%=left(rs2("b_writeday"),10)%></a>
	        <% end if %>		  
		  </td>
          <td width="40" align="center">
		    <% if rs2.eof then %>
			<% else %>
	          <%=rs2("b_readnum")%></a>
	        <% end if %>		  
		  </td>
        </tr>
        <tr bgcolor="#EBEBEB"> 
          <td colspan="6" height="1"></td>
        </tr>

      <% 
        SQL3="SELECT * FROM upboard WHERE no_idx = (SELECT MIN(no_idx) FROM upboard WHERE no_idx > '"&rs("no_idx")&"' and table_idx = '"&table_idx&"' and notice_idx = '"&n_id&"') and table_idx = '"&table_idx&"' and notice_idx = '"&n_id&"'"
        'set rs3=db.execute(SQL3)

        '추가 2013-01-18
        Set rs3 = RecordsetFromWS(provWSDL, SQL3)
      %>
        <tr> 
          <td height="22" width="50">다음 글</td>
          <td height="22" width="30" align="center">
		    <% if rs3.eof then %>
			<% else %>
	          <%=rs3("no_idx")%></a>
	        <% end if %>
		  </td>
          <td width="300">
		    <% if rs3.eof then %>
			<% else %>
		       <a href="content.asp?t_id=<%=rs3("table_idx")%>&id=<%=rs3("no_idx")%>&n_id=<%=rs3("notice_idx")%>&left_menu=<%=left_menu%>&page=<%=page%>&ref=<%=rs3("ref")%>&re_step=<%=rs3("re_step")%>" onfocus="this.blur()">  <%=rs3("b_title")%></a>
	        <% end if %>		  
          </td>
          <td width="90" align="center">
		    <% if rs3.eof then %>
        	<% else %>
				    <%=rs3("b_name")%>
        	<% end if %>
		  </td>
          <td width="90" align="center">
		    <% if rs3.eof then %>
			<% else %>
	          <%=left(rs3("b_writeday"),10)%></a>
	        <% end if %>		  
		  </td>
          <td width="40" align="center">
		    <% if rs3.eof then %>
			<% else %>
	          <%=rs3("b_readnum")%></a>
	        <% end if %>		  
		  </td>
        </tr>
        <tr bgcolor="#EBEBEB"> 
          <td colspan="6" height="1"></td>
        </tr>
        <tr> 
          <td colspan="6" height="1" bgcolor="#EBEBEB"></td>
        </tr>
      </table>

<br/><br/><br/><br/><br/><br/>

<!-- ########################################## 기타 등등 끝 ##########################################3 -->


			
			</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>