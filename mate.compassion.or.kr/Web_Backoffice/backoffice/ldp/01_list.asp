﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

 
<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

	keyfield = request("keyfield")	
	key = request("key")	
%>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>


<%

    sql = " select idx, conid, name, stu_name, email, hand, stu_idx, del_yn, cancel_yn, writeday "
    sql = sql & " from (select sp.idx, sp.conid, sp.name, sp.stu_name, sp.email, sp.hand, sp.stu_idx, sp.del_yn, sp.cancel_yn, "
    sql = sql & "     convert(varchar, sp.writeday, 120) as writeday "
    sql = sql & "     from ldp_sponsor sp join ldp_student st on st.idx = sp.stu_idx "
    sql = sql & "     union all "
    sql = sql & "     select idx, conid, name, stu_name, email, hand, stu_idx, del_yn, cancel_yn, writeday "
    sql = sql & "     from ldp_sponsor where stu_idx is null) A "

    if key = "" then
        sql = sql & " where del_yn='N' "
        sql = sql & " order by writeday desc "
    else
	    sql = sql & " where del_yn='N' and " & keyfield & " like '%" & key & "%'"
        sql = sql & " order by writeday desc "
    end if

  'set rs = server.CreateObject("adodb.recordset")
  'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
  
  'response.Write sql
  'response.End
  
  Set rs = RecordsetFromWS(provWSDL, sql)
  'response.End

'if Session("compassion_level") = "1" then 
  'rs.PageSize=10 
'else
  'rs.PageSize=10 
'end if

    'totalcount
    sqlCnt = " select idx, conid, name, stu_name, email, hand, stu_idx, del_yn, cancel_yn, writeday "
    sqlCnt = sqlCnt & " from (select sp.idx, sp.conid, sp.name, sp.stu_name, sp.email, sp.hand, sp.stu_idx, sp.del_yn, sp.cancel_yn, "
    sqlCnt = sqlCnt & "     convert(varchar, sp.writeday, 120) as writeday "
    sqlCnt = sqlCnt & "     from ldp_sponsor sp join ldp_student st on st.idx = sp.stu_idx "
    sqlCnt = sqlCnt & "     union all "
    sqlCnt = sqlCnt & "     select idx, conid, name, stu_name, email, hand, stu_idx, del_yn, cancel_yn, writeday "
    sqlCnt = sqlCnt & "     from ldp_sponsor where stu_idx is null) A "
                  
    if key = "" then
        sqlCnt = sqlCnt & " where del_yn='N' "
    else
	    sqlCnt = sqlCnt & " where del_yn='N' and " & keyfield & " like '%" & key & "%'"
    end if

    'response.Write sqlCnt
    'response.End                  
    Set rsCnt = RecordsetFromWS2(provWSDL, sqlCnt)

%>

<body>
<div align="center">
      <table width="850px" border="0" cellspacing="0" cellpadding="0">
        <tr> 
            <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
            <td><strong><font color="#3788D9">LDP신청 현황</font></strong></td>
            <td align="right">
            <a href="01_excel.asp?keyfield=<%=keyfield%>&key=<%=key%>"><strong><font color="#3788D9">엑셀 다운로드</font></strong></a>
            </td>
        </tr>
      </table> 
      <br />

	  <table width="850px" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="8%" height="40px" align="center" bgcolor="#EBEBEB"><b>번호</b></td>
          <td width="25%" align="center" bgcolor="#EBEBEB"><b>LDP학생</b></td>
		  <td width="12%" align="center" bgcolor="#EBEBEB"><b>후원자번호</b></td>
          <td width="10%" align="center" bgcolor="#EBEBEB"><b>후원자이름</b></td>
          <td width="10%" align="center" bgcolor="#EBEBEB"><b>핸드폰</b></td>
          <td width="10%" align="center" bgcolor="#EBEBEB"><b>등록일자</b></td>
          <td width="15%" align="center" bgcolor="#EBEBEB"><b>결연해지</b></td>
          <td width="10%" align="center" bgcolor="#EBEBEB"><b>삭제</b></td>
        </tr>
		
        <% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면%>
		<tr> 
          <td height="40" colspan="8" align="center"><b>자료가 없습니다!!!</b></td>
        </tr>

		<% else '데이터가 있다면
            
            rs.PageSize = 10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

			totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
            ''totalpage = rsCnt("totalCnt")

			rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
			endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
			''totalpage = Int(rsCnt("totalCnt") / rs.PageSize) + 1

            'Response.Write rsCnt("totalCnt") & "<br/>"
            'Response.Write rs.PageSize & "<br/>"
            'Response.Write rs.PageCount & "<br/>"
            'Response.End

            if endpage > totalpage then
				endpage = totalpage
			end If
                    
			if request("page")="" then
				npage = 1
			else
				npage = CInt(request("page"))
			end if
						
			trd = rs.RecordCount
            ''trd = rsCnt("totalCnt")
			j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)

            'Response.Write("totalpage : " & totalpage & "<br/>")
            'Response.Write("startpage : " & startpage & "<br/>")
            'Response.Write("endpage : " & endpage & "<br/>")
            'Response.Write("rs.PageSize : " & rs.PageSize & "<br/>")
            'Response.Write("npage : " & npage & "<br/>")
            'Response.Write("trd : " & trd & "<br/>")
		%>
		<%
			 i=1
			 do until rs.EOF or i > rs.pagesize
	    %>

        <script type="text/javascript">
        <!--
            function bbsDel<%=i%>()
            {
                var choose = confirm("글의 내용이 삭제됩니다.\n\n삭제 하시겠습니까?");
	            if(choose)
	            {
		            location.href="01_del.asp?idx=<%=rs("idx")%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>";
	            }
	            else return;
            }

            function bbsCancel<%=i%>()
            {
                var choose = confirm("결연이 해지됩니다.\n\n해지 하시겠습니까?");
	            if(choose)
	            {
                    if(<%=rs("stu_idx")%> != null && <%=rs("stu_idx")%> != "" && <%=rs("stu_idx")%> > 0){
		                location.href="01_cencel.asp?idx=<%=rs("idx")%>&stu_idx=<%=rs("stu_idx")%>&page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>";
                    }
                    else{
                        alert("학생번호가 없습니다. 관리자에게 문의하여 주세요.");
                    }
	            }
	            else return;
            }
        //-->
        </script>

		<tr> 
          <td height="30" align="center"><%=j%></td>
          <td align="center"><%=rs("stu_name")%></td>
          <td align="center"><%=rs("conid")%></td>
          <td align="center"><%=rs("name")%></td>
          <td align="center"><%=rs("hand")%></td>
          <td align="center"><%=Left(rs("writeday"),10)%></td>
          <td align="center" valign="middle">
            <% if rs("cancel_yn") = "N" then %>
                결연됨 
                <a href='javascript:bbsCancel<%=i%>();' onfocus="this.blur()">[취소]</a>
            <% else %>
                결연해지됨
            <% end if %>
          </td>
          <td align="center"><a href="javascript:bbsDel<%=i%>();" onfocus="this.blur()"><img src="image/112.gif" border="0"></a></td>
        </tr>
        <tr> 
          <td colspan="8" height="1" bgcolor="#EBEBEB"></td>
        </tr>
		<%
	 	   j=j-1
		   rs.MoveNext ' 다음 레코드로 이동한다.
	           i=i+1	
		   loop '레코드의 끝까지 loop를 돈다.
		%> 
		<% end if %>
      </table>
      

      <table width="850px" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="10" colspan="8"></td>
        </tr>
        <tr> 
          <td align="center"colspan="3">
		  <% if cint(startpage)<>cint(1) then%> 
             <a href="01_list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
             <img src="image/ppre.gif" width="15" height="15" border="0" align="absmiddle"></a>
          <%end if%>
			<% for i = startpage to endpage step 1 %>
             <% if cint(i) = cint(page) then%>
                 [<%=page%>]
             <%else%>
                 <a href="01_list.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
                  <%=i%> </a>
                 <!--해당되는 페이지로 이동시킨다.-->
             <%end if%>
            <% next%>
		  <% if cint(endpage)<>cint(rs.PageCount) then%>
             <a href="01_list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
             <img src="image/nnext.gif" width="15" height="15" border="0" align="absmiddle"></a>
          <% end if%>
		  </td>
        </tr>

		<tr> 
            <td align="center"colspan="3"><br />
			<table width="30%" border="0" cellspacing="2" cellpadding="0">
				<form name="searchForm" action="01_list.asp" method="post" onSubmit="return searchSendit();">	
                <tr> 
                    <td>
                        <select name="keyfield" style="height:20px">
                            <option value="name">후원자이름</option>
                            <option value="conid">후원자번호</option>
                            <option value="hand">연락처</option>
                        </select>
                    </td>
                    <td><input name="key" type="text" class="text"></td>
                    <td><input type="submit" name="Submit" value="찾기"></td>
                </tr>
				</form>
            </table>
            </td>
         </tr>

      </table>

<% '사용한 개체드릉 모두 반납한다.
    rs.Close
    set rs = nothing
    
    rsCnt.Close
	Set rsCnt = nothing
%>
</div>
</body>
</html>
