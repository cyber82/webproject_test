﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<% 
'주석처리 2013-01-18
'<!--#include file="../inc/top_main.asp"//--> 
%>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  


  link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"

  if request("idx") = "" then
    ''idx = 1
    response.write "<script type='text/javascript' charset='euc-kr'>alert('해당 글이 없습니다.');</script>"
    response.write link
  else
    idx=request("idx")
  end if
%>

<%
    sql = " select "
    sql = sql & " idx, k_code, kor_name, eng_name, gender, kuka, major, sponsor_year, s_content, movie, b_filename, conid, name, s_confrim, " 
    sql = sql & " convert(varchar, writeday, 120) as writeday "
    sql = sql & " from ldp_student "
    sql = sql & " where idx = " & idx

    'set rs = server.CreateObject("adodb.recordset")
    'rs.open sql, db  

    Set rs = RecordsetFromWS(provWSDL, sql)

    '변수에 값들을 저장
    idx = rs("idx")
    b_filename=rs("b_filename")
 
    keyfield = request("keyfield")	
    key = request("key")

    sqlcon = "select s_content from ldp_student where idx = " & idx
    s_content = ContentViewLoadWS(provWSDL, sqlcon)

    'Response.Write(sqlcon)
    'Response.End()

    's_content = rs("s_content")
    's_content = replace(rs("s_content"), chr(13) & chr(10), "<br>")
    s_content = replace(s_content, chr(10), "<br/>")
    s_content = replace(s_content, chr(13), "<br/>")
    s_content = replace(s_content, "&lt;", "<")
    s_content = replace(s_content, "&gt;", ">")
    s_content = replace(s_content, "&amp;", "&")
    s_content = replace(s_content, "&apos;", "'")
    s_content = replace(s_content, "&quot;", """")
%> 


<body leftmargin="0" topmargin="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
	 <!--#include file="../inc/top.asp"//-->
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="600"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9">LDP 학생</font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">후원여부</td>
                      <td bgcolor="#FFFFFF" style="padding-left:10;padding-right:10;"><%=rs("s_confrim")%> </td>
                    </tr>         
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">한글이름</td>
                      <td bgcolor="#FFFFFF" style="padding-left:10;padding-right:10;"><%=rs("kor_name")%> </td>
                    </tr> 
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">영문이름</td>
                      <td bgcolor="#FFFFFF" style="padding-left:10;padding-right:10;"><%=rs("eng_name")%> </td>
                    </tr> 
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">성별</td>
                      <td bgcolor="#FFFFFF" style="padding-left:10;padding-right:10;"><%=rs("gender")%> </td>
                    </tr> 					
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">국가</td>
                      <td bgcolor="#FFFFFF" style="padding-left:10;padding-right:10;"><%=rs("kuka")%> </td>
                    </tr> 
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">전공</td>
                      <td bgcolor="#FFFFFF" style="padding-left:10;padding-right:10;"><%=rs("major")%> </td>
                    </tr> 
                    <tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">후원기간</td>
                      <td bgcolor="#FFFFFF" style="padding-left:10;padding-right:10;"><%=rs("sponsor_year")%> 년 </td>
                    </tr> 

                    <tr bgcolor="#FFFFFF"> 
                      <td height="200" align="center" bgcolor="#F3F3F3">학생소개</td>
                      <td valign="top" style="padding-left:10;padding-top:10;padding-right:10;padding-bottom:10;"><%=s_content %></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                      <td height="25" align="center" bgcolor="#F3F3F3">학생 이미지</td>
                      <td height="25" style="padding-left:10;padding-right:10;">
                        <% If rs("b_filename") = "" then %>
                            파일이 없습니다.
                        <% else %>
                            <img src="/upload/ldp/<%=rs("b_filename")%>" width="300px">
                        <% end if%>
                      </td>
                    </tr>
                    <tr bgcolor="#F3F3F3"> 
                      <td height="25" align="center" bgcolor="#F3F3F3" colspan="2">동영상 링크</td>
                    </tr> 
                    <tr bgcolor="#FFFFFF"> 
                      <td valign="top"  align="center" colspan="2" style="padding-left:10;padding-top:10;padding-right:10;padding-bottom:10;"><%=rs("movie")%></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>

              <tr> 
                <td align="right"><table width="10%" border="0" cellspacing="2" cellpadding="0">
                    <tr>
                      
					  <td>
					    <a href="edit.asp?idx=<%=idx%>&page=<%=page%>" onfocus="this.blur()"><img src="../img/btn06.gif" width="44" height="20" border="0"></td>
                      <td><a href="javascript:delChk('delete_ok.asp?idx=<%=idx%>&page=<%=page%>');"><img src="../img/btn02.gif" width="44" height="20" border="0"></td>
                      <td><a href="list.asp?page=<%=page%>"><img src="../img/btn16.gif" width="42" height="20" border="0"></a></td>

                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
			

			
			</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>

<script type="text/javascript" charset='euc-kr'>
<!--
    function delChk(url) {
        if (confirm('정말 삭제하시겠습니까?')) {
            location.href = url;
        }
    }
//-->
</script>
</body>
</html>