﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

	keyfield = request("keyfield")	
	key = request("key")	
	m_code  = request("m_code")	
%>
<%
	table_idx = request("table_idx")
%>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>


<body leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="800px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22px"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td width="80px"><strong><font color="#3788D9">LDP 학생</font></strong></td>
                      <td><a href="write.asp?&page=<%=page%>" onfocus="this.blur()">
                        <img src="../img/btn17.gif" width="52" height="20" border="0"></a>
                      </td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>

            <!------- 게시판 목록 구하기 시작 ----------> 			
            <%
                sql = " select "
                sql = sql & " idx, k_code, kor_name, eng_name, gender, kuka, major, sponsor_year, s_content, movie, b_filename, conid, name, s_confrim, " 
                sql = sql & " convert(varchar, writeday, 120) as writeday "
                sql = sql & " from ldp_student "
                sql = sql & " order by writeday desc "

                'set rs = server.CreateObject("adodb.recordset")
                'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
                
                'response.Write sql
                'response.End 
                Set rs = RecordsetFromWS(provWSDL, sql)

                'totalcount
                sqlCnt = " select "
                sqlCnt = sqlCnt & " count(idx) as totalCnt " 
                sqlCnt = sqlCnt & " from ldp_student "

                'response.Write sqlCnt
                'response.End                  
                Set rsCnt = RecordsetFromWS2(provWSDL, sqlCnt)
            %>


   	              <table width="100%" border="0" cellspacing="0" cellpadding="0">
		            <% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면%>
		            <tr> 
                      <td height="22" colspan="5" align="center"><b>글이 없습니다!!!</b></td>
                    </tr>

		            <% else '데이터가 있다면
   
                        rs.PageSize = 6 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

			            totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
			            rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
			            endpage = startpage + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
			            if endpage > rs.PageCount then
				            endpage = rs.PageCount
			            end if
		            %>
		            <tr valign="top" align="left"> 


            <%	
                host = request.ServerVariables("HTTP_HOST")
                if host = "www.compassion.or.kr" then
                    addr = "http://www.compassion.or.kr"
                else 
                    addr = "http://www.compassionko.org"
                end if
                

	            i = 1
	            do until rs.EOF or i > rs.pagesize
	
	            b_filename = rs("b_filename")
            %>
                     <td width="200px" height="100px" align="center"> 
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="150px">
				              <table border="0" width="100%" cellspacing="0" cellpadding="0" bordercolor="#D8D8D8">
                              <tr>
				                 <td>
                                    <a href="content.asp?idx=<%=rs("idx")%>&page=<%=page%>" onfocus="this.blur()">
                                        <img src="/upload/ldp/<%=trim(b_filename)%>" width="150px" height="230px" border="0">
                                    </a>
                                 </td>
                              </tr>
				              </table>
			                </td>
                          </tr>

			              <tr>
                            <td width="150px" align="center">
				                <br />
				                <a href="content.asp?idx=<%=rs("idx")%>&page=<%=page%>" onfocus="this.blur()">
                                <strong><%=rs("kor_name")%></strong>
				                <br />
				                (결연 : <%=rs("s_confrim")%>)
				                </a>
				                <br /><br />
				            </td>
		                  </tr>
                        </table>
                     </td>
            <%
                  if (i mod 3 = 0) then
	                 response.write("</tr><tr>")
		            end if 
	
	            i = i+1		
	            rs.movenext
	            loop	 
            %>
                  </tr>
            <% end if %> 

                  </table>




				</td>
              </tr>
              <!--<tr> 
                <td>&nbsp;</td>
              </tr>-->

              <tr> 
                <td align="center">
					<% if cint(startpage)<>cint(1) then%> 
				         <a href="list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [이전]</a>
			        <%end if%> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then%>
				         [<%=page%>]
		             <%else%>
					     <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"> <%=i%> </a>
		             <%end if%>
				   <% next%>

				   <% if cint(endpage)<>cint(rs.PageCount) then%>
			             <a href="list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [다음]</a>
			       <% end if%>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
			  
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
