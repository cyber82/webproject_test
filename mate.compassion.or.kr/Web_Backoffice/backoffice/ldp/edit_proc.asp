﻿<%@Language="VBScript" CODEPAGE="65001"%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/freeASPUpload.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"
    
provWSDL = session("serviceUrl")

    if request("idx") = "" then
        idx = 0
    else
        idx=request("idx")
    end if

    table_idx = request("table_idx")
    no_idx = request("no_idx")
    page = request("page")

    keyfield = request("keyfield")	
    key = request("key")

    link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"
%>


<% 
DIM Data_Folder
DIM ofilename, ofiledate, ofilesize

'저장 폴더명
Data_Folder = Server.MapPath("/") & "\upload\ldp\"


fname = request("fname")

If instrRev(fname, "\") > 0 Then

    fstart = instrRev(fname, "\") + 1
    fmax = Len(fname)

    old_filename = Mid(fname, fstart, fmax)

End If

'============= 기존파일 처리 ==============
'If old_filename <> "" Then
'    strDelPathOld = Data_Folder & old_filename
'        
'    Set fso = CreateObject("Scripting.FileSystemObject")
'    '파일 존재시 문구 출력
'    If fso1.FileExists(strDelPathOld) Then
'        link2 = "location.href='list.asp?page=" & page & "&startpage=" & startpage & "';"
'        response.write "<script type='text/javascript' charset='euc-kr'>"
'        response.write "if(confirm('파일이 존재합니다. 덮어쓰겠습니까?')){"
'        response.write "} else {"
'        response.write link2          
'        response.write "}"
'        response.write "</script>"
'    End If
'    Set fso = Nothing
'End If
'============= 기존파일 처리 ==============

'response.Write(old_filename + "<br/>")
'response.end()
%>

<%
    sql = " select top 1 idx from ldp_student where idx = " & idx

    'set rs = db.Execute(sql)
    set rs = RecordsetFromWS(provWSDL, sql)

    if rs.BOF or rs.EOF then ' 만일 레코드가 없다면
        ssb_idx = 0
    else '데이터가 있다면
        ssb_idx = rs(0)
    end if

    
    'response.Write ssb_idx
    'response.end


    if CInt(ssb_idx) > 0 then


        '파일 등록
        SET Upload = NEW FreeASPUpload
    
        '***************** 이미지 저장 *****************************
        Upload.Save(Data_Folder)

        ks = Upload.UploadedFiles.keys
        if (UBound(ks) <> -1) then
            for each fileKey in Upload.UploadedFiles.keys
                SaveFileName = Upload.UploadedFiles(fileKey).FileName


                If SaveFileName <> "" Then

                    vFilename = SaveFileName						'파일이름
                    strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
                    strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

                    set fso = CreateObject("Scripting.FileSystemObject")
                    set file = fso.GetFile(Data_Folder & "\" & SaveFileName)

                    '새이름 생성
                    newFilename = MakeFileName2(strName) & "." & strExt
                    file.name = newFilename

                    set file = nothing
                    set fso = Nothing

                    '디비에 저장될 이미지 경로
                    strFilePath = Data_Folder & Replace(strDateDir,"\","/") & newFilename

                End If
            next
        end if
        '***************** 이미지 저장 *****************************


        old_filename = Upload.Form("old_filename")
        b_filename = Upload.Form("b_filename")
        kor_name = Upload.Form("kor_name")
        eng_name = Upload.Form("eng_name")  
        gender = Upload.Form("gender")
        kuka = Upload.Form("kuka")
        major = Upload.Form("major")
        sponsor_year = Upload.Form("sponsor_year")
        s_content = Upload.Form("s_content")
        movie = Upload.Form("movie")
        s_confrim = Upload.Form("s_confrim")
  
        s_content = replace(s_content,"'","''") '쿼리에 '가 들어가면 에러가 나기 때문에 그것을 replace처리 합니다.


        '============= 기존파일 같은파일이 아닐때 삭제 ==============
        If old_filename <> SaveFileName AND old_filename <> "" AND SaveFileName <> "" Then
            strDelPath1 = Data_Folder & old_filename
        
            Set fso = CreateObject("Scripting.FileSystemObject")
            '파일 존재시 파일 삭제
            If fso.FileExists(strDelPath1) Then
         	    fso.DeleteFile strDelPath1
            End If
            Set fso = Nothing
        End if
        '============= 기존파일 삭제 ==============

        '============= 기존파일 같은파일일때 처리 ==============
        If old_filename = SaveFileName AND old_filename <> "" AND SaveFileName <> "" Then
            strDelPath1 = Data_Folder & old_filename
        
            Set fso = CreateObject("Scripting.FileSystemObject")
            '파일 존재시 문구 출력
            If fso.FileExists(strDelPath1) Then
                link2 = "location.href='list.asp?page=" & page & "&startpage=" & startpage & "';"
                response.write "<script type='text/javascript' charset='euc-kr'>"
                response.write "if(confirm('파일이 존재합니다. 덮어쓰겠습니까?')){"
                response.write "} else {"
                response.write link2          
                response.write "}"
                response.write "</script>"
            End If
            Set fso = Nothing
        End If
        '============= 기존파일 처리 ==============



       '****************************************************
       '  Upload 할 파일이 없을때 적절한 값으로 대체   
       '****************************************************
       'if b_filename = "" then
       '    b_filesize=0
       '    b_filename=""
       'end if
   
       '*****************************************************
       '  file size변환
       '*****************************************************            
      
        'temp = clng(filesize)
        'if temp > 1024 then   '1024byte(1kbyte) 보다 크면
        '    temp = temp / 1024
        '    b_filesize = cstr(cint(temp)) & "k"
        'end if 


        if strFilePath <> "" then
	        '============= 기존파일 삭제 ==============
	        If b_filename <> "" Then
		        arr_fname = Split(b_filename, "/")
		        fname = arr_fname(Ubound(arr_fname))

		        strDateDir1 = arr_fname(Ubound(arr_fname)-2) & "\"
		        strDateDir2 = arr_fname(Ubound(arr_fname)-1) & "\"
		        strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
		        strDelPath = strSaveFolder & fname

		        Set fso = CreateObject("Scripting.FileSystemObject")
		        '파일 존재시 파일 삭제
		        If fso.FileExists(strDelPath) Then
		           fso.DeleteFile strDelPath
		        End If
		        Set fso = Nothing

	        End if
	        '============= 기존파일 삭제 ==============
	        strImgFile = strFilePath
        else
	        strImgFile = b_filename
        end If

        '추가 2013-04-15
        if strFilePath <> "" then
            strFileName = newFilename
        else 
            strFileName = old_filename
        end if 


        'if strImgFile <> "" then   
        
            Dim sql_save

            '============= 기존파일 처리 ==============
            'If old_filename <> "" Then
                'strDelPathOld = Data_Folder & old_filename
                
                'Set fso1 = CreateObject("Scripting.FileSystemObject")
                '파일 존재시 문구 출력
            '    If fso1.FileExists(strDelPathOld) Then
            '        link2 = "location.href='list.asp?page=" & page & "&startpage=" & startpage & "';"
            '        response.write "<script type='text/javascript' charset='euc-kr'>"
            '        response.write "if(confirm('파일이 존재합니다. 덮어쓰겠습니까?')){"
            '        response.write "} else {"
            '        response.write link2          
            '        response.write "}"
            '        response.write "</script>"
            '    End If
                '파일 존재시 파일 삭제
            '    If fso1.FileExists(strDelPathOld) Then
            '	    fso1.DeleteFile strDelPathOld
            '    End If
            '    Set fso = Nothing
            'End If
            '============= 기존파일 처리 ==============

            sql_save = "UPDATE ldp_student SET "
            sql_save = sql_save & " kor_name=N'" & kor_name & "' "
            sql_save = sql_save & ", eng_name=N'" & eng_name & "' "
            sql_save = sql_save & ", gender=N'" & gender & "' "
            sql_save = sql_save & ", kuka=N'" & kuka & "' "
            sql_save = sql_save & ", major=N'" & major & "' " 
            sql_save = sql_save & ", sponsor_year=N'" & sponsor_year & "' "
            sql_save = sql_save & ", s_content=N'" & s_content & "' "
            sql_save = sql_save & ", movie=N'" & movie & "' "
            sql_save = sql_save & ", b_filename=N'" & strFileName & "' "
            sql_save = sql_save & ", s_confrim=N'" & s_confrim & "' "
            sql_save = sql_save & " WHERE idx = " & idx


            'response.Write sql_save
            'response.end


            '글등록
            'db.Execute(sql_save)
            inresult = BoardWriteWS(provWSDL, sql_save)
    

            if inresult <> "10" AND inresult <> "" then

                '============= 파일 삭제 ==============
                If strImgFile <> "" Then
            	    strDelPath = Data_Folder & newFilename
            
            	    Set fso = CreateObject("Scripting.FileSystemObject")
            	    '파일 존재시 파일 삭제
            	    If fso.FileExists(strDelPath) Then
            		    fso.DeleteFile strDelPath
            	    End If
            	    Set fso = Nothing
                End if
                '============= 파일 삭제 ==============

                response.write "<script type='text/javascript' charset='euc-kr'>alert('" & inresult & "');</script>"
                response.write link

            else

                response.write "<script type='text/javascript' charset='euc-kr'>alert('수정되었습니다.');</script>"
                response.write link

            end if
        'else
            'response.write "<script type='text/javascript' charset='euc-kr'>alert('사진이 담겨 있지 않습니다. 다시 확인하시기 바랍니다.');</script>"
            'response.write "<script type='text/javascript' charset='euc-kr'>alert('strImgFile : " & strImgFile & "');</script>"
            'response.write link
        'end if


    else 


        '============= 기존파일 삭제 ==============
        'If SaveFileName <> "" Then
        '	strDelPath1 = Data_Folder & SaveFileName
        '
        '	Set fso = CreateObject("Scripting.FileSystemObject")
        '	'파일 존재시 파일 삭제
        '	If fso.FileExists(strDelPath1) Then
        '		fso.DeleteFile strDelPath1
        '	End If
        '	Set fso = Nothing
        'End if
        '============= 기존파일 삭제 ==============

        response.write "<script type='text/javascript' charset='euc-kr'>alert('해당 글이 없습니다.');</script>"
        response.write link

        
    end if
%>

</html>
