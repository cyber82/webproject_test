﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
    '전체 문자 치환
    Function eregi_replace(pattern, replace, text)
        Dim eregObj
        Set eregObj = New RegExp
        eregObj.Pattern = pattern '//패턴설정
        eregObj.IgnoreCase = True '//대소문자 구분 여부
        eregObj.Global = True '//전체 문서에서 검색
        eregi_replace = eregObj.Replace(text, replace) '//Replace String
    End Function

    '정규식 문자 치환
	Function strip_tags(str)
		Dim content
        content = str
        content = eregi_replace("[<][a-z|A-Z|/](.|\n)*?[>]", "", content)
        content = eregi_replace("&nbsp;", "", content)
		strip_tags = content
	End Function

  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if 
  

  keyfield = request("keyfield")	
  key = request("key")
%>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link type="text/css" href="../index.css" rel="stylesheet">

<% '추가 2013-07-17 %>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/tinymce_config.js" charset='euc-kr'></script>
<script type="text/javascript">
    tinymce_config("s_content");
</script>
</head>

<script type="text/javascript">
<!--
    function sendit()
    {
        if(document.myform.kor_name.value =="") {
            alert("한글이름을 입력해 주십시오.");
            document.myform.kor_name.focus();
            return;
        }

        if(document.myform.eng_name.value =="") {
            alert("영문이름을 입력해 주십시오.");
            document.myform.eng_name.focus();
            return;
        }

        if(document.myform.kuka.value =="") {
            alert("국가를 입력해 주십시오.");
            document.myform.kuka.focus();
            return;
        }

        if(document.myform.major.value =="") {
            alert("전공을 입력해 주십시오.");
            document.myform.major.focus();
            return;
        }

        if(document.myform.sponsor_year.value =="") {
            alert("후원기간을 입력해 주십시오.");
            document.myform.sponsor_year.focus();
            return;
        }
        /*
        if(document.myform.s_content.value =="") {
            alert("학생소개를 입력해 주십시오.");
            document.myform.s_content.focus();
            return;
        }
        */

        if(document.myform.movie.value =="") {
            alert("동영상 링크를 입력해 주십시오.");
            document.myform.movie.focus();
            return;
        }

        document.myform.action = "write_proc.asp?page=<%=page%>&startpage=<%=startpage%>&fname=" + document.myform.b_filename.value;
        document.myform.method = "post";
        document.myform.submit();
    }

    //- 숫자만입력
    function OnlyNumber() {
        key = event.keyCode;

        if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
            event.returnValue = true;
        }
        else {
            event.returnValue = false;
        }
    }

//-->
</script>


<body leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	 <!--#include file="../inc/top.asp"//-->
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9">LDP 학생</font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
<form name="myform" Enctype="multipart/form-data" method = "post" action="write_proc.asp?page=<%=page%>&keyfield=<%=keyfield%>&key=<%=key%>">
<input type="hidden" name="idx" value="<%=request("idx")%>">
<input type="hidden" name="ref" value="<%=request("ref")%>">
<input type="hidden" name="re_step" value="<%=request("re_step")%>">
<input type="hidden" name="re_level" value="<%=request("re_level")%>">
<input type="hidden" name="b_notice" value="N">

              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">후원여부</td>
                      <td bgcolor="#FFFFFF"><input type="radio" name="s_confrim" value="Y">Y <input type="radio" name="s_confrim" value="N" checked>N</td>
                    </tr>
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">한글이름</td>
                      <td bgcolor="#FFFFFF"><input type="text" name="kor_name" size="40" maxlength="50" style="ime-mode:active;"></td>
                    </tr>
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">영문이름</td>
                      <td bgcolor="#FFFFFF"><input type="text" name="eng_name" size="40" maxlength="80" style="ime-mode:disabled;"></td>
                    </tr>
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">성별</td>
                      <td bgcolor="#FFFFFF"><input type="radio" name="gender" value="남자" checked>남자 <input type="radio" name="gender" value="여자">여자</td>
                    </tr>
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">국가</td>
                      <td bgcolor="#FFFFFF"><input type="text" name="kuka" size="10" maxlength="10" style="ime-mode:active;"></td>
                    </tr>
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">전공</td>
                      <td bgcolor="#FFFFFF"><input type="text" name="major" size="10" maxlength="10" style="ime-mode:active;"></td>
                    </tr>
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">후원기간</td>
                      <td bgcolor="#FFFFFF"><input type="text" name="sponsor_year" size="10" maxlength="10" style="ime-mode:disabled;" onkeydown="javascript:OnlyNumber();">년 (숫자만 입력)</td>
                    </tr>
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">학생소개</td>
                      <td bgcolor="#FFFFFF">
                        <!--<textarea name="s_content" cols="60" rows="15" style="ime-mode:active;"></textarea>-->

                        <% '수정 2013-06-19 %>
                        <div>
	                        <textarea name="s_content" id="s_content" style="width:600px;height:170px;ime-mode:active;" class="sbox" tabindex="5"></textarea>
                        </div>
                        <!--<div><button onClick="addArea2();">Editor</button> <button onClick="removeArea2();">Html</button></div>-->
                      </td>
                    </tr>
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">동영상 링크</td>
                      <td bgcolor="#FFFFFF"><textarea name="movie" cols="60" rows="3"></textarea></td>
                    </tr>
                    <tr bgcolor="#FFFFFF"> 
                      <td height="25" align="center" bgcolor="#F3F3F3">학생 이미지</td>
                      <td><input type="file" name="b_filename" size="40" maxlength="30"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="right"><a href="javascript:sendit();" onfocus="this.blur()"><img src="../img/btn03.gif" width="44" height="20" border="0"></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>

    <% '추가 2013-06-19 %>
    <% '주석처리 2013-07-17 %>
    <!--<script src="/Web_Backoffice/backoffice/inc/js/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        var area1, area2;
        /*
        function toggleArea1() {
        if (!area1) {
        area1 = new nicEditor({ fullPanel: true }).panelInstance('myArea1', { hasPanel: true });
        } else {
        area1.removeInstance('myArea1');
        area1 = null;
        }
        }
        */

        function addArea2() {
            area2 = new nicEditor({ fullPanel: true }).panelInstance('s_content');
        }
        function removeArea2() {
            area2.removeInstance('s_content');
        }

        bkLib.onDomLoaded(function () { addArea2(); });
    </script>-->
</body>
</html>
