﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
</head>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/freeASPUpload.asp" -->
<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  

  code=request("code")
  selView=request("selView")
  viewYN=request("viewYN")

  if request("idx") = "" then
    idx = 0
  else
    idx=request("idx")
  end if

  keyfield = request("keyfield")	
  key = request("key")

  link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?code=" & code & "&page=" & page & "&startpage=" & startpage & "';</script>"
%>

<%
DIM Data_Folder, strDelPath1

'저장 폴더명
Data_Folder = Server.MapPath("/") & "\main_image\" & code & "\"

b_filename = Request.Form("b_filename")

'============= 기존파일 삭제 ==============
If b_filename <> "" Then
	strDelPath1 = Data_Folder & b_filename

	Set fso = CreateObject("Scripting.FileSystemObject")
	'파일 존재시 파일 삭제
	If fso.FileExists(strDelPath1) Then
		fso.DeleteFile strDelPath1
	End If
	Set fso = Nothing

End if

'response.Write(strDelPath1)
'response.end()
'============= 기존파일 삭제 ==============
%>

<%
    sql = "select top 1 idx from rolling_ssb where idx = "& idx
    'set rs = db.Execute(sql)
    set rs = RecordsetFromWS(provWSDL, sql)

    if rs.BOF or rs.EOF then ' 만일 레코드가 없다면
        main_idx = 0
    else '데이터가 있다면
        main_idx = rs(0)
    end if


    if CInt(main_idx) > 0 then
    '테이블 rolling_ssb

        sql = "update rolling_ssb set del_flag = N'Y'"
        sql = sql & " where idx = " & idx
        'db.Execute sql
        inresult = BoardWriteWS(provWSDL, sql)

        rs.Close
        set rs = nothing

        response.write "<script type='text/javascript' charset='euc-kr'>alert('삭제되었습니다.');</script>"
        response.write link
    else 

        response.write "<script type='text/javascript' charset='euc-kr'>alert('글이 선택되지 않았습니다.');</script>"
        response.write link
    end if
%>

</html>
      