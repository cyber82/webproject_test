﻿<%@Language="VBScript" CODEPAGE="65001"%>
<%
	Response.CharSet = "utf-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
'provWSDL = "http://10.181.111.110/WWW5Service/AdminService.asmx?WSDL"

'수정 2013-02-27
provWSDL = session("serviceUrl")
%>

<%
    '전체 문자 치환
    Function eregi_replace(pattern, replace, text)
        Dim eregObj
        Set eregObj = New RegExp
        eregObj.Pattern = pattern '//패턴설정
        eregObj.IgnoreCase = True '//대소문자 구분 여부
        eregObj.Global = True '//전체 문서에서 검색
        eregi_replace = eregObj.Replace(text, replace) '//Replace String
    End Function

    '정규식 문자 치환
	Function strip_tags(str)
		Dim content
        content = str
        content = eregi_replace("[<][a-z|A-Z|/](.|\n)*?[>]", "", content)
        content = eregi_replace("&nbsp;", "", content)
		strip_tags = content
	End Function

  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if 
  
  code = request("code") 

  keyfield = request("keyfield")	
  key = request("key")
%>

    <script type="text/javascript">
    <!--
        function b_list() {
            location.href = "list.asp?code=<%=code%>&page=<%=page%>&startpage=<%=startpage%>"
        }


        function sendit() {

            //파일명
            if (document.myform.b_filename.value == "") {

                alert("파일을 선택해 주십시오.");
                return;
            }

            //제목
            if (document.myform.b_title.value == "") {
                alert("제목을 입력해 주십시오.");
                return;
            }

            //순서
            if (document.myform.b_order.value == "") {
                alert("순서를 입력해 주십시오.");
                return;
            }

            //타켓
            if (document.myform.b_target.value == "") {
                alert("타켓을 선택해 주십시오.");
                return;
            }

            //보기여부
            if (document.myform.b_view.value == "") {
                alert("보기여부를 선택해 주십시오.");
                return;
            }

            document.myform.action = "write_proc.asp?code=<%=code%>&page=<%=page%>&startpage=<%=startpage%>&fname=" + document.myform.b_filename.value;
            document.myform.method = "post";
            document.myform.submit();
        }
      
    //-->
    </script>


<body leftmargin="0" topmargin="0" oncontextmenu='return false' ondragstart='return false'>

<form name="myform" method="post" action="" enctype="multipart/form-data" accept-charset="utf-8">
<!--<input type="hidden" name="code" value="<%=code%>">-->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	 <!--#include file="../inc/top.asp"//-->
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9"><!--#include file="title.asp"//--></font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
					<tr bgcolor="#F3F3F3"> 
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">파일업로드</td>
                      <td bgcolor="#FFFFFF">
                	      <input type="file" name="b_filename" size="50" style="ime-mode:inactive;">
                      </td>
                    </tr>

                    <tr bgcolor="#F3F3F3"> 
                      <td height="25" align="center" bgcolor="#F3F3F3">제목</td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_title" size="50" maxlength="50" value="" style="ime-mode:active;" /></td>
                    </tr>
                    <tr bgcolor="#F3F3F3"> 
                      <td height="25" align="center" bgcolor="#F3F3F3">순서</td>
                    <%
                        sql = " select "
                        sql = sql & " case when exists(select img_order from rolling_ssb where del_flag='N') "
                        sql = sql & " then (select max(img_order) from rolling_ssb where del_flag='N') "
                        sql = sql & " else 0 "
                        sql = sql & " end as b_order "

                        'set rs = db.Execute(sql)
                        set rs = RecordsetFromWS(provWSDL, sql)

                        if rs.BOF or rs.EOF then
                            temp = 0
                        else
                            do until rs.EOF 
                            temp = cint(rs("b_order")) + 1
                            rs.MoveNext
                            loop
                        end if
                    %>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_order" size="5" maxlength="10" value='<%=temp %>' style="ime-mode:disabled;" /></td>
                    </tr>
                    <tr bgcolor="#F3F3F3"> 
                      <td height="25" align="center" bgcolor="#F3F3F3">링크</td>
                      <td bgcolor="#FFFFFF"><input type="text" name="b_link" size="80" value="" style="ime-mode:disabled;" /></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                      <td height="25" align="center" bgcolor="#F3F3F3">타겟</td>
                      <td bgcolor="#FFFFFF">
						  <select name="b_target" style="height:18px;">
                            <option value="S" selected>현재창</option>
                            <option value="B">새창</option>
						  </select>
					  </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                      <td height="25" align="center" bgcolor="#F3F3F3">보기여부</td>
                      <td bgcolor="#FFFFFF">
						  <select name="b_view" style="height:18px;">
				            <option value="Y" selected>사용</option>
			                <option value="N">사용안함</option>
						  </select>
					  </td>
                    </tr>

                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="right"><a href="javascript:sendit();" onfocus="this.blur()"><img src="../img/btn03.gif" width="44" height="20" border="0"></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</form>

</body>
</html>
