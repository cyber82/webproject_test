﻿<%@Language="VBScript" CODEPAGE="65001"%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<%
    '전체 문자 치환
    Function eregi_replace(pattern, replace, text)
        Dim eregObj
        Set eregObj = New RegExp
        eregObj.Pattern = pattern '//패턴설정
        eregObj.IgnoreCase = True '//대소문자 구분 여부
        eregObj.Global = True '//전체 문서에서 검색
        eregi_replace = eregObj.Replace(text, replace) '//Replace String
    End Function

    '정규식 문자 치환
	Function strip_tags(str)
		Dim content
        content = str
        content = eregi_replace("[<][a-z|A-Z|/](.|\n)*?[>]", "", content)
        content = eregi_replace("&nbsp;", "", content)
		strip_tags = content
	End Function

  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if 
  

  keyfield = request("keyfield")	
  key = request("key")
%>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link type="text/css" href="../index.css" rel="stylesheet">
<link type="text/css" href="../inc/js/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../inc/js/ui/demos.css" rel="stylesheet" />
    <style type="text/css">
    .sbox{border:1px solid #bbb;}
    </style>

<script type="text/javascript" src="../inc/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.core.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.datepicker-ko.js"></script>

    <script type="text/javascript">
    <!--
        //$("#datepicker").datepicker($.datepicker.regional['ko']);

	    $(function() {
	        $('#start_date').datepicker({
			    changeMonth: true,
			    changeYear: true,
			    yearRange: '2009:2020'
		    });		
	    });

	    $(function() {
		    $('#end_date').datepicker({
			    changeMonth: true,
			    changeYear: true,
			    yearRange: '2009:2020'
		    });		
	    });

	    $(function() {
		    $('#close_date').datepicker({
			    changeMonth: true,
			    changeYear: true,
			    yearRange: '2009:2020'
		    });		
	    });


        function b_list() {
            location.href = "list.asp?page=<%=page%>&startpage=<%=startpage%>"
        }

        function sendit() {

            //방문국가
            if (document.myform.visit_nation.value == "") {
                alert("방문국가를 입력해 주십시오.");
                document.myform.visit_nation.focus();
                return;
            }

            //일정
            if (document.myform.start_date.value == "") {
                alert("일정(Start)을 선택해 주십시오.");
                document.myform.start_date.focus();
                return;
            }

            //일정
            if (document.myform.end_date.value == "") {
                alert("일정(End)을 선택해 주십시오.");
                document.myform.end_date.focus();
                return;
            }

            //상태
            if (document.myform.trip_state.value == "") {
                alert("상태를 선택해 주십시오.");
                document.myform.trip_state.focus();
                return;
            }

            //게시여부
            if (document.myform.trip_view.value == "") {
                alert("게시여부를 선택해 주십시오.");
                document.myform.trip_view.focus();
                return;
            }

            document.myform.action = "write_proc.asp?page=<%=page%>&startpage=<%=startpage%>";
            document.myform.method = "post";
            document.myform.submit();
        }

        //- 숫자만입력
        function OnlyNumber() {
            key = event.keyCode;

            if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
                event.returnValue = true;
            }
            else {
                event.returnValue = false;
            }
        }
      
    //-->
    </script>

</head>

<!--<body leftmargin="0" topmargin="0">-->
<body leftmargin="0" topmargin="0" oncontextmenu='return false' ondragstart='return false'>

<form name="myform" method="post" action="">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	 <!--#include file="../inc/top.asp"//-->
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="70">&nbsp;</td>
          <td width="900"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9"><!--#include file="title.asp"//--></font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
					<tr bgcolor="#F3F3F3">
                      <td width="15%" height="25" align="center" bgcolor="#F3F3F3">방문국가</td>
                      <td bgcolor="#FFFFFF"><span style="margin-left:10px;"></span>
                        <input type="text" id="visit_nation" name="visit_nation" size="30" maxlength="30" value="" style="ime-mode:active;" class="sbox" />
                      </td>
                    </tr>

                    <tr bgcolor="#F3F3F3">
                      <td height="25" align="center" bgcolor="#F3F3F3">일정</td>
                      <td bgcolor="#FFFFFF"><span style="margin-left:10px;"></span>

                        Start : <input type="text" id="start_date" name="start_date" size="10" maxlength="10" value="" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" /> 
                        &nbsp;&nbsp; ~ &nbsp;&nbsp;
                        End : <input type="text" id="end_date" name="end_date" size="10" maxlength="10" value="" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" />

                      </td>
                    </tr>

                    <tr bgcolor="#F3F3F3">
                      <td height="25" align="center" bgcolor="#F3F3F3">예상인원</td>
                      <td bgcolor="#FFFFFF"><span style="margin-left:10px;"></span>
                        <input type="text" id="expect_num" name="expect_num" size="30" maxlength="30" value="" style="ime-mode:active;" class="sbox" />
                      </td>
                    </tr>

                    <tr bgcolor="#F3F3F3">
                      <td height="25" align="center" bgcolor="#F3F3F3">신청비</td>
                      <td bgcolor="#FFFFFF"><span style="margin-left:10px;"></span>
                        <input type="text" id="request_cost" name="request_cost" size="30" maxlength="30" value="" style="ime-mode:active;" class="sbox" />
                      </td>
                    </tr>

                    <tr bgcolor="#F3F3F3">
                      <td height="25" align="center" bgcolor="#F3F3F3">여행비용</td>
                      <td bgcolor="#FFFFFF"><span style="margin-left:10px;"></span>
                        <input type="text" id="trip_cost" name="trip_cost" size="30" maxlength="30" value="" style="ime-mode:active;" class="sbox" />
                      </td>
                    </tr>

                    <tr bgcolor="#F3F3F3">
                      <td height="25" align="center" bgcolor="#F3F3F3">마감일</td>
                      <td bgcolor="#FFFFFF"><span style="margin-left:10px;"></span>
                        <input type="text" id="close_date" name="close_date" size="10" maxlength="10" value="" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" />
                      </td>
                    </tr>

                    <tr bgcolor="#FFFFFF">
                      <td height="25" align="center" bgcolor="#F3F3F3">상태</td>
                      <td bgcolor="#FFFFFF"><span style="margin-left:10px;"></span>
						  <select id="trip_state" name="trip_state" style="height:18px;" class="sbox">
				            <option value="Y" selected>신청중</option>
                            <option value="S">예정</option>
			                <option value="N">마감</option>
						  </select>
					  </td>
                    </tr>

                    <tr bgcolor="#FFFFFF">
                      <td height="25" align="center" bgcolor="#F3F3F3">게시여부</td>
                      <td bgcolor="#FFFFFF"><span style="margin-left:10px;"></span>
						  <select id="trip_view" name="trip_view" style="height:18px;" class="sbox">
				            <option value="Y" selected>Y</option>
			                <option value="N">N</option>
						  </select>
					  </td>
                    </tr>

                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="right">
                    <a href="javascript:sendit();" onfocus="this.blur()">
                    <img src="../img/btn03.gif" width="44" height="20" border="0">
                    </a>
                </td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>

</form>

</body>
</html>
