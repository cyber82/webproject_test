﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  if request("page") = "" then
    page = 1
  else
    page=request("page")
  end if
  
  if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   startpage=1
  else
   startpage = request("startpage")
  end if  


  link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"
%>

<%
    visit_nation = Request.Form("visit_nation")
    start_date = Request.Form("start_date")
    end_date = Request.Form("end_date")
    expect_num = Request.Form("expect_num")
    request_cost = Request.Form("request_cost")
    trip_cost = Request.Form("trip_cost")
    close_date = Request.Form("close_date")
    trip_state = Request.Form("trip_state")
    trip_view = Request.Form("trip_view")
%>


<%
if visit_nation <> "" then

    sql = "select count(*) from visiontrip where visit_nation='" & visit_nation & "' and start_date='" & start_date & "' and end_date='" & end_date & "' and del_flag='N'"
    'set rs = db.Execute(sql)
    set rs = RecordsetFromWS(provWSDL, sql)

    if rs.BOF or rs.EOF then ' 만일 레코드가 없다면
        ssb_idx = 0
    else '데이터가 있다면
        ssb_idx = rs(0)
    end if


    if CInt(ssb_idx) > 0 then

        response.write "<script type='text/javascript' charset='euc-kr'>alert('현재 글이 있어 등록되지 않습니다.');</script>"
        response.write link
    else 

        Dim sql_save

        sql_save = " INSERT INTO visiontrip "
        sql_save = sql_save & " (visit_nation, start_date, end_date, expect_num, request_cost, trip_cost, close_date, trip_state, trip_view) "
        sql_save = sql_save & " VALUES "
        sql_save = sql_save & " (N'" & visit_nation & "', N'" & start_date & "', N'" & end_date & "', N'" & expect_num & "', "
        sql_save = sql_save & " N'" & request_cost & "', N'" & trip_cost & "', N'" & close_date & "', N'" & trip_state & "', N'" & trip_view & "') "

        'response.Write sql_save
        'response.end

        '글등록
        'db.Execute(sql_save)
        inresult = BoardWriteWS(provWSDL, sql_save)
    
        response.write "<script type='text/javascript' charset='euc-kr'>alert('등록되었습니다.');</script>"
        response.write link
    end if

else
    response.write "<script type='text/javascript' charset='euc-kr'>alert('내용이 담겨 있지 않습니다. 다시 확인하시기 바랍니다.');</script>"
    response.write link
end if
%>

</html>