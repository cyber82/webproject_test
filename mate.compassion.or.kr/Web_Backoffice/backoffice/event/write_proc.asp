<% @CODEPAGE = 65001 %>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<%
'*******************************************************************************
'페이지 설명 : 캠페인&이벤트
'작성일      : 2013-05-21 김선오
'수정일      : 2013-09-11 김선오
'참고사항    :
'*******************************************************************************
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/freeaspupload.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
Dim strFilePath1, strFilePath2
Dim tempFile1, tempFile2


saveDefaultPath = "/Event/images/pastlist/"

'저장 폴더명
Dim strDateDir : strDateDir = getDateDivDir(0, "\")
strDefFolder = Server.MapPath(saveDefaultPath) & "\"
strSaveFolder = strDefFolder & strDateDir

Call MkDirAll(strSaveFolder)	'폴더 생성

uploadTempDir = strSaveFolder
Set Upload = New FreeASPUpload


'***************** 이미지 저장 *****************************
Upload.Save(uploadTempDir)

for each fileKey in Upload.UploadedFiles.keys

	Select Case fileKey
		Case "fname1" : tempFile1 = Upload.UploadedFiles("fname1").FileName
		Case "fname2" : tempFile2 = Upload.UploadedFiles("fname2").FileName
	End select

Next
'***************** 이미지 저장 *****************************

If tempFile1 <> "" Then
	vFilename = tempFile1						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile1)

	'새이름 생성
	newFilename1 = MakeFileName2(strName) & "."&strExt
	file.name = newFilename1

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로 (배너이미지)
	strFilePath1 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename1
End If


If tempFile2 <> "" Then
	vFilename = tempFile2						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile2)

	'새이름 생성
	newFilename2 = MakeFileName2(strName) & "."&strExt
	file.name = newFilename2

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로 (상세내용이미지)
	strFilePath2 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename2
End If

    wtype = Upload.Form("wtype") '등록, 수정 구분
    page = Upload.Form("page")
    startpage = Upload.Form("startpage")
    idx = Upload.Form("idx")

    event_num = Upload.Form("event_num")
    title = Upload.Form("title")
    easy_cont = Upload.Form("easy_cont")
    page_type = Upload.Form("page_type")
    head_flag = Upload.Form("head_flag")
    view_flag = Upload.Form("view_flag")
    contents = Upload.Form("contents")
    comment_table = Upload.Form("comment_table")
    comment_board = Upload.Form("comment_board")
    
    start_date = Upload.Form("sdate")
    end_date = Upload.Form("edate")
    issue_date = Upload.Form("idate")

    'Response.Write event_num + "<br/>"
    'Response.Write title + "<br/>"
    'Response.Write easy_cont + "<br/>"
    'Response.Write page_type + "<br/>"
    'Response.Write head_flag + "<br/>"
    'Response.Write view_flag + "<br/>"
    'Response.End


    'if contents = "" then
    '    host = request.ServerVariables("HTTP_HOST")
    '    addr = "http://www.compassion.or.kr"
    '    'addr = "http://www.compassionko.org"
    '
    '    contents = "<div style=""text-align:center;""><img src="""& strFilePath2 &""" alt=""""  style=""border: 0;"" /></div>"
    'end if
    
    title = replace(title, "'", "''")
    easy_cont = replace(easy_cont, "'", "''")
    contents = replace(contents, "'", "''")

    'title = replace(title, "`", "''")
    'easy_cont = replace(easy_cont, "`", "''")
    'contents = replace(contents, "`", "''")

    '파일
    fname1 = Upload.Form("fname1")	'배너이미지
    fname2 = Upload.Form("fname2")	'상단 내용 이미지

    thumbnail_img = Upload.Form("thumbnail_img") '배너이미지
    cont_img = Upload.Form("cont_img") '상단 내용 이미지

    link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"

'response.write "strFilePath1: " & strFilePath1 & "<br/>"
'response.write "tempFile1: " & tempFile1
'response.Write share_desc
'response.end


'등록
if wtype = "write" then

    sql = " INSERT INTO [compassweb4].[dbo].[tCampaignEvent] ("
    sql = sql & " event_num, title, easy_cont "
    sql = sql & " , [start_date], [end_date], [issue_date] "
    sql = sql & " , contents, thumbnail_img, cont_img " 
    sql = sql & " , page_type, head_flag, view_flag "
    sql = sql & " , CommentTable, CommentBoard "
    sql = sql & " ) VALUES ( "
    sql = sql & " '"& event_num &"', '"& title &"', '"& easy_cont &"' "
    
    if start_date <> "" then   sql = sql & " , '"&start_date&"' "  else   sql = sql & " , null "   end if 
    if end_date <> "" then   sql = sql & " , '"&end_date&"' "  else   sql = sql & " , null "   end if 
    if issue_date <> "" then   sql = sql & " , '"&issue_date&"' "  else   sql = sql & " , null "   end if 
    
    sql = sql & " , '"& contents &"','"& strFilePath1 &"','"& strFilePath2 &"' "
    sql = sql & " , '"& page_type &"','"& head_flag &"','"& view_flag &"' "
    sql = sql & " , '"& comment_table &"','"& comment_board &"' "
    sql = sql & " ) "

    'response.write sql
    'response.end
                
    inresult = BoardWriteWS(provWSDL, sql)

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('등록되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('등록중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if

'수정
Else

	if strFilePath1 <> "" then
		'============= 기존파일 삭제 ==============
		If thumbnail_img <> "" AND thumbnail_img <> newFilename1 Then
			arr_fname1 = Split(thumbnail_img, "/")
			fname = arr_fname1(Ubound(arr_fname1))

			strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
			strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============
		strImg1 = strFilePath1
	else
		strImg1 = thumbnail_img
	end If

	if strFilePath2 <> "" then
		'============= 기존파일 삭제 ==============
		If cont_img <> "" AND cont_img <> newFilename2 Then
			arr_fname2 = Split(cont_img, "/")
			fname = arr_fname2(Ubound(arr_fname2))

			strDateDir1 = arr_fname2(Ubound(arr_fname2)-2) & "\"
			strDateDir2 = arr_fname2(Ubound(arr_fname2)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============
		strImg2 = strFilePath2
	else
		strImg2 = cont_img
	end If
    	

    sql = " UPDATE [compassweb4].[dbo].[tCampaignEvent] SET "
    sql = sql & "event_num='"& event_num &"', title='"& title &"' "
    sql = sql & " , thumbnail_img='"& strImg1 &"', cont_img='"& strImg2 &"' "
    sql = sql & " , page_type='"& page_type &"', head_flag='"& head_flag &"', view_flag='"& view_flag &"' "
    sql = sql & " , contents='"& contents &"', CommentTable='"& comment_table &"', CommentBoard='"& comment_board &"' "
    '추가 2013-09-11
    sql = sql & " , easy_cont='"& easy_cont &"' "

    if start_date <> "" then   sql = sql & " , [start_date]='"& start_date &"' "  else   sql = sql & " , [start_date]=null "   end if 
    if end_date <> "" then   sql = sql & " , [end_date]='"& end_date &"' "  else   sql = sql & " , [end_date]=null "   end if 
    if issue_date <> "" then   sql = sql & " , [issue_date]='"& issue_date &"' "  else   sql = sql & " , [issue_date]=null "   end if 

    sql = sql & " WHERE idx = '"& idx &"' "

    'response.write sql
    'response.end

    inresult = BoardWriteWS(provWSDL, sql)

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('수정되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('수정중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if

End If

%>