﻿
<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 캠페인&이벤트
'작성일      : 2013-05-21
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
    if request("page") = "" then
        page = 1
    else
        page=request("page")
    end if
  
    if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
        startpage = 1
    else
        startpage = request("startpage")
    end if  

    if request("idx") = "" then
        idx = 0
    else 
        idx = request("idx")
    end if

	keyfield = request("keyfield")	
	key = request("key")	

    'test
    'idx = 94

If CInt(idx) > 0 Then
	wtype = "edit"
    
    sql = " SELECT "
    sql = sql & " idx, event_num, title, easy_cont, contents, thumbnail_img, cont_img, " 
    sql = sql & " page_type, head_flag, view_flag, del_flag, " 
    sql = sql & " [start_date], end_date, issue_date, "
    sql = sql & " convert(varchar(20),reg_date,120) as reg_date, "
    sql = sql & " CommentTable, CommentBoard "
    sql = sql & " FROM [compassweb4].[dbo].[tCampaignEvent] "
    sql = sql & " WHERE 1=1 AND del_flag=0 "
    sql = sql & " AND idx = " &idx

    set rs = RecordsetFromWS(provWSDL, sql) 

	if rs.BOF or rs.EOF then
    else
        idx = rs("idx")
        event_num = rs("event_num")
        title = rs("title")
        easy_cont = rs("easy_cont")
        contents = rs("contents")
        thumbnail_img = rs("thumbnail_img")
        cont_img = rs("cont_img")
        page_type = rs("page_type")
        head_flag = rs("head_flag")
        view_flag = rs("view_flag")
        start_date = rs("start_date")
        end_date = rs("end_date")
        issue_date = rs("issue_date")
        reg_date = rs("reg_date")
        comment_table = rs("CommentTable")
        comment_board = rs("CommentBoard")

        contents = replace(contents, "&lt;", "<")
        contents = replace(contents, "&gt;", ">")
        contents = replace(contents, "&amp;", "&")
        contents = replace(contents, "&apos;", "'")
        contents = replace(contents, "&quot;", """")
                

        'Response.Write event_num + "<br/>"
        'Response.Write title + "<br/>"
        'Response.Write easy_cont + "<br/>"
        'Response.Write page_type + "<br/>"
        'Response.Write head_flag + "<br/>"
        'Response.Write view_flag + "<br/>"
        'Response.End
        
        
        host = request.ServerVariables("HTTP_HOST")
        addr = "http://www.compassion.or.kr"
        'addr = "http://www.compassionko.org"
        
        '이미지 태그
        '수정 2013-06-04
		If thumbnail_img <> "" Then thumImg = "<img src='"& thumbnail_img &"' border='0'>" End If
        If cont_img <> "" Then contImg = "<img src='"& cont_img &"' border='0'>" End If

	End If
    
Else
	wtype = "write"

    sql = " SELECT "
    sql = sql & " CASE WHEN max(event_num) is not null THEN convert(varchar, max(event_num)+1) " 
    sql = sql & " WHEN max(event_num) is null THEN '1' " 
    sql = sql & " ELSE '1'  END AS maxNum " 
    sql = sql & " FROM [compassweb4].[dbo].[tCampaignEvent] "
    sql = sql & " WHERE 1=1 AND del_flag=0 "

    event_num = ContentViewLoadWS(provWSDL, sql) 

    head_flag = "False"
    view_flag = "False"
End If

%>
<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css" />
<link type="text/css" href="/Web_Backoffice/backoffice/inc/js/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="/Web_Backoffice/backoffice/inc/js/ui/demos.css" rel="stylesheet" />
    <style type="text/css">
    .sbox{border:1px solid #bbb;}
    </style>

<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/js/ui/ui.core.js"></script>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/js/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/js/ui/ui.datepicker-ko.js"></script>

<% '추가 2013-07-17 %>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/tinymce_config.js" charset='euc-kr'></script>
<script type="text/javascript">
    tinymce_config("contents");
</script>

    <script type="text/javascript">
        <!--
        $(function () {
            $('#sdate').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '2009:2020'
            });
        });

        $(function () {
            $('#edate').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '2009:2020'
            });
        });

        $(function () {
            $('#idate').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '2009:2020'
            });
        });


        function doWrite(){
            var f = document.write_f;

            if (f.event_num.value == "") {
                alert("순서를 입력해 주세요.");
                f.event_num.focus();
                return;
            }
		    if (f.title.value == "") {
		        alert("제목을 입력해 주세요.");
		        f.title.focus();
		        return;
		    }
		    if (f.easy_cont.value == "") {
		        alert("간편문구를 입력해 주세요.");
		        f.easy_cont.focus();
		        return;
		    }
            /*
	        if (f.sdate.value == "") {
		        alert("기간 시작일을 입력해 주세요.");
		        f.sdate.focus();
		        return;
	        }
	        if (f.edate.value == "") {
		        alert("기간 종료일을 입력해 주세요.");
		        f.edate.focus();
		        return;
		    }
		    if (f.idate.value == "") {
		        alert("발표일자를 입력해 주세요.");
		        f.idate.focus();
		        return;
		    }
            */
		    if (f.page_type.value == "") {
		        alert("페이지타입 여부를 선택해 주세요.");
		        f.page_type.focus();
		        return;
		    }
		    if (f.head_flag.value == "") {
		        alert("진행/지난 여부를 선택해 주세요.");
		        f.head_flag.focus();
		        return;
		    }
		    if (f.view_flag.value == "") {
		        alert("보기유형 여부를 선택해 주세요.");
		        f.view_flag.focus();
		        return;
		    }
		    if (f.fname1.value == "" && f.thumbnail_img.value == "") {
		        alert("배너 이미지를 선택해 주세요.");
		        f.fname1.focus();
		        return;
		    }
            /*
		    if (f.fname2.value == "" && f.cont_img.value == "") {
		        alert("상세화면 타이틀 이미지를 선택해 주세요.");
		        f.fname2.focus();
		        return;
		    }
            */
		    if (f.fname1.value != "" && f.fname2.value != "" && f.fname1.value == f.fname2.value) {
		        alert("배너 이미지와 파일명이 같습니다. \r\n파일명을 변경하여 주세요.");
		        f.fname2.focus();
		        return;
		    }

		    f.action = "write_proc.asp";
		    f.method = "post";
		    f.target = "_self";

		    if ("<%=wtype%>" == "edit") {
		        if (confirm("수정하시겠습니까?")) {
		            f.submit();
		        }
		        else {
		            return;
		        }
		    }
		    else {
		        if (confirm("등록하시겠습니까?")) {
		            f.submit();
		        }
		        else {
		            return;
		        }
		    }
	    }


        function doDel(){
	        if (confirm("[주의]삭제 하시겠습니까?")) {
		        var f = document.del_f;
		        f.action = "del_proc.asp";
		        f.submit();
	        }
		}

	    //- 숫자만입력
	    function OnlyNumber() {
	        key = event.keyCode;

	        if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
	            event.returnValue = true;
	        }
	        else {
	            event.returnValue = false;
	        }
	    }
        
        //추가 2013-05-24  
	    function pageSelect(val) {

	        var pageType = document.getElementById("page_type");
	        var ctable = document.getElementById("comment_table");
	        var cboard = document.getElementById("comment_board");
	        var comment = document.getElementById("comment");

	        //alert(pageType.value);

	        if (val == "3") {
	            comment.style.display = "block";
	        } else {
	            comment.style.display = "none";

	            ctable.value = "<%=comment_table %>";
	            cboard.value = "<%=comment_board %>";
	        }
	    }
    //-->
    </script>
</head>

<body>
<!--<body leftmargin="0" topmargin="0" oncontextmenu='return false' ondragstart='return false'>-->

<p style="margin:20px;">
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">캠페인&이벤트</font></strong>
</p>

<form name="del_f" id="del_f" method="post">
<input type="hidden" name="idx" value="<%=idx%>" />
<input type="hidden" name="page" value="<%=page%>" />
<input type="hidden" name="startpage" value="<%=startpage%>" />
</form>

<form name="write_f" id="write_f" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="idx" value="<%=idx%>" />
<input type="hidden" name="page" value="<%=page%>" />
<input type="hidden" name="startpage" value="<%=startpage%>" />
<input type="hidden" name="wtype" value="<%=wtype%>" />

<table width="870px" border="0" cellpadding="3" cellspacing="1" bgcolor="#DFDFDF" style="margin-left:20px;">
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">

    <% If CInt(idx) > 0 Then %>
        <input type="button" value="수정" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
        <input type="button" value="삭제" onclick="doDel();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" /> 
    <% Else %>
        <input type="button" value="등록" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    <% End if %>

        <input type="button" value="취소" onclick="location.href='list.asp?page=<%=page%>';" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    </td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="140px"><b>순서번호</b></td>
	<td bgcolor="#FFFFFF" width="730px">
		<input type="text" name="event_num" id="event_num" size="20" maxlength="20" value="<% if event_num <> "" then %><%=event_num%><% else %>1<% end if %>" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="140px"><b>제목</b></td>
	<td bgcolor="#FFFFFF" width="730px">
		<input type="text" name="title" id="title" size="50" value="<%=title%>" style="ime-mode:active;" class="sbox" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="100px"><b>간편문구</b></td>
	<td bgcolor="#FFFFFF" width="770px">
        <textarea name="easy_cont" id="easy_cont" style="width:700px;height:60px;" class="sbox"><%=easy_cont %></textarea>
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>이벤트 시작일</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="sdate" id="sdate" size="20" value="<%=start_date%>" style="cursor:pointer;" class="sbox" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>이벤트 종료일</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="edate" id="edate" size="20" value="<%=end_date%>" style="cursor:pointer;" class="sbox" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>발표일자</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="idate" id="idate" size="20" value="<%=issue_date%>" style="cursor:pointer;" class="sbox" />
	</td>
  </tr>

  <tr>
    <td bgcolor="#F3F3F3"><b>페이지타입 여부</b></td>
	<td bgcolor="#FFFFFF">
		<select id="page_type" name="page_type" class="" style="height:18px;" onchange="pageSelect(this.value);">
            <option value="0" <% if page_type = 0  then %> selected <% end if %>>일반페이지</option>
            <option value="1" <% if page_type = 1  then %> selected <% end if %>>새페이지(빈페이지)</option>
            <option value="2" <% if page_type = 2  then %> selected <% end if %>>링크연결안함</option>
            <option value="3" <% if page_type = 3  then %> selected <% end if %>>댓글게시판</option>
            <option value="4" <% if page_type = 4  then %> selected <% end if %>>로그인여부</option>
        </select>
        (일반페이지 기본값)

        <div id="comment" style="display:none;">
		    게시판번호: <input type="text" name="comment_table" id="comment_table" size="20" value="<%=comment_table%>" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" />
            &nbsp;&nbsp;
            글번호: <input type="text" name="comment_board" id="comment_board" size="20" value="<%=comment_board%>" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" />
        </div>
	</td>
  </tr>

  <tr>
    <td bgcolor="#F3F3F3"><b>진행/지난 여부</b></td>
	<td bgcolor="#FFFFFF">
		<input type="radio" name="head_flag" id="head_flag1" value="0" <% if head_flag = "False" then %> checked <% end if %> /> 지난이벤트 &nbsp;&nbsp;
        <input type="radio" name="head_flag" id="head_flag2" value="1" <% if head_flag = "True" then %> checked <% end if %> /> 진행이벤트
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>보기유형 여부</b></td>
	<td bgcolor="#FFFFFF">
        <input type="radio" name="view_flag" id="view_flag1" value="0" <% if view_flag = "False" then %> checked <% end if %> /> 보이기 &nbsp;&nbsp;
		<input type="radio" name="view_flag" id="view_flag2" value="1" <% if view_flag = "True" then %> checked <% end if %> /> 보이지 않기 &nbsp;&nbsp;
	</td>
  </tr>

  <tr>
    <td bgcolor="#F3F3F3"><b>상세내용</b></td>
	<td bgcolor="#FFFFFF">
		<!--<textarea name="contents" id="contents" style="width:700px;height:170px;" class="sbox"><%'=contents %></textarea>-->
        <% '수정 2013-06-19 %>
        <div>
	        <textarea name="contents" id="contents" style="width:600px;height:170px;" class="sbox"><%=contents %></textarea>
        </div>
        <!--<div><button onClick="addArea2();">Editor</button> <button onClick="removeArea2();">Html</button></div>-->
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>배너 이미지</b></td>
	<td bgcolor="#FFFFFF">
		<%=thumImg%><br />
        <%=thumbnail_img%><br />
        <input type="file" name="fname1" id="fname1" size="50" style="ime-mode:disabled;" class="sbox" />
        <input type="hidden" name="thumbnail_img" id="thumbnail_img" value="<%=thumbnail_img%>" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>상세화면 내용<br />이미지</b></td>
	<td bgcolor="#FFFFFF">
		<%=contImg%><br />
        <%=cont_img%><br />
        <input type="file" name="fname2" id="fname2" size="50" style="ime-mode:disabled;" class="sbox" />
        <input type="hidden" name="cont_img" id="cont_img" value="<%=cont_img%>" />
	</td>
  </tr>

  <tr>
    <td colspan="2" bgcolor="#FFFFFF">

    <% If CInt(idx) > 0 Then %>
        <input type="button" value="수정" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
        <input type="button" value="삭제" onclick="doDel();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" /> 
    <% Else %>
        <input type="button" value="등록" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    <% End if %>

        <input type="button" value="취소" onclick="location.href='list.asp?page=<%=page%>';" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    </td>
  </tr>
</table>
</form>

    <script type="text/javascript">
    <!--
        pageSelect("<%=page_type %>");
        document.write_f.title.focus();
    //-->
    </script>

    <% '추가 2013-06-19 %>
    <% '주석처리 2013-07-17 %>
    <!--<script src="/Web_Backoffice/backoffice/inc/js/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        var area1, area2;
        /*
        function toggleArea1() {
        if (!area1) {
        area1 = new nicEditor({ fullPanel: true }).panelInstance('myArea1', { hasPanel: true });
        } else {
        area1.removeInstance('myArea1');
        area1 = null;
        }
        }
        */

        function addArea2() {
            area2 = new nicEditor({ fullPanel: true }).panelInstance('contents');
        }
        function removeArea2() {
            area2.removeInstance('contents');
        }

        bkLib.onDomLoaded(function () { addArea2(); });
    </script>-->
</body>
</html>