﻿
<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  'if request("page") = "" then
    'page = 1
  'else
    'page=request("page")
  'end if
  
  'if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   'startpage=1
  'else
   'startpage = request("startpage")
  'end if  

    If request.QueryString("key") = "" Then 
    Else
        keyfield = request.QueryString("keyfield")	
        key = request.QueryString("key")
    End if 
  
    '추가 2013-05-06
    nowDate = Now()
    nDate = Left(nowDate, 10)
    
    sYear = Left(nowDate, 4) - 1
    sMonth = Mid(nowDate, 6, 2)
    sDay = Mid(nowDate, 9, 2)
                                
    if request("sdate") = "" then 
        'sdate = sYear & "-" & sMonth & "-" & sDay & " 00:00:00"
        sdate = sYear & "-" & sMonth & "-" & sDay
    else
        sdate = request("sdate")
    end if

    if request("edate") = "" then 
        'edate = nDate & " 23:59:59"
        edate = nDate
    else
        edate = request("edate")
    end if
%>


<%
	'If session("admin_id")="" Then
		
			'response.write "<script>alert('로그인 후 이용해주세요.'); top.location.href='/admin/login.asp';</script>"
		
	'response.end
	'End If
	
%>

<%
	Call setXls()
	Sub setXls()
		response.write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>")
	    '수정 ) 파일명 깨짐
        xls_filename = Server.URLEncode("캠페인_이벤트")
        'xls_filename = "캠페인_이벤트"
		Response.ContentType  = "application/x-excel"
	    Response.CacheControl  = "public" 
		Response.AddHeader  "Content-Disposition" , "attachment; filename="& xls_filename & ".xls" 	

	
	End Sub
%>

<html>
<head>
<title>관리자화면</title>
<!--link rel="stylesheet" type="text/css" href="../index.css"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
	<table cellpadding="1" cellspacing="1" border="0" width="100%">
		<tr align="center" bgcolor="#3399CC" height="30px" style="font-weight:bold; color:#FFFFFF;">
            <td width="8%" height="30px" align="center">번호</td>
            <td width="20%" align="center">메인배너</td>
            <td width="20%" align="center">이벤트명</td>
            <td width="19%" align="center">기간</td>
            <td width="15%" align="center">발표일자</td>
            <td width="10%" align="center">등록일</td>
            <td width="8%" align="center">순서</td>
		</tr>

		<%
            sql = " SELECT "
            sql = sql & " idx, event_num, title, easy_cont, contents, thumbnail_img, cont_img, " 
            sql = sql & " page_type, head_flag, view_flag, del_flag, " 
            sql = sql & " [start_date], end_date, issue_date, "
            sql = sql & " convert(varchar(20),reg_date,120) as reg_date, "
            sql = sql & " CommentTable, CommentBoard "
            sql = sql & " FROM [compassweb4].[dbo].[tCampaignEvent] "
            sql = sql & " WHERE 1=1 AND del_flag=0 "
            sql = sql & " AND reg_date between '"& sdate &" 00:00:00' AND '"& edate &" 23:59:59' "

            if key = "" then
	            sql = sql & " order by reg_date desc "
            else
	            sql = sql & " and "& keyfield &" like '%"& key &"%' order by reg_date desc "
            end if

            'set rs = server.CreateObject("adodb.recordset")
            'rs.PageSize=10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.
            'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
            set rs = RecordsetFromWS(provWSDL, sql)
		%>
		<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면%>
		<tr> 
			<td height="22" colspan="7" align="center"><b>글이 없습니다!!!</b></td>
		</tr>
		<% else '데이터가 있다면
   
			'totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
			'rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
			'endpage = startpage + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
			'if endpage > rs.PageCount then
				'endpage = rs.PageCount
			'end If


			'if request("page")="" then
				'npage=1
			'else
				'npage=cint(request("page"))
			'end if
						
			j = rs.recordcount
		%>
		<%
				i=1
				do until rs.EOF
		%>
        <tr bgcolor="#FFFFFF" style="border-bottom:1px solid #ccc;" height="30px">
            <td align="center"><%=j%></td>
            <td align="center"><%=rs("thumbnail_img") %></td>
			<td align="center"><%=rs("title") %></td>

            <td align="center">
                <% if rs("start_date") <> "" AND rs("end_date") <> "" then %>
                    <%=rs("start_date") %><br />~<br /><%=rs("end_date") %>
                <% else %>
                    <% if rs("start_date") <> "" AND rs("end_date") = "" then %>
                        <%=rs("start_date") %>
                    <% else %>
                        -
                    <% end if %>
                <% end if %>
            </td>
                      
            <td align="center">
                <% if rs("issue_date") <> "" then %>
                    <%=rs("issue_date") %>
                <% else %>
                    -
                <% end if %>
            </td>

            <!-- left 사용시 변함 --> <% '=left(rs("reg_date"),10)%>
            <td align="center"><%=left(rs("reg_date"),10) %></td>
            <td align="center"><%=rs("event_num") %></td>
        </tr>

        <% '주석처리 2013-05-20 %>
		<!--<tr>
			<td colspan="8" bgcolor="#cccccc" height="1"></td>
		</tr>-->
		<%									
			    j = j - 1
			    rs.movenext
            	i = i + 1
			Loop 				
		%>
		<%	
			End If 
			rs.close
			Set rs=Nothing 
		%>
	</table>
</body>
</html>