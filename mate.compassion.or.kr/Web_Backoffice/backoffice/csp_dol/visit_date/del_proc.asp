﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
'*******************************************************************************
'페이지 설명 : CSP 돌맞이 방문일자 관리
'작성일      : 2013-10-15 김선오
'수정일      : 2013-11-05 김선오
'참고사항    :
'*******************************************************************************
%>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%

if request("idx") = "" then
    idx = 0
else 
    idx = request("idx")
end if

page = Request("page")
startpage = Request("startpage")


link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"

if CInt(idx) > 0 then 

    '추가 2013-11-05  방문일을 사용중일때
    nsql = "select count(*) from [compassweb4].[dbo].[tDolVisitSelectDate] where 1=1 "
    nsql = nsql & " and UseYN = 'Y' "
    nsql = nsql & " and Idx = " & idx
    Set record = RecordsetFromWS(provWSDL, nsql)
    
    response.write record(0)    
    if record(0) > 0 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('사용중이므로 삭제할 수 없습니다.'); </script>"
        response.write link
        response.end
    end if

    '수정 2013-11-05 - 쿼리문 수정
    sql = " UPDATE [compassweb4].[dbo].[tDolVisitSelectDate] SET "
    sql = sql & " DeleteYN = 'Y' "
    sql = sql & " WHERE idx = " & idx

    inresult = BoardWriteWS(provWSDL, sql)

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('삭제되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('삭제중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if
else 
    response.write "<script type='text/javascript' charset='euc-kr'> alert('글이 없어 삭제할 수 없습니다.'); </script>"
    response.write link
end if 
%>

</html>
