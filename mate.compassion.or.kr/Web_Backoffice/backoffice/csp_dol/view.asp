﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../index.css">
</head>

<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
provWSDL = session("serviceUrl")


idx = request("idx")

page = Request("page")
startpage = Request("startpage")

keyfield = request("keyfield")	
key = request("key")

sdate = request("sdate")
edate = request("edate")
%>

<%
    sql = " SELECT "
    sql = sql & " Idx, Gubun, SponsorID, SponsorName, PayAmount, BabyName, BabyGender, BabyBirth, " 
    sql = sql & " BabyRelation, BabyImage, BabyCertificate, Zip1, Zip2, Address1, Address2, " 
    sql = sql & " Email, Tel, VisitIdx, VisitDate, PaymentYN, CancelYN, "
    sql = sql & " convert(varchar(20),RegDate,120) as RegDate "
    sql = sql & " FROM [compassweb4].[dbo].[tCspDol] "
    sql = sql & " WHERE 1=1 "
    sql = sql & " AND Idx = " & idx

    Set rs = RecordsetFromWS(provWSDL, sql)

    '변수에 값들을 저장
    idx = rs("Idx")
    gubun = rs("Gubun")
    sponsorID = rs("SponsorID")
    sponsorName = rs("SponsorName")
    payAmount = rs("PayAmount")
    babyName = rs("BabyName")
    babyGender = rs("BabyGender")
    babyBirth = rs("BabyBirth")
    babyRelation = rs("BabyRelation")
    babyImage = rs("BabyImage")
    zip1 = rs("Zip1")
    zip2 = rs("Zip2")
    address1 = rs("Address1")
    address2 = rs("Address2")
    email = rs("Email")
    tel = rs("Tel")
    visitIdx = rs("VisitIdx")
    visitDate = rs("VisitDate")
    paymentYN = rs("PaymentYN")
    cancelYN = rs("CancelYN")
    regDate = rs("RegDate")

    '도메인 주소
    domain_addr = request.ServerVariables("HTTP_HOST")


    sqlcon = "SELECT BabyCertificate FROM [compassweb4].[dbo].[tCspDol] WHERE 1=1 "
    sqlcon = sqlcon & " AND Idx = " & idx
    BabyCertificate = ContentViewLoadWS(provWSDL, sqlcon)

    'Response.Write(sqlcon)
    'Response.End()

    'BabyCertificate = replace(rs("BabyCertificate"), chr(13) & chr(10), "<br>")
    BabyCertificate = replace(BabyCertificate, chr(10), "<br/>")
    BabyCertificate = replace(BabyCertificate, chr(13), "<br/>")
    BabyCertificate = replace(BabyCertificate, "&lt;", "<")
    BabyCertificate = replace(BabyCertificate, "&gt;", ">")
    BabyCertificate = replace(BabyCertificate, "&amp;", "&")
    BabyCertificate = replace(BabyCertificate, "&apos;", "'")
    BabyCertificate = replace(BabyCertificate, "&quot;", """")
%>  


<body leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30">&nbsp;</td>
          <td width="650"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="40%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="22"><img src="../img/icon01.gif" width="22" height="18"></td>
                      <td><strong><font color="#3788D9">CSP 돌맞이 후원신청 뷰어</font></strong></td>
                    </tr>
                </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>

              <tr> 
                <td><table width="800px" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">

					<tr bgcolor="#F3F3F3"> 
                      <td width="120px" height="35" align="center" bgcolor="#F3F3F3"><b>구분</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;" colspan="3">
                      <% if gubun = "P" then %>온라인 나눔
                      <% elseif gubun = "V" then %>컴패션 방문
                      <% end if %>
                      </td>
                    </tr>

					<tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>후원자ID</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;" colspan="3"><%=sponsorID %> </td>
                    </tr>

					<tr bgcolor="#F3F3F3"> 
                      <td width="120px" height="35" align="center" bgcolor="#F3F3F3"><b>신청자명</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;" width="310px"><%=sponsorName %></td>

                      <td width="120px" height="35" align="center" bgcolor="#F3F3F3"><b>아기 이름</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;" width="310px"><%=babyName %></td>
                    </tr>

					<tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>신청금액</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;"><%=payAmount %></td>

                      <td height="35" align="center" bgcolor="#F3F3F3"><b>아기 성별</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;">
                      <% if babyGender = "M" then %>아들
                      <% elseif babyGender = "W" then%>딸
                      <% end if %>
                      </td>
                    </tr>
                           
                    <tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>이메일</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;"><%=email %></td>

                      <td height="35" align="center" bgcolor="#F3F3F3"><b>아기 생년월일</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;"><%=babyBirth %></td>
                    </tr>

                    <tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>연락처</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;"><%=tel %></td>

                      <td height="35" align="center" bgcolor="#F3F3F3"><b>아기와의 관계</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;"><%=babyRelation %></td>
                    </tr>

                    <tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>배송 주소</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;" colspan="3">
                      <% if address2 <> "" then %>
                          우편번호 : <%=zip1 %>-<%=zip2 %><br />
                          주소 : <%=address1 %> <%=address2 %>
                      <% end if %>
                      </td>
                    </tr>

                    <tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>아기사진 첨부</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;" colspan="3">
                      <%
                        host = ""
                        if domain_addr = "admin.compassion.or.kr" then
                            host = "http://ws.compassion.or.kr"
                        else
                            host = "http://ws.compassionko.org"
                        end if
                      %>

                      <% if Trim(babyImage) <> "" then %>첨부파일 : 
                        <a href='../inc/e_download.asp?filePath=/Files/CSP/BabyImg/&fileName=<%=babyImage %>'><%=babyImage %></a>
                        <br /><br />
                        <%
                            img_url = host & "/Files/CSP/BabyImg/" & babyImage
                        %>
                        <img src='<%=img_url %>' border="0" />
                      <% end if %>
                      </td>
                    </tr>

                    <tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>결제상태</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;" colspan="3">
                      <% if paymentYN = "Y" then %>결제완료
                      <% else %>-
                      <% end if %>
                      </td>
                    </tr>

                    <tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>방문일자</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;" colspan="3"><%=visitDate %></td>
                    </tr>
                    
                    <tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>신청상태</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;" colspan="3">
                      <% if cancelYN = "N" then %>신청됨
                      <% else %>취소됨
                      <% end if %>
                      </td>
                    </tr>

                    <tr bgcolor="#F3F3F3"> 
                      <td height="35" align="center" bgcolor="#F3F3F3"><b>등록일</b></td>
                      <td bgcolor="#FFFFFF" style="padding:10px;" colspan="3"><%=regDate %></td>
                    </tr>

                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>

              <tr> 
                <td><table width="800px" border="0" cellspacing="1" cellpadding="1">
                    <tr>
					<td align="right"><a href="list.asp?page=<%=page%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>&sdate=<%=sdate %>&edate=<%=edate %>"><img src="../img/btn16.gif" width="42" height="20" border="0"></a></td>
                    </tr>
                </table></td>
              </tr>     	
			</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>