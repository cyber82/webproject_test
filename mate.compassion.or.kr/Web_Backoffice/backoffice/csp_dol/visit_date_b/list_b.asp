﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css" />
    <style type="text/css">
    .sbox{border:1px solid #bbb;}
    </style>

<link type="text/css" href="../../inc/js/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../inc/js/ui/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../../inc/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../inc/js/ui/ui.core.js"></script>
<script type="text/javascript" src="../../inc/js/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../inc/js/ui/ui.datepicker-ko.js"></script>

</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
    if request("page") = "" then
        page = 1
    else
        page=request("page")
    end if
  
    if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
        startpage=1
    else
        startpage = request("startpage")
    end if  

	keyfield = request("keyfield")	
	key = request("key")	


    nowDate = Now()
    nDate = Left(nowDate, 10)
    
    sYear = Left(nowDate, 4) - 1
    sMonth = Mid(nowDate, 6, 2)
    sDay = Mid(nowDate, 9, 2)
                                
    if request("vdate") = "" then 
        sdate = sYear & "-" & sMonth & "-" & sDay
    else
        sdate = request("vdate")
    end if

%>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<body leftmargin="0" topmargin="0">

<script type="text/javascript" charset='euc-kr'>

    //주석처리 2013-11-05
    /*$(function () {
        $('#vdate').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    $(function () {
        $('#edit_vdate').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });*/

    function trColor(sw, idx) {
        var id = document.getElementById("dataTr_" + idx);

        if (sw == "1") {
            id.style.backgroundColor = "#ddd";
        }
        else if (sw == "0") {
            id.style.backgroundColor = "#fff";
        }
    }

    //- 숫자만입력
    function OnlyNumber() {
        key = event.keyCode;

        if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
            event.returnValue = true;
        }
        else {
            event.returnValue = false;
        }
    }

    //등록, 수정
    function writePage(num, wtype) {

        var f = document.searchForm;

        if (wtype == "write") {
            if (f.vdate.value == "") {
                alert("방문일자를 입력해 주세요.");
                f.vdate.focus();
                return;
            }
            else if (f.vorder.value == "") {
                alert("방문일자순서를 입력해 주세요.");
                f.vorder.focus();
                return;
            }
        }
        else if (wtype == "edit") {
            if (f.edit_vdate.value == "") {
                alert("방문일자를 입력해 주세요.");
                f.edit_vdate.focus();
                return;
            }
            else if (f.edit_vorder.value == "") {
                alert("방문일자순서를딘 입력해 주세요.");
                f.edit_vorder.focus();
                return;
            }
        }

        var idx = document.getElementById("idx");
        var wtypeID = document.getElementById("wtype");
        wtypeID.value = wtype;

        if (Number(num) > 0) {
            //idx.value = num;
        }
        else {
            idx.value = "";
        }

        document.searchForm.action = "write_proc_b.asp";
        document.searchForm.method = "post";
        document.searchForm.target = "_self";

        if (wtype == "edit") {
            if (confirm("수정하시겠습니까?")) {
                document.searchForm.submit();
            } else {
                return;
            }
        }
        else {
            document.searchForm.submit();
        }
    }

    //삭제
    function delPage(num) {

        var idx = document.getElementById("idx");

        if (Number(num) > 0) {
            idx.value = num;
        }
        else {
            idx.value = "";
        }

        document.searchForm.action = "del_proc_b.asp";
        document.searchForm.method = "post";
        document.searchForm.target = "_self";

        if (confirm("삭제하시겠습니까?")) {
            document.searchForm.submit();
        } else {
            return;
        }
    }

    //버튼 체인지
    function divChange(flag, num) {

        var div_btn1 = document.getElementById("div_btn1");
        var div_btn2 = document.getElementById("div_btn2");
        var idx = document.getElementById("idx");

        var edit_vdate = document.getElementById("edit_vdate_" + num);
        var edit_vorder = document.getElementById("edit_vorder_" + num);
        var svdate = document.getElementById("edit_vdate");
        var svorder = document.getElementById("edit_vorder");

        if (flag == true) {
            div_btn1.style.display = "none";
            div_btn2.style.display = "block";

            svdate.value = edit_vdate.value;
            svorder.value = edit_vorder.value;
            idx.value = num;
        }
        else {
            div_btn1.style.display = "block";
            div_btn2.style.display = "none";
            idx.value = "";
        }
    }

//-->
</script>
<form name="searchForm" action="list_s.asp" method="post">	
<input type="hidden" name="idx" id="idx" value="" />
<input type="hidden" name="page" id="page" value="<%=page%>" />
<input type="hidden" name="startpage" id="startpage" value="<%=startpage%>" />
<input type="hidden" name="wtype" id="wtype" value="" />

<input type="hidden" name="hd_vdate" id="hd_vdate" value="" />
<input type="hidden" name="hd_vorder" id="hd_vorder" value="" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30px">&nbsp;</td>
          <td width="900px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                        <td width="22px"><img src="../../img/icon01.gif" width="22" height="18"></td>
                        <td><strong><font color="#3788D9">CSP 돌맞이 방문일자 목록</font></strong></td>
                    </tr>
                </table></td>
              </tr>

              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <!--<tr>
                        <td width="80px"><font color="#3788D9">방문일자 추가</font></td>
                    </tr>-->

                    <tr>
                        <td>
            <%
                '마지막 순서번호
                sqlCnt = " SELECT case when MAX(Idx) is not null then MAX(Idx) "
                sqlCnt = sqlCnt & " else 0 end as num "
                sqlCnt = sqlCnt & " FROM [compassweb4].[dbo].[tDolVisitSelectDate] "
                sqlCnt = sqlCnt & " WHERE 1=1 "

                Set rsCnt = RecordsetFromWS2(provWSDL, sqlCnt)

                num = CInt(rsCnt("num")) + 1
            %>
                        <div id="div_btn1" style="display:block;">
                            방문일자: <input type="text" name="vdate" id="vdate" size="25" value="<%=vdate %>" style="ime-mode:active;cursor:pointer;" class="sbox" /> 
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            방문일자순서: <input type="text" name="vorder" id="vorder" size="5" value="<%=num %>" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" />
			                
                            <input type="button" value="추가" onclick='javascript:writePage(0, "write");' style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">
                        </div>

                        <div id="div_btn2" style="display:none;">
                            방문일자: <input type="text" name="edit_vdate" id="edit_vdate" size="25" value="" style="ime-mode:active;cursor:pointer;" class="sbox" /> 
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            방문일자순서: <input type="text" name="edit_vorder" id="edit_vorder" size="5" value="" style="ime-mode:disabled;" class="sbox" onkeydown="javascript:OnlyNumber();" />
			                
                            <input type="button" value="수정" onclick='javascript:writePage(1, "edit");' style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">
                            <input type="button" value="취소" onclick='javascript:divChange(false, "");' style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;">
                        </div>

            <% 
         	    rsCnt.Close
	            Set rsCnt = nothing 
            %>
		                </td> 
                    </tr>
                </table></td>
              </tr>

              <tr height="10px"><td>&nbsp;</td></tr>

              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                        <td width="8%" height="30px" align="center">번호</td>
                        <td width="32%" align="center">방문일자</td>
                        <td width="15%" align="center">방문일자순서</td>
                        <td width="10%" align="center">사용여부</td>
                        <td width="15%" align="center">신청번호</td>
                        <td width="20%" align="center">버튼</td>
                    </tr>


            <!------- 게시판 목록 구하기 시작 ----------> 
            <%
                sql = " SELECT "
                sql = sql & " vsd.Idx, vsd.VisitDate, vsd.VisitOrder, vsd.UseYN, vsd.DeleteYN, cd.Idx as DolIdx " 
                sql = sql & " FROM [compassweb4].[dbo].[tDolVisitSelectDate] vsd "
                sql = sql & " LEFT OUTER JOIN [compassweb4].[dbo].[tCspDol] cd ON vsd.Idx = cd.VisitIdx "
                sql = sql & " WHERE 1=1 "
                sql = sql & " AND DeleteYN = 'N' "
                sql = sql & " AND vsd.Region = 'B' "
                sql = sql & " ORDER BY vsd.VisitDate DESC "
                'sql = sql & " ORDER BY vsd.VisitOrder ASC "

                'if key = "" then
	            '    sql = sql & " order by VisitDate desc "
                'else
	            '    sql = sql & " and "& keyfield &" like '%"& key &"%' order by VisitDate desc "
                'end if


                Set rs = RecordsetFromWS(provWSDL, sql)
            %>					
					
				<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
					<tr> 
					  <td height="22" colspan="6" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
				<% else '데이터가 있다면
                    
                    rs.PageSize = 10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

					totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.

					rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
					endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 

                    if endpage > totalpage then
						endpage = totalpage
					end If
                    
					if request("page")="" then
						npage = 1
					else
						npage = CInt(request("page"))
					end if
						
					trd = rs.RecordCount
					j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)
				%>
				<%
                    host = request.ServerVariables("HTTP_HOST")
                    'response.write host
                    'response.end

				    i = 1
					do until rs.EOF or i > rs.PageSize
				%>
                    <tr id="dataTr_<%=rs("Idx") %>" bgcolor="#FFFFFF" onmouseover='javascript:trColor(1, "<%=rs("Idx") %>");' onmouseout='javascript:trColor(0, "<%=rs("Idx") %>");'> 
                      <td height="25" align="center"><%=j%></td>
                      
                      <!-- left 사용시 변함 --> <% '=left(rs("VisitDate"),10)%>
                      <td align="center">
                        <%=rs("VisitDate") %>
                        <input type="hidden" name="edit_vdate_<%=rs("Idx") %>" id="edit_vdate_<%=rs("Idx") %>" value="<%=rs("VisitDate") %>" />
                      </td>
					  <td align="center">
                        <%=rs("VisitOrder") %>
                        <input type="hidden" name="edit_vorder_<%=rs("Idx") %>" id="edit_vorder_<%=rs("Idx") %>" value="<%=rs("VisitOrder") %>"/>
                      </td>
                      <td align="center"><%=rs("UseYN") %></td>
                      <td align="center"><%=rs("DolIdx") %></td>

                      <td align="center">
                        <a href='javascript:divChange(true, "<%=rs("Idx") %>");' target="_self">[수정하기]</a>
                        &nbsp;&nbsp;
                        <a href='javascript:delPage("<%=rs("Idx") %>");' target="_self">[삭제하기]</a>
                      </td>
                    </tr>

            <%
                'Response.Write("j : " & j & "<br/>")
                'Response.Write("i : " & i & "<br/>")

                j=j-1
				rs.MoveNext ' 다음 레코드로 이동한다.
			    i=i+1
				loop '레코드의 끝까지 loop를 돈다.
			%> 
            <% 
         	    rs.Close
	            Set rs = nothing 

                end if
            %>               
            <!------- 게시판 목록 구하기 끝 ---------->  
                                    
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <!--<tr>
                <td align="right"><a href="write.asp?page=<%'=page%>" onfocus="this.blur()"><img src="../img/btn17.gif" width="52" height="20" border="0"></a></td>
              </tr>-->
              <tr> 
                <td align="center">
					<% if cint(startpage)<>cint(1) then%> 
				         <a href="list_b.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [이전]</a>
			        <%end if%> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then%>
				         [<%=page%>]
		             <%else%>
					     <a href="list_b.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()"> <%=i%> </a>
		             <%end if%>
				   <% next%>

				   <% if cint(endpage)<>cint(totalpage) then%>
			             <a href="list_b.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>" onfocus="this.blur()">
			             [다음]</a>
			       <% end if%>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
			  
              <!--<tr> 
                <td align="center"><table width="30%" border="0" cellspacing="2" cellpadding="0">
                    <tr> 
                      <td>
                        <select name="keyfield" style="height:19px;">
                            <option value="title">이벤트명</option>
                        </select>
                      </td>
                      <td><input name="key" type="text" class="text"></td>
                      <td><input type="submit" name="Submit2" value="찾기"></td>
                    </tr>
                 </table></td>
              </tr>-->
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</form>

</body>
</html>
