<% @CODEPAGE = 65001 %>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<%
'*******************************************************************************
'페이지 설명 : CSP 돌맞이 방문일자 관리
'작성일      : 2013-10-14 김선오
'수정일      : 2013-11-05 김선오
'참고사항    :
'*******************************************************************************
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%

    wtype = Request.Form("wtype") '등록, 수정 구분
    page = Request.Form("page")
    startpage = Request.Form("startpage")
    idx = Request.Form("idx")
    
    vdate = Request.Form("vdate")
    vorder = Request.Form("vorder")

    edit_vdate = Request.Form("edit_vdate")
    edit_vorder = Request.Form("edit_vorder")

    'if vdate <> "" then
        'vdate = vdate & " 00:00:00"
    'end if

    'if edit_vdate <> "" then
        'edit_vdate = edit_vdate & " 00:00:00"
    'end if

    'response.write "wtype: " & wtype & "<br/>"
    'response.write "page: " & page & "<br/>"
    'response.write "startpage: " & startpage & "<br/>"
    'response.write "idx: " & idx & "<br/>"
    'response.write "vdate: " & vdate & "<br/>"
    'response.write "vorder: " & vorder & "<br/>"
    'response.End


    link = "<script type='text/javascript' charset='euc-kr'>location.href='list_b.asp?page=" & page & "&startpage=" & startpage & "';</script>"


'등록
if wtype = "write" then

    nsql="select count(*) from [compassweb4].[dbo].[tDolVisitSelectDate] where 1=1 and convert(varchar(20),VisitDate,120) = '"& vdate &"' "
    Set record = RecordsetFromWS(provWSDL, nsql)
    
    'response.write "<script type='text/javascript' charset='euc-kr'> alert('"& record(0) &"'); </script>"
    'response.End

    if record(0) = 0 then

        sql = " INSERT INTO [compassweb4].[dbo].[tDolVisitSelectDate] ("
        sql = sql & " VisitDate, VisitOrder, Region "
        sql = sql & " ) VALUES ( "
        sql = sql & " '"& vdate &"', '"& vorder &"', 'B' "
        sql = sql & " ) "
                
        inresult = BoardWriteWS(provWSDL, sql)

        if inresult = 10 then
            response.write "<script type='text/javascript' charset='euc-kr'> alert('등록되었습니다.'); </script>"
            response.write link
        else
            response.write "<script type='text/javascript' charset='euc-kr'> alert('등록중 오류가 발생하였습니다.'); </script>"
            response.write link
        end if

    else
        
        response.write "<script type='text/javascript' charset='euc-kr'> alert('이미 등록 되었습니다.'); </script>"
        response.write link
    end if



'수정
Else 
    if wtype = "edit" then

        '추가 2013-11-05  방문일을 사용중일때
        nsql = "select count(*) from [compassweb4].[dbo].[tDolVisitSelectDate] where 1=1 "
        nsql = nsql & " and VisitDate = '"& edit_vdate &"' "
        nsql = nsql & " and UseYN = 'Y' "
        nsql = nsql & " and Idx = " & idx
        Set record = RecordsetFromWS(provWSDL, nsql)
        
        if record(0) > 0 then
            response.write "<script type='text/javascript' charset='euc-kr'> alert('사용중이므로 수정할 수 없습니다.'); </script>"
            response.write link
            response.end
        end if
        
        '수정 2013-11-05 - 쿼리문 수정
        nsql = "select count(*) from [compassweb4].[dbo].[tDolVisitSelectDate] where 1=1 "
        'nsql = nsql & " and VisitDate = '"& edit_vdate &"' "
        nsql = nsql & " and Idx = " & idx

        Set record = RecordsetFromWS(provWSDL, nsql)

        if record(0) > 0 then
            sql = " UPDATE [compassweb4].[dbo].[tDolVisitSelectDate] SET "
            sql = sql & "VisitDate='"& edit_vdate &"', VisitOrder='"& edit_vorder &"' "
            sql = sql & " WHERE Idx = " & idx

            inresult = BoardWriteWS(provWSDL, sql)

            if inresult = 10 then
                response.write "<script type='text/javascript' charset='euc-kr'> alert('수정되었습니다.'); </script>"
                response.write link
            else
                response.write "<script type='text/javascript' charset='euc-kr'> alert('수정중 오류가 발생하였습니다.'); </script>"
                response.write link
            end if
        else
        
            response.write "<script type='text/javascript' charset='euc-kr'> alert('이미 방문일자가 있습니다.'); </script>"
            response.write link
        end if

    end if

End If

%>