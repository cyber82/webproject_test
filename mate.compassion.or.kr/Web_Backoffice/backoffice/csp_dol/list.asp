﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css" />
    <style type="text/css">
    .sbox{border:1px solid #bbb;}
    </style>

<link type="text/css" href="../inc/js/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../inc/js/ui/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../inc/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.core.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../inc/js/ui/ui.datepicker-ko.js"></script>

</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
    if request("page") = "" then
        page = 1
    else
        page=request("page")
    end if
  
    if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
        startpage=1
    else
        startpage = request("startpage")
    end if  

	keyfield = request("keyfield")	
	key = request("key")	


    nowDate = Now()
    nDate = Left(nowDate, 10)
    
    sYear = Left(nowDate, 4) - 1
    sMonth = Mid(nowDate, 6, 2)
    sDay = Mid(nowDate, 9, 2)
                                
    if request("sdate") = "" then 
        sdate = sYear & "-" & sMonth & "-" & sDay
    else
        sdate = request("sdate")
    end if

    if request("edate") = "" then 
        edate = nDate
    else
        edate = request("edate")
    end if

    Region = request("Region")
%>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<body leftmargin="0" topmargin="0">

<script type="text/javascript" charset='euc-kr'>

    $(function () {
        $('#sdate').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    $(function () {
        $('#edate').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2009:2020'
        });
    });

    function trColor(sw, idx) {
        var id = document.getElementById("dataTr_" + idx);

        if (sw == "1") {
            id.style.backgroundColor = "#ddd";
        }
        else if (sw == "0") {
            id.style.backgroundColor = "#fff";
        }
    }

    //- 숫자만입력
    function OnlyNumber() {
        key = event.keyCode;

        if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
            event.returnValue = true;
        }
        else {
            event.returnValue = false;
        }
    }

    //주석처리 2013-11-05
    /*function writePage(num, wtype) {

        var idx = document.getElementById("idx");

        if (Number(num) > 0) {
            idx.value = num;
        }
        else {
            idx.value = "";
        }
        
        document.searchForm.action = "write.asp";
        document.searchForm.method = "post";
        document.searchForm.target = "_self";

        //if (wtype == "edit") {
        //    if (confirm("수정하시겠습니까?")) {
        //        document.searchForm.submit();
        //    } else {
        //        return;
        //    }
        //}
        //else {
            document.searchForm.submit();
        //}
    }*/

    //결제여부, 취소여부 실행
    //수정 2013-11-05, 2013-11-14
    function PaymentORCancel(num, chk, type, gubun) {

        var idx = document.getElementById("idx");
        var cancelYN = document.getElementById("cancelYN");
        var paymentYN = document.getElementById("paymentYN");
        var gubunCtrl = document.getElementById("gubun");

        if (Number(num) > 0) {
            idx.value = num;
        }
        else {
            idx.value = "";
        }

        gubunCtrl.value = gubun;

        var str = "";

        //수정 2013-11-14
        if (type == "cancel") {
            cancelYN.value = chk;
            document.searchForm.action = "cancel_proc.asp?Region=<%=Region%>";

            if (chk == "Y") str = "취소";
            else if (chk == "N") str = "신청";
        }
        else if (type == "payment") {
            paymentYN.value = chk;
            document.searchForm.action = "payment_proc.asp?Region=<%=Region%>";

            if (chk == "Y") str = "결제";
            else if (chk == "N") str = "취소";
        }

        document.searchForm.method = "post";
        document.searchForm.target = "_self";

        if (confirm(str + "하시겠습니까?")) {
            document.searchForm.submit();
        } else {
            return;
        }
    }
//-->
</script>
<form name="searchForm" action="list.asp?Region=<%=Region%>" method="post">	
<input type="hidden" name="idx" id="idx" />
<input type="hidden" name="cancelYN" id="cancelYN" />
<input type="hidden" name="paymentYN" id="paymentYN" />
<input type="hidden" name="gubun" id="gubun" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30px">&nbsp;</td>
          <td width="900px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                        <td width="22px"><img src="../img/icon01.gif" width="22" height="18"></td>
                        <td><strong><font color="#3788D9">CSP 돌맞이 후원신청 목록</font></strong></td>
                        <td align="right">
                            <a href="excel.asp?Region=<%=Region%>&keyfield=<%=keyfield%>&key=<%=key%>&sdate=<%=sdate %>&edate=<%=edate %>"><strong><font color="#3788D9">엑셀 다운로드</font></strong></a>
                        </td>
                    </tr>
                </table></td>
              </tr>

              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="80px"><font color="#3788D9">등록기간검색</font></td>
                        <td>
                            <input type="text" name="sdate" id="sdate" size="10" value="<%=sdate %>" style="ime-mode:disabled;cursor:pointer;" class="sbox" onkeydown="javascript:OnlyNumber();" /> 
                            &nbsp;&nbsp; ~ &nbsp;&nbsp;
                            <input type="text" name="edate" id="edate" size="10" value="<%=edate %>" style="ime-mode:disabled;cursor:pointer;" class="sbox" onkeydown="javascript:OnlyNumber();" />
                            &nbsp;&nbsp;
                            <input type="submit" name="Submit1" value="검색">
                        </td>
                    </tr>
                </table></td>
              </tr>

              <tr height="10px"><td>&nbsp;</td></tr>

              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                        <td width="8%" height="30px" align="center">번호</td>
                        <td width="8%" align="center">구분</td>
                        <td width="12%" align="center">후원자ID</td>
                        <td width="12%" align="center">신청자명</td>
                        <td width="12%" align="center">아기이름</td>
                        <td width="10%" align="center">후원금액</td>
                        <td width="8%" align="center">방문일</td>
                        <td width="10%" align="center">신청일</td>
                        <td width="10%" align="center">결제여부</td>
                        <td width="10%" align="center">취소여부</td>
                    </tr>


            <!------- 게시판 목록 구하기 시작 ----------> 
            <%
                if Region = "P" then
                    sql = " SELECT "
                    sql = sql & " cd.Idx, cd.Gubun, cd.SponsorID, cd.SponsorName, cd.PayAmount, cd.BabyName, cd.BabyGender, cd.BabyBirth, " 
                    sql = sql & " cd.BabyRelation, cd.BabyImage, cd.BabyCertificate, cd.Zip1, cd.Zip2, cd.Address1, cd.Address2, " 
                    sql = sql & " cd.Email, cd.Tel, cd.VisitIdx, cd.VisitDate, cd.PaymentYN, cd.CancelYN, "
                    sql = sql & " convert(varchar(20),RegDate,120) as RegDate "
                    sql = sql & " FROM [compassweb4].[dbo].[tCspDol] cd "
                    sql = sql & " LEFT OUTER JOIN ( SELECT idx, Region FROM [compassweb4].[dbo].[tDolVisitSelectDate] ) AS dv "
                    sql = sql & " ON cd.VisitIdx = dv.idx "
                    sql = sql & " WHERE 1=1 "
                    sql = sql & " AND cd.Gubun = 'P' "
                    <!--sql = sql & " AND dv.Region IS NULL"-->
                    sql = sql & " AND cd.RegDate between '"& sdate &" 00:00:00' AND '"& edate &" 23:59:59' "

                else
                    sql = " SELECT "
                    sql = sql & " cd.Idx, cd.Gubun, cd.SponsorID, cd.SponsorName, cd.PayAmount, cd.BabyName, cd.BabyGender, cd.BabyBirth, " 
                    sql = sql & " cd.BabyRelation, cd.BabyImage, cd.BabyCertificate, cd.Zip1, cd.Zip2, cd.Address1, cd.Address2, " 
                    sql = sql & " cd.Email, cd.Tel, cd.VisitIdx, cd.VisitDate, cd.PaymentYN, cd.CancelYN, "
                    sql = sql & " convert(varchar(20),RegDate,120) as RegDate "
                    sql = sql & " FROM [compassweb4].[dbo].[tCspDol] cd "
                    sql = sql & " LEFT OUTER JOIN ( SELECT idx, Region FROM [compassweb4].[dbo].[tDolVisitSelectDate] ) AS dv "
                    sql = sql & " ON cd.VisitIdx = dv.idx "
                    sql = sql & " WHERE 1=1 "
                    sql = sql & " AND cd.Gubun = 'V' "
                    sql = sql & " AND dv.Region = '"&Region&"'"
                    sql = sql & " AND cd.RegDate between '"& sdate &" 00:00:00' AND '"& edate &" 23:59:59' "
                end if

                if key = "" then
	                sql = sql & " order by RegDate desc "
                else
	                sql = sql & " and "& keyfield &" like '%"& key &"%' order by RegDate desc "
                end if

                Set rs = RecordsetFromWS(provWSDL, sql)
            %>					
					
				<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
					<tr> 
					  <td height="22" colspan="10" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
				<% else '데이터가 있다면
                    
                    rs.PageSize = 10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

					totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.

					rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
					endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 

                    if endpage > totalpage then
						endpage = totalpage
					end If
                    
					if request("page")="" then
						npage = 1
					else
						npage = CInt(request("page"))
					end if
						
					trd = rs.RecordCount
					j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)
				%>
				<%
                    host = request.ServerVariables("HTTP_HOST")

				    i = 1
					do until rs.EOF or i > rs.PageSize
				%>
                    <tr id="dataTr_<%=rs("idx") %>" bgcolor="#FFFFFF" onmouseover='javascript:trColor(1, "<%=rs("idx") %>");' onmouseout='javascript:trColor(0, "<%=rs("idx") %>");'> 
                      <td height="25" align="center"><%=j%></td>
                      <td align="center">
                        <% if rs("Gubun") = "P" then %> 온라인결제
                        <% elseif rs("Gubun") = "V" then %> 방문
                        <% end if %>
                      </td>
					  <td align="center"><%=rs("SponsorID") %></td>
					  <td align="center"><a href="view.asp?idx=<%=rs("idx") %>&page=<%=page%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>&sdate=<%=sdate %>&edate=<%=edate %>" target="_self"><%=rs("SponsorName") %></a></td>
                      <td align="center"><%=rs("BabyName") %></td>
                      <td align="center"><%=rs("PayAmount") %></td>
                      <td align="center">
                        <% if rs("VisitDate") <> "" then %> <%=rs("VisitDate") %>
                        <% else %> -
                        <% end if %>
                      </td>
                      <!-- left 사용시 변함 --> <% '=left(rs("RegDate"),10)%>
                      <td align="center"><%=rs("RegDate") %></td>
                      <td align="center">
                        <% if rs("PaymentYN") = "N" then %> - <a href='javascript:PaymentORCancel("<%=rs("Idx") %>", "Y", "payment", "");' target="_self">[결제]</a>
                        <% elseif rs("PaymentYN") = "Y" then %> 결제완료 <a href='javascript:PaymentORCancel("<%=rs("Idx") %>", "N", "payment", "");' target="_self">[취소]</a>
                        <% end if %>
                      </td>
                      <td align="center">
                        <% if rs("CancelYN") = "N" then %> 신청됨 <a href='javascript:PaymentORCancel("<%=rs("Idx") %>", "Y", "cancel", "<%=rs("Gubun") %>");' target="_self">[취소]</a>
                        <% elseif rs("CancelYN") = "Y" then %> 취소됨 <a href='javascript:PaymentORCancel("<%=rs("Idx") %>", "N", "cancel", "<%=rs("Gubun") %>");' target="_self">[신청]</a>
                        <% end if %>
                      </td>
                    </tr>

            <%
                j=j-1
				rs.MoveNext ' 다음 레코드로 이동한다.
			    i=i+1
				loop '레코드의 끝까지 loop를 돈다.
			%> 
            <% 
         	    rs.Close
	            Set rs = nothing 

                end if
            %>               
            <!------- 게시판 목록 구하기 끝 ---------->  
                                    
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <!--<tr>
                <td align="right"><a href="write.asp?page=<%'=page%>" onfocus="this.blur()"><img src="../img/btn17.gif" width="52" height="20" border="0"></a></td>
              </tr>-->
              <tr> 
                <td align="center">
					<% if cint(startpage)<>cint(1) then%> 
				         <a href="list.asp?Region=<%=Region%>&page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>&sdate=<%=sdate %>&edate=<%=edate %>" onfocus="this.blur()">
			             [이전]</a>
			        <%end if%> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then%>
				         [<%=page%>]
		             <%else%>
					     <a href="list.asp?Region=<%=Region%>&page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>&sdate=<%=sdate %>&edate=<%=edate %>" onfocus="this.blur()"> <%=i%> </a>
		             <%end if%>
				   <% next%>

				   <% if cint(endpage)<>cint(totalpage) then%>
			             <a href="list.asp?Region=<%=Region%>&page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>&sdate=<%=sdate %>&edate=<%=edate %>" onfocus="this.blur()">
			             [다음]</a>
			       <% end if%>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
			  
              <tr> 
                <td align="center"><table width="30%" border="0" cellspacing="2" cellpadding="0">
                    <tr> 
                      <td>
                        <select name="keyfield" style="height:19px;">
                            <option value="SponsorName">신청자명</option>
                            <option value="SponsorID">후원자ID</option>
                            <option value="BabyName">아기이름</option>
                            <option value="Gubun">구분</option>
                            <option value="CancelYN">취소여부</option>
                        </select>
                      </td>
                      <td><input name="key" type="text" class="text"></td>
                      <td><input type="submit" name="Submit2" value="찾기"></td>
                    </tr>
                 </table></td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</form>

</body>
</html>
