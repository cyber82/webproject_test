﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
'*******************************************************************************
'페이지 설명 : CSP 돌맞이 후원신청 결제여부
'작성일      : 2013-11-05 김선오
'수정일      : 2013-11-14 김선오
'참고사항    :
'*******************************************************************************
%>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%

if request("idx") = "" then
    idx = 0
else 
    idx = request("idx")
end if
paymentYN = request("paymentYN")

page = Request("page")
startpage = Request("startpage")

Region = Request("Region")

link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "&Region=" & Region & "';</script>"

if CInt(idx) > 0 then 

    sql = " UPDATE [compassweb4].[dbo].[tCspDol] SET "
    sql = sql & " PaymentYN = '"& paymentYN &"' "
    sql = sql & " WHERE Idx = " & idx

    inresult = BoardWriteWS(provWSDL, sql)

    'str = ""
    'if paymentYN == "Y" then str = "결제완료"
    'elseif paymentYN == "N" str = "결제취소"
    'end if

    %>
    <script type='text/javascript' charset='euc-kr'>
        //추가 2013-11-14
        var str = "";

        if ("<%=paymentYN %>" == "Y") {
            str = "결제완료";
        }
        else if ("<%=paymentYN %>" == "N") {
            str = "결제취소";
        }
    </script>
    <%

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert(str + '되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert(str + '중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if
else 
    response.write "<script type='text/javascript' charset='euc-kr'> alert('글이 없어 ' + str + '할 수 없습니다.'); </script>"
    response.write link
end if 
%>

</html>
