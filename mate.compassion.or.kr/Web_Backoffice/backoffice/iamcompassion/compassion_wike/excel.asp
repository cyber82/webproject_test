﻿
<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
  'if request("page") = "" then
    'page = 1
  'else
    'page=request("page")
  'end if
  
  'if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
   'startpage=1
  'else
   'startpage = request("startpage")
  'end if  

    if request("idx") = "" then
        share_idx = 0
    else
        share_idx=request("idx")
    end if
    
    If request.QueryString("key") = "" Then 
    Else
        keyfield = request.QueryString("keyfield")	
        key = request.QueryString("key")
    End if
%>


<%
	'If session("admin_id")="" Then
		
			'response.write "<script>alert('로그인 후 이용해주세요.'); top.location.href='/admin/login.asp';</script>"
		
	'response.end
	'End If
	
%>

<%
	Call setXls()
	Sub setXls()
		response.write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>")
	    '수정 ) 파일명 깨짐
        xls_filename = Server.URLEncode("컴패션위크_현황")

		Response.ContentType  = "application/x-excel"
	    Response.CacheControl  = "public" 
		Response.AddHeader  "Content-Disposition" , "attachment; filename="& xls_filename & ".xls" 	
	End Sub
%>

<html>
<head>
<title>관리자화면</title>
<!--link rel="stylesheet" type="text/css" href="../index.css"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
	<table cellpadding="1" cellspacing="1" border="0" width="100%">
		<tr height="30px">
            <td width="10%" align="center" bgcolor="#3399CC" style="font-weight:bold;color:#FFFFFF;">번호</td>
            <td width="70%" align="center" bgcolor="#3399CC" style="font-weight:bold;color:#FFFFFF;">강연명</td>
            <td width="20%" align="center" bgcolor="#3399CC" style="font-weight:bold;color:#FFFFFF;">동반자포함</td>
		</tr>
		<%
            sql = " SELECT user_date " '강연명
            sql = sql & " , sum(user_companions) + COUNT(user_date) as num_sum  " '동반자포함
            sql = sql & " FROM [iamcompassionStore].[dbo].[share_challenger] " 
            sql = sql & " WHERE 1=1 " 
            sql = sql & " AND share_idx in ('140', '141') "
            sql = sql & " AND del_flag = '0' "
            sql = sql & " GROUP BY user_date "
            sql = sql & " ORDER BY user_date "

            'if key = "" then
	        '    sql = sql & " order by reg_date desc "
            'else
	        '    sql = sql & " and "& keyfield &" like '%"& key &"%' order by reg_date desc "
            'end if

            'set rs = server.CreateObject("adodb.recordset")
            'rs.PageSize=10 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.
            'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
            set rs = RecordsetFromWS(provWSDL, sql)
		%>
		<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면%>
		<tr> 
			<td height="22" colspan="9" align="center"><b>글이 없습니다!!!</b></td>
		</tr>
		<% else '데이터가 있다면
   
			'totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
			'rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
			'endpage = startpage + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
			'if endpage > rs.PageCount then
				'endpage = rs.PageCount
			'end If


			'if request("page")="" then
				'npage=1
			'else
				'npage=cint(request("page"))
			'end if
						
			j = rs.recordcount
		%>
		<%
				i=1
				do until rs.EOF
		%>
        <tr height="30px"> 
            <td bgcolor="#FFFFFF" style="border-bottom:1px solid #ccc;" align="center"><%=j%></td>
            <td bgcolor="#FFFFFF" style="border-bottom:1px solid #ccc;" align="left"><%=rs("user_date") %></td>
			<td bgcolor="#FFFFFF" style="border-bottom:1px solid #ccc;" align="center"><%=rs("num_sum") %></td>
        </tr>

        <% '주석처리 2013-05-20 %>
		<!--<tr>
			<td colspan="9" bgcolor="#cccccc" height="1"></td>
		</tr>-->
		<%									
			    j = j - 1
			    rs.movenext
            	i = i + 1
			Loop 				
		%>
		<%	
			End If 
			rs.close
			Set rs=Nothing 
		%>
	</table>
</body>
</html>