﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css" />
    <style type="text/css">
    .sbox{border:1px solid #bbb;}
    </style>

<script type="text/javascript" src="../jquery-1.7.2.min.js"></script>

</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
    if request("page") = "" then
        page = 1
    else
        page=request("page")
    end if
  
    if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
        startpage=1
    else
        startpage = request("startpage")
    end if  

    if request("idx") = "" then
        share_idx = 0
    else
        share_idx=request("idx")
    end if


	keyfield = request("keyfield")	
	key = request("key")	
                                
%>
<!-- #include virtual ="/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<body leftmargin="0" topmargin="0">

<script type="text/javascript" charset='euc-kr'>
<!--
//    $(function () {
//        $('#start_date').datepicker({
//            changeMonth: true,
//            changeYear: true,
//            yearRange: '2009:2020'
//        });
//    });

//    $(function () {
//        $('#end_date').datepicker({
//            changeMonth: true,
//            changeYear: true,
//            yearRange: '2009:2020'
//        });
//    });

    function trColor(sw, idx) {
        var id = document.getElementById("dataTr_" + idx);

        if (sw == "1") {
            id.style.backgroundColor = "#ddd";
        }
        else if (sw == "0") {
            id.style.backgroundColor = "#fff";
        }
    }

    //- 숫자만입력
    function OnlyNumber() {
        key = event.keyCode;

        if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
            event.returnValue = true;
        }
        else {
            event.returnValue = false;
        }
    }

//-->
</script>

<form name="searchForm" action="list.asp" method="post">	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30px">&nbsp;</td>
          <td width="700px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                        <td width="22px"><img src="../../img/icon01.gif" width="22" height="18"></td>
                        <td><strong><font color="#3788D9">컴패션위크 현황</font></strong></td>
                        <td align="right">
                            <a href="excel.asp?keyfield=<%=keyfield%>&key=<%=key%>"><strong><font color="#3788D9">엑셀 다운로드</font></strong></a>
                        </td>
                    </tr>
                </table></td>
              </tr>

              <tr> 
                <td>&nbsp;</td>
              </tr>

              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                        <td width="10%" height="25px" align="center">번호</td>
                        <td width="70%" align="center">강연명</td>
                        <td width="20%" align="center">동반자포함</td>
                    </tr>


            <!------- 게시판 목록 구하기 시작 ----------> 
            <%
                sql = " SELECT user_date " '강연명
                sql = sql & " , sum(user_companions) + COUNT(user_date) as num_sum  " '동반자포함
                sql = sql & " FROM [iamcompassionStore].[dbo].[share_challenger] " 
                sql = sql & " WHERE 1=1 " 
                sql = sql & " AND share_idx in ('157', '158') "
                sql = sql & " AND del_flag = '0' "
                sql = sql & " GROUP BY user_date "
                sql = sql & " ORDER BY user_date "

                'if key = "" then
	            '    sql = sql & " order by reg_date desc "
                'else
	            '    sql = sql & " and "& keyfield &" like '%"& key &"%' order by reg_date desc "
                'end if

                'set rs = server.CreateObject("adodb.recordset")
                'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
 
                'response.Write sql
                'response.End  
                Set rs = RecordsetFromWS(provWSDL, sql)
            %>					
					
				<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
					<tr> 
					  <td height="22" colspan="8" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
				<% else '데이터가 있다면
                    
                    rs.PageSize = 50 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

					totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
                    ''totalpage = rsCnt("totalCnt")

					rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
					endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
					''totalpage = Int(rsCnt("totalCnt") / rs.PageSize) + 1

                    if endpage > totalpage then
						endpage = totalpage
					end If
                    
					if request("page")="" then
						npage = 1
					else
						npage = CInt(request("page"))
					end if
						
					trd = rs.RecordCount
                    ''trd = rsCnt("totalCnt")
					j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)

                    'Response.Write("totalpage : " & totalpage & "<br/>")
                    'Response.Write("startpage : " & startpage & "<br/>")
                    'Response.Write("endpage : " & endpage & "<br/>")
                    'Response.Write("rs.PageSize : " & rs.PageSize & "<br/>")
                    'Response.Write("npage : " & npage & "<br/>")
                    'Response.Write("trd : " & trd & "<br/>")
				%>
				<%
                    host = request.ServerVariables("HTTP_HOST")
                    'response.write host
                    'response.end

				    i = 1
					do until rs.EOF or i > rs.PageSize
				%>
                    <tr id="dataTr_<%=j %>" bgcolor="#FFFFFF" onmouseover='javascript:trColor(1, "<%=j %>");' onmouseout='javascript:trColor(0, "<%=j %>");'> 
                      <td height="30px" align="center"><%=j%></td>
                      <td align="left">&nbsp;&nbsp;<%=rs("user_date") %></td>
					  <td align="center"><%=rs("num_sum") %></td>
                    </tr>

            <%
                'Response.Write("j : " & j & "<br/>")
                'Response.Write("i : " & i & "<br/>")

                j=j-1
				rs.MoveNext ' 다음 레코드로 이동한다.
			    i=i+1
				loop '레코드의 끝까지 loop를 돈다.
			%> 
            <% 
         	    rs.Close
	            Set rs = nothing 

                end if
            %>               
            <!------- 게시판 목록 구하기 끝 ---------->  
                                    
                  </table></td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</form>

</body>
</html>
