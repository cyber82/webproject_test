<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 후원자 나눠요 
'작성일      : 2013-04-08
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/app_admin/inc/freeaspupload.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
Dim strFilePath1, strFilePath2, strFilePath3, strFilePath4, strFilePath5
Dim tempFile1, tempFile2, tempFile3, tempFile4, tempFile5


saveDefaultPath = "/upload/share/"

'저장 폴더명
Dim strDateDir : strDateDir = getDateDivDir(0, "\")
strDefFolder = Server.MapPath(saveDefaultPath) & "\"
strSaveFolder = strDefFolder & strDateDir
Call MkDirAll(strSaveFolder)	'폴더 생성

uploadTempDir = strSaveFolder
Set Upload = New FreeASPUpload


'***************** 이미지 저장 *****************************
Upload.Save(uploadTempDir)

for each fileKey in Upload.UploadedFiles.keys

	Select Case fileKey
		Case "fname1" : tempFile1 = Upload.UploadedFiles("fname1").FileName
		Case "fname2" : tempFile2 = Upload.UploadedFiles("fname2").FileName
		Case "fname3" : tempFile3 = Upload.UploadedFiles("fname3").FileName
        Case "fname4" : tempFile4 = Upload.UploadedFiles("fname4").FileName
        Case "fname5" : tempFile5 = Upload.UploadedFiles("fname5").FileName
	End select

Next
'***************** 이미지 저장 *****************************

If tempFile1 <> "" Then
	vFilename = tempFile1						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile1)

	'새이름 생성
	newFilename1 = MakeFileName2(strName) & "."&strExt
	file.name = newFilename1

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로
	strFilePath1 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename1
End If


If tempFile2 <> "" Then
	vFilename = tempFile2						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile2)

	'새이름 생성
	newFilename2 = MakeFileName2(strName) & "."&strExt
	file.name = newFilename2

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로
	strFilePath2 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename2
End If


If tempFile3 <> "" Then
	vFilename = tempFile3						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile3)

	'새이름 생성
	newFilename3 = MakeFileName2(strName) & "."&strExt
	file.name = newFilename3

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로
	strFilePath3 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename3
End If


If tempFile4 <> "" Then
	vFilename = tempFile4						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile4)

	'새이름 생성
	newFilename4 = MakeFileName2(strName) & "."&strExt
	file.name = newFilename4

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로
	strFilePath4 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename4
End If


If tempFile5 <> "" Then
	vFilename = tempFile5						'파일이름
	strName = Mid(vFilename, 1, InstrRev(vFilename,".")-1)	'파일명
	strExt = LCase(Mid(vFilename, InStrRev(vFilename,".")+1)) '확장자추출

	set fso = CreateObject("Scripting.FileSystemObject")
	set file = fso.GetFile(uploadTempDir & "\" & tempFile5)

	'새이름 생성
	newFilename5 = MakeFileName2(strName) & "."&strExt
	file.name = newFilename5

	set file = nothing
	set fso = Nothing

	'디비에 저장될 이미지 경로
	strFilePath5 = saveDefaultPath & Replace(strDateDir,"\","/") & newFilename5
End If
    

    wtype = Upload.Form("wtype") '등록, 수정 구분
    page = Upload.Form("page")
    startpage = Upload.Form("startpage")
    idx = Upload.Form("idx")

    win_type = Upload.Form("win_type")
    winner_no = Upload.Form("winner_no")
    draw_type = Upload.Form("draw_type")
    share_companions = Upload.Form("share_companions")
    share_date_type = Upload.Form("share_date_type")
    share_date = Upload.Form("share_date")
    user_id = Upload.Form("user_id")
    user_name = Upload.Form("user_name")
    share_title = Upload.Form("share_title")
    share_desc = Upload.Form("share_desc")
    if share_desc = "" then
        host = request.ServerVariables("HTTP_HOST")
        addr = "http://www.compassion.or.kr"
        'addr = "http://www.compassionko.org"

        share_desc = "<div style=""text-align:center;""><img src="""& addr & strFilePath5 &""" alt=""""  style=""border: 0;"" /></div>"
    end if
    share_desc = replace(share_desc, "'", "''")

    '파일
    fname1 = Upload.Form("fname1")	'배너이미지
    fname2 = Upload.Form("fname2")	'아이콘 이미지
    fname3 = Upload.Form("fname3")	'아이콘 이미지(딤드)
    fname4 = Upload.Form("fname4")	'상단 타이틀 이미지
    fname5 = Upload.Form("fname5")	'상단 내용 이미지

    start_date = Upload.Form("sdate")
    end_date = Upload.Form("edate")
    issue_date = Upload.Form("idate")
    win_way = Upload.Form("win_way") '당첨방법 여부 '추가 2013-04-25
    view_type = Upload.Form("view_type") '보기유형 여부 '추가 2013-05-06


    share_img = Upload.Form("share_img") '배너이미지
    share_thumbnail1 = Upload.Form("share_thumbnail1") '아이콘 이미지
    share_thumbnail2 = Upload.Form("share_thumbnail2") '아이콘 이미지(딤드)
    share_title_img = Upload.Form("share_title_img") '상단 타이틀 이미지
    share_content_img = Upload.Form("share_content_img") '상단 내용 이미지

    link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"

'response.write "strFilePath1: " & strFilePath1 & "<br/>"
'response.write "tempFile1: " & tempFile1
'response.Write share_desc
'response.end


'등록
if wtype = "write" Then

    sql = " INSERT INTO [iamcompassionStore].[dbo].[share_compassion] "
    sql = sql & " (win_type, winner_no, draw_type, share_companions, share_date_type, share_date, "
    sql = sql & " [user_id], [user_name], share_title, share_img, share_thumbnail1, share_thumbnail2, "
    sql = sql & " share_title_img, share_desc, [start_date], end_date, issue_date, reg_date, del_flag, win_way, view_type) "
    sql = sql & " VALUES "
    sql = sql & " ('"&win_type&"', '"&winner_no&"', '"&draw_type&"', '"&share_companions&"', '"&share_date_type&"', '"&share_date&"', "
    sql = sql & " 'compassion', 'IAMCOMPASSION', '"&share_title&"', '"&strFilePath1&"', '"&strFilePath2&"', '"&strFilePath3&"', "
    sql = sql & " '"&strFilePath4&"', '"&share_desc&"', '"&start_date&"', '"&end_date&"', '"&issue_date&"', GETDATE(), 0, '"&win_way&"', '"&view_type&"') "

    'response.write sql
    'response.end
                
    inresult = BoardWriteWS(provWSDL, sql)

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('등록되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('등록중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if

'수정
Else

	if strFilePath1 <> "" then
		'============= 기존파일 삭제 ==============
		If share_img <> "" AND share_img <> newFilename1 Then
			arr_fname1 = Split(share_img, "/")
			fname = arr_fname1(Ubound(arr_fname1))

			strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
			strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============
		strImg1 = strFilePath1
	else
		strImg1 = share_img
	end If

	if strFilePath2 <> "" then
		'============= 기존파일 삭제 ==============
		If share_thumbnail1 <> "" AND share_thumbnail1 <> newFilename2 Then
			arr_fname2 = Split(share_thumbnail1, "/")
			fname = arr_fname2(Ubound(arr_fname2))

			strDateDir1 = arr_fname2(Ubound(arr_fname2)-2) & "\"
			strDateDir2 = arr_fname2(Ubound(arr_fname2)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============
		strImg2 = strFilePath2
	else
		strImg2 = share_thumbnail1
	end If

	if strFilePath3 <> "" then
		'============= 기존파일 삭제 ==============
		If share_thumbnail2 <> "" AND share_thumbnail2 <> newFilename3 Then
			arr_fname3 = Split(share_thumbnail2, "/")
			fname = arr_fname3(Ubound(arr_fname3))

			strDateDir1 = arr_fname3(Ubound(arr_fname3)-2) & "\"
			strDateDir2 = arr_fname3(Ubound(arr_fname3)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============
		strImg3 = strFilePath3
	else
		strImg3 = share_thumbnail2
	end If

	if strFilePath4 <> "" then
		'============= 기존파일 삭제 ==============
		If share_title_img <> "" AND share_title_img <> newFilename4 Then
			arr_fname4 = Split(share_title_img, "/")
			fname = arr_fname4(Ubound(arr_fname4))

			strDateDir1 = arr_fname4(Ubound(arr_fname4)-2) & "\"
			strDateDir2 = arr_fname4(Ubound(arr_fname4)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============
		strImg4 = strFilePath4
	else
		strImg4 = share_title_img
	end If

	if strFilePath5 <> "" then
		'============= 기존파일 삭제 ==============
		If share_content_img <> "" AND share_content_img <> newFilename5 Then
			arr_fname5 = Split(share_content_img, "/")
			fname = arr_fname5(Ubound(arr_fname5))

			strDateDir1 = arr_fname5(Ubound(arr_fname5)-2) & "\"
			strDateDir2 = arr_fname5(Ubound(arr_fname5)-1) & "\"
			strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
			strDelPath1 = strSaveFolder & fname

			Set fso = CreateObject("Scripting.FileSystemObject")
			'파일 존재시 파일 삭제
			If fso.FileExists(strDelPath1) Then
			   fso.DeleteFile strDelPath1
			End If
			Set fso = Nothing

		End if
		'============= 기존파일 삭제 ==============
		strImg5 = strFilePath5
	else
		strImg5 = share_content_img
	end If
    	
    sql = " UPDATE [iamcompassionStore].[dbo].[share_compassion] SET "
    sql = sql & " win_type='"&win_type&"', winner_no='"&winner_no&"', draw_type='"&draw_type&"', share_companions='"&share_companions&"', "
    sql = sql & " share_date_type='"&share_date_type&"', share_date='"&share_date&"', share_title='"&share_title&"', share_img='"&strImg1&"', "
    sql = sql & " share_thumbnail1='"&strImg2&"', share_thumbnail2='"&strImg3&"', share_title_img='"&strImg4&"', "
    sql = sql & " share_desc='"&share_desc&"', [start_date]='"&start_date&"', end_date='"&end_date&"', issue_date='"&issue_date&"', win_way='"&win_way&"', view_type='"&view_type&"'"
    sql = sql & " WHERE idx="&idx

    'response.write sql
    'response.end

    inresult = BoardWriteWS(provWSDL, sql)

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('수정되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('수정중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if

End If

%>