﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<%
'*******************************************************************************
'페이지 설명 : 후원자 나눠요 
'작성일      : 2013-04-09
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
%>

<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<html>
<head>
<title>관리자화면</title>
<link rel="stylesheet" type="text/css" href="../../index.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%

if request("idx") = "" then
    idx = 0
else 
    idx = request("idx")
end if

page = Request("page")
startpage = Request("startpage")

'기본 저장위치
saveDefaultPath = "/upload/share/"


'test
'idx = 0


If CInt(idx) > 0 Then

    sql = " SELECT idx, "
    sql = sql & " share_img, share_thumbnail1, share_thumbnail2, share_title_img, del_flag " 
    sql = sql & " FROM [iamcompassionStore].[dbo].[share_compassion] "
    sql = sql & " WHERE [user_id]='compassion' AND del_flag=0 "
    'sql = sql & " AND APP_ShotURL is null AND APP_Content is null AND APP_ThumImg is null "
    'sql = sql & " AND APP_MainThumImg is null AND APP_ContentImg is null AND APP_RequestType is null "
    sql = sql & " AND idx = " &idx

    'response.Write sql
    'response.End

    Set rs = RecordsetFromWS(provWSDL, sql) 

    If Not rs.eof Then

        idx = rs("idx")
        share_img = rs("share_img")
        share_thumbnail1 = rs("share_thumbnail1")
        share_thumbnail2 = rs("share_thumbnail2")
        share_title_img = rs("share_title_img")

        host = request.ServerVariables("HTTP_HOST")
        '호스팅 주소 가져오기
        addr = "http://store.iamcompassion.or.kr"
        'addr = "http://store.compassionko.org"

		If share_img <> "" Then strImg1 = "<img src='"& addr & share_img &"' border='0'>" End If
        If share_thumbnail1 <> "" Then strImg2 = "<img src='"& addr & share_thumbnail1 &"' border='0'>" End If
        If share_thumbnail2 <> "" Then strImg3 = "<img src='"& addr & share_thumbnail2 &"' border='0'>" End If
        If share_title_img <> "" Then strImg4 = "<img src='"& addr & share_title_img &"' border='0' width='700px' height='250px'>" End If

	End If


    '============= 파일 삭제 ==============
    If share_img <> "" Then
	    arr_fname1 = Split(share_img, "/")
	    fname = arr_fname1(Ubound(arr_fname1))

	    strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
	    strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
	    strDefFolder = Server.MapPath(saveDefaultPath) & "\"
	    strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
	    strDelPath1 = strSaveFolder & fname

	    Set fso = CreateObject("Scripting.FileSystemObject")
	    '파일 존재시 파일 삭제
	    If fso.FileExists(strDelPath1) Then
	       fso.DeleteFile strDelPath1
	    End If
	    Set fso = Nothing

    End if
    '============= //파일 삭제 ==============
    '============= 파일 삭제 ==============
    If share_thumbnail1 <> "" Then
	    arr_fname1 = Split(share_thumbnail1, "/")
	    fname = arr_fname1(Ubound(arr_fname1))

	    strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
	    strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
	    strDefFolder = Server.MapPath(saveDefaultPath) & "\"
	    strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
	    strDelPath1 = strSaveFolder & fname

	    Set fso = CreateObject("Scripting.FileSystemObject")
	    '파일 존재시 파일 삭제
	    If fso.FileExists(strDelPath1) Then
	       fso.DeleteFile strDelPath1
	    End If
	    Set fso = Nothing

    End if
    '============= //파일 삭제 ==============
    '============= 파일 삭제 ==============
    If share_thumbnail2 <> "" Then
	    arr_fname1 = Split(share_thumbnail2, "/")
	    fname = arr_fname1(Ubound(arr_fname1))

	    strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
	    strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
	    strDefFolder = Server.MapPath(saveDefaultPath) & "\"
	    strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
	    strDelPath1 = strSaveFolder & fname

	    Set fso = CreateObject("Scripting.FileSystemObject")
	    '파일 존재시 파일 삭제
	    If fso.FileExists(strDelPath1) Then
	       fso.DeleteFile strDelPath1
	    End If
	    Set fso = Nothing

    End if
    '============= //파일 삭제 ==============
    '============= 파일 삭제 ==============
    If share_title_img <> "" Then
	    arr_fname1 = Split(share_title_img, "/")
	    fname = arr_fname1(Ubound(arr_fname1))

	    strDateDir1 = arr_fname1(Ubound(arr_fname1)-2) & "\"
	    strDateDir2 = arr_fname1(Ubound(arr_fname1)-1) & "\"
	    strDefFolder = Server.MapPath(saveDefaultPath) & "\"
	    strSaveFolder = strDefFolder & strDateDir1 & strDateDir2
	    strDelPath1 = strSaveFolder & fname

	    Set fso = CreateObject("Scripting.FileSystemObject")
	    '파일 존재시 파일 삭제
	    If fso.FileExists(strDelPath1) Then
	       fso.DeleteFile strDelPath1
	    End If
	    Set fso = Nothing

    End if
    '============= //파일 삭제 ==============    
End If


link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "';</script>"

if CInt(idx) > 0 then 

    sql = " UPDATE [iamcompassionStore].[dbo].[share_compassion] SET "
    sql = sql & " del_flag = 1 "
    sql = sql & " WHERE idx = " & idx

    inresult = BoardWriteWS(provWSDL, sql)

    if inresult = 10 then
        response.write "<script type='text/javascript' charset='euc-kr'> alert('삭제되었습니다.'); </script>"
        response.write link
    else
        response.write "<script type='text/javascript' charset='euc-kr'> alert('삭제중 오류가 발생하였습니다.'); </script>"
        response.write link
    end if
else 
    response.write "<script type='text/javascript' charset='euc-kr'> alert('글이 없어 삭제할 수 없습니다.'); </script>"
    response.write link
end if 
%>

</html>
