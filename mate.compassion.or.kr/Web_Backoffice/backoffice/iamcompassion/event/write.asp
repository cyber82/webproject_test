﻿<% @CODEPAGE = 65001 %>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>

<%
'*******************************************************************************
'페이지 설명 : 후원자 나눠요 
'작성일      : 2013-04-08 김선오
'수정일      : 2013-09-11 김선오
'참고사항    :
'*******************************************************************************
%>
<!--METADATA TYPE="typelib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!-- #include virtual ="/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
    if request("page") = "" then
        page = 1
    else
        page=request("page")
    end if
  
    if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
        startpage = 1
    else
        startpage = request("startpage")
    end if  

    if request("idx") = "" then
        idx = 0
    else 
        idx = request("idx")
    end if

	keyfield = request("keyfield")	
	key = request("key")	

    'test
    'idx = 94

If CInt(idx) > 0 Then
	wtype = "edit"


    sql = " SELECT idx, "
    sql = sql & " win_type, winner_no, draw_type, share_companions, share_date_type, " 
    sql = sql & " share_date, [user_id], [user_name], share_title, share_desc, win_way, view_type, " 
    sql = sql & " share_img, share_thumbnail1, share_thumbnail2, share_title_img, del_flag, " 
    sql = sql & " convert(varchar(20),[start_date],120) as [start_date], "
    sql = sql & " convert(varchar(20),end_date,120) as end_date, "
    sql = sql & " convert(varchar(20),issue_date,120) as issue_date, "
    sql = sql & " convert(varchar(20),reg_date,120) as reg_date "
    sql = sql & " FROM [iamcompassionStore].[dbo].[share_compassion] "
    sql = sql & " WHERE [user_id]='compassion' AND del_flag=0 "
    'sql = sql & " AND APP_ShotURL is null AND APP_Content is null AND APP_ThumImg is null "
    'sql = sql & " AND APP_MainThumImg is null AND APP_ContentImg is null AND APP_RequestType is null "
    sql = sql & " AND idx = " &idx

    set rs = RecordsetFromWS(provWSDL, sql) 

	if rs.BOF or rs.EOF then
    else
        idx = rs("idx")
        win_type = rs("win_type")
        winner_no = rs("winner_no")
        draw_type = rs("draw_type")
        share_companions = rs("share_companions")
        share_date_type = rs("share_date_type")
        share_date = rs("share_date")
        user_id = rs("user_id")
        user_name = rs("user_name")
        share_title = rs("share_title")
        share_desc = rs("share_desc")
        share_img = rs("share_img")
        share_thumbnail1 = rs("share_thumbnail1")
        share_thumbnail2 = rs("share_thumbnail2")
        share_title_img = rs("share_title_img")
        start_date = rs("start_date")
        end_date = rs("end_date")
        issue_date = rs("issue_date")
        reg_date = rs("reg_date")
        win_way = rs("win_way") '추가 2013-04-25
        view_type = rs("view_type") '추가 2013-05-06
        

        share_desc = rs.fields("share_desc")
        share_desc = replace(share_desc, "&lt;", "<")
        share_desc = replace(share_desc, "&gt;", ">")
        share_desc = replace(share_desc, "&amp;", "&")
        share_desc = replace(share_desc, "&apos;", "'")
        share_desc = replace(share_desc, "&quot;", """")

        '파일폴더/파일명 추출
        '수정 2013-09-11
        if Trim(share_desc) <> "" then
            if Cint(instr(share_desc, "<img")) > 0 then
                con1 = instr(share_desc, "src")
                share1 = Mid(share_desc, con1, Len(share_desc)-1)    
                arrCon = split(share1, "=""")
                
                '추가 2013-09-11
                arrCon2 = split(arrCon(1), """")


                '운용
                if Cint(instr(share_desc, "http://store.iamcompassion.or.kr")) > 0 then
                   shareCon = instr(arrCon(1), "http://store.iamcompassion.or.kr")
                   strhttp = "http://store.iamcompassion.or.kr"
                end if

                if Cint(instr(share_desc, "http://www.compassion.or.kr")) > 0 then
                    shareCon = instr(arrCon(1), "http://www.compassion.or.kr")
                    strhttp = "http://www.compassion.or.kr"
                end if

                '개발
                if Cint(instr(share_desc, "http://store.compassionko.org")) > 0 then
                   shareCon = instr(arrCon(1), "http://store.compassionko.org")
                   strhttp = "http://store.compassionko.org"
                end if

                if Cint(instr(share_desc, "http://www.compassionko.org")) > 0 then
                    shareCon = instr(arrCon(1), "http://www.compassionko.org")
                    strhttp = "http://www.compassionko.org"
                end if


                '테스트 2013-09-11
                'response.write "con1 : " & con1 & "<br/>"
                'response.write "share1 : " & share1 & "<br/>"
                'response.write "arrCon(0) : " & arrCon(0) & "<br/>"
                'response.write "arrCon(1) : " & arrCon(1) & "<br/><br/>"
                'response.write "arrCon2(0) : " & arrCon2(0) & "<br/>"
                'response.write "arrCon2(1) : " & arrCon2(1) & "<br/>"
                'response.end
                
                '주석처리 2013-09-11       
                'strShare = Mid(arrCon(1), shareCon, Len(share_desc)-1)
                
                share_content_img = replace(arrCon2(0), strhttp, "")
                
                'response.write "<script> alert('"&strShare&"'); </script>"
                'response.write share_content_img
                'response.end
            end if 
        end if


        host = request.ServerVariables("HTTP_HOST")
        
        '호스팅 주소 가져오기
        '수정 2013-04-23
        if share_img <> "" then
            sfile = share_img
            sarr = split(sfile, "/")
                        
            if sarr(1) = "upload" then
                addr1 = "http://www.compassion.or.kr"
                'addr1 = "http://www.compassionko.org"
            else
                addr1 = "http://store.iamcompassion.or.kr"
                'addr1 = "http://store.compassionko.org"
            end if
        end if

        if share_thumbnail1 <> "" then
            sfile = share_thumbnail1
            sarr = split(sfile, "/")
                        
            if sarr(1) = "upload" then
                addr2 = "http://www.compassion.or.kr"
                'addr2 = "http://www.compassionko.org"
            else
                addr2 = "http://store.iamcompassion.or.kr"
                'addr2 = "http://store.compassionko.org"
            end if
        end if

        if share_thumbnail2 <> "" then
            sfile = share_thumbnail2
            sarr = split(sfile, "/")
                        
            if sarr(1) = "upload" then
                addr3 = "http://www.compassion.or.kr"
                'addr3 = "http://www.compassionko.org"
            else
                addr3 = "http://store.iamcompassion.or.kr"
                'addr3 = "http://store.compassionko.org"
            end if
        end if

        if share_title_img <> "" then
            sfile = share_title_img
            sarr = split(sfile, "/")
                        
            if sarr(1) = "upload" then
                addr4 = "http://www.compassion.or.kr"
                'addr4 = "http://www.compassionko.org"
            else
                addr4 = "http://store.iamcompassion.or.kr"
                'addr4 = "http://store.compassionko.org"
            end if
        end if

        if share_content_img <> "" then
            sfile = share_content_img
            sarr = split(sfile, "/")
                        
            if sarr(1) = "upload" then
                addr5 = "http://www.compassion.or.kr"
                'addr5 = "http://www.compassionko.org"
            else
                addr5 = "http://store.iamcompassion.or.kr"
                'addr5 = "http://store.compassionko.org"
            end if
        end if

        
        '이미지 태그
		If share_img <> "" Then strImg1 = "<img src='"& addr1 & share_img &"' border='0'>" End If
        If share_thumbnail1 <> "" Then strImg2 = "<img src='"& addr2 & share_thumbnail1 &"' border='0'>" End If
        If share_thumbnail2 <> "" Then strImg3 = "<img src='"& addr3 & share_thumbnail2 &"' border='0'>" End If
        If share_title_img <> "" Then strImg4 = "<img src='"& addr4 & share_title_img &"' border='0' width='700px' height='250px'>" End If
        If share_content_img <> "" Then strImg5 = "<img src='"& addr5 & share_content_img &"' border='0'>" End If

	End If

Else
	wtype = "write"
End If

%>
<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css" />
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
    .sbox{border:1px solid #bbb;}
    </style>

<script type="text/javascript" src="../cal.js"></script>
<script type="text/javascript" src="../jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript" src="jquery-ui-timepicker-addon.js"></script>

<% '추가 2013-07-17 %>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="/Web_Backoffice/backoffice/inc/tinymce/tinymce_config.js" charset='euc-kr'></script>
<script type="text/javascript">
    tinymce_config("share_desc");
</script>

    <script type="text/javascript">
        <!--
        function doWrite(){
            var f = document.write_f;

            if (f.share_title.value == "") {
		        alert("제목을 입력해 주세요.");
		        f.share_title.focus();
		        return;
	        }
	        if (f.sdate.value == "") {
		        alert("이벤트 시작일을 입력해 주세요.");
		        f.sdate.focus();
		        return;
	        }
	        if (f.edate.value == "") {
		        alert("이벤트 종료일을 입력해 주세요.");
		        f.edate.focus();
		        return;
		    }
		    if (f.idate.value == "") {
		        alert("발표일자를 입력해 주세요.");
		        f.idate.focus();
		        return;
		    }
		    if (f.win_type.value == "") {
		        alert("당첨형태를 입력해 주세요.");
		        f.win_type.focus();
		        return;
		    }
		    if (f.draw_type.value == "") {
		        alert("응모대상을 입력해 주세요.");
		        f.draw_type.focus();
		        return;
		    }
		    if (f.share_companions.value == "") {
		        alert("동반인수를 입력해 주세요.");
		        f.share_companions.focus();
		        return;
		    }
		    if (f.share_date_type.value == "") {
		        alert("신청일자 여부를 선택해 주세요.");
		        f.share_date_type.focus();
		        return;
		    }
		    if (f.share_date_type.value == "True") {
		        if (f.share_date.value == "") {
		            alert("신청일자를 입력해 주세요.");
		            f.share_date.focus();
		            return;
		        }
		    }
		    //if (f.share_desc.value == "") {
		    //    alert("상세내용을 입력해 주세요.");
		    //    f.share_desc.focus();
		    //    return;
		    //}
		    if (f.fname1.value == "" && f.share_img.value == "") {
		        alert("배너 이미지를 선택해 주세요.");
		        f.fname1.focus();
		        return;
		    }
		    if (f.fname2.value == "" && f.share_thumbnail1.value == "") {
		        alert("아이콘 이미지를 선택해 주세요.");
		        f.fname2.focus();
		        return;
		    }
		    if (f.fname3.value == "" && f.share_thumbnail2.value == "") {
		        alert("아이콘 이미지(딤드)를 선택해 주세요.");
		        f.fname3.focus();
		        return;
		    }
		    if (f.fname4.value == "" && f.share_title_img.value == "") {
		        alert("상세화면 타이틀 이미지를 선택해 주세요.");
		        f.fname4.focus();
		        return;
		    }
		    if (f.fname5.value == "" && f.share_content_img.value == "") {
		        alert("상세화면 타이틀 이미지를 선택해 주세요.");
		        f.fname4.focus();
		        return;
		    }

		    f.action = "write_proc.asp";
		    f.method = "post";
		    f.target = "_self";

		    if ("<%=wtype%>" == "edit") {
		        if (confirm("수정하시겠습니까?")) {
		            f.submit();
		        }
		        else {
		            return;
		        }
		    }
		    else {
		        if (confirm("등록하시겠습니까?")) {
		            f.submit();
		        }
		        else {
		            return;
		        }
		    }
	    }


        function doDel(){
	        if (confirm("[주의]삭제 하시겠습니까?")) {
		        var f = document.del_f;
		        f.action = "del_proc.asp";
		        f.submit();
	        }
		}


        jQuery(document).ready(function() {
	        jQuery.timepicker.setDefaults({
		        monthNames: ['년 1월','년 2월','년 3월','년 4월','년 5월','년 6월','년 7월','년 8월','년 9월','년 10월','년 11월','년 12월'],
		        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		        showMonthAfterYear:true,
		        dateFormat: 'yy-mm-dd',
		        timeFormat: 'hh:mm:ss',
		        duration: '',
		        showTime: true,
		        showSecond: true,
		        constrainInput: false,
		        stepMinutes: 1,
		        stepHours: 1,
		        altTimeField: '',
		        time24h: true
	        });
	        jQuery("#sdate").timepicker();
	        jQuery("#edate").timepicker();
	        jQuery("#idate").timepicker();
	        }
        );

	    //- 숫자만입력
	    function OnlyNumber() {
	        key = event.keyCode;

	        if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
	            event.returnValue = true;
	        }
	        else {
	            event.returnValue = false;
	        }
	    }
    //-->
    </script>
</head>

<!--<body>-->
<body leftmargin="0" topmargin="0" oncontextmenu='return false' ondragstart='return false'>

<p style="margin:20px;">
<img src="/Web_Backoffice/backoffice/img/icon01.gif" width="22" height="18" align="absmiddle">
<strong><font color="#3788D9">이벤트관리</font></strong>
</p>

<form name="del_f" id="del_f" method="post">
<input type="hidden" name="idx" value="<%=idx%>" />
<input type="hidden" name="page" value="<%=page%>" />
<input type="hidden" name="startpage" value="<%=startpage%>" />
</form>

<form name="write_f" id="write_f" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="idx" value="<%=idx%>" />
<input type="hidden" name="page" value="<%=page%>" />
<input type="hidden" name="startpage" value="<%=startpage%>" />
<input type="hidden" name="wtype" value="<%=wtype%>" />

<table width="870px" border="0" cellpadding="3" cellspacing="1" bgcolor="#DFDFDF" style="margin-left:20px;">
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">

    <% If CInt(idx) > 0 Then %>
        <input type="button" value="수정" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
        <input type="button" value="삭제" onclick="doDel();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" /> 
    <% Else %>
        <input type="button" value="등록" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    <% End if %>

        <input type="button" value="취소" onclick="location.href='list.asp?page=<%=page%>';" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    </td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3" width="100px"><b>제목</b></td>
	<td bgcolor="#FFFFFF" width="770px">
		<input type="text" name="share_title" id="share_title" size="50" value="<%=share_title%>" style="ime-mode:active;" class="sbox" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>이벤트 시작일</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="sdate" id="sdate" size="20" value="<%=start_date%>" style="cursor:pointer;ime-mode:disabled;" class="sbox" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>이벤트 종료일</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="edate" id="edate" size="20" value="<%=end_date%>" style="cursor:pointer;ime-mode:disabled;" class="sbox" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>발표일자</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="idate" id="idate" size="20" value="<%=issue_date%>" style="cursor:pointer;ime-mode:disabled;" class="sbox" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>당첨형태</b></td>
	<td bgcolor="#FFFFFF" height="23">
        <input type="text" name="win_type" id="win_type" size="10" value="<% if win_type <> "" then %><%=win_type%><% else %>1<% end if %>" style="ime-mode:disabled;" onkeydown="javascript:OnlyNumber();" class="sbox" />
        기본값 : 1
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>당첨자번호</b></td>
	<td bgcolor="#FFFFFF" height="23">
        <input type="text" name="winner_no" id="winner_no" size="10" value="<% if winner_no <> "" then %><%=winner_no%><% else %>0<% end if %>" style="ime-mode:disabled;" onkeydown="javascript:OnlyNumber();" class="sbox" />
        기본값 : 0
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>응모대상</b></td>
	<td bgcolor="#FFFFFF" height="23">
        <input type="text" name="draw_type" id="draw_type" size="10" value="<% if draw_type <> "" then %><%=draw_type%><% else %>2<% end if %>" style="ime-mode:disabled;" onkeydown="javascript:OnlyNumber();" class="sbox" />
        기본값 : 2
	</td>
  </tr>    
  <tr>
    <td bgcolor="#F3F3F3"><b>동반인수</b></td>
	<td bgcolor="#FFFFFF" height="23">
        <input type="text" name="share_companions" id="share_companions" size="10" value="<%=share_companions%>" style="ime-mode:disabled;" onkeydown="javascript:OnlyNumber();" class="sbox" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>보기유형 여부</b></td>
	<td bgcolor="#FFFFFF">
        <input type="radio" name="view_type" id="view_type1" value="Y" <% if view_type = "Y" then %> checked <% end if %> /> 보이기 &nbsp;&nbsp;
		<input type="radio" name="view_type" id="view_type2" value="N" <% if view_type = "N" then %> checked <% end if %> /> 보이지 않기 &nbsp;&nbsp;
        (체크가 안되있을시에는 기본적으로 보이기로 됨)
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>당첨방법 여부</b></td>
	<td bgcolor="#FFFFFF">
		<input type="radio" name="win_way" id="win_way1" value="N" <% if win_way <> "Y" then %> checked <% end if %> /> 종료반영 &nbsp;&nbsp;
        <input type="radio" name="win_way" id="win_way2" value="Y" <% if win_way = "Y" then %> checked <% end if %> /> 당첨명단반영
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>신청일자 여부</b></td>
	<td bgcolor="#FFFFFF">
		<input type="radio" name="share_date_type" id="share_dtype1" value="True" <% if share_date_type="True" then %> checked <% end if %> /> 반영 &nbsp;&nbsp;
        <input type="radio" name="share_date_type" id="share_dtype2" value="False" <% if share_date_type="False" then %> checked <% end if %> /> 미반영
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>신청일자</b></td>
	<td bgcolor="#FFFFFF">
		<input type="text" name="share_date" id="share_date" size="70" value="<%=share_date%>" class="sbox" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>상세내용</b></td>
	<td bgcolor="#FFFFFF">
		<!--<textarea name="share_desc" id="share_desc" style="width:700px;height:120px;" class="sbox"><%'=share_desc %></textarea>-->
        <% '수정 2013-06-19 %>
        <div>
	        <textarea name="share_desc" id="share_desc" style="width:700px;height:170px;" class="sbox"><%=share_desc %></textarea>
        </div>
        <!--<div><button onClick="addArea2();">Editor</button> <button onClick="removeArea2();">Html</button></div>-->
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>배너 이미지</b></td>
	<td bgcolor="#FFFFFF">
		<%=strImg1%><br />
        <%=share_img%><br />
        <input type="file" name="fname1" id="fname1" size="50" style="ime-mode:disabled;" class="sbox" />
        <input type="hidden" name="share_img" id="share_img" value="<%=share_img%>" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>아이콘 이미지</b></td>
	<td bgcolor="#FFFFFF">
		<%=strImg2%><br />
        <%=share_thumbnail1%><br />
        <input type="file" name="fname2" id="fname2" size="50" style="ime-mode:disabled;" class="sbox" />
        <input type="hidden" name="share_thumbnail1" id="share_thumbnail1" value="<%=share_thumbnail1%>" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>아이콘 이미지(딤드)</b></td>
	<td bgcolor="#FFFFFF">
		<%=strImg3%><br />
        <%=share_thumbnail2%><br />
        <input type="file" name="fname3" id="fname3" size="50" style="ime-mode:disabled;" class="sbox" />
        <input type="hidden" name="share_thumbnail2" id="share_thumbnail2" value="<%=share_thumbnail2%>" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>상세화면 타이틀 이미지</b></td>
	<td bgcolor="#FFFFFF">
		<%=strImg4%><br />
        <%=share_title_img%><br />
        <input type="file" name="fname4" id="fname4" size="50" style="ime-mode:disabled;" class="sbox" />
        <input type="hidden" name="share_title_img" id="share_title_img" value="<%=share_title_img%>" />
	</td>
  </tr>
  <tr>
    <td bgcolor="#F3F3F3"><b>상세화면 내용 이미지</b></td>
	<td bgcolor="#FFFFFF">
		<%=strImg5%><br />
        <%=share_content_img%><br />
        <input type="file" name="fname5" id="fname5" size="50" style="ime-mode:disabled;" class="sbox" />
        <input type="hidden" name="share_content_img" id="share_content_img" value="<%=share_content_img%>" />
	</td>
  </tr>

  <tr>
    <td colspan="2" bgcolor="#FFFFFF">

    <% If CInt(idx) > 0 Then %>
        <input type="button" value="수정" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
        <input type="button" value="삭제" onclick="doDel();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" /> 
    <% Else %>
        <input type="button" value="등록" onclick="doWrite();" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    <% End if %>

        <input type="button" value="취소" onclick="location.href='list.asp?page=<%=page%>';" style="font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:23px;cursor:pointer;" />
    </td>
  </tr>
</table>

</form>

    <% '추가 2013-06-19 %>
    <% '주석처리 2013-07-17 %>
    <!--<script src="/Web_Backoffice/backoffice/inc/js/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        var area1, area2;
        /*
        function toggleArea1() {
        if (!area1) {
        area1 = new nicEditor({ fullPanel: true }).panelInstance('myArea1', { hasPanel: true });
        } else {
        area1.removeInstance('myArea1');
        area1 = null;
        }
        }
        */

        function addArea2() {
            area2 = new nicEditor({ fullPanel: true }).panelInstance('share_desc');
        }
        function removeArea2() {
            area2.removeInstance('share_desc');
        }

        bkLib.onDomLoaded(function () { addArea2(); });
    </script>-->

</body>
</html>