﻿<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>

<html>
<head>
<title>관리자화면</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/Web_Backoffice/backoffice/index.css" />
    <style type="text/css">
    .sbox{border:1px solid #bbb;}
    </style>

<script type="text/javascript" src="../jquery-1.7.2.min.js"></script>

</head>

<%
  ' 페이지 정해주기
  '넘오오는 페이지 번호가 없다면 무조건 첫페이지로 맞춘다.
  '만일 넘어오는 페이지 번호가 있다면 그 번호로 세팅한다.
  ' 이 넘어오는 번호가 absolutepage 로 지정되게 될 것이다.
    if request("page") = "" then
        page = 1
    else
        page=request("page")
    end if
  
    if request("startpage") = ""then 'startpage 라는 값으로 넘어오지 않으면 1로 세팅
        startpage=1
    else
        startpage = request("startpage")
    end if  

    if request("idx") = "" then
        share_idx = 0
    else
        share_idx=request("idx")
    end if


	keyfield = request("keyfield")	
	key = request("key")	
                                
%>
<!-- #include virtual ="/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual="/Web_Backoffice/backoffice/inc/record_xml.asp"-->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<body leftmargin="0" topmargin="0">

<script type="text/javascript" charset='euc-kr'>
<!--
//    $(function () {
//        $('#start_date').datepicker({
//            changeMonth: true,
//            changeYear: true,
//            yearRange: '2009:2020'
//        });
//    });

//    $(function () {
//        $('#end_date').datepicker({
//            changeMonth: true,
//            changeYear: true,
//            yearRange: '2009:2020'
//        });
//    });

    function trColor(sw, idx) {
        var id = document.getElementById("dataTr_" + idx);

        if (sw == "1") {
            id.style.backgroundColor = "#ddd";
        }
        else if (sw == "0") {
            id.style.backgroundColor = "#fff";
        }
    }

    //- 숫자만입력
    function OnlyNumber() {
        key = event.keyCode;

        if (
                (key >= 48 && key <= 57) // 키보드 상단 숫자키
                || (key >= 96 && key <= 105) // 키패드 숫자키
                || key == 8  // 백스페이스 키
                || key == 37 // 왼쪽 화살표 키
                || key == 39 // 오른쪽 화살표 키
                || key == 46 // DEL 키
                || key == 13 // 엔터 키
                || key == 9  // Tab 키
                || key == 109  // -(오른쪽) 키
                || key == 189  // -(중간) 키
               ) {
            event.returnValue = true;
        }
        else {
            event.returnValue = false;
        }
    }

    //당첨여부 수정
    function editProc() {

        document.searchForm.action = "edit_proc.asp";
        document.searchForm.method = "post";
        document.searchForm.target = "_self";

        if (confirm("수정하시겠습니까?")) {
            document.searchForm.submit();
        } else {
            return;
        }
    }

    //전체선택
    $(document).ready(function () {
        $("#checkall").click(function () {
            if ($(this).attr("checked")) {
                $(".chkbox").attr("checked", true);
            } else {
                $(".chkbox").attr("checked", false);
            }

            chkIdxSave();
        });
    });

    //선택한 번호 담기
    function chkIdxSave() {

        var idxArrY = document.getElementById("hdIdxArrY");
        var idxArrN = document.getElementById("hdIdxArrN");
        var ckList = $(".chkbox");
        var idxList = "";
        var idx = "";
        var temp1 = "";
        var temp2 = "";

        for (i = 0; i < ckList.length; i++) {
            
            if (ckList[i].checked) {
                idxList = ckList[i].id;
                idx = $("#" + idxList + "").val();
                temp1 += idx + ",";
            }
            else {
                idxList = ckList[i].id;
                idx = $("#" + idxList + "").val();
                temp2 += idx + ",";
            }
        }
        idxArrY.value = temp1;
        idxArrN.value = temp2;

        //alert("hdIdxArrY: " + idxArrY.value);
        //alert("hdIdxArrN: " + idxArrN.value);
    }


    //전체목록 추가 2013-09-17
    function AllList() {
        document.getElementById("keyfield").value = "";
        document.getElementById("key").value = "";

        document.searchForm.action = "list.asp";
        document.searchForm.method = "post";
        document.searchForm.target = "_self";
        document.searchForm.submit();

        //location.href = "list.aspx?idx=<%=share_idx %>";
    }
//-->
</script>
<form name="searchForm" action="list.asp" method="post">	
<input type="hidden" name="idx" id="idx" value="<%=share_idx %>" />
<input type="hidden" name="share_idx" id="share_idx" value="<%=share_idx %>" />

<table width="500px" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF" style="margin-left:30px;">
    <tr bgcolor="#F3F3F3"> 
        <td width="400px" align="center">시간</td>
        <td width="100px" align="center">동반자포함</td>
    </tr>

    <%
        'response.write keyfield & "<br/>"
        'response.write key & "<br/>"

        sql = " SELECT user_date, SUM(user_companions) + COUNT(reg_date) AS Cnt " 
        sql = sql & " FROM [iamcompassionStore].[dbo].[share_challenger] "
        sql = sql & " WHERE del_flag = 0 AND [user_id] NOT IN ('appani') AND share_idx = " & share_idx
        sql = sql & " GROUP BY [user_date] "
        sql = sql & " ORDER BY [user_date] "

        'response.write sql

        'set rs = server.CreateObject("adodb.recordset")
        'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
 
        'response.Write sql
        'response.End  
        Set rs = RecordsetFromWS(provWSDL, sql)

        sqlCnt = " SELECT user_date, SUM(user_companions) + COUNT(reg_date) AS Cnt " 
        sqlCnt = sqlCnt & " FROM [iamcompassionStore].[dbo].[share_challenger] "
        sqlCnt = sqlCnt & " WHERE del_flag = 0 AND [user_id] NOT IN ('appani') AND share_idx = " & share_idx
        sqlCnt = sqlCnt & " GROUP BY [user_date] "
        sqlCnt = sqlCnt & " ORDER BY [user_date] "

        'response.Write sqlCnt
        'response.End
        Set rsCnt = RecordsetFromWS2(provWSDL, sqlCnt)
    %>
    <% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
	    <tr> 
		    <td height="22" colspan="8" align="center"><b>글이 없습니다!!!</b></td>
	    </tr>				
    <% else '데이터가 있다면
        rs.PageSize = 50 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

		totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
        ''totalpage = rsCnt("totalCnt")

		rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
		endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
		''totalpage = Int(rsCnt("totalCnt") / rs.PageSize) + 1

        if endpage > totalpage then
			endpage = totalpage
		end If
                    
		if request("page")="" then
			npage = 1
		else
			npage = CInt(request("page"))
		end if
						
		trd = rs.RecordCount
        ''trd = rsCnt("totalCnt")
		j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)

        'Response.Write("totalpage : " & totalpage & "<br/>")
        'Response.Write("startpage : " & startpage & "<br/>")
        'Response.Write("endpage : " & endpage & "<br/>")
        'Response.Write("rs.PageSize : " & rs.PageSize & "<br/>")
        'Response.Write("npage : " & npage & "<br/>")
        'Response.Write("trd : " & trd & "<br/>")
    %>
    <%
        host = request.ServerVariables("HTTP_HOST")
        'response.write host
        'response.end

		i = 1
		do until rs.EOF or i > rs.PageSize
	%>
    <tr bgcolor="#FFFFFF"'> 
        <td align="center"><%=rs("user_date") %></td>
        <td align="center"><%=rs("Cnt") %></td>
    </tr>
    <%
        'Response.Write("j : " & j & "<br/>")
        'Response.Write("i : " & i & "<br/>")

        j=j-1
		rs.MoveNext ' 다음 레코드로 이동한다.
		i=i+1
		loop '레코드의 끝까지 loop를 돈다.
	%> 
    <% 
        rs.Close
	    Set rs = nothing 

        end if
    %>    
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30px">&nbsp;</td>
          <td width="900px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right">&nbsp;</td>
              </tr>
              
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                        <td width="22px"><img src="../../img/icon01.gif" width="22" height="18"></td>
                        <td><strong><font color="#3788D9">후원자나눠요 신청명단</font></strong></td>
                        <td align="right">
                            <a href="excel.asp?keyfield=<%=keyfield%>&key=<%=key%>&idx=<%=share_idx %>"><strong><font color="#3788D9">엑셀 다운로드</font></strong></a>
                        </td>
                    </tr>
                </table></td>
              </tr>

              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr style="padding:5px;">
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="10%">
                            <a href="javascript:AllList();" style="color:#3788D9;">전체목록</a>
                        </td>
                        <td align="right" width="90%">
                            <% '추가 2013-09-17 %>
                            <input type="hidden" id="hdPage" name="hdPage" value="<%=page %>" />
                            <input type="hidden" id="hdKeyfield" name="hdKeyfield" value="<%=keyfield %>" />
                            <input type="hidden" id="hdKey" name="hdKey" value="<%=key %>" />

                            <input type="hidden" id="hdIdxArrY" name="hdIdxArrY" />
                            <input type="hidden" id="hdIdxArrN" name="hdIdxArrN" />
			                <input type="checkbox" name="checkall" id="checkall">전체선택
                            <input type="button" value="당첨수정" onclick='javascript:editProc();' style="color:#000000; font-size: 9pt; background-color:#99CCFF; border:1 solid #330099;width:70;height:20px;cursor:pointer;">
		                </td> 
                    </tr>
                </table></td>
              </tr>

              <tr> 
                <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#DFDFDF">
                    <tr bgcolor="#F3F3F3"> 
                        <td width="50px" height="25" align="center">번호</td>
                        <td width="80px" align="center">웹아이디</td>
                        <td width="80px" align="center">신청자명</td>
                        <td width="100px" align="center">연락처</td>
                        <td width="50px" align="center">동반인</td>
                        <td width="" align="center">내용</td>
                        <td width="100px" align="center">신청일자</td>
                        <td width="80px" align="center">등록일</td>
                        <td width="50px" align="center">당첨여부</td>
                    </tr>


            <!------- 게시판 목록 구하기 시작 ----------> 
            <%
                'response.write keyfield & "<br/>"
                'response.write key & "<br/>"

                sql = " SELECT idx, "
                sql = sql & " share_idx, [user_id], [user_name], user_tel, user_desc, " 
                sql = sql & " user_companions, user_date, del_flag, win_flag, " 
                sql = sql & " convert(varchar(20),reg_date,120) as reg_date " 
                sql = sql & " FROM [iamcompassionStore].[dbo].[share_challenger] "
                sql = sql & " WHERE del_flag=0 AND share_idx=" & share_idx
                
                if key = "" then
	                sql = sql & " order by reg_date desc "
                else
	                sql = sql & " AND "& keyfield &" like '%"& key &"%' order by reg_date desc "
                end if

                'response.write sql

                'set rs = server.CreateObject("adodb.recordset")
                'rs.Open sql, db,1 '레코드 커서 값을 지정해 주어야 한다. 만일 지정이 안되면 페이징이 안된다.
 
                'response.Write sql
                'response.End  
                Set rs = RecordsetFromWS(provWSDL, sql)

                'totalcount
                sqlCnt = " SELECT idx, "
                sqlCnt = sqlCnt & " share_idx, [user_id], [user_name], user_tel, user_desc, " 
                sqlCnt = sqlCnt & " user_companions, user_date, del_flag, win_flag, " 
                sqlCnt = sqlCnt & " convert(varchar(20),reg_date,120) as reg_date " 
                sqlCnt = sqlCnt & " FROM [iamcompassionStore].[dbo].[share_challenger] "
                sqlCnt = sqlCnt & " WHERE del_flag=0 AND share_idx=" & share_idx

                if key <> "" then
	                sqlCnt = sqlCnt & " and "& keyfield &" like '%"& key &"%' "
                end if

                'response.Write sqlCnt
                'response.End
                Set rsCnt = RecordsetFromWS2(provWSDL, sqlCnt)
            %>					
					
				<% if rs.BOF or rs.EOF then ' 만일 레코드가 없다면 %>
					<tr> 
					  <td height="22" colspan="8" align="center"><b>글이 없습니다!!!</b></td>
			        </tr>
				<% else '데이터가 있다면
                    
                    rs.PageSize = 50 ' 페이지 사이즈를 정해준다.. 반드시 레코드셋 오픈전에 지정해 주어야한다.

					totalpage = rs.PageCount ' 총페이지수를 totalpage라는 변수에 넣는다.
                    ''totalpage = rsCnt("totalCnt")

					rs.AbsolutePage = page  '지정된 페이지로 레코드의 현재의 위치를 이동시킨다.
      
					endpage = CInt(startpage) + 9 'endpage 라는 변수에다가 startpage에 4를 더한값이 들어간다. 
					''totalpage = Int(rsCnt("totalCnt") / rs.PageSize) + 1

                    if endpage > totalpage then
						endpage = totalpage
					end If
                    
					if request("page")="" then
						npage = 1
					else
						npage = CInt(request("page"))
					end if
						
					trd = rs.RecordCount
                    ''trd = rsCnt("totalCnt")
					j = trd - Cint(rs.PageSize) * (Cint(npage) - 1)

                    'Response.Write("totalpage : " & totalpage & "<br/>")
                    'Response.Write("startpage : " & startpage & "<br/>")
                    'Response.Write("endpage : " & endpage & "<br/>")
                    'Response.Write("rs.PageSize : " & rs.PageSize & "<br/>")
                    'Response.Write("npage : " & npage & "<br/>")
                    'Response.Write("trd : " & trd & "<br/>")
				%>
				<%
                    host = request.ServerVariables("HTTP_HOST")
                    'response.write host
                    'response.end

				    i = 1
					do until rs.EOF or i > rs.PageSize
				%>
                    <tr id="dataTr_<%=rs("idx") %>" bgcolor="#FFFFFF" onmouseover='javascript:trColor(1, "<%=rs("idx") %>");' onmouseout='javascript:trColor(0, "<%=rs("idx") %>");'> 
                      <td height="40" align="center"><%=j%></td>
                      <td align="center"><%=rs("user_id") %></td>
					  <td align="center"><%=rs("user_name") %></td>
                      <td align="center"><%=rs("user_tel") %></td>
                      <td align="center"><%=rs("user_companions") %></td>
                      <td align="center"><%=rs("user_desc") %></td>
                      <td align="center"><%=rs("user_date") %></td>
                      <!-- left 사용시 변함 --> <% '=left(rs("reg_date"),10)%>
                      <td align="center"><%=left(rs("reg_date"),10) %></td>
                      <td align="center">
                        <input type="checkbox" id='chk_<%=rs("idx") %>' name='chkbox<%=i %>' value="<%=rs("idx") %>" class="chkbox" onclick="javascript:chkIdxSave();" <% if rs("win_flag") = "True" then %>checked<% end if %> />
                        <% if rs("win_flag") = "True" then %>
                            <br />당첨
                        <% end if %>
                      </td>
                    </tr>

            <%
                'Response.Write("j : " & j & "<br/>")
                'Response.Write("i : " & i & "<br/>")

                j=j-1
				rs.MoveNext ' 다음 레코드로 이동한다.
			    i=i+1
				loop '레코드의 끝까지 loop를 돈다.
			%> 
            <% 
         	    rs.Close
	            Set rs = nothing 

                end if
            %>               
            <!------- 게시판 목록 구하기 끝 ---------->  
                                    
                  </table></td>
              </tr>

              <tr> 
                <td>&nbsp;</td>
              </tr>
              <!--<tr>
                <td align="right"><a href="write.asp?page=<%'=page%>" onfocus="this.blur()"><img src="../img/btn17.gif" width="52" height="20" border="0"></a></td>
              </tr>-->
              <tr> 
                <td align="center">
					<% if cint(startpage)<>cint(1) then%> 
				         <a href="list.asp?page=<%=startpage-10%>&startpage=<%=startpage-10%>&keyfield=<%=keyfield%>&key=<%=key%>&idx=<%=share_idx %>" onfocus="this.blur()">
			             [이전]</a>
			        <%end if%> 

					<% for i = startpage to endpage step 1 %>
		             <% if cint(i) = cint(page) then%>
				         [<%=page%>]
		             <%else%>
					     <a href="list.asp?page=<%=i%>&startpage=<%=startpage%>&keyfield=<%=keyfield%>&key=<%=key%>&idx=<%=share_idx %>" onfocus="this.blur()"> <%=i%> </a>
		             <%end if%>
				   <% next%>

				   <% if cint(endpage)<>cint(totalpage) then%>
			             <a href="list.asp?page=<%=startpage+10%>&startpage=<%=startpage+10%>&keyfield=<%=keyfield%>&key=<%=key%>&idx=<%=share_idx %>" onfocus="this.blur()">
			             [다음]</a>
			       <% end if%>
				</td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
			  
              <tr> 
                <td align="center"><table width="30%" border="0" cellspacing="2" cellpadding="0">
                    <tr> 
                      <td>
                        <select name="keyfield" style="height:19px;">
                            <option value="user_id" <% if keyfield = "user_id" then %>selected<% end if %>>웹아이디</option>
                            <option value="user_name" <% if keyfield = "user_name" then %>selected<% end if %>>신청자명</option>
                            <option value="user_date" <% if keyfield = "user_date" then %>selected<% end if %>>신청일자</option>
                        </select>
                      </td>
                      <td><input name="key" type="text" class="text" value="<%=key %>"></td>
                      <td><input type="submit" name="Submit2" value="찾기"></td>
                    </tr>
                 </table></td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>

</form>

</body>
</html>
