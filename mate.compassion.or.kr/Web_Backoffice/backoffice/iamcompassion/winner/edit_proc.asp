<% @CODEPAGE = 65001 %>
<% Response.Charset = "utf-8" %>
<%
'*******************************************************************************
'페이지 설명 : 후원자 나눠요 신청명단 
'작성일      : 2013-04-25
'작성자      : 김선오
'참고사항    :
'*******************************************************************************
%>
<%
	Response.CharSet = "UTF-8"
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "no-cache"
	Response.CacheControl = "no-cache"
%>
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/func.asp" -->
<!-- #include virtual = "/Web_Backoffice/backoffice/inc/record_xml.asp" -->

<%
'웹서비스 링크 주소
provWSDL = session("serviceUrl")
%>

<%
Dim temp

'수정 2013-09-17
'page = Request.Form("page")
page = Request.Form("hdPage")
key = Request.Form("hdKey")
keyfield = Request.Form("hdKeyfield")

startpage = Request.Form("startpage")
share_idx = Request.Form("share_idx")
hdIdxArrY = Request.Form("hdIdxArrY")
hdIdxArrN = Request.Form("hdIdxArrN")
    
temp = ""
link = "<script type='text/javascript' charset='euc-kr'>location.href='list.asp?page=" & page & "&startpage=" & startpage & "&idx=" & share_idx & "&keyfield=" & keyfield & "&key=" & key & "';</script>"

if hdIdxArrY <> "" then

    yidx = Split(hdIdxArrY, ",")

    'Response.Write yidx(0)
    'Response.End
            
    For i = 0 To UBOUND(yidx) - 1

        idx = yidx(i)

        sql = " UPDATE [iamcompassionStore].[dbo].[share_challenger] SET "
        sql = sql & " win_flag = 'true' "
        sql = sql & " WHERE idx=" & idx & " and share_idx=" & share_idx

        inresult = BoardWriteWS(provWSDL, sql)
        temp = inresult

        if temp <> "10" then
            response.write "<script type='text/javascript' charset='euc-kr'> alert('수정중 오류가 발생하였습니다.'); </script>"
            response.write link
        end if
    Next

end if

if hdIdxArrN <> "" then

    nidx = Split(hdIdxArrN, ",")

    '수정 2013-09-17
    For k = 0 To UBOUND(nidx) - 1

        idx = nidx(k)

        sql2 = " UPDATE [iamcompassionStore].[dbo].[share_challenger] SET "
        sql2 = sql2 & " win_flag = 'false' "
        sql2 = sql2 & " WHERE idx=" & idx & " and share_idx=" & share_idx

        inresult = BoardWriteWS(provWSDL, sql2)
        temp = inresult

        if temp <> "10" then
            response.write "<script type='text/javascript' charset='euc-kr'> alert('수정중 오류가 발생하였습니다.'); </script>"
            response.write link
        end if
    Next

end if


if temp = "10" then
    response.write "<script type='text/javascript' charset='euc-kr'> alert('수정되었습니다.'); </script>"
    response.write link
end if


%>