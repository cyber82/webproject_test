﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BenefitList.aspx.cs" Inherits="Mate_nanum_BenefitList" %>
<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript">
        function GoNationBenefitList(orderby) {
            location.href = "BenefitList.aspx?option=nation&dropNation=" + document.getElementById('dropNation').value + "&orderby=" + orderby;              
        }

        function OpenLetter(val) {            
            NewWindow("BenefitDetail.aspx?idx=" + val, "bedetail", "530", "450", "auto");
            return false;
        }

        function GoBenefitList() {
            location.href = "BenefitList.aspx?option=" + document.getElementById('SearchOption').value
            + "&dropNation=" + document.getElementById('txtSearch').value + "&orderby=";
            return false;
        }
    </script>
</head>
<body style="overflow-X:hidden">
    <form id="form1" runat="server">     
    <div>        
        <!-- container --><div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="share benefit-letter">
 
		<!-- contents -->
		<div style="display:inline;float:left;position:relative;width:658px;margin:0 -1px 0 0;padding:0 50px 100px;overflow:hidden;">
			
			<div class="cont">
				<div class="cover">
					수혜국 어린이 편지 용어를 검색하세요 메이트 님께서 편지를 번역하시면서 잘 몰랐거나 궁금했던 용어들을 검색하세요.
				</div>
				<fieldset class="search-benefit-letter">
					<legend class="hide">수혜국 편지 용어 검색</legend>
					<div class="search">
                        <select id="SearchOption" runat="server">
							<option value="all">전체</option>
                            <option value="eword">용어(영문)</option>
                            <option value="comment">내용</option>
						</select>
						<input type="text" class="text" id="txtSearch" runat="server" />
                        <asp:Button ID="btnSeach" runat="server" Text="" CssClass="btn btn-search2" 
                            OnClientClick ="return GoBenefitList();"  />
					</div>
					<div class="sort">
						<strong class="txt txt-nation"><span>국가별 보기</span></strong>
                        <asp:DropDownList ID="dropNation" runat="server">
                        </asp:DropDownList>							
						<ul class="align">
							<li><a href="javascript:GoNationBenefitList('asc')" class="txt txt-ascending"><span>오름차순 정렬</span></a></li>
								<li><a href="javascript:GoNationBenefitList('readcount')" class="txt txt-hits"><span>조회순</span></a></li>
						</ul>
					</div>
				</fieldset>				
				<table class="mate-list">
					<thead>                      
						<tr>
							<!--<th class="td-num"><img src="/image/mate/text/td-num2.gif" alt="번호" /></th>-->
							<th class="td-initial"><img src="/image/mate/text/td-initial.gif" alt="초성" /></th>
							<th class="td-term"><img src="/image/mate/text/td-term.gif" alt="용어" /></th>
							<th class="td-nation"><img src="/image/mate/text/td-nation.gif" alt="국가" /></th>
							<th class="td-writer2"><img src="/image/mate/text/td-writer2.gif" alt="글쓴이" /></th>
							<th class="td-date2"><img src="/image/mate/text/td-date2.gif" alt="작성일" /></th>
							<th class="td-hits"><img src="/image/mate/text/td-hits.gif" alt="조회수" /></th>
						</tr>
					</thead>
					<tbody>
                    <asp:Repeater ID="repData" runat="server" onitemdatabound="repData_ItemDataBound">
                    <ItemTemplate>
						<tr class="first-tr">
							<!--<td class="td-num">1</td>-->
							<td class="td-initial"><%#DataBinder.Eval(Container.DataItem,"initial")%></td>
							<td class="td-term"><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#DataBinder.Eval(Container.DataItem,"eword")%></a><asp:Label ID="labNew" runat="server" Text="new" CssClass="ico-new"></asp:Label></td>
							<td class="td-writer2"><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#DataBinder.Eval(Container.DataItem,"nationStr")%></a></td>
							<td class="td-writer2"><%#DataBinder.Eval(Container.DataItem,"regUserName")%></td>
							<td class="td-date2"><%#Convert.ToDateTime (DataBinder.Eval(Container.DataItem,"RegisterDate")).ToString ("yyyy-MM-dd")%></td>
							<td class="td-hits"><%#DataBinder.Eval(Container.DataItem,"readCount")%></td>
						</tr>
                        <asp:HiddenField ID="hidb_writeday" runat="server" Value ='<%#DataBinder.Eval(Container.DataItem, "RegisterDate")%>' />  
                    </ItemTemplate>
                    </asp:Repeater>
					</tbody>
				</table>
			<!-- paginator -->
			<div class="paginator">
				<taeyo:paginghelper id="PagingHelper1" runat="server"  ononpageindexchanged="PagingHelper1_OnPageIndexChanged" CurrnetNumberColor="Silver" ForeColor="#404040"></taeyo:paginghelper>                 
			</div>
			<!-- // paginator -->
 
			</div>
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

    </div>    
    </form>
</body>
</html>
