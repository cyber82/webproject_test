﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class Mate_translation_BibleIndexList : System.Web.UI.Page
{
    public string sgHtml;

    protected void Page_Load(object sender, EventArgs e)
    {
        SetHtmlData();
    }

    private void SetHtmlData()
    {
        int iCnt = 1;

        DataSet ds = new DataSet();

        //가이드 쿼리 (구약성경, 신약성경)
        Object[] objSql = new object[2] { "SELECT * FROM MATE_Help WHERE HelpType = 'BIBLE' AND Description = 'OLD'",
            "SELECT * FROM MATE_Help WHERE HelpType = 'BIBLE' AND Description = 'NEW'" };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

        StringBuilder sb = new StringBuilder();

        //구약성경
        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td>");

        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td>");
        sb.Append("                구약");
        sb.Append("           </td>");
        sb.Append("        </tr>");
        sb.Append("</table>");
        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td style='width:15%; border:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                약자");
        sb.Append("            </td>");
        sb.Append("            <td style='width:30%; border:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                제목");
        sb.Append("            </td>");
        sb.Append("             <td style='width:20%; border:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                약자(영문)");
        sb.Append("           </td>");
        sb.Append("            <td style='width:35%; border:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                제목(영문)");
        sb.Append("            </td>");
        sb.Append("        </tr>");

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            if (iCnt == ds.Tables[0].Rows.Count)
            {
                sb.Append("        <tr>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["EngCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["EngName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }

            else
            {
                sb.Append("        <tr>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["EngCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["EngName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }

            iCnt++;
        }

        sb.Append("    </table>");

        sb.Append("           </td>");

        sb.Append("            <td style='width:15px;'>");
        sb.Append("            </td>");

        sb.Append("            <td style='vertical-align:top'>");

        //신약성경
        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td>");

        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td>");
        sb.Append("                신약");
        sb.Append("           </td>");
        sb.Append("        </tr>");
        sb.Append("</table>");
        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td style='width:15%; border:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                약자");
        sb.Append("            </td>");
        sb.Append("            <td style='width:30%; border:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                제목");
        sb.Append("            </td>");
        sb.Append("             <td style='width:20%; border:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                약자(영문)");
        sb.Append("           </td>");
        sb.Append("            <td style='width:35%; border:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                제목(영문)");
        sb.Append("            </td>");
        sb.Append("        </tr>");

        iCnt = 1;

        foreach (DataRow dr in ds.Tables[1].Rows)
        {
            if (iCnt == ds.Tables[1].Rows.Count)
            {

                sb.Append("        <tr>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["EngCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["EngName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }

            else
            {
                sb.Append("        <tr>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["EngCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='height:auto; border:1px solid #CCCCCC;'>");
                sb.Append(dr["EngName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }

            iCnt++;
        }

        sb.Append("    </table>");

        sb.Append("           </td>");
        sb.Append("        </tr>");
        sb.Append("</table>");

        sgHtml = sb.ToString();

        ds.Dispose();
        _WWW6Service.Dispose();
    }
}