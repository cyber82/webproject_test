﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TranslationHome.aspx.cs" Inherits="Mate_translation_TranslationHome" validateRequest="false"%>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateTranslationHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>

<%@ Register src="../Controls/TranslationRight.ascx" tagname="TranslationRight" tagprefix="uc5" %>
<%@ Register src="../Controls/BoardList.ascx" tagname="BoardList" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript">
        function ShowDetail(sCorrID, sPackageID, sPageCount, sMateType, sTitle) {

            //alert(sCorrID + "/" + sPackageID + "/" + sMateType);

            if (sCorrID != "") {
                if (sTitle != "오신고알람" && sTitle != "번역오역반송" && sTitle != "편지 신고") {
                    if (sMateType == 'SCREND') {
                        location.href = '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount + '&MateType=' + sMateType;
                    }
                    else if (sMateType == 'TRANSTART') {
                        location.href = '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount + '&MateType=' + sMateType;
                    }
                    else if (sMateType == 'SCRSTART') {
                        location.href = '/translation/TranslationDetailMain.aspx?CorrID=' + sCorrID + '&PackageID=' + sPackageID + '&PageCount=' + sPageCount + '&MateType=' + sMateType;
                    }
                }
            }

        }

        function openBrWindow(sAddr, sPopName, sWidth, sHeight) {

            //window.open("http://eschyles.mireene.com/", "", "");  //속성 지정하지 않은 기본창

            if (screen.width < 1025) {
                LeftPosition = 0;
                TopPosition = 0;
            } else {
                LeftPosition = (screen.width) ? (screen.width - sWidth) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - sHeight) / 2 : 100;
            }

            //sAddr = '?iTableIndex=1010&iNoIndex=51508&iBoardIdx=91849&b_num=93744&re_level=0&ref=93744&re_step=0&pageIdx=0&searchOption=all&searchTxt='

            //window.open(sAddr, "", "");
            window.open(sAddr, sPopName, "width=" + sWidth + ",height=" + sHeight + ",top=" + TopPosition + ",left=" + LeftPosition + ", scrollbars=yes, toolbar=no menubar=no, location=no");
        }

        function SetNoticeNum(sMaxNum, sNowNum) {
            document.getElementById('txtMaxNotice').value = sMaxNum;
            document.getElementById('txtNowNotice').value = sNowNum;
        }

        function SetMoveNoticeNum(sMoveValue) {
            if (Number(document.getElementById('txtNowNotice').value) == '') {
                return false;
            }

            if (sMoveValue == "0") {
                document.getElementById('txtNowNotice').value = Number(document.getElementById('txtNowNotice').value) - 1;
            }

            else {
                document.getElementById('txtNowNotice').value = Number(document.getElementById('txtNowNotice').value) + 1;
            }
        }

        function SetMousePointer(sCorrID, sIdx, sMateType, sType, sTitle) {
            var qa = document.getElementById("tr" + sIdx);

            if (sTitle != "오신고알람" && sTitle != "번역오역반송" && sTitle != "편지 신고") {
                if (sCorrID != '') {
                    if (sMateType != 'TRANSBEFORE') {
                        if (sType == 1) {
                            qa.style.cursor = 'pointer'
                        }
                        else {
                            qa.style.cursor = ''
                        }
                    }
                }
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
<div id="container" class="mate">

	<!-- wrapper -->
	<div id="wrapper" class="share our-story">

		<!-- sidebar -->
        <uc5:TranslationRight ID="TranslationRight" runat="server" />
		<!-- //sidebar -->
		<!-- contents -->
		<div id="contents">
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><strong>홈</strong>
			</div>
            <input type="hidden" id="txtMaxNotice" name="txtMaxNotice"/>
            <input type="hidden" id="txtNowNotice" name="txtNowNotice"/>
            <table>
                <tr>
                    <td style='width:60px; height:22px; font-weight:bold;'>
                    &nbsp;공지사항
                    </td>
                    <td style="vertical-align:top">
                    &nbsp
                        <asp:ImageButton ID="imgBtnPrev" runat="server" ImageUrl="~/image/imate/btn/btn_back.jpg" Height="20px" />
                        <asp:ImageButton ID="ImgBtnForw" runat="server" ImageUrl="~/image/imate/btn/btn_next.jpg" Height="20px"/>
                    </td>
                </tr>
            </table>
            <table style='width:100%;'>
				<tr>
					<th style='background-color:#E6E6E6; width:100px; height:20px; border:1px solid #CCCCCC; word-break:break-all;'>공지번호</th>
					<td colspan="3" style='word-break:break-all; border:1px solid #CCCCCC;'>&nbsp<asp:Label ID="lblNoticeNum" runat="server" Text=""></asp:Label></td>
				</tr>
				<tr>
					<th style='background-color:#E6E6E6; width:100px; height:20px; border:1px solid #CCCCCC;'>제목</th>
					<td colspan="3" style='word-break:break-all; border:1px solid #CCCCCC;'>&nbsp<asp:Label ID="lblNoticeTitle" runat="server" Text="" ></asp:Label></td>
                    
				</tr>
				<tr>
					<%--<th style='background-color:#E6E6E6; width:15%; height:30px; border:1px solid #CCCCCC;'>중요도</th>--%>
					<%--<td style='word-break:break-all; border:1px solid #CCCCCC;'><asp:Label ID="lblNoticeImportType" runat="server" Text=""></asp:Label></td>--%>
                    <th style='background-color:#E6E6E6; width:100px; height:20px; border:1px solid #CCCCCC;'>등록일</th>
					<td style='word-break:break-all; width:278px; border:1px solid #CCCCCC;'>&nbsp<asp:Label ID="lblNoticeWriteDate" runat="server" Text=""></asp:Label></td>
                    <th style='background-color:#E6E6E6; width:100px; height:20px; border:1px solid #CCCCCC;'>등록자</th>
					<td style='word-break:break-all; width:278px; border:1px solid #CCCCCC;'>&nbsp<asp:Label ID="lblNoticeWriteUser" runat="server" Text=""></asp:Label></td>
				</tr>
                <tr>
                
                
					<th style='background-color:#E6E6E6; width:100px; height:200px; border:1px solid #CCCCCC;'>내용</th>
					<td colspan="3" style='word-break:break-all; border:1px solid #CCCCCC;'>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Panel ID="pnlNoticeContent" runat="server" Width="756px" Height="200px" BorderWidth="0" ScrollBars="Vertical" style="padding:4px" >
                                <p><asp:Literal ID="txtNoticeContent" runat="server"></asp:Literal></p>
                                <%--<textarea ID="txtNoticeContent" runat="server" width="500px" height="200px" ></textarea>--%>
                                </asp:Panel>
                            </td>
                        </tr>
                        </table>
                    </td>
				</tr>
		    </table>
            <div id="divComment">
            </br>
            <table>
                <tr>
                    <td style="font-weight:bold;">
                    &nbsp;Comments
                    </td>
                </tr>
            </table>
            <table style='width:100%;'>
                <thead>
                    <tr>
                        <th style='background-color:#E6E6E6; width:10%; height:20px; border:1px solid #CCCCCC;'>
                        편지ID
                        </th>
                        <th style='background-color:#E6E6E6; width:15%; height:20px; border:1px solid #CCCCCC;'>
                        제목
                        </th>
                        <th style='background-color:#E6E6E6; width:45%; height:20px; border:1px solid #CCCCCC;'>
                        내용
                        </th>
                        <th style='background-color:#E6E6E6; width:15%; height:20px; border:1px solid #CCCCCC;'>
                        등록자
                        </th>
                        <th style='background-color:#E6E6E6; width:15%; height:20px; border:1px solid #CCCCCC;'>
                        등록일
                        </th>
                    </tr>
                </thead>
                <tbody>
                <asp:Repeater ID="CommentData" runat="server">
                    <ItemTemplate>
                        <tr id="tr<%#DataBinder.Eval(Container.DataItem, "Idx")%>" onmouseover="SetMousePointer('<%#DataBinder.Eval(Container.DataItem, "CorrID")%>','<%#DataBinder.Eval(Container.DataItem, "Idx")%>','<%#DataBinder.Eval(Container.DataItem, "MateType")%>','1','<%#DataBinder.Eval(Container.DataItem, "Title")%>')" onmouseout="SetMousePointer('<%#DataBinder.Eval(Container.DataItem, "CorrID")%>','<%#DataBinder.Eval(Container.DataItem, "Idx")%>','<%#DataBinder.Eval(Container.DataItem, "MateType")%>','0')" onclick="ShowDetail('<%#DataBinder.Eval(Container.DataItem, "CorrID")%>','<%#DataBinder.Eval(Container.DataItem, "PackageID")%>','<%#DataBinder.Eval(Container.DataItem, "CorrPage")%>','<%#DataBinder.Eval(Container.DataItem, "MateType")%>','<%#DataBinder.Eval(Container.DataItem, "Title")%>'); return false;" >
                            <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                                <center><%#DataBinder.Eval(Container.DataItem, "CorrID")%></center>
                            </td>
                            <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                                &nbsp<%#DataBinder.Eval(Container.DataItem, "Title")%>
                            </td>
                            <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                                &nbsp<%#DataBinder.Eval(Container.DataItem, "Content")%>
                            </td>
                            <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                                <center><%#DataBinder.Eval(Container.DataItem, "UserName")%></center>
                            </td>
                            <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                                <center><%#DataBinder.Eval(Container.DataItem, "InsertDate", "{0:yyyy-MM-dd}")%></center>
                            </td>
                        </tr>
                    </ItemTemplate> 
                </asp:Repeater>		
	            </tbody>
            </table>

            </div>

            <div id="divQnA" style="font-weight:bold;">
            </br>
            <table>
                <tr>
                    <td style="font-weight:bold;">
                    &nbsp;나의 질문내역 (최근 5개)
                    </td>
                </tr>
            </table>
            <table style='width:100%;'>
                <thead>
                    <tr>
                        <th style='background-color:#E6E6E6; width:10%; height:20px; border:1px solid #CCCCCC;'>
                        No
                        </th>
                        <th style='background-color:#E6E6E6; width:70%; height:20px; border:1px solid #CCCCCC;'>
                        제목
                        </th>
                        <th style='background-color:#E6E6E6; width:15%; height:20px; border:1px solid #CCCCCC;'>
                        등록일
                        </th>
                    </tr>
                </thead>
                <tbody>
                <asp:Repeater ID="QnaData" runat="server">
                    <ItemTemplate>
                    <tr style="cursor:pointer" onclick="openBrWindow('/translation/TranslationQnAView.aspx?iTableIndex=1010&iNoIndex=<%#DataBinder.Eval(Container.DataItem, "no_idx")%>&iBoardIdx=<%#DataBinder.Eval(Container.DataItem, "board_idx")%>&b_num=<%#DataBinder.Eval(Container.DataItem, "b_num")%>&re_level=<%#DataBinder.Eval(Container.DataItem, "re_level")%>&ref=<%#DataBinder.Eval(Container.DataItem, "ref")%>&re_step=<%#DataBinder.Eval(Container.DataItem, "re_step")%>&pageIdx=0&searchOption=all&searchTxt=','TranslationQnA','780','800'); return false;" >
                        <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                        <center><%#DataBinder.Eval(Container.DataItem, "no_idx")%></center>
                        </td>
                        <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                        &nbsp<%#DataBinder.Eval(Container.DataItem, "b_title")%>&nbsp<font color="#DB9700"><%#DataBinder.Eval(Container.DataItem, "ReplyCount")%></font>
                        </td>
                        <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                        <center><%#DataBinder.Eval(Container.DataItem, "b_writeday", "{0:yyyy-MM-dd}")%></center>
                        </td>
                    </tr>
                    </ItemTemplate> 
                </asp:Repeater>		
	            </tbody>
            </table>

            </div>
            <br />
            <div id="divTranslationCount">
            <table style='width:100%;'>
                <tr>
                 <%if (bThirdPL)
                   { %>
                    <td style='width:20%;'>
                    <%}
                   else
                   { %>
                    <td style='width:45%;'>
                    <%} %>
                        <table>
                            <%--김예인님 요청  iMATE 전체 번역현황 삭제,  나의 번역현황 위치 변경 수정 문희원 2017-04-05--%>
                            <%=sgTranHtml%> 
                        <%--<tr>
                                <td colspan="2" style="font-weight:bold;">&nbsp;iMATE 전체 번역현황</br>(어린이편지 및 후원자편지)
                                </td>
                            </tr>
                            <tr>
                                <th style='word-break:break-all; border:1px solid #CCCCCC; height:20px; width:50%;'>번역대기
                                </td>
                                <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;width:50%;'>
                                <center><asp:Label ID="lblTranslateAllNone" runat="server" Text=""></asp:Label></center>
                                </td>
                            </tr>
                            <tr>
                                <th style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>번역중
                                </td>
                                <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                                <center><asp:Label ID="lblTranslateAllIng" runat="server" Text=""></asp:Label></center>
                                </td>
                            </tr>
                            <tr>
                                <th style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>번역완료
                                </td>
                                <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                                <center><asp:Label ID="lblTranslateAllEnd" runat="server" Text=""></asp:Label></center>
                                </td>
                            </tr>--%>
                        </table>
                    
                    </td>
                    
                     <%if (bThirdPL)
                       { %>
                    <td style='width:5%;'>
                    </td>
                    <td style='width:20%;'>
                        <table>
                            <tr>
                                <td colspan="2" style="font-weight:bold;"><br />&nbsp;3PL 전체 번역현황
                                </td>
                            </tr>
                            <tr>
                                <th style='word-break:break-all; border:1px solid #CCCCCC; height:20px; width:50%;'>번역대기
                                </th>
                                <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;width:50%;'>
                                <center><asp:Label ID="lblThirdPLAllNone" runat="server" Text=""></asp:Label></center>
                                </td>
                            </tr>
                            <tr>
                                <th style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>번역중
                                </th>
                                <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                                <center><asp:Label ID="lblThirdPLAllIng" runat="server" Text=""></asp:Label></center>
                                </td>
                            </tr>
                            <tr>
                                <th style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>변역완료
                                </th>
                                <td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>
                                <center><asp:Label ID="lblThirdPLAllEnd" runat="server" Text=""></asp:Label></center>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <%} %>
                    <td style='width:5%;'>
                    </td>
                    <td  style='width:45%; vertical-align: bottom;'>
                        <%if (sgMateType != "2")
                       { %>
                        <table>
                            <%--김예인님 요청사항으로 위치 변경 문희원 수정 2017-04-05--%>
                            <th colspan="2" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>누적</th>
                            <th colspan="2" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>이번기수</th>
                            <th colspan="2" rowspan="2" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>
                                <center><asp:Label ID="lblClass" runat="server" Text=""></asp:Label></center>                           
                            </th>

                            <tr>
                                <td align="center"  style='word-break:break-all; border:1px solid #CCCCCC; height:40px;'>
                                    Excellent
                                </td>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    <center><asp:Label ID="lblLikeCount" runat="server" Text="0 개"></asp:Label></center>
                                </td>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    Excellent
                                </td>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    <center><asp:Label ID="lblLikeCount2" runat="server" Text="0 개"></asp:Label></center>
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;  height:20px;'>
                                    Good
                                </td>

                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    <center><asp:Label ID="lblSosoCount" runat="server" Text="0 개"></asp:Label></center>
                                </td>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    Good
                                </td>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    <center><asp:Label ID="lblSosoCount2" runat="server" Text="0 개"></asp:Label></center>
                                </td>
                            </tr>
                        
                        </table>
                        <%} %>
                    </td>
                </tr>
            </table>
            </div>
            <br />
            <%--<div>
            <table style='width:100%;'>
                <tr>
                 <%if (bThirdPL)
                   { %>
                    <td style='width:20%;'>
                    <%}
                   else
                   { %>
                    <td style='width:30%;'>
                    <%} %>
                        
                    </td>
                    
                     <%if (bThirdPL)
                       { %>
                    <td style='width:5%;'>
                    </td>
                    <td style='width:20%;'>
                        
                    </td>
                    <%} %>
                    <td style='width:5%;'>
                    </td>
                    <td  style='width:45%;'>
                        <table>
                            <th colspan="2" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>누적</th>
                            <th colspan="2" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>이번기수</th>
                            <th colspan="2" rowspan="3" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>
                                <center><asp:Label ID="lblClass" runat="server" Text=""></asp:Label></center>                           
                            </th>

                            <tr>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    Excellent
                                </td>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    <center><asp:Label ID="lblLikeCount" runat="server" Text="0 개"></asp:Label></center>
                                </td>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    Excellent
                                </td>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    <center><asp:Label ID="lblLikeCount2" runat="server" Text="0 개"></asp:Label></center>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    Good
                                </td>

                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    <center><asp:Label ID="lblSosoCount" runat="server" Text="0 개"></asp:Label></center>
                                </td>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    Good
                                </td>
                                <td align="center" style='word-break:break-all; border:1px solid #CCCCCC;'>
                                    <center><asp:Label ID="lblSosoCount2" runat="server" Text="0 개"></asp:Label></center>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </div>--%>

		</div>


	</div>
	

</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    <asp:hiddenfield ID="hidTableIdx" runat="server" Value ="1222"></asp:hiddenfield>    
    </form>
</body>
</html>
