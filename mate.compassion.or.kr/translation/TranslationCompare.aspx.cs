﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Data;

public partial class Mate_translation_TranslationCompare : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetCompareText();
    }

    private void SetCompareText()
    {
        string sCorrID = Request.QueryString["CorrID"];

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        StringBuilder sbList = new StringBuilder();

        sbList.Append("SELECT b.TranslationText, c.ScreeningText ");
        sbList.Append("FROM ");
        sbList.Append("CORR_Master a ");
        sbList.Append("INNER JOIN  ");
        sbList.Append("CORR_Translate b ");
        sbList.Append("ON a.CorrID = b.CorrID AND a.TranslationTimes = b.TranslationTimes ");
        sbList.Append("INNER JOIN  ");
        sbList.Append("Corr_Screening c ");
        sbList.Append("ON a.CorrID = c.CorrID AND a.ScreeningTimes = c.ScreeningTimes ");
        sbList.Append("WHERE a.CorrID = '" + sCorrID + "' ");

        //데이터 바인딩[0]:편지 글 및 상태 [1]:신고사유 리스트
        Object[] objSql = new object[1] { sbList.ToString() };

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

        //sTemplate = dr["Template"].ToString();
        //sTemplate = sTemplate.Replace("''", "'");
        ////sTemplate = sContents.Replace("<", "&lt;");
        ////sTemplate = sContents.Replace(">", "&gt;");
        //sTemplate = sTemplate.Replace("\r\n", "<br/>");
        //sTemplate = sTemplate.Replace("\n", "<br/>");

        string[] arrsDms = new string[] { "|n" };

        string sTranslate = ds.Tables[0].Rows[0]["TranslationText"].ToString().Replace("\n", "|n").Replace("\r\n", "|n");
        string[] arrsTranslate = sTranslate.Split(arrsDms, StringSplitOptions.None);

        string sScreening = ds.Tables[0].Rows[0]["ScreeningText"].ToString().Replace("\n", "|n").Replace("\r\n", "|n");
        string[] arrsScreening = sScreening.Split(arrsDms, StringSplitOptions.None);

        StringBuilder sbHtml = new StringBuilder();

        sbHtml.Append("<table><tr><td>");

        for (int iCnt = 0; iCnt < arrsScreening.Length; iCnt++)
        {
            if (iCnt < arrsTranslate.Length)
                sbHtml.Append(arrsScreening[iCnt] + "<br/>");

            else
                sbHtml.Append(arrsScreening[iCnt] + "<br/>");
        }

        sbHtml.Append("</td></tr></table>");

        this.txtTranslate.Text = ds.Tables[0].Rows[0]["TranslationText"].ToString().Replace("\n", "<br/>");
        this.txtScreening.Text = ds.Tables[0].Rows[0]["ScreeningText"].ToString().Replace("\n", "<br/>"); //sbHtml.ToString();

        ds.Dispose();
        _WWW6Service.Dispose();
    }
}