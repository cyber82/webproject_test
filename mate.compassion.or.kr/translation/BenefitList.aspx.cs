﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
 
public partial class Mate_nanum_BenefitList : System.Web.UI.Page
{
    int _PAGESIZE = 10;
    int _tableIdx;
    protected string _pageIdx = "0";

    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack)
        {
            InitPage();
            
            txtSearch.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSeach.ClientID + "').click();return false;}} else {return true}; ");
            
            _pageIdx = Request.QueryString["pageIdx"];
           
            if (String.IsNullOrEmpty(_pageIdx))
                _pageIdx = "0";

            string option = Request.QueryString["option"];
            string searchTxt = Request.QueryString["dropNation"];  
            string orderby = Request.QueryString["orderby"];

            if (!String.IsNullOrEmpty(option))
                DataBind(Convert.ToInt32(_pageIdx), option, searchTxt,orderby);            
        }
    }

    private void InitPage()
    {
        WWWService.Service service = new WWWService.Service();
        DataSet data = service.GetMateNationList(57);

        //DataRow row = data.Tables[0].NewRow();
        //row["title"] = "전체";
        //row["idx"] = 0;
        //data.Tables[0].Rows.InsertAt(row, 0); 

        dropNation.DataTextField = "title";
        dropNation.DataValueField = "idx";
        dropNation.DataSource = data.Tables[0];
        dropNation.DataBind();

        data.Dispose();
        service.Dispose();
    }

    private void PagingSetting(int totalCount, int curPage)
    {
        PagingHelper1.VirtualItemCount = totalCount;
        PagingHelper1.PageSize = _PAGESIZE;
        PagingHelper1.CurrentPageIndex = curPage;
    }

    protected void PagingHelper1_OnPageIndexChanged(object sender, TaeyoNetLib.PagingEventArgs e)
    {
        string option = Request.QueryString["option"];
        string searchTxt = Request.QueryString["dropNation"];
        string orderby = Request.QueryString["orderby"];

        if (!String.IsNullOrEmpty(option))
            DataBind(e.PageIndex, option, searchTxt, orderby);       
        else
            DataBind(e.PageIndex, SearchOption.Value, txtSearch.Value,"");

        _pageIdx = e.PageIndex.ToString();
    }

    private void DataBind(int idxPage, string searchOption, string searchStr, string orderBy)
    {
        WWWService.Service _wwwService = new WWWService.Service();
        DataSet data;
        data = _wwwService.GetBenefitList(searchOption, searchStr, orderBy);

        PagedDataSource pds = new PagedDataSource();
        pds.DataSource = data.Tables[0].DefaultView;
        pds.AllowPaging = true;
        pds.PageSize = _PAGESIZE;
        pds.CurrentPageIndex = idxPage;
        repData.DataSource = pds;
        repData.DataBind();

        PagingSetting(data.Tables[0].Rows.Count, idxPage);

        data.Dispose();
        _wwwService.Dispose();
    }

    protected void repData_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label labNew = (Label)e.Item.FindControl("labNew");

        HiddenField hidb_writeday = (HiddenField)e.Item.FindControl("hidb_writeday");
        DateTime dt = DateTime.Parse(hidb_writeday.Value);
        TimeSpan ts = DateTime.Now - dt;

        if (ts.TotalMinutes >= 10080)
            labNew.Visible = false;
    }
}