﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mate_translation_TranslationDetailWidthFrame : System.Web.UI.Page
{

    public string sgCorrID;
    public string sgPackageID;
    public string sgPageCount;
    public string sgFilePath;
    public string sgChildKey;
    public string sgThirdYear;
    public string sgProjectNumber;
    public string sgSearchKind;
    public string sgSearchText;

	public bool isMobile = false;

	protected void Page_Load(object sender, EventArgs e)
    {
        sgCorrID = Request.QueryString["CorrID"].ToString();
        sgPackageID = Request.QueryString["PackageID"].ToString();
        sgPageCount = Request.QueryString["PageCount"].ToString();
        sgFilePath = Request.QueryString["FilePath"].ToString();
        sgChildKey = Request.QueryString["ChildKey"].ToString();
        sgThirdYear = Request.QueryString["ThirdYear"].ToString();
        sgProjectNumber = Request.QueryString["ProjectNumber"].ToString();
        sgSearchKind = Request.QueryString["SearchKind"];
        sgSearchText = Request.QueryString["SearchText"];

		string userAgent = Request.UserAgent;

		String[] browser = { "iphone", "ipod", "ipad", "android", "blackberry", "windows ce", "nokia", "webos", "opera mini", "sonyericsson", "opera mobi", "iemobile", "windows phone" };
		for (int i = 0; i < browser.Length; i++) {
			if (userAgent.ToLower().Contains(browser[i]) == true) {
				isMobile = true;
				break;
			}
		}
	}
}