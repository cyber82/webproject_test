﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Data;

public partial class Mate_translation_TranslationHome : System.Web.UI.Page
{
    public string sgMateType;

    public string sgTranHtml;
    public bool bThirdPL;
    public bool bScreening;
    public bool bTranslate;

    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        string sJs;

        if (UserInfo.IsLogin)
        {
            string sUserId = new UserInfo().UserId;

            //권한 체크
            Object[] objSql = new object[1] { "MATE_MateYearCheck" };
            Object[] objParam = new object[2] { "MateID", "MateYearTypeCode" };
            Object[] objValue = new object[2] { sUserId, "" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //권한체크
            if (data.Tables[0].Rows.Count > 0)
            {
                //권한 체크
                objSql = new object[1] { "MATE_MateYearCheck" };
                objParam = new object[2] { "MateID", "MateYearTypeCode" };
                Object[] objThirdPLValue = new object[2] { new UserInfo().UserId, "31" };

                Object[] objScreeningValue = new object[2] { new UserInfo().UserId, "21" };
                Object[] objTranslateValue = new object[2] { new UserInfo().UserId, "11" };

                //3PL 권한체크
                if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objThirdPLValue).Tables[0].Rows.Count > 0)
                {
                    //new TranslateInfo().ThirdPLCheck = "true";
                    bThirdPL = true;
                }

                //스크리닝 권한체크
                if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objScreeningValue).Tables[0].Rows.Count > 0)
                {
                    //new TranslateInfo().ScreeningCheck = "true";
                    bScreening = true;
                }

                //번역 권한체크
                if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objTranslateValue).Tables[0].Rows.Count > 0)
                {
                    //new TranslateInfo().TranslateCheck = "true";
                    bTranslate = true;
                }

                _WWW6Service.Dispose();

                if (bThirdPL || bScreening || bTranslate)
                {
                    new TranslateInfo().MateYearValue = data.Tables[0].Rows[0][0].ToString();

                    this.imgBtnPrev.Attributes.Add("onclick", "return SetMoveNoticeNum('0')");
                    this.ImgBtnForw.Attributes.Add("onclick", "return SetMoveNoticeNum('1')");

                    //페이지 타입 쿠키 저장
                    new TranslateInfo().MatePageType = "00";

                    //공지사항 셋팅
                    SetNoticeList();

                    //Comment 내역 셋팅
                    SetCommentList();

                    //나의 질문내역 셋팅
                    SetQnaList();

                    //전체번역현황 셋팅
                    //문희원 수정 2017-04-05 김예인님 요청 iMATE 전체 번역현황 미표시
                    //SetTranslateAllCountList();

                    //3PL 전체번역현황
                    SetThirdPLAllCountList();

                    //내 번역 및 스크리닝 현황
                    SetMyTransCountList();
                }

                else
                    Response.Redirect("/default.aspx");
            }

            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                sb.Append("alert('해당 권한이 없습니다.'); location.href='http://" + Request.Url.Authority + "/';");
                sb.Append("</script>");

                Response.Write(sb.ToString());
            }
        }

        else
        {
            string sReturnUrl = Request.Url.AbsoluteUri.Replace("http://" + Request.Url.Authority, "");
            sReturnUrl = sReturnUrl.Replace("https://" + Request.Url.Authority, "");

            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("로그인 후 사용 가능합니다.");
            sJs += JavaScript.GetPageMoveScript("/login.aspx");
            sJs += JavaScript.GetHistoryBackScript();
            sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }
    }

    #region 공지사항 셋팅
    /// <summary>
    /// 공지사항 셋팅
    /// </summary>
    private void SetNoticeList()
    {
        StringBuilder sbList = new StringBuilder();
        DataSet data;

        string sNowNotice = Request["txtNowNotice"];
        string sMaxNotice = Request["txtMaxNotice"];

        //sbList.Append("SELECT TOP 1 no_idx, b_title, CONVERT(NVARCHAR(10),b_writeday,121)  AS b_writeday, b_name, b_content ");
        //sbList.Append("FROM mate_UpBoard where table_idx = '1001' ");
        //sbList.Append("ORDER BY no_idx DESC ");

        sbList.Append("SELECT (ROW_NUMBER() over(order by no_idx)) AS Idx, no_idx, b_title, CONVERT(NVARCHAR(10),b_writeday,121) AS b_writeday, b_name, b_content ");
        sbList.Append("FROM mate_UpBoard WITH (NOLOCK) WHERE table_idx = '1001' ");
        sbList.Append("AND (mate_type = 'imate' OR mate_type = 'imate-p') ");
        sbList.Append("ORDER BY no_idx DESC ");

        //Response.Write(sbList.ToString());
        Object[] objSql = new object[1] { sbList.ToString() };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        data = _WWW6Service.NTx_ExecuteQuery("SqlCompassWeb4", objSql, "TEXT", null, null);

        if (data.Tables[0].Rows.Count < 2)
        {
            //이전버튼
            this.imgBtnPrev.Visible = false;
            //다음버튼
            this.ImgBtnForw.Visible = false;
        }

        StringBuilder sb = new StringBuilder();

        if (string.IsNullOrEmpty(sNowNotice))
        {
            if (data.Tables[0].Rows.Count > 0)
            {
                sb = new StringBuilder();
                sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                sb.Append("SetNoticeNum('" + data.Tables[0].Rows[0]["idx"].ToString() + "','" + data.Tables[0].Rows[0]["idx"].ToString() + "');");
                sb.Append("</script>");

                this.RegisterStartupScript("TranslationHomeEmpty", sb.ToString());

                string sContents = data.Tables[0].Rows[0]["b_content"].ToString();
                sContents = sContents.Replace("''", "'");
                //sContents = sContents.Replace("<", "&lt;");
                //sContents = sContents.Replace(">", "&gt;");
                //sContents = sContents.Replace("\r\n", "<br/>");
                //sContents = sContents.Replace("\n", "<br/>");

                this.lblNoticeNum.Text = data.Tables[0].Rows[0]["no_idx"].ToString();//공지번호
                this.lblNoticeTitle.Text = data.Tables[0].Rows[0]["b_title"].ToString();//제목
                this.lblNoticeWriteDate.Text = data.Tables[0].Rows[0]["b_writeday"].ToString();//등록일
                this.lblNoticeWriteUser.Text = data.Tables[0].Rows[0]["b_name"].ToString();//등록자
                this.txtNoticeContent.Text = sContents;//내용
            }

            //다음버튼
            this.ImgBtnForw.Visible = false;
        }

        else
        {
            //Response.Write(sNowNotice);
            //Response.End();

            if (data.Tables[0].Rows.Count > 0)
            {
                sb = new StringBuilder();
                sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                sb.Append("SetNoticeNum('" + data.Tables[0].Rows[0]["idx"].ToString() + "','" + sNowNotice + "');");
                sb.Append("</script>");

                this.RegisterStartupScript("TranslationHome", sb.ToString());

                string sContents = data.Tables[0].Select("Idx='" + sNowNotice + "'")[0]["b_content"].ToString();
                sContents = sContents.Replace("''", "'");
                //sContents = sContents.Replace("<", "&lt;");
                //sContents = sContents.Replace(">", "&gt;");
                //sContents = sContents.Replace("\r\n", "<br/>");
                //sContents = sContents.Replace("\n", "<br/>");

                this.lblNoticeNum.Text = data.Tables[0].Select("Idx='" + sNowNotice + "'")[0]["no_idx"].ToString();//공지번호
                this.lblNoticeTitle.Text = data.Tables[0].Select("Idx='" + sNowNotice + "'")[0]["b_title"].ToString();//제목
                this.lblNoticeWriteDate.Text = data.Tables[0].Select("Idx='" + sNowNotice + "'")[0]["b_writeday"].ToString();//등록일
                this.lblNoticeWriteUser.Text = data.Tables[0].Select("Idx='" + sNowNotice + "'")[0]["b_name"].ToString();//등록자
                this.txtNoticeContent.Text = sContents;//내용
            }

            if (data.Tables[0].Rows.Count > 1)
            {
                if (sNowNotice == sMaxNotice)
                {
                    //이전버튼
                    this.imgBtnPrev.Visible = true;
                    //다음버튼
                    this.ImgBtnForw.Visible = false;
                }

                else if (sNowNotice == "1")
                {
                    //이전버튼
                    this.imgBtnPrev.Visible = false;
                    //다음버튼
                    this.ImgBtnForw.Visible = true;
                }

                else
                {
                    //이전버튼
                    this.imgBtnPrev.Visible = true;
                    //다음버튼
                    this.ImgBtnForw.Visible = true;
                }
            }
        }

        data.Dispose();
        _WWW6Service.Dispose();
    }
    #endregion

    #region Comment 내역 셋팅
    /// <summary>
    /// Comment 내역 셋팅
    /// </summary>
    private void SetCommentList()
    {
        try
        {
            string sUserId = new UserInfo().UserId;

            StringBuilder sbList = new StringBuilder();
            DataSet data;

            Object[] objSql = new object[1] { "MATE_CommentList" };
            Object[] objParam = new object[1] { "MateID" };
            Object[] objValue = new object[1] { sUserId };

            //Object[] objSql = new object[1] { "MATE_CommentList_Old" };
            //Object[] objParam = new object[1] { "MateID" };
            //Object[] objValue = new object[1] { sUserId };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            PagedDataSource pds = new PagedDataSource();
            pds.DataSource = data.Tables[0].DefaultView;
            pds.AllowPaging = true;
            CommentData.DataSource = pds;
            CommentData.DataBind();

            data.Dispose();
            _WWW6Service.Dispose();
        }

        //Exception Error
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region 나의 질문내역 셋팅
    /// <summary>
    /// 나의 질문내역 셋팅
    /// </summary>
    private void SetQnaList()
    {
        try
        {
            string sUserId = new UserInfo().UserId;

            StringBuilder sbList = new StringBuilder();
            DataSet data;

            sbList.Append("SELECT TOP 5 board_idx, b_title, b_writeday, no_idx, b_num, re_level, ref, re_step, ");
            sbList.Append("      (SELECT CASE WHEN COUNT(*) = 0 THEN NULL ELSE '(' + CONVERT(NVARCHAR(5), COUNT(*)) + ')' END ReplyCount  ");
            sbList.Append("       FROM mate_upboard_comment WITH (NOLOCK) ");
            sbList.Append("       WHERE table_idx = '1010'  AND seqno = mate_UpBoard.no_idx AND ref = mate_UpBoard.ref) AS ReplyCount  ");
            sbList.Append("FROM mate_UpBoard  WITH (NOLOCK) ");
            sbList.Append("WHERE table_idx = '1010' AND user_id = '" + sUserId + "'  ");
            sbList.Append("ORDER BY no_idx DESC  ");

            //[0]:번역대기, [1]:번역중, [2]:번역완료
            Object[] objSql = new object[1] { sbList.ToString() };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            data = _WWW6Service.NTx_ExecuteQuery("SqlCompassWeb4", objSql, "TEXT", null, null);

            PagedDataSource pds = new PagedDataSource();

            pds.DataSource = data.Tables[0].DefaultView;
            pds.AllowPaging = true;

            QnaData.DataSource = pds;
            QnaData.DataBind();

            data.Dispose();
            _WWW6Service.Dispose();
        }

        //Exception Error
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region iMATE 전체번역현황
    /// <summary>
    /// iMATE 전체번역현황
    /// </summary>
    private void SetTranslateAllCountList()
    {
        try
        {
            DataSet data;

            //수정 2013-09-10
            StringBuilder sb1 = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            StringBuilder sb3 = new StringBuilder();

            //sb.Append("SELECT COUNT(TranslationStatus) FROM CORR_Master ");
            //sb.Append("WHERE TranslationStatus = 'NONE' AND TranslationNeed = 'Y' AND ScanningDate IS NOT NULL ");
            //sb.Append("AND CorrType <> 'CHIDEP' ");
            //sb.Append("AND LEFT(CorrType,3) IN('SPN','XCS','CHI') ");
            //sb.Append("AND CorrID = ISNULL(RepCorrID,'') ");

            //번역대기
            sb1.Append("SELECT COUNT(TranslationStatus) FROM CORR_Master WITH (NOLOCK) ");
            sb1.Append("WHERE TranslationStatus = 'NONE' AND   TranslationNeed = 'Y' ");
            sb1.Append("AND   CorrType <> 'CHIDEP'       AND   LEFT(CorrType,3) IN('SPN','XCS','CHI') ");
            sb1.Append("AND   CorrID = RepCorrID ");

            //번역중
            sb2.Append("SELECT COUNT(TranslationStatus) FROM CORR_Master WITH (NOLOCK) WHERE TranslationStatus = 'START' AND TranslationNeed = 'Y'");

            //번역완료
            sb3.Append("SELECT COUNT(TranslationStatus) FROM CORR_Master WITH (NOLOCK) WHERE TranslationStatus = 'END' AND TranslationNeed = 'Y'");

            //[0]:번역대기, [1]:번역중, [2]:번역완료
            Object[] objSql = new object[3] { sb1.ToString(), sb2.ToString(), sb3.ToString() };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            //수정 2013-09-11  천단위 콤마


            data.Dispose();
            _WWW6Service.Dispose();
        }

        //Exception Error
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region 3PL 전체번역현황
    /// <summary>
    /// 3PL 전체번역현황
    /// </summary>
    private void SetThirdPLAllCountList()
    {
        try
        {
            DataSet data;

            //[0]:번역대기, [1]:번역중, [2]:번역완료
            Object[] objSql = new object[3] { "SELECT COUNT(ProjectNumber) FROM ThirdPL_Master WHERE TranslateStatus = 'NONE'",
                " SELECT COUNT(ProjectNumber) FROM ThirdPL_Master WHERE TranslateStatus = 'START'",
                " SELECT COUNT(ProjectNumber) FROM ThirdPL_Master WHERE TranslateStatus = 'END'" };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            //수정 2013-09-11  천단위 콤마
            this.lblThirdPLAllNone.Text = string.Format("{0:#,###}", Convert.ToInt32(data.Tables[0].Rows[0][0].ToString()));//번역대기
            this.lblThirdPLAllIng.Text = string.Format("{0:#,###}", Convert.ToInt32(data.Tables[1].Rows[0][0].ToString()));//번역중
            this.lblThirdPLAllEnd.Text = string.Format("{0:#,###}", Convert.ToInt32(data.Tables[2].Rows[0][0].ToString()));//번역완료

            data.Dispose();
            _WWW6Service.Dispose();
        }

        //Exception Error
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region 내 번역 및 스크리닝 현황
    /// <summary>
    /// 내 번역 및 스크리닝 현황
    /// </summary>
    private void SetMyTransCountList()
    {
        try
        {
            //////////////////////////////////////////스크리닝/////////////////////////////////////////////
            StringBuilder sb = new StringBuilder();

            string sUserId = new UserInfo().UserId;

            StringBuilder sbSEnd = new StringBuilder();
            StringBuilder sbSIng = new StringBuilder();
            StringBuilder sbSCaution = new StringBuilder();

            //누적 스크리닝완료
            //수정 2013-09-10
            //sbSEnd.Append("SELECT COUNT(ScreeningStatus) FROM CORR_Master WHERE ScreeningStatus = 'END' AND ScreeningID = '" + sUserId + "' ");
            sbSEnd.Append("SELECT COUNT(ScreeningStatus) FROM CORR_Master WITH (NOLOCK) ");
            sbSEnd.Append("WHERE TranslationStatus = 'END' AND ScreeningStatus = 'END' AND ScreeningID = '" + sUserId + "' ");
            sbSEnd.Append("AND CorrID = RepCorrID ");

            //금주 스크리닝완료
            //수정 2013-09-10
            //sbSIng.Append("SELECT COUNT(ScreeningStatus) FROM CORR_Master ");
            //sbSIng.Append("WHERE   ");
            //sbSIng.Append("(CONVERT(DATE, GETDATE() - DATEPART(DW, GETDATE())+2, 121) <= ScreeningDate ");
            //sbSIng.Append("AND ScreeningDate <= CONVERT(DATE, GETDATE() - DATEPART(DW, GETDATE())+8, 121))  ");
            //sbSIng.Append("AND ScreeningStatus = 'END'  ");
            //sbSIng.Append("AND ScreeningID = '" + sUserId + "' ");

            sbSIng.Append("DECLARE @DE DATE; ");
            sbSIng.Append("SET @DE = CONVERT(DATE, GETDATE()); ");
            sbSIng.Append("IF   DATEPART(DW, @DE) = 1   ");
            sbSIng.Append("      SET @DE = DATEADD(D, -6,@DE); ");
            sbSIng.Append("ELSE ");
            sbSIng.Append("      SET @DE = DATEADD(D, +2 -DATEPART(DW,@DE),@DE); ");
            sbSIng.Append("SELECT COUNT(ScreeningStatus) FROM CORR_Master WITH (NOLOCK) ");
            sbSIng.Append("WHERE ScreeningDate  BETWEEN @DE AND DATEADD(D, +6,@DE) ");
            sbSIng.Append("AND   TranslationStatus = 'END' ");
            sbSIng.Append("AND   ScreeningStatus = 'END' ");
            sbSIng.Append("AND   ScreeningID = '" + sUserId + "' ");
            sbSIng.Append("AND CorrID = RepCorrID ");

            //Response.Write(sbSIng.ToString());

            //경고수
            //주석처리 2013-09-10
            //sbSCaution.Append("SELECT ISNULL(MAX(CautionSeq),0) FROM MATE_Caution WHERE MateYearCode = dbo.uf_GetMateYearCode(2) AND MateID = '" + sUserId + "'");

            //////////////////////////////////////////번역/////////////////////////////////////////////
            StringBuilder sbTEnd = new StringBuilder();
            StringBuilder sbTIng = new StringBuilder();
            StringBuilder sbTCaution = new StringBuilder();

            //누적 번역완료
            //수정 2013-09-10
            //sbTEnd.Append("SELECT COUNT(TranslationStatus) FROM CORR_Master WHERE TranslationStatus = 'END' AND TranslationID = '" + sUserId + "' ");

            sbTEnd.Append("SELECT COUNT(TranslationStatus) FROM CORR_Master WITH (NOLOCK) ");
            sbTEnd.Append("WHERE TranslationStatus = 'END' AND TranslationID = '" + sUserId + "' ");
            sbTEnd.Append("AND CorrID = RepCorrID ");

            //금주 번역완료
            /* 주석처리 2013-08-26
            sbTIng.Append("SELECT COUNT(*) FROM CORR_Master ");
            sbTIng.Append("WHERE   ");
            sbTIng.Append("(CONVERT(DATE, GETDATE() - DATEPART(DW, GETDATE())+2, 121) <= TranslationDate ");
            sbTIng.Append("AND TranslationDate <= CONVERT(DATE, GETDATE() - DATEPART(DW, GETDATE())+8, 121))  ");
            sbTIng.Append("AND TranslationStatus = 'END'  ");
            sbTIng.Append("AND TranslationID = '" + sUserId + "' ");
            sbTing.Append("AND CorrID = RepCorrID ");
            
            */
            //수정 2013-08-26
            sbTIng.Append("DECLARE @DD DATE; ");
            sbTIng.Append("SET @DD = CONVERT(DATE, GETDATE()); ");
            sbTIng.Append("IF   DATEPART(DW, @DD) = 1 ");
            sbTIng.Append("      SET @DD = DATEADD(D, -6,@DD); ");
            sbTIng.Append("ELSE  ");
            sbTIng.Append("      SET @DD = DATEADD(D, +2 -DATEPART(DW,@DD),@DD); ");
            sbTIng.Append("SELECT COUNT(*) FROM CORR_Master WITH (NOLOCK) ");
            sbTIng.Append("WHERE TranslationDate  BETWEEN @DD AND DATEADD(D, +6,@DD) ");
            sbTIng.Append("AND TranslationStatus = 'END'  ");
            sbTIng.Append("AND TranslationID = '" + sUserId + "' ");
            sbTIng.Append("AND CorrID = RepCorrID ");

            //경고수
            //수정 2013-09-10
            //sbTCaution.Append("SELECT ISNULL(CONVERT(INT,SUM(ET.VALUE1)),0) AS [CautionCount]  ");
            //sbTCaution.Append("FROM MATE_Caution MC3 ");
            //sbTCaution.Append("LEFT JOIN ETS_TC_CODE_MEMBER ET ON MC3.CautionCode = ET.MEM_CODE  ");
            //sbTCaution.Append("WHERE MC3.MateID = '" + sUserId + "' AND MC3.MateYearCode = dbo.uf_GetMateYearCode(1) ");

            sbTCaution.Append("SELECT ISNULL(CONVERT(INT,SUM(ET.VALUE1)),0) AS [CautionCount] ");
            sbTCaution.Append("FROM MATE_Caution MC3 WITH (NOLOCK) ");
            sbTCaution.Append("LEFT JOIN ETS_TC_CODE_MEMBER ET ON MC3.CautionCode = ET.MEM_CODE ");
            sbTCaution.Append("WHERE MC3.MateID = '" + sUserId + "' AND MC3.MateYearCode = dbo.uf_GetMateYearCode(1) ");

            //Response.Write(sbTIng.ToString());

            //[0]:누적 번역완료 , [1]:금주 번역진행 , [2]:경고수
            //[3]:누적 스크리닝완료 , [4]:금주 스크리닝진행 , [5]:경고수
            //수정 2013-09-10
            //Object[] objSql = new object[6] { sbTEnd.ToString(),
            //    sbTIng.ToString(),
            //    sbTCaution.ToString(),
            //    sbSEnd.ToString(),
            //    sbSIng.ToString(),
            //    sbSCaution.ToString()};

            Object[] objSql = new object[5] { sbTEnd.ToString(), sbTIng.ToString(), sbTCaution.ToString(), sbSEnd.ToString(), sbSIng.ToString() };

            DataSet data;
            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            //권한 체크
            objSql = new object[1] { "MATE_RightCheck" };
            Object[] objParam = new object[1] { "MateID " };
            Object[] objValue = new object[1] { sUserId };

            DataSet dataRight = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //권한체크 [0]:번역 및 스크리닝 [1]:번역 [2]:스크리닝 []:권한없음
            sgMateType = dataRight.Tables[0].Rows[0][0].ToString();

            //this.lblTranslateEnd.Text = data.Tables[0].Rows.Count.ToString() + "통";//번역대기
            //this.lblTranslateIng.Text = data.Tables[1].Rows.Count.ToString() + "통";//번역중
            //this.lblTranslateCaution.Text = data.Tables[2].Rows[0][0].ToString() + "개";//번역완료

            //권한체크 [0]:번역 및 스크리닝 [1]:번역 [2]:스크리닝 []:권한없음
            if (sgMateType == "0")
            {
                sb.Append("<tr>");
                sb.Append("<th colspan=\"2\" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>");
                sb.Append("누적");
                sb.Append("</th>");
                sb.Append("<th colspan=\"2\" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>");
                sb.Append("이번 주");
                sb.Append("</th>");
                sb.Append("<th style='background-color:#E6E6E6; width:20%; height:20px; border:1px solid #CCCCCC;'>");
                sb.Append("알람(Alarm)");
                sb.Append("</th>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<th style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:20px;'>번역완료");
                sb.Append("</td>");
                sb.Append("<td style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:20px;'>");
                sb.Append("<center><asp:Label ID=\"lblTranslateEnd\" runat=\"server\" Text=''></asp:Label>"
                    //수정 2013-09-11
                    + string.Format("{0:#,##0}", Convert.ToInt32(data.Tables[0].Rows[0][0].ToString().ValueIfNull("0"))) + "통</center>");
                sb.Append("</td>");
                sb.Append("<th style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:20px;'>번역완료");
                sb.Append("</td>");
                sb.Append("<td style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:20px;'>");
                sb.Append("<center><asp:Label ID=\"lblTranslateIng\" runat=\"server\" Text=''>"
                    //수정 2013-09-11
                    + string.Format("{0:#,##0}", Convert.ToInt32(data.Tables[1].Rows[0][0].ToString())) + "통</asp:Label></center>");
                sb.Append("</td>");
                sb.Append("<td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>");
                sb.Append("<center><asp:Label ID=\"lblTranslateCaution\" runat=\"server\" Text=''></asp:Label>"
                    //수정 2013-09-11
                    + string.Format("{0:#,##0}", Convert.ToInt32(data.Tables[2].Rows[0][0].ToString())) + "개</center>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<th style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>스크리닝완료");
                sb.Append("</td>");
                sb.Append("<td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>");
                sb.Append("<center><asp:Label ID=\"lblScreeningEnd\" runat=\"server\" Text=''></asp:Label>"
                    //수정 2013-09-11
                    + string.Format("{0:#,##0}", Convert.ToInt32(data.Tables[3].Rows[0][0].ToString())) + "통</center>");
                sb.Append("</td>");
                sb.Append("<th style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>스크리닝완료");
                sb.Append("</td>");
                sb.Append("<td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>");
                sb.Append("<center><asp:Label ID=\"lblScreeningIng\" runat=\"server\" Text=''>"
                    //수정 2013-09-11
                    + string.Format("{0:#,##0}", Convert.ToInt32(data.Tables[4].Rows[0][0].ToString())) + "통</asp:Label></center>");
                sb.Append("</td>");
                sb.Append("<td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>");
                sb.Append("<center><asp:Label ID=\"lblScreeningIngCaution\" runat=\"server\" Text=''></asp:Label></center>");
                sb.Append("</tr>");
            }

            if (sgMateType == "1")
            {
                sb.Append("<tr>");
                sb.Append("<th colspan=\"2\" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>");
                sb.Append("누적");
                sb.Append("</th>");
                sb.Append("<th colspan=\"2\" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>");
                sb.Append("이번 주");
                sb.Append("</th>");
                sb.Append("<th style='background-color:#E6E6E6; width:20%; height:20px; border:1px solid #CCCCCC;'>");
                sb.Append("알람(Alarm)");
                sb.Append("</th>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<th style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:20px;'>번역완료");
                sb.Append("</td>");
                sb.Append("<td style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:40px;'>");
                sb.Append("<center><asp:Label ID=\"lblTranslateEnd\" runat=\"server\" Text=''>"
                    //수정 2013-09-11
                    + string.Format("{0:#,##0}", Convert.ToInt32(data.Tables[0].Rows[0][0].ToString())) + "통</asp:Label></center>");
                sb.Append("</td>");
                sb.Append("<th style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:20px;'>번역완료");
                sb.Append("</td>");
                sb.Append("<td style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:40px;'>");
                sb.Append("<center><asp:Label ID=\"lblTranslateIng\" runat=\"server\" Text=''>"
                    //수정 2013-09-11
                    + string.Format("{0:#,##0}", Convert.ToInt32(data.Tables[1].Rows[0][0].ToString())) + "통</asp:Label></center>");
                sb.Append("</td>");
                sb.Append("<td style='word-break:break-all; border:1px solid #CCCCCC; height:20px;'>");
                sb.Append("<center><asp:Label ID=\"lblTranslateCaution\" runat=\"server\" Text=''>"
                    //수정 2013-09-11
                    + string.Format("{0:#,##0}", Convert.ToInt32(data.Tables[2].Rows[0][0].ToString())) + "개</asp:Label></center>");
                sb.Append("</td>");
                sb.Append("</tr>");
            }

            if (sgMateType == "2")
            {
                sb.Append("<tr>");
                sb.Append("<th colspan=\"2\" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>");
                sb.Append("누적");
                sb.Append("</th>");
                sb.Append("<th colspan=\"2\" style='background-color:#E6E6E6; width:40%; height:20px; border:1px solid #CCCCCC;'>");
                sb.Append("이번 주");
                sb.Append("</th>");
                sb.Append("<tr>");
                sb.Append("<th style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:20px;'>스크리닝완료");
                sb.Append("</td>");
                sb.Append("<td style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:20px;'>");
                sb.Append("<center><asp:Label ID=\"lblScreeningEnd\" runat=\"server\" Text=''></asp:Label>"
                    //수정 2013-09-11
                    + string.Format("{0:#,##0}", Convert.ToInt32(data.Tables[3].Rows[0][0].ToString())) + "통</center>");
                sb.Append("</td>");
                sb.Append("<th style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:20px;'>스크리닝완료");
                sb.Append("</td>");
                sb.Append("<td style='word-break:break-all; border:1px solid #CCCCCC; width:20%; height:20px;'>");
                sb.Append("<center><asp:Label ID=\"lblScreeningIng\" runat=\"server\" Text=''></asp:Label>"
                    //수정 2013-09-11
                    + string.Format("{0:#,##0}", Convert.ToInt32(data.Tables[4].Rows[0][0].ToString())) + "통</center>");
                sb.Append("</td>");
                sb.Append("</tr>");
            }

            sgTranHtml = sb.ToString();

            StringBuilder sbClass = new StringBuilder();
            StringBuilder sbEvaluateCnt = new StringBuilder();
            StringBuilder sbEvaluateCnt2 = new StringBuilder();

            sbClass.Append("SELECT CASE TranslationLevel WHEN 'AA' THEN '번역의 달인' ");
            sbClass.Append("                             WHEN 'AB' THEN '고수 메이트' ");
            sbClass.Append("                             WHEN 'AC' THEN '초보 메이트' ");
            sbClass.Append("                             WHEN 'EA' THEN '번역의 달인' ");
            sbClass.Append("                             WHEN 'EB' THEN '고수 메이트' ");
            sbClass.Append("                             WHEN 'EC' THEN '초보 메이트' END AS Class ");
            sbClass.Append("FROM  MATE_Master ");
            sbClass.Append("WHERE MateID = '" + sUserId + "' ");
            sbClass.Append("AND   Status = 'ACTIVE' ");
            sbClass.Append("AND   MateYearCode IN ( ");
            sbClass.Append("            SELECT MateYearCode ");
            sbClass.Append("            FROM   MATE_Recruit ");
            sbClass.Append("            WHERE  CONVERT(DATE, GETDATE()) BETWEEN WorkingStart AND WorkingEnd ");
            sbClass.Append("            AND    (RIGHT(MateYearCode,2) = '11' OR RIGHT(MateYearCode,2) = '12') ");
            sbClass.Append("      ) ");

            sbEvaluateCnt.Append("SELECT TranslationEvaluate, COUNT(TranslationEvaluate) AS Cnt ");
            sbEvaluateCnt.Append("FROM   CORR_Master ");
            sbEvaluateCnt.Append("WHERE  TranslationID = '" + sUserId + "' ");
            sbEvaluateCnt.Append("AND    CorrID = RepCorrID ");
            sbEvaluateCnt.Append("GROUP BY TranslationEvaluate ");

            sbEvaluateCnt2.Append("SELECT TranslationEvaluate, COUNT(TranslationEvaluate) AS Cnt ");
            sbEvaluateCnt2.Append("FROM   CORR_Master A ");
            sbEvaluateCnt2.Append("JOIN   MATE_Master B ");
            sbEvaluateCnt2.Append("ON     A.TranslationID = B.MateID ");
            sbEvaluateCnt2.Append("JOIN   MATE_Recruit C ");
            sbEvaluateCnt2.Append("ON     B.MateYearCode = C.MateYearCode ");
            sbEvaluateCnt2.Append("WHERE  B.MateYearCode = ( SELECT MateYearCode ");
            sbEvaluateCnt2.Append("                          FROM   MATE_Recruit ");
            sbEvaluateCnt2.Append("                          WHERE  CONVERT(DATE, GETDATE()) BETWEEN WorkingStart AND WorkingEnd ");
            sbEvaluateCnt2.Append("                          AND    (RIGHT(MateYearCode,2) = '11' OR RIGHT(MateYearCode,2) = '12') ) ");
            sbEvaluateCnt2.Append("AND    A.TranslationDate BETWEEN C.WorkingStart AND C.WorkingEnd ");
            sbEvaluateCnt2.Append("AND    A.TranslationID = '" + sUserId + "' ");
            sbEvaluateCnt2.Append("AND    A.CorrID = A.RepCorrID ");
            sbEvaluateCnt2.Append("GROUP BY A.TranslationEvaluate ");

            Object[] objSql2 = new object[3] { sbClass.ToString(), sbEvaluateCnt.ToString(), sbEvaluateCnt2.ToString() };

            DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "TEXT", null, null);

            if (ds.Tables[0].Rows.Count > 0)
                lblClass.Text = ds.Tables[0].Rows[0]["Class"].ToString();

            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
            {
                if (ds.Tables[1].Rows[i]["TranslationEvaluate"].ToString() == "1")
                    lblSosoCount.Text = ds.Tables[1].Rows[i]["Cnt"].ToString() + " 개";

                else if (ds.Tables[1].Rows[i]["TranslationEvaluate"].ToString() == "2")
                    lblLikeCount.Text = ds.Tables[1].Rows[i]["Cnt"].ToString() + " 개";
            }

            for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
            {
                if (ds.Tables[2].Rows[i]["TranslationEvaluate"].ToString() == "1")
                    lblSosoCount2.Text = ds.Tables[2].Rows[i]["Cnt"].ToString() + " 개";

                else if (ds.Tables[2].Rows[i]["TranslationEvaluate"].ToString() == "2")
                    lblLikeCount2.Text = ds.Tables[2].Rows[i]["Cnt"].ToString() + " 개";
            }

            dataRight.Dispose();
            data.Dispose();
            //ds.Dispose();
            _WWW6Service.Dispose();
        }

        //Exception Error
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}