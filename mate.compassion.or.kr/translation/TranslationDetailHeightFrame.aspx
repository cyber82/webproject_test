﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TranslationDetailHeightFrame.aspx.cs" Inherits="Mate_translation_TranslationDetailHeightFrame" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%
    if (isMobile) {
        %>
    <style>
        .iframe-wrap {
            width: 100%;
            
            position: relative;
            overflow-x: scroll;
            overflow-y: scroll;
            -webkit-overflow-scrolling: touch;
        }
        .inner {
            width: 100%;
            /* Android 이중 스크롤 방지 */
            position:absolute; top:0; left:0;
        }
        
    </style>
    <%
    }
%>
    
<%

    if (sgMatePageType == "3" || sgMatePageType == ""){

        if (isMobile) {
%>
            <div class="iframe-wrap" style="height:900px;border:0px;">
                <iframe class="inner" style="height:900px;" src="TranslationDetailImage.aspx?CorrID=<%=sgCorrID%>&PackageID=<%=sgPackageID%>&PageCount=<%=sgPageCount%>&FilePath=<%=sgFilePath%>&ChildKey=<%=sgChildKey%>&ThirdYear=<%=sgThirdYear%>&ProjectNumber=<%=sgProjectNumber%>" name="TranslationDetailImage" id="TranslationDetailImage" frameborder="1"scrolling="yes" marginwidth="0" marginheight="0"></iframe>
            </div>
            <div class="iframe-wrap" style="height:200px;border:0px;">
                <iframe class="inner" style="height:500px;" src="TranslationDetailBottom.aspx?SearchKind=<%=sgSearchKind%>&SearchText=<%=sgSearchText%>" name="TranslationDetailBottom" frameborder="1" scrolling="no" marginwidth="0" marginheight="0" style="width:100%;"></iframe>
            </div>
<%
        } else {
%>
    <frameset rows="80%,*" framespacing="15" frameborder="2" bordercolor="#9D9DA1">
        <frame src="TranslationDetailImage.aspx?CorrID=<%=sgCorrID%>&PackageID=<%=sgPackageID%>&PageCount=<%=sgPageCount%>&FilePath=<%=sgFilePath%>&ChildKey=<%=sgChildKey%>" name="TranslationDetailImage" id="TranslationDetailImage" frameborder="1"scrolling="auto" marginwidth="0" marginheight="0"/>
        <frame src="TranslationDetailBottom.aspx?SearchKind=<%=sgSearchKind%>&SearchText=<%=sgSearchText%>" name="TranslationDetailBottom" frameborder="no"  scrolling="auto" marginwidth="0" marginheight="0"/>
    </frameset>
     
<%
        }
%>
        
<%
    }else{
        if (isMobile) {
%>
            <div class="iframe-wrap" style="height:300px;border:0px;">
                <iframe class="inner" style="height:300px;" src="TranslationDetailImage.aspx?CorrID=<%=sgCorrID%>&PackageID=<%=sgPackageID%>&PageCount=<%=sgPageCount%>&FilePath=<%=sgFilePath%>&ChildKey=<%=sgChildKey%>&ThirdYear=<%=sgThirdYear%>&ProjectNumber=<%=sgProjectNumber%>" name="TranslationDetailImage" id="TranslationDetailImage"></iframe>
            </div>
            <div class="iframe-wrap" style="height:500px;border:0px;">
                <iframe class="inner" style="height:500px;" src="TranslationDetailWrite.aspx?CorrID=<%=sgCorrID%>&PackageID=<%=sgPackageID%>&PageCount=<%=sgPageCount%>&ThirdYear=<%=sgThirdYear%>&ProjectNumber=<%=sgProjectNumber%>&ShowValue=0" name="TranslationDetailWrite"></iframe>
            </div>
            <iframe src="TranslationDetailBottom.aspx" name="TranslationDetailBottom" frameborder="no"  scrolling="auto" marginwidth="0" marginheight="0"></iframe>
<%
    } else {
%>
    <frameset rows="35%,60%,*" framespacing="15" frameborder="2" bordercolor="#9D9DA1">
        <frame src="TranslationDetailImage.aspx?CorrID=<%=sgCorrID%>&PackageID=<%=sgPackageID%>&PageCount=<%=sgPageCount%>&FilePath=<%=sgFilePath%>&ChildKey=<%=sgChildKey%>&ThirdYear=<%=sgThirdYear%>&ProjectNumber=<%=sgProjectNumber%>" name="TranslationDetailImage" id="TranslationDetailImage" frameborder="1 "scrolling="auto" marginwidth="0" marginheight="0"/>
        <frame src="TranslationDetailWrite.aspx?CorrID=<%=sgCorrID%>&PackageID=<%=sgPackageID%>&PageCount=<%=sgPageCount%>&ThirdYear=<%=sgThirdYear%>&ProjectNumber=<%=sgProjectNumber%>&ShowValue=0" name="TranslationDetailWrite" frameborder="1" scrolling="no" marginwidth="0" marginheight="0"/>
        <frame src="TranslationDetailBottom.aspx" name="TranslationDetailBottom" frameborder="no"  scrolling="auto" marginwidth="0" marginheight="0"/>
    </frameset>
<%
        }
     } 
%>
 <noframes>
 <body>
 </body>
 </noframes>
</html>
