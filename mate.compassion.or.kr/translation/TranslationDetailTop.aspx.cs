﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;

public partial class Mate_translation_TranslationDetailTop : System.Web.UI.Page
{
    public string sgPageCount;

    protected void Page_Load(object sender, EventArgs e)
    {
        sgPageCount = Request.QueryString["PageCount"].ToString();

        imgBtnExtension.Attributes.Add("onClick", "ExpCntCheck(); return false;");//확대
        imgBtnReduction.Attributes.Add("onClick", "RedCntCheck(); return false;");//축소

        imgBtnLeft.Attributes.Add("onClick", "MoveLeft('" + sgPageCount + "'); return false;");//이전 이미지
        imgBtnRight.Attributes.Add("onClick", "MoveRight('" + sgPageCount + "'); return false;");//다음 이미지

        //StringBuilder sb = new StringBuilder();
        //sb.Append("<script type='text/javascript' language ='javascript' >\n ");
        //sb.Append("document.getElementById('txtImgNum').value = '" + sgPageCount + "' ");
        //sb.Append("</script>");

        //this.RegisterStartupScript("AssetsCheckSchedule_Write", sb.ToString());    

    }
}