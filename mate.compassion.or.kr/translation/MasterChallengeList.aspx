﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MasterChallengeList.aspx.cs" Inherits="translation_MasterChallengeList" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/BoardList.ascx" tagname="BoardList" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript">
        function SearchValidate() {
            if (document.getElementById("BoardList1_txtSearch").value.trim() == "") {
                alert("검색어를 입력 하세요.");
                return false;
            }
            return true;
        }
    </script>
</head>
<body style="overflow-X:hidden">
    <form id="form1" runat="server">     
    <div>    
        <!-- container -->
<div id="container" class="mate">

	<!-- wrapper -->
	<div id="wrapper" class="share master-challenge">

		<div style="display:inline;float:left;position:relative;width:658px;margin:0 -1px 0 0;padding:0 50px 100px;overflow:hidden;">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>번달이 도전기</h3>
					<p>번역초보에서 번달이(번역의 달인)이 되기까지 이야기가 담겨있는 공간입니다.</p>
				</div>
			</div>
            <div class="master-challenge-cover">
				<strong>초보에서 달인까지! 번역메이트의 번달이 도전기 번역 초보에서 번달이(번역의 달인)가 되기까지 알아두면 유용한 번역 TIP!함께 나누어 주세요!</strong>
			</div>
			<div style="text-align:right" >
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="/image/btn/btn-write.png" onclick="ImageButton1_Click"/>
            </div>
			<uc4:BoardList ID="BoardList1" runat="server" />	
		</div>
		<!-- // contents -->

	</div>
	<!-- // wrapper -->

</div>

    </div> 
    <asp:hiddenfield ID="hidTableIdx" runat="server" Value ="1005"></asp:hiddenfield>       
    </form>
</body>
</html>
