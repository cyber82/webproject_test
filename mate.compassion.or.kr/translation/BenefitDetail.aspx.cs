﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
 
public partial class Mate_nanum_BenefitDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int idx = Convert.ToInt32(Request.QueryString["idx"]);

            WWWService.Service service = new WWWService.Service();

            DataSet data = service.GetBenefitDetail(idx);

            if (data.Tables[0].Rows.Count > 0)
            {
                string sContents = data.Tables[0].Rows[0]["comment"].ToString();
                sContents = sContents.Replace("''", "'");
                sContents = sContents.Replace("&", "&amp;");
                //sContents = sContents.Replace("<", "&lt;");
                //sContents = sContents.Replace(">", "&gt;");
                sContents = sContents.Replace("\r\n", "<br>");
                sContents = sContents.Replace("\n", "<br>");
                sContents = sContents.Replace("\t", "&nbsp;&nbsp;&nbsp;");
                labComment.Text = sContents;

                labEword1.Text = data.Tables[0].Rows[0]["eword"].ToString();
                labEword2.Text = data.Tables[0].Rows[0]["eword"].ToString();
                labNationStr.Text = data.Tables[0].Rows[0]["nationStr"].ToString();
                labReadCount.Text = data.Tables[0].Rows[0]["readCount"].ToString();
                labRegisterDate.Text = Convert.ToDateTime (data.Tables[0].Rows[0]["registerDate"]).ToString ("yyyy-MM-dd");
                labRegUserName.Text = data.Tables[0].Rows[0]["regUserName"].ToString();

                service.UpdateBenefit(idx); 
      
                if (UserInfo.IsLogin && new UserInfo().UserId == data.Tables[0].Rows[0]["regUserID"].ToString()
                    || UserInfo.IsLogin && new UserInfo().ConId == "120565")
                {
                    btnDel.CommandArgument = idx.ToString ();
                    btnDel.Visible = true;
                }

                else
                    btnDel.CommandArgument = "0";

                data.Dispose();
                service.Dispose();
            }
        }
    }
    protected void btnDel_Click(object sender, EventArgs e)
    {
        WWWService.Service service = new WWWService.Service();
        service.DeleteBenefit(Convert.ToInt32 (btnDel.CommandArgument));

        string sjs = JavaScript.HeaderScript.ToString();        
        sjs += JavaScript.GetFormCloseScript();
        sjs += "window.opener.location.reload();";
        sjs += JavaScript.FooterScript.ToString();
        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

        return;
    }
}