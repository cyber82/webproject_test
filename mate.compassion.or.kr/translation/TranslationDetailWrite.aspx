﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TranslationDetailWrite.aspx.cs" Inherits="Mate_translation_TranslationDetailWrite" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<style>
    #tabmenu{ position:relative; height:29px; font-family:'돋움',dotum; font-size:12px }
    #tabmenu ul,#tabmenu ul li{margin:0;padding:0;background-color:#E6E6E6}
    #tabmenu ul li{list-style:none}
    #tabmenu ul li{float:left;margin-right:-1px;line-height:26px}
    #tabmenu ul li a{display:inline-block;padding:2px 16px 1px;_padding:3px 16px 0;background-position: 100% 0;font-weight:bold;color:#666;cursor:pointer;text-decoration:none !important}
    #tabmenu ul li a:hover{color:#000}
    #tabmenu ul li.on,#tabmenu ul li.on a{background-color:#FFFFFF}
    #tabmenu ul li.on a{color:#000000}
    #tabcontent1{}
    #tabcontent2{}
    .test{width:100%; height:90%; min-height:230px;}
</style>
<script language="javascript">
    function show_leemocon(tabnum) {
        var i;
        var d = new Array(2);
        var tm = document.getElementById("tabmenu").getElementsByTagName("li");

        if (tabnum != '') {
            parent.parent.document.getElementById("txtTabSelectValue").value = tabnum;
        }
  
        for (i = 0; i <= 1; i++) {
            d[i] = document.getElementById("tabcontent" + i);

            d[i].style.display = "none";

            tm[i].className = "";
        };

        switch (tabnum) {
            case "0":
                d[0].style.display = "";

                tm[0].className = "on";

                break;

            case "1":
                d[1].style.display = "";

                tm[1].className = "on";

                break;

            case "2":
                d[0].style.display = "";

                tm[0].className = "on";

                break;
        };

        //화면 가로 세로 전환시 탭을 유지하기 위해 사용
        document.getElementById("txtTabShowValue").value = tabnum;
        //alert(parent.parent.document.getElementById("txtTranslateInf").value);
    };

    //번역 및 스크리닝 값 유지
    //스크리닝 결과에 따라 스크리닝 탭을 보여 줌
    function ScrShowCheck(TranStatus, ScreeningStatus) {
        document.getElementById("txtTranStatus").value = TranStatus;
        document.getElementById("txtScreeningStatus").value = ScreeningStatus;

        //번역일경우, 번역결과, 스크리닝 결과
        if ('<%=sgTabSelectValue%>' == '0' && (document.getElementById("txtTranStatus").value != 'END' || document.getElementById("txtScreeningStatus").value != 'END')) {
            document.all.tabScr.style.display = "none"
        }
    }
    
    //번역 완료 체크
    function TransFinishCheck() {

        if (trim(document.getElementById("txtTranslate").value) == "")
        {
            alert("번역을 하지 않았습니다");
            return false;
        }

        else {
            return confirm('완료된 번역 내용을 전송하시겠습니까?');
        }

    };

    //스크리닝 완료 체크
    function ScreeFinishCheck() {

        //alert(document.getElementById("rdoLike").checked);

        if (trim(document.getElementById("txtScreening").value) == "") {
            alert("스크리닝을 하지 않았습니다");
            return false;
        }

//        else if (document.getElementById("rdoLike").checked == false && document.getElementById("rdoSoso").checked == false) {
//            alert("스크리닝 완료 전에 번역 평가를 해주세요.");
//            return false;
        //        }



        var cbxEx = document.getElementById("rdoLike");
        if (!cbxEx.checked) {
            var cbx1 = document.getElementById("chkGood_1");
            var cbx2 = document.getElementById("chkGood_2");
            var cbx3 = document.getElementById("chkGood_3");
            var cbx4 = document.getElementById("chkGood_4");
            var cbx5 = document.getElementById("chkGood_5");

            if (!cbx1.checked && !cbx2.checked && !cbx3.checked  && !cbx4.checked  && !cbx5.checked ) {
                alert("사유를 선택해주세요.");
                return false;
            }
        }


        return confirm('완료된 스크리닝 내용을 전송하시겠습니까?');
    };

    //공백제거
    function trim(str)
    {
      while(str && str.indexOf(" ") == 0)
       str = str.substring(1);
 
      while(str && str.lastIndexOf(" ") == str.length-1)
       str = str.substring(0, str.length-1);
 
      return str;
    }

    function SetScreeningText(sScreeningText) {
        //alert(sScreeningText.split("|n").join("\n"));
        document.getElementById('txtScreening').value = sScreeningText.split("|n").join("\n");

    }

    function SetTranslateText(sTranslateText) {
        //  alert("sTranslateText);

        var sTranText = ''

        if (parent.parent.document.getElementById("txtTranslateInf").value == "T_FIRST_NULL") {
            document.getElementById('txtTranslate').value = sTranText.split("|n").join("\n");
        }
        else {
            document.getElementById('txtTranslate').value = sTranslateText.split("|n").join("\n");
        }
    }

    function GetThirdEnglish() {

        document.getElementById('txtThirdEnglish').value = parent.parent.window.frames["Showframe"].TranslationDetailImage.document.getElementById("txtThirdEnglish").value;
    }

    //3PL 완료 체크
    function ThirdFinishCheck() {
        if (trim(document.getElementById("txtThirdKorean").value) == "") {
            alert("번역을 하지 않았습니다");
            return false;
        }

        else {
            GetThirdEnglish();
            return confirm('번역을 완료 하시겠습니까?');
        }
    };
 
    //화면 셋팅
    function SetPageView() {
        //txtTranslate
        //document.getElementById('txtTranslate').value = "";
        if ('<%=sgMatePageType%>' == '40' || '<%=sgMatePageType%>' == '41') {
            document.getElementById('tabmenu').style.display = "none"
            document.getElementById('tabcontent0').style.display = "none"
            document.getElementById('tabcontent1').style.display = "none"
        }

        else {
            document.getElementById('ThirdPL').style.display = "none"
            show_leemocon('<%=sgTabSelectValue%>');         
        }
    }

    function DelCookie(cKey) {
        // 동일한 키(name)값으로
        // 1. 만료날짜 과거로 쿠키저장
        // 2. 만료날짜 설정 않는다. 
        //    브라우저가 닫힐 때 제명이 된다   

        var date = new Date(); // 오늘 날짜 
        var validity = -1;
        date.setDate(date.getDate() + validity);
        document.cookie =
          cKey + "=;expires=" + date.toGMTString();
    }

    function SetCookie(cKey, cValue)  // name,pwd
    {
        var date = new Date(); // 오늘 날짜
        // 만료시점 : 오늘날짜+1 설정
        var validity = 1;
        date.setDate(date.getDate() + validity);
        // 쿠키 저장
        document.cookie = cKey + '=' + escape(cValue) + ';expires=' + date.toGMTString();      
    }

    function GetCookie(cKey) {
        var allcookies = document.cookie;

        alert(allcookies);

        var cookies = allcookies.split("; ");

        for (var i = 0; i < cookies.length; i++) {
            var keyValues = cookies[i].split("=");
            if (keyValues[0] == cKey) {
                return unescape(keyValues[1]);
            }
        }

        return "";
    }

    function openBrWindow(sAddr, sPopName, sWidth, sHeight, sResizable) {  
        //window.open("http://eschyles.mireene.com/", "", "");  //속성 지정하지 않은 기본창

        if (screen.width < 1025) {
            LeftPosition = 0;
            TopPosition = 0;
        } else {
            LeftPosition = (screen.width) ? (screen.width - sWidth) / 2 : 100;
            TopPosition = (screen.height) ? (screen.height - sHeight) / 2 : 100;
        }


        if (sPopName == "TranslationPreView") {
            sAddr = sAddr + "&TabShowValue=" + document.getElementById("txtTabShowValue").value;
        }
        
        if (sResizable == "0") {
            window.open(sAddr, sPopName, "width=" + sWidth + ",height=" + sHeight + ",top=" + TopPosition + ",left=" + LeftPosition + ", scrollbars=yes, toolbar=no menubar=no, location=no");
        }

        else {
            window.open(sAddr, sPopName, "width=" + sWidth + ",height=" + sHeight + ",top=" + TopPosition + ",left=" + LeftPosition + ", scrollbars=yes, toolbar=no menubar=no, location=no, resizable=yes");
        }
    }

    function openModalDialog(sAddr, sWidth, sHeight)
    {
        if (screen.width < 1025) {
            LeftPosition = 0;
            TopPosition = 0;
        } else {
            LeftPosition = (screen.width) ? (screen.width - sWidth) / 2 : 100;
            TopPosition = (screen.height) ? (screen.height - sHeight) / 2 : 100;
        }

        //window.open(sAddr, "w", "width=" + sWidth + ",height=" + sHeight + ",top=" + TopPosition + ",left=" + LeftPosition + ", scrollbars=yes, toolbar=no menubar=no, location=no");
        popupObj = showModalDialog(sAddr, window, "dialogHeight=" + sHeight + "px; dialogWidth=" + sWidth + "px; scroll=yes; status=yes; help=no; center=yes");     
    }

    function SetXSS() {
        response.setHeader("X-XSS-Protection", "0");
    }

    function NoCopy() {
        var trans = document.getElementById('txtTranslate');
        var screen = document.getElementById('txtScreening');

        trans.onkeydown = function (evt) {
            evt = evt || window.event;
            evt.returnValue = false;
        };
    }
    </script>
</head>
<body style="padding:2px; height:100%;" onload="SetPageView()">
    <form id="form1" runat="server">
    <input type="hidden" id="txtTabShowValue" name="txtTabShowValue"/>
    <div id="tabmenu" style="border-bottom:1px solid #CCC;"> 
         <ul>
           <li id="tabTran" class="on" style='border:1px solid #CCCCCC; border-bottom:0px'><a onclick="show_leemocon('0')">번역내용</a> </li>
           <li id="tabScr" class="" style='border:1px solid #CCCCCC; border-bottom:0px'> <a onclick="show_leemocon('1')">스크리닝</a> </li>       
         </ul>

         <input type="hidden" id="txtTranStatus" name="txtTranStatus" />
         <input type="hidden" id="txtScreeningStatus" name="txtScreeningStatus" />
         &nbsp&nbsp&nbsp
         <asp:ImageButton ID="imgBtnPreView" runat="server" onclick="imgBtnPreView_Click" ImageUrl="~/image/imate/btn/btn_preview.jpg" Height="22px" ToolTip="미리보기 창을 불러옵니다."/>
         &nbsp
         <asp:ImageButton ID="ImgBtnTranslationTemplate" runat="server" ImageUrl="~/image/imate/btn/btn_emailTemp.jpg" ToolTip="번역 템플릿을 불러옵니다." Visible="true" Height="22px"/>
         &nbsp
         <asp:ImageButton ID="ImgBtnCompare" runat="server" ImageUrl="~/image/imate/btn/btn_screeningDetail.jpg" ToolTip="번역과 스크리닝 비교 창을 불러옵니다." Visible="false" Height="22px"/>                
    </div>

    <% //추가 2013-08-30 %>
    <div style="padding-left:90px;"><br /><asp:Label ID="lblCorrTypeText" runat="server" Text=""></asp:Label></div>

    <!--번역내용-->    
    <div id="tabcontent0" style="border:0px solid #CCCCCC;">
        <table style="width:100%; height:80%;">
            <tr>
                <td style="vertical-align:top; font-size:smaller; width:10%;"><center><br />번역</center></td>
                <td colspan="3" style="width:90%; height:100%;">
                    <asp:TextBox ID="txtTranslate" runat="server" TextMode="MultiLine" Font-Names="굴림" CssClass="test"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="font-size:smaller""><center>
                    <asp:Label ID="lblReportType" runat="server" Text="Label">신고사유</asp:Label></center>
                </td>
                <td style="width:70%;">
                    <asp:DropDownList ID="ddlReportType" runat="server" Width="30%" 
                        onselectedindexchanged="ddlReportType_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    <asp:TextBox ID="txtReportSub" runat="server" Width="65%" Enabled="true"></asp:TextBox>
                </td>
                <td>
                    <asp:ImageButton ID="imgBtnTranslateReport" runat="server" 
                                     onclick="imgBtnTranslateReport_Click" ImageUrl="~/image/imate/btn/btn_report.jpg" ToolTip="사유 선택 후, 해당 편지를 신고합니다." />
                    <asp:HiddenField ID="hidTransReport" runat="server" />
                </td>
                <td style="width:30%;text-align:right;">       
                    <asp:ImageButton ID="imgBtnTranslateSave" runat="server" 
                        onclick="imgBtnTranslateSave_Click" ImageUrl="~/image/imate/btn/btn_temp.jpg" ToolTip="번역 내용을 임시 저장합니다." />
                    <asp:ImageButton ID="imgBtnTranslateSend" runat="server"
                        onclick="imgBtnTranslateSend_Click" ImageUrl="~/image/imate/btn/btn_trans.jpg" ToolTip="완료된 번역 내용을 전송합니다." />
                </td>
            </tr>
            <tr id="Trans_End_Excellent" runat="server">
                <td colspan="4" style="text-align:right; font-size:smaller; ">
                    <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/image/imate/icon/icon_smile.png" style="margin-top:-4px;" />&nbsp;
                    <asp:Label ID="Label3" runat="server" Text="Excellent!"></asp:Label>&nbsp;
                    <asp:Label ID="Label4" runat="server" Text="번역의 달인 후보답게 잘 번역해주셨습니다!"></asp:Label>
                </td>
            </tr>
            <tr id="Trans_End_Good" runat="server">
                <td colspan="4" style="text-align:right; font-size:smaller;">
                    <asp:Image ID="Image4" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/image/imate/icon/icon_smile.png" style="margin-top:-4px;" />&nbsp;      
                    <asp:Label ID="Label5" runat="server" Text="Good!" ForeColor="#000099" Font-Bold="True" Visible="false"></asp:Label>&nbsp;
                    Tips! 
                    <asp:Label ID="txtGood_Trans" runat="server"></asp:Label>
                    만 보완된다면 완벽하겠네요!
                </td>
            </tr>
            <tr id="Trans_End_Etc" runat="server">
                <td colspan="4" style="text-align:right; font-size:smaller;">
                    <font color="#6A84B7">
                        <asp:Label ID="lblTranslationNote" runat="server" Font-Bold="True"></asp:Label>
                    </font>                
                </td>
            </tr>
            <%--<tr>
                <td colspan="4" style="text-align:right">
                <font color="#6A84B7">
                    <asp:Label ID="lblTransComment" runat="server" Font-Bold="True">스크리너 코멘트 : </asp:Label>&nbsp;&nbsp;
                    <asp:TextBox ID="txtTransComment" runat="server" Width="50%" ReadOnly=true></asp:TextBox>
                </font>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align:right">
                <font color="#6A84B7">
                    <asp:RadioButton ID="rdoLike_TransCheck" GroupName="RadioEvaluate_Trans" Text="Excellent" runat="server" Enabled=false />
                    <asp:Image ID="imgSmaile_TransCheck" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/image/imate/icon/icon_smile.png" />
                                
                    <asp:RadioButton ID="rdoSoso_TransCheck" GroupName="RadioEvaluate_Trans" Text="Good" runat="server" Enabled=false />
                    <asp:Image ID="imgSick_TransCheck" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/image/imate/icon/icon_smile.png" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblTranslationNote" runat="server" Font-Bold="True"></asp:Label>
                </font>
                </td>
            </tr>--%>
        </table>
    </div>

        <!--스크리닝-->
        <div id="tabcontent1" style="border: 0px solid #CCCCCC; display: block;">
            <table style="width: 100%; height: 80%;">
                <tr>
                    <td style="vertical-align: top; font-size: smaller; width: 10%;">
                        <center><br />스크리닝</center>
                    </td>
                    <td colspan="3" style="width: 90%; height: 100%;">
                        <asp:TextBox ID="txtScreening" runat="server" CssClass="test" TextMode="MultiLine" Font-Names="굴림"></asp:TextBox>
                    </td>
                </tr>
                <tr id="Screen_Start_3" runat="server">
                    <td></td>
                    <td colspan="3">
                        <table style="width:100%;">
                            <tr>
                                <td style="text-align: left; font-size: smaller;">
                                    <asp:CheckBox ID="chkCaution" runat="server" Visible="false" />
                                    <asp:DropDownList ID="ddlEvaluate" runat="server" Visible="false"></asp:DropDownList>
                                    <asp:CheckBox ID="chkGood_2" runat="server" Text="누락" />&nbsp;&nbsp;&nbsp;
                                </td>
                                <td style="text-align: left; font-size: smaller;">
                                    <asp:CheckBox ID="chkGood_3" runat="server" Text="오역" />
                                </td>
                                <td style="text-align: left; font-size: smaller;">
                                    <asp:CheckBox ID="chkGood_1" runat="server"  Text="번역유의사항 미반영" AutoPostBack="True" OnCheckedChanged="chkGood_1_CheckedChanged" />&nbsp;
                                    <asp:TextBox ID="txtGood_1" runat="server" Enabled="false" Width="180px"></asp:TextBox>
                                </td>
                                <td style="text-align: left; font-size: smaller;">
                                    <asp:CheckBox ID="chkHold" runat="server" Text="HOLD" />
                                </td>
                                <td style="text-align: left; font-size: smaller;">
                                    <asp:ImageButton ID="imgBtnScreeningSave" runat="server"
                                        OnClick="imgBtnScreeningSave_Click" ImageUrl="~/image/imate/btn/btn_temp.jpg" ToolTip="스크리닝 내용을 임시 저장합니다." />
                                </td>
                            </tr>
                            <tr id="Screen_Start_6" runat="server">
                                <td colspan="2" style="text-align: left; font-size: smaller;">
                                    <asp:CheckBox ID="chkGood_4" runat="server" Text="부자연스러운 번역" />
                                </td>
                                <td style="text-align: left; font-size: smaller;">
                                    <asp:CheckBox ID="chkGood_5" runat="server" Text="기타" AutoPostBack="True" OnCheckedChanged="chkGood_5_CheckedChanged" />
                                    <asp:TextBox ID="txtGood_5" runat="server" Enabled="false" Width="300px"></asp:TextBox>
                                </td>
                                <td style="text-align: left; font-size: smaller;">
                                    <asp:CheckBox ID="rdoLike" runat="server" Text="Excellent"  />
                                    <asp:Image ID="imgSmaile" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/image/imate/icon/icon_smile.png" />&nbsp;&nbsp;
                                </td>

                                <td style="text-align: left;">
                                    <asp:ImageButton ID="imgBtnScreeningFinish" runat="server"
                                        OnClick="imgBtnScreeningFinish_Click" ImageUrl="~/image/imate/btn/btn_complete.jpg" ToolTip="완료된 스크리닝 내용을 전송합니다." />
                                </td>
                            </tr>

                            <tr id="Screen_Start_7" runat="server">
                    <td colspan="5"></td>
                </tr>
                            <tr>
                                <td colspan="5" style="border-bottom:1px solid gray"></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                            </tr>
                <tr id="Screen_Start_1" runat="server">
                    <td style="font-size: smaller">
                        <center>
                            <asp:Label ID="lblReturnType" runat="server" Text="Label">반송사유</asp:Label></center>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="ddlReturnType" runat="server" Width="30%">
                        </asp:DropDownList>
                        <asp:TextBox ID="txtReturnSub" runat="server" Width="65%"></asp:TextBox>
                    </td>
                    <td style="text-align: left">
                        <asp:ImageButton ID="imgBtnScreeningReturn" runat="server" ImageUrl="~/image/imate/btn/btn_return.jpg"
                            OnClick="imgBtnScreeningReturn_Click" ToolTip="번역 메이트에게 해당 편지를 반송합니다." />
                    </td>

                                <%--<td style="height:100%; font-size:smaller"">
                    &nbsp;&nbsp;평가&nbsp;<asp:DropDownList ID="ddlEvaluate" runat="server">
                    </asp:DropDownList>
                </td>--%>
                </tr>

                <tr id="Screen_Start_2" runat="server">
                    <td style="font-size: smaller">
                        <center>
                            <asp:Label ID="lblReportType_Screen" runat="server" Text="Label">신고사유</asp:Label></center>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="ddlReportType_Screen" runat="server" Width="30%"
                            OnSelectedIndexChanged="ddlReportType_Screen_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:TextBox ID="txtReportSub_Screen" runat="server" Width="65%" Enabled="false"></asp:TextBox>
                    </td>
                    <td align="left">
                        <asp:ImageButton ID="imgBtnTranslateReport_Screen" runat="server"
                            OnClick="imgBtnTranslateReport_Screen_Click"
                            ImageUrl="~/image/imate/btn/btn_report.jpg" ToolTip="사유 선택 후, 해당 편지를 신고합니다." />
                    </td>
                </tr>
                            <%--
            <tr id="Screen_Start_5" runat="server">
                <td style="text-align:left; padding-left:90px;font-size:smaller;" colspan="2" >
                    
                    
                    
                </td>
                <td style="text-align:left;">
                                                            
                </td>   
            </tr>
                            --%>
                    </table>
                    </td>
                </tr>
                
            <%--<tr id="Screen_End_Excellent" runat="server" style="display:none;">
                <td colspan="4" style="text-align:right; font-size:smaller;">
                    <asp:Image runat="server" ImageAlign="AbsMiddle" ImageUrl="~/image/imate/icon/icon_smile.png" />&nbsp;
                    <asp:Label runat="server" Text="Excellent!" ForeColor="#000099" Font-Bold="True"></asp:Label>&nbsp;
                    <asp:Label ID="txtExcellent" runat="server" Text="지금처럼만 하시면 스크리닝이 필요 없는 번역의 달인이 될 수 있어요. Keep going~!" ForeColor="#00CC99"></asp:Label>
                </td>
            </tr>--%>
            <%--<tr id="Screen_End_Good" runat="server" style="display:none;">
                <td colspan="4" style="text-align:right; font-size:smaller;">
                    <asp:Image runat="server" ImageAlign="AbsMiddle" ImageUrl="~/image/imate/icon/icon_smile.png" />&nbsp;      
                    <asp:Label runat="server" Text="Good!" ForeColor="#000099" Font-Bold="True"></asp:Label>&nbsp;
                    <asp:Label ID="txtGood" runat="server" Text="" ForeColor="#00CC99"></asp:Label>
                </td>
            </tr>
            <tr id="Screen_End_Return" runat="server" style="display:none;">
                <td colspan="4" style="text-align:right;">
                    <font color="#6A84B7">
                        <asp:Label ID="lblScreeningNote" runat="server" Font-Bold="True"></asp:Label>
                    </font>
                </td>
            </tr>--%>
            <tr id="Screen_End_Excellent" runat="server">
                <td colspan="4" style="text-align:right; font-size:smaller; ">
                    <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/image/imate/icon/icon_smile.png" style="margin-top:-4px;" />&nbsp;
                    <asp:Label ID="Label1" runat="server" Text="Excellent!"  ></asp:Label>&nbsp;
                    <asp:Label ID="txtExcellent" runat="server" Text="번역의 달인 후보답게 잘 번역해주셨습니다!"></asp:Label>

                </td>
            </tr>
            <tr id="Screen_End_Good" runat="server">
                <td colspan="4" style="text-align:right; font-size:smaller;">
                    <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/image/imate/icon/icon_smile.png" style="margin-top:-4px;" />&nbsp;      
                    <asp:Label ID="Label2" runat="server" Text="Good!" Visible="false" ForeColor="#000099" Font-Bold="True"></asp:Label>&nbsp;
                    Tips! 
                    <asp:Label ID="txtGood" runat="server" ></asp:Label>
                    만 보완된다면 완벽하겠네요!
                </td>
            </tr>
            <tr id="Screen_End_Return" runat="server">
                <td colspan="4" style="text-align:right; font-size:smaller;">
                    <font color="#6A84B7">
                        <asp:Label ID="lblScreeningNote" runat="server" Font-Bold="True"></asp:Label>
                    </font>                
                </td>
            </tr>
            <%--<tr id="Screen_End_2" runat="server">
                <td colspan="4" style="text-align:right;">
                <font color="#6A84B7">
                    <asp:RadioButton ID="rdoLike_ScreenCheck" GroupName="RadioEvaluate_Screen" 
                        Text="Excellent" runat="server" Enabled=false />
                    <asp:Image ID="imgSmile_ScreenCheck" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/image/imate/icon/icon_smile.png" />
                                
                    <asp:RadioButton ID="rdoSoso_ScreenCheck" GroupName="RadioEvaluate_Screen" 
                        Text="Good" runat="server" Enabled=false />
                    <asp:Image ID="imgSick_ScreenCheck" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/image/imate/icon/icon_smile.png" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblScreeningNote" runat="server" Font-Bold="True"></asp:Label>
                </font>
                </td>
            </tr>--%>
        </table>
    </div>

    <!--3PL-->
    <div id="ThirdPL">
        <table style="width:100%; height:90%;">
            <tr>
                <td style="width:100%; height:90%;">
                    <asp:TextBox ID="txtThirdKorean" runat="server" CssClass="test" TextMode="MultiLine" Font-Names="굴림"></asp:TextBox>
                </td>          
            </tr>
            <tr>
                <td style="text-align:right;">
                    <font color="#6A84B7">
                        <asp:Label ID="lblThirdNote" runat="server" Font-Bold="True" Visible="false"></asp:Label>
                    </font>
                    <asp:ImageButton ID="imgBtnThirdPLSave" runat="server" 
                            ImageUrl="~/image/imate/btn/btn_temp.jpg" ToolTip="3PL 내용을 임시 저장합니다." 
                            onclick="imgBtnThirdPLSave_Click" />
                    <asp:ImageButton ID="imgBtnThirdPLFinish" runat="server" 
                            ImageUrl="~/image/imate/btn/btn_complete.jpg" ToolTip="완료된 3PL 내용을 전송합니다." 
                            onclick="imgBtnThirdPLFinish_Click" />
                 </td>
            </tr>
        </table>
    </div>

    <div style="display:none">
        <asp:TextBox ID="txtThirdEnglish" runat="server" TextMode="MultiLine" ReadOnly="False" Width="100%" Height="100%" Font-Names="굴림"></asp:TextBox>
    </div>
        
    </form>
</body>
</html>
