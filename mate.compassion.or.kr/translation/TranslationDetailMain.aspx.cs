﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Drawing;
using System.Text;
using System.Data;



public partial class Mate_translation_TranslationDetailMain : System.Web.UI.Page
{
    public string sgShowValue = "";
    public string sgShowList;
    public string sgTranslateInf;
    public string sgScreeningInf;
    public string sgThirdEngInf;
    public string sgThirdKorInf;
    public string sgPageCount;
    public string sgFromPage;
    public string sgMatePageType;
    public string sgHomeMateType;

    private string sgFilePath;
    private string sgCorrID;
    private string sgPackageID;
    private string sgTabSelectValue;
    public string sgMoveType;
    private string sgThirdYear;
    private string sgProjectNumber;
    private string sgSearchKind;
    private string sgSearchText;
    private string sChildLetterKey; /*20170206 child key 9자리 이미지 구분*/

    protected void Page_Load(object sender, EventArgs e)
    {
        //HttpContext.Current.Response.AddHeader("p3p", "CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");
        string sJs;

        
        

        
        
        sChildLetterKey = "";               /*20170206 child key 9자리 이미지 구분*/

        if (UserInfo.IsLogin)
        {

            sgCorrID = Request.QueryString["CorrID"];
            sgPackageID = Request.QueryString["PackageID"];
            sgPageCount = Request.QueryString["PageCount"];

            sgSearchKind = Request.QueryString["SearchKind"];
            sgSearchText = Request.QueryString["SearchText"];


            sgThirdYear = Request.QueryString["ThirdYear"];
            sgProjectNumber = Request.QueryString["ProjectNumber"];

            string sTableShow = Request["txtTableShow"];
            ImgBtnShowMateInfo.Attributes.Add("onclick", "IMateShow(); if(document.getElementById('txtTableShow').value == '1'){ this.src='/image/imate/btn/btn_hide.jpg'}else{this.src='/image/imate/btn/btn_show.jpg'}; return false;");

            sgHomeMateType = Request.QueryString["MateType"];//홈의 comment 에서 올 경우 스크리너인지 번역인지를 구분 [SCR, TRAN]


            //번역 상세로 들어온 페이지 분류[0]:번역 [1]:스크리닝 [2]: 편지검색
            sgMoveType = Request["TabSelectValue"];

            //if (Request.UrlReferrer == null)
            //{
            //    sJs = JavaScript.HeaderScript;
            //    sJs += JavaScript.GetAlertScript("잘못된 경로로 접근하셨습니다.");
            //    sJs += JavaScript.GetPageMoveScript("/translation/TranslationHome.aspx");
            //    sJs += JavaScript.FooterScript;
            //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
            //    return;
            //}
            //else {
            //    if (Request.UrlReferrer.AbsoluteUri.IndexOf("TranslationLetterSearch") != -1) {
            //        sgMoveType = "2";
            //    }
            //}

            new TranslateInfo().MateMoveType = sgMoveType;
            new TranslateInfo().MatePageType = new TranslateInfo().MatePageType;

            sgMatePageType = new TranslateInfo().MatePageType;

            //번역 탭 선택 설정 0:번역 1:스크리닝
            if (sgMatePageType == "00")
            {
                if (sgHomeMateType == "TRANSTART") { new TranslateInfo().TabSelectValue = "0"; }
                else if (sgHomeMateType == "SCREND") { new TranslateInfo().TabSelectValue = "1"; }
                else if (sgHomeMateType == "SCRSTART") { new TranslateInfo().TabSelectValue = "1"; }
            }
            if (sgMatePageType == "10" || sgMatePageType == "11") { new TranslateInfo().TabSelectValue = "0"; }

            if (sgMatePageType == "20" || sgMatePageType == "21") { new TranslateInfo().TabSelectValue = "1"; }

            //상세페이지 탭 유지
            string sTxtTabSelectValue = Request["txtTabSelectValue"];

            if (!string.IsNullOrEmpty(sTxtTabSelectValue))
            {
                new TranslateInfo().TabSelectValue = sTxtTabSelectValue;
            }

            //화면 가로 세로 변경시 변역중이던 텍스트 자료를 세션에 담기 위한 변수
            sgTranslateInf = Request["txtTranslateInf"];
            sgScreeningInf = Request["txtScreeningInf"];

            sgThirdEngInf = Request["txtThirdEngInf"];
            sgThirdKorInf = Request["txtThirdKorInf"];



            //가로 세로 선택 값("0",""->세로, "1"->가로)
            sgShowValue = Request["txtShowValue"];


            if (string.IsNullOrEmpty(sgShowValue))
            {
                sgShowValue = "";

            }

            if (new TranslateInfo().MatePageType == "40" || new TranslateInfo().MatePageType == "41")
            {


                //3PL 상세 정보
                sgCorrID = sgThirdYear + sgProjectNumber;
                SetThirdPLInfo(sgThirdYear, sgProjectNumber);

            }
            else
            {
                //번역 상세 정보
                SetTranslationInfo(sgCorrID);
                if (new TranslateInfo().MatePageType == "30")
                {

                    this.ImgBtnShowType.Visible = false;
                }
            }

            //페이지에 따라 상단 및 버튼 숨기기
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type='text/javascript' language ='javascript' >\n ");
            sb.Append("MainTopView('" + new TranslateInfo().MatePageType + "');");
            sb.Append("</script>");

            this.RegisterStartupScript("TranslationDetailMainTopView", sb.ToString());

            //이미지 및 변역 불러오기(처음 로드 할 때)
            if (sgShowValue == "")
            {
                // iMate Log
                UserInfo user = new UserInfo();

                WWWService.Service wwwService = new WWWService.Service();

                wwwService.writeErrorLog("CompassWeb4", "test", "test", @"");
                wwwService.writeErrorLog("CompassWeb4", "test", "test", @"/TranslationDetailMain.aspx  : 시작");
                wwwService.writeErrorLog("CompassWeb4", "test", "test", string.Format("DateTime : {0}, UserID : {1}, SessonID : {2} ", DateTime.Now.ToString(), user.UserId, Session.SessionID));

                SetImageStyle(sgCorrID, sgPackageID, sgPageCount);

                wwwService.writeErrorLog("CompassWeb4", "test", "test", @"");
                wwwService.writeErrorLog("CompassWeb4", "test", "test", @"/TranslationDetailMain.aspx  : 종료");
                wwwService.writeErrorLog("CompassWeb4", "test", "test", string.Format("DateTime : {0}, UserID : {1}, SessonID : {2} ", DateTime.Now.ToString(), user.UserId, Session.SessionID));
                wwwService.Dispose();
            }

            //var aa = document.getElementById("txt").value;
            //alert(aa);

            imgBtnExtension.Attributes.Add("onClick", "ExpCntCheck(); return false;");//확대
            imgBtnReduction.Attributes.Add("onClick", "RedCntCheck(); return false;");//축소
            ImgBtnTurnLeft.Attributes.Add("onClick", "ImageLeftTurn(); return false;");//이미지 왼쪽 회전
            ImgBtnTurnRight.Attributes.Add("onClick", "ImageRightTurn(); return false;");//이미지 오른쪽 회전

            imgBtnLeft.Attributes.Add("onClick", "MoveLeft('" + sgPageCount + "'); return false;");//이전 이미지
            imgBtnRight.Attributes.Add("onClick", "MoveRight('" + sgPageCount + "'); return false;");//다음 이미지

            ImgBtnShowType.Attributes.Add("onClick", "GetFrameText();");//가로 세보 보기전환

            //imgBtnPopUp.Attributes.Add("onClick", "openBrWindow('/translation/LetterImagePopUp.aspx?CorrID=" + sgCorrID + "&PackageID=" + sgPackageID + "&PageCount=" + sgPageCount + "&FilePath=" + sgFilePath + "&ChildKey=" + this.lblChildKey.Text + "','LetterImagePopUp','850','800','1'); return false;");
            imgBtnPopUp.Attributes.Add("onClick", "openBrWindow('/translation/LetterImagePopUp.aspx?CorrID=" + sgCorrID + "&PackageID=" + sgPackageID + "&PageCount=" + sgPageCount + "&FilePath=" + sgFilePath + "&ChildKey=" + sChildLetterKey + "','LetterImagePopUp','850','800','1'); return false;");
            

            sb = new StringBuilder();
            sb.Append("<script type='text/javascript' language ='javascript' >\n ");
            if (new TranslateInfo().MatePageType != "30")//편지 검색이 아닐 때
            {
                sb.Append("SetFrameText('" + sgShowValue + "','" + sgTabSelectValue + "');SetLotationImage();");
            }
            else//편지 검색일 경우 가로,세로 전환은 하지 않는다
            {
                sb.Append("SetFrameText('" + sgShowValue + "','" + sgTabSelectValue + "')");
            }
            sb.Append("</script>");

            this.RegisterStartupScript("TranslationDetailMain", sb.ToString());

            //번역 상세로 들어온 페이지 분류[0]:번역 [1]:스크리닝 [2]: 편지검색

            //Response.Write(new TranslateInfo().MateMoveType);

            if (new TranslateInfo().MateMoveType == "2")
            {
                this.ImgBtnShowType.Visible = false;
            }
        }

        else
        {   
            string sHomeUrl = Request.Url.AbsoluteUri.Replace("TranslationDetailMain", "TranslationHome"); 

            sJs = JavaScript.HeaderScript;
            sJs += JavaScript.GetAlertScript("로그인 후 사용 가능합니다.");
			//sJs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=" + Server.UrlEncode(sHomeUrl));
			sJs += JavaScript.GetPageMoveScript("/login.aspx");
			sJs += JavaScript.FooterScript;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", sJs);
        }
    }

    //3PL 상세 정보
    private void SetThirdPLInfo(string sgThirdYear, string sgProjectNumber)
    {
        StringBuilder sb = new StringBuilder();
        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        sb.Append("SELECT ProjectNumber, ProjectName, ActualAuthorRole, AuthorName ");
        sb.Append("FROM ThirdPL_Master ");
        sb.Append("WHERE ThirdYear = '" + sgThirdYear + "' AND ProjectNumber = '" + sgProjectNumber + "' ");

        Object[] objSql = new object[1] { sb.ToString() };

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

        this.lblThirdProjectNumber.Text = ds.Tables[0].Rows[0]["ProjectNumber"].ToString();
        this.lblThirdProjectName.Text = ds.Tables[0].Rows[0]["ProjectName"].ToString();
        this.lblActualAuthorRole.Text = ds.Tables[0].Rows[0]["ActualAuthorRole"].ToString();
        this.lblAuthorName.Text = ds.Tables[0].Rows[0]["AuthorName"].ToString();

        ds.Dispose();
        ds = null;
        _WWW6Service.Dispose();
    }

    //번역 상세 정보
    private void SetTranslationInfo(string sgCorrID)
    {
        Boolean bIsOld = false;         /*20170201 child key 9자리 이미지 구분*/

        Object[] objSql = new object[1] { "MATE_TranslateDetailInfo" };
        Object[] objParam = new object[1] { "CorrID " };
        Object[] objValue = new object[1] { sgCorrID };//번역

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

        sgFilePath = ds.Tables[0].Rows[0]["FilePath"].ToString();

        if (ds.Tables[0].Rows[0]["CorrClass"].ToString() != "")
        {
            imgBtnPopUp.Visible = false;
        }

        //번역일
        //string sTranslationDate = ds.Tables[0].Rows[0]["TranslationStart"].ToString();

        //if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["TranslationEnd"].ToString()))
        //{
        //    sTranslationDate = sTranslationDate + "~" + ds.Tables[0].Rows[0]["TranslationEnd"].ToString();
        //}

        //스크리너일
        //string sScreeningDate = ds.Tables[0].Rows[0]["ScreeningStart"].ToString();

        //if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["ScreeningEnd"].ToString()))
        //{
        //    sScreeningDate = sScreeningDate + "~" + ds.Tables[0].Rows[0]["ScreeningEnd"].ToString();
        //}

        //후원자
        this.lblConID.Text = ds.Tables[0].Rows[0]["ConID"].ToString();
        this.lblSponsorName.Text = ds.Tables[0].Rows[0]["SponsorName"].ToString();
        this.lblSponsorEngName.Text = ds.Tables[0].Rows[0]["SponsorEngName"].ToString();

        //this.lblUserID.Text = ds.Tables[0].Rows[0]["SponsorID"].ToString();

        if (ds.Tables[0].Rows[0]["SponsorGender"].ToString() == "FEMALE")
            this.lblUserGender.Text = "여";

        else if (ds.Tables[0].Rows[0]["SponsorGender"].ToString() == "MALE")
            this.lblUserGender.Text = "남";

        else
            this.lblUserGender.Text = "";

        //this.lblUserGender.Text = ds.Tables[0].Rows[0]["SponsorGender"].ToString();

        //어린이
        this.lblChildKey.Text = ds.Tables[0].Rows[0]["ChildKey"].ToString();
        this.lblChildName.Text = ds.Tables[0].Rows[0]["ChildName"].ToString() + " (" + ds.Tables[0].Rows[0]["PersonalNameKr"].ToString() + ")";
        this.lblChildEngName.Text = ds.Tables[0].Rows[0]["NameEng"].ToString() + " (" + ds.Tables[0].Rows[0]["PersonalNameEng"].ToString() + ")";

        if (ds.Tables[0].Rows[0]["ChildGender"].ToString() == "FEMALE")
            this.lblChildGender.Text = "여";

        else if (ds.Tables[0].Rows[0]["ChildGender"].ToString() == "MALE")
            this.lblChildGender.Text = "남";

        else
            this.lblChildGender.Text = "";

        //this.lblChildGender.Text = ds.Tables[0].Rows[0]["ChildGender"].ToString();

        //번역자
        this.lblTransID.Text = ds.Tables[0].Rows[0]["TranslationID"].ToString();
        this.lblTransName.Text = ds.Tables[0].Rows[0]["TranslationName"].ToString();
        //this.lblTranslationLevel.Text = ds.Tables[0].Rows[0]["TranslationLevel"].ToString();

        //번역일
        this.lblTransSDate.Text = ds.Tables[0].Rows[0]["TranslationStart"].ToString();
        this.lblTransEDate.Text = ds.Tables[0].Rows[0]["TranslationEnd"].ToString();

        //편지ID
        this.lblCorrID.Text = ds.Tables[0].Rows[0]["CorrID"].ToString();

        //편지타입
        this.lblCorrType.Text = ds.Tables[0].Rows[0]["CorrType"].ToString();

        //프로젝트명
        this.lblProjectID.Text = ds.Tables[0].Rows[0]["ProgramNameKr"].ToString();
        this.lblProjectName.Text = ds.Tables[0].Rows[0]["ProgramName"].ToString();

        //스크리너
        //this.lblScreeningID.Text = ds.Tables[0].Rows[0]["ScreenerID"].ToString();
        //this.lblScreeningName.Text = ds.Tables[0].Rows[0]["ScreenerName"].ToString();
        this.lblScreeningID.Text = ds.Tables[0].Rows[0]["ScreeningID"].ToString();
        this.lblScreeningName.Text = ds.Tables[0].Rows[0]["ScreeningName"].ToString();

        //스크리너일
        this.lblScreeningSDate.Text = ds.Tables[0].Rows[0]["ScreeningStart"].ToString();
        this.lblScreeningEDate.Text = ds.Tables[0].Rows[0]["ScreeningEnd"].ToString();

        /*20170201 이미지 child key 9자리 구분 로직 추가*/
        if (ds.Tables[0].Rows[0]["IsOld"] == null || ds.Tables[0].Rows[0]["IsOld"].Equals(""))
        {
            bIsOld = false;
        }
        else
        {
            bIsOld = (Boolean)ds.Tables[0].Rows[0]["IsOld"];
        }

        
        if (bIsOld == true && this.lblChildKey.Text.Trim().Length > 9)
        {
            sChildLetterKey = (this.lblChildKey.Text.Trim().Substring(0, 2) + this.lblChildKey.Text.Trim().Substring(3, 3) + this.lblChildKey.Text.Trim().Substring(7, 4));
        }
        else
        {
            sChildLetterKey = this.lblChildKey.Text.Trim();
        }

        ds.Dispose();
        ds = null;
        _WWW6Service.Dispose();
    }

    protected void ImgBtnShowType_Click(object sender, EventArgs e)
    {
        SetImageStyle(sgCorrID, sgPackageID, sgPageCount);
    }

    //이미지 및 번역 불러오기
    private void SetImageStyle(string sgCorrID, string sgPackageID, string sgPageCount)
    {
        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        if (new TranslateInfo().MatePageType == "40" || new TranslateInfo().MatePageType == "41")//3PL 일때
        {
            if (sgThirdEngInf == null)//최초 페이지 로드
            {
                Object[] objSqlTransNew = new object[1] { "MATE_CorrTemporarySave" };

                Object[] objParamTransNew = new object[3] { "DIVIS",
                                                    "rowstate",
                                                    "CorrID"//편지 코드
                                                    
                                            };
                Object[] objValueTransNew = new object[3] { "3PLEnglish",
                                                "NEW",
                                                sgCorrID
                                            };
                //텍스트 임시저장
                int iResultTransNew = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSqlTransNew, "SP", objParamTransNew, objValueTransNew);
            }
            else
            {

                if (string.IsNullOrEmpty(sgThirdEngInf))
                {
                    sgThirdEngInf = "";
                }

                Object[] objSqlTrans = new object[1] { "MATE_CorrTemporarySave" };

                Object[] objParamTrans = new object[4] { "DIVIS",
                                                    "rowstate",
                                                    "CorrID",//편지 코드
                                                    "TempText"//임시저장 텍스트
                                            };
                Object[] objValueTrans = new object[4] { "3PLEnglish",
                                                "MOD",
                                                sgCorrID,
                                                sgThirdEngInf
                                            };




                //텍스트 임시저장
                int iResultTrans = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSqlTrans, "SP", objParamTrans, objValueTrans);

            }


            if (sgThirdEngInf == null)//최초 페이지 로드
            {
                Object[] objSqlTransNew = new object[1] { "MATE_CorrTemporarySave" };

                Object[] objParamTransNew = new object[3] { "DIVIS",
                                                    "rowstate",
                                                    "CorrID"//편지 코드
                                                    
                                            };
                Object[] objValueTransNew = new object[3] { "3PLKorean",
                                                "NEW",
                                                sgCorrID
                                            };
                //텍스트 임시저장
                int iResultTransNew = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSqlTransNew, "SP", objParamTransNew, objValueTransNew);
            }
            else
            {

                if (string.IsNullOrEmpty(sgThirdKorInf))
                {
                    sgThirdKorInf = "";
                }

                Object[] objSqlTrans = new object[1] { "MATE_CorrTemporarySave" };

                Object[] objParamTrans = new object[4] { "DIVIS",
                                                    "rowstate",
                                                    "CorrID",//편지 코드
                                                    "TempText"//임시저장 텍스트
                                            };
                Object[] objValueTrans = new object[4] { "3PLKorean",
                                                "MOD",
                                                sgCorrID,
                                                sgThirdKorInf
                                            };
                //Response.Write(sgCorrID + "</br>" + sgThirdKorInf);
                //Response.End();
                //텍스트 임시저장
                int iResultTrans = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSqlTrans, "SP", objParamTrans, objValueTrans);

            }
        }
        else
        {

            //화면 가로 세로 변경시 번역중이던 텍스트 자료
            if (sgTranslateInf == null)//최초 페이지 로드
            {



                Object[] objSqlTransNew = new object[1] { "MATE_CorrTemporarySave" };

                Object[] objParamTransNew = new object[3] { "DIVIS",
                                                    "rowstate",
                                                    "CorrID"//편지 코드
                                                    
                                            };
                Object[] objValueTransNew = new object[3] { "Translation",
                                                "NEW",
                                                sgCorrID
                                            };
                //텍스트 임시저장
                int iResultTransNew = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSqlTransNew, "SP", objParamTransNew, objValueTransNew);
            }
            else
            {

                Object[] objSqlTrans = new object[1] { "MATE_CorrTemporarySave" };

                Object[] objParamTrans = new object[4] { "DIVIS",
                                                    "rowstate",
                                                    "CorrID",//편지 코드
                                                    "TempText"//임시저장 텍스트
                                            };
                Object[] objValueTrans = new object[4] { "Translation",
                                                "MOD",
                                                sgCorrID,
                                                sgTranslateInf
                                            };
                //텍스트 임시저장
                int iResultTrans = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSqlTrans, "SP", objParamTrans, objValueTrans);
            }

            if (sgScreeningInf == null)//최초 페이지 로드
            {



                Object[] objSqlScrNew = new object[1] { "MATE_CorrTemporarySave" };

                Object[] objParamScrNew = new object[3] { "DIVIS",
                                                    "rowstate",
                                                    "CorrID"//편지 코드
                                            };
                Object[] objValueScrNew = new object[3] { "Screening",
                                                "NEW",
                                                sgCorrID
                                            };
                //텍스트 임시저장
                int iResultScrNew = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSqlScrNew, "SP", objParamScrNew, objValueScrNew);
            }
            else
            {
                Object[] objSqlScrNew = new object[1] { "MATE_CorrTemporarySave" };

                Object[] objParamScrNew = new object[4] { "DIVIS",
                                                    "rowstate",
                                                    "CorrID",//편지 코드
                                                    "TempText"//임시저장 텍스트
                                            };
                Object[] objValueScrNew = new object[4] { "Screening",
                                                "MOD",
                                                sgCorrID,
                                                sgScreeningInf
                                            };
                //텍스트 임시저장
                int iResultScrNew = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSqlScrNew, "SP", objParamScrNew, objValueScrNew);
            }
        }

        if (sgShowValue == "0" || sgShowValue == "")
        {
            //sgShowValue = "1";<object></object>
            //sgShowList = "<iframe name='Showframe' src='TranslationDetailHeightFrame.aspx?CorrID=" + sgCorrID + "&PackageID=" + sgPackageID + "&PageCount=" + sgPageCount + "&FilePath=" + sgFilePath + "&ChildKey=" + this.lblChildKey.Text + "&ThirdYear=" + sgThirdYear + "&ProjectNumber=" + sgProjectNumber + "&SearchKind=" + sgSearchKind + "&SearchText=" + sgSearchText + "' width='100%' height='100%'></iframe>";
            sgShowList = "<iframe name='Showframe' src='TranslationDetailHeightFrame.aspx?CorrID=" + sgCorrID + "&PackageID=" + sgPackageID + "&PageCount=" + sgPageCount + "&FilePath=" + sgFilePath + "&ChildKey=" + sChildLetterKey + "&ThirdYear=" + sgThirdYear + "&ProjectNumber=" + sgProjectNumber + "&SearchKind=" + sgSearchKind + "&SearchText=" + sgSearchText + "' width='100%' height='100%'></iframe>";
            //this.ImgBtnShowType.Text = "가로보기";
        }
        else
        {
            //sgShowValue = "0";
            //sgShowList = "<iframe name='Showframe' src='TranslationDetailWidthFrame.aspx?CorrID=" + sgCorrID + "&PackageID=" + sgPackageID + "&PageCount=" + sgPageCount + "&FilePath=" + sgFilePath + "&ChildKey=" + this.lblChildKey.Text + "&ThirdYear=" + sgThirdYear + "&ProjectNumber=" + sgProjectNumber + "&SearchKind=" + sgSearchKind + "&SearchText=" + sgSearchText + "'  width='100%' height='100%'></iframe>";
            sgShowList = "<iframe name='Showframe' src='TranslationDetailWidthFrame.aspx?CorrID=" + sgCorrID + "&PackageID=" + sgPackageID + "&PageCount=" + sgPageCount + "&FilePath=" + sgFilePath + "&ChildKey=" + sChildLetterKey + "&ThirdYear=" + sgThirdYear + "&ProjectNumber=" + sgProjectNumber + "&SearchKind=" + sgSearchKind + "&SearchText=" + sgSearchText + "'  width='100%' height='100%'></iframe>";
            //this.ImgBtnShowType.Text = "세로보기";
        }

        _WWW6Service.Dispose();
    }
}