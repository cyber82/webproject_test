﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class Mate_translation_CountryAbbreviationList : System.Web.UI.Page
{
    public string sgHtml;

    protected void Page_Load(object sender, EventArgs e)
    {
        SetHtmlData();
    }

    private void SetHtmlData()
    {
        int iCnt = 1;

        DataSet ds = new DataSet();

        //가이드 쿼리 (국가약칭)
        Object[] objSql = new object[1] { "SELECT * FROM MATE_Help WHERE HelpType = 'COUNTRY' ORDER BY KorCode" };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

        StringBuilder sb = new StringBuilder();

        //국가약칭
        sb.Append("<table>");
        sb.Append("        <tr>");
        sb.Append("            <td style='border:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                국가코드");
        sb.Append("            </td>");
        sb.Append("            <td style='border:1px solid #CCCCCC; border-left:1px solid #CCCCCC; text-align:center; background-color:#E6E6E6'>");
        sb.Append("                국가명");
        sb.Append("            </td>");
        sb.Append("        </tr>");

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            if (iCnt == ds.Tables[0].Rows.Count)
            {
                sb.Append("        <tr>");
                sb.Append("            <td style='border:1px solid #CCCCCC; text-align:center;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='border:1px solid #CCCCCC; text-align:center;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }

            else
            {
                sb.Append("        <tr>");
                sb.Append("            <td style='border:1px solid #CCCCCC; text-align:center;'>");
                sb.Append(dr["KorCode"].ToString());
                sb.Append("            </td>");
                sb.Append("            <td style='border:1px solid #CCCCCC; text-align:center;'>");
                sb.Append(dr["KorName"].ToString());
                sb.Append("            </td>");
                sb.Append("        </tr>");
            }

            iCnt++;
        }

        sb.Append("    </table>");

        sgHtml = sb.ToString();

        ds.Dispose();
        _WWW6Service.Dispose();
    }
}