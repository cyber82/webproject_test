﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mate_translation_LetterImagePopUp : System.Web.UI.Page
{
    public string sgChildKey;
    public string sgFilePathLen;
    public string sgImgFilePath;
    public string sgPageCount;

    public int ngCorrID;

    protected void Page_Load(object sender, EventArgs e)
    {
        SetImage();

        imgBtnExtension.Attributes.Add("onClick", "ExpCntCheck(); return false;");//확대
        imgBtnReduction.Attributes.Add("onClick", "RedCntCheck(); return false;");//축소
        ImgBtnTurnLeft.Attributes.Add("onClick", "ImageLeftTurn(); return false;");//이미지 왼쪽 회전
        ImgBtnTurnRight.Attributes.Add("onClick", "ImageRightTurn(); return false;");//이미지 오른쪽 회전

        imgBtnLeft.Attributes.Add("onClick", "MoveLeft('" + sgPageCount + "'); return false;");//이전 이미지
        imgBtnRight.Attributes.Add("onClick", "MoveRight('" + sgPageCount + "'); return false;");//다음 이미지
    }

    /// <summary>
    /// 이미지 셋팅
    /// </summary>
    private void SetImage()
    {
        string sCorrID = Request.QueryString["CorrID"].ToString();
        string sPackageID = Request.QueryString["PackageID"].ToString();
        string sFilePath = Request.QueryString["FilePath"].ToString();

        sgPageCount = Request.QueryString["PageCount"].ToString();
        sgChildKey = Request.QueryString["ChildKey"].ToString();
        sgFilePathLen = sFilePath.Length.ToString();

        ngCorrID = int.Parse(sCorrID);

        if (ngCorrID >= 3000000)
        {
            sgImgFilePath = sFilePath + sPackageID + "/" + sCorrID;
        }

        else
        {
            sgImgFilePath = sFilePath + sPackageID + "/";
        }
    }
}