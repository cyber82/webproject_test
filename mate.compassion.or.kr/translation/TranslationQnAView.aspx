﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TranslationQnAView.aspx.cs" Inherits="Mate_nanum_TranslationQnAView" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>

<%@ Register src="../Controls/BoardDetail.ascx" tagname="BoardDetail" tagprefix="uc4" %>
<%@ Register src="../Controls/BoardList.ascx" tagname="BoardList" tagprefix="uc5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
     <script language="javascript">
         function Validate() {
             if (document.getElementById("BoardDetail1_comment").value.trim() == "") {
                 alert("댓글을 입력하세요.");
                 return false;
             }

             //200byte체크
             if (BytesCheck(document.getElementById('BoardDetail1_comment').value.trim()) > 1000) {
                 alert("댓글은 영문 1000자리 한글 500자리이상을 초과할수없습니다.");
                 document.getElementById('BoardDetail1_comment').focus();
                 return false;
             }
             return true;
         }

         function SearchValidate() {
             if (document.getElementById("BoardList1_txtSearch").value.trim() == "") {
                 alert("검색어를 입력 하세요.");
                 return false;
             }
             return true;
         }

         //-- 바이트수체크
         function BytesCheck(sValue) {
             var tcount = 0;

             var tmpStr = new String(sValue);
             var temp = tmpStr.length;

             var onechar;
             for (k = 0; k < temp; k++) {
                 onechar = tmpStr.charAt(k);
                 if (escape(onechar).length > 4) {
                     tcount += 2;
                 }
                 else {
                     tcount += 1;
                 }
             }

             return tcount;
         }        
        
    </script>
</head>
<body style="overflow-X:hidden">
    <form id="form1" runat="server">     
    <div>
        <!-- container -->
<div id="container" class="mate">

	<!-- wrapper -->
	<div id="wrapper" class="share translation-qna">
		<div style="display:inline;float:left;position:relative;width:658px;margin:0 -1px 0 0;padding:0 50px 100px;overflow:hidden;">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>번역 Q&amp;A</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<uc4:BoardDetail ID="BoardDetail1" runat="server" />
			<uc5:BoardList ID="BoardList1" runat="server" />
		</div>
		<!-- // contents -->

	</div>
	<!-- // wrapper -->

</div>
   
    </div>    
    <asp:hiddenfield ID="hidTableIdx" runat="server" Value ="1010"></asp:hiddenfield>       
    </form>
</body>
</html>
