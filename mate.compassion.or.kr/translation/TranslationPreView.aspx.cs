﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Data;

public partial class Mate_translation_TranslationPreView : System.Web.UI.Page
{
    public string sgHtml;

    protected void Page_Load(object sender, EventArgs e)
    {
        SetPreView();
    }

    /// <summary>
    /// 미리보기 셋팅
    /// </summary>
    private void SetPreView()
    {
        string sTabShowValue = Request.QueryString["TabShowValue"];
        string sCorrID = Request.QueryString["CorrID"];
        string sTempType;

        if (new TranslateInfo().TabSelectValue == "0")
            sTempType = "Translation";

        else
            sTempType = "Screening";

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        Object[] objSql = new object[1] { "SELECT * FROM CORR_TemporarySave WHERE TempType = '" + sTempType + "' AND CorrID = '" + sCorrID + "'" };

        //Object[] objSql = new object[1] { "SELECT ScreeningText as TempText FROM Corr_Screening where corrid='601' and ScreeningTimes = '2'" };

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

        string sPreView = ds.Tables[0].Rows[0]["TempText"].ToString();

        sPreView = sPreView.Replace("\r\n", "|");
        sPreView = sPreView.Replace("\n", "|");

        string sValue = "";

        int nNextCnt = 0;
        int nAddCnt = 0;
        int iPageCnt = 2;

        StringBuilder sbList = new StringBuilder();

        char[] obj = sPreView.ToCharArray();

        int maxB = 0;
        int iCnt = 0;

        for (int i = 0; i < obj.Length; i++)
        {
            if (obj[i].ToString() != "|")
            {
                //상위 1바이트를 가져온다
                byte oF = (byte)((obj[i] & 0xff00) >> 8);
                //하위 1바이트를 가져온다
                byte oB = (byte)(obj[i] & 0x00ff);

                if (oF == 0)//상위 1바이트가 0이면 알파벳이다
                    maxB = maxB + 6;

                else
                    maxB = maxB + 11;

                sValue += obj[i];

                if (iCnt > 20)
                {

                }

                if (maxB >= 528)//한줄에 대한 숫자 88개 문자 48개에 대한 최소 공배수
                {
                    sValue = sValue + "&#10;";
                    maxB = 0;
                    iCnt++;//줄 바꿈
                }
            }

            else
            {
                sValue = sValue + "&#10;";
                maxB = 0;
                iCnt++;//줄 바꿈
            }


            //1페이지
            if ((iCnt <= 24) && (i == obj.Length - 1) && (i == sPreView.Length - 1))//한 페이지 일 때
            {
                sbList.Append("<center>");
                sbList.Append("<table>");
                sbList.Append("<tr>");
                sbList.Append("<td>");
                sbList.Append("<center>");
                sbList.Append("[1 페이지]");
                sbList.Append("</center>");
                sbList.Append("</td>");
                sbList.Append("</tr>");
                sbList.Append("<tr>");
                sbList.Append("<td>");
                sbList.Append("<textarea cols='100' rows='25' readonly='readonly' style='overflow:hidden;font-family:굴림체;'>" + sValue + "</textarea>");
                sbList.Append("</td>");
                sbList.Append("<td>");
                sbList.Append("</tr>");
                sbList.Append("</table>");
                sbList.Append("</center>");

                sValue = "";
            }

            else if ((iCnt == 24) && (i != obj.Length - 1) && (maxB == 0))//다음 페이지가 있을 때
            {
                sbList.Append("<center>");
                sbList.Append("<table>");
                sbList.Append("<tr>");
                sbList.Append("<td>");
                sbList.Append("<center>");
                sbList.Append("[1 페이지]");
                sbList.Append("</center>");
                sbList.Append("</td>");
                sbList.Append("</tr>");
                sbList.Append("<tr>");
                sbList.Append("<td>");
                sbList.Append("<textarea cols='100' rows='25'  readonly='readonly' style='overflow:hidden;font-family:굴림체;'>" + sValue + "</textarea>");
                sbList.Append("</td>");
                sbList.Append("<td>");
                sbList.Append("</tr>");
                sbList.Append("</table>");
                sbList.Append("</center>");

                sValue = "";
                nNextCnt = iCnt + 27;
                nAddCnt = 24;
            }

            //2페이지 이상
            if (((nAddCnt < iCnt) && (nNextCnt >= iCnt)) && (i == obj.Length - 1))//한 페이지 일 때
            {
                sbList.Append("<center>");
                sbList.Append("<table>");
                sbList.Append("<tr>");
                sbList.Append("<td>");
                sbList.Append("<center>");
                sbList.Append("[" + iPageCnt + " 페이지]");
                sbList.Append("</center>");
                sbList.Append("</td>");
                sbList.Append("</tr>");
                sbList.Append("<tr>");
                sbList.Append("<td>");
                sbList.Append("<textarea cols='100' rows='28' readonly='readonly' style='overflow:hidden;font-family:굴림체;'>" + sValue + "</textarea>");
                sbList.Append("</td>");
                sbList.Append("<td>");
                sbList.Append("</tr>");
                sbList.Append("</table>");
                sbList.Append("</center>");

                iPageCnt++;
                sValue = "";
            }

            else if ((iCnt == nNextCnt) && (i != obj.Length - 1) && (maxB == 0))//다음 페이지가 있을 때
            {
                sbList.Append("<center>");
                sbList.Append("<table>");
                sbList.Append("<tr>");
                sbList.Append("<td>");
                sbList.Append("<center>");
                sbList.Append("[" + iPageCnt + " 페이지]");
                sbList.Append("</center>");
                sbList.Append("</td>");
                sbList.Append("</tr>");
                sbList.Append("<tr>");
                sbList.Append("<td>");
                sbList.Append("<textarea cols='100' rows='28' readonly='readonly' style='overflow:hidden;font-family:굴림체;'>" + sValue + "</textarea>");
                sbList.Append("</td>");
                sbList.Append("<td>");
                sbList.Append("</tr>");
                sbList.Append("</table>");
                sbList.Append("</center>");

                iPageCnt++;

                sValue = "";

                nNextCnt = nNextCnt + 27;
                nAddCnt = nAddCnt + 27;
            }
        }

        sgHtml = sbList.ToString();

        ds.Dispose();
        _WWW6Service.Dispose();
    }

    //한글 체크
    bool IsKorean(char ch)
    {
        //( 한글자 || 자음 , 모음 )
        if ((0xAC00 <= ch && ch <= 0xD7A3) || (0x3131 <= ch && ch <= 0x318E))
            return true;

        else
            return false;
    }

    //숫자 체크
    bool IsNumeric(char ch)
    {
        if (0x30 <= ch && ch <= 0x39)
            return true;

        else
            return false;
    }

    //영어 체크
    bool IsEnglish(char ch)
    {
        if ((0x61 <= ch && ch <= 0x7A) || (0x41 <= ch && ch <= 0x5A))
            return true;

        else
            return false;
    }
}