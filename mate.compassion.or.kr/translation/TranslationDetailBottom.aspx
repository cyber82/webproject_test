﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TranslationDetailBottom.aspx.cs" Inherits="Mate_translation_TranslationDetailBottom" EnableEventValidation="false" EnableViewStateMac="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function GoToList() {

            /* 주석처리 2013-08-28
            //추가 2013-08-28
            var page = document.getElementById("<%=hdIdxPage.ClientID %>").value;
            var sdate = document.getElementById("<%=hdStartDate.ClientID %>").value;
            var edate = document.getElementById("<%=hdEndDate.ClientID %>").value;
            var str = "?page=" + page + "&sdate=" + sdate + "&edate=" + edate;

            //수정 2013-08-28
            if ('<%=sgFromPage%>' == "00") {//홈
                parent.parent.document.location.href = '/translation/TranslationHome.aspx' + str;
            }
            else if ('<%=sgFromPage%>' == "10") {//번역리스트
                parent.parent.document.location.href = '/translation/TranslationMain.aspx' + str;
            }
            else if ('<%=sgFromPage%>' == "11") {//번역완료리스트
                parent.parent.document.location.href = '/translation/TranslationComplete.aspx' + str;
            }
            else if ('<%=sgFromPage%>' == "20") {//스크리닝리스트
                parent.parent.document.location.href = '/translation/ScreeningMain.aspx' + str;
            }
            else if ('<%=sgFromPage%>' == "21") {//스크리닝완료리스트
                parent.parent.document.location.href = '/translation/ScreeningComplete.aspx' + str;
            }
            else if ('<%=sgFromPage%>' == "30") {//검색리스트
                //parent.parent.document.location.href = '/translation/TranslationLetterSearch.aspx?SearchKind=<%=sgSearchKind%>&SearchText=<%=sgSearchText%>';
                history.back();
            }
            else if ('<%=sgFromPage%>' == "40") {//3PL리스트
                parent.parent.document.location.href = '/translation/ThirdPLMain.aspx' + str;
            }
            else if ('<%=sgFromPage%>' == "41") {//3PL완료리스트
                parent.parent.document.location.href = '/translation/ThirdPLComplete.aspx' + str;
            }*/

            history.back();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table style="width: 100%;">
        <tr>
            <% //수정 2013-09-25 %>
            <td style="text-align:left;">
                <asp:ImageButton ID="ImgBtnGoToList" runat="server" ToolTip="목록으로 돌아갑니다." ImageUrl="~/image/imate/btn/btn_list.jpg" />
            </td>
        </tr>
    </table>

    <% //추가 2013-08-28 %>
    <input type="hidden" id="hdIdxPage" name="hdIdxPage" runat="server" />
    <input type="hidden" id="hdStartDate" name="hdStartDate" runat="server" />
    <input type="hidden" id="hdEndDate" name="hdEndDate" runat="server" />

    </form>
</body>
</html>
