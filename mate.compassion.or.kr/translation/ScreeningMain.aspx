﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ScreeningMain.aspx.cs" Inherits="Mate_translation_ScreeningMain" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateTranslationHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>

<%@ Register src="../Controls/TranslationRight.ascx" tagname="TranslationRight" tagprefix="uc5" %>
<%@ Register src="../Controls/ScreeningMainList.ascx" tagname="ScreeningMainList" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
</head>
<body>
<form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
    <div id="container" class="mate">

	<!-- wrapper -->
	<div id="wrapper" class="share our-story">

		<!-- sidebar -->
        <uc5:TranslationRight ID="TranslationRight" runat="server" />
		<!-- //sidebar -->
		<!-- contents -->
		<div id="contents" style="min-height:900px;">
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><strong>스크리닝</strong>
			</div>
            <div>

            <uc4:ScreeningMainList ID="ScreeningMainList" runat="server" />
            </div>
		</div>
		<!-- // contents -->

	</div>
	<!-- // wrapper -->

</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    <asp:hiddenfield ID="hidTableIdx" runat="server" Value ="1222"></asp:hiddenfield>    
    </form>
</body>
</html>
