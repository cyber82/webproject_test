﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Data;

public partial class Mate_translation_TranslationTemplate : System.Web.UI.Page
{
    public string sgHtml;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetTranslationType() == "CHILDLETTER" || GetTranslationType() == "SCR")
        {
            //어린이편지 템플릿
            GetChildTemplate();
        }

        else if (GetTranslationType() == "SPONSORLETTER")
        {
            //후원자편지 템플릿
            GetSponsorTemplate();
        }
    }

    /// <summary>
    /// 번역 타입
    /// </summary>
    /// <returns>번역타입</returns>
    private string GetTranslationType()
    {
        Object[] objSql = new object[1] { "SELECT TranslationType FROM MATE_Master WHERE (MateYearCode = dbo.uf_GetMateYearCode(1) OR MateYearCode = dbo.uf_GetMateYearCode(2)) AND MateID = '" + new UserInfo().UserId + "'" };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

        return ds.Tables[0].Rows[0]["TranslationType"].ToString();
    }

    /// <summary>
    /// 어린이편지 템플릿
    /// </summary>
    private void GetChildTemplate()
    {
        StringBuilder sbList = new StringBuilder();
        StringBuilder sbListGroup = new StringBuilder();

        sbListGroup.Append("SELECT ROW_NUMBER() over(order bY MATE_TemplateGroup.CountryCode, MATE_TemplateGroup.NameEng) AS Idx, * FROM( ");
        sbListGroup.Append("SELECT ");
        sbListGroup.Append("a.CountryCode, b.NameEng, b.NameKr FROM MATE_Template a ");
        sbListGroup.Append("LEFT OUTER JOIN ");
        sbListGroup.Append("kr_Compass4.dbo.tCountryMaster b ");
        sbListGroup.Append("ON a.CountryCode = b.CountryCode ");
        sbListGroup.Append("WHERE CorrType IS NULL OR CorrType = '' ");
        sbListGroup.Append("GROUP BY a.CountryCode, b.NameEng, b.NameKr) AS MATE_TemplateGroup ");

        Object[] objSql = new object[1] { sbListGroup.ToString() };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        DataSet dsGroup = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

        string sTemplate = "";
        string sTemplateToPar = "";

        StringBuilder sbTemplate = new StringBuilder();

        foreach (DataRow drGroup in dsGroup.Tables[0].Rows)
        {
            sbTemplate.Append("<div style='font-size:10pt;'>");
            sbTemplate.Append("<input type='hidden' id='txtShowValue" + drGroup["Idx"].ToString() + "'/>");
            sbTemplate.Append("<table>");
            sbTemplate.Append("<tr style='cursor:pointer;' onclick=\"ShowTemplate('Level2_" + drGroup["Idx"].ToString() + "','txtShowValue" + drGroup["Idx"].ToString() + "','imgExtend" + drGroup["Idx"].ToString() + "','tdTitle" + drGroup["Idx"].ToString() + "')\">");
            sbTemplate.Append("<td>");
            sbTemplate.Append("<img id='imgExtend" + drGroup["Idx"].ToString() + "' src=\"../../image/imate/icon/icon_ext.png\">");
            sbTemplate.Append("</td>");
            sbTemplate.Append("<td id='tdTitle" + drGroup["Idx"].ToString() + "' title='템플릿 내용을 확인합니다'>");
            //sbTemplate.Append("<b>" + drGroup["NameKr"].ToString() + "(" + drGroup["NameEng"].ToString() + ")" + "</b>");
            sbTemplate.Append("<b>" + drGroup["CountryCode"].ToString() + " " + drGroup["NameKr"].ToString() + "</b>");
            sbTemplate.Append("</td>");
            sbTemplate.Append("</tr>");
            sbTemplate.Append("</table>");
            sbTemplate.Append("</div>");

            sbList = new StringBuilder();

            sbList.Append("SELECT ");
            sbList.Append("ROW_NUMBER() over(order bY a.OrderBy) AS Idx, ");
            sbList.Append("a.CountryCode, a.Title, a.Template , b.NameEng, b.NameKr FROM MATE_Template a ");
            sbList.Append("LEFT OUTER JOIN  ");
            sbList.Append("kr_Compass4.dbo.tCountryMaster b  ");
            sbList.Append("ON a.CountryCode = b.CountryCode  ");
            sbList.Append("WHERE (a.CorrType IS NULL OR a.CorrType = '') AND a.CountryCode = '" + drGroup["CountryCode"].ToString() + "' ");
            sbTemplate.Append("<div id='Level2_" + drGroup["Idx"].ToString() + "' style='font-size:10pt;'>");
            objSql = new object[1] { sbList.ToString() };

            DataSet dsList = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

            foreach (DataRow drList in dsList.Tables[0].Rows)
            {
                sTemplate = drList["Template"].ToString();
                sTemplate = sTemplate.Replace("''", "'");
                sTemplate = sTemplate.Replace("\r\n", "<br/>");
                sTemplate = sTemplate.Replace("\n", "<br/>");

                sTemplateToPar = drList["Template"].ToString();
                sTemplateToPar = sTemplateToPar.Replace("\r\n", "|n");
                sTemplateToPar = sTemplateToPar.Replace("\n", "|b");
                sTemplateToPar = sTemplateToPar.Replace("'", "＇");
                sTemplateToPar = sTemplateToPar.Replace("\"", "˝");
                
                sbTemplate.Append("<div id='Title" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "' style='margin:0px 0px 0px 20px;'>");
                sbTemplate.Append("<input type='hidden' id='txtShowMainValue" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "'/>");
                sbTemplate.Append("<table>");
                sbTemplate.Append("<tr style='cursor:pointer;' onclick=\"ShowTemplateMain('Template" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "','txtShowMainValue" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "','imgMainExtend" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "')\">");
                sbTemplate.Append("<td>");
                sbTemplate.Append("<img id='imgMainExtend" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "' src=\"../../image/imate/icon/icon_ext.png\">");
                sbTemplate.Append("</td>");
                sbTemplate.Append("<td style='width:250px;'>");
                sbTemplate.Append(drList["Title"].ToString());
                sbTemplate.Append("</td>");
                sbTemplate.Append("<td title='템플릿 내용을 번역 내용으로 입력합니다'>");
                sbTemplate.Append("<input type='image' src=\"../../image/imate/btn/btn_apply.jpg\" onclick=\"inputClose('" + sTemplateToPar + "'); return false;\"/>");
                sbTemplate.Append("</td>");
                sbTemplate.Append("</tr>");
                sbTemplate.Append("</table>");
                sbTemplate.Append("</div>");

                sbTemplate.Append("<div id='Template" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "' style='margin:0px 0px 0px 40px; font-size:10pt;'>");
                sbTemplate.Append("<table cellspacing='0' style='border: 1px solid #000000;border-style:solid'>");
                sbTemplate.Append("<tr>");
                sbTemplate.Append("<td>");
                sbTemplate.Append(sTemplate);
                sbTemplate.Append("</td>");
                sbTemplate.Append("</tr>");
                sbTemplate.Append("</table>");
                sbTemplate.Append("</div>");

                sbTemplate.Append("<script type='text/javascript'>");
                sbTemplate.Append("var TemplateMain = document.getElementById('Template" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "');");
                sbTemplate.Append("TemplateMain.style.display = 'none';");
                sbTemplate.Append("document.getElementById('txtShowMainValue" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "').value = '0';");
                sbTemplate.Append("</script>");
            }

            sbTemplate.Append("<script type='text/javascript'>");
            sbTemplate.Append("var Template = document.getElementById('Level2_" + drGroup["Idx"].ToString() + "');");
            sbTemplate.Append("Template.style.display = 'none';");
            sbTemplate.Append("document.getElementById('txtShowValue" + drGroup["Idx"].ToString() + "').value = '0';");
            sbTemplate.Append("</script>");
            sbTemplate.Append("</div>");

            dsList.Dispose();
        }

        sgHtml = sbTemplate.ToString();

        dsGroup.Dispose();
        _WWW6Service.Dispose();
    }

    /// <summary>
    /// 후원자편지 템플릿
    /// </summary>
    private void GetSponsorTemplate()
    {
        StringBuilder sbList = new StringBuilder();
        StringBuilder sbListGroup = new StringBuilder();

        sbListGroup.Append("SELECT ROW_NUMBER() over(order bY MATE_TemplateOrder.ORDER_BY) AS Idx, ");
        sbListGroup.Append("* FROM( ");
        sbListGroup.Append("SELECT CorrType, ");
        sbListGroup.Append("(SELECT dbo.uf_GetCommonName('CORR_TYPE_EXAMPLE',MATE_TemplateGroup.CorrType)) AS CorrName, ");
        sbListGroup.Append("(SELECT ORDER_BY FROM ETS_TC_CODE_MEMBER  ");
        sbListGroup.Append("WHERE DIM_CODE = 'CORR_TYPE_EXAMPLE' AND MEM_CODE = MATE_TemplateGroup.CorrType) AS ORDER_BY ");
        sbListGroup.Append("FROM (  ");
        sbListGroup.Append("SELECT CorrType ");
        sbListGroup.Append("FROM MATE_Template ");
        sbListGroup.Append("WHERE LEN(CorrType) > 0  ");
        sbListGroup.Append("GROUP BY CorrType) AS MATE_TemplateGroup  ");
        sbListGroup.Append(") AS MATE_TemplateOrder ");
        sbListGroup.Append("ORDER BY CorrName ");

        Object[] objSql = new object[1] { sbListGroup.ToString() };

        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        DataSet dsGroup = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

        string sTemplate = "";
        string sTemplateToPar = "";

        StringBuilder sbTemplate = new StringBuilder();

        foreach (DataRow drGroup in dsGroup.Tables[0].Rows)
        {
            sbTemplate.Append("<div>");
            sbTemplate.Append("<input type='hidden' id='txtShowValue" + drGroup["Idx"].ToString() + "'/>");
            sbTemplate.Append("<table>");
            sbTemplate.Append("<tr style='cursor:pointer;' onclick=\"ShowTemplate('Level2_" + drGroup["Idx"].ToString() + "','txtShowValue" + drGroup["Idx"].ToString() + "','imgExtend" + drGroup["Idx"].ToString() + "','tdTitle" + drGroup["Idx"].ToString() + "')\">");
            sbTemplate.Append("<td>");
            sbTemplate.Append("<img id='imgExtend" + drGroup["Idx"].ToString() + "' src=\"../../image/imate/icon/icon_ext.png\">");
            sbTemplate.Append("</td>");
            sbTemplate.Append("<td id='tdTitle" + drGroup["Idx"].ToString() + "' title='템플릿 내용을 확인합니다'>");
            sbTemplate.Append("<b>" + drGroup["CorrName"].ToString() + "</b>");
            sbTemplate.Append("</td>");
            sbTemplate.Append("</tr>");
            sbTemplate.Append("</table>");
            sbTemplate.Append("</div>");

            sbList = new StringBuilder();

            sbList.Append("SELECT ROW_NUMBER() over(order bY MATE_Template.OrderBy) AS Idx, ");
            sbList.Append("* ");
            sbList.Append("FROM MATE_Template ");
            sbList.Append("WHERE LEN(CorrType) > 0 AND CorrType = '" + drGroup["CorrType"].ToString() + "'");

            sbTemplate.Append("<div id='Level2_" + drGroup["Idx"].ToString() + "'>");
            objSql = new object[1] { sbList.ToString() };

            DataSet dsList = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

            foreach (DataRow drList in dsList.Tables[0].Rows)
            {
                sTemplate = drList["Template"].ToString();
                sTemplate = sTemplate.Replace("''", "'");
                sTemplate = sTemplate.Replace("\r\n", "<br/>");
                sTemplate = sTemplate.Replace("\n", "<br/>");
                
                sTemplateToPar = drList["Template"].ToString();
                sTemplateToPar = sTemplateToPar.Replace("\r\n", "|n");
                sTemplateToPar = sTemplateToPar.Replace("\n", "|b");
                sTemplateToPar = sTemplateToPar.Replace("'", "＇");
                sTemplateToPar = sTemplateToPar.Replace("\"", "˝");
                

                sbTemplate.Append("<div id='Title" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "' style='margin:0px 0px 0px 20px'>");
                sbTemplate.Append("<input type='hidden' id='txtShowMainValue" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "'/>");
                sbTemplate.Append("<table>");
                sbTemplate.Append("<tr style='cursor:pointer;' onclick=\"ShowTemplateMain('Template" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "','txtShowMainValue" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "','imgMainExtend" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "')\">");
                sbTemplate.Append("<td>");
                sbTemplate.Append("<img id='imgMainExtend" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "' src=\"../../image/imate/icon/icon_ext.png\">");
                sbTemplate.Append("</td>");
                sbTemplate.Append("<td style='width:250px;'>");
                sbTemplate.Append(drList["Title"].ToString());
                sbTemplate.Append("</td>");
                sbTemplate.Append("<td title='템플릿 내용을 번역 내용으로 입력합니다'>");
                sbTemplate.Append("<input type='image' src=\"../../image/imate/btn/btn_apply.jpg\" onclick=\"inputClose('" + sTemplateToPar + "'); return false;\"/>");
                sbTemplate.Append("</td>");
                sbTemplate.Append("</tr>");
                sbTemplate.Append("</table>");
                sbTemplate.Append("</div>");

                sbTemplate.Append("<div id='Template" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "' style='margin:0px 0px 0px 40px'>");
                sbTemplate.Append("<table cellspacing='0' style='border: 1px solid #000000;border-style:solid'>");
                sbTemplate.Append("<tr>");
                sbTemplate.Append("<td>");
                sbTemplate.Append(sTemplate);
                sbTemplate.Append("</td>");
                sbTemplate.Append("</tr>");
                sbTemplate.Append("</table>");
                sbTemplate.Append("</div>");

                sbTemplate.Append("<script type='text/javascript'>");
                sbTemplate.Append("var TemplateMain = document.getElementById('Template" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "');");
                sbTemplate.Append("TemplateMain.style.display = 'none';");
                sbTemplate.Append("document.getElementById('txtShowMainValue" + drGroup["Idx"].ToString() + "_" + drList["Idx"].ToString() + "').value = '0';");
                sbTemplate.Append("</script>");
            }

            sbTemplate.Append("<script type='text/javascript'>");
            sbTemplate.Append("var Template = document.getElementById('Level2_" + drGroup["Idx"].ToString() + "');");
            sbTemplate.Append("Template.style.display = 'none';");
            sbTemplate.Append("document.getElementById('txtShowValue" + drGroup["Idx"].ToString() + "').value = '0';");
            sbTemplate.Append("</script>");
            sbTemplate.Append("</div>");

            dsList.Dispose();
        }

        sgHtml = sbTemplate.ToString();

        dsGroup.Dispose();
    }
}