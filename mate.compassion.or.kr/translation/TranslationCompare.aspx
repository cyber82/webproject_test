﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TranslationCompare.aspx.cs" Inherits="Mate_translation_TranslationCompare" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
<div style="margin:20px 20px 20px 20px">
    <div style="position:relative; top:0px; left:0px; width:100%; height:100px; overflow:hidden;"> 
        <img scr='' src="../../image/mate/bg-header.jpg" />
        <div style='position:absolute; top:25%; left:30px; width:100px; height:80px; z-index:1;'>
        <img scr='' src="../../image/mate/logo.png" />
        </div> 
        <div style='position:absolute; top:40%; left:150px; z-index:2;font-weight:900;'>
        <font color="white" size = "5" face="굴림체" ><b>스크리닝 상세보기</b></font>
        </div> 
        
    </div>
        </br>

        <table style="width:100%;">
            <tr>
                
                <td style="width:49%;">
                <b>번역</b>
                </td>
                <td style="width:49%;">
                <b>스크리닝</b>
                </td>
                
            </tr>
        </table>
       
        <div style="width:49%; height:100%; float:left; border: 1px solid #CCCCCC;border-style:solid;font-family:굴림">
        <asp:Panel ID="pnlTranslate" runat="server" Width="100%" Height="620px" BorderWidth="0" ScrollBars="Both" >
            <asp:Literal ID="txtTranslate" runat="server"></asp:Literal>
        </asp:Panel>
        </div>
        
        
        <div style="width:49%; height:100%; float:right; border: 1px solid #CCCCCC;border-style:solid;font-family:굴림">
        <asp:Panel ID="pnlScreening" runat="server" Width="100%" Height="620px" BorderWidth="0" ScrollBars="Both" >
            <asp:Literal ID="txtScreening" runat="server"></asp:Literal>
        </asp:Panel>
        </div>
        
       
    </div>
    </form>
</body>
</html>
