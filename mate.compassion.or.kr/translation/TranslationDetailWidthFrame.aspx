﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TranslationDetailWidthFrame.aspx.cs" Inherits="Mate_translation_TranslationDetailWidthFrame" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <%
    if (isMobile) {
        %>
    <style>
        .iframe-wrap {
            width: 100%;
            
            position: relative;
            overflow-x: scroll;
            overflow-y: scroll;
            -webkit-overflow-scrolling: touch;
        }
        .inner {
            width: 100%;
            /* Android 이중 스크롤 방지 */
            position:absolute; top:0; left:0;
        }
        
    </style>
    <%
    }
%>


    <%
        if(isMobile) {
            %>
    
           
            <div class="iframe-wrap" style="height:900px;border:0px;width:50%;float:left">
                 <iframe class="inner" style="height:900px;width:400px;" src="TranslationDetailImage.aspx?CorrID=<%=sgCorrID%>&PackageID=<%=sgPackageID%>&PageCount=<%=sgPageCount%>&FilePath=<%=sgFilePath%>&ChildKey=<%=sgChildKey%>&ThirdYear=<%=sgThirdYear%>&ProjectNumber=<%=sgProjectNumber%>" name="TranslationDetailImage" ></iframe>
            </div>
            <div class="iframe-wrap" style="height:900px;border:0px;width:50%;float:left;">
                 <iframe class="inner" style="height:900px;width:400px;" src="TranslationDetailWrite.aspx?CorrID=<%=sgCorrID%>&PackageID=<%=sgPackageID%>&PageCount=<%=sgPageCount%>&ThirdYear=<%=sgThirdYear%>&ProjectNumber=<%=sgProjectNumber%>&ShowValue=1" name="TranslationDetailWrite" ></iframe>
            </div>
           <iframe src="TranslationDetailBottom.aspx" name="TranslationDetailBottom" frameborder="no" scrolling="auto" marginwidth="0" marginheight="0"/>


            
    <%  
        } else {
                      %>
    
    <frameset rows="80%, *"  framespacing="15" frameborder="2" bordercolor="#9D9DA1">
    <frameset cols="50%, *" framespacing="15" frameborder="2" bordercolor="#9D9DA1">
         <frame src="TranslationDetailImage.aspx?CorrID=<%=sgCorrID%>&PackageID=<%=sgPackageID%>&PageCount=<%=sgPageCount%>&FilePath=<%=sgFilePath%>&ChildKey=<%=sgChildKey%>&ThirdYear=<%=sgThirdYear%>&ProjectNumber=<%=sgProjectNumber%>" name="TranslationDetailImage" frameborder="1" scrolling="auto"  marginwidth="0" marginheight="0"/>
         <frame src="TranslationDetailWrite.aspx?CorrID=<%=sgCorrID%>&PackageID=<%=sgPackageID%>&PageCount=<%=sgPageCount%>&ThirdYear=<%=sgThirdYear%>&ProjectNumber=<%=sgProjectNumber%>&ShowValue=1" name="TranslationDetailWrite" frameborder="1" scrolling="no" marginwidth="0" marginheight="0"  />
   </frameset>
   <frame src="TranslationDetailBottom.aspx" name="TranslationDetailBottom" frameborder="no" scrolling="auto" marginwidth="0" marginheight="0"/>
</frameset>


    <%
                  }
    %>


 <noframes>
 <body>
 </body>
 </noframes>
</html>
