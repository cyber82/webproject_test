﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mate_translation_TranslationDetailBottom : System.Web.UI.Page
{
    public string sgFromPage;
    public string sgCorrID;
    public string sgSearchKind;
    public string sgSearchText;

    public string page;
    public string sdate;
    public string edate;

    /// <summary>
    ///  페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        // iMate Log
        UserInfo user = new UserInfo();

        WWWService.Service wwwService = new WWWService.Service();

        sgFromPage = new TranslateInfo().MatePageType;
        sgCorrID = Request.QueryString["CorrID"];
        sgSearchKind = Request.QueryString["SearchKind"];
        sgSearchText = Request.QueryString["SearchText"];

        if (Request["hdIdxPage"] != null)
        {
            page = Request["hdIdxPage"].ToString();
            hdIdxPage.Value = page;
        }
        if (Request["start_date"] != null)
        {
            sdate = Request["start_date"].ToString();
            hdStartDate.Value = sdate;
        }
        if (Request["end_date"] != null)
        {
            edate = Request["end_date"].ToString();
            hdEndDate.Value = edate;
        }
        
        ImgBtnGoToList.Attributes.Add("onClick", "GoToList(); return false;");//목록으로 돌아가기

        wwwService.Dispose();
    }
}