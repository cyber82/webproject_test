﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TranslationMain.aspx.cs" Inherits="Mate_translation_TranslationMain" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateTranslationHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>

<%@ Register src="../Controls/TranslationRight.ascx" tagname="TranslationRight" tagprefix="uc5" %>
<%@ Register src="../Controls/TranslationMainList.ascx" tagname="TranslationMainList" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript" type="text/javascript">
        //Modal Popup Start
        var m_pop_isUse = true;
        var m_pop_cookieName = "modalPopupTranslationMain";
        var m_pop_fromDate = '2018-01-19 16:00:00', m_pop_toDate = '2018-01-25 17:00:00';
       
        function getCookie(name) {
            var nameOfCookie = name + "=";
            var x = 0;
            while (x <= document.cookie.length) {
                var y = (x + nameOfCookie.length);
                if (document.cookie.substring(x, y) == nameOfCookie) {
                    if ((endOfCookie = document.cookie.indexOf(";", y)) == -1)
                        endOfCookie = document.cookie.length;
                    return unescape(document.cookie.substring(y, endOfCookie));
                }
                x = document.cookie.indexOf(" ", x) + 1;
                if (x == 0) break;
            }
            return "";
        }

        function setCookie(name, value, expiredays) {
            var domain = "";
            var loc = String(location.href);
            if (loc.indexOf('compassion.or.kr') != -1) domain = '.compassion.or.kr';
            else if (loc.indexOf('compassionkr.com') != -1) domain = '.compassionkr.com';
            else if (loc.indexOf('localhost') != -1) domain = 'localhost:53720';

            var todayDate = new Date();
            
            todayDate.setDate(todayDate.getDate() + expiredays);
            document.cookie = name + "=" + escape(value) + "; path=/; domain=" + domain + "; expires=" + todayDate.toGMTString() + ";"
        }

        function CloseWin() {
            if (document.getElementById("Notice").checked) {
                setCookie(m_pop_cookieName, "done", 1);
                $('#modalPopup').css('display', 'none');
            } else {
                $('#modalPopup').css('display', 'none');
            }
        }

        String.prototype.toDouble = function () {
            return (this.length < 2 ? '0' : '') + this;
        }

        function getNowDate() {
            var nowDate = new Date();
            return nowDate.getUTCFullYear() + '-' +
            String(nowDate.getUTCMonth() + 1).toDouble() + '-' +
            String(nowDate.getUTCDate()).toDouble() + ' ' + 
            String(nowDate.getUTCHours() + 9).toDouble() + ':00:00';
        }

        $(document).ready(function () {
            if (m_pop_isUse) {
                var nowDate = getNowDate();
                console.log(nowDate);
                if (nowDate >= m_pop_fromDate && nowDate <= m_pop_toDate) {
                    if (getCookie(m_pop_cookieName) != 'done') {
                        $('#modalPopup').css('display', 'block');
                    }
                }
            }
        });
        //Modal Popup End
    </script>
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
    <div id="container" class="mate">

	<!-- wrapper -->
	<div id="wrapper" class="share our-story">

		<!-- sidebar -->
        <uc5:TranslationRight ID="TranslationRight" runat="server" />
		<!-- //sidebar -->
		<!-- contents -->
		<div id="contents">
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><strong>번역</strong>
			</div>
            <div>
            <%--<table style='width:100%;'>
                <tr>
                    <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    스캔일자
                    </th>
                    <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    편지ID
                    </th>
                    <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    패키지ID
                    </th>
                    <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    편지타입
                    </th>
                    <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    후원자ID
                    </th>
                    <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    후원자명
                    </th>
                    <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    어린이키
                    </th>
                    <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    이린이명
                    </th>
                    <th style='background-color:#E6E6E6; height:25px; border:1px solid #CCCCCC;'>
                    쪽수
                    </th>
                </tr>
                <tr>
                    <td style='word-break:break-all; border:1px solid #CCCCCC; height:30px;'><center></center>
                    </td>
                    <td style='word-break:break-all; border:1px solid #CCCCCC; height:30px;'><center></center>
                    </td>
                    <td style='word-break:break-all; border:1px solid #CCCCCC; height:30px;'><center></center>
                    </td>
                    <td style='word-break:break-all; border:1px solid #CCCCCC; height:30px;'><center></center>
                    </td>
                    <td style='word-break:break-all; border:1px solid #CCCCCC; height:30px;'><center></center>
                    </td>
                    <td style='word-break:break-all; border:1px solid #CCCCCC; height:30px;'><center></center>
                    </td>
                    <td style='word-break:break-all; border:1px solid #CCCCCC; height:30px;'><center></center>
                    </td>
                    <td style='word-break:break-all; border:1px solid #CCCCCC; height:30px;'><center></center>
                    </td>
                    <td style='word-break:break-all; border:1px solid #CCCCCC; height:30px;'><center></center>
                    </td>
                </tr>
            </table>--%>
            <uc4:TranslationMainList ID="TranslationMainList" runat="server" />
            </div>
		</div>
		<!-- // contents -->

	</div>
	<!-- // wrapper -->

</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    <asp:hiddenfield ID="hidTableIdx" runat="server" Value ="1222"></asp:hiddenfield>    
    </form>

    <div id="modalPopup" style="display:none; border:2px solid #000000; width:400px; height:620px; position:absolute; top:100px; left:100px; z-index:100000; background-color:#ededed; ">
        <img src="../image/popup/MATE_POPUP_엔다카츄.jpg" usemap="#popupApply" />
        <map name="popupApply">
            <area shape="rect" coords="49,508,350,564" href="https://goo.gl/forms/NciEeSzWGfE6UmZs1" alt="신청하기" />
        </map>
        <div align="right" style="font-size:8pt">        
            <input type="checkbox" name="Notice" id="Notice"  >
              오늘하루 이창을 열지않습니다.[<a href="javascript:CloseWin();">닫기]</a>&nbsp;&nbsp;&nbsp; 
        </div>
    </div>
</body>
</html>
