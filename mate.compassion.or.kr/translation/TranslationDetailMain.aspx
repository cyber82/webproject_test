﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TranslationDetailMain.aspx.cs" Inherits="Mate_translation_TranslationDetailMain" EnableEventValidation="false" EnableViewStateMac="false" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register Src="../Controls/MateTranslationHeader.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="../Controls/MateTop.ascx" TagName="Top" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc3" %>
<%@ Register Src="../Controls/TranslationRight.ascx" TagName="TranslationRight" TagPrefix="uc5" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <style>
		iframe {
			min-width: 100%;
			width: 100px;
			*width: 100%;
		}
	</style>
    <script type="text/javascript">

    function IMateShow() {      
        if (document.getElementById('iMateInfo').style.display != "none") {         
            document.getElementById('iMateInfo').style.display = "none"// 혹은 "block" 

            document.getElementById("tbMain").style.height = "1125px";
            document.getElementById("txtTableShow").value = "0"
        }else {          
            document.getElementById('iMateInfo').style.display = ""

            document.getElementById("tbMain").style.height = "1000px";
            document.getElementById("txtTableShow").value = "1"
        }
    }

    function GetFrameText() {      
        document.getElementById("txtTranslateInf").value = window.frames["Showframe"].TranslationDetailWrite.document.getElementById("txtTranslate").value;
        document.getElementById("txtScreeningInf").value = window.frames["Showframe"].TranslationDetailWrite.document.getElementById("txtScreening").value;
        document.getElementById("txtThirdEngInf").value = window.frames["Showframe"].TranslationDetailImage.document.getElementById("txtThirdEnglish").value;
        document.getElementById("txtThirdKorInf").value = window.frames["Showframe"].TranslationDetailWrite.document.getElementById("txtThirdKorean").value;
        
        if (document.getElementById('txtShowValue').value == "") {
            document.getElementById('txtShowValue').value = "1"
        }else if (document.getElementById('txtShowValue').value == "1") {
            document.getElementById('txtShowValue').value = "0"
        }else if (document.getElementById('txtShowValue').value == "0") {
            document.getElementById('txtShowValue').value = "1"
        }
    }

    function SetFrameTextInf(sTranslateInf, sScreeningInf) {  
        document.getElementById("txtTranslateInf").value = sTranslateInf;
        document.getElementById("txtScreeningInf").value = sScreeningInf;     
    }

    function SetFrameText(sShowValue, sTabSelectValue) {
        document.getElementById('txtShowValue').value = sShowValue;

        if (document.getElementById('txtTabSelectValue').value == '') {         
            document.getElementById('txtTabSelectValue').value = sTabSelectValue;
        }
    }

    function SetLotationImage() {
        if(document.getElementById('txtShowValue').value == '1'){ 
            document.getElementById('ImgBtnShowType').src ='/image/imate/btn/btn_heightView.jpg'
        }else{
            document.getElementById('ImgBtnShowType').src ='/image/imate/btn/btn_widthView.jpg'
        }
    }

    function PageLoad() {
            document.getElementById("txtImgIndex").value ="1/" + '<%=sgPageCount%>'
            document.getElementById('txtImgCount').value = '<%=sgPageCount%>'
            document.getElementById('txtImgNum').value = '1'
            document.getElementById('txtTurnValue').value = '0'
    }
        
    function ExpCntCheck() {
        if (document.getElementById("txtCnt").value == "") {
            document.getElementById("txtCnt").value = 10
        }

        if (document.getElementById("txtCnt").value == 18) {
            alert("더 이상 확대 할 수 없습니다");

            return false;
        }
      
        document.getElementById("txtCnt").value = Number(document.getElementById("txtCnt").value) + 1
       
        window.frames["Showframe"].TranslationDetailImage.ExpCntCheck(document.getElementById("txtCnt").value);
    }

    function RedCntCheck() {
        if (document.getElementById("txtCnt").value == "") {

            document.getElementById("txtCnt").value = 8
        }

        if (document.getElementById("txtCnt").value == 0) {

            alert("더 이상 축소 할 수 없습니다");

            return false;
        }

        document.getElementById("txtCnt").value = Number(document.getElementById("txtCnt").value) - 1

        window.frames["Showframe"].TranslationDetailImage.RedCntCheck(document.getElementById("txtCnt").value);
    }

    //이전 이미지
    function MoveLeft() {
        if (document.getElementById("txtImgNum").value == 1) {

            alert("처음 이미지 입니다");

            return false;
        }else {
            document.getElementById("txtImgNum").value = Number(document.getElementById("txtImgNum").value) - 1
            document.getElementById("txtImgIndex").value = document.getElementById("txtImgNum").value + "/" + '<%=sgPageCount%>'
        }

        window.frames["Showframe"].TranslationDetailImage.ImageSet(document.getElementById("txtImgNum").value);
    }

    //다음 이미지
    function MoveRight() {
        if (document.getElementById("txtImgNum").value == '<%=sgPageCount%>') {

            alert("마지막 이미지 입니다");

            return false;
        } else {
            document.getElementById("txtImgNum").value = Number(document.getElementById("txtImgNum").value) + 1
            document.getElementById("txtImgIndex").value = document.getElementById("txtImgNum").value + "/" + '<%=sgPageCount%>'
        }

        window.frames["Showframe"].TranslationDetailImage.ImageSet(document.getElementById("txtImgNum").value);       
    }

    //이미지 왼쪽 회전
    function ImageLeftTurn() {
        if( document.getElementById('txtTurnValue').value == '0'){
            document.getElementById('txtTurnValue').value = '3'
        } else{
            document.getElementById('txtTurnValue').value = document.getElementById('txtTurnValue').value - 1
        }
        
        window.frames["Showframe"].TranslationDetailImage.ImageTurn(document.getElementById('txtTurnValue').value);
    }

    //이미지 오른쪽 회전
    function ImageRightTurn() {
        if( document.getElementById('txtTurnValue').value == '3') {
            document.getElementById('txtTurnValue').value = '0'
        } else{
            document.getElementById('txtTurnValue').value = Number(document.getElementById('txtTurnValue').value) + 1
        }

        window.frames["Showframe"].TranslationDetailImage.ImageTurn(document.getElementById('txtTurnValue').value);
    }

    function MainTopView(sMatePageType) {
        if (sMatePageType == '40' || sMatePageType == '41') {
            document.getElementById('iMateTop').style.display = "none"
            document.getElementById("tdImgMethod").style.display = "none";
        }else {
            document.getElementById('ThirdInfo').style.display = "none"
        }
    }

    //팝업창 생성
    function openBrWindow(sAddr, sPopName, sWidth, sHeight, sResizable) {
        //window.open("http://eschyles.mireene.com/", "", "");  //속성 지정하지 않은 기본창

        if (screen.width < 1025) {
            LeftPosition = 0;
            TopPosition = 0;
        } else {
            LeftPosition = (screen.width) ? (screen.width - sWidth) / 2 : 100;
            TopPosition = (screen.height) ? (screen.height - sHeight) / 2 : 100;
        }

        //window.open(sAddr, "", "");
        if (sResizable == "0") {
            window.open(sAddr, sPopName, "width=" + sWidth + ",height=" + sHeight + ",top=" + TopPosition + ",left=" + LeftPosition + ", scrollbars=yes, toolbar=no menubar=no, location=no");
        }else {
            window.open(sAddr, sPopName, "width=" + sWidth + ",height=" + sHeight + ",top=" + TopPosition + ",left=" + LeftPosition + ", scrollbars=yes, toolbar=no menubar=no, location=no, resizable=yes");
        }
    }

    </script>
<uc1:Header ID="Header1" runat="server" />
</head>
<body onload="PageLoad();">
    <form id="form1" runat="server">
    <div>
        <uc2:Top ID="Top1" runat="server" />
        <!-- container -->
        <div id="container" class="mate">
            <!-- wrapper -->
            <div id="wrapper" class="share our-story">
                <!-- sidebar -->
                <uc5:TranslationRight ID="TranslationRight" runat="server" />
                <!-- //sidebar -->
                <!-- contents -->
                <div id="contents">
                    <div class="breadcrumbs">
                            <%if (sgMatePageType == "00")
                              {
                                  if (sgHomeMateType == "TRANSTART")
                                  {%>
                                  <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a
                                    href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><a href="/translation/TranslationHome.aspx">홈</a><strong>번역상세</strong>
                                    <%}
                                  else if (sgHomeMateType == "SCREND")
                                  { %>
                                  <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a
                                    href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><a href="/translation/TranslationHome.aspx">홈</a><strong>스크리닝완료상세</strong>
                                    <%}
                                  else if (sgHomeMateType == "SCRSTART")
                                  {%>
                                  <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a
                                    href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><a href="/translation/TranslationHome.aspx">홈</a><strong>스크리닝상세</strong>
                                  <%} %>
                            <%} %>
                            <%if (sgMatePageType == "10")
                              { %>
                              <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a
                                    href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><a href="/translation/TranslationMain.aspx">번역</a><strong>번역상세</strong>
                            <%} %>
                            <%if (sgMatePageType == "11")
                              { %>
                              <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a
                                    href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><a href="/translation/TranslationComplete.aspx">번역완료</a><strong>번역완료상세</strong>
                            <%} %>
                            <%if (sgMatePageType == "20")
                              { %>
                              <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a
                                    href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><a href="/translation/ScreeningMain.aspx">스크리닝</a><strong>스크리닝상세</strong>
                            <%} %>
                            <%if (sgMatePageType == "21")
                              { %>
                              <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a
                                    href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><a href="/translation/ScreeningComplete.aspx">스크리닝완료</a><strong>스크리닝완료상세</strong>
                            <%} %>
                            <%if (sgMatePageType == "30")
                              { %>
                              <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a
                                    href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><a href="/translation/TranslationLetterSearch.aspx">편지조회</a><strong>편지조회상세</strong>
                            <%} %>
                            <%if (sgMatePageType == "40")
                              { %>
                              <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a
                                    href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><a href="/translation/ThirdPLMain.aspx">3PL번역</a><strong>3PL번역상세</strong>
                            <%} %>
                            <%if (sgMatePageType == "41")
                              { %>
                              <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a
                                    href="/Default.aspx">MATE</a><a href="/translation/TranslationHome.aspx">iMATE</a><a href="/translation/ThirdPLComplete.aspx">3PL번역완료</a><strong>3PL번역완료상세</strong>
                            <%} %>
                         </div>    
                    <div id="iMateTop"> 
                    <table>
                        <input type="hidden" id="txtTableShow" />
                        <tr>
                            <td>
                                &nbsp<asp:ImageButton ID="ImgBtnShowMateInfo" runat="server" ImageUrl="~/image/imate/btn/btn_hide.jpg"
                                    ToolTip="테이블을 숨기거나 보여줍니다." Height="22px" />
                            </td>
                        </tr>
                    </table>
                    </br>
                    <div id="iMateInfo">
                        <input type="text" id="test" runat="server" />
                        <table style='width: 100%;'>
                            <tr>
                                <th style='background-color: #E6E6E6; height: 20px; border: 1px solid #CCCCCC;'>
                                    후원자
                                </th>
                                <td colspan="2" style='border: 1px solid #CCCCCC;'>
                                    &nbsp;<asp:Label ID="lblConID" runat="server" Text=""></asp:Label>
                                </td>
                                <td colspan="2" style='border: 1px solid #CCCCCC;'>
                                    &nbsp;<asp:Label ID="lblSponsorName" runat="server" Text=""></asp:Label>
                                </td>
                                <td colspan="2" style='border: 1px solid #CCCCCC;'>
                                    &nbsp;<asp:Label ID="lblSponsorEngName" runat="server" Text=""></asp:Label>
                                </td>
                                <%--<td style='border:1px solid #CCCCCC;'>&nbsp;<asp:Label ID="lblUserID" runat="server" Text=""></asp:Label></td>--%>
                                <td style='border: 1px solid #CCCCCC; width: 18%;'>
                                    &nbsp;<asp:Label ID="lblUserGender" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th style='background-color: #E6E6E6; width: 10%; height: 20px; border: 1px solid #CCCCCC;'>
                                    어린이
                                </th>
                                <td colspan="2" style='border: 1px solid #CCCCCC; width: 22%;'>
                                    &nbsp;<asp:Label ID="lblChildKey" runat="server" Text=""></asp:Label>
                                </td>
                                <td colspan="2" style='border: 1px solid #CCCCCC; width: 23%;'>
                                    &nbsp;<asp:Label ID="lblChildName" runat="server" Text=""></asp:Label>
                                </td>
                                <td colspan="2" style='border: 1px solid #CCCCCC; width: 23%;'>
                                    &nbsp;<asp:Label ID="lblChildEngName" runat="server" Text=""></asp:Label>
                                </td>
                                <td style='border: 1px solid #CCCCCC; width: 22%;'>
                                    &nbsp;<asp:Label ID="lblChildGender" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th style='background-color: #E6E6E6; width: 10%; height: 20px; border: 1px solid #CCCCCC;'>
                                    어린이센터
                                </th>
                                <td colspan="3" style='border: 1px solid #CCCCCC; width: 45%;'>
                                    &nbsp;<asp:Label ID="lblProjectID" runat="server" Text=""></asp:Label>
                                </td>
                                <td colspan="4" style='border: 1px solid #CCCCCC; width: 45%;'>
                                    &nbsp;<asp:Label ID="lblProjectName" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th style='background-color: #E6E6E6; width: 10%; height: 20px; border: 1px solid #CCCCCC;'>
                                    편지ID
                                </th>
                                <td colspan="3" style='border: 1px solid #CCCCCC; width: 40%;'>
                                    &nbsp;<asp:Label ID="lblCorrID" runat="server" Text=""></asp:Label>
                                </td>
                                <th style='background-color: #E6E6E6; width: 10%; border: 1px solid #CCCCCC;'>
                                    편지타입
                                </th>
                                <td colspan="3" style='border: 1px solid #CCCCCC; width: 40%;'>
                                    &nbsp;<asp:Label ID="lblCorrType" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <%if (sgMatePageType != "30")
                              {%>
                            <tr>
                                <th style='background-color: #E6E6E6; width: 10%; height: 20px; border: 1px solid #CCCCCC;'>
                                    번역자
                                </th>
                                <td style='border: 1px solid #CCCCCC; width: 13%;'>
                                    &nbsp;<asp:Label ID="lblTransID" runat="server" Text=""></asp:Label>
                                </td>
                                <td style='border: 1px solid #CCCCCC; width: 13%;'>
                                    &nbsp;<asp:Label ID="lblTransName" runat="server" Text=""></asp:Label>
                                </td>
                                <td style='border: 1px solid #CCCCCC; width: 13%;'>
                                    &nbsp;<asp:Label ID="lblTranslationLevel" runat="server" Text=""></asp:Label>
                                </td>
                                <th style='background-color: #E6E6E6; width: 10%; border: 1px solid #CCCCCC;'>
                                    번역시작일
                                </th>
                                <td style='border: 1px solid #CCCCCC; width: 15%;'>
                                    &nbsp;<asp:Label ID="lblTransSDate" runat="server" Text=""></asp:Label>
                                </td>
                                <th style='background-color: #E6E6E6; width: 10%; border: 1px solid #CCCCCC;'>
                                    번역종료일
                                </th>
                                <td style='border: 1px solid #CCCCCC; width: 15%;'>
                                    &nbsp;<asp:Label ID="lblTransEDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <%} %>
                            <%if (sgMatePageType != "10")
                              {
                                  if (sgMatePageType != "30")
                                  {%>
                            <tr>
                                <th style='background-color: #E6E6E6; height: 20px; border: 1px solid #CCCCCC;'>
                                    스크리너
                                </th>
                                <td style='border: 1px solid #CCCCCC;'>
                                    &nbsp;<asp:Label ID="lblScreeningID" runat="server" Text=""></asp:Label>
                                </td>
                                <td colspan="2" style='border: 1px solid #CCCCCC;'>
                                    &nbsp;<asp:Label ID="lblScreeningName" runat="server" Text=""></asp:Label>
                                </td>
                                <th style='background-color: #E6E6E6; border: 1px solid #CCCCCC;'>
                                    스크리닝시작일
                                </th>
                                <td style='border: 1px solid #CCCCCC;'>
                                    &nbsp;<asp:Label ID="lblScreeningSDate" runat="server" Text=""></asp:Label>
                                </td>
                                <th style='background-color: #E6E6E6; border: 1px solid #CCCCCC;'>
                                    스크리닝종료일
                                </th>
                                <td style='border: 1px solid #CCCCCC;'>
                                    &nbsp;<asp:Label ID="lblScreeningEDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <%}
                              } %>
                        </table>
                    </div>
                    </div>
                    <div id="ThirdInfo">
                        <table style='width: 100%;'>
                            <tr>
                                <th style='background-color: #E6E6E6; height: 20px; border: 1px solid #CCCCCC;'>
                                    프로젝트
                                </th>
                                <td style='border: 1px solid #CCCCCC;'>
                                    &nbsp;<asp:Label ID="lblThirdProjectNumber" runat="server" Text=""></asp:Label>
                                </td>
                                <td style='border: 1px solid #CCCCCC;'>
                                    &nbsp;<asp:Label ID="lblThirdProjectName" runat="server" Text=""></asp:Label>
                                </td>
                                
                            </tr>
                            <tr>
                                <th style='background-color: #E6E6E6; height: 20px; border: 1px solid #CCCCCC;'>
                                    작성자
                                </th>
                                <td style='border: 1px solid #CCCCCC;'>
                                    &nbsp;<asp:Label ID="lblActualAuthorRole" runat="server" Text=""></asp:Label>
                                </td>
                                <td style='border: 1px solid #CCCCCC; '>
                                    &nbsp;<asp:Label ID="lblAuthorName" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                     </div>
                    </br>
                    <div>
                        <input type="hidden" id="txtTranslateInf" name="txtTranslateInf" />
                        <input type="hidden" id="txtScreeningInf" name="txtScreeningInf" />
                        <input type="hidden" id="txtThirdEngInf" name="txtThirdEngInf" />
                        <input type="hidden" id="txtThirdKorInf" name="txtThirdKorInf" />
                        <input type="hidden" id="txtShowValue" name="txtShowValue" />
                        <input type="hidden" id="txtTabSelectValue" name="txtTabSelectValue" />
                        <input type="hidden" id="txtTurnValue" name="txtTurnValue" />
                        <input id="txtCnt" name="txtCnt" type="hidden" />
                        <input id="txtImgNum" name="txtImgNum" type="hidden" />
                        <input id="txtImgCount" name="txtImgNum" type="hidden" />
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: left">
                                    &nbsp<asp:ImageButton ID="ImgBtnShowType" runat="server" OnClick="ImgBtnShowType_Click"
                                        ImageUrl="~/image/imate/btn/btn_heightView.jpg" ToolTip="편지이미지와 번역란을&#10;가로보기 또는 세로보기로 전환합니다." Height="22px" src=""/>
                                </td>
                                <td id="tdImgMethod" style="text-align: right">
                                    <input id="txtImgIndex" name="txtImgIndex" type="text" style="width: 25px; font-size:15px;" readonly="readonly" />
                                    <asp:ImageButton ID="imgBtnLeft" runat="server" ImageUrl="~/image/imate/btn/btn_left.jpg"
                                        ToolTip="이전 장으로 이동합니다." />
                                    <asp:ImageButton ID="imgBtnRight" runat="server" ImageUrl="~/image/imate/btn/btn_right.jpg"
                                        ToolTip="다음 장으로 이동합니다." />
                                    <asp:ImageButton ID="imgBtnExtension" runat="server" ImageUrl="~/image/imate/btn/btn_extension.jpg"
                                        ToolTip="이미지를 확대합니다." />
                                    <asp:ImageButton ID="imgBtnReduction" runat="server" ImageUrl="~/image/imate/btn/btn_reduction.jpg"
                                        ToolTip="이미지를 축소합니다." />
                                    <asp:ImageButton ID="ImgBtnTurnLeft" runat="server" ImageUrl="~/image/imate/btn/btn_turnleft.jpg"
                                        ToolTip="이미지를 왼쪽으로 회전합니다." />
                                    <asp:ImageButton ID="ImgBtnTurnRight" runat="server" ImageUrl="~/image/imate/btn/btn_turnright.jpg"
                                        ToolTip="이미지를 오른쪽으로 회전합니다." />
                                    <asp:ImageButton ID="imgBtnPopUp" runat="server" ImageUrl="~/image/imate/btn/btn_Popup.jpg"
                                        ToolTip="이미지를 팝업창으로 확인합니다." />
                                </td>
                            </tr>
                        </table>
                        </br>
                        <table id="tbMain" style="width: 100%; height: 1000px;">
                            <tr>
                                <td style="width: 100%; height: 100%;">
                                    <%--<input type="text" value="<%=sgMoveType %>" />--%>
                                    <%--<iframe src="Frame.aspx" width="100%" height="100%"></iframe>--%>
                                    <%=sgShowList %>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- // wrapper -->
        </div>
        <uc3:Footer ID="Footer1" runat="server" />
    </div>
    <asp:HiddenField ID="hidTableIdx" runat="server" Value="1222"></asp:HiddenField>
    </form>
</body>
</html>
