﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BenefitDefault.aspx.cs" Inherits="Mate_nanum_BenefitDefault" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript">
        function OpenLetter(val) {
            NewWindow("BenefitDetail.aspx?idx=" + val, "bedetail", "530", "450", "auto");
            return false;
        }

        function GoBenefitList() {
            location.href = "BenefitList.aspx?option=" + document.getElementById('SearchOption').value
            + "&dropNation=" + document.getElementById('txtSearch').value + "&orderby=";
            return false;
        }

        function GoNationBenefitList(orderby) {
            location.href = "BenefitList.aspx?option=nation&dropNation=" + document.getElementById('dropNation').value + "&orderby=" + orderby;
        }
    </script>    
</head>
<body style="overflow-X:hidden">
    <form id="form1" runat="server">     
	<div id="wrapper" class="share benefit-letter">

		<!-- sidebar -->
		
		<!-- //sidebar -->

		<!-- contents -->
		<div style="display:inline;float:left;position:relative;width:658px;margin:0 -1px 0 0;padding:0 50px 100px;overflow:hidden;">

			<div class="cont">
				<div class="cover">
					수혜국 어린이 편지 용어를 검색하세요 메이트 님께서 편지를 번역하시면서 잘 몰랐거나 궁금했던 용어들을 검색하세요.
				</div>	
				<fieldset class="search-benefit-letter">
					<legend class="hide">수혜국 편지 용어 검색</legend>
					<div class="search">
                        <select id="SearchOption" runat="server">
							<option value="all">전체</option>
                            <option value="eword">용어(영문)</option>
                            <option value="comment">내용</option>
						</select>
						<input type="text" class="text" id="txtSearch" runat="server" />
                        <asp:Button ID="btnSeach" runat="server" Text="" CssClass="btn btn-search2" OnClientClick ="return GoBenefitList();" />
					</div>
					<div class="sort">
						<strong class="txt txt-nation"><span>국가별 보기</span></strong>
                        <asp:DropDownList ID="dropNation" runat="server">
                        </asp:DropDownList>							
						<ul class="align">
							<li><a href="javascript:GoNationBenefitList('asc')" class="txt txt-ascending"><span>오름차순 정렬</span></a></li>
							<li><a href="javascript:GoNationBenefitList('readcount')" class="txt txt-hits"><span>조회순</span></a></li>
						</ul>
					</div>
				</fieldset>	
				<ul class="letters">
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=A&orderby="><strong class="letter letter-a">A</strong></a>
						<ul>
                            <asp:Repeater ID="RepeaterA" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem, "eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=B&orderby="><strong class="letter letter-b">B</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterB" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=C&orderby="><strong class="letter letter-c">C</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterC" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=D&orderby="><strong class="letter letter-d">D</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterD" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=E&orderby="><strong class="letter letter-e">E</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterE" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=F&orderby="><strong class="letter letter-f">F</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterF" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=G&orderby="><strong class="letter letter-g">G</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterG" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=H&orderby="><strong class="letter letter-h">H</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterH" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=I&orderby="><strong class="letter letter-i">I</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterI" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=J&orderby="><strong class="letter letter-j">J</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterJ" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=K&orderby="><strong class="letter letter-k">K</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterK" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=L&orderby="><strong class="letter letter-l">L</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterL" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=M&orderby="><strong class="letter letter-m">M</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterM" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=N&orderby="><strong class="letter letter-n">N</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterN" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=O&orderby="><strong class="letter letter-o">O</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterO" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=P&orderby="><strong class="letter letter-p">P</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterP" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=Q&orderby="><strong class="letter letter-q">Q</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterQ" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=R&orderby="><strong class="letter letter-r">R</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterR" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=S&orderby="><strong class="letter letter-s">S</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterS" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=T&orderby="><strong class="letter letter-t">T</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterT" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=U&orderby="><strong class="letter letter-u">U</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterU" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=V&orderby="><strong class="letter letter-v">V</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterV" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=W&orderby="><strong class="letter letter-w">w</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterW" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=X&orderby="><strong class="letter letter-x">X</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterX" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=Y&orderby="><strong class="letter letter-y">Y</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterY" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
					<li>
						<a href="BenefitList.aspx?option=initial&dropNation=Z&orderby="><strong class="letter letter-z">Z</strong></a>
						<ul>
							<asp:Repeater ID="RepeaterZ" runat="server">                            
                                <ItemTemplate>
							    <li><a href="javascript:void(0);" onclick="javascript:OpenLetter('<%#DataBinder.Eval(Container.DataItem,"idx")%>');"><%#GetPadding(DataBinder.Eval(Container.DataItem,"eword"),10)%></a></li>
                                </ItemTemplate> 
                            </asp:Repeater>
						</ul>
					</li>
				</ul>

			</div>
		</div>
		<!-- // contents -->

	</div>

    </form>
</body>
</html>
