﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TranslationDetailTop.aspx.cs" Inherits="Mate_translation_TranslationDetailTop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
    <!--

        function PageLoad() {

            document.getElementById("txtImgIndex").value ="1/" + '<%=sgPageCount%>'
            document.getElementById('txtImgCount').value = '<%=sgPageCount%>'
            document.getElementById('txtImgNum').value = '1'

        }
        
    function ExpCntCheck() {


        if (document.getElementById("txtCnt").value == "") {

            document.getElementById("txtCnt").value = 10
        }

        if (document.getElementById("txtCnt").value == 18) {

            alert("더 이상 확대 할 수 없습니다");

            return false;
        }

        
        document.getElementById("txtCnt").value = Number(document.getElementById("txtCnt").value) + 1
       

        parent.TranslationDetailImage.ExpCntCheck(document.getElementById("txtCnt").value);


    }

    function RedCntCheck() {

        if (document.getElementById("txtCnt").value == "") {

            document.getElementById("txtCnt").value = 8
        }

        if (document.getElementById("txtCnt").value == 0) {

            alert("더 이상 축소 할 수 없습니다");

            return false;
        }

        document.getElementById("txtCnt").value = Number(document.getElementById("txtCnt").value) - 1
        

        parent.TranslationDetailImage.RedCntCheck(document.getElementById("txtCnt").value);

    }

    //이전 이미지
    function MoveLeft() {




        if (document.getElementById("txtImgNum").value == 1) {

            alert("처음 이미지 입니다");

            return false;
        }
        else {
            document.getElementById("txtImgNum").value = Number(document.getElementById("txtImgNum").value) - 1
            document.getElementById("txtImgIndex").value = document.getElementById("txtImgNum").value + "/" + <%=sgPageCount%>
        }


        parent.TranslationDetailImage.ImageSet(document.getElementById("txtImgNum").value);


    }

    //다음 이미지
    function MoveRight() {


        if (document.getElementById("txtImgNum").value == '<%=sgPageCount%>') {

            alert("마지막 이미지 입니다");

            return false;
        }
        else {
            document.getElementById("txtImgNum").value = Number(document.getElementById("txtImgNum").value) + 1
            document.getElementById("txtImgIndex").value = document.getElementById("txtImgNum").value + "/" + '<%=sgPageCount%>'
        }

        
        top.frames['Showframe'].frames['TranslationDetailImage'].ImageSet(document.getElementById("txtImgNum").value);

        alert(document.getElementById("txtImgNum").value);
    }

    
    //-->

    </script>
</head>
<body onload="PageLoad();">
    <form id="form1" runat="server">
    <div>
    <input id="txtCnt" name="txtCnt" type="text"/>
    <input id="txtImgNum" name="txtImgNum" type="text"/>
    <input id="txtImgCount" name="txtImgNum" type="text"/>
    <table style="width:100%">
        <tr>
            <td style="text-align:right">
                <input id="txtImgIndex" name="txtImgIndex" type="text" style="text-align:right; border:0px;"/>
                <asp:ImageButton ID="imgBtnLeft" runat="server" />
                <asp:ImageButton ID="imgBtnRight" runat="server" />
                <asp:ImageButton ID="imgBtnExtension" runat="server" />
                <asp:ImageButton ID="imgBtnReduction" runat="server" />
                
            </td>
        </tr>
    </table>  
    </div>
    </form>
</body>
</html>
