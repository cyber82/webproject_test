﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Drawing;
using System.Net;
using System.IO;

public partial class Mate_translation_TranslationDetailImage : System.Web.UI.Page
{
    public string sgFilePathLen;
    public string sgImgFilePath;
    public string sgChildKey;
    public string sgFile;
    public string sgMatePageType;
    public string sgImgFullPath;

    public int ngCorrID;

    protected void Page_Load(object sender, EventArgs e)
    {
        UserInfo user = new UserInfo();

        string sTranslateText = "";
        sgMatePageType = new TranslateInfo().MatePageType;

        if (sgMatePageType == "40" || sgMatePageType == "41")//3PL 일때
        {
            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            string sThirdYear = Request.QueryString["ThirdYear"].ToString();
            string sProjectNumber = Request.QueryString["ProjectNumber"].ToString();

            Object[] objSqlThird = new object[2] { "SELECT ISNULL(LongTextEnglish,'') AS LongTextEnglish FROM ThirdPL_Master WHERE ThirdYear = '" + sThirdYear + "' AND ProjectNumber = '" + sProjectNumber + "' ", "SELECT * FROM CORR_TemporarySave WHERE TempType = '3PLEnglish' AND CorrID = '" + sThirdYear + sProjectNumber + "'" };
       
            DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSqlThird, "TEXT", null, null);

            if (ds.Tables[1].Rows[0]["TempText"] == DBNull.Value && ds.Tables[0].Rows[0]["LongTextEnglish"] != DBNull.Value)
                sTranslateText = ds.Tables[0].Rows[0]["LongTextEnglish"].ToString();

            else
                sTranslateText = ds.Tables[1].Rows[0]["TempText"].ToString();
            
            sTranslateText = sTranslateText.Replace("\n", "|n");
            sTranslateText = sTranslateText.Replace("\r\n", "|n");
            sTranslateText = sTranslateText.Replace("'", "\\'");

            ds.Dispose();
            _WWW6Service.Dispose();
        }

        else
        {
            string sCorrID = Request.QueryString["CorrID"].ToString();
            string sPackageID = Request.QueryString["PackageID"].ToString();
            string sPageCount = Request.QueryString["PageCount"].ToString();
            string sFilePath = Request.QueryString["FilePath"].ToString();

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
     
            //이미지가 없고 문자로 된 편지 데이터
            Object[] objSql = new object[1] { "SELECT ISNULL(LetterText,'') AS ScanText FROM CORR_Master CM JOIN CORR_SCAN CS ON CM.CorrID = CS.CorrID AND CM.ScanningTimes = CS.ScanningTimes WHERE CM.CorrID = '" + sCorrID + "'" };

            DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ScanText"].ToString() == "" || ds.Tables[0].Rows[0]["ScanText"] == null)
                {
                    ngCorrID = int.Parse(sCorrID);

                    sgChildKey = Request.QueryString["ChildKey"].ToString();
                    sgFilePathLen = sFilePath.Length.ToString();
                    sgFile = ".jpg";

                    if (ngCorrID >= 3000000)
                        sgImgFilePath = sFilePath + sPackageID + "/" + sCorrID;
//2015-03-31 유상묵 추가
                    else
                        sgImgFilePath = sFilePath + sPackageID + "/";
                }

                else
                {
                    sTranslateText = ds.Tables[0].Rows[0]["ScanText"].ToString();
                    sTranslateText = sTranslateText.Replace("\n", "|n");
                    sTranslateText = sTranslateText.Replace("\r\n", "|n");
                    sTranslateText = sTranslateText.Replace("\r", "ln");
                    sTranslateText = sTranslateText.Replace("'", "\\'");

                    StringBuilder sb = new StringBuilder();
                    sb.Append("<script type='text/javascript' language ='javascript' >\n ");
                    sb.Append(" SetTranslateText('" + sTranslateText + "','" + new TranslateInfo().MatePageType + "');");
                    sb.Append("</script>");

                    this.RegisterStartupScript("TranslationDetailImage", sb.ToString());
                }
            }

            else
                sTranslateText = "";

            ds.Dispose();
        }    
    }
}