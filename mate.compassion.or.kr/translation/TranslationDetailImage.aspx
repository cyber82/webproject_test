﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TranslationDetailImage.aspx.cs"
    Inherits="Mate_translation_TranslationDetailImage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        html, body, table, td{width:100%; height:98%;}
        .test{width:100%; height:98%; min-height:370px;}
    </style>
    <script type="text/javascript" src="/common/js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript">
        //페이지 로드시 셋팅
        function ImageSet(sImgNum) {
            document.getElementById('rowNum').value = sImgNum;

            if ('<%=sgMatePageType%>' == "40" || '<%=sgMatePageType%>' == "41") {
                document.getElementById('txtTranslation').style.display = "none";
                document.getElementById('imgTranslation').style.display = "none";
            }
            else {

                if ('<%=sgFilePathLen%>' > 0) {

                    document.getElementById('txtTranslation').style.display = "none";

                    //테스트 2013-08-26
                    //document.getElementById('imgTranslation').src = "/translation/test/main_bnr_9.jpg";

                    if ('<%=ngCorrID %>' >= 3000000) {
                        document.getElementById('imgTranslation').src = '<%=sgImgFilePath%>_' + sImgNum + '_<%=sgChildKey%>.JPG';
                    }

                    else {
                        document.getElementById('imgTranslation').src = '<%=sgImgFilePath%><%=sgChildKey%>-' + sImgNum + '.JPG';
                    }
                }

                else {
                    document.getElementById('imgTranslation').style.display = "none";
                }

                document.getElementById('txtThirdEnglish').style.display = "none";
            }
        }

        function ImageError() {
            var num = document.getElementById('rowNum').value;

            if ('<%=ngCorrID %>' >= 3000000) {
                document.getElementById('imgTranslation').src = '<%=sgImgFilePath%>_' + num + '_<%=sgChildKey%>.jpg';
            }

            else {
                document.getElementById('imgTranslation').src = '<%=sgImgFilePath%><%=sgChildKey%>-' + num + '.jpg';
            }
        }

        //이미지 확대
        function ExpCntCheck(sCnt) {

            var fCnt = 0.1; //0.5;
            var arr = [];
            //0.1 배율로 증가 (50% 까지 증가 및 감소)
            //for (iCnt = 0; iCnt < 11; iCnt++) {

            //    arr.push(fCnt);
            //    fCnt = fCnt + 0.1;

            //}
            //0.1 배율로 증가 (100% 까지 증가 및 감소)
            for (iCnt = 0; iCnt < 19; iCnt++) {

                arr.push(fCnt);
                fCnt = fCnt + 0.1;

            }


            //var imgTrans = document.getElementById("imgTranslation");

            if (document.getElementById('txtImgWidthSize').value + "" == "")//원본이미지 일경우(확대 및 축소 를 안한경우)
            {
                document.getElementById('txtImgWidthSize').value = document.getElementById("imgTranslation").width
                //주석처리 2013-08-23
                //document.getElementById('txtImgHeightSize').value = document.getElementById("imgTranslation").height
            }


            var OrgWSize = document.getElementById('txtImgWidthSize').value; //원본이미지 가로 사이즈
            var OrgHSize = document.getElementById('txtImgHeightSize').value; //원본이미지 세로 사이즈

            var nWSize = OrgWSize * arr[sCnt];
            var nHSize = OrgHSize * arr[sCnt];

            document.getElementById("imgTranslation").width = nWSize;
            //주석처리 2013-08-23
            //document.getElementById("imgTranslation").height = nHSize;

            setPosition();

            return true;

        }

        //이미지 축소
        function RedCntCheck(sCnt) {

            var fCnt = 0.1; //0.5;
            var arr = [];
            //0.1 배율로 증가 (50% 까지 증가 및 감소)
            //for (iCnt = 0; iCnt < 11; iCnt++) {

            //    arr.push(fCnt);
            //    fCnt = fCnt + 0.1;

            //}
            //0.1 배율로 증가 (100% 까지 증가 및 감소)
            for (iCnt = 0; iCnt < 19; iCnt++) {

                arr.push(fCnt);
                fCnt = fCnt + 0.1;

            }

            if (document.getElementById('txtImgWidthSize').value + "" == "") //원본이미지 일경우(확대 및 축소 를 안한경우)
            {
                document.getElementById('txtImgWidthSize').value = document.getElementById("imgTranslation").width
                //주석처리 2013-08-23
                //document.getElementById('txtImgHeightSize').value = document.getElementById("imgTranslation").height
            }

            var OrgWSize = document.getElementById('txtImgWidthSize').value; //원본이미지 가로 사이즈
            var OrgHSize = document.getElementById('txtImgHeightSize').value; //원본이미지 세로 사이즈

            var nWSize = OrgWSize * arr[sCnt];
            var nHSize = OrgHSize * arr[sCnt];

            document.getElementById("imgTranslation").width = nWSize;
            //주석처리 2013-08-23
            //document.getElementById("imgTranslation").height = nHSize;

            setPosition();

            return true;


        }

        //이미지 회전
        function ImageTurn(sRotation) {

            //sRotation => [0]:원복 [1]:90도 [2]:180도 [3]:-90도
            //imgTrans.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=' + sRotation + ')';

            //alert(sRotation);

            if (sRotation == "0") {
                sConvertRotation = "0"
            }
            else if (sRotation == "1") {
                sConvertRotation = "90"
            }
            else if (sRotation == "2") {
                sConvertRotation = "180"
            }
            else if (sRotation == "3") {
                sConvertRotation = "-90"
            }


            var Browser = navigator.userAgent.toLowerCase();

            //alert(Browser);

            //if (Browser.indexOf('msie 9.0') != -1) {

            //document.getElementById("imgTranslation").style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=' + sRotation + ')';
            //}
            //            else {


            document.getElementById("imgTranslation").style.cssText = "-webkit-transform: rotate(" + sConvertRotation + "deg);"
                        + "-moz-transform: rotate(" + sConvertRotation + "deg);"
                        + "-o-transform: rotate(" + sConvertRotation + "deg);"
                        + "-ms-transform: rotate(" + sConvertRotation + "deg);"
                        + "progid:DXImageTransform.Microsoft.BasicImage(rotation=" + sRotation + "')"
            //            }
            if (Browser.indexOf('msie 9.0') == -1) {
                document.getElementById("imgTranslation").style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=' + sRotation + ')';
            }

            setPosition(sRotation);
        }

        function getRotationDegrees(obj) {
            var matrix = obj.css("-webkit-transform") ||
            obj.css("-moz-transform") ||
            obj.css("-ms-transform") ||
            obj.css("-o-transform") ||
            obj.css("transform");
            if (matrix !== 'none') {
                var values = matrix.split('(')[1].split(')')[0].split(',');
                var a = values[0];
                var b = values[1];
                var angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
            } else { var angle = 0; }
            return (angle < 0) ? angle + 360 : angle;
        }

        // 이미지 위치 조정
        function setPosition(sRotation) {
            if (sRotation == null || (sRotation == '1' || sRotation == '3')) {
                var img = $('#imgTranslation');
                var degree = 0;
                var matrix = img.css("-webkit-transform") || img.css("-moz-transform") || img.css("-ms-transform") || img.css("-o-transform") || img.css("transform");
                if (matrix !== 'none') {
                    var values = matrix.split('(')[1].split(')')[0].split(',');
                    var a = values[0];
                    var b = values[1];
                    degree = Math.round(Math.atan2(b, a) * (180 / Math.PI));
                }
                degree = (degree < 0) ? degree + 360 : degree;
                if (degree == 90 || degree == 270) {
                    var w = img.width();
                    var h = img.height();
                    img.css('position', 'relative').css('top', (w - h) / 2).css('left', (h - w) / 2);
                }
            }
        }

        function initMoving(target, position, topLimit, btmLimit) {
            if (!target)
                return false;

            var obj = target;
            var initTop = position;
            var bottomLimit = 3000; // Math.max(document.documentElement.scrollHeight, document.body.scrollHeight) - btmLimit - obj.offsetHeight;
            var top = initTop;

            obj.style.position = 'absolute';

            if (typeof (window.pageYOffset) == 'number') {	//WebKit
                var getTop = function () {
                    return window.pageYOffset;
                }
            } else if (typeof (document.documentElement.scrollTop) == 'number') {
                var getTop = function () {
                    return Math.max(document.documentElement.scrollTop, document.body.scrollTop);
                }
            } else {
                var getTop = function () {
                    return 0;
                }
            }

            if (self.innerHeight) {	//WebKit
                var getHeight = function () {
                    return self.innerHeight;
                }
            } else if (document.documentElement.clientHeight) {
                var getHeight = function () {
                    return document.documentElement.clientHeight;
                }
            } else {
                var getHeight = function () {
                    return 500;
                }
            }

            function move() {
                if (initTop > 0) {
                    pos = getTop() + initTop;
                } else {
                    pos = getTop() + getHeight() + initTop;
                }

                if (pos > bottomLimit)
                    pos = bottomLimit;
                if (pos < topLimit)
                    pos = topLimit;

                interval = top - pos;
                top = top - interval / 3;
                obj.style.top = top + 'px';

                window.setTimeout(function () {
                    move();
                }, 25);
            }

            function addEvent(obj, type, fn) {
                if (obj.addEventListener) {
                    obj.addEventListener(type, fn, false);
                } else if (obj.attachEvent) {
                    obj['e' + type + fn] = fn;
                    obj[type + fn] = function () {
                        obj['e' + type + fn](window.event);
                    }
                    obj.attachEvent('on' + type, obj[type + fn]);
                }
            }

            addEvent(window, 'load', function () {
                move();
            });
        }

        function SetTranslateText(sTranslateText, sMatePageType) {
            if (sMatePageType == "40" || sMatePageType == "41") {
                document.getElementById('txtThirdEnglish').value = sTranslateText.split("|n").join("\n");
            }

            else {
                document.getElementById('txtTranslation').value = sTranslateText.split("|n").join("\n");
            }

            document.getElementById('txtTranslation').onmousedown = new Function('return false');
            document.getElementById('txtTranslation').onselectstart = new Function('return false');
            //            document.oncontextmenu = new Function('return false');
            //            document.ondragstart = new Function('return false');
        }
    </script>
</head>
<body onload="ImageSet('1');">
    <%--initMoving(움직일 대상, 스크롤위치, 상단 한계, 하단 한계)--%>
    <%--<script type="text/javascript">initMoving(document.getElementById("gotop"), 10, 1, 1);</script>--%>
    <form id="form1" runat="server">
    <div style="width:100%; height:100%;">
        <input type="hidden" id='txtImgWidthSize'>
        <input type="hidden" id='txtImgHeightSize'>
        <input type="hidden" id='rowNum'>

        <table>
            <tr title="편지 이미지 창과 번역란의 크기는 사용자의 편의에 따라 자유롭게 조절할 수 있습니다.&#10;경계선 위에 마우스를 올리고 화살표 방향에 따라 크기를 조절해 주세요.">
                <td>
                    <img id='imgTranslation' name="imgTranslation" src="" runat="server" onerror="ImageError();" >
                    <asp:TextBox ID="txtTranslation" runat="server" TextMode="MultiLine" ReadOnly="True"
                        Font-Names="굴림" CssClass="test"></asp:TextBox>
                    <asp:TextBox ID="txtThirdEnglish" runat="server" TextMode="MultiLine" ReadOnly="False"
                        Width="100%" Height="100%" Font-Names="굴림"></asp:TextBox>
                    <asp:Image ID="testimg" runat="server" Visible="false" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
