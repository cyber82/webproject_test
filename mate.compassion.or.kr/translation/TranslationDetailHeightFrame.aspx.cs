﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mate_translation_TranslationDetailHeightFrame : System.Web.UI.Page
{
    public string sgCorrID;
    public string sgPackageID;
    public string sgPageCount;
    public string sgFilePath;
    public string sgChildKey;
    public string sgMateMoveType;
    public string sgThirdYear;
    public string sgProjectNumber;
    public string sgSearchKind;
    public string sgSearchText;

	public bool isMobile = false;

    public string sgMatePageType;

    protected void Page_Load(object sender, EventArgs e)
    {
        // iMate Log
        UserInfo user = new UserInfo();

        WWWService.Service wwwService = new WWWService.Service();

        wwwService.writeErrorLog("CompassWeb4", "test", "test", @"");
        wwwService.writeErrorLog("CompassWeb4", "test", "test", @"/TranslationDetailHeightFrame.aspx  : 시작");
        wwwService.writeErrorLog("CompassWeb4", "test", "test", string.Format("DateTime : {0}, UserID : {1}, SessonID : {2} ", DateTime.Now.ToString(), user.UserId, Session.SessionID));

        sgCorrID = Request.QueryString["CorrID"].ToString();
        sgPackageID = Request.QueryString["PackageID"].ToString();
        sgPageCount = Request.QueryString["PageCount"].ToString();
        sgFilePath = Request.QueryString["FilePath"].ToString();
        sgChildKey = Request.QueryString["ChildKey"].ToString();
        sgThirdYear = Request.QueryString["ThirdYear"].ToString();
        sgProjectNumber = Request.QueryString["ProjectNumber"].ToString();
        sgSearchKind = Request.QueryString["SearchKind"];
        sgSearchText = Request.QueryString["SearchText"];

        //번역 상세로 들어온 페이지 분류[0]:번역 [1]:스크리닝 [2]: 편지검색
        sgMateMoveType = new TranslateInfo().MateMoveType;
        sgMatePageType = new TranslateInfo().MatePageType;
        sgMatePageType = sgMatePageType.EmptyIfNull();
        sgMatePageType = sgMatePageType.Length > 0 ? sgMatePageType.Substring(0, 1) : "";
        wwwService.Dispose();

		string userAgent = Request.UserAgent;

		String[] browser = { "iphone", "ipod", "ipad", "android", "blackberry", "windows ce", "nokia", "webos", "opera mini", "sonyericsson", "opera mobi", "iemobile", "windows phone" };
		for (int i = 0; i < browser.Length; i++) {
			if (userAgent.ToLower().Contains(browser[i]) == true) {
				isMobile = true;
				break;
			}
		}
	}
}