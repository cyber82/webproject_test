﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Text;
using Roamer.Config;

public partial class Mate_translation_TranslationDetailWrite : System.Web.UI.Page
{
    WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
    WWWService.Service _wwwService = new WWWService.Service();

    public string sgTabSelectValue;
    public string sgShowValue;
    public string sgTranStatus;
    public string sgScreeningStatus;
    public string sgMatePageType;

    public string sgThirdYear;
    public string sgProjectNumber;
    public string sgThirdEnglish;

    private string sgCorrID;
    private string sgThirdKorean;

    private string _MateYearValue = string.Empty;

    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        // iMate Log
        UserInfo user = new UserInfo();

        _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"#번역쓰기");
        _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"/TranslationDetailWrite.aspx  : 시작");
        _wwwService.writeErrorLog("CompassWeb4", "test", "test", string.Format("DateTime : {0}, UserID : {1}, SessonID : {2} ", DateTime.Now.ToString(), user.UserId, Session.SessionID));

        //평가 숨기기
        //rdoLike.Visible = false;
        //rdoSoso.Visible = false;
        //imgSmaile.Visible = false;
        //imgSick.Visible = false;
        //lblEmp.Visible = false;

        sgMatePageType = new TranslateInfo().MatePageType;

        //3PL 일때
        if (sgMatePageType == "40" || sgMatePageType == "41")
        {
            sgThirdYear = Request.QueryString["ThirdYear"].ToString();
            sgProjectNumber = Request.QueryString["ProjectNumber"].ToString();
            sgThirdKorean = Request["txtThirdKorean"];

            this.imgBtnThirdPLSave.Attributes.Add("onclick", "GetThirdEnglish();");
            this.imgBtnThirdPLFinish.Attributes.Add("onclick", "return ThirdFinishCheck();");

            if (!IsPostBack)
            {
                SetThirdPLPageData();
            }
        }
        else
        {
            sgCorrID = Request.QueryString["CorrID"];
            sgShowValue = Request.QueryString["ShowValue"];
            sgTabSelectValue = new TranslateInfo().TabSelectValue;

            sgTranStatus = Request["txtTranStatus"];
            sgScreeningStatus = Request["txtScreeningStatus"];

            string sUserId = new UserInfo().UserId;

            //권한 체크
            Object[] objSql = new object[1] { "MATE_RightCheck" };
            Object[] objParam = new object[1] { "MateID " };
            Object[] objValue = new object[1] { sUserId };

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            //탭 선택 "0"번역페이지, "1"스크리너페이지
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type='text/javascript' language ='javascript' >\n ");
            sb.Append("show_leemocon('');");
            sb.Append("</script>");

            this.RegisterStartupScript("TranslationDetailWrite", sb.ToString());

            //페이지 기본 셋팅
            if (!IsPostBack)
            {
                //권한체크 [0]:번역 및 스크리닝 [1]:번역 [2]:스크리닝 []:권한없음
                DataSet data = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

                _MateYearValue = data.Tables[0].Rows[0][0].ToString();

                SetMateYearView(data.Tables[0].Rows[0][0].ToString());

                SetTranslationPageData(sgCorrID);//번역 데이터
                SetScreeningPageData(sgCorrID);  //스크리닝 데이터  

                data.Dispose();
            }

            sb.Append("<script type='text/javascript' language ='javascript' >\n ");
            sb.Append("ScrShowCheck('" + sgTranStatus + "','" + sgScreeningStatus + "')");
            sb.Append("</script>");

            this.RegisterStartupScript("TranslationDetailWriteSetScrShow", sb.ToString());

            this.imgBtnTranslateReport.Attributes.Add("onClick", "return confirm('이 편지를 신고하시겠습니까?');");
            this.imgBtnScreeningReturn.Attributes.Add("onClick", "return confirm('편지를 반송하시겠습니까?');");
            this.imgBtnTranslateReport_Screen.Attributes.Add("onClick", "return confirm('이 편지를 신고하시겠습니까?');");
            this.imgBtnTranslateSend.Attributes.Add("onClick", "return TransFinishCheck();");
            this.imgBtnScreeningFinish.Attributes.Add("onClick", "return ScreeFinishCheck();");

            ImgBtnTranslationTemplate.Attributes.Add("onClick", "openBrWindow('/translation/TranslationTemplate.aspx','TranslationTemplate','780','800','0'); return false;");
            //ImgBtnTranslationTemplate.Attributes.Add("onClick", "openModalDialog('/mate/translation/TranslationTemplate.aspx','780','800'); return false;");
            ImgBtnCompare.Attributes.Add("onClick", "openBrWindow('/translation/TranslationCompare.aspx?CorrID=" + sgCorrID + "','TranslationCompare','780','800','0'); return false;");
        }

        //추가 2013-08-30  카피서비스 문구
        XCSPageData();

        _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"#번역쓰기");
        _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"/TranslationDetailWrite.aspx  : 종료");
        _wwwService.writeErrorLog("CompassWeb4", "test", "test", string.Format("DateTime : {0}, UserID : {1}, SessonID : {2} ", DateTime.Now.ToString(), user.UserId, Session.SessionID));
    }

    #region 카피서비스 문구  2013-08-30 추가
    private void XCSPageData()
    {
        string sgCorrID = string.Empty;
        DataSet ds = new DataSet();

        sgMatePageType = new TranslateInfo().MatePageType;

        if (Request.QueryString["CorrID"] != null && sgMatePageType != "40" && sgMatePageType != "41")
        {
            sgCorrID = Request.QueryString["CorrID"];

            Object[] objSql = new object[1] { "SELECT top 1 * FROM CORR_Master WHERE CorrID = '" + sgCorrID + "'" };
            ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["CorrType"].ToString().Substring(0, 3) == "XCS")
                    {
                        lblCorrTypeText.Text = "위 편지 번역시, 특정 어린이의 이름을 넣지 말아주세요! 여러 명의 어린이들에게 동일한 내용으로 전달되는 편지입니다.";
                        lblCorrTypeText.Attributes.Add("style", "color:red; font-size:9pt;");
                        return;
                    }
                }
            }
        }
    }
    #endregion

    #region 스크리닝 데이터
    /// <summary>
    /// 스크리닝 데이터
    /// </summary>
    /// <param name="sgCorrID"></param>
    private void SetScreeningPageData(string sgCorrID)
    {
        //편지 글 및 상태
        StringBuilder sbList = new StringBuilder();

        sbList.Append("SELECT b.ScreeningText, a.ScreeningStatus, c.ReturnCode, c.Remarks, d.CautionCode, a.TranslationEvaluate, ISNULL(a.HoldYN, 'N') AS HoldYN, a.ScreeningComment ");
        sbList.Append("FROM ");
        sbList.Append("CORR_Master a ");
        sbList.Append("LEFT OUTER JOIN ");
        sbList.Append("CORR_Screening b ");
        sbList.Append("ON a.CorrID = b.CorrID AND a.ScreeningTimes = b.ScreeningTimes ");
        sbList.Append("LEFT OUTER JOIN ");
        sbList.Append("MATE_TranslateReturn c ");
        sbList.Append("ON a.CorrID = c.CorrID ");
        sbList.Append("LEFT OUTER JOIN MATE_Caution d ");
        sbList.Append("ON a.CorrID = d.CorrID ");
        sbList.Append("WHERE a.CorrID = '" + sgCorrID + "'  ");

        //Response.Write(sbList.ToString());

        Object[] objSql = new object[4] { sbList.ToString(), "SELECT row_number() over(order by ORDER_BY) AS IDX, * FROM ETS_TC_CODE_MEMBER WHERE DIM_CODE = 'MATE_RETURN' ORDER BY ORDER_BY", "SELECT row_number() over(order by ORDER_BY) AS IDX, * FROM ETS_TC_CODE_MEMBER WHERE DIM_CODE = 'MATE_EVALUATE' ORDER BY ORDER_BY", "SELECT * FROM CORR_TemporarySave WHERE TempType = 'Screening' AND CorrID = '" + sgCorrID + "'" };

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

        if (ds.Tables[3].Rows[0]["TempText"] == DBNull.Value)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ScreeningStatus"] != DBNull.Value)
                {
                    if (ds.Tables[0].Rows[0]["ScreeningStatus"].ToString() == "START" && ds.Tables[0].Rows[0]["ScreeningText"].ToString().Trim() == "")
                    {
                        this.txtScreening.Text = this.txtTranslate.Text;
                    }
                    else
                    {
                        this.txtScreening.Text = ds.Tables[0].Rows[0]["ScreeningText"].ToString();
                    }
                }
            }
        }
        else
        {
            this.txtScreening.Text = ds.Tables[3].Rows[0]["TempText"].ToString();
        }

        if (ds.Tables[0].Rows[0]["HoldYN"].ToString() == "Y")
            chkHold.Checked = true;

        else
            chkHold.Checked = false;

        //반송사유 드롭다운리스트 셋팅
        ddlReturnType.DataSource = ds.Tables[1];
        //Real Value 실제 값
        ddlReturnType.DataValueField = ds.Tables[1].Columns["MEM_CODE"].ColumnName;
        //View  보여지는 값
        ddlReturnType.DataTextField = ds.Tables[1].Columns["MEM_NAME_LANG0"].ColumnName;
        ddlReturnType.DataBind();

        //로드시 드롭다운에 넣을 값을 셋팅
        string sReturnIdx = "";
        string sRemarks = "";
        string sEvaluateIdx = "0";

        if (ds.Tables[0].Rows.Count > 0)
        {
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["ReturnCode"].ToString()))
            {
                sReturnIdx = ds.Tables[1].Select("MEM_CODE='" + ds.Tables[0].Rows[0]["ReturnCode"].ToString() + "'")[0][0].ToString();
                sRemarks = ds.Tables[1].Select("MEM_CODE='" + ds.Tables[0].Rows[0]["ReturnCode"].ToString() + "'")[0][0].ToString();
            }

            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["TranslationEvaluate"].ToString()))
            {
                sEvaluateIdx = ds.Tables[2].Select("VALUE1='" + ds.Tables[0].Rows[0]["TranslationEvaluate"].ToString() + "'")[0][0].ToString();
            }

            //스크리닝화면 셋팅
            SetScreeningDisplayView(ds.Tables[0].Rows[0]["ScreeningStatus"].ToString(), sReturnIdx, sRemarks, sEvaluateIdx, ds.Tables[0].Rows[0]["ScreeningComment"].ToString());
        }

        ds.Dispose();
    }
    #endregion

    #region 3PL 데이터
    /// <summary>
    /// 3PL 데이터
    /// </summary>
    /// <param name="sgCorrID"></param>
    private void SetThirdPLPageData()
    {
        //[0]:편지 글 [1]:번역 임시저장 데이터
        Object[] objSqlThird = new object[2] { "SELECT ISNULL(LongTextKorean,'') AS LongTextKorean, TranslateStatus FROM ThirdPL_Master WHERE ThirdYear = '" + sgThirdYear + "' AND ProjectNumber = '" + sgProjectNumber + "' ", "SELECT * FROM CORR_TemporarySave WHERE TempType = '3PLKorean' AND CorrID = '" + sgThirdYear + sgProjectNumber + "'" };

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSqlThird, "TEXT", null, null);

        if (ds.Tables[1].Rows[0]["TempText"] == DBNull.Value && ds.Tables[0].Rows[0]["LongTextKorean"] != DBNull.Value)
        {
            this.txtThirdKorean.Text = ds.Tables[0].Rows[0]["LongTextKorean"].ToString();
        }
        else
        {
            this.txtThirdKorean.Text = ds.Tables[1].Rows[0]["TempText"].ToString();
        }

        if (ds.Tables[0].Rows[0]["TranslateStatus"].ToString() == "END")
        {
            SetThirdDisplayView();
        }

        ds.Dispose();
    }
    #endregion

    #region 번역 데이터
    /// <summary>
    /// 번역 데이터
    /// </summary>
    /// <param name="sgCorrID"></param>
    private void SetTranslationPageData(string sgCorrID)
    {
        //편지 글 및 상태
        StringBuilder sbList = new StringBuilder();

        sbList.Append("SELECT ISNULL(b.TranslationText,'') AS TranslationText,  ");
        sbList.Append("ISNULL(a.TranslationStatus,'') AS TranslationStatus,  ");
        sbList.Append("(SELECT TransReportCode FROM MATE_TranslateReport  ");
        sbList.Append("WHERE CorrID = a.CorrID AND TranslationTimes = a.TranslationTimes - 1) AS TransReportCode,  ");
        sbList.Append("a.ScreeningStatus, a.ScreeningComment, a.TranslationEvaluate ");
        sbList.Append("FROM ");
        sbList.Append("CORR_Master a ");
        sbList.Append("LEFT OUTER JOIN ");
        sbList.Append("CORR_Translate b  ");
        sbList.Append("ON a.CorrID = b.CorrID AND a.TranslationTimes = b.TranslationTimes ");
        sbList.Append("WHERE a.CorrID = '" + sgCorrID + "'  ");

        //Response.Write(sbList.ToString());

        //데이터 바인딩[0]:편지 글 및 상태 [1]:신고사유 리스트 [2]:번역 임시저장 데이터
        //Object[] objSql = new object[3] { sbList.ToString(), "SELECT row_number() over(order by ORDER_BY) AS IDX, * FROM ETS_TC_CODE_MEMBER WHERE DIM_CODE = 'MATE_TRANS_REPORT' ORDER BY ORDER_BY", "SELECT * FROM CORR_TemporarySave WHERE TempType = 'Translation' AND CorrID = '" + sgCorrID + "'" };

        //#12721 요청으로 SENSITIVE_WORD 제외 : ETS_TC_CODE_MEMBER 테이블에 삭제 flag 처리할 컬럼이 없음
        Object[] objSql = new object[3] { sbList.ToString(), "SELECT row_number() over(order by ORDER_BY) AS IDX, * FROM ETS_TC_CODE_MEMBER WHERE DIM_CODE = 'MATE_TRANS_REPORT' AND MEM_CODE != 'SENSITIVE_WORD' ORDER BY ORDER_BY", "SELECT * FROM CORR_TemporarySave WHERE TempType = 'Translation' AND CorrID = '" + sgCorrID + "'" };

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "Text", null, null);

        StringBuilder sb = new StringBuilder();

        if (ds.Tables[2].Rows[0]["TempText"] == DBNull.Value && ds.Tables[0].Rows[0]["TranslationText"] != DBNull.Value)
        {
            //Response.Write(ds.Tables[2].Rows[0]["TempText"].ToString() + "/");
            this.txtTranslate.Text = ds.Tables[0].Rows[0]["TranslationText"].ToString();
        }
        else
        {
            this.txtTranslate.Text = ds.Tables[2].Rows[0]["TempText"].ToString();
        }


        //신고사유 드롭다운리스트 셋팅
        ddlReportType.DataSource = ds.Tables[1];
        ddlReportType_Screen.DataSource = ds.Tables[1];

        //Real Value 실제 값
        ddlReportType.DataValueField = ds.Tables[1].Columns["MEM_CODE"].ColumnName;
        ddlReportType_Screen.DataValueField = ds.Tables[1].Columns["MEM_CODE"].ColumnName;

        //View  보여지는 값
        ddlReportType.DataTextField = ds.Tables[1].Columns["MEM_NAME_LANG0"].ColumnName;
        ddlReportType.DataBind();

        ddlReportType_Screen.DataTextField = ds.Tables[1].Columns["MEM_NAME_LANG0"].ColumnName;
        ddlReportType_Screen.DataBind();

        //로드시 드롭다운에 넣을 값을 셋팅
        string sReturnIdx = "";

        if (ds.Tables[0].Rows.Count > 0)
        {
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["TransReportCode"].ToString()))
            {
                sReturnIdx = ds.Tables[1].Select("MEM_CODE='" + ds.Tables[0].Rows[0]["TransReportCode"].ToString() + "'")[0]
                    [0].ToString();
            }

            //번역화면 셋팅
            sgTranStatus = ds.Tables[0].Rows[0]["TranslationStatus"].ToString();
            sgScreeningStatus = ds.Tables[0].Rows[0]["ScreeningStatus"].ToString();

            SetTranslationDisplayView(sgTranStatus, sgScreeningStatus, sReturnIdx, ds.Tables[0].Rows[0]["ScreeningComment"].ToString(), ds.Tables[0].Rows[0]["TranslationEvaluate"].ToString());
        }

        ds.Dispose();

    }
    #endregion

    #region 번역화면셋팅
    /// <summary>
    /// 번역화면셋팅
    /// </summary>
    /// <param name="sStatus"></param>
    /// <param name="sReturnIdx"></param>
    private void SetTranslationDisplayView(string sTranslationStatus, string sgScreeningStatus, string sReturnIdx, string sComment, string TranslationEvaluate)
    {
        //편지 검색으로 들어 왔을 경우 [권한이 없습니다] 메시지만 보여준다
        if (new TranslateInfo().MateMoveType != "2")
        {
            if (sTranslationStatus == "REPORT")
            {
                if (!string.IsNullOrEmpty(sReturnIdx))
                    this.ddlReportType.SelectedIndex = int.Parse(sReturnIdx) - 1;

                this.imgBtnTranslateReport.Visible = false;
                this.imgBtnTranslateSave.Visible = false;
                this.imgBtnTranslateSend.Visible = false;
                this.lblTranslationNote.Visible = true;
                //this.imgBtnPreView.Visible = false;
                this.lblTranslationNote.Text = "신고처리중 입니다";

                //this.rdoLike_TransCheck.Visible = false;
                //this.imgSmaile_TransCheck.Visible = false;
                //this.rdoSoso_TransCheck.Visible = false;
                //this.imgSick_TransCheck.Visible = false;

                //this.lblTransComment.Visible = false;
                //this.txtTransComment.Visible = false;

                Trans_End_Excellent.Attributes.Add("style", "display:none;");
                Trans_End_Good.Attributes.Add("style", "display:none;");
            }
            else if (sTranslationStatus == "END")
            {
                this.imgBtnTranslateReport.Visible = false;
                this.imgBtnTranslateSave.Visible = false;
                this.imgBtnTranslateSend.Visible = false;
                this.lblTranslationNote.Visible = true;
                this.txtTranslate.ReadOnly = true;
                //this.imgBtnPreView.Visible = false;
                this.lblReportType.Visible = false;
                this.ddlReportType.Visible = false;
                this.txtReportSub.Visible = false;

                txtTranslate.Attributes.Add("onkeydown", "return false");
                txtTranslate.Attributes.Add("oncontextmenu", "return false");

                if (sgScreeningStatus == "END")
                {
                    txtScreening.Attributes.Add("onkeydown", "return false");
                    txtScreening.Attributes.Add("oncontextmenu", "return false");

                    if (TranslationEvaluate == "1")
                    {
                        Trans_End_Etc.Attributes.Add("style", "display:none;");
                        Trans_End_Excellent.Attributes.Add("style", "display:none;");

                        txtGood_Trans.Text = sComment;

                        //rdoSoso_TransCheck.Checked = true;
                    }
                    else if (TranslationEvaluate == "2")
                    {
                        Trans_End_Etc.Attributes.Add("style", "display:none;");
                        Trans_End_Good.Attributes.Add("style", "display:none;");

                        if (sComment.Length > 0)
                            Label4.Text = "(" + sComment + ")만 보완된다면 완벽하겠네요!";

                        //rdoLike_TransCheck.Checked = true;
                    }
                    else
                    {
                        Trans_End_Excellent.Attributes.Add("style", "display:none;");
                        Trans_End_Good.Attributes.Add("style", "display:none;");

                        this.lblTranslationNote.Text = "번역이 완료된 편지입니다";
                    }
                }
                else
                {
                    Trans_End_Excellent.Attributes.Add("style", "display:none;");
                    Trans_End_Good.Attributes.Add("style", "display:none;");

                    this.lblTranslationNote.Text = "번역이 완료된 편지입니다";
                }
            }
            else
            {
                Trans_End_Excellent.Attributes.Add("style", "display:none;");
                Trans_End_Good.Attributes.Add("style", "display:none;");
                Trans_End_Etc.Attributes.Add("style", "display:none;");

                //this.lblTranslationNote.Visible = false;
                //this.ImgBtnTranslationTemplate.Visible = true;
                //this.lblTransComment.Visible = false;
                //this.txtTransComment.Visible = false;

                //this.rdoLike_TransCheck.Visible = false;
                //this.imgSmaile_TransCheck.Visible = false;
                //this.rdoSoso_TransCheck.Visible = false;
                //this.imgSick_TransCheck.Visible = false;
            }
        }
    }
    #endregion

    #region 스크리닝화면셋팅
    /// <summary>
    /// 스크리닝화면셋팅
    /// </summary>
    /// <param name="sStatus"></param>
    /// <param name="sReturnIdx"></param>
    private void SetScreeningDisplayView(string sStatus, string sReturnIdx, string sRemarks, string sEvaluateIdx, string sComment)
    {
        //편지 검색으로 들어 왔을 경우 [권한이 없습니다] 메시지만 보여준다
        if (new TranslateInfo().MateMoveType != "2")
        {
            if (sStatus == "RETURN")
            {
                if (!string.IsNullOrEmpty(sReturnIdx))
                {
                    this.ddlReturnType.SelectedIndex = int.Parse(sReturnIdx) - 1;
                    this.txtReturnSub.Text = sRemarks;
                }

                this.imgBtnScreeningReturn.Visible = false;
                this.imgBtnScreeningSave.Visible = false;
                this.imgBtnScreeningFinish.Visible = false;
                //this.Screen_End_Return.Visible = true;
                //this.lblScreeningNote.Visible = true;
                //this.lblScreeningNote.Text = "반송처리 된 편지 입니다";

                //this.rdoLike_ScreenCheck.Visible = false;
                //this.imgSmile_ScreenCheck.Visible = false;
                //this.rdoSoso_ScreenCheck.Visible = false;
                //this.imgSick_ScreenCheck.Visible = false;

                //this.txtComment.Visible = false;
                this.lblReportType_Screen.Visible = false;
                this.ddlReportType_Screen.Visible = false;
                this.imgBtnTranslateReport_Screen.Visible = false;

                Screen_End_Excellent.Attributes.Add("style", "display:none;");
                Screen_End_Good.Attributes.Add("style", "display:none;");
            }
            else if (sStatus == "END")
            {
                txtTranslate.Attributes.Add("onkeydown", "return false");
                txtTranslate.Attributes.Add("oncontextmenu", "return false");
                txtScreening.Attributes.Add("onkeydown", "return false");
                txtScreening.Attributes.Add("oncontextmenu", "return false");

                this.rdoLike.Visible = false;
                //this.rdoSoso.Visible = false;
                //this.imgSick.Visible = false;
                this.imgSmaile.Visible = false;

                if (sEvaluateIdx == "1")
                {
                    Screen_End_Return.Attributes.Add("style", "display:none;");
                    Screen_End_Good.Attributes.Add("style", "display:none;");

                    if (sComment.Length > 0)
                        txtExcellent.Text = "(" + sComment + ")만 보완된다면 완벽하겠네요!";


                    //this.rdoLike_ScreenCheck.Visible = true;
                    //this.imgSmile_ScreenCheck.Visible = true;
                    //this.rdoSoso_ScreenCheck.Visible = true;
                    //this.imgSick_ScreenCheck.Visible = true;

                    //this.rdoLike_ScreenCheck.Checked = true;
                }
                else if (sEvaluateIdx == "2")
                {
                    Screen_End_Return.Attributes.Add("style", "display:none;");
                    Screen_End_Excellent.Attributes.Add("style", "display:none;");

                    txtGood.Text = sComment;

                    //this.rdoLike_ScreenCheck.Visible = true;
                    //this.imgSmile_ScreenCheck.Visible = true;
                    //this.rdoSoso_ScreenCheck.Visible = true;
                    //this.imgSick_ScreenCheck.Visible = true;

                    //this.rdoSoso_ScreenCheck.Checked = true;
                }
                else
                {
                    Screen_End_Good.Attributes.Add("style", "display:none;");
                    Screen_End_Excellent.Attributes.Add("style", "display:none;");

                    this.lblScreeningNote.Text = "스크리닝이 완료된 편지입니다";
                }

                Screen_Start_1.Attributes.Add("style", "display:none");
                Screen_Start_2.Attributes.Add("style", "display:none");
                Screen_Start_3.Attributes.Add("style", "display:none");
                //Screen_Start_4.Attributes.Add("style", "display:none");
                //Screen_Start_5.Attributes.Add("style", "display:none");
                Screen_Start_6.Attributes.Add("style", "display:none");
                Screen_Start_7.Attributes.Add("style", "display:none");

                //Screen_End_Excellent.Attributes.Add("style", "display:block");
                //Screen_End_Good.Attributes.Add("style", "display:block");
                //Screen_End_Return.Attributes.Add("style", "display:block");

                this.chkHold.Visible = false;
                this.imgBtnScreeningReturn.Visible = false;
                this.imgBtnScreeningSave.Visible = false;
                this.imgBtnScreeningFinish.Visible = false;
                //this.lblScreeningNote.Visible = true;
                this.lblScreeningNote.Text = "스크리닝이 완료된 편지입니다";
                this.txtScreening.ReadOnly = true;
                this.ImgBtnCompare.Visible = true;
                this.lblReturnType.Visible = false;
                this.ddlReturnType.Visible = false;
                this.txtReturnSub.Visible = false;
                //this.lblEmp.Visible = false;
                //this.txtComment.Visible = false;
                this.lblReportType_Screen.Visible = false;
                this.ddlReportType_Screen.Visible = false;
                this.imgBtnTranslateReport_Screen.Visible = false;
                //this.txtComment.Visible = false;

                //txtScreeningComment.Text = sComment;

                string sjs2 = JavaScript.HeaderScript.ToString();
                sjs2 += "NoCopy_Screening();";
                sjs2 += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs2);
            }
            else
            {
                Screen_End_Return.Attributes.Add("style", "display:none;");
                Screen_End_Excellent.Attributes.Add("style", "display:none;");
                Screen_End_Good.Attributes.Add("style", "display:none;");

                //Screen_End_1.Attributes.Add("style", "display:none");
                //Screen_End_2.Attributes.Add("style", "display:none");

                ////this.lblTranslationNote.Visible = false;
                //this.lblScreeningNote.Visible = false;
                //this.txtScreeningComment.Visible = false;
                //this.lblScreeningComment.Visible = false;

                //this.rdoLike_ScreenCheck.Visible = false;
                //this.imgSmile_ScreenCheck.Visible = false;
                //this.rdoSoso_ScreenCheck.Visible = false;
                //this.imgSick_ScreenCheck.Visible = false;
            }
        }
    }
    #endregion

    #region 3PL 화면셋팅
    private void SetThirdDisplayView()
    {
        this.imgBtnThirdPLSave.Visible = false;
        this.imgBtnThirdPLFinish.Visible = false;
        this.lblThirdNote.Visible = true;
        this.lblThirdNote.Text = "3PL 번역 완료된 편지입니다";
    }
    #endregion

    #region 권한체크 [0]:번역 및 스크리닝 [1]:번역 [2]:스크리닝 []:권한없음
    /// <summary>
    /// 권한체크 [0]:번역 및 스크리닝 [1]:번역 [2]:스크리닝 []:권한없음
    /// </summary>
    /// <param name="sMateYearValue"></param>
    private void SetMateYearView(string sMateYearValue)
    {
        if (sMateYearValue == "1")
        {
            this.imgBtnScreeningReturn.Visible = false;
            this.imgBtnScreeningSave.Visible = false;
            this.imgBtnScreeningFinish.Visible = false;
            this.lblScreeningNote.Visible = true;
            this.lblScreeningNote.Text = "권한이 없습니다";
            this.txtScreening.ReadOnly = true;
        }
        else if (sMateYearValue == "2")
        {
            this.imgBtnTranslateReport.Visible = false;
            this.imgBtnTranslateSave.Visible = false;
            this.imgBtnTranslateSend.Visible = false;
            this.lblTranslationNote.Visible = true;
            this.lblTranslationNote.Text = "권한이 없습니다";
            this.txtTranslate.ReadOnly = true;
        }
        else if (sMateYearValue == "0")
        {
            Object[] objSql = new object[1] { "SELECT TranslationStatus FROM CORR_Master WHERE CorrID = '" + sgCorrID + "'" };

            if (_WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null).Tables[0].Rows[0][0].ToString() != "END")
            {
                this.imgBtnScreeningReturn.Visible = false;
                this.imgBtnScreeningSave.Visible = false;
                this.imgBtnScreeningFinish.Visible = false;
                this.lblScreeningNote.Visible = true;
                this.lblScreeningNote.Text = "번역이 완료되지 않았습니다";
                this.txtScreening.ReadOnly = true;
            }
        }

        //번역 상세로 들어온 페이지 분류[0]:번역 [1]:스크리닝 [2]: 편지검색
        if (new TranslateInfo().MateMoveType == "2")
        {
            this.imgBtnScreeningReturn.Visible = false;
            this.imgBtnScreeningSave.Visible = false;
            this.imgBtnScreeningFinish.Visible = false;
            //this.lblScreeningNote.Visible = true;
            //this.lblScreeningNote.Text = "권한이 없습니다";
            this.txtScreening.ReadOnly = true;

            this.imgBtnTranslateReport.Visible = false;
            this.imgBtnTranslateSave.Visible = false;
            this.imgBtnTranslateSend.Visible = false;
            this.lblTranslationNote.Visible = true;
            this.lblTranslationNote.Text = "권한이 없습니다";
            this.txtTranslate.ReadOnly = true;
        }
    }
    #endregion

    #region 번역신고버튼
    //번역신고버튼
    protected void imgBtnTranslateReport_Click(object sender, ImageClickEventArgs e)
    {
        int iResult;
        string sUserId = new UserInfo().UserId;
        //sUserId = "aeii";//테스트용 임시

        DateTime reqTime = DateTime.Now;
        if (CheckFreezingTime(reqTime) == false)
        {
            Response.Write("<script language='javascript'>alert('매주 월요일 00:00 ~ 00:10은 데이터 조정 시간입니다.\\r\\n잠시후 시도해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");
            return;
        }

        string Remark = String.Empty;

        Object[] objSql2 = new object[1] { " SELECT TranslationStatus FROM Corr_Master WHERE CorrID = '" + sgCorrID + "'" };

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "Text", null, null);

        if (ds.Tables[0].Rows[0]["TranslationStatus"].ToString() == "REPORT")
        {
            Response.Write("<script language='javascript'>alert('이미 신고하신 편지입니다..\\r\\n새로운 편지를 가져와 번역해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");

            return;
        }
        else if (ds.Tables[0].Rows[0]["TranslationStatus"].ToString() == "END")
        {
            Response.Write("<script language='javascript'>alert('이미 전송된 편지입니다..\\r\\n새로운 편지를 가져와 번역해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");

            return;
        }

        if (txtReportSub.Enabled)
        {
            if (txtReportSub.Text == "")
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("신고 사유를 기입해주세요.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }
            else
                Remark = txtReportSub.Text.ToString();
        }

        try
        {
            Object[] objSql = new object[1] { "CorrTranslate" };
            Object[] objParam = new object[6] { "DIVIS",//편지 코드
                                            "CorrID",//메이트 아이디
                                            "TranslationID",//신고 코드
                                            "TransReportCode",//신고 유형
                                            "Remarks",
                                            "TranslationText"
                                            };
            Object[] objValue = new object[6] { "REPORT",
                                            sgCorrID,
                                            sUserId,
                                            this.ddlReportType.SelectedValue,
                                            Remark,
                                            txtTranslate.Text
                                            };

            //편지신고
            iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //DB Error
            if (iResult < 1)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("신고 접수를 하지 못 했습니다.\\r\\n관리자에게 문의 하세요.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }

            //신청 등록 완료
            else
            {
                Response.Write("<script language='javascript'>alert('편지 신고 완료되었습니다.\\r\\n새로운 편지를 가져와 번역해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");
            }
        }

        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert
            _wwwService.writeErrorLog("CompassWeb5", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate편지 신고 접수 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }
    #endregion

    #region 번역저장버튼
    //번역저장버튼
    protected void imgBtnTranslateSave_Click(object sender, ImageClickEventArgs e)
    {
        int iResult = 0;
        string sUserId = new UserInfo().UserId;
        //sUserId = "aeii";//테스트용 임시
        string timeout = string.Empty;

        DateTime reqTime = DateTime.Now;
        if(CheckFreezingTime(reqTime) == false)
        {
            Response.Write("<script language='javascript'>alert('매주 월요일 00:00 ~ 00:10은 데이터 조정 시간입니다.\\r\\n잠시후 시도해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");
            return;
        }

        try
        {
            Object[] objSql2 = new object[1] { " SELECT TranslationStatus FROM Corr_Master WHERE CorrID = '" + sgCorrID + "'" };

            DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "Text", null, null);

            if (ds.Tables[0].Rows[0]["TranslationStatus"].ToString() == "REPORT")
                Response.Write("<script language='javascript'>alert('신고된 편지는 임시저장하실 수 없습니다.\\r\\n새로운 편지를 가져와 번역해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");

            else if (ds.Tables[0].Rows[0]["TranslationStatus"].ToString() == "END")
                Response.Write("<script language='javascript'>alert('이미 전송된 편지입니다..\\r\\n새로운 편지를 가져와 번역해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");

            else
            {
                //수정 2013-09-11  프로시져 수정
                object[] objSql = new object[1] { "MATE_TranslateLetterWriteSave" };

                object[] objParam = new object[3] { "CorrID"//편지 코드
                                                ,"MateID"//메이트 아이디
                                                ,"TranslationText"//번역 텍스트
                                              };
                object[] objValue = new object[3] { sgCorrID
                                                ,sUserId
                                                ,txtTranslate.Text
                                              };

                object[] objOutParam = new object[1] { "TimeOut" }; //타임아웃 저장여부 확인
                object[] objOutValue = new object[1] { timeout };


                //편지저장
                //iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);
                iResult = _WWW6Service.Tx_ExecuteQueryOut("SqlCompass5", objSql, "SP", objParam, objOutParam, objValue, out objOutValue);

                //테스트 2013-09-11
                //Response.Write("<script>alert('" + sgCorrID + "');</script>");
                //Response.Write("<script>alert('" + sUserId + "');</script>");
                //Response.Write("<script>alert('" + txtTranslate.Text + "');</script>");
                //Response.Write("<script>alert('" + objOutValue[0].ToString() + "');</script>");
                //return;

                //수정 2013-09-11
                if (objOutValue[0].ToString() == "Y")
                {
                    string sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("기한 초과로 저장이 되지 않았습니다.\\r\\n관리자에게 문의 하세요.");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                    return;
                }
                else
                {
                    //DB Error
                    if (iResult < 1)
                    {
                        string sjs = JavaScript.HeaderScript.ToString();
                        sjs += JavaScript.GetAlertScript("저장이 되지 않았습니다.\\r\\n관리자에게 문의 하세요.");
                        sjs += JavaScript.FooterScript.ToString();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                        return;
                    }
                    //저장 완료
                    else
                    {
                        string sjs = JavaScript.HeaderScript.ToString();
                        sjs += JavaScript.GetAlertScript("번역 내용을 임시 저장했습니다.");
                        sjs += JavaScript.FooterScript.ToString();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
                        return;
                    }
                }
            }
        }
        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert
            _wwwService.writeErrorLog("CompassWeb5", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate편지 저장 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }
    #endregion

    #region 번역전송버튼
    //번역전송버튼
    protected void imgBtnTranslateSend_Click(object sender, ImageClickEventArgs e)
    {
        int iResult = 0;
        string sUserId = new UserInfo().UserId;
        //sUserId = "aeii";//테스트용 임시
        string timeout = string.Empty;

        DateTime reqTime = DateTime.Now;
        if (CheckFreezingTime(reqTime) == false)
        {
            Response.Write("<script language='javascript'>alert('매주 월요일 00:00 ~ 00:10은 데이터 조정 시간입니다.\\r\\n잠시후 시도해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");
            return;
        }

        try {
            // CT.TranslationStart 추가 문희원 2017-09-27
            Object[] objSql2 = new object[1] { " SELECT CT.TranslationID, CT.TranslationStatus, CT.TranslationTimes, CT.TranslationStart" +
                                               " FROM   CORR_Master CM " +
                                               " JOIN   CORR_Translate CT " +
                                               " ON     CM.TranslationTimes = CT.TranslationTimes " +
                                               " AND    CM.CorrID = CT.CorrID " +
                                               " WHERE  CM.CorrID = '" + sgCorrID + "'" };

            DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "Text", null, null);


            //다음주 월요일 되었는지 체크 추가 문희원 2017-09-29
            DateTime dt = Convert.ToDateTime(ds.Tables[0].Rows[0]["TranslationStart"].ToString());

            // 2018-04-11 jun-heo : 차주 월요일 00:00 이후 번역완료인 경우(시간초과) 저장 안됨!
            if ( CheckDay(sUserId, sgCorrID, dt) == false )
            {
                Response.Write("<script language='javascript'>alert('번역 제출기간이 초과되었습니다.'); parent.parent.location.replace('TranslationMain.aspx');</script>");
            }
            else if (ds.Tables[0].Rows[0]["TranslationStatus"].ToString() == "REPORT")
            {
                Response.Write("<script language='javascript'>alert('신고된 편지는 전송하실 수 없습니다.\\r\\n새로운 편지를 가져와 번역해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");
            }
            else if (ds.Tables[0].Rows[0]["TranslationStatus"].ToString() == "END")
            {
                Response.Write("<script language='javascript'>alert('이미 전송된 편지입니다.\\r\\n새로운 편지를 가져와 번역해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");
            }
            else if (ds.Tables[0].Rows[0]["TranslationStatus"].ToString() == "EXCEED")
            {
                Response.Write("<script language='javascript'>alert('기한 초과로 전송이 되지 않았습니다.\\r\\n관리자에게 문의 하세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");
            }
            else if (ds.Tables[0].Rows[0]["TranslationStatus"].ToString() == "INIT")
            {
                Response.Write("<script language='javascript'>alert('초기화된 편지입니다.\\r\\n새로운 편지를 가져와 번역해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");
            }
            else if (ds.Tables[0].Rows[0]["TranslationStatus"].ToString() == "START" && ds.Tables[0].Rows[0]["TranslationID"].ToString().ToLower().Trim() != sUserId.ToLower().Trim())
            {
                Response.Write("<script language='javascript'>alert('이미 메이트님의 편지함에 없는 편지입니다.\\r\\n새로운 편지를 가져와 번역해주세요.'); parent.parent.location.replace('TranslationMain.aspx');</script>");
            }
            else
            {
                //수정 2013-09-11  프로시져 수정
                //수정 2013-09-12
                object[] objSql = new object[1] { "MATE_TranslateLetterSendSave" };

                object[] objParam = new object[4] { "CorrID"//편지 코드
                                                ,"MateID"//메이트 아이디
                                                ,"TranslationText"//번역 텍스트
                                                , "ReqDate" //전송시간 서버시간으로 넘김. 
                                              };
                object[] objValue = new object[4] { sgCorrID
                                                ,sUserId
                                                ,txtTranslate.Text
                                                ,reqTime
                                              };

                object[] objOutParam = new object[1] { "TimeOut" }; //타임아웃 저장여부 확인
                object[] objOutValue = new object[1] { timeout };


                //편지신고
                //iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);
                iResult = _WWW6Service.Tx_ExecuteQueryOut("SqlCompass5", objSql, "SP", objParam, objOutParam, objValue, out objOutValue);


                //수정 2013-09-11
                if (objOutValue[0].ToString() == "Y")
                {
                    string sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("기한 초과로 전송이 되지 않았습니다.\\r\\n관리자에게 문의 하세요.");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                    Response.Write("<script language='javascript'>parent.parent.location.replace('TranslationMain.aspx');</script>");
                }
                else
                {
                    //DB Error
                    if (iResult < 1)
                    {
                        string sjs = JavaScript.HeaderScript.ToString();
                        sjs += JavaScript.GetAlertScript("전송이 되지 않았습니다.\\r\\n관리자에게 문의 하세요.");
                        sjs += JavaScript.FooterScript.ToString();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                        return;
                    }

                    //전송 완료
                    else
                    {
                        //string sjs = JavaScript.HeaderScript.ToString();
                        //sjs += JavaScript.GetAlertScript("전송 했습니다.");
                        //sjs += JavaScript.FooterScript.ToString();
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
                        //SetTranslationDisplayView("END", "", "");

                        String sjs = JavaScript.HeaderScript.ToString();
                        sjs += "SetXSS();";
                        sjs += JavaScript.FooterScript.ToString();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "GoUrl", sjs);

                        Response.Write("<script language='javascript'>parent.parent.location.replace('TranslationMain.aspx');</script>");

                        //return;
                    }
                }
            }
        }

        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert
            _wwwService.writeErrorLog("CompassWeb5", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate편지 전송 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }
    #endregion

    #region 스크리닝 반송/경고
    //스크리닝 반송/경고
    protected void imgBtnScreeningReturn_Click(object sender, ImageClickEventArgs e)
    {
        int iResult;
        string sUserId = new UserInfo().UserId;
        string sUserName = new UserInfo().UserName;
        //sUserId = "aeii";//테스트용 임시

        DateTime reqTime = DateTime.Now;
        if (CheckFreezingTime(reqTime) == false)
        {
            Response.Write("<script language='javascript'>alert('매주 월요일 00:00 ~ 00:10은 데이터 조정 시간입니다.\\r\\n잠시후 시도해주세요.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");
            return;
        }

        try
        {
            //Object[] objSql2 = new object[1] { " SELECT * FROM Corr_Master WHERE CorrID = '" + sgCorrID + "' AND ScreeningStatus = 'REPORT'" };
            Object[] objSql2 = new object[1] { " SELECT * FROM Corr_Master WHERE CorrID = '" + sgCorrID + "'" };

            DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "Text", null, null);

            if (ds.Tables[0].Rows[0]["TranslationStatus"].ToString() == "REPORT")
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("이미 신고하신 편지입니다.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                return;
            }
            else if (ds.Tables[0].Rows[0]["ScreeningStatus"].ToString() == "END")
            {
                Response.Write("<script language='javascript'>alert('이미 스크리닝 완료된 편지입니다.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");

                return;
            }

            Object[] objSql = new object[1] { "CorrScreening" };

            Object[] objParam = new object[7] { "DIVIS",//편지 코드
                                                    "CorrID",//메이트 아이디
                                                    "ScreeningID",//스크리너ID
                                                    "ScreeningName",//스크리너명
                                                    "ScreeningText",//스크리닝 TEXT
                                                    "ReturnCode",//반송 코드
                                                    "Remarks"//반송 사유
                                            };
            Object[] objValue = new object[7] { "RETURN",
                                                sgCorrID,
                                                sUserId,
                                                sUserName,
                                                this.txtScreening.Text,
                                                this.ddlReturnType.SelectedValue,
                                                this.txtReturnSub.Text
                                            };


            //편지신고
            iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //DB Error
            if (iResult < 1)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("반송을 하지 못 했습니다.\\r\\n관리자에게 문의 하세요.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }
            //신청 등록 완료
            else
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("반송을 했습니다.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());

                Response.Write("<script language='javascript'>parent.parent.location.replace('ScreeningMain.aspx');</script>");
            }
        }
        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert
            _wwwService.writeErrorLog("CompassWeb5", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate편지 반송 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }
    #endregion

    #region 스크리닝 저장
    //스크리닝 저장
    protected void imgBtnScreeningSave_Click(object sender, ImageClickEventArgs e)
    {
        int iResult;
        string sUserId = new UserInfo().UserId;
        //sUserId = "aeii";//테스트용 임시
        string holdYN = string.Empty;

        DateTime reqTime = DateTime.Now;
        if (CheckFreezingTime(reqTime) == false)
        {
            Response.Write("<script language='javascript'>alert('매주 월요일 00:00 ~ 00:10은 데이터 조정 시간입니다.\\r\\n잠시후 시도해주세요.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");
            return;
        }

        try
        {
            //Object[] objSql2 = new object[1] { " SELECT * FROM Corr_Master WHERE CorrID = '" + sgCorrID + "' AND ScreeningStatus = 'REPORT'" };
            Object[] objSql2 = new object[1] { " SELECT * FROM Corr_Master WHERE CorrID = '" + sgCorrID + "'" };

            DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "Text", null, null);

            if (ds.Tables[0].Rows[0]["TranslationStatus"].ToString() == "REPORT")
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("이미 신고하신 편지입니다.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                return;
            }
            else if (ds.Tables[0].Rows[0]["ScreeningStatus"].ToString() == "END")
            {
                Response.Write("<script language='javascript'>alert('이미 스크리닝 완료된 편지입니다.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");

                return;
            }

            if (chkHold.Checked)
                holdYN = "Y";

            else
                holdYN = "N";

            Object[] objSql = new object[1] { "MATE_ScreeningLetterWriteSave" };
            Object[] objParam = new object[4] { "CorrID", //편지 코드
                                                "MateID", //메이트 아이디
                                                "ScreeningText", //번역 텍스트
                                                "HoldYN"
                                              };
            Object[] objValue = new object[4] { sgCorrID,
                                                sUserId,
                                                txtScreening.Text,
                                                holdYN
                                              };

            //편지저장
            iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //DB Error
            if (iResult < 1)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("저장을 하지 못 했습니다.\\r\\n관리자에게 문의 하세요.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }

            //저장 완료
            else
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("스크리닝 내용을 임시 저장했습니다.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
                return;
            }
        }

        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert
            _wwwService.writeErrorLog("CompassWeb5", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate편지 저장 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }
    #endregion

    #region 스크리닝 완료
    //스크리닝 완료
    protected void imgBtnScreeningFinish_Click(object sender, ImageClickEventArgs e)
    {
        _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"#1.스크리닝완료 : 시작");
        int iResult;

        string sUserId = new UserInfo().UserId;

        DateTime reqTime = DateTime.Now;
        if (CheckFreezingTime(reqTime) == false)
        {
            Response.Write("<script language='javascript'>alert('매주 월요일 00:00 ~ 00:10은 데이터 조정 시간입니다.\\r\\n잠시후 시도해주세요.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");
            return;
        }

        try
        {
            //평가 값
            string sRdoValue = "";
            string comment = "";

            if (this.rdoLike.Checked)
            {
                sRdoValue = "2";
            }
            else //if (this.rdoSoso.Checked)
            {
                sRdoValue = "1";
                //comment = txtComment.Text;
                /*
                if (txtLikeComment.Text.Length > 0)
                    comment = txtLikeComment.Text;
                    */
                if (!(chkGood_1.Checked || chkGood_2.Checked || chkGood_3.Checked || chkGood_4.Checked || chkGood_5.Checked))
                {
                    string sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("사유를 선택해주세요. (필수 항목)");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                    return;
                }
            } /*else {
				string sjs = JavaScript.HeaderScript.ToString();
				sjs += JavaScript.GetAlertScript("[Excellent/Good]을 선택해주세요. (필수 항목)");
				sjs += JavaScript.FooterScript.ToString();
				Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

				return;
			}
            */
              //comment = txtComment.Text;

            if (chkGood_1.Checked)
                comment += "번역유의사항 미반영";
            if (txtGood_1.Text.Length > 0)
                comment += "(" + txtGood_1.Text.ToString() + ")";

            if (chkGood_2.Checked)
            {
                if (comment.Length > 0)
                    comment += ", ";

                comment += "누락";
            }

            if (chkGood_3.Checked)
            {
                if (comment.Length > 0)
                    comment += ", ";

                comment += "오역";
            }

            if (chkGood_4.Checked)
            {
                if (comment.Length > 0)
                    comment += ", ";

                comment += "부자연스러운 번역";
            }

            if (chkGood_5.Checked)
            {
                if (comment.Length > 0)
                    comment += ", ";

                comment += "기타";

                if (txtGood_5.Text.Length > 0)
                    comment += "(" + txtGood_5.Text.ToString() + ")";
            }

            //Object[] objSql2 = new object[1] { " SELECT * FROM Corr_Master WHERE CorrID = '" + sgCorrID + "' AND ScreeningStatus = 'REPORT'" };
            Object[] objSql2 = new object[1] { " SELECT CS.ScreeningStatus, CS.ScreeningID " +
                                               " FROM   CORR_Master CM " +
                                               " JOIN   CORR_Screening CS " +
                                               " ON     CM.ScreeningTimes = CS.ScreeningTimes " +
                                               " AND    CM.CorrID = CS.CorrID " +
                                               " WHERE  CM.CorrID = '" + sgCorrID + "'" };

            _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"#2.스크리닝완료 : _WWW6Service 호출시작");

            DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "Text", null, null);

            _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"#3.스크리닝완료 : _WWW6Service 호출완료");

            if (ds.Tables[0].Rows[0]["ScreeningStatus"].ToString() == "REPORT")
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("이미 신고하신 편지입니다.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

                return;
            }
            else if (ds.Tables[0].Rows[0]["ScreeningStatus"].ToString() == "END")
            {
                Response.Write("<script language='javascript'>alert('이미 스크리닝 완료된 편지입니다.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");

                return;
            }
            else if (ds.Tables[0].Rows[0]["ScreeningStatus"].ToString() == "RETURN")
            {
                Response.Write("<script language='javascript'>alert('반송된 편지입니다.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");

                return;
            }
            else if (ds.Tables[0].Rows[0]["ScreeningStatus"].ToString() == "INIT")
            {
                Response.Write("<script language='javascript'>alert('초기화된 편지입니다.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");

                return;
            }
            else if (ds.Tables[0].Rows[0]["ScreeningStatus"].ToString() == "START" && ds.Tables[0].Rows[0]["ScreeningID"].ToString().Trim() != sUserId)
            {
                Response.Write("<script language='javascript'>alert('스크리너님의 편지함에 없는 편지입니다.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");

                return;
            }

            Object[] objSql = new object[1] { "MATE_ScreeningLetterFinishSave" };
            Object[] objParam = new object[5] { "CorrID", //편지 코드
                                                "MateID", //메이트 아이디
                                                "ScreeningText", //스크리닝
                                                "TranslationEvaluate", //평가코드
                                                "ScreeningComment" // 코멘트
                                              };
            Object[] objValue = new object[5] { sgCorrID,
                                                sUserId,
                                                txtScreening.Text,
                                                sRdoValue,
                                                comment
                                              };

            //스크리닝 완료
            _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"#4.스크리닝완료 : _WWW6Service 호출시작");

            iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"#5.스크리닝완료 : _WWW6Service 호출완료");

            //DB Error
            if (iResult < 1)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("스크리닝 완료를 하지 못 했습니다.\\r\\n관리자에게 문의 하세요.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }

            //신청 등록 완료
            else
            {
                //string sjs = JavaScript.HeaderScript.ToString();
                //sjs += JavaScript.GetAlertScript("스크리닝 완료를 했습니다.");
                //sjs += JavaScript.FooterScript.ToString();
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
                //SetScreeningDisplayView("END", "", "", sRdoValue);

                _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"#6.스크리닝완료 : 신청등록시작");

                String sjs = JavaScript.HeaderScript.ToString();
                sjs += "SetXSS();";
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "GoUrl", sjs);

                Response.Write("<script language='javascript'>parent.parent.location.replace('ScreeningMain.aspx');</script>");

                _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"#7.스크리닝완료 : 신청등록완료");
            }

            ds.Dispose();
        }

        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert
            _wwwService.writeErrorLog("CompassWeb5", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("스크리닝 완료 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            return;
        }
    }
    #endregion

    protected void imgBtnPreView_Click(object sender, ImageClickEventArgs e)
    {
        Object[] objSqlTrans = new object[1] { "MATE_CorrTemporarySave" };
        Object[] objParamTrans = new object[4] { "DIVIS",
                                                 "rowstate",
                                                 "CorrID",//편지 코드
                                                 "TempText"//임시저장 텍스트
                                               };
        Object[] objValueTrans = new object[4] { "Translation",
                                                 "MOD",
                                                 sgCorrID,
                                                 this.txtTranslate.Text
                                               };
        Object[] objValueScr = new object[4] { "Screening",
                                               "MOD",
                                               sgCorrID,
                                               this.txtScreening.Text
                                             };

        //텍스트 임시저장
        int iResultTrans = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSqlTrans, "SP", objParamTrans, objValueTrans);
        int iResultScr = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSqlTrans, "SP", objParamTrans, objValueScr);

        StringBuilder sb = new StringBuilder();
        sb.Append("<script type='text/javascript' language ='javascript' >\n ");
        sb.Append("openBrWindow('/translation/TranslationPreView.aspx?CorrID=" + sgCorrID + "','TranslationPreView','780','800','0');");
        sb.Append("</script>");

        this.RegisterStartupScript("TranslationDetailWritePreView", sb.ToString());
    }

    #region 3PL 임시저장
    /// <summary>
    /// 3PL 임시저장
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgBtnThirdPLSave_Click(object sender, ImageClickEventArgs e)
    {
        string sUserId = new UserInfo().UserId;
        string sUserName = new UserInfo().UserName;

        try
        {
            Object[] objSql = new object[1] { "MATE_ThirdPLWriteSave" };

            Object[] objParam = new object[8] { "DIVIS",
                                                "ThirdYear", // 편지 년도(Key)
                                                "ProjectNumber", // 프로젝트 키(Key)
                                                "LongTextEnglish", // 원본 영문
                                                "LongTextKorean", // 번역 한글
                                                "TranslateStatus", // 상태
                                                "ModifyID",
                                                "ModifyName"
                                              };

            Object[] objValue = new object[8] { "START",
                                                sgThirdYear,
                                                sgProjectNumber,
                                                txtThirdEnglish.Text,
                                                txtThirdKorean.Text,
                                                "START",
                                                sUserId,
                                                sUserName
                                              };


            //스크리닝 완료
            int iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //DB Error
            if (iResult < 1)
            {
                Response.Write("<script language='javascript'>alert('저장을 하지 못 했습니다.\\r\\n관리자에게 문의 하세요.');</script>");

            }
            //저장 완료
            else
            {
                Response.Write("<script language='javascript'>alert('저장 했습니다.');</script>");
            }

        }
        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert
            _wwwService.writeErrorLog("CompassWeb5", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("3PL 저장 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }
    #endregion

    #region 3PL 번역 완료
    /// <summary>
    /// 3PL 번역 완료
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgBtnThirdPLFinish_Click(object sender, ImageClickEventArgs e)
    {
        string sUserId = new UserInfo().UserId;
        string sUserName = new UserInfo().UserName;

        try
        {
            Object[] objSql = new object[1] { "MATE_ThirdPLWriteSave" };
            Object[] objParam = new object[8] { "DIVIS",
                                                "ThirdYear", // 편지 년도(Key)
                                                "ProjectNumber", // 프로젝트 키(Key)
                                                "LongTextEnglish", // 원본 영문
                                                "LongTextKorean", // 번역 한글
                                                "TranslateStatus", // 상태
                                                "ModifyID",
                                                "ModifyName"
                                              };
            Object[] objValue = new object[8] { "END",
                                                sgThirdYear,
                                                sgProjectNumber,
                                                txtThirdEnglish.Text,
                                                txtThirdKorean.Text,
                                                "END",
                                                sUserId,
                                                sUserName
                                              };


            //스크리닝 완료
            int iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            ////DB Error
            if (iResult < 1)
            {
                Response.Write("<script language='javascript'>alert('완료를 하지 못 했습니다.\\r\\n관리자에게 문의 하세요.');</script>");
            }

            //신청 등록 완료
            else
            {
                String sjs = JavaScript.HeaderScript.ToString();
                sjs += "SetXSS();";
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "GoUrl", sjs);

                Response.Write("<script language='javascript'>alert('3PL완료 했습니다.'); parent.parent.location.replace('ThirdPLMain.aspx');</script>");

            }
        }
        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert
            _wwwService.writeErrorLog("CompassWeb5", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("3PL 완료 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }
    #endregion

    #region Like / Soso 선택에 따른 Comment Enabled 변경
    protected void rdo_CheckedChanged(object sender, EventArgs e)
    {
        //if (rdoLike.Checked)
        //    txtComment.Enabled = false;

        //else
        //    txtComment.Enabled = true;
    }
    #endregion

    #region 스크리닝 신고 Event
    protected void imgBtnTranslateReport_Screen_Click(object sender, ImageClickEventArgs e)
    {
        int iResult;

        string sUserId = new UserInfo().UserId;
        string sUserName = new UserInfo().UserName;
        //sUserId = "aeii";//테스트용 임시

        DateTime reqTime = DateTime.Now;
        if (CheckFreezingTime(reqTime) == false)
        {
            Response.Write("<script language='javascript'>alert('매주 월요일 00:00 ~ 00:10은 데이터 조정 시간입니다.\\r\\n잠시후 시도해주세요.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");
            return;
        }

        string Remark = String.Empty;

        //Object[] objSql4 = new object[1] { " SELECT * FROM Corr_Master WHERE CorrID = '" + sgCorrID + "' AND ScreeningStatus = 'REPORT'" };
        Object[] objSql4 = new object[1] { " SELECT * FROM Corr_Master WHERE CorrID = '" + sgCorrID + "'" };

        DataSet ds2 = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql4, "Text", null, null);

        if (ds2.Tables[0].Rows[0]["TranslationStatus"].ToString() == "REPORT")
        {
            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("이미 신고하신 편지입니다.");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);

            return;
        }
        else if (ds2.Tables[0].Rows[0]["ScreeningStatus"].ToString() == "END")
        {
            Response.Write("<script language='javascript'>alert('이미 스크리닝 완료된 편지입니다.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");

            return;
        }

        if (txtReportSub_Screen.Enabled)
        {
            if (txtReportSub_Screen.Text == "")
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("기타 신고 사유를 기입해주세요.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }
            else
                Remark = txtReportSub_Screen.Text.ToString();
        }

        try
        {
            Object[] objSql = new object[1] { "CorrScreening" };
            Object[] objParam = new object[6] { "DIVIS",//편지 코드
                                            "CorrID",//메이트 아이디
                                            "ScreeningID",//신고 코드
                                            "TransReportCode",//신고 유형
                                            "Remark",
                                            "ScreeningText",
                                            };
            Object[] objValue = new object[6] { "REPORT",
                                            sgCorrID,
                                            sUserId,
                                            this.ddlReportType_Screen.SelectedValue,
                                            Remark,
                                            txtScreening.Text
                                            };

            //편지신고
            iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            //DB Error
            if (iResult < 1)
            {
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("신고 접수를 하지 못 했습니다.\\r\\n관리자에게 문의 하세요.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }

            //신청 등록 완료
            else
            {
                Object[] objSql2 = new object[1] { "SELECT MEM_NAME_LANG0 FROM ETS_TC_CODE_MEMBER WHERE DIM_CODE = 'MATE_TRANS_REPORT' AND MEM_CODE = '" + ddlReportType_Screen.SelectedValue + "' ORDER BY ORDER_BY" };

                DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "Text", null, null);

                String Comment = "메이트님께서 번역하신 [" + sgCorrID + "] 편지가 [" + ds.Tables[0].Rows[0]["MEM_NAME_LANG0"].ToString() + "](으)로 신고 처리 되었습니다.";

                Object[] objSql3 = new object[1] { "CorrScreening" };
                Object[] objParam3 = new object[5] { "DIVIS", "CorrID", "Remark", "UserID", "UserName" };
                Object[] objValue3 = new object[5] { "REMARK", sgCorrID, Comment, sUserId, sUserName };

                iResult = _WWW6Service.Tx_ExecuteQuery("SqlCompass5", objSql3, "SP", objParam3, objValue3);

                if (iResult < 1)
                {
                    string sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("신고 접수를 하지 못 했습니다.\\r\\n관리자에게 문의 하세요.");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                    return;
                }
                else
                    Response.Write("<script language='javascript'>alert('편지 신고 완료되었습니다.\\r\\n새로운 편지를 가져와 스크리닝해주세요.'); parent.parent.location.replace('ScreeningMain.aspx');</script>");

                ds.Dispose();
            }
        }

        //Exception Error
        catch (Exception ex)
        {
            //Exception Error Insert
            _wwwService.writeErrorLog("CompassWeb5", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

            string sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("Mate편지 신고 접수 중 오류가 발생했습니다. \\r\\nException Error Message : " +
                                              ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
    }
    #endregion

    protected void ddlReportType_Screen_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlReportType_Screen.SelectedValue == "ETC")
            txtReportSub_Screen.Enabled = true;

        else
            txtReportSub_Screen.Enabled = false;
    }

    protected void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlReportType.SelectedValue == "ETC")
        //	txtReportSub.Enabled = true;

        //else
        //	txtReportSub.Enabled = false;
    }
    /*
	protected void rdoSoso_CheckedChanged(object sender, EventArgs e) {
		if (rdoSoso.Checked) {
			txtLikeComment.Enabled = false;

			chkGood_1.Enabled = true;
			chkGood_2.Enabled = true;
			chkGood_3.Enabled = true;
			chkGood_4.Enabled = true;
			chkGood_5.Enabled = true;
		}
	}
    */
    protected void rdoLike_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoLike.Checked)
        {

            // txtLikeComment.Enabled = false;

            chkGood_1.Enabled = false;
            chkGood_2.Enabled = false;
            chkGood_3.Enabled = false;
            chkGood_4.Enabled = false;
            chkGood_5.Enabled = false;
        }
        else
        {
            // txtLikeComment.Enabled = true;

            chkGood_1.Enabled = true;
            chkGood_2.Enabled = true;
            chkGood_3.Enabled = true;
            chkGood_4.Enabled = true;
            chkGood_5.Enabled = true;
        }
    }

    protected void chkGood_5_CheckedChanged(object sender, EventArgs e)
    {
        if (chkGood_5.Checked)
            txtGood_5.Enabled = true;

        else
            txtGood_5.Enabled = false;
    }

    protected void chkGood_1_CheckedChanged(object sender, EventArgs e)
    {
        if (chkGood_1.Checked)
            txtGood_1.Enabled = true;

        else
            txtGood_1.Enabled = false;
    }

    bool CheckDay(string mateid, string corrid , DateTime sdt)
    {
        int nMonday = 0;
        switch(sdt.DayOfWeek)
        {
            case DayOfWeek.Monday: nMonday = 7;  break;
            case DayOfWeek.Tuesday: nMonday = 6;  break;
            case DayOfWeek.Wednesday: nMonday = 5;  break;
            case DayOfWeek.Thursday: nMonday = 4; break;
            case DayOfWeek.Friday: nMonday = 3;  break;
            case DayOfWeek.Saturday: nMonday = 2;  break;
            case DayOfWeek.Sunday: nMonday = 1;  break;
        }

        DateTime dtMonday = Convert.ToDateTime(sdt.ToString("yyyy-MM-dd").ToString() + " 00:00:00").AddDays(nMonday);
        int result = DateTime.Compare(dtMonday, DateTime.Now);

        _wwwService.writeErrorLog("CompassWeb4", "test", "test", @"MateID : "+ mateid +" CorrID : "+ corrid +" 번역시간 테스트 sdt : " + sdt.ToString("yyyy-MM-dd hh:mm:ss") + " dtMonday : " + dtMonday.ToString("yyyy-MM-dd hh:mm:ss") + "  now : " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + " checkResult : " + result.ToString());

        // 2018-04-11 jun-heo : 차주 월요일 00:00 이후 번역완료인 경우 저장 안됨!
        if (result < 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    //2018-05-17 이종진 - WO-171
    //월요일 00:00 스케쥴러가 처리안된 편지를 EXCEED 처리시킴 데이터의 충돌우려로 인해,
    //월요일 00:00 ~ 00:10 분까지는 '임시저장', '전송', '편지가져오기', '신고하기' 기능을 정지시킴
    //*시간은 서버시간으로 체크해야하므로 script로 처리하지않고, 로직에서 처리함
    bool CheckFreezingTime(DateTime now)
    {
        bool result = true;
        if (now.DayOfWeek == DayOfWeek.Monday)  //월요일
        {
            if (now.Hour == 0)  //0시
            {
                if (0 <= now.Minute && now.Minute <= 10)    //0~10분
                {
                    result = false;
                }
            }
        }

        return result;
    }

}
