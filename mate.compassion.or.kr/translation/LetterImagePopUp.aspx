﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LetterImagePopUp.aspx.cs" Inherits="Mate_translation_LetterImagePopUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/common/css/mate_Translation.css" />
    <script type="text/javascript">
    <!--
        //이미지 셋팅
        function ImageSet(sImgNum) {
            
            //테스트 2013-08-26
            //document.getElementById('imgTranslation').src = "/translation/test/main_bnr_9.jpg";

            if ('<%=ngCorrID %>' >= 3000000) {

                document.getElementById('imgTranslation').src = '<%=sgImgFilePath%>_' + sImgNum + '_<%=sgChildKey%>.JPG';
            }
            else {
                document.getElementById('imgTranslation').src = '<%=sgImgFilePath%><%=sgChildKey%>-' + sImgNum + '.JPG';
            }
            document.getElementById("txtImgNum").value = sImgNum;
            document.getElementById('txtImgIndex').value = sImgNum + "/" + '<%=sgPageCount%>';
            document.oncontextmenu = new Function('return false');
            document.ondragstart = new Function('return false');

            if (document.getElementById('txtTurnValue').value == '') {
                document.getElementById('txtTurnValue').value = '0';
            }
        }

        //이전 이미지
        function MoveLeft() {


            if (document.getElementById("txtImgNum").value == 1) {

                alert("처음 이미지 입니다");

                return false;
            }
            else {
                document.getElementById("txtImgNum").value = Number(document.getElementById("txtImgNum").value) - 1
                ImageSet(document.getElementById("txtImgNum").value);
            }

        }

        //다음 이미지
        function MoveRight() {


            if (document.getElementById("txtImgNum").value == '<%=sgPageCount%>') {

                alert("마지막 이미지 입니다");

                return false;
            }
            else {
                document.getElementById("txtImgNum").value = Number(document.getElementById("txtImgNum").value) + 1

                ImageSet(document.getElementById("txtImgNum").value);
            }


        }

        //이미지 왼쪽 회전
        function ImageLeftTurn() {

            if (document.getElementById('txtTurnValue').value == '0') {
                document.getElementById('txtTurnValue').value = '3'
            }
            else {
                document.getElementById('txtTurnValue').value = document.getElementById('txtTurnValue').value - 1
            }

            ImageTurn(document.getElementById('txtTurnValue').value);
        }
        //이미지 오른쪽 회전
        function ImageRightTurn() {

            if (document.getElementById('txtTurnValue').value == '3') {
                document.getElementById('txtTurnValue').value = '0'
            }
            else {
                document.getElementById('txtTurnValue').value = Number(document.getElementById('txtTurnValue').value) + 1
            }

            ImageTurn(document.getElementById('txtTurnValue').value);
        }

        //이미지 확대
        function ExpCntCheck() {


            if (document.getElementById("txtCnt").value == "") {

                document.getElementById("txtCnt").value = 10
            }

            if (document.getElementById("txtCnt").value == 18) {

                alert("더 이상 확대 할 수 없습니다");

                return false;
            }


            document.getElementById("txtCnt").value = Number(document.getElementById("txtCnt").value) + 1

            sCnt = document.getElementById("txtCnt").value;

            var fCnt = 0.1; //0.5;
            var arr = [];
            //0.1 배율로 증가 (50% 까지 증가 및 감소)
            //for (iCnt = 0; iCnt < 11; iCnt++) {

            //    arr.push(fCnt);
            //    fCnt = fCnt + 0.1;

            //}
            //0.1 배율로 증가 (100% 까지 증가 및 감소)
            for (iCnt = 0; iCnt < 19; iCnt++) {

                arr.push(fCnt);
                fCnt = fCnt + 0.1;

            }


            //var imgTrans = document.getElementById("imgTranslation");

            if (document.getElementById('txtImgWidthSize').value + "" == "")//원본이미지 일경우(확대 및 축소 를 안한경우)
            {
                document.getElementById('txtImgWidthSize').value = document.getElementById("imgTranslation").width
                //주석처리 2013-08-23
                //document.getElementById('txtImgHeightSize').value = document.getElementById("imgTranslation").height
            }


            var OrgWSize = document.getElementById('txtImgWidthSize').value; //원본이미지 가로 사이즈
            var OrgHSize = document.getElementById('txtImgHeightSize').value; //원본이미지 세로 사이즈

            var nWSize = OrgWSize * arr[sCnt];
            var nHSize = OrgHSize * arr[sCnt];

            document.getElementById("imgTranslation").width = nWSize;
            //주석처리 2013-08-23
            //document.getElementById("imgTranslation").height = nHSize;

            return true;

        }

        //이미지 축소
        function RedCntCheck() {


            if (document.getElementById("txtCnt").value == "") {

                document.getElementById("txtCnt").value = 8
            }

            if (document.getElementById("txtCnt").value == 0) {

                alert("더 이상 축소 할 수 없습니다");

                return false;
            }

            document.getElementById("txtCnt").value = Number(document.getElementById("txtCnt").value) - 1


            sCnt = document.getElementById("txtCnt").value;

            var fCnt = 0.1; //0.5;
            var arr = [];
            //0.1 배율로 증가 (50% 까지 증가 및 감소)
            //for (iCnt = 0; iCnt < 11; iCnt++) {

            //    arr.push(fCnt);
            //    fCnt = fCnt + 0.1;

            //}
            //0.1 배율로 증가 (100% 까지 증가 및 감소)
            for (iCnt = 0; iCnt < 19; iCnt++) {

                arr.push(fCnt);
                fCnt = fCnt + 0.1;

            }

            if (document.getElementById('txtImgWidthSize').value + "" == "") //원본이미지 일경우(확대 및 축소 를 안한경우)
            {
                document.getElementById('txtImgWidthSize').value = document.getElementById("imgTranslation").width
                //주석처리 2013-08-23
                //document.getElementById('txtImgHeightSize').value = document.getElementById("imgTranslation").height
            }

            var OrgWSize = document.getElementById('txtImgWidthSize').value; //원본이미지 가로 사이즈
            var OrgHSize = document.getElementById('txtImgHeightSize').value; //원본이미지 세로 사이즈

            var nWSize = OrgWSize * arr[sCnt];
            var nHSize = OrgHSize * arr[sCnt];

            document.getElementById("imgTranslation").width = nWSize;
            //주석처리 2013-08-23
            //document.getElementById("imgTranslation").height = nHSize;

            return true;

        }


        //이미지 회전
        function ImageTurn(sRotation) {

            //sRotation => [0]:원복 [1]:90도 [2]:180도 [3]:-90도
            //imgTrans.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=' + sRotation + ')';

            //alert(sRotation);

            if (sRotation == "0") {
                sConvertRotation = "0"
            }
            else if (sRotation == "1") {
                sConvertRotation = "90"
            }
            else if (sRotation == "2") {
                sConvertRotation = "180"
            }
            else if (sRotation == "3") {
                sConvertRotation = "-90"
            }


            var Browser = navigator.userAgent.toLowerCase();

            //alert(Browser);

            //if (Browser.indexOf('msie 9.0') != -1) {

            //document.getElementById("imgTranslation").style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=' + sRotation + ')';
            //}
            //            else {


            document.getElementById("imgTranslation").style.cssText = "-webkit-transform: rotate(" + sConvertRotation + "deg);"
                        + "-moz-transform: rotate(" + sConvertRotation + "deg);"
                        + "-o-transform: rotate(" + sConvertRotation + "deg);"
                        + "-ms-transform: rotate(" + sConvertRotation + "deg);"
                        + "progid:DXImageTransform.Microsoft.BasicImage(rotation=" + sRotation + "')"
            //            }
            if (Browser.indexOf('msie 9.0') == -1) {
                document.getElementById("imgTranslation").style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=' + sRotation + ')';
            }

        }
    //-->
    </script>
</head>


<body onload="ImageSet('1');" style="overflow-X:hidden; overflow-y:hidden">
    <form id="form1" runat="server">
    <div>
        <div style="width: 100%; height: 710px; overflow-x:scroll; overflow-y:scroll;" >
        <table>
            <tr>
                <td>
                    <img id='imgTranslation' src="">
                </td>
            </tr>
        </table>
        </div>
        <div style="width: 100%; height: 80px;">
        </br>
        <table>
            <tr>
                <td style="text-align:right;">
                    <input type="hidden" id='txtImgWidthSize'/>
                    <input type="hidden" id='txtImgHeightSize'/>
                    <input type="hidden" id="txtCnt" name="txtCnt"/>
                    <input type="hidden" id="txtTurnValue" name="txtTurnValue"/>
                    <input type="hidden" id="txtImgNum" name="txtImgNum"/>
                    <input id="txtImgIndex" name="txtImgIndex" type="text" style="width: 25px; font-size:15px;" readonly="readonly" />
                    <asp:ImageButton ID="imgBtnLeft" runat="server" ImageUrl="~/image/imate/btn/btn_left.jpg"
                                        ToolTip="이전 장으로 이동합니다." />
                    <asp:ImageButton ID="imgBtnRight" runat="server" ImageUrl="~/image/imate/btn/btn_right.jpg"
                                        ToolTip="다음 장으로 이동합니다." />
                    <asp:ImageButton ID="imgBtnExtension" runat="server" ImageUrl="~/image/imate/btn/btn_extension.jpg"
                                        ToolTip="이미지를 확대합니다." />
                    <asp:ImageButton ID="imgBtnReduction" runat="server" ImageUrl="~/image/imate/btn/btn_reduction.jpg"
                                        ToolTip="이미지를 축소합니다." />
                    <asp:ImageButton ID="ImgBtnTurnLeft" runat="server" ImageUrl="~/image/imate/btn/btn_turnleft.jpg"
                                        ToolTip="이미지를 왼쪽으로 회전합니다." />
                    <asp:ImageButton ID="ImgBtnTurnRight" runat="server" ImageUrl="~/image/imate/btn/btn_turnright.jpg"
                                        ToolTip="이미지를 오른쪽으로 회전합니다." />
                </td>
                <td>
                &nbsp;
                </td>
            </tr>
        </table>
        </div>
    </div>
    </form>
</body>
</html>
