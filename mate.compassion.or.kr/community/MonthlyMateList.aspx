﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MonthlyMateList.aspx.cs" Inherits="Mate_community_MonthlyMateList" %>
<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>

<%@ Register src="../Controls/MateCommunityRight.ascx" tagname="MateCommunityRight" tagprefix="uc5" %>
<%@ Register src="../Controls/BoardList.ascx" tagname="BoardList" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript">
        function SearchValidate() {
            if (document.getElementById("BoardList1_txtSearch").value.trim() == "") {
                alert("검색어를 입력 하세요.");
                return false;
            }
            return true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        
<div id="container" class="mate">

	<!-- wrapper -->
	<div id="wrapper" class="announce monthly-mate">

		<!-- sidebar -->
		<uc5:MateCommunityRight ID="MateCommunityRight1" runat="server" />
		<!-- //sidebar -->

		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>이달의 메이트</h3>
					<p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/community/noticelist.aspx">MATE 알림</a><strong>이달의 메이트</strong>
			</div>
			<div class="monthly-cover">
				<div class="medal"></div>
				<div class="thumbs1"><img src="" id="s_img" runat="server" width="228" height="155" alt="" /></div> <% //수정 2012-11-22 %>
				<p class="hide">이달의 MATE 를 소개합니다.</p>
                <div class="excellent-mate">
					<strong class="txt txt-mate1"><span>이달의 우수메이트</span></strong>
					<strong class="txt-name"><asp:Label ID="s_name" runat="server" Text=""></asp:Label></strong> <% //수정 2012-11-22 %>
					<strong class="txt txt-mate2" ID="mate2" runat="server"><span>님</span></strong> <% //수정 2013-07-01 %>
				</div>
				<p class="hide">컴패션 MATE가 추구하는 가치에 따라 어린이를 존귀하게 여기고 사랑하는 마음으로 자신의 달란트(재능)를 기꺼이 헌신하는 아름다운 MATE를 소개합니다.</p>
			</div>

            <uc4:BoardList ID="BoardList1" runat="server" />	
		</div>
		<!-- // contents -->

	</div>
	<!-- // wrapper -->

</div>
		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    <asp:hiddenfield ID="hidTableIdx" runat="server" Value ="1004"></asp:hiddenfield> 
    </form>
</body>
</html>
