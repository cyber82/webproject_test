﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="faq.aspx.cs" Inherits="Mate_community_faq" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>
<%@ Register src="../Controls/MateCommunityRight.ascx" tagname="MateCommunityRight" tagprefix="uc5" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        
<div id="container" class="mate">
 
	<!-- wrapper -->
	<div id="wrapper" class="announce mate-faq">
 
		<!-- sidebar -->
		<uc5:MateCommunityRight ID="MateCommunityRight1" runat="server" />
		<!-- //sidebar -->
 
		<!-- contents -->
		<div id="contents">
			<div class="headline headline-dep3">
				<div class="txt">
					<h3>FAQ</h3>
					<p>mate 활동을 하시는 동안 생기는 궁금한 내용을 해결하는 공간입니다.</p>
				</div>
			</div>
			<div class="breadcrumbs">
				<a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/community/noticelist.aspx">MATE 알림</a><strong>FAQ</strong>
			</div>
 
			<div class="txt txt-faq">
				<h4>가장 많이 묻는 질문</h4>
			</div>
 
			<ul class="tab4 tab-faq" id="tab-faq">
				<li class="on"><a href="faq.aspx?num=1" id="tab1" class="t1">전체</a></li>
				<li><a href="faq.aspx?num=2" id="tab2" class="t2">MATE 신청</a></li>
				<li><a href="faq.aspx?num=3" id="tab3" class="t3">봉사활동 확인서</a></li>
				<li><a href="faq.aspx?num=4" id="tab4" class="t4">번역프로그램</a></li>
				<li><a href="faq.aspx?num=5" id="tab5" class="t5">편지 번역</a></li>
			</ul>
 
			<dl id="cont1" class="faq-list">
				<dt><span class="txt1 txt-no1">1.</span> <a href="" class="txt tit-faq1">컴패션 메이트 신청은 어떻게 하나요?</a></dt>
				<dd><strong class="txt txt-faq1">컴패션 메이트 신청은 메이트 홈페이지 컴패션 iMate - iMate신청에서 온라인으로 신청하실 수 있습니다. 행사MATE와 전문MATE는 모집 시 컴패션 홈페이지(www.compassion.or.kr)을 통해 신청 가능하십니다.</strong></dd>
				<dt><span class="txt1 txt-no2">2.</span> <a href="" class="txt tit-faq2">번역메이트 신청 결과는 어떻게 확인하나요?</a></dt>
				<dd><strong class="txt txt-faq2">번역메이트 신청 결과는 신청 후 홈페이지 공지사항 및 마이페이지에서 확인하실 수 있습니다.</strong></dd>
				<dt><span class="txt1 txt-no3">3.</span> <a href="" class="txt tit-faq3">봉사활동확인서는 어떻게 발급받나요?</a></dt>
				<dd><strong class="txt txt-faq3">번역메이트이시라면 메인페이지 '봉사활동확인서출력' 버튼을 클릭하여 또는 마이페이지 '봉사내역확인 & 출력'에서 자신의 봉사활동내역을 확인 후, 출력하실 수 있습니다. 사무메이트이시라면, 봉사활동이 종료되는 날 컴패션에서 직접 발급해 드립니다.</strong></dd>
				<%--<dt><span class="txt1 txt-no4">4.</span> <a href="" class="txt tit-faq4">번역프로그램은 어떻게 설치하나요?</a></dt>
				<dd><strong class="txt txt-faq4">번역프로그램 (iMate)는 메인페이지 '번역프로그램' 버튼을 클릭하여 다운로드 받으실 수 있습니다. 번역 메이트로 활동중이시거나 TEST에 최종합격하신 분들만 다운로드 하실 수 있습니다.</strong></dd>--%>
				<dt><span class="txt1 txt-no4">4.</span> <a href="" class="txt tit-faq5">번역 유의사항은 어디서 다운로드받나요?</a></dt>
				<dd><strong class="txt txt-faq5">번역유의사항은 메인페이지 '번역프로그램' 버튼을 클릭하여 다운로드 받으실 수 있습니다.</strong></dd>
				<dt><span class="txt1 txt-no5">5.</span> <a href="" class="txt tit-faq6">iMate매뉴얼은 어디서 다운로드받나요?</a></dt>
				<dd><strong class="txt txt-faq6">iMate매뉴얼은 메인페이지 '번역프로그램' 버튼을 클릭하여 다운로드 받으실 수 있습니다.</strong></dd>
                <%--	<dt><span class="txt1 txt-no7">7.</span> <a href="" class="txt tit-faq7">번역프로그램을 설치하려고 해도 계속 오류가 발생해요. 어떻게 해야 하나요?</a></dt>
				<dd>
					<div class="txt txt-faq7">
						<p><strong>아래 설명을 읽어 보신 후 재설치해 주세요.</strong></p>
						<ol>
							<li><strong>제어판에서 기존에 설치된 iMate를 찾아 삭제합니다.</strong></li>
							<li><strong>각종 바이러스 프로그램 (V3, 노턴, 알약 등) 중단 후 iMate를 다운로드 받아 재설치합니다.</strong></li>
							<li><strong>중단해도 설치되지 않으면 바이러스 프로그램을 삭제 후 재설치합니다.</strong></li>
						</ol>
						<p><strong>위의 방법을 사용해도 설치되지 않으면 02)36683404 또는 hyunkim@compassion.or.kr로 연락 주시기 바랍니다.</strong></p>
					</div>
				</dd>--%>
				<dt><span class="txt1 txt-no6">6.</span> <a href="" class="txt tit-faq8">편지 번역을 하면 봉사활동시간은 얼마만큼 주어지나요? </a></dt>
				<dd><strong class="txt txt-faq8">편지 한 통을 번역할 때마다 30분의 봉사활동시간이 주어집니다.</strong></dd>
				<dt><span class="txt1 txt-no7">7.</span> <a href="" class="txt tit-faq9">스크리닝이 완료되지 않으면 봉사활동 시간으로 인정받지 못하는 건가요?</a></dt>
				<dd><strong class="txt txt-faq9">스크리닝 완료와 관계 없이 번역을 완료하고 전송한 편지는, 전송하는 순간 바로 봉사 활동 시간으로 인정됩니다.하지만 스크리닝 과정 중 반송되어 자신의 편지함으로 되돌아 온 편지는 봉사활동 내역에서 빠지게 됩니다. 반송된 편지를 수정 또는 추가 번역하여 다시 재전송하셔야만 다시 봉사활동시간으로 인정됩니다.</strong></dd>
				<dt><span class="txt1 txt-no8">8.</span> <a href="" class="txt tit-faq10">편지가 스크리닝 되는데 어느 정도의 시간이 걸리나요? </a></dt>
				<dd><strong class="txt txt-faq10">보통 편지 발송이 시급한 편지들 먼저 스크리닝 되며, 평균 2-3주가 소요됩니다. 방학 기간과 같이 메이트님들의 번역활동이 활발해져 번역 편지 양이 많아질 경우, 스크리닝이 완료되는데 더 오랜 시간이 소요되기도 합니다.</strong></dd>
				<dt><span class="txt1 txt-no9">9.</span> <a href="" class="txt tit-faq11">번역메이트로 신청했는데 불합격됐어요. 다시 신청할 수 있나요? </a></dt>
				<dd><strong class="txt txt-faq11">안타깝게도 재신청은 차기 기수 모집 시 가능합니다. 꾸준히 관심을 가져주시고 다음 기회에 재도전해주시면 감사드리겠습니다.</strong></dd>
				<dt><span class="txt1 txt-no10">10.</span> <a href="" class="txt tit-faq12">편지를 더 가져오려고 하는데 '일주일 번역 장수를 초과했다.'라는 메시지가 뜹니다.</a></dt>
				<dd><strong class="txt txt-faq12">메이트님에 따라 일주일에 가져올 수 있는 편지 양이 정해져 있습니다. (적게는 1통 많게는 수십통까지) 번역을 꾸준히 오래 하실수록 또 잘 하실수록 편지 장수는 늘어납니다. 일주일 번역 장수를 늘리고 싶으시다면 'MATE 나눔' - '온라인 게시판'에 올려진 장수요청에 대한 공지사항을 읽어 보신 후 요청 글을 남겨주시기 바랍니다.</strong></dd>
				<dt><span class="txt1 txt-no11">11.</span> <a href="" class="txt tit-faq13">편지를 가져오려고 하는데 번역대상편지 수가 0장이에요. 어떻게 해야 하나요? </a></dt>
				<dd><strong class="txt txt-faq13">주로 여름방학(7~8월)과 겨울방학(12월~1월) 기간동안에는 학생 메이트님들의 활동이 왕성해지면서 편지가빠른 속도로 사라집니다. "내가 번역을 잘 못해서 번역을 못 하게 된건가?"라는 걱정은 하지 않으셔도 됩니다. 편지가 사무실에 도착하는대로 스캔을 하고 있으니 인내심을 가지고 기다려주시면 감사드리겠습니다.</strong></dd>
				<dt><span class="txt1 txt-no12">12.</span> <a href="" class="txt tit-faq14">제가 번역하고 있던 편지가 갑자기 사라졌어요. 어떻게 해야 하나요?</a></dt>
				<dd><strong class="txt txt-faq14">오랫동안 번역이 되지 않고 있는 편지 또는 후원자님께서 급히 받아보셔야 하는 편지는 불가피하게 컴패션 담당직원이 가져가야할 때가 있습니다. 당황하지 마시고 양해해 주시면 감사 드리겠습니다.</strong></dd>
				<dt><span class="txt1 txt-no13">13.</span> <a href="" class="txt tit-faq15">'편지 가져오기'를 클릭해서 가져온 편지는 언제까지 번역해야 하나요?</a></dt>
				<dd><strong class="txt txt-faq15">가져가신 편지는 '편지가져오기'를 클릭한 시점에서 5일 이내에 번역해 주셔야 합니다. 5일이 지난 편지는 '번역저장'(편지함)에서 자동으로 사라집니다.</strong></dd>
				<dt><span class="txt1 txt-no14">14.</span> <a href="" class="txt tit-faq16">번역을 완료하고 전송한 편지가 다시 제 번역저장(편지함)으로 돌아왔어요. 어떻게 해야 하나요?</a></dt>
				<dd><strong class="txt txt-faq16">스크리닝(감수) 과정 중에 뒷 장이 번역되지 않았거나, 편지 원문과 번역 내용이 다르거나, 부분적으로 번역을 하지 않은 편지들이 발견되면 번역해주신 메이트님께 해당 편지가 반송됩니다. 편지가 반송될 때에는 문자로 안내해 드립니다. 반송된 편지는 5일 이내에 수정, 추가 번역하여 전송해 주셔야 합니다. 3회 이상 편지가 반송될 경우 활동 중단이 되니 유의해 주시기 바랍니다.</strong></dd>
				<dt><span class="txt1 txt-no15">15.</span> <a href="" class="txt tit-faq17">편지 글씨를 알아보기 어려운데 어떻게 해야 하나요? </a></dt>
				<dd><strong class="txt txt-faq17">홈페이지의 MATE 나눔 - 번역Q&amp;A에 해당 편지 ID, 이미지와 함께 질문을 남겨주시면 오랫동안 편지 번역을해주신 다른 메이트님들과 컴패션 직원이 답변을 드립니다. 답변을 확인하신 후 편지 번역을 완료하여 전송해 주시면 됩니다.</strong></dd>
			</dl>
 
			<dl id="cont2" class="faq-list">
				<dt><span class="txt1 txt-no1">1.</span> <a href="" class="txt tit-faq1">컴패션 메이트 신청은 어떻게 하나요?</a></dt>
				<dd><strong class="txt txt-faq1">컴패션 메이트 신청은 메이트 홈페이지 컴패션 iMate - iMate신청에서 온라인으로 신청하실 수 있습니다. 행사MATE와 전문MATE는 모집 시 컴패션 홈페이지(www.compassion.or.kr)을 통해 신청 가능하십니다.</strong></dd>
				<dt><span class="txt1 txt-no2">2.</span> <a href="" class="txt tit-faq2">번역메이트 신청 결과는 어떻게 확인하나요?</a></dt>
				<dd><strong class="txt txt-faq2">번역메이트 신청 결과는 신청 후 홈페이지 공지사항 및 마이페이지에서 확인하실 수 있습니다.</strong></dd>
				<dt><span class="txt1 txt-no3">3.</span> <a href="" class="txt tit-faq11">번역메이트로 신청했는데 불합격됐어요. 다시 신청할 수 있나요? </a></dt>
				<dd><strong class="txt txt-faq11">안타깝게도 재신청은 차기 기수 모집 시 가능합니다. 꾸준히 관심을 가져주시고 다음 기회에 재도전해주시면 감사드리겠습니다.</strong></dd>
			</dl>
 
			<dl id="cont3" class="faq-list">
				<dt><span class="txt1 txt-no1">1.</span> <a href="" class="txt tit-faq3">봉사활동확인서는 어떻게 발급받나요?</a></dt>
				<dd><strong class="txt txt-faq3">번역메이트이시라면 메인페이지 '봉사활동확인서출력' 버튼을 클릭하여 또는 마이페이지 '봉사내역확인 & 출력'에서 자신의 봉사활동내역을 확인 후, 출력하실 수 있습니다. 사무메이트이시라면, 봉사활동이 종료되는 날 컴패션에서 직접 발급해 드립니다.</strong></dd>
			</dl>
 
			<dl id="cont4" class="faq-list">
				<%--<dt><span class="txt1 txt-no1">1.</span> <a href="" class="txt tit-faq4">번역프로그램은 어떻게 설치하나요?</a></dt>
				<dd><strong class="txt txt-faq4">번역프로그램 (iMate)는 메인페이지 '번역프로그램' 버튼을 클릭하여 다운로드 받으실 수 있습니다. 번역 메이트로 활동중이시거나 TEST에 최종합격하신 분들만 다운로드 하실 수 있습니다.</strong></dd>--%>
				<dt><span class="txt1 txt-no1">1.</span> <a href="" class="txt tit-faq5">번역 유의사항은 어디서 다운로드받나요?</a></dt>
				<dd><strong class="txt txt-faq5">번역유의사항은 메인페이지 '번역프로그램' 버튼을 클릭하여 다운로드 받으실 수 있습니다.</strong></dd>
				<dt><span class="txt1 txt-no2">2.</span> <a href="" class="txt tit-faq6">iMate매뉴얼은 어디서 다운로드받나요?</a></dt>
				<dd><strong class="txt txt-faq6">iMate매뉴얼은 메인페이지 '번역프로그램' 버튼을 클릭하여 다운로드 받으실 수 있습니다.</strong></dd>
				<%--<dt><span class="txt1 txt-no4">4.</span> <a href="" class="txt tit-faq7">번역프로그램을 설치하려고 해도 계속 오류가 발생해요. 어떻게 해야 하나요?</a></dt>
				<dd>
					<div class="txt txt-faq7">
						<p><strong>아래 설명을 읽어 보신 후 재설치해 주세요.</strong></p>
						<ol>
							<li><strong>제어판에서 기존에 설치된 iMate를 찾아 삭제합니다.</strong></li>
							<li><strong>각종 바이러스 프로그램 (V3, 노턴, 알약 등) 중단 후 iMate를 다운로드 받아 재설치합니다.</strong></li>
							<li><strong>중단해도 설치되지 않으면 바이러스 프로그램을 삭제 후 재설치합니다.</strong></li>
						</ol>
						<p><strong>위의 방법을 사용해도 설치되지 않으면 02)36683404 또는 hyunkim@compassion.or.kr로 연락 주시기 바랍니다.</strong></p>
					</div>
				</dd>--%>
			</dl>
 
			<dl id="cont5" class="faq-list">
				<dt><span class="txt1 txt-no1">1.</span> <a href="" class="txt tit-faq8">편지 번역을 하면 봉사활동시간은 얼마만큼 주어지나요? </a></dt>
				<dd><strong class="txt txt-faq8">편지 한 통을 번역할 때마다 30분의 봉사활동시간이 주어집니다.</strong></dd>
				<dt><span class="txt1 txt-no2">2.</span> <a href="" class="txt tit-faq9">스크리닝이 완료되지 않으면 봉사활동 시간으로 인정받지 못하는 건가요?</a></dt>
				<dd><strong class="txt txt-faq9">스크리닝 완료와 관계 없이 번역을 완료하고 전송한 편지는, 전송하는 순간 바로 봉사 활동 시간으로 인정됩니다.하지만 스크리닝 과정 중 반송되어 자신의 편지함으로 되돌아 온 편지는 봉사활동 내역에서 빠지게 됩니다. 반송된 편지를 수정 또는 추가 번역하여 다시 재전송하셔야만 다시 봉사활동시간으로 인정됩니다.</strong></dd>
				<dt><span class="txt1 txt-no3">3.</span> <a href="" class="txt tit-faq10">편지가 스크리닝 되는데 어느 정도의 시간이 걸리나요? </a></dt>
				<dd><strong class="txt txt-faq10">보통 편지 발송이 시급한 편지들 먼저 스크리닝 되며, 평균 2-3주가 소요됩니다. 방학 기간과 같이 메이트님들의 번역활동이 활발해져 번역 편지 양이 많아질 경우, 스크리닝이 완료되는데 더 오랜 시간이 소요되기도 합니다.</strong></dd>
				<dt><span class="txt1 txt-no4">4.</span> <a href="" class="txt tit-faq12">편지를 더 가져오려고 하는데 '일주일 번역 장수를 초과했다.'라는 메시지가 뜹니다.</a></dt>
				<dd><strong class="txt txt-faq12">메이트님에 따라 일주일에 가져올 수 있는 편지 양이 정해져 있습니다. (적게는 1통 많게는 수십통까지) 번역을 꾸준히 오래 하실수록 또 잘 하실수록 편지 장수는 늘어납니다. 일주일 번역 장수를 늘리고 싶으시다면 'MATE 나눔' - '온라인 게시판'에 올려진 장수요청에 대한 공지사항을 읽어 보신 후 요청 글을 남겨주시기 바랍니다.</strong></dd>
				<dt><span class="txt1 txt-no5">5.</span> <a href="" class="txt tit-faq13">편지를 가져오려고 하는데 번역대상편지 수가 0장이에요. 어떻게 해야 하나요? </a></dt>
				<dd><strong class="txt txt-faq13">주로 여름방학(7~8월)과 겨울방학(12월~1월) 기간동안에는 학생 메이트님들의 활동이 왕성해지면서 편지가빠른 속도로 사라집니다. "내가 번역을 잘 못해서 번역을 못 하게 된건가?"라는 걱정은 하지 않으셔도 됩니다. 편지가 사무실에 도착하는대로 스캔을 하고 있으니 인내심을 가지고 기다려주시면 감사드리겠습니다.</strong></dd>
				<dt><span class="txt1 txt-no6">6.</span> <a href="" class="txt tit-faq14">제가 번역하고 있던 편지가 갑자기 사라졌어요. 어떻게 해야 하나요?</a></dt>
				<dd><strong class="txt txt-faq14">오랫동안 번역이 되지 않고 있는 편지 또는 후원자님께서 급히 받아보셔야 하는 편지는 불가피하게 컴패션 담당직원이 가져가야할 때가 있습니다. 당황하지 마시고 양해해 주시면 감사 드리겠습니다.</strong></dd>
				<dt><span class="txt1 txt-no7">7.</span> <a href="" class="txt tit-faq15">'편지 가져오기'를 클릭해서 가져온 편지는 언제까지 번역해야 하나요?</a></dt>
				<dd><strong class="txt txt-faq15">가져가신 편지는 '편지가져오기'를 클릭한 시점에서 5일 이내에 번역해 주셔야 합니다. 5일이 지난 편지는 '번역저장'(편지함)에서 자동으로 사라집니다.</strong></dd>
				<dt><span class="txt1 txt-no8">.</span> <a href="" class="txt tit-faq16">번역을 완료하고 전송한 편지가 다시 제 번역저장(편지함)으로 돌아왔어요. 어떻게 해야 하나요?</a></dt>
				<dd><strong class="txt txt-faq16">스크리닝(감수) 과정 중에 뒷 장이 번역되지 않았거나, 편지 원문과 번역 내용이 다르거나, 부분적으로 번역을 하지 않은 편지들이 발견되면 번역해주신 메이트님께 해당 편지가 반송됩니다. 편지가 반송될 때에는 문자로 안내해 드립니다. 반송된 편지는 5일 이내에 수정, 추가 번역하여 전송해 주셔야 합니다. 3회 이상 편지가 반송될 경우 활동 중단이 되니 유의해 주시기 바랍니다.</strong></dd>
				<dt><span class="txt1 txt-no9">9.</span> <a href="" class="txt tit-faq17">편지 글씨를 알아보기 어려운데 어떻게 해야 하나요? </a></dt>
				<dd><strong class="txt txt-faq17">홈페이지의 MATE 나눔 - 번역Q&amp;A에 해당 편지 ID, 이미지와 함께 질문을 남겨주시면 오랫동안 편지 번역을해주신 다른 메이트님들과 컴패션 직원이 답변을 드립니다. 답변을 확인하신 후 편지 번역을 완료하여 전송해 주시면 됩니다.</strong></dd>
			</dl>
 
 
			<div class="faq-box">
				<dl class="txt txt-more">
					<dt>FAQ에서 원하시는 답변을 찾지 못하셨나요?</dt>
					<dd>문의사항을 상담게시판에 남겨주세요. 빠른 시일 내에 메이트님께서 남겨주신 질문에 답변해 드리도록 노력하겠습니다. 로그인 후 이용하실 수 있으며, 문의하신 내용은 컴패션 담당자와 메이트님만 확인할 수 있습니다.</dd>
				</dl>
				<div class="btns">
					<a href="/nanum/OnlineList.aspx" class="btn btn-online-counsel"><span>1:1온라인상담</span></a>
				</div>
			</div>
			<script type="text/javascript">			    pageSplit('tab-faq', 'cont', 'tab');</script>
		</div>
		<!-- // contents -->
 
	</div>
	<!-- // wrapper -->
 
</div>

		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    </form>
</body>
</html>
