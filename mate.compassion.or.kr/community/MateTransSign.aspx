﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MateTransSign.aspx.cs" Inherits="Mate_community_MateTransSign" EnableViewStateMac="false" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="UTF-8">

    <link rel="stylesheet" href="/common/css/mate.css" />
    <style type="text/css"> 
    #wrap{width:740px;margin:0 auto;background:#fff;border-right:1px solid #ece9df;border-left:1px solid #ece9df;overflow:hidden;}
    #conts{display:inline;float:left;position:relative;width:700px;margin:0 -1px 0 0;padding:20px;overflow:hidden;}

    .apply .step-wrap{width:700px;margin:-1px auto;border:1px solid #d5d5d5; border-widht:0 1px 1px;}        
              
    .consent-sign .text{background:url('/image/mate/txt-step04_renew.png') no-repeat;}
    .consent-sign .text *{display:inline-block;text-indent:-10000em;}
    .consent-sign .txt-consent-form{height:920px;margin:50px 45px 47px 50px;padding-bottom:45px;background-position:0 0;border-bottom:1px solid #ddd;}
    .consent-sign .txt-activity-term{margin:0 50px 74px 48px;}
    .consent-sign .txt-activity-term .txt-activity-term1{display:block;width:131px;height:16px;padding-bottom:11px;background-position:0 -1000px;}
    .consent-sign .txt-activity-term .txt-activity-term2{color:#ff8700;font-size:1.2em;text-decoration:underline;}
    .consent-sign .txt-activity-term .txt-activity-term3{display:block;width:335px;height:16px;margin-top:11px;background-position:0 -1100px;}
    .consent-sign .signature-wrap{width:532px;;margin:0 auto 60px;border:1px solid #ddd;overflow:hidden;}
    .consent-sign .signature-wrap dt, .consent-sign .signature-wrap dd{float:left;overflow:hidden;}
    .consent-sign .signature-wrap dt{height:39px;}
    .consent-sign .signature-wrap dd{height:27px;padding:13px 0 0;}
    .consent-sign .signature-wrap strong{line-height:1.2}
    .consent-sign .signature-wrap .txt-sign-date1{width:70px;background-position:15px -1138px;background-color:#f7f7f7;border-right:1px solid #eee;}
    .consent-sign .signature-wrap .txt-sign-date2, .consent-sign .signature-wrap .txt-signing2{color:#777777;white-space:nowrap;}
    .consent-sign .signature-wrap .txt-sign-date2{width:104px;text-align:center;font-family:tahoma;font-size:1.2em;line-height:1.1}
    .consent-sign .signature-wrap .txt-signing1{width:68px;background-position:-178px -1138px;border-left:1px solid #eee;border-right:1px solid #eee;background-color:#f7f7f7;}
    .consent-sign .signature-wrap .txt-signing2{display:inline;float:left;width:67px;height:20px;margin:-6px 10px 0 10px;padding:3px 0 0;text-align:center;border:1px solid #ddd;font-weight:bold;line-height:1.4;}
    .consent-sign .signature-wrap .txt-signing3{float:left;width:176px;height:11px;background-position:-350px -1150px;}
    </style>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
    <script type="text/javascript">
    <!--
        //주석처리 2013-05-02
        //document.domain = "compassion.or.kr";

        //Backspace, F5, Ctrl+N, Ctrl+R, Ctrl+C, Ctrl+V 막기
        document.onkeydown = NewEdit;
        function NewEdit() {
            var keyval = event.keyCode;
            if (keyval == 116) {
                EventSet();
                alert("F5 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 78)) {
                EventSet();
                alert("Ctrl + N 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 82)) {
                EventSet();
                alert("Ctrl + R 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 86)) {
                EventSet();
                alert("Ctrl + V 키를 금지합니다.");
            }
            if (event.ctrlKey == true && (event.keyCode == 67)) {
                EventSet();
                alert("Ctrl + C 키를 금지합니다.");
            }

            //Backspace
            if (keyval == 8) {
                if (event.srcElement.tagName == "INPUT" || event.srcElement.tagName == "TEXTAREA") {
                } else {
                    EventSet();
                }
            }
        }

        function EventSet() {
            event.keyCode = 0;
            event.cancelBubble = true;
            event.returnValue = false;
        }

        //수정 2013-03-21
        function subData() {
            var str = document.getElementById("tranName").value;

            if ($.trim(str).length <= 0) {
                alert("서명을 작성하지 않았습니다.");
                document.getElementById("tranName").focus();
                //return;
                return false;
            }
            else {
                document.getElementById("real_name").value = document.getElementById("tranName").value;

                if (confirm("제출하시겠습니까?")) {
                    var f = document.form1;

                    f.target = "ifrm";
                    //개발서버
                    //f.action = "http://localhost:4000/MateTrans/Trans_ok.php";

                    //운영서버
                    //f.action = "http://compassionk.cafe24.com/MateTrans/Trans_ok.php";
                    //f.method = "post";
                    //f.submit();

                    return;
                }
                else {
                    alert("취소되었습니다.");
                    //document.getElementById("tranName").focus();
                    //return;

                    return false;
                }
            }
        }

        //수정 2013-05-02
        function SaveOK() {
            var str = "활동 연장을 신청해 주셔서 감사합니다."
                + "\n\r메이트님께서는 계속해서 2013년 12월 말까지"
                + "\n\r제4기 번역 메이트로 활동하시게 되었습니다."
                + "\n\r메이트님의 어린이 사랑에 진심으로"
                + "\n\r감사드립니다. God bless you!";

            alert(str);

            top.opener.winClose();
        }
    //-->
    </script>
</head>

<%--<body>--%>
<body oncontextmenu='return false' ondragstart='return false'>
    <form id="form1" runat="server">     
    <div>
        <!-- // container -->      
        <div id="container" class="mate">
 
	        <!-- wrap -->
	        <div id="wrap" class="apply consent-sign">
 
		        <!-- conts -->
		        <div id="conts">

			        <div class="step-wrap">
 
				        <div class="text txt-consent-form">
					        <p><strong>번역 메이트 활동 동의서</strong> 동의서를 확인하시고 마지막에 서명해 주시면 번역 메이트 지원이 완료 됩니다.</p>
					        <dl>
						        <dt>컴패션 편지 번역 메이트의 정체성 (Identity) 에 대한 동의</dt>
						        <dd>
							        <ul>
								        <li>1. 컴패션 편지번역 메이트는 후원자와 어린이 사이의 사랑과 희망이 온전히 전달될 수 있도록 보이지 않는 곳에서 섬기는 아름다운 사람입니다.</li>
								        <li>2. 컴패션 편지번역 메이트는 그리스도 안에서 컴패션과 후원자가 함께, 꿈을 잃은 어린이들의 삶에 기적을 만들어가는 동역자입니다.</li>
								        <li>3. 컴패션 편지번역 메이트는 하나님의 영광과 그 선한 뜻을 위하여 하나님께서 주신 재능을 기꺼이 쓰는 신실한 주의 자녀입니다.</li>
							        </ul>
						        </dd>
						        <dt>컴패션 편지 번역 메이트의 핵심가치 (Core Value) 에 대한 동의</dt>
						        <dd>
							        <ul>
								        <li>4. 정직(honesty)을 최우선으로 하여 어린이와 후원자 편지를 번역합니다. 편지번역의 모든 프로세스(메이트 신청, 번역, 오류편지신고, 스크리닝 과정 등) 에 정직함으로 임합니다.</li>
								        <li>5. 탁월한 번역(Excellence)을 향한 끊임없는 시도와 노력을 추구합니다. 한국컴패션에서 제작한 번역 관련 자료(번역유의사항,iMate메뉴얼,홈페이지에 연재중인 번역의 달인)를 꼼꼼히 읽고 숙지한 후 편지번역을 시작합니다.</li>
								        <li>6. 하나님의 청지기(Steward)로서 하나님께서 주신 재능에 감사하는 마음과 겸손한 자세로 성실하게 번역합니다</li>
								        <li>7. 어린이를 비롯한 모든 사람들을 존귀(Dignity)하게 여기며 그 동일한 마음으로 어린이와 후원자의 개인정보 및 편지 내용을 소중히 다루고 외부에 유출하지 않습니다.</li>
							        </ul>
						        </dd>
						        <dt>컴패션 편지 번역 메이트의 활동규칙에 대한 동의</dt>
						        <dd>
							        <ul>
								        <li>8. 정일주일에 (월요일 오전 00:00시부터 다음주 월요일 오전 00:00까지)  3회 (편지세통)이상 번역합니다. 단, 번역할 편지가 없는경우에는 해당되지 않습니다.</li>
								        <li>9. 편지이미지와 장수를 꼼꼼히 확인한 후, 수기로 기록된모든 내용 (편지내용, 성경, 구절, 추신, 날짜 등)을 빠짐없이 번역합니다. 편지내용 누락으로 편지가 반송되면 5일이내에 수정 및 추가 번역하여 재전송합니다.</li>
								        <li>10. 편지 원문의 내용과 다르게 번역되거나 또는 제5항에 언급된 번역유의사항의 내용이 반영되지 않았을 경우 오역으로 간주되며 오역으로 편지가 반송되면 5일 이내에 수정 및 추가 번역하여 재전송합니다.</li>
								        <li>11. 번역 시 글자 판독이 어렵거나 모르는 단어, 문장이 나올 경우 홈페이지의 번역 Q&amp;A 게시판을 통하여 컴패션직원 또는 다른 메이트들의 도움을 받아 번역을 완료합니다.</li>
								        <li>12. 제8항,9항,10항이 준수되지 않았을 경우 별도의 안내를 받게되며, 5회 이상 안내를 받았을 경우에는 내부관리 방침에 따라 활동이 중단될 수 있습니다.</li>
								        <li>13. 번역 메이트 활동이 중단될 경우, 차기 기수 모집 시 재 신청할 수 있습니다.</li>
								        <li>14. 어린이와 후원자가 받는 모든 편지번역본에는 번역 메이asdfasdfasdfasdf니다.</li>
							        </ul>
						        </dd>
					        </dl>
				        </div>
				        <div class="txt-activity-term">
					        <strong class="text txt-activity-term1"><span>상기내용에 동의하며</span></strong>
					        <strong class="txt-activity-term2">
                                <% //수정 2013-05-02 %>
                                <asp:Label ID="labPeriod" runat="server" Text="2013년 7월 1일 (월) ~ 2013년 12월 31일 (화)  6개월간"></asp:Label>
                            </strong>
					        <strong class="text txt-activity-term3"><span>컴패션 편지 번역 메이트로서 성실히 활동하겠습니다.</span></strong>
				        </div>				
                        <dl class="signature-wrap">
					        <dt class="text txt-sign-date1"><span>서명일</span></dt>
					        <dd class="txt-sign-date2">
                                <asp:Label ID="tranDate" runat="server" Text=""></asp:Label>
                            </dd>

					        <dt class="text txt-signing1"><span>서명</span></dt>
					        <dd>
                                <asp:TextBox ID="tranName" CssClass="txt-signing2" runat="server" style="ime-mode:active;"></asp:TextBox>
					        </dd>

                            <dt></dt>
                            <dd class="btn-r" style="padding-top:7px" >
                                <% //수정 2013-03-21 %>
                                <asp:Button ID="btnNext" runat="server" Text="" CssClass="btn btn-submit2" 
                                OnClientClick="return subData();" OnClick="btnNext_Click"/>
                            </dd>
				        </dl>       
			        </div>
 
		        </div>
		        <!-- // conts -->
 
	        </div>
	        <!-- // wrap -->
 
        </div>
		<!-- // container -->  
  
    </div>

    <% //수정 2013-05-02 %>
    <input type="hidden" name="conid" id="conid" runat="server" />
    <input type="hidden" name="user_name" id="user_name" runat="server" />
    <input type="hidden" name="userid" id="userid" runat="server" />
    <input type="hidden" name="Mate" id="Mate" runat="server" />
    <input type="hidden" name="mate_trans" id="mate_trans" runat="server" />
    <input type="hidden" name="real_name" id="real_name" runat="server" />

    <% //추가 2013-05-16 %>
    <input type="hidden" name="save_check" id="save_check" runat="server" />

    <iframe name="ifrm" id="ifrm" width="0" height="0" frameborder="0" src=""></iframe>

    </form>
</body>
</html>

<script type="text/javascript">
<!--
    document.getElementById("tranName").focus();
//-->
</script>
