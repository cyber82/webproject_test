﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MateTrans.aspx.cs" Inherits="Mate_community_MateTrans"%>

<%--<%@ Register src="/Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="/Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>

<%@ Register src="/Controls/MateCommunityRight.ascx" tagname="MateCommunityRight" tagprefix="uc5" %>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <%--<uc1:Header ID="Header1" runat="server" />--%>

    <style type="text/css">
    .trans .txt{position:relative;background:url('/image/mate/transmate_main.jpg') no-repeat}
    .trans .txt-desc{position:relative;width:600px;height:345px;background-position:0 0;float:left;left:0px;} /*수정 2013-05-02*/
    .trans .video{position:relative;width:389px;height:292px;float:left;left:100px;} /*추가 2013-05-02*/
    .trans .txt-cont{position:relative;width:600px;height:580px;background-position:0 -640px;float:left;left:0px;} /*추가 2013-05-02*/
    .trans .tran-btn{position:absolute;top:513px;left:222px;width:157px;height:46px;border:0px solid #777;} /*수정 2013-05-02*/
         
    /*#wrap{width:600px;margin:0 auto;background:#fff;border-right:1px solid #ece9df;border-left:1px solid #ece9df;overflow:hidden;}*/
    /*#conts{display:inline;float:left;position:relative;width:600px;margin:0 -1px 0 0;padding:20px;overflow:hidden;}*/

    /*.apply .step-wrap{width:700px;margin:-1px auto;border:1px solid #d5d5d5; border-widht:0 1px 1px;}*/
    </style>

    <script type="text/javascript">
    <!--
        var oNewWindow;

        function subData() {
            var f = document.form2;

            //f.target = "ifrm";
            //f.action = "http://localhost:4000/MateTrans/Trans_ok.php";
            //f.action = "http://compassionk.cafe24.com/MateTrans/Trans.php";
            //f.action = "http://compassionk.cafe24.com/MateTrans/Trans_ok.php";

            //f.target = "_blank";
            //f.action = "MateTransSign.aspx";
            //f.method = "post";
            //f.submit();

            //추가 2012-10-31
            //수정 2013-05-02
            oNewWindow = window.open('MateTransSign.aspx', 'as', 'left=50, top=50,toolbar=no,location=no,directories=no,menubar=no,resizable=no,scrollbars=1,status=0,width=890,height=700');
        }

        //추가 2013-05-02
        function winClose() {
            oNewWindow.window.close();
        }
    //-->
    </script>
</head>

<%--<body>--%>
<body oncontextmenu='return false' ondragstart='return false'>
    <form id="form2" runat="server">
    <div>
        <%--<uc2:Top ID="Top1" runat="server" />--%>        
        <!-- container -->
        
        <div id="container" class="mate">
 
	        <!-- wrap -->
	        <div id="wrap" class="announce notice">
 
		        <!-- sidebar -->
		        <%--<uc5:MateCommunityRight ID="MateCommunityRight1" runat="server" />--%>
		        <!-- //sidebar -->
 
		        <!-- conts -->
		        <div id="conts">
			        <%--<div class="headline headline-dep3">
				        <div class="txt">
					        <h3>활동연장 신청서</h3>
					        <p>메이트 활동연장 신청서입니다.</p>
				        </div>
			        </div>

			        <div class="breadcrumbs">
				        <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/community/noticelist.aspx">MATE 알림</a><strong>활동연장 신청서</strong>
			        </div>--%>

                    <div class="trans">
                        <% //수정 2013-05-02 %>
                        <div class="txt txt-desc">
                        </div>

                        <div class="video">
                            <object width="389" height="292"><param name="movie" value="http://www.youtube.com/v/H5_pu5YLezA?version=3&amp;hl=ko_KRversion=3&autohide=1&autoplay=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/H5_pu5YLezA?version=3&amp;hl=ko_KRversion=3&autohide=1&autoplay=1" type="application/x-shockwave-flash" width="389" height="292" allowscriptaccess="always" allowfullscreen="true"></embed></object>
                        </div>

                        <div class="txt txt-cont">
                            <a href="javascript:subData()" class="tran-btn" alt="신청하기"></a>
                        </div>


                        <dl style="display:none;">
                            <dt class="txt tran-date">서명일</dt>
                            <dd class="tran-date-txt"><asp:Label ID="tranDate" runat="server" Text=""></asp:Label></dd>
                        </dl>
                        <dl style="display:none;">
                            <dt class="txt tran-name">서명자</dt>
                            <dd class="tran-name-txt"><asp:Label ID="tranName" runat="server" Text=""></asp:Label></dd>
                        </dl>

                        <div class="txt tran-line" style="display:none;"></div>
                    </div>
		        </div>
		        <!-- // conts -->
 
	        </div>
	        <!-- // wrap -->

        </div>
		<!-- // container -->  
          
        <%--<uc3:Footer ID="Footer1" runat="server" />--%>      
    </div>

    <% //수정 2013-05-02 %>
    <input type="hidden" name="conid" id="conid" runat="server" />
    <input type="hidden" name="user_name" id="user_name" runat="server" />
    <input type="hidden" name="userid" id="userid" runat="server" />
    <input type="hidden" name="Mate" id="Mate" runat="server" />
    <input type="hidden" name="mate_trans" id="mate_trans" runat="server" />

    <%--<iframe name="ifrm" id="ifrm" width="0" height="0" frameborder="0" src=""></iframe>--%>

    </form>
</body>
</html>
