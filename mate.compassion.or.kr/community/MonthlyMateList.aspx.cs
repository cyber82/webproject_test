﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data;

public partial class Mate_community_MonthlyMateList : System.Web.UI.Page
{
    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //메이트 메인 업데이트 2012-11-22  추가
            MateSSB();
        }
    }


    #region 이달의 우수메이트 글 가져오기 2012-11-22
    private void MateSSB()
    {
        WWWService.Service _wwwService = new WWWService.Service();
        string code = "month_mate";
        string sjs = "";

        DataSet ssb_data = new DataSet();

        try
        {
            ssb_data = _wwwService.MateSsbView(code);
        }
        catch (Exception ex)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("데이터 불러오기를 실패하였습니다.\r\n" + ex.ToString());
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs.ToString());
            return;
        }

        //수정 2013-07-01
        string nameStr = string.Empty;
        if (ssb_data != null)
        {
            if (ssb_data.Tables[0].Rows.Count > 0)
            {
                s_img.Src = "/image/mate/temp/" + ssb_data.Tables[0].Rows[0]["code"].ToString() + "/" + ssb_data.Tables[0].Rows[0]["img_filename"].ToString();
                nameStr = ssb_data.Tables[0].Rows[0]["title"].ToString();
                
                //추가 2013-07-01
                if (nameStr.Trim() == "-" || nameStr.Trim() == "")
                {
                    mate2.Attributes.Add("style", "display:none;");
                    nameStr = "";
                }
               
                s_name.Text = nameStr;
            }
            else
            {
                mate2.Attributes.Add("style", "display:none;");
            }
        }
        else
        {
            mate2.Attributes.Add("style", "display:none;");
        }
    }
    #endregion
}