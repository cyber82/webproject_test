﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Mate_community_MateTransSign : System.Web.UI.Page
{
    WWWService.Service _WWWService = new WWWService.Service();
    DataSet dsMate = new DataSet();
    DateTime dtNow = DateTime.Now;

    string sjs = string.Empty;
    public string a;
    public string b;
    public string c;
    public string d;
    public string t;
    public bool mateTrans = false;

    protected string dateStartStr, dateEndStr;
    protected string dateStartFlag, dateEndFlag;
    protected string dateStartYN, dateEndYN;


    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        UserInfo sess = new UserInfo();
        _WWWService = new WWWService.Service();
        dsMate = new DataSet();


        //번역메이트 활동 연장 신청기간  2012-11-22 추가
        //수정 2013-05-02  
        dateStartStr = "2013-05-06 14:00:00";
        dateEndStr = "2013-05-24 14:00:00";

        dateStartFlag = dtNow.CompareTo(Convert.ToDateTime(dateStartStr)).ToString();
        dateEndFlag = dtNow.CompareTo(Convert.ToDateTime(dateEndStr)).ToString();

        if (int.Parse(dateStartFlag) > -1)
        {
            dateStartYN = "Y";
        }
        else
        {
            dateStartYN = "N";
        }

        if (int.Parse(dateEndFlag) > -1)
        {
            dateEndYN = "Y";
        }
        else
        {
            dateEndYN = "N";
        }

        //if (dateStartYN != "Y" || dateEndYN == "Y")
        //{
        //    //신청 마감시에 처리
        //    sjs = JavaScript.HeaderScript.ToString();
        //    sjs += JavaScript.GetAlertScript("활동 연장 기간이 마감되었습니다. 감사합니다.");
        //    sjs += "window.close();";
        //    sjs += JavaScript.FooterScript.ToString();
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", sjs);
        //    return;
        //}

        //수정 2013-05-02
        if (dateStartYN == "N" && dateEndYN == "N")
        {
            //추가 2013-05-16
            save_check.Value = "prev";

            //신청 이전시에 처리
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("활동 연장 신청은 5월 6일(월) 오후 2시부터 시작됩니다.\\r\\n감사합니다.");
            sjs += "top.opener.winClose();";
            //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
        //End 마감일이 지난 경우 문구 출력
        //추가 2013-05-02
        else if (dateStartYN == "Y" && dateEndYN == "Y")
        {
            //추가 2013-05-16
            save_check.Value = "next";

            //신청 마감시에 처리
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("활동 연장 기간이 마감되었습니다. 감사합니다.");
            sjs += "top.opener.winClose();";
            //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }


        if (!IsPostBack)
        {
            if (!UserInfo.IsLogin)
            {
                //문구수정 2012-11-05
                string sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("로그인 후, 신청 가능합니다.");
				//sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx?returnUrl=/community/MateTransSign.aspx");
				sjs += JavaScript.GetPageMoveScript("/login.aspx");
				sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }

            if (sess.SponsorID != null)
            {
                //번역메이트가 신청이 안되어 있을때
                //문구는 현재 번역 메이트로 활동 중이신 분들만 신청하실 수 있습니다. 이미지로 팝업창
                try
                {
                    dsMate = _WWWService.MateMyActive(sess.UserId.ToString().Trim());

                    if (dsMate.Tables[0].Rows.Count > 0)
                    {
                        if (dsMate.Tables[0].Rows[0]["TranslationMate"].ToString().Trim() == "Y" && dsMate.Tables[0].Rows[0]["CurrentUse"].ToString().Trim() == "Y")
                        {
                            //추가 2013-05-16
                            save_check.Value = "save";

                            mateTrans = true;
                        }
                        else
                        {
                            //추가 2013-05-16
                            save_check.Value = "matefalse";

                            mateTrans = false;
                        }
                    }
                    else
                    {
                        //추가 2013-05-16
                        save_check.Value = "matefalse";

                        mateTrans = false;
                    }

                    
                    if (mateTrans == false)
                    {
                        //문구수정 2012-11-05
                        sjs = JavaScript.HeaderScript.ToString();
                        sjs += JavaScript.GetAlertScript("현재 활동 중인 메이트만 신청 가능합니다.");
                        sjs += "top.opener.winClose();";
                        //sjs += JavaScript.GetNewWindowScript("/popup/trans_fail.html", "fail", 600, 200, false);
                        sjs += JavaScript.FooterScript.ToString();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "NewWin", sjs.ToString());
                        return;
                    }
                    
                }
                catch (Exception ex)
                {
                    //Exception Error Insert
                    WWWService.Service _wwwService = new WWWService.Service();
                    _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetAlertScript("회원님의 정보를 읽어오는 중 오류가 발생했습니다.\\r\\n관리자에게 문의해주세요.");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs.ToString());
                }

                //인증받지 않은 기업일때 
                if (sess.GenderCode == "C" && (sess.CertifyDate == null || sess.CertifyDate.ToString().Trim() == ""))
                {
                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetPageMoveScript("/membership/GroupToCallcenter.aspx");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert1", sjs.ToString());
                    return;
                }
                //인증받지 않은 미주일때 
                if (sess.LocationType == "미주" && (sess.CertifyDate == null || sess.CertifyDate.ToString().Trim() == ""))
                {
                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetPageMoveScript("/Mypage/EmailCertifyMy.aspx");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert2", sjs.ToString());
                    return;
                }
                //인증받지 않은 국외일때 
                if (sess.LocationType == "국외" && (sess.CertifyDate == null || sess.CertifyDate.ToString().Trim() == ""))
                {
                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += JavaScript.GetPageMoveScript("/Mypage/EmailCertifyMy.aspx");
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert3", sjs.ToString());
                    return;
                }
            }
        }

        //서명정보 셋팅
        ValueSet();

    }


    #region 서명정보 셋팅
    private void ValueSet()
    {
        UserInfo sess = new UserInfo();

        a = sess.ConId.ToString();
        b = sess.UserName.ToString();
        c = sess.UserId.ToString();

        if (sess.Mate != null)
        {
            d = sess.Mate.ToString();
        }
        else
        {
            d = "처음";
        }
        d = d.Replace("Mate", "");
        d = d.Replace("사무", "");
        d = d.Replace("전문", "");
        d = d.Replace("통역", "");
        d = d.Replace("처음", "");
        d = d.Replace("Mate", "");
        d = d.Replace(",", "");
        d = d.Replace("  ", "");
        d = d.Replace("<font color='#0054a6'>", "");
        d = d.Replace("</font>", "");
        d = d.Replace("<div>", "");
        d = d.Replace("</div>", "");

        if (mateTrans == true)
        {
            t = "Y";
        }
        else
        {
            t = "N";
        }

        tranDate.Text = dtNow.ToShortDateString().Trim().Substring(0, 4) + "년 "
                      + dtNow.ToShortDateString().Trim().Substring(5, 2) + "월 "
                      + dtNow.ToShortDateString().Trim().Substring(8, 2) + "일";
        tranName.Text = b;

        //Response.Write(a + "<br/>");
        //Response.Write(b + "<br/>");
        //Response.Write(c + "<br/>");
        //Response.Write(d + "<br/>");

        conid.Value = a;
        user_name.Value = b;
        userid.Value = c;
        Mate.Value = d;
        mate_trans.Value = t;
    }
    #endregion


    #region 저장버튼
    //추가 2013-03-21  저장버튼
    protected void btnNext_Click(object sender, EventArgs e)
    {
        ////테스트 2013-05-02
        //sjs = JavaScript.HeaderScript.ToString();
        //sjs += "SaveOK();";
        //sjs += JavaScript.FooterScript.ToString();
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "success", sjs);
        //return;

        string uname = user_name.Value.ToString();
        string uid = userid.Value.ToString();
        string rname = real_name.Value.ToString();
        string mate = Mate.Value.ToString();
        string cid = conid.Value.ToString();
        string savechk = save_check.Value.ToString(); //추가 2013-05-16
        
        string flag = "4"; //기수  수정 2013-05-02


        //예외처리 추가 2013-05-16
        if (savechk == "prev")
        {
            //신청 이전시에 처리
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("활동 연장 신청은 5월 6일(월) 오후 2시부터 시작됩니다.\\r\\n감사합니다.");
            sjs += "top.opener.winClose();";
            //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
        else if (savechk == "next")
        {
            //신청 마감시에 처리
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("활동 연장 기간이 마감되었습니다. 감사합니다.");
            sjs += "top.opener.winClose();";
            //sjs += JavaScript.GetPageMoveScript("/Default.aspx");
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
            return;
        }
        else if (savechk == "matefalse")
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += JavaScript.GetAlertScript("현재 활동 중인 메이트만 신청 가능합니다.");
            sjs += "top.opener.winClose();";
            //sjs += JavaScript.GetNewWindowScript("/popup/trans_fail.html", "fail", 600, 200, false);
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "NewWin", sjs.ToString());
            return;
        }
        else if (savechk == "save")
        {
            DataSet dsSave = new DataSet();

            //데이터 DB에 저장
            try
            {
                UserInfo user = new UserInfo();
                WWWService.Service service = new WWWService.Service();

                dsSave = service.SetInsertMateTrans(uname, uid, rname, mate, cid, flag);
            }
            catch (Exception ex)
            {
                //Exception Error Insert
                WWWService.Service _wwwService = new WWWService.Service();
                _wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);

                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("신청을 등록하는 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }


            bool result = false;

            if (dsSave != null)
            {
                if (dsSave.Tables.Count > 0)
                {
                    if (dsSave.Tables[0].Rows.Count > 0)
                    {
                        if (dsSave.Tables[0].Rows[0]["Result"].ToString() == "Y")
                        {
                            result = true;

                            //수정 2013-05-02
                            sjs = JavaScript.HeaderScript.ToString();
                            //sjs += JavaScript.GetAlertScript("저장 되었습니다.");
                            //sjs += JavaScript.GetOpenerPageMoveScript("/Default.aspx");
                            sjs += "SaveOK();";
                            sjs += JavaScript.FooterScript.ToString();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "success", sjs);
                            return;
                        }
                        else if (dsSave.Tables[0].Rows[0]["Result"].ToString() == "N")
                        {
                            result = true;

                            sjs = JavaScript.HeaderScript.ToString();
                            sjs += JavaScript.GetAlertScript(dsSave.Tables[0].Rows[0]["Result_String"].ToString());
                            sjs += JavaScript.FooterScript.ToString();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                            return;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }

            if (result == false)
            {
                sjs = JavaScript.HeaderScript.ToString();
                sjs += JavaScript.GetAlertScript("신청이 등록되지 않았습니다.");
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }
        }
    }
    #endregion

}