﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NoticeList.aspx.cs" Inherits="Mate_community_NoticeList" %>

<%@ Register Assembly="TaeyoNetLib" Namespace="TaeyoNetLib" TagPrefix="taeyo" %>
<%@ Register src="../Controls/MateHeader.ascx" tagname="Header" tagprefix="uc1" %>
<%@ Register src="../Controls/MateTop.ascx" tagname="Top" tagprefix="uc2" %>
<%@ Register src="~/Controls/Footer.ascx" tagname="Footer" tagprefix="uc3" %>

<%@ Register src="../Controls/MateCommunityRight.ascx" tagname="MateCommunityRight" tagprefix="uc5" %>

<%@ Register src="../Controls/BoardList.ascx" tagname="BoardList" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <uc1:Header ID="Header1" runat="server" />
    <script language="javascript">
        function SearchValidate() {
            if (document.getElementById("BoardList1_txtSearch").value.trim() == "") {
                alert("검색어를 입력 하세요.");
                return false;
            }
            return true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">     
    <div>
        <uc2:Top ID="Top1" runat="server" />        
        <!-- container -->
        <div id="container" class="mate">
 
	        <!-- wrapper -->
	        <div id="wrapper" class="announce notice">
 
		        <!-- sidebar -->
		        <uc5:MateCommunityRight ID="MateCommunityRight1" runat="server" />
		        <!-- //sidebar -->
 
		        <!-- contents -->
		        <div id="contents">
			        <div class="headline headline-dep3">
				        <div class="txt">
					        <h3>공지사항</h3>
					        <p>전 세계의 가난한 어린아이들을 위해 헌신하는 자원봉사자 mate를 소개합니다.</p>
				        </div>
			        </div>
			        <div class="breadcrumbs">
				        <a href="/Default.aspx">HOME</a><!--<a href="/Share/SponsorEnterprise.aspx">적극적참여</a>--><a href="/Default.aspx">MATE</a><a href="/community/noticelist.aspx">MATE 알림</a><strong>공지사항</strong>
			        </div>
			        <uc4:BoardList ID="BoardList1" runat="server" />	
		        </div>
		        <!-- // contents -->
 
	        </div>
	        <!-- // wrapper -->
 
        </div>


		<!-- // container -->    
       <uc3:Footer ID="Footer1" runat="server" />      
    </div>    
    <asp:hiddenfield ID="hidTableIdx" runat="server" Value ="1001"></asp:hiddenfield> 
    </form>
</body>
</html>
