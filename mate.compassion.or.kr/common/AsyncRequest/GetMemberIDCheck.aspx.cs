﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data; 

public partial class common_AsyncRequest_GetMemberIDCheck : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string userID = Request.Form["UserID"];
            if (!String.IsNullOrEmpty(userID))
            {
                WWWService.Service service = new WWWService.Service();

                DataSet ds = service.getSponsor("", "", "", "", userID, "", "", "");
                if (ds.Tables[0].Rows.Count == 0)
                {
                    Response.Write("1");
                }
                else if (ds.Tables[0].Rows.Count > 0)
                {
                    Response.Write("0");
                }
            }
            else
                Response.Write("-1");
        }
        else
            Response.Write("-1");
    }
}