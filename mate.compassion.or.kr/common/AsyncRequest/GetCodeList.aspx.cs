﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.IO;

public partial class common_AsyncRequest_GetCodeList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            DataSet data = GetApplyCode(Convert.ToInt32(Request.Form["parent"]));
            StringWriter st = new StringWriter ();
            data.Tables[0].WriteXml (st);
            Response.Write (st.ToString ());
        }
        catch (Exception ex)
        {
            Response.Write (ex.Message);  
        }
    }

    private DataSet GetApplyCode(int parent)
    {
        Recruit.RecruitService service = new Recruit.RecruitService();
        DataSet data = service.GetCodeMasterList(parent);
        return data;
    }
}