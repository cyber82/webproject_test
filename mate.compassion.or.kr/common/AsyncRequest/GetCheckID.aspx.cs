﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data;

public partial class common_AsyncRequest_GetCheckID : System.Web.UI.Page
{
    WWWService.Service _WWWService = new WWWService.Service();
    DataSet ds = new DataSet();
    
    string sjs = string.Empty;
    string userid = string.Empty;


    /// <summary>
    /// 페이지 로드
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["txtLoginID"] != null)
        {
            userid = Request.QueryString["txtLoginID"].ToString();
        }

        //sjs = JavaScript.HeaderScript.ToString();
        //sjs += JavaScript.GetAlertScript(userid);
        //sjs += JavaScript.FooterScript.ToString();
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "test", sjs.ToString());

        CheckID(userid);
    }


    #region 아이디 유무 검사  2013-07-03 추가
    private void CheckID(string sUserid)
    {
        if (sUserid.Length < 6 || sUserid.Length > 12)
        {
            sjs = JavaScript.HeaderScript.ToString();
            sjs += "setData('-1')";
            sjs += JavaScript.FooterScript.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "func0", sjs.ToString());
        }
        else if (sUserid.Trim() != string.Empty)
        {
            try
            {
                ds = _WWWService.getSponsor("", "", "", "", sUserid, "", "", "");
            }
            catch (Exception ex)
            {
                //메세지 띄우기
                sjs = JavaScript.HeaderScript;
                sjs += JavaScript.GetAlertScript("데이터를 조회하던 중 에러가 발생했습니다. 관리자에게 문의하세요. 원인 : " + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", ""));
                sjs += JavaScript.FooterScript;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
                return;
            }

            //수정 2013-07-04
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += "setData('0')";
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "func2", sjs.ToString());
                }
                else
                {
                    sjs = JavaScript.HeaderScript.ToString();
                    sjs += "setData('1')";
                    sjs += JavaScript.FooterScript.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "func3", sjs.ToString());
                }
            }
            else
            {
                sjs = JavaScript.HeaderScript.ToString();
                sjs += "setData('-1')";
                sjs += JavaScript.FooterScript.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "func1", sjs.ToString());
            }
        }
    }
    #endregion

}