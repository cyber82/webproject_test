﻿//탭사용하기
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->     

//팝업창///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//팝업1
var win = null; 
function NewWindow(mypage,myname,w,h,scroll){ 
		//LeftPosition = (screen.width) ? (screen.width-w)/2 : 0; 
		//TopPosition = (screen.height) ? (screen.height-h)/2 : 0; 
		LeftPosition=(screen.width-(2*w))/2;
		TopPosition= (screen.height-h)/2;
		settings = 'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll;
				       
		window.open(mypage,myname,settings).focus();
	}            
	
//팝업2
	function NewWindow2(mypage,myname,w,h,scroll){ 
		//LeftPosition = (screen.width) ? (screen.width-w)/2 : 0; 
		//TopPosition = (screen.height) ? (screen.height-h)/2 : 0; 
		LeftPosition=(screen.width-(2*w))/2;
		TopPosition= (screen.height-h)/2;
		settings = 'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable=no','toolbar=no','menubar=no','directories=no','titlebar=no';
				       
		window.open(mypage,myname,settings).focus();    
	}  
	
//팝업1
var win = null; 
function NewWindow3(mypage,myname,w,h,scroll){ 
		//LeftPosition = (screen.width) ? (screen.width-w)/2 : 0; 
		//TopPosition = (screen.height) ? (screen.height-h)/2 : 0; 
		LeftPosition=(screen.width-(2*w))/2;
		TopPosition= (screen.height-h)/2;
		settings = 'height=0,width=0,top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll;
				       
		window.open(mypage,myname,settings).focus();
	}    	   
               
    
//팝업3
 function notWindow1(ref,what) {
      var window_left = (screen.width-540)/2;
      var window_top = (screen.height-630)/2;
      ref = ref + "?what=" + what;       
          window.open(ref,"notWin",'toolbar=no,resizable=yes,scrollbars=no,location=no,resize=no,menubar=no,directories=no,copyhistory=0,width=540,height=630,top= 50,left = 50');
   }
//팝업4
 function notWindow2(ref,what) {
      var window_left = (screen.width-540)/2;
      var window_top = (screen.height-630)/2;
      ref = ref + "?what=" + what;        
         window.open(ref,"notWin",'toolbar=no,resizable=yes,scrollbars=no,location=no,resize=no,menubar=no,directories=no,copyhistory=0,width=540,height=630,top= 50,left = 50');
   }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
//-- 필드가 비어있는지 체크
function BlankCheck(iLen)
{
    if(iLen == 0)
    {
        return false;
    }
    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//-- 기준값(길이) 사이에 문자수가 정확한지 체크 (영문 + 숫자일경우 사용)
function LenAreaCheck(sValue, iStart, iEnd, sMsg)
{
    if(sValue.length < iStart || sValue.length > iEnd )
    {
        alert(sMsg + " " + iStart + "자 이상 " + iEnd + "자 이하이어야 합니다.");
        return false;
    }  
    return true;   
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Trim제공
String.prototype.trim = function()
{
 return this.replace(/(^\s*)|(\s*$)/g, "");
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//폼 이동
function moveForm(str,nextOb,len) {
	if(str.length == len ) nextOb.focus();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function getHost() {
    var host = '';
    var loc = String(location.href);
    if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
    else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
    else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
    return host;
}

//페이지이동
function DirectMovePage(Addr,Type)        
{
    var type=Type;
    var addr=Addr;   
    
    var host = getHost();
   
   if(type=="1")
   {
       document.location.href="http://"+host+"/"+addr;
   }
  else if(type=="2")
   {
      document.location.href="https://"+host+"/"+addr;
   }else{
   //콜센터페이지로    
      document.location.href="http://"+host+"/"+addr+"?SponsorName="+type;   
   }     
}
  
   ////////////////////////////////////////////////////////////////////////////////////////////////
   //로그아웃
   function LogOutPage()
   {
   document.location.href="http://"+getHost()+"/LogoutPage.aspx";
   }
   
   //메이트로그아웃
   function MateLogOutPage()
   {
       document.location.href="http://"+getHost()+"/LogoutPage.aspx?Title=Mate";
   }                   
       
   ////////////////////////////////////////////////////////////////////////////////////////////////
   //로그인페이지로 
   function MoveLoginPage()
   {
       document.location.href="https://"+getHost()+"/membership/MemberLogin.aspx";   
   } 
   
   ///////////////////////////////////////////////////////////////////////////////////////////////
   //메인페이지로
   function MoveMainPage()
   {
       document.location.href="http://"+getHost()+"/main/index.aspx";     
   }      
            
