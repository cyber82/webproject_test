//Trim제공
String.prototype.trim = function () {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//-- 필드가 비어있는지 체크
function BlankCheck(iLen) {
    if (iLen == 0) {
        return false;
    }
    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//-- 기준값(길이) 사이에 문자수가 정확한지 체크 (영문 + 숫자일경우 사용)
function LenAreaCheck(sValue, iStart, iEnd, sMsg) {
    if (sValue.length < iStart || sValue.length > iEnd) {
        alert(sMsg + " " + iStart + "자 이상 " + iEnd + "자 이하이어야 합니다.");
        return false;
    }
    return true;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////플래시이미지2//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
function MakeFlash(Url, Name, Width, Height, Wmode) {
    document.writeln("<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0\" id=\"" + Name + "\" width=\"" + Width + "\" height=\"" + Height + "\" onError=\"this.style.display='none'\" />");
    document.writeln("<param name=\"allowScriptAccess\" value=\"sameDomain\" />");
    document.writeln("<param name=\"movie\" value=\"" + Url + "\" />");
    document.writeln("<param name=\"quality\" value=\"high\" />");
    document.writeln("<param name=\"wmode\" value=\"" + Wmode + "\" />");
    document.writeln("<embed src=\"" + Url + "\" quality=\"high\" pluginspage=\"https://www.macromedia.com/go/getflashplayer\" type=\"application/x-shockwave-flash\" name=\"" + Name + "\" width=\"" + Width + "\"  height=\"" + Height + "\" />");
    document.writeln("</object>");
}
//플래시이미지////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function CreateControl(DivID, CLSID, WIDTH, HEIGHT, URL) {
    var d = document.getElementById(DivID);
    sObject = "<object classid='" + CLSID + "' codebase='https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0' width='" + WIDTH + "' height='" + HEIGHT + "' VIEWASTEXT>";
    sObject += "<param name='movie' value='" + URL + "'><param name='quality' value='high'><param name='wmode' value='transparent'>";
    sObject += "<embed src='" + URL + "' quality='high' pluginspage='https://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' width='" + WIDTH + "' height='" + HEIGHT + "'></embed>";
    sObject += "</object>";

    d.innerHTML = sObject;
}
//팝업창///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//팝업1
var win = null;
function NewWindow(mypage, myname, w, h, scroll) {
    //LeftPosition = (screen.width) ? (screen.width-w)/2 : 0; 
    //TopPosition = (screen.height) ? (screen.height-h)/2 : 0; 
    LeftPosition = (screen.width - (2 * w)) / 2;
    TopPosition = (screen.height - h) / 2;
    settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll;

    window.open(mypage, myname, settings).focus();
}

//팝업2
function NewWindow2(mypage, myname, w, h, scroll) {
    //LeftPosition = (screen.width) ? (screen.width-w)/2 : 0; 
    //TopPosition = (screen.height) ? (screen.height-h)/2 : 0; 
    LeftPosition = (screen.width - (2 * w)) / 2;
    TopPosition = (screen.height - h) / 2;
    settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll + ',resizable=no', 'toolbar=no', 'menubar=no', 'directories=no', 'titlebar=no';

    window.open(mypage, myname, settings).focus();
}



//Trim제공
String.prototype.trim = function () {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}

function getHost() {
    var host = '';
    var loc = String(location.href);
    if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
    else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
    else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
    return host;
}

//페이지이동
function DirectMovePage(Addr, Type) {
    var type = Type;
    var addr = Addr;

    var host = getHost();

    if (type == "1") {
        document.location.href = "http://" + host + "/" + addr;
    }
    else if (type == "2") {
        document.location.href = "https://" + host + "/" + addr;
    } else {
        //콜센터페이지로    
        document.location.href = "http://" + host + "/" + addr + "?SponsorName=" + type;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////
//로그아웃
function LogOutPage() {
    document.location.href = "http://" + getHost() + "/LogoutPage.aspx";
}

//메이트로그아웃
function MateLogOutPage() {
    document.location.href = "http://" + getHost() + "/LogoutPage.aspx?Title=Mate";
}

////////////////////////////////////////////////////////////////////////////////////////////////
//로그인페이지로 
function MoveLoginPage() {
    document.location.href = "https://" + getHost() + "/membership/MemberLogin.aspx";
}

///////////////////////////////////////////////////////////////////////////////////////////////
//메인페이지로
function MoveMainPage() {
    document.location.href = "http://" + getHost() + "/main/index.aspx";
}

function CountryMove() {
    //  alert(document.all.ctl00_selCountry.value);   
    var con = document.all.ctl00_selCountry.value;
    // alert(con);              
    if (con != "no") {
        //NewWindow2("http://"+con,"국가별사이트",500,600,"yes");              
        window.open("http://" + con, "_blank");
        //window.open("http://"+con,"small",'width=700,height=600,scrollbars=yes,menubar=yes,resizable=yes')
    }
}



//세션체크후 로그인페이지로       
function DenyPage() {
    document.location.href = "https://" + getHost() + "/membership/MemberLogin.aspx";
}

//////////////////////////////////////////////////////////////////////////////////////////////
//보안인증서 적용후 생기는 alert방지를 위해 redirect를 자바스크립트로 처리 

function Address(add) {
    document.location.href = "http://" + getHost() + "/" + add;
}

function CertifyAddress(add) {
    document.location.href = "https://" + getHost() + "/" + add;
}




function playFlash() {
    alert("실행");
}
function showFlashBanner() {
    var div_flashBanner = document.getElementById("div_flashBanner");

    var de = document.documentElement;
    var b = document.body;
    var scrollY = 0;
    scrollY = document.all ? (!de.scrollTop ? b.scrollTop : de.scrollTop) : (window.pageYOffset ? window.pageYOffset : window.scrollY);

    div_flashBanner.style.top = (scrollY + 150).toString() + "px";
    div_flashBanner.innerHTML = createFlashOBJ("/main/flash/loader.swf", "f2", 900, 689, "", true);
    div_flashBanner.style.display = "block";
}

function closeFlash() {
    div_flashBanner.innerHTML = "";
    div_flashBanner.style.display = "none";
}

function setComma(str) {
    str = "" + str + "";
    var retValue = "";
    for (i = 0; i < str.length; i++) {
        if (i > 0 && (i % 3) == 0) {
            retValue = str.charAt(str.length - i - 1) + "," + retValue;
        } else {
            retValue = str.charAt(str.length - i - 1) + retValue;
        }
    }
    return retValue;
}

function checkContentLength(content, max_length) {
    var maxStr = max_length / 2;
    var i;
    var string = content.value;
    var one_char;
    var str_byte = 0;
    var str_length = 0;
    for (i = 0; i < string.length; i++) {
        // 한글자추출
        one_char = string.charAt(i);

        // 한글이면 2를 더한다.
        if (escape(one_char).length > 4) {
            str_byte = str_byte + 2;
        }
        // 그외의 경우는 1을 더한다.
        else {
            str_byte++;
        }

        // 전체 크기가 li_max를 넘지않으면
        if (str_byte <= max_length) {
            str_length = i + 1;
        }
    }

    document.getElementById("countComment").innerText = str_byte;

    // 전체길이를 초과하면
    if (str_byte > max_length) {
        alert("영문" + max_length + "자리 한글 " + maxStr + "자리이상을 초과할 수 없습니다.");
        content.value = string.substr(0, str_length);
        document.getElementById("countComment").innerText = max_length;
    }
}

function checkUniCodeContentLength(content, max_length) {
    var maxStr = max_length / 2;
    var i;
    var string = content.value;
    var one_char;
    var str_byte = 0;
    var str_length = 0;
    for (i = 0; i < string.length; i++) {
        // 한글자추출
        one_char = string.charAt(i);

        // 한글도 1 더한다
        if (escape(one_char).length > 4) {
            str_byte++;
        }
        // 그외의 경우 모두 1 더한다
        else {
            str_byte++;
        }

        // 전체 크기가 li_max를 넘지않으면
        if (str_byte <= max_length) {
            str_length = i + 1;
        }
    }

    document.getElementById("countComment").innerText = str_byte;

    // 전체길이를 초과하면
    if (str_byte > max_length) {
        alert("글자수는 " + max_length + "자리이상을 초과할 수 없습니다.");
        content.value = string.substr(0, str_length);
        document.getElementById("countComment").innerText = max_length;
    }
}