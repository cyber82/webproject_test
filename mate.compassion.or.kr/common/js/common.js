function tabList(num)
{
	var list = document.getElementById('tabList')
	var anc = list.getElementsByTagName('fieldset')
	for(var i=1; i<=anc.length; i++)
	{
		document.getElementById('cont_'+i).style.display = 'none';
	}
	document.getElementById('cont_'+num).style.display = '';
}

// toggle Layer
function toggleLayer(id) {
	try {
	var obj = document.getElementById(id);
	obj.style.display = (obj.style.display == "none") ? "" : "none";
	} catch (e) {

	}
	return true;
}

// page split
function pageSplit(targetId,cont,tab){
	var num = location.href.split('?num=')[1] || 1;

	var list = document.getElementById(targetId);
	var anc = list.getElementsByTagName('a');

	for(var i=1; i<=anc.length; i++) {
		document.getElementById(cont+i).style.display = (num==i) ? 'block' : 'none';
		document.getElementById(tab+i).parentNode.className = (num==i) ? 'on' : '';
	}
	return false;
} 

(function () {
	var window = this,
		document = window.document,
		documentElement = document.documentElement,
		$ = window.jQuery,
		$document = $(document),
		msie = $.browser.msie && parseFloat($.browser.version);

	/* Selectbox */
	$.fn.selectbox = function () {
		var all_selects = this;

		function hideAll() {
			all_selects.each(function () {
				this.close();
			});
		}

		$document.find('body').bind('click', function () {
			hideAll();
		});

		return this.each(function () {
			var self = this,
				old_selectbox = $(this),
				select_width = parseInt(old_selectbox.outerWidth()) - 2,
				old_options = old_selectbox.find('option'),
				selected_index = getSelectedIndex(old_options),
				wrapper, value_holder, new_selectbox_string = '<ul class="new-selectbox">',
				new_selectbox, new_options;

			self.close = function () {
				new_selectbox.slideUp('fast');
				wrapper.css('z-index', 0);
			};

			self.reGen = function () {
				old_options = old_selectbox.find('option');
				new_selectbox.empty().removeClass('selectbox-scroll');
				new_selectbox_string = [];
				for (var i = 0, len = old_options.length; i < len; i++) {
					new_selectbox_string.push('<li>');
					new_selectbox_string.push($(old_options[i]).text());
					new_selectbox_string.push('</li>');
				}
				new_selectbox.append(new_selectbox_string.join(''));
				select_width = parseInt(old_selectbox.outerWidth()) - 2;
				new_options = new_selectbox.find('li');
				select($(new_options[0]));
				value_holder.css('width', select_width + 'px');
				new_selectbox.css('width', (select_width - 2) + 'px');
				optionBinder();
				if (new_options.length > 10) {
					new_selectbox.addClass('selectbox-scroll');
				}
			};

			generateNewSelectbox();

			function getSelectedIndex(opts) {
				var index = 0,
					len = opts.length,
					i;
				for (i = 0; i < len; i++) {
					if ($(opts[i]).attr('selected')) {
						index = i;
					}
				}
				return index;
			}

			function generateNewSelectbox() {

				var opt_len = old_options.length,
					i;
				old_selectbox.wrap('<div class="selectbox-wrap" />');
				wrapper = old_selectbox.parent('div.selectbox-wrap');
				for (i = 0; i < opt_len; i++) {
					new_selectbox_string += '<li>' + $(old_options[i]).text() + '</li>';
				}
				new_selectbox_string += '</ul>';
				wrapper.append('<input type="button" class="value-holder" value="' + $(old_options[selected_index]).text() + '" style="' + ($(all_selects).is(":disabled") ? "background-color:#ebebe4;" : "") + '">');
				wrapper.append(new_selectbox_string);

				value_holder = wrapper.find('input.value-holder');
				value_holder.css('width', select_width + 'px');
				new_selectbox = wrapper.find('ul.new-selectbox');
				new_selectbox.css('width', (select_width - 2) + 'px');
				new_options = new_selectbox.find('li');
				if (new_options.length > 10) {
					new_selectbox.addClass('selectbox-scroll');
				}
				$(new_options[selected_index]).addClass('selected');
			}

			function select(obj) {
				var index = new_options.index(obj);
				new_options.removeClass('selected');
				obj.addClass('selected');
				old_options.attr('selected', false);
				$(old_options[index]).attr('selected', true);
				value_holder.val(obj.text());
				old_selectbox.change();
			}

			value_holder.bind('click', function (e) {

				if ($(all_selects).is(":disabled")) {
					return false;
				}

				//ebebe4
				e.stopPropagation();
				e.preventDefault();
				if (new_selectbox.css('display') === 'none') {
					hideAll();
				}
				wrapper.css('z-index', '10');
				new_selectbox.slideToggle(100);
			});

			value_holder.bind('keydown', function (e) {
				var selected = new_options.filter('.selected');
				if (e.keyCode === 40) { // Down Arrow
					e.preventDefault();
					if (selected.next().length) {
						select(selected.next());
					}
				} else if (e.keyCode === 38) { // Up Arrow
					e.preventDefault();
					if (selected.prev().length) {
						select(selected.prev());
					}
				} else if (e.keyCode === 9) {
					new_selectbox.slideUp(100);
					wrapper.css('z-index', '0');
				}
			});

			optionBinder();

			function optionBinder() {
				new_options.bind('click', function () {
					select($(this));
					new_selectbox.slideUp(100);
					wrapper.css('z-index', '0');
					// value_holder.focus();
				});
				new_options.hover(function () {
					new_options.removeClass('selected');
					new_options.removeClass('hover');
					$(this).addClass('hover');
				});
			}
			this.newSelect = new_selectbox;
		});
	};

	/* On DOM Ready */
	$(function () {

		/* Selectbox */
		$document.find('select:not([multiple])').selectbox();


		/* ETC 섹션 버튼 사이즈 변경 */
		function joinBtn() {
			$('.joinwrap .btn-confirm1, .joinwrap .btn-find1').each(function(){
				$(this).height($(this).prev().height());
			});
		}
		joinBtn();
		$('.tab-certitype li a, .field-sort input, .chk-certi input').click(function () {
			joinBtn();
		});

		var tabContainers = $('.tab-list > fieldset');
		tabContainers.hide().filter(':first').show();
		
		$('.tab-list .sdf legend').click(function () {
			tabContainers.hide();
			tabContainers.hide().filter(this.hash).show();
			$('.tab-list .sdf legend').removeClass('selected');
			$(this).addClass('selected');
			return false;
		})

		$('.faq-list dd, .dl-list dd').hide();
		$('.dl-list dd:first').show();
		 $(".faq-list dt, .dl-list dt").click(function() {
			$(this).next().slideToggle().siblings('dd').slideUp();
			$(this).toggleClass('unfold').siblings().removeClass('unfold');
			return false;
		});
		
		$('.txt-before, .pay-gift .gift .txt-bold1').hide();
		$('.sec-gift').change(function (){
			$('.sec-gift option:selected').each(function () {
				$('.pay-gift .txt-before, .pay-gift .gift .txt-bold1').hide();
				$('.pay-gift .select-month').parent('.selectbox-wrap').show();
			});
			$('.sec-gift option:eq(1):selected').each(function () {
				$('.pay-gift .txt-before, .pay-gift .gift .txt-bold1').show();
				$('.pay-gift .select-month').parent('.selectbox-wrap').hide();
			});
		});


		/* tab */
		$(".tab-content").hide(); 
		$("ul.tabs li:first").addClass("on").show(); 
		$(".tab-content:first").show(); 
		$("ul.tabs li").click(function() {
			$("ul.tabs li").removeClass("on");
			$(this).addClass("on");
			$(".tab-content").hide(); 
			var activeTab = $(this).find("a").attr("href");
			$(activeTab).fadeIn();
			return false;
		});



		$(".tab-content1").hide(); 
		$("ul.tabs1 li:first").addClass("on").show(); 
		$(".tab-content1:first").show(); 
		$("ul.tabs1 li").click(function() {
			$("ul.tabs1 li").removeClass("on");
			$(this).addClass("on");
			$(".tab-content1").hide(); 
			var activeTab = $(this).find("a").attr("href");
			$(activeTab).show();
			return false;
		});


		$(".layer-search-detail").hide();
		$(".search-detail .btn, .relation-act .btn-delete3").click(function() {
			$('.search-detail .btn').toggleClass('btn-search-child1').toggleClass('btn-search-child2');
			$(".layer-search-detail").toggle();
			return false;
		});

		/* navigation */
		$(".type2 > li div, .type3 > li div").css('visibility','hidden');
		var timeout    = 500;
		var closetimer = 0;
		var ddmenuitem = 0;

		function jsddm_open() {
			jsddm_canceltimer();
			jsddm_close();
			ddmenuitem = $(this).find('div').css('visibility', 'visible');
		}

		function jsddm_close() {
			if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');
		}

		function jsddm_timer() {
			closetimer = window.setTimeout(jsddm_close, timeout);
		}

		function jsddm_canceltimer() {
			if(closetimer) {
				window.clearTimeout(closetimer);
				closetimer = null;
			}
		}

		$(document).ready(function() {
			$('.type2 > li, .type3 > li').bind('mouseover', jsddm_open)
			$('.type2 > li, .type3 > li').bind('mouseout',  jsddm_timer)
		});

		document.onclick = jsddm_close;


		$('#payCardOwner1').click(function(){
			$('.automated-credit').attr("rowSpan",'2');
		});

		$('#payCardOwner2').click(function(){
			$('.automated-credit').attr("rowSpan",'3');
		});


		/* 활동내역 - 카테고리 */
		$('.btn-down').click(function() {
			$('.td-category1').toggleClass('.td-unfold');
			$('.td-category1 .spread-area').toggle();
			return false;
		});
		$('.td-category1 .spread-area').hide();

		/* table border */
		$(".list1 th:last-child, .list1 td:last-child, .list2 th:last-child, .list2 td:last-child, .write1 th:last-child, .write1 td:last-child, .view1 th:last-child, .view1 td:last-child").addClass('last-td')
		$(".list1 tbody tr:last-child, .list2 tbody tr:last-child, .write1 tbody tr:last-child, .view1 tbody tr:last-child").addClass('last-tr')

		$('.list-faq tr.answer').hide();
		$('.list-faq .td-title a').click(function(){
			$('.list-faq tr.answer').hide();
			$(this).parent().parent().next().show();
		return false;
		});

			
		/* mate login */
		$('.index .login-area input.text').focus(function() {
			$(this).css("background-image", "none")
		});


		/* 수혜국 편지 용어 */ 
		$('.letters>li').hover(function() {
			$(this).addClass('on');
		}, function(){
			$(this).removeClass('on');
		});

		/* 메인 페이지 floating layer 닫기 */
		$('.floating-message input:button').click(function() {
			$(this).parents('.floating-message').hide();
		});


		$('.result-wrap .case-wrap').hide();
		$('.result-wrap .btn-case').hover(function() {
			$(this).next('.case-wrap').toggle();
		});


	});

}());

function lnbRollNavi(gnbseq, seq, sbseq) {
    var gnbNavi = document.getElementById("gnb");
    if (lnbNavi == null)
        return;

    var gnbsubMenu = gnbNavi.getElementsByTagName("ul");
    var gnbMenu = gnbNavi.getElementsByTagName("li");
    var gnblink = gnbNavi.getElementsByTagName("a");

    if (document.getElementById("gnb" + gnbseq)) {
        var gnbmn = document.getElementById("gnb" + gnbseq);
        var gnbmnUl = gnbmn.getElementsByTagName("ul");
        gnbmn.className = 'on';
        gnbmn.onmouseout = gnbmn.onblur = function () {
            this.className = 'on';
        }
    }

    var lnbNavi = document.getElementById("lnb");
    var subMenu = lnbNavi.getElementsByTagName("ul");
    var lnbMenu = lnbNavi.getElementsByTagName("li");
    var lnblink = lnbNavi.getElementsByTagName("a");

    if (document.getElementById("sub" + seq)) {
        var lnbmn = document.getElementById("sub" + seq);
        var lnbmnUl = lnbmn.getElementsByTagName("ul");
        lnbmn.className = 'on';
        lnbmn.onmouseout = lnbmn.onblur = function () {
            this.className = 'on';
        }
    }

    if (document.getElementById("sub" + seq + "_" + sbseq)) {
        var lnbmn2depth = document.getElementById("sub" + seq + "_" + sbseq);
        lnbmn2depth.className = 'on';

        lnbmn2depth.onmouseout = lnbmn2depth.onblur = function () {
            this.className = 'on';
        }
        lnbmn2depth.parentNode.style.display = 'block';
    }

    for (i = 0; i < subMenu.length; i++) {
        subMenu[i].style.display = "none"; // 전체 서브레이어 숨김.
        if (document.getElementById("_subEl" + seq)) {
            var lnbsubUl = document.getElementById("_subEl" + seq);
            lnbsubUl.style.display = 'block';
        }
    }

}

function lnbRollNavi2(ctlName,gnbseq, seq, sbseq) {
    var gnbNavi = document.getElementById("gnb");
    var gnbsubMenu = gnbNavi.getElementsByTagName("ul");
    var gnbMenu = gnbNavi.getElementsByTagName("li");
    var gnblink = gnbNavi.getElementsByTagName("a");

    if (document.getElementById("gnb" + gnbseq)) {
        var gnbmn = document.getElementById("gnb" + gnbseq);
        var gnbmnUl = gnbmn.getElementsByTagName("ul");
        gnbmn.className = 'on';
        gnbmn.onmouseout = gnbmn.onblur = function () {
            this.className = 'on';
        }
    }

    var lnbNavi = document.getElementById("lnb");
    var subMenu = lnbNavi.getElementsByTagName("ul");
    var lnbMenu = lnbNavi.getElementsByTagName("li");
    var lnblink = lnbNavi.getElementsByTagName("a");

    if (document.getElementById("sub" + seq)) {
        var lnbmn = document.getElementById("sub" + seq);
        var lnbmnUl = lnbmn.getElementsByTagName("ul");
        lnbmn.className = 'on';
        lnbmn.onmouseout = lnbmn.onblur = function () {
            this.className = 'on';
        }
    }

    if (document.getElementById("sub" + seq + "_" + sbseq)) {
        var lnbmn2depth = document.getElementById("sub" + seq + "_" + sbseq);
        lnbmn2depth.className = 'on';

        lnbmn2depth.onmouseout = lnbmn2depth.onblur = function () {
            this.className = 'on';
        }
        lnbmn2depth.parentNode.style.display = 'block';
    }

    for (i = 0; i < subMenu.length; i++) {
        subMenu[i].style.display = "none"; // 전체 서브레이어 숨김.
        if (document.getElementById(ctlName+"_subEl" + seq)) {
            var lnbsubUl = document.getElementById(ctlName+"_subEl" + seq);
            lnbsubUl.style.display = 'block';
        }
    }

}

function lnbRollNavi3(ctlName, gnbseq, seq, sbseq) {
    var gnbNavi = document.getElementById("gnb");
    var gnbsubMenu = gnbNavi.getElementsByTagName("ul");
    var gnbMenu = gnbNavi.getElementsByTagName("li");
    var gnblink = gnbNavi.getElementsByTagName("a");

    if (document.getElementById("gnb" + gnbseq)) {
        var gnbmn = document.getElementById("gnb" + gnbseq);
        var gnbmnUl = gnbmn.getElementsByTagName("ul");
        gnbmn.className = 'on';
        gnbmn.onmouseout = gnbmn.onblur = function () {
            this.className = 'on';
        }
    }

    var lnbNavi = document.getElementById("lnb");
    var subMenu = lnbNavi.getElementsByTagName("ul");
    var lnbMenu = lnbNavi.getElementsByTagName("li");
    var lnblink = lnbNavi.getElementsByTagName("a");

    if (document.getElementById(ctlName + "_sub" + seq)) {
        var lnbmn = document.getElementById(ctlName + "_sub" + seq);
        var lnbmnUl = lnbmn.getElementsByTagName("ul");
        lnbmn.className = 'on';
        lnbmn.onmouseout = lnbmn.onblur = function () {
            this.className = 'on';
        }
    }

    if (document.getElementById("sub" + seq + "_" + sbseq)) {
        var lnbmn2depth = document.getElementById("sub" + seq + "_" + sbseq);
        lnbmn2depth.className = 'on';

        lnbmn2depth.onmouseout = lnbmn2depth.onblur = function () {
            this.className = 'on';
        }
        lnbmn2depth.parentNode.style.display = 'block';
    }

    for (i = 0; i < subMenu.length; i++) {
        subMenu[i].style.display = "none"; // 전체 서브레이어 숨김.
        if (document.getElementById(ctlName + "_subEl" + seq)) {
            var lnbsubUl = document.getElementById(ctlName + "_subEl" + seq);
            lnbsubUl.style.display = 'block';
        }
    }

}

// IE Flicker bug
try {
	document.execCommand("BackgroundImageCache", false, true);
} catch(ignored) {}


// rotation
function ImageRotation() {
	// options
	var scrollType = 'vertical'; // 'horizontal', 'vertical', 'none';

	// private
	var currentNumber = 0;
	var objWrap = null;
	var objContentBox = null;
	var objWrapLIs = null;
	var cellWidth = 0;
	var cellHeight = 0;
	this.GoodsSetTime = null;

	// scroll animation variables.
	var scroll = {time:1, start:0, change:0, duration:15, timer:null};
	var originaltime = scroll.time;

	this.setScrollType = function (type) {
		switch (type) {
			case 'vertical':
			case 'horizontal':
			case 'none':
				scrollType = type;
				break;
			default:
				alert('!');
				break;
		}
	}
	// constructor
	this.initialize = function () {
		objWrap = document.getElementById(this.wrapId);
		objContentBox = document.getElementById(this.listId);
		objWrapLIs = objWrap.getElementsByTagName('li');
		cellWidth = objWrapLIs[0].offsetWidth;
		cellHeight= objWrapLIs[0].offsetHeight;

		objWrap.style.overflow = 'hidden'; //
		objWrap.style.position = 'relative'; //

		switch (scrollType) {
			case 'vertical':
				this.objWrapSize = cellHeight * this.listNum;
				this.objSize = objWrapLIs.length * cellHeight;
				break;
			case 'none':
				this.objWrapSize = cellWidth * this.listNum;
				this.objSize = objWrapLIs.length * cellWidth;
				break;
			default:
				this.objWrapSize = cellWidth * this.listNum;
				this.objSize = objWrapLIs.length * cellWidth;
				break;
		}
		if (this.objWrapSize < this.objSize) {

			if (objWrapLIs.length > 0) {
				switch (scrollType) {
					case 'vertical':
						objContentBox.style.height = objWrapLIs.length * cellHeight + 'px';
						objWrap.style.height = this.listNum * cellHeight + 'px';
						break;
					case 'none':
						objContentBox.style.width = objWrapLIs.length * cellWidth + 'px';
						objWrap.style.width = this.listNum * cellWidth + 'px';
						break;
					default:
						objContentBox.style.width = objWrapLIs.length * cellWidth + 'px';
						objWrap.style.width = this.listNum * cellWidth + 'px';
						break;
				}
			}
			if (this.btnPrev)
				document.getElementById(this.btnPrev).href = "javascript:" + this.objName + ".prev();";
			if (this.btnNext)
				document.getElementById(this.btnNext).href = "javascript:" + this.objName + ".next();";

			if (this.autoScroll == 'none') {
			} else {
				if (this.scrollDirection == 'direction') {
					this.GoodsSetTime = setInterval(this.objName + ".next()", this.scrollGap);
				} else {
					this.GoodsSetTime = setInterval(this.objName + ".prev()", this.scrollGap);
				}
			}
		}
	}

	this.next = function () {
		if (currentNumber == 0) {
			var objLastNode = objContentBox.removeChild(objContentBox.getElementsByTagName('li').item(objWrapLIs.length - 1));
			objContentBox.insertBefore(objLastNode, objContentBox.getElementsByTagName('li').item(0));
			switch (scrollType) {
				case 'vertical':
					objWrap.scrollTop += cellHeight;
					break;
				case 'none':
					objWrap.scrollLeft += cellWidth;
					break;
				default:
					objWrap.scrollLeft += cellWidth;
					break;
			}
			currentNumber++;
		}

		//objWrap.scrollLeft -= cellWidth;
		var position = getActionPoint('indirect');
		startScroll(position.start, position.end);

		currentNumber = currentNumber - 1;

		if (currentNumber > 0)
			currentNumber = 0;
		if (this.autoScroll == 'none') {
			// do nothing.
		} else {
			this.scrollDirection = 'direction';
			clearInterval(this.GoodsSetTime);
			this.GoodsSetTime = setInterval(this.objName + ".next()", this.scrollGap);
		}
	}

	this.prev = function () {
		if (currentNumber == objWrapLIs.length - 1) {
			var objLastNode = objContentBox.removeChild(objContentBox.getElementsByTagName('li').item(0));
			objContentBox.appendChild(objLastNode);
			switch (scrollType) {
				case 'vertical':
					objWrap.scrollTop -= cellHeight;
					break;
				case 'none':
					objWrap.scrollLeft -= cellWidth;
					break;
				default:
					objWrap.scrollLeft -= cellWidth;
					break;
			}
			currentNumber--;
		}

		//objWrap.scrollLeft += cellWidth;
		var position = getActionPoint('direct');
		startScroll(position.start, position.end);

		currentNumber = currentNumber + 1;

		if (currentNumber < objWrapLIs.length - 1)
			currentNumber = objWrapLIs.length - 1;

		if (this.autoScroll == 'none') {
			// do nothing.
		} else {
			this.scrollDirection = 'indirection';
			clearInterval(this.GoodsSetTime);
			this.GoodsSetTime = setInterval(this.objName + ".prev()", this.scrollGap);
		}
	}

	var startScroll = function (start, end) {
		if (scroll.timer != null) {
			clearInterval(scroll.timer);
			scroll.timer = null;
		}

		scroll.start = start;
		scroll.change = end - start;

		switch (scrollType) {
			case 'vertical':
				scroll.timer = setInterval(scrollVertical, 20);
				break;
			case 'none':
				objWrap.scrollLeft = end;
				break;
			default:
				scroll.timer = setInterval(scrollHorizontal, 90);
				break;
		}
	}

	var scrollVertical = function () {
		if (scroll.time > scroll.duration) {
			clearInterval(scroll.timer);
			scroll.time = originaltime;
			scroll.timer = null;
		} else {
			objWrap.scrollTop = sineInOut(scroll.time, scroll.start, scroll.change, scroll.duration);
			scroll.time++;
		}
	}

	var scrollHorizontal = function () {
		if (scroll.time > scroll.duration) {
			clearInterval(scroll.timer);
			scroll.time = originaltime;
			scroll.timer = null;
		} else {
			objWrap.scrollLeft = sineInOut(scroll.time, scroll.start, scroll.change, scroll.duration);
			scroll.time++;
		}
	}

	var getActionPoint = function (dir) {
		if (dir == 'direct') {
			var position = findElementPos(objWrap.getElementsByTagName('li').item(currentNumber + 1)); // target image.
			var offsetPos = findElementPos(objWrap.getElementsByTagName('li').item(currentNumber)); // first image.
		} else {
			var position = findElementPos(objWrap.getElementsByTagName('li').item(currentNumber - 1)); // target image.
			var offsetPos = findElementPos(objWrap.getElementsByTagName('li').item(currentNumber)); // first image.
		}

		switch (scrollType) {
			case 'vertical':
				var start = objWrap.scrollTop;
				var end = position[1] - offsetPos[1];
				break;
			case 'none':
				// do nothing.
				break;
			default:
				var start =  objWrap.scrollLeft;
				var end = position[0] - offsetPos[0];
				break;
		}

		var position = {start:0, end:0};
		position.start = start;
		position.end = end;

		return position;
	}

	var sineInOut = function (t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	}

	var findElementPos = function (elemFind) {
		var elemX = 0;
		var elemY = 0;
		do {
			elemX += elemFind.offsetLeft;
			elemY += elemFind.offsetTop;
		} while (elemFind = elemFind.offsetParent)

		return Array(elemX, elemY);
	}

}


/*
	-스크롤 위치

*/
  var getNowScroll = function(){
	var de = document.documentElement;
	var b = document.body;
	var now = {};
	now.X = document.all ? (!de.scrollLeft ? b.scrollLeft : de.scrollLeft) : (window.pageXOffset ? window.pageXOffset : window.scrollX);
	now.Y = document.all ? (!de.scrollTop ? b.scrollTop : de.scrollTop) : (window.pageYOffset ? window.pageYOffset : window.scrollY);
	return now;
  }

/*
	-공통 레이어 띄우기
	 pUrl : 레이어 파일명
	 w    : 레이어 width
	 h    : 레이어 height
*/

function layer(pUrl,w,h) {
	nowScroll = getNowScroll();
	_close(); // 레이어 초기화
	var divTag		= '';
	var backTag		= '';
	var iframeTag = '';
	backTag		+="<div id='backTag' style='dispaly:none;z-index:1000;background-color:#000;filter:alpha(opacity=50); opacity:0.5; -moz-opacity:0.5;'>";
	backTag		+="</div>";
	divTag		+="<div id='feedLayer' style='dispaly:none;z-index:1003;'>";
	divTag		+="</div>";
	iframeTag +="<iframe id='bkifrm' frameborder='0' style='z-index:1001;'/>"
	jQuery("body").append(backTag);
	jQuery("#backTag").show();

	var layer1 = jQuery('#backTag');
	layer1.css({
		position: 'absolute'
		, top   : '0'
		, left  : '0'
		, width : '100%'
		, height: document.body.clientHeight
		});

		//var iHeight = (document.body.clientHeight / 2) - w / 2 + document.body.scrollTop;
	var iHeight = nowScroll.Y + 250; // 스크롤 위치에서 200xp 위치
	var iWidth  = (document.body.clientWidth / 2) - w / 2 + document.body.scrollLeft;

	jQuery("body").append(iframeTag);
	jQuery('#bkifrm').css({
		position: 'absolute'
		, top   : iHeight
		, left  : iWidth
		, width : w
		, height: h-10
		});



	jQuery("body").append(divTag);
	jQuery("#feedLayer").show();
	var layer2 = jQuery('#feedLayer');
	layer2.load(pUrl);


	layer2.css({
		position: 'absolute'
		, top   : iHeight
		, left  : iWidth
		, width : w
		, height: h
		});
}
function _close(){
	jQuery("#feedLayer").remove();;
	jQuery("#backTag").remove();;
	jQuery("#bkifrm").remove();;
}