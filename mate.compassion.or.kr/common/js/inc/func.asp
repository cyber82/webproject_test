<%
'///  메시지 후 뒤로
Sub Alert_Back(msg)
	Response.Write "<script type='text/javascript'>alert('" & msg & "');history.back();</script>"
	Response.End
End Sub

'///  메시지 후 url로
Sub Alert_Go(msg, url)
	if Trim(msg) <> "" then
		if(Trim(url) <> "") then
			Response.Write "<script type='text/javascript'>alert('" & msg & "');location.href='" & url & "';</script>"
		else
			Response.Write "<script type='text/javascript'>alert('" & msg & "');</script>"
		end if
	else
		Response.Write "<script type='text/javascript'>location.href='" & url & "';</script>"
	end if
	Response.End
End Sub

'///  메시지 후 url로 치환
Sub Alert_Replace(msg, url)
	if Trim(msg) <> "" then
		Response.Write "<script type='text/javascript'>alert('" & msg & "');location.replace('" & url & "');</script>"
	else
		Response.Write "<script type='text/javascript'>location.replace('" & url & "');</script>"
	end if
	Response.End
End Sub

'/// 숫자로 된 selectbox 생성 (시작숫자,종료숫자,증가숫자,선택될값)
Function MakeNumSelBox(st,ed,intStep,sel,strSuffix)
	Dim i,selected
	For i=st To ed Step intStep
		If CStr(i)=CStr(sel) then
			selected=" selected"
		Else
			selected=""
		End If
		MakeNumSelBox=MakeNumSelBox & "<option value=" & i & selected & ">" & i & strSuffix & "</option>"
	Next
End Function

Function FormatDigit(intNum, digit)
	Dim i, tmpStr
	tmpStr = intNum
	For i = 1 To digit
		if Len(tmpStr) < digit then tmpStr = "0" & tmpStr
	Next
	FormatDigit = tmpStr
End Function

'날짜형식 포맷팅
Function fnFormatDate(strDate,isFullYear)
	if IsDate(strDate) then
		if isFullYear = 1 then 'yyyy
			fnFormatDate = FormatDateTime(strDate,vbShortDate)
		else 'yy
			fnFormatDate = Mid(FormatDateTime(strDate,vbShortDate),3,8)
		end if
	else
		fnFormatDate = ""
	end if
End Function

Function fnFormatDateTime(strDateTime,isFullYear)
	if IsDate(strDateTime) then
		fnFormatDateTime = fnFormatDate(strDateTime,isFullYear) & " " & FormatDateTime(strDateTime,vbShortTime)
	else
		fnFormatDateTime = "-"
	end if
End Function

'현재메뉴강조, select 또는 radio의 선택/체크 되도록 함
Function fnSelect(v1, v2, strOn)
	Dim retVal

	if v1 = "" AND v2 = "" then
		retVal = strOn
	else
		arrV2 = Split(v2, ",")
		For z=0 To Ubound(arrV2)

			if CStr(v1) = CStr(arrV2(z)) then
				retVal = strOn
				Exit For
			end if
		Next
	end if
	fnSelect = retVal
End Function

'///배열을 이용한selectbox 생성 MakeSelBox(array,시작인덱스,선택될값,value타입)
Function MakeSelect(ARR,st,sel,vType)
	Dim i, selected, optVal
	For i=st To Ubound(ARR)
		if vType=1 then '배열인덱스를 option value로 설정
			optVal=i
		Else
			optVal=ARR(i)  '배열값을 option value로 설정
		End if

		if Not IsNull(sel) then
			If CStr(optVal)=Cstr(sel) then selected=" selected" Else selected="" End If
		end if

		if ARR(i) <> "" then
			MakeSelect=MakeSelect & "<option value='" & optVal & "'" & selected & ">" & ARR(i) & "</option>"
		end if
	Next
End Function

'///문자열처리(HTML 금지)
Function CheckStr(CheckValue, bHtmlChk)
	CheckValue = Replace (CheckValue, "'","''")
	if bHtmlChk = 0 then
		'CheckValue = Replace (CheckValue, "&", "&amp;")
		CheckValue = Replace (CheckValue, ">", "&gt;")
		CheckValue = Replace (CheckValue, "<", "&lt;")
		CheckValue = Replace (CheckValue, """","&quot;")
	end if
	CheckStr = Trim (CheckValue)
End Function


'### 페이지 네비게이션
Class PageNavi

	Public PrevImage		'### 이전xx개 이미지		default = 이전xx개
	Public NextImage		'### 다음xx개 이미지		default = 다음xx개
	Public QueryString		'### 전달될 파라메터		default = ""
	Public NaviCount		'### 네비게이션 카운트		default = 10
	Public Style			'### 네비게이션 스타일		default = 2
'		Style 1 --> 1 | 2 | 3 | 4
'		Style 2 --> [1] [2] [3] [4]
	Public LinkClass		'### 링크 클래스명			default = ""
	Public CurrentPage		'### 현재 페이지			default = 1
	Public PageCount		'### 페이지 카운트			default = 1
	Public ParamPageName	'### 페이지 파라메터 이름	default = "page"
	Public GoFirst			'### 처음으로				default = False
	Public GoLast			'### 끝으로					default = False
	Public GoFirstImage		'### 처음으로 이미지		default = ""
	Public GoLastImage		'### 끝으로 이미지			default = ""
	Public DisplayNoLink	'### 링크없는 이전 다음표시	default = True

	Public SplitterImage	'### 분할 바 이미지

	Public Function MakeNavi
		Dim intStart, intEnd, strResult, i

		If CurrentPage Mod NaviCount = 0 Then
			intStart = ((CurrentPage \ NaviCount) - 1) * NaviCount + 1
		Else
			intStart = (CurrentPage \ NaviCount) * NaviCount + 1
		End If

		If intStart + NaviCount > PageCount Then
			intEnd = PageCount
		Else
			intEnd = intStart + NaviCount - 1
		End If

		'처음 페이지와 이전 10개
		If CurrentPage > 1 Then
			If GoFirst Then
				strResult = MakeLink(1, FirstLast(1, "FIRST")) & " "
				If Style = 3 Then strResult = strResult & "<img src=""" & SplitterImage & """ align=""absmiddle"" alt="""" style=""margin:0px 10px 0px 10px"" /> "
			End If

			If intStart - 1 > 0 Then
				strResult = strResult & MakeLink(intStart - 1, PrevNext("PREV")) & " "
			Else
				strResult = strResult & MakeLink(1, PrevNext("PREV")) & " "
			End If
			If Style = 3 Then strResult = strResult & "<img src=""" & SplitterImage & """ align=""absmiddle"" alt="""" style=""margin:0px 10px 0px 10px"" /> "
		ElseIf DisplayNoLink Then
			If GoFirst Then
				strResult = FirstLast(1, "FIRST") & " "
				If Style = 3 Then strResult = strResult & "<img src=""" & SplitterImage & """ align=""absmiddle"" alt="""" style=""margin:0px 10px 0px 10px"" /> "
			End If

			strResult = strResult & PrevNext("PREV") & " "
			If Style = 3 Then strResult = strResult & "<img src=""" & SplitterImage & """ align=""absmiddle"" alt="""" style=""margin:0px 10px 0px 10px"" /> "
		End If

		'바로가기
		For i = intStart To intEnd
			If CInt(i) < CInt(CurrentPage) Then
				strResult = strResult & MakeStyle(i, "FRONT") & " "
			ElseIf CInt(i) = CInt(CurrentPage) Then
				strResult = strResult & "<b>" & MakeStyle(i, "") & "</b>"
			ElseIf CInt(i) > CInt(CurrentPage) Then
				strResult = strResult & " " & MakeStyle(i, "REAR")
			End If
		Next

		'다음 10개
		If CDbl(CurrentPage) < CDbl(PageCount) Then
			If Style = 3 Then strResult = strResult & " <img src=""" & SplitterImage & """ align=""absmiddle"" alt="""" style=""margin:0px 10px 0px 10px"" />"
			If intEnd + 1 >= PageCount Then
				strResult = strResult & " " & MakeLink(PageCount, PrevNext("NEXT"))
			Else
				strResult = strResult & " " & MakeLink(intEnd + 1, PrevNext("NEXT"))
			End If

			If GoLast Then
				If Style = 3 Then strResult = strResult & " <img src=""" & SplitterImage & """ align=""absmiddle"" alt="""" style=""margin:0px 10px 0px 10px"" />"
				 strResult = strResult & " " & MakeLink(PageCount, FirstLast(PageCount, "LAST"))
			End If
		ElseIf DisplayNoLink Then
			If Style = 3 Then strResult = strResult & " <img src=""" & SplitterImage & """ align=""absmiddle"" alt="""" style=""margin:0px 10px 0px 10px"" />"
			strResult = strResult & " " & PrevNext("NEXT")

			If GoLast Then
				If Style = 3 Then strResult = strResult & " <img src=""" & SplitterImage & """ align=""absmiddle"" alt="""" style=""margin:0px 10px 0px 10px"" />"
				 strResult = strResult & " " & FirstLast(PageCount, "LAST")
			End If
		End If

		MakeNavi = strResult
	End Function

	Private Function MakeStyle(ByRef Page, ByRef Location)
		Select Case Style
			Case 1
				Select Case Location
					Case "FRONT"
						MakeStyle = MakeLink(Page, Page) & " |"
					Case "REAR"
						MakeStyle = "| " & MakeLink(Page, Page)
					Case Else
						MakeStyle = Page
				End Select
			Case 2
				MakeStyle = MakeLink(Page, "[" & Page & "]")
			Case 3
				Select Case Location
					Case "FRONT"
						MakeStyle = MakeLink(Page, Page) & "&nbsp; &nbsp;"
					Case "REAR"
						MakeStyle = "&nbsp; &nbsp;" & MakeLink(Page, Page)
					Case Else
						MakeStyle = Page
				End Select
			Case 4
				Select Case Location
					Case "FRONT"
						MakeStyle = MakeLink(Page, Page) & " &nbsp;&nbsp;"
					Case "REAR"
						MakeStyle = "&nbsp;&nbsp; " & MakeLink(Page, Page)
					Case Else
						MakeStyle = Page
				End Select
			'### 추가 Style은 여기에 만드십시오. ##########################


			'##############################################################
		End Select
	End Function

	Private Function PrevNext(ByRef TypeName)
		Select Case TypeName
			Case "PREV"
				If PrevImage = "" Then
					PrevNext = "이전" & NaviCount & "개"
				Else
					PrevNext = "<img src=""" & PrevImage & """ align=""absmiddle"" alt=""이전" & NaviCount & "개"" />"
				End If
			Case "NEXT"
				If NextImage = "" Then
					PrevNext = "다음" & NaviCount & "개"
				Else
					PrevNext = "<img src=""" & NextImage & """ align=""absmiddle"" alt=""다음" & NaviCount & "개"" />"
				End If
		End Select
	End Function

	Private Function FirstLast(ByRef Page, ByRef TypeName)
'		Select Case TypeName
'			Case "FIRST"
'				If GoFirstImage = "" Then
'					FirstLast = MakeStyle(Page, "")
'				Else
'					FirstLast = "<img src=""" & GoFirstImage & """ align=""absmiddle"" alt=""처음"" />"
'				End If
'			Case "LAST"
'				If GoLastImage = "" Then
'					FirstLast = MakeStyle(Page, "")
'				Else
'					FirstLast = "<img src=""" & GoLastImage & """ align=""absmiddle"" alt=""끝"" />"
'				End If
'		End Select
	End Function

	Private Function MakeLink(ByRef Page, ByRef str)
		MakeLink = "<a href=""?" & ParamPageName & "=" & Page
		If QueryString <> "" Then MakeLink = MakeLink & "&" & QueryString
		MakeLink = MakeLink & """"
		If LinkClass <> "" Then MakeLink = MakeLink & " class=""" & LinkClass & """"
		MakeLink = MakeLink & ">" & str & "</a> "
	End Function

	'### 기본 값 셋팅
	Private Sub Class_Initialize()
		PrevImage = ""
		NextImage = ""
		QueryString = ""
		NaviCount = 10
		Style = 2
		LinkClass = ""
		CurrentPage = 1
		PageCount = 1
		ParamPageName = "page"
		GoFirst = False
		GoLast = False
		GoFirstImage = ""
		GoLastImage = ""
		DisplayNoLink = True
	End Sub
End Class

'현재 년월일(시) 폴더명 (예:2008\01\01\00\)
Function getDateDivDir(is_hour, v_sep)
	Dim tmpDate, rtnVal
	tmpDate = Now()
	rtnVal = Year(tmpDate) & v_sep & FormatDigit(Month(tmpDate),2) & v_sep
	if is_hour then
		rtnVal = rtnVal & FormatDigit(Hour(tmpDate),2) & v_sep
	end if
	getDateDivDir = rtnVal
End Function

'파일명 생성
Function MakeFileName()
	Dim arrRnd(2)
	Dim vDate, tmpDateTime, arrTime, tmpTime
	Randomize
	arrRnd(1) = Int((122 - 97 + 1) * Rnd + 97)
	arrRnd(1) = Chr(arrRnd(1))
	Randomize
	arrRnd(2) = Int((9999 - 1 + 1) * Rnd + 1)

	vDate = Now()
'		tmpDate = Mid(Replace(FormatDateTime(vDate,2),"-",""),3)
'		arrTime = Split(Mid(FormatDateTime(vDate,3),4),":")
'		arrTime = Split(Mid(FormatDateTime(vDate,3),4),":")
'		tmpTime = FormatDigit(arrTime(0),2) & arrTime(1) & arrTime(2)

	tmpDateTime = Right(Year(vDate),2) & FormatDigit(Month(vDate),2) & FormatDigit(Day(vDate),2)
	tmpDateTime = tmpDateTime & FormatDigit(Hour(vDate),2) & FormatDigit(Minute(vDate),2) & FormatDigit(Second(vDate),2)

	MakeFileName = tmpDateTime & arrRnd(1) & arrRnd(2)

End Function


'기존파일명 변경
Function MakeFileName2(oldFile)
	Dim arrRnd(2)
	Dim vDate, tmpDateTime, arrTime, tmpTime
	Randomize
	arrRnd(1) = Int((122 - 97 + 1) * Rnd + 97)
	arrRnd(1) = Chr(arrRnd(1))
	Randomize
	arrRnd(2) = Int((9999 - 1 + 1) * Rnd + 1)

	vDate = Now()
'		tmpDate = Mid(Replace(FormatDateTime(vDate,2),"-",""),3)
'		arrTime = Split(Mid(FormatDateTime(vDate,3),4),":")
'		arrTime = Split(Mid(FormatDateTime(vDate,3),4),":")
'		tmpTime = FormatDigit(arrTime(0),2) & arrTime(1) & arrTime(2)

	tmpDateTime = Right(Year(vDate),2) & FormatDigit(Month(vDate),2) & FormatDigit(Day(vDate),2)
	tmpDateTime = tmpDateTime & FormatDigit(Hour(vDate),2) & FormatDigit(Minute(vDate),2) & FormatDigit(Second(vDate),2)

	MakeFileName2 = oldFile & "_" & tmpDateTime

End Function


Sub MkDirAll(strPath)
	Dim FSO, fldr, z
	Set FSO = Server.CreateObject("Scripting.FileSystemObject")
	fldr = ""
	arrPath = Split(strPath, "\")
	For z=0 To UBound(arrPath)-1
		fldr = fldr & arrPath(z) & "\"
		If z > 0 Then
			If FSO.FolderExists(fldr)=False Then
				Set f = FSO.CreateFolder(fldr)
			End if
		End if
	Next
	Set FSO = Nothing
End Sub

%>