﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.IO;


using System.Text.RegularExpressions;


/// <summary>
/// util의 요약 설명입니다.
/// </summary>
public class util
{
    public util()
    {
        //
        // TODO: 여기에 생성자 논리를 추가합니다.
       //
    }

    private string _tagName;
    public string tagName
    {
        get { return _tagName; }
        set { _tagName = value; }
    }
    
    public string GetXmlTagName(XmlNodeList nodelist, string getName)
    {
       
        foreach (XmlNode node in nodelist)
        {
            if (node.ChildNodes.Count > 1)
            {
                this._tagName = GetXmlTagName(node.ChildNodes, getName);
            }
            else
            {
                if (string.Equals(node.Name, getName))
                {
                    //Response.Write(node.Name + "-" + getName + "== 노드와 검색 노드가 같다.");
                    //Response.Write("<br>");
                    this._tagName = node.InnerText;
                }
                else
                {

                    if (node.HasChildNodes)
                    {
                        if (string.Equals(node.FirstChild.Name, node.LastChild.Name))
                        {
                            if (string.Equals(node.FirstChild.Name, getName))
                            {
                                // Response.Write(node.FirstChild.Name + "-" + getName + "현재노드와 검색노드는 다르지만 노드의첫번째 와 마지막 이 같아서 자식노드와 조회노드가 같다.");
                                //  Response.Write("<br>");
                                this._tagName = node.FirstChild.InnerText;
                            }
                            else
                            {
                                ///Response.Write("aaaaaaaaaaaaaaaaa<br>");
                                this._tagName = GetXmlTagName(node.ChildNodes, getName);

                            }
                        }
                    }

                }
            }
        }


        if(string.Equals(getName,_tagName))
            _tagName="";

        return this._tagName;
    }

    public XmlDocument Compassion_Common()
    {
        XmlDocument Doc = new XmlDocument();
        XmlDeclaration newDec = Doc.CreateXmlDeclaration("1.0", "utf-8", null);
        Doc.AppendChild(newDec);
        XmlElement Worklist = Doc.CreateElement("Compassion");
        Doc.AppendChild(Worklist);

        return Doc;
    }

    public XmlDocument XmlAddNode(XmlDocument Doc, string parTagName, string subTagName, string tagValue)
    {
        XmlElement Root = Doc.DocumentElement;
        bool ck = true;
        XmlNodeList AddNode = Root.ChildNodes;
        if (AddNode.Count > 0)
        {
            foreach (XmlNode node in AddNode)
            {
                if (string.Equals(node.Name, parTagName))
                {
                    ck = false;
                    XmlElement xmlRequest = Doc.CreateElement(subTagName);   // 태그명
                    XmlCDataSection cdata = Doc.CreateCDataSection(tagValue);  // CData  value
                    xmlRequest.AppendChild(cdata);
                    node.AppendChild(xmlRequest);
                }
            }

            if (ck)
            {
                // 부모태그가 같은게 없다면 추가
                XmlElement xmlCommon2 = Doc.CreateElement(parTagName);   // 부모 태그명
                Root.AppendChild(xmlCommon2);

                XmlElement xmlRequest = Doc.CreateElement(subTagName);   // 태그명
                XmlCDataSection cdata = Doc.CreateCDataSection(tagValue);  // CData  value
                xmlRequest.AppendChild(cdata);
                xmlCommon2.AppendChild(xmlRequest);
            }

        }
        else
        {
            XmlElement xmlCommon = Doc.CreateElement(parTagName);   // 부모 태그명
            Root.AppendChild(xmlCommon);

            XmlElement xmlRequest = Doc.CreateElement(subTagName);   // 태그명
            XmlCDataSection cdata = Doc.CreateCDataSection(tagValue);  // CData  value
            xmlRequest.AppendChild(cdata);
            xmlCommon.AppendChild(xmlRequest);

        }
        return Doc;
    }

    public XmlElement XmlAddElement(XmlDocument Doc, string TagName, string TagVal)
    {
        XmlElement XmlElement_ = Doc.CreateElement(TagName);
        XmlCDataSection cdata_Code = Doc.CreateCDataSection(TagVal);  // CData  value
        XmlElement_.AppendChild(cdata_Code);
        return XmlElement_;
    }

    public XmlDocument XmlAddNoneElement(XmlDocument Doc, string TagName)
    {
        XmlElement Root = Doc.DocumentElement;
        XmlElement XmlElement_ = Doc.CreateElement(TagName);
       // XmlCDataSection cdata_Code = Doc.CreateCDataSection(TagVal);  // CData  value
        //XmlElement_.AppendChild(cdata_Code);
        Root.AppendChild(XmlElement_);
        return Doc;
    }

    public XmlDocument XmlAddListItem(XmlDocument Doc, string tagName, XmlElement PostItem)
    {
        XmlNode Root = Doc.DocumentElement;
        XmlNodeList AddNode = Root.ChildNodes;

        foreach (XmlNode iNode in AddNode)
        {
            if (string.Equals(iNode.Name, tagName))
            {
                iNode.AppendChild(PostItem);
            }
        }
        return Doc;
    }




    public DataTable pageData(DataTable dt, int page, int pl)
    {

        //page = 현재 페이지 번호
        //pl = 페이지범위   예:10 ,20, 50 ...

        DataTable ReDT = dt.Copy();
        ReDT.Clear();
        int startINdex = (page - 1) * pl;

        int endINdex = page * pl;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (i >= startINdex && i < endINdex)
            {
                ReDT.Rows.Add(dt.Rows[i].ItemArray);
            }
        }
        return ReDT;
    }

    public class SendSMS
    {
        public SendSMS()
        {

        }

        public static bool SMSSend(string sSMS, string sSponsorName, string sContents)
        {
            string sjs = string.Empty;
			CommonLib.WWWService.ServiceSoap _wwwService = new CommonLib.WWWService.ServiceSoapClient();

			//SMS발송내용
			string[] strSMSContent = new string[4];
            strSMSContent[0] = sSMS; //수신자 전화번호
            strSMSContent[1] = sSponsorName.ToString().Trim();//수신자 이름
            strSMSContent[2] = ConfigurationManager.AppSettings["SMSSendNo"].ToString().Trim(); ;//발신자 전화번호
            strSMSContent[3] = sContents;

            //발송   
            string sResult = "";
            try
            {
                sResult = _wwwService.SendSMS(strSMSContent);
            }
            catch (Exception ex)
            {
                return false;
            }
            if (sResult.Substring(0, 2) != "OK")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 아이디,비번찾기시 사용할 SMS전송    
        /// </summary>
        /// <param name="sMobile"></param>
        /// <param name="sSponsorName"></param>
        /// <param name="sRandom"></param>   
        /// <returns></returns>
        public static string SMSInfoSearch(string sMobile, string sSponsorName, string sRandom)
        {
			CommonLib.WWWService.ServiceSoap _wwwService = new CommonLib.WWWService.ServiceSoapClient();
			string sRendom = sRandom;
            string[] strSMSContent = new string[4];
            //SMS발송내용
            string[] sSMSContent = new string[4];
            sSMSContent[0] = sMobile; //수신자 전화번호
            sSMSContent[1] = sSponsorName.ToString().Trim();//수신자 이름
            //sSMSContent[2] = "0236683477";//발신자 전화번호
            sSMSContent[2] = ConfigurationManager.AppSettings["SMSSendNo"].ToString().Trim(); ;//발신자 전화번호
            sSMSContent[3] = "[한국컴패션 인증번호] " + sRandom;

            string sResult = "";

            try
            {

                sResult = _wwwService.SendSMS(sSMSContent);
				
					ErrorLog.Write(HttpContext.Current, 0, sResult	);

			}
            catch (Exception ex)
            {
				ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
                sRendom = "30";
                return sRendom;
            }

            if (sResult.Substring(0, 2) != "OK")
            {
                sRendom = "30";
            }

            return sRendom;
        }

        /// <summary>
        /// 아이디,비번찾기시 사용할 SMS전송    
        /// 테스트환경에서도 실제 발송
        /// SMSInfoSearch()와 동일로직이지만 www가 아닌, 운영의 ums를 바로 호출함
        /// </summary>
        /// <param name="sMobile"></param>
        /// <param name="sSponsorName"></param>
        /// <param name="sRandom"></param>   
        /// <returns></returns>
        public static string SMSSend_DEV(string sMobile, string sSponsorName, string sRandom)
        {
            CommonLib.UMSService.ServiceSoap umsService = new CommonLib.UMSService.ServiceSoapClient();
            
            string sRendom = sRandom;
            string[] strSMSContent = new string[4];
            //SMS발송내용
            string[] sSMSContent = new string[4];
            sSMSContent[0] = sMobile; //수신자 전화번호
            //sSMSContent[0] = "01047562174"; //수신자 전화번호
            sSMSContent[1] = sSponsorName.ToString().Trim();//수신자 이름
            //sSMSContent[2] = "0236683477";//발신자 전화번호
            sSMSContent[2] = ConfigurationManager.AppSettings["SMSSendNo"].ToString().Trim(); ;//발신자 전화번호
            sSMSContent[3] = "[한국컴패션 인증번호] " + sRandom;

            string sResult = "";

            try
            {

                sResult = umsService.SendSMS(sSMSContent);

                ErrorLog.Write(HttpContext.Current, 0, sResult);

            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
                sRendom = "30";
                return sRendom;
            }

            if (sResult.Substring(0, 2) != "OK")
            {
                sRendom = "30";
            }

            return sRendom;
        }
    }


    public byte[] imageToByteArray(System.Drawing.Image imageIn)
    {
        MemoryStream ms = new MemoryStream();
        imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
        return ms.ToArray();
    }

    private byte[] StrToByteArray(string sStr)
    {
        System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
        return encoding.GetBytes(sStr);
    }

    public byte[] GetBytes(string path)
    {
        FileStream fs = null;
        BinaryReader br = null;


        byte[] buffer = null;
        fs = new FileStream(path, FileMode.Open, FileAccess.Read);
        br = new BinaryReader(fs);

        long numBytes = new FileInfo(path).Length;
        buffer = br.ReadBytes((int)numBytes);

        br.Close();
        fs.Close();

        return buffer;


    }


    public string getParam(string param, string keyName)
    {
        string paramValue = string.Empty;

        string[] Param1 = param.Split('|');

        for (int i = 0; i < Param1.Length; i++)
        {
            string[] Param2 = Param1[i].Split('=');
            for (int j = 0; j < Param2.Length; j++)
            {
                if (Param2[0] == keyName)
                {
                    paramValue = Param2[1];
                }
            }
        }
        return paramValue;
    }

    public string remove_html_tag(string html_str)
    {
        return Regex.Replace(html_str, @"[<][a-z|A-Z|/](.|\n)*?[>]", "");
    }

    // 남은 시간 구하기
    public string GetTimeMinute(DateTime dt)
    {
        //DateTime enddate = Convert.ToDateTime("2012-08-22 14:40:00.000");
        TimeSpan ts = dt - DateTime.Now;

        string reVal = string.Empty;
        string stringHour = "0";
        string stringMinute = "0";
        int intHour = 0;

        if (ts.Days > 0)
        {
            if (ts.Hours > 0)
            {
                intHour = (24 * ts.Days) + ts.Hours;
            }
            else
            {
                intHour = 24 * ts.Days;
            }
        }
        else
        {
            intHour = ts.Hours;
        }

        if (intHour > 0)
            stringHour = Convert.ToString(intHour);
        else
            stringHour = "0";


        if (ts.Minutes > 0)
            stringMinute = ts.Minutes.ToString();
        else
            stringMinute = "0";


        reVal = stringHour + "_" + stringMinute;
        return reVal;

    }
}