﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Net.Sockets;
using System.Text;
using XC3_COMLib;
using Newtonsoft.Json.Linq;


// ARS 전화녹취 기능
public partial class CMSARS {
	
	public CMSARS()
	{
	}

	// 전화 녹취 요청
	public Response Request(string memberId , string phone , string bankCode , string bankName , string bankAccount , string owner , string birthYMD) {

		Dictionary<string, object> data = new Dictionary<string, object>();
		data.Add("agreementAgentId", DateTime.Now.ToString("yyyyMMddhhmmss"));
		data.Add("memberId", memberId);
		data.Add("phone", phone);
		data.Add("callMode", "Outbound");
		data.Add("paymentKind", "CMS");
		data.Add("paymentCompany", new Dictionary<string, string>() { { "code", bankCode }, { "name", bankName } });
		data.Add("paymentNumber", bankAccount);
		data.Add("payerName", owner);
		data.Add("payerNumber", birthYMD);
		string postData = data.ToJson();

		using(WebClient wc = new WebClient()) {
			wc.Headers.Add("Authorization", string.Format("VAN {0}:{1} ", ConfigurationManager.AppSettings["tbSwKey"], ConfigurationManager.AppSettings["tbCustKey"]));
			//wc.Headers.Add("Authorization", string.Format("VAN {0}:{1}", "4LjFflzr6z4YSknp", "BT2z4D5DUm7cE5tl"));
			wc.Headers.Add("Content-Type", "application/json; charset=UTF-8");

			byte[] response = null;
			
			if (ConfigurationManager.AppSettings["stage"] == "dev") {
				response = wc.UploadData(string.Format("https://add.efnc.co.kr:1443/v1/custs/{0}/agreement-agents", ConfigurationManager.AppSettings["tbComId"]), "POST", Encoding.UTF8.GetBytes(postData));
			} else {
				response = wc.UploadData(string.Format("https://api.efnc.co.kr/v1/custs/{0}/agreement-agents", ConfigurationManager.AppSettings["tbComId"]), "POST", Encoding.UTF8.GetBytes(postData));
			}
			
			//response = wc.UploadData(string.Format("https://api.efnc.co.kr/v1/custs/{0}/agreement-agents", ConfigurationManager.AppSettings["tbComId"]), "POST", Encoding.UTF8.GetBytes(postData));

			var json = Encoding.UTF8.GetString(response);

			JToken token = JObject.Parse(json);
			return token.SelectToken("agreementAgent").ToObject<Response>();
		
		}
	}

	// 녹취 결과 조회
	// result.flag =  Y or N이 나올때까지 요청(timeout 필요)
	public Response Verify(string agreementAgentId ) {
		using(WebClient wc = new WebClient()) {
			wc.Headers.Add("Authorization", string.Format("VAN {0}:{1} ", ConfigurationManager.AppSettings["tbSwKey"], ConfigurationManager.AppSettings["tbCustKey"]));
			wc.Headers.Add("Content-Type", "application/json; charset=UTF-8");

			string json = null;
			if(ConfigurationManager.AppSettings["stage"] == "dev") {
				json = wc.DownloadString(string.Format("https://add.efnc.co.kr:1443/v1/custs/{0}/agreement-agents/{1}", ConfigurationManager.AppSettings["tbComId"], agreementAgentId));

				JToken token = JObject.Parse(json);
				var result = token.SelectToken("agreementAgent").ToObject<Response>();

				// 테스트환경에서는 항상 대기(미완료)상태로 오기때문에 강제로 완료처리해줌
				if (new Random().Next(100) % 3 == 0) {
					result.result.flag = "Y";
				}
				return result;

			} else {
				json = wc.DownloadString(string.Format("https://api.efnc.co.kr/v1/custs/{0}/agreement-agents/{1}", ConfigurationManager.AppSettings["tbComId"], agreementAgentId));
				JToken token = JObject.Parse(json);
				return token.SelectToken("agreementAgent").ToObject<Response>();
			}
			
			
			
		}

	}

	public class Response {

		public string status;
		public string agreementAgentId;
		public string memberId;
		public string phone;
		public string callMode;
		public string paymentKind;
		public paymentCompanyEntity paymentCompany;
		public string paymentNumber;
		public string payerName;
		public string requestDate;
		public string deadlineDate;
		public string completionDateTime;
		public resultEntity result;
		public List<linkEntity> links;

		public class paymentCompanyEntity {
			public string code;
			public string name;
		}

		public class resultEntity {
			public string flag;	// Y,N
			public string code;	// 4자리 결과코드
			public string message;
			
		}

		public class linkEntity {
			public string rel;
			public string href;
		}
	}
	
}