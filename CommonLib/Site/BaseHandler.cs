﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using NLog;
using System.Web.SessionState;


public abstract class BaseHandler : IHttpHandler, IRequiresSessionState {

	public virtual bool CheckReferrer {
		get {
			return true;
		}
	}

	public void ProcessRequest(HttpContext context) {

		if (this.CheckReferrer) {
			if (context.Request.UrlReferrer == null || context.Request.UrlReferrer.Host.Substring(context.Request.UrlReferrer.Host.IndexOf(".") + 1) != context.Request.Url.Host.Substring(context.Request.Url.Host.IndexOf(".") + 1)) {
			//if(context.Request.UrlReferrer == null ) {
				var stage = ConfigurationManager.AppSettings["stage"];
				if (stage == "dev") {
					
				} else {
					context.Response.End();
				}
				
			}
		}

		this.OnRequest(context);
	}

	public virtual void OnRequest(HttpContext context){


	}

	protected void SetCache(HttpContext context , TimeSpan refresh) {
		HttpCachePolicy c = context.Response.Cache;
		c.SetCacheability(HttpCacheability.Public);
		c.SetExpires(DateTime.Now.Add(refresh));
		c.SetMaxAge(refresh);
		c.SetCacheability(HttpCacheability.Server);
		context.Response.CacheControl = HttpCacheability.Public.ToString();
		c.SetValidUntilExpires(true);
	}

	public virtual bool IsReusable {
		get {
			return false;
		}
	}

}
