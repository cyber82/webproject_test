﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CommonLib;

public class AuthLibDataContext : AuthDataContext
{

	public AuthLibDataContext()
		: base(ConfigurationManager.ConnectionStrings["auth_connectionString"].ConnectionString) {
	}

	public AuthLibDataContext(string connectionString)
		: base(connectionString) {
	}

}