﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CommonLib;

public class StoreLibDataContext : StoreDataContext
{

	public StoreLibDataContext()
		: base(ConfigurationManager.ConnectionStrings["store_connectionString"].ConnectionString) {
	}

	public StoreLibDataContext( string connectionString)
		: base(connectionString) {
	}

}