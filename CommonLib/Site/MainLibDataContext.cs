﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CommonLib;

public class MainLibDataContext : MainDataContext
{

	public MainLibDataContext()
		: base(ConfigurationManager.ConnectionStrings["user_connectionString"].ConnectionString) {
	}

	public MainLibDataContext( string connectionString )
		: base(connectionString) {
	}

}