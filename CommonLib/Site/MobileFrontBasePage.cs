﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using NLog;
using System.Text;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;

public abstract class MobileFrontBasePage : FrontBasePage {
	
	public override string GetLoginPage() {
		return ConfigurationManager.AppSettings["domain_auth"] + "/m/login/?r=" + HttpUtility.UrlEncode(Request.Url.AbsoluteUri.Replace("default.aspx", "").Replace(".aspx", ""));

	}
	
}

