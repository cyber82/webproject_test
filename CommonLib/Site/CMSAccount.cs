﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Net.Sockets;
using System.Text;
using XC3_COMLib;
using CommonLib;

// 효성CMS 사용해서 계좌번호가 유효한지 검증한다.
public partial class CMSAccount {

	byte MSGTYPE_SESSION_KEY = 0x03;
	byte MSGTYPE_CIPHER = 0x04;

	byte[] type = new byte[1];
	int TIMEOUT = 5000;

	XC3 xc3 = null;
	TcpClient tc = null;
	NetworkStream netStream = null;
	BinaryWriter writer = null;
	BinaryReader reader = null;

	public CMSAccount()
	{
	}

	bool Connect() {

		var tbConfPath = ConfigurationManager.AppSettings["tbConfPath"];
		var tbIp = ConfigurationManager.AppSettings["tbIp"];
		var tbPort = ConfigurationManager.AppSettings["tbPort"];

		xc3 = new XC3_COMLib.XC3();
		int ret = xc3.init("", tbConfPath, ModeTypes.XC_SMODE_CLIENT);

		if(ret < 0) { xc3 = null; }

		try {
			tc = new TcpClient(tbIp, int.Parse(tbPort));
			
			tc.ReceiveBufferSize = 9999999;
			
			netStream = tc.GetStream();
			writer = new BinaryWriter(netStream);
			reader = new BinaryReader(netStream);
			
			this.Handshake();

			return true;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			return false;
		}
	}

	void Handshake() {

		string strToken = xc3.keyInit();
		type[0] = MSGTYPE_SESSION_KEY;
		byte[] byteToken = strHexToByte(strToken);
		this.xmTokenWrite(byteToken);

		byte[] byteServerToken = this.xmTokenRead();
		string strServerToken = BitConverter.ToString(byteServerToken).Replace("-", string.Empty).ToLower();
		byte[] finalRet = xc3.keyFinal(strServerToken, " ", 2);
	}

	void xmTokenWrite( byte[] data ) {
		byte[] header = new byte[3];
		header[0] = (byte)type[0];
		header[1] = (byte)((byte)(data.Length >> 8) & 0x000000FF);
		header[2] = (byte)((byte)data.Length & 0x000000FF);
		writer.Write(header[0]);
		writer.Write(header[1]);
		writer.Write(header[2]);
		writer.Write(data, 0, data.Length);
		writer.Flush();

	}

	byte[] xmTokenRead() {
		tc.ReceiveTimeout = TIMEOUT;
		byte[] charType = new byte[1];
		int res = reader.Read(charType, 0, 1);

		byte[] byteLength = new byte[2];
		reader.Read(byteLength, 0, 2);
		int nDataLength = (byteLength[0] << 8);
		nDataLength += byteLength[1];

		byte[] recv = new byte[nDataLength];
		recv = reader.ReadBytes(nDataLength);
		return recv;
	}

	byte[] strHexToByte( string data ) {
		byte[] byteData = new byte[data.Length / 2];
		int byteDataIdx = 0;
		for(int i = 0; i < data.Length; i = i + 2) {
			String strHex = data[i].ToString() + data[i + 1].ToString();
			byteData[byteDataIdx] = byte.Parse(strHex, System.Globalization.NumberStyles.HexNumber);
			byteDataIdx++;
		}
		return byteData;
	}

	public class VerifyResult {
		public string code;
		public bool success;
		public string msg;
		public string bankCode;
		public string bankAccount;
		public string name;
		public string birthYMD;
	}

	public JsonWriter Verify(string bankCode , string bankAccount , string birthYMD /*yymmdd*/ , string name) {

		JsonWriter result = new JsonWriter() { success = false };
		if(!this.Connect()) {
			result.message = "계좌검증 서버 연결에 실패했습니다.";
			return result;
		}
		
		try {

			var seq = "";
            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var entity = dao.sp_cms_seq_get_f().First();
                Object[] op1 = new Object[] { };
                Object[] op2 = new Object[] { };
                var list = www6.selectSP("sp_cms_seq_get_f", op1, op2).DataTableToList<sp_cms_seq_get_fResult>();
                var entity = list.First();

                if (entity.result == "Y")
                {
                    seq = entity.code;
                }
                else
                {
                    result.message = "계좌조회중 에러가 발생했습니다.(seq)";
                    return result;
                }
            }

			var tbComId = ConfigurationManager.AppSettings["tbComId"];
			var tbComPw = ConfigurationManager.AppSettings["tbComPw"];
			var tbProgramId = ConfigurationManager.AppSettings["tbProgramId"];
			var tbProgramPw = ConfigurationManager.AppSettings["tbProgramPw"];

			string msg = ""
			+ tbComId.PadRight(10, ' ')
			+ tbComPw.PadRight(10, ' ')
			+ tbProgramId.PadRight(10, ' ')
			+ tbProgramPw.PadRight(10, ' ')
			+ DateTime.Now.ToString("yyMMdd")
			+ "1000"
			+ "300"
			+ seq
			+ "".PadRight(6, ' ')
			+ "".PadRight(6, ' ')
			+ "".PadRight(1, ' ')
			+ "".PadRight(4, ' ')
			+ "".PadRight(24, ' ')
			// 공통부 끝
			+ bankCode  // 은행코드
			+ bankAccount.PadRight(15, ' ')    // 계좌번호
			+ birthYMD.PadRight(13, ' ') // 생년월일 or 사업자번호
			+ name.PadRight(20 - UTF8Encoding.Default.GetByteCount(name) / 2, ' ')

			+ "".PadRight(20, ' ')
			+ "".PadRight(15, ' ')
			+ "".PadRight(50, ' ')
			+ "".PadRight(45, ' ')
			+ "".PadRight(40, ' ')

			;
			
		//	ErrorLog.Write(HttpContext.Current, 0, "req > " + msg);

			string encMsg = xc3.encrypt(msg);
			type[0] = MSGTYPE_CIPHER;
			byte[] byteMsg = strHexToByte(encMsg);
			this.xmTokenWrite(byteMsg);

	//		ErrorLog.Write(HttpContext.Current, 0, "strEnc>" + encMsg.Length.ToString() + " , strMsg>" + msg.Length.ToString() + " , 바이트크기 : " + byteMsg.Length.ToString());

			// 응답
			byte[] byteMsgRes = this.xmTokenRead();
			string msgRes = xc3.decrypt(BitConverter.ToString(byteMsgRes).Replace("-", string.Empty).ToLower());

			//		ErrorLog.Write(HttpContext.Current, 0, "res > " + msgRes);

			var verityResult = new VerifyResult();
			var resultCode = msgRes.Substring(71, 1);
			var errorCode = msgRes.Substring(72, 4);
			verityResult.success = resultCode == "Y";
			verityResult.code = errorCode;
			verityResult.bankCode = bankCode;
			verityResult.bankAccount = bankAccount;
			verityResult.birthYMD = birthYMD;
			verityResult.name = name;

			if (resultCode == "N") {
				verityResult.msg = "계좌확인에 실패했습니다. 에러코드(" + errorCode + ")";
			}
			result.success = true;
			result.data = verityResult;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "계좌 확인도중 에러가 발생했습니다.";
			return result;
		}
	}
}