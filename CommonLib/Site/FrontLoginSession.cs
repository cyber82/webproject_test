﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class FrontLoginSession
{

    const int session_expire_minute = 1;
    public class LoginUser
    {

        public string SponsorID = "";               // 회원 식별자
        public string ConId = "";                   // 미국후원자번호
        public string UserId;                   // 회원 아이디
        public string UserName;                 // 회원명
        public string GroupNo;                  // 사업자등록번호(기업,단체여부)      
        public string Jumin = "";                    // 주민번호
        public string GenderCode = "";               // 성별 , M , F , C

        //public string VOC = "";						// VOC 값 , VOC , FOC , YVOC , WEB
        public string Mate = "";                        // Mate 종류 , 구분자
        public string UserClass;                // 회원종류 (14세미만,14세이상,외국인국내거주,기업,해외회원)  
        public string TranslationFlag = "";          // 편지번역여부Y/N
        public string LocationType;             // 위치
        public string CertifyDate;              // 인증날짜
        public int CommitCount = 0;                 // 결연건수
        public int ApplyCount = 0;                  // 일반후원건수
        public string Birth;                    // 생년월일
        public string ManageType = "";               // 주소관리 방식?
        public string SponsorType = "";               // 스폰서타입

        // 20171130 김은지
        // 백오피스 기능 관련 관리자 로그인 체크 변수 추가
        public bool AdminLoginCheck = false; // Admin용 로그인 체크
        //public string AdminLoginCheck = ""; // Admin용 로그인 체크

        // NK정보
        public string ZipCode = "";
        public string Addr1 = "";
        public string Addr2 = "";
        public string Phone = "";
        public string Mobile = "";
        public string Email = "";
        public string NKID = "";

        public string ReligionType = "";
        public string ChannelType = "";
        public string AgreeEmail = "";
        public string UserDate = "";
        public string RegisterDate = "";
        public string LastPaymentDate = "";
        
    }

    public FrontLoginSession()
    {
    }

    public static string GetDomain(HttpContext context)
    {
        //return context.Request.Url.Host;

        // 1차 도메인만 구한다.
        var host = context.Request.Url.Host;
        return host.Substring(host.IndexOf(".") + 1);

    }

    public static bool HasCookie(HttpContext context)
    {
        //return (context.Session[ENDUSER_COOKIE_NAME] != null);
        return context.Request.Cookies[Constants.ENDUSER_COOKIE_NAME] != null;
    }


    public static LoginUser GetCookie(HttpContext context)
    {
        /*
		var entity = JsonConvert.DeserializeObject<LoginUser>(context.Session[ENDUSER_COOKIE_NAME].ToString().Decrypt());
		return entity;
		*/

        HttpCookie cookie = context.Request.Cookies[Constants.ENDUSER_COOKIE_NAME];

        var jsonData = cookie.Value.ToString().Decrypt();
        var entity = JsonConvert.DeserializeObject<LoginUser>(jsonData);
        return entity;

    }


    public static void ClearCookie(HttpContext context)
    {
        /*
		context.Session[ENDUSER_COOKIE_NAME] = null;
		context.Session.Abandon();
		*/

        if (!HasCookie(context))
            return;

        HttpCookie cookie = context.Response.Cookies[Constants.ENDUSER_COOKIE_NAME];
        cookie.Domain = FrontLoginSession.GetDomain(context);
        cookie.Expires = DateTime.Now.AddYears(-1);
        context.Response.Cookies.Set(cookie);

    }


    public static void SetCookie(HttpContext context, LoginUser entity)
    {

        HttpCookie cookie = context.Request.Cookies[Constants.AUTOLOGIN_COOKIE_NAME];
        var autoLogin = cookie != null && cookie.Value == "Y";
        FrontLoginSession.SetCookie(context, entity, autoLogin);
    }

    public static void SetCookie(HttpContext context, LoginUser entity, bool autoLogin)
    {
        var encData = JsonWriter.toLowerCaseJson(entity).Encrypt();
        /*
		context.Session[ENDUSER_COOKIE_NAME] = encData;
		context.Session.Timeout = 1;
		*/

        {
            HttpCookie cookie = new HttpCookie(Constants.AUTOLOGIN_COOKIE_NAME);
            cookie.Path = "/";
            cookie.Expires = DateTime.Now.AddYears(100);
            cookie.Domain = FrontLoginSession.GetDomain(context);
            if (autoLogin)
            {
                cookie.Value = "Y";
            }
            else
            {
                cookie.Value = "N";
            }
            context.Response.Cookies.Set(cookie);
        }

        {
            context.Session[Constants.ENDUSER_COOKIE_NAME] = JsonWriter.toLowerCaseJson(entity).ToObject<FrontLoginSession.LoginUser>();

            HttpCookie cookie = new HttpCookie(Constants.ENDUSER_COOKIE_NAME);

            cookie.Value = encData;
            cookie.Path = "/";
            if (autoLogin)
                cookie.Expires = DateTime.Now.AddYears(100);

            cookie.Domain = FrontLoginSession.GetDomain(context);
            context.Response.Cookies.Set(cookie);
        }

    }




}