﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Roamer.Config;


public class UserInfo
{
    private HttpContext _httpContext;
    FrontLoginSession.LoginUser user;

    public UserInfo()
    {
        _httpContext = HttpContext.Current;


        if (_httpContext.Request.Cookies[Constants.ENDUSER_COOKIE_NAME] == null)
        {
            user = new FrontLoginSession.LoginUser();
        }
        else
        {

            var hostCookieName = "cps." + _httpContext.Request.Url.Host;

            if (_httpContext.Session[Constants.ENDUSER_COOKIE_NAME] == null || _httpContext.Request.Cookies[hostCookieName] == null ||
                _httpContext.Request.Cookies[hostCookieName].Value != _httpContext.Request.Cookies[Constants.ENDUSER_COOKIE_NAME].Value)
            {

                // 모든 세션 삭제
                //	HttpContext.Current.Session.Clear();

                user = _httpContext.Request.Cookies[Constants.ENDUSER_COOKIE_NAME].Value.Decrypt().ToObject<FrontLoginSession.LoginUser>();
                _httpContext.Session[Constants.ENDUSER_COOKIE_NAME] = user;

                // 편지갯수정보
                if (HttpContext.Current.Request.Cookies[Constants.LETTERCOUNT_COOKIE_NAME] != null)
                {
                    HttpCookie cookie = HttpContext.Current.Request.Cookies[Constants.LETTERCOUNT_COOKIE_NAME];
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }

                {
                    HttpCookie cookie = new HttpCookie(hostCookieName);
                    cookie.Path = "/";
                    cookie.Domain = _httpContext.Request.Url.Host;
                    cookie.Value = _httpContext.Request.Cookies[Constants.ENDUSER_COOKIE_NAME].Value;
                    _httpContext.Response.Cookies.Set(cookie);
                }

                //ErrorLog.Write(HttpContext.Current, 0, "새로운 세션생성 조회 " + user.UserId);

            }
            else
            {
                // 암복호화 비용손실을 막기위해 세션사용
                user = (FrontLoginSession.LoginUser)_httpContext.Session[Constants.ENDUSER_COOKIE_NAME];

            }

        }



    }

    public static bool IsLogin
    {
        get
        {
            if (HttpContext.Current.Request.Cookies[Constants.ENDUSER_COOKIE_NAME] == null)
                return false;
            else
                return true;
        }
    }

    public void UpdateCookies()
    {
        _httpContext.Request.Cookies.Set(_httpContext.Response.Cookies[Constants.ENDUSER_COOKIE_NAME]);
    }

    public void ClearCookie()
    {
        _httpContext.Session[Constants.ENDUSER_COOKIE_NAME] = null;

        if (HttpContext.Current.Request.Cookies[Constants.ENDUSER_COOKIE_NAME] != null)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[Constants.ENDUSER_COOKIE_NAME];
            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
        // 편지갯수정보
        if (HttpContext.Current.Request.Cookies[Constants.LETTERCOUNT_COOKIE_NAME] != null)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[Constants.LETTERCOUNT_COOKIE_NAME];
            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

    }

    public string Url
    {
        get
        {
            //return Cryptography.Decrypt(_httpContext.Request.Cookies[Constants.ENDUSER_COOKIE_NAME]["Url"]);
            return "사용안함";
        }
    }

    public string UserId
    {
        get { return user.UserId; }
    }

    public string GenderCode
    {
        get
        {
            return user.GenderCode;
        }
    }

    public string LocationType
    {
        get
        {
            return user.LocationType;
        }

    }

    public string Birth
    {
        get
        {
            return user.Birth;
        }

    }

    public string UserName
    {
        get
        {
            return user.UserName;
        }

    }

    public string SponsorID
    {
        get
        {
            return user.SponsorID;
        }

    }

    public string sTempSponsorId
    {
        get
        {
            return "사용안함";
        }

    }


    public string ManageType
    {
        get
        {
            return user.ManageType;
        }

    }

    public string Mate
    {
        get
        {
            return user.Mate;
        }

    }
    public string Jumin
    {
        get
        {
            return user.Jumin;
        }

    }
    public string CertifyDate
    {
        get
        {
            return user.CertifyDate;
        }

    }
    public string UserClass
    {
        get
        {
            return user.UserClass;
        }

    }
    public string ApplyCount
    {
        get
        {
            return user.ApplyCount.ToString();
        }

    }
    public string CommitCount
    {
        get
        {
            return user.CommitCount.ToString();
        }

    }
    public string GroupNo
    {
        get
        {
            return user.GroupNo;
        }

    }

    public string ConId
    {
        get
        {
            return user.ConId;
        }

    }

    public string TranslationFlag
    {
        get
        {
            return user.TranslationFlag;
        }

    }

    public string Email
    {
        get
        {
            return user.Email;
        }

    }

    public string Phone
    {
        get
        {
            return user.Phone;
        }

    }

    public string Mobile
    {
        get
        {
            return user.Mobile;
        }

    }

    public string SponsorType
    {
        get
        {
            return user.SponsorType;
        }

    }

    public string ReligionType
    {
        get
        {
            return user.ReligionType;
        }
    }

    public string ChannelType
    {
        get
        {
            return user.ChannelType;
        }
    }

    public string AgreeEmail
    {
        get
        {
            return user.AgreeEmail;
        }
    }

    public string UserDate
    {
        get
        {
            return user.UserDate;
        }
    }

    public string RegisterDate
    {
        get
        {
            return user.RegisterDate;
        }
    }

    public string LastPaymentDate
    {
        get
        {
            return user.LastPaymentDate;
        }
    }
    public bool AdminLoginCheck
    {
        get
        {
            return user.AdminLoginCheck;
        }

    }
}
