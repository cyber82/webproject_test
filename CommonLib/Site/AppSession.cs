﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class AppSession {
	
	public class Entity {
		public string device;
		public string version;
		public string build;
	}

	public AppSession() {
	}
	
	public static string GetDomain(HttpContext context) {
		//	return context.Request.Url.Host;

		// 1차 도메인만 구한다.
		var host = context.Request.Url.Host;
		return host.Substring(host.IndexOf(".") + 1);
	}

	public static bool HasCookie(HttpContext context) {
		return context.Request.Cookies[Constants.APP_COOKIE_NAME] != null;
	}
	

	public static void ClearCookie(HttpContext context) {
		
		if (!HasCookie(context))
			return;

		HttpCookie cookie = context.Response.Cookies[Constants.APP_COOKIE_NAME];
		cookie.Domain = AppSession.GetDomain(context);
		cookie.Expires = DateTime.Now.AddYears(-1);
		context.Response.Cookies.Set(cookie);

	}


	public static Entity GetCookie( HttpContext context ) {
		
		HttpCookie cookie = context.Request.Cookies[Constants.APP_COOKIE_NAME];

		var jsonData = cookie.Value.ToString();
		var entity = JsonConvert.DeserializeObject<Entity>(jsonData);
		return entity;

	}

	public static void SetCookie(HttpContext context, Entity entity ) {
		var encData = JsonWriter.toLowerCaseJson(entity);
		HttpCookie cookie = new HttpCookie(Constants.APP_COOKIE_NAME);

		cookie.Value = encData;
		cookie.Path = "/";
		cookie.Domain = AppSession.GetDomain(context);
		cookie.Expires = DateTime.Now.AddYears(1);
		context.Response.Cookies.Set(cookie);



	}



}