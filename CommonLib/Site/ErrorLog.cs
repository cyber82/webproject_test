﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;

public class ErrorLog
{
	public ErrorLog()
	{
	}


	public static void Write(HttpContext context, int code , string comment) {

		LogEventInfo evt = new LogEventInfo() {
			Level = LogLevel.Error, TimeStamp = DateTime.Now
		};

		evt.Properties["ls_code"] = code.ToString();
		evt.Properties["ls_url"] = context.Request.RawUrl;
        evt.Properties["ls_msg"] = comment.Replace(@"'", "");
        //Logger logger = LogManager.GetLogger("site.error");
        //logger.Log(evt);
        
        string sqlStr = string.Format(@"INSERT INTO log_site_error( ls_code , ls_url , ls_msg) 
                                                             VALUES ('{0}' , '{1}' , '{2}')"
                                     , code.ToString(),context.Request.RawUrl, comment.Replace(@"'", ""));
        www6.cud(sqlStr);
		

	}

}