﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class RecruitSession
{

	public class Entity {
		public string name;
		public string email;
		public int id;
		public int bj_id;
	}

	public RecruitSession()
	{
	}

	public static string GetDomain(HttpContext context) {
		return context.Request.Url.Host;
	}

	public static bool HasCookie(HttpContext context) {
		return context.Request.Cookies[Constants.RECRUIT_USER_COOKIE_NAME] != null;
	}


	public static Entity GetCookie(HttpContext context){
		HttpCookie cookie = context.Request.Cookies[Constants.RECRUIT_USER_COOKIE_NAME];

		var jsonData = cookie["data"].ToString().Decrypt();
		var entity = JsonConvert.DeserializeObject<Entity>(jsonData);
        return entity;
	}
	

	public static void ClearCookie(HttpContext context) {
		if (!HasCookie(context))
			return;

		HttpCookie cookie = context.Response.Cookies[Constants.RECRUIT_USER_COOKIE_NAME];
		cookie.Domain = RecruitSession.GetDomain(context);
		cookie.Expires = DateTime.Now.AddYears(-1);
		context.Response.Cookies.Set(cookie);
	}


	public static void SetCookie(HttpContext context, Entity entity) {
		HttpCookie cookie = new HttpCookie(Constants.RECRUIT_USER_COOKIE_NAME);

        var encData = JsonWriter.toLowerCaseJson(entity).Encrypt();
        cookie.Values["data"] = encData;
		cookie.Path = "/";
		cookie.Domain = RecruitSession.GetDomain(context);
		context.Response.Cookies.Set(cookie);

	}
	

}