﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using CommonLib;

public class StaticData {

	public class Code {
		const string KEY = "KEY_CODE";
		public static List<code> GetList(HttpContext context) {
			return GetList(context, false);
		}

		public static List<code> GetList(HttpContext context, bool reload) {

			List<code> data = new List<code>();

			if (context.Application[KEY] != null) {
				data = (List<code>)context.Application[KEY];
			}
            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //int count = dao.code.Count();
                int count = www6.selectQ<code>().Count;

                if (data.Count < 1 || reload || count != data.Count)
                {
                    var list = www6.selectQ<code>();
                    context.Application[KEY] = (from p in list//dao.code
                                                select p).ToList();
                }
            }
			return (List<code>)context.Application[KEY];
		}
	}

	public class Country {
		const string KEY = "KEY_COUNTRY";
		public static List<country> GetList( HttpContext context ) {
			return GetList(context, false);
		}

		public static List<country> GetList( HttpContext context, bool reload )
        {
			List<country> data = new List<country>();

			if(context.Application[KEY] != null)
            {
				data = (List<country>)context.Application[KEY];
			}

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //int count = dao.code.Count();
                int count = www6.selectQ<code>().Count();

                if (data.Count < 1 || reload || count != data.Count)
                {
                    var list = www6.selectQ<country>();
                    context.Application[KEY] = (from p in list//dao.country
                                                select p).ToList();
                }
            }
			return (List<country>)context.Application[KEY];
		}
	}


}
