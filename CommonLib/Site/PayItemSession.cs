﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CommonLib;

public class PayItemSession {
	
	//const int session_expire_minute = 1;
	public class Entity {

		public enum enumType {
			CSP_DOL , CSP_WEDDING , USER_FUNDING , SPECIAL_FUNDING , CDSP , GIFT_MONEY
		}

		public string TypeName {
			get {
				switch(this.type) {
					default:
						return this.type.ToString();
					case enumType.CDSP:
						return "1:1어린이양육";
					case enumType.CSP_DOL:
						return "첫생일 첫나눔";
					case enumType.CSP_WEDDING:
						return "결혼 첫나눔";
					case enumType.USER_FUNDING:
						return "나눔펀딩";
					case enumType.SPECIAL_FUNDING:
						return "특별한 나눔";
				}
			}
		}

		public enum enumPayMethod {
			card , card_oversea , cms , kakao , payco , phone , jiro , virtualAccount
		}

		public enumType type;         // 
		public string group;         // CDSP , CIV , CSP
		public string codeId;         // PBCIV , DS , AIDS 등
		public string codeName = string.Empty;         // 레이치료비후원금등
		public int amount;          // 금액			
		public int month = 1;          // 결제개월 , 해외카드인경우 적용			
		public string frequency;	// 일시 , 정기
		public string campaignId;   // CDSP 는 기본캠패인으로 처리시 하드코딩됨
		public string childMasterId = string.Empty;     // 아동 키 , CDSP 에서만 사용
        public string childKey;     // [이종진] 아동 키 , CDSP 에서만 사용
        public string childGlobalId;     // [이종진] 아동 키 , CDSP 에서만 사용
        public int relation_key;    // 관계된 테이블의 키 (CSPDOL , CSPWEDDING , USERFUNDING)
		public string sponsorId;   // 후원회원아이디 , 비회원,무기명의 경우 발급된 sponsorId를 저장
		public string sponsorName;   // 후원회원명 , 비회원,무기명의 경우 발급된 sponsorId를 저장

		public string userId;   
		public string commitmentId;   // 결연아이디
		public enumPayMethod payMethod;     // 결제수단

		public string extra;		// 추가정보(정기결제시 , 주민번호,CI,DI 등)

		public bool IsEmptyChildMasterId {
			get {
				return string.IsNullOrEmpty(childMasterId) || childMasterId == /*"90000000000000000"*/ "0000000000";
			}
		}

		public string PayMethodName {
			get {
				switch(payMethod) {
					default: 
						return payMethod.ToString();
					case enumPayMethod.card:
						return "신용카드";
					case enumPayMethod.cms:
						return "계좌이체";
					case enumPayMethod.card_oversea:
						return "신용카드(해외카드)";
					case enumPayMethod.kakao:
						return "카카오페이";
					case enumPayMethod.payco:
						return "페이코";
					case enumPayMethod.phone:
						return "휴대폰";
					case enumPayMethod.jiro:
						return "지로";
					case enumPayMethod.virtualAccount:
						return "가상계좌";
				}
			}
		}
	}

	public PayItemSession()
	{
	}

	public static string GetDomain( HttpContext context ) {
		//return context.Request.Url.Host;

		// 1차 도메인만 구한다.
		var host = context.Request.Url.Host;
		return host.Substring(host.IndexOf(".") + 1);

	}

	public static bool HasCookie(HttpContext context) {
		//return (context.Session[Constants.PAYINFO_COOKIE_NAME] != null);
		return context.Request.Cookies[Constants.PAYINFO_COOKIE_NAME] != null;
	}
	
	public static Entity GetCookie(HttpContext context){
		
		HttpCookie cookie = context.Request.Cookies[Constants.PAYINFO_COOKIE_NAME];

		var jsonData = cookie.Value.ToString().Decrypt();
		var entity = JsonConvert.DeserializeObject<Entity>(jsonData);
        return entity;
		
	}
	
	public static void ClearCookie(HttpContext context) {
		/*
		context.Session[Constants.PAYINFO_COOKIE_NAME] = null;
		context.Session.Abandon();
		*/
		
		if (!HasCookie(context))
			return;

		HttpCookie cookie = context.Response.Cookies[Constants.PAYINFO_COOKIE_NAME];
		cookie.Domain = PayItemSession.GetDomain(context);
		cookie.Expires = DateTime.Now.AddYears(-1);
		context.Response.Cookies.Set(cookie);
		
	}
	
	public static void SetCookie(HttpContext context, Entity entity) {
		var encData = JsonWriter.toLowerCaseJson(entity).Encrypt();
		/*
		context.Session[Constants.PAYINFO_COOKIE_NAME] = encData;
		context.Session.Timeout = 1;
		*/
		PayItemSession.ClearCookie(context);

		HttpCookie cookie = new HttpCookie(Constants.PAYINFO_COOKIE_NAME);

        cookie.Value = encData;
		cookie.Path = "/";
		cookie.Domain = PayItemSession.GetDomain(context);
        
		context.Response.Cookies.Set(cookie);
		
	}
	
	/* 
	결제페이지에 오면 cookie 의 값을 pay_item_session 테이블에 저장하고 이후는 cookie 대신 DB의 데이타로 사용한다.
	결제페이지에서 주문번호를 생성 및 키로 사용
	*/

	public class Store {

		public pay_item_session Create() {
			if(!PayItemSession.HasCookie(HttpContext.Current)) {
				return null;
			}

			var data = PayItemSession.GetCookie(HttpContext.Current);

			return this.Create(data);

		}
		public pay_item_session Create(PayItemSession.Entity data) {

			return this.Create(data.ToJson());
		}

		public pay_item_session Create( string data ) {

           return this.Create(DateTime.Now.ToString("yyyyMMddHHmmssff"), data);
		}

        public pay_item_session Create( string orderId , string data )
        {
            using (MainLibDataContext dao = new MainLibDataContext())
            {
                var entity = new pay_item_session()
                {
                    orderId = orderId,
                    data = data,
                    is_completed = false,
                    regdate = DateTime.Now
                };

                //dao.pay_item_session.InsertOnSubmit(entity);
                www6.insert(entity);
                //dao.SubmitChanges();

                return entity;
            }
        }

        public void Complete(string orderId ) {
            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var entity = dao.pay_item_session.FirstOrDefault(p => p.orderId == orderId);
                var entity = www6.selectQF<pay_item_session>("orderId", orderId);

                if (entity != null)
                {
                    entity.is_completed = true;

                    //dao.SubmitChanges();
                    www6.update(entity);
                }
            }

			PayItemSession.ClearCookie(HttpContext.Current);
		}

		public static string GetNewOrderID() {
			return DateTime.Now.ToString("yyyyMMddHHmmssff"); ;
		}
		
		// orderID의 값이 있으면 새로운 주문번호 생성
		public pay_item_session Update( string orderId, PayItemSession.Entity data , string newOrderId )
        {
            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var entity = dao.pay_item_session.FirstOrDefault(p => p.orderId == orderId);
                var entity = www6.selectQF<pay_item_session>("orderId", orderId);

                if (entity != null)
                {
                    entity.data = data.ToJson();
                    if (!string.IsNullOrEmpty(newOrderId))
                        entity.orderId = newOrderId;

                    //dao.SubmitChanges();
                    www6.update(entity);

                    return entity;
                }

            }
			return this.Create(data);
			//return null;
			
		}

		public pay_item_session Get( string orderId )
        {
            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var entity = dao.pay_item_session.FirstOrDefault(p => p.orderId == orderId);
                var entity = www6.selectQF<pay_item_session>("orderId", orderId);

                return entity;
            }
		}

	}

	
	
}