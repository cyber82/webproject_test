﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;

public partial class Authentication {
	public Authentication()
	{
	}

	public enum enumErrCode {

		Success = 0,
		UnauthorizedHost = 301,			// SSO 사이트에 등록되지 않은경우

		EmptyToken = 401,				// 토큰이 없는경우
		ExpireToken = 402,				// 만료된 토큰
		InvalidIPAddress = 403 ,		// token 발급된 ip와 인증요청한 ip가 다를경우
		InvalidToken = 404 ,            // 디비에 등록되지 않은 토큰
		LogoutToken = 405,				// 로그아웃된 토큰

	}

	public class Result {
		public bool success;
		public enumErrCode errCode;
		public string message;
		public object data;
	}


}