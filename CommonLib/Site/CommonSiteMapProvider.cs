﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonLib {
	public class CommonSiteMapProvider : XmlSiteMapProvider {

		public override SiteMapNode CurrentNode {
			get {
				SiteMapNode currentNode = base.CurrentNode;

				if(currentNode == null) {
					HttpContext context = HttpContext.Current;
					if(context != null) {
						
						if(context.Request.RawUrl.EndsWith("/")) {
							var text = context.Request.Path.Substring(0, context.Request.Path.Length - 1);
							currentNode = FindSiteMapNode(text);
							
							if(currentNode != null && !currentNode.IsAccessibleToUser(context)) {
								currentNode = null;
							}
							

						} else {

							var text = context.Request.Path.Substring(0, context.Request.Path.Length - 1);
							currentNode = FindSiteMapNode(text);

							if(currentNode == null) {
								text = text.Substring(0, text.LastIndexOf("/")+1);
								currentNode = FindSiteMapNode(text);

								if(currentNode != null && !currentNode.IsAccessibleToUser(context)) {
									currentNode = null;
								}
							} else {
								if(currentNode != null && !currentNode.IsAccessibleToUser(context)) {
									currentNode = null;
								}
							}
						}

						
					}
				}
				return currentNode;
			}
		}

	}

}