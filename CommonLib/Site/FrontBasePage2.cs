﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using NLog;
using System.Text;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;

public abstract class FrontBasePage2: Page
{

    public virtual string GetLoginPage()
    {
        return ConfigurationManager.AppSettings["domain_auth"] + "/login/?r=" + HttpUtility.UrlEncode(Request.Url.AbsoluteUri.Replace("default.aspx", "").Replace(".aspx", ""));

    }

    public string Hash
    {
        get
        {
            if (this.ViewState["pagehash"] == null)
                return "c";
            else
                return this.ViewState["pagehash"].ToString();
        }
        set
        {
            this.ViewState["pagehash"] = value;
        }
    }

    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public object PrimaryKey
    {
        set
        {
            this.ViewState.Add("primary_idx", value);
        }
        get
        {
            return this.ViewState["primary_idx"];
        }
    }

    protected object PageData
    {   // json
        get
        {
            return this.ViewState["pagedata"];
        }
        set
        {
            this.ViewState["pagedata"] = value;
        }
    }

    public virtual bool RequireLogin
    {
        get
        {
            return false;
        }
    }

    public virtual bool RequireSSL
    {
        get
        {
            return false;
        }
    }

    public virtual bool NoCache
    {
        get
        {
            return false;
        }
    }

    // http , https 상관없이 페이지 로드
    public virtual bool IgnoreProtocol
    {
        get
        {
            return false;
        }
    }


    public virtual bool RequireRecruitLogin
    {
        get
        {
            return false;
        }
    }


    /*
	protected override void Render(HtmlTextWriter writer) {
		StringBuilder sb = new StringBuilder();
		StringWriter sw = new StringWriter(sb);
		HtmlTextWriter hWriter = new HtmlTextWriter(sw);
		base.Render(hWriter);
		string html = sb.ToString();
		html = Regex.Replace(html, "<input[^>]*id=\"(__VIEWSTATE)\"[^>]*>", string.Empty, RegexOptions.IgnoreCase);
		writer.Write(html);
	}
	*/

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        

        if (NoCache)
        {
            Response.Buffer = true;
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
            Response.Expires = -1500;
            Response.AddHeader("Pragma", "no-cache");
            Response.AddHeader("cache-control", "no-cache");
            Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
        }
    }

    //20171129 김은지
    //어드민계정 로그인시 마스터페이지 변경 
    protected virtual void Page_PreInit(object sender, EventArgs e)
    {
        
    }

    protected virtual void Page_Load(object sender, EventArgs e)
    {
        //string root = string.Format("http://{0}", Request.Url.Host);
        //
        //Response.Redirect(root + Request.RawUrl, true);
        
        
        if (!IsPostBack)
            this.OnBeforePostBack();
        else
            this.OnAfterPostBack();
    }

    protected virtual void loadComplete(object sender, EventArgs e)
    {

    }

    protected virtual void OnBeforePostBack()
    {
        this.ViewState.Add("q", HttpUtility.UrlDecode(Request.QueryString.ToString()));

        base.LoadComplete += new EventHandler(loadComplete);

        this.ViewState["meta_keyword"] = "한국컴패션,어린이후원";
        this.ViewState["meta_description"] = "한국컴패션 │ 꿈을 잃은 어린이들에게 그리스도의 사랑을";
        this.ViewState["meta_url"] = Request.UrlEx();
        this.ViewState["meta_title"] = "";
        this.ViewState["meta_image"] = ConfigurationManager.AppSettings["domain"] + "common/img/sns_default_image.jpg";



    }

    protected virtual void OnAfterPostBack()
    {

    }

    public string GetTitle(string fix, string append)
    {
        return string.Format("{0}-{1}", append, fix);
    }

    protected void paging_Navigate(int page, string hash)
    {
        this.GetList(page);

        if (!string.IsNullOrEmpty(hash) && Page.IsPostBack)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "hash", string.Format("location.hash = '#{0}';", hash), true);
        }

    }

    protected virtual void Search(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.Hash) && Page.IsPostBack)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "hash", string.Format("location.hash = '#{0}';", this.Hash), true);
        }
        this.GetList(1);
    }

    protected virtual void GetList(int page)
    {
    }

    protected virtual void list_LoadComplete(object sender, EventArgs e)
    {
        var page = Request["p"].ValueIfNull("1");
        this.GetList(Convert.ToInt32(page));
    }

    protected void AlertWithJavascript(string msg, string script)
    {
        if (string.IsNullOrEmpty(msg))
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "", "", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, typeof(Page), "", string.Format("$(function () {{alert('{0}');{1};}});", msg.Replace("'", ""), script), true);
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "", string.Format("alert('{0}');{1};", msg , (back ? "history.back()" : "")), true);
    }

    protected void AlertWithJavascript(string msg)
    {
        this.AlertWithJavascript(msg, "");
    }

    protected void ConfirmWithJavascript(string msg, string script_fail)
    {

        this.ConfirmWithJavascript(msg, script_fail, "");
    }

    protected void ConfirmWithJavascript(string msg, string script_fail, string script_success)
    {

        ScriptManager.RegisterStartupScript(this, typeof(Page), "", string.Format("$(function () {{if (confirm('{0}')){{{1};}}else{{{2};}}}});", msg, script_success, script_fail), true);
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "", string.Format("alert('{0}');{1};", msg , (back ? "history.back()" : "")), true);
    }



    // Page refresh detect

    private bool _refreshState;
    private bool _isRefresh;

    public bool IsRefresh
    {
        get
        {
            return _isRefresh;
        }
    }


    protected override void LoadViewState(object savedState)
    {
        object[] AllStates = (object[])savedState;
        base.LoadViewState(AllStates[0]);
        _refreshState = bool.Parse(AllStates[1].ToString());
        _isRefresh = _refreshState == (Session["__ISREFRESH"] != null && bool.Parse(Session["__ISREFRESH"].ToString()));
    }

    protected override object SaveViewState()
    {
        Session["__ISREFRESH"] = _refreshState;
        object[] AllStates = new object[2];
        AllStates[0] = base.SaveViewState();
        AllStates[1] = !(_refreshState);
        return AllStates;
    }

}

