﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public partial class Constants
{

	public const string TOKEN_COOKIE_NAME = "cps.token";
	public const string ADMIN_USER_COOKIE_NAME = "cps.admin.user";
	public const string ADMIN_MENU_SESSION_NAME = "admin_menus";
	public const string ADMIN_AUTH_MENU_SESSION_NAME = "admin_auth_menus";

	public const string AUTOLOGIN_COOKIE_NAME = "cps.autologin";
	public const string ENDUSER_COOKIE_NAME = "cps.user";
	public const string APP_COOKIE_NAME = "cps.app";
	public const string PAYINFO_COOKIE_NAME = "cps.payitem";
	public const string LETTERCOUNT_COOKIE_NAME = "cps.lettercount";
	public const string APP_ROOT_COOKIE_NAME = "cps.app.root";


	public const string RECRUIT_USER_COOKIE_NAME = "cps.recruit";
}