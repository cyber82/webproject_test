﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CommonLib;

public class MateLibDataContext : MateDataContext
{

	public MateLibDataContext()
		: base(ConfigurationManager.ConnectionStrings["mate_connectionString"].ConnectionString) {
	}

	public MateLibDataContext( string connectionString)
		: base(connectionString) {
	}

}