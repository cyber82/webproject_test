﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;

public class Pushpia {
	public Pushpia()
	{
	}


	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public class Response {

		public string bizId;
		public string reqUid;
		public string custId;
		public string code;
		public string msg;

	}

	// https://api.pushpia.com:444/send/realtime?d={"bizId":"359a0c69fa364ac49b2e6d802dfec4c3","msgType": "T","pushTime":1800,"pushTitle":"title","pushMsg":"push message","pushKey":"l","pushValue":"http://www.pushpia.com","reserveTime":"20150725161700","reqUid":"pushpia_20150417101700","custId":"whilestomach"}
	public static Response Send(string bizId , string title , string message , string opt_link , string custId ) {

		try {

			using(WebClient wc = new WebClient()) {

				var date = DateTime.Now.ToString("yyyyMMddHHmmssff");
				var res = wc.DownloadString(string.Format(@"https://api.pushpia.com:444/send/realtime?d={{""bizId"":""{0}"",""msgType"": ""T"",""pushTime"":1800,""pushTitle"":""{1}"",""pushMsg"":""{2}"",""pushKey"":""l"",""pushValue"":""{3}"",""reserveTime"":""{4}"",""reqUid"":""{5}"",""custId"":""{6}""}}",
					bizId , title , message , string.IsNullOrEmpty(opt_link) ? "http://m.compassion.or.kr" : "" , date , string.Format("cps{0}" , date) , custId));

				return res.ToObject<Response>();
				
			}

		} catch (Exception e){
			throw e;
		}
	}
}