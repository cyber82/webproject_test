﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;

public class Download {
	public Download()
	{
	}

	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();
	
	// 외부사이트 다운로드
	public void OverUrl(string url) {

		url = HttpUtility.UrlDecode(url);
		ErrorLog.Write(HttpContext.Current, 0, url);

		var fileName = url.Substring(url.LastIndexOf("/") + 1);
		try {
			using(WebClient client = new WebClient()) {
				byte[] res = client.DownloadData(url);

				HttpContext.Current.Response.Clear();
				HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
				HttpContext.Current.Response.Buffer = false;
				HttpContext.Current.Response.ContentType = "application/octet-stream";
				HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
				
				MemoryStream ms = new MemoryStream(res);

				ms.WriteTo(HttpContext.Current.Response.OutputStream);
				//HttpContext.Current.Response.End();

			}
		}catch(Exception ex) {
			throw ex;
		}


	}

}