﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using System.Data;
using System.Data.Linq;
using System.Reflection;
using CommonLib;

/// <summary>
/// 서비스를 클래스로 랩핑
/// </summary>
public class www6
{
    //웹서비스 연결
    public static  CommonLib.WWW6Service.SoaHelperSoap _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
    
    //sql문을 입력해서 DataSet으로 결과 받음.    
    public static DataTable selectX(string str)
    {
        Object[] objSql = new object[1] { str };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);

        DataTable result = null;
        try
        {
            result = ds.Tables[0];
        }
        catch (Exception)
        {
            //throw new Exception("데이터가 없습니다.");
            return result;
        }

        return result;
    }

    #region 쿼리를 문자로 받아서 조회하는 영역 selectText, selectSP
    #region 테이블 조회
    public static DataTable selectText(string sqlQuery)
    {
        Object[] objSql = new object[1] { sqlQuery };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);

        //테이블이 있는 지 없는지 검사.
        ds = ds.ckDS();

        return ds.Tables[0];
    }
    
    public static DataTable selectTextStore(string sqlQuery)
    {
        Object[] objSql = new object[1] { sqlQuery };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionStore", objSql, "Text", null, null);

        //테이블이 있는 지 없는지 검사.
        ds = ds.ckDS();

        return ds.Tables[0];
    }

    public static DataTable selectTextAuth(string sqlQuery)
    {
        Object[] objSql = new object[1] { sqlQuery };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "Text", null, null);

        //테이블이 있는 지 없는지 검사.
        ds = ds.ckDS();

        return ds.Tables[0];
    }
    #endregion
    #region SP 조회
    public static DataTable selectSP(string spName, Object[] obj1, Object[] obj2)
    {
        Object[] objSql = new object[1] { spName };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "SP", obj1, obj2.setEmpty());

        //테이블이 있는 지 없는지 검사.
        ds = ds.ckDS();

        return ds.Tables[0];
        //List<recruit_jobcode> entity = ds.Tables[0].DataTableToList<recruit_jobcode>();
    } 

    public static DataTable selectSPStore(string spName, Object[] obj1, Object[] obj2)
    {
        Object[] objSql = new object[1] { spName };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionStore", objSql, "SP", obj1, obj2.setEmpty());

        //테이블이 있는 지 없는지 검사.
        ds = ds.ckDS();

        return ds.Tables[0];
        //List<recruit_jobcode> entity = ds.Tables[0].DataTableToList<recruit_jobcode>();
    }

    public static DataTable selectSPAuth(string spName, Object[] obj1, Object[] obj2)
    {
        Object[] objSql = new object[1] { spName };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "SP", obj1, obj2.setEmpty());

        //테이블이 있는 지 없는지 검사.
        ds = ds.ckDS();

        return ds.Tables[0];
        //List<recruit_jobcode> entity = ds.Tables[0].DataTableToList<recruit_jobcode>();
    }
    #endregion
    #endregion

    #region C, U, D를 처리하는 영역
    /// <summary>
    /// DB:Web - sql문으로 입력, 수정, 삭제 수행
    /// </summary>
    /// <param name="sqlQuery"></param>
    public static void cud(string sqlQuery)
    {
        Object[] objSql = new object[1] { sqlQuery };
        _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);
    }
    /// <summary>
    /// DB:Store - sql문으로 입력, 수정, 삭제 수행
    /// </summary>
    /// <param name="sqlQuery"></param>
    public static void cudStore(string sqlQuery)
    {
        Object[] objSql = new object[1] { sqlQuery };
        _www6Service.NTx_ExecuteQuery("SqlCompassionStore", objSql, "Text", null, null);

    }
    /// <summary>
    /// DB:Auth - sql문으로 입력, 수정, 삭제 수행
    /// </summary>
    /// <param name="sqlQuery"></param>
    public static void cudAuth(string sqlQuery)
    {
        Object[] objSql = new object[1] { sqlQuery };
        _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "Text", null, null);
    }
    #endregion

    #region entity로 자동생성하는 C, U, D 메소드 모듬
    /// <summary>
    /// 입력 매개변수 obj를 이용해서 자동으로 update구문 생성 후 query 수행
    /// </summary>
    /// <param name="obj"></param>
    public static void update(object obj)
    {
        string sqlQuery = string.Empty;

        string updateStr = MakeSQL.upQ(obj, utils.webService.MAIN);
        string whereClause = MakeSQL.whereClause(utils.webService.MAIN, obj);

        sqlQuery = updateStr + whereClause;

        Object[] objSql = new object[1] { sqlQuery };
        _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);
    }

    /// <summary>
    /// 입력 매개변수 obj와 pkstr을 이용해서 insert구문 생성 후 qeury 수행
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="pkStr"></param>
    public static void insert(Object obj)
    {
        string sqlQuery = string.Empty;

        string insertStr = MakeSQL.insertQ(obj, utils.webService.MAIN);

        sqlQuery = insertStr;

        Object[] objSql = new object[1] { sqlQuery };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);

        //입력 매개변수 obj의 프로퍼티 pk에 자동증가된 identity값을 할당
        utils.setProperty(obj, ds, utils.webService.MAIN);
    }

    /// <summary>
    /// 입력 매개변수 obj와 pkstr을 이용해서 delete구문 생성 후 qeury 수행
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="pkStr"></param>
    public static void delete(Object obj)
    {
        string deleteStr = MakeSQL.deleteQ(obj, utils.webService.MAIN);
        
        Object[] objSql = new object[1] { deleteStr };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);
    }

    /// <summary>
    /// 입력 매개변수 obj와 pkstr을 이용해서 delete구문 생성 후 qeury 수행
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="pkStr"></param>
    public static void delete<T>(List<T> objs)
    {
        if (objs.Count > 0)
        {
            string deleteStr = MakeSQL.deleteAll<T>(objs, utils.webService.MAIN);

            Object[] objSql = new object[1] { deleteStr };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);
        }
    }

    /// <summary>
    /// 입력 매개변수 obj를 이용해서 자동으로 update구문 생성 후 query 수행
    /// </summary>
    /// <param name="obj"></param>
    public static void updateAuth(object obj)
    {
        string sqlQuery = string.Empty;

        string updateStr = MakeSQL.upQ(obj, utils.webService.AUTH);
        string whereClause = MakeSQL.whereClause(utils.webService.AUTH, obj);

        sqlQuery = updateStr + whereClause;

        Object[] objSql = new object[1] { sqlQuery };
        _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "Text", null, null);
    }

    /// <summary>
    /// 입력 매개변수 obj와 pkstr을 이용해서 insert구문 생성 후 qeury 수행
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="pkStr"></param>
    public static void insertAuth(Object obj)
    {
        string sqlQuery = string.Empty;

        string insertStr = MakeSQL.insertQ(obj, utils.webService.AUTH);

        sqlQuery = insertStr;

        Object[] objSql = new object[1] { sqlQuery };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "Text", null, null);

        //입력 매개변수 obj의 프로퍼티 pk에 자동증가된 identity값을 할당
        utils.setProperty(obj, ds, utils.webService.AUTH);
    }

    /// <summary>
    /// 입력 매개변수 obj와 pkstr을 이용해서 delete구문 생성 후 qeury 수행
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="pkStr"></param>
    public static void deleteAuth(Object obj)
    {
        string deleteStr = MakeSQL.deleteQ(obj, utils.webService.AUTH);

        Object[] objSql = new object[1] { deleteStr };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "Text", null, null);
    }

    /// <summary>
    /// 입력 매개변수 obj와 pkstr을 이용해서 delete구문 생성 후 qeury 수행
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="pkStr"></param>
    public static void deleteAuth<T>(List<T> objs)
    {
        string deleteStr = MakeSQL.deleteAll<T>(objs, utils.webService.AUTH);

        Object[] objSql = new object[1] { deleteStr };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "Text", null, null);
    }

    /// <summary>
    /// 입력 매개변수 obj를 이용해서 자동으로 update구문 생성 후 query 수행
    /// </summary>
    /// <param name="obj"></param>
    public static void updateStore(object obj)
    {
        string sqlQuery = string.Empty;

        string updateStr = MakeSQL.upQ(obj, utils.webService.STORE);
        string whereClause = MakeSQL.whereClause(utils.webService.STORE, obj);

        sqlQuery = updateStr + whereClause;

        Object[] objSql = new object[1] { sqlQuery };
        _www6Service.NTx_ExecuteQuery("SqlCompassionStore", objSql, "Text", null, null);
    }

    /// <summary>
    /// 입력 매개변수 obj와 pkstr을 이용해서 insert구문 생성 후 qeury 수행
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="pkStr"></param>
    public static void insertStore(Object obj)
    {
        string sqlQuery = string.Empty;

        string insertStr = MakeSQL.insertQ(obj, utils.webService.STORE);

        sqlQuery = insertStr;

        Object[] objSql = new object[1] { sqlQuery };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionStore", objSql, "Text", null, null);

        //입력 매개변수 obj의 프로퍼티 pk에 자동증가된 identity값을 할당
        utils.setProperty(obj, ds, utils.webService.STORE);
    }

    /// <summary>
    /// 입력 매개변수 obj와 pkstr을 이용해서 delete구문 생성 후 qeury 수행
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="pkStr"></param>
    public static void deleteStore(Object obj)
    {
        string deleteStr = MakeSQL.deleteQ(obj, utils.webService.STORE);

        Object[] objSql = new object[1] { deleteStr };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionStore", objSql, "Text", null, null);
    }

    /// <summary>
    /// 입력 매개변수 obj와 pkstr을 이용해서 delete구문 생성 후 qeury 수행
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="pkStr"></param>
    public static void deleteStore<T>(List<T> objs)
    {
        string deleteStr = MakeSQL.deleteAll<T>(objs, utils.webService.STORE);

        Object[] objSql = new object[1] { deleteStr };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionStore", objSql, "Text", null, null);
    }
    #endregion

    #region 테이블 SELECT 리스트로 가져오기
    public static List<T> selectQ<T>(params Object[] objs) where T : class, new()
    {
        List<T> result = new List<T>();
        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return result;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(0, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);

            result = ds.Tables[0].DataTableToList<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception ex)
        {
            //throw new Exception("데이터가 없습니다." + ex.ToString());
            return result;
        }

        return result;
    }

    public static List<T> selectQ2<T>(params Object[] objs) where T : class, new()
    {
        List<T> result = new List<T>();
        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return result;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(1, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);

            result = ds.Tables[0].DataTableToList<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception)
        {
            //throw new Exception("데이터가 없습니다.");
            return result;
        }

        return result;
    }

    public static List<T> selectQAuth<T>(params Object[] objs) where T : class, new()
    {
        List<T> result = new List<T>();
        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return result;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(0, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "Text", null, null);

            result = ds.Tables[0].DataTableToList<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception)
        {
            //throw new Exception("데이터가 없습니다.");
            return result;
        }

        return result;
    }

    public static List<T> selectQ2Auth<T>(params Object[] objs) where T : class, new()
    {
        List<T> result = new List<T>();
        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return result;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(1, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "Text", null, null);

            result = ds.Tables[0].DataTableToList<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception)
        {
            //throw new Exception("데이터가 없습니다.");
            return result;
        }

        return result;
    }

    public static List<T> selectQStore<T>(params Object[] objs) where T : class, new()
    {
        List<T> result = new List<T>();
        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return result;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(0, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionStore", objSql, "Text", null, null);

            result = ds.Tables[0].DataTableToList<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception)
        {
            //throw new Exception("데이터가 없습니다.");
            return result;
        }

        return result;
    }

    public static List<T> selectQ2Store<T>(params Object[] objs) where T : class, new()
    {
        List<T> result = new List<T>();
        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return result;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(1, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionStore", objSql, "Text", null, null);

            result = ds.Tables[0].DataTableToList<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception)
        {
            //throw new Exception("데이터가 없습니다.");
            return result;
        }

        return result;
    }
    #endregion
    
    #region 테이블 SETLECT Row로 가져오기
    public static T selectQF<T>(params Object[] objs) where T : class, new()
    {
        T item = new T();

        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return item;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(0, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);

            //if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)


            item = ds.Tables[0].Rows[0].DataTableToFirst<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception)
        {
            //throw //new Exception("데이터가 없습니다.");
            return null;
        }

        return item;
    }

    public static T selectQF2<T>(params Object[] objs) where T : class, new()
    {
        T item = new T();

        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return item;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(1, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);

            item = ds.Tables[0].Rows[0].DataTableToFirst<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception)
        {
            //throw //new Exception("데이터가 없습니다.");
            return null;
        }

        return item;
    }

    public static T selectQFAuth<T>(params Object[] objs) where T : class, new()
    {
        T item = new T();

        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return item;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(0, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "Text", null, null);

            item = ds.Tables[0].Rows[0].DataTableToFirst<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception)
        {
            //throw //new Exception("데이터가 없습니다.");
            return null;
        }

        return item;
    }

    public static T selectQF2Auth<T>(params Object[] objs) where T : class, new()
    {
        T item = new T();

        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return item;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(1, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "Text", null, null);

            item = ds.Tables[0].Rows[0].DataTableToFirst<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception)
        {
            //throw //new Exception("데이터가 없습니다.");
            return null;
        }

        return item;
    }

    public static T selectQFStore<T>(params Object[] objs) where T : class, new()
    {
        T item = new T();

        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return item;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(0, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionStore", objSql, "Text", null, null);

            item = ds.Tables[0].Rows[0].DataTableToFirst<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception)
        {
            //throw //new Exception("데이터가 없습니다.");
            return null;
        }

        return item;
    }

    public static T selectQF2Store<T>(params Object[] objs) where T : class, new()
    {
        T item = new T();

        try
        {
            foreach (var obj in objs)
            {
                if (obj == null)
                    return item;
            }

            Type t = typeof(T);
            string tName = t.Name;

            string query = MakeSQL.selQ(1, tName, objs);
            Object[] objSql = new object[1] { query };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionStore", objSql, "Text", null, null);

            item = ds.Tables[0].Rows[0].DataTableToFirst<T>();
        }
        catch (ArgumentNullException ex)
        {
            throw new ArgumentNullException(ex.ToString());
        }
        catch (Exception)
        {
            //throw //new Exception("데이터가 없습니다.");
            return null;
        }

        return item;
    }
    #endregion
}