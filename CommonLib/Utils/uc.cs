﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Data.Linq.Mapping;


public static class DataContextHelpers
{
    public static List<MetaDataMember> getPK(utils.webService _WS, object obj)
    {
        
        DataContext dataContext = null;
        Type t = obj.GetType();

        //서비스 생성으로 datacontext 가져오기
        switch (_WS)
        {
            case utils.webService.MAIN:
                dataContext = dc<MainLibDataContext>(obj);
                break;
            case utils.webService.AUTH:
                dataContext = dc<AuthLibDataContext>(obj);
                break;
            case utils.webService.STORE:
                dataContext = dc<StoreLibDataContext>(obj);
                break;
            default:
                break;
        }

        //pk칼럼명 가져오기(pk이고, 자동증가인 값)
        List<MetaDataMember> pkList = dataContext.GetByPk(obj);

        return pkList;
    }

    public static string getPKUniq(utils.webService _WS, object obj)
    {

        DataContext dataContext = null;
        Type t = obj.GetType();

        //서비스 생성으로 datacontext 가져오기
        switch (_WS)
        {
            case utils.webService.MAIN:
                dataContext = dc<MainLibDataContext>(obj);
                break;
            case utils.webService.AUTH:
                dataContext = dc<AuthLibDataContext>(obj);
                break;
            case utils.webService.STORE:
                dataContext = dc<StoreLibDataContext>(obj);
                break;
            default:
                break;
        }

        //pk칼럼명 가져오기(pk이고, 자동증가인 값)
        string pk = dataContext.GetByUniq(obj);

        return pk;
    }

    public static string getUniq(utils.webService _WS, object obj)
    {

        DataContext dataContext = null;
        Type t = obj.GetType();

        //서비스 생성으로 datacontext 가져오기
        switch (_WS)
        {
            case utils.webService.MAIN:
                dataContext = dc<MainLibDataContext>(obj);
                break;
            case utils.webService.AUTH:
                dataContext = dc<AuthLibDataContext>(obj);
                break;
            case utils.webService.STORE:
                dataContext = dc<StoreLibDataContext>(obj);
                break;
            default:
                break;
        }

        //(자동증가인 값)
        string identity = dataContext.GetByUniqIdentity(obj);

        return identity;
    }

    private static List<MetaDataMember> GetByPk(this DataContext context, object obj)
    {
        List<MetaDataMember> pkList = new List<MetaDataMember>();
        //var mapping = context.Mapping.GetTable(typeof(T));
        var mapping = context.Mapping.GetTable(obj.GetType());
        
        //pk이고 자동증가하는 필드명 가져오기
        var pkfield = mapping.RowType.DataMembers.SingleOrDefault(d => d.IsPrimaryKey && d.IsDbGenerated);
        
        if (pkfield == null)
        {
            //자동증가 pk가 없다면 여러키 조합일 경우임.
            //다수의 pk를 리스트로 가져온다.
            var pkfieldList = mapping.RowType.DataMembers.Where(d => d.IsPrimaryKey);
            if (pkfieldList.Count() > 0)
            {
                pkList = pkfieldList.ToList();
            }
        }
        else
        {
            pkList.Add(pkfield);
        }

        return pkList;
    }

    private static string GetByUniq(this DataContext context, object obj)
    {
        //var mapping = context.Mapping.GetTable(typeof(T));
        string result = string.Empty;
        var mapping = context.Mapping.GetTable(obj.GetType());

        //pk이고 자동증가하는 필드명 가져오기
        var pkfield = mapping.RowType.DataMembers.SingleOrDefault(d => d.IsPrimaryKey && d.IsDbGenerated);
        if(pkfield != null)
            result = pkfield.Name;

        return result;
    }

    private static string GetByUniqIdentity(this DataContext context, object obj)
    {
        //var mapping = context.Mapping.GetTable(typeof(T));
        string result = string.Empty;
        var mapping = context.Mapping.GetTable(obj.GetType());

        //자동증가하는 필드명 가져오기
        var identityfield = mapping.RowType.DataMembers.SingleOrDefault(d => d.IsDbGenerated);
        if (identityfield != null)
            result = identityfield.Name;

        return result;
    }
    
    private static DataContext dc<T>(object obj) where T : new()
    {
        DataContext daoContext = null;
        
        Type t = typeof(T);

        var baseObj = new T();
        

        foreach (var pro in t.GetProperties())
        {
            //T의 프로퍼티와 obj의 이름이 같다면
            if(pro.Name.Equals(obj.GetType().Name))
            {
                //T의 프로퍼티의 값을 가져온다.
                //ex : Table<admin> admin
                var tmp = pro.GetValue(baseObj);

                //T의 프로퍼티의 형(type)을 가져온다.
                //ex Table<admin>
                Type tt = tmp.GetType();

                //T의 프로퍼티의 형식의 프로퍼티 "context"를 가져온다.
                PropertyInfo pInfo = tt.GetProperty("Context");
                object tmp2 = pInfo.GetValue(tmp);

                //프로퍼티 "context"를 DataContext로 형변환한다.
                daoContext = tmp2 as DataContext;

                break;
            }
        }

        return daoContext;
    }
}
