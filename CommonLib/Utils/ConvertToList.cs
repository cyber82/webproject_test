﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using Newtonsoft.Json.Serialization;
using System.Collections;
using NLog;
using Newtonsoft.Json.Converters;
using System.Data;

public static class ConvertToList
{
    public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
    {
        try
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            List<T> result = new List<T>();

            if (table == null) return result;

            foreach (var row in table.Rows)
            {
                var item = CreateItemFromRow<T>((DataRow)row, properties);
                result.Add(item);
            }

            return result;
            //List<T> list = new List<T>();
            //
            //if (table == null || table.Rows.Count == 0)
            //    return list;
            //
            //foreach (var row in table.AsEnumerable())
            //{
            //    T obj = new T();
            //    //list.Add(DataTableToFirst<T>(row));
            //
            //    foreach (var prop in obj.GetType().GetProperties())
            //    {
            //        try
            //        {
            //            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
            //            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
            //        }
            //        catch
            //        {
            //            throw;
            //        }
            //    }
            //
            //    list.Add(obj);
            //}
            //
            //return list;
        }
        catch
        {
            throw;
        }
    }

    private static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new()
    {
        T item = new T();
        foreach (var property in properties)
        {
            if (property.PropertyType == typeof(System.DayOfWeek))
            {
                DayOfWeek day = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), row[property.Name].ToString());
                property.SetValue(item, day, null);
            }
            else
            {
                if (row[property.Name] == DBNull.Value)
                    property.SetValue(item, null, null);
                else
                {
                    if(utils.isChar(property.PropertyType))
                        property.SetValue(item, row[property.Name].toChar(), null);
                    else
                        property.SetValue(item, row[property.Name], null);
                }
            }
        }
        return item;
    }

    public static T DataTableToFirst<T>(this DataRow row) where T : class, new()
    {
        try
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            T item = new T();

            if (row == null) return item;

            foreach (var property in properties)
            {
                if (property.PropertyType == typeof(System.DayOfWeek))
                {
                    DayOfWeek day = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), row[property.Name].ToString());
                    property.SetValue(item, day, null);
                }
                else
                {
                    if (row[property.Name] == DBNull.Value)
                    {
                        property.SetValue(item, null, null);
                    }
                    else
                    {
                        if (utils.isChar(property.PropertyType))
                            property.SetValue(item, row[property.Name].toChar(), null);
                        else
                            property.SetValue(item, row[property.Name], null);
                    }
                }
            }
            return item;
            //T obj = new T();
            //
            //if (row == null)
            //    return obj;
            //
            //foreach (var prop in obj.GetType().GetProperties())
            //{
            //    try
            //    {
            //        PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
            //        propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
            //    }
            //    catch
            //    {
            //        throw;
            //    }
            //}
            //
            //return obj;
        }
        catch
        {
            throw;
        }
    }

    

}
