﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;

public class RequestValidator {
	public enum Type {
		Numeric,
		Alphabet,
		AlphaNumeric
	}

	List<KeyValuePair<string, Type>> _list = null;
	public RequestValidator Add(string key, Type t) {
		if (_list == null)
			_list = new List<KeyValuePair<string, Type>>();
		_list.Add(new KeyValuePair<string, Type>(key, t));
		return this;
	}

	public bool Validate(HttpContext context) {
		return this.Validate(context , null);
	}

	public bool Validate(HttpContext context , string redirectUrl) {

		bool result = true;
		foreach (var entity in _list) {
			string value = context.Request[entity.Key];
			if (string.IsNullOrEmpty(value))
				continue;

			switch (entity.Value) {
				default:
				continue;
				case Type.Numeric :
				if (!value.CheckNumeric())
					result = false;
					break;
				case Type.Alphabet:
					if (!value.CheckAlphabet())
						result = false;
					break;
				case Type.AlphaNumeric:
					if (!value.CheckAlphaNumeric())
						result = false;
					break;
			}

			if (!result)
				break;
		}

		if (!result && !string.IsNullOrEmpty(redirectUrl)) {
			context.Response.Redirect(redirectUrl);
			context.Response.End();
		}
		return result;
		
	}

}
