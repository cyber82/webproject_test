﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;

public class Email
{
    public Email()
    {
    }

    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public static string SystemSender
    {
        get { return ConfigurationManager.AppSettings["emailSender"]; }
    }


    public static void Send(HttpContext context, string from, List<string> targets, string subject, string pathOrBody, Dictionary<string, string> keyValues, List<string> attachments)
    {
        Email.Send(context, from, targets, subject, pathOrBody, keyValues, attachments, true);
    }

    // useTemplatePath = true > pathOrBody 의 값은 템플릿경로 , useTemplatePath = false > pathOrBody 의 값은 내용
    public static void Send(HttpContext context, string from, List<string> targets, string subject, string pathOrBody, Dictionary<string, string> keyValues, List<string> attachments, bool useTemplatePath)
    {
        try
        {
            //ErrorLog.Write(HttpContext.Current, 0, "1.email시작");
            string body = "";

            if (useTemplatePath)
            {
                var file = System.IO.File.OpenText(context.Server.MapPath(pathOrBody));
                body = file.ReadToEnd();
                file.Close();
            }
            else
            {
                body = pathOrBody;
            }
            if (keyValues != null)
            {
                foreach (var keyValue in keyValues)
                {
                    body = body.Replace(keyValue.Key, keyValue.Value);
                }
            }

            body = body.Replace("{host}", "http://" + context.Request.Url.Host.Replace("auth.", "www.").Replace("m.", "www."));
            body = body.Replace("{mhost}", "http://" + context.Request.Url.Host.Replace("www.", "m.").Replace("auth.", "m."));
            body = body.Replace("{authhost}", "http://" + context.Request.Url.Host.Replace("www.", "auth.").Replace("m.", "auth."));

            body = body.Replace("{regdate}", DateTime.Now.ToString("yyyy.'MM'.dd"));
            body = body.Replace("{full_regdate}", DateTime.Now.ToString());

            SmtpClient client = new SmtpClient();

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(from);

            var stage = ConfigurationManager.AppSettings["stage"];
            var useSms = ConfigurationManager.AppSettings["useSms"];

            //ErrorLog.Write(HttpContext.Current, 0, "2.stage:" + stage);

            if (stage == "dev")
            {

                client.Timeout = 3000;      // 3초

                //msg.To.Add( new MailAddress( "whilestomach@gmail.com" ) );
                //msg.To.Add( new MailAddress( "swsong0917@gmail.com" ) );
                //var url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
                //if(url.IndexOf("compassionkr.com") > -1) {
                //    msg.To.Add(new MailAddress("hyunbr@pentabreed.com"));
                //    msg.To.Add(new MailAddress("hbr1941@nate.com"));
                //    msg.To.Add(new MailAddress("sylim@pentabreed.com"));
                //    msg.To.Add(new MailAddress("thirdasahan@naver.com"));
                //    msg.To.Add(new MailAddress("ruby@pentabreed.com"));
                //    msg.To.Add(new MailAddress("ruby4074@gmail.com "));
                //    msg.To.Add(new MailAddress("rebeca0210@pentabreed.com"));
                //    msg.To.Add(new MailAddress("rebeca021078@gmail.com"));
                //}
                // msg.To.Add(new MailAddress("e4koo850@naver.com"));
                //msg.To.Add(new MailAddress("heewon.moon@gowit.co.kr"));
                //msg.To.Add(new MailAddress("ruokman@naver.com"));

                //msg.To.Add(new MailAddress("tkim @compassion.or.kr"));  //김테레사 테스트
                //msg.To.Add(new MailAddress("jongjin.lee@gowit.co.kr"));  //김테레사 테스트

                //client.Timeout = 5000;      // 3초
                //foreach(string to in targets) {
                //    msg.To.Add(new MailAddress(to));
                //}
                if (useSms == "Y")
                {
                    foreach (string to in targets)
                    {
                        msg.To.Add(new MailAddress(to));
                    }
                }
                else
                {
                    msg.To.Add(new MailAddress("jongjin.lee@gowit.co.kr"));  //이종진
                }

            }
            else
            {
                foreach (string to in targets)
                {
                    msg.To.Add(new MailAddress(to));
                }
            }

            msg.IsBodyHtml = true;
            msg.Body = body;
            msg.Subject = subject;

            if (attachments != null)
            {
                foreach (string attachment in attachments)
                {
                    try
                    {
                        msg.Attachments.Add(new Attachment(context.Server.MapPath(attachment)));
                    }
                    catch (Exception ex)
                    {
                        Logger.Warn("파일에러 : " + attachment + " : " + ex.Message);
                    }
                }
            }
            //ErrorLog.Write(HttpContext.Current, 0, "3.end");
            client.Send(msg);

        }
        catch (Exception e)
        {

            ErrorLog.Write(HttpContext.Current, 0, e.ToString());

        }
    }
}