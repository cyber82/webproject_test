﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using CommonLib;
using System.Reflection;

/// <summary>
/// 서비스를 클래스로 랩핑
/// </summary>

public static class MakeSQL
{
    public static string selQ(int op, string tName, params Object[] objs)
    {
        string query = string.Empty;

        if(op ==0)
            query = whereClause(0, objs);
        else
            query = whereClause(1, objs);

        string selectStr = string.Format("select * from  [{0}]  {1} ", tName, query);

        return selectStr;
    }

    public static string delQ(int op, string tName, params Object[] objs)
    {
        string query = string.Empty;

        if (op == 0)
            query = whereClause(0, objs);
        else
            query = whereClause(1, objs);

        string delStr = string.Format("delete from {0} {1}", tName.ckKeyword(), query);

        return delStr;
    }

    //delete문 만듬.
    public static string deleteQ(Object obj, utils.webService _WS)
    {
        string delStr = string.Empty;
        string whereStr = whereClause(_WS, obj);

        delStr = string.Format("delete from {0} {1}", obj.GetType().Name.ckKeyword(), whereStr);

        return delStr;
    }

    //delete문 만듬.
    public static string deleteAll<T>(List<T> objs, utils.webService _WS)
    {
        string delStr = string.Empty;

        foreach (var obj in objs)
        {
            string whereStr = whereClause(_WS, obj);

            delStr += string.Format("delete from {0} {1}; ", obj.GetType().Name.ckKeyword(), whereStr);
        }
        
        return delStr;
    }

    //obj를 입력 받아서 whereClause 자동 생성
    //pkStr로 pk컬럼 수정 제외
    public static string upQ(Object obj, string pkStr)
    {
        string updateStr = string.Empty;
        List<string> query = new List<string>();
        Type t = obj.GetType();
        
        string whereStr = string.Empty;

        foreach (var pro in t.GetProperties())
        {
            //pk로 whereClause를 만든다.
            //1번만 수행
            if (pkStr.Equals(pro.Name))
            {
                whereStr = whereClause(0, pro.Name, pro.GetValue(obj));
                continue;
            }

            //스폰서정보중에서 패스워드는 스킵
            if (t.Name.ToLower().Equals("tsponsormaster"))
            {
                if (pro.Name.ToLower().Equals("userpw"))
                    continue;
            }

            //update문의 set절을 리스트에 담는다.
            query.Add(string.Format(@" {0} = {1}", pro.Name, makeStringStatements(pro.PropertyType, pro.GetValue(obj))));
        }

        updateStr = string.Format("update {0}  set {1} {2} ", obj.GetType().Name.ckKeyword(), string.Join(",", query), whereStr);

        return updateStr;
    }

    //whereClause이 없는 update문 만듬.
    public static string upQ(Object obj, utils.webService _WS)
    {
        string updateStr = string.Empty;
        List<string> query = new List<string>();
        Type t = obj.GetType();
        //pk이면서 자동증가값 가져오기
        string pk = DataContextHelpers.getPKUniq(_WS, obj);
        //pk에 무관하게 자동증가값 가져오기
        string identity = DataContextHelpers.getUniq(_WS, obj);

        foreach (var pro in t.GetProperties())
        {
            //pk가 unKnownPK가 아니고 pro.name이 pk와 같다면 건너 뛴다. 
            if (pro.Name.Equals(pk) || pro.Name.Equals(identity))
            {
                continue;
            }

            //스폰서정보중에서 패스워드는 스킵
            if (t.Name.ToLower().Equals("tsponsormaster"))
            {
                if (pro.Name.ToLower().Equals("userpw"))
                    continue;
            }

            //update문의 set절을 리스트에 담는다.
            query.Add(string.Format(@" {0} = {1}", pro.Name, makeStringStatements(pro.PropertyType, pro.GetValue(obj))));
        }

        updateStr = string.Format("update {0}  set {1} ", obj.GetType().Name.ckKeyword(), string.Join(",", query));

        return updateStr;
    }

    //whereClause로 수정 데이터를 필터링 
    //- pk 컬럼 수정 제외
    public static string updateQ(Object obj, string whereClause)
    {
        Type t = obj.GetType();
        List<string> query = new List<string>();

        //string[] pkStr = whereClause.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
        string[] pkStrArr = whereClause.Split('=');
        string pkStr = pkStrArr[0].Trim();
        
        foreach (var pro in t.GetProperties())
        {
            if (pkStr.Equals(pro.Name))
                continue;

            //update문의 set절을 리스트에 담는다.
            query.Add(string.Format(@" {0} = {1}", pro.Name, makeStringStatements(pro.PropertyType, pro.GetValue(obj))));
        } 

        string updateStr = string.Format("update {0}  set {1} where {2} ", obj.GetType().Name.ckKeyword(), string.Join(",", query), whereClause);

        return updateStr;
    }

    //whereClause로 수정 데이터를 필터링 하지 안는다. 
    //- pk칼럼이 없이 모두 수정할 경우
    public static string updateQ2(Object obj, string whereClause)
    {
        Type t = obj.GetType();
        List<string> query = new List<string>();
        
        foreach (var pro in t.GetProperties())
        {
            //update문의 set절을 리스트에 담는다.
            query.Add(string.Format(@" {0} = {1}", pro.Name, makeStringStatements(pro.PropertyType, pro.GetValue(obj))));
        }

        string updateStr = string.Format("update {0}  set {1} where {2} ", obj.GetType().Name.ckKeyword(), string.Join(",", query), whereClause);

        return updateStr;
    }

    //whereClause로 입력 데이터를 필터링 
    //- pk 컬럼 입력 제외
    public static string insertQ(Object obj, string pkStr)
    {
        string insertStr = string.Empty;

        Type t = obj.GetType();

        List<string> query1 = new List<string>();
        List<string> query2 = new List<string>();
        string queryStr1 = string.Empty;
        string queryStr2 = string.Empty;

        foreach (var pro in t.GetProperties())
        {
            //pk는 insert 불가
            if (pkStr.Equals(pro.Name))
                continue;

            query1.Add(string.Format(@"{0}", pro.Name));
            query2.Add(string.Format(@"{0}", makeStringStatements(pro.PropertyType, pro.GetValue(obj))));   
        }

        queryStr1 = "(" + string.Join(",", query1) + ")";
        queryStr2 = "(" + string.Join(",", query2) + ")";


        insertStr = string.Format("insert into {0}  {1} values {2}", obj.GetType().Name.ckKeyword(), queryStr1, queryStr2);
        insertStr += string.Format("; select IDENT_CURRENT('{0}')as idx;", obj.GetType().Name.ckKeyword());

        return insertStr;
    }

    //whereClause로 입력 데이터를 필터링 
    //- pk 컬럼 입력 제외
    public static string insertQ(Object obj, utils.webService _WS)
    {
        //pk칼럼명 가져오기(pk이고, 자동증가인 값)
        string pk = DataContextHelpers.getPKUniq(_WS, obj);

        //2018-04-03 이종진 - 자동증가값인 값 (pk에 상관없이)
        string identityField = DataContextHelpers.getUniq(_WS, obj); ;

        string insertStr = string.Empty;

        Type t = obj.GetType();

        List<string> query1 = new List<string>();
        List<string> query2 = new List<string>();
        string queryStr1 = string.Empty;
        string queryStr2 = string.Empty;
        
        foreach (var pro in t.GetProperties())
        {
            if(pro.Name.Equals(pk) || pro.Name.Equals(identityField))
            {
                //Pk가 있다면 pk컬럼은 자동증가
                //건너 뛴다.
                continue;
            }

            query1.Add(string.Format(@"{0}", pro.Name));
            query2.Add(string.Format(@"{0}", makeStringStatements(pro.PropertyType, pro.GetValue(obj))));
        }

        queryStr1 = "(" + string.Join(",", query1) + ")";
        queryStr2 = "(" + string.Join(",", query2) + ")";


        insertStr = string.Format("insert into {0}  {1} values {2}", obj.GetType().Name.ckKeyword(), queryStr1, queryStr2);
        insertStr += string.Format("; select IDENT_CURRENT('{0}')as idx;", obj.GetType().Name.ckKeyword());

        return insertStr;
    }

    //whereClause로 입력 데이터를 필터링 하지 안는다. 
    //- pk칼럼이 없이 모두 입력할 경우
    public static string insertQ2(Object obj)
    {
        Type t = obj.GetType();

        List<string> query1 = new List<string>();
        List<string> query2 = new List<string>();
        string queryStr1 = string.Empty;
        string queryStr2 = string.Empty;

        foreach (var pro in t.GetProperties())
        {
            query1.Add(string.Format(@"{0}", pro.Name));
            query2.Add(string.Format(@"{0}", makeStringStatements(pro.PropertyType, pro.GetValue(obj))));
        }

        queryStr1 = "(" + string.Join(",", query1) + ")";
        queryStr2 = "(" + string.Join(",", query2) + ")";


        string insertStr = string.Format("insert into {0}  {1} values {2}", obj.GetType().Name.ckKeyword(), queryStr1, queryStr2);

        return insertStr;
    }

    /// <summary>
    /// mssql whereClause 자동생성
    /// <para>1. op가 0이면 자동으로 비교문( = ) 생성</para>
    /// <para>2. op가 1이면 입력문에 사용자가 입력 해야 함</para>
    /// <para>3. objs는 쌍으로 입력해야 함.</para>
    /// <para>4. order by 문은 마지막 파라메터에 문자열로 입력</para>
    /// </summary>
    /// <param name="op"> 자동, 수종 설정 0, 1 </param>
    /// <param name="objs">컬럼명과 값</param>
    /// <returns>mssql whereClause 문자열</returns>
    public static string whereClause(int op, params Object[] objs)
    {
        string query = "where 1 = 1";
        
        //파라메터가 없으면 빈값 리턴
        if (objs.Count() == 0)
            return query;

        for (int i = 0; i < objs.Length; i++)
        {
            if (i % 2 == 0)
            {
                //인수가 짝수일때 마지막 인수는 orderby 문을 처리한다.
                if(i < objs.Length -1)
                {
                    //compare(op)로 비교문('=', '!=', '>='등) 만들고  andor()로 연결문('and', 'or') 만듬
                    //ex : and 'userid' = 
                    //ex : or 'userid' !=
                    query += string.Format(@"{0}", objs[i].ToString().compare(op)).andOr();
                }
                else
                {
                    //마지막 일때 order by문처리
                    query += string.Format(@" order by {0}", objs[i].ToString());
                }
            }
            else
            {
                var wName = makeStringStatements(objs[i].GetType(), objs[i]);
                query += string.Format(@"{0}", wName);
            }
        }

        return query;
    }

    /// <summary>
    /// mssql. pk를 이용한 whereClause 자동생성
    /// <para>1. pk 갯수만큼 where 구문 생성</para>
    /// </summary>
    /// <param name="objs">클래스의 객체</param>
    /// /// <param name="selectService">서비스명</param>
    /// <returns>mssql whereClause 문자열</returns>
    public static string whereClause(utils.webService _WS, object obj)
    {
        string query = "where 1 = 1";

        //pk칼럼명 가져오기
        List<MetaDataMember> pkList = DataContextHelpers.getPK(_WS, obj);

        //자동증가값 가져오기 (pk과 무관하게 자동증가값을 무조건 가져옴)
        string identity = DataContextHelpers.getUniq(_WS, obj);

        Type t = obj.GetType();

        //pk칼럼명으로 whereClause생성
        int k = 0;
        foreach (var pro in t.GetProperties())
        {
            //identity값이 있으면 where절 만들고 break;
            if (pro.Name.Equals(identity))
            {
                var pkValue = makeStringStatements(pro.PropertyType, pro.GetValue(obj));
                query += string.Format(" and {0} = {1}", identity, pkValue);
                break;
            }

            //pk수 만큼 루프
            foreach (MetaDataMember pk in pkList)
            {
                if (string.IsNullOrEmpty(identity) && pro.Name.Equals(pk.Name))
                {
                    var pkValue = makeStringStatements(pro.PropertyType, pro.GetValue(obj));
                    query += string.Format(" and {0} = {1}", pk.Name, pkValue);
                    k++;
                }
            }

            //매칭이 끝났거나 pk가없다면 빠져 나감.
            if (k == pkList.Count || pkList.Count == 0)
                break;
        }

        return query;
    }

    /// <summary>
    /// nullable과 일반 타입을 구분해서 따움표 자동 생성
    /// </summary>
    /// <param name="myType"></param>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string makeStringStatements(Type myType, object obj)
    {
        string result = string.Empty;

        if (myType.Name.Equals("Nullable`1"))
        {
            result = utils.makeSingle(Nullable.GetUnderlyingType(myType).Name, obj);
        }
        else
        {
            result = utils.makeSingle(myType.Name, obj);
        }

        return result;
    }
}