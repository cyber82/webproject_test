﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using Newtonsoft.Json.Serialization;
using System.Collections;
using NLog;
using Newtonsoft.Json.Converters;

public class JsonWriter {

	public bool success;
	public object data;
	public string message;
	public string action;
	
	public static string toLowerCaseJson(object input) {
		var settings = new JsonSerializerSettings();
		settings.ContractResolver = new LowercaseContractResolver();
		settings.Converters.Add(new IsoDateTimeConverter {
			DateTimeFormat = "yyyy'/'MM'/'dd HH:mm:ss",
			DateTimeStyles = System.Globalization.DateTimeStyles.None
		});
		
		return JsonConvert.SerializeObject(input, Newtonsoft.Json.Formatting.None, settings);
	}

	public void Write(HttpContext context) {
		Write(this, context, true);
	}

	public static void Write(object result, HttpContext context) {
		Write(result, context, true);
	}

	public static void Write(object result, HttpContext context, bool noCache) {
		context.Response.ClearContent();
		if (noCache)
			context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

		context.Response.ContentType = "application/json";
		context.Response.Write(toLowerCaseJson(result));
		
	}

	public class LowercaseContractResolver : DefaultContractResolver {
		protected override string ResolvePropertyName(string propertyName) {
			return propertyName.ToLower();
		}
	}

}
