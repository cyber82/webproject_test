﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Data;
using System.Configuration;

/// <summary>
/// util의 요약 설명입니다.
/// </summary>
public static class utils
{
    /// <summary>
    /// 문자열 null값 체크 및 " ' "를 " '' "로 변환
    /// </summary>
    /// <param name="obj">문자열로 변환 가능한 오브젝트</param>
    /// <returns>문자열 이나 빈문자</returns>
    public static string  ckNull(Object obj)
    {
        //문자열 처리
        //string result = string.Empty;
        string result = "NULL";

        if (obj != null)
            result = obj.ToString().Replace("'", "''");
        
        return result;
    }
    /// <summary>
    /// char형 오브젝트를 string으로 변환 및 '\0' 제거
    /// </summary>
    /// <param name="obj">char형 오브젝트</param>
    /// <returns>문자열이나 ' '</returns>
    public static string ckChar(Object obj)
    {
        //문자열 처리
        string result = "NULL";

        //char에서 '\0'을 공백으로 처리 후 문자열로 변환
        if (obj != null)
            result = obj.toChar().ToString();

        return result;
    }

    /// <summary>
    /// int형 오브젝트를 string으로 변환
    /// </summary>
    /// <param name="obj">int형 오브젝트</param>
    /// <returns>문자열이나 "0"</returns>
    public static string ckInt(Object obj)
    {
        //문자열 처리
        string result = "NULL";

        if (obj != null)
            result = obj.ToString();
        
        return result;
    }

    /// <summary>
    /// double형 오브젝트를 string으로 변환
    /// </summary>
    /// <param name="obj">double형 오브젝트</param>
    /// <returns>문자열이나 "0.0"</returns>
    public static string ckDouble(Object obj)
    {
        //문자열 처리
        string result = "NULL";

        if (obj != null)
            result = obj.ToString(); 

        return result;
    }

    /// <summary>
    /// List<string>형 오브젝트를 string으로 변환
    /// </summary>
    /// <param name="obj">List<string>형 오브젝트</param>
    /// <returns>문자열이나 빈문자</returns>
    public static string ckList(Object obj)
    {
        List<string> tmp = obj as List<string>;
        string result = string.Empty;
        if (tmp.Count == 0)
            return result;

        if (tmp[0] == null)
            return result;

        result = tmp[0];

        return result;
    }

    /// <summary>
    /// DateTime형 오브젝트를 string("yyyy-MM-dd HH:mm:ss")으로 변환
    /// </summary>
    /// <param name="obj">DateTime형 오브젝트</param>
    /// <returns>문자열이나 mssql함수 : GETDATE()</returns>
    public static string ckDate(Object obj)
    {
        string result = "NULL";

        DateTime dtime = Convert.ToDateTime(obj);
        if (obj != null)
            result = string.Format("'{0}'", dtime.ToString("yyyy-MM-dd HH:mm:ss"));

        return result;
    }

    /// <summary>
    /// Boolen형 오브젝트를 "0"이나 "1"로 변환
    /// </summary>
    /// <param name="obj">Boolen형 오브젝트</param>
    /// <returns>"0"나 "1"</returns>
    public static string ckBool(Object obj)
    {
        string result = "NULL";

        if (obj == null)
            return result;

        if (obj.ToString().Equals("True"))
            result = "1";
        else
            result = "0";

        return result;
    }

    /// <summary>
    /// param => %param%
    /// </summary>
    /// <param name="string">string</param>
    /// <returns>string</returns>
    public static string likeAll(this string str)
    {
        string result = "%" + str + "%";
        return result;
    }

    /// <summary>
    /// param => %param
    /// </summary>
    /// <param name="string">string</param>
    /// <returns>string</returns>
    public static string like1(this string str)
    {
        string result = "%" + str;
        return result;
    }

    /// <summary>
    /// param => param%
    /// </summary>
    /// <param name="string">string</param>
    /// <returns>string</returns>
    public static string like2(this string str)
    {
        string result = str + "%";
        return result;
    }

    public static string dt2strS(this string wName, string  stringFormat)
    {
        string result = "Convert( " + stringFormat  + ", "+ wName;
        return result;
    }

    public static string dt2strE(this string formatType)
    {
        string result = ", " + formatType + ")";
        return result;
    }

    /// <summary>
    /// 파라메터 obj1, obj2를 문자열 "obj1 and obj2"로 만듬
    /// </summary>
    /// <returns>List<string></returns>
    public static List<string> btween(Object obj1, Object obj2)
    {
        Type type1 = obj1.GetType();
        Type type2 = obj2.GetType();

        string tmp= string.Format("{0} and {1}", makeSingle(type1.Name, obj1), makeSingle(type2.Name, obj2));
        List<string> result = new List<string>();
        result.Add(tmp);

        return result;
    }

    /// <summary>
    /// 리스트를 문자열 "(item0, item1, ..., itemn)"으로 만듬
    /// </summary>
    /// <param name="objs">오브젝트 배열</param>
    /// <returns>List<string> </returns>
    public static List<string> include<T>(List<T> objs)
    {
        List<string> tmpList = new List<string>();

        foreach (var item in objs)
        {
            Type t = item.GetType();
            string str =  MakeSQL.makeStringStatements(t, item);
            tmpList.Add(str);
        }

        string tmp = string.Format("({0})", string.Join(",", tmpList));
        List<string> result = new List<string>();
        result.Add(tmp);

        return result;
    }

    /// <summary>
    /// 리스트를 문자열 "(item0, item1, ..., itemn)"으로 만듬
    /// </summary>
    /// <param name="objs">오브젝트 배열</param>
    /// <returns>List<string> </returns>
    public static List<string> include(object objs)
    {
        List<string> tmpList = new List<string>();
                
        Type t = objs.GetType();
        string str = MakeSQL.makeStringStatements(t, objs);
        
        string tmp = string.Format("({0})", str);
        List<string> result = new List<string>();
        result.Add(tmp);

        return result;
    }

    /// <summary>
    /// sql문 whereCluase 자동 생성시 연결문 and, or 생성
    /// <para>1. and는 자동으로 생성</para>
    /// <para>2. or는 문자열에 "+or 문자열"</para>
    /// <para>3. "- 문자열"을 만들면 and나 or를 생성 안 함.</para>
    /// </summary>
    /// <param name="str">문자열</param>
    /// <returns>string</returns>
    public static string andOr(this string str)
    {
        string temp = str.ToUpper();
        string[] sp = { " " };
        string[] result = temp.Split(sp, StringSplitOptions.RemoveEmptyEntries);
        string returnStr = str;

        //매개변수에 +or가 없는 문자열만 and를 붙임
        if (result[0].Equals("+OR") == false)
            returnStr = " and " + str;
        else
            returnStr = " " + str.Replace("+", "");
        
        
        //매개변수에 -가  추가 되어있다면 AND를 않 붙임.
        if (result[0].Equals("-"))
        {
            //리턴 값 초기화
            returnStr = string.Empty;
            //리스트로 변환
            var tmpList = result.ToList();
            //첫번째 요소 삭제
            tmpList.RemoveAt(0);

            returnStr = string.Join(" ", tmpList);            
        }

        return returnStr;
    }

    /// <summary>
    /// sql문 whereCluase 자동 생성시 비교문 (=, !=, >, etc)  생성
    /// <para>1. 파라미터 op가 0이면 " = "</para>
    /// <para>2. 파라미터 op가 1이면 패스</para>
    /// </summary>
    /// <param name="str"></param>
    /// <param name="op"></param>
    /// <returns></returns>
    public static string compare(this string str, int op)
    {
        string result = str;

        if (op == 0)
            result = str + " = ";

        return result;
    }

    public static string ss(this object obj)
    {
        string result = string.Empty;
        Type myType = obj.GetType();

        switch (myType.Name)
        {
            case "String":
                if (ckNull(obj).isFunc())
                    result = string.Format(@" {0} ", ckNull(obj));
                else
                    result = string.Format(@"'{0}'", ckNull(obj));
                break;
            case "Char":
                result = string.Format(@"'{0}'", ckChar(obj));
                break;
            case "DateTime":
                result = string.Format(@"{0}", ckDate(obj));
                break;
            case "Int32":
                result = string.Format(@"{0}", ckInt(obj));
                break;
            case "Double":
                result = string.Format(@"{0}", ckDouble(obj));
                break;
            case "Boolean":
                result = string.Format(@"{0}", ckBool(obj));
                break;
            case "List`1":
                result = string.Format(@"{0}", ckList(obj));
                break;
            case "Ept":
                result = string.Format(@"{0}", string.Empty);
                break;
            default:
                result = string.Format(@"{0}", ckNull(obj));
                break;
        }

        return result;
    }

    /// <summary>
    /// sql whereClause 생성시 입력받은 obj의 형을 파악해서 따움표를 자동생성
    /// </summary>
    /// <param name="myType">Type T</param>
    /// <param name="obj">Object obj</param>
    /// <returns>string</returns>
    public static string makeSingle(string myType, object obj)
    {
        string result = string.Empty;
        switch (myType)
        {
            case "String":
                if (ckNull(obj).isFunc())
                    result = string.Format(@" {0} ", ckNull(obj));
                else
                    result = string.Format(@"'{0}'", ckNull(obj));
                break;
            case "Char":
                if (ckChar(obj).isFunc())
                    result = string.Format(@" {0} ", ckChar(obj));
                else
                    result = string.Format(@"'{0}'", ckChar(obj));
                break;
            case "Guid":
                if (ckNull(obj).isFunc())
                    result = string.Format(@" {0} ", ckNull(obj));
                else
                    result = string.Format(@"'{0}'", ckNull(obj));
                break;
            case "DateTime":
                result = string.Format(@"{0}", ckDate(obj));
                break;
            case "Int32":
                result = string.Format(@"{0}", ckInt(obj));
                break;
            case "Double":
                result = string.Format(@"{0}", ckDouble(obj));
                break;
            case "Boolean":
                result = string.Format(@"{0}", ckBool(obj));
                break;
            case "List`1":
                result = string.Format(@"{0}", ckList(obj));
                break;
            case "Ept":
                result = string.Format(@"{0}", string.Empty);
                break;
            default:
                result = string.Format(@"{0}", ckNull(obj));
                break;
        }

        return result;
    }

    /// <summary>
    /// slq의 함수인지 체크 후 bool값 리턴
    /// </summary>
    /// <param name="funcStr">함수명</param>
    /// <returns>string</returns>
    private static bool isFunc(this string funcStr)
    {
        bool result = false;
        switch (funcStr.ToUpper())
        {
            case "IS NOT NULL":
                result = true;
                break;
            case "IS NULL":
                result = true;
                break;
            case "NULL":
                result = true;
                break;
            case "GETDATE()":
                result = true;
                break;
        }


        return result;
    }

    /// <summary>
    /// 예외처리를 위한 사용자 정의 문자열 체크 후 bool값 리턴
    /// </summary>
    /// <param name="funcStr">문자열</param>
    /// <returns>string</returns>
    private static bool isUserFunc(this string funcStr)
    {
        bool result = false;
        switch (funcStr)
        {
            case "SPACE(*)":
                result = true;
                break;
            
            case ")":
                result = true;
                break;
        }
        
        return result;
    }

    private static string ckUserFunc(this string funcStr)
    {
        string result = string.Empty;

        //사용자 함수가 어니면 매개변수를 리턴
        //사용자 함수이면 빈값을 리턴
        if (!funcStr.isUserFunc())
            result = funcStr;

        return result;
    }

    /// <summary>
    /// mssql sp의 입력 파라메터 중 null인 값을 빈문자로 변환 후 리턴
    /// </summary>
    /// <param name="objs">Object[] objs </param>
    /// <returns>Object[]</returns>
    public static Object[] setEmpty(this Object[] objs)
    {
        for(var i = 0; i < objs.Length; i++)
        {
            if(objs[i] == null)
            {
                objs[i] = string.Empty;
            }
        }

        return objs;

    }

    /// <summary>
    /// mssql insert후 자동증가 identy 값을 리턴 받아서 입력 파라미터 obj에 할당
    /// </summary>
    /// <param name="obj">할당 받을 object</param>
    /// <param name="ds">자동증가 값</param>
    /// <param name="pkStr">object의 프로퍼티 명</param>
    public static void setProperty(Object obj, DataSet ds, string pkStr)
    {

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["idx"] != null)
        {
            int idx = Convert.ToInt32(ds.Tables[0].Rows[0]["idx"]);

            Type t = obj.GetType();
                        
            foreach(var pro in t.GetProperties())
            {
                if(pro.Name.Equals(pkStr))
                pro.SetValue(obj, idx);
            }
        }    
    }

    /// <summary>
    /// mssql insert후 자동증가 identy 값을 리턴 받아서 입력 파라미터 obj에 할당
    /// </summary>
    /// <param name="obj">할당 받을 object</param>
    /// <param name="ds">자동증가 값</param>
    /// <param name="pkStr">object의 프로퍼티 명</param>
    public static void setProperty(Object obj, DataSet ds, webService _WS)
    {
        string pkStr = string.Empty;

        //pk칼럼명 가져오기(pk이고, 자동증가인 값)
        pkStr = DataContextHelpers.getPKUniq(_WS, obj);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["idx"] != DBNull.Value)
        {
            int idx = Convert.ToInt32(ds.Tables[0].Rows[0]["idx"]);

            Type tt = obj.GetType();

            foreach (var pro in tt.GetProperties())
            {
                if (pro.Name.Equals(pkStr))
                    pro.SetValue(obj, idx);
            }
        }
    }

    public static string getFirstWord(this string str)
    {
        string result = string.Empty;

        string temp = str.ToUpper();
        string[] sp = { " " };
        string[] tmpArr = temp.Split(sp, StringSplitOptions.RemoveEmptyEntries);

        result = tmpArr[0];

        return result;
    }

    /// <summary>
    /// 입력 받은 dataset이 null이거나 테이블이 없다면 
    /// 빈 테이블 삽입 후 리턴
    /// </summary>
    /// <param name="pDs">DataSet</param>
    /// <returns></returns>
    public static DataSet ckDS(this DataSet pDs)
    {
        if (pDs == null || pDs.Tables.Count ==0)
        {
            DataSet result = new DataSet();
            result.Tables.Add();

            return result;
        }
        else
        {
            return pDs;
        }
    }

    public static bool isChar(this Type obj)
    {
        bool result = false;

        if (obj.Name.Equals("Char"))
        {
            result = true;
        }

        if (obj.Name.Equals("Nullable`1"))
        {
            if (Nullable.GetUnderlyingType(obj).Name.Equals("Char"))
            {
                result = true;
            }
        }

        return result;
    }

    public static char toChar(this Object obj)
    {
        char[] result = obj.ToString().Trim('\0').ToCharArray();

        char ccc = ' ';
        if (result.Count() > 0)
            ccc = result[0];

        return ccc;
    }

    /// <summary>
    /// mssql 테이블명이나 키워드 용으로 입력받은 파라미터 에 [] 붙여서 리턴
    /// </summary>
    /// <param name="tName"></param>
    /// <returns></returns>
    public static string ckKeyword(this string tName)
    {
        string result = tName;

        if(tName.IndexOf("[") == -1)
            result = "[" + tName + "]";

        return result;
    }

    /// <summary>
    /// 관리자와 사용자에 따라서 설정값을 true, false로 세팅해줌.
    /// </summary>
    /// <returns></returns>
    public static bool chServerState()
    {
        var user = new UserInfo();

        //관리자일 경우
        if (user.AdminLoginCheck)
            return false; //관리자로 접근
        else
            return true; //사용자 접근
    }

    public enum webService
    {
        MAIN = 0,
        AUTH = 1,
        STORE = 2
    }
}