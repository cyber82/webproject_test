﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using NLog;
using System.Net;

public class SocialData {
	public List<Post> items{
		get;
		set;
	}

	public string prev{
		get;
		set;
	}

	public string next{
		get;
		set;
	}

	public class Post {

		public string provider {		// sns 서비스 제공자
			get;
			set;
		}

		public string id {
			get;
			set;
		}

		public string author {
			get;
			set;
		}

		public string title {
			get;
			set;
		}

		public string content {
			get;
			set;
		}

		public string link {
			get;
			set;
		}

		public string picture {
			get;
			set;
		}

		public DateTime regdate {
			get;
			set;
		}

		public int likeCount {
			get;
			set;
		}

		public int commentCount {
			get;
			set;
		}

		public object rawdata {
			get;
			set;
		}
	}
}

public interface ISocialService {

	SocialData GetPosts(int size , string pageVal);
	
	

}
