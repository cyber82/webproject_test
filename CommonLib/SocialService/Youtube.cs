﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using NLog;
using System.Net;
using System.Web.Helpers;

public class Youtube : ISocialService {

	HttpContext context;
	public Youtube(HttpContext context) {
		this.context = context;
	}


	public SocialData GetPosts(int size, string pageVal) {

		string url = string.Format(ConfigurationManager.AppSettings["youtubePostPath"], size, pageVal);

		using (WebClient wc = new WebClient()) {
			wc.Encoding = System.Text.Encoding.UTF8;
			string str = wc.DownloadString(url).Trim();

			dynamic data = Json.Decode(str);

			var items = new List<SocialData.Post>();

			foreach (var item in data.items) {

				items.Add(new SocialData.Post() {
					provider = "youtube", 
					title = item.snippet.title,
					content = item.snippet.description,
					link = item.id.videoId,
					picture = item.snippet.thumbnails.medium.url,
					author = item.snippet.channelTitle,
					regdate = DateTime.Parse(item.snippet.publishedAt),
					//rawdata = Json.Encode(item)	// 성능을 위해 일단 막음
				});

			}

			return new SocialData() {
				next = data.nextPageToken, prev = data.prevPageToken, items = items
			};

		}

	}

}


/*
 {
 "kind": "youtube#playlistItemListResponse",
 "etag": "\"dhbhlDw5j8dK10GxeV_UG6RSReM/OusWAqgxWz7FMg8ctVJJN8Mfm_w\"",
 "nextPageToken": "CAEQAA",
 "pageInfo": {
  "totalResults": 3,
  "resultsPerPage": 1
 },
 "items": [
  {
   "kind": "youtube#playlistItem",
   "etag": "\"dhbhlDw5j8dK10GxeV_UG6RSReM/Pn7_Hg_4j_A5wdXmmPB_kWOdlDg\"",
   "id": "PL5iwL1ytMsRJoD_KeNaiaalLPMN6tS6c5HA_wKR8yIAY",
   "snippet": {
    "publishedAt": "2015-06-04T07:46:41.000Z",
    "channelId": "UCACPqYs0JFSGqe3Yb82wUdw",
    "title": "2015.05.01 윤서",
    "description": "",
    "thumbnails": {
     "default": {
      "url": "https://i.ytimg.com/vi/FxEZWW6Urgc/default.jpg",
      "width": 120,
      "height": 90
     },
     "medium": {
      "url": "https://i.ytimg.com/vi/FxEZWW6Urgc/mqdefault.jpg",
      "width": 320,
      "height": 180
     },
     "high": {
      "url": "https://i.ytimg.com/vi/FxEZWW6Urgc/hqdefault.jpg",
      "width": 480,
      "height": 360
     }
    },
    "channelTitle": "jaeil wee",
    "playlistId": "PLiaV0pfHbmvCR8fa9UWxTBiQcREaqaBlv",
    "position": 0,
    "resourceId": {
     "kind": "youtube#video",
     "videoId": "FxEZWW6Urgc"
    }
   }
  }
 ]
}
 * 
 */