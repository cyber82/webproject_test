﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using NLog;
using System.Net;
using System.Web.Helpers;

public class Facebook : ISocialService {

	HttpContext context;
	public Facebook(HttpContext context) {
		this.context = context;
	}

	string GetAccessToken() {

		string result = "";

		var facebookAppId = ConfigurationManager.AppSettings["facebookAppId"];
		var facebookAppSecret = ConfigurationManager.AppSettings["facebookAppSecret"];

		string url = string.Format("https://graph.facebook.com/oauth/access_token?client_id={0}&client_secret={1}&grant_type=client_credentials&redirect_uri=", facebookAppId , facebookAppSecret);
		
		using(WebClient wc = new WebClient()){
			wc.Encoding = System.Text.Encoding.UTF8;

			result = wc.DownloadString(url).Trim();
			
			return result;
		}
	}

	public SocialData GetPosts(int size , string pageVal) {

		string url = pageVal;

		if (string.IsNullOrEmpty(pageVal)) {

            string key = "";
            string token = GetAccessToken();
            dynamic data = Json.Decode(token);
            foreach (KeyValuePair<string, object> item in data)
            {
                if (string.IsNullOrEmpty(key))
                    key = item.Value.ToString();
            }
            key = "access_token=" + key;
            url = string.Format(ConfigurationManager.AppSettings["facebookPostPath"], size, key);

			//url = string.Format(ConfigurationManager.AppSettings["facebookPostPath"], size, GetAccessToken());
			//url = "https://graph.facebook.com/compassion.Korea/posts?fields=message,full_picture,created_time,link,likes,name,comments,object_id&limit=10&access_token=896920290412806|4mMlCTqWABxuhbSiFkiGVa8EWkE";
		}
	
		using(WebClient wc = new WebClient()){
			wc.Encoding = System.Text.Encoding.UTF8;

			string str = wc.DownloadString(url);
			//context.Response.Write(str);
			dynamic data = Json.Decode(str);
			
			var items = new List<SocialData.Post>();
		
			foreach (var item in data.data) {

				var id = item.id.IndexOf('_') > -1 ? item.id.Split('_')[1] : item.id;

				items.Add(new SocialData.Post() {
					provider = "facebook" , 
					content = item.message,
					link = item.link,
					//picture = item.picture,
					picture=item.full_picture,
					//author = item.from.name,
					regdate = DateTime.Parse(item.created_time),
					likeCount = item.likes != null ? item.likes.data.Length : 0,
					commentCount = item.comments != null ? item.comments.data.Length : 0,
                    //id = item.id
                    //id = item.object_id
					id = id
					//rawdata = Json.Encode(item)	// 성능을 위해 일단 막음
				});
			}

			return new SocialData() {
				next = data.paging.next, prev = data.paging.previous, items = items
			};

		}
			


	}

}

/*
 {
   "data": [
      {
         "id": "488536094544255_904549246276269",
         "from": {
            "category": "Building Materials",
            "name": "\ub300\ub9bc\ubc14\uc2a4",
            "id": "488536094544255"
         },
         "message": "[\uc774\ubca4\ud2b8 \ub2f9\ucca8\uc790 \ubc1c\ud45c]\n '\uc9c0\uae08 \uc6b0\ub9ac\uc9d1\uc5d0 \uac00\uc7a5 \ud544\uc694\ud55c \uc544\uc774\ud15c\uc740?' \n\uc9dd\uc9dd\uc9dd! \ucd95\ud558\ud569\ub2c8\ub2e4 \u003C3\n\n\ub2f9\ucca8 \uba85\ub2e8\uacfc \uc544\ub798 \uc774\ubbf8\uc9c0 \uc218\ub839 \ubc29\ubc95\uc744 \ud655\uc778\ud6c4,\n [\uc6b0\ub9ac\uc9d1\uc544\uc774\ud15c]/\uc774\ub984/\uc5f0\ub77d\ucc98\ub97c \ub300\ub9bc\ubc14\uc2a4 \uba54\uc2dc\uc9c0\ub85c \ubcf4\ub0b4\uc8fc\uc138\uc694\n \uc774\ubca4\ud2b8\uc5d0 \ucc38\uc5ec\ud574 \uc8fc\uc154\uc11c \uac10\uc0ac\ud569\ub2c8\ub2e4 :)\n\n\ub2f9\ucca8\uc790 \uba85\ub2e8\n(\ubca0\uc2a4\ud0a8\ub77c\ube48\uc2a4 \uc2f1\uae00 \ud0b9 \uc544\uc774\uc2a4\ud06c\ub9bc \ubaa8\ubc14\uc77c\uad50\ud658\uad8c)\n\n\uae40\uc560\ub9ac, \uc624\ube5b\ub098\ub77c, \uc774\uc720\ub9bc, \uc8fc\ud76c\ub791, \uc591\ubbf8\ub780",
         "picture": "https://scontent.xx.fbcdn.net/hphotos-xtf1/v/t1.0-9/p130x130/11063791_904549246276269_9033892674937950063_n.png?oh=f28e2869773aa32f35b7c9a7f31f8324&oe=560007DD",
         "link": "https://www.facebook.com/daelimbath.co/photos/a.491389674258897.1073741828.488536094544255/904549246276269/?type=1",
         "icon": "https://www.facebook.com/images/icons/photo.gif",
         "privacy": {
            "value": "",
            "description": "",
            "friends": "",
            "allow": "",
            "deny": ""
         },
         "type": "photo",
         "status_type": "added_photos",
         "object_id": "904549246276269",
         "created_time": "2015-06-03T08:02:05+0000",
         "updated_time": "2015-06-03T12:11:03+0000",
         "shares": {
            "count": 1
         },
         "is_hidden": false,
         "is_expired": false,
         "likes": {
            "data": [
               {
                  "id": "865214313544526",
                  "name": "\uae40\ud6a8\uc815"
               },
               {
                  "id": "823251251084590",
                  "name": "\uc870\uc9c4\uc131"
               },
               {
                  "id": "660128247454630",
                  "name": "\uc591\uc2b9\ubbfc"
               },
               {
                  "id": "480082308823425",
                  "name": "\uae40\ubcd1\ucc3d"
               },
               {
                  "id": "1626217964259800",
                  "name": "\ubc29\uacbd\uc6d0"
               },
               {
                  "id": "664018880395836",
                  "name": "\uc815\uc131\ud61c"
               },
               {
                  "id": "483990175099849",
                  "name": "\uc774\uc720\ub9bc"
               },
               {
                  "id": "845712885504892",
                  "name": "\ud64d\uacbd\ubbf8"
               },
               {
                  "id": "726767397439667",
                  "name": "\uc591\uc724\uc790"
               },
               {
                  "id": "1615742605379583",
                  "name": "\uc870\uae38\uc21c"
               },
               {
                  "id": "687028751428831",
                  "name": "\uc8fc\ud76c\ub791"
               },
               {
                  "id": "1583737505233079",
                  "name": "\ubc30\ud604\uc815"
               },
               {
                  "id": "904726229574094",
                  "name": "\ubc15\uc9c0\uc740"
               },
               {
                  "id": "886713188051789",
                  "name": "\ubc15\ub974\ub124"
               },
               {
                  "id": "485905041574112",
                  "name": "\uc548\ud61c\uacbd"
               },
               {
                  "id": "838532926234949",
                  "name": "\uc591\ubbf8\ub780"
               },
               {
                  "id": "477677675714847",
                  "name": "\uad6c\uc601\uc219"
               }
            ],
            "paging": {
               "cursors": {
                  "after": "NDc3Njc3Njc1NzE0ODQ3",
                  "before": "ODY1MjE0MzEzNTQ0NTI2"
               }
            }
         },
         "comments": {
            "data": [
               {
                  "id": "904549246276269_904553459609181",
                  "from": {
                     "id": "838532926234949",
                     "name": "\uc591\ubbf8\ub780"
                  },
                  "message": "\uac10\uc0ac\ud574\uc694 \uba54\uc138\uc9c0\ubcf4\ub0b4\ub4dc\ub838\uc5b4\uc694 ^^",
                  "can_remove": false,
                  "created_time": "2015-06-03T08:11:30+0000",
                  "like_count": 3,
                  "user_likes": false
               },
               {
                  "id": "904549246276269_904605429603984",
                  "from": {
                     "id": "687028751428831",
                     "name": "\uc8fc\ud76c\ub791"
                  },
                  "message": "\uc9c0\uae30\ub2d8 \uac10\uc0ac\ud574\uc694..",
                  "can_remove": false,
                  "created_time": "2015-06-03T09:58:41+0000",
                  "like_count": 2,
                  "user_likes": false
               },
               {
                  "id": "904549246276269_904555342942326",
                  "from": {
                     "id": "729355740508944",
                     "name": "\uae40\uc560\ub9ac"
                  },
                  "message": "\uba54\uc2dc\uc9c0 \ubcf4\ub0c8\uc5b4\uc694 \uac10\uc0ac\ud569\ub2c8\ub2e4",
                  "can_remove": false,
                  "created_time": "2015-06-03T08:18:18+0000",
                  "like_count": 2,
                  "user_likes": false
               },
               {
                  "id": "904549246276269_904606106270583",
                  "from": {
                     "id": "848364118575673",
                     "name": "\ucd5c\uc2b9\ud76c"
                  },
                  "message": "\u0040\uc624\ube5b\ub098\ub77c \ub2d8 \ub2f9\ucca8 \ucd95\ud558\ud574\uc694",
                  "can_remove": false,
                  "created_time": "2015-06-03T10:02:22+0000",
                  "like_count": 2,
                  "user_likes": false
               },
               {
                  "id": "904549246276269_904618212936039",
                  "from": {
                     "id": "1615742605379583",
                     "name": "\uc870\uae38\uc21c"
                  },
                  "message": "\uc560\ub9ac~~\uc88b\uac9f\ub2e4^^",
                  "can_remove": false,
                  "created_time": "2015-06-03T10:12:29+0000",
                  "like_count": 2,
                  "user_likes": false
               },
               {
                  "id": "904549246276269_904654536265740",
                  "from": {
                     "id": "483990175099849",
                     "name": "\uc774\uc720\ub9bc"
                  },
                  "message": "\uac10\uc0ac\ud569\ub2c8\ub2e4 ^_^ \uba54\uc2dc\uc9c0 \ubcf4\ub0b4\ub4dc\ub838\uc5b4\uc694~",
                  "can_remove": false,
                  "created_time": "2015-06-03T11:30:39+0000",
                  "like_count": 1,
                  "user_likes": false
               },
               {
                  "id": "904549246276269_904619586269235",
                  "from": {
                     "id": "1404354603226242",
                     "name": "\uae40\ubbf8\uae40\ubbf8"
                  },
                  "message": "\ucd95\ud558\ub4dc\ub9bd\ub2c8\ub2e4",
                  "can_remove": false,
                  "created_time": "2015-06-03T10:17:38+0000",
                  "like_count": 1,
                  "user_likes": false
               },
               {
                  "id": "904549246276269_904643929600134",
                  "from": {
                     "id": "840302199388626",
                     "name": "\ubc15\uc601\uadfc"
                  },
                  "message": "\uae40\uc560\ub9ac\ub2d8 \ucd95\ud558\ud569\ub2c8\ub2e4 \u314e\u314e",
                  "message_tags": [
                     {
                        "id": "729355740508944",
                        "name": "\uae40\uc560\ub9ac",
                        "type": "user",
                        "offset": 0,
                        "length": 3
                     }
                  ],
                  "can_remove": false,
                  "created_time": "2015-06-03T10:58:15+0000",
                  "like_count": 2,
                  "user_likes": false
               },
               {
                  "id": "904549246276269_904555172942343",
                  "from": {
                     "id": "1109084569108921",
                     "name": "\uc804\uc218\uc601"
                  },
                  "message": "\uae40\uc560\ub9ac\ub2d8 \ucd95\ud558\ud574\uc694~",
                  "message_tags": [
                     {
                        "id": "729355740508944",
                        "name": "\uae40\uc560\ub9ac",
                        "type": "user",
                        "offset": 0,
                        "length": 3
                     }
                  ],
                  "can_remove": false,
                  "created_time": "2015-06-03T08:16:33+0000",
                  "like_count": 2,
                  "user_likes": false
               },
               {
                  "id": "904549246276269_904576396273554",
                  "from": {
                     "id": "1677386545825088",
                     "name": "\uc2e0\ud604\uc219"
                  },
                  "message": "\uc774\uc720\ub9bc \ub2d8 \ucd95\ud558\ud574\uc694",
                  "message_tags": [
                     {
                        "id": "483990175099849",
                        "name": "\uc774\uc720\ub9bc",
                        "type": "user",
                        "offset": 0,
                        "length": 3
                     }
                  ],
                  "can_remove": false,
                  "created_time": "2015-06-03T09:07:10+0000",
                  "like_count": 2,
                  "user_likes": false
               },
               {
                  "id": "904549246276269_904555306275663",
                  "from": {
                     "id": "886713188051789",
                     "name": "\ubc15\ub974\ub124"
                  },
                  "message": "\uc8fc\ud76c\ub791\ub2d8 \ucd95\ud558\ub4dc\ub824\uc694!!",
                  "message_tags": [
                     {
                        "id": "687028751428831",
                        "name": "\uc8fc\ud76c\ub791",
                        "type": "user",
                        "offset": 0,
                        "length": 3
                     }
                  ],
                  "can_remove": false,
                  "created_time": "2015-06-03T08:17:51+0000",
                  "like_count": 1,
                  "user_likes": false
               }
            ],
            "paging": {
               "cursors": {
                  "after": "MQ==",
                  "before": "MTE="
               }
            }
         }
      }
   ],
   "paging": {
      "previous": "https://graph.facebook.com/v2.2/488536094544255/posts?limit=10&since=1433318525&access_token=1589059901308630|CE1RyHsoJq859_79eY9YwlpeLcA&__paging_token=enc_AdDMaCQapp5KRAZC0skLBVBPvshZBCJu3FcgJwo8UXQPEGstj2QLuVEOAVR3M2v9ZBGBu9zyw6FbZBBYU5LpM3mLV8MziZAJ8h2mDY9YZCIELz8S7nLgZDZD&__previous=1",
      "next": "https://graph.facebook.com/v2.2/488536094544255/posts?limit=10&access_token=1589059901308630|CE1RyHsoJq859_79eY9YwlpeLcA&until=1431414098&__paging_token=enc_AdDfMG17OspNWKkMhdtN8z0b0jwuwgPP2jNWZCR7gA8rRhY0aLMlM257HsBlbgWOf0Dih4UxOnrcnU1nLiYvq0HdW00LZC4Gm3Aie8xFv5ZCsN5uQZDZD"
   }
}
 */