﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using NLog;
using System.Net;
using System.Web.Helpers;
using System.Net.Http;
using System.Threading.Tasks;
using CommonLib;

public class Instagram : ISocialService {

	HttpContext context;
	public Instagram(HttpContext context) {
		this.context = context;
	}

	public void Authorize(string client_id , string redirect_uri) {
		HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("https://api.instagram.com/oauth/authorize/?client_id={0}&redirect_uri={1}&response_type=code",
			client_id , redirect_uri
			));

		using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {

			var nextUri = response.ResponseUri.ToString();
			nextUri = nextUri.Substring(0, nextUri.LastIndexOf("?")) + HttpUtility.UrlEncode(nextUri.Substring(nextUri.LastIndexOf("?")));
			context.Response.Redirect(nextUri);

		}

	}

	Task<OAuthResponse> RequestAccessToken( string client_id , string client_secret , string code) {

		var client = new HttpClient {
			BaseAddress = new Uri("https://api.instagram.com/oauth/")
		};
		
		var request = new HttpRequestMessage(HttpMethod.Post, new Uri(client.BaseAddress, "access_token"));
		var myParameters = string.Format("client_id={0}&client_secret={1}&grant_type={2}&redirect_uri={3}&code={4}",
				client_id.UrlEncode(),
				client_secret.UrlEncode(),
				"authorization_code".UrlEncode(),
				(context.Request.Domain() + context.Request.Path).UrlEncode(),
				code.UrlEncode());

		//request.Content = new StringContent(myParameters);
		request.Content = new StringContent(myParameters, System.Text.Encoding.UTF8, "application/x-www-form-urlencoded");

		return client.ExecuteAsync<OAuthResponse>(request);

	}

	/*
 instagram 은 oauth 를 사용하기때문에, 사용자페이지에서 실시간으로 인스타그램 데이타를 가져올 수 없다. (반드시 사용자 인증과정을 거치기 때문)
 * 그래서, 관리자를 통해 최신 데이타를 oauth 인증 후 가져오고, 내부디비에 저장한다.
 * 사용자는 저장된 데이타를 조회한다.
 */
	public SocialData GetPosts(int size, string pageVal)
    {
		var items = new List<SocialData.Post>();

        using (MainLibDataContext dao = new MainLibDataContext())
        {
            //var data = dao.sns_post.Where(p => p.provider == "instagram").OrderByDescending(p => p.regdate).Take(size).ToList();
            var data = www6.selectQ<sns_post>("provider", "instagram", "regdate desc").Take(size).ToList();

            foreach (var item in data)
            {
                items.Add(new SocialData.Post()
                {
                    provider = item.provider,
                    content = item.content,
                    link = item.link,
                    picture = item.picture,
                    author = item.author,
                    regdate = item.regdate,
                    likeCount = item.like_count,
                    commentCount = item.comment_count
                    //rawdata = Json.Encode(item)	// 성능을 위해 일단 막음
                });
            }
        }

		return new SocialData() {
			next = null, prev = null, items = items
		};

	}

	public SocialData GetPostsWithAuth(int size, string pageVal) {

		var code = context.Request["code"].EmptyIfNull();
		var client_id = ConfigurationManager.AppSettings["instagramClientId"];
		var client_secret = ConfigurationManager.AppSettings["instagramClientSecret"];

		// querystring에 code를 넘겨받고 나서 access_token 조회가 가능함.
		if (string.IsNullOrEmpty(code)) {
			this.Authorize(client_id, context.Request.UrlEx());
			return null;
		}

		var access_token = this.RequestAccessToken(client_id, client_secret , code).Result.AccessToken;

	//	context.Response.Write("code > " + code + "<br>");
	//	context.Response.Write("access_token > " + access_token);

		if (string.IsNullOrEmpty(access_token)) {			
			throw new Exception("access_token is null");
		}

		var url = string.Format("https://api.instagram.com/v1/users/self/media/recent?access_token={0}&count={1}", access_token, size);
		using(WebClient wc = new WebClient()){
			wc.Encoding = System.Text.Encoding.UTF8;
			string str = wc.DownloadString(url);

			dynamic data = Json.Decode(str);

			var items = new List<SocialData.Post>();

			foreach (var item in data.data) {

				items.Add(new SocialData.Post() {
					provider = "instagram",
					id = item.id , 
					content = item.caption.text,
					link = item.link,
					picture = item.images.low_resolution.url,	/*low_resolution = 306 , thumbnail = 150 , standard_resolution = 612*/
					author = item.user.username,
					regdate = DateExtension.UnixTimeStampToDateTime(Convert.ToInt64(item.created_time)),
					likeCount = item.likes.count ,
					commentCount = item.comments.count
					//rawdata = Json.Encode(item)	// 성능을 위해 일단 막음
				});//
				
			}


			return new SocialData() {
				next = data.pagination.next_url, prev = null, items = items
			};


		}


	}


	public class UserInfo {
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>
		/// The identifier.
		/// </value>
		public int Id {
			get;
			set;
		}
		/// <summary>
		/// Gets or sets the username.
		/// </summary>
		/// <value>
		/// The username.
		/// </value>
		public string Username {
			get;
			set;
		}
		/// <summary>
		/// Gets or sets the full name.
		/// </summary>
		/// <value>
		/// The full name.
		/// </value>
		[JsonProperty("full_name")]
		public string FullName {
			get;
			set;
		}
		/// <summary>
		/// Gets or sets the profile picture.
		/// </summary>
		/// <value>
		/// The profile picture.
		/// </value>
		[JsonProperty("profile_picture")]
		public string ProfilePicture {
			get;
			set;
		}
	}

	public class OAuthResponse {
		/// <summary>
		/// Gets or sets the user.
		/// </summary>
		/// <value>
		/// The user.
		/// </value>
		public UserInfo User {
			get;
			set;
		}
		/// <summary>
		/// Gets or sets the access_ token.
		/// </summary>
		/// <value>
		/// The access_ token.
		/// </value>
		[JsonProperty("Access_Token")]
		public string AccessToken {
			get;
			set;
		}
	}

}


/*
 {"pagination":{},"meta":{"code":200},"data":[{"attribution":null,"tags":[],"type":"image","location":null,"comments":{"count":0,"data":[]},"filter":"Normal","created_time":"1435295613","link":"https:\/\/instagram.com\/p\/4YVXV7HGsm\/","likes":{"count":0,"data":[]},"images":{"low_resolution":{"url":"https:\/\/scontent.cdninstagram.com\/hphotos-xfa1\/t51.2885-15\/s320x320\/e15\/11423420_428089504035669_304891054_n.jpg","width":320,"height":320},"thumbnail":{"url":"https:\/\/scontent.cdninstagram.com\/hphotos-xfa1\/t51.2885-15\/s150x150\/e15\/11423420_428089504035669_304891054_n.jpg","width":150,"height":150},"standard_resolution":{"url":"https:\/\/scontent.cdninstagram.com\/hphotos-xfa1\/t51.2885-15\/e15\/11423420_428089504035669_304891054_n.jpg","width":640,"height":640}},"users_in_photo":[],"caption":{"created_time":"1435295613","text":"\uc704\uc724\uc11c","from":{"username":"weejaeil","profile_picture":"https:\/\/igcdn-photos-e-a.akamaihd.net\/hphotos-ak-xaf1\/t51.2885-19\/11379825_1629273443952236_1599922096_a.jpg","id":"2089406817","full_name":"\uc704\uc7ac\uc77c"},"id":"1015655680253388964"},"user_has_liked":false,"id":"1015655679037041446_2089406817","user":{"username":"weejaeil","profile_picture":"https:\/\/igcdn-photos-e-a.akamaihd.net\/hphotos-ak-xaf1\/t51.2885-19\/11379825_1629273443952236_1599922096_a.jpg","id":"2089406817","full_name":"\uc704\uc7ac\uc77c"}},{"attribution":null,"tags":[],"type":"image","location":null,"comments":{"count":0,"data":[]},"filter":"Normal","created_time":"1435295584","link":"https:\/\/instagram.com\/p\/4YVTyZHGsV\/","likes":{"count":0,"data":[]},"images":{"low_resolution":{"url":"https:\/\/scontent.cdninstagram.com\/hphotos-xaf1\/t51.2885-15\/s320x320\/e15\/11377494_1451532605166137_1674065190_n.jpg","width":320,"height":320},"thumbnail":{"url":"https:\/\/scontent.cdninstagram.com\/hphotos-xaf1\/t51.2885-15\/s150x150\/e15\/11377494_1451532605166137_1674065190_n.jpg","width":150,"height":150},"standard_resolution":{"url":"https:\/\/scontent.cdninstagram.com\/hphotos-xaf1\/t51.2885-15\/e15\/11377494_1451532605166137_1674065190_n.jpg","width":640,"height":640}},"users_in_photo":[],"caption":{"created_time":"1435295584","text":"\uc900\ube44\ubb3c","from":{"username":"weejaeil","profile_picture":"https:\/\/igcdn-photos-e-a.akamaihd.net\/hphotos-ak-xaf1\/t51.2885-19\/11379825_1629273443952236_1599922096_a.jpg","id":"2089406817","full_name":"\uc704\uc7ac\uc77c"},"id":"1015655435985512468"},"user_has_liked":false,"id":"1015655434727222037_2089406817","user":{"username":"weejaeil","profile_picture":"https:\/\/igcdn-photos-e-a.akamaihd.net\/hphotos-ak-xaf1\/t51.2885-19\/11379825_1629273443952236_1599922096_a.jpg","id":"2089406817","full_name":"\uc704\uc7ac\uc77c"}}]}
 * 
 */