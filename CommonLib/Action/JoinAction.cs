﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using System.Text;
using CommonLib;

public partial class JoinAction {

	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;

	public JoinAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
		
	}

	// 기업회원 중복여부 조회
	public JsonWriter CheckCompRegisterNo( string comRegistrationNo , string comName ) {

		var result = new JsonWriter() { success = false };
		result.success = false;

		if(string.IsNullOrEmpty(comRegistrationNo) || string.IsNullOrEmpty(comName)) {
			result.message = "필수정보 누락";
			return result;
		}


        using (AuthLibDataContext dao = new AuthLibDataContext())
        {
            //var entity = dao.tSponsorMaster.FirstOrDefault(p => p.ComRegistration == comRegistrationNo.Encrypt() && p.CurrentUse == "Y");
            var entity = www6.selectQFAuth<tSponsorMaster>("ComRegistration", comRegistrationNo.Encrypt(), "CurrentUse", "Y");
            if (entity != null)
            {
                result.success = true;
                result.data = new Dictionary<string, object> { { "exist", true }, { "confirm", entity.CertifyDate.HasValue } };       // confirm = 관리자가 승인했는지 여부
                return result;
            }
        }

		
		#region DAT 에서 검색
		
		//DataSet dsComSponsor = null;
		//try {
		//	dsComSponsor = _wwwService.listSponsor("", "", comRegistrationNo, "", "");

		//	if(dsComSponsor == null) {
		//		result.success = false;
		//		result.message = "가입여부 정보를 가져오지 못했습니다. \\r\\n관리자에게 문의해주세요.(101)";
				
		//		return result;
		//	}

		//} catch(Exception ex) {

		//	ErrorLog.Write(HttpContext.Current, 0, ex.Message);
		//	result.success = false;
		//	result.message = "가입여부 정보를 가져오는 중 오류가 발생하였습니다.(100)";
			
		//	return result;
		//}

		////Compass4에 가입정보가 없으면
		//if(dsComSponsor.Tables[0].Rows.Count == 0) {
		//	result.success = true;
		//	result.data = new Dictionary<string, object> { { "exist", false }, { "comp_register_no", comRegistrationNo } };
			
		//	return result;

		//	//Compass4에 가입정보가 있으면
		//} else if(dsComSponsor.Tables[0].Rows.Count > 0) {
		//	string sSponsorID = dsComSponsor.Tables[0].Rows[0]["SponsorID"].ToString();
		//	DataSet dsDouble = null;

		//	for(int i = 0; i < dsComSponsor.Tables[0].Rows.Count; i++) {
		//		sSponsorID = dsComSponsor.Tables[0].Rows[i]["SponsorID"].ToString();
		//		dsDouble = _wwwService.getSponsorDouble(sSponsorID);

		//		if(dsDouble.Tables[0].Rows.Count > 0) {
		//			break;
		//		}
		//	}

		//	//tSponsorDouble에 등록되어 있으면 
		//	if(dsDouble.Tables[0].Rows.Count > 0) {
		//		//tSponsorDouble에 등록된 정보제외
		//		DataSet dsExceptSponsor = _wwwService.exceptSponsorDouble("", comRegistrationNo, "", "");

		//		//tSponsorDouble에 등록된 정보제외했을경우 Row가 0건이면
		//		if(dsExceptSponsor.Tables[0].Rows.Count == 0) {
		//			/*
		//			 sjs = JavaScript.HeaderScript;
		//			sjs += JavaScript.GetPageMoveScript("GroupToCallcenter.aspx");
		//			sjs += JavaScript.FooterScript;
		//			Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
		//			*/
		//			result.success = false;
		//			result.action = "callcenter";
		//			result.message = "콜센터로 문의해주세요(정보없음3)";
					
		//			return result;
		//		}

		//		//tSponsorDouble에 등록된 정보제외했을경우 가입정보가 있지만 UserID가 없을경우 
		//		//수정 20120406
		//		if(dsExceptSponsor.Tables[0].Rows.Count > 0 && dsExceptSponsor.Tables[0].Rows[0]["UserID"].ToString() == "") {
		//			//MemberWebIDReg();
		//			result.success = false;
		//			result.action = "callcenter";
		//			result.message = "콜센터로 문의해주세요(아이디생성필요)";
					
		//			return result;
		//		}
		//		//수정 20120315 UserID가 있을경우
		//		if(dsExceptSponsor.Tables[0].Rows.Count == 1 && dsExceptSponsor.Tables[0].Rows[0]["UserID"].ToString() != "") {
		//			/*
		//		sjs = JavaScript.HeaderScript.ToString();
		//		sjs += JavaScript.GetPageMoveScript("NotExistIDToCallcenter.aspx?SponsorName=" + sName.ToString().Trim() + "&idxTab=2");
		//		sjs += JavaScript.FooterScript.ToString();
		//		Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs.ToString());
		//		return false;
		//		*/
		//			result.success = false;
		//			result.action = "callcenter";
		//			result.message = "콜센터로 문의해주세요(정보없음1)";
					
		//			return result;

		//		}

		//		//같은번호를 가진 데이터가 1건이상이면 콜센터 문의 페이지로 이동   
		//		if(dsExceptSponsor.Tables[0].Rows.Count > 1) {
		//			/*
		//			 sjs = JavaScript.HeaderScript;
		//			sjs += JavaScript.GetPageMoveScript("AlreadyToCallcenter.aspx?SponsorName=" + sName + "&idxTab=2");
		//			sjs += JavaScript.FooterScript;
		//			Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
		//			return false;
		//			*/
		//			result.success = false;
		//			result.action = "callcenter";
		//			result.message = "콜센터로 문의해주세요(중복정보)";
					
		//			return result;
		//		}
		//		//입력한 이름과 번호가 입력되어있는 데이터와 일치하지 않으면 콜센터 문의 페이지로 이동
		//		else if(dsExceptSponsor.Tables[0].Rows[0]["SponsorName"].ToString().Trim() != comName ||
		//				 dsExceptSponsor.Tables[0].Rows[0]["JuminID"].ToString().Trim() != comRegistrationNo) {
		//			/*
		//			 sjs = JavaScript.HeaderScript;
		//			sjs += JavaScript.GetPageMoveScript("AlreadyToCallcenter.aspx?SponsorName=" + sName + "&idxTab=2");
		//			sjs += JavaScript.FooterScript;
		//			Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
		//			*/
		//			result.success = false;
		//			result.action = "callcenter";
		//			result.message = "콜센터로 문의해주세요(이름불일치)";
					
		//			return result;
		//		} else {
		//			/*
		//			 sjs = JavaScript.HeaderScript;
		//			sjs += JavaScript.GetAlertScript("이미 가입하셨습니다.");
		//			sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx");
		//			sjs += JavaScript.FooterScript;
		//			Page.ClientScript.RegisterStartupScript(this.GetType(), "Msg", sjs);
		//			return false;
		//			*/
		//			result.success = false;
		//			result.action = "callcenter";
		//			result.message = "콜센터로 문의해주세요(이미가입됨)";
					
		//			return result;
		//		}
		//	}
		//	//tSponsorDouble에 등록되어있지 않으면 
		//	else if(dsDouble.Tables[0].Rows.Count == 0) {
		//		//Compass4에 가입정보가 있지만 UserID가 없을경우
		//		//수정 20120406
		//		if(dsComSponsor.Tables[0].Rows.Count > 0 && dsComSponsor.Tables[0].Rows[0]["UserID"].ToString() == "") {
		//			//MemberWebIDReg();
		//			result.success = false;
		//			result.action = "callcenter";
		//			result.message = "콜센터로 문의해주세요(아이디생성필요)";
					
		//			return result;
		//		}
		//		//수정 20120315 UserID가 있을경우
		//		else if(dsComSponsor.Tables[0].Rows.Count == 1 && dsComSponsor.Tables[0].Rows[0]["UserID"].ToString() != "") {
		//			/*
		//		sjs = JavaScript.HeaderScript.ToString();
		//		sjs += JavaScript.GetPageMoveScript("NotExistIDToCallcenter.aspx?SponsorName=" + sName.ToString().Trim() + "&idxTab=2");
		//		sjs += JavaScript.FooterScript.ToString();
		//		Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs.ToString());
		//		return false;
		//		*/
		//			result.success = false;
		//			result.action = "callcenter";
		//			result.message = "콜센터로 문의해주세요(정보없음2)";
					
		//			return result;
		//		}
		//		//같은번호를 가진 데이터가 1건이상이면 콜센터 문의 페이지로 이동   
		//		else if(dsComSponsor.Tables["tSponsorMaster"].Rows.Count > 1) {
		//			/*
		//			sjs = JavaScript.HeaderScript;
		//		sjs += JavaScript.GetPageMoveScript("AlreadyToCallcenter.aspx?SponsorName=" + sName + "&idxTab=2");
		//		sjs += JavaScript.FooterScript;
		//		Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
		//		return false;
		//			*/
		//			result.success = false;
		//			result.action = "callcenter";
		//			result.message = "콜센터로 문의해주세요(중복데이타)";
					
		//			return result;


		//		}
		//		//입력한 이름과 번호가 입력되어있는 데이터와 일치하지 않으면 콜센터 문의 페이지로 이동
		//		else if(dsComSponsor.Tables["tSponsorMaster"].Rows[0]["SponsorName"].ToString().Trim() != comName ||
		//				 dsComSponsor.Tables["tSponsorMaster"].Rows[0]["ComRegistration"].ToString().Trim() != comRegistrationNo) {
		//			/*
		//		sjs = JavaScript.HeaderScript;
		//		sjs += JavaScript.GetPageMoveScript("AlreadyToCallcenter.aspx?SponsorName=" + sName + "&idxTab=2");
		//		sjs += JavaScript.FooterScript;
		//		Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", sjs);
		//		return false;
		//		*/
		//			result.success = false;
		//			result.action = "callcenter";
		//			result.message = "콜센터로 문의해주세요(이름불일치)";
					
		//			return result;
		//		} else {
		//			/*
		//		sjs = JavaScript.HeaderScript;
		//		sjs += JavaScript.GetAlertScript("이미 가입하셨습니다.");
		//		sjs += JavaScript.GetPageMoveScript("/membership/Login.aspx");
		//		sjs += JavaScript.FooterScript;
		//		Page.ClientScript.RegisterStartupScript(this.GetType(), "Msg", sjs);
		//		return false;
		//		*/
		//			result.success = false;
		//			result.action = "callcenter";
		//			result.message = "콜센터로 문의해주세요(이미가입됨)";
					
		//			return result;
		//		}
		//	}
		//}
		#endregion

		result.success = true;
		result.data = new Dictionary<string, object> { { "exist", false }, { "comp_register_no", comRegistrationNo } };
		
		return result;

	}

	// 휴대폰 인증요청 SMS 발송
	public JsonWriter SendPhone( string name , string no , string birthDate )
    {
        //인증 시도 시, session 초기화
        HttpContext.Current.Session["offline.join.sponsorId"] = "";      // 세션에 저장하고 가입페이지에서 체크 및 조회한다.

        var result = new JsonWriter() { success = false };
		result.success = false;

		if(string.IsNullOrEmpty(name) || string.IsNullOrEmpty(no) || string.IsNullOrEmpty(birthDate.Replace("-" , "")))
        {
			result.message = "인증에 필요에 필수 정보가 부족합니다.(휴대폰번호,이름,생년월일)";
			return result;
		}
        
		try
        {
            using (AuthLibDataContext dao = new AuthLibDataContext())
            {
                var exist = www6.selectQAuth<tSponsorMaster>("SponsorName", name, "BirthDate", birthDate.Replace("-", "").Encrypt(), "Mobile", no.Encrypt());
                //if (dao.tSponsorMaster.Any(p => p.SponsorName == name && p.BirthDate == birthDate.Replace("-", "").Encrypt() && p.Mobile == no.Encrypt()))
                if(exist.Any())
                {
                    var entity = exist[0]; //dao.tSponsorMaster.First(p => p.SponsorName == name && p.BirthDate == birthDate.Replace("-", "").Encrypt() && p.Mobile == no.Encrypt());
                    var user_id = entity.UserID.Mask("*", 3);
                    result.data = new Dictionary<string, object> { { "has_account", true }, { "user_id", user_id } };
                    result.message = "이미 한국컴패션 웹회원으로 가입되어 있습니다.\n로그인 페이지로 이동하시겠습니까?\n아이디 : " + user_id;
                    result.action = "exist-online-id";
                    return result;
                }
            }

			Object[] objSql = new object[1] { string.Format(@"select top 1 sponsorId from tSponsorMaster with(nolock) where CurrentUse = 'Y' and sponsorId in (
select sponsorId from tSponsorCommunication where CommunicationType = '휴대번호' and CurrentUse = 'Y' and CommunicationContents='{0}') and sponsorName = '{1}' and birthdate = '{2}' and sponsorid not in (select sponsorid from tsponsordouble) ", no, name, birthDate) };
			DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

			if(ds.Tables[0].Rows.Count > 0)
            {
				var sponsorId = ds.Tables[0].Rows[0]["sponsorId"].ToString().Trim();
				result.success = false;

                using (AuthLibDataContext dao = new AuthLibDataContext())
                {
                    var exist = www6.selectQAuth<tSponsorMaster>("SponsorID", sponsorId, "CurrentUse", "Y");
                    //if (dao.tSponsorMaster.Any(p => p.SponsorID == sponsorId && p.CurrentUse == "Y"))
                    if(exist.Any())
                    {
                        var entity = exist[0]; //dao.tSponsorMaster.First(p => p.SponsorID == sponsorId && p.CurrentUse == "Y");
                        var user_id = entity.UserID.Mask("*", 3);
                        result.data = new Dictionary<string, object> { { "has_account", true }, { "user_id", user_id } };
                        result.message = "이미 한국컴패션 웹회원으로 가입되어 있습니다.\n로그인 페이지로 이동하시겠습니까?\n아이디 : " + user_id;
                        result.action = "exist-online-id";
                        return result;

                    }
                    //2018-04-17 이종진 - 웹간편회원모듈적용. 이미후원이면 인증 후에 체크함.
                    else
                    {
                        result.data = new Dictionary<string, object> { { "has_account", false }, { "user_id", "" } };
                        //result.message = "이미 후원회원으로 가입되어 있습니다.";
                        result.message = "이미 후원회원으로 가입되어 있습니다. 기존후원 간편가입 페이지로 이동합니다.";
                        result.action = "exist-offline-id";
                        HttpContext.Current.Session["offline.join.sponsorId"] = sponsorId;      // 세션에 저장하고 가입페이지에서 체크 및 조회한다.
                    }

                }

                //return result;
			}


			var code = this.MakeCertificationNumber(4);

            if (ConfigurationManager.AppSettings["stage"] == "dev") {
                //20180420 이종진 - 테스트환경에서도 실제 발송되도록 수정함
                if (ConfigurationManager.AppSettings["useSms"] == "Y")
                {
                    // SMS 발송로직 추가	
                    var sMobileRandom = util.SendSMS.SMSSend_DEV(no, name, code);
                    //휴대폰전송을 성공했을때 
                    if (sMobileRandom == "30")
                    {
                        result.success = false;
                        result.message = "SMS 발송 실패";
                    }
                    else
                    {
                        result.success = true;
                    }
                }
                else
                {
                    code = "1111";
                    result.success = true;
                }
            }
            else {
			
				// SMS 발송로직 추가	
				var sMobileRandom = util.SendSMS.SMSInfoSearch(no, name, code);
				//휴대폰전송을 성공했을때 
				if(sMobileRandom == "30") {
					result.success = false;
					result.message = "SMS 발송 실패";
				} else {
					result.success = true;
				}

			}

			HttpContext.Current.Session["sendPhoneCode"] = code;
			HttpContext.Current.Session["sendPhoneNo"] = no;

			result.success = true;
			return result;

		} catch(Exception ex) {

			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "오류가 발생했습니다. 잠시 뒤 다시 이용해주세요";
			result.success = false;
			return result;
		}


	}

	// 인증여부 확인
	public JsonWriter CheckPhone( string code) {

		var result = new JsonWriter() { success = false };
		result.success = false;

		if(string.IsNullOrEmpty(code) || HttpContext.Current.Session["sendPhoneCode"] == null) {

			result.message = "값이 유효하지 않습니다.";
			return result;
		}

		if(HttpContext.Current.Session["sendPhoneCode"].ToString() != code) {
            result.message = "인증번호가 일치하지 않습니다.";
            return result;
        }

        //2018-04-17 이종진 - 기존후원 웹간편가입으로 인해 추가
        //session에 sponsorid 가 있을 경우, 기존웹회원가입임. (번호 인증 시도 시에, session을 만들어줌)
        if (HttpContext.Current.Session["offline.join.sponsorId"] != null &&
            HttpContext.Current.Session["offline.join.sponsorId"].ToString() != "")
        {
            Object[] objSql = new object[1] { string.Format(@"select top 1 sponsorId from tSponsorMaster with(nolock) where CurrentUse = 'Y' and sponsorId in ('{0}') and sponsorid not in (select sponsorid from tsponsordouble)", HttpContext.Current.Session["offline.join.sponsorId"].ToString()) };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
            if(ds.Tables[0].Rows.Count > 0)
            {
                result.message = "exist-offline-id";
            }
        }

        result.data = HttpContext.Current.Session["sendPhoneNo"].ToString();
        result.success = true;
        return result;
        

	}

	// 이메일 인증요청 메일 보내기
	public JsonWriter SendEmail(string email , string name , string birthDate) {

        //인증 시도 시, session 초기화
        HttpContext.Current.Session["offline.join.sponsorId"] = "";      // 세션에 저장하고 가입페이지에서 체크 및 조회한다.

        var result = new JsonWriter() { success = false };
		
		if(string.IsNullOrEmpty(email) || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(birthDate.Replace("-",""))) {

			result.message = "인증에 필요에 필수 정보가 부족합니다.(이메일,이름,생년월일)";
			return result;
		}

		try {

            using (AuthLibDataContext dao = new AuthLibDataContext())
            {
                var exist = www6.selectQAuth<tSponsorMaster>("SponsorName", name, "BirthDate", birthDate.Replace("-", "").Encrypt(), "Email", email.Encrypt());
                //if (dao.tSponsorMaster.Any(p => p.SponsorName == name && p.BirthDate == birthDate.Replace("-", "").Encrypt() && p.Email == email.Encrypt()))
                if(exist.Any())
                {
                    var entity = exist[0]; //dao.tSponsorMaster.First(p => p.SponsorName == name && p.BirthDate == birthDate.Replace("-", "").Encrypt() && p.Email == email.Encrypt());
                    var user_id = entity.UserID.Mask("*", 3);
                    result.data = new Dictionary<string, object> { { "has_account", true }, { "user_id", user_id } };
                    result.message = "이미 한국컴패션 웹회원으로 가입되어 있습니다.\n로그인 페이지로 이동하시겠습니까?\n아이디 : " + user_id;
                    result.action = "exist-online-id";
                    return result;
                }
            }

			Object[] objSql = new object[1] { string.Format(@"select top 1 sponsorId from tSponsorMaster with(nolock) where CurrentUse = 'Y' and sponsorId in (
select sponsorId from tSponsorCommunication where CommunicationType = '이메일' and CurrentUse = 'Y' and CommunicationContents='{0}') and sponsorName = '{1}' and birthdate = '{2}' and sponsorid not in (select sponsorid from tsponsordouble) ", email , name , birthDate) };
			DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

			if(ds.Tables[0].Rows.Count > 0) {

				var sponsorId = ds.Tables[0].Rows[0]["sponsorId"].ToString().Trim();
				result.success = false;

                using (AuthLibDataContext dao = new AuthLibDataContext())
                {
                    var exist = www6.selectQAuth<tSponsorMaster>("SponsorID", sponsorId, "CurrentUse", "Y");
                    //if (dao.tSponsorMaster.Any(p => p.SponsorID == sponsorId && p.CurrentUse == "Y"))
                    if(exist.Any())
                    {
                        var entity = exist[0]; //dao.tSponsorMaster.First(p => p.SponsorID == sponsorId && p.CurrentUse == "Y");
                        var user_id = entity.UserID.Mask("*", 3);
                        result.data = new Dictionary<string, object> { { "has_account", true }, { "user_id", user_id } };
                        result.message = "이미 한국컴패션 웹회원으로 가입되어 있습니다.\n로그인 페이지로 이동하시겠습니까?\n아이디 : " + user_id;
                        result.action = "exist-online-id";
                        return result;
                    }
                    //2018-04-17 이종진 - 웹간편회원모듈적용. 이미후원이면 인증 후에 체크함.
                    else
                    {
                        result.data = new Dictionary<string, object> { { "has_account", false }, { "user_id", "" } };
                        result.message = "이미 후원회원으로 가입되어 있습니다. 기존후원 간편가입 페이지로 이동합니다.";
                        //result.message = "이미 후원회원으로 가입되어 있습니다.";
                        result.action = "exist-offline-id";
                        HttpContext.Current.Session["offline.join.sponsorId"] = sponsorId;      // 세션에 저장하고 가입페이지에서 체크 및 조회한다.
                        //return result;
                    }

                }
            }

			var code = "";
            using (AuthLibDataContext dao = new AuthLibDataContext())
            {
                //code = dao.sp_user_email_auth_insert_f(email).First().ue_id.ToString();
                Object[] op1 = new Object[] { "ue_email" };
                Object[] op2 = new Object[] { email };
                var list = www6.selectSPAuth("sp_user_email_auth_insert_f", op1, op2).DataTableToList<sp_user_email_auth_insert_fResult>();

                code = list.First().ue_id.ToString();
            }

			var args = new Dictionary<string, string>() {
				{ "{code}" , code }
			};

			HttpContext.Current.Session["sendEmailCode"] = code;

			// to user
			Email.Send(HttpContext.Current, Email.SystemSender, new List<string>() { email }
				, ConfigurationManager.AppSettings["mail_title_email_confirm"]
				, "/mail/email-verify.html"
                , args
			, null);

			result.success = true;
			return result;

		} catch(Exception ex) {

			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "오류가 발생했습니다. 잠시 뒤 다시 이용해주세요";
			result.success = false;
			return result;
		}


	}

	// 이메일 인증완료
	public JsonWriter CheckEmail( string email ) {
		
		var result = new JsonWriter() { success = false };
		result.success = false;

		if(string.IsNullOrEmpty(email) || HttpContext.Current.Session["sendEmailCode"] == null) {
			result.message = "값이 유효하지 않습니다.";
			return result;
		}

        using (AuthLibDataContext dao = new AuthLibDataContext())
        {
            try
            {
                //var entity = dao.user_email_auth.First(p => p.ue_email == email && p.ue_id == Guid.Parse(HttpContext.Current.Session["sendEmailCode"].ToString()));
                var entity = www6.selectQFAuth<user_email_auth>("ue_email", email, "ue_id", Guid.Parse(HttpContext.Current.Session["sendEmailCode"].ToString()));
                if (entity.ue_active)
                {
                    result.data = email;
                    result.success = true;

                    //2018-04-17 이종진 - 기존후원 웹간편가입으로 인해 추가
                    //session에 sponsorid 가 있을 경우, 기존웹회원가입임. (메일 인증 시도 시에, session을 만들어줌)
                    if (HttpContext.Current.Session["offline.join.sponsorId"] != null &&
                        HttpContext.Current.Session["offline.join.sponsorId"].ToString() != "")
                    {
                        Object[] objSql = new object[1] { string.Format(@"select top 1 sponsorId from tSponsorMaster with(nolock) where CurrentUse = 'Y' and sponsorId in ('{0}') and sponsorid not in (select sponsorid from tsponsordouble)", HttpContext.Current.Session["offline.join.sponsorId"].ToString()) };
                        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            result.message = "exist-offline-id";
                            return result;
                        }
                    }
                }
                else
                {
                    result.message = "인증되지 않았습니다.";
                    result.success = false;
                }



            }
            catch (Exception ex)
            {

                ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                result.message = "오류가 발생했습니다. 잠시 뒤 다시 이용해주세요";
                result.success = false;
                return result;
            }

        }
		return result;
	}

	// 웹회원아이디 중복 검사 
	public JsonWriter CheckUserId(string userId) {
		
		var result = new JsonWriter() { success = false };
		
		if(string.IsNullOrEmpty(userId)) {
			result.message = "필수정보 누락";
			return result;
		}

		try {
            using (AuthLibDataContext dao = new AuthLibDataContext())
            {
                //var exist = dao.tSponsorMaster.Any(p => p.UserID == userId);
                var exist = www6.selectQAuth<tSponsorMaster>("UserID", userId).Any();

                result.success = true;
                result.data = new Dictionary<string, object> { { "exist", exist }, { "user_id", userId } };

                return result;
            }
		} catch(Exception ex) {

			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "오류가 발생했습니다. 잠시 뒤 다시 이용해주세요";
			result.success = false;
			return result;
		}
	}

	// 인증코드 발생
	public string MakeCertificationNumber( int length ) {
		StringBuilder result = new StringBuilder();

		int i;
		Random random = new Random();
		for(i = 0; i < length; i++)
			result.Append(random.Next(0, 10));

		return result.ToString();
	}

}