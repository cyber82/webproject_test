﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;
using Newtonsoft.Json;
using TCPTModel.Response.Beneficiary;

public partial class CADAction {

	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;
	public CADAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

	}

    // [이종진] CAD 결연요청 가능 여부
    public JsonWriter CanRequestCheck(string globalId, string childKey)
    {
        JsonWriter result = new JsonWriter() { success = false };

        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        JsonWriter actionResult;
        ChildAction childAction = new ChildAction();

        var sess = new UserInfo();

        var sponsorId = sess.SponsorID;
        
        if (string.IsNullOrEmpty(sponsorId))
        {
            result.message = "CAD 서비스는 후원회원으로 등록되신 애드보킷이나 어린이를 양육 중이신 후원자만 참여 가능합니다";
            return result;
        }

        actionResult = new SponsorAction().HasCDSP();
        if (!actionResult.success)
        {
            result.message = actionResult.message;
            return result;
        }


        var isAdvocate = new VOCAction().CheckUser().success;

        if (((DataTable)actionResult.data).Rows.Count < 1 && !isAdvocate)
        {
            result.success = false;
            result.message = "CAD 서비스는 애드보킷이나 어린이를 양육 중이신 후원자만 참여 가능합니다";
            return result;
        }

        // 요청한 어린이가 이미 결연됬는지
        if (!string.IsNullOrEmpty(childKey))
        {
            actionResult = childAction.CheckApplyByChildKey(childKey);
            if (!actionResult.success && actionResult.action == "exist")
            {
                result.message = actionResult.message;
                return result;
            }
        }

        // 1주일이내 결연신청한 내역
        using (MainLibDataContext dao = new MainLibDataContext())
        {
            //var listWithin7Days = dao.tCAD.Where(p => p.sponsorId == sess.SponsorID && p.regdate >= DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")).AddDays(-7)).Select(p => p.childKey).ToList();
            var listWithin7Days = www6.selectQ2<tCAD>("sponsorId = ", sess.SponsorID, "regdate >=", DateTime.Now.AddDays(-7)).Select(p => p.childKey).ToList();

            int requestCountWithin7Days = listWithin7Days.Count;

            // 1일이내 결연신청한 어린이가 결연됬는지
            foreach (var id in listWithin7Days)
            {
                actionResult = childAction.CheckApplyByChildKey(id);
                if (!actionResult.success)
                {
                    if (actionResult.action == "exist")
                    {        // 결연됨
                        requestCountWithin7Days--;
                    }
                    else
                    {
                        result.message = actionResult.message;
                        return result;
                    }
                }
            }

            if (requestCountWithin7Days > 0)
            {
                result.success = false;
                result.message = "CAD 메시지를 보낸지 일주일이 지나지 않았습니다. 마이컴패션에서 추천 어린이의 결연 현황을 확인해 보세요!";
                result.action = "mypage";
                return result;
            }

            var iTimeStamp = _wwwService.GetDate().ToString("yyyyMMddHHmmssff2");
            result.data = iTimeStamp;   // tCAD ID
            result.success = true;
            return result;

        }

    }

    // CAD 결연요청 가능 여부
    public JsonWriter CanRequest( string globalId, string childKey ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		JsonWriter actionResult;
		ChildAction childAction = new ChildAction();

		var sess = new UserInfo();

		var sponsorId = sess.SponsorID;

        //[이종진]후원가능한 어린이인지 체크
        //[이종진]추가 - 나눔펀딩 어린이 선택 시,
        //BeneficiaryKit조회 (어린이 상세정보 조회)
        if (ConfigurationManager.AppSettings["dbgp_kind"] == "2")
        {
            string childMasterId = globalId;
            ChildAction.ChildItem item = new ChildAction.ChildItem();
            //오류
            if (string.IsNullOrEmpty(childMasterId) || string.IsNullOrEmpty(childKey))
            {
                result.success = false;
                result.message = "어린이 정보가 잘못되었습니다.";
                result.action = "child";
                return result;
            }
            // beneficiary kit 가져오기
            CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();
            string beneDetail = _OffRampService.Beneficiary_GET(childMasterId.Substring(2));
            TCPTModel.TCPTResponseMessage response = JsonConvert.DeserializeObject<TCPTModel.TCPTResponseMessage>(beneDetail);
            //실패
            if (!response.IsSuccessStatusCode)
            {
                // 실패
                result.success = false;
                result.message = "함께 할 수 없는 어린이 입니다.";
                result.action = "child";
                return result;
            }
        }

        if (string.IsNullOrEmpty(sponsorId)) {
			result.message = "CAD 서비스는 후원회원으로 등록되신 애드보킷이나 어린이를 양육 중이신 후원자만 참여 가능합니다";
			return result;
		}

		actionResult = new SponsorAction().HasCDSP();
		if(!actionResult.success) {
			result.message = actionResult.message;
			return result;
		}


		var isAdvocate = new VOCAction().CheckUser().success;
		
		if(((DataTable)actionResult.data).Rows.Count < 1 && !isAdvocate) {
			result.success = false;
			result.message = "CAD 서비스는 애드보킷이나 어린이를 양육 중이신 후원자만 참여 가능합니다";
			return result;
		}

		// 요청한 어린이가 이미 결연됬는지
		if(!string.IsNullOrEmpty(childKey)) {
			actionResult = childAction.CheckApplyByChildKey(childKey);
			if(!actionResult.success && actionResult.action == "exist") {
				result.message = actionResult.message;
				return result;
			}
		}

        // 1주일이내 결연신청한 내역
        using (MainLibDataContext dao = new MainLibDataContext())
        {
            //var listWithin7Days = dao.tCAD.Where(p => p.sponsorId == sess.SponsorID && p.regdate >= DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")).AddDays(-7)).Select(p => p.childKey).ToList();
            var listWithin7Days = www6.selectQ2<tCAD>("sponsorId = ", sess.SponsorID, "regdate >=", DateTime.Now.AddDays(-7)).Select(p => p.childKey).ToList();

            int requestCountWithin7Days = listWithin7Days.Count;

            // 1일이내 결연신청한 어린이가 결연됬는지
            foreach (var id in listWithin7Days)
            {
                actionResult = childAction.CheckApplyByChildKey(id);
                if (!actionResult.success)
                {
                    if (actionResult.action == "exist")
                    {        // 결연됨
                        requestCountWithin7Days--;
                    }
                    else
                    {
                        result.message = actionResult.message;
                        return result;
                    }
                }
            }

            if (requestCountWithin7Days > 0)
            {
                result.success = false;
                result.message = "CAD 메시지를 보낸지 일주일이 지나지 않았습니다. 마이컴패션에서 추천 어린이의 결연 현황을 확인해 보세요!";
                result.action = "mypage";
                return result;
            }

            var iTimeStamp = _wwwService.GetDate().ToString("yyyyMMddHHmmssff2");
            result.data = iTimeStamp;   // tCAD ID
            result.success = true;
            return result;

        }

	}

	// CAD 결연요청
	public JsonWriter Request(string CampaignID,  string globalId , string childKey , string childName , string msg ) {
		JsonWriter result = new JsonWriter() { success = false };

		
		result = this.CanRequest(globalId, childKey);
		if(!result.success)
			return result;

		UserInfo sess = new UserInfo();
		var sponsorId = sess.SponsorID;

        // 1주일이내 결연신청한 내역
        using (MainLibDataContext dao = new MainLibDataContext())
        {

            // 결연신청
            //var add_result = dao.sp_tCAD_insert_f(CampaignID, sess.SponsorID, globalId, childKey, msg).First();
            Object[] op1 = new Object[] { "c_id", "SponsorID", "globalId", "childKey", "msg" };
            Object[] op2 = new Object[] { CampaignID, sess.SponsorID, globalId, childKey, msg };
            var add_result = www6.selectSP("sp_tCAD_insert_f", op1, op2).DataTableToList<sp_tCAD_insert_fResult>().First();

            if (add_result.Result == "Y")
            {
                result.message = "신청되었습니다.";
                result.success = true;

                string campaignTitle = string.Format("CAD(1:1결연)-{0}({1})-{2}({3})", childName, childKey, sess.UserName, sess.ConId);
                // consign

                Object[] objSql = new object[1] { "sp_web_tCampaignMaster_cad_insert_f" };
                Object[] objParam = new object[] { "CampaignID", "CampaignName", "sponsorId", "spoonsorName", "childMasterId" };
                Object[] objValue = new object[] { CampaignID, campaignTitle, sess.SponsorID, sess.ConId, globalId };
                var dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];


            }
            else
            {
                result.message = add_result.Result_String;
                result.success = false;
            }

            return result;

        }

	}

	// CAD 결연목록
	public class CADItem {
		public DateTime regDate;
		public DateTime? startDate;
		public string nameKr;
		public string status;
		public string globalId;
		public string commitmentId;
		public DateTime birthDate;
		public string gender;
		public string childKey;
		public int total;
        public string country;

    }
	public JsonWriter GetList( int page , int rowsPerPage) {
		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

		var sponsorId = sess.SponsorID;
		if(string.IsNullOrEmpty(sponsorId)) {
			result.data = new List<CADItem>();
			result.success = true;
			return result;
		}

        using (MainLibDataContext dao = new MainLibDataContext())
        {
            //var list = dao.sp_tCAD_list_f(page, rowsPerPage, sponsorId).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "sponsorid" };
            Object[] op2 = new Object[] { page, rowsPerPage, sponsorId };
            var list = www6.selectSP("sp_tCAD_list_f", op1, op2).DataTableToList<sp_tCAD_list_fResult>();

            var childKeys = list.Select(p => p.childKey).ToJson().Replace("[", "").Replace("]", "").Replace("\"", "");

            Object[] objSql = new object[] { "sp_web_cad_list_f" };
            Object[] objParam = new object[] { "childKeys" };
            Object[] objValue = new object[] { childKeys };

            DataTable cadedList = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

            var data = new List<CADItem>();
            foreach (var item in list)
            {

                cadedList.DefaultView.RowFilter = string.Format("childKey = '{0}'", item.childKey);
                if (cadedList.DefaultView.Count > 0)
                {
                    var commitmentId = cadedList.DefaultView[0]["commitmentId"].ToString();
                    var namekr = cadedList.DefaultView[0]["namekr"].ToString();
                    var commitstatus = cadedList.DefaultView[0]["commitstatus"].ToString();
                    var childKey = cadedList.DefaultView[0]["childKey"].ToString();
                    var country = cadedList.DefaultView[0]["countryname"].ToString();
                    var gender = cadedList.DefaultView[0]["genderCode"].ToString() == "M" ? "남자" : "여자";
                    var birthDate = Convert.ToDateTime(cadedList.DefaultView[0]["birthDate"]);
                    var status = "";
                    DateTime? startDate = null;

                    if (string.IsNullOrEmpty(commitmentId))
                    {

                        if (DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")).Subtract(Convert.ToDateTime(item.regdate.ToString("yyyy-MM-dd"))).Days > 7)
                        {
                            status = "종료";
                        }
                        else
                        {
                            status = "대기";
                        }

                    }
                    else
                    {
                        // 결연되었지만 7일 이후에 결연된거면 다른 경우로 결연된것으로 처리
                        startDate = Convert.ToDateTime(cadedList.DefaultView[0]["startDate"].ToString());
                        if (DateTime.Parse(startDate.Value.ToString("yyyy-MM-dd")).Subtract(Convert.ToDateTime(item.regdate.ToString("yyyy-MM-dd"))).Days > 7)
                        {
                            status = "종료";
                        }
                        else
                        {
                            status = "결연";

                            // stamp 발급
                            new StampAction().Add("cad");

                        }

                    }

                    data.Add(new CADItem()
                    {
                        birthDate = birthDate,
                        childKey = childKey,
                        globalId = item.globalId,
                        commitmentId = commitmentId,
                        gender = gender,
                        nameKr = namekr,
                        regDate = item.regdate,
                        startDate = startDate,
                        status = status,
                        total = item.total.Value,
                        country = country
                    });

                }

            }

            result.data = data;
            result.success = true;
            return result;
        }
		
	}
	
}

