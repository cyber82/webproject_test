﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;


public partial class CodeAction {

	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;
	public CodeAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

	}

	public static string GetGenderByJumin(string ssn ) {
		if(string.IsNullOrEmpty(ssn))
			return "";
		if (ssn.Length < 7) {
			return "";
		}

		var gender = ssn.Substring(6, 1);
		if (gender == "1" || gender == "3" || gender == "5") {
			return "M";
		}

		return "F";

	}

	public static string ChannelType {
		get {
			if(AppSession.HasCookie(HttpContext.Current)) {
				return "mapp";
			}

			var url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
			if(url.IndexOf("compassionko.org") > -1) {
				if(url.Contains("m.compassionko.org") || url.Contains("auth.compassionko.org/m/")) {
					return "mWeb";
				} else {
					return "Web";
				}
			} else if(url.IndexOf("compassionkr.com") > -1) {
				if(url.Contains("m.compassionkr.com") || url.Contains("auth.compassionkr.com/m/")) {
					return "mWeb";
				} else {
					return "Web";
				}
			} else {
				if(url.Contains("m.compassion.or.kr") || url.Contains("auth.compassion.or.kr/m/")) {
					return "mWeb";
				} else {
					return "Web";
				}
			}

		}
	}
	// 은행정보 , CodeName , CodeID
	public JsonWriter Banks() {
		JsonWriter result = new JsonWriter() { success = false };

		try {
			DataSet ds = _wwwService.selectSystemCode("4000", "");
			if(ds != null) {
				result.data = ds.Tables[0];
				result.success = true;

			} else {
				result.message = "가상계좌발급을 위한 은행정보 조회에 실패했습니다.";
			}


		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "가상계좌발급을 위한 은행정보 조회에 실패했습니다.";


		}

		return result;
	}

	// 국가정보 , CodeName , CodeName
	public JsonWriter Countries() {
		JsonWriter result = new JsonWriter() { success = false };

		DataSet dsCountry = new DataSet();
		try {
			//getData
			dsCountry = _wwwService.listSystemCode("DMCountry", "Y");
			if(dsCountry == null) {
				result.message = "국가항목을 가져오지 못했습니다";
				return result;
			}

			DataTable dtCountryHouse = dsCountry.Tables[0];

			dtCountryHouse.DefaultView.Sort = "DisplayOrder";
			dtCountryHouse.DefaultView.RowFilter = "CodeName <> '한국'";

			result.data = dtCountryHouse.DefaultView.ToTable();
			result.success = true;
			return result;

		} catch(Exception ex) {

			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "국가항목을 가져오는 중 오류가 발생하였습니다";
			return result;

		}


	}

	// 후원대상 종류 조회 , 1depth =CIV , CSP등 | 2depth = PBCIV등
	public class SponsorTypeEntity {
		public string groupType {
			get;
			set;
		}
		public string codeId {
			get;
			set;
		}
		public string codeName {
			get;
			set;
		}
	}
	public JsonWriter SponsorTypes() {
		return SponsorTypes("");
	}
	public JsonWriter SponsorTypes( string groupType ) {
		JsonWriter result = new JsonWriter() { success = false };


		try {

			Object[] objSql = new object[1] { "sp_web_sponsorType_code_list" };
			Object[] objParam = new object[] { "groupType" };
			Object[] objValue = new object[] { groupType };
			var list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			var data = new List<SponsorTypeEntity>();
			foreach(DataRow dr in list.Rows) {
				data.Add(new SponsorTypeEntity() {
					groupType = dr["groupType"].ToString(),
					codeId = dr["codeId"].ToString(),
					codeName = dr["codeName"].ToString()
				});
			}

			result.data = data;
			result.success = true;
			return result;

		} catch(Exception ex) {

			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "후원대상종류를 가져오는 중 오류가 발생하였습니다";
			return result;

		}
	}

	// CMS 대상 은행 , 컬럼 : code , name
	public JsonWriter CMSBanks() {
		JsonWriter result = new JsonWriter() { success = false };


		try {

			Object[] objSql = new object[1] { "sp_web_cms_bank_f" };
			Object[] objParam = new object[] { };
			Object[] objValue = new object[] { };
			var list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			result.data = list;
			result.success = true;
			return result;

		} catch(Exception ex) {

			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "은행정보를 가져오는 중 오류가 발생하였습니다";
			return result;

		}


	}
}