﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;

public partial class StampAction {
	
	public StampAction() {
		
	}

	// 스탬프발급
	public JsonWriter Add(string s_id) {
		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

        using (MainLibDataContext dao = new MainLibDataContext())
        {
            //var data = dao.sp_stamp_user_insert_f(s_id, sess.UserId, sess.UserName).First();
            Object[] op1 = new Object[] { "s_id", "userId", "userName" };
            Object[] op2 = new Object[] { s_id, sess.UserId, sess.UserName };
            var data = www6.selectSP("sp_stamp_user_insert_f", op1, op2).DataTableToList<sp_stamp_user_insert_fResult>().First();


            result.success = data.result == "Y";
            result.message = data.msg;
        }
		
		return result;
	}

	// 스탬프 목록
	public JsonWriter GetList(string year) {
		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

        using (MainLibDataContext dao = new MainLibDataContext())
        {

            result.success = true;
            //result.data = dao.sp_stamp_list_f(sess.UserId, year).ToList();
            Object[] op1 = new Object[] { "userID", "year" };
            Object[] op2 = new Object[] { sess.UserId, year };
            result.data = www6.selectSP("sp_stamp_list_f", op1, op2).DataTableToList<sp_stamp_list_fResult>();
        }

		return result;
	}


	// 스탬프조회 가능한년도
	public JsonWriter GetYears() {
		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

        using (MainLibDataContext dao = new MainLibDataContext())
        {

            result.success = true;
            //result.data = dao.sp_stamp_year_f(sess.UserId).ToList();
            Object[] op1 = new Object[] { "userID" };
            Object[] op2 = new Object[] { sess.UserId };
            result.data = www6.selectSP("sp_stamp_year_f", op1, op2).DataTableToList<sp_stamp_year_fResult>();

        }

        return result;
	}

}

