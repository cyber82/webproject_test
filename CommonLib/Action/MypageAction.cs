﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;

public partial class MypageAction {

	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;
	public MypageAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
		
	}

	// 나의 어린이 목록 , childKey 가 있으면 선택된 어린이 상세정보도 같이 보여준다.
	public JsonWriter MainData() {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}
		
		var sess = new UserInfo();
		
		Dictionary<string, object> data = new Dictionary<string, object>();
		data.Add("sponsorname", sess.UserName);
		data.Add("count_cdsp", 0);
		data.Add("count_non_cdsp", 0);     // 특별한 나눔갯수
		data.Add("count_letter", 0);
		data.Add("count_make_funding", 0);      // 개설한 펀딩
		data.Add("count_join_funding", 0);      // 참여한 펀딩
		data.Add("count_cad", 0);               // CAD 추천수
		data.Add("count_stamp", 0);             // 획득 스탬프수
		data.Add("count_event", 0);             // 참여한 캠페인/이벤트수
		
	
		try {

			if(string.IsNullOrEmpty(sess.SponsorID)) {
				data.Add("child", "");

			} else { 
				Object[] objSql = new object[1] { "sp_web_mypage_main_f" };
				Object[] objParam = new object[] { "SponsorID" };
				Object[] objValue = new object[] { sess.SponsorID };
				DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

				var count_cdsp = 0;
				if(ds.Tables[0].Rows.Count > 0) {
					count_cdsp = Convert.ToInt32(ds.Tables[0].Rows[0]["total"].ToString());

					DataRow dr = ds.Tables[0].Rows[0];
					var row = new Dictionary<string, object>();

					string country = dr["CountryCode"].ToString();
					string days = Convert.ToDateTime(dr["startdate"]).DaysAgo().ToString("N0");
					row.Add("childMasterId", dr["ChildMasterID"].ToString());
					row.Add("childKey", dr["childKey"].ToString());
					row.Add("name", dr["NameKr"].ToString());
					row.Add("name_en", dr["NameEng"].ToString());
					row.Add("gender", dr["GenderCode"].ToString() == "M" ? "남자" : "여자");
					row.Add("age", Convert.ToDateTime(dr["BirthDate"].ToString()).GetAge().ToString());
					row.Add("birth", dr["BirthDate"].ToString());
					row.Add("country", country);
                    //row.Add("pic", ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()));
                    row.Add("pic", new ChildAction().GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()));
                    row.Add("days", days);
                    row.Add("paid", dr["paid"].ToString() == "Y");
                    


                    var sponsorTypeEng = dr["sponsorTypeEng"].ToString();
					var CanGift = true;
					var CanLetter = true;
					if(sponsorTypeEng == "CORRES" || sponsorTypeEng == "LDPCOR") {      // 편지후원자
						CanGift = false;
					} else if(sponsorTypeEng == "CHIMON" || sponsorTypeEng == "LDPMON") {        // 머니후원자
						CanLetter = false;
					}
					row.Add("CanGift", CanGift);
					row.Add("CanLetter", CanLetter);


					data.Add("child", row);
				} else {
					data.Add("child", "");
				}

				data["count_cdsp"] = count_cdsp;
				data["count_non_cdsp"] = Convert.ToInt32(ds.Tables[1].Rows[0]["count_non_cdsp"].ToString());     // 특별한 나눔갯수

			}

			data["sponsorname"] = sess.UserName;
			
			//	data["count_letter"] = Convert.ToInt32(ds.Tables[1].Rows[0]["count_letter"].ToString());
			data["count_letter"] = this.GetTotalLetter();

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var entity = dao.sp_mypage_summary_f(sess.UserId).FirstOrDefault();
                Object[] op1 = new Object[] { "UserId" };
                Object[] op2 = new Object[] { sess.UserId };
                var list = www6.selectSP("sp_mypage_summary_f", op1, op2).DataTableToList<sp_mypage_summary_fResult>();
                var entity = list.FirstOrDefault();

                if (entity != null)
                {
                    data["count_make_funding"] = entity.cnt_userfunding_create.ToString().ValueIfNull("0");
                    data["count_join_funding"] = entity.cnt_userfunding_join.ToString().ValueIfNull("0");
                    data["count_cad"] = entity.cnt_cad.ToString().ValueIfNull("0");
                    data["count_stamp"] = entity.cnt_stamp.ToString().ValueIfNull("0");
                    data["count_event"] = entity.cnt_event.ToString().ValueIfNull("0");

                    // 특별한 나눔에서 나눔펀딩수를 뺀다(normal로 참여한것만)
                    //var normal_count_join_funding = dao.ExecuteQuery<int>(string.Format().First();
                    string sqlStr = string.Format("select count(*) as cnt from tUserFundingUser uu inner join tUserFunding uf on uu.uu_uf_id = uf.uf_id and uu.userid = '{0}' and uu.uu_deleted = 0 and uf.uf_type = 'normal'", sess.UserId);
                    var normal_count_join_funding = Convert.ToInt32(www6.selectX(sqlStr).Rows[0]["cnt"]);
                                                            
                    var balance_count_non_cdsp = Convert.ToInt32(data["count_non_cdsp"]) - normal_count_join_funding;
                    data["count_non_cdsp"] = balance_count_non_cdsp < 0 ? 0 : balance_count_non_cdsp;
                }
            }

			result.data = data;
			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

	int GetTotalLetter() {

		var sess = new UserInfo();
		int total_receive = 0, total_send = 0, total_temp = 0 , total_ready = 0;
		{

			Object[] objSql = new object[1] { "Corr_MyPageLetterList2016" };
			Object[] objParam = new object[] { "DIVIS", "SponsorID" };
			Object[] objValue = new object[] { "TOTAL", sess.SponsorID };
			var list = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];

			if(list.Rows.Count > 0) {
				total_receive = Convert.ToInt32(list.Rows[0]["total_receive"]);
				total_send = Convert.ToInt32(list.Rows[0]["total_send"]);
			} 

		}

		{

			Object[] objSql = new object[1] { "sp_web_letter_corr_total_f" };
			Object[] objParam = new object[] { "SponsorID" };
			Object[] objValue = new object[] { sess.SponsorID };
			var list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			if(list.Rows.Count > 0) {
				total_temp = Convert.ToInt32(list.Rows[0]["total_temp"]);
				total_ready = Convert.ToInt32(list.Rows[0]["total_ready"]);
			} 

		}

		return total_receive + total_send + total_ready;
		
	}

}