﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;

public partial class CountryAction {

	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;
    //CommonLib.WWW7Service.SoaHelperSoap _www7Service;
    public CountryAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
        //_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient("SoaHelperSoap", "http://krds01.krpc.ci.org/WWW6Service_penta/SoaHelper.asmx");

        // _www7Service = new CommonLib.WWW7Service.SoaHelperSoapClient();
    }

	public class WeatherItem {

		public List<weatherSection> weather;

		public mainSection main;

		public int cod;

		public string name;

		public class mainSection {
			public float temp;
			public float pressure;
			public float humidity;
			public float temp_min;
			public float temp_max;
		}

		public class weatherSection {
			public int id;
			public string main;
			public string description;
			public string icon;
		}
	}

	public class TimeItem {
		public int dstOffset;
		public int rawOffset;
		public string status;
		public string timeZoneId;
		public string timeZoneName;
	}

	// 국가목록
	public JsonWriter GetList() {
		return GetList("");
	}


	// 국가목록
	public JsonWriter GetList(string order_name) {
		JsonWriter result = new JsonWriter() { success = false };
		try {
            //using (MainLibDataContext dao = new MainLibDataContext())
            //{
            //    result.success = true;
            //    result.data = dao.sp_country_list_f("", order_name).ToList();
            //    return result;
            //}
            string dbName = "SqlCompassionWeb";
            object[] objSql = new object[1] { "sp_country_list_f" };
            string dbType = "SP";
            object[] objParam = new object[2] { "region", "order_name" };
            object[] objValue = new object[2] { "", order_name };
            DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, objParam, objValue);
            List<sp_country_list_fResult> list = ds.Tables[0].DataTableToList<sp_country_list_fResult>();
            
            result.success = true;
            result.data = list;
            return result;

        } catch (Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
            //result.message = "국가정보를 조회중에 장애가 발생했습니다.";
            result.message = ex.Message;
            return result;
		}

	}

	// 지역별 국가목록
	public JsonWriter GetListByRegion(string region)
    {
		JsonWriter result = new JsonWriter() { success = false };
        
		try {
            using (MainLibDataContext dao = new MainLibDataContext())
            {
                result.success = true;
                //result.data = dao.sp_country_list_f(region, "").ToList();
                Object[] op1 = new Object[] { "region", "order_name" };
                Object[] op2 = new Object[] { region, "" };
                result.data = www6.selectSP("sp_country_list_f", op1, op2).DataTableToList<sp_country_list_fResult>();
                return result;
            }
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "국가정보를 조회중에 장애가 발생했습니다.";
			return result;
		}

	}

	// 선택된 국가의 시간 , 날짜 , 기온정보
	public JsonWriter GetInfo( string countryName )
    {
		JsonWriter result = new JsonWriter() { success = false };
        using (MainLibDataContext dao = new MainLibDataContext())
        {
            //var entity = dao.country.FirstOrDefault(p => p.c_name == countryName);
            var entity = www6.selectQF<country>("c_name", countryName);
            if (entity == null)
            {
                result.message = "국가정보가 등록되지 않음";
                return result;
            }

            return this.GetInfoById(entity.c_id);
        }

	}

	public JsonWriter GetInfoById(string countryCode) {
		JsonWriter result = new JsonWriter() { success = false };

		var weather_url = ConfigurationManager.AppSettings["openWeatherMapUrl"];
		var time_url = ConfigurationManager.AppSettings["timeOffsetUrl"];
		
		bool error = false;

		try {
            using (MainLibDataContext dao = new MainLibDataContext())
            {
                var exist = www6.selectQ<country>("c_id", countryCode);
                //if (!dao.country.Any(p => p.c_id == countryCode))
                if(!exist.Any())
                {
                    result.message = "국가정보가 등록되지 않음";
                    return result;
                }

                var entity = exist[0]; //dao.country.First(p => p.c_id == countryCode);
                entity.c_id = entity.c_id;
                var lat = entity.c_lat;
                var lng = entity.c_lng;

                // 1시간 마다 조회
                // 26개 국가
                // 날씨 : 분당 60회 제한
                // 시간 : 일별 2500회 제한
                if (!entity.c_api_update.HasValue || DateTime.Now.Subtract(entity.c_api_update.Value).TotalHours >= 1)
                {

                    #region 날씨정보 조회
                    // http://api.openweathermap.org/data/2.5/weather?appid=791be28114fdc434f3912168d837f009&lat=22.396428&lon=114.109497&units=metric
                    using (WebClient client = new WebClient())
                    {

                        weather_url = string.Format(weather_url, lat, lng);
                        Stream stream = client.OpenRead(weather_url);
                        WeatherItem weather = (new StreamReader(stream).ReadToEnd()).ToObject<WeatherItem>();

                        if (weather.cod == 200)
                        {
                            entity.c_loc_name = weather.name;
                            entity.c_temperature = weather.main.temp;
                            entity.c_temperature_min = weather.main.temp_min;
                            entity.c_weather = weather.weather[0].main;
                            entity.c_weather_icon = weather.weather[0].icon;
                        }
                        else
                        {
                            error = true;
                            ErrorLog.Write(HttpContext.Current, 0, "날씨정보 조회실패 " + weather.ToJson());
                        }

                    }
                    #endregion

                    #region 시간정보 조회
                    // https://maps.googleapis.com/maps/api/timezone/json?location=-16.290154,-63.588653&timestamp=1331766000&language=ko&key=AIzaSyDxCGxGZjodx4rpu98fNhIANSEyt0Vz-Yw
                    using (WebClient client = new WebClient())
                    {

                        var unixTimestamp = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        time_url = string.Format(time_url, lat, lng, unixTimestamp);

                        Stream stream = client.OpenRead(time_url);
                        TimeItem time = (new StreamReader(stream).ReadToEnd()).ToObject<TimeItem>();

                        if (time.status == "OK")
                        {
                            entity.c_timeoffset = time.rawOffset;

                        }
                        else
                        {
                            error = true;
                            ErrorLog.Write(HttpContext.Current, 0, "시간정보 조회실패 " + time.ToJson());
                        }

                    }
                    #endregion

                    if (!error)
                    {
                        entity.c_api_update = DateTime.Now;
                    }

                    //dao.SubmitChanges();
                    www6.update(entity);
                }

                result.success = true;
                result.data = entity;
                return result;
            }
		}catch(Exception ex) {
            
            ErrorLog.Write(HttpContext.Current, 0, "weather_url > " + weather_url);
            ErrorLog.Write(HttpContext.Current, 0,  ex.ToString());
			result.message = "국가정보를 조회중에 장애가 발생했습니다.";
			return result;
		}
		
	}

}

