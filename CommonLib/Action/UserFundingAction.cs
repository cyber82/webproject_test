﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;

public partial class UserFundingAction {

	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;
	public UserFundingAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

	}
	
	// 목록
	public JsonWriter GetList( int page, int rowsPerPage, string type, string sort , string keyword ) {
		JsonWriter result = new JsonWriter() { success = false };
		
		try {

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var list = dao.sp_tUserFunding_list_f(page, rowsPerPage, type, sort, keyword).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage", "uf_type", "sort", "keyword"};
                Object[] op2 = new Object[] { page, rowsPerPage, type, sort, keyword };
                var list = www6.selectSP("sp_tUserFunding_list_f", op1, op2).DataTableToList<sp_tUserFunding_list_fResult>();

                foreach (var item in list)
                {
                    item.sf_image = item.sf_image.WithFileServerHost();
                    item.sf_image_m = item.sf_image_m.WithFileServerHost();
                    item.sf_thumb = item.sf_thumb.WithFileServerHost();
                    item.sf_thumb_m = item.sf_thumb_m.WithFileServerHost();

                }
                result.data = list;
            }

			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "정보 조회에 실패했습니다.";
			return result;

		}

	}

	// 로그인 계정별 요약정보 , 총개설 , 총참여 , 개설자이미지등
	public JsonWriter GetUserSummary() {
		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.success = false;
			result.message = "로그인이 필요합니다.";
			return result;
		}

		try {

			var userId = new UserInfo().UserId;

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var entity = dao.sp_tUserFunding_summary_f(userId).FirstOrDefault();
                Object[] op1 = new Object[] { "UserID" };
                Object[] op2 = new Object[] { userId };
                var list = www6.selectSP("sp_tUserFunding_summary_f", op1, op2).DataTableToList<sp_tUserFunding_summary_fResult>();
                var entity = list.FirstOrDefault();

                entity.creator_image = entity.creator_image.WithFileServerHost();
                result.data = entity;
            }

			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "정보 조회에 실패했습니다.";
			return result;

		}

	}

	// 특정 개설자의 나눔후원 개설목록
	public JsonWriter GetCreateList( int page, int rowsPerPage, string startdate, string enddate) {
		JsonWriter result = new JsonWriter() { success = false };
		
		if(!UserInfo.IsLogin) {
			result.success = false;
			result.message = "로그인이 필요합니다.";
			return result;
		}
		
		try {

			var userId = new UserInfo().UserId;

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var list = dao.sp_tUserFunding_list_by_userId_f(page, rowsPerPage, userId, startdate, enddate).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage", "UserID", "startdate", "enddate" };
                Object[] op2 = new Object[] { page, rowsPerPage, userId, startdate, enddate };
                var list = www6.selectSP("sp_tUserFunding_list_by_userId_f", op1, op2).DataTableToList<sp_tUserFunding_list_by_userId_fResult>();

                foreach (var item in list)
                {
                    if (item.uf_type == "child")
                        item.sf_image = new ChildAction().GetChildImage(item.ChildMasterID, item.ChildKey);//ChildAction.GetPicByChildKey(item.ChildKey);
                    item.uf_image = item.uf_image.WithFileServerHost();

                }

                result.data = list;
            }

			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "정보 조회에 실패했습니다.";
			return result;

		}

	}

	// 참여목록
	public JsonWriter GetJoinList( int page, int rowsPerPage, string startdate, string enddate ) {
		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.success = false;
			result.message = "로그인이 필요합니다.";
			return result;
		}

		try {

			var userId = new UserInfo().UserId;

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var list = dao.sp_tUserFundingUser_list_f(page, rowsPerPage, userId, startdate, enddate).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage", "UserID", "startdate", "enddate" };
                Object[] op2 = new Object[] { page, rowsPerPage, userId, startdate, enddate };
                var list = www6.selectSP("sp_tUserFundingUser_list_f", op1, op2).DataTableToList<sp_tUserFundingUser_list_fResult>();

                foreach (var item in list)
                {
                    item.uf_image = item.uf_image.WithFileServerHost();
                }

                result.data = list;
            }

			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "정보 조회에 실패했습니다.";
			return result;

		}

	}

	// 1:1 결연 타입인 경우 컴파스에 campaign 등록
	public JsonWriter AddCampaign(string childMasterId , string childKey , string childName , int targetAmount , string startDate , string endDate) {
		JsonWriter result = new JsonWriter() { success = false };
		
		var sess = new UserInfo();
		var sponsorId = sess.SponsorID;

		if(string.IsNullOrEmpty(sponsorId)) {
			result.message = "후원회원아이디가 없습니다.";
			return result;
		}

		try {

			var iTimeStamp = _wwwService.GetDate().ToString("yyyyMMddHHmmssff2");

			Object[] objSql = new object[1] { "sp_web_tCampaignMaster_uf_insert_f" };
			Object[] objParam = new object[] { "CampaignID" , "CampaignName" , "sponsorId" , "spoonsorName" , "childMasterId", "TargetAmount" , "StartDate" , "EndDate" };
			Object[] objValue = new object[] { iTimeStamp , string.Format("나눔펀딩(1:1결연)-{0}({1})-{2}({3})", childName , childKey , sess.UserName , sess.ConId)  , sponsorId , sess.UserName , childMasterId, targetAmount  , startDate , endDate};
			var dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			result.data = iTimeStamp;
			result.message = dt.Rows[0]["msg"].ToString();
			result.success = dt.Rows[0]["result"].ToString() == "Y";
			return result;

			} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "캠페인 생성에 실패했습니다.";
			return result;

		}

	}

	// 미완료 나눔펀딩 조회 및 tSponsorCall 등록
	public void CheckFinishUserFunding() {

        using (MainLibDataContext dao = new MainLibDataContext())
        {
            //나눔펀딩미달공지

            var date = _wwwService.GetDate();
            var actionResult = new JsonWriter();
            //var list = dao.sp_tUserFunding_un_notification_fail_list().ToList();
            Object[] op1 = new Object[] {  };
            Object[] op2 = new Object[] {  };
            var list = www6.selectSP("sp_tUserFunding_un_notification_fail_list", op1, op2).DataTableToList<sp_tUserFunding_un_notification_fail_listResult>();

            if (list.Count > 0)
            {

                int i = 0;
                foreach (var item in list)
                {
                    i++;

                    actionResult = this.AddSponsorCall(date.AddSeconds(i).ToString("yyyyMMddHHmmssff2"), item.uf_id, "", "", "", "나눔펀딩 모금 결과", string.Format("모금 실패({0}% 모금완료)", item.per));
                    if (actionResult.success)
                    {
                        //dao.sp_message_log_create(item.uf_id, "나눔펀딩미달공지", "compass4");
                        Object[] op3 = new Object[] { "ml_ref_id", "ml_kind", "ml_method"};
                        Object[] op4 = new Object[] { item.uf_id, "나눔펀딩미달공지", "compass4" };
                        www6.selectSP("sp_message_log_create", op3, op4);
                    }

                    #region 메일발송
                    try
                    {

                        actionResult = new SponsorAction().GetCommunications(item.SponsorID);
                        if (!actionResult.success)
                        {
                            ErrorLog.Write(HttpContext.Current, 0, actionResult.message);
                            continue;
                        }

                        var email = ((SponsorAction.CommunicationResult)actionResult.data).Email;
                        if (string.IsNullOrEmpty(email))
                            continue;

                        var from = ConfigurationManager.AppSettings["emailUserFunding"];

                        var args = new Dictionary<string, string> {
                            {"{userName}" ,  item.SponsorName} ,
                            {"{typeName}" , item.uf_type == "child" ? "어린이 결연 펀딩" : "양육을 돕는 펀딩"} ,
                            {"{duration}" , string.Format("{0} ~ {1}" , item.uf_date_start.ToString("yyyy.MM.dd") , item.uf_date_end.ToString("yyyy.MM.dd")) } ,
                            {"{goal_amount}" , item.uf_goal_amount.ToString("N0") } ,
                            {"{current_amount}" , item.uf_current_amount.ToString("N0") } ,
                            {"{title}" , item.uf_title} ,
                            {"{uf_id}" , item.uf_id.ToString()}
                        };

                        Email.Send(HttpContext.Current, from, new List<string>() { email },
                            "[한국컴패션] 나눔펀딩 모금이 종료되었습니다.", " /mail/uf-expire.html",
                            args, null);

                    }
                    catch (Exception e)
                    {
                        ErrorLog.Write(HttpContext.Current, 0, e.Message);
                        throw e;
                    }

                    #endregion
                }
            }
        }

	}

	// 상담내역 남김
	public JsonWriter AddSponsorCall( int uf_id, string CenterLClass, string CenterMClass, string CenterSClass, string CenterQuestion, string CenterContext ) {
		var iTimeStamp = _wwwService.GetDate().ToString("yyyyMMddHHmmssff2");
		return this.AddSponsorCall(iTimeStamp, uf_id, CenterLClass, CenterMClass, CenterSClass, CenterQuestion, CenterContext);
	}

	public JsonWriter AddSponsorCall(string ProcessID , int uf_id , string CenterLClass, string CenterMClass, string CenterSClass, string CenterQuestion, string CenterContext ) {
		JsonWriter result = new JsonWriter() { success = false };
		
		try {

			string sponsorId = "" , sponsorName = "" , userId = "";
            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var uf = dao.sp_tUserFunding_get(uf_id).FirstOrDefault();
                Object[] op1 = new Object[] { "uf_id" };
                Object[] op2 = new Object[] { uf_id };
                var list = www6.selectSP("sp_tUserFunding_get", op1, op2).DataTableToList<sp_tUserFunding_getResult>();
                var uf = list.FirstOrDefault();

                if (uf == null)
                {
                    result.message = "나눔펀딩 정보가 없습니다.";
                    return result;
                }

                sponsorId = uf.SponsorID;
                sponsorName = uf.SponsorName;
                userId = uf.UserID;

            }

			
			Object[] objSql = new object[1] { "sp_web_tSponsorCall_insert_f" };
			Object[] objParam = new object[] { "SponsorID", "SponsorName", "ProcessID", "CenterLClass", "CenterMClass", "CenterSClass", "CounselChannel", "CounselType", "CenterQuestion", "CenterContext", "UserID" };
			Object[] objValue = new object[] { sponsorId, sponsorName, ProcessID, CenterLClass, CenterMClass, CenterSClass, "Compass4", "기타", CenterQuestion, CenterContext, userId };
			_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "상담내역 저장에 실패했습니다.";
			return result;

		}

	}
	
	// 문의목록
	public class sp_tUserFundingReply_list_fReultEx :sp_tUserFundingReply_list_fResult {
		public bool is_owner { get; set; }
	}

	// 댓글 목록
	public JsonWriter GetReply(int page, int rowsPerPage, int id) {
		JsonWriter result = new JsonWriter() { success = false };

		var userId = "";
		if (UserInfo.IsLogin) {
			userId = new UserInfo().UserId;
		}

		try {

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var list = dao.sp_tUserFundingReply_list_f(page, rowsPerPage, id).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage", "uf_id"};
                Object[] op2 = new Object[] { page, rowsPerPage, id };
                var list = www6.selectSP("sp_tUserFundingReply_list_f", op1, op2).DataTableToList<sp_tUserFundingReply_list_fResult>();

                List<sp_tUserFundingReply_list_fReultEx> data = new List<sp_tUserFundingReply_list_fReultEx>();

                foreach (sp_tUserFundingReply_list_fResult item in list)
                {

                    data.Add(new sp_tUserFundingReply_list_fReultEx()
                    {

                        is_owner = userId == item.UserID,
                        rownum = item.rownum,
                        UserID = item.UserID.Mask("*", 3),
                        total = item.total,
                        ur_a_id = item.ur_a_id,
                        //ur_content = item.ur_content.ToHtml(),
                        ur_content = item.ur_content,
                        ur_deleted = item.ur_deleted,
                        ur_id = item.ur_id,
                        ur_regdate = item.ur_regdate,
                        ur_uf_id = item.ur_uf_id

                    });
                }

                result.data = data;
            }

			result.success = true;

		} catch (Exception ex) {
			ErrorLog.Write( HttpContext.Current, 0, ex.Message );
			result.message = "정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;
		}

		return result;
	}
	
	// 문의목록
	public class sp_tUserFundingNotice_list_fResultEx :sp_tUserFundingNotice_list_fResult {
		public bool is_owner { get; set; }
	}
	
	// 업데이트 목록
	public JsonWriter GetNotice(int page, int rowsPerPage, int id) {
		JsonWriter result = new JsonWriter() { success = false };

		var userId = "";
		if (UserInfo.IsLogin) {
			userId = new UserInfo().UserId;
		}

		try {

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var list = dao.sp_tUserFundingNotice_list_f(page, rowsPerPage, id).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage", "uf_id" };
                Object[] op2 = new Object[] { page, rowsPerPage, id };
                var list = www6.selectSP("sp_tUserFundingNotice_list_f", op1, op2).DataTableToList<sp_tUserFundingNotice_list_fResult>();

                List<sp_tUserFundingNotice_list_fResultEx> data = new List<sp_tUserFundingNotice_list_fResultEx>();


                foreach (sp_tUserFundingNotice_list_fResult item in list)
                {

                    data.Add(new sp_tUserFundingNotice_list_fResultEx()
                    {

                        is_owner = userId == item.UserID,
                        UserID = item.UserID.Mask("*", 3),
                        rownum = item.rownum,
                        SponsorName = item.SponsorName,
                        total = item.total,
                        un_a_id = item.un_a_id,
                        un_content = item.un_content,
                        un_deleted = item.un_deleted,
                        un_id = item.un_id,
                        un_image = item.un_image,
                        un_regdate = item.un_regdate,
                        un_uf_id = item.un_uf_id,
                        UserPic = item.UserPic
                    });
                }

                foreach (var item in data)
                {
                    if (!string.IsNullOrEmpty(item.un_image))
                        item.un_image = item.un_image.WithFileServerHost();
                    if (!string.IsNullOrEmpty(item.UserPic))
                        item.UserPic = item.UserPic.WithFileServerHost();
                }

                result.data = data;
            }

			result.success = true;

		} catch (Exception ex) {
			ErrorLog.Write( HttpContext.Current, 0, ex.Message );
			result.message = "정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;
		}

		return result;
	}

	public JsonWriter GetUser(int page, int rowsPerPage, int id) {
		JsonWriter result = new JsonWriter() { success = false };

		try {

            using (MainLibDataContext dao = new MainLibDataContext())
            {

                //var data = dao.sp_tUserFundingUser_list_by_uf_f(page, rowsPerPage, id).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage", "uf_id" };
                Object[] op2 = new Object[] { page, rowsPerPage, id };
                var data = www6.selectSP("sp_tUserFundingUser_list_by_uf_f", op1, op2).DataTableToList<sp_tUserFundingUser_list_by_uf_fResult>();

                List<sp_tUserFundingUser_list_by_uf_fResult> list = new List<sp_tUserFundingUser_list_by_uf_fResult>();

                foreach (sp_tUserFundingUser_list_by_uf_fResult item in data)
                {
                    list.Add(new sp_tUserFundingUser_list_by_uf_fResult()
                    {
                        UserID = item.UserID.Mask("*", 3),
                        rownum = item.rownum,
                        SponsorName = item.SponsorName,
                        total = item.total,
                        CommitmentID = item.CommitmentID,
                        SponsorID = item.SponsorID,
                        uu_amount = item.uu_amount,
                        uu_deleted = item.uu_deleted,
                        uu_id = item.uu_id,
                        uu_moddate = item.uu_moddate,
                        uu_regdate = item.uu_regdate,
                        uu_uf_id = item.uu_uf_id
                    });
                }

                result.data = list;
            }

			result.success = true;

		} catch (Exception ex) {
			ErrorLog.Write( HttpContext.Current, 0, ex.Message );
			result.message = "정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;
		}

		return result;
	}

	// 첨부파일 삭제
	public JsonWriter FileDelete( string fileNameWithPath ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}
		
		var extension = fileNameWithPath.Substring(fileNameWithPath.LastIndexOf('.')).ToLower();
		if(!new List<string>() { ".jpg", ".png", "jpeg", "gif" }.Contains(extension)) {
			result.message = "허용되지 않은 확장자 입니다.";
			return result;
		}

		var cookieValue = HttpContext.Current.Request.Cookies[Constants.ENDUSER_COOKIE_NAME].Value;

		var apiUrl = ConfigurationManager.AppSettings["domain_file"] + "/common/handler/file-delete.ashx?path=" + HttpUtility.UrlEncode(fileNameWithPath);
		var cc = new CookieContainer();
		cc.Add(new Cookie(Constants.ENDUSER_COOKIE_NAME, cookieValue, "/", FrontLoginSession.GetDomain(HttpContext.Current)));

		HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
		request.CookieContainer = cc;
		request.AllowAutoRedirect = false;

		using(HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {

			using(var reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8)) {
				string responseText = reader.ReadToEnd();
				//ErrorLog.Write(HttpContext.Current, 0, "responseText>" + responseText);
			}

		}


		result.success = true;
		return result;

	}

}