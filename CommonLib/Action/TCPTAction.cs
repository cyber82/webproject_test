﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;

public partial class TCPTAction {
	/*
	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;
	public TCPTAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

	}

	// 어린이 목록
	public class ChildListItem {
		public string GlobalId {get; set;}
		public int Age{get; set;}
		public DateTime BirthDate { get; set; }
		public string Country { get; set; }
		public string PreferredName { get; set; }
		public string FullName { get; set; }
		public string Gender { get; set; }
		public string ImageUrl { get; set; }
		public int WaitingSinceDate { get; set; }

		// 변경해야함
		public string ChildmasterId {
			get {
				return this.GlobalId;
			}
		}
		public int RemainAmount {
			get {
				// 남은 후원기간을 가지고 계산된 남은 개월수 * 45,000원 (3개월 단위 올림)
				var val = (this.BirthDate.AddYears(22).Month - DateTime.Now.Month) + 12 * (this.BirthDate.AddYears(22).Year - DateTime.Now.Year);
				if(val % 3 > 0) {
					val = val / 3 + 3;
				}
				return val * 45000;
			}
		}	
	}
	*/
//	public JsonWriter GetChildren(string country /*BO , EC 등*/ , string gender /*M , F*/ , int? birthMonth , int? birthDay, int? minAge , int? maxAge , bool? isOrphan /*고아*/ , bool? hasSpecialNeeds /*장애*/ , 
//		int page , int rowsPerPage ) {
/*
		JsonWriter result = new JsonWriter() { success = false };

		// 임시로 compass 연동
		Object[] objSql = new object[1] { string.Format(@"select top {0} * from tchildMaster where birthdate > '1997-01-01' and childMasterId not in (select childMasterId from tcommitmentMaster)
and childMasterId not in (select top {1} childMasterId from tchildMaster where birthdate > '1997-01-01')", rowsPerPage , (page -1) * rowsPerPage) };

		DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null).Tables[0];

		// 국가정보
		var countries = StaticData.Country.GetList(HttpContext.Current);

		var data = new List<ChildListItem>();
		foreach(DataRow dr in dt.Rows) {
			data.Add(new ChildListItem() {
				GlobalId = dr["ChildMasterID"].ToString(),
				Age = (DateTime.Now.Year - Convert.ToDateTime(dr["BirthDate"]).Year),
				BirthDate = Convert.ToDateTime(dr["BirthDate"]),
				Country = countries.FirstOrDefault(p=>p.c_id == dr["CountryCode"].ToString()).c_name,
				PreferredName = dr["PersonalNameKr"].ToString(),
				FullName = dr["NameKr"].ToString(),
				Gender = dr["GenderCode"].ToString() == "M" ? "Male" : "Female",
				ImageUrl = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
				WaitingSinceDate = 50
			});
		}
		
		result.data = data;
		result.success = true;
		return result;
	}

	
	public class ChildItem {
	//	public string GlobalId { get; set; }
	//	public string LocalBeneficiaryId { get; set; }
		public int Age { get; set; }
		public DateTime BirthDate { get; set; }
		public string Country { get; set; }
		public string PreferredName { get; set; }
		public string FullName { get; set; }
		public string Gender { get; set; }
		public string ImageUrl { get; set; }
		public string Favorites { get; set; }
	}

	public JsonWriter GetChild( string childMasterId ) {

		JsonWriter result = new JsonWriter() { success = false };

		// 임시로 compass 연동
		Object[] objSql = new object[1] { string.Format(@"select top 1 * from tchildMaster with(nolock) where childMasterId = '{0}'", childMasterId) };

		DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null).Tables[0];

		// 국가정보
		var countries = StaticData.Country.GetList(HttpContext.Current);

		DataRow dr = dt.Rows[0];
		
		result.data = new ChildItem() {
		//	GlobalId = dr["ChildMasterID"].ToString(),
		//	LocalBeneficiaryId = dr["ChildKey"].ToString(),
			Age = (DateTime.Now.Year - Convert.ToDateTime(dr["BirthDate"]).Year),
			BirthDate = Convert.ToDateTime(dr["BirthDate"]),
			Country = countries.FirstOrDefault(p => p.c_id == dr["CountryCode"].ToString()).c_name,
			PreferredName = dr["PersonalNameKr"].ToString(),
			FullName = dr["NameKr"].ToString(),
			Gender = dr["GenderCode"].ToString() == "M" ? "Male" : "Female",
			ImageUrl = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
			Favorites = "Maths"
		};
		
		result.success = true;
		return result;
	}
	*/

}

