﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Net.Sockets;
using System.Text;
using System.Data;

public partial class HSCMSAction {

	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;
	CommonLib.PAY4Service.ServiceSoap _pay4Service;
    CommonLib.TCPTService.ServiceSoapClient _tcptservice;
    string pDOC = @"\\krfilesvr\FTPData\CMSProofData\DOC\";
    string pVOC = @"\\krfilesvr\FTPData\CMSProofData\VOC\";
    public HSCMSAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
		_pay4Service = new CommonLib.PAY4Service.ServiceSoapClient();
        _tcptservice = new CommonLib.TCPTService.ServiceSoapClient();

    }

	// bankCode : 3자리
	public JsonWriter Verify( string bankCode, string bankAccount, string birthYMD /*yymmdd*/ , string name ) {

		JsonWriter result = new JsonWriter() { success = false };

		result = this.GetUniqueNo(bankAccount, "ACC_CHECK");
		if(!result.success) {
			return result;
		}

		var uniqueNo = result.data.ToString();

		if (bankCode == "012") {     // 농협회원조합
			bankCode = "011";
		}

		try {
			var dt = _pay4Service.PaymentCMSCheckBankAccount(uniqueNo, bankCode.Substring(1), bankAccount, birthYMD, name).Tables[0];
			var resultCode = dt.Rows[0]["ResponseYN"].ToString();
			var errorCode = dt.Rows[0]["ResponseCode"].ToString();

			var verityResult = new VerifyResult();
			verityResult.success = resultCode == "Y";
			verityResult.code = errorCode;
			verityResult.bankCode = bankCode;
			verityResult.bankAccount = bankAccount;
			verityResult.birthYMD = birthYMD;
			verityResult.name = name;

			if(resultCode == "N") {
				verityResult.msg = "계좌확인에 실패했습니다. (" + this.GetErrorMessage(errorCode) + ")";
			}
			result.success = true;
			result.data = verityResult;

			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.success = false;
			result.message = "계좌조회에 실패했습니다.";
			return result;

		}
	}

	// 출금
	// bankCode : 3자리
	public JsonWriter Pay( string bankCode, string bankAccount, string birthYMD /*yymmdd*/ , string name , int amount , string paymentAccountID , string commitmentID ) {

		JsonWriter result = new JsonWriter() { success = false };

		result = this.GetUniqueNo(bankAccount, "PAYMENT_REQ");
		if(!result.success) {
			return result;
		}

		var uniqueNo = result.data.ToString();
		var sess = new UserInfo();
		string paymentMasterID = _wwwService.GetDate().ToString("yyyyMMddHHmmssff2");

		if(bankCode == "012")   // 농협회원조합 , 효성CMS는 농협종류가 11 뿐이다
			bankAccount = "011";
		else if (bankCode == "240") {		// 삼성카드 , 24로 넘겨야함
			bankAccount = "024";
		}

		// 즉시출금 가능은행
		// 경남은행 39  국민은행 04  농협 11  대구은행 31  새마을금고 45  신한은행 88  씨티은행 27  외환은행 05  우리은행 20  우체국 71  SC은행 23  하나은행 81  신협 48  유안타증권 09  삼성증권 24  광주은행 34  기업은행 03  산업은행 02  부산은행 32  제주은행 35 
		string[] available_banks = new string[]{
			"39" , "04" , "11", "31", "45"
				, "88","27", "05", "20", "71"
				, "23", "81", "48", "09", "24"
				, "34", "03", "02", "32", "35"
		};

		var paymentResult = new PaymentResult();

		// 즉시출금 대상기관이 아니면 성공
		if(!available_banks.Contains(bankCode.Substring(1))) {
			//ErrorLog.Write(HttpContext.Current , 0 , "즉시결제 대상은행이 아님 code:" + bankCode);
			paymentResult.success = true;
			result.success = true;
			result.data = paymentResult;
            paymentResult.msg = "즉시결제 대상은행이 아님 code:" + bankCode;
			return result;
		}

        // 증빙자료 업로드 추가 문희원 현재 DER 만 가능
        try
        {
            JsonWriter proofResult = RegisterProof(bankAccount, sess.SponsorID);

            if (!proofResult.success)
            {
                ErrorLog.Write(HttpContext.Current, 0, proofResult.data.ToString());
                result.success = false;
                result.message = "계좌출금에 실패했습니다.";
                return result;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message );
            result.success = false;
            result.message = "계좌출금에 실패했습니다.";
            return result;
        }

		try {
			var dt = _pay4Service.PaymentCMS(uniqueNo, bankCode.Substring(1), bankAccount, birthYMD, name, sess.SponsorID, amount.ToString(), paymentMasterID).Tables[0];
			// >>>{"Table1":[{"ResponseYN":"Y","ResponseCode":"0000","ResponseMSG":null,"ResponseDate":"160907","ResponseTime":"175403","PaymentCharge":"000000000000700"}]}
			var resultCode = dt.Rows[0]["ResponseYN"].ToString();
			var errorCode = dt.Rows[0]["ResponseCode"].ToString();
			
			paymentResult.success = resultCode == "Y";
			paymentResult.code = errorCode;

            if (dt != null && dt.Rows.Count > 0)
            {
                string s = "";
                foreach (DataColumn col in dt.Columns)
                {
                    s += dt.Rows[0][col].ToString() + " | ";
                }

                ErrorLog.Write(HttpContext.Current, 0, s);
            }

			if(resultCode == "N") {
				var msg = dt.Rows[0]["ResponseMSG"];
				if(msg == null) {
					paymentResult.msg = "에러코드(" + this.GetErrorMessage(errorCode) + ")";
				} else {
					paymentResult.msg = msg.ToString();
				}
			} else {

				#region temp 에 임시저장
				try {


					string bankName = "";

					var bankData = _wwwService.selectSystemCode("BankCode", "");
					foreach(DataRow dr in bankData.Tables[0].Rows) {
						if(dr["codeId"].ToString() == bankCode) {
							bankName = dr["codeName"].ToString();

							break;
						}
					}

					// paymentMaster
					{
						Object[] objParam = new object[] { "PaymentMasterID", "PaymentAccountID", "SponsorID", "PaymentType", "PaymentName", "PaymentKind", "PaymentKindName",
							"PaymentDate", "Amount", "BankCode", "BankName", "Account", "PartitioningFlag", "ProcessCode", "ChannelType", "RegisterID", "RegisterName",
							"KeyDate", "UniqueNumber", "ResultCode" };
						Object[] objValue = new object[] { paymentMasterID, paymentAccountID, sess.SponsorID, "0023" , "CMS즉시출금" , "CTRB" , "Contribution" ,
							DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") , amount , bankCode , bankName , bankAccount , "Y" , "10" , CodeAction.ChannelType , sess.UserId , sess.UserName ,
							DateTime.Now.ToString("yyyy-MM-dd") , Convert.ToInt32(uniqueNo) , errorCode };
						Object[] objSql = new object[] { "sp_I_PaymentMasterTemp" };
						_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
					}

					// paymentPartitioning
					{
						// commitment 
						DataRow dr = _www6Service.NTx_ExecuteQuery("SqlCompass4",
							new object[1] { " SELECT top 1 * FROM   tCommitmentMaster with(nolock) WHERE commitmentId = '" + commitmentID + "'" }
							, "Text", null, null).Tables[0].Rows[0];

						var childMasterId = dr["childMasterId"].ToString();
						var accountClass = dr["sponsorItemEng"].ToString();
						var accountClassName = dr["sponsorItem"].ToString();
						var ptd = accountClass == "DS" || accountClass == "LS" ? Convert.ToDateTime(DateTime.Now.AddMonths(1).ToString("yyyy-MM-01")).AddDays(-1).ToString("yyyyMMdd") : "19600101";
						var paymentPartitioningID = _wwwService.GetDate().ToString("yyyyMMddHHmmssff2");
						var campaignID = dr["campaignID"].ToString();

						// add paymentPartitioningtemp
						Object[] objParam = new object[] { "PaymentMasterID", "PaymentPartitioningID", "PaymentDate", "SponsorID", "ChildMasterID",
							"PaymentType", "PaymentName", "AccountClass", "AccountClassName", "Amount",
							"PTD", "CommitmentID", "RegisterID", "RegisterName", "CampaignID" };
						Object[] objValue = new object[] { paymentMasterID, paymentPartitioningID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") , sess.SponsorID, childMasterId ,
							"0023" , "CMS즉시출금" , accountClass , accountClassName , amount ,
							ptd , commitmentID , sess.UserId , sess.UserName , campaignID};
						Object[] objSql = new object[] { "sp_I_PaymentPartitioningTemp" };
						_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
					}


				} catch(Exception ex) {
					ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
					result.success = false;
					result.message = "출금정보 임시저장에 실패했습니다.";
					return result;

				}
				#endregion

				// 결제가 성공하면 확인
				result = this.GetUniqueNo(bankAccount, "PAYMENT_CHECK");
				if(!result.success) {
					return result;
				}

				var checkUniqueNo = result.data.ToString();

				dt = _pay4Service.PaymentCMSCheck(checkUniqueNo, DateTime.Now.ToString("yyMMdd"), uniqueNo).Tables[0];
			//	ErrorLog.Write(HttpContext.Current, 0, "check>>>" + dt.ToJson());

				resultCode = dt.Rows[0]["ResponseYN"].ToString();
				errorCode = dt.Rows[0]["ResponseCode"].ToString();

				paymentResult = new PaymentResult();
				paymentResult.success = resultCode == "Y";
				paymentResult.code = errorCode;

				if(resultCode == "N") {
					var msg = dt.Rows[0]["ResponseMSG"];
					if(msg == null)
						paymentResult.msg = "계좌이체에 실패했습니다. 에러코드(" + errorCode + ")";
					else
						paymentResult.msg = "계좌이체에 실패했습니다.  " + msg.ToString();
				} else {

					// 이체확인 완료되면 temp에서 실데이타로 이동
					// add paymentPartitioningtemp
					Object[] objParam = new object[] { "PaymentMasterID", "RegisterID", "RegisterName", "PayCheckCode" };
					Object[] objValue = new object[] { paymentMasterID, sess.UserId , sess.UserName , errorCode };
					Object[] objSql = new object[] { "sp_I_CMSPaymentInformation_web" };
					_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

				}
			}
			result.success = true;
			result.data = paymentResult;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.success = false;
			result.message = "계좌출금에 실패했습니다.";
			return result;

		}
	}

    JsonWriter RegisterProof(string bankAccount, string sponsorID)
    {
        JsonWriter result = new JsonWriter() { success = false };
        //-proofData 가져오기 
        DataSet dsProof = new DataSet();
        string query = "select top 1 proofID, PDType, PDName, PDFormat, RegisterDate, status, SignedData, OriginData from tSponsorCMS_ProofData ";
        query += " where sponsorid = '"+sponsorID+"' order by RegisterDate DESC"; 

        Object[] objSql = new object[1] { query };
        dsProof = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

        // 증빙 정보 조회되지 않음.
        if (dsProof == null || dsProof.Tables.Count == 0 || dsProof.Tables[0].Rows.Count == 0)
        {
            result.data = "증빙자료 조회되지 않음.";
            return result;
        }

        string PDPath = string.Empty;
        string sPDType = dsProof.Tables[0].Rows[0]["PDType"].ToString();
        string sPDName = dsProof.Tables[0].Rows[0]["PDName"].ToString();
        string sPDFormat = dsProof.Tables[0].Rows[0]["PDFormat"].ToString();
        string sSignedData = dsProof.Tables[0].Rows[0]["SignedData"].ToString();
        string dataType = sPDFormat;
        string dataSec = "";
        object proofData = null;
        long fileLength = 0;
        DataSet proofResult = new DataSet();
        DataSet payResult = new DataSet();
        string derCreatResult = string.Empty;
        switch (sPDType)
        {
            case "DOC":
                dataSec = "01";

                PDPath = pDOC + sPDName;
                break;
            case "VOC":
                dataSec = "02";

                PDPath = pVOC + sPDName;
                break;
            case "DER":
                dataSec = "03";
                break;
        }

        //- DER인 경우
        if (sPDType == "DER")
        {
            proofData = Encoding.ASCII.GetBytes(sSignedData);
            fileLength = sSignedData.Length;
        }
        else
        {
            //result.data = "증빙자료 형식 DER 아님. " + sPDType;
            proofData = _tcptservice.GetProofFile(PDPath);

            if (proofData == null)
            {
                result.data = "증빙자료 조회 오류 " + sPDType;
                return result;
            }
        }

        result = this.GetUniqueNo(bankAccount, "USER_PROOF");
        if (!result.success)
        {
            result.data = "증빙자료 연번생성 오류";
            return result;
        }

        var sess = new UserInfo();
        var uniqueNo = result.data.ToString();

        try
        {
            //ErrorLog.Write(HttpContext.Current, 0, "증빙자료 업로드 시작");
            var req = new CommonLib.PAY4Service.PaymentCMSProofRequest(uniqueNo, sess.SponsorID, dataSec, dataType, fileLength.ToString(), proofData as byte[]);
            var res = _pay4Service.PaymentCMSProof(req);
            var dt = res.PaymentCMSProofResult.Tables[0];
            if (dt.Rows[0]["ResponseYN"].ToString() == "Y")
            {
                //ErrorLog.Write(HttpContext.Current, 0, "증빙자료 업로드 성공");
                result.success = true;
            }
            else
            {
                result.message = dt.Rows[0]["ResponseMSG"].ToString();
                ErrorLog.Write(HttpContext.Current, 0, "res >> " + dt.ToJson());
            }
            return result;
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
            result.success = false;
            result.message = "증빙자료 업로드 실패했습니다.";
            return result;
        }
    } 

    string GetErrorMessage(string code) {
        try {
            CommonLib.WWW6Service.SoaHelperSoap _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
            Object[] objSql = new object[1] { string.Format(" select top 1 codename from [tSystemCode] where CodeType = 'HSPaymentResultCode' and codeid='{0}'", code) };
            var ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
            if (ds != null && ds.Tables[0].Rows.Count > 0) {
                return ds.Tables[0].Rows[0]["codename"].ToString();
            }

            return code;
        }catch(Exception ex) {
            return code;
        }
    }

    public class PaymentResult {
		public string code;
		public bool success;
		public string msg;
	}

	public JsonWriter RegistProof( string bankAccount , string dataSec, string dataType, string dataLength, byte[] proofFile ) {

		JsonWriter result = new JsonWriter() { success = false };

		result = this.GetUniqueNo(bankAccount, "USER_PROOF");
		if(!result.success) {
			return result;
		}

		var sess = new UserInfo();
		var uniqueNo = result.data.ToString();

		try {
			var req = new CommonLib.PAY4Service.PaymentCMSProofRequest(uniqueNo, sess.SponsorID, dataSec, dataType, dataLength, proofFile);
			var res = _pay4Service.PaymentCMSProof(req);
			var dt = res.PaymentCMSProofResult.Tables[0];
			if(dt.Rows[0]["ResponseYN"].ToString() == "Y") {
				result.success = true;
			} else {
				result.message = dt.Rows[0]["ResponseMSG"].ToString();
				ErrorLog.Write(HttpContext.Current, 0, "res >> " + dt.ToJson());
			}
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.success = false;
			result.message = "증빙데이터저장에 실패했습니다.";
			return result;

		}
	}

	public class VerifyResult {
		public string code;
		public bool success;
		public string msg;
		public string bankCode;
		public string bankAccount;
		public string name;
		public string birthYMD;
	}

	// 연번사용 할 업무 구분 (ACC_CHECK : 계좌확인, PAYMENT_REQ: 즉시출금요청, PAYMENT_CHECK : 출금확인, USER_PROOF : 회원 증빙
	JsonWriter GetUniqueNo(string bankAccount , string workType) {

		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			result.action = "login";
			return result;
		}

		var sess = new UserInfo();

		try {
			Object[] objParam = new object[] { "Account", "WorkType", "SponsorMasterID", "RegisterID" };
			Object[] objValue = new object[] { bankAccount, workType, sess.SponsorID, sess.UserId };
			Object[] objSql = new object[] { "sp_S_HSCMSUniqueNumber" };
			DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			result.data = list.Rows[0][0].ToString();
			result.success = true;
			return result;
		}catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "연번생성에 실패했습니다.";
			return result;

		}
	}

    private byte[] ReadFile(string sPath)
    {
        byte[] data = null;

        try
        {
            FileInfo fInfo = new FileInfo(sPath);
            long ByteLen = fInfo.Length;

            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);

            BinaryReader br = new BinaryReader(fStream);

            data = br.ReadBytes((int)ByteLen);
        }
        catch (Exception ex)
        {
            string sErrorMsg = "증빙파일 로드 실패 --- " + sPath + " --- " + ex.Message;
            ErrorLog.Write(HttpContext.Current, 0, sErrorMsg);
        }

        return data;
    }
}