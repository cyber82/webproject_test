﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;

public partial class MateAction {


	// 셈플문제
	public JsonWriter GetSampleTestList() {
		JsonWriter result = new JsonWriter() { success = false };
        using (MainLibDataContext dao = new MainLibDataContext())
        {
            result.success = true;
            //result.data = dao.mate_sample_test.Where(p => p.st_display).OrderBy(p => p.st_order).ToList();
            result.data = www6.selectQ<mate_sample_test>("st_display", 1, "st_order");
            return result;
        }
	}


	public JsonWriter GetSampleTestList(string type) {
		JsonWriter result = new JsonWriter() { success = false };
        using (MainLibDataContext dao = new MainLibDataContext())
        {
            result.success = true;
            //result.data = dao.mate_sample_test.Where(p => p.st_display && p.st_type == type).OrderBy(p => p.st_order).ToList();
            result.data = www6.selectQ<mate_sample_test>("st_display", 1, "st_type", type, "st_order");

            return result;
        }
	}


	public JsonWriter GetSampleTestInfo()
    {
		JsonWriter result = new JsonWriter() { success = false };
        using (MainLibDataContext dao = new MainLibDataContext())
        {
            result.success = true;
            //result.data = dao.mate_sample_test_info.First();
            result.data = www6.selectQ<mate_sample_test_info>().First();
            return result;
        }
	}

}

