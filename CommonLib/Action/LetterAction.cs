﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;
using System.Text;
using CommonLib;

public partial class LetterAction
{

    CommonLib.WWWService.ServiceSoap _wwwService;
    CommonLib.WWW6Service.SoaHelperSoap _www6Service;
    public LetterAction()
    {
        _wwwService = new CommonLib.WWWService.ServiceSoapClient();
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

    }

    // 편지종류
    public JsonWriter GetCodeList()
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        try
        {

            Object[] objSql = new object[1] { " select codeId , codeName , description from tSystemCode where codetype = 'CorrType' and currentUse ='Y' order by cast(displayOrder as int) asc" };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

            result.data = ds.Tables[0];
            result.success = true;


        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "코드목록 조회에 실패했습니다.";
        }

        return result;
    }

    // 발송대기/임시저장 데이타 갯수 조회
    public JsonWriter GetTemporaryTotal()
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();

        try
        {

            Object[] objSql = new object[1] { "sp_web_letter_corr_total_f" };
            Object[] objParam = new object[] { "SponsorID" };
            Object[] objValue = new object[] { sess.SponsorID };
            var list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

            var data = new Dictionary<string, object>();

            if (list.Rows.Count > 0)
            {
                data.Add("total_temp", Convert.ToInt32(list.Rows[0]["total_temp"]));
                data.Add("total_ready", Convert.ToInt32(list.Rows[0]["total_ready"]));
            }
            else
            {
                data.Add("total_temp", 0);
                data.Add("total_ready", 0);
            }

            result.data = data;
            result.success = true;


        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "임시저장데이타 조회에 실패했습니다.";
        }

        return result;
    }


    // 편지목록 type : send , receive , all , send_temp , include_ready : Y,N
    public JsonWriter GetList(int page, int rowsPerPage, string type, string childMasterId, string include_ready)
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();
        int total = 0;
        DataTable list = null;
        var data = new Dictionary<string, object>();

        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }

        try
        {

            if (page == 1)
            {
                #region 보낸/받은/임시/발송대기 총 갯수
                {

                    Object[] objSql = new object[1] { "Corr_MyPageLetterList2016" };
                    Object[] objParam = new object[] { "DIVIS", "SponsorID" };
                    Object[] objValue = new object[] { "TOTAL", sess.SponsorID };
                    list = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];

                    if (list.Rows.Count > 0)
                    {
                        data.Add("total_receive", Convert.ToInt32(list.Rows[0]["total_receive"]));
                        data.Add("total_send", Convert.ToInt32(list.Rows[0]["total_send"]));
                    }
                    else
                    {
                        data.Add("total_receive", 0);
                        data.Add("total_send", 0);
                    }

                }

                {

                    Object[] objSql = new object[1] { "sp_web_letter_corr_total_f" };
                    Object[] objParam = new object[] { "SponsorID" };
                    Object[] objValue = new object[] { sess.SponsorID };
                    list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

                    if (list.Rows.Count > 0)
                    {
                        data.Add("total_temp", Convert.ToInt32(list.Rows[0]["total_temp"]));
                        data.Add("total_ready", Convert.ToInt32(list.Rows[0]["total_ready"]));
                    }
                    else
                    {
                        data.Add("total_temp", 0);
                        data.Add("total_ready", 0);
                    }

                }

                #endregion
            }

            #region 보낸/받은 편지
            {
                string CorrespondenceType = "";
                if (type == "send")
                    CorrespondenceType = "SPN";
                else if (type == "receive")
                    CorrespondenceType = "CHI";
                else if (type == "send_temp" || type == "send_ready")
                    CorrespondenceType = "XXX";      // 임시발송함은 스키마가 필요하기때문에 데이타가 없는 쿼리를 요청한다.

                Object[] objSql = new object[1] { "Corr_MyPageLetterList2016" };
                Object[] objParam = new object[6] { "DIVIS", "SponsorID", "correspondenceType", "page", "rowsPerPage", "ChildMasterID" };
                Object[] objValue = new object[6] { "MAIN", sess.SponsorID, CorrespondenceType, page, rowsPerPage, childMasterId };
                list = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];

                list.Columns.Add("is_pic_letter");  // 사진편지여부, Y : N
                list.Columns.Add("pic");            // 아이사진
                list.Columns.Add("direction");      // 받은편지 , 보낸편지여부
                list.Columns.Add("is_smart");       // 스마트레터여부
                list.Columns.Add("display_date");   // 받은편지 보낸편지에 따른 화면 표시 일자
                list.Columns.Add("is_editable");    // 임시편지의 경우 수정가능한지 여부
                list.Columns.Add("content");        // 임시/대기편지의 경우 내용
                                                    //list.Columns.Add("correspondencetype");

                foreach (DataRow dr in list.Rows)
                {
                    if (!string.IsNullOrEmpty(dr["childKey"].ToString()) && dr["childKey"].ToString() != "0000000000")
                        dr["pic"] = new ChildAction().GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString());//ChildAction.GetPicByChildKey(dr["childKey"].ToString());

                    total = Convert.ToInt32(dr["total"]);

                    var correspondenceType = dr["correspondencetype"].ToString().ToUpper();
                    if (correspondenceType.IndexOf("CHI") > -1)
                    {
                        dr["direction"] = "receive";
                        dr["display_date"] = dr["displaydate"];
                    }
                    else
                    {
                        dr["direction"] = "send";
                        dr["display_date"] = dr["displaydate"];
                    }

                    //if(correspondenceType.IndexOf("WS") > -1 || correspondenceType.IndexOf("XCS") > -1) {
                    if (correspondenceType.IndexOf("WS") > -1)
                    {
                        dr["is_smart"] = true;
                    }
                    else
                    {
                        dr["is_smart"] = false;
                    }

                }
            }
            #endregion

            #region 임시저장/등록대기
            if ((include_ready == "Y" && ((type == "all" && page == 1) || (type == "send" && page == 1))) || type == "send_temp" || type == "send_ready")
            {

                Object[] objSql = new object[1] { "sp_web_letter_corr_f" };
                Object[] objParam = new object[] { "SponsorID", "type", "childMasterId" };
                Object[] objValue = new object[] { sess.SponsorID, type, childMasterId };
                DataTable corrList = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

                if (corrList != null)
                {

                    total += corrList.Rows.Count;

                    foreach (DataRow dr in corrList.Rows)
                    {
                        var new_row = list.NewRow();
                        var corr_status = dr["CorrStatus"].ToString().ToUpper();

                        // 임시저장은 전체편지 목록에서 제외
                        if (corr_status == "T" && type == "all")
                        {
                            continue;
                        }

                        var is_editable = corr_status == "T" || (corr_status == "N" && dr["RegisterDate"].ToString().Substring(0, 10) == DateTime.Now.ToString("yyyy-MM-dd"));

                        if (is_editable)
                        {
                            new_row["state"] = corr_status == "T" ? "임시저장" : "등록 대기중";

                            // 사진 편지 여부 확인
                            using (MainLibDataContext dao = new MainLibDataContext())
                            {
                                var exist = www6.selectQ<tLetterPic>("RepCorrespondenceID", dr["RepCorrespondenceID"].ToString());
                                //if (dao.tLetterPic.Any(p => p.RepCorrespondenceID == dr["RepCorrespondenceID"].ToString()))
                                if(exist.Any())
                                {
                                    new_row["is_pic_letter"] = "Y";
                                }
                                else
                                {
                                    new_row["is_pic_letter"] = "N";
                                }

                            }

                        }
                        else
                        {
                            new_row["state"] = "등록중";
                        }

                        new_row["direction"] = corr_status == "T" ? "send_temp" : "send_ready";
                        new_row["is_editable"] = is_editable;
                        new_row["countryname"] = dr["CountryCode"].ToString();
                        new_row["namekr"] = dr["ChildName"].ToString();
                        new_row["childkey"] = dr["ChildKey"].ToString();
                        new_row["display_date"] = dr["RegisterDate"];
                        new_row["correspondenceid"] = dr["RepCorrespondenceID"];
                        new_row["total"] = type == "send_temp" || type == "send_ready" ? corrList.Rows.Count : total;       // 임시저장은 별도 화면에서 임시저장데이타만 불러오기때문에 총갯수를 따로 관리한다.
                        new_row["is_smart"] = false;
                        new_row["pic"] = new ChildAction().GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString());//ChildAction.GetPicByChildKey(dr["childKey"].ToString());
                        new_row["content"] = dr["correspondenceContext"].ToString().Limit(50).ToHtml();
                        new_row["correspondencetype"] = dr["correspondencetype"].ToString();


                        list.Rows.InsertAt(new_row, 0);
                    }
                }
            }
            #endregion

            data.Add("list", list);
            result.data = data;
            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
            result.message = "정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }

    // 편지상세 attach_path  : 첨부파일 디렉토리 
    public JsonWriter GetDetail(string corrId, string attach_path)
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        if (string.IsNullOrEmpty(corrId) || string.IsNullOrEmpty(attach_path))
        {
            result.message = "필수정보가 누락됬습니다.";
            return result;
        }

        var sess = new UserInfo();

        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }

        var direction = "";
        var sCorrespondenceType = "";
        string sScreeningDate = "";
        string sFilePath = "";
        string sTranslationNeed = "";
        string sScreeningStatus = "";
        string sScreeningName = "";
        string sTranslationStatus = "";
        string sTranslationName = "";
        string sScreeningContext = "";
        string sSendDate = "";
        string sPackageId = "";
        string sChildKey = "";
        Boolean bIsOld = false;             /*20170201 child key 9자리 이미지 구분*/
        string sChildLetterKey = "";

        int imageCount = 0;

        DataTable list = null;
        DataTable detail = null;
        List<string> screening_images = new List<string>();
        string rtn_sendContent = "";
        string rtn_translateContent = "";
        string rtn_attachment = "";     // 첨부파일 웹경로
        string correspondenceId = "", childMasterId = "";


        try
        {

            #region 기본정보

            {

                Object[] objSql = new object[1] { "Corr_MyPageLetterList2016" };
                Object[] objParam = new object[] { "DIVIS", "SponsorID", "CorrID" };
                Object[] objValue = new object[] { "MAIN", sess.SponsorID, corrId };
                list = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];

                list.Columns.Add("pic");            // 아이사진
                list.Columns.Add("direction");      // 받은편지 , 보낸편지여부
                list.Columns.Add("is_smart");       // 스마트레터여부
                list.Columns.Add("display_date");   // 받은편지 보낸편지에 따른 화면 표시 일자
                list.Columns.Add("is_editable");    // 임시편지의 경우 수정가능한지 여부

                if (list.Rows.Count < 1)
                {
                    result.message = "조회결과가 존재하지 않습니다(0).";
                    return result;
                }

                foreach (DataRow dr in list.Rows)
                {
                    if (!string.IsNullOrEmpty(dr["childKey"].ToString()) && dr["childKey"].ToString() != "0000000000")
                        dr["pic"] = new ChildAction().GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString());//ChildAction.GetPicByChildKey(dr["childKey"].ToString());

                    sPackageId = dr["PackageId"].ToString();
                    //sChildKey = dr["childKey"].ToString();
                    sChildKey = dr["CorrChildKey"].ToString();

                    correspondenceId = dr["correspondenceId"].ToString();
                    childMasterId = dr["childMasterId"].ToString();

                    imageCount = Convert.ToInt32(dr["pagecount"].ToString());
                    var correspondenceType = dr["correspondencetype"].ToString().ToUpper();
                    if (correspondenceType.IndexOf("CHI") > -1)
                    {
                        direction = "receive";
                        dr["direction"] = "receive";
                        dr["display_date"] = dr["displaydate"];
                    }
                    else
                    {
                        direction = "send";
                        dr["direction"] = "send";
                        dr["display_date"] = dr["displaydate"];
                    }

                    //if(correspondenceType.IndexOf("WS") > -1 || correspondenceType.IndexOf("XCS") > -1) {
                    if (correspondenceType.IndexOf("WS") > -1)
                    {
                        dr["is_smart"] = true;
                    }
                    else
                    {
                        dr["is_smart"] = false;
                    }
                }
            }
            #endregion

            #region 상세정보
            {
                Object[] objSql = new object[1] { "Corr_MyPageLetterList2016" };
                Object[] objParam = new object[] { "DIVIS", "SponsorID", "CorrID" };
                Object[] objValue = new object[] { direction == "receive" ? "RCVDETAIL" : "SNDDETAIL", sess.SponsorID, corrId };
                detail = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];

                if (detail.Rows.Count < 1)
                {
                    result.message = "조회결과가 존재하지 않습니다(1).";
                    return result;
                }

                if (direction == "send")
                    sCorrespondenceType = detail.Rows[0]["CorrespondenceType"].ToString();

                sScreeningDate = detail.Rows[0]["ScreeningDate"].ToString().Trim();
                sFilePath = detail.Rows[0]["FilePath"].ToString().Trim();
                sTranslationNeed = detail.Rows[0]["TranslationNeed"].ToString();
                sScreeningStatus = detail.Rows[0]["ScreeningStatus"].ToString();
                sScreeningName = detail.Rows[0]["ScreeningName"].ToString();
                sTranslationStatus = detail.Rows[0]["TranslationStatus"].ToString();
                sTranslationName = detail.Rows[0]["TranslationName"].ToString();
                sScreeningContext = detail.Rows[0]["ScreeningContext"].ToString();
                sSendDate = detail.Rows[0]["SendDate"].ToString();
                //bIsOld = detail.Rows[0]["IsOld"].ToString();
                if (detail.Rows[0]["IsOld"] == null || detail.Rows[0]["IsOld"].Equals(""))
                {
                    bIsOld = false;
                }
                else
                {
                    bIsOld = (Boolean)detail.Rows[0]["IsOld"];
                }


                /*20170201 이미지 child key 9자리 구분 로직 추가*/
                if (bIsOld == true && sChildKey.ToString().Trim().Length > 9)
                {
                    sChildLetterKey = (sChildKey.ToString().Trim().Substring(0, 2) + sChildKey.ToString().Trim().Substring(3, 3) + sChildKey.ToString().Trim().Substring(7, 4));
                }
                else
                {
                    sChildLetterKey = sChildKey;
                }

            }
            #endregion

            if (direction == "send")
            {

                rtn_sendContent = detail.Rows[0]["CorrespondenceContext"].ToString().ToHtml();

                #region 보낸편지 데이타 
                /*20170406 수정 : 번역여부와 관계없이 스크리닝 텍스트가 있으면 표시*/
                //if(sScreeningDate != "") {
                if (sFilePath == "")
                {
                    if (sCorrespondenceType == "WSCC")
                    {

                        if (detail.Rows[0]["CorrespondenceContext"].ToString() == "")
                        {
                            screening_images.Add(ConfigurationManager.AppSettings["domain_file"] + "/common/img/WSCC_Dafule.png");
                        }
                    }
                }
                // 요청한 적이 없는 기능이라 삭제 문희원 2017-05-29
                //if (sTranslationNeed.Equals("Y"))
                //{
                if (sScreeningStatus == "END")
                {
                    rtn_translateContent = sScreeningContext.ToHtml();

                    //if (sSendDate == "")
                    //{
                    //    rtn_translateContent += "<br/><br/>발송 준비중입니다.";
                    //}
                }
                //    else
                //    {
                //        rtn_translateContent = "번역중입니다.";
                //    }
                //    //} else {
                //    //if(sSendDate == "") {
                //    //    rtn_translateContent = "발송 준비중입니다.";
                //    //}
                //}
                //else
                //{
                //    if (sSendDate == "")
                //    {
                //        rtn_translateContent += "<br/><br/>발송 준비중입니다.";
                //    }
                //}

                /*
            } else {
                if(detail.Rows[0]["CorrClass"].ToString().Trim() == "") {

                    rtn_translateContent = "이미지 스캔 전입니다.";
                } else {
                    if(sTranslationNeed.Equals("Y")) {
                        //if (isScreeningFlag == true)
                        if(sScreeningStatus == "END") {
                            rtn_translateContent = sScreeningContext.ToHtml();

                            if(sSendDate == "") {
                                rtn_translateContent += "<br/><br/>발송 준비중입니다.";
                            }
                        } else {
                            rtn_translateContent = "번역중입니다.";
                        }
                    } else {
                        if(sSendDate == "") {
                            rtn_translateContent = "발송 준비중입니다.";
                        }
                    }
                }
            }
            */
                #endregion

                #region 보낸편지 이미지

                if (sFilePath != "" && imageCount > 0)
                {

                    for (int i = 1; i <= imageCount; i++)
                    {
                        var sUrl = "";
                        if (int.Parse(sPackageId) < 200000)
                        {
                            //sUrl = sFilePath //"http://ws.compassion.or.kr/Files/correspondence/"   //Url
                            //sUrl = "http://ws.compassion.or.kr/Files/correspondence/"   //Url
                            sUrl = sFilePath  //Url
                                    + int.Parse(sPackageId).ToString().Trim()              //패키지ID
                                    + "/" + sChildLetterKey.ToString().Trim()                    //어린이키
                                    + "-"
                                    + i.ToString() + ".jpg";                   //페이지수
                        }
                        else
                        {
                            //sUrl = sFilePath // "http://ws.compassion.or.kr/Files/correspondence/"
                            //sUrl = "http://ws.compassion.or.kr/Files/correspondence/"
                            sUrl = sFilePath
                                    + int.Parse(sPackageId).ToString().Trim() + "/"
                                    + corrId
                                    + "_"
                                    + i.ToString()
                                    + "_"
                                    + sChildLetterKey.ToString().Trim()
                                    + ".jpg";
                        }
                        screening_images.Add(sUrl);
                    }
                }

                #endregion

                #region 첨부파일 

                try
                {
                    //편지 대표ID 바인딩
                    string sRepCorrespondenceID = _wwwService.getRepCorrespondenceID(sess.SponsorID, corrId);

                    //DataSet
                    DataSet dsChildInfo = _wwwService.getWebMailUpdateList(sRepCorrespondenceID);

                    //기존정보 바인딩
                    if (dsChildInfo != null && dsChildInfo.Tables[0].Rows.Count > 0)
                    {
                        //변수선언
                        string sAttachYn = string.Empty; //첨부파일여부

                        sAttachYn = dsChildInfo.Tables[0].Rows[0]["AttachYn"].ToString();
                        //Response.Write("<script>alert('sAttachYn: " + sAttachYn + "');</script>");

                        //첨부파일이 있을경우
                        if (sAttachYn.Equals("Y"))
                        {
                            DataSet dsAttachFileYn = _wwwService.getWebMailFileName(sRepCorrespondenceID);

                            if (dsAttachFileYn.Tables[0].Rows.Count > 0)
                            {
                                string fileName = dsAttachFileYn.Tables[0].Rows[0]["FileName"].ToString();

                                if (!string.IsNullOrEmpty(fileName))
                                {

                                    //rtn_attachment = (attach_path + fileName).WithFileServerHost();
                                    rtn_attachment = "http://ws.compassion.or.kr" + (attach_path + fileName);

                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
                }


                #endregion

            }
            else
            {

                // 읽음처리
                this.SetRead(correspondenceId, childMasterId);

                #region 받는편지 데이타
                /*20170406 수정 : 번역여부와 관계없이 스크리닝 텍스트가 있으면 표시*/
                //if(sTranslationNeed.Equals("Y")) {
                if (sScreeningStatus == "END")
                {
                    rtn_translateContent += sScreeningContext.ToHtml();
                }
                else
                {
                    rtn_translateContent = "번역중입니다.";
                }
                //} else {
                //	rtn_translateContent = "";
                //}

                if (sScreeningDate == "")
                {
                    rtn_translateContent = "이미지 스캔 전입니다.";
                }
                #endregion

                #region 받은편지 이미지

                if (sFilePath != "" && imageCount > 0)
                {
                    var sUrl = "";
                    if (int.Parse(sPackageId) < 53640)
                    {
                        // 이미지 문구 : 2008년 11월 중순 이후 한국컴패션에 도착한 어린이 편지부터 조회가 가능합니다.
                        //sUrl = "/image/mypage/no_letter_img.jpg";
                        //screening_images.Add(sUrl);
                    }
                    else
                    {
                        for (int i = 1; i <= imageCount; i++)
                        {
                            if (Convert.ToInt32(corrId) >= 3000000)
                            {
                                //sUrl = sFilePath + Convert.ToInt32(sPackageId).ToString() + "/" + corrId + "_" + i.ToString() + "_" + sChildKey + ".jpg";
                                sUrl = sFilePath + Convert.ToInt32(sPackageId).ToString() + "/" + corrId + "_" + i.ToString() + "_" + sChildLetterKey + ".jpg";
                            }
                            else
                            {
                                //sUrl = sFilePath + Convert.ToInt32(sPackageId).ToString() + "/" + sChildKey + "-" + i.ToString() + ".jpg";
                                sUrl = sFilePath + Convert.ToInt32(sPackageId).ToString() + "/" + sChildLetterKey + "-" + i.ToString() + ".jpg";

                            }

                            screening_images.Add(sUrl);
                        }
                    }


                }

                #endregion

            }

            Dictionary<string, object> data = new Dictionary<string, object>() {
                { "baseData" , list } ,
                { "detailData" , detail } ,
                { "images" , screening_images } ,
                { "sendContent" , rtn_sendContent } ,
                { "translateContent" , rtn_translateContent } ,
                { "attachment" , rtn_attachment } ,
                { "screeningStatus" , sScreeningStatus } ,
                { "screeningName" , sScreeningName } ,
                { "translationStatus" , sTranslationStatus } ,
                { "translationName" , sTranslationName } ,
            };

            result.data = data;
            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }

    // 발송대기,임시저장 데이타 조회
    public JsonWriter GetTemporaryDetail(string RepCorrespondenceID)
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();

        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }

        try
        {


            Object[] objSql = new object[1] { "sp_web_tCorrespondenceWeb_list_f" };
            Object[] objParam = new object[] { "SponsorID", "RepCorrespondenceID" };
            Object[] objValue = new object[] { sess.SponsorID, @RepCorrespondenceID };
            var list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

            if (list.Rows.Count < 1)
            {
                result.message = "조회결과가 존재하지 않습니다(0).";
                return result;
            }

            result.data = list;
            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }

    // 읽지 않은 받은편지 총수 
    public class UnreadEntity
    {
        public int cnt;
        public string childMasterId;
        public string childKey;
        public string childName;
    }

    public JsonWriter GetUnreadCount()
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();

        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }

        var cookie = HttpContext.Current.Request.Cookies[Constants.LETTERCOUNT_COOKIE_NAME];

        try
        {

            if (cookie != null)
            {
                result.data = cookie.Value.Decrypt().ToObject<UnreadEntity>();
                result.success = true;
                return result;
            }

            Object[] objSql = new object[1] { "Corr_MyPageLetterUnread" };
            Object[] objParam = new object[] { "SponsorID" };
            Object[] objValue = new object[] { sess.SponsorID };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);



            //result.data = Convert.ToInt32(ds.Tables[0].Rows[0]["cnt"].ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                result.data = new UnreadEntity()
                {
                    cnt = Convert.ToInt32(ds.Tables[0].Rows[0]["cnt"].ToString()),
                    childMasterId = ds.Tables[0].Rows[0]["childMasterId"].ToString(),
                    childKey = ds.Tables[0].Rows[0]["childKey"].ToString(),
                    childName = ds.Tables[0].Rows[0]["ChildName"].ToString()
                };
                result.success = true;

                cookie = new HttpCookie(Constants.LETTERCOUNT_COOKIE_NAME);
                cookie.Value = result.data.ToJson().Encrypt();
                cookie.Path = "/";
                cookie.Expires = DateTime.Now.AddMinutes(10);
                HttpContext.Current.Response.Cookies.Set(cookie);
            }

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, (cookie != null ? cookie.Value + " : " : "") + ex.Message);
            result.message = "읽지않은 편지수정보를 가져오는데 실패했습니다.";
        }

        return result;
    }

    // 선택한 어린이의 편지요약정보 , childMasterIds : , 구분자
    public JsonWriter GetTotal(string childMasterIds)
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();

        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }


        try
        {

            Object[] objSql = new object[1] { "Corr_MyPageLetterList2016" };
            Object[] objParam = new object[] { "DIVIS", "SponsorID", "ChildMasterID" };
            Object[] objValue = new object[] { "TOTAL_CHILD", sess.SponsorID, childMasterIds };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            result.data = ds.Tables[0];
            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "데이타를 가져오는데 실패했습니다.";
        }

        return result;
    }

    // 읽음처리
    public JsonWriter SetRead(string correspondenceId, string childMasterId)
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();

        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }

        if (string.IsNullOrEmpty(correspondenceId) || string.IsNullOrEmpty(childMasterId))
        {
            result.message = "필수값 부족";
            return result;
        }

        try
        {
            string sResult = _wwwService.setWebMailListModify(correspondenceId);

            //2018-04-18 이종진 #CO-120 : agent 접속모드 확인하여 넣어줌 tCorrespondenceView.RegAgent 필드 추가하여 해당필드에 넣음
            var isApp = AppSession.HasCookie(HttpContext.Current);
            var WebMode = HttpContext.Current.Request.UserAgent.ToLower();
            if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0)) { WebMode = "MOBILE"; }
            else { WebMode = "PC"; }
            string agent = isApp ? "APP" : WebMode;   //채널_액세스유형    WWW / M / APP

            Object[] objSql = new object[1] { "usp_CorrespondenceView_I" };
            Object[] objParam = new object[] { "CorrespondenceID", "SponsorID", "ChildMasterID", "RegAgent" };
            Object[] objValue = new object[] { correspondenceId, sess.SponsorID, childMasterId, agent };
            _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

            result.success = true;

            // 갯수변동시 쿠키제거
            HttpCookie cookie = HttpContext.Current.Response.Cookies[Constants.LETTERCOUNT_COOKIE_NAME];
            cookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Set(cookie);

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "읽음처리 실패";
        }

        return result;
    }

    // 발송취소
    public JsonWriter Cancel(string correspondenceId)
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();

        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }

        try
        {
            string sResult = _wwwService.setWebMailListModify(correspondenceId);

            if (sResult.Equals("10"))
            {

                Object[] objSql = new object[1] { " UPDATE tCorrespondenceWeb " +
                                                      " SET    ModifyID = '" + sess.UserId + "', " +
                                                      "        ModifyName = '" + sess.UserName + "', " +
                                                      "        ModifyDate = GETDATE() " +
                                                      " WHERE  RepCorrespondenceID = '" + correspondenceId + "' " };
                DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

                result.message = "편지가 삭제되었습니다.";
                result.success = true;
            }
            else
            {
                result.message = "발송취소 실패.\\r\\n 다시 시도해 주세요.";

            }


        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "발송취소 실패.\\r\\n 다시 시도해 주세요.";
        }

        return result;
    }

    // 대상어린이 국가
    public JsonWriter GetCountries(string type)
    {
        JsonWriter result = new JsonWriter() { success = false };

        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();

        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }

        string correspondenceType = "";
        if (type == "send" || type == "send_temp")
            correspondenceType = "SPN";
        else if (type == "receive")
            correspondenceType = "CHI";

        try
        {
            Object[] objSql = new object[1] { "Corr_MyPageLetterList2016" };
            Object[] objParam = new object[3] { "DIVIS", "SponsorID", "correspondenceType" };
            Object[] objValue = new object[3] { "DDLCountryName", sess.SponsorID, correspondenceType };

            // CountryID , CountryName
            result.data = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "국가 목록을 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }

    // 선택한 국가의 어린이
    public JsonWriter GetChildren(string type, string countryName)
    {
        JsonWriter result = new JsonWriter() { success = false };

        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();

        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }

        string correspondenceType = "";
        if (type == "send" || type == "send_temp")
            correspondenceType = "SPN";
        else if (type == "receive")
            correspondenceType = "CHI";

        try
        {
            Object[] objSql = new object[1] { "Corr_MyPageLetterList2016" };
            Object[] objParam = new object[4] { "DIVIS", "SponsorID", "correspondenceType", "CountryName" };
            Object[] objValue = new object[4] { "DDLChildName", sess.SponsorID, correspondenceType, countryName };

            // NameKr , ChildMasterID
            result.data = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "어린이 목록을 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }

    // 샘플 레터
    public JsonWriter GetSampleLetterTypes()
    {
        JsonWriter result = new JsonWriter() { success = false };
        DataSet sdata = new DataSet();

        try
        {
            sdata = _wwwService.getSampleLetterTypeOrder();

            if (sdata != null)
            {

                if (sdata.Tables[0].Rows.Count > 0)
                {

                    List<LetterTypeEntity> list = new List<LetterTypeEntity>();
                    foreach (DataRow dr in sdata.Tables[0].Rows)
                    {

                        var SampleLetterType = dr["SampleLetterType"].ToString();
                        if (LetterAction.LetterTypes.Any(p => p.code3 == SampleLetterType))
                        {
                            list.Add(new LetterTypeEntity()
                            {
                                code3 = SampleLetterType,
                                code6 = LetterAction.LetterTypes.First(p => p.code3 == SampleLetterType).code6,
                                title = LetterAction.LetterTypes.First(p => p.code3 == SampleLetterType).title,
                            });
                        }
                    }

                    result.data = list;
                    result.success = true;
                    return result;
                }
                else
                {
                    result.message = "데이터 불러오기를 실패하였습니다";
                    return result;
                }
            }
            else
            {

                result.message = "데이터 불러오기를 실패하였습니다";
                return result;
            }

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
            result.message = "데이터 불러오기를 실패하였습니다";
            return result;
        }

    }

    // 샘플 레터
    public JsonWriter GetSampleLetters(string type)
    {
        JsonWriter result = new JsonWriter() { success = false };

        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();

        try
        {
            Object[] objSql = new object[1] { "sp_web_sample_letter_list_f" };
            Object[] objParam = new object[] { "type" };
            Object[] objValue = new object[] { type };

            // NameKr , ChildMasterID
            var list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
            list.Columns.Add("used");

            // 사용된 예문인지 여부
            using (MainLibDataContext dao = new MainLibDataContext())
            {

                foreach (DataRow dr in list.Rows)
                {
                    var SampleLetterCode = dr["SampleLetterCode"].ToString();
                    //dr["used"] = dao.tLetterUsedSample.Any(p => p.SponsorID == sess.SponsorID && p.SampleLetterCode == SampleLetterCode) ? "Y" : "N";
                    dr["used"] = www6.selectQ<tLetterUsedSample>("SponsorID", sess.SponsorID, "SampleLetterCode", SampleLetterCode).Any() ? "Y" : "N";
                }

            }

            result.data = list;
            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "샘플편지를 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }

    // 발송취소
    public class SaveEntity
    {

        public bool is_pic_letter = false;  // 사진편지
        public string status;               // T : 임시저장 , N : 신규등록
        public string samplelettercode;     // 선택한 편지샘플 코드
        public string letterComment;        // 내용
        public List<child> children = new List<child>();        // 선택된 아이
        public string lang;                 // 언어 , ko , en
        public string fileNameWithPath;     // 첨부파일 경로 예 (/images/letter/~)
        public string repCorrespondenceID;  // 
        public string fileName
        {
            get
            {
                if (string.IsNullOrEmpty(this.fileNameWithPath))
                    return "";
                return this.fileNameWithPath.Substring(this.fileNameWithPath.LastIndexOf('/') + 1);
            }

        }

        int _fileSize = 0;
        // 파일크기를 못구한경우 실제 크기를 조회한다.
        public int fileSize
        {
            get
            {
                if (_fileSize < 1)
                {
                    if (string.IsNullOrEmpty(this.fileNameWithPath))
                    {
                        return _fileSize;
                    }

                    try
                    {
                        var path = HttpContext.Current.Server.MapPath(this.fileNameWithPath);
                        if (File.Exists(path))
                        {
                            return Convert.ToInt32(new FileInfo(path).Length / 1024);
                        }
                        else
                        {
                            return _fileSize;
                        }

                    }
                    catch
                    {
                        return _fileSize;
                    }
                }
                return _fileSize;
            }
            set { this._fileSize = value; }
        }

        public string fileExtension
        {
            get
            {
                if (string.IsNullOrEmpty(this.fileNameWithPath))
                    return "";
                return this.fileNameWithPath.Substring(this.fileNameWithPath.LastIndexOf('.') + 1).ToLower();
            }
        }
        public string letterType;           // 6자리
        public int isTempChange = 0;

        public class child
        {
            public string childMasterId;
            public string childKey;
            public string nameKr;
            public string personalNameEng;
        }

        public string newFile;

        public string giftSeq;
        public string giftIsStoreItem;

    }

    public class LetterTypeEntity
    {
        public string code3
        {
            get;
            set;
        }
        public string code6
        {
            get;
            set;
        }
        public string title
        {
            get;
            set;
        }
    }

    public static List<LetterTypeEntity> LetterTypes
    {
        get
        {

            return
                new List<LetterTypeEntity>() {
                    new LetterTypeEntity(){code3 = "BIO" , code6 = "SPNBIO" , title = "첫편지" } ,
                    new LetterTypeEntity(){code3 = "BDC" , code6 = "SPNBDC" , title = "생일카드" } ,
                    new LetterTypeEntity(){code3 = "BBI" , code6 = "SPNBBI" , title = "성장축하편지" } ,
                    new LetterTypeEntity(){code3 = "BBC" , code6 = "SPNBBC" , title = "어린이날편지" } ,
                    new LetterTypeEntity(){code3 = "CC" , code6 = "SPNCMS" , title = "크리스마스편지" } ,
                    new LetterTypeEntity(){code3 = "LTR" , code6 = "SPNLTR" , title = "일반편지" }

                };
        }
    }

    // 첨부파일 삭제
    public JsonWriter FileDelete(string fileNameWithPath)
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();
        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }

        var extension = fileNameWithPath.Substring(fileNameWithPath.LastIndexOf('.')).ToLower();
        if (!new List<string>() { ".jpg", ".png", ".jpeg", ".gif" }.Contains(extension))
        {
            result.message = "허용되지 않은 확장자 입니다.";
            return result;
        }

        var cookieValue = HttpContext.Current.Request.Cookies[Constants.ENDUSER_COOKIE_NAME].Value;

        var apiUrl = ConfigurationManager.AppSettings["domain_file"] + "/common/handler/file-delete.ashx?path=" + HttpUtility.UrlEncode(fileNameWithPath);
        var cc = new CookieContainer();
        cc.Add(new Cookie(Constants.ENDUSER_COOKIE_NAME, cookieValue, "/", FrontLoginSession.GetDomain(HttpContext.Current)));

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
        request.CookieContainer = cc;
        request.AllowAutoRedirect = false;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {

            using (var reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8))
            {
                string responseText = reader.ReadToEnd();
                //ErrorLog.Write(HttpContext.Current, 0, "responseText>" + responseText);
            }

        }


        result.success = true;
        return result;

    }

    // 등록
    public JsonWriter Add(SaveEntity entity)
    {
        ErrorLog.Write(HttpContext.Current, 0, "LetterAction.Add Start");

        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        if (string.IsNullOrEmpty(entity.letterType))
        {
            result.message = "편지종류를 선택하지 않았습니다(1)";
            return result;
        }

        if (entity.children.Count < 1)
        {
            result.message = "어린이를 선택하지 않았습니다.";
            return result;
        }

        UserInfo sess = new UserInfo();
        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }

        DataTable dtSponChildList = _wwwService.getSponChildList_Web(sess.SponsorID).Tables[0];

        string sChildID = string.Empty; //childMasterID
        string sChildKey = string.Empty; //childKey
        string sCorrClass = entity.letterType; //편지종류
        string sContext = entity.letterComment.Replace("'", "`");
        string sLanguageType = entity.lang == "ko" ? "한글" : "영어";
        string sAttachYn = string.IsNullOrEmpty(entity.fileName) ? "N" : "Y";
        string giftSeq = entity.giftSeq;

        foreach (var child in entity.children.OrderBy(p => p.childKey).ToList())
        {
            dtSponChildList.DefaultView.RowFilter = "childMasterId='" + child.childMasterId + "'";
            if (dtSponChildList.DefaultView.Count > 0 &&
                dtSponChildList.DefaultView[0]["FirstFundedDate"].ToString() == "" && dtSponChildList.DefaultView[0]["SponsorTypeEng"].ToString().IndexOf("COR") < 0)
            {
                result.message = "첫 후원금 납부 후 어린이에게 편지를 쓰실 수 있습니다\n납부 관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr ";
                return result;
            }
            else
            {

                sChildID += child.childMasterId + ",";
                sChildKey += child.childKey + ",";
            }

        }

        ErrorLog.Write(HttpContext.Current, 0, "isPicLetter:" + entity.is_pic_letter.ToString() + " / entity.giftSeq:" + entity.giftSeq + " / sCorrClass:" + sCorrClass);

        if (entity.children.Count > 1)
        {
            switch (entity.letterType)
            {
                case "SPNBIO":
                    sCorrClass = "XCSBIO";  //첫편지
                    break;
                case "SPNBDC":
                    sCorrClass = "XCSBDC";  //생일편지
                    break;
                case "SPNBBI":
                    sCorrClass = "XCSBBI";  //성장보고 답장편지
                    break;
                case "SPNBBC":
                    sCorrClass = "XCSBBC";  //어린이날편지 
                    break;
                case "SPNCMS":
                    sCorrClass = "XCSCC";   //크리스마스편지
                    break;
                case "SPNLTR":
                    sCorrClass = "XCSLTR";  //일반편지
                    break;
            }

            if (sAttachYn == "Y" && sCorrClass == "XCSLTR")
                sCorrClass = "XCSATM";
            if (sAttachYn == "Y" && sCorrClass == "SPNLTR")
                sCorrClass = "SPNATM";
            // 사진 편지의 경우 SPNPIC로 등록되도록 추가 문희원 - 2017-04-17
            if (entity.is_pic_letter)
                sCorrClass = "SPNPIC";

            ErrorLog.Write(HttpContext.Current, 0, "isPicLetter:" + entity.is_pic_letter.ToString() + " / entity.giftSeq:" + entity.giftSeq + " / sCorrClass:" + sCorrClass);


            // 선물편지
            //if (!entity.giftSeq.Equals("0"))
            //{
            //    //2018-04-19 이종진 - WO-114 
            //    //무료선물편지일 경우, SPNATM -> SPNPPR로 변경요청. SPNLTR로 들어가면, compass5에서 확인 시, SPNLTR->SPNATM으로 변경함.
            //    sCorrClass = entity.giftIsStoreItem.Equals("Y") ? "SPNPRE" : "SPNPPR";
            //    //sCorrClass = entity.giftIsStoreItem.Equals("Y") ? "SPNPRE" : "SPNLTR";
            //}
        }

        // 선물편지
        if (!entity.giftSeq.Equals("0"))
        {
            //2018-04-19 이종진 - WO-114 
            //무료선물편지일 경우, SPNATM -> SPNPPR로 변경요청. SPNLTR로 들어가면, compass5에서 확인 시, SPNLTR->SPNATM으로 변경함.
            sCorrClass = entity.giftIsStoreItem.Equals("Y") ? "SPNPRE" : "SPNPPR";
            //sCorrClass = entity.giftIsStoreItem.Equals("Y") ? "SPNPRE" : "SPNLTR";
        }

        ErrorLog.Write(HttpContext.Current, 0, "isPicLetter:" + entity.is_pic_letter.ToString() + " / entity.giftSeq:" + entity.giftSeq + " / sCorrClass:" + sCorrClass);
        //데이터 저장
        try
        {
            string fname = entity.fileName;
            string fileExtension = entity.fileExtension;


            //등록
            DataSet dRs = new DataSet();

            // Mac에서 파일업로드시 한글 자모분리 방지

            string fname1 = fname.Normalize(NormalizationForm.FormD);
            string fname2 = fname1.Normalize(NormalizationForm.FormC);
            fname = fname2;

            ErrorLog.Write(HttpContext.Current, 0, "sCorrClass:" + sCorrClass + " / entity.giftSeq:" + giftSeq);

            var CorrespondenceClass = AppSession.HasCookie(HttpContext.Current) ? "앱편지" : "웹편지";
            Object[] objSql = new object[1] { "sp_ins_tCorrespondence" };
            Object[] objParam = new object[] { "ArrChildMasterID" , "ArrChildKey", "CorrespondenceWebID", "SponsorID", "CorrespondenceType", "CorrespondenceClass", "LanguageType", "AttachYN",
                "CorrespondenceContext", "CorrStatus", "RepCorrespondenceID", "PrintFlag", "CurrentUse", "RegisterID", "RegisterName", "FileID", "FileName", "FileExtention", "FileSize", "IsTempChange",
                "SampleLetterCode", "GiftSeq" };
            Object[] objValue = new object[] { sChildID, sChildKey,"" , sess.SponsorID  , sCorrClass , CorrespondenceClass , sLanguageType , sAttachYn ,
                sContext , entity.status , "1" , "N" , "Y" , sess.SponsorID ,  sess.UserName , DateTime.Now.ToString("yyyyMMddHHmmssff2") ,  fname , fileExtension , entity.fileSize, entity.isTempChange,
                entity.samplelettercode, giftSeq };

            // NameKr , ChildMasterID
            dRs = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

            if (dRs == null)
            {
                this.FileDelete(entity.fileNameWithPath);
                result.message = "편지등록에 실패했습니다.";
                return result;

            }
            else if (dRs.Tables.Count > 0)
            {
                if (dRs.Tables[0].Rows.Count > 0)
                {
                    if (dRs.Tables[0].Rows[0]["Result"].ToString() != "Y")
                    {

                        this.FileDelete(entity.fileNameWithPath);
                        result.message = dRs.Tables[0].Rows[0]["Result_String"].ToString();
                        return result;

                    }
                    else if (dRs.Tables[0].Rows[0]["Result"].ToString() == "Y")
                    {

                        try
                        {

                            Object[] objSql2 = new object[1] { string.Format("select top 1 RepCorrespondenceId, CorrespondenceWebID from tCorrespondenceWeb where sponsorId='{0}' order by registerDate desc", sess.SponsorID) };
                            var CorrData = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "TEXT", null, null);
                            //var ReqCorrespondenceId = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "TEXT", null, null).Tables[0].Rows[0]["RepCorrespondenceId"].ToString();
                            var ReqCorrespondenceId = CorrData.Tables[0].Rows[0]["RepCorrespondenceId"].ToString();
                            result.data = ReqCorrespondenceId;

                            var CorrespondenceWebID = CorrData.Tables[0].Rows[0]["CorrespondenceWebID"].ToString();
                            result.message = CorrespondenceWebID.EmptyIfNull();

                            using (MainLibDataContext dao = new MainLibDataContext())
                            {

                                // 사진 편지인 경우 기록남김 (선물편지인 경우 추가)
                                if (entity.is_pic_letter || !entity.giftSeq.Equals("0"))
                                {

                                    var item = new tLetterPic() { RepCorrespondenceID = ReqCorrespondenceId, SponsorID = sess.SponsorID, RegisterDate = DateTime.Now };
                                    //dao.tLetterPic.InsertOnSubmit(item);
                                    www6.insert(item);
                                    //dao.SubmitChanges();
                                }

                                // 샘플 사용정보 저장
                                if (!string.IsNullOrEmpty(entity.samplelettercode))
                                {

                                    var item = new tLetterUsedSample()
                                    {
                                        RegisterDate = DateTime.Now,
                                        SampleLetterCode = entity.samplelettercode,
                                        SponsorID = sess.SponsorID
                                    };

                                    //dao.tLetterUsedSample.InsertOnSubmit(item);
                                    www6.insert(item);
                                    //dao.SubmitChanges();

                                }
                            }

                            // stamp 발급
                            new StampAction().Add("mail");

                        }
                        catch (Exception ex)
                        {
                            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
                        }

                        result.success = true;
                        return result;

                    }
                    else
                    {
                        this.FileDelete(entity.fileNameWithPath);
                        result.message = "편지등록에 실패했습니다.";
                        return result;
                    }
                }
                else
                {
                    this.FileDelete(entity.fileNameWithPath);
                    result.message = "편지등록에 실패했습니다.";
                    return result;
                }
            }
            else
            {
                this.FileDelete(entity.fileNameWithPath);
                result.message = "편지등록에 실패했습니다.";
                return result;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
            this.FileDelete(entity.fileNameWithPath);
            result.message = "편지쓰기 등록하는 중 오류가 발생했습니다.";
            return result;

        }

    }

    // 수정
    public JsonWriter Update(SaveEntity entity)
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        if (string.IsNullOrEmpty(entity.letterType))
        {
            result.message = "편지종류를 선택하지 않았습니다";
            return result;
        }

        if (entity.children.Count < 1)
        {
            result.message = "어린이를 선택하지 않았습니다.";
            return result;
        }

        UserInfo sess = new UserInfo();
        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            return result;
        }

        DataTable dtSponChildList = _wwwService.getSponChildList_Web(sess.SponsorID).Tables[0];

        string sChildID = entity.children[0].childMasterId; //childMasterID , SP 내부에서 사용하지 않지만, 기존로직과 맞추기 위해서 대상어린이 첫번째 정보로 세팅한다.
        string sChildKey = entity.children[0].childKey; //childKey
        string sCorrClass = entity.letterType; //편지종류
        string sContext = entity.letterComment.Replace("'", "`");
        string sLanguageType = entity.lang == "ko" ? "한글" : "영어";
        string sAttachYn = string.IsNullOrEmpty(entity.fileName) ? "N" : "Y";
        string newFile = entity.newFile;
        string giftSeq = entity.giftSeq;

        if (entity.children.Count > 1)
        {
            switch (entity.letterType)
            {
                case "SPNBIO":
                    sCorrClass = "XCSBIO";  //첫편지
                    break;
                case "SPNBDC":
                    sCorrClass = "XCSBDC";  //생일편지
                    break;
                case "SPNBBI":
                    sCorrClass = "XCSBBI";  //성장보고 답장편지
                    break;
                case "SPNBBC":
                    sCorrClass = "XCSBBC";  //어린이날편지 
                    break;
                case "SPNCMS":
                    sCorrClass = "XCSCC";   //크리스마스편지
                    break;
                case "SPNLTR":
                    sCorrClass = "XCSLTR";  //일반편지
                    break;
            }

            if (sAttachYn == "Y" && sCorrClass == "XCSLTR")
                sCorrClass = "XCSATM";
            if (sAttachYn == "Y" && sCorrClass == "SPNLTR")
                sCorrClass = "SPNATM";

            // 선물편지
            //if (!entity.giftSeq.Equals("0"))
            //{
            //    //2018-04-19 이종진 - WO-114 
            //    //무료선물편지일 경우, SPNATM -> SPNPPR로 변경요청. SPNLTR로 들어가면, compass5에서 확인 시, SPNLTR->SPNATM으로 변경함.
            //    sCorrClass = entity.giftIsStoreItem.Equals("Y") ? "SPNPRE" : "SPNPPR";
            //    //sCorrClass = entity.giftIsStoreItem.Equals("Y") ? "SPNPRE" : "SPNLTR";
            //}
        }

        // 선물편지
        if (!entity.giftSeq.Equals("0"))
        {
            //2018-04-19 이종진 - WO-114 
            //무료선물편지일 경우, SPNATM -> SPNPPR로 변경요청. SPNLTR로 들어가면, compass5에서 확인 시, SPNLTR->SPNATM으로 변경함.
            sCorrClass = entity.giftIsStoreItem.Equals("Y") ? "SPNPRE" : "SPNPPR";
            //sCorrClass = entity.giftIsStoreItem.Equals("Y") ? "SPNPRE" : "SPNLTR";
        }

        //데이터 저장
        try
        {

            DataSet dRs = new DataSet();

            // Mac에서 파일업로드시 한글 자모분리 방지
            string fname = entity.fileName;
            if (newFile.Equals("Y"))
            {
                string fname1 = fname.Normalize(NormalizationForm.FormD);
                string fname2 = fname1.Normalize(NormalizationForm.FormC);
                fname = fname2;
            }

            Object[] objSql = new object[1] { "sp_web_tCorrespondenceWeb_update_f" };
            Object[] objParam = new object[] { "ChildMasterID" , "ChildKey", "SponsorID", "CorrespondenceType", "CorrespondenceClass", "LanguageType", "AttachYN",
                "CorrespondenceContext", "RepCorrespondenceID", "ModifyID", "ModifyName", "FileID", "FileName", "FileExtention", "FileSize" , "CorrStatus", "IsTempChange",
                "SampleLetterCode", "GiftSeq" };
            Object[] objValue = new object[] { sChildID, sChildKey, sess.SponsorID  , sCorrClass , "웹편지" , sLanguageType , sAttachYn ,
                sContext , entity.repCorrespondenceID , sess.SponsorID ,  sess.UserName , DateTime.Now.ToString("yyyyMMddHHmmssff2") ,  fname , entity.fileExtension , entity.fileSize , entity.status, entity.isTempChange,
                entity.samplelettercode, giftSeq };

            dRs = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

            if (dRs == null)
            {
                this.FileDelete(entity.fileNameWithPath);
                result.message = "편지등록에 실패했습니다.";
                return result;

            }
            else if (dRs.Tables.Count > 0)
            {
                if (dRs.Tables[0].Rows.Count > 0)
                {
                    if (dRs.Tables[0].Rows[0]["Result"].ToString() != "Y")
                    {

                        this.FileDelete(entity.fileNameWithPath);
                        result.message = dRs.Tables[0].Rows[0]["Result_String"].ToString();
                        return result;

                    }
                    else if (dRs.Tables[0].Rows[0]["Result"].ToString() == "Y")
                    {

                        try
                        {

                            Object[] objSql2 = new object[1] { string.Format("select top 1 RepCorrespondenceId, CorrespondenceWebID from tCorrespondenceWeb where sponsorId='{0}' and RepCorrespondenceID='{1}' order by registerDate desc", sess.SponsorID, entity.repCorrespondenceID) };
                            var CorrData = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "TEXT", null, null);
                            //var ReqCorrespondenceId = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "TEXT", null, null).Tables[0].Rows[0]["RepCorrespondenceId"].ToString();
                            var ReqCorrespondenceId = CorrData.Tables[0].Rows[0]["RepCorrespondenceId"].ToString();
                            result.data = ReqCorrespondenceId.EmptyIfNull();

                            var CorrespondenceWebID = CorrData.Tables[0].Rows[0]["CorrespondenceWebID"].ToString();
                            result.message = CorrespondenceWebID.EmptyIfNull();

                            // 샘플 사용정보 저장
                            if (!string.IsNullOrEmpty(entity.samplelettercode))
                            {
                                using (MainLibDataContext dao = new MainLibDataContext())
                                {

                                    var item = new tLetterUsedSample()
                                    {
                                        RegisterDate = DateTime.Now,
                                        SampleLetterCode = entity.samplelettercode,
                                        SponsorID = sess.SponsorID
                                    };

                                    //dao.tLetterUsedSample.InsertOnSubmit(item);
                                    www6.insert(item);
                                    //dao.SubmitChanges();
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
                        }

                        result.success = true;
                        return result;

                    }
                    else
                    {
                        this.FileDelete(entity.fileNameWithPath);
                        result.message = "편지등록에 실패했습니다.";
                        return result;
                    }
                }
                else
                {
                    this.FileDelete(entity.fileNameWithPath);
                    result.message = "편지등록에 실패했습니다.";
                    return result;
                }
            }
            else
            {
                this.FileDelete(entity.fileNameWithPath);
                result.message = "편지등록에 실패했습니다.";
                return result;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
            this.FileDelete(entity.fileNameWithPath);
            result.message = "편지쓰기 등록하는 중 오류가 발생했습니다.";
            return result;

        }

    }
}