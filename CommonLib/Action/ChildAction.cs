﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using TCPTModel.Response.ICP_FO;
using Newtonsoft.Json;
using TCPTModel;
using TCPTModel.Response.Beneficiary;
using TCPTModel.Request.Beneficiary;
using System.Collections;
using TCPTModel.Request.Hold.Beneficiary;
using System.Web.UI;
using TCPTModel.Response.Hold.Beneficiary;
using CommonLib;
using System.Data.SqlClient;
using System.Drawing;

public partial class ChildAction
{
	CommonLib.WWWService.ServiceSoap _wwwService; //www5_penta
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;

    Hashtable htCountry;

    public ChildAction() {
        _wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        htCountry = new Hashtable();

        var actionResult = new CountryAction().GetList();

        var countries = (List<sp_country_list_fResult>)actionResult.data;
        foreach (var item in countries)
        {
            htCountry[item.c_id] = item.c_name;
        }

        //htCountry["GH"] = "가나";
        //htCountry["BF"] = "부르키나파소";
        //htCountry["TG"] = "토고";
        //htCountry["UG"] = "우간다";
        //htCountry["RW"] = "르완다";
        //htCountry["LK"] = "스리랑카";
        //htCountry["ET"] = "에티오피아";
        //htCountry["KE"] = "케냐";
        //htCountry["TZ"] = "탄자니아";
        //htCountry["IN"] = "인도";
        //htCountry["BD"] = "방글라데시";
        //htCountry["TH"] = "태국";
        //htCountry["PH"] = "필리핀";
        //htCountry["IO"] = "인도네시아";
        //htCountry["ME"] = "멕시코";
        //htCountry["GU"] = "과테말라";
        //htCountry["ES"] = "엘살바도르";
        //htCountry["NI"] = "니카리과";
        //htCountry["CO"] = "콜롬비아";
        //htCountry["EC"] = "에콰도르";
        //htCountry["PE"] = "페루";
        //htCountry["BO"] = "볼리비아";
        //htCountry["HA"] = "아이티";
        //htCountry["DR"] = "도미니카공화국";
        //htCountry["HO"] = "온두라스";
        //htCountry["BR"] = "브라질";
        //htCountry["ID"] = "인도네시아";
    }

    public JsonWriter Ensure(/*string strChildKey, string strPic*/)
    {

        JsonWriter result = new JsonWriter() { success = false };

        if (!PayItemSession.HasCookie(HttpContext.Current))
        {
            result.message = "선택된 후원정보가 없습니다.";
            return result;
        }

        var payInfo = PayItemSession.GetCookie(HttpContext.Current);

        if (string.IsNullOrEmpty(payInfo.childMasterId))
        {
            result.message = "후원가능한 아동정보가 없습니다.";
            return result;
        }

        return this.Ensure(payInfo.childMasterId);
    }

    //public JsonWriter Ensure(string strChildKey, string strPic)
    //{

    //    JsonWriter result = new JsonWriter() { success = false };

    //    if (!PayItemSession.HasCookie(HttpContext.Current))
    //    {
    //        result.message = "선택된 후원정보가 없습니다.";
    //        return result;
    //    }

    //    var payInfo = PayItemSession.GetCookie(HttpContext.Current);

    //    if (string.IsNullOrEmpty(payInfo.childMasterId))
    //    {
    //        result.message = "후원가능한 아동정보가 없습니다.";
    //        return result;
    //    }

    //    return this.Ensure(payInfo.childMasterId, strChildKey, strPic);
    //}

    public JsonWriter Ensure(string childMasterId)
    {
        // 기존 방식 -어린이 리스트 DB조회
        JsonWriter result = new JsonWriter() { success = false };
        string sponsorID = "";
        if (UserInfo.IsLogin)
        {
            sponsorID = new UserInfo().SponsorID;
        }

        // 아동 keep 기능 추가
        var child_result = new ChildAction().Keep(sponsorID, childMasterId);
        if (!child_result.success)
        {
            result.message = child_result.message;
            return result;
        }
        // 결연된 어린이인지 조회
        child_result = new ChildAction().CheckApply(childMasterId);
        if (!child_result.success)
        {
            result.message = child_result.message;
            return result;
        }

        result.success = true;
        return result;
    }


    //public JsonWriter Ensure(string childMasterId, string strChildKey, string strPic)
    //{

    //    JsonWriter result = new JsonWriter() { success = false };
    //    string sponsorID = "";
    //    if (UserInfo.IsLogin)
    //    {
    //        sponsorID = new UserInfo().SponsorID;
    //    }

    //    //      // 20171127 고진혁 수정 - 결연된 어린이인지 조회 구문을 위로 변경
    //    //// 아동 keep 기능 추가
    //    //var child_result = new ChildAction().Keep(sponsorID , childMasterId);
    //    //if(!child_result.success) {
    //    //	result.message = child_result.message;
    //    //	return result;
    //    //}
    //    //// 결연된 어린이인지 조회
    //    //child_result = new ChildAction().CheckApply(childMasterId);
    //    //if(!child_result.success) {
    //    //	result.message = child_result.message;
    //    //	return result;
    //    //}

    //    // 결연된 어린이인지 조회
    //    var child_result = new ChildAction().CheckApply(childMasterId);
    //    if (!child_result.success)
    //    {
    //        result.message = child_result.message;
    //        return result;
    //    }
    //    // 아동 keep 기능 추가
    //    child_result = new ChildAction().Keep(sponsorID, childMasterId, strChildKey, strPic);
    //    if (!child_result.success)
    //    {
    //        result.message = child_result.message;
    //        return result;
    //    }
    //    result.message = "strChildKey : " + strChildKey;
    //    result.success = true;
    //    return result;
    //}

    public JsonWriter EnsureGp(string childMasterId, string childKey, string Pic, string IsOrphan, string IsHandicapped, string WaitingDays, string Age, string BirthDate, string CountryCode, string Gender, string HangulName, string HangulPreferredName, string FullName, string PreferredName)
    {
        // 신규 방식 -어린이 리스트 Global Pool조회
        JsonWriter result = new JsonWriter() { success = false };
        string UserId = "";
        //[이종진] 추가 : SponsorID도 저장하도록 추가함
        string SponsorID = "";
        if (UserInfo.IsLogin)
        {
            UserId = new UserInfo().UserId;
            SponsorID = string.IsNullOrEmpty(new UserInfo().SponsorID) ? "" : new UserInfo().SponsorID;
        }
        
        // 아동 keep 기능 추가
        // GlobalPool 에 E-Commerce Hold 및 tTmpChildEnsure 테이블에 Insert
        var child_result = new ChildAction().KeepGp(UserId, childMasterId, childKey, Pic, IsOrphan, IsHandicapped, WaitingDays, Age, BirthDate, CountryCode, Gender, HangulName, HangulPreferredName, FullName, PreferredName, SponsorID);
        if (!child_result.success)
        {
            result.message = child_result.message;
            return result;
        }

        // 2017-12-15 이종진
        // KeepGp에서 이미 체크하고 홀딩하는 로직임. 
        // 결연된 어린이인지 조회
        //child_result = new ChildAction().CheckApply(childMasterId);
        //if (!child_result.success)
        //{
        //    result.message = child_result.message;
        //    return result;
        //}
        
        result.success = true;
        return result;
    }

    //Keep 체크시 sessionID 체크 안함
    public JsonWriter EnsureNew(string childMasterId)
    {

        JsonWriter result = new JsonWriter() { success = false };

        string sponsorID = "";
        if (UserInfo.IsLogin)
        {
            sponsorID = new UserInfo().SponsorID;
        }

        // 아동 keep 기능 추가
        var child_result = new ChildAction().KeepNew(sponsorID, childMasterId);
        if (!child_result.success)
        {
            result.message = child_result.message;
            return result;
        }

        // 결연된 어린이인지 조회
        child_result = new ChildAction().CheckApply(childMasterId);

        if (!child_result.success)
        {
            result.message = child_result.message;
            return result;
        }

        result.success = true;
        return result;
    }

    // keep 한 어린이 목록
    public JsonWriter GetEnsures() {
        JsonWriter result = new JsonWriter() { success = false };
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();

        if (strdbgp_kind == "1") // 기존 방식 -어린이 리스트 DB조회
        {
            result = GetEnsuresDb();
        }
        else if (strdbgp_kind == "2") // 신규 방식 -어린이 리스트 Global Pool조회
        {
            result = GetEnsuresGp();
        }

        return result;
    }

    // keep 한 어린이 목록
    public JsonWriter GetEnsuresDb()
    {
        JsonWriter result = new JsonWriter() { success = false };

        DataTable dt;

        if (HttpContext.Current.Request.Cookies["sessionId"] == null)
        {
            return result;
        }

        HttpCookie cookie = HttpContext.Current.Request.Cookies["sessionId"];
        var sessionId = cookie.Value;

        string sponsorID = "";
        if (UserInfo.IsLogin)
        {
            sponsorID = new UserInfo().SponsorID;
        }

        dt = _wwwService.listDATChildEnsure(sessionId, sponsorID).Tables[0];
        result.data = dt;
        result.success = true;

        return result;
    }

    // keep 한 어린이 목록
    public JsonWriter GetEnsuresGp()
    {
        JsonWriter result = new JsonWriter() { success = false };

        DataTable dt;

        if (HttpContext.Current.Request.Cookies["sessionId"] == null)
        {
            return result;
        }

        HttpCookie cookie = HttpContext.Current.Request.Cookies["sessionId"];
        var sessionId = cookie.Value;

        string UserId = "";
        if (UserInfo.IsLogin)
        {
            UserId = new UserInfo().UserId;
        }

        Object[] objSql = new object[] { "sp_S_tTmpChildEnsure" };
        Object[] objParam = new object[] { "Kind", "SessionID", "ChildMasterID", "UserID" };
        Object[] objValue = new object[] { "01", sessionId, "", UserId };
        dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

        result.data = dt;
        result.success = true;

        return result;
    }

    // keep 한 어린이 목록 : sessionId 체크 안함
    public JsonWriter GetEnsuresNew()
    {
        JsonWriter result = new JsonWriter() { success = false };

        string sponsorID = "";
        if (UserInfo.IsLogin)
        {
            sponsorID = new UserInfo().SponsorID;
            DataTable dt = _wwwService.listDATChildEnsure(null, sponsorID).Tables[0];
            result.data = dt;
            result.success = true;
        }
        
        return result;
    }

    // 어린이 선택 , 1시간 keep
    public JsonWriter Keep(string sponsorID, string sChildMasterID)
    {
        // 기존 방식 -어린이 리스트 DB조회
        // 최근 2명까지 
        // 중복되는 경우 

        JsonWriter result = new JsonWriter();

        var sessionId = string.Format("{0}{1}", DateTime.Now.ToString("ddHHmmssfff"), new JoinAction().MakeCertificationNumber(9));
        if (HttpContext.Current.Request.Cookies["sessionId"] == null)
        {
            HttpCookie cookie = new HttpCookie("sessionId");
            cookie.Value = sessionId;
            cookie.Path = "/";
            HttpContext.Current.Response.Cookies.Set(cookie);
        }
        else
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["sessionId"];
            sessionId = cookie.Value;
        }

        //var sessionId = HttpContext.Current.Request.UserHostAddress;

        try
        {

            DataTable dt = _wwwService.getDATChildEnsure("", sChildMasterID, "").Tables[0];

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["sessionid"].ToString() == sessionId && (dt.Rows[0]["sponsorid"].ToString() == "" || dt.Rows[0]["sponsorid"].ToString() == sponsorID))
                {

                    _wwwService.deleteDATChildEnsure(sessionId, sChildMasterID, "");

                }
                else
                {
                    //result.message = "앗, 이 어린이는 이미 양육 중인 어린이입니다. 다른 어린이를 만나보시겠어요?";
                    result.message = "선택하신 어린이는 다른 사용자가 조회중입니다.";
                    result.success = false;
                    return result;
                }

            }

            string sResult = _wwwService.registerDATChildEnsure(sessionId,
                                                                sponsorID, //후원자ID
                                                                sChildMasterID,
                                                                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") //등록일
                                                                , "0"
                                                            );
            if (sResult.Substring(0, 2) == "30")
            { //DB Error
                result.message = "선택하신 어린이를 등록하지 못했습니다. \\r\\n" +
                                                sResult.Substring(2).ToString().Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
                result.success = false;
            }
            else
            {
                result.success = true;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "선택하신 어린이를 등록 중 오류가 발생했습니다.";
            result.success = false;
        }

        #region 최신 2건을 제외하고 나머지 삭제
        try
        {
            DataTable dt = _wwwService.listDATChildEnsure(sessionId, sponsorID).Tables[0];
            dt.DefaultView.RowFilter = "isHide = 0";

            if (dt.Rows.Count > 2)
            {
                List<string> targets = new List<string>();
                dt.DefaultView.Sort = "RegisterDate desc";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i > 1)
                    {
                        targets.Add(dt.DefaultView[i]["ChildMasterId"].ToString());
                    }
                }

                foreach (var target in targets)
                {
                    _wwwService.deleteDATChildEnsure(sessionId, target, sponsorID //후원자ID
                    );
                }
            }
        }
        catch
        {

        }
        #endregion

        return result;
    }

    // 어린이 선택 , 1시간 keep
    public JsonWriter KeepGp(string UserId, string sChildMasterID, string childKey, string Pic, string IsOrphan, string IsHandicapped, string WaitingDays, string Age, string BirthDate, string CountryCode, string Gender, string HangulName, string HangulPreferredName, string FullName, string PreferredName, string SponsorID)
    {
        // 신규 방식 -어린이 리스트 Global Pool조회
        // 최근 2명까지 
        // 중복되는 경우 
        JsonWriter result = new JsonWriter();
        var strHoldId = "";
        var strHoldType = "E-Commerce Hold";

        string strHoldEndDateTimeUtc = "";     // HOLD END DATE - EDT + "T23:59:59Z"

        var childGlobalID = sChildMasterID.Substring(2);
        string strResult = "";

        var sessionId = string.Format("{0}{1}", DateTime.Now.ToString("ddHHmmssfff"), new JoinAction().MakeCertificationNumber(9));
        if (HttpContext.Current.Request.Cookies["sessionId"] == null)
        {
            HttpCookie cookie = new HttpCookie("sessionId");
            cookie.Value = sessionId;
            cookie.Path = "/";
            cookie.Domain = FrontLoginSession.GetDomain(HttpContext.Current);
            HttpContext.Current.Response.Cookies.Set(cookie);
        }
        else
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["sessionId"];
            sessionId = cookie.Value;
        }

        try
        {
            Object[] objSql1 = new object[] { "sp_S_tTmpChildEnsure" };
            Object[] objParam1 = new object[] { "Kind", "SessionID", "ChildMasterID", "UserID" };
            Object[] objValue1 = new object[] { "03", sessionId, sChildMasterID, UserId };

            DataTable dt1 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql1, "SP", objParam1, objValue1).Tables[0];

            if (dt1.Rows.Count == 0)
            {
                result.message = "선택하신 어린이를 등록 중 오류가 발생했습니다.";
                result.success = false;
                return result;
            }

            if (dt1.Rows.Count > 1)
            {
                result.message = "선택하신 어린이는 다른 사용자가 조회중입니다.";
                result.success = false;
                return result;
            }

            strHoldEndDateTimeUtc = dt1.Rows[0]["FullBodyImageURL"].ToString();

            #region Global Pool 홀딩
            // 홀딩 가능 여부 검사
            strResult = CheckChildAvailable(sChildMasterID.Substring(2));
            if (strResult != "")
            {
                result.message = strResult;
                result.success = false;
                return result;
            }

            // TCPT Hold 처리용 킷 생성
            BeneficiaryHoldRequestList item = new BeneficiaryHoldRequestList();
            //                    BeneficiaryHoldRequestList item = SetHoldReqItem(childGlobalID, holdID, HoldType, EDT);
            item.Beneficiary_GlobalID = childGlobalID;
            item.BeneficiaryState = strHoldType;
            //                    item.HoldEndDate = 2017-11-11 + "T23:59:59Z";
            item.HoldEndDate = strHoldEndDateTimeUtc;
            item.IsSpecialHandling = false;
            item.PrimaryHoldOwner = string.IsNullOrEmpty(UserId) ? sessionId : UserId; //비로그인 시, UserID가 없으면 세션id로 대신 넣어서 홀드
            item.GlobalPartner_ID = "KR";

            //                    if (!string.IsNullOrEmpty(HoldID))
            //                        kit.HoldID = HoldID;

            string json = JsonConvert.SerializeObject(item);
            string strHoldResult = string.Empty;
            //                    string processname = "";

            CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();

            // 홀드요청
            strHoldResult = _OffRampService.BeneficiaryHoldRequest_Single_POST(childGlobalID, json);
            //                    processname = "BeneficiaryHoldRequest_Single_POST";

            //// 기존 홀드ID가 있으면 해당 ID로 홀드 정보 업데이트   // 결제 실패해서 노머니 홀드 호출시 필요 코드
            //if (!string.IsNullOrEmpty(HoldId))
            //{
            //    strHoldResult = _OffRampService.BeneficiaryHoldSingle_PUT(childGlobalID, item.HoldID, json);
            //    TCPTResponseMessage updateResult = JsonConvert.DeserializeObject<TCPTResponseMessage>(strHoldResult);

            //    if (!updateResult.IsSuccessStatusCode)
            //    {
            //        strHoldResult = _OffRampService.BeneficiaryHoldRequest_Single_POST(childGlobalID, json);
            //        processname = "BeneficiaryHoldRequest_Single_POST";
            //    }
            //    processname = "BeneficiaryHoldSingle_PUT";
            //}

            TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(strHoldResult);

            // 여기서 Catch 문으로 이동 strHoldEndDateTimeUtc : 2017-12-05T14:42:17Z
            //에러 메시지 :
            //startindex는 문자열의 길이보다 클 수 없습니다.
            //매개 변수 이름 : startindex
            //실패
            if (!msg.IsSuccessStatusCode)
            {
                //result.message = "선택하신 어린이를 등록하지 못했습니다. \\r\\n" +
                //                                strResult.Substring(2).ToString().Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
                result.message = "선택하신 어린이는 다른 사용자가 조회중입니다";
                result.success = false;
                return result;
            }

            // 성공
            BeneficiaryHoldRequestResponse_Kit response = JsonConvert.DeserializeObject<BeneficiaryHoldRequestResponse_Kit>(msg.RequestMessage.ToString());
            BeneficiaryHoldResponseList_Kit res = JsonConvert.DeserializeObject<BeneficiaryHoldResponseList_Kit>(msg.RequestMessage.ToString());
            response = res.BeneficiaryHoldResponseList[0];

            // 노머니 홀드일 겨우만 (결제 시도 후 실패시, 스폰서 글로발 아이디 생성실패 시) 호출 필요
            // InsertHoldHistory 의 경우 문서에 있음
            //                        int n = InsertHoldHistory(response.HoldID, response.Beneficiary_GlobalID, response.Code.ToString(), response.Message, HoldUID, HoldType, EDT);
            //InsertHoldHistory(response);
            
            //Global Pool 에서 홀드 후, HoldID를 받아와서 쿠키에 저장
            strHoldId = response.HoldID;
            if (HttpContext.Current.Request.Cookies["HoldID"] == null)
            {
                HttpCookie cookie = new HttpCookie("HoldID");
                cookie.Value = strHoldId;
                cookie.Path = "/";
                HttpContext.Current.Response.Cookies.Set(cookie);
            }
            else
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies["HoldID"];
                cookie.Value = strHoldId;
            }

            #endregion

            // 기존 홀드 목족중에 스폰서 ID와 세선 아이디가 같으면서 차일드마스터 아이디가 다른 차일드마스터 아이디를 조회 후 전체 삭제
            Object[] objSql3 = new object[] { "sp_S_tTmpChildEnsure" };
            Object[] objParam3 = new object[] { "Kind", "SessionID", "ChildMasterID", "UserID" };
            Object[] objValue3 = new object[] { "04", sessionId, sChildMasterID, UserId };

            DataTable dt3 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql3, "SP", objParam3, objValue3).Tables[0];

            if (dt3.Rows.Count > 0)
            {
                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    Release(dt3.Rows[i]["ChildMasterID"].ToString());
                }
            }

            // 신규 어린이 저장
            Object[] objSql2 = new object[] { "sp_I_tTmpChildEnsure" };
            Object[] objParam2 = new object[] { "SessionID", "ChildMasterID", "HoldId", "UserID", "RegisterID", "ChildKey", "Pic", "IsOrphan", "IsHandicapped", "WaitingDays", "Age", "BirthDate", "CountryCode", "Gender", "HangulName", "HangulPreferredName", "FullName", "PreferredName", "SponsorID" };
            Object[] objValue2 = new object[] { sessionId, sChildMasterID, strHoldId, UserId, UserId, childKey, Pic, IsOrphan, IsHandicapped, WaitingDays, Age, BirthDate, CountryCode, Gender, HangulName, HangulPreferredName, FullName, PreferredName, SponsorID };

            DataTable dt2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2).Tables[0];

            if (dt2.Rows.Count == 0)
            {
                result.message = "선택하신 어린이를 등록 중 오류가 발생했습니다.";
                result.success = false;
                return result;
            }

            if (dt2.Rows[0]["RTNCODE"].ToString() != "1")
            {
                result.message = dt2.Rows[0]["RTNMESSAGE"].ToString();
                result.success = false;
                return result;
            }

            result.success = true;
        }
        catch (Exception ex)
        {

            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "선택하신 어린이를 등록 중 오류가 발생했습니다.";
            //            result.message = ex.Message;
            result.success = false;
        }

        //#region 최신 2건을 제외하고 나머지 삭제
        //try
        //{

        //    DataTable dt = _wwwService.listDATChildEnsure(sessionId, sponsorID).Tables[0];
        //    dt.DefaultView.RowFilter = "isHide = 0";

        //    if (dt.Rows.Count > 2)
        //    {

        //        List<string> targets = new List<string>();
        //        dt.DefaultView.Sort = "RegisterDate desc";

        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            if (i > 1)
        //            {
        //                targets.Add(dt.DefaultView[i]["ChildMasterId"].ToString());
        //            }
        //        }

        //        foreach (var target in targets)
        //        {
        //            _wwwService.deleteDATChildEnsure(sessionId,
        //                                   target,
        //                                   sponsorID //후원자ID
        //                               );
        //        }
        //    }
        //}
        //catch
        //{

        //}
        //#endregion

        return result;
    }

    ///  어린이 결연 가능여부 체크 참고
    string CheckChildAvailable(string Beneficiary_GlobalID)
    {
        try
        {
            //어린이가 available 인지 체크
            CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();
            string result = _OffRampService.BeneficiaryAvailability_GET(Beneficiary_GlobalID);
            // Ineligible 인 경우
            if (result.Contains("Ineligible"))
            {
                return "현재 어린이 상태가 Ineligible(이)라 결연할 수 없습니다.";
            }
            else if (result.Contains("Inactive"))
            {
                return "현재 어린이 상태가 Inactive(이)라 결연할 수 없습니다.";
            }
            else if (result.Contains("Sponsored"))
            {
                return "현재 어린이 상태가 Sponsored(이)라 결연할 수 없습니다.";
            }
        }
        catch (Exception ex)
        {
            return "어린이 상태 확인 중 오류가 발생했습니다.";
        }

        return "";
    }

    // 어린이 선택 , 1시간 keep, sessionID 체크 안함으로 변경 
    public JsonWriter KeepNew(string sponsorID, string sChildMasterID)
    {

        // 최근 2명까지 
        // 중복되는 경우 

        JsonWriter result = new JsonWriter();

        var sessionId = string.Format("{0}{1}", DateTime.Now.ToString("ddHHmmssfff"), new JoinAction().MakeCertificationNumber(9));
        if (HttpContext.Current.Request.Cookies["sessionId"] == null)
        {
            HttpCookie cookie = new HttpCookie("sessionId");
            cookie.Value = sessionId;
            cookie.Path = "/";
            HttpContext.Current.Response.Cookies.Set(cookie);
        }
        else
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["sessionId"];
            sessionId = cookie.Value;
        }

        //var sessionId = HttpContext.Current.Request.UserHostAddress;

        try
        {

            DataTable dt = _wwwService.getDATChildEnsure("", sChildMasterID, "").Tables[0];

            if (dt.Rows.Count > 0)
            {
                if ((dt.Rows[0]["sponsorid"].ToString() == "" || dt.Rows[0]["sponsorid"].ToString() == sponsorID))
                {

                    _wwwService.deleteDATChildEnsure(sessionId, sChildMasterID, "");

                }
                else
                {
                    //result.message = "앗, 이 어린이는 이미 양육 중인 어린이입니다. 다른 어린이를 만나보시겠어요?";
                    result.message = "선택하신 어린이는 다른 사용자가 조회중입니다.";
                    result.success = false;
                    return result;
                }

            }

            string sResult = _wwwService.registerDATChildEnsure(sessionId,
                                                                sponsorID, //후원자ID
                                                                sChildMasterID,
                                                                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") //등록일
                                                                , "0"
                                                            );
            if (sResult.Substring(0, 2) == "30")
            { //DB Error
                result.message = "선택하신 어린이를 등록하지 못했습니다. \\r\\n" +
                                                sResult.Substring(2).ToString().Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
                result.success = false;
            }
            else
            {
                result.success = true;
            }


        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "선택하신 어린이를 등록 중 오류가 발생했습니다.";
            result.success = false;
        }

        #region 최신 2건을 제외하고 나머지 삭제
        try
        {

            DataTable dt = _wwwService.listDATChildEnsure(sessionId, sponsorID).Tables[0];
            dt.DefaultView.RowFilter = "isHide = 0";

            if (dt.Rows.Count > 2)
            {

                List<string> targets = new List<string>();
                dt.DefaultView.Sort = "RegisterDate desc";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i > 1)
                    {
                        targets.Add(dt.DefaultView[i]["ChildMasterId"].ToString());
                    }
                }

                foreach (var target in targets)
                {
                    _wwwService.deleteDATChildEnsure(sessionId,
                                           target,
                                           sponsorID //후원자ID
                                       );
                }
            }
        }
        catch
        {

        }
        #endregion
        return result;
    }

    // keep 해지
    public string Release( string sChildMasterID, bool isGlobalPoolHoldRelease = true ) {
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();
        string strReturnMsg = "";

        string sponsorID = "";
        string UserId = "";

        if (UserInfo.IsLogin) {
			sponsorID = new UserInfo().SponsorID;
            UserId = new UserInfo().UserId;

        }

		var sessionId = HttpContext.Current.Request.UserHostAddress;

        if (strdbgp_kind == "1") // 기존 방식 -어린이 리스트 DB조회
        {
            string sResult = _wwwService.deleteDATChildEnsure("",
                                            sChildMasterID,
                                            sponsorID //후원자ID
                                        );
        }
        else if (strdbgp_kind == "2") // 신규 방식 -어린이 리스트 Global Pool조회
        {
            Object[] objSql1 = new object[] { "sp_S_tTmpChildEnsure" };
            Object[] objParam1 = new object[] { "Kind", "SessionID", "ChildMasterID", "UserID" };
            Object[] objValue1 = new object[] { "02", "", sChildMasterID, UserId };
            DataTable dt1 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql1, "SP", objParam1, objValue1).Tables[0];

            if (dt1.Rows.Count == 0)
            {
                strReturnMsg = "후원취소를 하는 도중 오류가 발생하였습니다1.";
                return strReturnMsg;
            }

            //            dt1.Rows[0]["HoldId"].ToString()


            //[이종진] 경우에 따라서, GP에 홀딩푸는 작업은 하지 않음
            if (isGlobalPoolHoldRelease)
            {
                BeneficiaryHoldReleaseRequestList_POST kit = new BeneficiaryHoldReleaseRequestList_POST();

                BeneficiaryHoldReleaseRequestList item = new BeneficiaryHoldReleaseRequestList();

                kit.BeneficiaryHoldReleaseRequestList.Add(item);

                item.Beneficiary_GlobalID = sChildMasterID.Substring(2);

                item.HoldID = dt1.Rows[0]["HoldId"].ToString();

                item.GlobalPartner_ID = "KR";

                string json = JsonConvert.SerializeObject(kit);
                CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();
                string result = _OffRampService.BeneficiaryHoldReleaseRequestList_POST(json);
                TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(result);

                if (!msg.IsSuccessStatusCode)
                {
                    strReturnMsg = "후원취소를 하는 도중 오류가 발생하였습니다2.";
                }
            }

            Object[] objSql2 = new object[] { "sp_D_tTmpChildEnsure" };
            Object[] objParam2 = new object[] { "Kind", "SessionID", "ChildMasterID", "UserID" };
            Object[] objValue2 = new object[] { "01", "", sChildMasterID, UserId };
            DataTable dt2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2).Tables[0];

            if (dt2.Rows.Count == 0)
            {
                strReturnMsg = "후원취소를 하는 도중 오류가 발생하였습니다3.";
                return strReturnMsg;
            }

            if (dt2.Rows[0]["RTNCODE"].ToString() != "1")
            {
                strReturnMsg = "후원취소를 하는 도중 오류가 발생하였습니다4.";
                return strReturnMsg;
            }

            strReturnMsg = "후원취소를 완료하였습니다.";
        }

        return strReturnMsg;
    }

    // 결연된 어린이인지 여부 확인
    public JsonWriter CheckApply(string sChildMasterID ) {
		DataSet data = new DataSet();
		JsonWriter result = new JsonWriter();
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();

        HttpCookie cookie = HttpContext.Current.Request.Cookies["sessionId"];

        string UserId = "";
        if (UserInfo.IsLogin)
        {
            UserId = new UserInfo().UserId;
        }

        try
        {
            if (strdbgp_kind == "1") // 기존 방식 -어린이 리스트 DB조회
            {
                data = _wwwService.CheckApplyChild("", sChildMasterID, 0, 9999, "");
                result.success = true;

                try
                {
                    ErrorLog.Write(HttpContext.Current, 0, data.ToJson());
                }
                catch { }
                if (data.Tables.Count > 0)
                {
                    if (data.Tables[0].Rows.Count > 0)
                    {
                        result.action = "exist";
                        result.message = "죄송합니다.\\r\\현재 이 어린이는 후원자님을 만났습니다.\\r\\n후원을 기다리고 있는 또 다른 어린이의 손을 잡아 주세요.";
                        result.success = false;
                    }
                }
            }
            else if (strdbgp_kind == "2") // 신규 방식 -어린이 리스트 Global Pool조회
            {
                result.success = true;

                var sessionId = cookie.Value;

                Object[] objSql1 = new object[] { "sp_S_tTmpChildEnsure" };
                Object[] objParam1 = new object[] { "Kind", "SessionID", "ChildMasterID", "UserID" };
                Object[] objValue1 = new object[] { "03", sessionId, sChildMasterID, UserId };

                DataTable dt1 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql1, "SP", objParam1, objValue1).Tables[0];

                if (dt1.Rows.Count == 0)
                {
                    result.action = "exist";
                    result.message = "선택하신 어린이를 등록 중 오류가 발생했습니다1.";
                    result.success = false;
                    return result;
                }

                if (dt1.Rows.Count > 1)
                {
                    result.action = "exist";
                    result.message = "죄송합니다.\\r\\현재 이 어린이는 후원자님을 만났습니다.\\r\\n후원을 기다리고 있는 또 다른 어린이의 손을 잡아 주세요.";
                    result.success = false;
                    return result;
                }
            }
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "어린이 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}
	
	// 후원내역
	public JsonWriter CheckApplyByChildKey( string childKey ) {
		JsonWriter result = new JsonWriter() { success = false };
		
		try {

			Object[] objParam = new object[] { "childKey" };
			Object[] objValue = new object[] { childKey };
			Object[] objSql = new object[] { "sp_web_commitment_cdsp_by_childkey_f" };
			DataSet data = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

			result.success = true;

			if(data.Tables.Count > 0) {
				if(data.Tables[0].Rows.Count > 0) {
					result.action = "exist";
					result.message = "죄송합니다.\\r\\현재 이 어린이는 후원자님을 만났습니다.\\r\\n후원을 기다리고 있는 또 다른 어린이의 손을 잡아 주세요.";
					result.success = false;
				}
			}
			
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "어린이 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

	// 결연된 어린이 목록
	public class MyChildByCountryEntity {
		public string country;
		public List<Child> children = new List<Child>();

		public class Child {
			public string childMasterId;
			public string childKey;
			public string nameKr;
		}
	}

	public class MyChildItem {

		public int Total { get; set; }
		public int Age { get; set; }
		public DateTime BirthDate { get; set; }
		public string CountryName { get; set; }
		public string CountryCode { get; set; }
		public string NameKr { get; set; }
		public string NameEn { get; set; }
		public string Name { get; set; }
        public string PersonalNameEng { get; set; }
        public string Gender { get; set; }
		public string ChildMasterId { get; set; }
		public string ChildKey { get; set; }
		public bool IsOrphan { get; set; }
		public bool IsHandicapped { get; set; }
		public string Pic { get; set; }
		public int CommitmentDays { get; set; }        // 후원경과일
		public DateTime CommitmentDate { get; set; }        // 후원시작일

		public string BioKr { get; set; }   
		public string ProjectInfo { get; set; }		// 상세조회에서만 있음
        public string Cognitive { get; set; }		// 상세조회에서만 있음 -- TCPT 용 추가 문희원 2017-04-06
        public string Physical { get; set; }		// 상세조회에서만 있음 -- TCPT 용 추가 문희원 2017-04-06
        public string SocioEmotional { get; set; }	// 상세조회에서만 있음 -- TCPT 용 추가 문희원 2017-04-06
        public string Spiritual { get; set; }		// 상세조회에서만 있음 -- TCPT 용 추가 문희원 2017-04-06
		public ChildCaseStudy CaseStudy { get; set; } // 상세조회에서만 있음

		public bool CanGift { get; set; }	// 선물금보내기 가능여부
		public bool CanLetter { get; set; } // 편지쓰기 가능여부

		public bool PAID { get; set; } // 후원금 결제내역이 있는지

        public string isSupported { get; set; } //후원중 여부 

        public string commitmentId { get; set; } //후원ID  20170404 항목 추가 

        public string sponsorTypeEng { get; set; } //후원 구분 VT용 추가 170925 정다솜
    }

	// 나의 어린이 목록 
	// checkLetter = 1 , 0	(1인경우 편지쓰기가 가능한 어린이만 노출 )
	public JsonWriter MyChildren( string childKey, int page, int rowsPerPage ) {
		return this.MyChildren(childKey, page, rowsPerPage, false);
	}

	public JsonWriter MyChildren( string childKey, int page, int rowsPerPage , bool checkLetter ) {
		return this.MyChildren(childKey, page, rowsPerPage, "old" , checkLetter);
	}
	public JsonWriter MyChildren( string childKey , int page , int rowsPerPage , string orderby , bool checkLetter ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			result.message = "후원중인 어린이가 없습니다.";
			return result;
		}
		
		try {

			var data = new List<MyChildItem>();

			Object[] objParam = new object[] { "page" , "rowsPerPage" , "sponsorId" , "childKey" , "orderby" , "checkLetter" };
			Object[] objValue = new object[] { page, rowsPerPage , sess.SponsorID , childKey , orderby , checkLetter ? "1" : "0" };
			Object[] objSql = new object[] { "sp_web_mychild_list_f" };
			DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			foreach(DataRow dr in dt.Rows) {

				var sponsorTypeEng = dr["sponsorTypeEng"].ToString();
				var CanGift = true;
				var CanLetter = true;
				if (sponsorTypeEng == "CORRES" || sponsorTypeEng == "LDPCOR") {		// 편지후원자
					CanGift = false;
				}else if(sponsorTypeEng == "CHIMON" || sponsorTypeEng == "LDPMON") {        // 머니후원자
					CanLetter = false;
				}

                data.Add(new MyChildItem() {
                    Total = Convert.ToInt32(dr["total"]),
                    IsOrphan = dr["Orphan"].ToString() == "Y",
                    IsHandicapped = dr["handicapped"].ToString() == "Y",
                    CommitmentDate = Convert.ToDateTime(dr["startDate"]),
                    CommitmentDays = Convert.ToInt32(Convert.ToDateTime(dr["startdate"].ToString()).DaysAgo()),
                    Age = Convert.ToInt32(dr["childAge"]),
                    BirthDate = Convert.ToDateTime(dr["BirthDate"]),
                    CountryCode = dr["countryCode"].ToString(),
                    CountryName = dr["countryName"].ToString(),
                    ChildMasterId = dr["childMasterId"].ToString(),
                    ChildKey = dr["ChildKey"].ToString(),
                    NameKr = dr["NameKr"].ToString(),
                    NameEn = dr["NameEng"].ToString(),
                    Name = dr["Name"].ToString(),
                    PersonalNameEng = dr["PersonalNameEng"].ToString(),
                    Gender = dr["Gender"].ToString(), // 남자,여자
                    BioKr = dr["BioKr"].ToString().Replace("니다. ", "니다. <br>"),
                    //Pic = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),

                    //수정 문희원 2017-04-20
                    //Pic = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()),
                    //Pic = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
                    // #12770 : Web.Config 설정에 따라 Connection, DB 결정
                    Pic = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()),

                    CanGift = CanGift,
                    CanLetter = CanLetter,
                    PAID = dr["paid"].ToString() == "Y",
                    commitmentId = dr["CommitmentID"].ToString()        /*20170404 commitmentid 항목 추가*/
                    ,
                    sponsorTypeEng = dr["sponsorTypeEng"].ToString()
                });
			}

			result.data = data;
			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "어린이 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}


    public JsonWriter MyAllChildren(string childKey, int page, int rowsPerPage, string orderby, bool checkLetter)
    {
        JsonWriter result = new JsonWriter() { success = false };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var sess = new UserInfo();

        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            result.action = "not_sponsor";
            result.message = "후원중인 어린이가 없습니다.";
            return result;
        }

        try
        {
            var data = new List<MyChildItem>();

            Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId", "childKey", "orderby", "checkLetter" };
            Object[] objValue = new object[] { page, rowsPerPage, sess.SponsorID, childKey, orderby, checkLetter ? "1" : "0" };
            Object[] objSql = new object[] { "sp_web_myAllchild_list_f" };
            DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

            foreach (DataRow dr in dt.Rows)
            {
                var sponsorTypeEng = dr["sponsorTypeEng"].ToString();
                var CanGift = true;
                var CanLetter = true;
                if (sponsorTypeEng == "CORRES" || sponsorTypeEng == "LDPCOR")
                {       // 편지후원자
                    CanGift = false;
                }
                else if (sponsorTypeEng == "CHIMON" || sponsorTypeEng == "LDPMON")
                {        // 머니후원자
                    CanLetter = false;
                }

                data.Add(new MyChildItem()
                {
                    Total = Convert.ToInt32(dr["total"]),
                    IsOrphan = dr["Orphan"].ToString() == "Y",
                    IsHandicapped = dr["handicapped"].ToString() == "Y",
                    CommitmentDate = Convert.ToDateTime(dr["startDate"]),
                    CommitmentDays = Convert.ToInt32(Convert.ToDateTime(dr["startdate"].ToString()).DaysAgo()),
                    Age = Convert.ToInt32(dr["childAge"]),
                    BirthDate = Convert.ToDateTime(dr["BirthDate"]),
                    CountryCode = dr["countryCode"].ToString(),
                    CountryName = dr["countryName"].ToString(),
                    ChildMasterId = dr["childMasterId"].ToString(),
                    ChildKey = dr["ChildKey"].ToString(),
                    NameKr = dr["NameKr"].ToString(),
                    NameEn = dr["NameEng"].ToString(),
                    Name = dr["Name"].ToString(),
                    Gender = dr["Gender"].ToString(), // 남자,여자
                    BioKr = dr["BioKr"].ToString().Replace("니다. ", "니다. <br>"),
                    //Pic = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
                    //수정 문희원 2017-04-20
                    //Pic = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()),
                    //Pic = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
                    //#12770
                    Pic = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()),
                    CanGift = CanGift,
                    CanLetter = CanLetter,
                    PAID = dr["paid"].ToString() == "Y",
                    isSupported = dr["SUPPORTED_YN"].ToString(),
                    PersonalNameEng = dr["PersonalNameEng"].ToString()
                });
            }

            result.data = data;
            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "어린이 정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;
        }

        return result;
    }   

    public JsonWriter MyChild( string childKey ) {
		JsonWriter result = new JsonWriter() { success = false };

		var actionResult = this.MyChildren(childKey, 1, 1);
		if(!actionResult.success) {
			return actionResult;
		}

		var children = (List<MyChildItem>)actionResult.data;
		if (children.Count < 1) {
			result.message = "어린이 정보를 찾지 못했습니다.";
			return result;
		}

		try {

			var child = children[0];
			var childMasterId = child.ChildMasterId;

            // 센터 소개 수정 문희원 2017-04-06
            var objSql = new object[1] { " SELECT  " +
                                            " JsonData " +
                                         " FROM  " +
                                            " dbo.tChildMaster CM WITH (NOLOCK)  " +
                                            " INNER JOIN dbo.TCPT_ICP ICP WITH (NOLOCK) ON SUBSTRING(CM.ChildKey, 1, 6) = ICP.ICP_ID " +
                                         " WHERE CM.ChildMasterID = '" + childMasterId + "' AND ICP.TranslationDate is not null " };

            DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
            if (ds2.Tables[0].Rows.Count > 0)
            {
                int age = ((MyChildItem)child).Age;
                string json = ds2.Tables[0].Rows[0]["JsonData"].ToString();
                string[] projInfo = GetProjectInfo(json, age);
                child.ProjectInfo = projInfo[0];
                child.Cognitive = projInfo[1];
                child.Physical = projInfo[2];
                child.SocioEmotional = projInfo[3];
                child.Spiritual = projInfo[4];
            }

            result.data = child;

			// 가족 , 취미등
			actionResult = this.GetCaseStudy(childMasterId , childKey);
			if(actionResult.success) {
				child.CaseStudy = (ChildCaseStudy)actionResult.data;
			}

			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "어린이 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

	public JsonWriter MyChildCountries() {
		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}

		try {

			Object[] objParam = new object[] {"sponsorId"};
			Object[] objValue = new object[] { sess.SponsorID};
			Object[] objSql = new object[] { "sp_web_mychild_country_list_f" };
			DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			List<MyChildByCountryEntity> children = new List<MyChildByCountryEntity>();

			foreach(DataRow dr in dt.Rows) {
				string country = dr["CountryName"].ToString();
			
				if(!children.Any(p => p.country == country)) {

					children.Add(new MyChildByCountryEntity() {
						country = country
					});

				}

				var item = children.First(p => p.country == country).children;
				item.Add(new MyChildByCountryEntity.Child() {
					childMasterId = dr["childMasterId"].ToString() , 
					childKey = dr["childKey"].ToString(), nameKr = dr["NameKr"].ToString()
				});
				
			}

			result.data = children;
			result.success = true;


		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "어린이 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

	// 선물금가능한 어린이 목록
	public JsonWriter GetGitfableList() {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}

		try {
			
			Object[] objParam = new object[] { "sponsorId" };
			Object[] objValue = new object[] { sess.SponsorID };
			Object[] objSql = new object[] { "sp_web_present_child_list_f" };
			DataTable data = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
			data.Columns.Add("age");
			data.Columns.Add("pic");
			data.Columns.Add("days");
			data.Columns.Add("total");

			List<MyChildByCountryEntity> children = new List<MyChildByCountryEntity>();

		//	List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
			foreach(DataRow dr in data.Rows) {
				//var row = new Dictionary<string, object>();

				string country = dr["CountryCode"].ToString();
				string days = "";
				
				days = Convert.ToDateTime(dr["startdate"].ToString()).DaysAgo().ToString("N0");
				dr["age"] = Convert.ToDateTime(dr["BirthDate"].ToString()).GetAge().ToString();
                //수정 문희원 2017-04-20
                //dr["pic"] = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString());//ChildAction.GetPicByChildKey(dr["ChildKey"].ToString());
                dr["pic"] = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString());
                dr["days"] = days;
				dr["total"] = data.Rows.Count;
				
				if(!children.Any(p => p.country == country)) {

					children.Add(new MyChildByCountryEntity() {
						country = country
					});

				}

				var item = children.First(p => p.country == country).children;
				item.Add(new MyChildByCountryEntity.Child() {
					childKey = dr["childKey"].ToString(), nameKr = dr["NameKr"].ToString()
				});

			}


			result.data = new Dictionary<string, object>() {
				{ "children" , children }, {"list" , data } 
			};
			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "어린이 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}
	
	public static string GetPicByChildKey(string childKey ) {
        
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();

        // 어린이 이미지 DB에서 URL 조회 수정 문희원 2017-04-17
        CommonLib.WWW6Service.SoaHelperSoap _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        if (strdbgp_kind == "1") // 기존 방식 -어린이 리스트 DB조회
        {
            Object[] objParam = new object[] { "ChildKey" };
            Object[] objValue = new object[] { childKey };
            Object[] objSql = new object[] { "sp_s_FullBodyImageURl_For_Web" };
            DataTable data = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

            if (data != null && data.Rows.Count > 0)
            {
                return data.Rows[0]["FullBodyImageURL"].ToString().Replace("w_150", "w_300");
            }
        }
        else if (strdbgp_kind == "2") // 신규 방식 -어린이 리스트 Global Pool조회
        {
            Object[] objParam = new object[] { "Kind", "ChildKey" };
            Object[] objValue = new object[] { "01", childKey };
            Object[] objSql = new object[] { "sp_S_tTmpChildMasterKey" };
            DataTable data = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

            if (data != null && data.Rows.Count > 0)
            {
                return data.Rows[0]["FullBodyImageURL"].ToString();
            }
        }
        
        // 조회 된 데이터가 없을 경우 웹에서 조회
        string oldKey = childKey.Substring(0, 2) + childKey.Substring(3, 3) + childKey.Substring(7, 4);
        string result = GetChildImageFromFileServer(oldKey);
        if (string.IsNullOrEmpty(result))
        {
            result = GetChildImageFromFileServer(childKey);
        }

        return result;
    }

    public static string GetChildImageFromFileServer(string childKey)
    {
        string url = string.Empty;
        try
        {
            url = "http://ws.compassion.or.kr/Files/Child/" + childKey.ToString().Substring(0, 2) + "/" + childKey + ".jpg";
            //Creating the HttpWebRequest
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            //Setting the Request method HEAD, you can also use GET too.
            request.Method = "HEAD";
            //Getting the Web Response.
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            //Returns TRUE if the Status code == 200
            if (response.StatusCode != HttpStatusCode.OK)
            {
                url = ChildAction.GetPicByChildKey(childKey.Substring(0, 2) + childKey.Substring(3, 3) + childKey.Substring(7, 4));
            }
            response.Close();
        }
        catch
        {
            url = "";
        }
        return url;
    }

    public class ChildItem {

        public int Total { get; set; }
        public int Age { get; set; }
        public DateTime BirthDate { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string NameKr { get; set; }
        public string NameEn { get; set; }
        public string Name { get; set; }
        //        public string NameDB { get; set; }

        public string HangulName { get; set; }
        public string HangulPreferredName { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }

        public string PreferredName { get; set; }
        public string ChildMasterId { get; set; }
        public string ChildKey { get; set; }
        public string ChildGlobalId { get; set; }
		public bool IsOrphan { get; set; }
		public bool IsHandicapped { get; set; }
		public string Pic { get; set; }
		public int WaitingDays { get; set; }        // 결연대기일
        public string CommitmentID { get; set; }

		public DateTime? CompletionDate { get; set; }        // 수료예정일 , CompletionMinMonth & CompletionMaxMonth 로 검색하는 경우만 세팅
		public int RemainAmount {       // 수료까지 필요한 금액 , CompletionMinMonth & CompletionMaxMonth 로 검색하는 경우만 세팅
			get {
				if (!CompletionDate.HasValue)
					return -1;

				var val = CompletionDate.Value.MonthDifference(DateTime.Now);
				if(val % 3 > 0) {
					val += (3 - val % 3);
				}
				return val * 45000;

			}
		}
	}

    //후원가능 어린이 조회
    //country : 여러국가를 조회하는 경우 ES,ET,GH,HA 와 같이 , 를 구분자로 사용
    //public JsonWriter GetChildren(string strDbGpKind, string country /*BO , EC 등*/ , string gender /*M , F*/ , int? birthMonth, int? birthDay, int? minAge, int? maxAge, bool? isOrphan /*고아*/ , bool? isHandicapped /*장애*/ ,
    //    int page, int rowsPerPage, string orderby, int CompletionMinMonth, int CompletionMaxMonth)
    public JsonWriter GetChildren(string country /*BO , EC 등*/ , string gender /*M , F*/ , int? birthMonth, int? birthDay, int? minAge, int? maxAge, bool? isOrphan /*고아*/ , bool? isHandicapped /*장애*/ ,
        int page, int rowsPerPage, string orderby, int CompletionMinMonth, int CompletionMaxMonth)
    {
        JsonWriter result = new JsonWriter() { success = false };
        //2017-12-01 이종진 - 쿠키값이 Null일 경우, 기존방식으로 처리
        //string strdbgp_kind = "1";
        //if(HttpContext.Current.Request.Cookies["sdbgp_kind"] != null)
        //{
        //    strdbgp_kind = HttpContext.Current.Request.Cookies["sdbgp_kind"].Value;
        //}

        result = GetChildrenDb(country /*BO , EC 등*/ , gender /*M , F*/ , birthMonth, birthDay, minAge, maxAge, isOrphan /*고아*/
            , isHandicapped /*장애*/ , page, rowsPerPage, orderby, CompletionMinMonth, CompletionMaxMonth);



        //if (strdbgp_kind == "1") // 기존 방식 -어린이 리스트 DB조회
        //{
        //    result = GetChildrenDb(country /*BO , EC 등*/ , gender /*M , F*/ , birthMonth, birthDay, minAge, maxAge, isOrphan /*고아*/
        //        , isHandicapped /*장애*/ , page, rowsPerPage, orderby, CompletionMinMonth, CompletionMaxMonth);
        //}
        //else if (strdbgp_kind == "2") // 신규 방식 -어린이 리스트 Global Pool조회
        //{
        //    //result = GetChildrenDb(country /*BO , EC 등*/ , gender /*M , F*/ , birthMonth, birthDay, minAge, maxAge, isOrphan /*고아*/
        //    //    , isHandicapped /*장애*/ , page, rowsPerPage, orderby, CompletionMinMonth, CompletionMaxMonth);

        //    result = GetChildrenGp(country /*BO , EC 등*/ , gender /*M , F*/ , birthMonth, birthDay, minAge, maxAge, isOrphan /*고아*/
        //        , isHandicapped /*장애*/ , page, rowsPerPage, orderby, CompletionMinMonth, CompletionMaxMonth);
        //}

        return result;
    }

    //후원가능 어린이 조회 - DB
    //country : 여러국가를 조회하는 경우 ES,ET,GH,HA 와 같이 , 를 구분자로 사용
    public JsonWriter GetChildrenDb( string country /*BO , EC 등*/ , string gender /*M , F*/ , int? birthMonth, int? birthDay, int? minAge, int? maxAge, bool? isOrphan /*고아*/ , bool? isHandicapped /*장애*/ ,
		int page, int rowsPerPage , string orderby , int CompletionMinMonth , int CompletionMaxMonth ) {

		JsonWriter result = new JsonWriter() { success = false };

		string birthMM = "";
		if (birthMonth.HasValue) {
			birthMM = birthMonth.Value.ToString().PadLeft(2, '0');
		}

		string birthDD = "";
		if(birthDay.HasValue) {
			birthDD = birthDay.Value.ToString().PadLeft(2, '0');
		}

		var data = new List<ChildItem>();
		if (CompletionMinMonth < 0 && CompletionMaxMonth < 0) {
			Object[] objParam = new object[] { "page", "rowsPerPage", "countryCode", "gender", "birthMM", "birthDD", "minAge", "maxAge", "isOrphan", "isHandicap", "orderby" };
			Object[] objValue = new object[] { page, rowsPerPage, country.EmptyIfNull(), gender.EmptyIfNull(), birthMM ,birthDD, minAge.HasValue ? minAge.Value : -1 , maxAge.HasValue ? maxAge.Value : -1,
			isOrphan.HasValue ? (isOrphan.Value ? "Y" : "N") : "", isHandicapped.HasValue ? (isHandicapped.Value?"Y":"N") : "" , string.IsNullOrEmpty(orderby) ? "new" : orderby};
			Object[] objSql = new object[] { "sp_web_tChildMaster_list_f" };
			DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
			
			foreach(DataRow dr in dt.Rows) {
                data.Add(new ChildItem() {
                    IsOrphan = dr["Orphan"].ToString() == "Y",
                    IsHandicapped = dr["handicapped"].ToString() == "Y",
                    WaitingDays = Convert.ToInt32(dr["WaitingDays"]),
                    Age = Convert.ToInt32(dr["childAge"]),
                    BirthDate = Convert.ToDateTime(dr["BirthDate"]),
                    CountryCode = dr["countryCode"].ToString(),
                    CountryName = dr["countryName"].ToString(),
                    ChildMasterId = dr["childMasterId"].ToString(),
                    ChildKey = dr["ChildKey"].ToString(),
                    NameKr = dr["NameKr"].ToString(),
                    NameEn = dr["NameEng"].ToString(),
                    Name = dr["Name"].ToString(),
                    Gender = dr["childGender"].ToString(), // 남자,여자

                    HangulName = "",
                    HangulPreferredName = "",
                    FullName = "",
                    PreferredName = "",

                    //Pic = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()),//ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
                    //Pic = dr["FullBodyImageURL"].ToString().Replace("w_150", "w_300"), // 어린이 사진 URL
                    // Global Pool 에서 어린이 이미지 조회
                    Pic = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
                });
			}

			// 수료 임박한 미결연 어린이(나눔펀딩)
		} else {

			Object[] objParam = new object[] { "page", "rowsPerPage", "CompletionMinMonth", "CompletionMaxMonth" };
			Object[] objValue = new object[] { page, rowsPerPage, CompletionMinMonth  , CompletionMaxMonth };
			Object[] objSql = new object[] { "sp_web_tChildMaster_user_funding_list_f" };
			DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			foreach(DataRow dr in dt.Rows) {
                data.Add(new ChildItem() {

                    IsOrphan = dr["Orphan"].ToString() == "Y",
                    IsHandicapped = dr["handicapped"].ToString() == "Y",
                    WaitingDays = Convert.ToInt32(dr["WaitingDays"]),
                    Total = Convert.ToInt32(dr["total"]),
                    Age = Convert.ToInt32(dr["childAge"]),
                    BirthDate = Convert.ToDateTime(dr["BirthDate"]),
                    CountryCode = dr["countryCode"].ToString(),
                    CountryName = dr["countryName"].ToString(),
                    ChildMasterId = dr["childMasterId"].ToString(),
                    ChildKey = dr["ChildKey"].ToString(),
                    NameKr = dr["NameKr"].ToString(),
                    NameEn = dr["NameEng"].ToString(),
                    Name = dr["Name"].ToString(),
                    Gender = dr["childGender"].ToString(), // 남자,여자

                    HangulName = "",
                    HangulPreferredName = "",
                    FullName = "",
                    PreferredName = "",

                    // 수정 문희원 2017-04-20
                    //Pic = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()),//ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
                    //Pic = dr["FullBodyImageURL"].ToString().Replace("w_150", "w_300"), // 어린이 사진 URL
                    Pic = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
                    CompletionDate = DateTime.ParseExact(dr["CompletionDate"].ToString(), "yyyy-MM-dd", null) 
				});
			}

		}
        //result.message = "GetChildrenDb";
        result.data = data;
		result.success = true;
		return result;
	}

    //후원가능 어린이 조회 - Global Pool
    //country : 여러국가를 조회하는 경우 ES,ET,GH,HA 와 같이 , 를 구분자로 사용
    public JsonWriter GetChildrenGp(string country /*BO , EC 등*/ , string gender /*M , F*/ , int? birthMonth, int? birthDay, int? minAge, int? maxAge, bool? isOrphan /*고아*/ , bool? isHandicapped /*장애*/ ,
        int page, int rowsPerPage, string orderby, int CompletionMinMonth, int CompletionMaxMonth, bool isUserFunding = false)
    {
        JsonWriter result = new JsonWriter() { success = false };

        string sResult = "";
        string strCountryCode = "";
        string strEName = "";

        string strChildMasterID = "";
        string strChildKey = "";
        string strFullBodyImageURL = "";

        //string IsOrphan = "";
        //string IsHandicapped = "";
        //string WaitingDays = "";
        //string Age = "";
        //string BirthDate = "";

        //string HangulName = "";
        //string HangulPreferredName = "";
        //string FullName = "";

        //string Gender = "";

        BeneficiaryAvailabilityQueryRequest_POST REQ = new BeneficiaryAvailabilityQueryRequest_POST();
        DataTable dt = new DataTable();

        var data = new List<ChildItem>();
        if ((CompletionMinMonth < 0 && CompletionMaxMonth < 0) || isUserFunding)
        {
            // 국가
            if (!string.IsNullOrEmpty(country))
            {
                List<string> value = new List<string>();
                //[이종진] 모바일의 CAD에서는 countryCode가 ','로 구분지어서와서 추가
                string[] sepe = { "," };
                foreach (string coun in country.Split(sepe, StringSplitOptions.None))
                {
                    value.Add(coun);
                }
                
                Filter f = new Filter();
                f.Field = "FieldOffice";
                f.Operator = "anyof";
                f.Value = value;
                REQ.BeneficiarySearchRequestList.Filter.Add(f);
            }

            // 성별
            if (!string.IsNullOrEmpty(gender))
            {
                List<string> value = new List<string>();
                value.Add(gender);
                Filter f = new Filter();
                f.Field = "Gender";
                f.Operator = "anyof";
                f.Value = value;
                REQ.BeneficiarySearchRequestList.Filter.Add(f);
            }

            // 생일 월
            string birthMM = "";
            if (birthMonth.HasValue)
            {
                birthMM = birthMonth.Value.ToString().PadLeft(2, '0');

                List<string> value = new List<string>();
                value.Add(birthMM);
                Filter f = new Filter();
                f.Field = "BirthMonth";
                f.Operator = "equalto";
                f.Value = value;
                REQ.BeneficiarySearchRequestList.Filter.Add(f);
            }

            string birthDD = "";
            if (birthDay.HasValue)
            {
                birthDD = birthDay.Value.ToString().PadLeft(2, '0');

                List<string> value = new List<string>();
                value.Add(birthDD);
                Filter f = new Filter();
                f.Field = "BirthDay";
                f.Operator = "equalto";
                f.Value = value;
                REQ.BeneficiarySearchRequestList.Filter.Add(f);
            }

            // 나이
            if (!string.IsNullOrEmpty(minAge.ToString()) && !string.IsNullOrEmpty(maxAge.ToString()))
            {
                List<string> value = new List<string>();
                value.Add(minAge.ToString());
                value.Add(maxAge.ToString());
                Filter f = new Filter();
                f.Field = "Age";
                f.Operator = "between";
                f.Value = value;
                REQ.BeneficiarySearchRequestList.Filter.Add(f);
            }

            // 고아
            if (!string.IsNullOrEmpty(isOrphan.ToString()))
            {
                List<string> value = new List<string>();
                value.Add(isOrphan.ToString());
                Filter f = new Filter();
                f.Field = "IsOrphan";
                f.Operator = "is";
                f.Value = value;
                REQ.BeneficiarySearchRequestList.Filter.Add(f);
            }

            // 장애
            if (!string.IsNullOrEmpty(isHandicapped.ToString()))
            {
                List<string> value = new List<string>();
                value.Add(isHandicapped.ToString());
                Filter f = new Filter();
                f.Field = "PhysicalDisabilities";
                f.Operator = "is";
                f.Value = value;
                REQ.BeneficiarySearchRequestList.Filter.Add(f);
            }

            //나눔펀딩
            if (isUserFunding)
            {
                List<string> value = new List<string>();
                value.Add(DateTime.Now.AddMonths(CompletionMinMonth).ToString("yyyy-MM-dd"));
                value.Add(DateTime.Now.AddMonths(CompletionMaxMonth).ToString("yyyy-MM-dd"));
                Filter f = new Filter();
                f.Field = "PlannedCompletionDate";
                f.Operator = "within";
                f.Value = value;
                REQ.BeneficiarySearchRequestList.Filter.Add(f);
            }

            {
                // 상태
                List<string> value = new List<string>();
                value.Add("Available");

                Filter f = new Filter();
                f.Field = "BeneficiaryAMState";
                f.Operator = "anyof";
                f.Value = value;

                REQ.BeneficiarySearchRequestList.Filter.Add(f);
            }



            //REQ.BeneficiarySearchRequestList.NumberOfBeneficiaries = page * rowsPerPage;
            //REQ.BeneficiarySearchRequestList.Start = ((page - 1) * rowsPerPage) + 1;
            REQ.BeneficiarySearchRequestList.NumberOfBeneficiaries = rowsPerPage; //개수
            REQ.BeneficiarySearchRequestList.Start = ((page - 1) * rowsPerPage) + 1; //조회시작점

            string json = JsonConvert.SerializeObject(REQ);
            CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();
            sResult = _OffRampService.BeneficiaryAvailabilityQueryRequest_POST(json);

            TCPTModel.TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(sResult);

            if (msg.IsSuccessStatusCode)
            {
                TCPTModel.Response.Beneficiary.BeneficiarySearchResponseList_Kit kit = new TCPTModel.Response.Beneficiary.BeneficiarySearchResponseList_Kit();
                kit = JsonConvert.DeserializeObject<BeneficiarySearchResponseList_Kit>(msg.RequestMessage.ToString());

                //lblGridViewCount.Text = kit.BeneficiarySearchResponseList.Count.ToString();

                if (kit.BeneficiarySearchResponseList == null || kit.BeneficiarySearchResponseList.Count <= 0)
                {
                    //MessageBox.Show("조회된 어린이가 없습니다.");
                }
                else
                {
                    // 국가정보
                    var countries = StaticData.Country.GetList(HttpContext.Current);
                    int iCount = 0;

                    DateTime dt1 = DateTime.Now;

                    foreach (BeneficiarySearchResponseList item in kit.BeneficiarySearchResponseList)
                    {
                        if (strCountryCode == "")
                        {
                            strCountryCode = item.Beneficiary_LocalID.Substring(0, 2);
                            strEName = item.PreferredName;

                            strChildMasterID = "09" + item.Beneficiary_GlobalID;
                            strChildKey = item.Beneficiary_LocalID;
                            strFullBodyImageURL = item.FullBodyImageURL;

                            //IsOrphan = item.IsOrphan == true ? "1" : "0";
                            //IsHandicapped = item.IsSpecialNeeds == true ? "1" : "0";
                            //WaitingDays = ((TimeSpan)(DateTime.Now - Convert.ToDateTime(item.WaitingSinceDate))).Days.ToString();
                            //Age = item.Age.ToString();
                            //BirthDate = item.BirthDate;
                            //HangulName = item.HangulName;
                            //HangulPreferredName = item.HangulPreferredName;
                            //FullName = item.FullName;
                            //Gender = item.Gender;

                        }
                        else
                        {
                            strCountryCode += "|" + item.Beneficiary_LocalID.Substring(0, 2);
                            strEName += "|" + item.PreferredName;

                            strChildMasterID += "|" + "09" + item.Beneficiary_GlobalID;
                            strChildKey += "|" + item.Beneficiary_LocalID;
                            strFullBodyImageURL += "|" + item.FullBodyImageURL;

                            //IsOrphan += "|" + (item.IsOrphan == true ? "1" : "0");
                            //IsHandicapped += "|" + (item.IsSpecialNeeds == true ? "1" : "0");
                            //WaitingDays += "|" + ((TimeSpan)(DateTime.Now - Convert.ToDateTime(item.WaitingSinceDate))).Days.ToString();
                            //Age += "|" + item.Age.ToString();
                            //BirthDate += "|" + item.BirthDate;
                            //HangulName += "|" + item.HangulName;
                            //HangulPreferredName += "|" + item.HangulPreferredName;
                            //FullName += "|" + item.FullName;
                            //Gender += "|" + item.Gender;
                        }
                    }

                    Object[] objSql = new object[] { "sp_S_tChildShotName" };
                    Object[] objParam = new object[] { "CountryCode", "EName", "ChildMasterID", "ChildKey", "FullBodyImageURL" };
                    Object[] objValue = new object[] { strCountryCode, strEName, strChildMasterID, strChildKey, strFullBodyImageURL };

                    //Object[] objParam = new object[] { "CountryCode", "EName", "ChildMasterID", "ChildKey", "FullBodyImageURL"
                    //    , "IsOrphan", "IsHandicapped", "WaitingDays", "Age", "BirthDate", "HangulName", "HangulPreferredName", "FullName", "Gender"};
                    //Object[] objValue = new object[] { strCountryCode, strEName, strChildMasterID, strChildKey, strFullBodyImageURL
                    //    , IsOrphan, IsHandicapped, WaitingDays, Age, BirthDate, HangulName, HangulPreferredName, FullName, Gender};

                    dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
                    DateTime dt2 = DateTime.Now;

                    //TimeSpan ts = dt2 - dt1;
                    //string sss = ts.ToString();

                    //result.message = sss;

                    string strNameKr = "";
                    string strName = "";

                    string strHangulPreferredName = "";
                    string strPreferredName = "";

                    if (dt.Rows.Count > 0)
                    {
                        List<string> tmp = new List<string>();
                        foreach (BeneficiarySearchResponseList item in kit.BeneficiarySearchResponseList)
                        {
                            strHangulPreferredName = string.IsNullOrEmpty(item.HangulPreferredName) ? "" : item.HangulPreferredName.Replace("?", "");
                            strPreferredName = string.IsNullOrEmpty(item.PreferredName) ? "" : item.PreferredName.Replace("?", "");

                            strNameKr = !string.IsNullOrEmpty(dt.Rows[iCount]["KName"].ToString()) ? dt.Rows[iCount]["KName"].ToString() : (!string.IsNullOrEmpty(strHangulPreferredName) ? strHangulPreferredName : strPreferredName);
                            strName = !string.IsNullOrEmpty(dt.Rows[iCount]["KName"].ToString()) ? dt.Rows[iCount]["KName"].ToString() : (!string.IsNullOrEmpty(strHangulPreferredName) ? strHangulPreferredName : strPreferredName);

                            //strNameKr = string.IsNullOrEmpty(dt.Rows[iCount]["KName"].ToString()) ?
                            //        (string.IsNullOrEmpty(item.HangulName) ? "" : item.HangulName) : dt.Rows[iCount]["KName"].ToString();
                            //strName = string.IsNullOrEmpty(dt.Rows[iCount]["KName"].ToString()) ?
                            //        (string.IsNullOrEmpty(item.HangulPreferredName) ? "" : item.HangulPreferredName) : dt.Rows[iCount]["KName"].ToString();

                            data.Add(new ChildItem()
                            {
                                IsOrphan = item.IsOrphan == true,
                                IsHandicapped = item.IsSpecialNeeds == true,
                                WaitingDays = ((TimeSpan)(DateTime.Now - Convert.ToDateTime(item.WaitingSinceDate))).Days,   // 현재일자 - 대기시작일자
                                Age = Convert.ToInt32(item.Age),
                                BirthDate = Convert.ToDateTime(item.BirthDate),         // 원래 샘플 : {2010-03-02 오전 12:00:00} 변경 샘플 : 1996-12-13 확인 필요
                                CountryCode = item.Beneficiary_LocalID.Substring(0, 2),
                                //                            CountryName = countries.FirstOrDefault(p => p.c_id == item.Beneficiary_LocalID.Substring(0, 2)).c_name,
                                CountryName = string.IsNullOrEmpty(item.Beneficiary_LocalID) ? "" : GetCountryName(item.Beneficiary_LocalID.Substring(0, 2)),
                                //        ChildMasterId = dr["childMasterId"].ToString(),
                                ChildMasterId = "09" + item.Beneficiary_GlobalID,
                                ChildKey = item.Beneficiary_LocalID,
                                //                                NameKr = string.IsNullOrEmpty(item.HangulName) ? "" : item.HangulName,
                                ChildGlobalId = item.Beneficiary_GlobalID,
                                NameEn = string.IsNullOrEmpty(item.FullName) ? "" : item.FullName,

                                HangulName = item.HangulName,
                                HangulPreferredName = item.HangulPreferredName,
                                FullName = item.FullName,
                                PreferredName = item.PreferredName,

                                NameKr = strNameKr,
                                Name = strName,

                                //NameKr = string.IsNullOrEmpty(dt.Rows[iCount]["KName"].ToString()) ?
                                //    (string.IsNullOrEmpty(item.HangulName) ? "" : item.HangulName) : dt.Rows[iCount]["KName"].ToString(),
                                //Name = string.IsNullOrEmpty(dt.Rows[iCount]["KName"].ToString()) ?
                                //    (string.IsNullOrEmpty(item.HangulPreferredName) ? "" : item.HangulPreferredName) : dt.Rows[iCount]["KName"].ToString(),

                                //NameKr = string.IsNullOrEmpty(dt.Rows[iCount]["KName"].ToString()) ?
                                //    (string.IsNullOrEmpty(item.HangulName) ? "" : item.HangulName) : dt.Rows[iCount]["KName"].ToString(),
                                //Name = string.IsNullOrEmpty(dt.Rows[iCount]["KName"].ToString()) ?
                                //    (string.IsNullOrEmpty(item.HangulPreferredName) ? "" : item.HangulPreferredName) : dt.Rows[iCount]["KName"].ToString(),

                                //NameKr = string.IsNullOrEmpty(item.HangulName) ? "" : item.HangulName,
                                //Name = string.IsNullOrEmpty(item.HangulPreferredName) ? "" : item.HangulPreferredName,
                                //NameDB = string.IsNullOrEmpty(dt.Rows[iCount]["KName"].ToString()) ? "" : dt.Rows[iCount]["KName"].ToString(),

                                Gender = item.Gender.Substring(0, 1) == "M" ? "남자" : "여자",
                                //        //Pic = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()),//ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
                                //        //Pic = dr["FullBodyImageURL"].ToString().Replace("w_150", "w_300"), // 어린이 사진 URL
                                //        // Global Pool 에서 어린이 이미지 조회
                                Pic = item.FullBodyImageURL,
                            });

                            iCount++;
                            //[이종진] 나눔펀딩의 경우, 최대5명
                            if (isUserFunding && iCount == 5)
                            {
                                break;
                            }
                            //테스트
                            //string beneDetail = _OffRampService.Beneficiary_GET(item.Beneficiary_GlobalID);
                            //TCPTModel.TCPTResponseMessage response = JsonConvert.DeserializeObject<TCPTModel.TCPTResponseMessage>(beneDetail);
                            //if(response.IsSuccessStatusCode)
                            //{
                            //    tmp.Add(item.Beneficiary_GlobalID);
                            //}
                        }
                    }
                }
            }
            // 수료 임박한 미결연 어린이(나눔펀딩)
        }
        else
        {
            Object[] objParam = new object[] { "page", "rowsPerPage", "CompletionMinMonth", "CompletionMaxMonth" };
            Object[] objValue = new object[] { page, rowsPerPage, CompletionMinMonth, CompletionMaxMonth };
            Object[] objSql = new object[] { "sp_web_tChildMaster_user_funding_list_f" };
            dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

            foreach (DataRow dr in dt.Rows)
            {
                data.Add(new ChildItem()
                {
                    IsOrphan = dr["Orphan"].ToString() == "Y",
                    IsHandicapped = dr["handicapped"].ToString() == "Y",
                    WaitingDays = Convert.ToInt32(dr["WaitingDays"]),
                    Total = Convert.ToInt32(dr["total"]),
                    Age = Convert.ToInt32(dr["childAge"]),
                    BirthDate = Convert.ToDateTime(dr["BirthDate"]),
                    CountryCode = dr["countryCode"].ToString(),
                    CountryName = dr["countryName"].ToString(),
                    ChildMasterId = dr["childMasterId"].ToString(),
                    ChildKey = dr["ChildKey"].ToString(),
                    NameKr = dr["NameKr"].ToString(),
                    NameEn = dr["NameEng"].ToString(),
                    Name = dr["Name"].ToString(),
                    Gender = dr["childGender"].ToString(), // 남자,여자

                    HangulName = "",
                    HangulPreferredName = "",
                    FullName = "",
                    PreferredName = "",

                    // 수정 문희원 2017-04-20
                    //Pic = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()),//ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
                    //Pic = dr["FullBodyImageURL"].ToString().Replace("w_150", "w_300"), // 어린이 사진 URL
                    Pic = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
                    CompletionDate = DateTime.ParseExact(dr["CompletionDate"].ToString(), "yyyy-MM-dd", null)
                });
                
            }
        }

        //result.message = "GetChildrenGp";

        result.data = data;
        result.success = true;
        return result;
    }





    public JsonWriter GetEventChildren(string campaignID)
    {
        //#13506
        //하루에 한 어린이만 노출
        //해당 어린이의 결연 여부를 체크해서 이미 결연이 되었다면 어린이 이미지 대신 별도의 다른 이미지를 노출

        JsonWriter result = new JsonWriter() { success = false };
        
        var data = new List<ChildItem>();
        
        Object[] objParam = new object[] { "campaignID", "NoUse" };
        Object[] objValue = new object[] { campaignID, 0 };
        Object[] objSql = new object[] { "sp_S_CampaignHoldChildList" };
        DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];


        //dt.DefaultView.Sort = " [ChildMasterID] ASC ";
        //dt = dt.DefaultView.ToTable();



        


        string nowChildMasterID = "";
        if (dt.Rows.Count > 0)
        {
            string nowDate = DateTime.Now.ToString("yyyy-MM-dd");

            //당일 날짜가 생성된 어린이 체크
            Object[] objSql2 = new object[1] { " SELECT ChildMasterID FROM tCampaignConsign WHERE CampaignID = '" + campaignID + "' AND EventDate = '" + nowDate + "' " };
            DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "Text", null, null);
            if (ds2.Tables[0].Rows.Count > 0)
            {
                nowChildMasterID = ds2.Tables[0].Rows[0]["ChildMasterID"].ToString().Trim();
            }

            //당일 날짜가 생성된 어린이가 없으면
            if (nowChildMasterID.Equals(""))
            {
               //날짜가 없는 미결연 어린이 가져오기
                Object[] objSql3 = new object[1] {
                    " SELECT top 1 CPC.ChildMasterID FROM tCampaignConsign CPC " +
                    " LEFT OUTER JOIN tcommitmentmaster ComM WITH (NOLOCK)ON CPC.ChildMasterID = ComM.ChildMasterID " +
                    " AND ComM.sponsorTypeeng IN('ChiSpo', 'ChiMon', 'LDPMON', 'LDPSPO') " +
                    " AND ComM.sponsoritemeng IN('ds', 'ls') " +
                    " AND isnull(ComM.stopdate, '') = '' " +
                    " WHERE CPC.CampaignID = '" + campaignID + "' AND CPC.EventDate is null AND ComM.CommitmentID is null " + 
                    " ORDER BY CPC.ChildMasterID " };
                DataSet ds3 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql3, "Text", null, null);
                if (ds3.Tables[0].Rows.Count > 0)
                {
                    //날짜가 없는 미결연 어린이가 있으면
                    nowChildMasterID = ds3.Tables[0].Rows[0]["ChildMasterID"].ToString().Trim();

                    //당일 날짜 업데이트
                    Object[] objSql4 = new object[1] { " UPDATE tCampaignConsign SET EventDate = '" + nowDate + "' WHERE CampaignID = '" + campaignID + "' AND ChildMasterID = '" + nowChildMasterID + "' " };
                    _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql4, "Text", null, null);
                }
                else
                {
                    //날짜값이 없는 미결연 어린이가 없으면 날짜 전체 null 처리
                    Object[] objSql4 = new object[1] { " UPDATE tCampaignConsign SET EventDate = null WHERE CampaignID = '" + campaignID + "' " };
                    _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql4, "Text", null, null);

                    //날짜가 없는 미결연 어린이 가져오기
                    Object[] objSql5 = new object[1] {
                        " SELECT top 1 CPC.ChildMasterID FROM tCampaignConsign CPC " +
                        " LEFT OUTER JOIN tcommitmentmaster ComM WITH (NOLOCK)ON CPC.ChildMasterID = ComM.ChildMasterID " +
                        " AND ComM.sponsorTypeeng IN('ChiSpo', 'ChiMon', 'LDPMON', 'LDPSPO') " +
                        " AND ComM.sponsoritemeng IN('ds', 'ls') " +
                        " AND isnull(ComM.stopdate, '') = '' " +
                        " WHERE CPC.CampaignID = '" + campaignID + "' AND CPC.EventDate is null AND ComM.CommitmentID is null " +
                        " ORDER BY CPC.ChildMasterID " };
                    DataSet ds5 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql5, "Text", null, null);
                    if (ds3.Tables[0].Rows.Count > 0)
                    {
                        //날짜가 없는 미결연 어린이가 있으면
                        nowChildMasterID = ds3.Tables[0].Rows[0]["ChildMasterID"].ToString().Trim();

                        //당일 날짜 업데이트
                        Object[] objSql6 = new object[1] { " UPDATE tCampaignConsign SET EventDate = '" + nowDate + "' WHERE CampaignID = '" + campaignID + "' AND ChildMasterID = '" + nowChildMasterID + "' " };
                        _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql6, "Text", null, null);
                    }
                }
            }

            if (!nowChildMasterID.Equals(""))
            {
                EnumerableRowCollection<DataRow> query = from CPC in dt.AsEnumerable()
                                                         where CPC.Field<string>("ChildMasterID") == nowChildMasterID
                                                         select CPC;
                DataView view = query.AsDataView();
                dt = view.ToTable();

                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["childMasterId"].ToString() == nowChildMasterID)
                    {
                        data.Add(new ChildItem()
                        {
                            WaitingDays = Convert.ToInt32(dr["WaitingDays"]),
                            Age = Convert.ToInt32(dr["childAge"]),
                            BirthDate = Convert.ToDateTime(dr["BirthDate"]),
                            CountryCode = dr["countryCode"].ToString(),
                            CountryName = dr["countryName"].ToString(),
                            ChildMasterId = dr["childMasterId"].ToString(),
                            ChildKey = dr["ChildKey"].ToString(),
                            NameKr = dr["NameKr"].ToString(),
                            NameEn = dr["NameEng"].ToString(),
                            Name = dr["ChildName"].ToString(),
                            Gender = dr["GenderCode"].ToString(), // 남자,여자
                            HangulName = "",
                            HangulPreferredName = "",
                            FullName = "",
                            PreferredName = "",
                            Pic = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
                            CommitmentID = dr["CommitmentID"].ToString()
                        });
                    }
                }
            }            
        }
        
        result.data = data;
        result.success = true;
        return result;
    }




    private string GetCountryName(string strCountryCode)
    {
        try
        {
            return htCountry[strCountryCode].ToString();
        }
        catch
        {
            return "";
        }
    }

    // 어린이 정보
    //    public JsonWriter GetChild(string strDbGpKind, string childMasterId)
    public JsonWriter GetChild(string childMasterId, string childGlobalId = "")
    {
        JsonWriter result = new JsonWriter() { success = false };
        //        string strdbgp_kind = HttpContext.Current.Request.Cookies["sdbgp_kind"].Value;

        if (string.IsNullOrEmpty(childGlobalId))
        {
            result = GetChildDb(childMasterId);
        }
        else
        {
            result = GetChildDbFromGlobalID(childGlobalId);
        }

        ////        DateTime dt1 = DateTime.Now;
        //if (strdbgp_kind == "1") // 기존 방식 -어린이 리스트 DB조회
        //{
        //    result = GetChildDb(childMasterId);
        //}
        //else if (strdbgp_kind == "2") // 신규 방식 -어린이 리스트 Global Pool조회
        //{
        //    //result = GetChildDb(childMasterId);
        //    result = GetChildGp(childMasterId);
        //}

        //DateTime dt2 = DateTime.Now;

        //TimeSpan ts = dt2 - dt1;
        //string sss = ts.ToString();
        return result;
    }

    // 어린이 정보 DB
    public JsonWriter GetChildDb(string childMasterId)
    {
        JsonWriter result = new JsonWriter() { success = false };
        DataTable dt = new DataTable();

        // 임시로 compass 연동

        Object[] objParam = new object[] { "childMasterId" };
        Object[] objValue = new object[] { childMasterId };
        Object[] objSql = new object[] { "sp_web_tChildMaster_get_f" };
        dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

        if (dt.Rows.Count == 0)
        {
            result.message = "어린이 조회 실패";
            result.success = false;
            return result;
        }

        DataRow dr = dt.Rows[0];


        result.data = new ChildItem()
        {
            //	GlobalId = dr["ChildMasterID"].ToString(),
            //	LocalBeneficiaryId = dr["ChildKey"].ToString(),
            IsOrphan = dr["Orphan"].ToString() == "Y",
            IsHandicapped = dr["handicapped"].ToString() == "Y",
            WaitingDays = Convert.ToInt32(dr["WaitingDays"]),
            Age = Convert.ToInt32(dr["ChildAge"]),
            BirthDate = Convert.ToDateTime(dr["BirthDate"]),
            CountryCode = dr["countryCode"].ToString(),
            CountryName = dr["countryName"].ToString(),
            ChildMasterId = dr["childMasterId"].ToString(),
            ChildKey = dr["ChildKey"].ToString(),
            NameKr = dr["name"].ToString(),   //[이종진] 애칭으로 가져오도록 변경
            NameEn = dr["NameEng"].ToString(),
            Name = dr["Name"].ToString(),
            Gender = dr["GenderCode"].ToString() == "M" ? "남자" : "여자",

            HangulName = "",
            HangulPreferredName = "",
            FullName = "",
            PreferredName = "",

            // 수정 문희원 2017-04-20
            //Pic = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()) //ChildAction.GetPicByChildKey(dr["ChildKey"].ToString())
            Pic = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
        };

        result.success = true;
        return result;
    }

    // 어린이 정보 DB
    public JsonWriter GetChildDbFromGlobalID(string childGlobalId)
    {
        JsonWriter result = new JsonWriter() { success = false };
        DataTable dt = new DataTable();

        // 임시로 compass 연동

        Object[] objParam = new object[] { "childGlobalId" };
        Object[] objValue = new object[] { childGlobalId };
        Object[] objSql = new object[] { "sp_web_tChildMaster_get_f_GlobalID" };
        dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

        if (dt.Rows.Count == 0)
        {
            result.message = "어린이 조회 실패";
            result.success = false;
            return result;
        }

        DataRow dr = dt.Rows[0];


        result.data = new ChildItem()
        {
            //	GlobalId = dr["ChildMasterID"].ToString(),
            //	LocalBeneficiaryId = dr["ChildKey"].ToString(),
            IsOrphan = dr["Orphan"].ToString() == "Y",
            IsHandicapped = dr["handicapped"].ToString() == "Y",
            WaitingDays = Convert.ToInt32(dr["WaitingDays"]),
            Age = Convert.ToInt32(dr["ChildAge"]),
            BirthDate = Convert.ToDateTime(dr["BirthDate"]),
            CountryCode = dr["countryCode"].ToString(),
            CountryName = dr["countryName"].ToString(),
            ChildMasterId = dr["childMasterId"].ToString(),
            ChildKey = dr["ChildKey"].ToString(),
            NameKr = dr["name"].ToString(),   //[이종진] 애칭으로 가져오도록 변경
            NameEn = dr["NameEng"].ToString(),
            Name = dr["Name"].ToString(),
            Gender = dr["GenderCode"].ToString() == "M" ? "남자" : "여자",

            HangulName = "",
            HangulPreferredName = "",
            FullName = "",
            PreferredName = "",

            // 수정 문희원 2017-04-20
            //Pic = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()) //ChildAction.GetPicByChildKey(dr["ChildKey"].ToString())
            Pic = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
        };

        result.success = true;
        return result;
    }

    // 어린이 정보 Global Pool
    public JsonWriter GetChildGp( string childMasterId ) {
		JsonWriter result = new JsonWriter() { success = false };

        string sResult = "";
        string strCountryCode = "";
        string strEName = "";

        string strChildMasterID = "";
        string strChildKey = "";
        string strFullBodyImageURL = "";

        DataTable dt = new DataTable();

        BeneficiaryAvailabilityQueryRequest_POST REQ = new BeneficiaryAvailabilityQueryRequest_POST();
        var data = new List<ChildItem>();

        // 어린이 키
        if (!string.IsNullOrEmpty(childMasterId))
        {
            string childGlobalId = "";
            //[이종진] 2018-02-26 - childMasterId로 ChildGlobalID를 조회한 후, 해당 GlobalID를 넘김.
            //단, GP에서 조회한 후, 아직 어린이정보Insert가 되지 않았다면, 앞에두자리를 제거 후 조회
            Object[] objSql = new object[] { "sp_S_ChildGlobalID" };
            Object[] objParam = new object[] { "ChildMasterID" };
            Object[] objValue = new object[] { childMasterId };
            dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
            if(dt.Rows.Count > 0 && dt.Rows[0]["ChildGlobalID"] != null)
            {
                childGlobalId = dt.Rows[0]["ChildGlobalID"].ToString();
            }
            else
            {
                childGlobalId = childMasterId.Substring(2);
            }

            List<string> value = new List<string>();
            value.Add(childGlobalId);

            Filter f = new Filter();
            f.Field = "BeneficiaryGlobalId";
            f.Operator = "is";
            f.Value = value;

            REQ.BeneficiarySearchRequestList.Filter.Add(f);
        }

        REQ.BeneficiarySearchRequestList.NumberOfBeneficiaries = 1;
        REQ.BeneficiarySearchRequestList.Start = 1;

        string json = JsonConvert.SerializeObject(REQ);
        CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();
        sResult = _OffRampService.BeneficiaryAvailabilityQueryRequest_POST(json);

        TCPTModel.TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(sResult);

        if (msg.IsSuccessStatusCode)
        {
            TCPTModel.Response.Beneficiary.BeneficiarySearchResponseList_Kit kit = new TCPTModel.Response.Beneficiary.BeneficiarySearchResponseList_Kit();
            kit = JsonConvert.DeserializeObject<BeneficiarySearchResponseList_Kit>(msg.RequestMessage.ToString());

            if (kit.BeneficiarySearchResponseList == null || kit.BeneficiarySearchResponseList.Count <= 0)
            {
                //MessageBox.Show("조회된 어린이가 없습니다.");
                result.success = false;
            }
            else
            {
                foreach (BeneficiarySearchResponseList item in kit.BeneficiarySearchResponseList)
                {
                    strCountryCode = item.Beneficiary_LocalID.Substring(0, 2);
                    strEName = item.PreferredName;

                    strChildMasterID = "09" + item.Beneficiary_GlobalID;
                    strChildKey = item.Beneficiary_LocalID;
                    strFullBodyImageURL = item.FullBodyImageURL;
                }

                Object[] objSql = new object[] { "sp_S_tChildShotName" };
                Object[] objParam = new object[] { "CountryCode", "EName", "ChildMasterID", "ChildKey", "FullBodyImageURL" };
                Object[] objValue = new object[] { strCountryCode, strEName, strChildMasterID, strChildKey, strFullBodyImageURL };
                dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

                if (dt.Rows.Count > 0)
                {
                    string strNameKr = "";
                    string strName = "";

                    string strHangulPreferredName = "";
                    string strPreferredName = "";

                    foreach (BeneficiarySearchResponseList item in kit.BeneficiarySearchResponseList)
                    {
                        strHangulPreferredName = item.HangulPreferredName.Replace("?", "");
                        strPreferredName = item.PreferredName.Replace("?", "");

                        strNameKr = !string.IsNullOrEmpty(dt.Rows[0]["KName"].ToString()) ? dt.Rows[0]["KName"].ToString() : (!string.IsNullOrEmpty(strHangulPreferredName) ? strHangulPreferredName : strPreferredName);
                        strName = !string.IsNullOrEmpty(dt.Rows[0]["KName"].ToString()) ? dt.Rows[0]["KName"].ToString() : (!string.IsNullOrEmpty(strHangulPreferredName) ? strHangulPreferredName : strPreferredName);

                        result.data = new ChildItem()
                        {
                            //	GlobalId = dr["ChildMasterID"].ToString(),
                            //	LocalBeneficiaryId = dr["ChildKey"].ToString(),

                            IsOrphan = item.IsOrphan == true,
                            IsHandicapped = item.IsSpecialNeeds == true,
                            WaitingDays = ((TimeSpan)(DateTime.Now - Convert.ToDateTime(item.WaitingSinceDate))).Days,   // 현재일자 - 대기시작일자
                            Age = Convert.ToInt32(item.Age),
                            BirthDate = Convert.ToDateTime(item.BirthDate),         // 원래 샘플 : {2010-03-02 오전 12:00:00} 변경 샘플 : 1996-12-13 확인 필요
                            CountryCode = item.Beneficiary_LocalID.Substring(0, 2),
                            CountryName = string.IsNullOrEmpty(item.Beneficiary_LocalID) ? "" : GetCountryName(item.Beneficiary_LocalID.Substring(0, 2)),
                            //                    ChildMasterId = dr["childMasterId"].ToString(),
                            ChildMasterId = "09" + item.Beneficiary_GlobalID,
                            ChildKey = item.Beneficiary_LocalID,
                            NameEn = string.IsNullOrEmpty(item.FullName) ? "" : item.FullName,

                            NameKr = strNameKr,
                            Name = strName,

                            //NameKr = string.IsNullOrEmpty(dt.Rows[0]["KName"].ToString()) ?
                            //        (string.IsNullOrEmpty(item.HangulName) ? "" : item.HangulName) : dt.Rows[0]["KName"].ToString(),
                            //Name = string.IsNullOrEmpty(dt.Rows[0]["KName"].ToString()) ?
                            //        (string.IsNullOrEmpty(item.HangulPreferredName) ? "" : item.HangulPreferredName) : dt.Rows[0]["KName"].ToString(),

                            //NameKr = string.IsNullOrEmpty(item.HangulName) ? "" : item.HangulName,
                            //Name = string.IsNullOrEmpty(item.HangulPreferredName) ? "" : item.HangulPreferredName,
                            //NameDB = string.IsNullOrEmpty(dt.Rows[0]["KName"].ToString()) ? "" : dt.Rows[0]["KName"].ToString(),

                            Gender = item.Gender.Substring(0, 1) == "M" ? "남자" : "여자",

                            HangulName = item.HangulName,
                            HangulPreferredName = item.HangulPreferredName,
                            FullName = item.FullName,
                            PreferredName = item.PreferredName,

                            // 수정 문희원 2017-04-20
                            //Pic = this.GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString()) //ChildAction.GetPicByChildKey(dr["ChildKey"].ToString())
                            Pic = item.FullBodyImageURL,
                        };
                    }

                    result.success = true;
                }
                else
                {
                    result.success = false;
                }
            }
        }
        else
        {
            result.success = false;
        }

        return result;
	}

    //[이종진]추가 - BeneficiaryKit조회 (어린이 상세정보)
    public JsonWriter Beneficiary_GET(string childMasterId)
    {
        JsonWriter result = new JsonWriter() { success = false };

        // beneficiary kit 가져오기
        CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();
        string beneDetail = _OffRampService.Beneficiary_GET(childMasterId.Substring(2));
        TCPTModel.TCPTResponseMessage response = JsonConvert.DeserializeObject<TCPTModel.TCPTResponseMessage>(beneDetail);

        //ChildAction.ChildItem item = new ChildAction.ChildItem();

        //성공
        if (response.IsSuccessStatusCode)
        {
            BeneficiaryResponseList_Kit beneKit = JsonConvert.DeserializeObject<BeneficiaryResponseList_Kit>(response.RequestMessage.ToString());
            foreach (var child in beneKit.BeneficiaryResponseList)
            {
                result.data = child;
                //item.IsOrphan = child.IsOrphan;
                //item.IsHandicapped = child.IsSpecialNeeds;
                ////item.WaitingDays = ((TimeSpan)(DateTime.Now - Convert.ToDateTime(child.WaitingSinceDate))).Days;   // 현재일자 - 대기시작일자
                //item.Age = Convert.ToInt32(item.Age);
                //item.BirthDate = Convert.ToDateTime(item.BirthDate);         // 원래 샘플 : {2010-03-02 오전 12:00:00} 변경 샘플 : 1996-12-13 확인 필요
                //item.CountryCode = child.Beneficiary_LocalID.Substring(0, 2);
                ////item.CountryName = string.IsNullOrEmpty(child.Beneficiary_LocalID) ? "" : GetCountryName(child.Beneficiary_LocalID.Substring(0, 2));
                //item.ChildMasterId = "08" + child.Beneficiary_GlobalID;
                //item.ChildKey = child.Beneficiary_LocalID;
                //item.NameEn = string.IsNullOrEmpty(child.FullName) ? "" : child.FullName;
                //item.HangulName = child.HangulName;
                //item.HangulPreferredName = child.HangulPreferredName;
                //item.FullName = child.FullName;
                //item.PreferredName = child.PreferredName;
                //item.NameKr = child.HangulPreferredName;
                //item.Name = child.FullName;
                //item.Gender = child.Gender.Substring(0, 1) == "M" ? "남자" : "여자";
                //item.Pic = child.FullBodyImageURL;
                //item.CompletionDate = DateTime.Parse(child.PlannedCompletionDate);
            }
            result.success = true;
        }
        else
        {
            // 실패
            result.success = false;
        }

        return result;
    }

    //[이종진]추가 - BeneficiaryKit조회 (어린이 상세정보)
    public JsonWriter Beneficiary_GET_UserFunding(string childMasterId, int age)
    {
        JsonWriter result = new JsonWriter() { success = false };

        ChildAction.ChildItem item = new ChildAction.ChildItem();

        // beneficiary kit 가져오기
        CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();
        string beneDetail = _OffRampService.Beneficiary_GET(childMasterId.Substring(2));
        TCPTModel.TCPTResponseMessage response = JsonConvert.DeserializeObject<TCPTModel.TCPTResponseMessage>(beneDetail);

        //성공
        if (response.IsSuccessStatusCode)
        {
            BeneficiaryResponseList_Kit beneKit = JsonConvert.DeserializeObject<BeneficiaryResponseList_Kit>(response.RequestMessage.ToString());
            foreach (var child in beneKit.BeneficiaryResponseList)
            {
                item.IsOrphan = child.IsOrphan;
                item.IsHandicapped = child.IsSpecialNeeds;
                //item.WaitingDays = ((TimeSpan)(DateTime.Now - Convert.ToDateTime(child.WaitingSinceDate))).Days;   // 현재일자 - 대기시작일자
                item.Age = age;
                item.BirthDate = Convert.ToDateTime(child.BirthDate);         // 원래 샘플 : {2010-03-02 오전 12:00:00} 변경 샘플 : 1996-12-13 확인 필요
                item.CountryCode = child.Beneficiary_LocalID.Substring(0, 2);
                item.CountryName = string.IsNullOrEmpty(child.Beneficiary_LocalID) ? "" : GetCountryName(child.Beneficiary_LocalID.Substring(0, 2));
                item.ChildMasterId = "09" + child.Beneficiary_GlobalID;
                item.ChildKey = child.Beneficiary_LocalID;
                item.NameEn = string.IsNullOrEmpty(child.FullName) ? "" : child.FullName;
                item.HangulName = child.HangulName;
                item.HangulPreferredName = child.HangulPreferredName;
                item.FullName = child.FullName;
                item.PreferredName = child.PreferredName;
                item.NameKr = child.HangulPreferredName;
                item.Name = child.FullName;
                item.Gender = child.Gender;
                item.Pic = child.FullBodyImageURL;
                item.CompletionDate = DateTime.Parse(child.PlannedCompletionDate);
            }

            //애칭 조회
            CommonLib.WWW6Service.SoaHelperSoap _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
            Object[] objSql = new object[1] { "SELECT ISNULL(KName, '" + item.PreferredName + "') AS KName FROM tChildShotName WHERE CountryCode = '" + item.CountryCode + "' AND EName = '" + item.PreferredName + "'" };
            DataSet obj = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
            item.Name = obj.Tables[0].Rows.Count > 0 ? obj.Tables[0].Rows[0]["KName"].ToString() : item.Name;
            

            result.success = true;
            result.data = item;
            return result;
        }
        else
        {
            // 실패
            return result;
        }
    }

    //[이종진]추가 - BeneficiaryKit조회 (어린이 상세정보)
    public JsonWriter InsertTCPTBene_tChild(object beneKit, string childMasterId, string childKey, bool isCDSP = false)
    {
        JsonWriter result = new JsonWriter() { success = false };

        //BeneficiaryResponseList kit = JsonConvert.DeserializeObject<BeneficiaryResponseList>(beneKit.ToString());
        BeneficiaryResponseList detail = (BeneficiaryResponseList)beneKit;
        string childGlobalID = childMasterId.Substring(2);

        // [이종진] 정보 체크
        // TCPT_Bene에 정보가 있는지 체크함
        Object[] objSql0 = new object[] { "sp_S_TCPT_Bene_Check" };
        Object[] objParam0 = new object[] { "ChildGlobalID" };
        Object[] objValue0 = new object[] { childGlobalID };
        DataTable dt0 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql0, "SP", objParam0, objValue0).Tables[0];

        //어린이 정보가 존재하는지 체크 후, 있다면 ChildMasterID를 조회해옴
        // [이종진] 어린이 정보 체크
        Object[] objSql2 = new object[] { "sp_S_tChildGp_Check" };
        Object[] objParam2 = new object[] { "ChildGlobalID", "ChildKey" };
        Object[] objValue2 = new object[] { childMasterId.Substring(2), childKey };
        DataTable dt2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2).Tables[0];

        string searchChildMasterId = "";
        //어린이 정보가 존재할 경우
        if (!string.IsNullOrEmpty(dt2.Rows[0]["RESULT"].ToString()))
        {
            searchChildMasterId = dt2.Rows[0]["RESULT"].ToString();
            childMasterId = dt2.Rows[0]["RESULT"].ToString();
        }

        //TCPT_Bene에 정보가 없다면 insert함
        // - 있다면 어린이 정보도 존재한다고 보고 어린이정보insert를 하지 않음
        if (dt0.Rows[0]["RESULT"].ToString() == "")
        {
            //2018-03-21 이종진 - 1:1결연에도 해당 로직을 적용.
            string saveHistory = "CreateUserFunding";
            string EventDetail = "나눔펀딩";
            if (isCDSP) saveHistory = "Case Study";
            if (isCDSP) EventDetail = "";

            //TCPT_Bene, TCPT_Household, TCPT_Household_Member insert 및
            //searchChildMasterId의 값이 ""면, 어린이정보insert하고 값이존재하면 update함
            string[] res = new ChildAction().InsertBeneficiartKit_DB(detail, "", searchChildMasterId, true, saveHistory, EventDetail, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            if (res.Length == 3 && !string.IsNullOrEmpty(res[1]))
            {
                childMasterId = res[1];  //ChildMasterId 리턴
            }

            // 어린이 이미지 다운로드
            if (res.Length == 3 && !string.IsNullOrEmpty(res[0]) && !string.IsNullOrEmpty(res[1]) && !string.IsNullOrEmpty(res[2]))
            {
                DataSet dsCheck = CheckChildMaster(childGlobalID);
                if (string.IsNullOrEmpty(dsCheck.Tables[0].Rows[0]["LastPhotoDate"].ToString()) || dsCheck.Tables[0].Rows[0]["LastPhotoDate"].ToString().Substring(0, 10) != detail.LastPhotoDate.Substring(0, 10))
                    DownloadImage(res[1], res[0], detail);
            }
        }

        result.success = true;
        result.data = childMasterId;
        return result;
    }

    /// <summary>
    /// Beneficiary Kit Insert
    /// </summary>
    /// <param name="detail"></param>
    /// <param name="uid"></param>
    /// <returns></returns>
    public string[] InsertBeneficiartKit_DB(BeneficiaryResponseList detail, string uid, string ChildMasterID, bool saveHistory, string historyType, string EventDetail, string eventDate)
    {
        //[0] TCPT_BENE UID
        //[1] ChildMasterID
        //[2] ChildDetailID
        string[] result = new string[3];
        //string result = "";

        string SP_Bene = "sp_I_TCPT_Bene";
        string SP_House = "sp_I_TCPT_Bene_Household";
        string SP_Member = "sp_I_TCPT_Bene_Household_Member";
        string SP_Master = "sp_I_tChildMaster_Detail";

        string userId = "";
        string userName = "";
        if(UserInfo.IsLogin)
        {
            userId = new UserInfo().UserId;
            userName = new UserInfo().UserName;
        }

        Guid UID = Guid.NewGuid();
        Guid houseUID = Guid.NewGuid();
        Guid OriginalKitUID = string.IsNullOrEmpty(uid) ? Guid.Empty : new Guid(uid);

        string eventDetail = string.IsNullOrEmpty(EventDetail) ? detail.RecordType_Name : EventDetail;
        string ChristianActivity_Name = "";
        string FavoriteProjectActivity = "";
        string FavoriteSchoolSubject = "";
        string ThingsILike = "";
        string HouseholdDuty_Name = "";
        string PhysicalDisability_Name = "";
        string ChronicIllness_Name = "";

        // tChildMaster, tChildDetail Insert용
        string childMasterID = string.Empty;

        if (string.IsNullOrEmpty(ChildMasterID))
        {
            if (detail.Beneficiary_CompassID == null)
                childMasterID = "09" + detail.Beneficiary_GlobalID.PadLeft(8, '0');
            else
                childMasterID = "07" + detail.Beneficiary_CompassID.PadLeft(8, '0');
        }
        else
            childMasterID = ChildMasterID;

        //string childDetailID = _DATService.getTimeStamp_Number("2");
        string childDetailID = DateTime.Now.ToString("yyyyMMddHHmmssff") + "2";
        string genderCode = detail.Gender == "남자" || detail.Gender == "Male" ? "M" : "F";
        string birthKnown = detail.BirthDate == "" || detail.BirthDate == null ? "N" : "Y";
        string orphan = detail.IsOrphan ? "Y" : "N";
        string handicapped = detail.PhysicalDisability_Name.Count > 0 ? "Y" : "N";

        foreach (string s in detail.ChristianActivity_Name)
            ChristianActivity_Name += s + ",";

        foreach (string s in detail.FavoriteProjectActivity)
            FavoriteProjectActivity += s + ",";

        foreach (string s in detail.FavoriteSchoolSubject)
            FavoriteSchoolSubject += s + ",";

        foreach (string s in detail.ThingsILike)
            ThingsILike += s + ",";

        foreach (string s in detail.HouseholdDuty_Name)
            HouseholdDuty_Name += s + ",";

        foreach (string s in detail.PhysicalDisability_Name)
            PhysicalDisability_Name += s + ",";

        foreach (string s in detail.ChronicIllness_Name)
            ChronicIllness_Name += s + ",";

        Object[] objParam1 = new object[56];
        Object[] objValue1 = new object[56];
        Object[] objSql1 = new object[] { SP_Bene };

        objParam1[0] = "UID";                          objValue1[0] = UID;
        objParam1[1] = "ChildDetailID";                objValue1[1] = childDetailID.ValueIfNull("");
        objParam1[2] = "Beneficiary_CompassID";        objValue1[2] = detail.Beneficiary_CompassID.ValueIfNull("");
        objParam1[3] = "Beneficiary_LocalID";          objValue1[3] = detail.Beneficiary_LocalID.ValueIfNull("");
        objParam1[4] = "Beneficiary_LocalNumber";      objValue1[4] = detail.Beneficiary_LocalNumber.ValueIfNull("");
        objParam1[5] = "AcademicPerformance_Name";     objValue1[5] = detail.AcademicPerformance_Name.ValueIfNull("");
        objParam1[6] = "AgeInYearsAndMonths";          objValue1[6] = detail.AgeInYearsAndMonths.ValueIfNull("");
        objParam1[7] = "BeneficiaryStatus";            objValue1[7] = detail.BeneficiaryStatus.ValueIfNull("");
        objParam1[8] = "BirthDate";                    objValue1[8] = detail.BirthDate.ValueIfNull("");
        objParam1[9] = "CorrespondenceLanguage";       objValue1[9] = detail.CorrespondenceLanguage.ValueIfNull("");
        objParam1[10] = "FirstName";                   objValue1[10] = detail.FirstName.ValueIfNull("");
        objParam1[11] = "LastName";                    objValue1[11] = detail.LastName.ValueIfNull("");
        objParam1[12] = "FullName";                    objValue1[12] = detail.FullName.ValueIfNull("");
        objParam1[13] = "FullBodyImageURL";            objValue1[13] = detail.FullBodyImageURL.ValueIfNull("");
        objParam1[14] = "FundType";                    objValue1[14] = detail.FundType.ValueIfNull("");
        objParam1[15] = "Gender";                      objValue1[15] = detail.Gender.ValueIfNull("");
        objParam1[16] = "Beneficiary_GlobalID";        objValue1[16] = detail.Beneficiary_GlobalID.ValueIfNull("");
        objParam1[17] = "IsInHIVAffectedArea";         objValue1[17] = detail.IsInHIVAffectedArea == true ? 1 : 0;
        objParam1[18] = "IsBirthDateEstimated";        objValue1[18] = detail.IsBirthDateEstimated == true ? 1 : 0;
        objParam1[19] = "IsOrphan";                    objValue1[19] = detail.IsOrphan == true ? 1 : 0;
        objParam1[20] = "IsSpecialNeeds";              objValue1[20] = detail.IsSpecialNeeds == true ? 1 : 0;
        objParam1[21] = "LastPhotoDate";               objValue1[21] = detail.LastPhotoDate.ValueIfNull("");
        objParam1[22] = "LastReviewDate";              objValue1[22] = detail.LastReviewDate.ValueIfNull("");
        objParam1[23] = "PreferredName";               objValue1[23] = detail.PreferredName.ValueIfNull("");
        objParam1[24] = "PrimaryCaregiverName";        objValue1[24] = detail.PrimaryCaregiverName.ValueIfNull("");
        objParam1[25] = "ProgramDeliveryType";         objValue1[25] = detail.ProgramDeliveryType.ValueIfNull("");
        objParam1[26] = "ChristianActivity_Name";      objValue1[26] = ChristianActivity_Name.ValueIfNull("");
        objParam1[27] = "FavoriteProjectActivity";     objValue1[27] = FavoriteProjectActivity.ValueIfNull("");
        objParam1[28] = "PhysicalDisability_Name";     objValue1[28] = PhysicalDisability_Name.ValueIfNull("");
        objParam1[29] = "ChronicIllness_Name";         objValue1[29] = ChronicIllness_Name.ValueIfNull("");
        objParam1[30] = "Cluster_Name";                objValue1[30] = detail.Cluster_Name.ValueIfNull("");
        objParam1[31] = "CognitiveAgeGroup_Name";      objValue1[31] = detail.CognitiveAgeGroup_Name.ValueIfNull("");
        objParam1[32] = "Community_Name";              objValue1[32] = detail.Community_Name.ValueIfNull("");
        objParam1[33] = "Country";                     objValue1[33] = detail.Country.ValueIfNull("");
        objParam1[34] = "FieldOffice_Name";            objValue1[34] = detail.FieldOffice_Name.ValueIfNull("");
        objParam1[35] = "GradeLevelLocal_Name";        objValue1[35] = detail.GradeLevelLocal_Name.ValueIfNull("");
        objParam1[36] = "GradeLevelUS_Name";           objValue1[36] = detail.GradeLevelUS_Name.ValueIfNull("");
        objParam1[37] = "HouseholdDuty_Name";          objValue1[37] = HouseholdDuty_Name.ValueIfNull("");
        objParam1[38] = "ICP_Country";                 objValue1[38] = detail.ICP_Country.ValueIfNull("");
        objParam1[39] = "ICP_ID";                      objValue1[39] = detail.ICP_ID.ValueIfNull("");
        objParam1[40] = "ICP_Name";                    objValue1[40] = detail.ICP_Name.ValueIfNull("");
        objParam1[41] = "RecordType_Name";             objValue1[41] = detail.RecordType_Name.ValueIfNull("");
        objParam1[42] = "FavoriteSchoolSubject";       objValue1[42] = FavoriteSchoolSubject.ValueIfNull("");
        objParam1[43] = "FormalEducationLevel";        objValue1[43] = detail.FormalEducationLevel.ValueIfNull("");
        objParam1[44] = "MajorOrCourseOfStudy";        objValue1[44] = detail.MajorOrCourseOfStudy.ValueIfNull("");
        objParam1[45] = "NotEnrolledInEducationReason";objValue1[45] = detail.NotEnrolledInEducationReason.ValueIfNull("");
        objParam1[46] = "PlannedCompletionDate";       objValue1[46] = detail.PlannedCompletionDate.ValueIfNull("");
        objParam1[47] = "PlannedCompletionDateChangeReason"; objValue1[47] = detail.PlannedCompletionDateChangeReason.ValueIfNull("");
        objParam1[48] = "ReviewStatus";                objValue1[48] = detail.ReviewStatus.ValueIfNull("");
        objParam1[49] = "SponsorshipStatus";           objValue1[49] = detail.SponsorshipStatus.ValueIfNull("");
        objParam1[50] = "ThingsILike";                 objValue1[50] = ThingsILike.ValueIfNull("");
        objParam1[51] = "VocationalTrainingType_Name"; objValue1[51] = detail.VocationalTrainingType_Name.ValueIfNull("");
        objParam1[52] = "HangulName";                  objValue1[52] = detail.HangulName.ValueIfNull("");
        objParam1[53] = "HangulPreferredName";         objValue1[53] = detail.HangulPreferredName.ValueIfNull("");
        objParam1[54] = "OriginalKitUID";              objValue1[54] = OriginalKitUID;
        objParam1[55] = "HouseHoldUID";                objValue1[55] = houseUID;
        
        int n = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql1, "SP", objParam1, objValue1);


        if (n > 0)
        {
            //TCPT_BENE UID
            result[0] = UID.ToString();

            TCPTModel.Response.Beneficiary.BeneficiaryHouseholdList houseHold = detail.BeneficiaryHouseholdList[0];

            Object[] objParam2 = new object[18];
            Object[] objValue2 = new object[18];
            Object[] objSql2 = new object[] { SP_House };

            objParam2[0] = "UID";                           objValue2[0] = houseUID;
            objParam2[1] = "Household_ID";                  objValue2[1] = houseHold.Household_ID.ValueIfNull("");
            objParam2[2] = "FemaleGuardianEmploymentStatus";objValue2[2] = houseHold.FemaleGuardianEmploymentStatus.ValueIfNull("");
            objParam2[3] = "FemaleGuardianOccupation";      objValue2[3] = houseHold.FemaleGuardianOccupation.ValueIfNull("");
            objParam2[4] = "IsNaturalFatherLivingWithChild";objValue2[4] = houseHold.IsNaturalFatherLivingWithChild;
            objParam2[5] = "IsNaturalMotherLivingWithChild";objValue2[5] = houseHold.IsNaturalMotherLivingWithChild;
            objParam2[6] = "MaleGuardianEmploymentStatus";  objValue2[6] = houseHold.MaleGuardianEmploymentStatus.ValueIfNull("");
            objParam2[7] = "MaleGuardianOccupation";        objValue2[7] = houseHold.MaleGuardianOccupation.ValueIfNull("");
            objParam2[8] = "Household_Name";                objValue2[8] = houseHold.Household_Name.ValueIfNull("");
            objParam2[9] = "NaturalFatherAlive";            objValue2[9] = houseHold.NaturalFatherAlive.ValueIfNull("");
            objParam2[10] = "NaturalMotherAlive";           objValue2[10] = houseHold.NaturalMotherAlive.ValueIfNull("");
            objParam2[11] = "NumberOfBrothers";             objValue2[11] = houseHold.NumberOfBrothers;
            objParam2[12] = "NumberOfSiblingBeneficiaries"; objValue2[12] = houseHold.NumberOfSiblingBeneficiaries;
            objParam2[13] = "NumberOfSisters";              objValue2[13] = houseHold.NumberOfSisters;
            objParam2[14] = "ParentsMaritalStatus";         objValue2[14] = houseHold.ParentsMaritalStatus.ValueIfNull("");
            objParam2[15] = "ParentsTogether";              objValue2[15] = houseHold.ParentsTogether.ValueIfNull("");
            objParam2[16] = "YouthHeadedHousehold";         objValue2[16] = houseHold.YouthHeadedHousehold;
            objParam2[17] = "RevisedValues";                objValue2[17] = houseHold.RevisedValues.ValueIfNull("");
            
            n = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2);

            // 등록실패
            if (n <= 0)
            {

            }
            else if (n > 0)
            {
                string sibling = "N";
                foreach (TCPTModel.Response.Beneficiary.BeneficiaryHouseholdMemberList member in houseHold.BeneficiaryHouseholdMemberList)
                {
                    if (!string.IsNullOrEmpty(member.GlobalID) && member.GlobalID != detail.Beneficiary_GlobalID && sibling == "N")
                        sibling = "Y";

                    Object[] objParam3 = new object[9];
                    Object[] objValue3 = new object[9];
                    Object[] objSql3 = new object[] { SP_Member };

                    objParam3[0] = "Household_UID";         objValue3[0] = houseUID;
                    objParam3[1] = "UID";                   objValue3[1] = Guid.NewGuid();
                    objParam3[2] = "FullName";              objValue3[2] = member.FullName.ValueIfNull("");
                    objParam3[3] = "GlobalID";              objValue3[3] = member.GlobalID.ValueIfNull("");
                    objParam3[4] = "LocalID";               objValue3[4] = member.LocalID.ValueIfNull("");
                    objParam3[5] = "HouseholdMemberRole";   objValue3[5] = member.HouseholdMemberRole.ValueIfNull("");
                    objParam3[6] = "IsCaregiver";           objValue3[6] = member.IsCaregiver == true ? 1 : 0;
                    objParam3[7] = "IsPrimaryCaregiver";    objValue3[7] = member.IsPrimaryCaregiver == true ? 1 : 0;
                    objParam3[8] = "HouseholdMember_Name";  objValue3[8] = member.HouseholdMember_Name.ValueIfNull("");
                                    
                    n = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql3, "SP", objParam3, objValue3);
                }

                //애칭조회
                string PersonalNameKr = "";
                string queryStr = "SELECT KName " +
                                  "FROM tChildShotName " +
                                  "WHERE EName = '" + detail.PreferredName.ValueIfNull("") + "' " +
                                  "AND CountryCode = " +
                                  "(SELECT CodeID " +
                                  "   FROM tSystemCode " +
                                  "  WHERE CodeType = 'ChildCountry' AND CodeNameEng = '"+ detail.Country.ValueIfNull("") + "')";
                Object[] objSql5 = new object[] { queryStr };
                DataSet nameDs = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql5, "Text", null, null);
                if (nameDs.Tables[0].Rows.Count > 0) //DB에 애칭이 있다면 DB애칭을 tChildMaster에 넣어줌
                {
                    PersonalNameKr = nameDs.Tables[0].Rows[0]["KName"].ToString();
                }
                else //DB에 애칭이 없다면, GlobalPool에서 준 한글애칭을 사용. 그것도 없다면 영문애칭을 넣음
                {
                    PersonalNameKr = string.IsNullOrEmpty(detail.HangulPreferredName) ? detail.PreferredName.ValueIfNull("") : detail.HangulPreferredName;
                }  
                
                Object[] objParam4 = new object[33];
                Object[] objValue4 = new object[33];
                Object[] objSql4 = new object[] { SP_Master };

                objParam4[0] = "ChildMasterID";         objValue4[0] = childMasterID.ValueIfNull("");
                objParam4[1] = "ChildDetailID";         objValue4[1] = childDetailID.ValueIfNull("");
                objParam4[2] = "ChildID";               objValue4[2] = detail.Beneficiary_CompassID == null ? "" : detail.Beneficiary_CompassID;
                objParam4[3] = "ChildKey";              objValue4[3] = detail.Beneficiary_LocalID.ValueIfNull("");
                objParam4[4] = "ProgramID";             objValue4[4] = "";
                objParam4[5] = "ProjectID";             objValue4[5] = "";
                objParam4[6] = "NameKr";                objValue4[6] = detail.HangulName.ValueIfNull("");
                objParam4[7] = "NameEng";               objValue4[7] = detail.FullName.ValueIfNull("");
                objParam4[8] = "AbbrNameKr";            objValue4[8] = detail.HangulName.ValueIfNull("");
                objParam4[9] = "AbbrNameEng";           objValue4[9] = detail.FullName.ValueIfNull("");
                objParam4[10] = "PersonalNameKr";        objValue4[10] = PersonalNameKr;
                objParam4[11] = "PersonalNameEng";       objValue4[11] = detail.PreferredName.ValueIfNull("");
                objParam4[12] = "CountryName";           objValue4[12] = detail.Country.ValueIfNull("");
                objParam4[13] = "GenderCode";            objValue4[13] = genderCode.ValueIfNull("");
                objParam4[14] = "BirthDate";             objValue4[14] = detail.BirthDate.ValueIfNull("");
                objParam4[15] = "BirthKnown";            objValue4[15] = birthKnown.ValueIfNull("");
                objParam4[16] = "Sibling";               objValue4[16] = sibling.ValueIfNull("");
                objParam4[17] = "Orphan";                objValue4[17] = orphan.ValueIfNull("");
                objParam4[18] = "Handicapped";           objValue4[18] = handicapped.ValueIfNull("");
                objParam4[19] = "StartDate";             objValue4[19] = DateTime.Now.ToString("yyyy-MM-dd");
                objParam4[20] = "NeedAllocHold";         objValue4[20] = "N";
                objParam4[21] = "LastCSDate";            objValue4[21] = eventDate.ValueIfNull("");
                objParam4[22] = "LastCSModDate";         objValue4[22] = eventDate.ValueIfNull("");
                objParam4[23] = "SponsorAmount";         objValue4[23] = 0;
                objParam4[24] = "CommentEng";            objValue4[24] = "";
                objParam4[25] = "CommentKr";             objValue4[25] = "";
                objParam4[26] = "ChangeDate";            objValue4[26] = DateTime.Now.ToString("yyyy-MM-dd");
                objParam4[27] = "ChangeType";            objValue4[27] = eventDetail.ValueIfNull("");
                objParam4[28] = "ChangeContext";         objValue4[28] = "";
                objParam4[29] = "RegisterID";            objValue4[29] = userId.ValueIfNull("");
                objParam4[30] = "RegisterName";          objValue4[30] = userName.ValueIfNull("");
                objParam4[31] = "ChildGlobalID";         objValue4[31] = detail.Beneficiary_GlobalID.ValueIfNull("");
                objParam4[32] = "BeneficiaryStatus";     objValue4[32] = detail.BeneficiaryStatus.ValueIfNull("");
                   
                n = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql4, "SP", objParam4, objValue4);

                result[1] = childMasterID;
                result[2] = childDetailID;

                // tChildHistory Insert
                if (saveHistory)
                    n = InsertTChildHistory(detail.LastReviewDate, childMasterID, detail.Beneficiary_GlobalID, historyType, "50", eventDetail, childDetailID, "", "", "", UID);
            }
        }

        return result;
    }



    /// <summary>
    /// tChildHistory 테이블에Insert
    /// </summary>
    /// <param name="childMasterID"></param>
    /// <param name="historyType"></param>
    /// <param name="sortPref"></param>
    /// <param name="eventDetail"></param>
    /// <param name="childDetailID"></param>
    /// <param name="correctionReason"></param>
    /// <returns></returns>
    private int InsertTChildHistory(string eventDate, string childMasterID, string beneficiary_GlobalID, string historyType, string sortPref,
        string eventDetail, string childDetailID, string correctionBefore, string correctionAfter, string correctionReason, Guid KitUID)
    {
        string userId = "";
        string userName = "";
        if (UserInfo.IsLogin)
        {
            userId = new UserInfo().UserId;
            userName = new UserInfo().UserName;
        }

        string SP_Histroy = "sp_I_tChildHistory";
        Object[] objParam = new object[13];
        Object[] objValue = new object[13];
        Object[] objSql = new object[] { SP_Histroy };

        objParam[0] = "ChildMasterID";          objValue[0] = childMasterID.ValueIfNull("");
        objParam[1] = "EventDate";              objValue[1] = eventDate.ValueIfNull("");
        objParam[2] = "Beneficiary_GlobalID";   objValue[2] = beneficiary_GlobalID.ValueIfNull("");
        objParam[3] = "HistoryType";            objValue[3] = historyType.ValueIfNull("");
        objParam[4] = "SortPref";               objValue[4] = sortPref.ValueIfNull("");
        objParam[5] = "EventDetail";            objValue[5] = eventDetail.ValueIfNull("");
        objParam[6] = "ChildDetailID";          objValue[6] = childDetailID.ValueIfNull("");
        objParam[7] = "CorrectionBefore";       objValue[7] = correctionBefore.ValueIfNull("");
        objParam[8] = "CorrectionAfter";        objValue[8] = correctionAfter.ValueIfNull("");
        objParam[9] = "CorrectionReason";       objValue[9] = correctionReason.ValueIfNull("");
        objParam[10] = "RegisterID";            objValue[10] = userId.ValueIfNull("");
        objParam[11] = "RegisterName";          objValue[11] = userName.ValueIfNull("");
        objParam[12] = "KitUID";                objValue[12] = KitUID;
            
        int n = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

        return n;
    }

    //[이종진] TCPT_HOLD_BENEFICIARY INSERT
    public bool InsertHoldHistory(Guid newHoldUID, string HoldID, string childGlobalId, string BeneficiaryState, string endDT, string UserId, string UserName, string code, string message)
    {
        string SP_HOLD = "sp_I_TCPT_Hold_Beneficiary";
        Object[] objParam = new object[15];
        Object[] objValue = new object[15];
        Object[] objSql = new object[] { SP_HOLD };

        objParam[0] = "HoldUID";                objValue[0] = newHoldUID;
        objParam[1] = "HoldID";                 objValue[1] = HoldID.ValueIfNull("");
        objParam[2] = "Beneficiary_GlobalID";   objValue[2] = childGlobalId.ValueIfNull("");
        objParam[3] = "HoldType";               objValue[3] = BeneficiaryState.ValueIfNull("");
        objParam[4] = "HoldEndDate";            objValue[4] = endDT.ValueIfNull("");
        objParam[5] = "PrimaryHoldOwner";       objValue[5] = UserId.ValueIfNull("");
        objParam[6] = "SecondaryHoldOwner";     objValue[6] = "";
        objParam[7] = "IsSpecialHandling";      objValue[7] = 0;
        objParam[8] = "EstimatedNoMoneyYieldRate"; objValue[8] = "";
        objParam[9] = "GlobalPartner_ID";       objValue[9] = "KR";
        objParam[10] = "LastReturnCode";        objValue[10] = code.ValueIfNull("");
        objParam[11] = "LastReturnMessage";     objValue[11] = message.ValueIfNull("");
        objParam[12] = "NotificationReason";    objValue[12] = "";
        objParam[13] = "CreateUserID";          objValue[13] = UserId.ValueIfNull("");
        objParam[14] = "CreateUserName";        objValue[14] = UserName.ValueIfNull("");
        
        _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
        return true;
    }

    /// <summary>
    /// child Global ID로 어린이 정보 조회
    /// </summary>
    /// <param name="Beneficiary_GlobalID"></param>
    /// <returns></returns>
    public DataSet CheckChildMaster(string Beneficiary_GlobalID)
    {
        // chidl global ID 로 
        
        Object[] objSql2 = new object[] { "sp_S_TCPT_BeneficiaryCnt" };
        Object[] objParam2 = new object[] { "Beneficiary_GlobalID" };
        Object[] objValue2 = new object[] { Beneficiary_GlobalID };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2);
        return ds;

    }

    /// <summary>
    /// 어린이 이미지 저장
    /// </summary>
    /// <param name="childMasterID"></param>
    /// <param name="Bene_UID"></param>
    /// <param name="item"></param>
    public void DownloadImage(string childMasterID, string Bene_UID, BeneficiaryResponseList item)
    {
        byte[] childImage;
        byte[] childSmallImage;
        byte[] headShot;
        byte[] childSmallImageForWeb;
        string url = item.FullBodyImageURL;

        try
        {
            //url에 있는 image를 byte[]로 저장
            var webClient = new WebClient();
            childSmallImage = webClient.DownloadData(url.Replace("w_150", "w_300,a_90"));
            childImage = webClient.DownloadData(url.Replace("w_150", "w_1200"));
            headShot = webClient.DownloadData(url.Replace("w_150", "w_150,c_thumb"));
            childSmallImageForWeb = webClient.DownloadData(url.Replace("w_150", "w_300"));

            MemoryStream ms1 = new MemoryStream(childSmallImage, 0, childSmallImage.Length);
            ms1.Write(childSmallImage, 0, childSmallImage.Length);
            Image childSmallImg = Image.FromStream(ms1, true);//Exception occurs here

            MemoryStream ms2 = new MemoryStream(childImage, 0, childImage.Length);
            ms2.Write(childImage, 0, childImage.Length);
            Image childImg = Image.FromStream(ms2, true);//Exception occurs here

            MemoryStream ms3 = new MemoryStream(headShot, 0, headShot.Length);
            ms3.Write(headShot, 0, headShot.Length);
            Image headShotImg = Image.FromStream(ms3, true);//Exception occurs here

            //이미지 DB에 저장
            string childDBID = "";
            string imageID = new Random().Next(0, 999999).ToString();
            string imageDate = string.IsNullOrEmpty(item.LastPhotoDate) ? "" : Convert.ToDateTime(item.LastPhotoDate).ToString("yyyy-MM-dd hh:mm:ss"); 
            string childKey = item.Beneficiary_LocalID;
            string imageGlag = "T";
            int imageWidth = headShotImg.Width;
            int imageHeight = headShotImg.Height;
            string tcpt_Bene_UID = Bene_UID;
            string UserId = "";
            string UserName = "";
            if (UserInfo.IsLogin)
            {
                UserId = new UserInfo().UserId;
                UserName = new UserInfo().UserName;
            }

            string SP_HIS = "sp_I_ChildImage";
            Object[] objParam = new object[12];
            Object[] objValue = new object[12];
            Object[] objSql = new object[] { SP_HIS };
            
            objParam[0] = "ChildMasterID"; objValue[0] = childMasterID;
            objParam[1] = "ImageDBID"; objValue[1] = "7";
            objParam[2] = "ImageDate"; objValue[2] = imageDate;
            objParam[3] = "ChildKey"; objValue[3] = childKey;
            objParam[4] = "ImageFlag"; objValue[4] = imageGlag.ValueIfNull("");
            objParam[5] = "ImageWidth"; objValue[5] = imageWidth;
            objParam[6] = "ImageHeight"; objValue[6] = imageHeight;
            objParam[7] = "TCPT_BENE_UID"; objValue[7] = new Guid(tcpt_Bene_UID);
            objParam[8] = "Image"; objValue[8] = childImage;
            objParam[9] = "ImageSmall"; objValue[9] = childSmallImage;
            //objParam[8] = "Image"; objValue[8] = childImg;
            //objParam[9] = "ImageSmall"; objValue[9] = childSmallImg;
            objParam[10] = "RegisterID"; objValue[10] = UserId;
            objParam[11] = "RegisterName"; objValue[11] = UserName;
            
            _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
            //_www6Service.
            
            //서버에 이미지 업로드
            //CommonLib.WWW4Service.ServiceSoapClient www4 = new CommonLib.WWW4Service.ServiceSoapClient();
            //string a = _www4Service.uploadChildThumbnailImage_New(childSmallImageForWeb, childImage, headShot, childKey);
            //string uploadResult = www4.uploadChildThumbnailImage_New(childSmallImageForWeb, childImage, headShot, childKey);
            //2018-03-28 이종진 - 사진가져오기 되지 않아 임의로 건너뜀. 개발예정
            string uploadResult = "11";
            //- 반환값이 10이 아닐경우 에러창 팝업 --------------------
            if (uploadResult != "10")
            {
                ErrorLog.Write(HttpContext.Current, 0, "Web CDSP DownloadImage : " + DateTime.Now.ToString("yyyy-MM-dd") + " : 서버에 이미지업로드실패 : " + uploadResult);
                return;
            }
        }
        catch(Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, "Web CDSP DownloadImage : " + DateTime.Now.ToString("yyyy-MM-dd") + " : " + ex.Message);
            return;
        }

    }

    // 어린이 casestudy 
    public class ChildCaseStudy {
		public string family;	// 가족
		public string health;	// 건강
		public string hobby;	// 취미
		public string schooling;	// 학교
		public string christianActivities;	// 종교활동
		public string familyDuties;		// 집안일
	}

	public JsonWriter GetCaseStudy( string childMasterId , string childKey ) {

		JsonWriter result = new JsonWriter() { success = false };

		try {

            //TCPT이후, 최근데이터 조회 2018-04 이종진 추가
            result = GetCaseStudyAfterTCPT(childMasterId, childKey);
            if(result.success)
            {
                return result;
            }

            //TCPT 이후 데이터 존재하지 않으면 TCPT이전 데이터를 조회 - 기존 방법
            result = GetCaseStudyBeforeTCPT(childMasterId, childKey);
            return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;
			return result;
		}

	}

    //TCPT 이후 어린이데이터를 가져옴. TCPT 는 2017년 상반기에 되었음. TCPT_Bene 테이블 등
    public JsonWriter GetCaseStudyAfterTCPT(string childMasterId, string childKey)
    {

        JsonWriter result = new JsonWriter() { success = false };

        try
        {
            Object[] objParam = new object[] { "ChildKey" };
            Object[] objValue = new object[] { childKey };
            Object[] objSql = new object[] { "sp_web_child_new_caseStudy_get_f" };
            DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

            //데이터 존재하지 않으면 return
            if(dt.Rows[0]["IS_EXISTS"].ToString() == "N")
            {
                return result;
            }

            ChildCaseStudy data = new ChildCaseStudy();
            //건강(장애)
            data.health = dt.Rows[0]["PhysicalDisability_Name"] == null ? "" : dt.Rows[0]["PhysicalDisability_Name"].ToString();
            //보호자
            data.family = dt.Rows[0]["Caregiver"] == null ? "" : dt.Rows[0]["Caregiver"].ToString(); 
            //취미
            data.hobby = dt.Rows[0]["ThingsILike"] == null ? "" : dt.Rows[0]["ThingsILike"].ToString(); ;
            //학교
            data.schooling = dt.Rows[0]["FormalEducationLevel"] == null ? "" : dt.Rows[0]["FormalEducationLevel"].ToString(); ;
            //종교활동
            data.christianActivities = dt.Rows[0]["ChristianActivity_Name"] == null ? "" : dt.Rows[0]["ChristianActivity_Name"].ToString(); ;
            //집안일
            data.familyDuties = dt.Rows[0]["HouseholdDuty_Name"] == null ? "" : dt.Rows[0]["HouseholdDuty_Name"].ToString(); ;

            result.data = data;
            result.success = true;
            return result;
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;
            return result;
        }

    }

    //TCPT 이전 어린이데이터를 가져옴. compass3 DB - TCPT 는 2017년 상반기에 되었음.
    public JsonWriter GetCaseStudyBeforeTCPT(string childMasterId, string childKey)
    {

        JsonWriter result = new JsonWriter() { success = false };

        // 임시로 compass 연동
        try
        {
            Object[] objParam = new object[] { "childKey" };
            Object[] objValue = new object[] { childKey };
            Object[] objSql = new object[] { "sp_web_child_caseStudy_get_f" };
            DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];


            ChildCaseStudy data = new ChildCaseStudy();
            data.health = this.GetHandicapTranslate(childMasterId);

            foreach (DataRow dr in dt.Rows)
            {
                var category = dr["category"].ToString();
                var content = dr["content"].ToString();

                switch (category)
                {
                    case "보호자":
                        data.family = content;
                        break;
                    case "건강":
                        //data.health = content;	
                        break;
                    case "취미":
                        data.hobby = content;
                        break;
                    case "학교":
                        data.schooling = content;
                        break;
                    case "종교활동":
                        data.christianActivities = content;
                        break;
                    case "집안일":
                        data.familyDuties = content;
                        break;

                }
            }

            result.data = data;
            result.success = true;
            return result;
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;
            return result;
        }

    }

    string GetHandicapTranslate( string childMasterId ) {

		#region src
		try {

			string sTranslationString = "";
			Object[] objSql = new object[1] { "sp_web_child_handicap_get_f" };
			Object[] objParam = new object[] { "childMasterId" };
			Object[] objValue = new object[] { childMasterId };


			var ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
			var dtHealth = ds.Tables[0];
			var sHealth = ds.Tables[1].Rows[0]["studyContent"].ToString();

			var saHealth = sHealth != "" ? sHealth.Split(',') : null;

			//- saHH의 배열값이 없을 경우
			if(saHealth == null) {
				return "";
			}

			string[] saHealth0; //for
			string[] saHealth5;
			string[] saHealth6;
			string[] saHealth7;
			string[] saHealth8;
			string[] saHealth9;
			string[] saHealth10;
			string[] saHealth11;
			string[] saHealth12;
			string[] saHealth13;
			string[] saHealth14;
			string[] saHealth15;
			string[] saHealth16;
			string[] saHealth17;
			string[] saHealth18;
			string[] saHealth19;
			string[] saHealth20;


			//- saHealth : Epilepsy, Asthma, Polio, developmentally disabled

			for(int i = 0; i < 4; i++) //saHH의 1~4번만
			{
				saHealth0 = saHealth[i].Split(':');
				if(saHealth0[1].ToString().Trim() == "T") //2016-07-11 문지예
				{
					dtHealth.DefaultView.RowFilter = "CodeID = '" + saHealth0[0].Trim() + "'";
					sTranslationString += " " + dtHealth.DefaultView[0]["CodeName"].ToString();
				}

			}


			//- saHealth : Spine due to
			saHealth5 = saHealth[5].Split(':');
			if(saHealth5[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 척추골 장애가 있습니다.";
			}

			//- saHealth : Left Foot due to, Right Foot due to
			saHealth6 = saHealth[6].Split(':');
			saHealth7 = saHealth[7].Split(':');
			if(saHealth6[1].ToLower().Contains("normal") == false && saHealth7[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 양쪽 발에 장애가 있습니다.";
			} else if(saHealth6[1].ToLower().Contains("normal") == false && saHealth7[1].ToLower().Contains("normal") == true) {
				sTranslationString += " 왼발에 장애가 있습니다.";
			} else if(saHealth6[1].ToLower().Contains("normal") == true && saHealth7[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 오른발에 장애가 있습니다.";
			}

			//- saHealth : Left Hand due to, Right Hand due to
			saHealth8 = saHealth[8].Split(':');
			saHealth9 = saHealth[9].Split(':');
			if(saHealth8[1].ToLower().Contains("normal") == false && saHealth9[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 양쪽 손에 장애가 있습니다.";
			} else if(saHealth8[1].ToLower().Contains("normal") == false && saHealth9[1].ToLower().Contains("normal") == true) {
				sTranslationString += " 왼손에 장애가 있습니다.";
			} else if(saHealth8[1].ToLower().Contains("normal") == true && saHealth9[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 오른손에 장애가 있습니다.";
			}

			//- saHealth : Left Leg due to, Right Leg due to
			saHealth10 = saHealth[10].Split(':');
			saHealth11 = saHealth[11].Split(':');
			if(saHealth10[1].ToLower().Contains("normal") == false && saHealth11[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 양쪽 다리에 장애가 있습니다.";
			} else if(saHealth10[1].ToLower().Contains("normal") == false && saHealth11[1].ToLower().Contains("normal") == true) {
				sTranslationString += " 왼다리에 장애가 있습니다.";
			} else if(saHealth10[1].ToLower().Contains("normal") == true && saHealth11[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 오른다리에 장애가 있습니다.";
			}

			//- saHealth : Normal, Left Arm due to, Right Arm due to
			saHealth12 = saHealth[12].Split(':');
			saHealth13 = saHealth[13].Split(':');
			if(saHealth12[1].ToLower().Contains("normal") == false && saHealth13[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 양쪽 팔에 장애가 있습니다.";
			} else if(saHealth12[1].ToLower().Contains("normal") == false && saHealth13[1].ToLower().Contains("normal") == true) {
				sTranslationString += " 왼팔에 장애가 있습니다.";
			} else if(saHealth12[1].ToLower().Contains("normal") == true && saHealth13[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 오른팔에 장애가 있습니다.";
			}

			//- saHealth : Speech
			saHealth14 = saHealth[14].Split(':');
			if(saHealth14[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 언어 장애가 있습니다.";
			} else if(saHealth14[1].ToLower().Contains("mute") == true) {
				sTranslationString += " 말을 하지 못합니다.";
			}

			//- saHealth : Hearing Left Ear, Hearing Right Ear
			saHealth15 = saHealth[15].Split(':');
			saHealth16 = saHealth[16].Split(':');
			if(saHealth15[1].ToLower().Contains("deaf") == true && saHealth16[1].ToLower().Contains("deaf") == true) {
				sTranslationString += " 소리를 듣지 못합니다.";
			} else if(saHealth15[1].ToLower().Contains("defective") == true && saHealth16[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 청각 장애가 있습니다.";
			} else if(saHealth15[1].ToLower().Contains("deaf") == true && saHealth16[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 왼쪽 귀가 들리지 않고 오른쪽 청각에도 장애가 있습니다.";
			} else if(saHealth15[1].ToLower().Contains("defective") == true && saHealth16[1].ToLower().Contains("deaf") == true) {
				sTranslationString += " 오른쪽 귀가 들리지 않고 왼쪽 청각에도 장애가 있습니다.";
			} else if(saHealth15[1].ToLower().Contains("deaf") == true) {
				sTranslationString += " 왼쪽 귀가 들리지 않습니다.";
			} else if(saHealth15[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 왼쪽 청각에 장애가 있습니다.";
			} else if(saHealth16[1].ToLower().Contains("deaf") == true) {
				sTranslationString += " 오른쪽 귀가 들리지 않습니다.";
			} else if(saHealth16[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 오른쪽 청각에 장애가 있습니다.";
			}

			//- saHealth : Sight Left Eye, Sight Right Eye
			saHealth17 = saHealth[17].Split(':');
			saHealth18 = saHealth[18].Split(':');
			if(saHealth17[1].ToLower().Contains("blind") == true && saHealth18[1].ToLower().Contains("blind") == true) {
				sTranslationString += " 앞을 보지 못합니다.";
			} else if(saHealth17[1].ToLower().Contains("defective") == true && saHealth18[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 시각 장애가 있습니다.";
			} else if(saHealth17[1].ToLower().Contains("blind") == true && saHealth18[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 왼쪽 눈을 볼 수 없고 오른쪽 눈에도 장애가 있습니다.";
			} else if(saHealth17[1].ToLower().Contains("defective") == true && saHealth18[1].ToLower().Contains("blind") == true) {
				sTranslationString += " 오른쪽 눈을 볼 수 없고 왼쪽 눈에도 장애가 있습니다.";
			} else if(saHealth17[1].ToLower().Contains("blind") == true) {
				sTranslationString += " 왼쪽 눈을 볼 수 없습니다.";
			} else if(saHealth17[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 왼쪽 눈에 장애가 있습니다.";
			} else if(saHealth18[1].ToLower().Contains("blind") == true) {
				sTranslationString += " 오른쪽 눈을 볼 수 없습니다.";
			} else if(saHealth18[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 오른쪽 눈에 장애가 있습니다.";
			}

			//- saHealth : Regular medical treatmnt?, Regular medication?
			saHealth19 = saHealth[19].Split(':');
			saHealth20 = saHealth[20].Split(':');
			if(saHealth19[1].Trim() != "F" && saHealth20[1].Trim() != "F") {
				sTranslationString += " 정기적인 치료를 받고 약을 복용하고 있습니다.";
			} else {

				if(saHealth19[1].Trim() != "F") {
					dtHealth.DefaultView.RowFilter = "CodeID = '" + saHealth19[0].Trim() + "'";
					sTranslationString += " " + dtHealth.DefaultView[0]["CodeName"].ToString();
				} else if(saHealth20[1].Trim() != "F") {
					dtHealth.DefaultView.RowFilter = "CodeID = '" + saHealth20[0].Trim() + "'";
					sTranslationString += " " + dtHealth.DefaultView[0]["CodeName"].ToString();
				}
			}

			return sTranslationString;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			return "";
		}
		#endregion

		return "";
	}

    // 어린이 정보
    /*
	public JsonWriter GetChildImage( string childMasterId , string childKey ) {

		JsonWriter result = new JsonWriter() { success = false };

        String sqlText = " SELECT top 1  tChildImage_EN.image_file as ChildImage " +
				 "      , tChildImage.ChildImage AS ChildImage_2 " +
				 "      , ChildSmallImage " +
				 "      , Convert(varchar(10), ImageDate, 121) AS CaseStudyDate " +
				 " FROM  kr_Compass4_image.dbo.tChildImage tChildImage WITH (NOLOCK) " +
				 " INNER JOIN   compass_image.dbo.need_image tChildImage_EN WITH (NOLOCK) " +
				 "   ON  tChildImage_EN.image_date = tChildImage.ImageDate " +
				 "  AND  tChildImage_EN.need_key   = tChildImage.ChildKey COLLATE Korean_Wansung_CI_AS " +
				 "  WHERE ChildMasterID IS NOT NULL " +
				 "  AND  ChildMasterID = '" + childMasterId + "'" +
				 "  ORDER BY ImageDate DESC ";
		
		Object[] objSql = new object[1] { sqlText };
		DataSet obj = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

		if(obj == null) {
			return result;
		}
		var dt = obj.Tables[0];
		if(dt.Rows.Count < 1) {
			result.data = ChildAction.GetPicByChildKey(childKey);
		} else {
			DataRow dr = dt.Rows[0];

			var bytes = (byte[])dr["ChildImage"]; //어린이 이미지
			string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
			string img = "data:image/jpeg;base64," + base64String;

			result.data = img;
		}

		result.success = true;
		return result;


	}
    */

    public string GetChildImage(string childMasterId, string childKey)
    {
        //ErrorLog.Write(HttpContext.Current, 0, "GetChildImage:Start");
        string childImg = "";

        string childImageType = ConfigurationManager.AppSettings["childImageType"]; //DB, Connect

        if (childImageType.Equals("DB"))
        {
            //ErrorLog.Write(HttpContext.Current, 0, "GetChildImage:DB");
            try
            {
                string sponsorID = "";
                if (UserInfo.IsLogin)
                {
                    sponsorID = new UserInfo().SponsorID;
                }

                CommonLib.WWW6Service.SoaHelperSoap _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
                Object[] objParam = new object[] { "ChildMasterID", "SponsorID" };
                Object[] objValue = new object[] { childMasterId, sponsorID };
                Object[] objSql = new object[] { "sp_s_FullBodyImageDB_For_Web" };
                DataTable data = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
                if (data == null || data.Rows.Count == 0)
                {
                    Object[] objParam1 = new object[] { "Kind", "ChildKey" };
                    Object[] objValue1 = new object[] { "01", childKey };
                    Object[] objSql1 = new object[] { "sp_S_tTmpChildMasterKey" };
                    DataTable data1 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql1, "SP", objParam1, objValue1).Tables[0];

                    if (data1 != null && data1.Rows.Count > 0)
                    {
                        childImg = data1.Rows[0]["FullBodyImageURL"].ToString();
                    }
                }
                else
                {
                    var bytes = (byte[])data.Rows[0]["ChildImage"]; //어린이 이미지
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    childImg = "data:image/jpeg;base64," + base64String;
                }
            }
            catch (Exception e)
            {
                ErrorLog.Write(HttpContext.Current, 0, "GetChildImage:Error:" + e.Message.ToString());
            }

        }
        else
        {
            childImg = ChildAction.GetPicByChildKey(childKey);
            //ErrorLog.Write(HttpContext.Current, 0, childImg);
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(childImg) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    childImg = ChildAction.GetPicByChildKey(childKey.Substring(0, 2) + childKey.Substring(3, 3) + childKey.Substring(7, 4));
                }
                response.Close();
                //            return (response.StatusCode == HttpStatusCode.OK);012345678900
            }
            catch
            {
                try
                {
                    childImg = ChildAction.GetPicByChildKey(childKey.Substring(0, 2) + childKey.Substring(3, 3) + childKey.Substring(7, 4));
                }
                catch
                {
                    childImg = "";
                }
            }
        }
        //ErrorLog.Write(HttpContext.Current, 0, "GetChildImage:End");
        return childImg;

        /*
        String sqlText = " SELECT top 1  tChildImage_EN.image_file as ChildImage " +
                 "      , tChildImage.ChildImage AS ChildImage_2 " +
                 "      , ChildSmallImage " +
                 "      , Convert(varchar(10), ImageDate, 121) AS CaseStudyDate " +
                 " FROM  kr_Compass4_image.dbo.tChildImage tChildImage WITH (NOLOCK) " +
                 " INNER JOIN   compass_image.dbo.need_image tChildImage_EN WITH (NOLOCK) " +
                 "   ON  tChildImage_EN.image_date = tChildImage.ImageDate " +
                 "  AND  tChildImage_EN.need_key   = tChildImage.ChildKey COLLATE Korean_Wansung_CI_AS " +
                 "  WHERE ChildMasterID IS NOT NULL " +
                 "  AND  ChildMasterID = '" + childMasterId + "'" +
                 "  ORDER BY ImageDate DESC ";

        Object[] objSql = new object[1] { sqlText };
        DataSet obj = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

        if(obj == null) {
            return ChildAction.GetPicByChildKey(childKey);
        }
        var dt = obj.Tables[0];
        if(dt.Rows.Count < 1) {
            return ChildAction.GetPicByChildKey(childKey);
        } else {
            DataRow dr = dt.Rows[0];

            var bytes = (byte[])dr["ChildImage"]; //어린이 이미지
            string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            string img = "data:image/jpeg;base64," + base64String;

            return img;
        }
        */

    }

    // ICP Json으로 센터 정보 
    public string[] GetProjectInfo(string json, int age)
    {
        string[] result = new string[5];

        string projInfo = string.Empty;
        string Spiritual = string.Empty;
        string Cognitive = string.Empty;
        string Physical = string.Empty;
        string SocioEmotional = string.Empty;
        string ICP_ChurchMinistries = string.Empty;
        string ICP_Facilities = string.Empty;
        string ICP_PrimaryDiet = string.Empty;
        string ICP_PrimaryOccupation = string.Empty;
        string ICP_UtilitiesOnSite = string.Empty;

        ICPResponseList list = JsonConvert.DeserializeObject<ICPResponseList>(json);
        if (list != null)
        {
            foreach (string s in list.ChurchMinistry)
                ICP_ChurchMinistries += s + ",";
            foreach (string s in list.Facilities)
                ICP_Facilities += s + ",";
            foreach (string s in list.PrimaryDiet)
                ICP_PrimaryDiet += s + ",";
            foreach (string s in list.PrimaryOccupation)
                ICP_PrimaryOccupation += s + ",";
            foreach (string s in list.UtilitiesOnSite)
                ICP_UtilitiesOnSite += s + ",";

            if (!string.IsNullOrEmpty(ICP_ChurchMinistries))
                ICP_ChurchMinistries = ICP_ChurchMinistries.Substring(0, ICP_ChurchMinistries.Length - 1);
            if (!string.IsNullOrEmpty(ICP_Facilities))
                ICP_Facilities = ICP_Facilities.Substring(0, ICP_Facilities.Length - 1);
            if (!string.IsNullOrEmpty(ICP_PrimaryDiet))
                ICP_PrimaryDiet = ICP_PrimaryDiet.Substring(0, ICP_PrimaryDiet.Length - 1);
            if (!string.IsNullOrEmpty(ICP_PrimaryOccupation))
                ICP_PrimaryOccupation = ICP_PrimaryOccupation.Substring(0, ICP_PrimaryOccupation.Length - 1);
            if (!string.IsNullOrEmpty(ICP_UtilitiesOnSite))
                ICP_UtilitiesOnSite = ICP_UtilitiesOnSite.Substring(0, ICP_UtilitiesOnSite.Length - 1);

            projInfo += "■ 어린이센터<br/>";
            projInfo += "ㆍ어린이센터 번호 : " + list.ICP_ID + "(" + list.ICP_Name + ")<br/>";

            if (!string.IsNullOrEmpty(ICP_Facilities))
                projInfo += "ㆍ시설 : " + ICP_Facilities + "<br/>";
            if (!string.IsNullOrEmpty(list.NumberOfSponsorshipBeneficiaries))
                projInfo += "ㆍ후원 어린이 수 : " + String.Format("{0:##,##0}", Convert.ToInt32(list.NumberOfSponsorshipBeneficiaries)) + "명<br/>";

            projInfo += "<br/>";
            projInfo += "■ 어린이센터 지역 정보<br/>";

            if (!string.IsNullOrEmpty(list.Community_Name))
                projInfo += "ㆍ지역이름 : " + list.Community_Name + "<br/>";
            if (!string.IsNullOrEmpty(list.ClosestMajorCityEnglish))
                projInfo += "ㆍ가장 가까운 도시 : " + list.ClosestMajorCityEnglish + "<br/>";
            if (!string.IsNullOrEmpty(list.Population))
                projInfo += "ㆍ인구 : " + list.Population + "명<br/>";
            if (!string.IsNullOrEmpty(list.PreferredLanguage))
                projInfo += "ㆍ언어 : " + list.PreferredLanguage + "<br/>";
            if (!string.IsNullOrEmpty(list.Climate))
                projInfo += "ㆍ기후 : " + list.Climate + "<br/>";
            if (!string.IsNullOrEmpty(list.Terrain))
                projInfo += "ㆍ지형 : " + list.Terrain + "<br/>";
            if (!string.IsNullOrEmpty(ICP_PrimaryDiet))
                projInfo += "ㆍ주요 음식 : " + ICP_PrimaryDiet + "<br/>";
            if (!string.IsNullOrEmpty(ICP_PrimaryOccupation))
                projInfo += "ㆍ주요 직업 : " + ICP_PrimaryOccupation + "<br/>";
            if (!string.IsNullOrEmpty(list.UnemploymentRate))
                projInfo += "ㆍ실업률 : " + list.UnemploymentRate + "%" + "<br/>";

            projInfo += "ㆍ집 구조<br/>";
            projInfo += "&nbsp;&nbsp;&nbsp;&nbsp;지붕 : " + list.HomeRoof + ", 바닥 : " + list.HomeFloor + ", 벽 : " + list.HomeWall + "<br/>";

            if (!string.IsNullOrEmpty(ICP_UtilitiesOnSite))
                projInfo += "ㆍ공공시설 : " + ICP_UtilitiesOnSite + "<br/>";

            
            if (age < 6)
            {
                foreach (string s in list.CognitiveActivities0To5)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        Cognitive += s + "<br/>";

                foreach (string s in list.PhysicalActivities0To5)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        Physical += s + "<br/>";

                foreach (string s in list.SocioEmotionalActivities0To5)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        SocioEmotional += s + "<br/>";

                foreach (string s in list.SpiritualActivities0To5)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        Spiritual += s + "<br/>";
            }
            else if (age < 12)
            {
                foreach (string s in list.CognitiveActivities6To11)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        Cognitive += s + "<br/>";

                foreach (string s in list.PhysicalActivities6To11)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        Physical += s + "<br/>";

                foreach (string s in list.SocioEmotionalActivities6To11)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        SocioEmotional += s + "<br/>";

                foreach (string s in list.SpiritualActivities6To11)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        Spiritual += s + "<br/>";
            }

            else if (age >= 12)
            {
                foreach (string s in list.CognitiveActivities12Plus)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        Cognitive += s + "<br/>";

                foreach (string s in list.PhysicalActivities12Plus)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        Physical += s + "<br/>";

                foreach (string s in list.SocioEmotionalActivities12Plus)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        SocioEmotional += s + "<br/>";

                foreach (string s in list.SpiritualActivities12Plus)
                    if (!IsEnglish(s.ToCharArray()[0]))
                        Spiritual += s + "<br/>";
            }
        }

        result[0] = projInfo;
        result[1] = Cognitive;
        result[2] = Physical;
        result[3] = SocioEmotional;
        result[4] = Spiritual;
        return result;
    }

    bool IsEnglish(char ch)
    {
        if ((0x61 <= ch && ch <= 0x7A) || (0x41 <= ch && ch <= 0x5A))
            return true;
        else
            return false;
    }
}
