﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;

public partial class SponsorAction {
	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;

	public string[] SponsorInfoCodeList {
		get {
			return new string[] { "ChildInfoSub1", "ChildInfoSub2", "ChildInfoSub3", "ChildInfoSub4", "ChildLetterSub2", "ETCSub3" };
		}
	}

	public SponsorAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
	}

	// 교회정보 조회
	// 후원정보 수신여부  
	public class ChurchResult {
		public string organizationName = "";
		public string organizationId = "";
	}

	public JsonWriter GetChurch() {
		var sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		var sponsorId = sess.SponsorID;
		ChurchResult data = new ChurchResult();

		if(string.IsNullOrEmpty(sponsorId)) {
			result.data = data;
			result.success = true;
			return result;
		}

		try {

			DataTable dtChurch = _wwwService.getOrganizationSponsorList(sponsorId, "Y").Tables[0];

			if(dtChurch.Rows.Count > 0) {
				data.organizationName = dtChurch.Rows[0]["ChurchOrganizationName"].ToString().Trim();
				data.organizationId = dtChurch.Rows[0]["ChurchOrganizationID"].ToString().Trim();
			}

			result.data = data;
			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "교회정보를 읽어오는 중 오류가 발생했습니다";
			return result;
		}
	}

	// 교회정보 수정 , 기독교만 해당
	public JsonWriter UpdateChurch( string sponsorId, string organizationId, string ChurchName ) {
		var sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		if(string.IsNullOrEmpty(ChurchName)) {
			result.success = true;
			return result;
		}

		try {

			DataSet dsChurch = new DataSet();    //후원자교회정보

			#region //tSponsorOrganzation  *****후원자의 교회정보여부 (2009-06-06)
			try {

				dsChurch = _wwwService.ExsitOrganizationSponsor(ChurchName, sponsorId);

				if(dsChurch == null) {
					result.message = "회원님의 교회정보를 로드 중 오류가 발생했습니다. \\r\\n관리자에게 문의해주세요";
					return result;

				}

			} catch(Exception ex) {

				ErrorLog.Write(HttpContext.Current, 0, ex.Message);
				result.message = "회원님의 교회정보를 로드 중 오류가 발생했습니다.\\r\\n관리자에게 문의해주세요.";
				return result;

			}

			#endregion

			if(dsChurch.Tables[0].Rows.Count == 0) {
				//insert

				try {

					if(string.IsNullOrEmpty(organizationId)) {
						DataSet dsChurch2 = _wwwService.ExsitChurchOrganization(ChurchName);

						if(dsChurch2.Tables[0].Rows.Count > 0) {
							organizationId = dsChurch2.Tables[0].Rows[0]["OrganizationID"].ToString();
						}
					}
					var sResult = _wwwService.registerOrganizationSponsor(sponsorId, organizationId, ChurchName,
											  "", "", "", "", sponsorId, sess.UserName);

					if(sResult.Substring(0, 2) == "30") {

						result.message = "회원님의 교회정보를 수정하지 못했습니다";
						return result;

					}

				} catch(Exception ex) {

					ErrorLog.Write(HttpContext.Current, 0, ex.Message);
					result.message = "회원님의 교회정보를 수정 중 오류가 발생했습니다.";
					return result;

				}

			}

			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "후원정보 수신여부를 수정중 오류가 발생했습니다";
			return result;
		}
	}

	// 연말정산 영수증 발급여부 업데이트
	public JsonWriter UpdateReceiptReceive( string sponsorId, bool is_receive ) {
		var sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		Int64 iTimeStamp = Convert.ToInt64(_wwwService.GetDate().ToString("yyyyMMddHHmmssff2"));

		try {

			DataSet dsReceipt = _wwwService.listDATSponsorRefuse(sponsorId, "연말정산영수증", "Y");

			if(is_receive) {      // 신청인경우 거부테이블에 데이타가 있으면 삭제
				if(dsReceipt.Tables[0].Rows.Count > 0) {
					_wwwService.deleteDATSponsorRefuse(sponsorId, dsReceipt.Tables[0].Rows[0]["RefuseID"].ToString());
				}
			} else {
				if(dsReceipt.Tables[0].Rows.Count > 0) {
					_wwwService.modifyDATSponsorRefuse(sponsorId, dsReceipt.Tables[0].Rows[0]["RefuseID"].ToString(), CodeAction.ChannelType, "연말정산영수증", "", null, "Y");
				} else {
					_wwwService.registerDATSponsorRefuse(sponsorId, (iTimeStamp + 1).ToString(), CodeAction.ChannelType, "연말정산영수증", "Y", "", sess.UserId, sess.UserName);
				}

			}



			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "후원정보 수신여부를 수정중 오류가 발생했습니다";
			return result;
		}
	}

	// 후원정보 수신여부
	public JsonWriter GetReceiveSponsorInfo() {
		var sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		var sponsorId = sess.SponsorID;

		if(string.IsNullOrEmpty(sponsorId)) {
			result.message = "후원회원아이디가 없습니다.";
			return result;
		}

		try {

			DataTable dtDMSub = _wwwService.listSponsorDMRefuse(sponsorId).Tables[1];

			bool is_receive = false;
			foreach(string item in SponsorInfoCodeList) {
				if(string.IsNullOrEmpty(item))
					continue;

				DataRow dr = dtDMSub.Select("CodeID='" + item + "'").FirstOrDefault();
				if(dr != null) {
					// 하나라도 수신중이면 수신중으로 표시	
					if(dr["Status"].ToString() == "수신중") {
						is_receive = true;
					} else {
						is_receive = false;
					}
					break;
				}
			}

			result.data = is_receive;
			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "후원정보 수신여부를 읽어오는 중 오류가 발생했습니다";
			return result;
		}
	}

	// 후원정보 수신여부 업데이트
	public JsonWriter UpdateReceiveSponsorInfo( string sponsorId, bool is_receive ) {
		var sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		try {

			string xCommunicationType = "DM";
			//저장용 데이터 셋 구성
			DataSet dsReuse_Data = new DataSet();
			DataSet dsReuse_DataCancel = new DataSet();
			DataTable dtRefuseInsert = new DataTable("dtRefuseInsert");
			dtRefuseInsert.Columns.Add("CommunicationType", typeof(string));
			dtRefuseInsert.Columns.Add("RefuseType", typeof(string));
			dtRefuseInsert.Columns.Add("DateFrom", typeof(string));
			dtRefuseInsert.Columns.Add("DateTo", typeof(string));
			dtRefuseInsert.Columns.Add("RefuseID", typeof(string));
			DataTable dtRefuseCancel = dtRefuseInsert.Copy();

			// 기존 수신 거부 목록 조회 
			DataSet dsDMRefuse = _wwwService.listSponsorDMRefuse(sponsorId);
			DataTable dtDMSub = dsDMRefuse.Tables[1];
			foreach(string item in SponsorInfoCodeList) {
				if(string.IsNullOrEmpty(item))
					continue;

				string xStartDate = null;
				string xClosingDate = null;
				string xRefuseID = null;
				string xCodeID = item;

				DataRow dr = dtDMSub.Select("CodeID='" + xCodeID + "'").FirstOrDefault();

				if(is_receive) {
					if(dr != null && dr["Status"].ToString() != "수신중") {
						xRefuseID = dr["RefuseID"].ToString();
						dtRefuseCancel.Rows.Add(xCommunicationType, xCodeID, xStartDate, xClosingDate, xRefuseID);
					}
				} else {
					if(dr != null && dr["Status"].ToString() == "수신중")
						dtRefuseInsert.Rows.Add(xCommunicationType, xCodeID, xStartDate, xClosingDate, xRefuseID);
				}
			}

			dsReuse_Data.Tables.Add(dtRefuseInsert);
			dsReuse_DataCancel.Tables.Add(dtRefuseCancel);

			if(dsReuse_Data.Tables[0].Rows.Count > 0) {
				_wwwService.deleteSponsorRefuseRegister_Combine(sponsorId, sponsorId, sess.UserName, dsReuse_Data);
			}

			if(dtRefuseCancel.Rows.Count > 0) {
				_wwwService.deleteSponsorRefuseCancel_New(sponsorId, sponsorId, sess.UserName, dsReuse_DataCancel);
			}


			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "후원정보 수신여부를 수정중 오류가 발생했습니다";
			return result;
		}
	}


	public class AddressResult {
		public string AddressType;
		public string Country;
		public string Zipcode;
		public string Addr1;
		public string Addr2;
		public bool ExistAddress;

        public string DspAddrJibun = "";
        public string DspAddrDoro = "";
	}

	public JsonWriter GetAddress() {
		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();
		return this.GetAddress(sess.UserId, sess.SponsorID, sess.ManageType, sess.LocationType);

	}

	public JsonWriter GetAddress( string userId, string sponsorId, string manageType, string location) {
		
		JsonWriter result = new JsonWriter() { success = false };
        DataSet dsSponsorMaster = new DataSet();
        DataSet dsAddress = new DataSet();
        DataSet dsMailing = new DataSet();
		
		string rtnAddressType = "수령지";
		string rtnCountry = "";
		string rtnZipcode = "";
		string rtnAddr1 = "";
		string rtnAddr2 = "";
		bool rtnExistAddress = false;       // 기존에 등록된 주소가 있는지

		if(string.IsNullOrEmpty(sponsorId)) {

            using (AuthLibDataContext dao = new AuthLibDataContext())
            {
                ErrorLog.Write(HttpContext.Current, 0, "Login GetAddress sp_tSponsorMaster_get_f Start : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                //var list = dao.sp_tSponsorMaster_get_f(userId, "", "Y", "", "", "", "").ToList();
                Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                Object[] op2 = new Object[] { userId, "", "Y", "", "", "", "" };
                var list = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();


                ErrorLog.Write(HttpContext.Current, 0, "Login GetAddress sp_tSponsorMaster_get_f End : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var user = list.First();

                var str_addr1 = user.Addr1.EmptyIfNull().Split('$')[0];
                var str_location = user.Addr1.EmptyIfNull().Split('$').Length > 1 ? user.Addr1.EmptyIfNull().Split('$')[1] : "한국";


                result.data = new AddressResult()
                {
                    Country = (user.LocationType == "국내" ? "한국" : str_location),
                    Zipcode = user.ZipCode,
                    Addr1 = str_addr1,
                    Addr2 = user.Addr2,
                    AddressType = "수령지",
                    ExistAddress = !string.IsNullOrEmpty(user.ZipCode)
                };

                result.success = true;
                return result;
            }
		}


		#region SponsorAddress Select
		try {

            Object[] objSqlTemp = new object[1] { string.Format(" SELECT top 1 ManageType from tSponsorMaster with(nolock) where sponsorid='{0}' and currentUse='Y' ", sponsorId) };
            dsSponsorMaster = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSqlTemp, "Text", null, null);

            ErrorLog.Write(HttpContext.Current, 0, "Login GetAddress _www6Service Start : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            //dsAddress = _wwwService.getSponsorAddress(sponsorId, "", "", "Y");
            Object[] objSql = new object[1] { string.Format(" SELECT top 1 SponsorID, AddressID, AddressType, CountryCode, ZipCode, Address1, Address2, ReceiverName, CurrentUse, MainFlag, RegisterID, RegisterName, RegisterDate, ModifyID, ModifyName, ModifyDate, AddressGubun from tsponsorAddress with(nolock) where sponsorid='{0}' and currentUse='Y' order by registerDate desc ", sponsorId) };
            dsAddress = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
            ErrorLog.Write(HttpContext.Current, 0, "Login GetAddress _www6Service End : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            ErrorLog.Write(HttpContext.Current, 0, "Login GetAddress listDATSponsorMailing Start : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            dsMailing = _wwwService.listDATSponsorMailing(sponsorId, "어린이편지", "");
            ErrorLog.Write(HttpContext.Current, 0, "Login GetAddress listDATSponsorMailing End : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

        } catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, "sponsorId > '" + sponsorId + "'" + ex.ToString());
			result.message = "회원님의 주소정보를 읽어오는 중 오류가 발생했습니다";
			return result;
		}

        //if(dsAddress == null || dsAddress.Tables.Count < 1 || dsAddress.Tables[0].Rows.Count < 1) {
        if(dsAddress == null) { 
            result.message = "회원님의 주소정보를 가져오지 못했습니다.\\r\\n관리자에게 문의해주세요.";
			return result;
		}
        #endregion

        // 스폰서 메일링에 어린이 편지가 있는 회원은 메일링의 수신주소로 조회
        string strManageType = "";
        if (dsSponsorMaster.Tables[0].Rows.Count > 0)
        {
            strManageType = dsSponsorMaster.Tables[0].Rows[0]["ManageType"].ToString().EmptyIfNull();
        }

        if (strManageType.Equals("") || strManageType.Equals("0"))
        {
            if (dsAddress.Tables[0].Rows.Count > 0)
            {
                rtnAddressType = "수령지";
                rtnExistAddress = true;
            }
        }
        else {
            if (dsMailing.Tables[0].Rows.Count > 0)
            {
                rtnAddressType = dsMailing.Tables["tSponsorMailing"].Rows[0]["AddressType"].ToString();
                rtnExistAddress = true;
            }
            else
            {
                // 없는 경우는 tSponsorAddress 의 addressType 으로 조회
                if (dsAddress.Tables[0].Rows.Count > 0)
                {
                    rtnAddressType = dsAddress.Tables[0].Rows[0]["AddressType"].ToString();
                    rtnExistAddress = true;
                }
                else
                {
                    rtnExistAddress = false;
                }
            }
        }
        

        string rtnDspAddr1 = "", rtnDspAddr2 = "";

        if(rtnExistAddress) {
            if(location == "국내") {
            } else {
                rtnCountry = dsAddress.Tables[0].Rows[0]["CountryCode"].ToString();
            }

			if (dsAddress.Tables[0].Rows.Count > 0) {
				rtnZipcode = dsAddress.Tables[0].Rows[0]["ZipCode"].ToString(); //우편번호
				rtnAddr1 = dsAddress.Tables[0].Rows[0]["Address1"].ToString(); //집주소 동
				rtnAddr2 = dsAddress.Tables[0].Rows[0]["Address2"].ToString(); //집주소 번지
			}


            // addressGubun 2 = 지번 , 3= 도로명
            //Object[] objSql = new object[1] { " SELECT addressGubun , address1 , address2 + ' ' + reference as addr2 FROM tSponsorAddressAll with(nolock) WHERE SponsorID = '" + sponsorId + "' AND currentUse='Y' and addressType='" + rtnAddressType + "' " };
            ErrorLog.Write(HttpContext.Current, 0, "Login GetAddress _www6Service2 Start : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            Object[] objSql = new object[1] { string.Format(" SELECT addressGubun , address1 , address2 + ' ' + reference as addr2 FROM tSponsorAddressAll with(nolock) WHERE sponsorId = '{0}' and addressType='{1}' and addressId = (select top 1 addressId from tSponsorAddressAll with(nolock)  where SponsorID = '{0}' AND currentUse='Y' and addressType='{1}') ", sponsorId, rtnAddressType) };
            DataSet dsAddr = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
            ErrorLog.Write(HttpContext.Current, 0, "Login GetAddress _www6Service2 End : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            if (dsAddr != null && dsAddr.Tables.Count > 0) {
                foreach(DataRow dr in dsAddr.Tables[0].Rows) {
                    if(dr["addressGubun"].ToString() == "2") {
                        rtnDspAddr1 = (dr["address1"].ToString() + " " + dr["addr2"].ToString()).Trim();
                    } else if(dr["addressGubun"].ToString() == "3") {
                        rtnDspAddr2 = (dr["address1"].ToString() + " " + dr["addr2"].ToString()).Trim();
                    }
                }
            }

        }


        #region //주소수신동의->우편물 수신으로 수정됨 *****2009-06-06

        //string[] usedAddresType = new string[2];
        //// 다주소 후원자만  우편물 수신 쪽 사용
        //if(manageType == "1" || manageType == "2") {

        //	//수정 2013-09-26  히든값 설정

        //	// 현재 사용중은 주소타입 2개만 체크 (2개 이상 쓰는사용자 없다고함)
        //	foreach(DataRow dr in dsMailing.Tables["tSponsorMailing"].Rows) {
        //		if(string.IsNullOrEmpty(usedAddresType[0])) {
        //			usedAddresType[0] = dr["AddressType"].ToString();
        //		} else {
        //			string type = dr["AddressType"].ToString();
        //			if(type != usedAddresType[0]) {
        //				usedAddresType[1] = type;
        //				break;
        //			}
        //		}
        //	}

        //	if(string.IsNullOrEmpty(usedAddresType[1])) {
        //		if(dsAddress != null && dsAddress.Tables.Count > 0) {
        //			DataTable t = dsAddress.Tables[0];
        //			DataRow[] drs = t.Select("AddressType <> '" + usedAddresType[0] + "'").ToArray();

        //			if(drs != null && drs.Length > 0)
        //				usedAddresType[1] = drs[0]["AddressType"].ToString();
        //			else {
        //				if(usedAddresType[0] == "집")
        //					usedAddresType[1] = "직장";
        //				else
        //					usedAddresType[1] = "집";
        //			}
        //		}
        //	}


        //}

        #endregion

        #region //후원자 주소정보

        //for(int i = dsAddress.Tables[0].Rows.Count - 1; i >= 0; i--) {
        //	// 다주소 후원자의 경우 기존 방식
        //	var isUse = false;
        //	if(manageType == "1" || manageType == "2") {
        //		//집
        //		if(dsAddress.Tables[0].Rows[i]["AddressType"].ToString() == usedAddresType[0] && dsAddress.Tables[0].Rows[i]["CurrentUse"].ToString() != "N") {
        //			isUse = true;
        //			rtnAddressType = usedAddresType[0];
        //			//직장
        //		} else if(dsAddress.Tables[0].Rows[i]["AddressType"].ToString() == usedAddresType[1] && dsAddress.Tables[0].Rows[i]["CurrentUse"].ToString() != "N") {
        //			isUse = true;
        //			rtnAddressType = usedAddresType[1];
        //		}

        //		// 단일화 후원자의 경우 집 주소 만 보여주고 수령지로 표시
        //	} else {
        //		isUse = true;
        //	}

        //	if(isUse) {
        //		rtnExistAddress = true;
        //		if(location == "국내") {
        //		} else {
        //			rtnCountry = dsAddress.Tables[0].Rows[i]["CountryCode"].ToString();
        //		}

        //		rtnZipcode = dsAddress.Tables[0].Rows[i]["ZipCode"].ToString(); //우편번호
        //		rtnAddr1 = dsAddress.Tables[0].Rows[i]["Address1"].ToString(); //집주소 동
        //		rtnAddr2 = dsAddress.Tables[0].Rows[i]["Address2"].ToString(); //집주소 번지
        //	}

        //}

        //if(dsAddress.Tables[0].Rows.Count < 1) {

        //	rtnExistAddress = false;

        //}



        #endregion

        result.data = new AddressResult() {
			AddressType = rtnAddressType,
			Addr1 = rtnAddr1,
			Addr2 = rtnAddr2,
			Country = rtnCountry,
			ExistAddress = rtnExistAddress,
			Zipcode = rtnZipcode ,
            DspAddrJibun = rtnDspAddr1,
            DspAddrDoro = rtnDspAddr2,
        };
		result.success = true;
		return result;
	}

	// 국내/국외 정보 변경
	public JsonWriter UpdateLocation( string location ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

		var sponsorId = sess.SponsorID;

        using (AuthLibDataContext dao = new AuthLibDataContext())
        {
            //var user = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
            var user = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);
            user.LocationType = location;

            //dao.SubmitChanges();
            www6.updateAuth(user);
        }

		if(!string.IsNullOrEmpty(sponsorId)) {

			try {
				Object[] objSql = new object[1] { "sp_web_tSponsorMaster_update_f" };
				Object[] objParam = new object[] { "sponsorId", "LocationType" };
				Object[] objValue = new object[] { sess.SponsorID, location };
				_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
			} catch(Exception ex) {
				ErrorLog.Write(HttpContext.Current, 0, ex.Message);
				result.message = "회원님의 거주구분을 수정 중 오류가 발생했습니다.";
				return result;
			}
		}

		result.success = true;
		return result;
	}

	// location : 국내 , 국외
	public JsonWriter UpdateAddress( bool allowUpdate, string addressType, string location, string country, string zipcode, string addr1, string addr2 ) {

		var sess = new UserInfo();
		return this.UpdateAddress(allowUpdate, sess.UserId , sess.SponsorID , sess.UserName, addressType, location, country, zipcode, addr1, addr2);
	}

	public JsonWriter UpdateAddress( bool allowUpdate, string userId , string sponsorId, string sponsorName , string addressType, string location, string country, string zipcode, string addr1, string addr2 ) {
		
		JsonWriter result = new JsonWriter() { success = false };

		if(!string.IsNullOrEmpty(sponsorId)) {
			 
			Int64 iTimeStamp = Convert.ToInt64(_wwwService.GetDate().ToString("yyyyMMddHHmmssff2"));

			#region 주소 수정

			string sResult;
			bool houseAddrState = false;

			if(location == "국내")
				country = "한국";

			#region tsponsormaster 의 locationType 변경
			try {
				Object[] objSql = new object[1] { "sp_web_tSponsorMaster_update_f" };
				Object[] objParam = new object[] { "sponsorId", "LocationType" };
				Object[] objValue = new object[] { sponsorId, location };
				_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
			} catch(Exception ex) {
				ErrorLog.Write(HttpContext.Current, 0, ex.Message);
				result.message = "회원님의 거주구분을 수정 중 오류가 발생했습니다.";
				return result;
			}

			#endregion

			#region //SponsorAddress Select : 이미 등록된 수령지1 주소가 있는지 확인
			DataSet dsZipCode = new DataSet();

			//select SponsorAddress
			try {
				dsZipCode = _wwwService.getSponsorAddress(sponsorId, "", addressType, "Y");

				if(dsZipCode == null) {
					result.message = "회원님의 수령지 주소를 가져오지 못했습니다. \\r\\n관리자에게 문의해주세요.";
					return result;
				}

			} catch(Exception ex) {
				ErrorLog.Write(HttpContext.Current, 0, ex.Message);
				result.message = "회원님의 수령지 주소를 확인 중 오류가 발생했습니다.";
				return result;
			}

			#endregion End SponsorAddress Select

			#region 수령지 수정
			//이미등록된 주소가 있을경우
			if(dsZipCode.Tables[0].Rows.Count > 0) {

				DataRow dr = dsZipCode.Tables[0].Rows[0];
				if(allowUpdate) {
					
					//입력한 집주소와 사용중인 집주소가 같지 않으면 사용중인 집주소 사용여부 N으로 수정 후 새로 주소등록
					if (country != dr["CountryCode"].ToString() || zipcode != dr["ZipCode"].ToString() || addr1 != dr["Address1"].ToString() || addr2 != dr["Address2"].ToString()) {

						// 한국인데 주소 수정 후 session 에 값이 없으면 예외오루
						if (HttpContext.Current.Session["dsHouseAddress"] == null && country == "한국") {
							ErrorLog.Write(HttpContext.Current, 0, "dsHouseAddress null");
							result.message = "회원님의 수령지주소를 수정 중 오류가 발생했습니다. 주소 검색을 다시해 주세요";
							return result;
						}

						#region //이미 등록된 주소가 있고, 주소를 수정했으면 사용여부 N으로 수정


						string sAddressID = dr["AddressID"].ToString(); //주소ID
						try {
							sResult = _wwwService.modifyDATSponsorAddress(sponsorId, sAddressID, null, null, null, null,
																			null, null, null, "N", sponsorId, sponsorName);
						} catch(Exception ex) {
							ErrorLog.Write(HttpContext.Current, 0, ex.Message);
							result.message = "회원님의 집주소를 수정 중 오류가 발생했습니다.";
							return result;
						}
						if(sResult.Substring(0, 2) == "30") {
							result.message = "회원님의 집주소를 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
							return result;
						}

						#endregion

						ErrorLog.Write(HttpContext.Current, 0, "주소수정 , country =" + country + "(" + dr["CountryCode"].ToString() + ")" + " ,addr1=" + addr1 + "(" + dr["Address1"].ToString() + ") , addr2=" + addr2 + "(" + dr["Address2"].ToString() + ")");
						#region //Insert 수령지
						//Insert
						try {
							String sAddressGubun = "0";
							String Reference = "";
							String BuildingCode = "";
							String ResultCode = "";

							String[] sAddressParam = { /*zipcode, addr1, addr2, "0",*/
												"", "", "", "2",
												"", "", "", "3" };
							
							// 새 주소 검증을 탔을 시
							if(HttpContext.Current.Session["dsHouseAddress"] != null) {
								DataSet dsAddress = (DataSet)HttpContext.Current.Session["dsHouseAddress"];
								dr = dsAddress.Tables[0].Rows[0];
								sAddressGubun = dr["AddressUsage"].ToString();

								sAddressParam = new string[] { /*dr["inputZip"].ToString(), dr["inputAddr1"].ToString(), dr["inputAddr2"].ToString(), dr["inputCode"].ToString(),*/
														dr["jibunZip"].ToString(), dr["jibunAddr1"].ToString(), dr["jibunAddr2"].ToString(), dr["jibunCode"].ToString(),
														dr["roadZip"].ToString(), dr["roadAddr1"].ToString(), dr["roadAddr2"].ToString(), dr["roadCode"].ToString() };

								Reference = dr["Reference"].ToString().Replace(" " , "");
								BuildingCode = dr["NNMZ"].ToString();
								ResultCode = dr["MSG"].ToString();
							}

							/*
                            var adr1 = addr1;
                            if (adr1.IndexOf("//") > -1) {
                                adr1 = adr1.Substring(0, adr1.IndexOf("//"));
                            }
							*/
							var adr1 = sAddressParam[5];
							var adr2 = sAddressParam[6];
							if (country != "한국") {
								adr1 = addr1;
								adr2 = addr2;
								Reference = "";
								sAddressParam = new string[]{ /*zipcode, addr1, addr2, "0",*/
									"", "", "", "2",
											"", "", "", "3" };
							}

							sResult = _wwwService.registerDATSponsorAddress_New(sponsorId, iTimeStamp.ToString(), addressType,
																				country, zipcode, adr1 , adr2 + " " + Reference,
																				"", sponsorName, "Y", sponsorId, sponsorName, sAddressGubun);

                        //    ErrorLog.Write(HttpContext.Current, 0, "registerDATSponsorAddress_New" + sResult);

                            if (sResult.Substring(0, 2) != "30") {
								 
                                //sResult = _wwwService.registerSponsorAddressAll(sponsorId, iTimeStamp.ToString(), addressType, sAddressParam, Reference, BuildingCode, sAddressGubun, ResultCode, "1", sponsorId, sponsorName);
                                // 기존 사용 취소
                                {
                                    Object[] objSql = new object[1] { string.Format("update tSponsorAddressAll set CurrentUse = 'N' where SponsorID  = '{0}' and AddressType = '{1}' and CurrentUse  = 'Y' ", sponsorId, addressType) };
                                    _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
                                }
                                {
                                    // 지번
                                    Object[] objSql = new object[1] {
                                        string.Format(@"INSERT INTO dbo.tSponsorAddressAll (
[SponsorID],[AddressID],[AddressType],[ZipCode],[Address1],[Address2],[AddressGubun],[Reference],[BuildingCode],[AddressUsage],
[ResultCode],[ChannelType],[CurrentUse], [RegisterID],[RegisterName],[RegisterDate]) 
values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}',getdate())"
, sponsorId , iTimeStamp.ToString() ,addressType , sAddressParam[0] , sAddressParam[1] , sAddressParam[2] , sAddressParam[3] , "" , BuildingCode ,  "N" , "" , "1", "N" , sponsorId , sponsorName   ) };

                                    _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
                                }

                                {
                                    // 도로명
                                    Object[] objSql = new object[1] {
                                        string.Format(@"INSERT INTO dbo.tSponsorAddressAll (
[SponsorID],[AddressID],[AddressType],[ZipCode],[Address1],[Address2],[AddressGubun],[Reference],[BuildingCode],[AddressUsage],
[ResultCode],[ChannelType],[CurrentUse], [RegisterID],[RegisterName],[RegisterDate]) 
values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}',getdate())"
, sponsorId , iTimeStamp.ToString() ,addressType , sAddressParam[4] , sAddressParam[5] , sAddressParam[6] , sAddressParam[7] , Reference , BuildingCode ,  "Y" , "" , "1" , "Y" , sponsorId , sponsorName ) };
                                    
                                    _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
                                }

                            }

							//추가 2013-09-05  등록여부 확인
							if(sResult == "10")
								houseAddrState = true;


						} catch(Exception ex) {

							ErrorLog.Write(HttpContext.Current, 0, ex.Message);
							result.message = "회원님의 수령지주소를 수정 중 오류가 발생했습니다.";
							return result;

						}

						if(sResult.Substring(0, 2) == "30") {
							result.message = "회원님의 수령지주소를 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
							return result;
						}
						#endregion End Insert
					}


					#region 거주구분 변경 및 이력 저장
					DataSet ds2 = _wwwService.listSponsor(sponsorId, "", "", "", "");
					string newLoc = location;
					string oldLoc = ds2.Tables[0].Rows[0]["LocationType"].ToString();

					if(newLoc != oldLoc) {
						sResult = _wwwService.UpdateLocationType(sponsorId, newLoc);
						// 거주구분 변경 이력 저장
						_wwwService.InsertLocationChangeHistory(iTimeStamp.ToString(), sponsorId, iTimeStamp.ToString(), newLoc, oldLoc, sponsorId, sponsorName);
					}
					#endregion

					#region 반송처리 (발송중지일 경우 해당 주소에 맞게 수신중으로 풀어줌)

					string sendStop = string.Empty;

					//수정 2013-09-05

					if(houseAddrState == true) {
						#region //주소를 수정했으면 처리
						try {
							sendStop = _wwwService.SetReturnSendStop("CHANGEADDRESS", sponsorId, "", addressType);
						} catch(Exception ex) {

							ErrorLog.Write(HttpContext.Current, 0, ex.Message);

						}
						if(sendStop.Substring(0, 2) == "30") {

						}
						#endregion
					}

					#endregion

				}

			} else {

				#region //Insert 수령지
				//Insert
				try {
					String sAddressGubun = "0";
					String Reference = "";
					String BuildingCode = "";
					String ResultCode = "";

					String[] sAddressParam = { /*zipcode, addr1, addr2, "0",*/
											"", "", "", "2",
											"", "", "", "3" };

					// 한국인데 주소 수정 후 session 에 값이 없으면 예외오루
					if (HttpContext.Current.Session["dsHouseAddress"] == null && country == "한국") {
						ErrorLog.Write(HttpContext.Current, 0, "dsHouseAddress null");
						result.message = "회원님의 수령지주소를 수정 중 오류가 발생했습니다. 주소 검색을 다시해 주세요";
						return result;
					}

					// 새 주소 검증을 탔을 시
					if (HttpContext.Current.Session["dsHouseAddress"] != null) {
						DataSet dsAddress = (DataSet)HttpContext.Current.Session["dsHouseAddress"];
						DataRow dr = dsAddress.Tables[0].Rows[0];

						sAddressGubun = dr["AddressUsage"].ToString();

						sAddressParam = new string[] {/* dr["inputZip"].ToString(), dr["inputAddr1"].ToString(), dr["inputAddr2"].ToString(), dr["inputCode"].ToString(),*/
													dr["jibunZip"].ToString(), dr["jibunAddr1"].ToString(), dr["jibunAddr2"].ToString(), dr["jibunCode"].ToString(),
													dr["roadZip"].ToString(), dr["roadAddr1"].ToString(), dr["roadAddr2"].ToString(), dr["roadCode"].ToString() };

                        Reference = dr["Reference"].ToString().Replace(" ", "");
                        BuildingCode = dr["NNMZ"].ToString();
						ResultCode = dr["MSG"].ToString();
					}

                   /*
                    var adr1 = addr1;
                    if(adr1.IndexOf("//") > -1) {
                        adr1 = adr1.Substring(0, adr1.IndexOf("//"));
                    }
					*/

					var adr1 = sAddressParam[5];
					var adr2 = sAddressParam[6];
					if (country != "한국") {
						adr1 = addr1;
						adr2 = addr2;
						Reference = "";
						sAddressParam = new string[]{ /*zipcode, addr1, addr2, "0",*/
									"", "", "", "2",
											"", "", "", "3" };
					}

					sResult = _wwwService.registerDATSponsorAddress_New(sponsorId, iTimeStamp.ToString(), addressType,
																		country, zipcode, adr1, adr2 + " " + Reference,
																		"", sponsorName, "Y", sponsorId, sponsorName, sAddressGubun);

					if(sResult.Substring(0, 2) != "30")
                                //sResult = _wwwService.registerSponsorAddressAll(sponsorId, iTimeStamp.ToString(), addressType, sAddressParam, Reference, BuildingCode, sAddressGubun, ResultCode, "1", sponsorId, sponsorName);

                                // 기존 사용 취소
                                {
                        Object[] objSql = new object[1] { string.Format("update tSponsorAddressAll set CurrentUse = 'N' where SponsorID  = '{0}' and AddressType = '{1}' and CurrentUse  = 'Y' ", sponsorId, addressType) };
                        _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
                    }
                    {
                        // 지번
                        Object[] objSql = new object[1] {
                                        string.Format(@"INSERT INTO dbo.tSponsorAddressAll (
[SponsorID],[AddressID],[AddressType],[ZipCode],[Address1],[Address2],[AddressGubun],[Reference],[BuildingCode],[AddressUsage],
[ResultCode],[ChannelType],[CurrentUse], [RegisterID],[RegisterName],[RegisterDate]) 
values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}',getdate())"
, sponsorId , iTimeStamp.ToString() ,addressType , sAddressParam[0] , sAddressParam[1] , sAddressParam[2] , sAddressParam[3] , "" , BuildingCode ,  "N" , "" , "1", "N" , sponsorId , sponsorName   ) };

                        _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
                    }

                    {
                        // 도로명
                        Object[] objSql = new object[1] {
                                        string.Format(@"INSERT INTO dbo.tSponsorAddressAll (
[SponsorID],[AddressID],[AddressType],[ZipCode],[Address1],[Address2],[AddressGubun],[Reference],[BuildingCode],[AddressUsage],
[ResultCode],[ChannelType],[CurrentUse], [RegisterID],[RegisterName],[RegisterDate]) 
values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}',getdate())"
, sponsorId , iTimeStamp.ToString() ,addressType , sAddressParam[4] , sAddressParam[5] , sAddressParam[6] , sAddressParam[7] , Reference , BuildingCode ,  "Y" , "" , "1" , "Y" , sponsorId , sponsorName ) };

                        _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
                    }

                    //추가 2013-09-05  등록여부 확인
                    if (sResult == "10")
						houseAddrState = true;
				} catch(Exception ex) {
					ErrorLog.Write(HttpContext.Current, 0, ex.Message);
					result.message = "회원님의 집주소를 수정 중 오류가 발생했습니다.";
					return result;
				}

				if(sResult.Substring(0, 2) == "30") {
					result.message = "회원님의 집주소를 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
					return result;
				}
				#endregion End Insert
			}


			#endregion

			#endregion

		}

        #region 웹주소수정

        using (AuthLibDataContext dao = new AuthLibDataContext())
        {
            //var user = dao.tSponsorMaster.First(p => p.UserID == userId);
            var user = www6.selectQFAuth<tSponsorMaster>("UserID", userId); 

            //user.Addr1 = addr1.Encrypt();
            user.Addr1 = (addr1 + "$" + country).Encrypt();
            user.Addr2 = addr2.Encrypt();
            user.ZipCode = zipcode.Encrypt();
            user.LocationType = location;

            //dao.SubmitChanges();
            www6.updateAuth(user);
        }
		#endregion

		result.success = true;
		return result;
	}

	public class CommunicationResult {
		public string Email;
		public string Mobile;
		public string Phone;
	}

	public JsonWriter GetCommunications() {

		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		UserInfo sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {

            using (AuthLibDataContext dao = new AuthLibDataContext())
            {
                //var u = dao.sp_tSponsorMaster_get_f(sess.UserId, "", "", "", "", "", "").First();
                Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                Object[] op2 = new Object[] { sess.UserId, "", "", "", "", "", "" };
                var list = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();
                var u = list.First();

                result.data = new CommunicationResult()
                {
                    Email = u.Email,
                    Mobile = u.Mobile,
                    Phone = u.Phone
                };

            }
			
			result.success = true;
			return result;
		}

		return this.GetCommunications(sess.SponsorID);
	}

	public JsonWriter GetCommunications(string sponsorId) {
        JsonWriter result = new JsonWriter() { success = false };
		DataSet dsCommunication = new DataSet();
		
		string rtnEmail = "", rtnMobile = "", rtnPhone = "";

		#region //SponsorAddress Select
		try {
			dsCommunication = _wwwService.getSponsorCommunication(sponsorId, "", "", "Y");
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "회원님의 연락처정보를 읽어오는 중 오류가 발생했습니다";
			return result;
		}

		if(dsCommunication == null) {
			result.message = "회원님의 연락처정보를 가져오지 못했습니다.\\r\\n관리자에게 문의해주세요.";
			return result;
		}
		#endregion

		#region //후원자 연락처
		
		//-- 후원자연락처(이메일,전화번호,휴대번호)
		for(int i = dsCommunication.Tables[0].Rows.Count - 1; i >= 0; i--) {
			DataRow dr = dsCommunication.Tables[0].Rows[i];
			if(dr["CommunicationType"].ToString() == "이메일" && dr["CommunicationContents"].ToString().IndexOf("@") > 0) {
				rtnEmail = dr["CommunicationContents"].ToString();
			} else if(dr["CommunicationType"].ToString() == "휴대번호") {
				rtnMobile = dr["CommunicationContents"].ToString();
			} else if(dr["CommunicationType"].ToString() == "전화번호") {
				rtnPhone = dr["CommunicationContents"].ToString();
			}
		}
		#endregion

		result.data = new CommunicationResult() {
			Email = rtnEmail,
			Mobile = rtnMobile,
			Phone = rtnPhone
		};
		result.success = true;
		return result;
	}

	public JsonWriter UpdateCommunications( bool allowUpdate, string sponsorId, string email, string mobile ) {
		return this.UpdateCommunications(allowUpdate, sponsorId, email, mobile, null);
	}

	public JsonWriter UpdateCommunications( bool allowUpdate, string sponsorId, string email, string mobile, string phone ) {

		var sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		if(string.IsNullOrEmpty(sponsorId)) {
			result.message = "후원회원아이디가 없습니다.";
			return result;
		}

		var sResult = "";
		Int64 iTimeStamp = Convert.ToInt64(_wwwService.GetDate().ToString("yyyyMMddHHmmssff2"));

		if(email != null) {
			#region 이메일
			DataSet demail = new DataSet();

			try {
				demail = _wwwService.getSponsorCommunication(sponsorId, "", "이메일", "Y");

				if(demail == null) {
					result.message = "회원님의 이메일주소를 가져오지 못했습니다. \\r\\n관리자에게 문의해주세요.";
					return result;
				}

			} catch(Exception ex) {
				ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
				result.message = "회원님의 이메일주소를 확인 중 오류가 발생했습니다. ";
				return result;
			}

			//이미 등록된 이메일이 있으면 사용여부 N으로 수정 후 등록
			if(demail.Tables[0].Rows.Count > 0) {
				//입력한 이메일과 사용중인 이메일이 같지 않으면 사용중인 이메일 사용여부 N으로 수정 후 새로 등록
				if(email != demail.Tables[0].Rows[0]["CommunicationContents"].ToString()) {

					if(allowUpdate) {
						#region //Update 사용여부 N

						string sCommunicationID = demail.Tables[0].Rows[0]["CommunicationID"].ToString();

						//이메일 수정
						try {
							sResult = _wwwService.modifyDATSponsorCommunication(sponsorId, sCommunicationID, "이메일", null, "종료", "N", DateTime.Now.ToString("yyyy-MM-dd")); //2009.01.20 박송이 ClosingDate 수정

							if(sResult.Substring(0, 2) == "30") {
								result.message = "회원님의 이메일주소를 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
								return result;
							}

						} catch(Exception ex) {
							ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
							result.message = "회원님의 이메일주소를 수정 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
							return result;
						}

						#endregion

						#region //Insert 이메일
						if(!string.IsNullOrEmpty(email)) {
							try {
								sResult = _wwwService.registerDATSponsorCommunication(sponsorId, iTimeStamp.ToString(), "이메일", email, "수신", "Y", "", sponsorId, sess.UserName);
							} catch(Exception ex) {
								ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
								result.message = "회원님의 이메일을 수정 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
								return result;
							}
							if(sResult.Substring(0, 2) == "30") {
								result.message = "회원님의 이메일을 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
								return result;
							}
						}
						#endregion
					}
				}
			}
			//이미 등록된 이메일이 없으면 등록
			else //if (demail.Tables[0].Rows.Count == 0)
			{
				#region //Insert 이메일

				//string sCommunicationID = iTimeStamp.ToString().Trim();
				if(!string.IsNullOrEmpty(email)) {
					//Insert
					try {
						sResult = _wwwService.registerDATSponsorCommunication(sponsorId, iTimeStamp.ToString(), "이메일", email, "수신", "Y", "", sponsorId, sess.UserName);
					} catch(Exception ex) {
						ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
						result.message = "회원님의 이메일을 수정 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
						return result;
					}
					if(sResult.Substring(0, 2) == "30") {
						result.message = "회원님의 이메일을 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
						return result;
					}
				}
				#endregion
			}
			#endregion
		}

		if(mobile != null) {
			#region 휴대폰
			DataSet dpmobile = new DataSet();

			//select SponsorCommunication
			try {
				dpmobile = _wwwService.getSponsorCommunication(sponsorId, "", "휴대번호", "Y");

				if(dpmobile == null) {
					result.message = "회원님의 휴대전화번호를 가져오지 못했습니다. \\r\\n관리자에게 문의해주세요.";
					return result;
				}

			} catch(Exception ex) {
				ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
				result.message = "회원님의 휴대전화번호를 확인 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
				return result;
			}


			//휴대번호가 없으면
			if(dpmobile.Tables[0].Rows.Count > 0) {

				if(mobile != dpmobile.Tables[0].Rows[0]["CommunicationContents"].ToString()) {
					if(allowUpdate) {
						#region 기존꺼 종료
						string sCommunicationID = dpmobile.Tables[0].Rows[0]["CommunicationID"].ToString(); //연락처ID

						//update SponsorCommunication
						try {
							sResult = _wwwService.modifyDATSponsorCommunication(sponsorId, sCommunicationID, "휴대번호", null, "종료", "N", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); //2009.01.20 박송이 ClosingDate 수정
						} catch(Exception ex) {
							ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
							result.message = "회원님의 휴대전화번호를 수정 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
							return result;
						}
						if(sResult.Substring(0, 2) == "30") {
							result.message = "회원님의 휴대전화번호를 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
							return result;
						}
						#endregion

						if(!string.IsNullOrEmpty(mobile)) {
							#region //Insert 휴대번호
							//Insert
							try {
								sResult = _wwwService.registerDATSponsorCommunication(sponsorId, Convert.ToString(iTimeStamp + 2), "휴대번호",
																					  mobile, "수신", "Y", "", sponsorId, sess.UserName);
								if(sResult.Substring(0, 2) == "30") {

									result.message = "회원님의 휴대전화번호를 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
									return result;
								}

							} catch(Exception ex) {
								ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
								result.message = "회원님의 휴대전화번호를 수정 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
								return result;
							}

							#endregion End Insert
						}
					}

				}

			} else {

				if(!string.IsNullOrEmpty(mobile)) {
					#region //Insert 휴대번호
					//Insert
					try {
						sResult = _wwwService.registerDATSponsorCommunication(sponsorId, Convert.ToString(iTimeStamp + 2), "휴대번호",
																				mobile, "수신", "Y", "", sponsorId, sess.UserName);
						if(sResult.Substring(0, 2) == "30") {

							result.message = "회원님의 휴대전화번호를 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
							return result;
						}

					} catch(Exception ex) {
						ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
						result.message = "회원님의 휴대전화번호를 수정 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
						return result;
					}

					#endregion End Insert
				}
			}
			#endregion
		}

		if(phone != null) {
			#region 집전화
			DataSet dphone = new DataSet();

			//select SponsorCommunication
			try {
				dphone = _wwwService.getSponsorCommunication(sponsorId, "", "전화번호", "Y");

				if(dphone == null) {
					result.message = "회원님의 전화번호를 가져오지 못했습니다. \\r\\n관리자에게 문의해주세요.";
					return result;
				}

			} catch(Exception ex) {
				ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
				result.message = "회원님의 전화번호를 확인 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
				return result;
			}


			//전화번호가 없으면
			if(dphone.Tables[0].Rows.Count > 0) {

				if(phone != dphone.Tables[0].Rows[0]["CommunicationContents"].ToString()) {
					if(allowUpdate) {
						#region 기존꺼 종료
						string sCommunicationID = dphone.Tables[0].Rows[0]["CommunicationID"].ToString(); //연락처ID

						//update SponsorCommunication
						try {
							sResult = _wwwService.modifyDATSponsorCommunication(sponsorId, sCommunicationID, "전화번호", null, "종료", "N", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); //2009.01.20 박송이 ClosingDate 수정
						} catch(Exception ex) {
							ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
							result.message = "회원님의 휴대전화번호를 수정 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
							return result;
						}
						if(sResult.Substring(0, 2) == "30") {
							result.message = "회원님의 휴대전화번호를 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
							return result;
						}
						#endregion
						if(!string.IsNullOrEmpty(phone)) {
							#region //Insert 전화번호
							//Insert
							try {
								sResult = _wwwService.registerDATSponsorCommunication(sponsorId, Convert.ToString(iTimeStamp + 3), "전화번호",
																					  phone, "수신", "Y", "", sponsorId, sess.UserName);
								if(sResult.Substring(0, 2) == "30") {

									result.message = "회원님의 전화번호를 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
									return result;
								}

							} catch(Exception ex) {
								ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
								result.message = "회원님의 전화번호를 수정 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
								return result;
							}

							#endregion End Insert
						}
					}

				}

			} else {
				if(!string.IsNullOrEmpty(phone)) {
					#region //Insert 전화번호
					//Insert
					try {
						sResult = _wwwService.registerDATSponsorCommunication(sponsorId, Convert.ToString(iTimeStamp + 3), "전화번호",
																				phone, "수신", "Y", "", sponsorId, sess.UserName);
						if(sResult.Substring(0, 2) == "30") {

							result.message = "회원님의 전화번호를 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
							return result;
						}

					} catch(Exception ex) {
						ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
						result.message = "회원님의 전화번호를 수정 중 오류가 발생했습니다. \\r\\n" + ex.Message.Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
						return result;
					}

					#endregion End Insert
				}
			}
			#endregion
		}

        #region 웹주소수정

        using (AuthLibDataContext dao = new AuthLibDataContext())
        {
            //var user = dao.tSponsorMaster.FirstOrDefault(p => p.UserID == sess.UserId);
            var user = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);

            if (user != null)
            {
                if (phone != null)
                    user.Phone = phone.Encrypt();
                if (email != null)
                    user.Email = email.Encrypt();
                if (mobile != null)
                    user.Mobile = mobile.Encrypt();
                //dao.SubmitChanges();
                www6.updateAuth(user);
            }
        }
		#endregion

		result.success = true;
		return result;
	}

	// 이메일/SMS 수신동의
	public class AgreeCommunicationResult {
		public bool Email = true;
		public bool SMS = true;
	}

	public JsonWriter GetAgreeCommunications() {
		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();
		return this.GetAgreeCommunications(sess.UserId , sess.SponsorID);

	}

	public JsonWriter GetAgreeCommunications(string userId , string sponsorId) {
		
		JsonWriter result = new JsonWriter() { success = false };
		DataSet dsRefuse = new DataSet();
		AgreeCommunicationResult data = new AgreeCommunicationResult();

		if(string.IsNullOrEmpty(sponsorId)) {

            using (AuthLibDataContext dao = new AuthLibDataContext())
            {
                //var user = dao.tSponsorMaster.First(p => p.UserID == userId);
                var user = www6.selectQFAuth<tSponsorMaster>("UserID", userId);

                result.data = new AgreeCommunicationResult()
                {
                    Email = user.AgreeEmail.HasValue ? user.AgreeEmail.Value : false,
                    SMS = user.AgreePhone.HasValue ? user.AgreePhone.Value : false
                };
                result.success = true;
                return result;
            }
		

		} else { 
			
			try {
				dsRefuse = _wwwService.listDATSponsorRefuse(sponsorId, "", "Y");

				if(dsRefuse == null) {
					result.message = "회원님의 동의정보를 가져오지 못했습니다.\\r\\n관리자에게 문의해주세요.";
					return result;
				}

			} catch(Exception ex) {
				ErrorLog.Write(HttpContext.Current, 0, ex.Message);
				result.message = "회원님의 동의정보를 읽어오는 중 오류가 발생했습니다";
				return result;
			}

			dsRefuse.Tables[0].DefaultView.RowFilter = "CommunicationType = '이메일'";
			var email = dsRefuse.Tables[0].DefaultView.Count == 0;
			dsRefuse.Tables[0].DefaultView.RowFilter = "CommunicationType = 'SMS'";
			var sms = dsRefuse.Tables[0].DefaultView.Count == 0;

			result.data = new AgreeCommunicationResult() {
				Email = email,
				SMS = sms
			};
			result.success = true;
		}

		return result;
	}


	// 이메일/SMS 수신동의
	public JsonWriter UpdateAgreeCommunications( string SponsorID, string SponsorName, bool agree_phone, bool agree_email, string email, string phone ) {

		JsonWriter result = new JsonWriter() { success = false };

		Int64 iTimeStamp = Convert.ToInt64(_wwwService.GetDate().ToString("yyyyMMddHHmmssff2")); //ID : TimeStamp
		string sResult = "";

		#region 수신동의 정보 조회
		DataSet dsRefuse = new DataSet();

		try {
			dsRefuse = _wwwService.listDATSponsorRefuse(SponsorID, "", "");
		} catch(Exception ex) {

			if(dsRefuse == null) {
				result.message = "회원님의 수신동의 정보를 가져오지 못했습니다. \\r\\n관리자에게 문의해주세요.";
				return result;
			}

			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "회원님의 수신동의 정보를 가져오는 중 오류가 발생했습니다.";
			return result;

		}
		#endregion

		#region 이동전화 수신거부
		if(!string.IsNullOrEmpty(phone)) {

			//등록여부확인
			dsRefuse.Tables[0].DefaultView.RowFilter = "CommunicationType = 'SMS' AND RefuseType = '캠페인' AND CurrentUse = 'Y'";
			DataTable dtRefuseMobile = dsRefuse.Tables[0].DefaultView.ToTable();

			if(agree_phone) {
				#region

				//등록되어있으면 삭제
				if(dtRefuseMobile.Rows.Count > 0) {
					string sRefuseID = dtRefuseMobile.Rows[0]["RefuseID"].ToString().Trim();

					try {
						sResult = _wwwService.modifyDATSponsorRefuse(SponsorID, sRefuseID, null, null, null, DateTime.Now.ToString(), "N");

						if(sResult.Substring(0, 2) == "30") {
							result.message = "회원님의 SMS 수신동의를 수정하지 못했습니다.\n" + sResult.Substring(2).ToString().Replace("\n", "");
							return result;
						}

					} catch(Exception ex) {

						ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
						result.message = "회원님의 SMS 수신동의를 수정 중 오류가 발생했습니다";
						return result;

					}
				}
				#endregion
			} else {
				#region

				//등록되어있지 않으면 Insert            
				if(dtRefuseMobile.Rows.Count == 0) {
					string sRefuseID = Convert.ToString(iTimeStamp + 1);

					try {
						sResult = _wwwService.registerDATSponsorRefuse(SponsorID, sRefuseID, CodeAction.ChannelType, "SMS", "Y", "캠페인", SponsorID, SponsorName);

						if(sResult.Substring(0, 2) == "30") {
							result.message = "회원님의 SMS 수신동의를 수정하지 못했습니다.\n" + sResult.Substring(2).ToString().Replace("\n", "");
							return result;
						}

					} catch(Exception ex) {
						ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
						result.message = "회원님의 SMS 수신동의를 수정 중 오류가 발생했습니다";
						return result;
					}

				}
				#endregion
			}

		}
		#endregion

		#region 이메일 수신거부

		//등록여부확인
		dsRefuse.Tables[0].DefaultView.RowFilter = "CommunicationType = '이메일' AND RefuseType = '캠페인' AND CurrentUse = 'Y'";
		DataTable dtRefuseEmail = dsRefuse.Tables[0].DefaultView.ToTable();

		if(agree_email) {
			#region

			//등록(거부)되어있으면 수정
			if(dtRefuseEmail.Rows.Count > 0) {
				string sRefuseID = dtRefuseEmail.Rows[0]["RefuseID"].ToString().Trim();

				try {
					sResult = _wwwService.modifyDATSponsorRefuse(SponsorID, sRefuseID, null, null, null, DateTime.Now.ToString(), "N");

					if(sResult.Substring(0, 2) == "30") {
						result.message = "회원님의 이메일 수신동의를 수정하지 못했습니다.\n" + sResult.Substring(2).ToString().Replace("\n", "");
						return result;
					}

				} catch(Exception ex) {

					ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
					result.message = "회원님의 이메일 수신동의를 수정 중 오류가 발생했습니다.";
					return result;

				}

			}
			#endregion
		} else //if(rbnEmailN.Checked == true)
		  {
			#region

			//등록되어있지 않으면(거부로 등록되어 있지 않은) Insert
			if(dtRefuseEmail.Rows.Count == 0) {
				//string sRefuseID = dtRefuseEmail.Rows[0]["RefuseID"].ToString().Trim();
				string sRefuseID = iTimeStamp.ToString().Trim();

				try {
					sResult = _wwwService.registerDATSponsorRefuse(SponsorID, sRefuseID, CodeAction.ChannelType, "이메일", "Y", "캠페인", SponsorID, SponsorName);

					if(sResult.Substring(0, 2) == "30") {
						result.message = "회원님의 이메일 수신동의를 수정하지 못했습니다.\n" + sResult.Substring(2).ToString().Replace("\n", "");
						return result;

					}
				} catch(Exception ex) {
					ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
					result.message = "회원님의 이메일 수신동의를 수정 중 오류가 발생했습니다.";
					return result;
				}

			}
			#endregion
		}
		#endregion

		#region 이메일수신동의
		DataSet dsSponsorAllow = new DataSet();
		try {
			//(수정 : 2012-01-25 By 주형준)
			dsSponsorAllow = _wwwService.getSponsorAllow(SponsorID, "DM", "ETCSub1"); // 매거진 CodeID

			if(dsSponsorAllow == null) {
				result.message = "회원님의 이메일명세서 수신동의 정보를 가져오지 못했습니다";
				return result;
			}

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "회원님의 이메일명세서 수신동의 정보를 가져오는 중 오류가 발생했습니다";
			return result;
		}


		//등록되어있지 않으면 Insert
		//(수정 : 2012-01-25 By 주형준) 매거진으로 변경
		DataRow[] findRow = dsSponsorAllow.Tables[0].Select("CurrentUse = 'Y'");

		if(findRow.Length == 0) {
			//이메일명세서 수신동의 정보가 등록되어있지 않으면서 체크가 Y일경우 등록
			//이메일명세서 수신동의 정보가 등록되어있지 않으면서 체크가 N일경우 넘어감
			//수정 2013-10-04
			//if(chkReceiveEmailYN.Checked == true) //(rbnEmailY.Checked)
			if(agree_email) {
				//등록 (수정 : 2012-01-25 By 주형준)
				sResult = _wwwService.registerSponsorAllow(SponsorID, "DM", "ETCSub1", SponsorID, SponsorName); // 매거진 CodeID
			}
		} else {
			//이메일명세서 수신동의 정보가 등록되어있으면서 체크가 Y일경우 넘어감
			//이메일명세서 수신동의 정보가 등록되어있으면서 체크가 N일경우 삭제
			//수정 2013-10-04
			//if(chkReceiveEmailYN.Checked == false) //(rbnEmailN.Checked)
			if(!agree_email) {
				//등록 (수정 : 2012-01-25 By 주형준) CurrentUse 'N' 업데이트로 수정됨
				sResult = _wwwService.deleteSponsorAllow(SponsorID, "DM", "ETCSub1"); // 매거진 CodeID
			}
		}

		#endregion


		result.success = true;
		return result;

	}

	// 연말정산영수증 신청여부
	public JsonWriter IsPayReceipt() {

		var sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		string sponsorId = sess.SponsorID;

		if(string.IsNullOrEmpty(sponsorId)) {
			result.message = "후원회원아이디가 없습니다.";
			return result;
		}

		try {
			DataSet dsReceipt = _wwwService.listDATSponsorRefuse(sess.SponsorID, "연말정산영수증", "Y");

			if(dsReceipt.Tables[0].Rows.Count < 1) {
				result.data = true;		// 거부정보가 없으면 신청
			} else {
				result.data = false;
			}
			
			result.success = true;
			return result;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "회원님의 연말정산영수증 신청정보를 조회하는중 오류가 발생했습니다";
			return result;
		}
	}

	// 연말정산영수증 발급
	public JsonWriter UpdatePayReceipt( string sponsorId, bool request ) {

		var sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		if(string.IsNullOrEmpty(sponsorId)) {
			result.message = "후원회원아이디가 없습니다.";
			return result;
		}

		try {
			Int64 iTimeStamp = Convert.ToInt64(_wwwService.GetDate().ToString("yyyyMMddHHmmssff2"));

			DataSet dsReceipt = _wwwService.listDATSponsorRefuse(sponsorId, "연말정산영수증", "Y");

			if(request) {      // 신청인경우 거부테이블에 데이타가 있으면 삭제
				if(dsReceipt.Tables[0].Rows.Count > 0) {
					_wwwService.deleteDATSponsorRefuse(sponsorId, dsReceipt.Tables[0].Rows[0]["RefuseID"].ToString());
				}
			} else {
				if(dsReceipt.Tables[0].Rows.Count > 0) {
					_wwwService.modifyDATSponsorRefuse(sponsorId, dsReceipt.Tables[0].Rows[0]["RefuseID"].ToString(), CodeAction.ChannelType, "연말정산영수증", "", null, "Y");
				} else {
					_wwwService.registerDATSponsorRefuse(sponsorId, (iTimeStamp + 1).ToString(), CodeAction.ChannelType, "연말정산영수증", "Y", "", sess.UserId, sess.UserName);
				}

			}

			result.success = true;
			return result;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "회원님의 연말정산영수증 발급정보를 수정하는중 오류가 발생했습니다";
			return result;
		}
	}

	public class CommitResult {
		public int CDSP_count;
		public int CIVCSP_count;
	}
	// 결연여부
	public JsonWriter GetCommitInfo() {
		UserInfo sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		try {
			int CDSPCnt = 0;
			int CIVCSPCnt = 0;
			
			var actionResult = new ChildAction().MyChildren("", 1, 1);
			if(!actionResult.success) {
				throw new Exception(actionResult.message);
			}

			var myChildren = (List<ChildAction.MyChildItem>)actionResult.data;
			var hasCDSP = myChildren.Count > 0;

			if(hasCDSP) {
				//CDSPCnt = Convert.ToInt32(myChildren[0]["total"]);
				CDSPCnt = myChildren[0].Total;
			}

			DataSet dsCIVCSP = _wwwService.ComplementaryApplySearch(sess.SponsorID);

			if(dsCIVCSP != null) {
				if(dsCIVCSP.Tables[0].Rows.Count > 0) {
					for(int i = 0; i < dsCIVCSP.Tables[0].Rows.Count; i++)
						CIVCSPCnt += Convert.ToInt32(dsCIVCSP.Tables[0].Rows[i]["SponsorAmount"]);
				}
			}

			if(CDSPCnt == 0 && CIVCSPCnt == 0) {
				result.message = "후원자만 신청가능합니다.";
				return result;
			} else {

				result.data = new CommitResult() { CDSP_count = CDSPCnt, CIVCSP_count = CIVCSPCnt };

			}

			result.success = true;
			return result;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "후원정보를 가져오던 도중 오류가 발생하였습니다.";
			return result;
		}
	}

	// 정기결연 갯수
	public JsonWriter HasRegularCommitment() {
		var sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		var sponsorId = sess.SponsorID;

		if(string.IsNullOrEmpty(sponsorId)) {
			result.message = "후원회원아이디가 없습니다.";
			return result;
		}

		try {

			Object[] objSql = new object[1] { " SELECT COUNT(*) AS Cnt FROM tCommitmentMaster WHERE SponsorID = '" + sponsorId + "' AND FundingFrequency <> '1회' AND StopDate IS NULL " };
			DataSet dsSponsor = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

			if(dsSponsor == null) {
				result.message = "회원님의 정보를 가져오지 못했습니다.\\r\\n관리자에게 문의해주세요.";
				return result;
			}

			DataTable dtSponsor = dsSponsor.Tables[0];
			var count = Convert.ToInt32(dtSponsor.Rows[0]["cnt"].ToString());
			result.success = count > 0;
			return result;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "회원님의 정보를 읽어오는 중 오류가 발생했습니다";
			return result;
		}
	}

	// 1:1양육 데이타가 있는지여부 , 어린이편지 번역여부 영역 노출에서 사용
	public JsonWriter HasCDSP() {
		var sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		var sponsorId = sess.SponsorID;

		if(string.IsNullOrEmpty(sponsorId)) {
			result.message = "후원회원 정보가 없습니다.";
			return result;
		}

		try {

			Object[] objSql = new object[1] { "sp_web_commitment_random_get_f" };
			Object[] objParam = new object[] { "sponsorId" };
			Object[] objValue = new object[] { sess.SponsorID};
			DataSet dsSponsor = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
			
			if(dsSponsor == null) {
				result.message = "회원님의 정보를 가져오지 못했습니다.\\r\\n관리자에게 문의해주세요.";
				return result;
			}

			DataTable dtSponsor = dsSponsor.Tables[0];
			//var count = Convert.ToInt32(dtSponsor.Rows[0]["total"].ToString());
			result.data = dtSponsor;
			result.success = true;
			return result;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "회원님의 정보를 읽어오는 중 오류가 발생했습니다";
			return result;
		}
	}

	// 본인인증 완료여부 , action = sync 면 본인인증 창 오픈
	public JsonWriter IsAuthenticated() {
		UserInfo sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		try {

			var sponsorId = "";
            using (AuthLibDataContext dao = new AuthLibDataContext())
            {
                //sponsorId = dao.tSponsorMaster.First(p => p.UserID == sess.UserId).SponsorID;
                sponsorId  = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId).SponsorID;
                //result.success = !string.IsNullOrEmpty(sponsorId);
            }

			if(string.IsNullOrEmpty(sponsorId)) {
				result.message = "후원회원 정보 부족";
				result.action = "sync";
				return result;
			}

			Object[] objSql = new object[1] { "SELECT * FROM tSponsorMaster with(nolock) WHERE CurrentUse = 'Y' and sponsorId = '" + sess.SponsorID + "'" };
			DataSet dsSponsor = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

			if(dsSponsor == null) {
				result.message = "회원님의 정보를 가져오지 못했습니다.\\r\\n관리자에게 문의해주세요.";
				return result;
			}

			DataTable dtSponsor = dsSponsor.Tables[0];
			if(dtSponsor.Rows[0]["UserClass"].ToString().Trim() == null ||
				dtSponsor.Rows[0]["UserClass"].ToString().Trim() == "") {
				result.message = "회원님의 가입유형이 존재하지 않습니다. \\r\\n관리자에게 문의해주세요.";
				return result;
			}

			var ci = dtSponsor.Rows[0]["ci"].ToString().Trim();
			if(string.IsNullOrEmpty(ci)) {

				result.success = false;
				result.action = "sync";
			} else {
				result.success = true;
			}

			result.data = dtSponsor;


			return result;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "회원님의 정보를 읽어오는 중 오류가 발생했습니다";
			return result;
		}
	}

	// 본인인증 처리 ,  gender = M or F
	/*
	1. 컴파스에 CI가 있으면 
		1-1. 싱크전 : 웹디비에도 회원정보가 있으면 이미 가입된 계정이 있다고 메세지 처리 없으면 동기화
		1-2. 싱크후 : CI의 sponsorId 와 회원의 sponsorId 가 일치하면 이미 인증됬다고 메세지 처리 일치하지 않으면 콜센터 문의
	2. 컴파스에 CI가 없으면
		2-1. 싱크전 : 동기화
		2-2. 싱크후 : 인증정보 업데이트 

		*/
	public JsonWriter Sync( string ci, string di, string gender, string jumin ) {

		JsonWriter result = new JsonWriter() { success = false, action = "" };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		UserInfo sess = new UserInfo();

		try {

			var certifyOrgan = "서울신용평가";

			if(string.IsNullOrEmpty(ci) || string.IsNullOrEmpty(di) || string.IsNullOrEmpty(gender)) {
				result.message = "필수정보가 누락되었습니다.";
				return result;
			}

			var sponsorId = "";
			var conId = "";

            using (AuthLibDataContext dao = new AuthLibDataContext())
            {

                //var user = dao.sp_tSponsorMaster_get_f(sess.UserId, "", "", "", "", "", "").First();
                Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                Object[] op2 = new Object[] { sess.UserId, "", "", "", "", "", "" };
                var user = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>().First();

                //if (string.IsNullOrEmpty(user.FirstName) || string.IsNullOrEmpty(user.LastName))
                //{
                //    result.message = "영문성, 영문이름이 필요합니다. 개인정보수정에서 입력해 주시기 바랍니다.";
                //    return result;
                //}

                bool exist_in_compass = false;

                DataSet dsSponsor = _www6Service.NTx_ExecuteQuery("SqlCompass4", new object[1] { "SELECT top 1 * FROM tSponsorMaster WHERE CurrentUse = 'Y' and ci = '" + ci + "'" }, "Text", null, null);
                DataTable dtSponsor = null;
                
                string compass4FirstName = "";
                string compass4LastName = "";

                if (dsSponsor != null && dsSponsor.Tables[0].Rows.Count > 0)
                {
                    exist_in_compass = true;

                    dtSponsor = dsSponsor.Tables[0];
                    sponsorId = dtSponsor.Rows[0]["sponsorId"].ToString();
                    conId = dtSponsor.Rows[0]["conId"].ToString();

                    compass4FirstName = dtSponsor.Rows[0]["FirstName"].ToString().ValueIfNull("");
                    compass4LastName = dtSponsor.Rows[0]["LastName"].ToString().ValueIfNull("");
                }

                if (string.IsNullOrEmpty(sess.SponsorID))
                {

                    #region 1. 싱크전
                    // 1-1. CI있음
                    if (exist_in_compass)
                    {

                        // 웹회원으로 가입되어 있으면
                        var exist = www6.selectQAuth<tSponsorMaster>("SponsorID", sponsorId);
                        //if (dao.tSponsorMaster.Any(p => p.SponsorID == sponsorId))
                        if(exist.Any())
                        {
                            var userId = dtSponsor.Rows[0]["userid"].ToString().Mask("*", 3);
                            //result.message = string.Format("이미 인증된 회원정보가 있습니다. (아이디 : {0}). \\n해당 회원으로 로그인 후에 진행해 주십시요.", userId);
                            result.action = "login";
                            result.data = new Dictionary<string, object>() { { "user_id", userId } };
                            result.success = true;
                            return result;
                        }
                        else
                        {

                            // 후원회원이면 동기화
                            // 웹과 컴파스 회원데이타 연결

                            string tempFirstName = compass4FirstName == "" ? user.FirstName : compass4FirstName;
                            string tempLastName = compass4LastName == "" ? user.LastName : compass4LastName;
                            
                            var update_compass = this.UpdateSponsor(sponsorId, tempFirstName, tempLastName, jumin, user.TranslationFlag, user.Mobile, user.Email, user.Phone,
                                null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            if (!update_compass.success)
                            {
                                result.message = update_compass.message;
                                return result;
                            }

                        }

                    }
                    else
                    {
                        //1-2. CI없음
                        // 신규등록
                        var parent_juminId = user.ParentJuminID;
                        if (!string.IsNullOrEmpty(parent_juminId))
                        {
                            parent_juminId = Roamer.Config.Cryptography.Decrypt(parent_juminId);
                        }

                        var update_compass = this.AddSponsor(ci, di, jumin, user.ComRegistration, "", "", "", "", null, null,
                        gender, "", "", user.LocationType, "한국", "", "", "", user.ParentName, parent_juminId, user.ParentMobile,
                        "", user.AgreePhone.HasValue ? user.AgreePhone.Value : true, null, null);
                        if (!update_compass.success)
                        {
                            result.message = update_compass.message;
                            return result;
                        }

                        sponsorId = conId = update_compass.data.ToString();

                    }
                    #endregion

                }
                else
                {

                    #region 2. 싱크후

                    var equalToSponsor = false;
                    // CI 있음
                    if (exist_in_compass)
                    {
                        if (sponsorId == sess.SponsorID)
                        {

                            equalToSponsor = true;
                            result.message = "이미 인증된 회원입니다.";
                        }
                        else
                        {
                            // 국외 회원(CI가 없는 회원)이 국내 전환 후 인증시(실명or본인) 이미 동륵된 정보가 있을수 있다. 
                            result.message = "후원자님은 이미 컴패션에 본인인증정보가 등록되어있습니다.\n후원지원팀으로 문의해주세요.\n후원지원팀(02 - 740 - 1000, 평일 9시~18시 / 공휴일제외)";
                            return result;
                        }

                    }
                    else
                    {
                        // CI없음
                        // 국외 회원(CI가 없는 회원)이 국내 전환 후 인증시(실명or본인) 본인인증 정보값만 업데이트 
                        result.message = "인증이 완료되었습니다.";
                        equalToSponsor = true;

                    }

                    if (equalToSponsor)
                    {
                        try
                        {

                            Object[] objSql = new object[1] { "sp_web_tSponsorMaster_update_f" };
                            Object[] objParam = new object[] { "sponsorId", "ci", "di", "genderCode", "JuminID" };
                            Object[] objValue = new object[] { sess.SponsorID, ci, di, gender, jumin };
                            DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
                            if (dt.Rows.Count < 1)
                            {
                                result.message = "회원정보를 수정중에 오류가 발생했습니다.";
                                return result;
                            }

                            if (dt.Rows[0]["result"].ToString() == "N")
                            {
                                result.message = dt.Rows[0]["msg"].ToString();
                                return result;
                            }

                            conId = dt.Rows[0]["conId"].ToString();
                            sponsorId = sess.SponsorID;

                        }
                        catch (Exception ex)
                        {
                            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                            result.message = "회원정보를 수정중에 오류가 발생했습니다.";
                            return result;
                        }
                    }

                    #endregion

                }

            }

            using (AuthLibDataContext dao = new AuthLibDataContext())
            {
                //var user = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
                var user = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);

                if (string.IsNullOrEmpty(user.SponsorID))
                {
                    user.SponsorID = sponsorId;
                    user.tempSponsorID = sponsorId;
                }
                user.ConID = conId;
                user.GenderCode = gender;
                user.CertifyOrgan = certifyOrgan;
                user.CertifyDate = DateTime.Now;
                if (!string.IsNullOrEmpty(jumin))
                    user.JuminID = Roamer.Config.Cryptography.Encrypt(jumin);
                //dao.SubmitChanges();
                www6.updateAuth(user);

                var cookie = FrontLoginSession.GetCookie(HttpContext.Current);
                cookie.SponsorID = sponsorId;
                cookie.GenderCode = gender;
                FrontLoginSession.SetCookie(HttpContext.Current, cookie);

            }

			result.action = "reload";
			result.success = true;
			return result;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "회원님의 정보를 수정하는 중 오류가 발생했습니다";
			return result;
		}
	}

	// 인증된 회원이 있는지 여부 조회
	public JsonWriter CheckSponsor( string ci ) {

		var result = new JsonWriter() { success = false };

		if(string.IsNullOrEmpty(ci)) {
			result.message = "CI 누락";
			return result;
		}

		try {

			Object[] objSql = new object[1] { string.Format("SELECT SponsorID , userid FROM tSponsorMaster WHERE CurrentUse = 'Y' and (UserId is not null and userId <> '') and CI = '{0}'", ci) };
			DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

			// 일치하는 데이타가 있는경우
			if(ds.Tables[0].Rows.Count > 0) {

				if(UserInfo.IsLogin) {
					UserInfo sess = new UserInfo();
					if(sess.SponsorID == ds.Tables[0].Rows[0]["SponsorID"].ToString().Trim()) {
						result.success = true;
						return result;
					}
				}

				var userid = ds.Tables[0].Rows[0]["userid"].ToString().Trim();
				result.action = "login";
				result.message = "인증정보로 가입된 회원정보가 있습니다. 아이디 : " + userid.Mask("*", 3);
				return result;
			} else {
				result.success = true;
				return result;
			}

			//	context.Response.Write(result);

		} catch(Exception e) {

			ErrorLog.Write(HttpContext.Current, 0, e.ToString());
			result.message = "회원정보 조회중에 장애가 발생했습니다. ";
			return result;
		}

	}

	// 오프라인 회원인지여부조회
	public JsonWriter FindOfflineUser( string ci ) {

		var result = new JsonWriter() { success = false };

		if(string.IsNullOrEmpty(ci)) {
			result.message = "CI 누락";
			return result;
		}

		try {

			//Object[] objSql = new object[1] { string.Format("SELECT SponsorID FROM tSponsorMaster WHERE CurrentUse = 'Y' and (UserId is not null or userId <> '') and CI = '{0}'", ci) };
			Object[] objSql = new object[1] { string.Format("SELECT SponsorID , userId FROM tSponsorMaster WHERE CurrentUse = 'Y' and CI = '{0}'", ci) };
			DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

			// 일치하는 데이타가 있는경우
			if(ds.Tables[0].Rows.Count > 0) {
				var SponsorID = ds.Tables[0].Rows[0]["SponsorID"].ToString().Trim();
				var userId = ds.Tables[0].Rows[0]["userId"].ToString().Trim();
				
				if(!string.IsNullOrEmpty(userId)) {
					result.action = "exist";
					result.data = new Dictionary<string, object> { { "has_account", true }, { "user_id", userId.Mask("*", 3) } };
					result.message = string.Format("이미 한국컴패션 웹회원으로 가입되어 있습니다.\n로그인 페이지로 이동하시겠습니까? ");
					return result;
				} else {
					result.success = true;
					result.data = new Dictionary<string, object> { { "has_account", false }, { "user_id", "" } };
					HttpContext.Current.Session["offline.join.sponsorId"] = SponsorID;      // 세션에 저장하고 가입페이지에서 체크 및 조회한다.
					return result;
				}

				/*
				using(AuthLibDataContext dao = new AuthLibDataContext()) {
					var entity = dao.tSponsorMaster.FirstOrDefault(p => p.SponsorID == SponsorID && p.CurrentUse == "Y");

					if(entity == null) {

						result.success = true;
						result.data = new Dictionary<string, object> { { "has_account", false }, { "user_id", "" } };
						HttpContext.Current.Session["offline.join.sponsorId"] = SponsorID;      // 세션에 저장하고 가입페이지에서 체크 및 조회한다.
						
					} else {
						result.action = "exist";
						result.data = new Dictionary<string, object> { { "has_account", true }, { "user_id", user.UserID.Mask("*", 3) } };
						result.message = string.Format("이미 인증이 완료된 계정이 있습니다. ");
					}
					return result;

				}
				*/

			} else {

				result.action = "notfound";
				result.message = "일치하는 정보가 없습니다.";
				return result;
			}

			//	context.Response.Write(result);

		} catch(Exception e) {

			ErrorLog.Write(HttpContext.Current, 0, e.ToString());
			result.message = "회원정보 조회중에 장애가 발생했습니다. ";
			return result;
		}

	}

	// 인증된 회원이 있는지 여부 조회
	public class SponsorItem {

		/*SponsorID, ConID, SponsorName, JuminID, ComRegistration, FirstName, LastName, LocationType, LanguageType, TranslationFlag, 
ReligionType, ChurchName, CommitmentCount, SponsorType, BirthDate, 
BirthDateClass, GenderCode, MotiveCode, MotiveCodeName
UserID, UserClass, CertifyDate, CertifyOrgan, ParentName, ParentJuminID, ParentMobile, ParentEmail, 
DI, CI,ManageType*/
		public string SponsorID;
		public string ConID;
		public string SponsorName;
		public string JuminID;
		public string ComRegistration;
		public string FirstName;
		public string LastName;
		public string LocationType;
		public string LanguageType;
		public string TranslationFlag;
		public string ReligionType;
		public string ChurchName;
		public int CommitmentCount;
		public string SponsorType;
		public DateTime? BirthDate;
		public string BirthDateClass;
		public string GenderCode;
		public string MotiveCode;
		public string MotiveCodeName;
		public string UserID;
		public string UserClass;
		public string CertifyDate;
		public string CertifyOrgan;
		public string ParentName;
		public string ParentJuminID;
		public string ParentMobile;
		public string ParentEmail;
		public string DI;
		public string CI;
		public string ManageType;

	}
	
	public JsonWriter GetSponsor() {

		var result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		UserInfo sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {

			result.action = "not_registered";
			result.message = "후원회원 정보가 없습니다.";
			return result;
		}

		try {

			Object[] objSql = new object[1] { "SELECT top 1 * FROM tSponsorMaster with(nolock) WHERE CurrentUse = 'Y' and sponsorId = '" + sess.SponsorID + "'" };
			DataSet dsSponsor = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

			if(dsSponsor == null) {
				result.message = "회원님의 정보를 가져오지 못했습니다.\\r\\n관리자에게 문의해주세요.";
				return result;
			}
			
			if (dsSponsor.Tables[0].Rows.Count < 1) {
				result.message = "회원님의 정보가 없습니다.";
				return result;
			}

			DataRow dr = dsSponsor.Tables[0].Rows[0];

			SponsorItem item = new SponsorItem() {

				SponsorID = dr["SponsorID"].ToString(),
				ConID = dr["ConID"].ToString(),
				SponsorName = dr["SponsorName"].ToString(),
				JuminID = dr["JuminID"].ToString(),
				ComRegistration = dr["ComRegistration"].ToString(),
				FirstName = dr["FirstName"].ToString(),
				LastName = dr["LastName"].ToString(),
				LocationType = dr["LocationType"].ToString(),
				LanguageType = dr["LanguageType"].ToString(),
				TranslationFlag = dr["TranslationFlag"].ToString(),
				ReligionType = dr["ReligionType"].ToString(),
				ChurchName = dr["ChurchName"].ToString(),
				CommitmentCount = Convert.ToInt32(dr["CommitmentCount"]),
				SponsorType = dr["SponsorType"].ToString(),
				BirthDateClass = dr["BirthDateClass"].ToString(),
				GenderCode = dr["GenderCode"].ToString(),
				MotiveCode = dr["MotiveCode"].ToString(),
				MotiveCodeName = dr["MotiveCodeName"].ToString(),
				UserID = dr["UserID"].ToString(),
				UserClass = dr["UserClass"].ToString(),
				CertifyDate = dr["CertifyDate"].ToString(),
				CertifyOrgan = dr["CertifyOrgan"].ToString(),
				ParentName = dr["ParentName"].ToString(),
				ParentJuminID = dr["ParentJuminID"].ToString(),
				ParentMobile = dr["ParentMobile"].ToString(),
				ParentEmail = dr["ParentEmail"].ToString(),
				DI = dr["DI"].ToString(),
				CI = dr["CI"].ToString(),

				ManageType = dr["ManageType"].ToString()
			};

            if(dr["BirthDate"] != DBNull.Value) {
                item.BirthDate = Convert.ToDateTime(dr["BirthDate"]);
            }

			result.data = item;
			result.success = true;
			return result;

		} catch(Exception e) {

			ErrorLog.Write(HttpContext.Current, 0, e.ToString());
			result.message = "회원정보 조회중에 장애가 발생했습니다. ";
			return result;
		}

	}

	// 후원자수정 컴파스
	public JsonWriter UpdateSponsor( string sponsorId, string firstName, string lastName, string juminId, string translateYN, string mobile, string email, string phone, string location,
		string country, string manageType, string zipcode, string addr1, string addr2,
		string ReligionType, string ChurchName, string organizationId, string motiveCode , string motiveCodeName ,
		bool? agree_receive  /*이메일/SMS 수신동의*/ , bool? is_receipt /*국세청 연말정산 영수증 신청*/ , bool? is_sponsor_info /*후원정보 수신*/) {

		JsonWriter result = new JsonWriter() { success = false, action = "" };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		UserInfo sess = new UserInfo();
		var actionResult = new JsonWriter();
		string sponsorName = sess.UserName;
		string sMotiveSponsor = "", sMotiveSponsorName = "";

		if(location == "국내") {
			country = "한국";
		}

		DataSet ds = new DataSet();
		string sResult = string.Empty;
		Int64 iTimeStamp = Convert.ToInt64(_wwwService.GetDate().ToString("yyyyMMddHHmmssff2"));

		try {

            using (AuthLibDataContext dao = new AuthLibDataContext())
            {
                //var entity = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
                var entity = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);

                if (!string.IsNullOrEmpty(sponsorId))
                {

                    #region DAT 회원정보수정
                    try
                    {
                        //개인일경우(14세이상, 14세미만, 국내외국인)
                        sResult = _wwwService.modifyDATSponsor_NEW(sponsorId, sponsorName, string.IsNullOrEmpty(juminId) ? null : juminId, entity.ComRegistration.Decrypt(), null, firstName, lastName,
                                                    translateYN, ReligionType, ChurchName, null, entity.BirthDateClass, entity.UserID,
                                                    null, null, sMotiveSponsor, sMotiveSponsorName,
                                                    null, motiveCode, motiveCodeName,
                                                    null, entity.UserDate.HasValue ? entity.UserDate.Value.ToString("yyyy-MM-dd") : null, entity.UserClass, entity.CertifyDate.HasValue ? entity.CertifyDate.Value.ToString("yyyy-MM-dd") : null, entity.CertifyOrgan);

                        if (sResult.Substring(0, 2) == "30")
                        {
                            result.message = "회원님의 정보를 수정하지 못했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
                            return result;
                        }

                    }
                    catch (Exception ex)
                    {
                        ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
                        result.message = "회원님의 정보를 수정하는 중 오류가 발생했습니다.(DAT)";
                        return result;
                    }

                    #endregion
                }

                #region WEB 회원정보 수정

                if (string.IsNullOrEmpty(entity.SponsorID))
                {
                    //#WO-208 이종진 - 웹회원가입만 할 시, sponsorid(X), tempsponsorid만 존재하고, sponsorid 가 ""로 오기 때문에, 모두지워져 버리는 것 방지
                    if (!string.IsNullOrEmpty(sponsorId))
                    {
                        entity.SponsorID = sponsorId;
                        entity.tempSponsorID = sponsorId;
                    }
                }
                entity.ModifyDate = DateTime.Now;
                //if(juminId != null)
                //	entity.JuminID = Roamer.Config.Cryptography.Encrypt(juminId);
                if (firstName != null)
                    entity.FirstName = firstName;
                if (lastName != null)
                    entity.LastName = lastName;
                if (!string.IsNullOrEmpty(location))
                    entity.LocationType = location;
                entity.ModifyID = sponsorId;
                entity.ModifyName = sponsorName;
                if (sMotiveSponsor != null)
                    entity.MotiveSponsor = sMotiveSponsor;
                if (sMotiveSponsorName != null)
                    entity.MotiveSponsorName = sMotiveSponsorName;
                if (mobile != null)
                    entity.Mobile = mobile.Encrypt();
                if (email != null)
                    entity.Email = email.Encrypt();
                if (phone != null)
                    entity.Phone = phone.Encrypt();
                if (!string.IsNullOrEmpty(ReligionType))
                {
                    entity.ReligionType = ReligionType;
                    if (ReligionType == "기독교")
                    {
                        entity.ChurchName = ChurchName;
                    }
                    else
                    {
                        entity.ChurchName = "";
                    }
                }
                if (agree_receive.HasValue)
                {
                    entity.AgreeEmail = entity.AgreePhone = agree_receive.Value;
                }
                if (!string.IsNullOrEmpty(translateYN))
                    entity.TranslationFlag = translateYN;
                if (!string.IsNullOrEmpty(motiveCode))
                    entity.MotiveCode = motiveCode;
                if (!string.IsNullOrEmpty(motiveCodeName))
                    entity.MotiveCodeName = motiveCodeName;
                if (zipcode != null)
                    entity.ZipCode = zipcode.Encrypt();
                if (addr1 != null)
                    entity.Addr1 = (addr1 + "$" + country).Encrypt();
                //entity.Addr1 = addr1.Encrypt();
                if (addr2 != null)
                    entity.Addr2 = addr2.Encrypt();

                //dao.SubmitChanges();
                www6.updateAuth(entity);

                #endregion
            }

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "회원님의 정보를 수정하는 중 오류가 발생했습니다.(WEB)";
			return result;
		}

		
		// 후원회원인경우 
		if(!string.IsNullOrEmpty(sponsorId)) {


            #region 후원정보 수신여부
            if (is_sponsor_info.HasValue)
            {
                actionResult = this.UpdateReceiveSponsorInfo(sponsorId, is_sponsor_info.Value);
                if (!actionResult.success)
                {
                    result.message = actionResult.message;
                    return result;
                }
            }
            #endregion

			if(!string.IsNullOrEmpty(location) && !string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(zipcode) && !string.IsNullOrEmpty(addr1) && !string.IsNullOrEmpty(addr2)) {
				#region 주소수정
				actionResult = this.GetAddress();
				if(!actionResult.success) {
					result.message = actionResult.message;
					return result;
				}
				SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)actionResult.data;

				actionResult = this.UpdateAddress(true, sess.UserId , sponsorId, sponsorName, addr_data.AddressType, location, country, zipcode, addr1, addr2);
				if(!actionResult.success) {
					result.message = actionResult.message;
					return result;
				}
				#endregion
			}

			
			#region 이메일 & 휴대전화 수정
			var comm_result = this.UpdateCommunications(true, sponsorId, email, mobile, phone);
			if(!comm_result.success) {
				result.message = comm_result.message;
				return result;
			}
			#endregion
			
			#region 이메일 SMS 수신
			if(agree_receive.HasValue) {
				var agree_result = this.UpdateAgreeCommunications(sponsorId, sess.UserName, agree_receive.Value, agree_receive.Value, email, mobile);
				if(!agree_result.success) {
					result.message = agree_result.message;
					return result;
				}
			}
			#endregion

			#region 국세청 연말정산 영수증 신청
			if(is_receipt.HasValue) {
				actionResult = this.UpdateReceiptReceive(sponsorId, is_receipt.Value);
				if(!actionResult.success) {
					result.message = actionResult.message;
					return result;
				}
			}
			#endregion


			// 다주소 후원자만 진행
			if(manageType == "1" || manageType == "2") {
				#region //지로로 납부중이면 Account 지로용지수신 주소ID로 수정

				//등록된 주소가 있으면
				//if (rbnHouse.Checked == true || rbnJob.Checked == true || rbnSeaHouse.Checked == true || rbnSeaJob.Checked == true)
				//{
				//지로로 납부중이면 Account 바꾸기
				DataSet dsGiro = new DataSet();
				try {
					dsGiro = _wwwService.selectUsingPaymentAccount(sponsorId, "0005"); //현재 지로로 납부중인지 확인
																					   //DB Error
					if(dsGiro == null) {

						result.message = "회원님의 납부정보를 수정하는 중 오류가 발생했습니다";
						return result;


					}
					//납부방법 Account 수정
					else if(dsGiro.Tables["NowusingAccountT"].Rows.Count > 0) {
						string sPaymentAccountID = dsGiro.Tables["NowusingAccountT"].Rows[0]["PaymentAccountID"].ToString(); //납부방법ID 가져오기
						string sAddressID = _wwwService.getGiroAddressID(sponsorId); //지로용지 수신중인 주소 가져오기
						if(!string.IsNullOrEmpty(sAddressID)) {
							sResult = _wwwService.modifyGiroAccout(sPaymentAccountID, sAddressID, sponsorId, sess.UserName);

							//DB Error
							if(sResult.Substring(0, 2) == "30") {
								result.message = "회원님의 납부정보를 수정하는 중 오류가 발생했습니다. \\r\\n" +
																 sResult.Substring(2).ToString().Replace("\n", "");
								return result;

							}
						}
					}
				} catch(Exception ex) {

					ErrorLog.Write(HttpContext.Current, 0, ex.Message);
					result.message = "회원님의 납부정보를 수정하는 중 서비스오류가 발생했습니다";
					return result;

				}
				//}

				#endregion
			}

			if(!string.IsNullOrEmpty(ReligionType) && ReligionType == "기독교" && !string.IsNullOrEmpty(ChurchName)) {

				actionResult = this.UpdateChurch(sponsorId, organizationId, ChurchName);
				if(!actionResult.success) {
					result.message = actionResult.message;
					return result;
				}

			}

		}

		// 쿠키 갱신
		var cookie = FrontLoginSession.GetCookie(HttpContext.Current);
		if(!string.IsNullOrEmpty(location))
			cookie.LocationType = location;
		FrontLoginSession.SetCookie(HttpContext.Current, cookie);

		result.success = true;
		return result;
	}

    public JsonWriter AddSponsor(string location, string country, string zipcode, string addr1, string addr2,string mobile, string email, string phone, string birthDate)
    {
        var actionResult = new JsonWriter();
        JsonWriter result = new JsonWriter() { success = false, action = "" };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        
        UserInfo sess = new UserInfo();

        string SponsorID = sess.SponsorID;
        string ConID = string.Empty;
        string ConIDCheck = string.Empty;
        string SponsorName = sess.UserName;
        string JuminID = string.Empty;
        string ComRegistration = string.Empty;
        string PassportID = string.Empty;
        string FirstName = string.Empty;
        string LastName = string.Empty;
        string LocationType = location;
        string USConID = string.Empty;
        string LanguageType = string.Empty;
        string TranslationFlag = string.Empty;
        string Correspondence = string.Empty;
        string ReligionType = string.Empty;
        string ChurchName = string.Empty;
        System.Nullable<int> CommitmentCount  = null;
        string SponsorType = string.Empty;
        string PaymentMethod = string.Empty;
        Nullable<DateTime> LastPTD = null;
        Nullable<DateTime> LastPaymentDate = null;
        string ChannelType = "Web";
        string CharacterType = string.Empty;
        string BirthDate = birthDate;
        string BirthDateClass = string.Empty;
        string SponsorshipLevel = string.Empty;
        string GenderCode = string.Empty;
        string CurrentUse = string.Empty;
        string MotiveCode = string.Empty;
        string MotiveCodeName = string.Empty;
        string RecommendType = string.Empty;
        string MotiveSponsor = string.Empty;
        string MotiveSponsorName = string.Empty;
        string CampaignID = string.Empty;
        string CampaignName = string.Empty;
        string Remark = string.Empty;
        string UserID = sess.UserId;
        string UserClass = sess.UserClass;
        Nullable<DateTime> UserDate = null;
        string JobCode = string.Empty;
        Nullable<DateTime> CertifyDate = null;
        string CertifyOrgan = string.Empty;
        string ParentName = string.Empty;
        string ParentJuminID = string.Empty;
        string ParentMobile = string.Empty;
        string ParentEmail = string.Empty;
        string ImComYN = string.Empty;
        string OpenInfoYN = string.Empty;
        string MergeID = string.Empty;
        string RegisterID = string.Empty;
        string RegisterName = string.Empty;
        //string RegisterDate = string.Empty;

        string ModifyID = string.Empty;
        string ModifyName = string.Empty;
        string ModifyDate = string.Empty;

        string MotiveSponsor_Name = string.Empty;
        string MotiveSponsor_TelNo = string.Empty;
        string CallSopnsorName = string.Empty;
        string DI = string.Empty;
        string CI = string.Empty;
        string CIVersion = string.Empty;
        string ManageType = string.Empty;
        string ProofData_Batch = string.Empty;
        string LetterPreference = string.Empty;
        string CharacterTypeComment = string.Empty;
        string IsMajorDonor = string.Empty;
        string SponsorGlobalID = string.Empty;
        string CorrReceiveType = string.Empty;
        string SimplifyYN = string.Empty;

        #region 웹회원수정
        using (AuthLibDataContext dao = new AuthLibDataContext())
        {
            //var s = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
            var s = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);

            SponsorID = s.SponsorID;
            GenderCode = s.GenderCode;

            if (string.IsNullOrEmpty(SponsorID))
            {
                SponsorID = s.tempSponsorID;
            }
            if (string.IsNullOrEmpty(SponsorID))
            {
                SponsorID = this.GetNewSponsorID();
            }

            if (string.IsNullOrEmpty(ConID))
            {
                ConID = s.ConID;
            }

            if (string.IsNullOrEmpty(ConIDCheck))
            {
                ConIDCheck = s.ConIDCheck;
            }

            if (string.IsNullOrEmpty(SponsorName))
            {
                SponsorName = s.SponsorName;
            }

            if (string.IsNullOrEmpty(JuminID))
            {
                JuminID = s.JuminID;
            }

            if (string.IsNullOrEmpty(ComRegistration))
            {
                ComRegistration = s.ComRegistration;
            }

            if (string.IsNullOrEmpty(PassportID))
            {
                PassportID = s.PassportID;
            }

            if (string.IsNullOrEmpty(FirstName))
            {
                FirstName = s.FirstName;
            }

            if (string.IsNullOrEmpty(LastName))
            {
                LastName = s.LastName;
            }

            if (string.IsNullOrEmpty(LocationType))
            {
                LocationType = s.LocationType;
            }

            if (string.IsNullOrEmpty(USConID))
            {
                USConID = s.USConID;
            }

            if (string.IsNullOrEmpty(LanguageType))
            {
                LanguageType = s.LanguageType;
            }

            if (string.IsNullOrEmpty(TranslationFlag))
            {
                TranslationFlag = s.TranslationFlag;
            }
            if (string.IsNullOrEmpty(Correspondence))
            {
                Correspondence = s.Correspondence;
            }
            if (string.IsNullOrEmpty(ReligionType))
            {
                ReligionType = s.ReligionType;
            }
            if (string.IsNullOrEmpty(ChurchName))
            {
                ChurchName = s.ChurchName;
            }
            if (CommitmentCount == null || CommitmentCount == 0)
            {
                CommitmentCount = s.CommitmentCount;
            }
            if (string.IsNullOrEmpty(SponsorType))
            {
                SponsorType = s.SponsorType;
            }
            if (string.IsNullOrEmpty(PaymentMethod))
            {
                PaymentMethod = s.PaymentMethod;
            }
            if (LastPTD == null)
            {
                LastPTD = s.LastPTD;
            }
            if (LastPaymentDate == null)
            {
                LastPaymentDate = s.LastPaymentDate;
            }
            if (string.IsNullOrEmpty(ChannelType))
            {
                ChannelType = s.ChannelType;
            }
            if (string.IsNullOrEmpty(CharacterType))
            {
                CharacterType = s.CharacterType;
            }
            if (string.IsNullOrEmpty(BirthDate))
            {
                BirthDate = s.BirthDate;
            }
            if (string.IsNullOrEmpty(BirthDateClass))
            {
                BirthDateClass = s.BirthDateClass;
            }
            if (string.IsNullOrEmpty(SponsorshipLevel))
            {
                SponsorshipLevel = s.SponsorshipLevel;
            }
            if (string.IsNullOrEmpty(GenderCode))
            {
                GenderCode = s.GenderCode;
            }
            if (string.IsNullOrEmpty(CurrentUse))
            {
                CurrentUse = s.CurrentUse;
            }
            if (string.IsNullOrEmpty(MotiveCode))
            {
                MotiveCode = s.MotiveCode;
            }
            if (string.IsNullOrEmpty(MotiveCodeName))
            {
                MotiveCodeName = s.MotiveCodeName;
            }
            if (string.IsNullOrEmpty(RecommendType))
            {
                RecommendType = s.RecommendType;
            }
            if (string.IsNullOrEmpty(MotiveSponsor))
            {
                MotiveSponsor = s.MotiveSponsor;
            }
            if (string.IsNullOrEmpty(MotiveSponsorName))
            {
                MotiveSponsorName = s.MotiveSponsorName;
            }
            if (string.IsNullOrEmpty(CampaignID))
            {
                CampaignID = s.CampaignID;
            }
            if (string.IsNullOrEmpty(CampaignName))
            {
                CampaignName = s.CampaignName;
            }
            if (string.IsNullOrEmpty(Remark))
            {
                Remark = s.Remark;
            }
            if (string.IsNullOrEmpty(UserID))
            {
                UserID = s.UserID;
            }
            if (string.IsNullOrEmpty(UserClass))
            {
                UserClass = s.UserClass;
            }
            if (UserDate == null)
            {
                UserDate = s.UserDate;
            }
            if (string.IsNullOrEmpty(JobCode))
            {
                JobCode = s.JobCode;
            }

            if (CertifyDate == null)
            {
                CertifyDate = s.CertifyDate;
            }
            if (string.IsNullOrEmpty(CertifyOrgan))
            {
                CertifyOrgan = s.CertifyOrgan;
            }
            if (string.IsNullOrEmpty(ParentName))
            {
                ParentName = s.ParentName;
            }
            if (string.IsNullOrEmpty(ParentJuminID))
            {
                ParentJuminID = s.ParentJuminID;
            }
            if (string.IsNullOrEmpty(ParentMobile))
            {
                ParentMobile = s.ParentMobile;
            }
            if (string.IsNullOrEmpty(ParentEmail))
            {
                ParentEmail = s.ParentEmail;
            }
            if (string.IsNullOrEmpty(ImComYN))
            {
                ImComYN = s.ImComYN;
            }
            /*
            if (string.IsNullOrEmpty(OpenInfoYN))
            {
                OpenInfoYN = s.OpenInfoYN;
            }
            */
            if (string.IsNullOrEmpty(MergeID))
            {
                MergeID = s.MergeID;
            }
            if (string.IsNullOrEmpty(RegisterID))
            {
                RegisterID = s.UserID;
            }
            if (string.IsNullOrEmpty(RegisterName))
            {
                RegisterName = s.SponsorName;
            }
            if (string.IsNullOrEmpty(ModifyID))
            {
                ModifyID = s.UserID;
            }
            if (string.IsNullOrEmpty(ModifyName))
            {
                ModifyName = s.SponsorName;
            }
            if (string.IsNullOrEmpty(MotiveSponsor_Name))
            {
                MotiveSponsor_Name = s.MotiveSponsor_Name;
            }
            if (string.IsNullOrEmpty(MotiveSponsor_TelNo))
            {
                MotiveSponsor_TelNo = s.MotiveSponsor_TelNo;
            }
            /*
            if (string.IsNullOrEmpty(CallSopnsorName))
            {
                CallSopnsorName = s.CallSopnsorName;
            }
            if (string.IsNullOrEmpty(DI))
            {
                DI = s.DI;
            }
            if (string.IsNullOrEmpty(CI))
            {
                CI = s.CI;
            }
            if (string.IsNullOrEmpty(CIVersion))
            {
                CIVersion = s.CIVersion;
            }
            if (string.IsNullOrEmpty(ManageType))
            {
                ManageType = s.ManageType;
            }
            if (string.IsNullOrEmpty(ProofData_Batch))
            {
                ProofData_Batch = s.ProofData_Batch;
            }
            if (string.IsNullOrEmpty(LetterPreference))
            {
                LetterPreference = s.LetterPreference;
            }
            if (string.IsNullOrEmpty(CharacterTypeComment))
            {
                CharacterTypeComment = s.CharacterTypeComment;
            }
            if (string.IsNullOrEmpty(IsMajorDonor))
            {
                IsMajorDonor = s.IsMajorDonor;
            }
            if (string.IsNullOrEmpty(SponsorGlobalID))
            {
                SponsorGlobalID = s.SponsorGlobalID;
            }
            if (string.IsNullOrEmpty(CorrReceiveType))
            {
                CorrReceiveType = s.CorrReceiveType;
            }
            if (string.IsNullOrEmpty(SimplifyYN))
            {
                SimplifyYN = s.SimplifyYN;
            }
            */      
            
            /*
            if (!string.IsNullOrEmpty(BirthDate))
            {
                s.BirthDate = BirthDate;
            }

            if (!string.IsNullOrEmpty(mobile))
            {
                s.Mobile = mobile;
            }

            if (!string.IsNullOrEmpty(email))
            {
                s.Email = email;
            }

            if (!string.IsNullOrEmpty(phone))
            {
                s.Phone = phone;
            }
            */
             
            //dao.SubmitChanges();
        }

        #endregion

        bool is_regist_dat_sponsor = true;

        #region DAT 회원 등록
        #region old code
        /*
       string strSql = "INSERT INTO tSponsorMaster"
                                                      + " ([SponsorID] "
                                                      + " ,[ConID]"
                                                      + " ,[ConIDCheck]"
                                                      + " ,[SponsorName]"
                                                      + " ,[JuminID]"
                                                      + " ,[ComRegistration]"
                                                      + " ,[PassportID]"
                                                      + " ,[FirstName]"
                                                      + " ,[LastName]"
                                                      + " ,[LocationType]"
                                                      + " ,[USConID]"
                                                      + " ,[LanguageType]"
                                                      + " ,[TranslationFlag]"
                                                      + " ,[Correspondence]"
                                                      + " ,[ReligionType]"
                                                      + " ,[ChurchName]"
                                                      + " ,[CommitmentCount]"
                                                      + " ,[SponsorType]"
                                                      + " ,[PaymentMethod]"
                                                      + " ,[LastPTD]"
                                                      + " ,[LastPaymentDate]"
                                                      + " ,[ChannelType]"
                                                      + " ,[CharacterType]"
                                                      + " ,[BirthDate]"
                                                      + " ,[BirthDateClass]"
                                                      + " ,[SponsorshipLevel]"
                                                      + " ,[GenderCode]"
                                                      + " ,[CurrentUse]"
                                                      + " ,[MotiveCode]"
                                                      + " ,[MotiveCodeName]"
                                                      + " ,[RecommendType]"
                                                      + " ,[MotiveSponsor]"
                                                      + " ,[MotiveSponsorName]"
                                                      + " ,[CampaignID]"
                                                      + " ,[CampaignName]"
                                                      + " ,[Remark]"
                                                      + " ,[UserID]"
                                                      + " ,[UserClass]"

                                                      + " ,[JobCode]"
                                                      + " ,[CertifyDate]"
                                                      + " ,[CertifyOrgan]"




                                                      + " ,[ImComYN]"
                                                      + " ,[OpenInfoYN]"
                                                      + " ,[MergeID]"
                                                      + " ,[RegisterID]"
                                                      + " ,[RegisterName]"
                                                      + " ,[RegisterDate]"
                                                      + " ,[ModifyID]"
                                                      + " ,[ModifyName]"
                                                      + " ,[ModifyDate]"
                                                      + " ,[MotiveSponsor_Name]"
                                                      + " ,[MotiveSponsor_TelNo]"
                                                      + " ,[CallSopnsorName]"
                                                      + " ,[DI]"
                                                      + " ,[CI]"
                                                      + " ,[CIVersion]"
                                                      + " ,[ManageType]"
                                                      + " ,[ProofData_Batch]"
                                                      + " ,[LetterPreference]"
                                                      + " ,[CharacterTypeComment]"
                                                      + " ,[IsMajorDonor]"
                                                      + " ,[SponsorGlobalID]"
                                                      + " ,[CorrReceiveType]"
                                                      + " ,[SimplifyYN])"
                                                + " VALUES"
                                                      + " ('" + SponsorID +"'"
                                                      + " ,'" + ConID +"'"
                                                      + " ,'" + ConIDCheck +"'"
                                                      + " ,'" + SponsorName +"'"
                                                      + " ,'" + JuminID +"'"
                                                      + " ,'" + ComRegistration +"'"
                                                      + " ,'" + PassportID +"'"
                                                      + " ,'" + FirstName +"'"
                                                      + " ,'" + LastName +"'"
                                                      + " ,'" + LocationType +"'"
                                                      + " ,'" + USConID +"'"
                                                      + " ,'" + LanguageType +"'"
                                                      + " ,'" + TranslationFlag +"'"
                                                      + " ,'" + Correspondence +"'"
                                                      + " ,'" + ReligionType +"'"
                                                      + " ,'" + ChurchName  +"'"
                                                      + " ,'" + CommitmentCount +"'"
                                                      + " ,'" + SponsorType +"'"
                                                      + " ,'" + PaymentMethod +"'"
                                                      + " ,'" + LastPTD +"'"
                                                      + " ,'" + LastPaymentDate +"'"
                                                      + " ,'" + ChannelType +"'"
                                                      + " ,'" + CharacterType +"'"
                                                      + " ,'" + BirthDate +"'"
                                                      + " ,'" + BirthDateClass +"'"
                                                      + " ,'" + SponsorshipLevel +"'"
                                                      + " ,'" + GenderCode +"'"
                                                      + " ,'" + CurrentUse +"'"
                                                      + " ,'" + MotiveCode +"'"
                                                      + " ,'" + MotiveCodeName +"'"
                                                      + " ,'" + RecommendType +"'"
                                                      + " ,'" + MotiveSponsor +"'"
                                                      + " ,'" + MotiveSponsorName +"'"
                                                      + " ,'" + CampaignID +"'"
                                                      + " ,'" + CampaignName +"'"
                                                      + " ,'" + Remark +"'"
                                                      + " ,'" + UserID +"'"
                                                      + " ,'" + UserClass +"'"

                                                      + " ,'" + JobCode +"'"
                                                      + " ,'" + CertifyDate +"'"
                                                      + " ,'" + CertifyOrgan+"'"




                                                      + " ,'" + ImComYN +"'"
                                                      + " ,'" + OpenInfoYN +"'"
                                                      + " ,'" + MergeID +"'"
                                                      + " ,'" + RegisterID +"'"
                                                      + " ,'" + RegisterName +"'"
                                                      + " ,GetDate()"
                                                      + " ,'" + ModifyID +"'"
                                                      + " ,'" + ModifyName +"'"
                                                      + " ,GetDate()"
                                                      + " ,'" + MotiveSponsor_Name +"'"
                                                      + " ,'" + MotiveSponsor_TelNo +"'"
                                                      + " ,'" + CallSopnsorName +"'"
                                                      + " ,'" + DI +"'"
                                                      + " ,'" + CI +"'"
                                                      + " ,'" + CIVersion +"'"
                                                      + " ,'" + ManageType +"'"
                                                      + " ,'" + ProofData_Batch +"'"
                                                      + " ,'" + LetterPreference +"'"
                                                      + " ,'" + CharacterTypeComment +"'"
                                                      + " ,'" + IsMajorDonor +"'"
                                                      + " ,'" + SponsorGlobalID +"'"
                                                      + " ,'" + CorrReceiveType +"'"
                                                      + " ,'" + SimplifyYN  +"')";
       try
       {
           Object[] objSql = new object[1] { strSql };
           _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
       }
       catch (Exception ex)
       {

           ErrorLog.Write(HttpContext.Current, 0, ex.Message);
           is_regist_dat_sponsor = false;
       }
       
       */
        #endregion old code

        string sResult = null;
        try
        {
            // tSponsorMaster에 LetterPreference, CorrReceiveType 컬럼은 default값으로 처리 ('Digital', 'online')
            sResult = _wwwService.registerDATSponsor2(SponsorID, sess.UserName, JuminID.EmptyIfNull(),
                                                            ComRegistration.EmptyIfNull(),
                                                            "",
                                                            FirstName.EmptyIfNull(), LastName.EmptyIfNull(),
                                                            location.EmptyIfNull(), TranslationFlag.EmptyIfNull(), ReligionType.EmptyIfNull(), ChurchName.EmptyIfNull(),
                                                            BirthDate.EmptyIfNull(), "", GenderCode.EmptyIfNull(), sess.UserId, CampaignID.EmptyIfNull(),
                                                            CampaignName.EmptyIfNull(), MotiveSponsor.EmptyIfNull(), MotiveSponsorName.EmptyIfNull(),
                                                            "", MotiveCode.EmptyIfNull(), MotiveCodeName.EmptyIfNull(), "",
                                                            DateTime.Now.ToString("yyyy-MM-dd"), UserClass.EmptyIfNull().Replace(" ", ""), CertifyDate.ToString().EmptyIfNull(),
                                                            CertifyOrgan.EmptyIfNull(), SponsorID, sess.UserName, "", "",
                                                            "", "", "", "", DI.EmptyIfNull(), CI.EmptyIfNull(), "0");


            if (sResult.Substring(0, 2) == "30")
            {
                is_regist_dat_sponsor = false;
            }

        }
        catch (Exception ex)
        {

            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            is_regist_dat_sponsor = false;
        }
         

        #endregion

        #region 주소/이메일/이동전화 등록
        //if (!string.IsNullOrEmpty(location) && !string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(zipcode) && !string.IsNullOrEmpty(addr1) && !string.IsNullOrEmpty(addr2))
        if (!string.IsNullOrEmpty(zipcode) && !string.IsNullOrEmpty(addr1) && !string.IsNullOrEmpty(addr2))
        {
            #region 주소수정
            actionResult = this.GetAddress();
            if (!actionResult.success)
            {
                result.message = actionResult.message;
                return result;
            }
            SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)actionResult.data;

            actionResult = this.UpdateAddress(true, sess.UserId, SponsorID, SponsorName, addr_data.AddressType, location, country, zipcode, addr1, addr2);
            if (!actionResult.success)
            {
                result.message = actionResult.message;
                return result;
            }
            #endregion
        }


        #region 이메일 & 휴대전화 수정
        var comm_result = this.UpdateCommunications(true, SponsorID, email, mobile, phone);
        if (!comm_result.success)
        {
            result.message = comm_result.message;
            return result;
        }
        #endregion

        #endregion

        #region 웹회원수정

        try
        {

            using (AuthLibDataContext dao = new AuthLibDataContext())
            {

                //var entity = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
                var entity = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);

                //if (string.IsNullOrEmpty(entity.tempSponsorID)) entity.tempSponsorID = SponsorID;
                if (string.IsNullOrEmpty(entity.SponsorID))
                {
                    entity.SponsorID = SponsorID;
                    entity.tempSponsorID = SponsorID;
                }
                
                entity.ModifyID = SponsorID;
                entity.ModifyName = sess.UserName;
                entity.ModifyDate = DateTime.Now;
                /*
                if (!string.IsNullOrEmpty(mobile))
                    entity.Mobile = mobile.Encrypt();
                if (!string.IsNullOrEmpty(email))
                    entity.Email = email.Encrypt();
                    */
                //dao.SubmitChanges();
                www6.updateAuth(entity);
            }

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
            result.message = "회원님의 정보를 수정하는 중 오류가 발생했습니다.(WEB)";
            return result;
        }

        #endregion


        var cookie = FrontLoginSession.GetCookie(HttpContext.Current);
        cookie.SponsorID = SponsorID;
        cookie.GenderCode = GenderCode;
        if (!string.IsNullOrEmpty(location))
            cookie.LocationType = location;
        FrontLoginSession.SetCookie(HttpContext.Current, cookie);

        result.data = SponsorID;
        result.success = true;
        return result;

    }

    public JsonWriter UpdateSponsor(string location, string country, string zipcode, string addr1, string addr2, string mobile, string email, string phone, string birthDate)
    {
        var actionResult = new JsonWriter();
        JsonWriter result = new JsonWriter() { success = false, action = "" };
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }


        UserInfo sess = new UserInfo();

        string SponsorID = sess.SponsorID;
        string ConID = string.Empty;
        string ConIDCheck = string.Empty;
        string SponsorName = sess.UserName;
        string JuminID = string.Empty;
        string ComRegistration = string.Empty;
        string PassportID = string.Empty;
        string FirstName = string.Empty;
        string LastName = string.Empty;
        string LocationType = location;
        string USConID = string.Empty;
        string LanguageType = string.Empty;
        string TranslationFlag = string.Empty;
        string Correspondence = string.Empty;
        string ReligionType = string.Empty;
        string ChurchName = string.Empty;
        System.Nullable<int> CommitmentCount = null;
        string SponsorType = string.Empty;
        string PaymentMethod = string.Empty;
        Nullable<DateTime> LastPTD = null;
        Nullable<DateTime> LastPaymentDate = null;
        string ChannelType = "Web";
        string CharacterType = string.Empty;
        string BirthDate = birthDate;
        string BirthDateClass = string.Empty;
        string SponsorshipLevel = string.Empty;
        string GenderCode = string.Empty;
        string CurrentUse = string.Empty;
        string MotiveCode = string.Empty;
        string MotiveCodeName = string.Empty;
        string RecommendType = string.Empty;
        string MotiveSponsor = string.Empty;
        string MotiveSponsorName = string.Empty;
        string CampaignID = string.Empty;
        string CampaignName = string.Empty;
        string Remark = string.Empty;
        string UserID = sess.UserId;
        string UserClass = sess.UserClass;
        Nullable<DateTime> UserDate = null;
        string JobCode = string.Empty;
        Nullable<DateTime> CertifyDate = null;
        string CertifyOrgan = string.Empty;
        string ParentName = string.Empty;
        string ParentJuminID = string.Empty;
        string ParentMobile = string.Empty;
        string ParentEmail = string.Empty;
        string ImComYN = string.Empty;
        string OpenInfoYN = string.Empty;
        string MergeID = string.Empty;
        string RegisterID = string.Empty;
        string RegisterName = string.Empty;
        //string RegisterDate = string.Empty;

        string ModifyID = string.Empty;
        string ModifyName = string.Empty;
        string ModifyDate = string.Empty;

        string MotiveSponsor_Name = string.Empty;
        string MotiveSponsor_TelNo = string.Empty;
        string CallSopnsorName = string.Empty;
        string DI = string.Empty;
        string CI = string.Empty;
        string CIVersion = string.Empty;
        string ManageType = string.Empty;
        string ProofData_Batch = string.Empty;
        string LetterPreference = string.Empty;
        string CharacterTypeComment = string.Empty;
        string IsMajorDonor = string.Empty;
        string SponsorGlobalID = string.Empty;
        string CorrReceiveType = string.Empty;
        string SimplifyYN = string.Empty;

        #region 웹회원수정
        using (AuthLibDataContext dao = new AuthLibDataContext())
        {
            //var s = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
            var s = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);

            SponsorID = s.tempSponsorID;
            GenderCode = s.GenderCode;

            if (string.IsNullOrEmpty(SponsorID))
            {
                SponsorID = s.tempSponsorID;
            }
            if (string.IsNullOrEmpty(SponsorID))
            {
                SponsorID = this.GetNewSponsorID();
            }

            if (string.IsNullOrEmpty(ConID))
            {
                ConID = s.ConID;
            }

            if (string.IsNullOrEmpty(ConIDCheck))
            {
                ConIDCheck = s.ConIDCheck;
            }

            if (string.IsNullOrEmpty(SponsorName))
            {
                SponsorName = s.SponsorName;
            }

            if (string.IsNullOrEmpty(JuminID))
            {
                JuminID = s.JuminID;
            }

            if (string.IsNullOrEmpty(ComRegistration))
            {
                ComRegistration = s.ComRegistration;
            }

            if (string.IsNullOrEmpty(PassportID))
            {
                PassportID = s.PassportID;
            }

            if (string.IsNullOrEmpty(FirstName))
            {
                FirstName = s.FirstName;
            }

            if (string.IsNullOrEmpty(LastName))
            {
                LastName = s.LastName;
            }

            if (string.IsNullOrEmpty(LocationType))
            {
                LocationType = s.LocationType;
            }

            if (string.IsNullOrEmpty(USConID))
            {
                USConID = s.USConID;
            }

            if (string.IsNullOrEmpty(LanguageType))
            {
                LanguageType = s.LanguageType;
            }

            if (string.IsNullOrEmpty(TranslationFlag))
            {
                TranslationFlag = s.TranslationFlag;
            }
            if (string.IsNullOrEmpty(Correspondence))
            {
                Correspondence = s.Correspondence;
            }
            if (string.IsNullOrEmpty(ReligionType))
            {
                ReligionType = s.ReligionType;
            }
            if (string.IsNullOrEmpty(ChurchName))
            {
                ChurchName = s.ChurchName;
            }
            if (CommitmentCount == null || CommitmentCount == 0)
            {
                CommitmentCount = s.CommitmentCount;
            }
            if (string.IsNullOrEmpty(SponsorType))
            {
                SponsorType = s.SponsorType;
            }
            if (string.IsNullOrEmpty(PaymentMethod))
            {
                PaymentMethod = s.PaymentMethod;
            }
            if (LastPTD == null)
            {
                LastPTD = s.LastPTD;
            }
            if (LastPaymentDate == null)
            {
                LastPaymentDate = s.LastPaymentDate;
            }
            if (string.IsNullOrEmpty(ChannelType))
            {
                ChannelType = s.ChannelType;
            }
            if (string.IsNullOrEmpty(CharacterType))
            {
                CharacterType = s.CharacterType;
            }
            if (string.IsNullOrEmpty(BirthDate))
            {
                BirthDate = s.BirthDate;
            }
            if (string.IsNullOrEmpty(BirthDateClass))
            {
                BirthDateClass = s.BirthDateClass;
            }
            if (string.IsNullOrEmpty(SponsorshipLevel))
            {
                SponsorshipLevel = s.SponsorshipLevel;
            }
            if (string.IsNullOrEmpty(GenderCode))
            {
                GenderCode = s.GenderCode;
            }
            if (string.IsNullOrEmpty(CurrentUse))
            {
                CurrentUse = s.CurrentUse;
            }
            if (string.IsNullOrEmpty(MotiveCode))
            {
                MotiveCode = s.MotiveCode;
            }
            if (string.IsNullOrEmpty(MotiveCodeName))
            {
                MotiveCodeName = s.MotiveCodeName;
            }
            if (string.IsNullOrEmpty(RecommendType))
            {
                RecommendType = s.RecommendType;
            }
            if (string.IsNullOrEmpty(MotiveSponsor))
            {
                MotiveSponsor = s.MotiveSponsor;
            }
            if (string.IsNullOrEmpty(MotiveSponsorName))
            {
                MotiveSponsorName = s.MotiveSponsorName;
            }
            if (string.IsNullOrEmpty(CampaignID))
            {
                CampaignID = s.CampaignID;
            }
            if (string.IsNullOrEmpty(CampaignName))
            {
                CampaignName = s.CampaignName;
            }
            if (string.IsNullOrEmpty(Remark))
            {
                Remark = s.Remark;
            }
            if (string.IsNullOrEmpty(UserID))
            {
                UserID = s.UserID;
            }
            if (string.IsNullOrEmpty(UserClass))
            {
                UserClass = s.UserClass;
            }
            if (UserDate == null)
            {
                UserDate = s.UserDate;
            }
            if (string.IsNullOrEmpty(JobCode))
            {
                JobCode = s.JobCode;
            }
            if (CertifyDate == null)
            {
                CertifyDate = s.CertifyDate;
            }
            if (string.IsNullOrEmpty(CertifyOrgan))
            {
                CertifyOrgan = s.CertifyOrgan;
            }
            if (string.IsNullOrEmpty(ParentName))
            {
                ParentName = s.ParentName;
            }
            if (string.IsNullOrEmpty(ParentJuminID))
            {
                ParentJuminID = s.ParentJuminID;
            }
            if (string.IsNullOrEmpty(ParentMobile))
            {
                ParentMobile = s.ParentMobile;
            }
            if (string.IsNullOrEmpty(ParentEmail))
            {
                ParentEmail = s.ParentEmail;
            }
            if (string.IsNullOrEmpty(ImComYN))
            {
                ImComYN = s.ImComYN;
            }
            /*
            if (string.IsNullOrEmpty(OpenInfoYN))
            {
                OpenInfoYN = s.OpenInfoYN;
            }
            */
            if (string.IsNullOrEmpty(MergeID))
            {
                MergeID = s.MergeID;
            }
            if (string.IsNullOrEmpty(RegisterID))
            {
                RegisterID = s.UserID;
            }
            if (string.IsNullOrEmpty(RegisterName))
            {
                RegisterName = s.SponsorName;
            }
            if (string.IsNullOrEmpty(ModifyID))
            {
                ModifyID = s.UserID;
            }
            if (string.IsNullOrEmpty(ModifyName))
            {
                ModifyName = s.SponsorName;
            }
            if (string.IsNullOrEmpty(MotiveSponsor_Name))
            {
                MotiveSponsor_Name = s.MotiveSponsor_Name;
            }
            if (string.IsNullOrEmpty(MotiveSponsor_TelNo))
            {
                MotiveSponsor_TelNo = s.MotiveSponsor_TelNo;
            }
         
        }

        #endregion

        bool is_regist_dat_sponsor = true;
        
        #region 주소/이메일/이동전화 등록
        if (!string.IsNullOrEmpty(location) && !string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(zipcode) && !string.IsNullOrEmpty(addr1) && !string.IsNullOrEmpty(addr2))
        {
            #region 주소수정
            actionResult = this.GetAddress();
            if (!actionResult.success)
            {
                result.message = actionResult.message;
                return result;
            }
            SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)actionResult.data;

            actionResult = this.UpdateAddress(true, sess.UserId, SponsorID, SponsorName, addr_data.AddressType, location, country, zipcode, addr1, addr2);
            if (!actionResult.success)
            {
                result.message = actionResult.message;
                return result;
            }
            #endregion
        }


        #region 이메일 & 휴대전화 수정
        var comm_result = this.UpdateCommunications(true, SponsorID, email, mobile, phone);
        if (!comm_result.success)
        {
            result.message = comm_result.message;
            return result;
        }
        #endregion

        #endregion




        var cookie = FrontLoginSession.GetCookie(HttpContext.Current);
        cookie.SponsorID = SponsorID;
        cookie.GenderCode = GenderCode;
        if (!string.IsNullOrEmpty(location))
            cookie.LocationType = location;
        FrontLoginSession.SetCookie(HttpContext.Current, cookie);

        result.data = SponsorID;
        result.success = true;
        return result;

    }


    // 후원자등록 컴파스
    // 첫후원인경우 , 본인,실명인증인경우 , 오프라인회원전환인경우 사용
    public JsonWriter AddSponsor( string ci, string di, string juminId, string comRegistration , string translateYN, string ReligionType, string ChurchName, string organizationId, string motiveCode, string motiveCodeName,
		string gender, string firstName, string lastName, string location, string country, string zipcode, string addr1, string addr2,
		string ParentName, string ParentJuminID, string ParentMobile, string ParentEmail,
		bool? agree_receive  /*이메일/SMS 수신동의*/ , bool? is_receipt /*국세청 연말정산 영수증 신청*/ , bool? is_sponsor_info /*후원정보 수신*/) {

		var actionResult = new JsonWriter();
		JsonWriter result = new JsonWriter() { success = false, action = "" };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

	//	Int64 iTimeStamp = Convert.ToInt64(_wwwService.GetDate().ToString("yyyyMMddHHmmssff2")); //ID : TimeStamp
	//	var sponsorId = iTimeStamp.ToString();
		string mobile = "", email = "", phone = "", BirthDate = "";
		string UserClass = "", userGroup = "", CertifyDate = "", CertifyOrgan = "", sMotiveSponsor = "", sMotiveSponsorName = "", CampaignID = "", CampaignTitle = "";

		if(location == "국내") {
			country = "한국";
		}

		UserInfo sess = new UserInfo();
		var sponsorName = sess.UserName;

		string sponsorId = "";		// 웹회원의 tempSponsorId 로 사용
		if(!string.IsNullOrEmpty(sess.SponsorID)) {
			result.message = "이미 후원회원으로 등록되어 있습니다.";
			return result;
		}


        #region 웹회원수정
        using (AuthLibDataContext dao = new AuthLibDataContext())
        {
            //var s = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
            var s = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);

            comRegistration = s.ComRegistration.Decrypt();
            sponsorId = s.SponsorID;

            if (string.IsNullOrEmpty(sponsorId))
            {
                sponsorId = s.tempSponsorID;
            }
            if (string.IsNullOrEmpty(sponsorId))
            {
                sponsorId = this.GetNewSponsorID();
            }

            if (string.IsNullOrEmpty(s.BirthDate))
            {
                BirthDate = "";
            }
            else
            {
                BirthDate = s.BirthDate.Decrypt().ToDateFormat("-");
            }
            mobile = s.Mobile.Decrypt();
            email = s.Email.Decrypt();
            phone = s.Phone.Decrypt();
            UserClass = s.UserClass.EmptyIfNull();
            userGroup = s.UserGroup.EmptyIfNull();
            CertifyDate = string.IsNullOrEmpty(ci) ? "" : DateTime.Now.ToString();
            CertifyOrgan = string.IsNullOrEmpty(ci) ? "" : "서울신용평가";
            sMotiveSponsor = s.MotiveSponsor.EmptyIfNull();
            sMotiveSponsorName = s.MotiveSponsorName.EmptyIfNull();
            CampaignID = s.CampaignID.EmptyIfNull();
            CampaignTitle = s.CampaignName.EmptyIfNull();

            //		if(!string.IsNullOrEmpty(juminId))
            //			s.JuminID = Roamer.Config.Cryptography.Encrypt(juminId);
            if (!string.IsNullOrEmpty(firstName))
                s.FirstName = firstName;
            if (!string.IsNullOrEmpty(lastName))
                s.LastName = lastName;
            if (!string.IsNullOrEmpty(translateYN))
                s.TranslationFlag = translateYN;
            if (!string.IsNullOrEmpty(ReligionType))
                s.ReligionType = ReligionType;
            if (!string.IsNullOrEmpty(ChurchName))
                s.ChurchName = ChurchName;
            if (!string.IsNullOrEmpty(ci))
            {
                s.CertifyOrgan = CertifyOrgan;
                s.CertifyDate = DateTime.Now;
            }
            if (!string.IsNullOrEmpty(gender))
                s.GenderCode = gender;
            if (ParentName != null)
            {
                s.ParentName = ParentName;
                if (ParentJuminID != null)
                    s.ParentJuminID = Roamer.Config.Cryptography.Encrypt(ParentJuminID);
                if (ParentMobile != null)
                    s.ParentMobile = ParentMobile.Decrypt();
            }

            if (!string.IsNullOrEmpty(motiveCode))
                s.MotiveCode = motiveCode;
            if (!string.IsNullOrEmpty(motiveCodeName))
                s.MotiveCodeName = motiveCodeName;
            if (agree_receive.HasValue)
            {
                s.AgreeEmail = s.AgreePhone = agree_receive.Value;
            }
            //dao.SubmitChanges();
            www6.updateAuth(s);

        }

		#endregion

		#region DAT 회원 등록

		DataSet dsResult = new DataSet();

		string sResult = null;
		bool is_regist_dat_sponsor = true;

		try {
            // tSponsorMaster에 LetterPreference, CorrReceiveType 컬럼은 default값으로 처리 ('Digital', 'online')
            sResult = _wwwService.registerDATSponsor2(sponsorId, sess.UserName, juminId.EmptyIfNull(),
															comRegistration.EmptyIfNull(),
															"", 
															firstName.EmptyIfNull(), lastName.EmptyIfNull(), 
															location.EmptyIfNull(), translateYN.EmptyIfNull(), ReligionType.EmptyIfNull(), ChurchName.EmptyIfNull(),
															BirthDate.EmptyIfNull(), "", gender.EmptyIfNull(), sess.UserId, CampaignID.EmptyIfNull(), 
															CampaignTitle.EmptyIfNull(), sMotiveSponsor.EmptyIfNull(), sMotiveSponsorName.EmptyIfNull(), 
															"", motiveCode.EmptyIfNull(), motiveCodeName.EmptyIfNull(), "",
															DateTime.Now.ToString("yyyy-MM-dd"), UserClass.EmptyIfNull().Replace(" ", ""), CertifyDate.EmptyIfNull(),
															CertifyOrgan.EmptyIfNull(), sponsorId , sess.UserName, "", "",
															ParentName.EmptyIfNull(), ParentJuminID.EmptyIfNull(), ParentMobile.EmptyIfNull(), "", di.EmptyIfNull(), ci.EmptyIfNull(), "0");


			if(sResult.Substring(0, 2) == "30") {
				is_regist_dat_sponsor = false;
			}

		} catch(Exception ex) {

			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			is_regist_dat_sponsor = false;
		}

		if(!is_regist_dat_sponsor) {
			result.message = "회원님의 정보를 등록하는 중 오류가 발생했습니다";
			return result;
		}

		#endregion

		#region DAT tSponsorOrganization 등록
		try {
			sResult = _wwwService.registerDATSponsorOrganization(sponsorId, sponsorId, sess.UserName);

			if(sResult.Substring(0, 2) == "30") {
				result.message = "회원님의 이메일 주소를 등록하지 못했습니다.\n" + sResult.Substring(2).ToString().Replace("\n", "");
				return result;
			}


		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "회원님의 정보를 등록하지 못했습니다";
			return result;
		}
		#endregion

		if(userGroup == "기업" || userGroup == "교회" || userGroup == "단체") {

			#region  DAT OrganizationMaster Register

			//추가 20120423 tOrganizationMaster
			try {
				
				// tOrganizationMaster
				sResult = _wwwService.registerOrganization(sponsorId, sess.UserName, userGroup,
														   sponsorId, firstName, lastName,
														   comRegistration, location, country,
														   zipcode, addr1, addr2,
														   sponsorId, sess.UserName);
			} catch(Exception ex) {

				ErrorLog.Write(HttpContext.Current, 0, ex.Message);
				result.message = "회원님의 정보를 등록하는 중 오류가 발생했습니다";
				return result;
			}

			#endregion

			#region DAT tOrganizationSponsor Register

			try {
				// tOrganizationSponsor

				if(userGroup == "기업") {
					sResult = _wwwService.registerOrganizationSponsor(sponsorId, "", "",
																		sponsorId, sess.UserName, "", "", sponsorId, sess.UserName);
				} else if(userGroup == "교회") {
					sResult = _wwwService.registerOrganizationSponsor(sponsorId, sponsorId, sess.UserName,
																		"", "", "", "", sponsorId, sess.UserName);
				} else if(userGroup == "단체") {
					sResult = _wwwService.registerOrganizationSponsor(sponsorId, "", "",
																		"", "", sponsorId, sess.UserName, sponsorId, sess.UserName);
				}

				//DB Error
				if(sResult.Substring(0, 2) == "30") {
					result.message = "회원님의 정보를 등록하지 못했습니다  \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
					return result;
				}

			} catch(Exception ex) {
				ErrorLog.Write(HttpContext.Current, 0, ex.Message);
				result.message = "회원님의 정보를 등록하는 중 오류가 발생했습니다";
				return result;

			}

			#endregion
		}


		#region 웹회원수정

		try {

            using (AuthLibDataContext dao = new AuthLibDataContext())
            {

                //var entity = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
                var entity = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);

                if (string.IsNullOrEmpty(entity.SponsorID))
                {
                    entity.SponsorID = sponsorId;
                    entity.tempSponsorID = sponsorId;
                }
                //if (string.IsNullOrEmpty(entity.SponsorID)) entity.SponsorID = sponsorId;
                if (string.IsNullOrEmpty(entity.ConID)) entity.ConID = sponsorId;
                if (!string.IsNullOrEmpty(juminId))
                    entity.JuminID = Roamer.Config.Cryptography.Encrypt(juminId);
                if (!string.IsNullOrEmpty(firstName))
                    entity.FirstName = firstName;
                if (!string.IsNullOrEmpty(lastName))
                    entity.LastName = lastName;
                if (!string.IsNullOrEmpty(location))
                    entity.LocationType = location;
                entity.ModifyID = sponsorId;
                entity.ModifyName = sess.UserName;
                entity.MotiveSponsor = sMotiveSponsor;
                entity.MotiveSponsorName = sMotiveSponsorName;
                if (!string.IsNullOrEmpty(mobile))
                    entity.Mobile = mobile.Encrypt();
                if (!string.IsNullOrEmpty(email))
                    entity.Email = email.Encrypt();
                if (!string.IsNullOrEmpty(gender))
                    entity.GenderCode = gender;
                if (!string.IsNullOrEmpty(CertifyOrgan))
                    entity.CertifyOrgan = CertifyOrgan;
                if (!string.IsNullOrEmpty(CertifyDate))
                    entity.CertifyDate = Convert.ToDateTime(CertifyDate);


                //dao.SubmitChanges();
                www6.updateAuth(entity);
            }

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "회원님의 정보를 수정하는 중 오류가 발생했습니다.(WEB)";
			return result;
		}

		#endregion

		#region 주소/이메일/이동전화 등록
		if(!string.IsNullOrEmpty(location) && !string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(zipcode) && !string.IsNullOrEmpty(addr1) && !string.IsNullOrEmpty(addr2)) {
			#region 주소수정
			actionResult = this.GetAddress();
			if(!actionResult.success) {
				result.message = actionResult.message;
				return result;
			}
			SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)actionResult.data;

			actionResult = this.UpdateAddress(true, sess.UserId , sponsorId, sponsorName , addr_data.AddressType, location, country, zipcode, addr1, addr2);
			if(!actionResult.success) {
				result.message = actionResult.message;
				return result;
			}
			#endregion
		}


		#region 이메일 & 휴대전화 수정
		var comm_result = this.UpdateCommunications(true, sponsorId, email, mobile, phone);
		if(!comm_result.success) {
			result.message = comm_result.message;
			return result;
		}
		#endregion

		#endregion

		#region 이메일/SMS 수신여부
		if(agree_receive.HasValue) {
			actionResult = this.UpdateAgreeCommunications(sponsorId, sess.UserName, agree_receive.Value, agree_receive.Value, email, mobile);
			if(!actionResult.success) {
				result.message = actionResult.message;
				return result;
			}
		}
		#endregion

		#region 국세청 연말정산 영수증 신청
		if(is_receipt.HasValue) {
			actionResult = this.UpdateReceiptReceive(sponsorId, is_receipt.Value);
			if(!actionResult.success) {
				result.message = actionResult.message;
				return result;
			}
		}
		#endregion

		#region 후원정보 수신여부
		if(is_sponsor_info.HasValue) {
			actionResult = this.UpdateReceiveSponsorInfo(sponsorId, is_sponsor_info.Value);
			if(!actionResult.success) {
				result.message = actionResult.message;
				return result;
			}
		}
		#endregion

		if(ReligionType == "기독교" && !string.IsNullOrEmpty(ChurchName)) {

			actionResult = this.UpdateChurch(sponsorId, organizationId, ChurchName);
			if(!actionResult.success) {
				result.message = actionResult.message;
				return result;
			}

		}
        
        //[이종진] 신규방법이고 결연하기 한 어린이가 있다면. sponsorid를 update해줌
        if (ConfigurationManager.AppSettings["dbgp_kind"].ToString() == "2"
            && HttpContext.Current.Request.Cookies["sessionId"] != null) //sessionId cookie는 결연하기 할 시, 생성됨
        {
            HttpCookie sessionIdcookie = HttpContext.Current.Request.Cookies["sessionId"];
            string sessionId = string.IsNullOrEmpty(sessionIdcookie.Value) ? "" : sessionIdcookie.Value;
            Object[] objSql = new object[1] { "UPDATE tTmpChildEnsure SET SponsorID = '" + sponsorId + "' WHERE SessionID='" + sessionId + "' AND UserID='"+ sess.UserId+ "' AND ISNULL(SponsorID,'')='' AND RegisterDate > DATEADD(HOUR, -1, GETDATE()) " };
            _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
        }

        // 쿠키 갱신
        var cookie = FrontLoginSession.GetCookie(HttpContext.Current);
		cookie.SponsorID = sponsorId;
		cookie.GenderCode = gender;
		if (!string.IsNullOrEmpty(location))
			cookie.LocationType = location;
		FrontLoginSession.SetCookie(HttpContext.Current, cookie);

		result.data = sponsorId;
		result.success = true;
		return result;
	}

	// 회원탈퇴
	public JsonWriter Withdraw( string pwd ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();
		
		// 후원중인 회원은 탈퇴 안됨
		if( sess.CommitCount != "0" || sess.ApplyCount != "0" ) {
			result.message = "회원님께서는 컴패션에 후원중이십니다. \\n후원지원팀(02-740-1000 평일 9시~18시/공휴일제외)으로 전화주시면\\n바로 도움 드리겠습니다.";
			return result;
		}


        using (MainLibDataContext dao = new MainLibDataContext())
        {
            //var exist_uf = dao.tUserFunding.Any(p => p.UserID == sess.UserId && p.uf_deleted == false);
            var exist_uf = www6.selectQAuth<tUserFunding>("UserID", sess.UserId, "uf_deleted", 0).Any();

            if (exist_uf)
            {
                result.message = "나눔펀딩을 개설한 회원은 탈퇴 할 수 없습니다.\\n후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) 으로 전화주시면\\n바로 도움 드리겠습니다.";
                return result;
            }
        }

		var sponsorId = sess.SponsorID;
        using (AuthLibDataContext dao = new AuthLibDataContext())
        {
            //var data = dao.sp_tSponsorMaster_get_f(sess.UserId, pwd, "", "", "", "", "").ToList();
            Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
            Object[] op2 = new Object[] { sess.UserId, pwd, "", "", "", "", "" };
            var data = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

            if (data.Count < 1)
            {
                result.success = false;
                result.message = "비밀번호가 일치하지 않습니다.";
                return result;
            }

            //var user = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
            var user = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);

            user.SponsorID = "";
            user.ConID = "";
            user.ConIDCheck = "";
            user.UserPW = "";
            user.CurrentUse = "N";
            user.JobCode = "";
            user.ParentEmail = "";
            user.UserPic = "";
            user.Addr1 = "";
            user.Addr2 = "";
            user.ZipCode = "";
            user.Phone = "";
            user.Mobile = "";
            user.Email = "";
            user.ParentJuminID = "";
            user.ParentMobile = "";
            user.ParentEmail = "";
            user.ModifyID = user.SponsorID;
            user.ModifyName = user.SponsorName;
            user.ModifyDate = DateTime.Now;
            user.Remark = "탈퇴";
            //dao.SubmitChanges();
            www6.updateAuth(user);
        }

		if(!string.IsNullOrEmpty(sponsorId)) {

			try {
				Object[] objSql = new object[1] { "sp_web_tSponsorMaster_withdraw_f" };
				Object[] objParam = new object[] { "sponsorId", "sponsorName" };
				Object[] objValue = new object[] { sess.SponsorID, sess.UserName };
				_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
			} catch(Exception ex) {
				ErrorLog.Write(HttpContext.Current, 0, ex.Message);
				result.message = "회원님의 정보를 수정 중 오류가 발생했습니다.";
				return result;
			}
		}

		result.success = true;
		return result;
	}

	public JsonWriter UpdateUserPic( string path ) {

		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

        using (AuthLibDataContext dao = new AuthLibDataContext())
        {
            //var user = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
            var user = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);
            user.UserPic = path;

            //dao.SubmitChanges();
            www6.updateAuth(user);

        }
		
		result.success = true;
		return result;
	}

	// 오프라인회원 조회
	public JsonWriter SearchOfflineUser(string keyword) {

		var result = new JsonWriter() { success = false };
		
		try {

			Object[] objSql = new object[1] { string.Format("SELECT top 100 * FROM tSponsorMaster with(nolock) WHERE CurrentUse = 'Y' and (UserId is null or UserId = '') and (sponsorId = '{0}' or sponsorName = '{0}' or conId = '{0}')", keyword.EscapeSqlInjection()) };
			DataSet dsSponsor = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

			if(dsSponsor == null) {
				result.message = "정보를 가져오지 못했습니다.\\r\\n관리자에게 문의해주세요.";
				return result;
			}

			if(dsSponsor.Tables[0].Rows.Count < 1) {
				result.message = "조회 결과가 없습니다.";
				return result;
			}

			List<SponsorItem> data = new List<SponsorItem>();
			foreach(DataRow dr in dsSponsor.Tables[0].Rows) {

				SponsorItem item = new SponsorItem() {

					SponsorID = dr["SponsorID"].ToString(),
					ConID = dr["ConID"].ToString(),
					SponsorName = dr["SponsorName"].ToString(),
					JuminID = dr["JuminID"].ToString(),
					ComRegistration = dr["ComRegistration"].ToString(),
					FirstName = dr["FirstName"].ToString(),
					LastName = dr["LastName"].ToString(),
					LocationType = dr["LocationType"].ToString(),
					LanguageType = dr["LanguageType"].ToString(),
					TranslationFlag = dr["TranslationFlag"].ToString(),
					ReligionType = dr["ReligionType"].ToString(),
					ChurchName = dr["ChurchName"].ToString(),
					CommitmentCount = Convert.ToInt32(dr["CommitmentCount"]),
					SponsorType = dr["SponsorType"].ToString(),
					BirthDateClass = dr["BirthDateClass"].ToString(),
					GenderCode = dr["GenderCode"].ToString(),
					MotiveCode = dr["MotiveCode"].ToString(),
					MotiveCodeName = dr["MotiveCodeName"].ToString(),
					UserID = dr["UserID"].ToString(),
					UserClass = dr["UserClass"].ToString(),
					CertifyDate = dr["CertifyDate"].ToString(),
					CertifyOrgan = dr["CertifyOrgan"].ToString(),
					ParentName = dr["ParentName"].ToString(),
					ParentJuminID = dr["ParentJuminID"].ToString(),
					ParentMobile = dr["ParentMobile"].ToString(),
					ParentEmail = dr["ParentEmail"].ToString(),
					DI = dr["DI"].ToString(),
					CI = dr["CI"].ToString(),
					ManageType = dr["ManageType"].ToString()
				};

				if(dr["BirthDate"] != DBNull.Value)
					item.BirthDate = Convert.ToDateTime(dr["BirthDate"]);


				data.Add(item);
				
			}
			
			result.data = data;
			result.success = true;
			return result;

		} catch(Exception e) {

			ErrorLog.Write(HttpContext.Current, 0, e.ToString());
			result.message = "회원정보 조회중에 장애가 발생했습니다. ";
			return result;
		}

	}

	// 웹회원 - 오프라인회원 연결
	public JsonWriter JoinOnOffUse( SponsorItem item) {

		JsonWriter result = new JsonWriter() { success = false, action = "" };
		
		try {

			if (string.IsNullOrEmpty(item.SponsorID)) {
				ErrorLog.Write(HttpContext.Current, 0, "SponsorAction.JoinOnOffUse : " + item.ToJson());
				result.message = "후원회원 아이디가 없습니다.";
				return result;
			}

			Object[] objSql = new object[1] { "sp_web_tSponsorMaster_update_f" };
			Object[] objParam = new object[] { "sponsorId", "UserID" };
			Object[] objValue = new object[] { item.SponsorID , item.UserID };
			DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
			if(dt.Rows.Count < 1) {
				result.message = "회원정보를 수정중에 오류가 발생했습니다.";
				return result;
			}

			if(dt.Rows[0]["result"].ToString() == "N") {
				result.message = dt.Rows[0]["msg"].ToString();
				return result;
			}

            using (AuthLibDataContext dao = new AuthLibDataContext())
            {
                //var user = dao.tSponsorMaster.First(p => p.UserID == item.UserID);
                var user = www6.selectQFAuth<tSponsorMaster>("UserID", item.UserID);

                user.SponsorID = item.SponsorID;
                user.tempSponsorID = item.SponsorID; //#12534 요청으로 주석 제거
                user.ConID = item.ConID;
                user.GenderCode = item.GenderCode;
                user.CertifyOrgan = item.CertifyOrgan;
                if (!string.IsNullOrEmpty(item.CertifyDate))
                    user.CertifyDate = Convert.ToDateTime(item.CertifyDate);
                if (!string.IsNullOrEmpty(item.JuminID))
                    user.JuminID = Roamer.Config.Cryptography.Encrypt(item.JuminID);

                //dao.SubmitChanges();
                www6.updateAuth(user);
            }

			result.success = true;
			return result;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "정보를 수정하는 중 오류가 발생했습니다";
			return result;
		}
	}

	public string GetNewSponsorID() {
		return _wwwService.GetDate().ToString("yyyyMMddHHmmssff2");
	}

	// 오프라인 뉴스레터 구독여부
	public bool CheckNewsletter(string SponsorID) {
		bool result = false;
		DataSet dsSponsorAllow = new DataSet();
		try {
			dsSponsorAllow = _wwwService.getSponsorAllow(SponsorID, "DM", "매거진");
			if (dsSponsorAllow != null) {
				DataRow[] findRow = dsSponsorAllow.Tables[0].Select("CurrentUse = 'Y'");
				if (findRow.Length != 0) result = true;
			}
		} catch (Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
		}
		return result;
	}


	// 오프라인 뉴스레터 구독신청
	public JsonWriter AgreeNewsletter(string SponsorID, string SponsorName) {

		JsonWriter result = new JsonWriter() { success = false };

		string sResult = "";

		DataSet dsSponsorAllow = new DataSet();
		try {
			//(수정 : 2012-01-25 By 주형준)
			//dsSponsorAllow = _WWWService.getSponsorAllow(sSponsorID, "이메일", "명세서");
			dsSponsorAllow = _wwwService.getSponsorAllow(SponsorID, "DM", "매거진");
			if (dsSponsorAllow == null) {
				result.message = "회원님의 이메일명세서 수신동의 정보를 가져오지 못했습니다.";
				return result;
			}
		} catch (Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "회원님의 이메일명세서 수신동의 정보를 가져오는 중 오류가 발생했습니다";
			return result;
		}

		DataRow[] findRow = dsSponsorAllow.Tables[0].Select("CurrentUse = 'Y'");
		if (findRow.Length == 0) {
			sResult = _wwwService.registerSponsorAllow(SponsorID, "DM", "매거진", SponsorID, SponsorName);
		} else {
			result.message = "이미 구독 중입니다.";
			return result;
		}

		result.success = true;
		return result;

	}


	// 오프라인 뉴스레터 구독취소
	public JsonWriter DisagreeNewsletter(string SponsorID) {
		JsonWriter result = new JsonWriter() { success = false };
		string sResult = "";
		DataSet dsSponsorAllow = new DataSet();
		try {
			//(수정 : 2012-01-25 By 주형준)
			//dsSponsorAllow = _WWWService.getSponsorAllow(sSponsorID, "이메일", "명세서");
			dsSponsorAllow = _wwwService.getSponsorAllow(SponsorID, "DM", "매거진");
			if (dsSponsorAllow == null) {
				result.message = "회원님의 이메일명세서 수신동의 정보를 가져오지 못했습니다.";
				return result;
			}
		} catch (Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "회원님의 이메일명세서 수신동의 정보를 가져오는 중 오류가 발생했습니다";
			return result;
		}

		DataRow[] findRow = dsSponsorAllow.Tables[0].Select("CurrentUse = 'Y'");
		if (findRow.Length != 0) {
			sResult = _wwwService.deleteSponsorAllow(SponsorID, "DM", "매거진");
		} else {
			result.message = "구독중인 정보가 없습니다.";
			return result;
		}

		result.success = true;
		return result;

	}
}