﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;

public partial class StoreAction
{

    public class cartItem
    {

        public string user_id;

        public int item_id;

        public int option;

        public string option_name;

        public int option_price;

        public int quantity;

        public List<child> children = new List<child>();

        public class child
        {
            public string childId;
            public string childName;
            public int ea;

        }

        public string repCorrespondenceId;

        public string CorrespondenceWebID;
    }

    // 결제시 사용
    public class orderItem
    {

        public string orderNo;

        public string device = "W";

        // 결제관련
        public string payMethod;        // kcp = 000000001 등

        public string payKey;           // kcp = tno

        public string cardType = "";

        public string cardName = "";

        public string cardPeriod = "";

        public string authDate = "";

        public string authNumber = "";

        public string bank = "";

        public string bankCode = "";

        public int orderPrice;

        public int optionPrice;

        public int deliveryFee;

        public int discountPrice = 0;

        public int settlePrice;

        // 배송지
        public string receiver;

        public string zipcode;

        public string addr1;

        public string addr2;

        public string mobile;

        public string phone;

        public string memo;

        public string email;

        // 결제정보
        public string site_code;

        public string req_tx;

        public string resCD;

        public string resMsg;

        // 상품
        public List<sp_temp_order_cart_list_fResult> cartList = new List<sp_temp_order_cart_list_fResult>();
        /*
		public class item {
			public int idx; // temp_order_cart 의 idx
			public int basket_id; // basket 의 idx
			public string child_names;	// 선물 받는 어린이 &nbsp; 포함된 문자열로 어린이목록을 문자열로 구성
			public string child_ids;    // 선물 받는 어린이 &nbsp; 포함된 문자열로 어린이목록을 문자열로 구성

		}
		*/
        /*receiver = m_f__get_post_data("receiver");
		zipcode = m_f__get_post_data("zipcode");
		addr1 = m_f__get_post_data("addr1");
		addr2 = m_f__get_post_data("addr2");
		mobile = m_f__get_post_data("mobile");
		phone = m_f__get_post_data("phone");
		memo = m_f__get_post_data("memo");
		email = m_f__get_post_data("email");*/

    }


    CommonLib.WWWService.ServiceSoap _wwwService;
    CommonLib.WWW6Service.SoaHelperSoap _www6Service;
    public StoreAction()
    {
        _wwwService = new CommonLib.WWWService.ServiceSoapClient();
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

    }

    // 선물 가능한 어린이목록
    /*
	ChildAction 으로 이동
	public JsonWriter GetChildren() {

		JsonWriter json = new JsonWriter() {
			success = true
		};

		if(!UserInfo.IsLogin) {
			json.success = false;
			json.message = "로그인이 필요합니다.";
			return json;
		}

		DataSet ds = _wwwService.PresentChild(new UserInfo().SponsorID, "", "", "");
		var children = ds.Tables["PresentT"];
		json.data = children;

		return json;

	}
	*/

    // 카트 목록
    public JsonWriter GetBasket(string imagePath)
    {
        JsonWriter json = new JsonWriter()
        {
            success = true
        };

        if (!UserInfo.IsLogin)
        {
            json.success = false;
            json.message = "로그인이 필요합니다.";
            return json;
        }

        var userId = new UserInfo().UserId;

        using (StoreLibDataContext dao = new StoreLibDataContext())
        {
            //var list = dao.sp_basket_list_f(userId).ToList();
            Object[] op1 = new Object[] { "userID" };
            Object[] op2 = new Object[] { userId };
            var list = www6.selectSPStore("sp_basket_list_f", op1, op2).DataTableToList<sp_basket_list_fResult>().ToList();

            foreach (var item in list)
            {
                item.name_img = (imagePath + item.name_img).WithFileServerHost();

                List<basket_child> children = (List<basket_child>)this.GetBasketChildren(item.order_idx).data;
                string child_name = "";
                foreach (var child in children)
                {
                    child_name += child.child_name + " &nbsp;";
                }

                item.ChildName = child_name;

            }
            json.data = list;
        }

        return json;
    }

    // 카트 어린이 목록
    public JsonWriter GetBasketChildren(int basket_idx)
    {
        JsonWriter json = new JsonWriter()
        {
            success = true
        };

        using (StoreLibDataContext dao = new StoreLibDataContext())
        {
            //json.data = dao.basket_child.Where(p => p.basket_idx == basket_idx).ToList();
            json.data = www6.selectQStore<basket_child>("basket_idx", basket_idx);
        }

        return json;
    }

    // 카트 -> 주문페이지
    public JsonWriter SetOrderTemp(List<int> basket_ids)
    {
        JsonWriter json = new JsonWriter()
        {
            success = true
        };

        if (!UserInfo.IsLogin)
        {
            json.success = false;
            json.message = "로그인이 필요합니다.";
            return json;
        }

        var userId = new UserInfo().UserId;

        using (StoreLibDataContext dao = new StoreLibDataContext())
        {
            var exist = www6.selectQStore<temp_order_cart>("user_id", userId);
            //if (dao.temp_order_cart.Any(p => p.user_id == userId))
            if(exist.Any())
            {
                //dao.temp_order_cart.DeleteAllOnSubmit(dao.temp_order_cart.Where(p => p.user_id == userId));
                //dao.SubmitChanges();
                www6.deleteStore(exist);
            }

            List<temp_order_cart> list = new List<temp_order_cart>();
            foreach (var basket_id in basket_ids)
            {
                //var b = dao.basket.First(p => p.idx == basket_id && p.user_id == userId);
                var b = www6.selectQFStore<basket>("idx", basket_id, "user_id", userId);

                var entity = new temp_order_cart()
                {
                    user_id = b.user_id,
                    product_idx = b.product_idx,
                    reg_date = b.reg_date,
                    option1_idx = b.option1_idx,
                    option2_idx = 0,
                    option1_name = b.option1_name,
                    option2_name = "",
                    option1_price = b.option1_price,
                    option2_price = 0,
                    quantity = b.quantity,
                    gift_flag = b.gift_flag,
                    basket_idx = b.idx
                };
                list.Add(entity);
            }
            //dao.temp_order_cart.InsertAllOnSubmit(list);
            foreach (var arg in list)
            {
                www6.insertStore(arg);
            }
            //dao.SubmitChanges();
        }

        return json;
    }

    // 주문페이지
    public JsonWriter GetOrderTemp(string imagePath)
    {
        JsonWriter json = new JsonWriter()
        {
            success = true
        };

        if (!UserInfo.IsLogin)
        {
            json.success = false;
            json.message = "로그인이 필요합니다.";
            return json;
        }

        var userId = new UserInfo().UserId;

        using (StoreLibDataContext dao = new StoreLibDataContext())
        {
            //var list = dao.sp_temp_order_cart_list_f(userId).ToList();
            Object[] op1 = new Object[] { "userId" };
            Object[] op2 = new Object[] { userId };
            var list = www6.selectSPStore("sp_temp_order_cart_list_f", op1, op2).DataTableToList<sp_temp_order_cart_list_fResult>().ToList();

            foreach (var item in list)
            {
                item.name_img = (imagePath + item.name_img).WithFileServerHost();

                List<basket_child> children = (List<basket_child>)this.GetBasketChildren(item.basket_idx.Value).data;
                string child_name = "";
                string childID = string.Empty;
                foreach (var child in children)
                {
                    child_name += child.child_name + " &nbsp;";
                    childID += child.child_id + " &nbsp;";
                }

                item.ChildName = child_name;
                item.ChildID = childID;

            }
            json.data = list;
        }

        return json;
    }

    // 카트 항목삭제
    public JsonWriter RemoveBasket(List<int> basket_ids)
    {
        JsonWriter json = new JsonWriter()
        {
            success = true
        };

        if (!UserInfo.IsLogin)
        {
            json.success = false;
            json.message = "로그인이 필요합니다.";
            return json;
        }

        var userId = new UserInfo().UserId;

        using (StoreLibDataContext dao = new StoreLibDataContext())
        {
            //var list = dao.basket.Where(p => basket_ids.Contains(p.idx) && p.user_id == userId);
            //dao.basket.DeleteAllOnSubmit(list);
            //dao.SubmitChanges();
            var list = www6.selectQ2Store<basket>("user_id = ", userId, "idx IN", utils.include(basket_ids));
            www6.deleteStore(list);

            //foreach (var idx in basket_ids)
            //{
            //    www6.cudStore(MakeSQL.delQ(0, "basket", "idx", idx, "user_id", userId));
            //}
        }

        return json;
    }

    // 카트 항목 수정
    public JsonWriter UpdateBasket(int basket_id, int ea)
    {
        JsonWriter json = new JsonWriter()
        {
            success = true
        };

        if (!UserInfo.IsLogin)
        {
            json.success = false;
            json.message = "로그인이 필요합니다.";
            return json;
        }

        var userId = new UserInfo().UserId;

        if (ea < 1)
        {
            json.success = false;
            json.message = "수량은 1개보다 커야 합니다.";
            return json;
        }

        using (StoreLibDataContext dao = new StoreLibDataContext())
        {
            //var entity = dao.basket.First(p => p.idx == basket_id && p.user_id == userId);
            var entity = www6.selectQFStore<basket>("idx", basket_id, "user_id", userId);
            entity.quantity = ea;

            //dao.SubmitChanges();
            www6.updateStore(entity);
        }

        return json;
    }

    // 카트 등록
    public JsonWriter SetBasket(string data_json)
    {
        ErrorLog.Write(HttpContext.Current, 0, "SetBasket Start");
        JsonWriter json = new JsonWriter()
        {
            success = true
        };

        try
        {
            var data = data_json.ToObject<cartItem>();
            var is_gift = (data.children != null && data.children.Count > 0);
            using (StoreLibDataContext dao = new StoreLibDataContext())
            {

                var entity = new basket()
                {
                    user_id = data.user_id,
                    product_idx = data.item_id,
                    reg_date = DateTime.Now,
                    option1_idx = data.option,
                    option1_name = data.option_name,
                    option1_price = data.option_price,
                    quantity = data.quantity,
                    gift_flag = is_gift,
                    repCorrespondenceId = data.repCorrespondenceId,
                    CorrespondenceWebID = data.CorrespondenceWebID
                };

                //dao.basket.InsertOnSubmit(entity);
                www6.insertStore(entity);
                //dao.SubmitChanges();

                ErrorLog.Write(HttpContext.Current, 0, "SetBasket Save");
                var basket_id = entity.idx;

                // 선물하기인경우 
                if (is_gift)
                {

                    List<basket_child> children = new List<basket_child>();

                    foreach (var child in data.children)
                    {

                        children.Add(new basket_child()
                        {
                            basket_idx = basket_id,
                            child_id = child.childId,
                            child_name = child.childName,
                            quantity = child.ea,
                            reg_date = DateTime.Now
                        });

                    }
                    //dao.basket_child.InsertAllOnSubmit(children);
                    foreach (var arg in children)
                    {
                        www6.insertStore(arg);
                    }
                    //dao.SubmitChanges();

                }
            }

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            json.success = false;
            json.message = "예외오류가 발생했습니다.";
        }

        return json;
    }

    public JsonWriter Order(orderItem item)
    {

        JsonWriter json = new JsonWriter()
        {
            success = true
        };

        if (!UserInfo.IsLogin)
        {
            json.success = false;
            json.message = "로그인이 필요합니다.";
            return json;
        }

        var sess = new UserInfo();

        return this.Order(item, sess.UserId, sess.SponsorID);
    }

    // payco 의 경우 payco서버에서 결제페이지를 호출하기때문에 세션정보가 없어서 아이디를 넘긴다.
    public JsonWriter Order(orderItem item, string userId, string sponsorId)
    {

        JsonWriter json = new JsonWriter()
        {
            success = true
        };

        try
        {

            using (StoreLibDataContext dao = new StoreLibDataContext())
            {

                // 0. 결제로그 저장
                try
                {
                    var kcplog = new payKCP()
                    {
                        deviceType = Convert.ToChar(item.device.Substring(0, 1)),
                        siteCode = item.site_code,
                        orderNo = item.orderNo,
                        req_tx = item.req_tx,
                        tno = item.payKey,
                        resCD = item.resCD,
                        resMsg = item.resMsg,
                        dtReg = DateTime.Now
                    };
                    //dao.payKCP.InsertOnSubmit(kcplog);
                    www6.insertStore(kcplog);
                }
                catch (Exception ex)
                {
                    ErrorLog.Write(HttpContext.Current, 0, "orderNo=" + item.orderNo + " : " + ex.ToString());
                }

                // 1 주문마스터
                var om = new orderMaster()
                {
                    orderNo = item.orderNo,
                    deviceType = item.device,
                    UserID = userId,
                    SponsorID = sponsorId,
                    payMethod = item.payMethod,
                    payKey = item.payKey,
                    cardType = item.cardType,
                    cardName = item.cardName,
                    cardPeriod = item.cardPeriod,
                    authDate = item.authDate,
                    authNumber = item.authNumber,
                    bank = item.bank,
                    bankCode = item.bankCode,
                    bankAccount = "",
                    orderPrice = item.orderPrice,
                    optionPrice = item.optionPrice,
                    deliveryFee = item.deliveryFee,
                    discountPrice = item.discountPrice,
                    settlePrice = item.settlePrice,
                    State = 2,
                    IP = HttpContext.Current.Request.ServerVariables.Get("REMOTE_ADDR"),
                    dtReg = DateTime.Now
                };

                //dao.orderMaster.InsertOnSubmit(om);
                www6.insertStore(om);

                // 2 배송
                var od = new orderDelivery()
                {
                    orderNo = item.orderNo,
                    deliveryCo = 0,
                    deliveryNum = "",
                    deliveryDate = DateTime.Parse("1900-01-01"),
                    deliveryStatus = '1',
                    orderName = item.receiver,
                    orderPhone = item.mobile,
                    orderTel = item.phone,
                    orderEmail = item.email,
                    orderAddr = item.addr1,
                    orderAddrDetail = item.addr2,
                    orderZipCode = item.zipcode,
                    rcvName = item.receiver,
                    rcvPhone = item.mobile,
                    rcvTel = item.phone,
                    rcvEmail = item.email,
                    rcvAddr = item.addr1,
                    rcvAddrDetail = item.addr2,
                    rcvZipCode = item.zipcode,
                    memo = item.memo
                };

                //dao.orderDelivery.InsertOnSubmit(od);
                www6.insertStore(od);

                // 3. 주문상세
                var ods = new List<orderDetail>();
                foreach (var cart_item in item.cartList)
                {

                    //cart_item.basket_idx

                    int basketIdx = Convert.ToInt32(cart_item.basket_idx);
                    //var repCorrespondenceId = dao.basket.Where(p => p.idx == basketIdx).FirstOrDefault().repCorrespondenceId;
                    var repCorrespondenceId = www6.selectQFStore<basket>("idx", basketIdx).repCorrespondenceId;
                    //var CorrespondenceWebID = dao.basket.Where(p => p.idx == basketIdx).FirstOrDefault().CorrespondenceWebID;
                    var CorrespondenceWebID = www6.selectQFStore<basket>("idx", basketIdx).CorrespondenceWebID;

                    ods.Add(
                        new orderDetail()
                        {
                            orderNo = item.orderNo,
                            productIDX = cart_item.product_idx,
                            productTitle = cart_item.name,
                            price = cart_item.selling_price,
                            qty = cart_item.quantity,
                            discountPrice = 0,
                            optionIDX = cart_item.option1_idx.Value,
                            optionDetail = cart_item.option1_name,
                            optionPrice = cart_item.option1_price.Value,
                            totalPrice = cart_item.orderPrice.Value,
                            childName = cart_item.ChildName,
                            childID = cart_item.ChildID,
                            dtReg = DateTime.Now,
                            repCorrespondenceId = repCorrespondenceId,
                            CorrespondenceWebID = CorrespondenceWebID
                        }
                        );

                }

                //dao.orderDetail.InsertAllOnSubmit(ods);
                foreach (var arg in ods)
                {
                    www6.insertStore(arg);
                }

                // 4. 주문한 항목 basket에서 삭제 
                //var cartIdx = Convert.ToInt32(item.cartList.Select(q => q.basket_idx));
                var cartIdxs = item.cartList.Select(q => q.basket_idx);
                var orderIdxs = item.cartList.Select(q => q.order_idx);

                var exist0 = www6.selectQ2Store<basket>("idx IN", utils.include(cartIdxs.ToList()));
                if(exist0.Any())
                {
                    www6.deleteStore(exist0);
                }

                var exist1 = www6.selectQ2Store<basket_child>("basket_idx IN", utils.include(cartIdxs.ToList()));
                if (exist1.Any())
                {
                    www6.deleteStore(exist1);
                }

                var exist2 = www6.selectQ2Store<temp_order_cart>("idx IN", utils.include(orderIdxs.ToList()));
                if (exist2.Any())
                {
                    www6.deleteStore(exist2);
                }

                //[jongjin.lee] 2018-04-30 #CO4-182 
                //5. 유료선물편지의 경우, kr_compass4.tCorrespondenceWeb.CorrStatus를 최초 'D'로 넣고, 결제되면 'N'으로 변경함
                foreach (var arg in ods)
                {
                    if(string.IsNullOrEmpty(arg.CorrespondenceWebID)
                        || string.IsNullOrEmpty(arg.repCorrespondenceId))
                    {
                        continue;
                    }

                    Object[] objSql = new object[1] { "sp_web_tCorrespondenceWeb_CorrStatus_update_f" };
                    Object[] objParam = new object[] { "RepCorrespondenceID" };
                    Object[] objValue = new object[] { arg.repCorrespondenceId };

                    // NameKr , ChildMasterID
                    _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

                }


                //foreach (var cl in item.cartList)
                //{
                //    //if (dao.basket.Any(p => item.cartList.Select(q => q.basket_idx).Contains(p.idx)))
                //    {
                //        www6.cudStore(MakeSQL.delQ(0, "basket", "idx", cl.basket_idx));
                //    }
                //}

                //foreach (var cl in item.cartList)
                //{
                //    //if (dao.basket_child.Any(p => item.cartList.Select(q => q.basket_idx).Contains(p.basket_idx)))
                //    {
                //        //dao.basket_child.DeleteAllOnSubmit(dao.basket_child.Where(p => item.cartList.Select(q => q.basket_idx).Contains(p.basket_idx)));
                //        www6.cudStore(MakeSQL.delQ(0, "basket_child", "basket_idx", cl.basket_idx));
                //    }
                //}

                //var orderIdx = item.cartList.Select(q => q.order_idx);
                ////var orderIdx = Convert.ToInt32(item.cartList.Select(q => q.order_idx));

                //foreach (var cl in item.cartList)
                //{
                //    //if (dao.temp_order_cart.Any(p => item.cartList.Select(q => q.order_idx).Contains(p.idx)))
                //    {
                //        //dao.temp_order_cart.DeleteAllOnSubmit(dao.temp_order_cart.Where(p => item.cartList.Select(q => q.order_idx).Contains(p.idx)));
                //        www6.cudStore(MakeSQL.delQ(0, "temp_order_cart", "idx", cl.order_idx));
                //    }
                //}
                //dao.SubmitChanges();
            }

            // stamp 발급
            new StampAction().Add("store");

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            json.success = false;
            json.message = "예외오류가 발생했습니다.";
        }

        return json;
    }

    // 후기목록
    public JsonWriter GetReviewList(int page, int rowsPerPage, int product_idx, String user_id)
    {
        JsonWriter result = new JsonWriter() { success = false };

        try
        {

            using (StoreLibDataContext dao = new StoreLibDataContext())
            {
                //result.data = dao.sp_review_list_f(page, rowsPerPage, product_idx, user_id).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage", "product_idx", "user_id" };
                Object[] op2 = new Object[] { page, rowsPerPage, product_idx, user_id };
                result.data = www6.selectSPStore("sp_review_list_f", op1, op2).DataTableToList<sp_review_list_fResult>().ToList();
            }

            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
            result.message = "정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }

    // 문의목록
    public class sp_qna_list_fResultEx : sp_qna_list_fResult
    {
        public bool is_owner { get; set; }
    }

    public JsonWriter GetQnaList(int page, int rowsPerPage, int product_idx, string type, string imagePath)
    {
        JsonWriter result = new JsonWriter() { success = false };

        var userId = "";
        if (UserInfo.IsLogin)
        {
            userId = new UserInfo().UserId;
        }

        try
        {

            using (StoreLibDataContext dao = new StoreLibDataContext())
            {
                //var list = dao.sp_qna_list_f(page, rowsPerPage, product_idx, type == "all" ? "" : userId).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage", "product_idx", "user_id" };
                Object[] op2 = new Object[] { page, rowsPerPage, product_idx, type == "all" ? "" : userId };
                var list = www6.selectSPStore("sp_qna_list_f", op1, op2).DataTableToList<sp_qna_list_fResult>().ToList();

                List<sp_qna_list_fResultEx> data = new List<sp_qna_list_fResultEx>();
                foreach (sp_qna_list_fResult item in list)
                {

                    data.Add(new sp_qna_list_fResultEx()
                    {
                        rownum = item.rownum,
                        total = item.total,
                        idx = item.idx,
                        isOpen = item.isOpen != null ? item.isOpen : true,
                        product_idx = item.product_idx,
                        title = (item.isOpen.Value && userId != item.user_id) ? "비공개 글 입니다." : item.title.EmptyIfNull() == "" ? item.question : item.title,
                        name = item.name,
                        name_img = (imagePath + item.name_img).WithFileServerHost(),
                        question = (item.isOpen.Value && userId != item.user_id) ? "비공개 글 입니다. 글쓴이와 관리자만 열람 가능합니다." : item.question,
                        answer = (item.isOpen.Value && userId != item.user_id) ? "비공개 글 입니다. 글쓴이와 관리자만 열람 가능합니다." : item.answer,
                        answer_date = item.answer_date,
                        reg_date = item.reg_date,
                        is_owner = userId == item.user_id,
                        user_id = item.user_id.Mask("*", 3),
                    });

                }
                result.data = data;
            }

            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }

    // 공지목록
    public JsonWriter GetNoticeList(int page, int rowsPerPage, int idx, string type, string keyword)
    {
        JsonWriter result = new JsonWriter() { success = false };

        try
        {

            using (StoreLibDataContext dao = new StoreLibDataContext())
            {
                //result.data = dao.sp_notice_list_f(page, rowsPerPage, idx, type, keyword).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage", "idx", "keyword_type", "keyword" };
                Object[] op2 = new Object[] { page, rowsPerPage, idx, type, keyword };
                result.data = www6.selectSPStore("sp_notice_list_f", op1, op2).DataTableToList<sp_notice_list_fResult>().ToList();
            }

            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }

    // 공지목록
    public JsonWriter GetReviewableList()
    {
        JsonWriter result = new JsonWriter() { success = false };

        if (!UserInfo.IsLogin)
        {
            result.success = false;
            result.message = "로그인이 필요합니다.";
            return result;
        }

        var userId = new UserInfo().UserId;

        try
        {

            using (StoreLibDataContext dao = new StoreLibDataContext())
            {
                //result.data = dao.sp_reviewable_list_f(userId).ToList();
                Object[] op1 = new Object[] { "user_id" };
                Object[] op2 = new Object[] { userId };
                result.data = www6.selectSPStore("sp_reviewable_list_f", op1, op2).DataTableToList<sp_reviewable_list_fResult>().ToList();
            }

            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }


    // 주문내역
    public JsonWriter GetOrderList(int page, int rowsPerPage, string startdate, string enddate, string imagePath)
    {
        JsonWriter json = new JsonWriter()
        {
            success = true
        };

        if (!UserInfo.IsLogin)
        {
            json.success = false;
            json.message = "로그인이 필요합니다.";
            return json;
        }

        var userId = new UserInfo().UserId;

        using (StoreLibDataContext dao = new StoreLibDataContext())
        {
            //var list = dao.sp_order_list_f(userId, page, rowsPerPage, startdate, enddate).ToList();
            Object[] op1 = new Object[] { "userid", "page", "rowsPerPage", "startdate", "enddate" };
            Object[] op2 = new Object[] { userId, page, rowsPerPage, startdate, enddate };
            var list = www6.selectSPStore("sp_order_list_f", op1, op2).DataTableToList<sp_order_list_fResult>().ToList();

            foreach (var item in list)
            {
                item.img = (imagePath + item.img).WithFileServerHost();
            }
            json.data = list;
        }

        return json;
    }

    // 주문상세내역
    public JsonWriter GetOrderDetailList(string orderNo, string imagePath)
    {
        JsonWriter json = new JsonWriter()
        {
            success = true
        };

        if (!UserInfo.IsLogin)
        {
            json.success = false;
            json.message = "로그인이 필요합니다.";
            return json;
        }

        var userId = new UserInfo().UserId;

        using (StoreLibDataContext dao = new StoreLibDataContext())
        {
            //var list = dao.sp_orderDetail_list_f(orderNo).ToList();
            Object[] op1 = new Object[] { "orderNo" };
            Object[] op2 = new Object[] { orderNo };
            var list = www6.selectSPStore("sp_orderDetail_list_f", op1, op2).DataTableToList<sp_orderDetail_list_fResult>().ToList();

            foreach (var item in list)
            {
                item.name_img = (imagePath + item.name_img).WithFileServerHost();
            }
            json.data = list;
        }

        return json;
    }

    // 상품리스트
    public JsonWriter GetStoreList(int page, int rowsPerPage, int gift_flag, string keyword, string imagePath)
    {
        JsonWriter result = new JsonWriter() { success = false };

        try
        {

            using (StoreLibDataContext dao = new StoreLibDataContext())
            {
                //var list = dao.sp_product_list_f(page, rowsPerPage, gift_flag, keyword).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage", "gift_flag", "keyword" };
                Object[] op2 = new Object[] { page, rowsPerPage, gift_flag, keyword };
                var list = www6.selectSPStore("sp_product_list_f", op1, op2).DataTableToList<sp_product_list_fResult>().ToList();

                foreach (var item in list)
                {
                    item.name_img = (imagePath + item.name_img).WithFileServerHost();
                }
                result.data = list;
            }

            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }

    // 랜덤상품리스트
    public JsonWriter GetRandomList(int rows)
    {
        JsonWriter result = new JsonWriter() { success = false };

        try
        {

            using (StoreLibDataContext dao = new StoreLibDataContext())
            {
                //var list = dao.sp_randomProduct_list_f(rows).ToList();
                Object[] op1 = new Object[] { "num"};
                Object[] op2 = new Object[] { rows };
                var list = www6.selectSPStore("sp_randomProduct_list_f", op1, op2).DataTableToList<sp_randomProduct_list_fResult>().ToList();
            }

            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;

        }

        return result;
    }




}