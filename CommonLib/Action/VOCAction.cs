﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;

public partial class VOCAction {

	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;
	public VOCAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

	}

	public JsonWriter CheckUser() {
		JsonWriter result = new JsonWriter() { success = false };

		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

        using (MainLibDataContext dao = new MainLibDataContext())
        {
            var exist = www6.selectQ<advocate_user>("au_user_id", sess.UserId);
            //if (dao.advocate_user.Any(p => p.au_user_id == sess.UserId))
            if(exist.Any())
            {
                result.success = true;
                result.data = CodeAction.ChannelType;
                return result;
            }

            var sponsorId = sess.SponsorID;
            if (!string.IsNullOrEmpty(sponsorId))
            {

                Object[] objSql = new object[] { "sp_web_tSponsorGroup_get_f" };
                Object[] objParam = new object[] { "sponsorId" };
                Object[] objValue = new object[] { sponsorId };

                DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

                if (list.Rows.Count > 0)
                {

                    var entity = new advocate_user()
                    {
                        au_user_id = sess.UserId,
                        au_from = "COMPASS",
                        au_regdate = DateTime.Now
                    };

                    //dao.advocate_user.InsertOnSubmit(entity);
                    www6.insert(entity);
                    //dao.SubmitChanges();


                    result.success = true;
                    result.data = list.Rows[0]["GroupType"].ToString();
                    return result;
                }

            }
        }

		return result;

	}
	
}

