﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using TCPTModel.Request.Supporter;
using Newtonsoft.Json;
using TCPTModel;
using TCPTModel.Response.Supporter;
using TCPTModel.Request.Hold.Beneficiary;
using static iTextSharp.text.pdf.AcroFields;
using TCPTModel.Response.Hold.Beneficiary;
using CommonLib;
using TCPTModel.Request.Commitment;
using TCPTModel.Response.Commitment;

public partial class CommitmentAction {
    CommonLib.WWWService.ServiceSoap _wwwService;
    CommonLib.WWW6Service.SoaHelperSoap _www6Service;

    //CommonLib.TCPTService.ServiceSoap _tcptService;
    public CommitmentAction() {
        _wwwService = new CommonLib.WWWService.ServiceSoapClient();
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
        // _tcptService = new CommonLib.TCPTService.ServiceSoapClient();

    }

    // 어린이 선택 , 1시간 keep
    public JsonWriter DoCDSP(string childMasterId, string childKey, string campaignId, string codeName, string motiveCode, string motiveName, bool isWedding = false) {
        return this.DoCDSP(childMasterId, childKey, campaignId, codeName, motiveCode, motiveName, null, 1, isWedding);
    }

    // 해외카드 즉시결제의 경우 sponsorAmount 수동지정
    public JsonWriter DoCDSP(string childMasterId, string childKey, string campaignId, string codeName, string motiveCode, string motiveName, int? sponsorAmount, int month, bool isWedding = false) {

        JsonWriter result = new JsonWriter();
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();

        if (!UserInfo.IsLogin) {
            result.message = "로그인이 필요합니다.";
            result.action = "login";
            return result;
        }

        //[이종진] 추가 - 초기 childmasterid (즉, "09"+global_id 를 가지고있기위함)
        string originChildMasterId = childMasterId;

        var sess = new UserInfo();
        var sSponsorID = sess.SponsorID;
        var sSponsorName = sess.UserName;
        var UserId = sess.UserId;
        var UserName = sess.UserName;

        if (!ValidateCDSP(childMasterId, campaignId, codeName, sSponsorID, ref result))
        {
            result.success = false;
            return result;
        }

        
        // 기존 방식일 경우에는, 이미 어린이 정보가 들어 있으므로, 결연정보만 등록함
        if (strdbgp_kind == "1")
        {
            // 결연정보 등록 CDSP (tCommitmentMaster Insert)
            if(!RegisterCDSPCommitment(childMasterId, campaignId, sSponsorID, sSponsorName, motiveCode, motiveName, sponsorAmount, month, ref result))
            {
                result.success = false;
            }
        }

        // 신규 방식 (어린이 리스트 Global Pool조회)
        // ** GlobalPool에 NomoneyHold요청 실패 시, DeletetCommitmentMaster(결연정보삭제)
        else //if (strdbgp_kind == "2")
        {
            //어린이 정보 insert
            //2018-03-21 이종진 - TCPT_Bene등의 테이블에도 데이터 추가
            //beneficiary kit(어린이 상세정보) 조회
            JsonWriter childDetail = new JsonWriter();
            ChildAction childAction = new ChildAction();
            childDetail = childAction.Beneficiary_GET(childMasterId);
            if (childDetail.success)    //상세정보 존재 시, 어린이정보와 TCPT_Bene등 기타 데이터도 입력
            {
                JsonWriter insertResult = new JsonWriter();
                insertResult = childAction.InsertTCPTBene_tChild(childDetail.data, childMasterId, childKey, true);
                if (insertResult.success)
                {
                    childMasterId = insertResult.data.ToString();
                }
                else
                {
                    result.success = false;
                    result.message = "어린이 정보 생성이 실패하였습니다. 다른 어린이를 선택해주세요.";
                }
            }
            else //미존재 시, tchildmaster 데이터만 등록
            {
                result = InsertChild(childMasterId, childKey, sSponsorID, UserId, UserName, isWedding);
                if (result.success == false) return result;
                //[이종진] insert 작업 후, childMasterId 값 변경해주기(어린이 정보가 존재한다면 존재하는 childMasterId가 새로옴)
                childMasterId = result.data.ToString();
            }

            //[이종진] 쿠키값의 ChildMasterId도 변경해줌
            var payInfo = PayItemSession.GetCookie(HttpContext.Current);
            payInfo.childMasterId = childMasterId;
            PayItemSession.SetCookie(HttpContext.Current, payInfo);

            // 기존에 DB에서 1시간 결연 구문 추가
            childAction.Ensure(childMasterId);

            // 결연정보 등록 CDSP (tCommitmentMaster Insert)
            // result.data에 CommitmentId를 반환함
            if (!RegisterCDSPCommitment(childMasterId, campaignId, sSponsorID, sSponsorName, motiveCode, motiveName, sponsorAmount, month, ref result))
            {
                result.success = false;
            }
            
            string commitmentId = result.data == null ? "" : result.data.ToString().Split(',')[0];
            // GlobalPool에 NomoneyHold 및, 
            // KR_Compass4에 NomoneyHold 정보 insert 와 임시결연정보 insert
            if(!NomoneyHoldGP(commitmentId, originChildMasterId).success)
            {
                //실패 시, 결연정보 삭제
                DeleteCommitment(commitmentId);
                //실패 시, KR_Compass4 DB에 NomoneyHold 정보, 임시결연정보 삭제

                result.success = false;
            }
            
        }
        
        return result;
    }

    public JsonWriter DoCDSP(string childMasterId, string childKey, string campaignId, string codeName, string motiveCode, string motiveName, int? sponsorAmount, int month, string first)
    {

        ErrorLog.Write(HttpContext.Current, 0, "DoCDSP  START");

        JsonWriter result = new JsonWriter();

        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            result.action = "login";
            return result;
        }

        var sess = new UserInfo();
        var sSponsorID = sess.SponsorID;
        var sSponsorName = sess.UserName;

        ErrorLog.Write(HttpContext.Current, 0, "ValidateCDSP  START");
        if (!ValidateCDSP(first, childMasterId, campaignId, codeName, sSponsorID, ref result))
        {
            ErrorLog.Write(HttpContext.Current, 0, "ValidateCDSP  FALSE");
            result.success = false;
            return result;
        }
        /*
        ErrorLog.Write(HttpContext.Current, 0, "updatePTD  START");

        if(!updatePTD(sSponsorID, childMasterId, ref result))
        {
            ErrorLog.Write(HttpContext.Current, 0, "updatePTD  FALSE");
            result.success = false;
        }
        */
        return result;

    }


    // codeID : PBCIV등 , frequency : 일시 , 정기 , payMethod : 100000000000 등과 같은 결제수단 코드 , payMethod 가 무통장인 경우 예외처리를 하고 있음
    public JsonWriter DoCIV(string sponsorId, int amount, int month, string payMethod, string frequency, string codeId, string codeName, string childMasterId, string campaignId, string motiveCode, string motiveName) {

        JsonWriter result = new JsonWriter() { data = "" };

        string sFundingFrequency = "";
        if (frequency == "일시") {
            sFundingFrequency = "1회";
        } else {
            sFundingFrequency = "매월";

            if (!UserInfo.IsLogin) {
                result.message = "로그인이 필요합니다.";
                result.action = "login";
                return result;
            }

        }
        if (UserInfo.IsLogin) {

            ValidateCIV(sponsorId, frequency, codeId, codeName, campaignId, ref result);
            if (!result.success) {
                return result;
            }
        }

        // registerCommitmentComplement
        if (!RegisterCIVCommitment(sponsorId, motiveCode, motiveName, amount, month, payMethod, sFundingFrequency, codeId, childMasterId, campaignId, ref result)) {
            result.success = false;
        }

        return result;

    }

    public JsonWriter DoCIV(string sponsorId, int amount, int month, string payMethod, string frequency, string codeId, string codeName, string childMasterId, string campaignId, string motiveCode, string motiveName, string isFirstPay)
    {

        JsonWriter result = new JsonWriter() { data = "" };

        string sFundingFrequency = "";
        if (frequency == "일시")
        {
            sFundingFrequency = "1회";
        }
        else
        {
            sFundingFrequency = "매월";

            if (!UserInfo.IsLogin)
            {
                result.message = "로그인이 필요합니다.";
                result.action = "login";
                return result;
            }
        }
        if (UserInfo.IsLogin)
        {
            ValidateCIV(sponsorId, frequency, codeId, codeName, campaignId, ref result);
            if (!result.success)
            {
                return result;
            }
        }
        return result;
    }

    // codeID : CF , BG ,GG 등 , frequency : 일시 , 정기 , payMethod : 100000000000 등과 같은 결제수단 코드 , payMethod 가 무통장인 경우 예외처리를 하고 있음
    // childMasterIds = , 로 구분되는 어린이 아이디들
    public JsonWriter DoGIFT(string sponsorId, string childMasterIds, string amount, string payMethod, string month, string frequency, string codeId) {

        JsonWriter result = new JsonWriter();

        /*
		payco 의 경우 서버에서 callback 으로 웹페이지를 호출하기때문에 세션정보가 없다.
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			result.action = "login";
			return result;
		}
		*/

        string sFundingFrequency = "";
        if (frequency == "일시") {
            sFundingFrequency = "1회";
        } else {
            sFundingFrequency = "매년";
        }
        /*	
		ValidateCIV(sponsorId, frequency, codeId, codeName, campaignId, ref result);
		if(!result.success) {
			return result;
		}
		*/

        // registerCommitmentComplement
        if (!RegisterCommitmentPresent(sponsorId, childMasterIds, amount, payMethod, sFundingFrequency, codeId, month, ref result)) {
            result.success = false;
        }

        return result;
    }

    // 20171207 고진혁 생성 - 결제 성공시 1. 글로벌 스폰서 아이디 체크 2. 글로벌 스폰서 아이디없으면 글로벌 풀에서 생성. 3. No money hold 정보 및 임시 결연정보 삭제(GlobalPool이 아닌 KR_Compass4 DB). 4. Global Commitment 생성
    public JsonWriter ProcSuccessPay(string strCommitmentID)
    {
        JsonWriter result = new JsonWriter() { success = false };
        
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();

        //기존방법이면 바로 return
        if (strdbgp_kind == "1")
        {
            result.success = true;
            return result;
        }

        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();
        DataTable dt3 = new DataTable();
        DataTable dt4 = new DataTable();

        string strFirstName = "";
        string strLastName = "";
        string strGenderCode = "";
        string strConId = "";

        var sess = new UserInfo();
        var sponsorID = sess.SponsorID;
        var UserId = sess.UserId;

        var GlobalSponsorID = "";
        var ChildMasterID = "";
        var ChildGlobalID = "";
        var HoldId = "";

        string strResult = "";

        //        1.글로벌 스폰서 아이디 체크
        #region 글로벌 스폰서 아이디 체크 및 생성
        Object[] objSql1 = new object[] { "sp_S_tSponsorMaster" };
        Object[] objParam1 = new object[] { "SponsorID" };
        Object[] objValue1 = new object[] { sponsorID };
        dt1 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql1, "SP", objParam1, objValue1).Tables[0];

        if (dt1.Rows.Count == 0)
        {
            result.success = false;
            result.message = "결제 후 처리 중, 후원자 조회 오류가 발생하였습니다.";
            return result;
        }

        GlobalSponsorID = dt1.Rows[0]["SponsorGlobalID"] == null ? "" : dt1.Rows[0]["SponsorGlobalID"].ToString();

        strFirstName = dt1.Rows[0]["FirstName"].ToString();
        strLastName = dt1.Rows[0]["LastName"].ToString();
        strGenderCode = dt1.Rows[0]["GenderCode"].ToString();
        strConId = dt1.Rows[0]["ConID"].ToString();

        if (string.IsNullOrEmpty(GlobalSponsorID))
        {
            // 2. 글로벌 스폰서 아이디없으면 글로벌 풀에서 생성

            try
            {
                SupporterCreateInSFCI_POST kit1 = new SupporterCreateInSFCI_POST();
                SupporterProfile profile = new SupporterProfile();

                profile.GlobalPartner = "Compassion Korea";
                profile.CorrespondenceDeliveryPreference = "Digital";
                profile.FirstName = strFirstName;
                profile.LastName = strLastName;
                if (!string.IsNullOrEmpty(strGenderCode))
                    profile.Gender = strGenderCode;
                profile.GPID = "54-" + strConId;
                profile.PreferredName = "";
                profile.Status = "Active";
                profile.StatusReason = "New";
                profile.MandatoryReview = false;

                kit1.SupporterProfile.Add(profile);

                CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();

                string json = JsonConvert.SerializeObject(kit1);
                strResult = _OffRampService.SupporterCreateInSFCI_POST(json);
                TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(strResult);
                if (msg.IsSuccessStatusCode)
                {
//                    test += "Sponsor GlobalID  생성 성공 / ";
                    SupporterProfileResponse_Kit response = JsonConvert.DeserializeObject<SupporterProfileResponse_Kit>(msg.RequestMessage.ToString());
                    GlobalSponsorID = response.SupporterProfileResponse[0].GlobalID;

//                    test += "Sponsor GlobalID  생성 성공 " + GlobalSponsorID + " / ";
//                    test += "Sponsor GlobalID  업데이트 시작 / ";
                    bool b = _OffRampService.UpdateSponsorGlobalID(sponsorID, GlobalSponsorID);

                    if (b)
                    {
                        result.success = true;
//                        test += "Sponsor GlobalID  업데이트 성공 / ";
                    }
                    else
                    {
                        //                        test += "Sponsor GlobalID  업데이트 실패 / ";
                        result.message = "결제 후 처리 중, 후원자 정보 업데이트 오류가 발생하였습니다.";
                        result.success = false;
//                        result.message = "Sponsor GlobalID  업데이트 실패";
//                        return result;
                    }
                }
                else
                {
                    //                    test += "Sponsor GlobalID  생성 실패 " + msg.RequestMessage.ToString() + " / ";
                    //오류
                    result.message = "결제 후 처리 중, 후원자 정보 생성 오류가 발생하였습니다.";
                    result.success = false;
//                    result.message = "Sponsor GlobalID  생성 실패";
//                    return result;
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.message = "Sponsor GlobalID  생성 오류 " + ex.Message;
                return result;
                //                test += "Sponsor GlobalID  생성 오류 " + ex.Message + "/ ";
            }

            if (result.success == false)
            {
                return result;
            }
        }

        #endregion

        #region Global Commitment 생성 및 tCommitmentMaster Update
        //ChildMasterID, HoldID 조회
        Object[] objSql2 = new object[] { "sp_S_TCPT_CommitmentTemp_HoldBeneficiary" };
        Object[] objParam2 = new object[] { "SponsorID", "CommitmentID" };
        Object[] objValue2 = new object[] { sponsorID, strCommitmentID };
        dt2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2).Tables[0];

        if (dt2.Rows.Count == 0)
        {
            result.success = false;
            result.message = "결제 후 킷 발송 중, 오류가 발생하였습니다.";
            return result;
        }

        if (dt2.Rows[0]["RTNCODE"].ToString() != "1")
        {
            result.message = dt2.Rows[0]["RTNMESSAGE"].ToString();
            result.success = false;
            return result;
        }
        //[이종진][체크] ChildGlobalID
        ChildMasterID = dt2.Rows[0]["ChildMasterID"].ToString();
        HoldId = dt2.Rows[0]["HoldId"].ToString();
        ChildGlobalID = dt2.Rows[0]["ChildGlobalID"].ToString();

        try
        {
            // 3.Global Commitment 생성
            CreateSponsorshipCommitmentList_POST kit2 = new CreateSponsorshipCommitmentList_POST();
            BeneficiaryCommitmentList item = SetCommitmentKit(ChildGlobalID, GlobalSponsorID, HoldId, true, UserId);
            kit2.BeneficiaryCommitmentList.Add(item);

            // 3.1 결연 처리킷 발송
            result = RegisterCommitmentTCPT(strCommitmentID, kit2);

            if (!result.success)
            {
                result.message = "결연처리 킷 발송 오류 ";
                return result;
            }
        }
        catch (Exception ex)
        {
            result.success = false;
            result.message = "GlobalPool Commitment 생성 및 결연처리 킷 발송 오류 " + ex.Message;
            return result;
        }

        #endregion

        #region No money hold 정보 및 임시 결연정보 삭제 

        // 4.No money hold 정보 및 임시 결연정보 삭제 
        // No money hold정보와 임시결연정보 삭제 

        // [이종진] ChildMasterID와 HoldID를 조회 후, GlobalCommitment 생성 후,
        // NomoneyHold정보 및 임시결연정보 삭제함. 
        // ** GlobalCommitment생성 시, 오류나면 NomoneyHold정보를 가지고있어서, Compass4프로그램에서 실무자가 처리함
        Object[] objSql3 = new object[] { "sp_D_TCPT_CommitmentTemp_HoldBeneficiary" };
        Object[] objParam3 = new object[] { "SponsorID", "CommitmentID" };
        Object[] objValue3 = new object[] { sponsorID, strCommitmentID };
        dt3 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql3, "SP", objParam3, objValue3).Tables[0];

        if (dt3.Rows.Count == 0)
        {
            result.success = false;
            result.message = "결제 후 결연 처리 중, 오류가 발생하였습니다.";
            return result;
        }

        if (dt3.Rows[0]["RTNCODE"].ToString() != "1")
        {
            result.message = dt3.Rows[0]["RTNMESSAGE"].ToString();
            result.success = false;
            return result;
        }

        #endregion

        #region Release 처리 (GlobalPool Hold는 풀지않음)

        //09+childGlobalID 인 이유 - 초기 결연시, tTmpChildEnsure테이블에 09+ChildGlobalID 로 ChildMasterID를 넣어주기때문
        new ChildAction().Release("09" + ChildGlobalID, false);

        #endregion

        return result;
    }

    /// <summary>
    /// 결연 생성용 kit
    /// </summary>
    /// <param name="childGlobalID"></param>
    /// <param name="isCorrCommit"></param>
    /// <returns></returns>
    BeneficiaryCommitmentList SetCommitmentKit(string childGlobalID, string sponsorGlobalID, string holdID, bool isCorrCommit, string UserId)
    {
        BeneficiaryCommitmentList kit = new BeneficiaryCommitmentList();
        kit.IsSponsorCorrespondent = isCorrCommit;
        kit.PrimaryHoldOwner = UserId;
        kit.SponsorCorrespondenceLanguage = "English";
        kit.SponsorGlobalPartnerID = "KR";
        kit.SponsorSupporterGlobalID = sponsorGlobalID;
        kit.Beneficiary_GlobalID = childGlobalID;
        kit.HoldID = holdID;
        return kit;
    }

    /// <summary>
    /// 결연 처리 kit 발송
    /// </summary>
    /// <param name="childGlobalID"></param>
    /// <param name="isCorrCommit"></param>
    /// <returns></returns>
    JsonWriter RegisterCommitmentTCPT(string strCommitmentID, CreateSponsorshipCommitmentList_POST kit)
    {
        JsonWriter result = new JsonWriter() { success = false };
        string strResult = "";

//        int errorCnt = 0;
        //bool bSuccess = true;
        //string strRetMsg = "";

        string json = JsonConvert.SerializeObject(kit);
        CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();

        strResult = _OffRampService.CreateSponsorshipCommitmentList_POST(json);

        TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(strResult);

        // 발송 성공
        if (msg.IsSuccessStatusCode)
        {
            BeneficiaryCommitmentResponseList_Kit response = JsonConvert.DeserializeObject<BeneficiaryCommitmentResponseList_Kit>(msg.RequestMessage.ToString());

            foreach (BeneficiaryCommitmentResponseList item in response.BeneficiaryCommitmentResponseList)
            {
                
                // CommitmentID, CorCommitmentID 사용해서 아래 쿼리 처럼 Kr_compass4.dbo.tCommitmentMaster 에 업데이트
                string GlobalCommitmentID = item.Commitment_ID;
                string GlobalCorCommitmentID = item.CorrespondentCommitmentID;
                //string ReturnCode = item.Code.ToString();
                //string ReturnMessage = item.Message;
                //bSuccess = ReturnCode == "2000" ? true : false;

                //실패
                if (item.Code.ToString() != "2000")
                {
                    result.success = false;
                    result.message = item.Message;
                }
                else //성공
                {
                    // 신규 방식 결제 최종 tCommitmentMaster 업데이트
                    result = UpdateCommitmentMaster(strCommitmentID, GlobalCommitmentID, GlobalCorCommitmentID);
                }
            }
        }
        // 발송 실패
        else
        {
            //OffRamp 에러 메시지 저장
            //Common.RegisterTCPTOffRampError(msg, "wTCPTCommitmentProcess",  "RegisterCommitmentTCPT", "CreateSponsorshipCommitmentList_POST");
            result.success = false;

            TCPTModel.TCPT_Error terr = JsonConvert.DeserializeObject<TCPTModel.TCPT_Error>(msg.RequestMessage.ToString());
            TCPTModel.ErrorResponse err = terr.ErrorResponse;
            string returnMessage = string.Empty;

            if (err.ErrorId == null)
            {
                BeneficiaryCommitmentResponseList_Kit response = JsonConvert.DeserializeObject<BeneficiaryCommitmentResponseList_Kit>(msg.RequestMessage.ToString());

                if (response.BeneficiaryCommitmentResponseList.Count > 0)
                {
                    foreach (BeneficiaryCommitmentResponseList item in response.BeneficiaryCommitmentResponseList)
                    {
                        result.message = "결연 킷 발송 실패 : " + item.Message;
                    }
                }
                else
                {
                    foreach (BeneficiaryCommitmentList item in kit.BeneficiaryCommitmentList)
                    {
                        result.message = "결연 킷 발송 실패 : " + msg.RequestMessage.ToString();
                    }
                }

            }
            else
            {
                result.message = "결연 킷 발송 실패 : " + err.ErrorMessage;
            }
        }

        return result;
    }

    public JsonWriter UpdateCommitmentMaster(string strCommitmentID, string strGlobalCommitmentID, string strGlobalCorrCommitmentID)
    {
        JsonWriter result = new JsonWriter() { success = false };
        DataTable dt = new DataTable();

        Object[] objSql = new object[] { "sp_U_tCommitmentMaster" };
        Object[] objParam = new object[] { "CommitmentID", "GlobalCommitmentID", "CorrespondentCommitmetID" };
        Object[] objValue = new object[] { strCommitmentID, strGlobalCommitmentID, strGlobalCorrCommitmentID };
        dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

        if (dt.Rows.Count == 0)
        {
            result.success = false;
            result.message = "결연 처리 중 오류가 발생하였습니다.";
            return result;
        }

        result.success = true;
        return result;
    }

    // [이종진] 20171222 생성 - GlobalPool에 NomoneyHold 및
    // KR_Compass4에 NomoneyHold 정보 Insert 및 임시결연정보 Insert
    public JsonWriter NomoneyHoldGP(string commitmentId, string originChildMasterId)
    {
        JsonWriter result = new JsonWriter() { success = false };
        //        string strResult = "";
        DataTable dt = new DataTable();

        //기존방법을 사용하면 바로 return
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();
        if (strdbgp_kind == "1")
        {
            result.success = true;
            return result;
        }
        
        string ChildMasterID = "";
        string childGlobalID = "";
        string strHoldType = "No Money Hold";
        string strHoldEndDateTimeUtc = "";
        string strHoldEndDateTimeKst = "";

        string HoldID = "";
        //[이종진] Compass4에 NomoneyHold 정보 insert시 key
        Guid HoldUID = Guid.NewGuid();
        //Guid HoldUID = new Guid(); 

        var sess = new UserInfo();
        var sponsorID = sess.SponsorID;
        var UserId = sess.UserId;
        var UserName = sess.UserName;

        Object[] objSql = new object[] { "sp_S_tCommitmentMaster" };
        Object[] objParam = new object[] { "SponsorID", "CommitmentID" };
        Object[] objValue = new object[] { sponsorID, commitmentId };
        dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

        if (dt.Rows.Count == 0)
        {
            result.success = false;
            result.message = "결제 실패 처리 중, 결연정보 조회 오류가 발생하였습니다.";
            return result;
        }

        ChildMasterID = dt.Rows[0]["ChildMasterID"].ToString();
        childGlobalID = originChildMasterId.Substring(2);
        HoldID = dt.Rows[0]["HoldId"].ToString();
        strHoldEndDateTimeUtc = dt.Rows[0]["HoldEndDateTimeUtc"].ToString();
        strHoldEndDateTimeKst = dt.Rows[0]["HoldEndDateTimeKst"].ToString();

        if (string.IsNullOrEmpty(HoldID))
        {
            result.success = false;
            return result;
        }

        #region 글로벌 풀에 No Money Hold 저장

        // TCPT Hold 처리용 킷 생성
        BeneficiaryHoldRequestList item = new BeneficiaryHoldRequestList();
        item.Beneficiary_GlobalID = childGlobalID;
        item.BeneficiaryState = strHoldType;
        item.HoldEndDate = strHoldEndDateTimeUtc;
        item.IsSpecialHandling = false;
        item.PrimaryHoldOwner = sponsorID;
        item.GlobalPartner_ID = "KR";
        item.HoldID = HoldID;

        string json = JsonConvert.SerializeObject(item);
        string strHoldResult = string.Empty;
        //        string processname = "";

        CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();

        // 홀드요청
        //strHoldResult = _OffRampService.BeneficiaryHoldRequest_Single_POST(childGlobalID, json);
        //                    processname = "BeneficiaryHoldRequest_Single_POST";

        strHoldResult = _OffRampService.BeneficiaryHoldSingle_PUT(childGlobalID, HoldID, json);
        TCPTResponseMessage updateResult = JsonConvert.DeserializeObject<TCPTResponseMessage>(strHoldResult);

        if (!updateResult.IsSuccessStatusCode)
        {
            strHoldResult = _OffRampService.BeneficiaryHoldRequest_Single_POST(childGlobalID, json);
        }

        TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(strHoldResult);

        //실패
        if (!msg.IsSuccessStatusCode)
        {
            //            result.message = "선택하신 어린이를 등록하지 못했습니다. \\r\\n" +
            //                                            strResult.Substring(2).ToString().Replace("'", "").Replace(@"""", "").Replace("\r", "").Replace("\n", "");
            result.success = false;
            result.message = "결제 실패 처리 중, 어린이 홀드 오류가 발생하였습니다.";
            return result;
        }

        // 성공
        BeneficiaryHoldRequestResponse_Kit response = JsonConvert.DeserializeObject<BeneficiaryHoldRequestResponse_Kit>(msg.RequestMessage.ToString());
        BeneficiaryHoldResponseList_Kit res = JsonConvert.DeserializeObject<BeneficiaryHoldResponseList_Kit>(msg.RequestMessage.ToString());
        response = res.BeneficiaryHoldResponseList[0];

        #endregion

        #region KR_Compass4에 NoMoneyHold 정보 INSERT 및 임시결연정보 INSERT

        // NoMoneyHold 정보 KR_Compass4에 INSERT       
        // 노머니 홀드일 겨우만(결제 시도 후 실패시, 스폰서 글로발 아이디 생성실패 시) 호출 필요
        int n = InsertHoldHistory(response.HoldID, response.Beneficiary_GlobalID, response.Code.ToString(), response.Message, HoldUID, strHoldType, strHoldEndDateTimeKst, UserId, UserName);
        //        InsertHoldHistory(response);
        //        strHoldId = response.HoldID;

        //임시결연정보 Insert - KR_Compass4에 Insert
        try
        {
            string s = _OffRampService.InsertTCPT_CommitmentTemp(commitmentId, sponsorID, ChildMasterID, HoldUID.ToString(), HoldID, sponsorID, "");
        }
        catch
        {

        }
        #endregion

        result.success = true;

        return result;
    }
    
    //NoMoneyHold 정보 KR_Compass4에 INSERT
    public int InsertHoldHistory(string holdID, string childGlobalID, string code, string message, Guid HoldUID, string HoldType, string HoldEDT, string UserId, string UserName)
    {
        Object[] objSql = new object[] { "sp_I_TCPT_Hold_Beneficiary" };
        Object[] objParam = new object[] { "HoldUID", "HoldID", "Beneficiary_GlobalID", "HoldType", "HoldEndDate", "PrimaryHoldOwner", "SecondaryHoldOwner", "IsSpecialHandling", "EstimatedNoMoneyYieldRate", "GlobalPartner_ID", "LastReturnCode", "LastReturnMessage", "NotificationReason", "CreateUserID", "CreateUserName" };
        Object[] objValue = new object[] { HoldUID.ToString(), holdID, childGlobalID, HoldType, HoldEDT, UserId, "", "0", "", "KR", code, message, "", UserId, UserName };

        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
        return 0;
    }

    //NoMoneyHold 정보 KR_Compass4에 DELETE
    public int DeleteHoldHistory(Guid HoldUID)
    {
        Object[] objSql = new object[] { "sp_D_TCPT_Hold_Beneficiary" };
        Object[] objParam = new object[] { "HoldUID"};
        Object[] objValue = new object[] { HoldUID.ToString() };

        DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
        return 0;
    }

    // 결연정보 삭제
    public bool DeleteCommitment(string sCommitmentIDs ) {
		
		try {
			if(!string.IsNullOrEmpty(sCommitmentIDs)) {
				foreach(var sCommitmentID in sCommitmentIDs.Split(',')) {
					_wwwService.deleteCommiment(sCommitmentID);
				}
			}
			return true;	

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			return false;
		}

	}

    // 20171206 고진혁 - 결제 시 어린이 정보 1시간 홀딩정보에서 조회하여 DB 저장
    public JsonWriter InsertChild(string childMasterId, string childKey, string SponsorID, string UserId, string UserName, bool isWedding = false)
    {
        JsonWriter result = new JsonWriter() { success = false, data = childMasterId };
        DataTable dt2 = new DataTable();
        DataTable dt = new DataTable();

        // [이종진] 어린이 정보 체크
        Object[] objSql2 = new object[] { "sp_S_tChildGp_Check" };
        Object[] objParam2 = new object[] { "ChildGlobalID", "ChildKey" };
        Object[] objValue2 = new object[] { childMasterId.Substring(2), childKey };
        dt2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2).Tables[0];

        //어린이 정보가 존재하지 않으면, 어린이 정보 insert함
        if(dt2.Rows[0]["RESULT"].ToString() == "")
        {
            // 어린이 정보 저장
            Object[] objSql = new object[] { "sp_I_tChildGp" };
            Object[] objParam = new object[] { "ChildMasterID", "UserId", "UserName" };
            Object[] objValue = new object[] { childMasterId, UserId, UserName };
            dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

            if (dt.Rows.Count == 0)
            {
                result.message = "어린이 정보 저장 중 오류가 발생하였습니다.";
                result.success = false;
                return result;
            }

            if (dt.Rows[0]["RTNCODE"].ToString() != "1")
            {
                result.message = dt.Rows[0]["RTNMESSAGE"].ToString();
                result.success = false;
                return result;
            }

            //이미지저장
            //ttmpcihldensure.pic binary code만들어서 db에 저장.
            //ltrservice로 파일 업로드
        }
        else //존재할 경우는, ChildMasterID를 존재하는 id로 교체후, 상위 호출한 Method로 ChildMasterID전달하기 위해 result에 넣어줌
        {
            childMasterId = dt2.Rows[0]["RESULT"].ToString();
            
            //[이종진] 쿠키값의 ChildMasterId도 변경해줌
            //var payInfo = PayItemSession.GetCookie(HttpContext.Current);
            //payInfo.childMasterId = dt2.Rows[0]["RESULT"].ToString();
            //PayItemSession.SetCookie(HttpContext.Current, payInfo);
        }
        
        result.data = childMasterId;
        result.success = true;
        return result;
    }

    // 결연정보 등록 CDSP
    bool RegisterCDSPCommitment(string childMasterId ,string campaignId , string sSponsorID, string sSponsorName , string motiveCode , string motiveName , int? sponsorAmount , int month, ref JsonWriter result) {
		
		string sResult = string.Empty;
		string sTimeStamp = DateTime.Now.ToString("yyyyMMddHHmmssff2"); //타임스탬프
		
		//DataSet dsChild = _wwwService.getDATChildEnsure("" , childMasterId, "");
		DataTable dt = _wwwService.listDATChildEnsure("", sSponsorID).Tables[0];

		dt.DefaultView.RowFilter = string.Format("childMasterId='{0}'" , childMasterId);
		
		if(dt.DefaultView.Count == 0) {
			result.message = "결연아동 정보가 없습니다.";
			return false;
		}
		
		//결연정보 등록
		try {
			DataSet dsCommitmentGroup = new DataSet();
			DataTable dtCommitmentGroup = new DataTable();

			//Columns Add
			dtCommitmentGroup.Columns.Add("CommitmentID");
			dtCommitmentGroup.Columns.Add("ChildMasterID");
			dtCommitmentGroup.Columns.Add("SponsorID");
			dtCommitmentGroup.Columns.Add("SponsorTypeEng");
			dtCommitmentGroup.Columns.Add("SponsorItemEng");
			dtCommitmentGroup.Columns.Add("SponsorAmount");
			dtCommitmentGroup.Columns.Add("MotiveCode");
			dtCommitmentGroup.Columns.Add("MotiveCodeName");
			dtCommitmentGroup.Columns.Add("CampaignID");
			dtCommitmentGroup.Columns.Add("FundingFrequency");
			dtCommitmentGroup.Columns.Add("Remark");
			dtCommitmentGroup.Columns.Add("MotiveSponsor_Name");
			dtCommitmentGroup.Columns.Add("MotiveSponsor_TelNo");
			dtCommitmentGroup.Columns.Add("PaymentType");
			
			//Rows Add
			string sChildID = childMasterId;
			string sSponsorTypeEng = "CHISPO";
			string sSponsorItemEng = "DS";
			string sSponsorAmount = (sponsorAmount.HasValue) ? (sponsorAmount.Value).ToString() : dt.DefaultView[0]["SponsorAmount"].ToString();
			string sMotiveCode = motiveCode;
			string sMotiveCodeName = motiveName;
			string sCampaignID = string.IsNullOrEmpty(campaignId) ? "90000000000000000" : campaignId;     //기본캠페인
			string sRemark = ""; //비고

			dtCommitmentGroup.Rows.Add(sTimeStamp, sChildID, sSponsorID
											, sSponsorTypeEng, sSponsorItemEng, sSponsorAmount
											, sMotiveCode, sMotiveCodeName
											, sCampaignID, "매월", sRemark  
											, "", "" );

			if (month > 1) {

				var sCommitmentId = (Convert.ToInt64(sTimeStamp) + 1).ToString();
				sTimeStamp += "," + sCommitmentId;

				sSponsorTypeEng = "";
				sSponsorItemEng = "DSADD";
				sSponsorAmount = (sponsorAmount.Value * (month - 1)).ToString();
				dtCommitmentGroup.Rows.Add(sCommitmentId, sChildID, sSponsorID
					, sSponsorTypeEng, sSponsorItemEng, sSponsorAmount
					, sMotiveCode, sMotiveCodeName
					, sCampaignID, "1회", sRemark
					, "", "");

			}
			
			//DataSet SetData
			dsCommitmentGroup.Tables.Add(dtCommitmentGroup);

			//WWWService Call : CommitmentMaster Insert

			// CDSP(정기) 1개월 이상은 해외카드만
			if(month > 1) {
				sResult = _wwwService.registerCommitment_Foreign(dsCommitmentGroup , CodeAction.ChannelType);
				
			} else {
				sResult = _wwwService.registerCommitment_Group(dsCommitmentGroup , CodeAction.ChannelType);
			}
				
			//이미후원중인 어린이가 있을경우
			if(sResult == "30AlreadyCommitmentChild") {
				result.message = "선택하신 어린이는 이미 결연처리된 어린이가 있습니다. 다시 결연신청을 해주세요.";
				return false;
			}

			//DB Error
			if(sResult.Substring(0, 2) == "30") {
				result.message = "1:1 결연등록 중 오류가 발생했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
				return false;
			}
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "1:1 결연등록 중 오류가 발생했습니다. ";
			return false;
		}

        // TCPT 처리 관련 추가 문희원 2017-04-28
        //try
        //{
        //    RegisterCommitMentTemp(sTimeStamp, childMasterId, sSponsorID);
        //}
        //catch (Exception ex)
        //{
        //    ErrorLog.Write(HttpContext.Current, 0, "RegisterCommitMentTemp:::" + ex.Message);
        //}

		result.success = true;
		result.data = sTimeStamp;		// commitmentId
		return true;
	}

    /*
    bool RegisterCDSPCommitment(string first, string childMasterId, string campaignId, string sSponsorID, string sSponsorName, string motiveCode, string motiveName, int? sponsorAmount, int month, ref JsonWriter result)
    {
        string sResult = string.Empty;
        string sTimeStamp = DateTime.Now.ToString("yyyyMMddHHmmssff2"); //타임스탬프

        //DataSet dsChild = _wwwService.getDATChildEnsure("" , childMasterId, "");
        if(first != null && !first.Equals("1")) {

            ErrorLog.Write(HttpContext.Current, 0, "RegisterCDSPCommitment::listDATChildEnsure  START");

            DataTable dt = _wwwService.listDATChildEnsure("", sSponsorID).Tables[0];

            dt.DefaultView.RowFilter = string.Format("childMasterId='{0}'", childMasterId);

            if (dt.DefaultView.Count == 0)
            {
                ErrorLog.Write(HttpContext.Current, 0, "RegisterCDSPCommitment::listDATChildEnsure  FALSE"); 
                result.message = "결연아동 정보가 없습니다.";
                return false;
            }
        }
        //결연정보 등록
        try
        {
            DataSet dsCommitmentGroup = new DataSet();
            DataTable dtCommitmentGroup = new DataTable();

            //Columns Add
            dtCommitmentGroup.Columns.Add("CommitmentID");
            dtCommitmentGroup.Columns.Add("ChildMasterID");
            dtCommitmentGroup.Columns.Add("SponsorID");
            dtCommitmentGroup.Columns.Add("SponsorTypeEng");
            dtCommitmentGroup.Columns.Add("SponsorItemEng");
            dtCommitmentGroup.Columns.Add("SponsorAmount");
            dtCommitmentGroup.Columns.Add("MotiveCode");
            dtCommitmentGroup.Columns.Add("MotiveCodeName");
            dtCommitmentGroup.Columns.Add("CampaignID");
            dtCommitmentGroup.Columns.Add("FundingFrequency");
            dtCommitmentGroup.Columns.Add("Remark");
            dtCommitmentGroup.Columns.Add("MotiveSponsor_Name");
            dtCommitmentGroup.Columns.Add("MotiveSponsor_TelNo");
            dtCommitmentGroup.Columns.Add("PaymentType");

            //Rows Add
            string sChildID = childMasterId;
            string sSponsorTypeEng = "CHISPO";
            string sSponsorItemEng = "DS";
            //string sSponsorAmount = (sponsorAmount.HasValue) ? (sponsorAmount.Value).ToString() : dt.DefaultView[0]["SponsorAmount"].ToString();
            string sSponsorAmount = (sponsorAmount.Value).ToString();
            string sMotiveCode = motiveCode;
            string sMotiveCodeName = motiveName;
            string sCampaignID = string.IsNullOrEmpty(campaignId) ? "90000000000000000" : campaignId;     //기본캠페인
            string sRemark = ""; //비고

            

            if (first != null && !first.Equals("1"))
            {
                dtCommitmentGroup.Rows.Add(sTimeStamp, sChildID, sSponsorID
                                            , sSponsorTypeEng, sSponsorItemEng, sSponsorAmount
                                            , sMotiveCode, sMotiveCodeName
                                            , sCampaignID, "매월", sRemark
                                            , "", "");

            }
            else
            {
                dtCommitmentGroup.Rows.Add(sTimeStamp, sChildID, sSponsorID
                                              , sSponsorTypeEng, sSponsorItemEng, sSponsorAmount
                                              , sMotiveCode, sMotiveCodeName
                                              , sCampaignID, "1회", sRemark
                                              , "", "");
            }

            if (month > 1)
            {

                var sCommitmentId = (Convert.ToInt64(sTimeStamp) + 1).ToString();
                sTimeStamp += "," + sCommitmentId;

                sSponsorTypeEng = "";
                sSponsorItemEng = "DSADD";
                sSponsorAmount = (sponsorAmount.Value * (month - 1)).ToString();
                dtCommitmentGroup.Rows.Add(sCommitmentId, sChildID, sSponsorID
                    , sSponsorTypeEng, sSponsorItemEng, sSponsorAmount
                    , sMotiveCode, sMotiveCodeName
                    , sCampaignID, "1회", sRemark
                    , "", "");

            }

            //DataSet SetData
            dsCommitmentGroup.Tables.Add(dtCommitmentGroup);

            //WWWService Call : CommitmentMaster Insert

            // CDSP(정기) 1개월 이상은 해외카드만
            if (month > 1)
            {
                ErrorLog.Write(HttpContext.Current, 0, "RegisterCDSPCommitment::registerCommitment_Foreign");
                sResult = _wwwService.registerCommitment_Foreign(dsCommitmentGroup, CodeAction.ChannelType);

            }
            else
            {
                ErrorLog.Write(HttpContext.Current, 0, "RegisterCDSPCommitment::registerCommitment_Group");
                sResult = _wwwService.registerCommitment_Group(dsCommitmentGroup, CodeAction.ChannelType);
            }

            //이미후원중인 어린이가 있을경우
            if (sResult == "30AlreadyCommitmentChild")
            {
                result.message = "선택하신 어린이는 이미 결연처리된 어린이가 있습니다. 다시 결연신청을 해주세요.";
                return false;
            }

            //DB Error
            if (sResult.Substring(0, 2) == "30")
            {
                result.message = "1:1 결연등록 중 오류가 발생했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
                return false;
            }

            ErrorLog.Write(HttpContext.Current, 0, "RegisterCDSPCommitment:::"+sResult+ ":::"+result.message);
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.message = "1:1 결연등록 중 오류가 발생했습니다. ";
            return false;
        }


        // TCPT 처리 관련 추가 문희원 2017-04-28
        //try
        //{
        //    RegisterCommitMentTemp(sTimeStamp, childMasterId, sSponsorID);
        //}
        //catch (Exception ex)
        //{
        //    ErrorLog.Write(HttpContext.Current, 0, "RegisterCommitMentTemp:::" + ex.Message);
        //}

        result.success = true;
        result.data = sTimeStamp;       // commitmentId
        return true;
    }
    */

    // 결연정보 등록 CDSP , 해외카드인경우 마이페이지에서 후원금 결제할때 사용
    public bool RegisterCDSPCommitmentOverseaCardAddOn( string childMasterId, string campaignId, string sSponsorID , int sponsorAmount, int month, ref JsonWriter result ) {

		result.data = "";
		string sResult = string.Empty;
		string sTimeStamp = DateTime.Now.ToString("yyyyMMddHHmmssff2"); //타임스탬프
		
		//결연정보 등록
		try {
			DataSet dsCommitmentGroup = new DataSet();
			DataTable dtCommitmentGroup = new DataTable();

			//Columns Add
			dtCommitmentGroup.Columns.Add("CommitmentID");
			dtCommitmentGroup.Columns.Add("ChildMasterID");
			dtCommitmentGroup.Columns.Add("SponsorID");
			dtCommitmentGroup.Columns.Add("SponsorTypeEng");
			dtCommitmentGroup.Columns.Add("SponsorItemEng");
			dtCommitmentGroup.Columns.Add("SponsorAmount");
			dtCommitmentGroup.Columns.Add("MotiveCode");
			dtCommitmentGroup.Columns.Add("MotiveCodeName");
			dtCommitmentGroup.Columns.Add("CampaignID");
			dtCommitmentGroup.Columns.Add("FundingFrequency");
			dtCommitmentGroup.Columns.Add("Remark");
			dtCommitmentGroup.Columns.Add("MotiveSponsor_Name");
			dtCommitmentGroup.Columns.Add("MotiveSponsor_TelNo");
			dtCommitmentGroup.Columns.Add("PaymentType");

			//Rows Add
			string sChildID = childMasterId;
			string sMotiveCode = "M0001";
			string sMotiveCodeName = "본인의지";
			string sCampaignID = string.IsNullOrEmpty(campaignId) ? "90000000000000000" : campaignId;     //기본캠페인
			string sRemark = ""; //비고
			string sSponsorTypeEng = "";
			string sSponsorItemEng = "DSADD";
			string sSponsorAmount = (sponsorAmount * month).ToString();

			dtCommitmentGroup.Rows.Add(sTimeStamp, sChildID, sSponsorID
				, sSponsorTypeEng, sSponsorItemEng, sSponsorAmount
				, sMotiveCode, sMotiveCodeName
				, sCampaignID, "1회", sRemark
				, "", "");
			
			//DataSet SetData
			dsCommitmentGroup.Tables.Add(dtCommitmentGroup);
			sResult = _wwwService.registerCommitment_Foreign(dsCommitmentGroup , CodeAction.ChannelType);
			
			//이미후원중인 어린이가 있을경우
			if(sResult == "30AlreadyCommitmentChild") {
				result.message = "선택하신 어린이는 이미 결연처리된 어린이가 있습니다. 다시 결연신청을 해주세요.";
				return false;
			}

			//DB Error
			if(sResult.Substring(0, 2) == "30") {
				result.message = "1:1 결연등록 중 오류가 발생했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
				return false;
			}
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "1:1 결연등록 중 오류가 발생했습니다. ";
			return false;
		}


		result.success = true;
		result.data = sTimeStamp;       // commitmentId
		return true;
	}
	
	// 결연정보 등록 CIV
	public bool RegisterCIVCommitment( string sponsorId , string motiveCode, string motiveName, int sponsorAmount , int month , string payMethod , string sFundingFrequency, string codeID, string childMasterId , string campaignID , ref JsonWriter result ) {

		result.data = "";
		DataSet dsChild = new DataSet();
		string sJs = string.Empty;
		string sTimeStamp = _wwwService.GetTimeStamp(); //타임스탬프
		sTimeStamp = sTimeStamp.Substring(0, sTimeStamp.Length - 1) + "0";

		try {
			//Create Tabel
			DataSet dsCommitmentGroup = new DataSet();
			DataTable dtCommitmentGroup = new DataTable();

			//Columns Add
			dtCommitmentGroup.Columns.Add("CommitmentID");
			dtCommitmentGroup.Columns.Add("ChildMasterID");
			dtCommitmentGroup.Columns.Add("SponsorID");
			dtCommitmentGroup.Columns.Add("SponsorTypeEng");
			dtCommitmentGroup.Columns.Add("SponsorItemEng");
			dtCommitmentGroup.Columns.Add("SponsorAmount");
			dtCommitmentGroup.Columns.Add("MotiveCode");
			dtCommitmentGroup.Columns.Add("MotiveCodeName");
			dtCommitmentGroup.Columns.Add("CampaignID");
			dtCommitmentGroup.Columns.Add("FundingFrequency");
			dtCommitmentGroup.Columns.Add("Remark");
			dtCommitmentGroup.Columns.Add("PaymentType");
			dtCommitmentGroup.Columns.Add("MotiveSponsor_Name");
			dtCommitmentGroup.Columns.Add("MotiveSponsor_TelNo");

			//Rows Add
			string sChildID = string.IsNullOrEmpty(childMasterId) ? "0000000000" : childMasterId;
			string sSponsorTypeEng = "";
			string sSponsorItemEng = codeID;
			
			string sCampaignID = campaignID;
			string sRemark = ""; //비고

			if(sFundingFrequency == "1회") {
				dtCommitmentGroup.Rows.Add(sTimeStamp, sChildID, sponsorId
												, sSponsorTypeEng, sSponsorItemEng, (sponsorAmount * month).ToString()
												, "", ""
												, sCampaignID, sFundingFrequency, sRemark, payMethod
												, null, null);
			} else {

				dtCommitmentGroup.Rows.Add(sTimeStamp, sChildID, sponsorId
												, sSponsorTypeEng, sSponsorItemEng, (sponsorAmount ).ToString()
												, motiveCode, motiveName
												, sCampaignID, sFundingFrequency, sRemark, payMethod
												, null, null);


				if(month > 1) {

					var sCommitmentId = (Convert.ToInt64(sTimeStamp) + 1).ToString();
					sTimeStamp += "," + sCommitmentId;
					
					dtCommitmentGroup.Rows.Add(sCommitmentId, sChildID, sponsorId
												, sSponsorTypeEng, sSponsorItemEng, (sponsorAmount * (month - 1)).ToString()
												, "", ""
												, sCampaignID, "1회", sRemark, payMethod
												, null, null);
					
				}

			}
			
			//DataSet SetData
			dsCommitmentGroup.Tables.Add(dtCommitmentGroup);

			//WWWService Call : CommitmentMaster Insert
			//수정 (By 주형준 2011-10-03)
			//string sResult = _wwwService.registerCommitment_Group(dsCommitmentGroup);
			//정기건 일시건 둘다 올수 있고 gift의 경우 중복되므로 campaign코드까지 체크 해야함
			string sResult = _wwwService.registerCommitment_Group_Campaign(dsCommitmentGroup , CodeAction.ChannelType);
			
			//이미후원중인 어린이가 있을경우
			if(sResult == "30AlreadyCommitmentChild") {

				result.message = "이미 기부중이신 양육보완프로그램이 있습니다. 다시 양육보완프로그램 신청을 해주세요.";
				return false;

			}

			//Success
			if(sResult != "10") {
				result.message = "등록 중 오류가 발생했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
				return false;

			}

			//등록된 CommitmentID 넣기
			//sGroupCommitmentID = sGroupCommitmentID;
		} catch(Exception ex) {

			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "등록 중 오류가 발생했습니다. ";
			return false;

		}

		result.success = true;
		result.data = sTimeStamp;       // commitmentId
		return true;

	}

	// 결연정보 등록 CIV 선물금
	bool RegisterCommitmentPresent( string sponsorId, string childMasterIds , string money, string payMethod, string sFundingFrequency, string codeID, string month , ref JsonWriter result ) {

		DataSet dsChild = new DataSet();
		string sJs = string.Empty;
		string sTimeStamp = _wwwService.GetTimeStamp(); //타임스탬프
		sTimeStamp = sTimeStamp.Substring(0, sTimeStamp.Length - 1) + "0";
		string commitmentIds = "";

		if(string.IsNullOrEmpty(childMasterIds)) {
			result.message = "어린이 아이디가 없습니다.";
			return false;
		}

		try {

			DataSet dsCommitmentGroup = new DataSet();
			DataTable dtCommitmentGroup = new DataTable();

			//Columns Add
			dtCommitmentGroup.Columns.Add("CommitmentID");
			dtCommitmentGroup.Columns.Add("ChildMasterID");
			dtCommitmentGroup.Columns.Add("SponsorID");
			dtCommitmentGroup.Columns.Add("SponsorTypeEng");
			dtCommitmentGroup.Columns.Add("SponsorItemEng");
			dtCommitmentGroup.Columns.Add("SponsorAmount");
			dtCommitmentGroup.Columns.Add("SponsorMonth");
			dtCommitmentGroup.Columns.Add("MotiveCode");
			dtCommitmentGroup.Columns.Add("MotiveCodeName");
			dtCommitmentGroup.Columns.Add("CampaignID");
			dtCommitmentGroup.Columns.Add("FundingFrequency");
			dtCommitmentGroup.Columns.Add("Remark");
			dtCommitmentGroup.Columns.Add("PaymentType");
			dtCommitmentGroup.Columns.Add("MotiveSponsor_Name");
			dtCommitmentGroup.Columns.Add("MotiveSponsor_TelNo");

			// 생일선물금 month 는 DAT에서 계산
			foreach(var childMasterId in childMasterIds.Split(',')) {
				string sChildID = childMasterId.Trim();
				
				dtCommitmentGroup.Rows.Add(sTimeStamp, sChildID, sponsorId
											 , "", codeID, money , month 
											 , "M0001", "본인의지"
											 , "90000000000000000", sFundingFrequency , "", payMethod
											 , "", "");

				commitmentIds += "," + sTimeStamp;
				sTimeStamp = Convert.ToInt64(Convert.ToInt64(sTimeStamp) + 1).ToString();
			}

			//DataSet SetData
			dsCommitmentGroup.Tables.Add(dtCommitmentGroup);

			string sResult = "";
			if(sFundingFrequency == "매년") {
				sResult = _wwwService.registerCommitment_GroupYear(dsCommitmentGroup , CodeAction.ChannelType);
			} else {
				sResult = _wwwService.registerCommitment_Group(dsCommitmentGroup , CodeAction.ChannelType);
			}

			//이미후원중인 어린이가 있을경우
			if(sResult == "30AlreadyCommitmentChild") {
				if(codeID == "BG") {
					result.message = "이미 처리된 선물금이 있습니다.";
					return false;
				} else {
					result.message = "이미 선물금 예약이 등록되어 있습니다.";
					return false;
				}
			}

			//Success
			if(sResult == "10") {
				result.success = true;
				result.data = commitmentIds.Substring(1);       
				return true;
			}else {
				if(codeID == "BG") {
					result.message = "생일선물금 등록 중 오류가 발생했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
					return false;
				} else {
					result.message = "선물금 등록 중 오류가 발생했습니다. \\r\\n" + sResult.Substring(2).ToString().Replace("\n", "");
					return false;
				}
			}

		} catch(Exception ex) {

			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "등록 중 오류가 발생했습니다. ";
			return false;

		}

		

	}


	#region CIV 등록가능여부 체크 , CIVValidate
	// frequency = 정기 , 일시
	// codeId = PBIV 등
	// 일시는 로그인한 회원만 유효성 체크
	public void ValidateCIV( string sSponsorID, string frequency , string codeId , string codeName , string campaignId , ref JsonWriter result ) {
		//이미 양육보완하고있는지 확인
		try {
			
			if (string.IsNullOrEmpty(sSponsorID)) {
				result.success = true;
				return;
			}

			//결연중인 목록 가져오기 : 일반후원만
			string sAlready = string.Empty;

			DataTable dtActiveChild = new DataTable();
			DataSet dsActiveChild = _wwwService.listActiveCommitment(sSponsorID, "0000000000", "", "");
			
			//결연중인 목록에서 정기일반후원항목이 있는지 확인한다.            

			DataRow[] alreadyRow = dsActiveChild.Tables[0].Select("SponsorItemEng = '" + codeId + "' and FundingFrequency <> '1회' and CampaignID='" + campaignId + "'");

			//정기후원이면서 이미 납부중인 양육보완프로그램이 있을경우 (일시후원은 항상 등록가능)
			if(alreadyRow.Length > 0) {

				if(frequency == "정기") {
					result.message = codeName + "\\n항목은 이미 기부하고 계신 프로그램입니다."
												+ "\\n후원 내역을 변경하고 싶으실 경우,"
												+ "\\n(02)740-1000으로 문의해주세요.평일 9시~18시/공휴일제외";
					result.success = false;
					return;
				} else {
					result.message = "이미 정기결제가 예약되어 있습니다.\\n예약된 금액 외에 지금 추가로 결제하기 원하십니까?";
					result.action = "confirm";

				}
				
			}


		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "선택하신 양육보완프로그램이 이미 기부중인지 확인하는 중 오류가 발생했습니다.";
			result.success = false;
			return;
		}

		result.success = true;
	}
	#endregion

	#region CDSP 등록가능여부
	public bool ValidateCDSP( string childMasterId , string campaignId, string codeName , string sSponsorID, ref JsonWriter result ) {

		result.success = true;

		// 후원자 기본정보 유효성
		if(!GetSonsorValidation(sSponsorID, ref result)) {
			return false;
		}

		// 기본 후원이 아니고 캠페인 후원의 경우 중복되는 값이 있는지 체크
		if(campaignId != "90000000000000000" && !string.IsNullOrEmpty(codeName) && !IsExistExtraCDSPCampaign(sSponsorID , campaignId , codeName , ref result)) {
			return false;
		}

		// 미납금이 있을경우
		if(!GetNonPayment(sSponsorID, ref result)) {
			return false;
		}

		// 후원자취소가 3건이상일경우 
		if(!GetSponsorCancel(sSponsorID, ref result)) {
			return false;
		}

        // keep 하고 1시간 경과

        if (!GetTimeover(childMasterId, sSponsorID, ref result))
        {
            return false;
        }
        return true;
	}

    public bool ValidateCDSP(string first, string childMasterId, string campaignId, string codeName, string sSponsorID, ref JsonWriter result)
    {

        result.success = true;

        ErrorLog.Write(HttpContext.Current, 0, "ValidateCDSP::GetSonsorValidation  START");
        // 후원자 기본정보 유효성
        if (!GetSonsorValidation(sSponsorID, ref result))
        {
            ErrorLog.Write(HttpContext.Current, 0, "ValidateCDSP::GetSonsorValidation  FALSE");
            return false;
        }


        // 기본 후원이 아니고 캠페인 후원의 경우 중복되는 값이 있는지 체크
        if (campaignId != "90000000000000000" && !string.IsNullOrEmpty(codeName) && !IsExistExtraCDSPCampaign(sSponsorID, campaignId, codeName, ref result))
        {
            return false;
        }

        ErrorLog.Write(HttpContext.Current, 0, "ValidateCDSP::GetNonPayment  START");
        // 미납금이 있을경우
        if (!GetNonPayment(sSponsorID, ref result))
        {
            ErrorLog.Write(HttpContext.Current, 0, "ValidateCDSP::GetNonPayment  FALSE");
            return false;
        }

        ErrorLog.Write(HttpContext.Current, 0, "ValidateCDSP::GetSponsorCancel  START");
        // 후원자취소가 3건이상일경우 
        if (!GetSponsorCancel(sSponsorID, ref result))
        {
            ErrorLog.Write(HttpContext.Current, 0, "ValidateCDSP::GetSponsorCancel  FALSE");
            return false;
        }

        // keep 하고 1시간 경과
        if (first != null && !first.Equals("1"))
        {
            ErrorLog.Write(HttpContext.Current, 0, "ValidateCDSP::GetTimeover  START");
            if (!GetTimeover(childMasterId, sSponsorID, ref result))
            {
                ErrorLog.Write(HttpContext.Current, 0, "ValidateCDSP::GetTimeover  FALSE");
                return false;
            }
        }

        return true;
    }
    #endregion

    #region CDSP 중복캠페인 여부 확인
    // frequency = 정기 , 일시
    // codeId = PBIV 등
    // 일시는 로그인한 회원만 유효성 체크
    public bool IsExistExtraCDSPCampaign( string sSponsorID, string campaignId, string codeName , ref JsonWriter result ) {
		//이미 양육보완하고있는지 확인
		try {

			if(string.IsNullOrEmpty(sSponsorID)) {
				result.success = false;
				result.message = "회원 아이디가 없습니다.";
				return false;
			}

			string codeId = "DS";
			//결연중인 목록 가져오기 : 일반후원만
			string sAlready = string.Empty;

			DataTable dtActiveChild = new DataTable();
			DataSet dsActiveChild = _wwwService.listActiveCommitment(sSponsorID, "", "", "");
			
			//결연중인 목록에서 정기일반후원항목이 있는지 확인한다.            

			DataRow[] alreadyRow = dsActiveChild.Tables[0].Select("SponsorItemEng = '" + codeId + "' and FundingFrequency <> '1회' and CampaignID='" + campaignId + "'");
			//정기후원이면서 이미 납부중인 양육보완프로그램이 있을경우 (일시후원은 항상 등록가능)
			if(alreadyRow.Length > 0) {

				result.message = codeName + "\\n항목은 이미 기부하고 계신 프로그램입니다."
											+ "\\n문의 사항은 후원지원팀으로 문의해주세요"
												+ "\\n후원지원팀 (02-740-1000, 평일 9시~18시/공휴일제외)";
				result.success = false;
				return false;

			}


		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "선택하신 1:1 어린이 양육 프로그램이 이미 기부중인지 확인하는 중 오류가 발생했습니다.";
			result.success = false;
			return false;
		}

		result.success = true;
		return true;
	}
	#endregion

	#region 후원자 정보 유효성
	bool GetSonsorValidation( string sSponsorID, ref JsonWriter result ) {
		// 유효성 체크
		Object[] objSql = new object[1] { " SELECT ci , di , firstname , lastname , parentjuminid , birthdate , userClass FROM tSponsorMaster WHERE CurrentUse = 'Y' and sponsorId = '" + sSponsorID + "'" };
		DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
	
		if(ds.Tables[0].Rows.Count > 0) {
			var firstname = ds.Tables[0].Rows[0]["firstname"].ToString().Trim();
			var lastname = ds.Tables[0].Rows[0]["lastname"].ToString().Trim();
			var ci = ds.Tables[0].Rows[0]["ci"].ToString().Trim();
			var parentjuminid = ds.Tables[0].Rows[0]["parentjuminid"].ToString().Trim();
			var userClass = ds.Tables[0].Rows[0]["userClass"].ToString().Trim();

			if(string.IsNullOrEmpty(firstname) || string.IsNullOrEmpty(lastname)) {
				result.message = "영문명이 없으면 결연을 맺으실 수 없습니다. \\r\\n회원정보수정화면에서 영문명을 등록 후 다시 시도해주세요.";
				result.success = false;
				return false;
			}

			// CI 값이 없는경우 국외 회원
			/* 
			if(string.IsNullOrEmpty(ci)) {
				result.message = "본인인증정보가 없으면 결연을 맺으실 수 없습니다.";
				result.success = false;
				return false;
			}
			*/

			// 20세 미만의 경우 부모동의 
			/*
			if(userClass != "기업" && userClass != "기타") {
				var age = Convert.ToDateTime(ds.Tables[0].Rows[0]["birthdate"].ToString().Trim().Substring(0, 10)).GetAge();        // 만나이
				if(age < 20 && string.IsNullOrEmpty(parentjuminid)) {
					result.message = "만 20세 미만인 경우 부모님의 동의가 필요합니다.";
					result.success = false;
					return false;
				}
			}
			*/

		} else {
			result.message = "회원정보를 확인 중 오류가 발생했습니다.";
			result.success = false;
			return false;
		}

		DataSet dsPaymentType = _wwwService.selectUsingPaymentAccount(sSponsorID, "");

		if(dsPaymentType.Tables[0].Rows.Count > 0) {
			//지로용지 수신가능한 주소가 있는지 체크	
			try {
				DataSet dsGiro = _wwwService.selectUsingPaymentAccount(sSponsorID, "0005");

				if(dsGiro == null) {
					result.message = "지로용지 수신지 정보를 확인 중 오류가 발생했습니다.";
					result.success = false;
					return false;

				} else if(dsGiro.Tables["NowusingAccountT"].Rows.Count > 0 && dsGiro.Tables["NowusingAccountT"].Rows[0]["Account"].ToString() == "") {
					result.message = "지로용지 수신지 정보가 없습니다. \\r\\n주소와 수신동의 여부를 확인해주세요." + dsGiro.ToJson();
					result.success = false;
					return false;
				}
			} catch(Exception ex) {
				ErrorLog.Write(HttpContext.Current, 0, ex.Message);
				result.message = "지로용지 수신지 정보를 확인 중 서비스오류가 발생했습니다";
				result.success = false;
				return false;
			}
		}

		return true;
	}
	#endregion

	#region 어린이 선택시간 초과
	bool GetTimeover( string childMasterId , string sSponsorID, ref JsonWriter result ) {
        DataTable dt1 = new DataTable();
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();

        string UserId = "";
        if (UserInfo.IsLogin)
        {
            UserId = new UserInfo().UserId;
        }

        try
        {

            //----- 어린이장바구니목록 가져오기
            if (strdbgp_kind == "1") // 기존 방식 -어린이 리스트 DB조회
            {
                DataSet dsChildEnsure = _wwwService.getDATChildEnsure("", childMasterId, "");
                //----- Row가 0건이면
                if (dsChildEnsure.Tables[0].Rows.Count == 0)
                {
                    result.message = "이미 결연 되었거나, 어린이를 선택하신 지 1시간이 경과되었습니다.\\r\\n결연을 희망하시는 어린이를 다시 확인해주세요!";
                    result.success = false;
                    return false;
                }

            }
            else if (strdbgp_kind == "2") // 신규 방식 -어린이 리스트 Global Pool조회
            {
                Object[] objSql1 = new object[] { "sp_S_tTmpChildEnsure" };
                Object[] objParam1 = new object[] { "Kind", "SessionID", "ChildMasterID", "UserID" };
                Object[] objValue1 = new object[] { "02", "", childMasterId, UserId };
                dt1 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql1, "SP", objParam1, objValue1).Tables[0];

                if (dt1.Rows.Count == 0)
                {
                    result.message = "이미 결연 되었거나, 어린이를 선택하신 지 1시간이 경과되었습니다.\\r\\n결연을 희망하시는 어린이를 다시 확인해주세요!";
                    result.success = false;
                    return false;
                }
            }

            result.success = true;
            return true;
        }
        catch (Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "선택하신 어린이 선택시간을 체크 중 오류가 발생했습니다. \\r\\n다시 시도해주세요.";
			result.success = false;
			return false;
		}

		return true;
	}
    #endregion

    #region 총미납금 가져오기
    public bool GetNonPayment(string sSponsorID , ref JsonWriter result ) {
		
		string sTotalMoney = string.Empty; //총미납금   

		try {

			DataSet dsData = new DataSet();
			dsData = _wwwService.DeliquentPayment_List(sSponsorID);

			//미납금 로드 오류
			if(dsData.Tables[0] == null) {
				result.message = "미납금 정보를 가져오지 못했습니다.";
				result.success = false;
				return false;
			}
			//미납금 로드 성공 : 미납금이 없을경우
			else if(string.IsNullOrEmpty(dsData.Tables[2].Rows[0][1].ToString()) || dsData.Tables[2].Rows[0][1].ToString() == "0") {
				return true;
			}
			//미납금 로드 성공 : 미납금이 있을경우
			else {
				result.message = "지연된 후원금이 존재합니다.\\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다.";
				result.success = false;
				return false;
			}

			/*
			//미납금 로드
			DataSet dsNon = _wwwService.NonPayAccount(sSponsorID);

			//미납금 로드 오류
			if(dsNon.Tables["tNonPayment"] == null) {
				result.message = "미납금 정보를 가져오지 못했습니다.";
				result.success = false;
				return false;
			}
			//미납금 로드 성공 : 미납금이 없을경우
			else if(dsNon.Tables["tNonPayment"].Rows.Count == 0 || dsNon.Tables["tNonPayment"].Rows[0]["NonPayment"].ToString().Trim() == "0") {
				return true;
			}
			//미납금 로드 성공 : 미납금이 있을경우
			else {
				result.message = "지연된 후원금이 존재합니다.\\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다.";
				result.success = false;
				return false;
			}
			*/

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "미납금 정보를 가져오지 못했습니다.";
			result.success = false;
			return false;
		}
	}
	#endregion

	#region 후원자취소 가져오기
	bool GetSponsorCancel( string sSponsorID, ref JsonWriter result ) {

		int iStopCount = 0;
		
		var sess = new UserInfo();

		try {

			/*
			DataSet dsCommitment = _wwwService.listStopCommitment(sSponsorID, "", "", "");
			
			if(dsCommitment == null) {
				result.message = "결연정보를 가져오지 못했습니다.";
				result.success = false;
				return false;
			} else if(dsCommitment.Tables[0].Rows.Count == 0) {
				return true;
			} else {
				for(int i = 0; i < dsCommitment.Tables[0].Rows.Count; i++) {
					if(dsCommitment.Tables[0].Rows[i]["DelinkType"].ToString() == "PtcpCh" ||
						dsCommitment.Tables[0].Rows[i]["DelinkType"].ToString() == "Rsgn" ||
						//dsCommitment.Tables[0].Rows[i]["DelinkType"].ToString() == "SpCh" ||  //주석처리 2013-10-17
						dsCommitment.Tables[0].Rows[i]["DelinkType"].ToString() == "Spsbcg" ||
						dsCommitment.Tables[0].Rows[i]["DelinkType"].ToString() == "TypChg"
					   ) {
						iStopCount++;
					}
				}
				if(iStopCount > 3) {
					result.message = "후원자님께서 취소하신 결연건수가 3건 이상이므로 새로 결연을 맺으실 수 없습니다.";
					result.success = false;
					return false;
				}
			}
			*/

			Object[] objParam = new object[] { "sponsorId"};
			Object[] objValue = new object[] { sess.SponsorID};
			Object[] objSql = new object[] { "sp_web_commitment_cancel_count_f" };
			DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			iStopCount = Convert.ToInt32(list.Rows[0]["cnt"].ToString());

			if(iStopCount > 3) {
				result.message = "후원자님께서 취소하신 결연건수가 3건 이상이므로 새로 결연을 맺으실 수 없습니다.";
				result.success = false;
				return false;
			}

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "결연정보를 가져오는 중 오류가 발생했습니다.";
			result.success = false;
			return false;
		}

		return true;
	}
	#endregion

	// 후원내역 , fundingfrequency = 일시후원 , 정기후원
	public JsonWriter GetCommitmentList( int page, int rowsPerPage , string fundingfrequency, string sponsorItemEng = "") {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

		try {

			Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId" , "fundingfrequency", "sponsorItemEng" };
            Object[] objValue = new object[] { page, rowsPerPage, sess.SponsorID , fundingfrequency, sponsorItemEng };
			Object[] objSql = new object[] { "sp_web_commitment_list_f" };
			DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			list.Columns.Add("pic");
			list.Columns.Add("age");

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                foreach (DataRow dr in list.Rows)
                {

                    var childMasterId = dr["childMasterId"].ToString();

                    if (!string.IsNullOrEmpty(dr["childKey"].ToString()) && dr["childKey"].ToString() != "0000000000")
                    {
                        dr["pic"] = new ChildAction().GetChildImage(dr["childMasterId"].ToString(), dr["ChildKey"].ToString());
                        //ChildAction.GetPicByChildKey(dr["childKey"].ToString());
                        if (dr["birthdate"] != null && dr["birthdate"].ToString().Length > 10)
                        {
                            dr["age"] = Convert.ToDateTime(dr["birthdate"].ToString().Trim().Substring(0, 10)).GetAge();
                        }
                    }

                    var commitmentid = dr["commitmentid"].ToString();
                    var campaignid = dr["campaignid"].ToString();
                    var accountClass = dr["accountClass"].ToString();


                    if (campaignid == "20131106162453202")
                    {     // 첫생일 첫나눔
                        dr["pic"] = "/common/img/mail/mail_img_BR.jpg".WithFileServerHost();
                    }
                    else if (campaignid == "20150128114815082")
                    {     // 결혼 첫나눔
                        dr["pic"] = "/common/img/mail/mail_img_WD.jpg".WithFileServerHost();
                    }

                    //var uf = dao.sp_tUserFundingUser_get_f(commitmentid).FirstOrDefault();
                    Object[] op1 = new Object[] { "CommitmentID" };
                    Object[] op2 = new Object[] { commitmentid };
                    var uf = www6.selectSP("sp_tUserFundingUser_get_f", op1, op2).DataTableToList<sp_tUserFundingUser_get_fResult>().FirstOrDefault();

                    // 나눔펀딩 아닌경우
                    if (uf == null)
                    {

                        if (childMasterId == "0000000000")
                        {        // 어린이 후원이 아닌경우 특별한 나눔에서 조회

                            //var sf = dao.tSpecialFunding.FirstOrDefault(p => p.CampaignID == campaignid && p.AccountClass == accountClass);
                            var sf = www6.selectQF<tSpecialFunding>("CampaignID", campaignid, "AccountClass", accountClass);
                            if (sf != null)
                            {
                                dr["pic"] = sf.sf_thumb.WithFileServerHost();
                            }
                        }

                        // 나눔펀딩인 경우
                    }
                    else
                    {
                        dr["accountclassname"] = string.Format("나눔펀딩");
                        dr["accountclassdetail"] = string.Format("{0}", uf.uf_title);

                        if (childMasterId == "0000000000")
                        {        // 어린이 후원이 아닌경우 등록된 이미지 사용
                            dr["pic"] = uf.uf_image.WithFileServerHost();
                        }
                    }

                }

            }

			result.data = list;

			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "후원내역 정보를 가져오는 중 오류가 발생했습니다"+ ex.Message;
			result.success = false;

		}

		return result;
	}

	// 정기결제시 1회 즉시결제 -> 결제정보 insert
	public JsonWriter BatchPay( string paymentAccountID , string commitmentIds , string good_mny, string cardCd, string cardName, string appNo, string appTime, string tno, string ordr_idxx ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

		try {
			// 0003 자동결제
			// 0022 신용카드즉시결제
			var sResult = _wwwService.RegisterPaymentMasterBase(paymentAccountID , "0022", sess.SponsorID
										, good_mny
										, cardCd
										, cardName
										, appNo
										, appTime
										, "10"
										, tno
										, ordr_idxx
										, "00"
										, "N"
										, CodeAction.ChannelType
										, ""
										, sess.UserId
										, sess.UserName);

			if(sResult.Length >= 2 && sResult.Substring(0, 2).Equals("30")) {

                //[이종진] 2017-12-20 : 결연정보 삭제하는 로직 제거 -> Log쌓기
                //this.DeleteCommitment(commitmentIds);
                string errMsg = "paymentAccountID : " + paymentAccountID;
                errMsg += ", paymentType : 0022";
                errMsg += ", SponsorID : " + sess.SponsorID;
                errMsg += ", Amount : " + good_mny;
                errMsg += ", BankCode : " + cardCd;
                errMsg += ", BankName : " + cardName;
                errMsg += ", AppNo : " + appNo;
                errMsg += ", AppTime : " + appTime;
                errMsg += ", ProcessCode : 10";
                errMsg += ", KCPTNo : " + tno;
                errMsg += ", NotificationID : " + ordr_idxx;
                errMsg += ", CardInstallment : 00";
                errMsg += ", NoFee : N";
                errMsg += ", ChannelType : " + CodeAction.ChannelType;
                errMsg += ", Remarks : ";
                errMsg += ", UserID : " + sess.UserId;
                errMsg += ", UserName : " + sess.UserName;
                ErrorLog.Write(HttpContext.Current, 30, errMsg);
				result.message = sResult.Substring(2);
				return result;
			} else {

				try {
					// partitioning
					sResult = _wwwService.paymentPartitiong_CommitmentGroup(ordr_idxx, commitmentIds.Split(','), sess.SponsorID, sess.UserName);
				} catch(Exception ex) {
					ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
					result.message = "결제정보 저장에 실패했습니다.";
					return result;
				}

				if(!this.updatePTD(commitmentIds)) {
					result.message = "결제정보 저장에 실패했습니다.(PTD)";
					return result;
				}

			}


			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "결제 정보를 저장 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

    private bool updatePTD(string sponsorid, string childmasterid, ref JsonWriter result)
    {
        String sResult = String.Empty;

        try
        {

            string PTD = Convert.ToDateTime(DateTime.Now.AddMonths(1).ToString("yyyy-MM-01")).AddDays(-1).ToString("yyyy-MM-dd");
            string fundingDate = DateTime.Now.ToString("yyyy-MM-dd");
           

                Object[] objSql = new object[1] { " SELECT CommitmentID, SponsorItemEng " +
                                                " FROM   tCommitmentMaster " +
                                                " WHERE  sponsorid = '" + sponsorid + "' AND ChildMasterID='"+childmasterid+"' AND SPONSORTYPEENG='CHISPO' AND STOPDATE IS NULL  " };

                DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

            ErrorLog.Write(HttpContext.Current, 0, "updatePTD  :: "+ds.Tables[0].Rows.Count);

            if (ds.Tables[0].Rows.Count > 0)
                {
                    
                        {
                            Object[] objSql2 = new object[1] { " UPDATE tCommitmentMaster " +
                                                        " SET   PTD = '" + PTD + "' ,FirstFundedDate = GETDATE()" +
                                                        " WHERE  CommitmentID = '" + ds.Tables[0].Rows[0]["CommitmentID"].ToString() + "' " };

                    ErrorLog.Write(HttpContext.Current, 0, " UPDATE tCommitmentMaster " +
                                                        " SET  PTD = '" + PTD + "' ,FirstFundedDate = GETDATE()" +
                                                        " WHERE  CommitmentID = '" + ds.Tables[0].Rows[0]["CommitmentID"].ToString() + "' ");

                    DataSet iResult = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "Text", null, null);
                        }

                result.data = ds.Tables[0].Rows[0]["CommitmentID"].ToString();       // commitmentId
            }

            

            return true;
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            return false;
        }
    }

    private bool updatePTD( string sCommitmentIDs ) {
		String sResult = String.Empty;

		try {
			
			string PTD = Convert.ToDateTime(DateTime.Now.AddMonths(1).ToString("yyyy-MM-01")).AddDays(-1).ToString("yyyy-MM-dd");
			string fundingDate = DateTime.Now.ToString("yyyy-MM-dd");
			foreach(var sCommitmentID in sCommitmentIDs.Split(',')) {

				Object[] objSql = new object[1] { " SELECT SponsorItemEng " +
												" FROM   tCommitmentMaster " +
												" WHERE  CommitmentID = '" + sCommitmentID + "' " };

				DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

				if(ds.Tables[0].Rows.Count > 0) {
					if(ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "DS" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "LS" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "DSADD" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "LSADD") {

						{
							Object[] objSql2 = new object[1] { " UPDATE tCommitmentMaster " +
														" SET  fundingDate = '"+ fundingDate +"' , PTD = '" + PTD + "'" +
														" WHERE  CommitmentID = '" + sCommitmentID + "' " };

							DataSet iResult = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "Text", null, null);
						}
					}

					// 2016-11-25
					// 오픈 후 신규 요청에 의해 fundingDate 를 익월 1일로 변경
					{

						fundingDate = DateTime.Now.AddMonths(1).ToString("yyyy-MM-01");
						Object[] objSql2 = new object[1] { " UPDATE tCommitmentMaster " +
														" SET  fundingDate = '"+ fundingDate +"'" +
														" WHERE  CommitmentID = '" + sCommitmentID + "' " };

						DataSet iResult = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "Text", null, null);
					}
					


				}

			}

			return true;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			return false;
		}
	}


    // TCPT 처리  후원자 GlobalCommitmentID 생성, 어린이 No Money Hold, TCPT_CommitmentTemp 테이블에 Insert
    private void RegisterCommitMentTemp(string sTimeStamp, string childMasterId, string sponsorid)
    {
        CommonLib.TCPTService.ServiceSoap _tcptService;
        _tcptService = new CommonLib.TCPTService.ServiceSoapClient();

        bool regGlobalID = false;
        bool updateHold = false;

        // 문희원 테스트용 추가
        string test = "";
        try
        {
            test += "결연정보 조회 / ";
            //결연정보 조회
            DataSet ds = GetCommitmentInfo(sTimeStamp, childMasterId, sponsorid);

            if (ds == null || ds.Tables.Count == 0)
            {
                test += "결연정보 조회 실패 / ";
            }
            else
            {
                //결연정보 조회되지 않음
                if (ds.Tables[0].Rows.Count == 0)
                {
                    test += "결연정보 조회 되지 않음 / ";
                }
                else
                {
                    test += "결연정보 조회됨 / ";
                    // GlobalSponsorID  생성
                    DataRow dr = ds.Tables[0].Rows[0];

                    bool regSuccess = false;
                    string result = string.Empty;
                    string userid = "";
                    string userName = "";
                    string GlobalSponsorID = string.Empty;
                    string ChildGlobalID = dr["ChildGlobalID"].ToString();
                    string CommitmentID = dr["CommitmentID"].ToString();
                    string holdID = dr["HoldID"].ToString();
                    string holdUID = dr["HoldUID"].ToString();
                    string holdType = dr["HoldType"].ToString();
                    string conid = "54-" + dr["ConID"].ToString();
                    string firstName = dr["FirstName"].ToString();
                    string lastName = dr["LastName"].ToString();
                    string genderCode = dr["GenderCode"].ToString();
                    if (dr["SponsorGlobalID"].ToString() != "")
                    {
                        GlobalSponsorID = dr["SponsorGlobalID"].ToString();
                        regSuccess = true;
                    }

                    if (genderCode == "M")
                        genderCode = "Male";
                    else if (genderCode == "F")
                        genderCode = "Female";

                    if (string.IsNullOrEmpty(GlobalSponsorID))
                    {
                        test += "Sponsor GlobalID  생성 시작 / ";

                        try
                        {
                            SupporterCreateInSFCI_POST kit = new SupporterCreateInSFCI_POST();
                            SupporterProfile profile = new SupporterProfile();

                            profile.GlobalPartner = "Compassion Korea";
                            profile.CorrespondenceDeliveryPreference = "Digital";
                            profile.FirstName = firstName;
                            profile.LastName = lastName;
                            if (!string.IsNullOrEmpty(genderCode))
                                profile.Gender = genderCode;
                            profile.GPID = "54-" + conid;
                            profile.PreferredName = "";
                            profile.Status = "Active";
                            profile.StatusReason = "New";
                            profile.MandatoryReview = false;

                            kit.SupporterProfile.Add(profile);

                            string json = JsonConvert.SerializeObject(kit);
                            result = _tcptService.SupporterCreateInSFCI_POST(json);
                            TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(result);
                            if (msg.IsSuccessStatusCode)
                            {
                                test += "Sponsor GlobalID  생성 성공 / ";
                                SupporterProfileResponse_Kit response = JsonConvert.DeserializeObject<SupporterProfileResponse_Kit>(msg.RequestMessage.ToString());
                                GlobalSponsorID = response.SupporterProfileResponse[0].GlobalID;

                                test += "Sponsor GlobalID  생성 성공 " + GlobalSponsorID + " / ";
                                test += "Sponsor GlobalID  업데이트 시작 / ";
                                bool b = _tcptService.UpdateSponsorGlobalID(sponsorid, GlobalSponsorID);

                                if (b)
                                {
                                    regGlobalID = true;
                                    test += "Sponsor GlobalID  업데이트 성공 / ";
                                }
                                else
                                {
                                    test += "Sponsor GlobalID  업데이트 실패 / ";
                                }
                            }
                            else
                            {
                                test += "Sponsor GlobalID  생성 실패 " + msg.RequestMessage.ToString() + " / ";
                                //오류
                            }
                        }
                        catch (Exception ex)
                        {
                            test += "Sponsor GlobalID  생성 오류 " + ex.Message + "/ ";
                        }
                    }


                    test += "no money hold 로 업데이트 시작 / ";
                    //  no money hold 로 업데이트
                    //if (!string.IsNullOrEmpty(GlobalSponsorID))
                    //{

                    string endDT = DateTime.Now.AddMonths(3).ToString("yyyy-MM-dd");

                    try
                    {
                        BeneficiaryHoldRequestList hKit = new BeneficiaryHoldRequestList();
                        hKit.Beneficiary_GlobalID = ChildGlobalID;
                        hKit.BeneficiaryState = "No Money Hold";
                        hKit.HoldEndDate = endDT + "T23:59:59Z";
                        hKit.IsSpecialHandling = false;
                        hKit.PrimaryHoldOwner = conid;
                        hKit.GlobalPartner_ID = "KR";
                        hKit.HoldID = holdID;

                        string hJson = JsonConvert.SerializeObject(hKit);

                        result = _tcptService.BeneficiaryHoldSingle_PUT(ChildGlobalID, hKit.HoldID, hJson);
                        TCPTResponseMessage updateResult = JsonConvert.DeserializeObject<TCPTResponseMessage>(result);
                        if (updateResult.IsSuccessStatusCode)
                        {
                            test += "no money hold 로 업데이트 성공 / ";
                            test += "기존 hold  expired  처리 시작 / ";
                            // 기존 hold  expired  처리
                            bool b = _tcptService.UpdateHoldStatus(new Guid(holdUID), holdID, hKit.BeneficiaryState, endDT, "Expired", "2000", "", sponsorid, sponsorid);

                            if (b)
                            {
                                test += "기존 hold  expired  처리 성공 / ";
                            }
                            else
                            {
                                test += "기존 hold  expired  처리 실패 / ";
                            }
                            // 신규  hold insert
                            Guid newHoldUID = Guid.NewGuid();
                            test += "신규 hold insert / ";
                            b = _tcptService.InsertHoldHistory(newHoldUID, holdID, ChildGlobalID, hKit.BeneficiaryState, endDT, sponsorid, "", "2000", "web 어린이 결연");
                            if (b)
                            {
                                test += "신규 hold insert 성공 / ";
                            }
                            else
                            {
                                if (b)
                                {
                                    test += "신규 hold insert 성공 / ";
                                    updateHold = true;
                                }
                                else
                                {
                                    test += "신규 hold insert 실패 / ";
                                }
                            }

                            // TCPT_CommitmentTemp 테이블에 insert
                            test += "TCPT_CommitmentTemp 테이블에 insert / ";
                            string s = _tcptService.InsertTCPT_CommitmentTemp(CommitmentID, sponsorid, childMasterId, newHoldUID.ToString(), holdID, sponsorid, "");
                            test += "TCPT_CommitmentTemp 테이블에 insert 결과 : " + s + " / ";

                        }
                        else
                        {
                            // hold update error
                            test += "no money hold 로 업데이트 실패 " + updateResult.RequestMessage.ToString() + " / ";
                        }
                    }
                    catch (Exception ex)
                    {
                        test += "no money hold 로 업데이트 오류 " + ex.Message + " / ";
                    }
                }

            }
            // }
        }
        catch (Exception ex)
        {
            test += "오류 오류 " + ex.Message + " / ";
        }

        ErrorLog.Write(HttpContext.Current, 0, test);
    }

    private DataSet GetCommitmentInfo(string sTimeStamp, string childMasterId, string sponsorid)
    {
        Object[] objParam = new object[] { "CommitmentID", "ChildMasterID", "SponsorID" };
        Object[] objValue = new object[] { sTimeStamp, childMasterId, sponsorid };
        Object[] objSql = new object[] { "sp_web_commitment_cancel_count_f" };
        return _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
    }
}