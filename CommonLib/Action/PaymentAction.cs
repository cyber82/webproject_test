﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;

public partial class PaymentAction {

	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;
	public PaymentAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

	}

	// 결제내역 , startDate & endDate = yyyy-MM-dd
	public JsonWriter GetHistories( int page, int rowsPerPage, string startDate, string endDate ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}

		try {

			Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId", "startdate", "endDate" };
			Object[] objValue = new object[] { page, rowsPerPage, sess.SponsorID, startDate, endDate };
			Object[] objSql = new object[] { "sp_web_payment_history_list_f" };
			DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                foreach (DataRow dr in list.Rows)
                {

                    var childMasterId = dr["childMasterId"].ToString();
                    var commitmentid = dr["commitmentid"].ToString();
                    var campaignid = dr["campaignid"].ToString();
                    var accountClass = dr["accountClass"].ToString();

                    //var uf = dao.sp_tUserFundingUser_get_f(commitmentid).FirstOrDefault();
                    Object[] op1 = new Object[] { "CommitmentID" };
                    Object[] op2 = new Object[] { commitmentid };
                    var uf = www6.selectSP("sp_tUserFundingUser_get_f", op1, op2).DataTableToList<sp_tUserFundingUser_get_fResult>().FirstOrDefault();


                    // 나눔펀딩인 경우
                    if (uf != null)
                    {
                        //dr["accountclassname"] = string.Format("[나눔펀딩]{0}", uf.uf_title);
                        dr["accountclassname"] = string.Format("나눔펀딩");
                        dr["accountclassdetail"] = string.Format("{0}", uf.uf_title);

                    }
                }

            }

			result.data = list;
			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "결제 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

	// 정기결제예약내역
	public JsonWriter GetReservations( int page, int rowsPerPage ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}

		try {

			Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId" };
			Object[] objValue = new object[] { page, rowsPerPage, sess.SponsorID };
			Object[] objSql = new object[] { "sp_web_payment_reservation_list_f" };
			DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			result.data = list;
			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "예약 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

	// 정기결제 결제정보
	public JsonWriter GetPaymentWay() {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();
		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}

		Dictionary<string, object> obj = new Dictionary<string, object>();

		try {
			var rtn_payTitle = "";      // 결제수단
			var rtn_payMethod = "";     // 결제수단
			var rtn_payName = "";       // 카드명 , 은행명
			var rtn_payDay = 0;         // 결제일
			var rtn_address = "";       // 지로 주소
			var rtn_address_type = "";  // 지로 주소 배송지
			var rtn_payAccount = "";    // 결제계좌
			DateTime? rtn_ptd = null;			// 마지막 PTD
			var rtn_hasDetail = true;
			object rtn_virtualAccounts = null;

			DataSet ds = _wwwService.Payway(sess.SponsorID);

			//DataSet data = _wwwService.MypageApplyChildInfo(sess.SponsorID.Trim(), "", "", 0, 9999, "");
			DataSet dsList = _wwwService.ComplementaryApplySearch(sess.SponsorID.Trim());

			var actionResult = new ChildAction().MyChildren("", 1, 1);
			if(!actionResult.success) {
				throw new Exception(actionResult.message);
			}

			var myChildren = (List<ChildAction.MyChildItem>)actionResult.data;
			var hasCDSP = myChildren.Count > 0;

			var dsListCnt = dsList.Tables[0].Rows.Count;
			var dsPlayCnt = 0;
			//추가 2013-09-26  0건이상일 경우 CMS 신청중입니다.
			DataSet dsCMSPlay = _wwwService.getCMSApplicationPlay(sess.SponsorID.Trim());

			if(dsCMSPlay != null)
				dsPlayCnt = dsCMSPlay.Tables[0].Rows.Count;

			#region 결제수단에 따른 값지정
			if(ds.Tables["BankT"].Rows.Count > 0 && (hasCDSP || dsListCnt > 0)) {
				//- 결제 방법
				if(ds.Tables["BankT"].Rows[0]["PaymentName"] != null) {
					//수정 2013-09-26, 2013-10-16
					if(dsPlayCnt > 0) {
						rtn_payTitle = "CMS신청 중";
					} else {
						rtn_payTitle = ds.Tables["BankT"].Rows[0]["PaymentName"].ToString().Trim();
					}

				}

				switch(ds.Tables["BankT"].Rows[0]["PaymentName"].ToString().Trim()) {
					case "해외카드":
						
						rtn_payMethod = "해외카드";
						break;

					case "신용카드자동결제":

						rtn_payName = ds.Tables["BankT"].Rows[0]["BankName"].ToString().Trim();
						rtn_payDay = Convert.ToInt32(ds.Tables["BankT"].Rows[0]["PaymentDay"].ToString());

						rtn_payMethod = "신용카드자동결제";
						break;

					case "지로":

						// 수정 2012-03-27
						DataSet dtAddressType = _wwwService.listupDATSponsorMailing_CurrentUse(sess.SponsorID, "지로명세서", "", "", "Y");

						string addressTp = "";

						if(dtAddressType.Tables[0].Rows.Count > 0)
							addressTp = dtAddressType.Tables[0].Rows[0]["AddressType"].ToString();

						else
							addressTp = "";

						DataTable dtAddress = _wwwService.getSponsorAddress(sess.SponsorID, ds.Tables["BankT"].Rows[0]["Account"].ToString(), addressTp, "Y").Tables[0];

						rtn_address = dtAddress.Rows[0]["Address1"].ToString() + " "
									  + dtAddress.Rows[0]["Address2"].ToString() + " ("
									  + dtAddress.Rows[0]["ZipCode"].ToString().Substring(0, 3) + "-"
									  + dtAddress.Rows[0]["ZipCode"].ToString().Substring(3) + ")";
						rtn_address_type = dtAddress.Rows[0]["AddressType"].ToString();
						
						rtn_payMethod = "지로";

						break;

					case "CMS":


						rtn_payName = ds.Tables["BankT"].Rows[0]["BankName"].ToString().Trim();

						rtn_payAccount = ds.Tables["BankT"].Rows[0]["Account"].ToString().Substring(0, 2) +
										ds.Tables["BankT"].Rows[0]["Account"].ToString().Substring(2, 3) + "*********";

						rtn_payDay = Convert.ToInt32(ds.Tables["BankT"].Rows[0]["PaymentDay"].ToString());

						rtn_payMethod = "CMS";

						break;

					case "가상계좌":

						rtn_payName = ds.Tables["BankT"].Rows[0]["BankName"].ToString().Trim();
						rtn_payAccount = ds.Tables["BankT"].Rows[0]["Account"].ToString();
						rtn_virtualAccounts = this.GetVirtualAccounts().data;
						rtn_payMethod = "가상계좌";

						break;

					//추가 2012-04-02
					case "미주":
						rtn_payTitle = "설정되지 않았습니다. (미주후원자)";
						rtn_hasDetail = false;
						break;

					case "휴대폰자동결제":

						rtn_payMethod = "휴대폰자동결제";
						rtn_hasDetail = false;
						break;
				}
			} else {
				//수정 2012-04-02
				rtn_hasDetail = false;
				rtn_payTitle = "설정되지 않았습니다.";
			}
			#endregion


			obj.Add("virtual_accounts", rtn_virtualAccounts);
			obj.Add("has_detail", rtn_hasDetail);
			obj.Add("pay_title", rtn_payTitle);
			obj.Add("pay_method", rtn_payMethod);
			obj.Add("pay_name", rtn_payName);
			obj.Add("pay_day", rtn_payDay);
			obj.Add("address", rtn_address);
			obj.Add("address_type", rtn_address_type);
			obj.Add("pay_account", rtn_payAccount);
			obj.Add("last_PTD", rtn_ptd);
			
			result.success = true;
			result.data = obj;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "납부방법을 가져오는 중 오류가 발생했습니다";
			result.success = false;
		}


		return result;
	}

	// 발급된 가상계좌정보
	public JsonWriter GetVirtualAccounts() {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}

		Dictionary<string, object> obj = new Dictionary<string, object>();

		try {

			DataSet ds = _wwwService.listPaymentVirtual_IVR(sess.SponsorID, "");

			//기존의 가상계좌가 한건이상일때(pnlY를 visible시킨다.)
			if(ds != null && ds.Tables[0].Rows.Count > 0) {
				result.data = ds.Tables[0];
			}

			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "가상계좌정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;
		}


		return result;
	}

	// 정기결제중인 은행계좌정보
	public class BankAccount {
		public string code;
		public string account;
	}
	public JsonWriter GetBankAccount() {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}
		
		try {

			DataSet ds = _wwwService.GetDateUseYPaymentAccount(sess.SponsorID, "0004");

			result.data = null;
			//기존의 가상계좌가 한건이상일때(pnlY를 visible시킨다.)
			if(ds != null && ds.Tables[0].Rows.Count > 0) {
				result.data = new BankAccount() {
					code = ds.Tables[0].Rows[0]["BankCode"].ToString() ,
					account = ds.Tables[0].Rows[0]["Account"].ToString()
				};
			}

			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "계좌정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;
		}


		return result;
	}


	// 해외카드 결제 가능여부
	public JsonWriter CanUseOverseaCard() {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		/*
		1. 최초 결제시 해외발급카드 결제자 
		2. 해외 거주자 
		3. 결제정보가 없는 사람

		*/
		var sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.success = true;
			result.data = true;
			return result;
		}

		/*
		if (sess.LocationType == "국외" || sess.LocationType == "미주") {
			result.success = true;
			result.data = true;
			return result;
		}
		*/

		try {

			DataSet ds = _wwwService.Payway(sess.SponsorID);

			if(ds == null) {
				result.message = "데이타 조회중에 장애가 발생했습니다.";
				return result;
			}

			if (ds.Tables[0].Rows.Count < 1) {
				result.success = true;
				result.data = true;
				return result;
			} else {

				if (ds.Tables["BankT"].Rows[0]["PaymentName"].ToString().Trim() == "해외카드") {     
					result.success = true;
					result.data = true;
					return result;
				}else{
					result.data = false;
					return result;
				}

			}
			
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "해외카드 사용가능여부를 조회하는 중 오류가 발생했습니다";
			result.success = false;
		}


		return result;
	}

	// 가상계좌 발급
	public JsonWriter SetVirtualAccount( string bankCode, string bankName ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();
		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}

		//기존에 같은 은행에 가상계좌가 있는지 조회한다.
		DataSet ds = _wwwService.listPaymentVirtual_IVR(sess.SponsorID, "");
		if(ds != null) {
			for(int i = 0; i < ds.Tables[0].Rows.Count; i++) {
				if(bankCode == ds.Tables[0].Rows[i]["BankCode"].ToString().Trim()) {
					result.message = "가상계좌가 이미 발급된 은행입니다.";
					return result;
				}
			}
		} else {
			result.message = "가상계좌발급을 위한 기존가상계좌 조회에 실패했습니다.";
			return result;
		}

		//******가상계좌발급과기존결제방법유무에 따라 발급된 가상계좌를 tPaymentAccount에 Insert한다.
		DataSet dsResult = new DataSet();
		string strBuyerName = sess.UserName.ToString(); //후원자명
		string strUniqueKey = sess.SponsorID.ToString(); //후원자ID
		string strIpgmName = sess.UserName.ToString(); //후원자명
		string strIpgmDate = string.Empty;     //입력일
		string strIpAddress = HttpContext.Current.Request.UserHostAddress.ToString().Trim();  //후원자IP   
		string strSponsorID = sess.SponsorID.ToString();   //후원자ID
		string strSponsorName = sess.UserName.ToString(); //이름
		string strRegisterNo = string.Empty;   //주민번호
		string sResult = string.Empty;       //결과값
		string sRegisterDate = string.Empty;   //입력날짜
		string sAccountNumber = string.Empty;   //가상계좌번호

		ds = null;

		if(string.IsNullOrEmpty(sess.Jumin)) {
			result.message = "가상계좌발급을 위한 정보가 부족합니다.\\r\\n  콜센터로 문의해 주세요.";
			return result;

		}

		strRegisterNo = sess.Jumin.ToString().Replace("-", "").Trim();

		try {
			//가상계좌발급
			ds = _wwwService.IssueVirtualAccount(bankCode, strSponsorID, strSponsorName, strRegisterNo);

			if(ds == null) {
				result.message = "가상계좌발급에 실패했습니다.\\r\\n 다시 시도해 주세요..";
				return result;
			}

			//tPaymentAccount에 기존 결제방법이 있는지 여부를 판단.
			dsResult = _wwwService.MyAccountway(strSponsorID);
			//타임스탬프
			strIpgmDate = _wwwService.GetTimeStamp();
			//현재날짜얻기
			sRegisterDate = _wwwService.GetDate().ToString("yyyy-MM-dd HH:mm:ss");

			if(dsResult == null) {
				result.message = "가상계좌발급에 실패했습니다.\\r\\n 다시 시도해 주세요..";
				return result;
			}

			//기존결제방법이 없음.(기존결제방법으로 가상계좌번호를 넣어줌)
			if(dsResult.Tables[0].Rows.Count == 0) {
				//가상계좌번호를 가져온다.
				ds = null;
				ds = _wwwService.listPaymentVirtualIVR(strSponsorID);
				if(ds != null && ds.Tables[0].Rows.Count == 1) {
					sAccountNumber = ds.Tables[0].Rows[0]["AccountNumber"].ToString().Trim();
					//- 결제방법으로 가상계좌등록 
					sResult = _wwwService.registerVirtualPaymentAccount(strIpgmDate
																			 , strSponsorID
																			 , "0001"
																			 , "가상계좌"
																			 , bankCode
																			 , bankName
																			 , sAccountNumber
																			 , "한국사이버결제"
																			 , strSponsorName
																			 , ""
																			 , ""
																			 , strRegisterNo
																			 , strIpgmDate
																			 , "web"
																			 , sRegisterDate
																			 , ""
																			 , ""
																			 , ""
																			 , strSponsorID
																			 , strSponsorName);
					if(sResult == "10") {
						result.message = "후원자님의 가상계좌가 발급되었습니다.";
						result.success = true;
						return result;
					} else if(sResult == "30") {

						result.message = "가상계좌발급에 실패했습니다." + sResult.Substring(2).ToString().Replace("\n", "");
						return result;
					}

				}//새로발급된 가상계좌번호를 가져오는 실패했을 경우(tPaymentAccount에 Insert불가한 경우)
				else {
					result.message = "후원자님의 가상계좌가 발급되었습니다.";
					result.success = true;
					return result;
				}
			} else {
				result.message = "후원자님의 가상계좌가 발급되었습니다.";
				result.success = true;
				return result;
			}


		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "가상계좌발급에 실패했습니다.\\r\\n 다시 시도해 주세요.";
			result.success = false;

		}
		
		return result;
	}

	// 월별 commitment 결제내역
	public JsonWriter GetMonthlyHistoriesByCommitmentId( string commitmentId , string year) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();
		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}
		try {

			Object[] objParam = new object[] { "commitmentId", "sponsorId" , "year" };
			Object[] objValue = new object[] { commitmentId , sess.SponsorID , year };
			Object[] objSql = new object[] { "sp_web_commitment_payment_list_f" };
			DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			result.data = list;
			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "결제 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

	// 선물가능한 어린이 수
	public JsonWriter GiftableChildCount() {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();
		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}
		try {
			
			DataTable dt = _wwwService.PresentChild(sess.SponsorID, "", "", "").Tables["PresentT"];
			result.data = Convert.ToInt32(dt.Rows.Count.ToString());
			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "예약 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

	// 선물금 결제내역
	public JsonWriter GetGiftPayments( int page, int rowsPerPage , string childMasterId ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();
		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}
		try {

			Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId" , "childMasterId" };
			Object[] objValue = new object[] { page, rowsPerPage , sess.SponsorID , childMasterId };
			Object[] objSql = new object[] { "sp_web_present_payment_list_f" };
			DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			result.data = list;
			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "예약 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

	// 선물금 예약내역
	public JsonWriter GetGiftReservations( int page, int rowsPerPage , string childMasterId ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();
		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}
		try {

			Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId" , "childMasterId" };
			Object[] objValue = new object[] { page, rowsPerPage, sess.SponsorID , childMasterId };
			Object[] objSql = new object[] { "sp_web_present_reservation_list_f" };
			DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			result.data = list;
			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "예약 정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

	// 선물금 예약삭제
	public JsonWriter DeleteGiftReservation( string commitmentId) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();
		if(string.IsNullOrEmpty(sess.SponsorID)) {
			result.action = "not_sponsor";
			return result;
		}
		try {

			Object[] objParam = new object[] { "sponsorId" , "sponsorName", "commitmentId" };
			Object[] objValue = new object[] { sess.SponsorID,sess.UserName , commitmentId };
			Object[] objSql = new object[] { "sp_web_present_reservation_cancel_f" };
			result.success = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0].Rows[0]["result"].ToString() == "1";
			
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "예약정보를 삭제하는중에 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}

	// 특정 캠페인으로 결제된 총금액
	public JsonWriter GetTotalAmountByCampaign( string campaignId ) {
		JsonWriter result = new JsonWriter() { success = false };
		
		try {

			Object[] objParam = new object[] { "CampaignID" };
			Object[] objValue = new object[] { campaignId};
			Object[] objSql = new object[] { "sp_web_payment_total_by_campaignId_f" };
			DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			result.data = Convert.ToInt64(list.Rows[0]["totalAmount"]);
			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "총금액정보를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;
	}
	
	/*
	public JsonWriter GetCommitmentIdByNotificationId( string sponsorId , string notificationId ) {

		JsonWriter result = new JsonWriter() { success = false };

		try {

			Object[] objParam = new object[] { "sponsorId" , "notificationId" };
			Object[] objValue = new object[] { sponsorId , notificationId };
			Object[] objSql = new object[] { "sp_web_commitment_by_notificationId_f" };
			DataTable list = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			result.data = list;
			result.success = true;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "데이타를 가져오는 중 오류가 발생했습니다";
			result.success = false;

		}

		return result;

		
	}
	*/

}