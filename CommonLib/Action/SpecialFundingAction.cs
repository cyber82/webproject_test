﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Data;
using CommonLib;

public class tSpecialFundingEx : tSpecialFunding {
	public bool registered = false;
}

public partial class SpecialFundingAction {

	CommonLib.WWWService.ServiceSoap _wwwService;
	CommonLib.WWW6Service.SoaHelperSoap _www6Service;
	public SpecialFundingAction() {
		_wwwService = new CommonLib.WWWService.ServiceSoapClient();
		_www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

	}

	// 특별한 나눔 데이타
	public JsonWriter GetItem( string campaignId )
    {
		JsonWriter result = new JsonWriter() { success = false };

		try {

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var entity = dao.sp_tSpecialFunding_get_f(campaignId).FirstOrDefault();
                Object[] op1 = new Object[] { "CampaignID" };
                Object[] op2 = new Object[] { campaignId };
                var list = www6.selectSP("sp_tSpecialFunding_get_f", op1, op2).DataTableToList<sp_tSpecialFunding_get_fResult>();

                var entity = list.FirstOrDefault();
                result.data = entity;
            }

			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "정보 조회에 실패했습니다.";
			return result;

		}

	}


	// 등록가능한 캠페인조회
	public JsonWriter GetCandidateList() {
		JsonWriter result = new JsonWriter() { success = false };


		try {
			List<tSpecialFunding> registererdList = new List<tSpecialFunding>();
			List<tSpecialFundingEx> data = new List<tSpecialFundingEx>();

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //registererdList = dao.tSpecialFunding.Where(p => p.sf_deleted == false).ToList();
                registererdList = www6.selectQ<tSpecialFunding>("sf_deleted", 0);
            }

			Object[] objSql = new object[1] { "sp_web_campaign_list" };
			Object[] objParam = new object[] {  };
			Object[] objValue = new object[] {  };
			var dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
			
			foreach(DataRow dr in dt.Rows) {

				var CampaignID = dr["CampaignID"].ToString();
				var entity = new tSpecialFundingEx() {
					//AccountClass = "PBCIV", 
					//AccountClassGroup = "CIV",
					CampaignID = CampaignID , 
					CampaignName = dr["CampaignName"].ToString() , 
					sf_summary = dr["CampaignContext"].ToString(),
					sf_content = "" , 
					sf_current_amount = 0 , 
					sf_goal_amount = Convert.ToInt32(dr["TargetAmount"].ToString().ValueIfNull("0")),
					StartDate = Convert.ToDateTime(dr["StartDate"]),
					EndDate = Convert.ToDateTime(dr["EndDate"]),
					sf_display = true ,
					sf_regdate = DateTime.Now , 
					registered = registererdList.Any(p => p.CampaignID == CampaignID)
				};

				data.Add(entity);
			}

			result.data = data;
			result.success = true;
			return result;

			} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "정보 조회에 실패했습니다.";
			return result;

		}

	}

	// 현재 모금금액 업데이트 , 결제시 호출
	public JsonWriter UpdateCurrentAmount( string campaignId , int amount ) {
		JsonWriter result = new JsonWriter() { success = false };

		if(string.IsNullOrEmpty(campaignId)) {
			result.message = "캠페인아이디가 필요합니다.";
			return result;
		}

		if(campaignId == "90000000000000000") {
			result.message = "기본캠페인은 집계되지 않습니다.";
			return result;
		}

		if(amount < 0) {
			result.message = "금액을 확인해주세요";
			return result;
		}

		try {

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                // 하루가 경과한경우 업데이트
                //var list = dao.tSpecialFunding.Where(p => p.CampaignID == campaignId);
                var list = www6.selectQ<tSpecialFunding>("CampaignID", campaignId);
                if (list.Count() > 0)
                {

                    foreach (var item in list)
                    {
                        item.sf_current_amount += amount;

                        //dao.SubmitChanges();
                        www6.update(item);
                    }

                    
                }
            }

			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "업데이트에 실패했습니다.";
			return result;

		}

	}

	// 현재 모금금액 업데이트 , 정기결제의 경우에는 웹에서 결제금액을 알수가 없기때문에, 일정 기간마다 컴파스에서 총 금액을 얻어와서 업데이트한다.
	public JsonWriter UpdateTotalCurrentAmount(string campaignId) {
		JsonWriter result = new JsonWriter() { success = false };


		if(string.IsNullOrEmpty(campaignId)) {
			result.message = "캠페인아이디가 필요합니다.";
			return result;
		}

		if(campaignId == "90000000000000000") {
			result.message = "기본캠페인은 집계되지 않습니다.";
			return result;
		}
		

		try {

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                // 하루가 경과한경우 업데이트
                //var list = dao.tSpecialFunding.Where(p => p.CampaignID == campaignId && p.sf_last_request_date < DateTime.Now.AddDays(-1));
                var list = www6.selectQ2<tSpecialFunding>("CampaignID = ", campaignId, "sf_last_request_date < ", DateTime.Now.AddDays(-1));
                if (list.Count() > 0)
                {
                    var action = new PaymentAction().GetTotalAmountByCampaign(campaignId);
                    if (!action.success)
                    {
                        result.message = action.message;
                        return result;
                    }

                    foreach (var item in list)
                    {
                        item.sf_current_amount = Convert.ToInt64(action.data);
                        item.sf_last_request_date = DateTime.Now;

                        //dao.SubmitChanges();
                        www6.update(item);
                    }

                    
                }
            }

			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "업데이트에 실패했습니다.";
			return result;

		}

	}

	// 리포트 
	public JsonWriter GetReportList(int page , int rowsPerPage , string campaignId , string upload_root) {
		JsonWriter result = new JsonWriter() { success = false };


		try {

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var list = dao.sp_tSpecialFundingReport_list_f(page, rowsPerPage, campaignId).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage", "campaignid" };
                Object[] op2 = new Object[] { page, rowsPerPage, campaignId };
                var list = www6.selectSP("sp_tSpecialFundingReport_list_f", op1, op2).DataTableToList<sp_tSpecialFundingReport_list_fResult>();

                foreach (var item in list)
                {
                    item.sr_file = (upload_root + item.sr_file).WithFileServerHost();
                }
                result.data = list;
            }

			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "정보 조회에 실패했습니다.";
			return result;

		}

	}

	// 종료된 캠페인 
	public JsonWriter GetCompleteList( int page, int rowsPerPage ) {
		JsonWriter result = new JsonWriter() { success = false };


		try {

            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //var list = dao.sp_tSpecialFunding_complete_list_f(page, rowsPerPage).ToList();
                Object[] op1 = new Object[] { "page", "rowsPerPage" };
                Object[] op2 = new Object[] { page, rowsPerPage };
                var list = www6.selectSP("sp_tSpecialFunding_complete_list_f", op1, op2).DataTableToList<sp_tSpecialFunding_complete_list_fResult>();

                result.data = list;
            }

			result.success = true;
			return result;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.Message);
			result.message = "정보 조회에 실패했습니다.";
			return result;

		}

	}
}