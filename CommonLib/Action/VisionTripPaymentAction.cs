﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Net.Sockets;
using System.Data;
using CommonLib;

public partial class VisionTripPaymentAction
{
    CommonLib.WWWService.ServiceSoap _wwwService;
    CommonLib.PAY4Service.ServiceSoap _pay4Service;
    public VisionTripPaymentAction()
    {
        _wwwService = new CommonLib.WWWService.ServiceSoapClient();
        _pay4Service = new CommonLib.PAY4Service.ServiceSoapClient();
    }


    // 가상계좌 발급
    public JsonWriter SetVirtualAccount( string bankCode, string bankName, int planDetailID, string amountType, string amount ) {
		JsonWriter result = new JsonWriter() { success = false };
		if(!UserInfo.IsLogin) {
			result.message = "로그인이 필요합니다.";
			return result;
		}

		var sess = new UserInfo();
		//if(string.IsNullOrEmpty(sess.SponsorID)) {
		//	result.action = "not_sponsor";
		//	return result;
		//}
         
		//******가상계좌발급과기존결제방법유무에 따라 발급된 가상계좌를 tPaymentAccount에 Insert한다.
		DataSet dsResult = new DataSet();
		string strBuyerName = sess.UserName.ToString(); //후원자명
		string strUniqueKey = sess.SponsorID.ToString(); //후원자ID
		string strIpgmName = sess.UserName.ToString(); //후원자명
		string strIpgmDate = string.Empty;     //입력일
        string strIpAddress = GetIP4Address();//HttpContext.Current.Request.UserHostAddress.ToString().Trim();  //후원자IP   
		string strSponsorID = sess.SponsorID.ToString();   //후원자ID
		string strSponsorName = sess.UserName.ToString(); //이름
		string strRegisterNo = string.Empty;   //주민번호
		string sResult = string.Empty;       //결과값
		string sRegisterDate = string.Empty;   //입력날짜
		string sAccountNumber = string.Empty;   //가상계좌번호

		DataSet ds = null;

        //if (string.IsNullOrEmpty(sess.Jumin))
        //{
        //    result.message = "가상계좌발급을 위한 정보가 부족합니다.\\r\\n  콜센터로 문의해 주세요.";
        //    return result;

        //}

        //strRegisterNo = sess.Jumin.ToString().Replace("-", "").Trim();

        try
        {
            DataSet dsData = new DataSet();
            //타임스탬프
            strIpgmDate = _wwwService.GetTimeStamp();
            //현재날짜얻기
            sRegisterDate = _wwwService.GetDate().ToString("yyyy-MM-dd HH:mm:ss");

            string strOrderIdxx = strIpgmDate;
            dsData = MakeCardDataSet("pay", strOrderIdxx, "비전트립비용", amount, sess.UserName,
                                            "", "", "", "410", "VCNT",
                                            "", "", "", "",
                                            strSponsorID, sess.UserName, "BK" + bankCode, DateTime.Now.AddDays(3).ToString("yyyyMMdd"),
                                            "STMR", "", "", strIpAddress);


            //_pay4Service = new CommonLib.PAY4Service.ServiceSoapClient();
            //가상계좌발급 );
            dsResult = _pay4Service.PaymentByVCNTHub(dsData, "VT", "");
             

            if (dsResult == null)
            {
                result.message = "가상계좌발급에 실패했습니다.\\r\\n 다시 시도해 주세요..";
                return result;
            }

            //기존결제방법이 없음.(기존결제방법으로 가상계좌번호를 넣어줌)
            //if (dsResult.Tables[0].Rows.Count == 0) {
            //가상계좌번호를 가져온다.
            ds = null;
            //ds = _wwwService.listPaymentVirtualIVR(strSponsorID);
            #region 가상계좌 저장
            if (dsResult != null && dsResult.Tables[0].Rows.Count == 1)
            {
                if (dsResult.Tables[0].Rows[0]["ResponseResult"].ToString() == "fail")
                {
                    ErrorLog.Write(HttpContext.Current, 0, dsResult.Tables[0].Rows[0]["ResMsg"].ToString());
                    result.message = "가상계좌발급에 실패했습니다. \\r\\n " + dsResult.Tables[0].Rows[0]["ResMsg"].ToString().Replace("\n", "");
                    return result;
                }
                else
                {
                    if (dsResult.Tables[0].Rows[0]["ResultCode"].ToString() == "10")
                    {
                        try
                        {
                            using (MainLibDataContext dao = new MainLibDataContext())
                            {
                                var payment = new CommonLib.tVisionTripPayment();

                                var AccountInfo = dsResult.Tables[0].Rows[0]["Account"].ToString().Trim();
                                //tVisionTripPayment 저장
                                payment.PlanDetailID = planDetailID;
                                payment.SponsorID = strSponsorID;
                                payment.PaymentName = "가상계좌";
                                payment.PaymentType = "001000000000";
                                payment.BankCode = bankCode;
                                payment.BankName = bankName;
                                payment.OrderNo = strOrderIdxx;
                                payment.Amount = Convert.ToDecimal(amount);
                                payment.AmountType = Convert.ToChar(amountType);
                                payment.PaymentResultYN = 'N';
                                payment.Requester = sess.UserName;
                                payment.Account = AccountInfo;
                                payment.DueDate = DateTime.Now.AddDays(3);
                                payment.KCPTNo = dsResult.Tables[0].Rows[0]["OrderTNo"].ToString().Trim();
                                payment.CurrentUse = 'Y';
                                payment.RegisterDate = DateTime.Now;
                                payment.RegisterID = sess.UserId;
                                payment.RegisterName = sess.UserName;

                                //dao.tVisionTripPayment.InsertOnSubmit(payment);
                                www6.insert(payment);
                                //dao.SubmitChanges();

                                result.message = "후원자님의 가상계좌가 발급되었습니다.";
                                result.success = true;
                                #region 발급 완료 SMS
                                if (result.success)
                                {
                                    var amounttype = amountType == "R" ? "신청비" : amountType == "T" ? "트립비" : amountType == "C" ? "어린이 만남비" : "";

                                    //var detailEntity = dao.tVisionTripPlanDetail.Where(p => p.PlanDetailID == planDetailID).FirstOrDefault();
                                    var detailEntity = www6.selectQF<tVisionTripPlanDetail>("PlanDetailID", planDetailID);
                                    if (detailEntity != null)
                                    {
                                        //var applyEntity = dao.tVisionTripApply.Where(p => p.ApplyID == detailEntity.ApplyID).FirstOrDefault();
                                        var applyEntity = www6.selectQF<tVisionTripApply>("ApplyID", detailEntity.ApplyID);

                                        UserInfo userInfo = new UserInfo();
                                        var smsMessage = "[컴패션] " + userInfo.UserName + "님, 비전트립 " + amounttype + " 전용 가상계좌가 발급되었습니다.";

                                        smsMessage += bankName + " 계좌번호 :" + AccountInfo;

                                        var smsSendYN = util.SendSMS.SMSSend(applyEntity.Tel.Decrypt(), userInfo.UserName, smsMessage);
                                        var entity = new tVisionTripMessage()
                                        {
                                            ApplyID = applyEntity.ApplyID,
                                            UserID = userInfo.UserId,
                                            SponsorID = applyEntity.SponsorID,
                                            Message = smsMessage,
                                            GroupNo = StringExtension.GetRandomString(3) + DateTime.Now.ToString("yyyyMMddHHmmss"),
                                            ReceiveTel = applyEntity.Tel.Decrypt(),
                                            ReceiveID = applyEntity.UserID,
                                            ReceiveName = userInfo.UserName,
                                            SendType = 'D', //즉시발송
                                            MessageType = "가상계좌발급",
                                            SendTel = ConfigurationManager.AppSettings["SMSSendNo"].ToString().Trim(),
                                            SendYN = smsSendYN ? 'Y' : 'N',
                                            SendDate = DateTime.Now,
                                            CurrentUse = 'Y',
                                            RegisterID = userInfo.UserId,
                                            RegisterName = userInfo.UserName,
                                            RegisterDate = DateTime.Now
                                        };
                                        //dao.tVisionTripMessage.InsertOnSubmit(entity);
                                        www6.insert(entity);
                                        //dao.SubmitChanges();
                                    }
                                }

                                #endregion

                                return result;
                            }
                        }
                        catch (Exception ex)
                        {
                            result.message = "가상계좌발급에 실패했습니다." + sResult.Substring(2).ToString().Replace("\n", "");
                            return result;
                        }
                    }
                    else if (dsResult.Tables[0].Rows[0]["ResultCode"].ToString() == "30")
                    {
                        result.message = "가상계좌발급에 실패했습니다.";
                        return result;
                    }
                }

            }
            else
            {
                result.message = "가상계좌발급에 실패했습니다.\\r\\n 다시 시도해 주세요.";
                result.success = false;
            }
            #endregion


            //sAccountNumber = ds.Tables[0].Rows[0]["AccountNumber"].ToString().Trim();
            ////- 결제방법으로 가상계좌등록 
            //sResult = _wwwService.registerVirtualPaymentAccount(strIpgmDate
            //														 , strSponsorID
            //														 , "001000000000"
            //														 , "가상계좌"
            //														 , bankCode
            //														 , bankName
            //														 , sAccountNumber
            //														 , "한국사이버결제"
            //														 , strSponsorName
            //														 , ""
            //														 , ""
            //														 , strRegisterNo
            //														 , strIpgmDate
            //														 , "web"
            //														 , sRegisterDate
            //														 , ""
            //														 , ""
            //														 , ""
            //														 , strSponsorID
            //														 , strSponsorName);
            //               if (sResult == "10") {
            //	result.message = "후원자님의 가상계좌가 발급되었습니다.";
            //	result.success = true;
            //	return result;
            //} else if(sResult == "30") {

            //	result.message = "가상계좌발급에 실패했습니다." + sResult.Substring(2).ToString().Replace("\n", "");
            //	return result;
            //}

            //}//새로발급된 가상계좌번호를 가져오는 실패했을 경우(tPaymentAccount에 Insert불가한 경우)
            //else {
            //	result.message = "후원자님의 가상계좌가 발급되었습니다.";
            //	result.success = true;
            //	return result;
            //}
            //} else {
            //	result.message = "후원자님의 가상계좌가 발급되었습니다.";
            //	result.success = true;
            //	return result;
            //}


        } catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "가상계좌발급에 실패했습니다.\\r\\n 다시 시도해 주세요.";
			result.success = false; 
		}
		
		return result;
	}


    /// <summary>
    /// 신용카드, 가상계좌 결제 정보 데이터셋 정의, 초기화
    /// </summary>    
    /// <param name="strReqTx">요청종류</param>
    /// <param name="strOrderIdxx">주문번호</param>
    /// <param name="strGoodName">상품정보</param>
    /// <param name="strGoodMny">결제금액</param>
    /// <param name="strBuyerName">주문자이름</param>
    /// <param name="strBuyerMail">주문자이메일</param>
    /// <param name="strBuyerTel1">주문자전화번호</param>
    /// <param name="strBuyerTel2">주문자휴대폰번호</param>
    /// <param name="strCurrency">화폐단뒤</param>
    /// <param name="strPayMethod">결제수단</param>
    /// 수기카드 관련정보
    /// <param name="strCardNo">카드번호</param>
    /// <param name="strQuota">할부개월</param>
    /// <param name="strExpiryyy">유효기간년</param>
    /// <param name="strExpirymm">유효기간 월</param>
    /// 가상계좌 관련정보
    /// <param name="strUniqueKey">유니트키 : 가상계좌</param>
    /// <param name="strIpgmName">입금자명 : 가상계좌</param>
    /// <param name="strIpgmBank">입금은행 : 가상계좌</param>    
    /// <param name="strIpgmDate">입금예정일 : 가상계좌</param>
    /// <param name="strModType">취소구분</param>
    /// <param name="strTno">KCP주문번호</param>
    /// <param name="strModDesc">취소사유</param>
    /// <param name="strCustIp">아이피</param>
    /// <returns>데이터셋</returns>
    public DataSet MakeCardDataSet(string strReqTx, string strOrderIdxx, string strGoodName
                                        , string strGoodMny, string strBuyerName, string strBuyerMail
                                        , string strBuyerTel1, string strBuyerTel2, string strCurrency
                                        , string strPayMethod, string strCardNo, string strQuota
                                        , string strExpiryyy, string strExpirymm
                                        , string strUniqueKey, string strIpgmName, string strIpgmBank
                                        , string strIpgmDate
                                        , string strModType, string strTno, string strModDesc
                                        , string strCustIp)
    {
        DataTable dtCommonT = new DataTable();
        DataTable dtSslT = new DataTable();
        DataTable dtModT = new DataTable();
        DataSet dsData = new DataSet();

        dtCommonT.TableName = "CommonT";
        dtCommonT.Columns.Add("req_tx", typeof(System.String));
        dtCommonT.Columns.Add("ordr_idxx", typeof(System.String));
        dtCommonT.Columns.Add("good_name", typeof(System.String));
        dtCommonT.Columns.Add("good_mny", typeof(System.String));
        dtCommonT.Columns.Add("buyr_name", typeof(System.String));
        dtCommonT.Columns.Add("buyr_mail", typeof(System.String));
        dtCommonT.Columns.Add("buyr_tel1", typeof(System.String));
        dtCommonT.Columns.Add("buyr_tel2", typeof(System.String));
        dtCommonT.Columns.Add("currency", typeof(System.String));
        dsData.Tables.Add(dtCommonT);

        dsData.Tables["CommonT"].Rows.Add();
        dsData.Tables["CommonT"].Rows[0]["req_tx"] = strReqTx;
        dsData.Tables["CommonT"].Rows[0]["ordr_idxx"] = strOrderIdxx;
        dsData.Tables["CommonT"].Rows[0]["good_name"] = strGoodName;
        dsData.Tables["CommonT"].Rows[0]["good_mny"] = strGoodMny;
        dsData.Tables["CommonT"].Rows[0]["buyr_name"] = strBuyerName;
        dsData.Tables["CommonT"].Rows[0]["buyr_mail"] = strBuyerMail;
        dsData.Tables["CommonT"].Rows[0]["buyr_tel1"] = strBuyerTel1;
        dsData.Tables["CommonT"].Rows[0]["buyr_tel2"] = strBuyerTel2;
        dsData.Tables["CommonT"].Rows[0]["currency"] = strCurrency;


        dtSslT.TableName = "SslT";

        if (strPayMethod == "SSL")
        {
            dtSslT.Columns.Add("pay_method", typeof(System.String));
            dtSslT.Columns.Add("card_no", typeof(System.String));
            dtSslT.Columns.Add("quota", typeof(System.String));
            dtSslT.Columns.Add("expiry_yy", typeof(System.String));
            dtSslT.Columns.Add("expiry_mm", typeof(System.String));
            dsData.Tables.Add(dtSslT);

            dsData.Tables["SslT"].Rows.Add();
            dsData.Tables["SslT"].Rows[0]["pay_method"] = strPayMethod;
            dsData.Tables["SslT"].Rows[0]["card_no"] = strCardNo;
            dsData.Tables["SslT"].Rows[0]["quota"] = strQuota;
            dsData.Tables["SslT"].Rows[0]["expiry_yy"] = strExpiryyy;
            dsData.Tables["SslT"].Rows[0]["expiry_mm"] = strExpirymm;
        }
        else if (strPayMethod == "VCNT")
        {
            dtSslT.Columns.Add("pay_method", typeof(System.String));
            dtSslT.Columns.Add("uniq_key", typeof(System.String));
            dtSslT.Columns.Add("ipgm_name", typeof(System.String));
            dtSslT.Columns.Add("ipgm_bank", typeof(System.String));
            dtSslT.Columns.Add("ipgm_date", typeof(System.String));
            dsData.Tables.Add(dtSslT);

            dsData.Tables["SslT"].Rows.Add();
            dsData.Tables["SslT"].Rows[0]["pay_method"] = strPayMethod;
            dsData.Tables["SslT"].Rows[0]["uniq_key"] = strUniqueKey;
            dsData.Tables["SslT"].Rows[0]["ipgm_name"] = strIpgmName;
            dsData.Tables["SslT"].Rows[0]["ipgm_bank"] = strIpgmBank;
            dsData.Tables["SslT"].Rows[0]["ipgm_date"] = strIpgmDate;
        }


        dtModT.TableName = "ModT";
        dtModT.Columns.Add("mod_type", typeof(System.String));
        dtModT.Columns.Add("tno", typeof(System.String));
        dtModT.Columns.Add("mod_desc", typeof(System.String));
        dtModT.Columns.Add("m_strCustIP", typeof(System.String));
        dsData.Tables.Add(dtModT);

        dsData.Tables["ModT"].Rows.Add();
        dsData.Tables["ModT"].Rows[0]["mod_type"] = strModType;
        dsData.Tables["ModT"].Rows[0]["tno"] = strTno;
        dsData.Tables["ModT"].Rows[0]["mod_desc"] = strModDesc;
        dsData.Tables["ModT"].Rows[0]["m_strCustIP"] = strCustIp;

        return dsData;
    }

    public JsonWriter GetVirtualAccounts(int planDetailID, string amountType)
    {
        JsonWriter result = new JsonWriter() { success = false };

        UserInfo userinfo = new UserInfo();
        if (!UserInfo.IsLogin)
        {
            result.message = "로그인이 필요합니다.";
            return result;
        }

        try
        {
            using (MainLibDataContext dao = new MainLibDataContext())
            {
                //duedate 지난 계좌 삭제처리
                //var datecheck = dao.tVisionTripPayment.Where(p => p.PlanDetailID == planDetailID
                //        && p.AmountType == Convert.ToChar(amountType) && p.PaymentName == "가상계좌" && p.CurrentUse == 'Y' && p.DueDate < DateTime.Now).ToList();
                var datecheck = www6.selectQ2<tVisionTripPayment>("PlanDetailID = ", planDetailID, "AmountType = ", Convert.ToChar(amountType), "PaymentName = ", "가상계좌", "CurrentUse = ", "Y", "DueDate < ", DateTime.Now);

                foreach (tVisionTripPayment pm in datecheck)
                {
                    pm.CurrentUse = 'N';
                    pm.ModifyDate = DateTime.Now;
                    pm.ModifyID = userinfo.UserId;
                    pm.ModifyName = userinfo.UserName;
                    
                    //dao.SubmitChanges(); 
                    www6.update(pm);
                }


                //var list = dao.tVisionTripPayment.Where(p => p.PlanDetailID == planDetailID && p.AmountType == Convert.ToChar(amountType) && p.PaymentName == "가상계좌" && p.CurrentUse == 'Y').ToList();
                var list = www6.selectQ<tVisionTripPayment>("PlanDetailID", planDetailID, "AmountType", Convert.ToChar(amountType), "PaymentName", "가상계좌", "CurrentUse", "Y");
                result.data = list;
            }

            result.success = true;

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
            result.message = "가상계좌정보를 가져오는 중 오류가 발생했습니다";
            result.success = false;
        } 

        return result; 
    }

    public static string GetIP4Address()
    {
        string strIP4Address = String.Empty;

        foreach (IPAddress objIP in Dns.GetHostAddresses(Dns.GetHostName()))
        {
            if (objIP.AddressFamily.ToString() == "InterNetwork")
            {
                strIP4Address = objIP.ToString();
                break;
            }
        }
        return strIP4Address;
    }

}