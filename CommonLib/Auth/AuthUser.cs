﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using CommonLib;

public class AuthUser{

    CommonLib.WWWService.ServiceSoap _wwwService;
    CommonLib.WWW6Service.SoaHelperSoap _www6Service;

    public AuthUser() {
        _wwwService = new CommonLib.WWWService.ServiceSoapClient();
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
    }

    JsonWriter GetUser(string userId , string userPwd) {
        
        JsonWriter actionResult = new JsonWriter();
		JsonWriter result = new JsonWriter();
		FrontLoginSession.LoginUser sess = new FrontLoginSession.LoginUser();
        var NKID = "";

        using (AuthDataContext dao = new AuthDataContext())
        {
            // NKID
            //var nkUser = dao.nkUser.FirstOrDefault(p => p.CompassionID == userId);
            var nkUser = www6.selectQFAuth<nkUser>("CompassionID", userId);

            if (nkUser != null)     // 전환된 NK계정
                NKID = nkUser.UserID;

            if (userPwd == "ADMINCHECK")
            {
                sp_tSponsorMaster_getResult entity = null;
                //var data = dao.sp_tSponsorMaster_get(userId).ToList();
                Object[] op1 = new Object[] { "UserID" };
                Object[] op2 = new Object[] { userId };
                var data = www6.selectSPAuth("sp_tSponsorMaster_get", op1, op2).DataTableToList<sp_tSponsorMaster_getResult>();

                if (data.Count < 1)
                {
                    result.success = false;
                    result.message = "입력하신 정보에 일치하는 회원정보를 찾을 수 없습니다.";
                    return result;
                }

                entity = data.First();

                if (!entity.CertifyDate.HasValue && entity.UserClass == "기업")
                {
                    result.success = false;
                    result.message = "승인되지 않은 기업회원입니다.";
                    return result;
                }

                sess.Birth = entity.BirthDate.ToDateFormat("-");
                sess.GenderCode = entity.GenderCode;
                sess.UserClass = entity.UserClass;
                sess.SponsorID = entity.SponsorId.Trim();
                sess.LocationType = entity.LocationType;
                sess.UserId = userId;
                sess.UserName = string.IsNullOrEmpty(entity.SponsorName) ? "***" : entity.SponsorName;

                sess.ZipCode = entity.ZipCode;
                sess.Addr1 = entity.Addr1;
                sess.Addr2 = entity.Addr2;
                sess.Phone = entity.Phone;
                sess.Mobile = entity.Mobile;
                sess.Email = entity.Email;
                sess.NKID = NKID;

                sess.AdminLoginCheck = true;
                //sess.AdminLoginCheck = "admin";
            }
            else
            {
                sp_tSponsorMaster_get_fResult entity = null;
                //var data = dao.sp_tSponsorMaster_get_f(userId, userPwd, "", "", "", "", "").ToList();
                Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                Object[] op2 = new Object[] { userId, userPwd, "", "", "", "", "" };
                var data = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

                //string dbName = "SqlCompassionAuth";
                //object[] objSql = new object[1] { "sp_tSponsorMaster_get_f" };
                //string dbType = "SP";
                //object[] objParam = new object[7] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                //object[] objValue = new object[7] { userId, userPwd, "", "", "", "", "" };
                //DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, objParam, objValue);
                //List<sp_tSponsorMaster_get_fResult> data = ds.Tables[0].DataTableToList<sp_tSponsorMaster_get_fResult>();

                //Object[] objSql = new object[] { "sp_tSponsorMaster_get_f" };
                //Object[] objParam = new object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                //Object[] objValue = new object[] { userId, userPwd, "", "", "", "", "" };
                //DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompassionAuth", objSql, "SP", objParam, objValue).Tables[0];
                //List<sp_tSponsorMaster_get_fResult> data = dt.DataTableToList<sp_tSponsorMaster_get_fResult>();

                if (data.Count < 1)
                {
                    result.success = false;
                    result.message = "입력하신 정보에 일치하는 회원정보를 찾을 수 없습니다.";
                    return result;
                }

                entity = data.First();
                if (!entity.CertifyDate.HasValue && entity.UserClass == "기업")
                {
                    result.success = false;
                    result.message = "승인되지 않은 기업회원입니다.";
                    return result;
                }

                sess.Birth = entity.BirthDate.ToDateFormat("-");
                sess.GenderCode = entity.GenderCode;
                sess.UserClass = entity.UserClass;
                sess.SponsorID = entity.SponsorId.Trim();
                sess.LocationType = entity.LocationType;
                sess.UserId = userId;
                sess.UserName = string.IsNullOrEmpty(entity.SponsorName) ? "***" : entity.SponsorName;

                sess.ZipCode = entity.ZipCode;
                sess.Addr1 = entity.Addr1;
                sess.Addr2 = entity.Addr2;
                sess.Phone = entity.Phone;
                sess.Mobile = entity.Mobile;
                sess.Email = entity.Email;
                sess.NKID = NKID;

                sess.AdminLoginCheck = false;
                sess.ReligionType = entity.ReligionType.EmptyIfNull();
                sess.ChannelType = entity.ChannelType.EmptyIfNull();
                sess.AgreeEmail = entity.AgreeEmail.HasValue ? (entity.AgreeEmail.Value ? "Y" : "N") : "";
                sess.UserDate = entity.UserDate.HasValue ? entity.UserDate.Value.ToString("yyyy-MM-dd") : "";
                sess.RegisterDate = entity.RegisterDate.HasValue ? entity.RegisterDate.Value.ToString("yyyy-MM-dd") : "";

            }



        }
        
        

        // 웹회원은 sponsorId 가 없다.
        if (!string.IsNullOrEmpty(sess.SponsorID)) {

            #region 후원회원 확인
            ErrorLog.Write(HttpContext.Current, 0, "Login _wwwService.getSponsorInformation Start : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            DataSet ds = _wwwService.getSponsorInformation(sess.SponsorID);
            ErrorLog.Write(HttpContext.Current, 0, "Login _wwwService.getSponsorInformation End : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            if (ds != null && ds.Tables["tSponsorMaster"] != null && ds.Tables["tSponsorMaster"].Rows.Count >= 1) {

				string sUserId = ds.Tables["tSponsorMaster"].Rows[0]["UserID"].ToString().Trim();  //아이디
				string sSponsorName = ds.Tables["tSponsorMaster"].Rows[0]["SponsorName"].ToString().Trim();//후원자이름
				string sJuminId = ds.Tables["tSponsorMaster"].Rows[0]["JuminID"].ToString().Trim();//주민번호
				string sCertifyDate = ds.Tables["tSponsorMaster"].Rows[0]["CertifyDate"].ToString().Trim();//인증날짜
				string sUserClass = ds.Tables["tSponsorMaster"].Rows[0]["UserClass"].ToString().Trim(); //회원종류 (14세미만,14세이상,외국인국내거주,기업,해외회원)  
				string sComRegistration = ds.Tables["tSponsorMaster"].Rows[0]["ComRegistration"].ToString().Trim();//사업자등록번호
				string sConId = ds.Tables["tSponsorMaster"].Rows[0]["ConID"].ToString().Trim();//미국후원자번호
				string sGenderCode = ds.Tables["tSponsorMaster"].Rows[0]["GenderCode"].ToString().Trim(); //성별 
				string sLocationType = ds.Tables["tSponsorMaster"].Rows[0]["LocationType"].ToString().Trim();
				string sTranslationFlag = ds.Tables["tSponsorMaster"].Rows[0]["TranslationFlag"].ToString().Trim();//편지번역여부Y/N
				string sBirthDate = ds.Tables["tSponsorMaster"].Rows[0]["BirthDate"].ToString().Trim();//생년월일
				string sSponsorType = ds.Tables["tSponsorMaster"].Rows[0]["SponsorType"].ToString().Trim();//스폰서구분

				sess.Jumin = sJuminId;
				sess.CertifyDate = sCertifyDate;
				sess.UserClass = sUserClass;
				sess.GroupNo = sComRegistration;
				sess.ConId = sConId;
				//		sess.GenderCode = sGenderCode;
				sess.LocationType = sLocationType;
				sess.TranslationFlag = sTranslationFlag;
				sess.Birth = sBirthDate;
				sess.SponsorType = sSponsorType;

			}

            ErrorLog.Write(HttpContext.Current, 0, "Login _www6Service 0 Start : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            //WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
			Object[] objSql = new object[1] { " SELECT top 1 manageType FROM tSponsorMaster WHERE CurrentUse = 'Y' and sponsorId = '" + sess.SponsorID + "'" };
			ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
            ErrorLog.Write(HttpContext.Current, 0, "Login _www6Service 0 End : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            if (ds.Tables[0].Rows.Count > 0) {
				sess.ManageType = ds.Tables[0].Rows[0]["manageType"].ToString().Trim();
			}
            #endregion

            #region 마지막결제일
            Object[] objSqlGA = new object[1] { " select top 1 PaymentDate from tPaymentMaster where SponsorID = '" + sess.SponsorID + "' order by PaymentDate desc" };
            DataSet dsGA = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSqlGA, "Text", null, null);
            if (dsGA.Tables[0].Rows.Count > 0)
            {
                sess.LastPaymentDate = Convert.ToDateTime(dsGA.Tables[0].Rows[0]["PaymentDate"]).ToString("yyyy-MM-dd");
            }
            #endregion

            #region Mate 확인
            ErrorLog.Write(HttpContext.Current, 0, "Login _wwwService.loginMate Start : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            ds = _wwwService.loginMate(sess.UserId.ToString().Trim());
            ErrorLog.Write(HttpContext.Current, 0, "Login _wwwService.loginMate End : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            if (ds != null && ds.Tables["loginCorrespondence"] != null && ds.Tables["loginCorrespondence"].Rows.Count > 0) {
				if(ds.Tables["loginCorrespondence"].Rows.Count > 0 && ds.Tables["loginCorrespondence"].Rows[0]["CurrentUse"].ToString() == "Y") {
					if(ds.Tables["loginCorrespondence"].Rows[0]["OfficeMate"].ToString() == "Y")
						sess.Mate += ",사무";

					if(ds.Tables["loginCorrespondence"].Rows[0]["TranslationMate"].ToString() == "Y") {
						if(ds.Tables["loginCorrespondence"].Rows[0]["TranslationLevel"].ToString() != "영작")

							sess.Mate += ",번역";

						else
							sess.Mate += ",영작";
					}

					if(ds.Tables["loginCorrespondence"].Rows[0]["ProfessionMate"].ToString() == "Y")
						sess.Mate += ",전문";
				}

				//,자르기
				if(!string.IsNullOrEmpty(sess.Mate)) {
					if(sess.Mate.Length > 0)
						sess.Mate = "Mate  " + "<font color='#0054a6'>" + sess.Mate.Substring(1) + "</div>";
				}
			} else {
				sess.Mate = "";
			}
            #endregion

            #region 애드보킷여부, 결연정보, 일반후원정보
            if (sess.CertifyDate != null) {


                //VOC여부       
                /*
				ds = _wwwService.getVOCYN(sess.SponsorID.ToString().Trim());

				if(ds.Tables["GroupT"].Rows.Count > 0) {
					if(ds.Tables["GroupT"].Rows[0]["GroupType"].ToString().Trim() == "VOC")
						sess.VOC = ds.Tables["GroupT"].Rows[0]["GroupType"].ToString().Trim();
				}
				*/
                ErrorLog.Write(HttpContext.Current, 0, "Login _wwwService.getApplyCount Start : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                ds = _wwwService.getApplyCount(sess.SponsorID.ToString().Trim());
                ErrorLog.Write(HttpContext.Current, 0, "Login _wwwService.getApplyCount End : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                //결연
                if (ds.Tables["ApplyT"].Rows.Count > 0) {
					if(ds.Tables["ApplyT"].Rows[0]["apply"].ToString().Trim() != "0")
						sess.CommitCount = Convert.ToInt32(ds.Tables["ApplyT"].Rows[0]["apply"].ToString().ValueIfNull("0"));
				}

				// 일반후원건수
				if(ds.Tables["ApplyT"].Rows.Count > 1) {
					if(ds.Tables["ApplyT"].Rows[1]["apply"].ToString().Trim() != "0")
						sess.ApplyCount = Convert.ToInt32(ds.Tables["ApplyT"].Rows[1]["apply"].ToString().Trim().ValueIfNull("0"));
				}
			}
            #endregion

            #region 주소
            ErrorLog.Write(HttpContext.Current, 0, "Login GetAddress Start : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            actionResult = new SponsorAction().GetAddress(sess.UserId , sess.SponsorID , sess.ManageType , sess.LocationType);
			if(!actionResult.success) {
				result.message = actionResult.message;
				result.success = false;
				return result;
			}

			var addr_result = (SponsorAction.AddressResult)actionResult.data;
			sess.ZipCode = addr_result.Zipcode;
			sess.Addr1 = addr_result.Addr1;
			sess.Addr2 = addr_result.Addr2;
            ErrorLog.Write(HttpContext.Current, 0, "Login GetAddress End : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            #endregion

            #region 연락처
            actionResult = new SponsorAction().GetCommunications(sess.SponsorID);
			if(!actionResult.success) {
				result.message = actionResult.message;
				result.success = false;
				return result;
			}

			var comm_result = (SponsorAction.CommunicationResult)actionResult.data;
			sess.Email = comm_result.Email;
			sess.Phone = comm_result.Phone;
			sess.Mobile = comm_result.Mobile;
            #endregion
        }
        
        result.success = true;
		result.data = sess;
		return result;
	}

	public JsonWriter Login( string userId, string userPwd, string sessionId="") {
		return this.Login(userId, userPwd, false, sessionId);
	}
	public JsonWriter Login( string userId, string userPwd ,bool autoLogin, string sessionId="") {
        JsonWriter result = this.GetUser(userId , userPwd);
        if (!result.success) {
			return result;
		}

		FrontLoginSession.LoginUser user = (FrontLoginSession.LoginUser)result.data;

		var ip = HttpContext.Current.Request.UserHostAddress;
		var guid = Guid.NewGuid().ToString();
		var expire_date = DateTime.Now.AddHours(12);

		string token = string.Format("{0}|{1}|{2}", guid, ip, expire_date.ToString("yyyy-MM-dd HH:mm:ss")).Encrypt();

		//	Response.Write("user : " + user.ToJson().Encrypt() + "<br>");
		//HttpContext.Current.Response.Write("enc : " + HttpUtility.UrlEncode(token) + "<br>");
		//Response.Write("dec : " + token.Decrypt());
		
		// token 을 cookie 에 심어서 세션이 종료 됬을경우 자동로그인 시킨다.
		{
			HttpCookie cookie = new HttpCookie(Constants.TOKEN_COOKIE_NAME);
			cookie.Value = token;
			cookie.Path = "/";
			cookie.Domain = FrontLoginSession.GetDomain(HttpContext.Current);

            //cookie.Expires = 
			
			HttpContext.Current.Response.Cookies.Set(cookie);
		}
		// 로그인 정보 저장 
		{
			FrontLoginSession.SetCookie(HttpContext.Current , user , autoLogin);
		}

        // 로그아웃시 token 비활성화 
        using (AuthDataContext dao = new AuthDataContext())
        {

            var entity = new user_token()
            {
                ut_key = token,
                ut_value = user.ToJson().Encrypt(),
                ut_regdate = DateTime.Now,
                ut_date_expire = expire_date,
                ut_active = true
            };

            //dao.user_token.InsertOnSubmit(entity);
            //www6.cudAuth(MakeSQL.insertQ2(entity));
            www6.insertAuth(entity);
            //dao.SubmitChanges();

        }

        //[이종진] 신규방법이고 결연하기 한 어린이가 있다면. 로그인 시, UserID를 변경해줌
        if (ConfigurationManager.AppSettings["dbgp_kind"].ToString() == "2"
            && sessionId != "") //sessionId cookie는 결연하기 할 시, 생성됨
        {
            Object[] objSql = new object[1] { "UPDATE tTmpChildEnsure SET UserID = '" + user.UserId + "', SponsorId= '"+user.SponsorID+"' WHERE SessionID='" + sessionId + "' AND ISNULL(UserID, '')='' AND RegisterDate > DATEADD(HOUR, -1, GETDATE()) " };
            _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
        }

        result.data = token;
		return result;
	}
	
	public void Logout() {

		this.ExpireCookie();

		this.ExpireToken();

		this.ExpirePayInfo();
	}

	void ExpirePayInfo() {

		HttpCookie cookie = HttpContext.Current.Request.Cookies[Constants.PAYINFO_COOKIE_NAME];
		if(cookie == null)
			return;

		cookie.Expires = DateTime.Now.AddYears(-1);
		cookie.Domain = FrontLoginSession.GetDomain(HttpContext.Current);
		HttpContext.Current.Response.Cookies.Set(cookie);

	}

	void ExpireCookie() {

		HttpCookie cookie = HttpContext.Current.Request.Cookies[Constants.ENDUSER_COOKIE_NAME];
		if(cookie == null)
			return;

		cookie.Expires = DateTime.Now.AddYears(-1);
		cookie.Domain = FrontLoginSession.GetDomain(HttpContext.Current);
		HttpContext.Current.Response.Cookies.Set(cookie);

	}

	// 토큰 비활성(DB) 및 제거
	void ExpireToken() {

		HttpCookie cookie = HttpContext.Current.Request.Cookies[Constants.TOKEN_COOKIE_NAME];
		if(cookie == null)
			return;

		try {
            using (AuthDataContext dao = new AuthDataContext())
            {
                //var entity = dao.user_token.First(p => p.ut_key == cookie.Value);
                var entity = www6.selectQFAuth<user_token>("ut_key", cookie.Value);

                entity.ut_active = false;
                //dao.SubmitChanges();
                www6.updateAuth(entity);
            }
		} catch {

		}

		cookie.Expires = DateTime.Now.AddYears(-1);
		cookie.Domain = FrontLoginSession.GetDomain(HttpContext.Current);
		HttpContext.Current.Response.Cookies.Set(cookie);

	}

	// NK 로그인
	public JsonWriter LoginNK( string userId, string userPwd ) {

		JsonWriter result = new JsonWriter() {
			success = false
		};

        using (AuthDataContext dao = new AuthDataContext())
        {
            //var nkUser = dao.nkUser.FirstOrDefault(p => p.UserID == userId);
            var nkUser = www6.selectQFAuth<nkUser>("UserID", userId);

            // NK테이블에 아이디가 없으면 컴패션회원
            if (nkUser == null)
            {
                result.success = true;
                return result;

            }
            else
            {
                if (nkUser.IsCompleted.HasValue && nkUser.IsCompleted.Value)
                {

                    if (nkUser.UserID == nkUser.CompassionID)
                    {
                        result.success = true;
                        return result;

                    }
                    else
                    {
                        result.message = string.Format("{0}로 아이디가 전환되었습니다. \\n해당계정으로 로그인해주세요", nkUser.CompassionID);
                        return result;
                    }

                    // 미전환 계정
                }
                else
                {

                    // 비번일치시 , 비번 암호화 로직은 확인필요
                    if (nkUser.UserPW == userPwd)
                    {

                        //var list = dao.sp_tSponsorMaster_get_f(userId, "", "", "", "", "", "").ToList();
                        Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                        Object[] op2 = new Object[] { userId, "", "", "", "", "", "" };
                        var list = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

                        if (list.Count < 1)
                        {      // NK아이디유지 , CPS 신규생성(NK아이디 사용가능)

                            result.action = "NK_USABLE";            // 
                            return result;

                        }
                        else
                        {

                            var user = list.First();
                            if (nkUser.UserName == user.SponsorName && !string.IsNullOrEmpty(nkUser.BirthDate) && !string.IsNullOrEmpty(user.BirthDate) && nkUser.BirthDate == user.BirthDate)
                            {
                                // NK아이디유지 , CPS 기등록됨
                                result.action = "NK_USABLE";
                                return result;
                            }
                            else
                            {
                                // CPS 신규아이디발급대상
                                result.action = "NEWID";
                                return result;

                            }

                        }


                    }
                    else
                    {

                        result.message = "입력하신 정보에 해당하는 회원정보가 없습니다.";
                        return result;
                    }

                }
            }
        }
		
	}

	// NK 아이디전환
	public JsonWriter ChangeNK( string userId, string newUserId, string userPwd ) {

		JsonWriter result = new JsonWriter() {
			success = false
		};

        using (AuthDataContext dao = new AuthDataContext())
        {
            //var nkUser = dao.nkUser.FirstOrDefault(p => p.UserID == userId);
            var nkUser = www6.selectQFAuth<nkUser>("UserID", userId);

            // NK테이블에 아이디가 없으면 컴패션회원
            if (nkUser == null)
            {
                result.message = "NK아이디가 없습니다.";
                result.success = false;
                return result;

            }
            else
            {
                if (nkUser.IsCompleted.HasValue && nkUser.IsCompleted.Value)
                {
                    result.success = false;
                    result.message = string.Format("{0}로 이미 전환된 아이디입니다.\\n해당계정으로 로그인해주세요", nkUser.CompassionID);
                    return result;

                    // 미전환 계정
                }
                else
                {
                    //var list = dao.sp_tSponsorMaster_get_f(newUserId, "", "", "", "", "", "").ToList();
                    Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                    Object[] op2 = new Object[] { newUserId, "", "", "", "", "", "" };
                    var list = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

                    if (list.Count < 1)
                    {

                        var sponsorId = new SponsorAction().GetNewSponsorID();

                        if (string.IsNullOrEmpty(sponsorId))
                        {
                            result.success = false;
                            result.message = "시스템 장애발생 잠시 후 다시 시도해주세요";
                            return result;
                        }

                        //var userResult = dao.sp_tSponsorMaster_insert_f(CodeAction.ChannelType, "", sponsorId, "", "", "", "", "", nkUser.UserName, "", nkUser.BirthDate, "", "", "", "", "NK회원전환",
                        //    newUserId, "14세이상", null, null, null, null, null, userPwd, null, null, null, nkUser.Mobile, nkUser.Email, nkUser.Phone, null, null, null,
                        //    nkUser.ZipCode, string.Format("{0}({1})", nkUser.AddrDoro1, nkUser.AddrBungi1), nkUser.Addr2).First();
                        Object[] op3 = new Object[] { "ChannelType", "SponsorID", "tempSponsorID", "ConID", "ConIDCheck", "JuminID", "FirstName", "LastName", "SponsorName", "ComRegistration", "BirthDate", "BirthDateClass", "GenderCode", "CampaignID", "CampaignTitle", "Remark", "UserID", "UserClass", "CertifyDate", "CertifyOrgan", "ParentName", "ParentJuminID", "ParentMobile", "UserPW", "ManagerName", "FilePath", "AuthMethod", "Mobile", "Email", "Phone", "UserGroup", "AgreePhone", "AgreeEmail", "ZipCode", "Addr1", "Addr2" };
                        Object[] op4 = new Object[] { CodeAction.ChannelType, "", sponsorId, "", "", "", "", "", nkUser.UserName, "", nkUser.BirthDate, "", "", "", "", "NK회원전환", newUserId, "14세이상", null, null, null, null, null, userPwd, null, null, null, nkUser.Mobile, nkUser.Email, nkUser.Phone, null, null, null, nkUser.ZipCode, string.Format("{0}({1})", nkUser.AddrDoro1, nkUser.AddrBungi1), nkUser.Addr2 };
                        var userResult = www6.selectSPAuth("sp_tSponsorMaster_insert_f", op3, op4).DataTableToList<sp_tSponsorMaster_insert_fResult>().First();

                        if (userResult.Result.Value == 'Y')
                        {

                            nkUser.UserPW = "";
                            nkUser.ZipCode = "";
                            nkUser.Addr2 = "";
                            nkUser.AddrBungi1 = "";
                            nkUser.AddrDoro1 = "";
                            nkUser.BirthDate = "";
                            nkUser.ChurchName = "";
                            nkUser.Email = "";
                            nkUser.LocationType = "";
                            nkUser.Mobile = "";
                            nkUser.Phone = "";
                            nkUser.UserName = "";

                            nkUser.IsCompleted = true;
                            nkUser.CompleteDate = DateTime.Now;
                            nkUser.CompassionID = newUserId;
                            //dao.SubmitChanges();
                            www6.updateAuth(nkUser);

                            result.success = true;
                            return result;

                        }
                        else
                        {

                            result.success = false;
                            result.message = userResult.Result_String;
                            return result;

                        }
                    }
                    else
                    {

                        var user = list.First();
                        if (nkUser.UserName == user.SponsorName && !string.IsNullOrEmpty(nkUser.BirthDate) && !string.IsNullOrEmpty(user.BirthDate) && nkUser.BirthDate == user.BirthDate)
                        {

                            nkUser.UserPW = "";
                            nkUser.ZipCode = "";
                            nkUser.Addr2 = "";
                            nkUser.AddrBungi1 = "";
                            nkUser.AddrDoro1 = "";
                            nkUser.BirthDate = "";
                            nkUser.ChurchName = "";
                            nkUser.Email = "";
                            nkUser.LocationType = "";
                            nkUser.Mobile = "";
                            nkUser.Phone = "";
                            nkUser.UserName = "";

                            nkUser.IsCompleted = true;
                            nkUser.CompleteDate = DateTime.Now;
                            nkUser.CompassionID = newUserId;

                            //dao.sp_tSponsorMaster_change_pwd_f(newUserId, userPwd);
                            Object[] op5 = new Object[] { "UserID", "UserPW" };
                            Object[] op6 = new Object[] { newUserId, userPwd };
                            www6.selectSPAuth("sp_tSponsorMaster_change_pwd_f", op5, op6);
                            //dao.SubmitChanges();
                            www6.updateAuth(nkUser);

                            result.success = true;
                            return result;

                        }
                        else
                        {

                            result.success = false;
                            result.message = "이미 사용중인 아이디입니다. 새로운 아이디를 입력해주세요.";
                            result.action = "change";
                            return result;

                        }

                    }


                }
            }
        }


	}

}