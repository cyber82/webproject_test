﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;

public static class SiteMapExtension
{
    public static Menu ToMenu(this SiteMapNode node)
    {
        return new Menu().Generate(node);
    }

    public class Menu
    {

        public string url;
        public string name;
        public string key;
        public string priority;
        public string domain;

        public List<Menu> sub;

        public Menu Generate(SiteMapNode root)
        {

            Menu result = new Menu();

            result.name = root.Title;
            result.key = root.ResourceKey;
            result.priority = root["priority"];
            result.domain = root["domain"];
            result.url = root.Url;

            //HttpRequest hr;
            var isLocal = HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("local") > -1;

            if (result.domain == "auth")
            {
                if (isLocal)
                    result.url = "https://auth.local.compassionkr.com" + "/" + result.url;
                else
                    result.url = ConfigurationManager.AppSettings["domain_auth"] + "/" + result.url;
            }
            else if (result.domain == "external")
            {
                if (!string.IsNullOrEmpty(result.url) && result.url.Length > 0)
                    result.url = result.url.ToString();
            }
            else
            {
                if (!string.IsNullOrEmpty(result.url) && result.url.Length > 0)
                {
                    if (isLocal)
                        result.url = "http://www.local.compassionkr.com/" + result.url.Substring(1);
                    else
                        result.url = ConfigurationManager.AppSettings["domain"] + result.url.Substring(1);
                }

            }

            if (root.HasChildNodes)
                result.sub = new List<Menu>();
            foreach (SiteMapNode node in root.ChildNodes)
            {
                var display = node["display"];
                if (display == null || display != "none")
                {
                    result.sub.Add(this.Generate(node));
                }

            }

            return result;
        }
    }

}


