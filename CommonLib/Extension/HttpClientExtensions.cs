﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;


public static class HttpClientExtensions {
	private const string RateLimitRemainingHeader = "X-Ratelimit-Remaining";
	private const string RateLimitHeader = "X-Ratelimit-Limit";

	public static async Task<T> ExecuteAsync<T>(this HttpClient client, HttpRequestMessage request) {
		var response = await client.SendAsync(request);
		string resultData = await response.Content.ReadAsStringAsync();
		var result = JsonConvert.DeserializeObject<T>(resultData);

		var endpointResponse = result as Response;

		if (endpointResponse != null) {
			if (response.Headers.Contains(RateLimitHeader)) {
				endpointResponse.RateLimitLimit =
					response.Headers
						.GetValues(RateLimitHeader)
						.Select(int.Parse)
						.SingleOrDefault();
			}

			if (response.Headers.Contains(RateLimitRemainingHeader)) {
				endpointResponse.RateLimitRemaining =
					response.Headers
						.GetValues(RateLimitRemainingHeader)
						.Select(int.Parse)
						.SingleOrDefault();
			}
		}

		return result;
	}

	public abstract class Response {
		/// <summary>
		/// Gets or sets the meta.
		/// </summary>
		/// <value>
		/// The meta.
		/// </value>
		public Meta Meta {
			get;
			set;
		}

		/// <summary>
		/// The total number of calls allowed within the 1-hour window
		/// </summary>
		public int RateLimitLimit {
			get;
			set;
		}

		/// <summary>
		/// The remaining number of calls available to your app within the 1-hour window
		/// </summary>
		public int RateLimitRemaining {
			get;
			set;
		}
	}

	public class Meta {
		/// <summary>
		/// Gets or sets the code.
		/// </summary>
		/// <value>
		/// The code.
		/// </value>
		public HttpStatusCode Code {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the type of the error.
		/// </summary>
		/// <value>
		/// The type of the error.
		/// </value>
		[JsonProperty("error_type")]
		public string ErrorType {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		/// <value>
		/// The error message.
		/// </value>
		[JsonProperty("error_message")]
		public string ErrorMessage {
			get;
			set;
		}
	}

}



