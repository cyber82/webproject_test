﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System.Security.Cryptography;
using Ganss.XSS;
using CsQuery;

public static class XssExtension
{



	public static string RemoveXss(this string s) {
		string result = "";
		var sanitizer = new HtmlSanitizer();
		sanitizer.AllowedTags.Add("iframe");
		result = sanitizer.Sanitize(s);

		result = removeNotYoutubeIframe(result);
		return result;
	}

	public static string removeNotYoutubeIframe(string html) {
		CQ dom = html;
		CQ iframeArray = dom["iframe"];

		foreach (var iframe in iframeArray) {
			var iframe_src = iframe["src"];
			if (iframe_src.IndexOf("www.youtube.com") < 0) {
				iframe.Remove();
			}
		}
		return dom.Render();
	}

	

	

}

