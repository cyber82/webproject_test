﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;

public static class DateExtension {
	public static string TimeAgo(this DateTime date) {

		TimeSpan timeSince = DateTime.Now.Subtract(date);
		
		if (timeSince.TotalMilliseconds < 1)
			return "";
		if (timeSince.TotalMinutes < 1)
			return "1분이내";
		if (timeSince.TotalMinutes < 2)
			return "1 분전";
		if (timeSince.TotalMinutes < 60)
			return string.Format("{0} 분전", timeSince.Minutes);
		if (timeSince.TotalMinutes < 120)
			return "1 시간전";
		if (timeSince.TotalHours < 24)
			return string.Format("{0} 시간전", timeSince.Hours);
		if (timeSince.TotalDays == 1)
			return "어제";
		if (timeSince.TotalDays < 7)
			return string.Format("{0} 일전", timeSince.Days);
		if (timeSince.TotalDays < 14)
			return "지난주";
		if (timeSince.TotalDays < 21)
			return "2 주전";
		if (timeSince.TotalDays < 28)
			return "3 주전";
		if (timeSince.TotalDays < 60)
			return "지난달";
		if (timeSince.TotalDays < 365)
			return string.Format("{0} 달전", Math.Round(timeSince.TotalDays / 30));
		if (timeSince.TotalDays < 730)
			return "작년";

		//last but not least...
		return string.Format("{0} 년전", Math.Round(timeSince.TotalDays / 365));

	}

	public static string TimeIn(this DateTime date) {

		TimeSpan timeSince = date.Subtract(DateTime.Now);

		if (timeSince.TotalMilliseconds < 1)
			return "";
		if (timeSince.TotalMinutes < 1)
			return "1분이내";
		if (timeSince.TotalMinutes < 2)
			return "1 분후";
		if (timeSince.TotalMinutes < 60)
			return string.Format("{0} 분후", timeSince.Minutes );
		if (timeSince.TotalMinutes < 120)
			return "1 시간후";
		if (timeSince.TotalHours < 24)
			return string.Format("{0} 시간후", timeSince.Hours );
		if (timeSince.TotalDays == 1)
			return "내일";
		if (timeSince.TotalDays < 7)
			return string.Format("{0} 일후", timeSince.Days );
		if (timeSince.TotalDays < 14)
			return "다음주";
		if (timeSince.TotalDays < 21)
			return "2 주후";
		if (timeSince.TotalDays < 28)
			return "3 주후";
		if (timeSince.TotalDays < 60)
			return "다음달";
		if (timeSince.TotalDays < 365)
			return string.Format("{0} 달후", Math.Round(timeSince.TotalDays / 30));
		if (timeSince.TotalDays < 730)
			return "내년";

		//last but not least...
		return string.Format("{0} 년후", Math.Round(timeSince.TotalDays / 365));

	}

	public static string ToDHM(this DateTime date, DateTime compare_date) {

		TimeSpan ts = date.Subtract(compare_date);
		if (compare_date > date)
			ts = compare_date.Subtract(date);

		var d = Math.Truncate(ts.TotalDays);
		var h = Math.Truncate(ts.TotalHours);
		var m = Math.Truncate(ts.TotalMinutes);

		if (d > 0)
			return string.Format("{0}일 {1}시간 {2}분" , d , h - (d * 24) , m - (h * 60));
		else if (h > 0)
			return string.Format("{0}시간 {1}분", h , m - (h * 60));
		else
			return string.Format("{0}분", m);

	}

	public static DateTime UnixTimeStampToDateTime(double unixTimeStamp) {
		// Unix timestamp is seconds past epoch
		System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
		dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
		return dtDateTime;
	}

	public static int MonthDifference(this DateTime lValue, DateTime rValue) {
		return Math.Abs((lValue.Month - rValue.Month) + 12 * (lValue.Year - rValue.Year));
	}

	// 만나이
	public static int GetAge( this DateTime dt ) {

		var now = DateTime.Now;
		if(dt.Month < now.Month) {
			return now.Year - dt.Year;
		} else if(dt.Month == now.Month) {
			if(dt.Day <= now.Day) {
				return now.Year - dt.Year;
			} else
				return now.Year - dt.Year - 1;
		} else {
			return now.Year - dt.Year - 1;
		}

	}

	public static double DaysAgo( this DateTime date ) {

		TimeSpan timeSince = DateTime.Now.Subtract(date);
		return timeSince.TotalDays;
		
	}
}


