﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Web.UI.WebControls;

    public static class FileExtension
    {

		public static string GetFileName(this string path) {
			return path.Split('/')[path.Split('/').Length - 1];
		}

		public static string GetUniqueName(this string fileName, HttpContext context, string path) {
			string fullPath = context.Server.MapPath(path + fileName);
			if (!File.Exists(fullPath))
				return fileName;

			int i = 0;
			string newFileName = fileName;
			do {
				string name = Path.GetFileNameWithoutExtension(fileName);
				string ext = Path.GetExtension(fileName);

				newFileName = string.Format("{0}({1}){2}", name, i.ToString(), ext);
				if (!File.Exists(context.Server.MapPath(path + newFileName)))
					break;

				i++;
			} while (true);

			return newFileName;
		}

		public static string GetUniqueName(this HttpPostedFile file, HttpContext context, string path) {
			string fileName = file.FileName.Split('\\')[file.FileName.Split('\\').Length - 1];
			string fullPath = context.Server.MapPath(path + fileName);
			if (!File.Exists(fullPath))
				return fileName;

			int i = 0;
			string newFileName = fileName;
			do {
				string name = Path.GetFileNameWithoutExtension(fileName);
				string ext = Path.GetExtension(fileName);

				newFileName = string.Format("{0}({1}){2}", name, i.ToString(), ext);
				if (!File.Exists(context.Server.MapPath(path + newFileName)))
					break;

				i++;
			} while (true);

			return newFileName;
		}

		public static void SaveAs(this HttpPostedFile file, HttpContext context, string path , string fileName) {

			if (!Directory.Exists(context.Server.MapPath(path))) {
				Directory.CreateDirectory(context.Server.MapPath(path));
			}

			string savePath = path + fileName;
			savePath = context.Server.MapPath(savePath);
			file.SaveAs(savePath);

		}
		

    }

