﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using Microsoft.AspNet.FriendlyUrls;

public static class SiteExtension {


	public static string Href(this HttpContext context, string virtualPath, params object[] segments) {
		string path = FriendlyUrl.Href(virtualPath, segments);
		return string.Format("{0}", path);
	}


	public static int ToPercent(this int price, int origin_price) {

		if(price < 1)
			return 0;
		return Convert.ToInt32(price * 100.0 / (origin_price < 1 ? price : origin_price));

	}

	public static string ShowNewIcon(this object date , string type) {

		var template = @"<span class='icon_new3'></span>";

		if (type == "box") {
			template = @"<div class='new'><span class='icon_new2'>NEW</span></div>";
		}
		if (date is DateTime) {
			TimeSpan timeSince = DateTime.Now.Subtract((DateTime)date);
			var days = Convert.ToInt32(ConfigurationManager.AppSettings["new_days"]);
			if (timeSince.TotalDays <= days)
				return template;	
		} 

		return "";
		
	}

	public static string GetBoardStatusName(this string s, DateTime dateBegin, DateTime dateEnd) {
		string result = "";
		if (System.DateTime.Now < dateBegin) {
			result = "게시전";
		} else if (System.DateTime.Now > dateEnd) {
			result = "종료";
		} else {
			switch (s) {
				case "ing":
					result = "진행중";
					break;
				case "end":
					result = "종료";
					break;
			}
		}

		return result;
	}
}


