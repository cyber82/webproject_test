﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System.Security.Cryptography;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public static class ImageExtension {

	public static bool IsImage(this string path) {
		string[] exts = new string[] { ".jpg", ".gif", ".png", ".jpeg", ".bmp" };
		return exts.Contains(Path.GetExtension(path).ToLower());
	}

	public static Image CropImage(Image i, int w, int h) {
		Image outimage;
		MemoryStream mm = null;
		try {
			if (i.Height < h) {
				h = i.Height;
			}

			if (i.Width < w) {
				w = i.Height;
			}

			float offsetX = (i.Width - w) / 2;
			float offsetY = (i.Height - h) / 2;

			using (Bitmap bm = new Bitmap(w, h, PixelFormat.Format24bppRgb)) {
				bm.SetResolution(72, 72);

				using (Graphics graphic = Graphics.FromImage(bm)) {
					graphic.SmoothingMode = SmoothingMode.AntiAlias;
					graphic.InterpolationMode = InterpolationMode.HighQualityBilinear;
					graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;

					graphic.DrawImage(i, new Rectangle(0, 0, w, h), offsetX, offsetY, w, h, GraphicsUnit.Pixel);


					mm = new MemoryStream();
					bm.Save(mm, System.Drawing.Imaging.ImageFormat.Jpeg);
					i.Dispose();
					bm.Dispose();
					graphic.Dispose();
					outimage = Image.FromStream(mm);
				}
			}
			return outimage;

		} catch (Exception ex) {
			throw new Exception("Error cropping image, the error was: " + ex.Message);
		}
	}

	//Image resizing
	public static Image ResizeImage(int w, int h, Image i , int orientation) {

		try {
			int ow = i.Width;
			int oh = i.Height;
			if (ow < w || oh < h)
				return i;

			int sourceWidth = i.Width;
			int sourceHeight = i.Height;

			float nPercent = 0;
			float nPercentW = 0;
			float nPercentH = 0;

			nPercentW = ((float)w / (float)sourceWidth);
			nPercentH = ((float)h / (float)sourceHeight);

			if (nPercentH < nPercentW)
				nPercent = nPercentW;
			else
				nPercent = nPercentH;

			int destWidth = (int)(sourceWidth * nPercent);
			int destHeight = (int)(sourceHeight * nPercent);
			
			Bitmap b = new Bitmap(destWidth, destHeight);
			b.SetResolution(72, 72);
			using (Graphics graphic = Graphics.FromImage((Image)b)) {

				//g.InterpolationMode = InterpolationMode.Default;
				/*
				g.InterpolationMode = InterpolationMode.NearestNeighbor;
				g.PixelOffsetMode = PixelOffsetMode.Half;
				*/
				graphic.SmoothingMode = SmoothingMode.AntiAlias;
				graphic.InterpolationMode = InterpolationMode.HighQualityBilinear;
				graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;

				graphic.DrawImage(i, 0, 0, destWidth, destHeight);

				graphic.Dispose();
			}

			if (orientation == 3) {
				b.RotateFlip(RotateFlipType.Rotate180FlipNone);
			} else if (orientation == 6) {
				b.RotateFlip(RotateFlipType.Rotate90FlipNone);
			} else if (orientation == 8) {
				b.RotateFlip(RotateFlipType.Rotate270FlipNone);
			}

			return CropImage((Image)b, w, h);

		}catch(Exception ex) {

			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			return null;
		}

	}

	public static Image Crop(this Image i, int w, int h) {
		return CropImage(i, w, h);
	}

	public static Image Resize(this Image i, int w, int h) {
		return ResizeImage(w, h, i , 1);
	}

	public static Image Resize(this Image i, int w, int h , int orientation) {
		return ResizeImage(w, h, i , orientation);
	}


}

