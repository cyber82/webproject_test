﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System.Security.Cryptography;

public static class StringExtension {


	public static string UrlEncode( this string input ) {
		return Uri.EscapeDataString(input);
	}

	public static string GetLastImageSrcValue( this string source ) {
		var pattern = @"src=(?:(['""])(?<src>(?:(?!\1).)*)\1|(?<src>[^\s>]+))";
		var regex = new Regex(pattern, RegexOptions.IgnoreCase);

		var matches = regex.Matches(source);
		if(matches.Count < 1)
			return string.Empty;

		var last = matches[matches.Count - 1];
		return last.Groups["src"].Value;

	}

	public static string ToHtml( this string source ) {
		return source.ToHtml(true);
	}

	
	public static string ToHtml( this string source, bool replaceBr ) {
		source = source.EmptyIfNull();

		if(replaceBr)
			source = source.Replace("\n", "<br/>");

		return source
			.Replace("	", "&nbsp;&nbsp;&nbsp;&nbsp;")
			.Replace("&lt;", "<")
			.Replace("&gt;", ">")
			.Replace("&amp;", "&")
			.Replace("&nbsp;", " ")
			.Replace("&quot;", "\"")
			;
	}

	public static string ToPlain( this string source ) {

		return source.EmptyIfNull().Replace("<br>", "\r\n")
			.Replace("<br/>", "\r\n")
			.Replace("\"", "&quot;")
			.Replace("'", "&apos;")
			.Replace("<", "&lt;")
			.Replace(">", "&gt;")
			.Replace("&", "&amp;");
	}

	public static string ToDBString( this string source ) {
		return source.Replace("'", "''")
			;
	}

	public static string ToDateFormat( this string source, string delimiter ) {
		if(string.IsNullOrEmpty(source))
			return source;

		if(source.Length == 6)
			return string.Format("{1}{0}{2}", delimiter, source.Substring(0, 4), source.Substring(4, 2));

		if(source.Length < 8)
			return source;

		return string.Format("{1}{0}{2}{0}{3}", delimiter, source.Substring(0, 4), source.Substring(4, 2), source.Substring(6, 2));
	}

	public static string Limit( this string s, int length ) {
		if(s == null)
			return "";
		if(s.Length > length)
			return s.Substring(0, length) + "...";
		return s;
	}

	public static bool IsValidEmailAddress( this string s ) {
		return new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,6}$").IsMatch(s);
	}

	public static bool IsValidUrl( this string url ) {
		string strRegex = "^(https?://)"
	+ "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //user@
	+ @"(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP- 199.194.52.184
	+ "|" // allows either IP or domain
	+ @"([0-9a-z_!~*'()-]+\.)*" // tertiary domain(s)- www.
	+ @"([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]" // second level domain
	+ @"(\.[a-z]{2,6})?)" // first level domain- .com or .museum is optional
	+ "(:[0-9]{1,5})?" // port number- :80
	+ "((/?)|" // a slash isn't required if there is no file name
	+ "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
		return new Regex(strRegex).IsMatch(url);
	}

	static MD5CryptoServiceProvider s_md5 = null;

	public static string MD5( this string s ) {

		if(string.IsNullOrEmpty(s))
			return "";
		if(s_md5 == null) //creating only when needed
			s_md5 = new MD5CryptoServiceProvider();
		Byte[] newdata = Encoding.Default.GetBytes(s);
		Byte[] encrypted = s_md5.ComputeHash(newdata);
		return BitConverter.ToString(encrypted).Replace("-", "").ToLower();
	}

	public static string EmptyIfNull( this string s ) {
		if(string.IsNullOrEmpty(s))
			return string.Empty;
		else
			return s;
	}

	public static string ValueIfNull( this string s, string val ) {
		if(string.IsNullOrEmpty(s))
			return val;
		else
			return s;
	}

	public static string ConvertEncoding( this string s, Encoding src, Encoding dst ) {
		byte[] srcBytes = src.GetBytes(s);
		byte[] dstBytes = Encoding.Convert(src, dst, srcBytes);
		return dst.GetString(dstBytes);
	}

	/// <summary>
	/// encrypt , decrypt
	/// </summary>
	/* 반드시 Crypto 와 salt 동기화해야 한다. */
	const string encryptSalt = "cps.2016.salt.aes256.pentabreed.cps.2016.salt.aes256.pentabreed.";

	public static string Encrypt( this string s ) {
		return Encrypt(s, encryptSalt);
	}

	public static string Encrypt( this string s, string salt ) {

		if(string.IsNullOrEmpty(s))
			return s;
		return AESEncrypt256(s, salt.Substring(0, 256 / 8));
		/*
			byte[] keyArray;
			byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(s);

			MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
			keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(salt));
			hashmd5.Clear();

			TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
			tdes.Key = keyArray;
			tdes.Mode = CipherMode.ECB;
			tdes.Padding = PaddingMode.PKCS7;

			ICryptoTransform cTransform = tdes.CreateEncryptor();
			byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
			tdes.Clear();
			return Convert.ToBase64String(resultArray, 0, resultArray.Length);
			*/
	}

	public static string Decrypt( this string s ) {
		return Decrypt(s, encryptSalt);
	}

	public static string Decrypt( this string s, string salt ) {
		if(string.IsNullOrEmpty(s))
			return s;
		return AESDecrypt256(s, salt.Substring(0, 256 / 8));

		/*
			byte[] keyArray;
			byte[] toEncryptArray = Convert.FromBase64String(s);

			MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
			keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(salt));
			hashmd5.Clear();

			TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
			tdes.Key = keyArray;
			
			tdes.Mode = CipherMode.ECB;
			tdes.Padding = PaddingMode.PKCS7;

			ICryptoTransform cTransform = tdes.CreateDecryptor();
			byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);          
			tdes.Clear();
			return UTF8Encoding.UTF8.GetString(resultArray);
			*/
	}

	public static string Mask( this string s, string ch, int append_length ) {
		if(string.IsNullOrEmpty(s))
			return "";

		if(s.Length - append_length <= 0)
			return s;

		s = s.Substring(0, s.Length - append_length);
		for(int i = 0; i < append_length; i++) {
			s += ch.ToString();
		}

		return s;
	}

	public static string StripHtml( this string s ) {
		return Regex.Replace(s, "<.*?>", String.Empty);
	}

	public static string SHA256Hash( this string s ) {
		SHA256 sha = new SHA256Managed();
		byte[] hash = sha.ComputeHash(Encoding.ASCII.GetBytes(s));

		StringBuilder stringBuilder = new StringBuilder();
		foreach(byte b in hash) {
			stringBuilder.AppendFormat("{0:x2}", b);
		}
		return stringBuilder.ToString();
	}

	//AES_256 암호화
	public static String AESEncrypt256( String Input, String key ) {
		RijndaelManaged aes = new RijndaelManaged();
		aes.KeySize = 256;
		aes.BlockSize = 128;
		aes.Mode = CipherMode.CBC;
		aes.Padding = PaddingMode.PKCS7;
		aes.Key = Encoding.UTF8.GetBytes(key);
		aes.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		var encrypt = aes.CreateEncryptor(aes.Key, aes.IV);
		byte[] xBuff = null;
		using(var ms = new MemoryStream()) {
			using(var cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write)) {
				byte[] xXml = Encoding.UTF8.GetBytes(Input);
				cs.Write(xXml, 0, xXml.Length);
			}

			xBuff = ms.ToArray();
		}

		String Output = Convert.ToBase64String(xBuff);
		return Output;
	}


	//AES_256 복호화
	public static String AESDecrypt256( String Input, String key ) {
		RijndaelManaged aes = new RijndaelManaged();
		aes.KeySize = 256;
		aes.BlockSize = 128;
		aes.Mode = CipherMode.CBC;
		aes.Padding = PaddingMode.PKCS7;
		aes.Key = Encoding.UTF8.GetBytes(key);
		aes.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		var decrypt = aes.CreateDecryptor();
		byte[] xBuff = null;
		using(var ms = new MemoryStream()) {
			using(var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write)) {
				byte[] xXml = Convert.FromBase64String(Input);
				cs.Write(xXml, 0, xXml.Length);
			}

			xBuff = ms.ToArray();
		}

		String Output = Encoding.UTF8.GetString(xBuff);
		return Output;
	}


	//AES_128 암호화
	public static String AESEncrypt128( String Input, String key ) {

		RijndaelManaged RijndaelCipher = new RijndaelManaged();

		byte[] PlainText = System.Text.Encoding.Unicode.GetBytes(Input);
		byte[] Salt = Encoding.ASCII.GetBytes(key.Length.ToString());

		PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(key, Salt);
		ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));

		MemoryStream memoryStream = new MemoryStream();
		CryptoStream cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);

		cryptoStream.Write(PlainText, 0, PlainText.Length);
		cryptoStream.FlushFinalBlock();

		byte[] CipherBytes = memoryStream.ToArray();

		memoryStream.Close();
		cryptoStream.Close();

		string EncryptedData = Convert.ToBase64String(CipherBytes);

		return EncryptedData;
	}

	//AE_S128 복호화
	public static String AESDecrypt128( String Input, String key ) {
		RijndaelManaged RijndaelCipher = new RijndaelManaged();

		byte[] EncryptedData = Convert.FromBase64String(Input);
		byte[] Salt = Encoding.ASCII.GetBytes(key.Length.ToString());

		PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(key, Salt);
		ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
		MemoryStream memoryStream = new MemoryStream(EncryptedData);
		CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);

		byte[] PlainText = new byte[EncryptedData.Length];

		int DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);

		memoryStream.Close();
		cryptoStream.Close();

		string DecryptedData = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);

		return DecryptedData;
	}

	public static string GetRandomString( int length ) {
		//It will generate string with combination of small,capital letters and numbers    
		char[] charArr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
		string randomString = string.Empty;
		Random objRandom = new Random();
		for(int i = 0; i < length; i++) {
			//Don't Allow Repetation of Characters         
			int x = objRandom.Next(1, charArr.Length);
			if(!randomString.Contains(charArr.GetValue(x).ToString()))
				randomString += charArr.GetValue(x);
			else
				i--;
		}
		return randomString;
	}

	public static string TranslatePhoneNumber(this string s) {
		string result = "";
		Int64 tryParse;
		if(Int64.TryParse(s, out tryParse)){
			if (s.Length == 10) {
				result = s.Substring(0, 3) + "-" + s.Substring(3, 3) + "-" + s.Substring(6, 4);
			}else if(s.Length == 11) {
				result = s.Substring(0, 3) + "-" + s.Substring(3, 4) + "-" + s.Substring(7, 4);
			}
		}

		return result;
	}
}

