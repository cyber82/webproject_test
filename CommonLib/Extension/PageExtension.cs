﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System.Security.Cryptography;

    public static class PageExtension
    {
		public static Control FindAnyControl(this Page page, string controlId) {
			return FindControlRecursive(controlId, page.Form);
		}

		public static Control FindAnyControl(this UserControl control, string controlId) {
			return FindControlRecursive(controlId, control);
		}

		public static Control FindControlRecursive(string controlId, Control parent) {
			foreach (Control control in parent.Controls) {
				Control result = FindControlRecursive(controlId, control);
				if (result != null) {
					return result;
				}
			}
			return parent.FindControl(controlId);
		}

		public static T Clone<T>(this T source) {
			var dcs = new DataContractSerializer(typeof(T));
			using (var ms = new System.IO.MemoryStream()) {
				dcs.WriteObject(ms, source);
				ms.Seek(0, System.IO.SeekOrigin.Begin);
				return (T)dcs.ReadObject(ms);
			}
		}

    }

