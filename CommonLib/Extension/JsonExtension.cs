﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

    public static class JsonExtension
    {
		public static string ToJson(this object obj) {
			var settings = new JsonSerializerSettings();
			return JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.None, settings);
		}

	public static string ToLowerCaseJson( this object obj ) {
		return JsonWriter.toLowerCaseJson(obj);
	}

	public static T ToObject<T>(this string source) {
			return JsonConvert.DeserializeObject<T>(source);
		}

    }

