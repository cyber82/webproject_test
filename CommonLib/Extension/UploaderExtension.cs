﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System.Security.Cryptography;
using Ganss.XSS;
using CsQuery;

public static class UploaderExtension {

	public static string WithFileServerHost( this string source ) {
		source = source.EmptyIfNull();

		if (source.IndexOf( "src=\"https://www.youtube.com" ) > -1 || source.IndexOf( "src=\"http://www.youtube.com" ) > -1) {
			source = source.Replace( "src=\"https://www.youtube.com", "src1=\"https://www.youtube.com" );
			source = source.Replace( "src=\"http://www.youtube.com", "src1=\"http://www.youtube.com" );
			source = source.Replace( "src=\"", string.Format( "src=\"{0}", ConfigurationManager.AppSettings["domain_file"] ) );
			source = source.Replace( "src1=\"https://www.youtube.com", "src=\"https://www.youtube.com" );
			source = source.Replace( "src1=\"http://www.youtube.com", "src=\"http://www.youtube.com" );
			return source;
		}

		if(source.StartsWith("http"))
			return source;

		if (source.IndexOf("src=\"http") > -1)
			return source;

		if(source.IndexOf("src=") > -1)
			return source.Replace("src=\"", string.Format("src=\"{0}", ConfigurationManager.AppSettings["domain_file"]));


		if (source.StartsWith( "/" ))
			return ConfigurationManager.AppSettings["domain_file"] + source;
		else
			return source;
		
	}
}