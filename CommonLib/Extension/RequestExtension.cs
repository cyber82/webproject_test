﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.UI;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public static class RequestExtension {

	public static bool CheckNumeric( this string source ) {
		if(string.IsNullOrEmpty(source))
			return true;

		int result;
		return Int32.TryParse(source, out result);
	}

	public static bool CheckAlphaNumeric( this string source ) {
		if(string.IsNullOrEmpty(source))
			return true;
		Regex reg = new Regex(@"^([a-zA-Z0-9]+)$");
		return reg.IsMatch(source);
	}

	public static bool CheckAlphabet( this string source ) {
		if(string.IsNullOrEmpty(source))
			return true;
		Regex reg = new Regex(@"^([a-zA-Z]+)$");
		return reg.IsMatch(source);
	}

	public static string EscapeSqlInjection( this string source ) {
		if(string.IsNullOrEmpty(source))
			return source;

		return source.Replace("'", "").Replace("\"", "").Replace("--", "").Replace(";", "").Replace("%", "").Replace("<", "").Replace(">", "")
			.Replace("declare", "").Replace("exec", "").Replace("sp_", "").Replace("xp_", "").Replace("insert", "")
			.Replace("update", "").Replace("delete", "").Replace("drop", "").Replace("select", "").Replace("union", "").Replace("truncate", "")
			.Replace("script", "");

	}

	/// <summary>
	/// TextBox의 값 sql injection 처리
	/// </summary>
	/// <param name="source"></param>
	public static void EscapeAllTextBox( this ControlCollection source ) {
		foreach(Control c in source) {
			if(c.GetType() == typeof(TextBox)) {
				((TextBox)c).Text = ((TextBox)c).Text.EscapeSqlInjection();
			} else if(c.GetType() == typeof(HtmlInputText)) {
				((HtmlInputText)c).Value = ((HtmlInputText)c).Value.EscapeSqlInjection();
			}
		}
	}

	/// <summary>
	/// Request 값을 TextBox에 적용
	/// </summary>
	/// <param name="source"></param>
	/// <param name="context"></param>
	public static void BindAllTextBox( this ControlCollection source, HttpContext context ) {
		foreach(Control c in source) {

			var val = context.Request[c.ClientID];

			if(c.GetType() == typeof(TextBox)) {
				((TextBox)c).Text = val;
			} else if(c.GetType() == typeof(HtmlInputText)) {
				((HtmlInputText)c).Value = val;
			} else if(c.GetType() == typeof(DropDownList)) {
				((DropDownList)c).SelectedValue = val;
			} else if(c.GetType() == typeof(RadioButtonList)) {
				if(!string.IsNullOrEmpty(val))
					((RadioButtonList)c).SelectedValue = val;
			} else if(c.GetType() == typeof(CheckBox)) {
				if(!string.IsNullOrEmpty(val))
					((CheckBox)c).Checked = val == "1" || val == "on";
			} else if(c.GetType() == typeof(CheckBoxList)) {
				if(!string.IsNullOrEmpty(val)) {
					foreach(var v in val.Split(',')) {
						((CheckBoxList)c).Items.FindByValue(v).Selected = true;
					}

					/*
					 전부 체크되어 있으면,
					 * 값이 "" 인것도 체크
					 */
					int selectedCount = 0;
					foreach(ListItem v in ((CheckBoxList)c).Items) {
						if(v.Selected)
							selectedCount++;
					}

					if(selectedCount + 1 == ((CheckBoxList)c).Items.Count) {
						var item = ((CheckBoxList)c).Items.FindByValue("");
						if(item != null)
							item.Selected = true;
					}
				}

			}

		}

		EscapeAllTextBox(source);
	}

	public static string UrlWithoutQueryString( this HttpRequest arg ) {
		var str = arg.Url.ToString().Replace("default.aspx", "");

		if (str.IndexOf("?") > -1) {
			return str.Substring(0, str.LastIndexOf("?"));
		}

		return str;
	}

	public static string UrlEx( this HttpRequest arg ) {
		return arg.Url.ToString().Replace("default.aspx", "");
	}

	public static string UrlEx( this HttpRequest arg, string except ) {
		return arg.Url.ToString().Replace(except, "");
	}

	public static string OnlyPath( this HttpRequest arg ) {
		return arg.Path.Substring(0, arg.Path.LastIndexOf("/") + 1);
	}

	public static double LongIP( this HttpRequest arg ) {
		return LongIP(arg, string.Empty);
	}

	public static double LongIP( this HttpRequest arg, string val ) {

		string sourceIP = arg.UserHostAddress;

		if(!string.IsNullOrEmpty(val))
			sourceIP = val;

		int i;
		string[] arrDec;
		double num = 0;
		if(sourceIP == "") {
			return 0;
		} else {
			arrDec = sourceIP.Split('.');
			for(i = arrDec.Length - 1; i >= 0; i--) {
				num += ((int.Parse(arrDec[i]) % 256) * Math.Pow(256, (3 - i)));
			}
			return num;
		}
	}
	
	public static string Domain( this HttpRequest arg ) {

		//Return variable declaration
		var appPath = string.Empty;

		//Getting the current context of HTTP request
		var context = HttpContext.Current;

		//Checking the current context content
		if(context != null) {
			//Formatting the fully qualified website url/name
			appPath = string.Format("{0}://{1}{2}",
									context.Request.Url.Scheme,
									context.Request.Url.Host,
									context.Request.Url.Port == 80 || context.Request.Url.Port == 443
										? string.Empty
										: ":" + context.Request.Url.Port);
		}

		return appPath;

	}
}

