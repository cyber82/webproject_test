﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Text.RegularExpressions;
using System.Text;
using System.Security.Cryptography;
using System.IO;

public partial class Crypto
{
	[return: SqlFacet(MaxSize = -1)]
	[Microsoft.SqlServer.Server.SqlFunction]
    public static SqlString enc( [SqlFacet(MaxSize = -1)]SqlString plain)
    {
		if(plain.Equals(SqlString.Null) || plain.Equals(""))
			return plain;
		return (SqlString)Crypto.Encrypt(Convert.ToString(plain));
		//return (SqlString)Convert.ToString(plain).Encrypt();
		// 여기에 코드를 입력합니다.
		//return new SqlString (string.Empty);
	}

	[return: SqlFacet(MaxSize = -1)]
	[Microsoft.SqlServer.Server.SqlFunction]
    public static SqlString dec( [SqlFacet(MaxSize = -1)]SqlString encrypt )
    {

		if(encrypt.Equals(SqlString.Null) || encrypt.Equals(""))
			return encrypt;
		return Crypto.Decrypt(Convert.ToString(encrypt));
		//return (SqlString)Convert.ToString(plain).Encrypt();
		// 여기에 코드를 입력합니다.
		//return new SqlString (string.Empty);
	}

	const string encryptSalt = "cps.2016.salt.aes256.pentabreed.cps.2016.salt.aes256.pentabreed.";

	 static string Encrypt( string s ) {
		return Encrypt(s, encryptSalt);
	}

	 static string Encrypt( string s, string salt ) {
		if(string.IsNullOrEmpty(s))
			return s;
		return AESEncrypt256(s, salt.Substring(0, 256 / 8));
		/*
			byte[] keyArray;
			byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(s);

			MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
			keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(salt));
			hashmd5.Clear();

			TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
			tdes.Key = keyArray;
			tdes.Mode = CipherMode.ECB;
			tdes.Padding = PaddingMode.PKCS7;

			ICryptoTransform cTransform = tdes.CreateEncryptor();
			byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
			tdes.Clear();
			return Convert.ToBase64String(resultArray, 0, resultArray.Length);
			*/
	}

	 static string Decrypt( string s ) {
		return Decrypt(s, encryptSalt);
	}

	public static string Decrypt( string s, string salt ) {
		if(string.IsNullOrEmpty(s))
			return s;
		return AESDecrypt256(s, salt.Substring(0, 256 / 8));

		/*
			byte[] keyArray;
			byte[] toEncryptArray = Convert.FromBase64String(s);

			MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
			keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(salt));
			hashmd5.Clear();

			TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
			tdes.Key = keyArray;
			
			tdes.Mode = CipherMode.ECB;
			tdes.Padding = PaddingMode.PKCS7;

			ICryptoTransform cTransform = tdes.CreateDecryptor();
			byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);          
			tdes.Clear();
			return UTF8Encoding.UTF8.GetString(resultArray);
			*/
	}

	//AES_256 암호화
	 static String AESEncrypt256( String Input, String key ) {
		RijndaelManaged aes = new RijndaelManaged();
		aes.KeySize = 256;
		aes.BlockSize = 128;
		aes.Mode = CipherMode.CBC;
		aes.Padding = PaddingMode.PKCS7;
		aes.Key = Encoding.UTF8.GetBytes(key);
		aes.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		var encrypt = aes.CreateEncryptor(aes.Key, aes.IV);
		byte[] xBuff = null;
		using(var ms = new MemoryStream()) {
			using(var cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write)) {
				byte[] xXml = Encoding.UTF8.GetBytes(Input);
				cs.Write(xXml, 0, xXml.Length);
			}

			xBuff = ms.ToArray();
		}

		String Output = Convert.ToBase64String(xBuff);
		return Output;
	}


	//AES_256 복호화
	 static String AESDecrypt256( String Input, String key ) {
		RijndaelManaged aes = new RijndaelManaged();
		aes.KeySize = 256;
		aes.BlockSize = 128;
		aes.Mode = CipherMode.CBC;
		aes.Padding = PaddingMode.PKCS7;
		aes.Key = Encoding.UTF8.GetBytes(key);
		aes.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		var decrypt = aes.CreateDecryptor();
		byte[] xBuff = null;
		using(var ms = new MemoryStream()) {
			using(var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write)) {
				byte[] xXml = Convert.FromBase64String(Input);
				cs.Write(xXml, 0, xXml.Length);
			}

			xBuff = ms.ToArray();
		}

		String Output = Encoding.UTF8.GetString(xBuff);
		return Output;
	}
}
