﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mailSender {
	class Program {
		static void Main(string[] args) {

			DoRecruit();

			DoUserFunding();

			//Console.ReadLine();
		}
		
		static void DoRecruit() {
			var work = new Recruit();
			work.StepProceeded += work_StepProceeded;
			work.DoWork();
		}

		static void DoUserFunding() {
			var work = new UserFunding();
			work.StepProceeded += work_StepProceeded;
			work.DoWork();
		}
		
		static void work_StepProceeded( object sender, CommonEventArgs e) {
			Console.WriteLine(e.Msg);

		}

	}
}
