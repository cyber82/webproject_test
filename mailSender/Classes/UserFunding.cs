﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using CommonLib;

namespace mailSender {

	public class UserFunding {

		const int totalStep = 4;
		public event EventHandler<CommonEventArgs> StepProceeded;
	

		public void DoWork() {

			this.OnFireEvent(new CommonEventArgs() {
				Msg = string.Format("#나눔펀딩=======================================\n{0}/{1}. {2}", 1, totalStep , "준비")
			});

			this.ExpireSoon80();


			this.OnFireEvent(new CommonEventArgs() {
				Msg = string.Format("{0}/{1}. {2}\n\n", 4, totalStep, "완료")
			});

		}

		// 종료까지 80%남은 시점에 노티메일 발송
		void ExpireSoon80() {

			// 1. 메일템플릿 로드
            var path = ConfigurationManager.AppSettings["root"] + @"\mail-template\uf-expiresoon80.html";
            string html = "";

			using(var f = File.OpenText(path)) {
				html = f.ReadToEnd();
			}
			
			// 메일제목
			var subject = ConfigurationManager.AppSettings["uf_expiresoon80_title"];

			// 2. 대상자 선정

			var targets = new List<int>();
            using (FrontDataContext dao = new FrontDataContext())
            {
                // 메일발송이 안된 나눔후원개설목록
                //targets = dao.sp_tUserFunding_expireSoon80_list().Select(p => p.uf_id).ToList();
                Object[] op1 = new Object[] { };
                Object[] op2 = new Object[] { };
                var list = www6.selectSP("sp_tUserFunding_expireSoon80_list", op1, op2).DataTableToList<sp_tUserFunding_expireSoon80_listResult>();
                targets = list.Select(p => p.uf_id).ToList();

                this.OnFireEvent(new CommonEventArgs()
                {
                    Msg = string.Format("{0}/{1}. {2} (총 : {3}명)", 2, totalStep, "대상 조회 완료", targets.Count)
                });


                // 3. 메일 발송
                var stage = ConfigurationManager.AppSettings["stage"];
                if (stage == "dev")
                {
                    //	targets = targets.Take(1).ToList();
                }

                foreach (var target in targets)
                {

                    //var entity = dao.sp_tUserFunding_get(target).FirstOrDefault();
                    Object[] op3 = new Object[] { "uf_id" };
                    Object[] op4 = new Object[] { target };
                    var list2 = www6.selectSP("sp_tUserFunding_get", op3, op4).DataTableToList<sp_tUserFunding_getResult>();
                    var entity = list2.FirstOrDefault();

                    if (entity == null)
                        continue;

                    //var email = dao.sp_message_log_create(target, "나눔펀딩80%경과", "email").First();
                    Object[] op5 = new Object[] { "ml_ref_id", "ml_kind", "ml_method" };
                    Object[] op6 = new Object[] { target, "나눔펀딩80%경과", "email" };
                    var list3 = www6.selectSP("sp_message_log_create", op1, op2).DataTableToList<sp_message_log_createResult>();
                    var email = list3.First();

                    if (email.canSend == "Y")
                    {

                        // 발송
                        var args = new Dictionary<string, string> {
                            {"{pic}" , entity.uf_image.WithFileServerHost() } ,
                            {"{title}" , entity.uf_title} ,
                            {"{typeName}" , entity.uf_type == "child" ? "어린이 결연 펀딩" : "양육을 돕는 펀딩"} ,
                            {"{year}" , entity.uf_date_start.ToString("yyyy")} ,
                            {"{month}" , entity.uf_date_start.ToString("MM")} ,
                            {"{day}" , entity.uf_date_start.ToString("dd")} ,
                            {"{duration}" , string.Format("{0} ~ {1}" , entity.uf_date_start.ToString("yyyy.MM.dd") , entity.uf_date_end.ToString("yyyy.MM.dd")) } ,
                            {"{goal_amount}" , entity.uf_goal_amount.ToString("N0") } ,
                            {"{current_amount}" , entity.uf_current_amount.ToString("N0") } ,
                            {"{creator_name}" , entity.SponsorName} ,
                            {"{creator_id}" , entity.UserID} ,
                            {"{creator_conid}" , entity.ConID} ,
                            {"{creator_sponsorid}" , entity.SponsorID}
                        };

                        AppEmail.Send(ConfigurationManager.AppSettings["uf_sender"],
                            new List<string>() { entity.Email },
                            string.Format(subject, entity.SponsorName),
                            html, args);

                        //dao.message_log.First(p => p.ml_id == email.ml_id).ml_is_send = true;
                        var entity2 = www6.selectQF<message_log>("ml_id", email.ml_id);
                        entity2.ml_is_send = true;

                        //dao.SubmitChanges();
                        www6.update(entity2);

                        this.OnFireEvent(new CommonEventArgs()
                        {
                            Msg = string.Format("{0}/{1}. 발송 ({2})", 3, totalStep, "uf_id=" + entity.uf_id.ToString())
                        });

                    }
                }


            }
			
			
		}
		
		
		void OnFireEvent(CommonEventArgs e) {
			if (StepProceeded != null) {
				StepProceeded(this, e);
			}
		}
	}

}
