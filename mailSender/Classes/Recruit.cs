﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using CommonLib;

namespace mailSender {

	public class Recruit {

		const int totalStep = 4;
		public event EventHandler<CommonEventArgs> StepProceeded;
		
		public void DoWork() {

			this.OnFireEvent(new CommonEventArgs() {
				Msg = string.Format("#채용=======================================\n{0}/{1}. {2}", 1, totalStep , "준비")
			});

			// 1. 메일템플릿 로드
			//var path = System.Environment.CurrentDirectory+ "\\mail-template\\recruit.html";
            var path = ConfigurationManager.AppSettings["root"] + @"\mail-template\recruit.html";

            string html = "";

			using(var f = File.OpenText(path)){
				html = f.ReadToEnd();
			}
			//Console.WriteLine(html);
			
			// 메일제목
			var subject = ConfigurationManager.AppSettings["recruit_title"];

            // 2. 대상자 선정
            using (FrontDataContext dao = new FrontDataContext())
            {
                //var targets = dao.sp_resume_expire_target_list().ToList();
                Object[] op1 = new Object[] {  };
                Object[] op2 = new Object[] {  };
                var targets = www6.selectSP("sp_resume_expire_target_list", op1, op2).DataTableToList<sp_resume_expire_target_listResult>();

                this.OnFireEvent(new CommonEventArgs()
                {
                    Msg = string.Format("{0}/{1}. {2} (총 : {3}명)", 2, totalStep, "대상자 조회 완료", targets.Count)
                });

                // 3. 메일 발송
                var stage = ConfigurationManager.AppSettings["stage"];
                if (stage == "dev")
                {
                    targets = targets.Take(1).ToList();
                }

                foreach (var t in targets)
                {
                    AppEmail.Send(ConfigurationManager.AppSettings["recruit_sender"], new List<string>() { t.r_email }, subject, html, new Dictionary<string, string>() {
                        { "{u_name}",t.r_name }
                    });

                    //dao.resume.First(p => p.r_id == t.r_id).r_expire_mail = true;
                    var entity = www6.selectQF<resume>("r_id", t.r_id);
                    entity.r_expire_mail = true;

                    //dao.SubmitChanges();
                    www6.update(entity);

                    this.OnFireEvent(new CommonEventArgs()
                    {
                        Msg = string.Format("{0}/{1}. 발송 ({2})", 3, totalStep, "email=" + t.r_email + " | " + "name=" + t.r_name)
                    });

                }
            }
			
			this.OnFireEvent(new CommonEventArgs() {
				Msg = string.Format("{0}/{1}. {2}\n\n", 4, totalStep, "완료")
			});

		}
		

		void OnFireEvent(CommonEventArgs e) {
			if (StepProceeded != null) {
				StepProceeded(this, e);
			}
		}
	}

}
