﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
//using NLog;
using System.Configuration;

public class AppEmail
{
	public AppEmail()
	{
	}
	
	// useTemplatePath = true > pathOrBody 의 값은 템플릿경로 , useTemplatePath = false > pathOrBody 의 값은 내용
	public static void Send(string from, List<string> targets, string subject, string body, Dictionary<string, string> keyValues ) {
		try {

			foreach (var keyValue in keyValues) {
				body = body.Replace(keyValue.Key, keyValue.Value);
			}

			body = body.Replace("{host}", ConfigurationManager.AppSettings["domain"]);
			body = body.Replace("{mhost}", ConfigurationManager.AppSettings["domain"].Replace("www.", "m.").Replace("auth.", "m."));
			body = body.Replace("{regdate}", DateTime.Now.ToString("yyyy-MM-dd"));
			body = body.Replace("{full_regdate}", DateTime.Now.ToString());
			

			SmtpClient client = new SmtpClient();
			MailMessage msg = new MailMessage();
			msg.From = new MailAddress(from);

			var stage = ConfigurationManager.AppSettings["stage"];
			if (stage == "dev") {
				msg.To.Add(new MailAddress("whilestomach@gmail.com"));
				msg.To.Add(new MailAddress("stomach@nate.com"));

			} else {
				foreach (string to in targets) {
					msg.To.Add(new MailAddress(to));	
				}
			}
			
			
			msg.IsBodyHtml = true;
			msg.Body = body;
			msg.Subject = subject;

			client.Send(msg);

		} catch (Exception e){
			throw e;
		}
	}
}