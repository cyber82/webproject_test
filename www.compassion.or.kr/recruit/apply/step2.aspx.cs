﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;

public partial class recruit_apply_step2 : FrontBasePage {

	public override bool RequireRecruitLogin{
		get{
			return true;
		}
	}
	
	public override bool RequireSSL {
		get {return true;}
	}
	
	protected int b_id {
		set {
			this.ViewState.Add("b_id", value);
		}
		get {
			if (this.ViewState["b_id"] == null)
				return 0;
			return Convert.ToInt32(this.ViewState["b_id"]);
		}
	}


	const string listPath = "/recruit";


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		// check param
		var requests = Request.GetFriendlyUrlSegments();

		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);


		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}

		base.PrimaryKey = requests[0];

		// 해당 채용으로 로그인 했는지 확인
		if (RecruitSession.GetCookie(Context).bj_id.ToString() != PrimaryKey.ToString()) {
			Response.Redirect(listPath, true);
		}


		ValidateParam();

		LoadData();

	}


	protected void ValidateParam()
    {
		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            // recruit_job검증
            var exist = www6.selectQ<CommonLib.recruit_job>("bj_id", Convert.ToInt32(PrimaryKey));
            //if (!dao.recruit_job.Any(p => p.bj_id == Convert.ToInt32(PrimaryKey)))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            // recruit 검증
            //var recruitJob = dao.recruit_job.First(p => p.bj_id == Convert.ToInt32(PrimaryKey));
            var recruitJob = www6.selectQF<CommonLib.recruit_job>("bj_id", Convert.ToInt32(PrimaryKey));
            var exist2 = www6.selectQ<CommonLib.recruit>("b_id", recruitJob.bj_b_id, "b_display", 1);

            //if (!dao.recruit.Any(p => p.b_id == recruitJob.bj_b_id && p.b_display))
            if(!exist2.Any())
            {
                Response.Redirect(listPath, true);
            }

            // 지원서 검증
            //var recruit = dao.recruit.First(p => p.b_id == recruitJob.bj_b_id);
            var recruit = www6.selectQF<CommonLib.recruit>("b_id", recruitJob.bj_b_id);
            b_id = recruit.b_id;

            var exist3 = www6.selectQ<CommonLib.resume>("r_ap_id", cookie.id, "r_b_id", b_id);
            //if (!dao.resume.Any(p => p.r_ap_id == cookie.id && p.r_b_id == b_id))
            if(!exist3.Any())
            {
                Response.Redirect(listPath, true);
            }
        }
	}


	protected void LoadData()
    {
		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var resume = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == b_id);
            var resume = www6.selectQF<CommonLib.resume>("r_ap_id", cookie.id, "r_b_id", b_id);

            r_txt_aboutme_1.Value = resume.r_txt_aboutme_1;
            r_txt_career.Value = resume.r_txt_career;
        }
	}

	protected void save_Click(object sender, EventArgs e) {
		if (base.IsRefresh) {
			return;
		}
		Save();
	}

	protected void next_Click(object sender, EventArgs e)
    {
		Save();
        using (FrontDataContext dao = new FrontDataContext())
        {
            var cookie = RecruitSession.GetCookie(Context);
            //dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == b_id).r_regist_status = "step3";
            www6.selectQF<CommonLib.resume>("r_ap_id", cookie.id, "r_b_id", b_id).r_regist_status = "step3";
        }
		Response.Redirect("/recruit/apply/step3/" + PrimaryKey);
	}

	protected void prev_Click(object sender, EventArgs e)
    {
		Save();
        using (FrontDataContext dao = new FrontDataContext())
        {
            var cookie = RecruitSession.GetCookie(Context);
            //dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == b_id).r_regist_status = "step1";
            www6.selectQF<CommonLib.resume>("r_ap_id", cookie.id, "r_b_id", b_id).r_regist_status = "step3";
        }
		Response.Redirect("/recruit/apply/step1/" + PrimaryKey);
	}

	protected void Save() {


		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var resume = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == b_id);
            var resume = www6.selectQF<CommonLib.resume>("r_ap_id", cookie.id, "r_b_id", b_id);

            resume.r_txt_aboutme_1 = r_txt_aboutme_1.Value;
            resume.r_txt_career = r_txt_career.Value;

            //dao.SubmitChanges();
            www6.update(resume);
            Response.Write("<script>alert('저장되었습니다.');</script>");
        }
	}

}