﻿
var format = {
	number: { match: true, message: "숫자만 입력 가능합니다.", pattern: /[0-9\-]+/g },
	date: { match: false, message: "yyyy.mm.dd 형식으로 입력해 주세요.", pattern: /^[0-9]{4}\.[0-9]{2}\.[0-9]{2}$/ },
	yyyymm: { match: false, message: "yyyy.mm 형식으로 입력해 주세요", pattern: /^[0-9]{4}\.[0-9]{2}$/ },
	money: { match: false, message: "숫자와 ','만 입력 가능합니다.", pattern: /^\d+[\d|,]*\d$/ },
	decimal: { match: true, message: "숫자와 '.'만 입력 가능합니다.", pattern: /^[0-9]+\.?[0-9]*$/g },
	phone: { match: false, message: "휴대폰 번호 양식이 맞지 않습니다. 10자 이상의 숫자만 입력 가능합니다.", pattern: /(01[016789])[-](\d{4}|\d{3})[-]\d{4}$/g }
}


$(function () {
	$page.init();

	var uploader = attachUploader("btn_file_path");
	uploader._settings.data.fileDir = $("#upload_root").val();
	uploader._settings.data.fileType = "file";
	uploader._settings.data.limit = 5120;

	$page.setFileEvent();
	if ($(".r_disability_status:checked").val() == "비대상") {
		$("#r_disability_type").selectbox("disable");
		$("#r_disability_type").prop("disabled", true);
		$("#r_disability_level").selectbox("disable");
		$("#r_disability_level").prop("disabled", true);
	}
	//$(".r_disability_status").trigger("click");

});

var $page = {

	template: {
		edu: null,				// 학력
		lang: null,				// 어학능력
		certificate: null,		// 자격증
		work: null				// 경력
	},

	init: function () {

		// 각 탬플릿 초기화
		$.each($(".template"), function () {
			var id = $(this).attr("data-id");
			var item = $(this).clone();
			item.attr("data-id", "");
			item.removeClass("template").addClass("item");
			$page.template[id] = item;
			$(this).remove();
		});

		// 어학능력은 tr이 두개
		{
			var item = $($(".lang_template").html());
			item.addClass("item");
			item.attr("data-id", "0");
			$page.template['lang'] = item;
			$(".lang_template").html("");
			$(".lang_template").removeClass("lang_template");
		}
		
		// 더하기 클릭
		$.each($(".btnAdd"), function () {
			$(this).click(function () {
				var id = $(this).attr("data-id");
				$page.add(id);
				return false;
			});
		});

		//$page.loadEdu();

		// 데이터가 모두 로드된 후 바인딩함
		$page.load(function () {
			$page.setFormat();
			$page.setEvent();
			$page.dataBind();	
		});



	},

	setFormat: function () {
		
		// validation
		$("[data-format=number]").formatValidate(format.number);
		$("[data-format=date]").formatValidate(format.date);
		//$("[data-format=money]").formatValidate(format.money);
		$("[data-format=decimal]").formatValidate(format.decimal);
		$("[data-format=yyyymm]").formatValidate(format.yyyymm);
		$("[data-format=phone]").formatValidate(format.phone);

		// 날짜 포멧
		$("[data-format=yyyymm]").dateFormat("yyyy.mm");
		$("[data-format=date]").dateFormat("yyyy.mm.dd");

		$("[data-format=phone]").mobileFormat();

		// 금액 포멧
		//$("[data-format=money]").currencyFormat();

	},

	setEvent: function () {

		// 성별 클릭 여성의 경우 군별 해당없음으로
		$(".r_gender").click(function () {
			if ($(this).val() == "여" && $("#r_military_status").val() == "") {
				$("#r_military_status").selectbox("change", "해당없음", "해당없음(여성)");
			} else if ($(this).val() == "남" && $("#r_military_status").val() == "") {
				$("#r_military_status").selectbox("change", "", "선택하세요");
			}
			$("#r_gender").val($(this).val());
		});

		// 보훈여부
		$(".r_veterans_is").click(function () {
			if ($(this).val() == 0) $("#r_veterans_no").val("");
			$("#r_veterans_no").prop("disabled", $(this).val() == 0);
		});

		// 장애여부
		$(".r_disability_status").click(function () {
			if ($(this).val() == "비대상") {
				$("#r_disability_type").selectbox("change", "", "해당없음");
				$("#r_disability_type").selectbox("disable");
				$("#r_disability_type").prop("disabled", true);

				$("#r_disability_level").selectbox("change", "", "해당없음");
				$("#r_disability_level").selectbox("disable");
				$("#r_disability_level").prop("disabled", true);
			} else {
				$("#r_disability_type").selectbox("enable");
				$("#r_disability_level").selectbox("enable");
				$("#r_disability_type").prop("disabled", false);
				$("#r_disability_level").prop("disabled", false);
			}
		});

		// 후원여부
		$(".r_is_sponsor").click(function () {
			if ($(this).val() == 0) {
				//$("#r_sponsor_remark").val("");
			}
			//$("#r_sponsor_remark").prop("disabled", $(this).val() == 0);
		});
		

		$("#r_sponsor_remark").textCount($("#sponsor_remark_count"));

	},

	addFormat: function (element) {

		// validation
		element.find("[data-format=number]").formatValidate(format.number);
		element.find("[data-format=date]").formatValidate(format.date);
		element.find("[data-format=money]").formatValidate(format.currency);
		element.find("[data-format=decimal]").formatValidate(format.decimal);
		element.find("[data-format=yyyymm]").formatValidate(format.yyyymm);


		// 날짜 포멧
		element.find("[data-format=yyyymm]").dateFormat("yyyy.mm", false);
		element.find("[data-format=date]").dateFormat("yyyy.mm.dd", false);


		// 근무기간은 yyyy.mm or '재직 중'
		element.find("[data-format=date_allow_string]").dateFormat("yyyy.mm", true);
		element.find("[data-role=rw_date_end]").focusout(function () {
			if ($(this).val() != "") {
				var val = $(this).val().split(" ").join("");
			
				if (!(/^[0-9]{4}\.[0-9]{2}$/.test(val) || val == "재직중")) {
					alert("근무기간은 'YYYY.MM ~YYYY.MM' 또는  'YYYY.MM ~재직 중'의 형태로 입력해 주세요");
					$(this).val("");
					$(this).focus();
					return false;
				}
			}
		});
		
		
		
	},

	addSelectBoxEvent: function (element) {
		element.find(".sbHolder").remove();
		
		element.find(".custom_sel").selectbox({

			onOpen: function (inst) {
			},

			onChange: function (val, inst) {

			}
		});
	},

	addTextareaEvent: function (element) {
		element.find(".lang_textarea").focusin(function () {
			if ($(this).val() == "해당 어학능력에 대해 자유롭게 기술하세요.") {
				$(this).val("");
			}
		}).focusout(function () {
			if ($(this).val() == "") {
				$(this).val("해당 어학능력에 대해 자유롭게 기술하세요.")
			}
		})

		element.find(".lang_textarea").textCount(element.find(".lang_textarea_count"));
		console.log(element.find(".lang_textarea_count"));

	},

	dataBind: function () {

		// 성별
		if ($("#r_gender").val()) {
			$(".r_gender[value=" + $("#r_gender").val() + "]").trigger("click");
		}

		//보훈
		if ($("#r_veterans_is").val()) {
			$(".r_veterans_is[value=" + $("#r_veterans_is").val() + "]").trigger("click");
		}


		// 장애사항
		if ($("#r_disability_status").val()) {
			$(".r_disability_status[value='" + $("#r_disability_status").val() + "']").trigger("click");
		}

		//컴패션 후원여부
		if ($("#r_is_sponsor").val()) {
			$(".r_is_sponsor[value=" + $("#r_is_sponsor").val() + "]").trigger("click");
		}
		
	},

	load: function (callback) {
		$.each($(".loaded-data"), function () {
			var id = $(this).attr("data-id");

			var data = $(this).val();

			if (data == "" || data == '[]') {
				// 데이터가 없어도 한번은 호출해서 빈 열 나오도록
				$page.add(id);

				// 학력은 최초 2개
				if (id == "edu") $page.add(id);

				return;
			}

			var array = $.parseJSON(data);
			$.each(array, function () {
				$page.add(id, this);
			});

			

		});


		if (callback) callback();

	},

	add: function (id, entity) {

		var item = $page.template[id].clone();
		item = $page.makeItem[id](item, entity);


		$page.addFormat(item);

		$(".fill-box[data-id=" + id + "]").append(item);

		// 삭제 버튼
		item.find("[data-role=remove]").click(function () {
			$page.remove($(this));
			return false;
		});

		if (id == "edu") {
			item.find(".sbHolder").remove();

			item.find(".custom_sel").selectbox({

				onOpen: function (inst) {
				},

				onChange: function (val, inst) {

					if (val == "고등학교") {
						$(this).parent().parent().parent().find(".input_type2[data-role='ru_major']").val(" ").prop("disabled", "disabled");
					} else {
						$(this).parent().parent().parent().find(".input_type2[data-role='ru_major']").val("").prop("disabled", "")
					}

				}
			});
		} else {
			$page.addSelectBoxEvent(item);
		}


		if (id == "lang") { $page.addTextareaEvent(item) };

		

	},
	
	makeItem: {
		edu: function (item, entity) {

			if (entity) {
				item.find("[data-role=ru_grade]").val(entity.ru_grade);
				item.find("[data-role=ru_name]").val(entity.ru_name);
				item.find("[data-role=ru_major]").val(entity.ru_major);
				item.find("[data-role=ru_date_begin]").val(entity.ru_date_begin);
				item.find("[data-role=ru_date_end]").val(entity.ru_date_end);
				item.find("[data-role=ru_location]").val(entity.ru_location);

				if (entity.ru_grade == "고등학교") {item.find("[data-role=ru_major]").prop("disabled", true);}
			}
			return item;
		},

		lang: function (item, entity) {
			if (entity) {
				item.find("[data-role=rle_name]").val(entity.rle_name);
				item.find("[data-role=rle_organization]").val(entity.rle_organization);
				item.find("[data-role=rle_point]").val(entity.rle_point);
				item.find("[data-role=rle_date]").val(entity.rle_date);
				item.find("[data-role=rle_comment]").val(entity.rle_comment);
			}
			return item;
		},

		certificate: function (item, entity) {
			if (entity) {
				item.find("[data-role=rc_name]").val(entity.rc_name);
				item.find("[data-role=rc_issue]").val(entity.rc_issue);
				item.find("[data-role=rc_no]").val(entity.rc_no);
				item.find("[data-role=rc_date]").val(entity.rc_date);
			}
			return item;
		}
		,

		work: function (item, entity) {
			if (entity) {
				item.find("[data-role=rw_name]").val(entity.rw_name);
				item.find("[data-role=rw_status]").val(entity.rw_status);
				item.find("[data-role=rw_date_begin]").val(entity.rw_date_begin);
				item.find("[data-role=rw_date_end]").val(entity.rw_date_end);
				item.find("[data-role=rw_role]").val(entity.rw_role);
				item.find("[data-role=rw_position]").val(entity.rw_position);
				item.find("[data-role=rw_work]").val(entity.rw_work);
			}
			return item;
		}
		
	},

	remove: function ($element) {

		if (confirm("선택하신 항목을 삭제하시겠습니까?")) {

			var $table = $element.closest(".fill-box");

			// 학력사항은 최소 1개 이상
			var id = $table.attr("data-id");
		
			if (id == "edu" && $(".fill-box[data-id=edu]").find(".item").length < 2) {
				alert("학력사항은 최소 1개이상 입력하셔야 합니다.");
				return false;
			}

			// 언어영역은 tr이 두개
			var rowCount = id == "lang" ? $table.find(".item").length / 2 : $table.find(".item").length;

			// 최소 1개의 입력란은 놔둬야 함
			if (rowCount < 2) {
				$.each($table.find("input[type=text], textarea, input[type=hidden]"), function () {
					$(this).val("");

					if ($(this).hasClass("lang_textarea")) {
						$(this).val("해당 어학능력에 대해 자유롭게 기술하세요.")
					}
				});

				// select box는 custom되어 있음
				$.each($table.find("select"), function () {
					var text = $(this).find("[value='']").text();
					$(this).selectbox("change", "", text)
				});

			
			} else {
				var removeElement = $element.closest(".item")


				// 어학능력은 tr이 두개
				if (id == "lang") {
					$(removeElement).next().remove();
				}
				$element.closest(".item").remove();

			
			}

		}
	},

	validate: function () {

		// 기본 인적
		if (!validateForm([
				{ id: "#r_name_en", msg: "이름(영문) 이름을 입력해 주세요" },
				{ id: "#r_familyname_en", msg: "이름(영문) 성을 입력해 주세요." },
				{ id: "#birth_year", msg: "생년월일을 입력해 주세요.", type: "numeric" },
				{ id: "#birth_month", msg: "생년월일을 입력해 주세요.", type: "numeric" },
				{ id: "#birth_day", msg: "생년월일을 입력해 주세요.", type: "numeric" }
		])) {
			return false;
		}

		// 생년월일 validate
		if (!$page.birthValidate($("#birth_year").val(), $("#birth_month").val(), $("#birth_day").val())) {
			alert("생년월일이 입력이 올바르지 않습니다.");
			$("#birth_year").focus();
			return false;
		}

		var birth_month = $("#birth_month").val();
		var birth_day = $("#birth_day").val();

		if ($(".r_gender:checked").length < 1) {
			alert("성별을 선택하세요");
			$(".r_gender").eq(0).focus();
			return false;
		}
		//$("#r_gender").val($(".r_gender").val());

		if (!validateForm([
				{ id: "#r_addr", msg: "현 거주지 주소를 입력해 주세요." },
				{ id: "#phone", msg: "휴대폰번호를 입력해 주세요.", type: "phone" },
				{ id: "#r_salary", msg: "현재연봉을 입력해 주세요."},
				{ id: "#r_hope_salary", msg: "희망연봉을 입력해 주세요." }
		])) {
			return false;
		}

		// 병역
		var military_status = $("#r_military_status").val();
		if (!validateForm([
				{ id: "#r_military_status", msg: "병역구분을 선택해 주세요" }
		])) {
			return false;
		}


		// 보훈대상
		if ($(".r_veterans_is:checked").length < 1) {
			alert("보훈여부를 선택하세요");
			$(".r_veterans_is").eq(0).focus();
			return false;
		}


		$("#r_veterans_is").val($(".r_veterans_is:checked").val());
		if ($("#r_veterans_is").val() == "1") {
			if (!validateForm([
					{ id: "#r_veterans_no", msg: "(보훈여부-대상) 보훈번호를 입력해 주세요." }
			])) {
				return false;
			}
		}


		// 장애
		$("#r_disability_status").val($(".r_disability_status:checked").val());
		if ($(".r_disability_status:checked").length < 1) {
			alert("장애여부를 선택하세요");
			$(".r_disability_status").eq(0).focus();
			return false;
		}
		if ($("#r_disability_status").val() == "대상") {

			if (!validateForm([
					{ id: "#r_disability_type", msg: "장애유형을 선택해 주세요" },
					{ id: "#r_disability_level", msg: "장애등급을 선택해 주세요" }
			])) {
				return false;
			}
		}


		// 출석교회
		if (!validateForm([
				{ id: "#r_church_name", msg: "출석교회명을 입력해 주세요." },
				{ id: "#r_church_team", msg: "소속단체명을 입력해 주세요." }
		])) {
			return false;
		}


		// 후원여부
		$("#r_is_sponsor").val($(".r_is_sponsor:checked").val());
		if ($(".r_is_sponsor:checked").length < 1) {
			alert("컴패션 후원여부를 선택하세요");
			$(".r_is_sponsor").eq(0).focus();
			return false;
		}
		if ($("#r_sponsor_remark").val().length > 2000) {
			alert("컴패션 후원과 관련된 특이사항은 2000자 이하로 입력해주세요.");
			$("#r_sponsor_remark").focus();
			return false;
		}

		/*
		$("#r_is_sponsor").val($(".r_is_sponsor:checked").val());
		if ($("#r_is_sponsor").val() == "1") {
			if (!validateForm([
					{ id: "#r_sponsor_remark", msg: "특이사항을 입력해 주세요." }
			])) {
				return false;
			}
		}
		*/

		return true;
	},

	birthValidate: function (yyyy, mm, dd) {
		mm = parseInt(mm);
		dd = parseInt(dd);

		var today = new Date();
		var year = today.getFullYear();

		if (yyyy > year) return false;

		if (year - yyyy > 100) return false;

		if (mm < 1 || mm > 12) return false;

		if (dd > 31) return false;

		var d = new Date(yyyy, mm - 1, dd);

		if (yyyy != d.getFullYear() || mm - 1 != d.getMonth() || dd != d.getDate()) return false;

		return true;
	},

	loopValidate: function (id) {

		var result = true;
		$.each($(".fill-box"), function () {
			var id = $(this).attr("data-id");
			$.each($(this).find(".item"), function () {

				var totalCount = $(this).find("input[type=text], textarea, select").length;
				var empty = $(this).find("input[type=text], textarea, select").filter(function () { return $(this).val() == "" || $(this).val() == "선택해 주세요"; });
				var emptyCount = empty.length;
				if (id == "lang") {
					console.log(emptyCount, totalCount);
				}
				if (emptyCount != totalCount) {
					$.each(empty, function (i) {

						var required = $(this)[0].hasAttribute("data-required");
						if (required) {
							var title = $(this).attr("title");
							var msg = "";
							var grade = "";

							if (title == "학교명") {
								msg = "학교명을 입력해 주세요.";
							} else if (title == "전공") {
								msg = "전공을 입력해 주세요.";
							} else if (title == "입학연월") {
								msg = "입학연월은 YYYY.MM의 형태로 입력해 주세요.";
							} else if (title == "졸업연월") {
								msg = "졸업연월은 YYYY.MM의 형태로 입력해 주세요.";
							} else if (title == "소재지") {
								msg = "소재지를 입력해 주세요.";
							}
							if (msg != "") {
								alert(msg);
							}

							result = false;
							this.focus();
							return false;
						}

					});
				}


				if (!result) return false;

			});

			if (!result) return false;
		});


		// 어학능력 검사
		$(".lang_textarea").each(function () {
			if ($(this).val() != "") {
				if ($(this).parentsUntil("tr").parent().prev().find("input[data-role=rle_name]").val() == "") {
					alert("외국어명을 입력해 주세요.");
					$(this).parentsUntil("tr").parent().prev().find("input[data-role=rle_name]").focus();
					result = false;
					return false;
				}
			}
		});

		return result;
	},

	// 임시 저장 - 자료형만 체크 
	save: function () {

		// 어학능력 2000자 이하
		var lang_ok = true;
		$.each($("textarea[data-role=rle_comment]"), function () {
			if ($(this).val().length > 2000) {
				alert("어학능력에 대한 기술은 2000자 이하로 입력해주세요.");
				$(this).focus();
				lang_ok = false;
				return false;
			}
		});

		if (!lang_ok) return false;

		// 어학능력에 '해당 어학능력에 대해 자유롭게 기술하세요.' 지우기
		$.each($("textarea[data-role=rle_comment]"), function () {
			if ($(this).val() == "해당 어학능력에 대해 자유롭게 기술하세요.") $(this).val("");
		});

		// 기본정보
		if (!$page.validate()) return false;


		// 학력사항 등 반복 정보
		if (!$page.loopValidate()) return false;

		// 학력사항은 최소 1개 이상
		if ($.grep($('[data-role="ru_name"]'), function (e) { return $(e).val() != ""; }).length < 1) {
			alert("학력 사항은 최소 1개 이상 입력하셔야 다음 단계 진행이 가능합니다.");
			$('[data-role="ru_grade"]').eq(0).focus();
			return false;
		}


		// 최종학력
		$("#last_edu").val($page.getLastEdu());


		// 정보수집
		$page.collect();

		return true;
	},

	collect: function () {
		
		$.each($(".fill-box"), function () {
			var id = $(this).attr('data-id');
			var data = [];


			$.each($(this).find(".item"), function () {

				var emptyFields = $(this).find("input[type=text], select").filter(function () {
					var required = $(this)[0].hasAttribute("data-required");
					return required && $(this).val() == "";
				}).length;

				// 값의 입력이 있는 경우만
				var enteredFields = $(this).find("input[type=text], textarea, select").filter(function () {
					return $(this).val() != "";
				}).length;

				// 필수값 모두 입력 시
				if (emptyFields == 0 && enteredFields > 0) {
					var entity = {};
					$.each($(this).find("input[type=text], textarea, input[type=hidden], select"), function () {
						var name = $(this).attr("data-role");
						var value = $(this).val();

						entity[name] = value;
					});
					data.push(entity);
				}
			});

			// 어학능력의 경우 tr이 두개(.item)이 두개라 data가 두번들어감	
			if (data.length > 1 && id == "lang") {
				var tempData = data;
				var saveData = {};
				data = [];
				for (var i = 0 ; i < tempData.length ; i += 2) {
					var saveData = {};
					$.extend(true, saveData, tempData[i], tempData[i + 1]);
					data.push(saveData);
				}
					/*
					var saveData = {};
					$.each(data, function () {
						$.extend(true, saveData, this);
					});
					data = [];
					data.push(saveData);
					*/
				}

			$(".save-data[data-id=" + id + "]").val(JSON.stringify(data));
		});
		
	},

	// 최종학력  
	getLastEdu: function () {
		var array = [];
		$("[data-id='edu']").find(".item").each(function () {
			var date_end = $(this).find('[data-role="ru_date_end"]');
			if (date_end.val() != "") {
				var obj = {
					ru_date_end: date_end.val().split(".").join(""),
					ru_grade: $(this).find('[data-role="ru_grade"]').val()
				};
				array.push(obj);
			}

		});

		array.sort(function (a, b) { return (a.ru_date_end > b.ru_date_end) ? -1 : ((b.ru_date_end > a.ru_date_end) ? 1 : 0); });

		return array[0].ru_grade;
	},

	prev: function () {
		return confirm("이전 단계로 이동하시겠습니까?");
	},
	tempSave: function () {
		if (confirm("임시저장 하시겠습니까?")) {
			return $page.save();
		} else {
			return false;
		}
	},


	setFileEvent: function () {

		if ($("#lb_file_path").val() != "") {
			$("#btn_remove").show();
		}

		$("#btn_remove").click(function () {
			$("#file_path").val("");
			$("#lb_file_path").val("");

			return false;
		});
	},

}






var attachUploader = function (button) {
	return new AjaxUpload(button, {
		action: '/common/handler/upload',
		responseType: 'json',
		onChange: function (file) {
			if (/[`~!@#$%^&*|\\\'\";:\/?]/gi.test(file)) {
				alert("파일명에 특수문자를 포함할 수 없습니다.");
				return false;
			}
		},
		onSubmit: function (file, ext) {
			
			this.disable();
		},
		onComplete: function (file, response) {

			this.enable();

			if (response.success) {
				//$("#file_path").val(response.name);
				$("#file_path").val(response.name.replace(/^.*[\\\/]/, ''));
				$("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
				//	$(".temp_file_size").val(response.size);
				$("#btn_remove").show();

			} else
				alert(response.msg);
		}
	});
}