﻿$(function () {
	$("#r_txt_aboutme_2").textCount($("#aboutme_2_count"));
	//$("#r_txt_aboutme_3").textCount($("#aboutme_3_count"), { limit: 5000 });



	var uploader = attachUploader("btn_file_path");
	uploader._settings.data.fileDir = $("#upload_root").val();
	uploader._settings.data.fileType = "file";
	uploader._settings.data.limit = 5120;

	$page.setFileEvent();


	// 파일창에 글쓰기 없기
	$("#lb_file_path").focus(function () {
		$(this).blur();
	});
});


var $page = {
	save: function () {


		if (!validateForm([
				{ id: "#r_txt_aboutme_2", msg: "신앙간증문 내용을 입력하세요" },
				{ id: "#r_txt_aboutme_3", msg: "추천서 내용을 입력하세요" }
		])) {
			return false;
		}

		if ($("#r_txt_aboutme_2").val().length > 5000) {
			alert("신앙간증문은 5000자 이하로 입력해주세요.");
			$("#r_txt_aboutme_2").focus();
			return false;
		}

		return true;

	},

	setFileEvent: function () {

		if ($("#lb_file_path").val() != "") {
			$("#btn_remove").show();
		}

		$("#btn_remove").click(function () {
			$("#file_path").val("");
			$("#lb_file_path").val("");

			return false;
		});
	},

	prev: function () {
		return confirm("이전 단계로 이동하시겠습니까?");
	},
	tempSave: function () {
		if ($page.save()) {
			return confirm("임시저장 하시겠습니까?");
		} else {
			return false;
		}
	}
}



var attachUploader = function (button) {
	return new AjaxUpload(button, {
		action: '/common/handler/upload',
		responseType: 'json',
		onChange: function (file) {
			if (/[`~!@#$%^&*|\\\'\";:\/?]/gi.test(file)) {
				alert("파일명에 특수문자를 포함할 수 없습니다.");
				return false;
			}
		},
		onSubmit: function (file, ext) {
			this.disable();
		},
		onComplete: function (file, response) {

			this.enable();

			if (response.success) {
				//$("#file_path").val(response.name);
				$("#file_path").val(response.name.replace(/^.*[\\\/]/, ''));
				$("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
				//	$(".temp_file_size").val(response.size);

				$("#btn_remove").show();

			} else
				alert(response.msg);
		}
	});
}