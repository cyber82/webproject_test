﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using System.Data.Linq;
using CommonLib;

public partial class recruit_apply_step1 : FrontBasePage {
	const string listPath = "/recruit";

	public override bool RequireRecruitLogin{
		get{return true;}
	}
	

	public override bool RequireSSL {
		get {return true;}
	}

	List<recruit_jobcode> Jobcode{
		get{
			if (this.ViewState["recruit_jobcode"] == null) {

                using (AdminDataContext dao = new AdminDataContext())
                {
                    //this.ViewState["recruit_jobcode"] = dao.recruit_jobcode.Where(p => p.rj_display == true).ToJson();
                    this.ViewState["recruit_jobcode"] = www6.selectQ<recruit_jobcode>("rj_display", 1).ToJson();
                }
			}
			return this.ViewState["recruit_jobcode"].ToString().ToObject<List<recruit_jobcode>>();
		}
		set{
			this.ViewState["recruit_jobcode"] = value.ToJson();
		}
	}
	
	
	protected int b_id {
		set {
			this.ViewState.Add("b_id", value);
		}
		get {
			if (this.ViewState["b_id"] == null)
				return 0;
			return Convert.ToInt32(this.ViewState["b_id"]);
		}
	}




	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		
		// check param
		var requests = Request.GetFriendlyUrlSegments();
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}

		base.PrimaryKey = requests[0];


		// 해당 채용으로 로그인 했는지 확인
		if (RecruitSession.GetCookie(Context).bj_id.ToString() != PrimaryKey.ToString()) {
			Response.Redirect(listPath, true);
		}

        using (FrontDataContext dao = new FrontDataContext())
        {
            // recruit_job검증
            var exist = www6.selectQ<recruit_job>("bj_id", Convert.ToInt32(PrimaryKey));
            //if (!dao.recruit_job.Any(p => p.bj_id == Convert.ToInt32(PrimaryKey)))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }
            //var recruitJob = dao.recruit_job.First(p => p.bj_id == Convert.ToInt32(PrimaryKey));
            var recruitJob = www6.selectQF<recruit_job>("bj_id", Convert.ToInt32(PrimaryKey));

            // recruit 검증
            var exist2 = www6.selectQ<recruit>("b_id", recruitJob.bj_b_id, "b_display", 1);
            //if (!dao.recruit.Any(p => p.b_id == recruitJob.bj_b_id && p.b_display))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var recruit = dao.recruit.First(p => p.b_id == recruitJob.bj_b_id);
            var recruit = www6.selectQF<recruit>("b_id", recruitJob.bj_b_id);

            b_id = recruit.b_id;

            if (recruit.b_date_begin > DateTime.Now || recruit.b_date_end < DateTime.Now)
            {
                Response.Write("<script>alert('지원기간이 아닙니다.'); location.href='" + listPath + "'; </script>");
            }


            // 지원분야
            //depth1_name.Text = dao.recruit_jobcode.First(p => p.rj_depth1 == recruitJob.bj_rj_depth1 && p.rj_depth2 == "").rj_name;
            depth1_name.Text = www6.selectQF<recruit_jobcode>("rj_depth1", recruitJob.bj_rj_depth1, "rj_depth2", "").rj_name;
            //depth2_name.Text = dao.recruit_jobcode.First(p => p.rj_depth1 == recruitJob.bj_rj_depth1 && p.rj_depth2 == recruitJob.bj_rj_depth2).rj_name;
            depth2_name.Text = www6.selectQF<recruit_jobcode>("rj_depth1", recruitJob.bj_rj_depth1, "rj_depth2", recruitJob.bj_rj_depth2).rj_name;

            //b_title.Text = dao.recruit.First(p => p.b_id == recruitJob.bj_b_id).b_title;
            b_title.Text = www6.selectQF<recruit>("b_id", recruitJob.bj_b_id).b_title;
        }


		loadData();
	}

	protected void loadData()
    {
		// 셀렉트박스 초기화
		SetDropDownList();
        
		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var applicant = dao.applicant.First(p => p.ap_id == cookie.id);
            var applicant = www6.selectQF<applicant>("ap_id", cookie.id);

            r_name.Text = applicant.ap_name;
            r_email.Text = applicant.ap_email;


            if (applicant.ap_phone != null)
            {
                /*
				var phone = applicant.ap_phone.Split('-');
				phone1.SelectedValue = phone[0];
				phone2.Value = phone[1];
				phone3.Value = phone[2];
				*/
                phone.Value = applicant.ap_phone;
            }

            var exist = www6.selectQ<resume>("r_ap_id", cookie.id, "r_b_id", b_id);
            //if (dao.resume.Any(p => p.r_ap_id == cookie.id && p.r_b_id == b_id))
            if(exist.Any())
            {
                //var resume = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == b_id);
                var resume = www6.selectQF<resume>("r_ap_id", cookie.id, "r_b_id", b_id);

                upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.recruit, resume.r_id.ToString());

                // 기본 인적사항
                r_gender.Value = resume.r_gender;

                if (resume.r_birth != null)
                {
                    var r_birth = resume.r_birth.Split('-');
                    birth_year.Value = r_birth[0];
                    birth_month.Value = r_birth[1];
                    birth_day.Value = r_birth[2];
                }


                r_name_en.Value = resume.r_name_en;
                r_familyname_en.Value = resume.r_familyname_en;

                r_addr.Value = resume.r_addr;


                if (resume.r_salary != null) r_salary.Value = resume.r_salary.ToString();
                if (resume.r_hope_salary != null) r_hope_salary.Value = resume.r_hope_salary.ToString();


                // 병역사항
                r_military_status.SelectedValue = resume.r_military_status;


                // 보훈사항
                if (resume.r_veterans_is != null)
                {
                    r_veterans_is.Value = (bool)resume.r_veterans_is ? "1" : "0";
                }
                r_veterans_no.Value = resume.r_veterans_no;


                // 장애사항
                r_disability_status.Value = resume.r_disability_status;
                r_disability_type.SelectedValue = resume.r_disability_type;
                r_disability_level.SelectedValue = resume.r_disability_level;

                // 출석교회 정보
                r_church_name.Value = resume.r_church_name;
                r_church_team.Value = resume.r_church_team;

                if (resume.r_is_sponsor != null)
                {
                    r_is_sponsor.Value = (bool)resume.r_is_sponsor ? "1" : "0";
                }
                r_sponsor_remark.Value = resume.r_sponsor_remark;

                // 최종학력
                last_edu.Value = resume.r_education;

                //포트폴리오
                r_portfolio.Value = resume.r_portfolio;
                lb_file_path.Value = resume.r_file;



                // 학력사항
                //data_edu.Value = dao.resume_edu.Where(p => p.ru_r_id == resume.r_id).ToJson();
                data_edu.Value = www6.selectQ<resume_edu>("ru_r_id", resume.r_id).ToJson();


                // 어학능력
                //data_lang.Value = dao.resume_lang_exam.Where(p => p.rle_r_id == resume.r_id).ToJson();
                data_lang.Value = www6.selectQ<resume_lang_exam>("rle_r_id", resume.r_id).ToJson();

                // 자격증
                //data_certificate.Value = dao.resume_certificate.Where(p => p.rc_r_id == resume.r_id).ToJson();
                data_certificate.Value = www6.selectQ<resume_certificate>("rc_r_id", resume.r_id).ToJson();

                // 경력사항
                //data_work.Value = dao.resume_work.Where(p => p.rw_r_id == resume.r_id).ToJson();
                data_work.Value = www6.selectQ<resume_work>("rw_r_id", resume.r_id).ToJson();
            }
            else
            {
                var resume = new resume()
                {
                    r_ap_id = cookie.id,
                    r_b_id = b_id,
                    r_name = applicant.ap_name,
                    r_email = applicant.ap_email,
                    r_regist_status = "step1",
                    r_ip = Request.ServerVariables["REMOTE_ADDR"],
                    r_regdate = DateTime.Now,
                    r_moddate = DateTime.Now,
                    r_expire_mail = false
                };

                //dao.resume.InsertOnSubmit(resume);
                www6.insert(resume);
                //dao.SubmitChanges();


                upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.recruit, resume.r_id.ToString());
            }


        }
	}


	void SetDropDownList() {


		// 병역구분
		foreach (var a in Recruit.MilitaryStatus()) {
			r_military_status.Items.Add(new ListItem(a.Value, a.Key));
		}
		
		// 장애유형
		foreach (var a in Recruit.DisabilityType()) {
			r_disability_type.Items.Add(new ListItem(a.Value, a.Key));
		}
		// 장애등급
		foreach (var a in Recruit.DisabilityLevel()) {
			r_disability_level.Items.Add(new ListItem(a.Value, a.Key));
		}
	}



	// 임시저장
	protected void save_Click(object sender, EventArgs e) {
		if (base.IsRefresh) {
			return;
		}
		Save();
	}

	// 저장 후 next
	protected void next_Click(object sender, EventArgs e)
    {
		Save();

        using (FrontDataContext dao = new FrontDataContext())
        {
            var cookie = RecruitSession.GetCookie(Context);
            //dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == b_id).r_regist_status = "step2";
            www6.selectQF<resume>("r_ap_id", cookie.id, "r_b_id", b_id).r_regist_status = "step2";
        }
		Response.Redirect("/recruit/apply/step2/"+PrimaryKey);
	}

	protected void Save() {

		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            var r_id = -1;
            //var applicant = dao.applicant.First(p => p.ap_id == cookie.id);
            var applicant = www6.selectQF<applicant>("ap_id", cookie.id);
            //var recruitJob = dao.recruit_job.First(p => p.bj_id == Convert.ToInt32(PrimaryKey));
            var recruitJob = www6.selectQF<recruit_job>("bj_id", Convert.ToInt32(PrimaryKey));


            birth_month.Value = birth_month.Value.Length < 2 ? "0" + birth_month.Value : birth_month.Value;
            birth_day.Value = birth_day.Value.Length < 2 ? "0" + birth_day.Value : birth_day.Value;


            //var resume = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == b_id);
            var resume = www6.selectQF<resume>("r_ap_id", cookie.id, "r_b_id", b_id);
            // 기본 인적사항
            resume.r_name = applicant.ap_name;
            resume.r_email = applicant.ap_email;
            resume.r_name_en = r_name_en.Value;
            resume.r_familyname_en = r_familyname_en.Value;
            resume.r_gender = r_gender.Value;
            resume.r_birth = birth_year.Value + "-" + birth_month.Value + "-" + birth_day.Value;
            resume.r_addr = r_addr.Value;
            resume.r_phone = phone.Value;
            resume.r_salary = r_salary.Value;
            resume.r_hope_salary = r_hope_salary.Value;

            // 병역사항
            resume.r_military_status = r_military_status.SelectedValue;

            // 보훈대상
            resume.r_veterans_is = r_veterans_is.Value == "1";
            if (r_veterans_is.Value == "1")
            {
                resume.r_veterans_no = r_veterans_no.Value;
            }
            else
            {
                resume.r_veterans_no = "";
            }

            // 장애사항
            resume.r_disability_status = r_disability_status.Value;
            resume.r_disability_type = r_disability_type.SelectedValue;
            resume.r_disability_level = r_disability_level.SelectedValue;

            // 출석교회
            resume.r_church_name = r_church_name.Value;
            resume.r_church_team = r_church_team.Value;
            resume.r_is_sponsor = r_is_sponsor.Value == "1";
            resume.r_sponsor_remark = r_sponsor_remark.Value;


            // 최종학력(가장 늦은 졸업년도)
            resume.r_education = last_edu.Value;

            // 직무코드
            resume.r_rj_depth1 = recruitJob.bj_rj_depth1;
            resume.r_rj_depth2 = recruitJob.bj_rj_depth2;

            // 포트폴리오
            resume.r_portfolio = r_portfolio.Value;
            resume.r_file = lb_file_path.Value;


            if (resume.r_regist_status != "done")
            {
                resume.r_regist_status = "step1";
            }
            resume.r_moddate = DateTime.Now;

            r_id = resume.r_id;



            // 학력사항
            //dao.resume_edu.DeleteAllOnSubmit(dao.resume_edu.Where(p => p.ru_r_id == r_id));
            var resumeList = www6.selectQ<resume_edu>("ru_r_id", r_id);
            www6.delete(resumeList);

            if (data_edu.Value != "")
            {   
                foreach (var add in data_edu.Value.ToObject<List<resume_edu>>())
                {
                    add.ru_r_id = r_id;
                    //dao.resume_edu.InsertOnSubmit(add);
                    www6.insert(add);
                }
            }


            // 어학능력
            //dao.resume_lang_exam.DeleteAllOnSubmit(dao.resume_lang_exam.Where(p => p.rle_r_id == r_id));
            var resume_lang_examList = www6.selectQ<resume_lang_exam>("rle_r_id", r_id);
            www6.delete(resume_lang_examList);

            if (data_lang.Value != "")
            {
                foreach (var add in data_lang.Value.ToObject<List<resume_lang_exam>>())
                {
                    add.rle_r_id = r_id;
                    //dao.resume_lang_exam.InsertOnSubmit(add);
                    www6.insert(add);
                }
            }


            // 자격 및 면허사항
            //dao.resume_certificate.DeleteAllOnSubmit(dao.resume_certificate.Where(p => p.rc_r_id == r_id));
            var resume_certificateList = www6.selectQ<resume_certificate>("rc_r_id", r_id);
            www6.delete(resume_certificateList);

            if (data_certificate.Value != "")
            {
                foreach (var add in data_certificate.Value.ToObject<List<resume_certificate>>())
                {
                    add.rc_r_id = r_id;
                    //dao.resume_certificate.InsertOnSubmit(add);
                    www6.insert(add);
                }
            }


            // 경력사항
            //dao.resume_work.DeleteAllOnSubmit(dao.resume_work.Where(p => p.rw_r_id == r_id));
            var resume_workList = www6.selectQ<resume_work>("rw_r_id", r_id);
            www6.delete(resume_workList);

            if (data_work.Value != "")
            {
                foreach (var add in data_work.Value.ToObject<List<resume_work>>())
                {
                    add.rw_r_id = r_id;
                    //dao.resume_work.InsertOnSubmit(add);
                    www6.insert(add);
                }
            }
            //dao.SubmitChanges();
            www6.update(resume);
            Response.Write("<script>alert('저장되었습니다.');</script>");

        }

	}
}