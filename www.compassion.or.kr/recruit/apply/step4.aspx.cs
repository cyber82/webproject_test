﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using System.Data.Linq;
using System.Configuration;
using CommonLib;

public partial class recruit_apply_step4 : FrontBasePage {

	Uploader.FileGroup file_group = Uploader.FileGroup.recruit;

	public override bool RequireRecruitLogin{
		get{return true;}
	}

	
	
	public override bool RequireSSL {
		get {return true;}
	}
	protected int b_id {
		set {
			this.ViewState.Add("b_id", value);
		}
		get {
			if (this.ViewState["b_id"] == null)
				return 0;
			return Convert.ToInt32(this.ViewState["b_id"]);
		}
	}

	resume ResumeData	{
		get {
			if (this.ViewState["resume"] == null)
				this.ViewState["resume"] = new resume().ToJson();
			return this.ViewState["resume"].ToString().ToObject<resume>();
		}
		set	{
			this.ViewState["resume"] = value.ToJson();
		}
	}


	const string listPath = "/recruit";


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		// check param
		var requests = Request.GetFriendlyUrlSegments();

		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);


		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}

		base.PrimaryKey = requests[0];

		// 해당 채용으로 로그인 했는지 확인
		if (RecruitSession.GetCookie(Context).bj_id.ToString() != PrimaryKey.ToString()) {
			Response.Redirect(listPath, true);
		}

		// 입사지원
		apply.Visible = true;


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var recruitJob = dao.recruit_job.First(p => p.bj_id == Convert.ToInt32(PrimaryKey));
            var recruitJob = www6.selectQF<CommonLib.recruit_job>("bj_id", Convert.ToInt32(PrimaryKey));
            // 지원분야
            //depth1_name.Text = dao.recruit_jobcode.First(p => p.rj_depth1 == recruitJob.bj_rj_depth1 && p.rj_depth2 == "").rj_name;
            depth1_name.Text = www6.selectQF<CommonLib.recruit_jobcode>("rj_depth1", recruitJob.bj_rj_depth1, "rj_depth2", "").rj_name;

            //depth2_name.Text = dao.recruit_jobcode.First(p => p.rj_depth1 == recruitJob.bj_rj_depth1 && p.rj_depth2 == recruitJob.bj_rj_depth2).rj_name;
            depth2_name.Text = www6.selectQF<CommonLib.recruit_jobcode>("rj_depth1", recruitJob.bj_rj_depth1, "rj_depth2", recruitJob.bj_rj_depth2).rj_name;

            //b_title.Text = dao.recruit.First(p => p.b_id == recruitJob.bj_b_id).b_title;
            b_title.Text = www6.selectQF<CommonLib.recruit>("b_id", recruitJob.bj_b_id).b_title;


            //var recruit = dao.recruit.First(p => p.b_id == recruitJob.bj_b_id);
            var recruit = www6.selectQF<CommonLib.recruit>("b_id", recruitJob.bj_b_id);
            b_id = recruit.b_id;
        }

		loadData();
	}


	protected void ValidateParam()
    {
		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {

            // recruit_job검증
            var exist = www6.selectQ<CommonLib.recruit_job>("bj_id", Convert.ToInt32(PrimaryKey));
            //if (!dao.recruit_job.Any(p => p.bj_id == Convert.ToInt32(PrimaryKey)))
            if (!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var recruitJob = dao.recruit_job.First(p => p.bj_id == Convert.ToInt32(PrimaryKey));
            var recruitJob = www6.selectQF<CommonLib.recruit_job>("bj_id", Convert.ToInt32(PrimaryKey));

            // recruit 검증
            var exist2 = www6.selectQ<CommonLib.recruit>("b_id", recruitJob.bj_b_id, "b_display", 1);
            //if (!dao.recruit.Any(p => p.b_id == recruitJob.bj_b_id && p.b_display))
            if (!exist2.Any())
            {
                Response.Redirect(listPath, true);
            }

            // 지원서 검증
            var exist3 = www6.selectQ<CommonLib.resume>("r_id", cookie.id);
            //if (!dao.resume.Any(p => p.r_id == cookie.id))
            if (!exist3.Any())
            {
                Response.Redirect(listPath, true);
            }
        }
	}

	protected void loadData()
    {
		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //ResumeData = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == b_id);
            ResumeData = www6.selectQF<CommonLib.resume>("r_ap_id", cookie.id, "r_b_id", b_id);

            // 기본 인적사항
            r_name.Text = ResumeData.r_name;
            r_name2.Text = ResumeData.r_name;
            r_name_en.Text = ResumeData.r_name_en + " " + ResumeData.r_familyname_en;
            r_gender.Text = ResumeData.r_gender + "자";


            var birthday = Convert.ToDateTime(ResumeData.r_birth);
            r_birth.Text = birthday.ToString("yyyy년 M월 d일");
            age.Text = (DateTime.Now.Year - int.Parse(ResumeData.r_birth.Substring(0, 4))).ToString();



            r_email.Text = ResumeData.r_email;
            r_addr.Text = ResumeData.r_addr;


            r_phone.Text = ResumeData.r_phone;


            r_salary.Text = ResumeData.r_salary;
            r_hope_salary.Text = ResumeData.r_hope_salary;


            // 병역사항
            r_military_status.Text = ResumeData.r_military_status;


            // 보훈사항
            if (ResumeData.r_veterans_is != null)
            {
                r_veterans_is.Text = (bool)ResumeData.r_veterans_is ? "대상" : "비대상";
            }
            r_veterans_no.Text = ResumeData.r_veterans_no;


            // 장애사항
            r_disability_status.Text = ResumeData.r_disability_status;
            r_disability_type.Text = ResumeData.r_disability_type;
            r_disability_level.Text = ResumeData.r_disability_level;

            // 출석교회 정보
            r_church_name.Text = ResumeData.r_church_name;
            r_church_team.Text = ResumeData.r_church_team;

            if (ResumeData.r_is_sponsor != null)
            {
                r_is_sponsor.Text = (bool)ResumeData.r_is_sponsor ? "예" : "아니요";
            }
            r_sponsor_remark.Text = ResumeData.r_sponsor_remark;


            // 포트폴리오
            r_portfolio.Text = ResumeData.r_portfolio == "" ? "등록된 데이터가 존재하지 않습니다. " : ResumeData.r_portfolio;
            //r_file.Text = ResumeData.r_file;
            r_file.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, ResumeData.r_id.ToString()), ResumeData.r_file, ResumeData.r_id, ResumeData.r_file);


            // 학력사항
            //repeater_edu.DataSource = dao.resume_edu.Where(p => p.ru_r_id == ResumeData.r_id).ToList();
            repeater_edu.DataSource = www6.selectQ<CommonLib.resume_edu>("ru_r_id", ResumeData.r_id);
            repeater_edu.DataBind();


            // 어학능력
            //repeater_lang.DataSource = dao.resume_lang_exam.Where(p => p.rle_r_id == ResumeData.r_id).ToList();
            repeater_lang.DataSource = www6.selectQ<CommonLib.resume_lang_exam>("rle_r_id", ResumeData.r_id);
            repeater_lang.DataBind();

            // 자격증
            //repeater_certificate.DataSource = dao.resume_certificate.Where(p => p.rc_r_id == ResumeData.r_id).ToList();
            repeater_certificate.DataSource = www6.selectQ<CommonLib.resume_certificate>("rc_r_id", ResumeData.r_id);
            repeater_certificate.DataBind();

            // 경력사항
            //repeater_work.DataSource = dao.resume_work.Where(p => p.rw_r_id == ResumeData.r_id).ToList();
            repeater_work.DataSource = www6.selectQ<CommonLib.resume_work>("rw_r_id", ResumeData.r_id);
            repeater_work.DataBind();

            // 경력참고인
            //repeater_testifier.DataSource = dao.resume_testifier.Where(p => p.rt_r_id == ResumeData.r_id).ToList();
            //repeater_testifier.DataBind();


            r_txt_aboutme_1.Text = ResumeData.r_txt_aboutme_1;
            r_txt_aboutme_2.Text = ResumeData.r_txt_aboutme_2;
            //r_txt_aboutme_3.Text = ResumeData.r_txt_aboutme_3;
            r_txt_career.Text = ResumeData.r_txt_career;

            r_txt_aboutme_4.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, ResumeData.r_id.ToString()), ResumeData.r_txt_aboutme_4, ResumeData.r_id, ResumeData.r_txt_aboutme_4);
            /*
			if (dao.file.Any(p => p.f_group == file_group.ToString() && p.f_group2 == Recruit.FileGroup2.recommend.ToString() && p.f_ref_id == cookie.id.ToString())) {
				var f = dao.file.First(p => p.f_group == file_group.ToString() && p.f_group2 == Recruit.FileGroup2.recommend.ToString() && p.f_ref_id == cookie.id.ToString());
				lbRecommend_file.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, cookie.id.ToString()), f.f_name, cookie.id, f.f_display_name);
			}
			*/
        }
	}


	// 이력서 수정
	protected void update_Click(object sender, EventArgs e)
    {
		if (base.IsRefresh) 
			return;
		

		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == b_id).r_regist_status = "step1";
            www6.selectQF<CommonLib.resume>("r_ap_id", cookie.id, "r_b_id", b_id).r_regist_status = "step1";

        }
		Response.Redirect("/recruit/apply/step1/" + PrimaryKey);
	}


	// 입사지원
	protected void apply_Click(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var cookie = RecruitSession.GetCookie(Context);

            // recruit_job검증
            var exist = www6.selectQ<CommonLib.recruit_job>("bj_id", Convert.ToInt32(PrimaryKey));
            //if (!dao.recruit_job.Any(p => p.bj_id == Convert.ToInt32(PrimaryKey)))
            if (!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            // recruit 검증
            //var recruitJob = dao.recruit_job.First(p => p.bj_id == Convert.ToInt32(PrimaryKey));
            var recruitJob = www6.selectQF<CommonLib.recruit_job>("bj_id", Convert.ToInt32(PrimaryKey));

            var exist2 = www6.selectQ<CommonLib.recruit>("b_id", recruitJob.bj_b_id, "b_display", 1);
            //if (!dao.recruit.Any(p => p.b_id == recruitJob.bj_b_id && p.b_display))
            if (!exist2.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var RecruitData = dao.recruit.First(p => p.b_id == Convert.ToInt32(recruitJob.bj_b_id));
            var RecruitData = www6.selectQF<CommonLib.recruit>("b_id", recruitJob.bj_b_id);

            if (RecruitData.b_completed)
            {
                Response.Write("<script>alert('해당 채용 공고는 마감되었습니다.');</script>");
                return;
            }

            // 지원 기간 확인
            if (DateTime.Now < RecruitData.b_date_begin || RecruitData.b_date_end < DateTime.Now)
            {
                Response.Write("<script>alert('지원 기간이 아닙니다.');</script>");
                return;
            }

            //var resume = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == b_id);
            var resume = www6.selectQF<CommonLib.resume>("r_ap_id", cookie.id, "r_b_id", b_id);

            resume.r_regist_status = "done";
            resume.r_process_status = "doc1";
            resume.r_result = "standby";

            //dao.SubmitChanges();
            www6.update(resume);



            string bTitle = RecruitData.b_title;
            bTitle = "한국 컴패션에 지원해 주셔서 감사합니다.";
            
            Response.Write("<script>alert('지원이 완료되었습니다.'); location.href='/recruit/apply/done'</script>");
            SendMail(resume.r_email, resume.r_name, bTitle);
        }
		Response.Redirect("/recruit/apply/done/");
	}


	void SendMail(string to, string name, string bTitle) {

		try {

            string mailContens = "";
            mailContens += "한국컴패션 채용에 관심을 갖고 지원해 주셔서 감사 드립니다.<br/>";
            //mailContens += "현재 지원해주신 서류를 검토 중에 있습니다.<br/>";
            mailContens += "서류전형 결과는 이메일로 전달되며 이메일 발송에 대해 문자 안내가 있을 예정입니다.<br/>";
            mailContens += "서류 전형 결과는 <span style=\"color:#333;font-weight:bold;font-family:Dotum;\">\'한국컴패션 홈페이지 > 인재채용 > 지원결과 조회\' 에서도 확인하실 수 있습니다.</span>";
            

            var from = ConfigurationManager.AppSettings["recruitEmailSender"];
			
			var args = new Dictionary<string, string> {
				{"{name}" , name },
                { "{mailContens}", mailContens},
                { "{title}", bTitle }
            };
			var title = "[한국컴패션] 지원해 주셔서 감사합니다.";



			Email.Send(this.Context, from, new List<string>() { to },
                //title, "/mail/recruit_apply.html",
                title, "/mail/recruit_result.html",
                args
            , null);

		} catch (Exception e) {
			ErrorLog.Write(this.Context , 0 , e.Message);
			throw e;
		}

	}

}