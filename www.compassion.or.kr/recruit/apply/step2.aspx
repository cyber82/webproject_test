﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="step2.aspx.cs" Inherits="recruit_apply_step2" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    
    <script src="/recruit/apply/step2.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	 <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%" class="on"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->
				<div class="relative">
					<h2 class="sub_tit line">지원서 작성</h2>
					<span class="nec_info">표시는 필수입력 사항입니다.</span>
				</div>

				<!-- 작성프로세스 -->
				<div class="apply_process">
					<ol>
						<li>
							<span class="wrap">
								<span class="step step1">STEP 01</span>
								<span class="txt">기본 이력사항</span>
							</span>
						</li>
						<li><span class="wrap">
								<span class="step step2 on">STEP 02</span>
								<span class="txt">자기소개/경력기술서</span>
							</span>
						</li>
						<li>
							<span class="wrap">
								<span class="step step3">STEP 03</span>
								<span class="txt">신앙간증문/추천서</span>
							</span>
						</li>
						<li>
							<span class="wrap">
								<span class="step step4">STEP 04</span>
								<span class="txt">미리보기 및 제출</span>
							</span>
						</li>
					</ol>
					<ul class="info">
						<li><span class="s_con1">30분 이내 ‘저장’하지 않으면, 세션이 종료되어 로그인 정보가 사라집니다. 30분마다 ‘임시저장’ 해주세요.</span></li>
						<li><span class="s_con1">접수 마감일 전까지는 지원서 수정 및 확인이 가능합니다.</span></li>
					</ul>
				</div>
				<!--// 작성프로세스 -->

				<!-- 자기소개 -->
				<p class="table_tit">
					<span class="tit"><label for="r_txt_aboutme_1">자기소개</label></span>
					<span class="word"><span id="aboutme_1_count">0</span>/5000</span>
				</p>
				<div class="mb60">
					  <textarea runat="server" id="r_txt_aboutme_1" class="textarea_type2" rows="18" ></textarea>
				</div>
				<!--// 자기소개 -->

				<!-- 경력기술서 -->
				<p class="table_tit">
					<span class="tit"><label for="r_txt_career">경력기술서</label></span>
					<span class="word"><span id="career_count">0</span>/5000</span>
				</p>
				<div class="table_line mb40">
					<textarea runat="server" id="r_txt_career" class="textarea_type2" rows="18"></textarea>
				</div>
				<!--// 경력기술서 -->
				

				<div class="btn_ac">
					<div>
						<asp:LinkButton runat="server" class="btn_type2 fl mr10" ID="prev" OnClientClick="return $page.prev()" OnClick="prev_Click">이전</asp:LinkButton>
						<asp:LinkButton runat="server" class="btn_type3 fl mr10" ID="save" OnClientClick="return $page.tempSave()" OnClick="save_Click">임시저장</asp:LinkButton>
						<asp:LinkButton runat="server" class="btn_type1 fl" ID="next" OnClientClick="return $page.save()" OnClick="next_Click">다음</asp:LinkButton>
					</div>
				</div>

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

</asp:Content>