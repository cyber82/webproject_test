﻿$(function () {
	$("#r_txt_aboutme_1").textCount($("#aboutme_1_count"));
	$("#r_txt_career").textCount($("#career_count"));
});


var $page = {
	save: function () {


		if (!validateForm([
				{ id: "#r_txt_aboutme_1", msg: "자기소개 내용을 입력하세요" },
				{ id: "#r_txt_career", msg: "경력기술서 내용을 입력하세요" }
		])) {
			return false;
		}


		if ($("#r_txt_aboutme_1").val().length > 5000) {
			alert("자기소개는 5000자 이하로 입력해주세요.");
			$("#r_txt_aboutme_1").focus();
			return false;
		}


		if ($("#r_txt_career").val().length > 5000) {
			alert("경력기술서는 5000자 이하로 입력해주세요.");
			$("#r_txt_career").focus();
			return false;
		}

		return true;

	},

	prev: function () {
		return confirm("이전 단계로 이동하시겠습니까?");
	},
	tempSave: function () {
		if ($page.save()) {
			return confirm("임시저장 하시겠습니까?");
		} else {
			return false;
		}
	}
}
