﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="step1.aspx.cs" Inherits="recruit_apply_step1" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    
    <script src="/recruit/apply/step1.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />

    <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%" class="on"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->
				<div class="relative">
					<h2 class="sub_tit line">지원서 작성</h2>
					<span class="nec_info">표시는 필수입력 사항입니다.</span>
				</div>

				<!-- 작성프로세스 -->
				<div class="apply_process">
					<ol>
						<li>
							<span class="wrap">
								<span class="step step1 on">STEP 01</span>
								<span class="txt">기본 이력사항</span>
							</span>
						</li>
						<li><span class="wrap">
								<span class="step step2">STEP 02</span>
								<span class="txt">자기소개/경력기술서</span>
							</span>
						</li>
						<li>
							<span class="wrap">
								<span class="step step3">STEP 03</span>
								<span class="txt">신앙간증문/추천서</span>
							</span>
						</li>
						<li>
							<span class="wrap">
								<span class="step step4">STEP 04</span>
								<span class="txt">미리보기 및 제출</span>
							</span>
						</li>
					</ol>
					<ul class="info">
						<li><span class="s_con1">30분 이내 ‘저장’하지 않으면, 세션이 종료되어 로그인 정보가 사라집니다. 30분마다 ‘임시저장’ 해주세요.</span></li>
						<li><span class="s_con1">접수 마감일 전까지는 지원서 수정 및 확인이 가능합니다.</span></li>
					</ul>
				</div>
				<!--// 작성프로세스 -->

				<!-- 기본인적사항 -->
				<p class="table_tit"><span class="tit">기본 인적사항</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>기본 인적사항 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:30%" />
							<col style="width:20%" />
							<col style="width:30%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><span class="nec">이름 (한글)</span></th>
								<td><asp:Literal ID="r_name" runat="server" /></td>
								<th scope="row"><span class="nec">이름 (영문)</span></th>
								<td>
									<label for="r_name_en" class="hidden">이름</label>
									<input type="text" runat="server" id="r_name_en" class="input_type2 mr10" style="width:130px" maxlength="50" placeholder="이름" />
									<label for="r_familyname_en" class="hidden">성</label>
									<input type="text" runat="server" id="r_familyname_en" class="input_type2" style="width:80px" maxlength="50" placeholder="성" />
								</td>
							</tr>
							<tr>
								<th scope="row"><span class="nec">생년월일</span></th>
								<td colspan="3">
									<label for="birth_yy" class="hidden">년도</label>
									<input type="text" runat="server" id="birth_year" maxlength="4" data-format="number" class="input_type2 mr5" style="width:158px" />년
									
									<label for="birth_mm" class="hidden">월</label>
									<input type="text" runat="server" id="birth_month" maxlength="2" data-format="number" class="input_type2 mr5 ml10" style="width:80px" />월

									<label for="birth_dd" class="hidden">일</label>
									<input type="text" runat="server" id="birth_day" maxlength="2" data-format="number" class="input_type2 mr5 ml10" style="width:80px" />일
								</td>
							</tr>
							<tr>
								<th scope="row"><span class="nec">성별</span></th>
								<td colspan="3">
									 <input type="hidden" runat="server" id="r_gender" value="" />
									<span class="radio_ui">
										<input type="radio" name="r_gender" class="r_gender css_radio" value="남" id="male" checked />
										<label for="male" class="css_label">남자</label>
									</span>
									<span class="radio_ui ml30">
										<input type="radio" name="r_gender" class="r_gender css_radio" value="여" id="female" />
										<label for="female" class="css_label">여자</label>
									</span>
								</td>
							</tr>
							<tr>
								<th scope="row"><span class="nec">지원분야</span></th>
								<td colspan="3">[<asp:Literal runat="server" ID="depth1_name" /> - <asp:Literal runat="server" ID="depth2_name" />] <asp:Literal runat="server" ID="b_title" /></td>
							</tr>
							<tr>
								<th scope="row"><span class="nec">이메일</span></th>
								<td colspan="3"><asp:Literal runat="server" ID="r_email"/></td>
							</tr>
							<tr>
								<th scope="row"><label for="r_addr"><span class="nec">현 거주지 주소</span></label></th>
								<td colspan="3"><input type="text" runat="server" id="r_addr" maxlength="100" class="input_type2 mr10" style="width:400px" />시(도)/구(군) 까지만 입력해 주세요.</td>
							</tr>
							<tr>
								<th scope="row"><label for="phone"><span class="nec">휴대폰</span></label></th>
								<td colspan="3"><input type="text" runat="server" id="phone" data-format="phone" class="input_type2" maxlength="30" style="width:400px" /></td>
							</tr>
							<tr>
								<th scope="row"><label for="r_salary"><span class="nec">현재연봉</span></label></th>
								<td><input type="text" runat="server" id="r_salary" maxlength="30" class="input_type2" style="width:240px" /></td>
								<th scope="row"><label for="r_hope_salary"><span class="nec">희망연봉</span></label></th>
								<td><input type="text" runat="server" id="r_hope_salary" maxlength="30" class="input_type2" style="width:240px" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 기본인적사항 -->

				<!-- 병역사항 -->
				<p class="table_tit"><span class="tit">병역사항</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>병역사항 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><label for="r_military_status"><span class="nec">병역구분</span></label></th>
								<td>
									<span class="sel_type2" style="width:400px;">
										<asp:DropDownList class="custom_sel" runat="server" ID="r_military_status"></asp:DropDownList>
									</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 병역사항 -->

				<!-- 보훈사항 -->
				<p class="table_tit"><span class="tit">보훈사항</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>보훈사항 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><span class="nec">보훈여부</span></th>
								<td>
									<input type="hidden" runat="server" id="r_veterans_is" value="" />
									<span class="radio_ui">
										<input type="radio" id="r_veterans_is1" name="r_veterans_is" value="1" class="css_radio r_veterans_is" />
										<label for="r_veterans_is1" class="css_label">대상</label>
									</span>
									<span class="radio_ui ml30">
										<input type="radio" id="r_veterans_is2" name="r_veterans_is" value="0" class="css_radio r_veterans_is" checked />
										<label for="r_veterans_is2" class="css_label">비대상</label>
									</span>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="r_veterans_no"><span>보훈번호</span></label></th>
								<td><input type="text" runat="server" id="r_veterans_no" maxlength="50" disabled class="input_type2" style="width:400px"/></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 보훈사항 -->

				<!-- 장애사항 -->
				<p class="table_tit"><span class="tit">장애사항</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>장애사항 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><span class="nec">장애여부</span></th>
								<td>
									<input type="hidden" runat="server" id="r_disability_status" value="" />
									<span class="radio_ui">
										<input type="radio" name="r_disability_status" id="r_disability_status1" value="대상" class="css_radio r_disability_status" />
										<label for="r_disability_status1" class="css_label">대상</label>
									</span>
									<span class="radio_ui ml30">
										<input type="radio" name="r_disability_status" id="r_disability_status2" value="비대상" class="css_radio r_disability_status" checked />
										<label for="r_disability_status2" class="css_label">비대상</label>
									</span>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="r_disability_type"><span>장애유형</span></label></th>
								<td>
									<span class="sel_type2 disability_1" style="width:400px;">
										<asp:DropDownList class="custom_sel" runat="server" ID="r_disability_type" title="장애유형" ></asp:DropDownList>
									</span>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="r_disability_level"><span>장애등급</span></label></th>
								<td>
									<span class="sel_type2" style="width:400px;">
										<asp:DropDownList class="custom_sel" runat="server" ID="r_disability_level" title="장애등급" ></asp:DropDownList>
									</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 장애사항 -->

				<!-- 출석교회정보 -->
				<p class="table_tit"><span class="tit">출석교회 정보</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>출석교회 정보 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><label for="r_church_name"><span class="nec">출석교회명</span></label></th>
								<td><input type="text" runat="server" id="r_church_name" maxlength="100" class="input_type2" style="width:400px" /></td>
							</tr>
							<tr>
								<th scope="row"><label for="r_church_team"><span class="nec">소속교단명</span></label></th>
								<td><input type="text" runat="server" id="r_church_team" maxlength="100" class="input_type2" style="width:400px" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 출석교회정보 -->

				<!-- 컴패션 후원여부 -->
				<p class="table_tit"><span class="tit">컴패션 후원여부</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>컴패션 후원여부 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><span class="nec">컴패션 후원여부</span></th>
								<td>
									<input type="hidden" id="r_is_sponsor" runat="server" />
									<span class="radio_ui">
										<input type="radio" id="r_is_sponsor1" name="r_is_sponsor"  value="1" class="css_radio r_is_sponsor" checked />
										<label for="r_is_sponsor1" class="css_label">예</label>
									</span>
									<span class="radio_ui ml30">
										<input type="radio" id="r_is_sponsor2" name="r_is_sponsor"  value="0" class="css_radio r_is_sponsor" />
										<label for="r_is_sponsor2" class="css_label">아니오</label>
									</span>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<label class="desc1 mb15">컴패션 후원과 관련된 특이사항이 있는 경우 자유롭게 기술해주세요.</label>
									<p class="table_tit">
										<textarea class="textarea_type2" runat="server" id="r_sponsor_remark" rows="5" style="width:97%"></textarea><br />
										<span style="right:0; position:absolute; margin-right:27px;"><span id="sponsor_remark_count">0</span>/2000</span>
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 컴패션 후원여부 -->

				<!-- 학력사항 -->
				<p class="table_tit">
					<span class="tit nec">학력사항</span>
					<span class="btn">
						<span class="sub_desc">추가 버튼을 누르시면 해당 항목을 추가하여 작성하실 수 있습니다.</span>
						<button class="btn_s_type1 btnAdd" data-id="edu"><span>추가</span></button>
					</span>
				</p>
				<div class="tableWrap2 mb60">
					<input type="hidden" runat="server" id="data_edu" class="loaded-data save-data" data-id="edu" value=""/>
					<input type="hidden" runat="server" id="last_edu" value=""/> 
					<table class="tbl_type3 fill-box" data-id="edu">
						<caption>학력사항 테이블</caption>
						<colgroup>
							<col style="width:7%" />
							<col style="width:16%" />
							<col style="width:21%" />
							<col style="width:14%" />
							<col style="width:14%" />
							<col style="width:14%" />
							<col style="width:14%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col">삭제</th>
								<th scope="col">학력구분</th>
								<th scope="col">학교명</th>
								<th scope="col">전공</th>
								<th scope="col">입학연월</th>
								<th scope="col">졸업연월</th>
								<th scope="col">소재지</th>
							</tr>
						</thead>
						<tbody>
							<tr class="template" data-id="edu">
								<td><button class="vam" data-role="remove"><span><img src="/common/img/btn/del.png" alt="삭제"/></span></button></td>
								<td>
									<label class="hidden" for="school_1_1">학력구분</label>
									<span class="sel_type2 padding1" style="width:100%">
										<select class="custom_sel" data-role="ru_grade" data-required title="학력을 선택해주세요" name="">
											<option value="선택해 주세요">선택해 주세요</option>
											<option value="고등학교">고등학교</option>
											<option value="전문대">전문대</option>
											<option value="대학교">대학교</option>
											<option value="대학교(편입)">대학교(편입)</option>
											<option value="대학원(석사)">대학원(석사)</option>
											<option value="대학원(박사)">대학원(박사)</option>
										</select>
									</span>
								</td>
								<td>
									<label class="hidden" for="ru_name">학교명</label>
									<input type="text" id="ru_name" data-role="ru_name" title="학교명" maxlength="100" class="input_type2"  data-required/>
								</td>
								<td>
									<label class="hidden" for="ru_major">전공</label>
									<input type="text" id="ru_major" data-role="ru_major" title="전공" maxlength="100" class="input_type2" data-required/>
								</td>
								<td>
									<label class="hidden" for="ru_date_begin">입학연월</label>
									<input type="text" id="ru_date_begin" data-role="ru_date_begin" title="입학연월" data-format="yyyymm" maxlength="7" class="input_type2" data-required/>
								</td>
								<td>
									<label class="hidden" for="ru_date_end">졸업연월</label>
									<input type="text" id="ru_date_end" data-role="ru_date_end" title="졸업연월" data-format="yyyymm" maxlength="7" class="input_type2" data-required/>
								</td>
								<td>
									<label class="hidden" for="ru_location">소재지</label>
									<input type="text" id="ru_location" data-role="ru_location" title="소재지" maxlength="10" class="input_type2" data-required/>

								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 학력사항 -->

				<!-- 어학능력 -->
				<p class="table_tit">
					<span class="tit">어학능력</span>
					<span class="btn"><button class="btn_s_type1 btnAdd" data-id="lang"><span>추가</span></button></span>
				</p>
				<div class="tableWrap2 mb30">
					<input type="hidden" runat="server" id="data_lang" class="loaded-data save-data" data-id="lang" value=""/>
					<table class="tbl_type3 fill-box" data-id="lang">
						<caption>어학능력 테이블</caption>
						<colgroup>
							<col style="width:7%" />
							<col style="width:16%" />
							<col style="width:49%" />
							<col style="width:14%" />
							<col style="width:14%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col">삭제</th>
								<th scope="col">외국어명</th>
								<th scope="col">인증기관</th>
								<th scope="col">점수/등급</th>
								<th scope="col">취득일</th>
							</tr>
						</thead>
						<tbody class="lang_template">
							<tr class="" data-id="lang">
								<td><button class="vam" data-role="remove"><span><img src="/common/img/btn/del.png" alt="삭제" /></span></button></td>
								<td>
									<label class="hidden" for="rle_name">외국어명</label>
									<input type="text" id="rle_name" data-role="rle_name" title="외국어명" maxlength="50" class="input_type2" data-required/>
								</td>
								<td>
									<label class="hidden" for="rle_organization">인증기관</label>
									<input type="text" id="rle_organization" data-role="rle_organization" title="인증기관"  maxlength="50" class="input_type2" data-required/>
								</td>
								<td class="grade">
									<label class="hidden" for="rle_point">점수/등급</label>
									<input type="text" id="rle_point" data-role="rle_point" title="점수(급)"  maxlength="3" class="input_type2 mr5" style="width:45%" data-required/>
									<span>점수(급)</span>
								</td>
								<td>
									<label class="hidden" for="rle_date">취득일</label>
									<input type="text" id="rle_date" data-role="rle_date" title="취득일" data-format="date" maxlength="10" class="input_type2" data-required/>
								</td>
							</tr>
							<tr data-id="lang">
								<td colspan="5">
									<label for="rle_comment" class="hidden">어학능력 자유기술</label>
									<p class="table_tit">
										<textarea id="rle_comment" data-role="rle_comment" class="textarea_type2 lang_textarea" rows="5" placeholder="해당 어학능력에 대해 자유롭게 기술하세요."></textarea>
										<span style="display:inline-block;width:100%;text-align:right;"><span class="lang_textarea_count">0</span>/2000</span>
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="guide mb60">
					<ul class="padding0">
						<li><span class="s_con1">추가 버튼을 누르시면 해당 항목을 추가하여 작성하실 수 있습니다.</span></li>
						<li><span class="s_con1">외국어 인증 점수를 입력한 경우 최종 합격 시 이를 증빙할 수 있는 서류를 제출하셔야 합니다.</span></li>
					</ul>
				</div>
				<!--// 어학능력 -->

				<!-- 자격및면허사항 -->
				<p class="table_tit">
					<span class="tit">자격 및 면허사항</span>
					<span class="btn"><button class="btn_s_type1 btnAdd" data-id="certificate"><span>추가</span></button></span>
				</p>
				<div class="tableWrap2 mb30">
					<input type="hidden" runat="server" id="data_certificate" class="loaded-data save-data" data-id="certificate" value=""/>
					<table class="tbl_type3 fill-box"  data-id="certificate">
						<caption>자격 및 면허사항 테이블</caption>
						<colgroup>
							<col style="width:7%" />
							<col style="width:45%" />
							<col style="width:16%" />
							<col style="width:16%" />
							<col style="width:16%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col">삭제</th>
								<th scope="col">자격증명</th>
								<th scope="col">발급기관</th>
								<th scope="col">자격증번호</th>
								<th scope="col">합격일</th>
							</tr>
						</thead>
						<tbody>
							<tr class="template" data-id="certificate">
								<td><button class="vam" data-role="remove"><span><img src="/common/img/btn/del.png" alt="삭제" /></span></button></td>
								<td>
									<label class="hidden" for="rc_name">자격증명</label>
									<input type="text" id="rc_name" data-role="rc_name" title="자격증명" maxlength="50" class="input_type2" data-required/>
								</td>
								<td>
									<label class="hidden" for="rc_issue">발급기관</label>
									<input type="text" id="rc_issue" data-role="rc_issue" title="발급기관"  maxlength="50" class="input_type2" data-required/>
								</td>
								<td>
									<label class="hidden" for="rc_no">자격증번호</label>
									<input type="text" id="rc_no" data-role="rc_no" title="자격증번호" maxlength="50" class="input_type2" data-required/>
								</td>
								<td>
									<label class="hidden" for="rc_date">합격일</label>
									<input type="text" id="rc_date" data-role="rc_date" title="합격일" data-format="date" maxlength="10" class="input_type2" data-required/>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="guide mb60">
					<ul class="padding0">
						<li><span class="s_con1">추가 버튼을 누르시면 해당 항목을 추가하여 작성하실 수 있습니다.</span></li>
						<li><span class="s_con1">자격 및 면허 사항을 입력한 경우 최종 합격 시 이를 증빙할 수 있는 서류를 제출하셔야 합니다.</span></li>
					</ul>
				</div>
				<!--// 자격및면허사항 -->

				<!-- 경력사항 -->
				<p class="table_tit">
					<span class="tit">경력사항</span>
					<span class="btn"><button class="btn_s_type1 btnAdd" data-id="work"><span>추가</span></button></span>
				</p>
				<div class="tableWrap2 mb30">
					<input type="hidden" runat="server" id="data_work" class="loaded-data save-data" data-id="work" value=""/>
					<table class="tbl_type3 fill-box" data-id="work">
						<caption>경력사항 테이블</caption>
						<colgroup>
							<col style="width:7%" />
							<col style="width:17%" />
							<col style="width:18%" />
							<col style="width:21%" />
							<col style="width:14%" />
							<col style="width:9%" />
							<col style="width:14%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col">삭제</th>
								<th scope="col">회사명</th>
								<th scope="col">고용형태</th>
								<th scope="col">근무기간</th>
								<th scope="col">근무부서</th>
								<th scope="col">직급</th>
								<th scope="col">담당업무</th>
							</tr>
						</thead>
						<tbody>
							<tr class="template" data-id="work">
								<td><button class="vam" data-role="remove"><span><img src="/common/img/btn/del.png" alt="삭제" /></span></button></td>
								<td>
									<label class="hidden" for="rw_name">회사명</label>
									<input type="text" id="rw_name" data-role="rw_name" title="회사명"  maxlength="50" class="input_type2" data-required/>
								</td>
								<td>
									<label class="hidden" for="rw_status">고용형태</label>
									<span class="sel_type2 padding1" style="width:100%">
										 <select class="custom_sel" ID="rw_status" title="고용형태" data-role="rw_status" data-required>
											<option value="">선택해주세요</option>
											<option value="정규직">정규직</option>
											<option value="계약직">계약직</option>
											<option value="파견직">파견직</option>
											<option value="기타">기타</option>
										</select>
									</span>
								</td>
								<td>
									<label class="hidden" for="rw_date_begin">근무기간 시작일</label>
									<input type="text" id="rw_date_begin" data-role="rw_date_begin" title="근무기간" data-format="yyyymm" maxlength="7" class="input_type2" style="width:43%" data-required/>
									&nbsp;~&nbsp;
									<label class="hidden" for="career_1_4">근무기간 종료일</label>
									<input type="text" data-role="rw_date_end" title="근무기간" data-format="date_allow_string" class="input_type2" style="width:43%" maxlength="7" />
								</td>
								<td>
									<label class="hidden" for="rw_role">근무부서</label>
									<input type="text" id="rw_role" data-role="rw_role" title="근무부서" maxlength="50" class="input_type2" data-required/>
								</td>
								<td>
									<label class="hidden" for="rw_position">직급</label>
									<input type="text" id="rw_position" data-role="rw_position" title="직급" maxlength="50" class="input_type2" data-required/>
								</td>
								<td>
									<label class="hidden" for="rw_work">담당업무</label>
									<input type="text" id="rw_work" data-role="rw_work" title="담당업무" maxlength="300" class="input_type2" data-required/>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="guide mb60">
					<ul class="padding0">
						<li><span class="s_con1">추가 버튼을 누르시면 해당 항목을 추가하여 작성하실 수 있습니다.</span></li>
						<li><span class="s_con1">경력사항을 입력한 경우 최종 합격 시 이를 증빙할 수 있는 서류를 제출하셔야 합니다.</span></li>
						<li><span class="s_con1">경력은 최종 경력에서부터 최초 경력 순으로 순차적으로 기재하시기 바랍니다.</span></li>
						<li><span class="s_con1">재직 중일 경우에는 근무기간에 입사연월 ~ 재직 중으로 표기하시기 바랍니다.</span></li>
						<li><span class="s_con1">근무기간은 YYYY.MM ~ YYYY.MM의 형태로 작성해주시기 바랍니다.</span></li>
					</ul>
				</div>
				<!--// 경력사항 -->

				<!-- 포트폴리오 -->
				<p class="table_tit"><span class="tit">포트폴리오</span></p>
				<div class="tableWrap2 mb30">
					<table class="tbl_type2">
						<caption>포트폴리오 입력 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><label for="portfolio_1"><span>URL</span></label></th>
								<td>
									<span class="http_wrap">
										<label class="hidden" for="r_portfolio">url</label>
										<input type="text" title="r_portfolio" runat="server" id="r_portfolio" maxlength="300" 1placeholder="http://" class="input_type2 url" />
										<span class="http">http://</span>
									</span>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="lb_file_path"><span>첨부파일</span></label></th>
								<td>
									<div class="btn_attach clear2 relative">
										<input type="text" runat="server" id="lb_file_path" readonly value="" class="input_type1 fl mr10" style="width:400px;background:#fdfdfd;border:1px solid #d8d8d8;" />
										<a href="#" id="btn_file_path" class="btn_type8 fl mr10"><span>파일선택</span></a>
                                        
                                        <a href="#" id="btn_remove" style="display:none;" class="btn_type9 fl"><span>삭제</span></a>
									</div>
									<p class="pt5"><span class="s_con1">첨부 파일은 5MB 이내로 제한합니다.</span></p>

								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="guide mb60">
					<ul class="padding0">
						<li><span class="s_con1">채용 공고 내용을 통해 포트폴리오 필수 첨부 여부를 확인 부탁 드립니다.</span></li>
						<li><span class="s_con1">필수 제출인 경우 반드시 제출 해주셔야 합니다.</span></li>
						<li><span class="s_con1">필수 제출이 아닌 경우에라도 본인의 포트폴리오를 제출하기 원하시면 제출하실 수 있습니다.</span></li>
						<li><span class="s_con1">파일 첨부 또는 URL 주소로 입력하여 주십시오.</span></li>
					</ul>
				</div>
				<!--// 포트폴리오 -->

				<div class="btn_ac">
					<div>
						<asp:LinkButton runat="server" class="btn_type3 fl mr10" ID="save" OnClientClick="return $page.tempSave()" OnClick="save_Click">임시저장</asp:LinkButton>
						<asp:LinkButton runat="server" class="btn_type1 fl" ID="next" OnClientClick="return $page.save()" OnClick="next_Click">다음</asp:LinkButton>
					</div>
				</div>

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

</asp:Content>
