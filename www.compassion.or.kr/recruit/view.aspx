﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="recruit_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
		<!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<!-- w980 -->
			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%" class="on"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->

				<!-- 채용공고상세 -->
				<div class="boardView_1">

					<div class="tit_box noline">
						<span class="tit"><asp:Literal runat="server" ID="b_title" /></span>
						<span class="txt">
							<span><asp:Literal runat="server" ID="depth1_name" /><%-- - <asp:Literal runat="server" ID="depth2_name" />--%></span>
							<span class="bar"></span>
							<span><asp:Literal runat="server" ID="b_date_begin" /> ~ <asp:Literal runat="server" ID="b_date_end" /></span>
							<span class="bar"></span>
							<span><asp:Literal runat="server" ID="b_status" /></span>
						</span>
					</div>
					<div class="view_contents line2">
						<asp:Literal runat="server" ID="b_content" />
					</div>

				</div>
				<!--// 채용공고상세 -->

				<div class="clear2">
					 <a href="#" class="btn_type4 fr" title="목록" runat="server" id="btnList">목록</a>
					 <asp:LinkButton class="btn_type1 fr mr10" runat="server" ID="update_resume" OnClick="update_resume_Click">입사지원</asp:LinkButton>
				</div>


			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

</asp:Content>