﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="recruit_process" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    
    <script src="/recruit/apply/step3.js"></script>
    <script type="text/javascript">

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
   <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%" class="on"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->

				<h2 class="sub_tit line">전형절차</h2>
				<div class="clear2 mb80">
					<p class="fr pt15"><span class="s_con1">전형절차는 상황에 따라 달리 진행될 수 있습니다.</span></p>
				</div>

				<div class="total_process">
					<ol>
						<li class="step1">온라인 입사지원</li>
						<li class="step2">서류심사</li>
						<li class="step3">통합역량검사</li>
						<li class="step4">직무테스트<br /><span>(해당 경우 진행)</span></li>
                        <li class="step5">현업 인터뷰</li>
						<li class="step6">HR 인터뷰</li>
						<li class="step7">경영진 인터뷰</li>
						<li class="step8">채용 확정</li>
					</ol>
				</div>

				<div class="tp_box">
					<div class="box">
						<p class="tit">진행 중인 채용</p>
						<p class="desc">현재 채용을 진행하고 있는 모집 분야를<br />확인 후 지원하실 수 있습니다.</p>
						<a href="/recruit/" class="btn_type8">온라인 입사 지원</a>
					</div>

					<div class="box">
						<p class="tit">상시지원</p>
						<p class="desc">지원서를 등록하시면 결원 및 채용 계획<br />발생시 서류 심사 대상이 될 수 있습니다.</p>
						<a href="/recruit/pool/" class="btn_type8">상시지원</a>
					</div>

					<div class="box">
						<p class="tit">채용 관련 문의</p>
						<p class="desc">
							FAQ를 참고해주시고 기타 사항은<br />아래로 문의 해주세요.<br />
							<span>E-mail : recruit@compassion.or.kr<br />TEL : 02-3668-3488 (인사팀)</span>
						</p>
						<a href="/recruit/faq/" class="btn_type8">FAQ</a>
					</div>
				</div>
				
			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

</asp:Content>