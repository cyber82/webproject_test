﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class recruit_view : FrontBasePage {

	const string listPath = "/recruit";


	recruit RecruitData{
		get{
			if (this.ViewState["recruit"] == null)
				this.ViewState["recruit"] = new recruit().ToJson();
			return this.ViewState["recruit"].ToString().ToObject<recruit>();
		}
		set{
			this.ViewState["recruit"] = value.ToJson();
		}
	}



	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		base.PrimaryKey = requests[0];


		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}



	}


	protected override void loadComplete(object sender, EventArgs e)
    {
        using (RecruitDataContext dao = new RecruitDataContext())
        {
            // recruit_job검증
            var exist = www6.selectQ<recruit_job>("bj_id", Convert.ToInt32(PrimaryKey));
            //if (!dao.recruit_job.Any(p => p.bj_id == Convert.ToInt32(PrimaryKey)))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            // recruit 검증
            var recruitJob = exist[0]; //dao.recruit_job.First(p => p.bj_id == Convert.ToInt32(PrimaryKey));

            var exist2 = www6.selectQ<recruit>("b_id", recruitJob.bj_b_id, "b_display", 1);
            //if (!dao.recruit.Any(p => p.b_id == recruitJob.bj_b_id && p.b_display))
            if(!exist2.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var entity = dao.recruit.First(p => p.b_id == Convert.ToInt32(recruitJob.bj_b_id));
            var entity = exist2.First(p => p.b_id == Convert.ToInt32(recruitJob.bj_b_id));
            RecruitData = entity;

            b_title.Text = entity.b_title;
            //depth1_name.Text = dao.recruit_jobcode.First(p => p.rj_depth1 == recruitJob.bj_rj_depth1 && p.rj_depth2 == "").rj_name;
            depth1_name.Text = www6.selectQF<recruit_jobcode>("rj_depth1", recruitJob.bj_rj_depth1, "rj_depth2", "").rj_name;
            b_content.Text = entity.b_content.ToHtml();
            b_date_begin.Text = entity.b_date_begin.ToString("yyyy.MM.dd");
            b_date_end.Text = entity.b_timeless ? "채용시까지" : entity.b_date_end.ToString("yyyy.MM.dd");
            b_status.Text = entity.b_completed ? "채용마감" : entity.b_date_end.CompareTo(DateTime.Now) > -1 ? "진행중" : "접수마감";




            if (RecruitData.b_completed || RecruitData.b_date_end < DateTime.Now)
            {
                update_resume.Visible = false;
            }


        }
	} 


	protected void update_resume_Click(object sender, EventArgs e) {


		if (RecruitData.b_completed) {
			base.AlertWithJavascript("해당 채용 공고는 마감되었습니다.", "goList()");
			return;
		}

		// 지원 기간 확인
		if (DateTime.Now < RecruitData.b_date_begin) {
			base.AlertWithJavascript("지원 기간이 아닙니다.", "");
			return;
		}

		if (RecruitData.b_date_end < DateTime.Now) {
			base.AlertWithJavascript("해당 채용 공고는 마감되었습니다.", "goList()");
			return;
		}



		/*
		if (RecruitSession.HasCookie(Context)) {

			using (RecruitDataContext dao = new RecruitDataContext()) {

				var cookie = RecruitSession.GetCookie(Context);
				var now = DateTime.Now;
				// 지원 완료 확인
				if (RecruitData.b_date_begin > now || RecruitData.b_date_end < now) {
					Response.Write("<script>alert('지원기간이 아닙니다.');</script>");
					return;
				}

				var step = "step1";
				if (dao.resume.Any(p => p.r_b_id == Convert.ToInt32(PrimaryKey))) {
					var resume = dao.resume.First(p => p.r_id == Convert.ToInt32(PrimaryKey) && p.r_ap_id == cookie.id);
					step = resume.r_regist_status;
					if (step == "done") step = "step1";
				}
				Response.Redirect("/recruit/apply/" + step + "/" + PrimaryKey);
			}


		} else {
			Response.Write("<script>alert('로그인 후 지원하세요.');location.href='/recruit/login/?r=/recruit/apply/step1/"+PrimaryKey+"'; </script>");
		}
		*/

		Response.Redirect("/recruit/login/?id=" + PrimaryKey);
	}
}