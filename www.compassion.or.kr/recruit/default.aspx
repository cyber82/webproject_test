﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="recruit_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/paging.ascx" TagPrefix="uc" TagName="paging" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	    <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<!-- w980 -->
			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%" class="on"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->

				<!-- 채용공고리스트 -->
				<div class="tableWrap3 mb40">
					<table class="tbl_type4">
						<caption>채용공고 리스트</caption>
						<colgroup>
							<col style="width:20%">
							<col style="width:44%">
							<col style="width:22%">
							<col style="width:14%">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">모집부문</th>
								<th scope="col">모집분야</th>
								<th scope="col">접수기간</th>
								<th scope="col">상태</th>
							</tr>
						</thead>
						<tbody>
							<asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
								<ItemTemplate>
									<tr>
										<td class="tit"><%#Eval("depth1_name")%><%---<%#Eval("depth2_name") %>--%> </td>
										<td><a href="/recruit/view/<%#Eval("bj_id") %>"><%#Eval("b_title")%></a></td>
										<td><%#Eval("b_date_begin" , "{0:yyyy.MM.dd}")%> ~ <asp:Literal runat="server" ID="lbDateEnd"></asp:Literal></td>
										<td><asp:Literal runat=server ID=lbStatus></asp:Literal></td>
									</tr>	
								</ItemTemplate>
									<FooterTemplate>
										<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
											<td colspan="4" class="no_content">진행 중인 채용 공고가 없습니다.</td>
										</tr>
								</FooterTemplate>
							</asp:Repeater>		
						</tbody>
					</table>
				</div>
				<!--// 채용공고리스트 -->

				<uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" Hash="c" />

				<div class="guide">
					<p>알아두세요!</p>
					<ul>
						<li><span class="s_con1">입사지원서는 서류 접수 마감일 전까지 자유롭게 등록 및 수정하실 수 있습니다.</span></li>
						<li><span class="s_con1">입사지원 관련 궁금하신 점은 채용 문의 메일 (recruit@compassion.or.kr)을 이용하여 주시기 바랍니다.</span></li>
						<li><span class="s_con1">합격여부는 지원결과조회를 통해 확인 가능하며, 결과 확인이 가능함을 email로 안내 드립니다.</span></li>
					</ul>
				</div>

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

  

    
</asp:Content>