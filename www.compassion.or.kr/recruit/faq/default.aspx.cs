﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class recruit_faq_default : FrontBasePage {


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_recruit_faq_list_f(1, 10000);
            Object[] op1 = new Object[] { "page", "rowsPerPage" };
            Object[] op2 = new Object[] { 1, 10000 };
            var list = www6.selectSP("sp_recruit_faq_list_f", op1, op2).DataTableToList<sp_recruit_faq_list_fResult>();

            repeater.DataSource = list;
            repeater.DataBind();
        }
	}


	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
	}

}