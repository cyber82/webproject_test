﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="recruit_faq_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/paging.ascx" TagPrefix="uc" TagName="paging" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        $(function () {
            $(".question").click(function () {
                if ($(this).next().is(":visible")) {
                    $(this).next().hide();
                } else {
                    $(this).next().show();
                }
                
            });
        });

		
		$(function () {
			$(".q_wrap").click(function () {

				if ($(this).hasClass("on") == true) {
					$(this).parent().find(".a_wrap").slideUp(300);
					$(".q_wrap").removeClass("on");
				}
				else {
					$(".a_wrap").slideUp(300);
					$(this).parent().find(".a_wrap").slideDown(300);
					$(".q_wrap").removeClass("on");
					$(this).addClass("on");
				}
				return false;
			})
		})
		
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%" class="on"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->
				<div class="faq">
					<!-- set -->
					<asp:Repeater runat=server ID=repeater >
						<ItemTemplate>
							<div class="faqSet">
								<button class="q_wrap">
									<span><%#Eval("f_question")%></span>
								</button>
								<div class="a_wrap">
									<%#Eval("f_answer")%>
								</div>
							</div>
						</ItemTemplate>
					</asp:Repeater>		
					<!--// -->
				</div>
			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

</asp:Content>