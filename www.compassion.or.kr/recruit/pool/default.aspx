﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="recruit_poll_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script src="/assets/jquery/wisekit/form.js"></script>
    <script src="/assets/jquery/wisekit/validation/password.js"></script>
    <script src="/assets/jquery/wisekit/message.js"></script>
    <script type="text/javascript">

        $(function () {
            $("#bj_rj_depth1").selectbox({
                onChange: function (val, inst) {
                    if (val != "") {
                        fillDepth2(val);
                    } else {
                        $("#bj_rj_depth2").empty();
                    }
                }
            });


            $("#bj_rj_depth2").selectbox();
        });


        var fillDepth2 = function (depth1) {
            $.get("/api/recruit.ashx?t=get_depth2&depth1="+depth1, function (r) {
                    
                if (r.success) {
                    var list = $.parseJSON(r.data);

                    $("#bj_rj_depth2").empty();
                    $("#bj_rj_depth2").selectbox('detach');


                    $("#bj_rj_depth2").append("<option value=''>분야선택</option>");
                    $.each(list, function () {
                        $("#bj_rj_depth2").append("<option value='"+this.rj_depth2+"'>"+this.rj_name+"</option>");
                    });

                    $("#bj_rj_depth2").selectbox({
                        onChange: function (val, inst) {
                            $("#hidden_depth2").val(val);
                        }
                    });
                }
            });

        }


        var goSubmit = function () {
            if ($("#email").val() == "") {
                $("#msg_email").show();
                $("#msg_email_txt").text("정보를 입력해 주세요.");
                $("#email").focus();
                return false;
            }

            if ($("#password").val() == "") {
                $("#msg_pwd").show();
                $("#msg_pwd_txt").text("정보를 입력해 주세요.");
                $("#password").focus();
                return false;
            }

            if ($("#bj_rj_depth1").val() == "") {
                $("#msg_part").show();
                $("#msg_part_txt").text("모집부문을 선택해 주세요.");
                $("#bj_rj_depth1").focus();
                return false;
            }
            if ($("#bj_rj_depth2").val() == "") {
                $("#msg_part").show();
                $("#msg_part_txt").text("모집분야를 선택해 주세요.");
                $("#bj_rj_depth2").focus();
                return false;
            }



        }
    </script>
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" runat="server" id="hidden_depth2" />

	<!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%" class="on"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->

				<h2 class="sub_tit">본인확인 및 모집부문 선택</h2>

				<div class="box_con1">
					<div class="field">
						<label class="tit" for="email">이메일</label>
						<asp:TextBox runat="server" ID="email" placeholder=""/>
					</div>
					<p class="comt1" id="msg_email" style="display:none" runat="server"><span class="guide_comment2" runat="server" id="msg_email_txt">입력 오류 메시지</span></p>

					<div class="field">
						<label class="tit" for="password">비밀번호</label>
						<asp:TextBox TextMode="Password" runat="server" ID="password" placeholder=""/>
					</div>
					<p class="comt1" id="msg_pwd" runat="server" style="display:none"><span class="guide_comment2" runat="server" id="msg_pwd_txt">입력 오류 메시지</span></p>

					<div class="field padding1">
						<p class="tit">모집부문</p>
					</div>
					<div class="mb30">
							
						<label class="hidden" for="part1">부문선택</label>
						<span class="sel_type2 mr10" style="width:235px;">
                            <select id="bj_rj_depth1" runat="server"></select>
						</span>
							
						<label class="hidden" for="part2">분야선택</label>
						<span class="sel_type2" style="width:235px;">
							<select class="sel_type2" runat="server" ID="bj_rj_depth2">
                                <option value="">분야선택</option>
							</select>
						</span>
							
					</div>
					<p class="comt1" id="msg_part" style="display:none"><span class="guide_comment2" id="msg_part_txt">입력 오류 메시지</span></p>

					<p class="check_pw">비밀번호를 잊으셨나요?<a href="/recruit/login/identify" class="btn_arr_type1" runat="server" id="btnFind">비밀번호 찾기<span></span></a></p>
					<asp:LinkButton class="btn_b_type4" runat="server" ToolTip="확인" ID="btnRegist" OnClientClick="return goSubmit()" OnClick="btnRegist_Click">확인</asp:LinkButton>

				</div>
				<div class="box_info1">
					<span class="txt">처음 지원하시는 분은 지원자 등록을 먼저 해주세요.</span>
					<a href="/recruit/signup/?r=/recruit/pool/" class="btn_s_type2 ml10">지원자 등록</a>
				</div>

				<div class="guide2 mt40">
					<span class="s_con2">상시지원에 지원서를 등록하시면, 본인이 희망하는 직무의 채용이 진행될 때 검토하며 해당 업무에 적합할 것으로 판단될 경우 개별 연락 드립니다.</span>
                    <span class="s_con2">상시지원에 등록된 이력서는 개인정보보호를 위하여 등록 후 6개월이 경과되면 자동 삭제됩니다.</span>
				</div>

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->
   
	
    

    
    
    

</asp:Content>