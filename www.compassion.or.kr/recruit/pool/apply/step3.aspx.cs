﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using System.IO;
using System.Configuration;
using CommonLib;

public partial class recruit_apply_step3 : FrontBasePage {

	Uploader.FileGroup file_group = Uploader.FileGroup.recruit;

	public override bool RequireRecruitLogin{
		get{
			return true;
		}
	}

	public override bool RequireSSL {
		get {return true;}
	}
	
	protected string depth1{
		set {
			this.ViewState.Add("depth1", value);
		}
		get {
			if (this.ViewState["depth1"] == null) return "";
			return this.ViewState["depth1"].ToString();
		}
	}
	
	protected string depth2{
		set {
			this.ViewState.Add("depth2", value);
		}
		get {
			if (this.ViewState["depth2"] == null) return "";
			return this.ViewState["depth2"].ToString();
		}
	}


	const string listPath = "/recruit/pool/";


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		// check param
		var requests = Request.GetFriendlyUrlSegments();


		if (requests.Count > 0) {
			depth1 = requests[0];


			upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.recruit, depth1);

			if (requests.Count > 1) {
				depth2 = requests[1];
			}

			ValidateParam();
		} else {
			Response.Redirect(listPath, true);
		}

		LoadData();
	}





	protected void ValidateParam()
    {
		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            // recruit_jobcode검증
            var exist = www6.selectQ<recruit_jobcode>("rj_depth1", depth1, "rj_depth2", depth2, "rj_display", 1);
            //if (!dao.recruit_jobcode.Any(p => p.rj_depth1 == depth1 && p.rj_depth2 == depth2 && p.rj_display))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            // 지원서 검증
            var exist2 = www6.selectQ<resume>("r_ap_id", cookie.id, "r_rj_depth1", depth1, "r_rj_depth2", depth2);
            //if (!dao.resume.Any(p => p.r_ap_id == cookie.id && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2))
            if (!exist2.Any())
            {
                Response.Redirect(listPath, true);
            }

        }
	}

	protected void LoadData()
    {
		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var resume = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2);
            var resume = www6.selectQF2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2);
            r_txt_aboutme_2.Value = resume.r_txt_aboutme_2;
            //r_txt_aboutme_3.Value = resume.r_txt_aboutme_3;

            upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.recruit, resume.r_id.ToString());

            lb_file_path.Value = resume.r_txt_aboutme_4;
        }
	}

	protected void save_Click(object sender, EventArgs e) {
		Save();
	}

	protected void next_Click(object sender, EventArgs e)
    {
		Save();
        using (FrontDataContext dao = new FrontDataContext())
        {
            var cookie = RecruitSession.GetCookie(Context);
            //dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2).r_regist_status = "step4";
           www6.selectQF2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2).r_regist_status = "step4";
        }
		Response.Redirect("/recruit/pool/apply/step4/" + depth1 + (depth2 == "" ? "" : "/" + depth2));
	}

	protected void prev_Click(object sender, EventArgs e)
    {
		Save();
        using (FrontDataContext dao = new FrontDataContext())
        {
            var cookie = RecruitSession.GetCookie(Context);
            //dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2).r_regist_status = "step2";
            www6.selectQF2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2).r_regist_status = "step2";
        }
		Response.Redirect("/recruit/pool/apply/step2/" + depth1 + (depth2 == "" ? "" : "/" + depth2));
	}



	protected void Save()
    {
		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var resume = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2);
            var resume = www6.selectQF2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2);
            resume.r_txt_aboutme_2 = r_txt_aboutme_2.Value;
            //resume.r_txt_aboutme_3 = r_txt_aboutme_3.Value;


            resume.r_txt_aboutme_4 = lb_file_path.Value;
            resume.r_expire_mail = false;


            //dao.SubmitChanges();
            www6.update(resume);
            Response.Write("<script>alert('저장되었습니다.');</script>");
        }
	}
}