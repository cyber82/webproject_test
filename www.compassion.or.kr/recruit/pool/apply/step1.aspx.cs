﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using System.Data;
using System.Data.Linq;
using CommonLib;

public partial class recruit_apply_step1 : FrontBasePage {
	const string listPath = "/recruit/pool";

	public override bool RequireRecruitLogin{
		get{return true;}
	}

	
	public override bool RequireSSL {
		get {return true;}
	}

	List<recruit_jobcode> Jobcode
    {
		get{
			if (this.ViewState["recruit_jobcode"] == null)
            {
                CommonLib.WWW6Service.SoaHelperSoap _www6Service;
                _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

                using (AdminDataContext dao = new AdminDataContext())
                {
                    //this.ViewState["recruit_jobcode"] = dao.recruit_jobcode.Where(p => p.rj_display == true).ToJson();
                    string dbType = "Text";
                    string dbName = "SqlCompassionWeb";

                    Object[] objSql = new object[1] { "select * from recruit_jobcode where rj_display = 1" };
                    DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, null, null);
                    List<recruit_jobcode> entity = ds.Tables[0].DataTableToList<recruit_jobcode>();
                    this.ViewState["recruit_jobcode"] = entity.ToJson();
                }
			}
			return this.ViewState["recruit_jobcode"].ToString().ToObject<List<recruit_jobcode>>();
		}
		set{
			this.ViewState["recruit_jobcode"] = value.ToJson();
		}
	}
	

	protected string depth1{
		set {
			this.ViewState.Add("depth1", value);
		}
		get {
			if (this.ViewState["depth1"] == null) return "";
			return this.ViewState["depth1"].ToString();
		}
	}
	
	protected string depth2{
		set {
			this.ViewState.Add("depth2", value);
		}
		get {
			if (this.ViewState["depth2"] == null) return "";
			return this.ViewState["depth2"].ToString();
		}
	}



	protected override void OnBeforePostBack()
    {
		base.OnBeforePostBack();

		// check param
		var requests = Request.GetFriendlyUrlSegments();

		if (requests.Count > 0)
        {

			depth1 = requests[0];

			if (requests.Count > 1)
            {
				depth2 = requests[1];
			}

            CommonLib.WWW6Service.SoaHelperSoap _www6Service;
            _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

            using (FrontDataContext dao = new FrontDataContext())
            {
                string dbType = "Text";
                string dbName = "SqlCompassionWeb";

                Object[] objSql = new object[1] { "select * from recruit_jobcode where rj_depth1 = '"+ depth1 + "' and rj_depth2 = '"+ depth2 + "' and rj_display = 1" };
                DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, null, null);
                List<recruit_jobcode> entity = ds.Tables[0].DataTableToList<recruit_jobcode>();

                // recruit_jobcode검증
                //if (!dao.recruit_jobcode.Any(p => p.rj_depth1 == depth1 && p.rj_depth2 == depth2 && p.rj_display))
                if (entity == null)
                {
                    Response.Redirect(listPath, true);
                }

                // 지원분야
                //depth1_name.Text = dao.recruit_jobcode.First(p => p.rj_depth1 == depth1 && p.rj_depth2 == "").rj_name;
                Object[] objSql2 = new object[1] { "select * from recruit_jobcode where rj_depth1 = '" + depth1 + "' and rj_depth2 = ''"};
                DataSet ds2 = _www6Service.NTx_ExecuteQuery(dbName, objSql2, dbType, null, null);
                recruit_jobcode entity2 = ds2.Tables[0].Rows[0].DataTableToFirst<recruit_jobcode>();
                depth1_name.Text = entity2.rj_name;

                if (depth2 != "")
                {
                    //depth2_name.Text = dao.recruit_jobcode.First(p => p.rj_depth1 == depth1 && p.rj_depth2 == depth2).rj_name;
                    Object[] objSql3 = new object[1] { "select * from recruit_jobcode where rj_depth1 = '" + depth1 + "' and rj_depth2 = '" + depth2 + "'" };
                    DataSet ds3 = _www6Service.NTx_ExecuteQuery(dbName, objSql3, dbType, null, null);
                    recruit_jobcode entity3 = ds3.Tables[0].Rows[0].DataTableToFirst<recruit_jobcode>();
                    depth2_name.Text = entity3.rj_name;
                }
            }

		} else {
			Response.Redirect(listPath, true);
		}

		loadData();
	}

	protected void loadData()
    {
		// 셀렉트박스 초기화
		SetDropDownList();


		var cookie = RecruitSession.GetCookie(Context);
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var applicant = dao.applicant.First(p => p.ap_id == cookie.id);
            string dbType = "Text";
            string dbName = "SqlCompassionWeb";

            var applicant = www6.selectQF<applicant>("ap_id", cookie.id);

            r_name.Text = applicant.ap_name;
            r_email.Text = applicant.ap_email;


            if (applicant.ap_phone != null)
            {
                phone.Value = applicant.ap_phone;
            }

            var entity = www6.selectQ2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2);
            //if (dao.resume.Any(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2))
            if(entity.Any())
            {

                resume resume = entity.First();
                
                upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.recruit, resume.r_id.ToString());

                // 기본 인적사항
                r_gender.Value = resume.r_gender;

                if (resume.r_birth != null)
                {
                    var r_birth = resume.r_birth.Split('-');
                    birth_year.Value = r_birth[0];
                    birth_month.Value = r_birth[1];
                    birth_day.Value = r_birth[2];
                }

                r_name_en.Value = resume.r_name_en;
                r_familyname_en.Value = resume.r_familyname_en;

                r_addr.Value = resume.r_addr;

                if (resume.r_salary != null) r_salary.Value = resume.r_salary.ToString();
                if (resume.r_hope_salary != null) r_hope_salary.Value = resume.r_hope_salary.ToString();


                // 병역사항
                r_military_status.SelectedValue = resume.r_military_status;


                // 보훈사항
                if (resume.r_veterans_is != null)
                {
                    r_veterans_is.Value = (bool)resume.r_veterans_is ? "1" : "0";
                }
                r_veterans_no.Value = resume.r_veterans_no;


                // 장애사항
                r_disability_status.Value = resume.r_disability_status;
                r_disability_type.SelectedValue = resume.r_disability_type;
                r_disability_level.SelectedValue = resume.r_disability_level;

                // 출석교회 정보
                r_church_name.Value = resume.r_church_name;
                r_church_team.Value = resume.r_church_team;

                if (resume.r_is_sponsor != null)
                {
                    r_is_sponsor.Value = (bool)resume.r_is_sponsor ? "1" : "0";
                }
                r_sponsor_remark.Value = resume.r_sponsor_remark;

                // 최종학력
                last_edu.Value = resume.r_education;


                //포트폴리오
                r_portfolio.Value = resume.r_portfolio;
                lb_file_path.Value = resume.r_file;


                // 학력사항
                //data_edu.Value = dao.resume_edu.Where(p => p.ru_r_id == resume.r_id).ToJson();
                var entity_edu = www6.selectQ<resume_edu>("ru_r_id", resume.r_id);
                if(entity_edu.Count > 0)
                {
                    data_edu.Value = entity_edu.ToJson();
                }


                // 어학능력
                //data_lang.Value = dao.resume_lang_exam.Where(p => p.rle_r_id == resume.r_id).ToJson();
                var entity_lang = www6.selectQ<resume_lang_exam>("rle_r_id", resume.r_id);
                if (entity_lang.Count > 0)
                {
                    data_lang.Value = entity_lang.ToJson();
                }

                // 자격증
                //data_certificate.Value = dao.resume_certificate.Where(p => p.rc_r_id == resume.r_id).ToJson();
                var entity_certi = www6.selectQ<resume_certificate>("rc_r_id", resume.r_id);
                if (entity_certi.Count > 0)
                {
                    data_certificate.Value = entity_certi.ToJson();
                }

                // 경력사항
                //data_work.Value = dao.resume_work.Where(p => p.rw_r_id == resume.r_id).ToJson();
                var entity_work = www6.selectQ<resume_work>("rw_r_id", resume.r_id);
                if (entity_work.Count > 0)
                {
                    data_work.Value = entity_work.ToJson();
                }

            }
            else
            {

                var resume = new resume()
                {
                    r_ap_id = cookie.id,
                    r_name = applicant.ap_name,
                    r_email = applicant.ap_email,
                    r_regist_status = "step1",
                    r_ip = Request.ServerVariables["REMOTE_ADDR"],
                    r_regdate = DateTime.Now,
                    r_moddate = DateTime.Now,
                    r_rj_depth1 = depth1,
                    r_rj_depth2 = depth2,
                    r_expire_mail = false
                };
                www6.insert(resume);
                //dao.resume.InsertOnSubmit(resume);
                //dao.SubmitChanges();
                //string insertQuery = string.Format(@"insert into resume  
                //                                    (
                //                                        'r_ap_id'
                //                                      , 'r_name'
                //                                      , 'r_email'
                //                                      , 'r_regist_status'
                //                                      , 'r_ip'
                //                                      , 'r_regdate'
                //                                      , 'r_moddate'
                //                                      , 'r_rj_depth1'
                //                                      , 'r_rj_depth2'
                //                                      , 'r_expire_mail'
                //                                    )
                //
                //                                    values
                //                                    (
                //                                      {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}
                //                                    )"
                //                                    , cookie.id
                //                                    , applicant.ap_name
                //                                    , applicant.ap_email
                //                                    , "step1"
                //                                    , Request.ServerVariables["REMOTE_ADDR"]
                //                                    , DateTime.Now
                //                                    , DateTime.Now
                //                                    , depth1
                //                                    , depth2
                //                                    , false);
                //
                //Object[] objSql_insert = new object[1] { insertQuery };
                //int ds_insert = _www6Service.Tx_ExecuteQuery(dbName, objSql_insert, dbType, null, null);

                upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.recruit, resume.r_id.ToString());
            }


        }
	}


	void SetDropDownList()
    {
		// 병역구분
		foreach (var a in Recruit.MilitaryStatus()) {
			r_military_status.Items.Add(new ListItem(a.Value, a.Key));
		}
		
		// 장애유형
		foreach (var a in Recruit.DisabilityType()) {
			r_disability_type.Items.Add(new ListItem(a.Value, a.Key));
		}
		// 장애등급
		foreach (var a in Recruit.DisabilityLevel()) {
			r_disability_level.Items.Add(new ListItem(a.Value, a.Key));
		}
	}



	// 임시저장
	protected void save_Click(object sender, EventArgs e) {
		Save();
	}

	// 저장 후 next
	protected void next_Click(object sender, EventArgs e)
    {
		Save();
        
        using (FrontDataContext dao = new FrontDataContext())
        {
            var cookie = RecruitSession.GetCookie(Context);
            //dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2).r_regist_status = "step2";
            www6.selectQF2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2).r_regist_status = "step2";
        }

		var next = "/recruit/pool/apply/step2/" + depth1 + (depth2 == "" ? "" : "/" + depth2);

		Response.Redirect(next);
	}

	protected void Save()
    {
        var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {


            var r_id = -1;
            //var applicant = dao.applicant.First(p => p.ap_id == cookie.id);
            var applicant = www6.selectQF<applicant>("ap_id", cookie.id);

            birth_month.Value = birth_month.Value.Length < 2 ? "0" + birth_month.Value : birth_month.Value;
            birth_day.Value = birth_day.Value.Length < 2 ? "0" + birth_day.Value : birth_day.Value;


            //var resume = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2);
            var resume = www6.selectQF2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2);
            // 기본 인적사항
            resume.r_name = applicant.ap_name;
            resume.r_email = applicant.ap_email;
            resume.r_name_en = r_name_en.Value;
            resume.r_familyname_en = r_familyname_en.Value;
            resume.r_gender = r_gender.Value;
            resume.r_birth = birth_year.Value + "-" + birth_month.Value + "-" + birth_day.Value;
            resume.r_addr = r_addr.Value;
            resume.r_phone = phone.Value;
            resume.r_salary = r_salary.Value;
            resume.r_hope_salary = r_hope_salary.Value;

            // 병역사항
            resume.r_military_status = r_military_status.SelectedValue;

            // 보훈대상
            resume.r_veterans_is = r_veterans_is.Value == "1";
            if (r_veterans_is.Value == "1")
            {
                resume.r_veterans_no = r_veterans_no.Value;
            }
            else
            {
                resume.r_veterans_no = "";
            }

            // 장애사항
            resume.r_disability_type = r_disability_type.SelectedValue;
            resume.r_disability_status = r_disability_status.Value;
            resume.r_disability_level = r_disability_level.SelectedValue;

            // 출석교회
            resume.r_church_name = r_church_name.Value;
            resume.r_church_team = r_church_team.Value;
            resume.r_is_sponsor = r_is_sponsor.Value == "1";
            resume.r_sponsor_remark = r_sponsor_remark.Value;


            // 최종학력(가장 늦은 졸업년도)
            resume.r_education = last_edu.Value;

            // 직무코드
            resume.r_rj_depth1 = depth1;
            resume.r_rj_depth2 = depth2;


            // 포트폴리오
            resume.r_portfolio = r_portfolio.Value;
            resume.r_file = lb_file_path.Value;

            if (resume.r_regist_status != "done")
            {
                resume.r_regist_status = "step1";
            }

            resume.r_expire_mail = false;

            r_id = resume.r_id;


            // 학력사항
            //dao.resume_edu.DeleteAllOnSubmit(dao.resume_edu.Where(p => p.ru_r_id == r_id));
            var resume_eduList = www6.selectQ<resume_edu>("ru_r_id", r_id);
            www6.delete(resume_eduList);

            foreach (var add in data_edu.Value.ToObject<List<resume_edu>>())
            {
                add.ru_r_id = r_id;
                //dao.resume_edu.InsertOnSubmit(add);
                www6.insert(add);
            }

            // 어학능력
            //dao.resume_lang_exam.DeleteAllOnSubmit(dao.resume_lang_exam.Where(p => p.rle_r_id == r_id));
            var resume_lang_examList = www6.selectQ<resume_lang_exam>("rle_r_id", r_id);
            www6.delete(resume_lang_examList);

            foreach (var add in data_lang.Value.ToObject<List<resume_lang_exam>>())
            {
                add.rle_r_id = r_id;
                //dao.resume_lang_exam.InsertOnSubmit(add);
                www6.insert(add);
            }

            // 자격 및 면허사항
            //dao.resume_certificate.DeleteAllOnSubmit(dao.resume_certificate.Where(p => p.rc_r_id == r_id));
            var resume_certificateList = www6.selectQ<resume_certificate>("rc_r_id", r_id);
            www6.delete(resume_certificateList);

            foreach (var add in data_certificate.Value.ToObject<List<resume_certificate>>())
            {
                add.rc_r_id = r_id;
                //dao.resume_certificate.InsertOnSubmit(add);
                www6.insert(add);
            }

            // 경력사항
            //dao.resume_work.DeleteAllOnSubmit(dao.resume_work.Where(p => p.rw_r_id == r_id));
            var resume_workList = www6.selectQ<resume_work>("rw_r_id", r_id);
            www6.delete(resume_workList);

            foreach (var add in data_work.Value.ToObject<List<resume_work>>())
            {
                add.rw_r_id = r_id;
                //dao.resume_work.InsertOnSubmit(add);
                www6.insert(add);
            }

            //dao.SubmitChanges();
            www6.update(resume);
            Response.Write("<script>alert('저장되었습니다.');</script>");



        }

	}
}