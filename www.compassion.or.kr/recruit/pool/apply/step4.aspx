﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="step4.aspx.cs" Inherits="recruit_apply_step4" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <style>
    </style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%" class="on"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->
				<div class="relative">
					<h2 class="sub_tit line">지원서 작성</h2>
				</div>

				<!-- 작성프로세스 -->
				<div class="apply_process">
					<ol>
						<li>
							<span class="wrap">
								<span class="step step1">STEP 01</span>
								<span class="txt">기본 이력사항</span>
							</span>
						</li>
						<li><span class="wrap">
								<span class="step step2">STEP 02</span>
								<span class="txt">자기소개/경력기술서</span>
							</span>
						</li>
						<li>
							<span class="wrap">
								<span class="step step3">STEP 03</span>
								<span class="txt">신앙간증문/추천서</span>
							</span>
						</li>
						<li>
							<span class="wrap">
								<span class="step step4 on">STEP 04</span>
								<span class="txt">미리보기 및 제출</span>
							</span>
						</li>
					</ol>
					<ul class="info">
						<li><span class="s_con1">30분 이내 ‘저장’하지 않으면, 세션이 종료되어 로그인 정보가 사라집니다. 30분마다 ‘임시저장’ 해주세요.</span></li>
						<li><span class="s_con1">상시지원에 등록해 주신 지원 정보는 최대 6개월까지 보관이 되며 이후 삭제될 예정입니다. 보관 기간 동안에는 지원 정보를 자유롭게 수정하실 수 있습니다.</span></li>
					</ul>
				</div>
				<!--// 작성프로세스 -->

				<!-- 기본인적사항 -->
				<p class="table_tit"><span class="tit">기본 인적사항</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>기본 인적사항 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:30%" />
							<col style="width:20%" />
							<col style="width:30%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">이름 (한글)</th>
								<td><asp:Literal ID="r_name" runat="server" /></td>
								<th scope="row">이름 (영문)</th>
								<td><asp:Literal ID="r_name_en" runat="server" /><asp:Literal ID="r_family_name_en" runat="server" /></td>
							</tr>
							<tr>
								<th scope="row">생년월일</th>
								<td colspan="3"><asp:Literal ID="r_birth" runat="server" />(<asp:Literal ID="age" runat="server" />세)</td>
							</tr>
							<tr>
								<th scope="row">성별</th>
								<td colspan="3"><asp:Literal ID="r_gender" runat="server" /></td>
							</tr>
							<tr>
								<th scope="row">지원분야</th>
								<td colspan="3">[<asp:Literal runat="server" ID="depth1_name" /> - <asp:Literal runat="server" ID="depth2_name" />]<asp:Literal runat="server" ID="b_title" /></td>
							</tr>
							<tr>
								<th scope="row">이메일</th>
								<td colspan="3"><asp:Literal runat="server" ID="r_email"/></td>
							</tr>
							<tr>
								<th scope="row">현 거주지 주소</th>
								<td colspan="3"><asp:Literal runat="server" ID="r_addr"/></td>
							</tr>
							<tr>
								<th scope="row">휴대폰</th>
								<td colspan="3"><asp:Literal runat="server" ID="r_phone"/></td>
							</tr>
							<tr>
								<th scope="row">현재연봉</th>
								<td><asp:Literal runat="server" ID="r_salary"/></td>
								<th scope="row">희망연봉</th>
								<td><asp:Literal runat="server" ID="r_hope_salary"/></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 기본인적사항 -->

				<!-- 병역사항 -->
				<p class="table_tit"><span class="tit">병역사항</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>병역사항 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">병역구분</th>
								<td><asp:Literal runat="server" ID="r_military_status"/></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 병역사항 -->

				<!-- 보훈사항 -->
				<p class="table_tit"><span class="tit">보훈사항</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>보훈사항 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">보훈여부</th>
								<td><asp:Literal runat="server" ID="r_veterans_is"/></td>
							</tr>
							<tr>
								<th scope="row">보훈번호</th>
								<td><asp:Literal runat="server" ID="r_veterans_no"/></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 보훈사항 -->

				<!-- 장애사항 -->
				<p class="table_tit"><span class="tit">장애사항</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>장애사항 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">장애여부</th>
								<td><asp:Literal runat="server" ID="r_disability_status"/></td>
							</tr>
							<tr>
								<th scope="row">장애유형</th>
								<td><asp:Literal runat="server" ID="r_disability_type"/></td>
							</tr>
							<tr>
								<th scope="row">장애등급</th>
								<td><asp:Literal runat="server" ID="r_disability_level"/></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 장애사항 -->

				<!-- 출석교회정보 -->
				<p class="table_tit"><span class="tit">출석교회 정보</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>출석교회 정보 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">출석교회명</th>
								<td><asp:Literal runat="server" ID="r_church_name"/></td>
							</tr>
							<tr>
								<th scope="row">소속교단명</th>
								<td><asp:Literal runat="server" ID="r_church_team"/></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 출석교회정보 -->

				<!-- 컴패션 후원여부 -->
				<p class="table_tit"><span class="tit">컴패션 후원여부</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>컴패션 후원여부 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">컴패션 후원여부</th>
								<td><asp:Literal runat="server" ID="r_is_sponsor"/></td>
							</tr>
							<tr>
								<td colspan="2">
									<p class="desc1 mb15">컴패션 후원과 관련된 특이사항이 있는 경우 자유롭게 기술해주세요.</p>
									<div class="box_type1 cols1" style="white-space:pre-wrap;"><asp:Literal runat="server" ID="r_sponsor_remark"/></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 컴패션 후원여부 -->

				<!-- 학력사항 -->
				<p class="table_tit"><span class="tit">학력사항</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type3">
						<caption>학력사항 테이블</caption>
						<colgroup>
							<col style="width:17%" />
							<col style="width:23%" />
							<col style="width:15%" />
							<col style="width:15%" />
							<col style="width:15%" />
							<col style="width:15%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col">학력구분</th>
								<th scope="col">학교명</th>
								<th scope="col">전공</th>
								<th scope="col">입학연월</th>
								<th scope="col">졸업연월</th>
								<th scope="col">소재지</th>
							</tr>
						</thead>
						<tbody>
							<asp:Repeater runat=server ID=repeater_edu >
								<ItemTemplate>
									<tr class="item">
										<td><%#Eval("ru_grade") %></td>
										<td><%#Eval("ru_name") %></td>
										<td><%#Eval("ru_major") %></td>
										<td><%#Eval("ru_date_begin") %></td>
										<td><%#Eval("ru_date_end") %></td>
										<td><%#Eval("ru_location") %></td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
									<tr runat="server" Visible="<%#repeater_edu.Items.Count == 0 %>">
										<td colspan="6">등록된 데이터가 존재하지 않습니다.</td>
									</tr>
								</FooterTemplate>
							</asp:Repeater>	
						</tbody>
					</table>
				</div>
				<!--// 학력사항 -->

				<!-- 어학능력 -->
				<p class="table_tit"><span class="tit">어학능력</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type3">
						<caption>어학능력 테이블</caption>
						<colgroup>
							<col style="width:18%" />
							<col style="width:50%" />
							<col style="width:16%" />
							<col style="width:16%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col">외국어명</th>
								<th scope="col">인증기관</th>
								<th scope="col">점수/등급</th>
								<th scope="col">취득일</th>
							</tr>
						</thead>
						<tbody>
							 <asp:Repeater runat=server ID=repeater_lang >
								<ItemTemplate>
									<tr class="item">
										<td><%#Eval("rle_name") %></td>
										<td><%#Eval("rle_organization") %></td>
										<td><%#Eval("rle_point") %></td>
										<td><%#Eval("rle_date") %></td>
									</tr>
									<tr class="item">
										<td colspan="4">
											<div class="box_type1 cols2" style="white-space:pre-wrap"><%#Eval("rle_comment") %></div>
										</td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
									<tr runat="server" Visible="<%#repeater_lang.Items.Count == 0 %>">
										<td colspan="4">등록된 데이터가 존재하지 않습니다.</td>
									</tr>
								</FooterTemplate>
							</asp:Repeater>	
						</tbody>
					</table>
				</div>
				<!--// 어학능력 -->

				<!-- 자격및면허사항 -->
				<p class="table_tit"><span class="tit">자격 및 면허사항</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type3">
						<caption>자격 및 면허사항 테이블</caption>
						<colgroup>
							<col style="width:43%" />
							<col style="width:25%" />
							<col style="width:16%" />
							<col style="width:16%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col">자격증명</th>
								<th scope="col">발급기관</th>
								<th scope="col">자격증번호</th>
								<th scope="col">합격일</th>
							</tr>
						</thead>
						<tbody>
							 <asp:Repeater runat=server ID=repeater_certificate >
								<ItemTemplate>
									<tr class="item">
										<td><%#Eval("rc_name") %></td>
										<td><%#Eval("rc_issue") %></td>
										<td><%#Eval("rc_no") %></td>
										<td><%#Eval("rc_date") %></td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
									<tr runat="server" Visible="<%#repeater_certificate.Items.Count == 0 %>">
										<td colspan="4">등록된 데이터가 존재하지 않습니다.</td>
									</tr>
								</FooterTemplate>
							</asp:Repeater>	
						</tbody>
					</table>
				</div>
				<!--// 자격및면허사항 -->

				<!-- 경력사항 -->
				<p class="table_tit"><span class="tit">경력사항</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type3">
						<caption>경력사항 테이블</caption>
						<colgroup>
							<col style="width:24%" />
							<col style="width:16%" />
							<col style="width:20%" />
							<col style="width:14%" />
							<col style="width:12%" />
							<col style="width:14%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col">회사명</th>
								<th scope="col">고용형태</th>
								<th scope="col">근무기간</th>
								<th scope="col">근무부서</th>
								<th scope="col">직급</th>
								<th scope="col">담당업무</th>
							</tr>
						</thead>
						<tbody>
							<asp:Repeater runat=server ID=repeater_work >
								<ItemTemplate>
									<tr class="item">
										<td><%#Eval("rw_name") %></td>
										<td><%#Eval("rw_status") %></td>
										<td><%#Eval("rw_date_begin") %>~<%#Eval("rw_date_end") %></td>
										<td><%#Eval("rw_role") %></td>
										<td><%#Eval("rw_position") %></td>
										<td><%#Eval("rw_work") %></td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
									<tr runat="server" Visible="<%#repeater_work.Items.Count == 0 %>">
										<td colspan="6">등록된 데이터가 존재하지 않습니다.</td>
									</tr>
								</FooterTemplate>
							</asp:Repeater>	
						</tbody>
					</table>
				</div>
				<!--// 경력사항 -->

				<!-- 포트폴리오 -->
				<p class="table_tit"><span class="tit">포트폴리오</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>포트폴리오 정보 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">포트폴리오</th>
								<td><asp:Literal runat="server" id="r_portfolio" /></td>
							</tr>
							<tr>
								<th scope="row">첨부파일</th>
								<td><button class="ref"><span><asp:Literal runat="server" id="r_file" /></span></button></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 포트폴리오 -->

				<!-- 자기소개 -->
				<p class="table_tit"><span class="tit">자기소개</span></p>
				<div class="box_type1 cols3 mb60" style="word-wrap:break-word;white-space:pre-wrap"><asp:Literal runat="server" ID="r_txt_aboutme_1"/></div>
				<!--// 자기소개 -->

				<!-- 경력기술서 -->
				<p class="table_tit"><span class="tit">경력기술서</span></p>
				<div class="box_type1 cols3 mb60" style="word-wrap:break-word;white-space:pre-wrap"><asp:Literal runat="server" ID="r_txt_career"/></div>
				<!--// 경력기술서 -->

				<!-- 신앙간증문 -->
				<p class="table_tit"><span class="tit">신앙간증문</span></p>
				<div class="box_type1 cols3 mb60" style="word-wrap:break-word;white-space:pre-wrap"><asp:Literal runat="server" ID="r_txt_aboutme_2"/></div>
				<!--// 신앙간증문 -->

				<!-- 추천서 -->
				<p class="table_tit"><span class="tit">추천서 첨부파일</span></p>
				<div class="tableWrap2 mb60">
					<table class="tbl_type2">
						<caption>추천서 첨부파일 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">첨부파일</th>
								<td><button class="ref"><span><asp:Literal runat="server" ID="r_txt_aboutme_4" /></span></button></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 추천서 -->

				<!-- 사인 -->
				<div class="box_type1 sign mb60">
					입사지원서상의 모든 기재사항이 사실과 다름이 없으며, 합격 후 또는 입사 후에라도 허위기재 사실이 확인되어 합격 또는 입사가 취소되더라도<br />
					이의를 제기하지 않을 것을 서약합니다.

					<span class="name">지원자<em class="em1"><asp:Literal ID="r_name2" runat="server" /></em> <em class="em2">(인)</em></span>
				</div>
				<!--// 사인 -->

				<div class="btn_ac">
					<div>
						<asp:LinkButton class="btn_type3 fl mr10" runat="server" ID="update" OnClick="update_Click" >이력서 수정</asp:LinkButton>
						<asp:LinkButton class="btn_type1 fl" runat="server" ID="apply" OnClick="apply_Click" >제출완료</asp:LinkButton>
					</div>
				</div>

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->
</asp:Content>
