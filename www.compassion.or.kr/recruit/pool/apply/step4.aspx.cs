﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using System.Data.Linq;
using System.Configuration;
using CommonLib;

public partial class recruit_apply_step4 : FrontBasePage {

	Uploader.FileGroup file_group = Uploader.FileGroup.recruit;

	public override bool RequireRecruitLogin{
		get{return true;}
	}

	
	public override bool RequireSSL {
		get {return true;}
	}
	
	protected string depth1{
		set {
			this.ViewState.Add("depth1", value);
		}
		get {
			if (this.ViewState["depth1"] == null) return "";
			return this.ViewState["depth1"].ToString();
		}
	}
	
	protected string depth2{
		set {
			this.ViewState.Add("depth2", value);
		}
		get {
			if (this.ViewState["depth2"] == null) return "";
			return this.ViewState["depth2"].ToString();
		}
	}

	resume ResumeData	{
		get {
			if (this.ViewState["resume"] == null)
				this.ViewState["resume"] = new resume().ToJson();
			return this.ViewState["resume"].ToString().ToObject<resume>();
		}
		set	{
			this.ViewState["resume"] = value.ToJson();
		}
	}


	const string listPath = "/recruit/pool/";


	protected override void OnBeforePostBack()
    {
		base.OnBeforePostBack();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
        
		// 인재풀에서 넘어오면 키가 없음
		if (requests.Count > 0)
        {
			if (requests.Count > 0)
            {
				depth1 = requests[0];

				if (requests.Count > 1)
                {
					depth2 = requests[1];
				}

				ValidateParam();
			}
            else
            {
				Response.Redirect(listPath, true);
			}

            using (FrontDataContext dao = new FrontDataContext())
            {
                // 지원분야
                //depth1_name.Text = dao.recruit_jobcode.First(p => p.rj_depth1 == depth1 && p.rj_depth2 == "").rj_name;
                depth1_name.Text = www6.selectQF<recruit_jobcode>("rj_depth1", depth1, "rj_depth2", "").rj_name;
                if (depth2 != "")
                {
                    //depth2_name.Text = dao.recruit_jobcode.First(p => p.rj_depth1 == depth1 && p.rj_depth2 == depth2).rj_name;
                    depth2_name.Text = www6.selectQF<recruit_jobcode>("rj_depth1", depth1, "rj_depth2", "").rj_name;
                }
            }
		}
        else
        {
			Response.Redirect(listPath, true);
		}
		loadData();
	}


	protected void ValidateParam() {
		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            // recruit_jobcode검증
            var exist = www6.selectQ<recruit_jobcode>("rj_depth1", depth1, "rj_depth2", depth2);
            //if (!dao.recruit_jobcode.Any(p => p.rj_depth1 == depth1 && p.rj_depth2 == depth2))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            // 지원서 검증
            var exist2 = www6.selectQ<resume>("r_ap_id", cookie.id, "r_rj_depth1", depth1, "r_rj_depth2", depth2);
            //if (!dao.resume.Any(p => p.r_ap_id == cookie.id && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2))
            if(!exist2.Any())
            {
                Response.Redirect(listPath, true);
            }
        }
	}

	protected void loadData()
    {
		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //ResumeData = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2);
            ResumeData = www6.selectQF2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2);

            // 기본 인적사항
            r_name.Text = ResumeData.r_name;
            r_name2.Text = ResumeData.r_name;
            r_name_en.Text = ResumeData.r_name_en;
            r_family_name_en.Text = ResumeData.r_familyname_en;
            r_gender.Text = ResumeData.r_gender + "자";


            var birthday = Convert.ToDateTime(ResumeData.r_birth);
            r_birth.Text = birthday.ToString("yyyy년 M월 d일");
            age.Text = (DateTime.Now.Year - int.Parse(ResumeData.r_birth.Substring(0, 4))).ToString();



            r_email.Text = ResumeData.r_email;
            r_addr.Text = ResumeData.r_addr;


            r_phone.Text = ResumeData.r_phone;


            r_salary.Text = ResumeData.r_salary;
            r_hope_salary.Text = ResumeData.r_hope_salary;


            // 병역사항
            r_military_status.Text = ResumeData.r_military_status;


            // 보훈사항
            if (ResumeData.r_veterans_is != null)
            {
                r_veterans_is.Text = (bool)ResumeData.r_veterans_is ? "대상" : "비대상";
            }
            r_veterans_no.Text = ResumeData.r_veterans_no;


            // 장애사항
            r_disability_status.Text = ResumeData.r_disability_status;
            r_disability_type.Text = ResumeData.r_disability_type;
            r_disability_level.Text = ResumeData.r_disability_level;

            // 출석교회 정보
            r_church_name.Text = ResumeData.r_church_name;
            r_church_team.Text = ResumeData.r_church_team;

            if (ResumeData.r_is_sponsor != null)
            {
                r_is_sponsor.Text = (bool)ResumeData.r_is_sponsor ? "예" : "아니요";
            }
            //r_sponsor_remark.Text = ResumeData.r_disability_no; 진성 수정 
            r_sponsor_remark.Text = ResumeData.r_sponsor_remark;

            // 포트폴리오
            r_portfolio.Text = ResumeData.r_portfolio == "" ? "등록된 데이터가 존재하지 않습니다. " : ResumeData.r_portfolio;
            //r_file.Text = ResumeData.r_file;
            r_file.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, ResumeData.r_id.ToString()), ResumeData.r_file, ResumeData.r_id, ResumeData.r_file);

            // 학력사항
            //repeater_edu.DataSource = dao.resume_edu.Where(p => p.ru_r_id == ResumeData.r_id).ToList();
            repeater_edu.DataSource = www6.selectQ<resume_edu>("ru_r_id", ResumeData.r_id);
            repeater_edu.DataBind();
            
            // 어학능력
            //repeater_lang.DataSource = dao.resume_lang_exam.Where(p => p.rle_r_id == ResumeData.r_id).ToList();
            repeater_lang.DataSource = www6.selectQ<resume_lang_exam>("rle_r_id", ResumeData.r_id);
            repeater_lang.DataBind();

            // 자격증
            //repeater_certificate.DataSource = dao.resume_certificate.Where(p => p.rc_r_id == ResumeData.r_id).ToList();
            repeater_certificate.DataSource = www6.selectQ<resume_certificate>("rc_r_id", ResumeData.r_id);
            repeater_certificate.DataBind();

            // 경력사항
            //repeater_work.DataSource = dao.resume_work.Where(p => p.rw_r_id == ResumeData.r_id).ToList();
            repeater_work.DataSource = www6.selectQ<resume_work>("rw_r_id", ResumeData.r_id);
            repeater_work.DataBind();

            // 경력참고인
            //repeater_testifier.DataSource = dao.resume_testifier.Where(p => p.rt_r_id == ResumeData.r_id).ToList();
            //repeater_testifier.DataBind();


            r_txt_aboutme_1.Text = ResumeData.r_txt_aboutme_1;
            r_txt_aboutme_2.Text = ResumeData.r_txt_aboutme_2;
            //r_txt_aboutme_3.Text = ResumeData.r_txt_aboutme_3;
            r_txt_career.Text = ResumeData.r_txt_career;

            r_txt_aboutme_4.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, ResumeData.r_id.ToString()), ResumeData.r_txt_aboutme_4, ResumeData.r_id, ResumeData.r_txt_aboutme_4);
            /*
			if (dao.file.Any(p => p.f_group == file_group.ToString() && p.f_group2 == Recruit.FileGroup2.recommend.ToString() && p.f_ref_id == cookie.id.ToString())) {
				var f = dao.file.First(p => p.f_group == file_group.ToString() && p.f_group2 == Recruit.FileGroup2.recommend.ToString() && p.f_ref_id == cookie.id.ToString());
				lbRecommend_file.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, cookie.id.ToString()), f.f_name, cookie.id, f.f_display_name);
			}
			*/

        }
	}


	// 이력서 수정
	protected void update_Click(object sender, EventArgs e)
    {
		var cookie = RecruitSession.GetCookie(Context);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2).r_regist_status = "step1";
            www6.selectQF2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2).r_regist_status = "step1";
        }
		Response.Redirect("/recruit/pool/apply/step1/" + PrimaryKey);
	}


	// 입사지원
	protected void apply_Click(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var cookie = RecruitSession.GetCookie(Context);

            // recruit_jobcode검증
            var exist = www6.selectQ<recruit_jobcode>("rj_depth1", depth1, "rj_depth2", depth2, "rj_display", 1);
            //if (!dao.recruit_jobcode.Any(p => p.rj_depth1 == depth1 && p.rj_depth2 == depth2 && p.rj_display))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var resume = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2);
            var resume = www6.selectQF2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2);
            resume.r_regist_status = "done";
            resume.r_process_status = "doc1";
            resume.r_result = "standby";
            resume.r_expire_mail = false;

            //dao.SubmitChanges();
            www6.update(resume);

            Response.Write("<script>alert('지원이 완료되었습니다.'); location.href='/recruit/pool/apply/done'</script>");
            SendMail(resume.r_email, resume.r_name);
        }
		Response.Redirect("/recruit/pool/apply/done/");
	}



	void SendMail(string to, string name) {

		try {

			var from = ConfigurationManager.AppSettings["recruitEmailSender"];

			var args = new Dictionary<string, string> {
				{"{name}" , name }
			};
			var title = "[한국컴패션_인재채용] 한국컴패션에 지원해 주셔서 감사합니다.";



			Email.Send(this.Context, from, new List<string>() { to },
				title, "/mail/recruit_apply.html",
				args
			, null);

		} catch (Exception e) {
			Response.Write(e.Message);
			throw e;
		}

	}
}