﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class recruit_poll_default : FrontBasePage {


	List<recruit_jobcode> Jobcode
	{
		get
		{
			if (this.ViewState["recruit_jobcode"] == null) {

                using (AdminDataContext dao = new AdminDataContext())
                {
                    //this.ViewState["recruit_jobcode"] = dao.recruit_jobcode.Where(p => p.rj_display == true).ToJson();
                    this.ViewState["recruit_jobcode"] = www6.selectQ<recruit_jobcode>("rj_display", 1).ToJson();
                }
			}
			return this.ViewState["recruit_jobcode"].ToString().ToObject<List<recruit_jobcode>>();
		}
		set
		{
			this.ViewState["recruit_jobcode"] = value.ToJson();
		}
	}


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		bj_rj_depth1.Items.Add(new ListItem("부문선택", ""));
		foreach (var a in this.Jobcode.Where(p => p.rj_depth2 == "").OrderBy(p => p.rj_order)) {
			bj_rj_depth1.Items.Add(new ListItem(a.rj_name, a.rj_depth1));
		}

	}

	protected void btnRegist_Click(object sender, EventArgs e)
    {
		var mail = email.Text.EmptyIfNull();
		var pass = password.Text.EmptyIfNull();
		var depth1 = bj_rj_depth1.Value.EmptyIfNull();
		var depth2 = hidden_depth2.Value.EmptyIfNull();

		if (mail == "" || pass == "" || depth1 == "") {
			return;
		}

        using (FrontDataContext dao = new FrontDataContext())
        {
            var entity = www6.selectQF<applicant>("ap_email", mail);
            //if (dao.applicant.Any(p => p.ap_email == mail))
            if(entity != null)
            {
                //var entity = dao.applicant.First(p => p.ap_email == mail);
                //var entity = www6.selectQF<applicant>("ap_email", mail);

                if (entity.ap_pwd == pass.SHA256Hash())
                {

                    // 이메일 인증여부 확인
                    var exist = www6.selectQ2<applicant_email_certification>("rec_ap_id = ", entity.ap_id, "rec_confirm_date ", "IS NOT NULL");
                    //if (dao.applicant_email_certification.Any(p => p.rec_ap_id == entity.ap_id && p.rec_confirm_date != null))
                   if(exist.Any())
                    {
                        var loginEntity = new RecruitSession.Entity()
                        {
                            id = entity.ap_id,
                            name = entity.ap_name,
                            email = entity.ap_email
                        };

                        RecruitSession.SetCookie(Context, loginEntity);
                        var cookie = RecruitSession.GetCookie(Context);

                        var step = "step1";

                        var exist2 = www6.selectQ2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2);
                        //if (dao.resume.Any(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2))
                        if(exist2.Any())
                        {
                            //var resume = dao.resume.First(p => p.r_ap_id == cookie.id && p.r_b_id == null && p.r_rj_depth1 == depth1 && p.r_rj_depth2 == depth2);
                            var resume = www6.selectQF2<resume>("r_ap_id = ", cookie.id, "r_b_id ", "IS NULL", "r_rj_depth1 = ", depth1, "r_rj_depth2 = ", depth2);

                            step = resume.r_regist_status;
                            if (step == "done") step = "step1";

                            base.AlertWithJavascript("해당 공고에 " + resume.r_regist_status == "done" ? "제출 완료한 " : "작성 중이던 " + "지원서가 있습니다. 수정하시겠습니까?", "location.href='/recruit/pool/apply/step1/" + depth1 + (depth2 == "" ? "" : "/" + depth2) + "'");
                        }
                        else
                        {
                            Response.Redirect("/recruit/pool/apply/step1/" + depth1 + (depth2 == "" ? "" : "/" + depth2));
                        }

                    }
                    else
                    {
                        //Response.Write("<script>alert('이메일 인증이 완료되지 않았습니다.');</script>");
                        msg_email.Style["display"] = "block";
                        msg_email_txt.InnerHtml = "이메일 인증이 완료되지 않았습니다.";
                    }

                }
                else
                {
                    //Response.Write("<script>alert('이메일 또는 비밀번호가 올바르지 않습니다. 다시 확인해 주세요.');</script>");
                    msg_pwd.Style["display"] = "block";
                    msg_pwd_txt.InnerHtml = "이메일 또는 비밀번호가 올바르지 않습니다. 다시 확인해 주세요.";
                }
            }
            else
            {
                //Response.Write("<script>alert('이메일 또는 비밀번호가 올바르지 않습니다. 다시 확인해 주세요.');</script>");
                msg_pwd.Style["display"] = "block";
                msg_pwd_txt.InnerHtml = "이메일 또는 비밀번호가 올바르지 않습니다. 다시 확인해 주세요.";
            }
        }

	}
}