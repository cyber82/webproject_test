﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class recruit_default : FrontBasePage {


	protected override void OnBeforePostBack() {

		base.Hash = "c";
		base.OnBeforePostBack();
		paging.Hash = base.Hash;
		base.LoadComplete += new EventHandler(list_LoadComplete);
	}


	protected override void OnAfterPostBack() {

	}

	protected override void GetList(int page) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.sp_recruit_list_f(page, paging.RowsPerPage, "", "").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "rj_depth1", "rj_depth2" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, "", "" };
            var list = www6.selectSP("sp_recruit_list_f", op1, op2).DataTableToList<sp_recruit_list_fResult>();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            //lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }
	}
	


	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {

				sp_recruit_list_fResult entity = e.Item.DataItem as sp_recruit_list_fResult;
				((Literal)e.Item.FindControl("lbDateEnd")).Text = entity.b_timeless ? "채용시까지" : entity.b_date_end.ToString("yyyy.MM.dd");
				/*
				var status = "<span class='status ing'>진행중</span>";
				if (entity.b_completed) {
					status = "<span class='status result'>결과<br/>발표</span>";
				} else if (entity.b_date_end.CompareTo(DateTime.Now) < 1) {
					status = "<span class='status end'>마감</span>";
				}

				((Literal)e.Item.FindControl("lbStatus")).Text = status;
				*/
				((Literal)e.Item.FindControl("lbStatus")).Text = !entity.b_completed && entity.b_date_end.CompareTo(DateTime.Now) > -1 ? "<span class='status ing'>진행중</span>" : "<span class='status end'>마감</span>";
			}
		}
	}
}