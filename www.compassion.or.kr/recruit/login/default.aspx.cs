﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class recruit_login_default : FrontBasePage {



    protected string id
    {
        set
        {
            this.ViewState.Add("id", value);
        }
        get
        {
            if(this.ViewState["id"] == null)
                return "";
            return this.ViewState["id"].ToString();
        }
    }


    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();


		/*
		if (RecruitSession.HasCookie(Context)) {
			Response.Redirect("/recruit/");
			return;
		}
		*/

		id = Request["id"];
		if (id.EmptyIfNull() == "") {
			Response.Redirect("/recruit/");
		}

        btnFind.HRef = "/recruit/login/identify?id=" + id;
        btnRegist.HRef = "/recruit/signup/?id=" + id;
    }

    protected void btnLogin_Click( object sender, EventArgs e ) {
        var mail = email.Text.EmptyIfNull();
        var pass = password.Text.EmptyIfNull();
        var depth1 = password.Text.EmptyIfNull();
        var depth2 = password.Text.EmptyIfNull();


        if(mail == "" || pass == "") {
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            // 이메일 확인
            var exist = www6.selectQ<applicant>("ap_email", mail);
            //if (!dao.applicant.Any(p => p.ap_email == mail))
            if(!exist.Any())
            {
                //Response.Write("<script>alert('이메일 또는 비밀번호가 올바르지 않습니다. 다시 확인해 주세요.');</script>");
                msg_pwd.Style["display"] = "block";
                msg_pwd_txt.InnerHtml = "이메일 또는 비밀번호가 올바르지 않습니다. 다시 확인해 주세요.";
                return;
            }

            //var entity = dao.applicant.First(p => p.ap_email == mail);
            var entity = www6.selectQF<applicant>("ap_email", mail);

            // 비밀번호 확인
            if (entity.ap_pwd == pass.SHA256Hash())
            {

                // 이메일 인증여부 확인
                var exist2 = www6.selectQ2<applicant_email_certification>("rec_ap_id = ", entity.ap_id, "rec_confirm_date", "IS NOT NULL");
                //if (dao.applicant_email_certification.Any(p => p.rec_ap_id == entity.ap_id && p.rec_confirm_date != null))
                if(exist2.Any())
                {
                    var loginEntity = new RecruitSession.Entity()
                    {
                        id = entity.ap_id,
                        name = entity.ap_name,
                        email = entity.ap_email,
                        bj_id = Convert.ToInt32(id)
                    };

                    RecruitSession.SetCookie(Context, loginEntity);

                    if (id == "")
                    {
                    }
                    else
                    {
                        var cookie = RecruitSession.GetCookie(Context);
                        var step = "step1";

                        //var recruitJob = dao.recruit_job.First(p => p.bj_id == Convert.ToInt32(id));
                        var recruitJob = www6.selectQF<recruit_job>("bj_id", Convert.ToInt32(id));
                        //var recruit = dao.recruit.First(p => p.b_id == recruitJob.bj_b_id);
                        var recruit = www6.selectQF<recruit>("b_id", recruitJob.bj_b_id);

                        var exist3 = www6.selectQ<resume>("r_b_id", recruit.b_id, "r_ap_id", cookie.id);
                        //if (dao.resume.Any(p => p.r_b_id == recruit.b_id && p.r_ap_id == cookie.id))
                        if(exist3.Any())
                        {
                            //var resume = dao.resume.First(p => p.r_b_id == recruit.b_id && p.r_ap_id == cookie.id);
                            var resume = www6.selectQF<resume>("r_b_id", recruit.b_id, "r_ap_id", cookie.id);
                            step = resume.r_regist_status;
                            if (step == "done") step = "step1";

                            base.AlertWithJavascript("해당 공고에 " + resume.r_regist_status == "done" ? "제출 완료한 " : "작성 중이던 " + "지원서가 있습니다. 수정하시겠습니까?", "location.href='/recruit/apply/" + step + "/" + id + "'");

                        }
                        else
                        {
                            Response.Redirect("/recruit/apply/step1/" + id);
                        }

                    }

                }
                else
                {
                    //Response.Write("<script>alert('이메일 인증이 완료되지 않았습니다.');</script>");
                    msg_email.Style["display"] = "block";
                    msg_email_txt.InnerHtml = "이메일 인증이 완료되지 않았습니다.";
                    return;
                }

            }
            else
            {
                //Response.Write("<script>alert('이메일 또는 비밀번호가 올바르지 않습니다. 다시 확인해 주세요.');</script>");
                msg_pwd.Style["display"] = "block";
                msg_pwd_txt.InnerHtml = "이메일 또는 비밀번호가 올바르지 않습니다. 다시 확인해 주세요.";
                return;
            }
        }
    }
}