﻿var validate = {
	email: function (value) {
		if (value == "") {
			$("#email_validation_reuslt").text("정보를 입력해 주세요.");
			$("#email_validation_reuslt").parent().show();
			return false;
		}
		if (!emailValidation.check(value).result) {
			$("#email_validation_reuslt").text("유효한 이메일 주소가 아닙니다.");
			$("#email_validation_reuslt").parent().show();
			return false;
		}
		$("#email_validation_reuslt").text("유효한 이메일 주소입니다.");
		$("#email_validation_reuslt").parent().hide();
		return true;
	},

	name: function (value) {
		if (value == "") {
			$("#name_validation_reuslt").text("정보를 입력해 주세요.");
			$("#name_validation_reuslt").parent().show();
			return false;
		}

		// 한글 영문만 가능
		if (!/^[가-힣a-zA-Z]+$/.test(value)) {
			$("#name_validation_reuslt").text("이름을 정확히 입력해주세요.");
			$("#name_validation_reuslt").parent().show();
			return false;
		}

		$("#name_validation_reuslt").text("");
		$("#name_validation_reuslt").parent().hide();
		return true;
	},

	phone: function (value) {

		if (value == "") {
			$("#phone_validation_reuslt").text("정보를 입력해 주세요.");
			$("#phone_validation_reuslt").parent().show();
			return false;
		}

		if (!/(01[016789])[-](\d{4}|\d{3})[-]\d{4}$/g.test(value)) {
			$("#phone_validation_reuslt").text("휴대폰 번호 양식이 맞지 않습니다. 10자 이상의 숫자만 입력 가능합니다.");
			$("#phone_validation_reuslt").parent().show();
			return false;
		}

		$("#phone_validation_reuslt").text("사용 가능한 번호입니다.");
		$("#phone_validation_reuslt").parent().hide();
		return true;
	}

}


$(function () {

	$("#phone").mobileFormat();


	$("#email").focusout(function () {
		validate.email($(this).val());
	});

	$("#name").focusout(function () {
		validate.name($(this).val());
	});

	$("#phone").focusout(function () {
		validate.phone($(this).val());
	});

});



var goSubmit = function () {

	var result = true;

	if (!validate.email($("#email").val())) result = false;
	if(!validate.name($("#name").val())) result = false;
	if(!validate.phone($("#phone").val())) result = false;

	if (!result) {
		return false;
	}

	return true

}