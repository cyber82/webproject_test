﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="identify.aspx.cs" Inherits="recruit_login_identify" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script src="/assets/jquery/wisekit/validation/email.js"></script>
    
    <script src="identify.js"></script>
    <script type="text/javascript">
        $(function () {
        });

    </script>
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->

				<h2 class="sub_tit">비밀번호 찾기</h2>

				<div class="box_con1">
					<div class="field">
						<label class="tit" for="name">이름</label>
						<asp:TextBox runat="server" ID="name" value=""/>
					</div>
					<p class="comt1" style="display:none;"><span class="guide_comment2" id="name_validation_reuslt"></span></p>
					<div class="field">
						<label class="tit" for="email">이메일</label>
						<asp:TextBox runat="server" ID="email" placeholder="지원자 등록 시 사용했던 이메일을 입력해 주세요."/>
					</div>
					<p class="comt1" style="display:none;"><span class="guide_comment2" id="email_validation_reuslt"></span></p>
					<div class="field">
						<label class="tit" for="phone">휴대폰</label>
						 <asp:TextBox runat="server" ID="phone" placeholder="예 : 010-1234-5678"/>
					</div>
					<p class="comt1"  style="display:none;"><span class="guide_comment2" id="phone_validation_reuslt"></span></p>

					<p class="check_pw">등록 시 사용한 정보를 입력하시면, 해당 이메일로 임시 비밀번호가 발송됩니다.</p>
					<asp:LinkButton class="btn_b_type4" runat="server" ID="btnFind" OnClick="btnFind_Click" OnClientClick="return goSubmit()">확인</asp:LinkButton>
				</div>
				
			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->



    
    
</asp:Content>