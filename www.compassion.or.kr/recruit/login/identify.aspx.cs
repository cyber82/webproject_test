﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;


public partial class recruit_login_identify : FrontBasePage {



	protected string id{
		set{
			if (!string.IsNullOrEmpty(value)) {
				this.ViewState.Add("id", value);
			}
		}
		get{
			if (this.ViewState["id"] == null)
				return "";
			return this.ViewState["id"].ToString();
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		/*
		if (RecruitSession.HasCookie(Context)) {
			Response.Redirect("/recruit/");
			return;
		}
		*/

		id = Request["id"];

	}

	public string GetRandomString(int length) {
		Random rnd = new Random();
		string charPool = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

		StringBuilder rs = new StringBuilder();

		while (length-- > 0)
			rs.Append(charPool[(int)(rnd.NextDouble() * charPool.Length)]);

		return rs.ToString();
	}

	void SendMail(string to, string name, string code) {

		try {

			var args = new Dictionary<string, string> {
				{"{name}" , name } , {"{pwd}" , code }, {"{email}", to }
			};
			var title = "[한국컴패션_인재채용] 임시 비밀번호를 발급하였습니다.";


			var from = ConfigurationManager.AppSettings["recruitEmailSender"];

			Email.Send(this.Context, from, new List<string>() { to },
				title, "/mail/recruit_temp_pwd.html",
				args
			, null);

		} catch (Exception e) {
			ErrorLog.Write(this.Context, 0, e.Message);
			throw e;
		}

	}



	protected void btnFind_Click(object sender, EventArgs e) {
		var ap_name = name.Text.EmptyIfNull();
		var ap_email = email.Text;
		var ap_phone = phone.Text;


		if (ap_name == "" || ap_email == "" || ap_phone == "") {
			return;
		}

        using (FrontDataContext dao = new FrontDataContext())
        {
            // 정보 확인
            var exist = www6.selectQ<applicant>("ap_name", ap_name, "ap_email", ap_email, "ap_phone", ap_phone);
            //if (!dao.applicant.Any(p => p.ap_name == ap_name && p.ap_email == ap_email && p.ap_phone == ap_phone))
            if(!exist.Any())
            {
                Response.Write("<script>alert('입력한 정보와 일치하는 정보가 없습니다.\\n다시 한번 확인해주세요');</script>");
                return;
            }

            //var entity = dao.applicant.First(p => p.ap_email == ap_email && p.ap_name == ap_name && p.ap_phone == ap_phone);
            var entity = www6.selectQF<applicant>("ap_name", ap_name, "ap_email", ap_email, "ap_phone", ap_phone);

            // 임시비밀번호 발급
            string temp_pass = GetRandomString(6);

            entity.ap_pwd = temp_pass.SHA256Hash();
            //dao.SubmitChanges();
            www6.update(entity);

            SendMail(entity.ap_email, entity.ap_name, temp_pass);
            Response.Write("<script>alert('임시 비밀번호가 발급되었습니다.');location.href='/recruit/login/recover/?email=" + ap_email + "'</script>");
            //Response.Write(ap_email + "<br/>");
            //Response.Write(ap_phone + "<br/>");


        }
	}
}