﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;


public partial class recruit_login_recover : FrontBasePage {


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var requestMail = Request["email"].EmptyIfNull();

		if (requestMail == "") {
			Response.Write("<script>alert('올바른 접근이 아닙니다.');</script>");
			return;
		}
		email.Value = Server.UrlDecode(requestMail);
	}


	public string GetRandomString(int length) {
		Random rnd = new Random();
		string charPool = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

		StringBuilder rs = new StringBuilder();

		while (length-- > 0)
			rs.Append(charPool[(int)(rnd.NextDouble() * charPool.Length)]);

		return rs.ToString();
	}

	void SendMail(string to, string name, string code) {

		try {

			var from = ConfigurationManager.AppSettings["recruitEmailSender"];

			var args = new Dictionary<string, string> {
				{"{name}" , name } , {"{pwd}" , code }
			};
			var title = "[한국컴패션_인재채용] 임시 비밀번호를 발급하였습니다.";



			Email.Send(this.Context, from, new List<string>() { to },
				title, "/mail/recruit_temp_pwd.html",
				args
			, null);

		} catch (Exception e) {
			Response.Write(e.Message);
			throw e;
		}

	}



	protected void btnSubmit_Click(object sender, EventArgs e) {

		var ap_email = email.Value.EmptyIfNull();
		var temp_pass = temp_password.Text.EmptyIfNull();
		var pass = password.Text.EmptyIfNull();


		if (ap_email == "" || temp_pass == "" || pass == "") {
			return;
		}

        using (FrontDataContext dao = new FrontDataContext())
        {
            // 이메일 확인
            var exist = www6.selectQ<applicant>("ap_email", ap_email);
            //if (!dao.applicant.Any(p => p.ap_email == ap_email))
            if(!exist.Any())
            {
                Response.Write("<script>alert('이메일 주소 혹은 비밀번호가 맞지 않습니다.\\n다시 확인해 주세요.');</script>");
                return;
            }

            //var entity = dao.applicant.First(p => p.ap_email == ap_email);
            var entity = www6.selectQF<applicant>("ap_email", ap_email);

            // 비밀번호 확인
            if (entity.ap_pwd == temp_pass.SHA256Hash())
            {
                entity.ap_pwd = pass.SHA256Hash();

                //dao.SubmitChanges();
                www6.update(entity);

                Response.Write("<script>alert('비밀번호가 변경되었습니다.');location.href='/recruit/'</script>");
            }
            else
            {

                Response.Write("<script>alert('이메일 주소 혹은 비밀번호가 맞지 않습니다.\\n다시 확인해 주세요.');</script>");
            }

        }
	}
}