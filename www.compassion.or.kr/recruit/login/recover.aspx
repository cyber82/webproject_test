﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="recover.aspx.cs" Inherits="recruit_login_recover" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js"></script>
    <script type="text/javascript">
        $(function () {
        });

        var goSubmit = function () {


            if (!validateForm([
				{ id: "#email", msg: "이메일을 입력하세요" },
				{ id: "#temp_password", msg: "이메일로 전송 된 임시 비밀번호를 입력하세요." },
				{ id: "#password", msg: "비밀번호는 띄어쓰기 없이 영문, 숫자, 특수문자 중 2가지 이상 조합으로 5~15자 이내로 입력해 주세요." },
				{ id: "#re_password", msg: "입력한 비밀번호를 다시 한 번 입력해 주세요." }

            ])) {
                return false;
            }

            if ($("#password").val() != $("#re_password").val()) {
                alert("신규 비밀번호와 비밀번호 확인이 다릅니다.");
                return false;
            }

            var validCnt = 0;
            var value = $("#password").val();

            // 공백여부
            if (/[\s]/g.test(value) == true) {
                alert('비밀번호는 띄어쓰기 없이 영문, 숫자, 특수문자 중 2가지 이상 조합으로 5~15자 이내로 입력해 주세요.');
                return false;
            }

            // 영문 포함여부 확인
            if (value.match(/[a-zA-Z]+/g)) {
                validCnt++;
            }

            // 숫자 포함여부
            if (value.match(/[0-9]+/g)) {
                validCnt++;
            }

            // 특문 포함여부
            if (/[~!@\#$%<>^&*\()\-=+_\’]/gi.test(value)) {
                validCnt++;
            }
            if (validCnt < 2) {
                alert("비밀번호는 띄어쓰기 없이 영문, 숫자, 특수문자 중 2가지 이상 조합으로 5~15자 이내(대소문자 구별)로 입력해주세요.");
                return false;
            }

        }
    </script>
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" name="email" id="email" runat="server" />
	 <!-- sub body -->
	<section class="sub_body">
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->

				<h2 class="sub_tit">비밀번호 변경</h2>

				<div class="box_con1">
					<div class="field w2">
						<label class="tit" for="temp_password">임시 비밀번호</label>
						<asp:TextBox runat="server" TextMode="Password" id="temp_password" placeholder="이메일로 전송된 임시 비밀번호를 입력하세요." />
					</div>
					<div class="field w2">
						<label class="tit" for="password">신규 비밀번호</label>
						<asp:TextBox runat="server" TextMode="Password" id="password" placeholder="띄어쓰기 없이 영문,숫자,특수문자 2가지 이상 조합으로 5-15자." />
					</div>
					<div class="field w2">
						<label class="tit" for="re_password">비밀번호 확인</label>
						<asp:TextBox runat="server" TextMode="Password" id="re_password" placeholder="입력한 비밀번호를 다시 한 번 입력해주세요." />
					</div>
					<div class="mb50"></div>
					<asp:LinkButton class="btn_b_type4" runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" OnClientClick="return goSubmit()">확인</asp:LinkButton>
				</div>
				
			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->


    
  
    
</asp:Content>