﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="recruit_login_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        $(function () {
        });

        var goSubmit = function () {
            if ($("#email").val() == "") {
                $("#msg_email").show();
                $("#msg_email_txt").text("정보를 입력해 주세요.");
                $("#email").focus();
                return false;
            }

            if ($("#password").val() == "") {
                $("#msg_pwd").show();
                $("#msg_pwd_txt").text("정보를 입력해 주세요.");
                $("#password").focus();
                return false;
            }
        }
    </script>
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	 <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%" class="on"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->

				<h2 class="sub_tit">본인확인</h2>

				<div class="box_con1">
					<div class="field">
						<label class="tit" for="email">이메일</label>
						<asp:TextBox runat="server" ID="email" value=""/><br />
					</div>
					<p class="comt1" id="msg_email" runat="server" style="display:none"><span id="msg_email_txt" runat="server" class="guide_comment2">정보를 입력해 주세요.</span></p>

					<div class="field">
						<label class="tit" for="password">비밀번호</label>
						<asp:TextBox TextMode="Password" runat="server" ID="password" value=""/><br />
					</div>
					<p class="comt1" style="display:none" runat="server" id="msg_pwd"><span class="guide_comment2" runat="server" id="msg_pwd_txt">정보를 입력해 주세요.</span></p>

					<p class="check_pw">비밀번호를 잊으셨나요? <a href="/recruit/login/identify" class="btn_arr_type1" runat="server" id="btnFind">비밀번호 찾기<span></span></a></p>
					<asp:LinkButton runat="server" class="btn_b_type4" ToolTip="로그인" ID="btnLogin" OnClientClick="return goSubmit()" OnClick="btnLogin_Click">확인</asp:LinkButton>
				</div>
				<div class="box_info1">
					<span class="txt">처음 지원하시는 분은 지원자 등록을 먼저 해주세요.</span>
					<a href="/recruit/signup/" class="btn_s_type2 ml10" runat="server" id="btnRegist">지원자 등록</a>
				</div>


			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->


   
    
    

    
    
</asp:Content>