﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class recruit_result_default : FrontBasePage {


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
	}


	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
	}


	protected void btnResult_Click(object sender, EventArgs e) {
		var r_email = email.Value.EmptyIfNull();
		var r_pwd = password.Value.EmptyIfNull();

		if (r_email == "" || r_pwd == "") return;

        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<applicant>("ap_email", r_email, "ap_pwd", r_pwd.SHA256Hash());
            //if (dao.applicant.Any(p => p.ap_email == r_email && p.ap_pwd == r_pwd.SHA256Hash()))
            if(exist.Any())
            {
                //var entity = dao.applicant.First(p => p.ap_email == r_email && p.ap_pwd == r_pwd.SHA256Hash());
                var entity = exist[0];

                // 지원여부 검색

                //var resume = dao.resume.FirstOrDefault(p => p.r_ap_id == entity.ap_id);
                var resume = www6.selectQF<resume>("r_ap_id", entity.ap_id);
                if (resume == null)
                {
                    AlertWithJavascript("지원하신 내역이 없습니다. 지원여부 및 지원자 등록 정보를 확인해 주세요.");
                }
                else
                {

                    var loginEntity = new RecruitSession.Entity()
                    {
                        id = entity.ap_id,
                        name = entity.ap_name,
                        email = entity.ap_email
                    };
                    RecruitSession.SetCookie(Context, loginEntity);
                    Response.Redirect("/recruit/result/list");
                }



            }
            else
            {
                //AlertWithJavascript("이메일 주소 혹은 비밀번호가 맞지 않습니다. 다시 확인해 주세요.");
                msg_pwd.Style["display"] = "block";
                msg_pwd_txt.InnerHtml = "이메일 또는 비밀번호가 올바르지 않습니다. 다시 확인해 주세요.";
            }
        }
	}


	void SendMail(string to, string name) {

		try {
			var from = ConfigurationManager.AppSettings["recruitEmailSender"];
			var args = new Dictionary<string, string> {
				{"{name}" , name } 
			};
			var title = "지원결과 조회 안내";



			Email.Send(this.Context, from, new List<string>() { to },
				title, "/mail/recruit_result.html",
				args
			, null);

		} catch (Exception e) {
			Response.Write(e.Message);
			throw e;
		}

	}
}