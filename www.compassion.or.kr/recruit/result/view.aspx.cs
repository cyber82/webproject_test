﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class recruit_result_view : FrontBasePage {

	public override bool RequireRecruitLogin{
		get { return true; }
	}

	const string listPath = "/recruit/result/list";


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		base.PrimaryKey = requests[0];


		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}

		GetData();

	}


	void GetData()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<resume>("r_id", Convert.ToInt32(PrimaryKey));
            //if (!dao.resume.Any(p => p.r_id == Convert.ToInt32(PrimaryKey)))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            var resume = exist[0];//dao.resume.First(p => p.r_id == Convert.ToInt32(PrimaryKey));


            //rj_name.Text = dao.recruit_jobcode.First(p => p.rj_depth1 == resume.r_rj_depth1 && p.rj_depth2 == "").rj_name;
            rj_name.Text = www6.selectQF<recruit_jobcode>("rj_depth1", resume.r_rj_depth1, "rj_depth2", "").rj_name;

            //b_title.Text = dao.recruit_jobcode.First(p => p.rj_depth1 == resume.r_rj_depth1 && p.rj_depth2 == resume.r_rj_depth2).rj_name;
            b_title.Text = www6.selectQF<recruit_jobcode>("rj_depth1", resume.r_rj_depth1, "rj_depth2", resume.r_rj_depth2).rj_name;

            //title.InnerText = dao.recruit.First(p => p.b_id == resume.r_b_id).b_title;
            title.InnerText = www6.selectQF<recruit>("b_id", resume.r_b_id).b_title;

            r_result.Text = GetResultStatus(resume.r_result);

            DisplayResultText(resume.r_process_status, resume.r_result, resume.r_name);

        }
	}

	string GetResultStatus(string r_result) {
		string result = "";
		switch (r_result) {
			case "standby": result = "심사중"; break;
			case "pass": result = "최종 합격"; break;
			case "fail": result = "불합격"; break;
		}


		return result;
	}

	string DisplayResultText(string r_process_status, string r_result, string r_name) {
		string result = "";

		// 최종합격
		if (r_result == "pass") {
			resultPass.Text = string.Format(resultPass.Text, r_name, r_name);
			resultPass.Visible = true;
		} else if (r_result == "standby") {
			if (r_process_status == "doc1") {
				resultStandby.Text = string.Format(resultStandby.Text, r_name);
				resultStandby.Visible = true;

			} else if (r_process_status == "doc2" || r_process_status == "interview1") {
				resultDocPass.Text = string.Format(resultDocPass.Text, r_name);
				resultDocPass.Visible = true;
			} else if (r_process_status == "interview2" || r_process_status == "interview3") {
				resultInterviewPass.Text = string.Format(resultInterviewPass.Text, r_name, r_name);
				resultInterviewPass.Visible = true;
			}

		//불합격
		} else {
			// 서류전형 불합격
			if(r_process_status == "standby" || r_process_status == "doc1") {
				resultDocFail.Text = string.Format(resultDocFail.Text, r_name);
				resultDocFail.Visible = true;
			} else if(r_process_status == "interview3"){
				resultFail.Text = string.Format(resultInterviewFail.Text, r_name);
				resultFail.Visible = true;
			} else {
				resultInterviewFail.Text = string.Format(resultInterviewFail.Text, r_name);
				resultInterviewFail.Visible = true;
			}
		}

		return result;
	}



}