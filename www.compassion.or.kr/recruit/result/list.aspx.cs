﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class recruit_result_list : FrontBasePage {


	public override bool RequireRecruitLogin{
		get { return true; }
	}


	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();

		base.LoadComplete += new EventHandler(list_LoadComplete);
	}


	protected override void OnAfterPostBack() {

	}

	protected override void GetList(int page)
    {

		var cookie = RecruitSession.GetCookie(Context);
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.sp_recruit_apply_list_f(cookie.id).ToList();
            Object[] op1 = new Object[] { "ap_id" };
            Object[] op2 = new Object[] { cookie.id };
            var list = www6.selectSP("sp_recruit_apply_list_f", op1, op2).DataTableToList<sp_recruit_apply_list_fResult>();

            repeater.DataSource = list;
            repeater.DataBind();

        }
	}
	


	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {

				sp_recruit_apply_list_fResult entity = e.Item.DataItem as sp_recruit_apply_list_fResult;
				((Literal)e.Item.FindControl("lbDateEnd")).Text = entity.b_timeless ? "채용시까지" : entity.b_date_end.ToString("yyyy.MM.dd");
				((Literal)e.Item.FindControl("lbStatus")).Text = entity.b_completed ? "<span class='status result'>결과<br />발표</span>" : " <span class='status ing'>진행중</span>";
			}
		}
	}

}