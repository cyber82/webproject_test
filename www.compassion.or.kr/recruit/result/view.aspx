﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="recruit_result_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
    <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<!-- w980 -->
			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%" class="on"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->

				<!-- 채용공고상세 -->
				<div class="boardView_1">

					<div class="tit_box noline">
						<span class="tit" runat="server" id="title"></span>
						<span class="txt">
							<span><asp:Literal runat="server" ID="rj_name" /> - <asp:Literal runat="server" ID="b_title" /></span>
							<span class="bar"></span>
							<span><asp:Literal runat="server" ID="r_result" /></span>
						</span>
					</div>
					<div class="view_contents line2">
						<asp:Literal runat="server" ID="resultStandby" Visible="false" Text="안녕하세요. {0}님. 한국컴패션 채용담당자 입니다. <br/><br/>먼저 많은 관심과 애정을 가지고 한국컴패션에 지원해 주셔서 감사 드립니다. <br/>현재 제출 해주신 지원서를 검토 하고 있습니다. 검토가 마무리되면 전형 결과에 대해서 <br/>안내 드리도록 하겠습니다. <br/>감사합니다. "/> 
						<asp:Literal runat="server" ID="resultDocPass" Visible="false" Text="안녕하세요. {0}님. 한국컴패션 채용담당자 입니다. <br/><br/>{0}님께서는 서류 전형 결과 인터뷰 대상자가 되셨습니다. <br/>인터뷰 일정 및 장소 안내는 곧 유선상으로 연락 드리도록 하겠습니다. <br/>감사합니다."/> 
						<asp:Literal runat="server" ID="resultDocFail" Visible="false" Text="안녕하세요. {0}님. 한국컴패션 채용담당자 입니다. <br/><br/>어린이를 향한 사랑으로 컴패션에 지원해주시고, 정성스럽게 지원서를 작성해 주셔서 감사 드립니다. <br/>아쉽게도 이번 채용 기회를 통해서는 함께 하실 수 없게 됨을 알려 드리게 되었습니다.<br/><br/>이번 기회로는 함께 하지 못하게 되었지만, 계속해서 컴패션 사역과 어린이들을 위해 응원하고 기도해 주시길 부탁 드립니다.<br/>어느 곳에 계시든지 주님과 동행하는 은혜가 가득하시길 소망합니다.<br/>감사합니다."/> 
                        <asp:Literal runat="server" ID="resultInterviewPass" Visible="false" Text="안녕하세요 {0}님 한국컴패션 채용담당자 입니다. <br /><br />어린이를 향한 사랑으로 컴패션에 지원해주시고, 인터뷰 프로세스에 참여 해주셔서 감사 드립니다.<br/>{1}님께서는 인터뷰 결과  해당 인터뷰 전형에 합격하셨습니다.<br/>자세한 안내는 곧 유선상으로 연락 드리도록 하겠습니다.<br/>감사합니다."/> 
						<asp:Literal runat="server" ID="resultInterviewFail" Visible="false" Text="안녕하세요 {0}님<br/><br/>한국컴패션 채용담당자 입니다. <br /><br />어린이를 향한 사랑으로 컴패션에 지원해주시고, 인터뷰 프로세스에 참여 해주셔서 감사 드립니다.<br/>인터뷰 결과 아쉽게도 이번 채용 기회를 통해서는 함께 하실 수 없게 됨을 알려 드리게 되었습니다.<br/><br/>이번 기회로는 함께 하지 못하게 되었지만, 계속해서 컴패션 사역과 어린이들을 위해 응원하고 기도해 주시길 부탁 드립니다.<br/>어느 곳에 계시든지 주님과 동행하는 은혜가 가득하시길 소망합니다. <br/>감사합니다."/> 
						<asp:Literal runat="server" ID="resultPass" Visible="false" Text="안녕하세요. {0}님. 한국컴패션 채용담당자 입니다. <br/><br/>어린이를 향한 사랑으로 컴패션에 지원해주시고, 인터뷰 프로세스에 참여 해주셔서 감사 드립니다. <br/>{1}님께서는 모든 채용 전형에서 최종 합격하셨습니다.<br/>자세한 안내는 다시 유선상으로 안내 드리도록 하겠습니다.<br/>감사합니다. "/> 
                        <asp:Literal runat="server" ID="resultFail" Visible="false" Text="안녕하세요. {0}님. 한국컴패션 채용담당자 입니다. <br/><br/>어린이를 향한 사랑으로 컴패션에 지원해주시고, 인터뷰 프로세스에 참여 해주셔서 감사 드립니다. <br/>인터뷰 결과 아쉽게도 이번 채용 기회를 통해서는 함께 하실 수 없게 됨을 알려 드리게 되었습니다.<br/><br/>이번 기회로는 함께 하지 못하게 되었지만, 계속해서 컴패션 사역과 어린이들을 위해 응원하고 기도해 주시길 부탁 드립니다.<br/>어느 곳에 계시든지 주님과 동행하는 은혜가 가득하시길 소망합니다. <br/>감사합니다. "/> 
					</div>

				</div>
				<!--// 채용공고상세 -->

				<div class="tar">
					<a href="#" class="btn_type4" title="목록" runat="server" id="btnList">목록</a>
				</div>


			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

</asp:Content>