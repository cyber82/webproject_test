﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="recruit_signup_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script src="/assets/jquery/wisekit/form.js"></script>
    <script src="/assets/jquery/wisekit/validation/password.js"></script>
    <script src="/assets/jquery/wisekit/validation/email.js"></script>
    <script src="/assets/jquery/wisekit/message.js"></script>
    
    <script src="default.js"></script>
    <script type="text/javascript">
    </script>
    <style>
        .tbl_rules {
            width: 100%;
            border-collapse: collapse;
        }
        .tbl_rules thead tr th {
            font-family: 'noto_d';
            font-size: 100%;
            color: #767676;
            line-height: 30px;
            font-weight: normal;
            vertical-align: middle;
            padding: 1px 2px;
            border-top: 1px solid #ddd;
            border-left: 1px solid #ddd;
            background: #f5f5f5;
        }
        .tbl_rules tr td {
            font-family: 'noto_d';
            font-size: 100%;
            color: #767676;
            line-height: 30px;
            font-weight: normal;
            vertical-align: middle;
            text-align: center;
            padding: 1px 2px;
            border: 1px solid #ddd;
        }
    </style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	 <!-- sub body -->
	<asp:PlaceHolder runat="server" ID="before">
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">한국컴패션은 꿈을 잃은 어린이들에게 희망을 전할 열정과 역량을 가지고 있는 분들을 기다립니다</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->

				<h2 class="sub_tit line">지원자 등록</h2>

				<!-- 등록테이블 -->
				<table class="tbl_type1 mb40">
					<caption>지원자 등록 테이블</caption>
					<colgroup>
						<col style="width:18%" />
						<col style="width:82%" />
					</colgroup>
					<tbody>
						<tr>
							<th scope="row"><label for="email">이메일</label></th>
							<td>
								<asp:TextBox class="input_type2 rw1" runat="server" ID="email" placeholder="본인 확인을 위한 ID로 사용됩니다. 정확히 입력해주세요."/><br />
								<p class="pt10" style="display:none;"><span class="guide_comment1" id="email_validation_reuslt">유효한 이메일 주소입니다.</span></p>		
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="name">이름</label></th>
							<td>
								<asp:TextBox class="input_type2 rw1" runat="server" ID="name" /><br />
								<p class="pt10"  style="display:none;"><span class="guide_comment2" id="name_validation_reuslt"></span></p>
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="phone">휴대폰</label></th>
							<td>
								<asp:TextBox class="input_type2 rw1"  runat="server" ID="phone" MaxLength="13" placeholder="숫자만 입력해주세요." /><br />
								<p class="pt10"  style="display:none;"><span class="guide_comment1" id="phone_validation_reuslt">사용 가능한 번호입니다.</span></p>
							</td>
						</tr>
        
						
						<tr class="padding1">
							<th scope="row"><label for="password">비밀번호</label></th>
							<td>
								<asp:TextBox class="input_type2 rw1" runat="server" ID="password" TextMode="Password"  placeholder="띄어쓰기 없이 영문,숫자,특수문자 중 2가지 이상 조합으로 5-15자 이내로 입력해주세요."/><br />
								<p class="pt10"  style="display:none;"><span class="guide_comment2" id="password_validation_reuslt">비밀번호는 띄어쓰기 없이 영문, 숫자, 특수문자 중 2가지 이상 조합으로 5~15자 이내로 입력해주세요.</span></p>
							</td>
						</tr>
						
						<tr class="padding2">
							<th scope="row"><label for="re_password">비밀번호 확인</label></th>
							<td>
								<asp:TextBox class="input_type2 rw1" runat="server" ID="re_password" TextMode="Password" /><br />
								<p class="pt10" style="display:none;"><span class="guide_comment2" id="re_password_validation_reuslt">비밀번호와 확인 비밀번호가 일치하지 않습니다. 다시 한번 확인해주세요.</span></p>
							</td>
						</tr>
					</tbody>
				</table>
				<!--// 등록테이블 -->

				<!-- 정보이용동의 -->
				<div class="apply_agreement">
					<div class="box box1">
						<p class="tit">개인정보 수집 및 이용에 대한 동의</p>
						<div class="terms">
 한국컴패션에서는 기관의 직원 채용 진행을 위해 아래와 같이 개인정보를 수집 및 활용합니다. <br />
한국컴패션은 지원자의 개인정보를 중요시하며 개인정보보호에 관한 법률을 준수합니다. 개인정보보호법에 따라 지원자가 제공하는 개인정보가 어떠한 목적으로 이용되는지 알려드리고 정보수집 및 이용에 대한 동의를 받고 있습니다. 지원자는 아래 내용에 동의를 거부할 수 있으며 동의하지 않는 경우 채용심사에서 제외되는 불이익을 받을 수 있음을 알려드립니다.<br />
<br />
1. 수집하는 개인정보 항목 및 수집방법<br />
기관은 회원가입, 상담, 서비스 신청 등을 위해 아래와 같은 개인정보를 수집하고 있습니다.<br />
<br />
1)	개인정보 수집항목<br />
<br />
(홈페이지 회원가입)<br />
- 필수항목 : 이름, 생년월일, 휴대폰번호, 이메일, 전화번호, 아이디(ID), 비밀번호, 법정대리인 이름, 법정대리인 핸드폰번호, 교회직분<br />
<br />
(기업/단체회원)<br />
- 필수항목 : 기업명(교회명), 사업자번호, 담당자이름, 담당자휴대폰번호, 이메일, 전화번호, 아이디(ID), 비밀번호,<br />
- 선택항목 : 사업자등록증<br />
<br />
(후원신청)<br />
- 필수항목 : 영문이름, 종교, 교회명, 주소, 후원 계기<br />
- 선택항목: 주민등록번호(국세청 연말정산간소화서비스 등록을 위한 수집 시)<br />
<br />
(비회원 후원)<br />
- 필수항목 : 이름<br />
- 선택항목 : 주민등록번호(국세청 연말정산간소화서비스 등록을 위한 수집 시)<br />
<br />
(후원금결제)<br />
- 신용카드 : 카드사명, 카드번호, 유효기간, 카드명의자명, 카드명의자 생년월일<br />
- 자동이체(CMS) : 은행명, 계좌번호, 예금주명, 예금주 생년월일<br />
<br />
(첫 생일 첫 나눔)<br />
 - 필수항목 : 아기 성별, 아기이름, 생년월일, 사진, 신청자 이름, 아기와의 관계, <br />
휴대폰번호, E-Mail<br />
 - 선택항목 : 주민등록번호(국세청 연말정산간소화서비스 등록을 위한 수집 시)<br />
<br />
(결혼첫나눔-정기후원)<br />
 - 필수항목 : 신부이름, 신랑이름, 결혼년월일, 휴대폰번호, E-Mail, 결혼사진, 주소, <br />
영문이름, 종교, 교회명, 후원 계기<br />
- 선택항목 : 주민등록번호(국세청 연말정산간소화서비스 등록을 위한 수집 시)<br />
<br />
(결혼첫나눔-일시후원)<br />
- 필수항목 : 신부이름, 신랑이름, 결혼년월일, 휴대폰번호, E-Mail, 결혼사진, 주소<br />
- 선택항목 : 주민등록번호(국세청 연말정산간소화서비스 등록을 위한 수집 시)<br />
<br />
(컴패션사역훈련 수강신청)<br />
- 필수항목 : 이름, 아이디(ID), 생년월일, 휴대전화, 주소<br />
- 선택항목 : 소속교회, 교회직분<br />
<br />
(스토어)<br />
- 필수항목 : 수령인, 휴대폰, 배송주소<br />
- 선택항목 : 비상연락처<br />
<br />
(모바일 결연서)<br />
- 필수항목 : 이름, 영문이름, 휴대폰번호, 생년월일, 주소<br />
- 선택항목 : 주민번호 뒷자리(국세청 연말정산간소화서비스 등록을 위한 수집 시), E-Mail, 아이디(ID), 비밀번호<br />
<br />
(인재채용)<br />
- 필수항목 : 이름(한글,영문), 생년월일, 성별, 거주지 주소, 휴대폰번호, E-Mail, 연봉, 병역구분, 보훈대상, 보훈번호, 학력구분, 학교명, 전공, 입학/졸업연월, 소재지, 컴패션 후원여부, 자기소개서, 경력기술서, 신앙간증문, 추천인, 종교<br />
- 선택항목 : 외국어명, 인증기관, 점수/등급, 취득일, 자격증명, 발급기관, 자격증번호, 합격일, 이전 회사명, 고용형태, 근무기간, 근무부서, 직급, 담당업무, 포트폴리오<br />
- 민감정보 : 장애여부, 장애유형, 장애등급<br />
<br />
2) 개인정보 수집방법<br />
- 홈페이지, 서명양식, 팩스, 전화, 상담게시판, E-Mail, 이벤트 응모, SNS 및 캠페인 <br />
등 확장 플랫폼을 통한 수집<br />
- 협력회사 및 해외 Compassion으로부터의 제공<br />
- 생성정보 수집 툴을 통한 수집(쿠키 등)<br />
<br />
<br />
2. 개인정보의 수집 및 이용 목적<br />
기관에서 정의한 “개인정보”라 함은 생존하는 개인에 관한 정보로서, 성명, 연락처, 주소 등의 사항에 의하여 개인을 식별할 수 있는 정보를 말하며, 기관은 서비스를 통해 홈페이지 회원 및 후원회원, 쇼핑, 사역 등을 통한 후원관리를 위하여 이용자 개인의 정보를 수집하고 있습니다. <br />
기관은 수집된 정보를 다음과 같은 목적으로 이용합니다.<br />
<br />
3. 개인정보의 제3자 제공 및 위탁   <br />                      
기관은 이용자 및 후원자의 개인정보를 “개인정보 수집 및 이용목적”에서 고지한 범위 내에서만 사용하며, 이용자 및 후원자의 사전 동의 없이는 동 범위를 초과하여 이용하거나 원칙적으로 외부에 제공하지 않습니다. 다만, 아래의 경우에는 예외로 합니다.<br />
- 이용자들이 사전에 동의한 경우<br />
- 법령의 규정에 의거하거나, 수사 목적으로 법령에 정해진 절차와 방법에 따라 수사기관의 요구가 있는 경우 <br />
1)	수집한 개인정보의 이용 및 제3자 제공<br />
-	개인정보 제3자 제공<br />
기관은 법령에 근거가 있는 등의 예외적인 경우를 제외하고 이용자의 동의 없이 개인정보를 제3자에게 제공하지 않습니다. 다만, 제휴사, 후원사 등에 이용자의 개인정보를 제공할 수 있으나, 이는 이용자에게 최적, 양적의 서비스를 제공하기 위한 목적으로만 행해지는 것이고, 그 경우에도 “개인정보 보호법”에 따른 5가지 고지사항을 준수합니다.<br />
•	제공받는 자<br />
•	제공받는 자의 이용 목적<br />
•	제공 개인정보 항목<br />
•	개인정보 보유 및 이용기간<br />
•	동의 거부에 따른 불이익<br />
<table class="tbl_rules">
	<caption>개인정보를 위탁중인 외부 전문업체 리스트</caption>
	<colgroup>
		<col style="width:25%" />
		<col style="width:25%" />
        <col style="width:25%" />
        <col style="width:25%" />
	</colgroup>
	<thead>
		<tr>
			<th scope="col">제공받는 자</th>
			<th scope="col">제공받는 자의 이용 목적</th>
            <th scope="col">제공 개인정보 항목</th>
            <th scope="col">개인정보 보유 및 이용기간</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>소속교회 북한사역 담당 목회자</td>
			<td>소속교회 참가자 인적 정보 확인</td>
            <td>이름, 휴대폰번호, E-Mail, 교회직책</td>
            <td>회원 탈퇴 및 목적 달성시까지</td>
		</tr>
	</tbody>
</table>
<br />                           
※ 제3자 제공 동의 거부 시 소속교회 북한사역 담당 목회자에게 개인정보가 전달될 수 없음으로 소속교회 참가자 인적 정보 확인이 소속교회에서는 불가합니다.<br />
<br />
2)	개인정보 취급 위탁<br />
기관은 후원자 서비스 향상을 위하여 다음과 같이 일부 개인정보 취급 업무를 외부 전문업체에 위탁하고 있으며, 개인정보 보호법 26조에 따라 위탁계약 시 개인정보가 안전하게 관리될 수 있도록 필요한 사항을 규정하고 관리/감독하고 있습니다.<br />
<table class="tbl_rules">
	<caption>개인정보를 위탁중인 외부 전문업체 리스트</caption>
	<colgroup>
		<col style="width:30%" />
		<col style="width:45%" />
        <col style="width:25%" />
	</colgroup>
	<thead>
		<tr>
			<th scope="col">수탁업체</th>
			<th scope="col">위탁업무 내용</th>
            <th scope="col">개인정보 보유 및 이용기간</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>㈜고우아이티</td>
			<td>온라인 서비스 유지보수, 후원자관리 프로그램 유지보수</td>
            <td rowspan="16">후원/회원 탈퇴 및 위탁계약 종료 시 까지</td>
		</tr>
        <tr>
			<td>㈜다우기술</td>
			<td>후원자 및 회원에게 SMS 발송</td>
		</tr>
        <tr>
			<td>㈜케이티</td>
			<td>후원자 및 회원으로부터 SMS 수신</td>
		</tr>
        <tr>
			<td>㈜월드피에이디</td>
			<td>후원자 뉴스레터, 매거진 발송 대행</td>
		</tr>
        <tr>
			<td>Compassion International</td>
			<td>어린이 결연 및 후원자 응대</td>
		</tr>
        <tr>
			<td>서울신용평가정보㈜</td>
			<td>실명인증 및 본인확인</td>
		</tr>
        <tr>
			<td>㈜한국사이버결제</td>
			<td>전자 결제 처리, 신용카드 결제 처리</td>
		</tr>
        <tr>
			<td>금융결제원</td>
			<td>계좌이체 결제 처리</td>
		</tr>
        <tr>
			<td>효성에프엠에스㈜</td>
			<td>계좌이체 결제 처리(1회성)</td>
		</tr>
        <tr>
			<td>주식회사 SMTNT</td>
			<td>MO(Message Oriented) 서비스 제공</td>
		</tr>
        <tr>
			<td>종로6가우편집중국</td>
			<td>어린이 사진, 편지 및 각종 우편물 발송</td>
		</tr>
        <tr>
			<td>한국갤럽</td>
			<td>후원자 설문조사 프로그래밍 및 조사 진행</td>
		</tr>
        <tr>
			<td>테크빌교육(주)</td>
			<td>NK교육훈련사이트 운영 및 유지보수</td>
		</tr>
        <tr>
			<td>(주)커스토머인사이트</td>
			<td>CRM시스템 유지보수</td>
		</tr>
        <tr>
			<td>(주)여행상점</td>
			<td>왕복항공권 구매, 비자 발급, 여행자보험 가입</td>
		</tr>
        <tr>
			<td>(주)베러웨이시스템즈</td>
			<td>어린이 자료 등 후원관련 우편물의 인쇄 및 발송</td>
		</tr>
	</tbody>
</table>
<br />
<br />
4. 개인정보의 보유 및 이용기간<br />
기관은 수집한 개인정보의 이용목적이 달성하면 지체 없이 파기하며, 특히 탈퇴 회원 및 탈퇴 후원자 개인정보는 7일 이내에 파기하고 있습니다. 다만, 개인정보의 수집 및 이용목적이 달성된 경우에도 상법, 전자상거래 등에서의 소비자 보호에 관한 법률 등 관련 법령의 규정에 의하여 보존할 필요성이 있을 경우 및 사전에 보유기간을 이용자에게 고지하거나 명시한 경우 등은 그에 따라 개인정보를 보관할 수 있습니다. 개인정보를 보존하려고 하는 경우 사전에 그 보존근거 및 보존하는 개인정보 항목을 E-Mail, 서면, 유선전화로 개별통지 하거나 인터넷 사이트에 명시하여 이용자의 동의를 받도록 하겠습니다.<br />
• 관련법령에 의한 정보보유 사유<br />
- 계약 또는 청약철회 등에 관한 기록<br />
- 보존 이유 : 전자상거래 등에서의 소비자보호에 관한 법룔 <br />
- 보존 기간 : 5년<br />
• 대금결제 및 재화 등의 공급에 관한 기록<br />
- 보존 이유 : 전자상거래 등에서의 소비자보호에 관한 법룔 <br />
- 보존 기간 : 5년<br />
• 소비자의 불만 또는 분쟁처리에 관한 기록<br />
- 보존 이유 : 전자상거래 등에서의 소비자보호에 관한 법룔<br />
- 보존 기간 : 3년<br />
• 본인확인에 관한 기록<br />
- 보존 이유 : 정보통신망 이용촉진 및 정보보호에 관한 법률 <br />
- 보존 기간 : 6개월<br />
•  접속에 관한 기록<br />
- 보존 이유 : 통신비밀보호법<br />
- 보존 기간 : 3개월 <br />
<br />
<br />
5. 개인정보의 파기절차 및 방법<br />
기관은 원칙적으로 개인정보 수집 및 이용목적이 달성된 후에는 해당 정보를 지체 없이 파기합니다.<br />
파기절차 및 방법은 다음과 같습니다.<br />
<br />
1) 파기절차<br />
- 회원이 회원가입 등을 위해 입력한 정보는 목적이 달성된 후 별도의 데이터베이스로 옮겨져(종이의 경우 별도의 서류함) 내부 방침 및 기타 관련 법령에 의한 정보보호 사유에 따라(보유 및 이용기간 참조) 일정 기간 저장된 후 파기됩니다.<br />
별도 데이터베이스로 옮겨진 개인정보는 법률에 의한 경우가 아니고서는 다른 목적으로 이용되지 않습니다.<br />
<br />
2) 파기방법<br />
- 전자적 파일형태로 저장된 개인정보는 기록을 재생할 수 없는 기술적 방법을 사용하여 삭제, 종이에 출력된 개인정보는 분쇄기로 분쇄하거나 소각을 통하여 파기합니다. <br />

						</div>
						<span class="checkbox_ui">
							<input type="checkbox" runat="server" class="css_checkbox" ID="privacy" />
							<label for="privacy" class="css_label font2">개인정보 수집 및 이용에 동의합니다.</label>
						</span>
					</div>
					<div class="box box2">
						<p class="tit">민감정보의 처리에 대한 동의</p>
                        <div class="terms">
 한국컴패션에서는 기관의 직원 채용 진행을 위해 아래와 같이 민감정보를 수집 및 활용합니다. <br />
한국컴패션은 지원자의 개인정보를 중요시하며 개인정보보호에 관한 법률을 준수합니다. 개인정보보호법에 따라 지원자가 제공하는 개인정보가 어떠한 목적으로 이용되는지 알려드리고 민감정보수집 및 이용에 대한 동의를 받고 있습니다. 지원자는 아래 내용에 동의를 거부할 수 있으며 동의하지 않는 경우 채용심사에서 제외되는 불이익을 받을 수 있음을 알려드립니다.<br />
<br />
1. 수집하는 개인정보 항목 및 수집방법<br />
기관은 회원가입, 상담, 서비스 신청 등을 위해 아래와 같은 개인정보를 수집하고 있습니다.<br />
<br />
1)	개인정보 수집항목<br />
<br />
(홈페이지 회원가입)<br />
- 필수항목 : 이름, 생년월일, 휴대폰번호, 이메일, 전화번호, 아이디(ID), 비밀번호, 법정대리인 이름, 법정대리인 핸드폰번호, 교회직분<br />
<br />
(기업/단체회원)<br />
- 필수항목 : 기업명(교회명), 사업자번호, 담당자이름, 담당자휴대폰번호, 이메일, 전화번호, 아이디(ID), 비밀번호,<br />
- 선택항목 : 사업자등록증<br />
<br />
(후원신청)<br />
- 필수항목 : 영문이름, 종교, 교회명, 주소, 후원 계기<br />
- 선택항목: 주민등록번호(국세청 연말정산간소화서비스 등록을 위한 수집 시)<br />
<br />
(비회원 후원)<br />
- 필수항목 : 이름<br />
- 선택항목 : 주민등록번호(국세청 연말정산간소화서비스 등록을 위한 수집 시)<br />
<br />
(후원금결제)<br />
- 신용카드 : 카드사명, 카드번호, 유효기간, 카드명의자명, 카드명의자 생년월일<br />
- 자동이체(CMS) : 은행명, 계좌번호, 예금주명, 예금주 생년월일<br />
<br />
(첫 생일 첫 나눔)<br />
 - 필수항목 : 아기 성별, 아기이름, 생년월일, 사진, 신청자 이름, 아기와의 관계, <br />
휴대폰번호, E-Mail<br />
 - 선택항목 : 주민등록번호(국세청 연말정산간소화서비스 등록을 위한 수집 시)<br />
<br />
(결혼첫나눔-정기후원)<br />
 - 필수항목 : 신부이름, 신랑이름, 결혼년월일, 휴대폰번호, E-Mail, 결혼사진, 주소, <br />
영문이름, 종교, 교회명, 후원 계기<br />
- 선택항목 : 주민등록번호(국세청 연말정산간소화서비스 등록을 위한 수집 시)<br />
<br />
(결혼첫나눔-일시후원)<br />
- 필수항목 : 신부이름, 신랑이름, 결혼년월일, 휴대폰번호, E-Mail, 결혼사진, 주소<br />
- 선택항목 : 주민등록번호(국세청 연말정산간소화서비스 등록을 위한 수집 시)<br />
<br />
(컴패션사역훈련 수강신청)<br />
- 필수항목 : 이름, 아이디(ID), 생년월일, 휴대전화, 주소<br />
- 선택항목 : 소속교회, 교회직분<br />
<br />
(스토어)<br />
- 필수항목 : 수령인, 휴대폰, 배송주소<br />
- 선택항목 : 비상연락처<br />
<br />
(모바일 결연서)<br />
- 필수항목 : 이름, 영문이름, 휴대폰번호, 생년월일, 주소<br />
- 선택항목 : 주민번호 뒷자리(국세청 연말정산간소화서비스 등록을 위한 수집 시), E-Mail, 아이디(ID), 비밀번호<br />
<br />
(인재채용)<br />
- 필수항목 : 이름(한글,영문), 생년월일, 성별, 거주지 주소, 휴대폰번호, E-Mail, 연봉, 병역구분, 보훈대상, 보훈번호, 학력구분, 학교명, 전공, 입학/졸업연월, 소재지, 컴패션 후원여부, 자기소개서, 경력기술서, 신앙간증문, 추천인, 종교<br />
- 선택항목 : 외국어명, 인증기관, 점수/등급, 취득일, 자격증명, 발급기관, 자격증번호, 합격일, 이전 회사명, 고용형태, 근무기간, 근무부서, 직급, 담당업무, 포트폴리오<br />
- 민감정보 : 장애여부, 장애유형, 장애등급<br />
<br />
2) 개인정보 수집방법<br />
- 홈페이지, 서명양식, 팩스, 전화, 상담게시판, E-Mail, 이벤트 응모, SNS 및 캠페인 <br />
등 확장 플랫폼을 통한 수집<br />
- 협력회사 및 해외 Compassion으로부터의 제공<br />
- 생성정보 수집 툴을 통한 수집(쿠키 등)<br />
<br />
<br />
2. 개인정보의 수집 및 이용 목적<br />
기관에서 정의한 “개인정보”라 함은 생존하는 개인에 관한 정보로서, 성명, 연락처, 주소 등의 사항에 의하여 개인을 식별할 수 있는 정보를 말하며, 기관은 서비스를 통해 홈페이지 회원 및 후원회원, 쇼핑, 사역 등을 통한 후원관리를 위하여 이용자 개인의 정보를 수집하고 있습니다. <br />
기관은 수집된 정보를 다음과 같은 목적으로 이용합니다.<br />
						</div>

						<span class="checkbox_ui">
							<input type="checkbox" class="css_checkbox" runat="server" ID="agree" />
							<label for="agree" class="css_label font2">민감정보의 처리에 동의합니다.</label>
						</span>
					</div>
				</div>
				<!--// -->

				<div class="tac"><asp:LinkButton class="btn_type1" runat="server" ToolTip="지원자등록" ID="btnRegist" OnClientClick="return goSubmit()" OnClick="btnRegist_Click">다음</asp:LinkButton></div>

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
	 </asp:PlaceHolder>

    <asp:PlaceHolder runat="server" ID="after" Visible="false">
    <section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>인재<em>채용</em></h1>
				<span class="desc">가난 가운데 있는 어린이들의 세상을 변화시키는 일, 한국컴패션에서 이루어 보세요.</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span class="current">인재채용</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 recruit">

			<div class="visual">
				<div class="visual_inr w980">
					<p class="tit">한국컴패션은</p>
					<span class="bar"></span>
					<p class="txt">
						예수님을 향한 사랑과 그 분의 사명에 따라 어린이들의 삶에 의미 있는 변화를<br />
						꿈꾸는 열정과 역량을 가진 각 분야의 인재를 기다리고 있습니다.
					</p>
				</div>
			</div>

			<div class="w980">

				<!-- tab menu -->
				<ul class="tab_type1 mb60">
					<li style="width:20%"><a href="/recruit/">진행 중인 채용</a></li>
					<li style="width:20%"><a href="/recruit/pool/">상시지원</a></li>
					<li style="width:20%"><a href="/recruit/process/">채용 프로세스</a></li>
					<li style="width:20%"><a href="/recruit/faq/">FAQ</a></li>
					<li style="width:20%"><a href="/recruit/result">지원결과 조회</a></li>
				</ul>
				<!--// -->

				<h2 class="sub_tit">지원자 등록</h2>

				<div class="box_con1">
					
					<p class="box_tit">한국컴패션에 지원해 주셔서 감사합니다!</p>
					<p class="box_txt1">
						지원자 등록에 사용한 이메일로 인증 메일을 발송했습니다.<br />
						이메일을 확인하고 인증 링크를 클릭하시면 지원을 하실 수 있습니다.
					</p>
					
					  <a runat="server" class="btn_b_type4" ID="btnConfirm" >완료</a>
				</div>
				<div class="box_info1">
					<span class="txt">이메일이 도착하지 않은 경우, 이메일 재전송을 눌러주세요.</span>
					<asp:LinkButton class="btn_s_type2 ml10" runat="server" ID="btnReSend" OnClick="btnReSend_Click" >이메일 재전송</asp:LinkButton>
				</div>


			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

	</section>
    </asp:PlaceHolder>

    <!--// sub body -->


 


        
  

    

</asp:Content>