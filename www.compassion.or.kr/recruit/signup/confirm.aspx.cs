﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class recruit_join_confirm : FrontBasePage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		string code = Request["code"];
		string id = Request["id"];


        using (FrontDataContext dao = new FrontDataContext())
        {
            // 코드값 확인
            var exist = www6.selectQ<applicant_email_certification>("rec_code", code);
            //if (dao.applicant_email_certification.Any(p => p.rec_code == code))
            if(exist.Any())
            {
                // 인증여부 확인
                var entity = exist[0]; //dao.applicant_email_certification.First(p => p.rec_code == code);

                if (entity.rec_confirm_date == null)
                {
                    entity.rec_confirm_date = DateTime.Now;
                    //dao.SubmitChanges();
                    www6.update(entity);

                    confirm_success.Visible = true;

                    //var applicant = dao.applicant.First(p => p.ap_id == entity.rec_ap_id);
                    var applicant = www6.selectQF<applicant>("ap_id", entity.rec_ap_id);
                    //SendMail(applicant.ap_email, applicant.ap_name);

                    btnConfirm.HRef = "/recruit/login/?id=" + id;

                }
                else
                {
                    confirm_already.Visible = true;
                }

            }
            else
            {
                confirm_fail.Visible = true;
            }
        }
	}



	void SendMail(string to, string name) {

		try {

			var from = ConfigurationManager.AppSettings["recruitEmailSender"];

			var args = new Dictionary<string, string> {
				{"{name}" , name }
			};
			var title = "지원자 등록 완료 이메일";

			Email.Send(this.Context, from, new List<string>() { to },
				title, "/mail/recruit_signup.html",
				args
			, null);

		} catch (Exception e) {
			Response.Write(e.Message);
			throw e;
		}

	}

}