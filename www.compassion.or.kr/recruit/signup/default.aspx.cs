﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class recruit_signup_default : FrontBasePage {


	public class SignupInfo {
		public string name;
		public string email;
		public string uuid;
	}


	SignupInfo Signup {
		get{
			if (this.ViewState["signupInfo"] == null)
				this.ViewState["signupInfo"] = new SignupInfo().ToJson();
			return this.ViewState["signupInfo"].ToString().ToObject<SignupInfo>();
		}
		set{
			this.ViewState["signupInfo"] = value.ToJson();
		}
	}

	
	protected string id{
		set {
			this.ViewState.Add("id", value);
		}
		get {
			if (this.ViewState["id"] == null) return "";
			return this.ViewState["id"].ToString();
		}
	}


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();



		id = Request["id"];


		var href = "";
		if (id != "") {
			href = "/recruit/login/?id=" + id;
		} else {
			href = "/recruit/";
		}

		btnConfirm.HRef = href;

	}

	protected void btnRegist_Click(object sender, EventArgs e) {


		var ap_email = email.Text;
		var ap_name = name.Text;
		var ap_pwd = password.Text;
		var ap_phone = phone.Text;

        using (FrontDataContext dao = new FrontDataContext())
        {
            // 등록여부 확인하기
            var exist = www6.selectQ<applicant>("ap_email", ap_email);
            //if (dao.applicant.Any(p => p.ap_email == ap_email))
            if(exist.Any())
            {
                Response.Write("<script>alert('등록된 이메일입니다.');</script>");
                return;
            }
            
            try
            {

                var entity = new applicant()
                {
                    ap_email = ap_email,
                    ap_name = ap_name,
                    ap_phone = ap_phone,
                    ap_pwd = ap_pwd.SHA256Hash(),
                    ap_regdate = DateTime.Now
                };

                //dao.applicant.InsertOnSubmit(entity);
                www6.insert(entity);
                //dao.SubmitChanges();

                var uuid = Guid.NewGuid().ToString();
                var certification = new applicant_email_certification()
                {
                    rec_ap_id = entity.ap_id,
                    rec_code = uuid,
                    rec_regdate = DateTime.Now
                };
                //dao.applicant_email_certification.InsertOnSubmit(certification);
                www6.insert(certification);
                // 가입메일 발송
                SendMail(ap_email, ap_name, uuid);

                //dao.SubmitChanges();


                // 정보저장
                Signup = new SignupInfo() { name = ap_name, email = ap_email, uuid = uuid };

                before.Visible = false;
                after.Visible = true;

            }
            catch (Exception ee)
            {
                Response.Write(ee.Message);
            }



        }
	}

	void SendMail(string to, string name, string code) {

		try {

			var link = ConfigurationManager.AppSettings["domain"] + "/recruit/signup/confirm?code="+code+"&id="+id;
			var from = ConfigurationManager.AppSettings["recruitEmailSender"];
			/*
			var args = new List<KeyValuePair<string, string>>() {
						new KeyValuePair<string,string>("{name}" , name ) ,
						new KeyValuePair<string,string>("{link}" , link) ,
					};
					*/
			var args = new Dictionary<string, string> {
				{"{name}" , name } , {"{link}" , link }
			};
			var title = "[한국컴패션_인재채용] 지원자 등록 완료 인증메일 입니다.";

			

			Email.Send(this.Context, from, new List<string>() { to },
				title, "/mail/recruit_certification.html",
				args
			, null);

		} catch (Exception e) {
			Response.Write(e.Message);
			throw e;
		}

	}

	protected void btnReSend_Click(object sender, EventArgs e) {
		SendMail(Signup.email, Signup.name, Signup.uuid);
		Response.Write("<script>alert('인증 메일을 재전송하였습니다.');</script>");
	}

	protected void btnConfirm_Click(object sender, EventArgs e) {

	}
}