﻿var validate = {
	email: function (value) {
		if (value == "") {
			$("#email_validation_reuslt").text("정보를 입력해 주세요.").parent().show();
			$("#email_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1")
			//$("#email").focus();

			return false;
		}
		if (!emailValidation.check(value).result) {
			$("#email_validation_reuslt").text("유효한 이메일 주소가 아닙니다.").parent().show();
			$("#email_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1")
			//$("#email").focus();
			return false;
		}

		$.get("/api/recruit.ashx?t=check_email&email=" + value, function (r) {
			if (r.success) {
				$("#email_checked").val(1);
				$("#email_validation_reuslt").text("유효한 이메일 주소입니다.").parent().show();
				$("#email_validation_reuslt").addClass("guide_comment1").removeClass("guide_comment2")
				return true;
			} else {
				$("#email_checked").val(0);
				$("#email_validation_reuslt").text("이미 등록되어 있는 이메일입니다. 다시 한번 확인해주세요.").parent().show();
				$("#email_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1")
				//$("#email").focus();
				return false;
			}
		});
	},

	name: function (value) {
		if (value == "") {
			$("#name_validation_reuslt").text("정보를 입력해 주세요.").parent().show();
			$("#name_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1");
			//$("#name").focus();
			return false;
		}

		// 한글 영문만 가능
		if (!/^[가-힣a-zA-Z]+$/.test(value)) {
			$("#name_validation_reuslt").text("이름을 정확히 입력해주세요.").parent().show();
			$("#name_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1")
			//$("#name").focus();
			return false;
		}

		$("#name_validation_reuslt").text("").parent().hide();
		$("#name_validation_reuslt").addClass("guide_comment1").removeClass("guide_comment2");
		return true;
	},

	phone: function (value) {

		if (value == "") {
			$("#phone_validation_reuslt").text("정보를 입력해 주세요.").parent().show();
			$("#phone_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1");
			//$("#phone").focus();
			return false;
		}

		if (!/(01[016789])[-](\d{4}|\d{3})[-]\d{4}$/g.test(value)) {
			$("#phone_validation_reuslt").text("휴대폰 번호 양식이 맞지 않습니다. 10자 이상의 숫자만 입력 가능합니다.").parent().show();
			$("#phone_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1");
			//$("#phone").focus();
			return false;
		}

		$("#phone_validation_reuslt").text("사용 가능한 번호입니다.").parent().show();
		$("#phone_validation_reuslt").addClass("guide_comment1").removeClass("guide_comment2")
		return true;
	},

	password: function (value) {

		if (value == "") {
			$("#password_validation_reuslt").text("정보를 입력해 주세요.").parent().show();
			$("#password_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1")
			//$("#password").focus();
			return false;
		}

		if (value.length < 5 || value.length > 15) {
			$("#password_validation_reuslt").text("비밀번호는 띄어쓰기 없이 영문, 숫자, 특수문자 중 2가지 이상 조합으로 5~15자 이내(대소문자 구별)로 입력해주세요.").parent().show();
			$("#password_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1")
			//$("#password").focus();
			return false;
		}

		var validCnt = 0;
		// 영문 포함여부 확인
		if (value.match(/[a-zA-Z]+/g)) {
			validCnt++;
		}
		
		// 숫자 포함여부
		if (value.match(/[0-9]+/g)) {
			validCnt++;
		}

		// 특문 포함여부
		if (/[~!@\#$%<>^&*\()\-=+_\’]/gi.test(value)) {
			validCnt++;
		} 

		if (validCnt < 2) {
			$("#password_validation_reuslt").text("비밀번호는 띄어쓰기 없이 영문, 숫자, 특수문자 중 2가지 이상 조합으로 5~15자 이내(대소문자 구별)로 입력해주세요.").parent().show();
			$("#password_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1")
			//$("#password").focus();
			return false;
		}

		$("#password_validation_reuslt").text("사용 가능한 비밀번호입니다.").parent().show();
		$("#password_validation_reuslt").addClass("guide_comment1").removeClass("guide_comment2")
		return true;
	},

	re_password: function (value) {
		var password = $("#password").val();
		if (password == "") return;


		if (value == "") {
			$("#re_password_validation_reuslt").text("정보를 입력해 주세요.").parent().show();
			$("#re_password_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1")
			//$("#re_password").focus();
			return false;
		}

		if (value != password) {
			$("#re_password_validation_reuslt").text("비밀번호가 일치하지 않습니다. 다시 한번 확인해주세요.").parent().show();
			$("#re_password_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1")
			//$("#re_password").focus();
			return false;
		}

		$("#re_password_validation_reuslt").text("입력한 비밀번호와 일치합니다.").parent().show();
		$("#re_password_validation_reuslt").addClass("guide_comment1").removeClass("guide_comment2");
		return true;
	}

}


$(function () {
	
	$("#phone").mobileFormat();


	$("#email").focusout(function () {
		validate.email($(this).val());
	});

	$("#name").focusout(function () {
		validate.name($(this).val());
	});

	$("#phone").focusout(function () {
		validate.phone($(this).val());
	});

	$("#password").focusout(function () {
		validate.password($(this).val());
	})

	$("#re_password").focusout(function () {
		validate.re_password($(this).val());
	})

});



var goSubmit = function () {

	if ($("#email").val() == "") {
	    $("#email_validation_reuslt").text("정보를 입력해 주세요.").parent().show();
	    $("#email_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1");
	}

	if ($("#email").val() != "" && $("#email_checked").val() == "0") {
	    $("#email_validation_reuslt").text("유효한 이메일 주소가 아닙니다.").parent().show();
	    $("#email_validation_reuslt").addClass("guide_comment2").removeClass("guide_comment1");
	}

	if ($("#email").val() != "" && $("#email_checked").val() == "1") {
		$("#email_validation_reuslt").text("유효한 이메일 주소입니다.");
		$("#email_validation_reuslt").addClass("guide_comment1").removeClass("guide_comment2")
	}

	var result = true;

	if(!validate.name($("#name").val())) result = false;
	if(!validate.phone($("#phone").val())) result = false;
	if(!validate.password($("#password").val())) result = false;
	if(!validate.re_password($("#re_password").val())) result = false;

	if (!result) {
		return false;
	}


	if ($("#privacy:checked").length < 1) {
		alert('개인정보 수집 및 이용에 대해 동의해주세요.');
		return false;
	}


	if ($("#agree:checked").length < 1) {
		alert('민감정보 처리에 대해 동의해주세요.');
		return false;
	}

}