﻿(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, $sce, paramService) {

        $scope.requesting = false;
        $scope.total = -1;


        $scope.list = [];
        $scope.params = {
            page: 1,
            rowsPerPage: 10,
            top: -1
        };

        $scope.params = $.extend($scope.params, paramService.getParameterValues());

        if ($scope.params.s_type) {
			console.log(11)

            $("#category li a[data-category=" + $scope.params.s_type + "]").addClass("on");
        } else {
            $("#category li a[data-category=F]").addClass("on");
            $scope.params.s_type = "F";
        }

        if ($scope.params.k_word) {
            $("#k_word").val($scope.params.k_word);
            $scope.params.s_type = "";
        }

        // 검색
        $scope.search = function ($event) {
        	if ($event) {
        		$event.preventDefault();
        	}

        	$scope.params.s_type = "";
            //$scope.params = $.extend($scope.params, params);
            $scope.params.k_word = $("#k_word").val();
            $("#category li a").removeClass("on");
            $scope.getList($scope.params);
        }

        // 키워드
        $scope.keyword = function (params) {
            $scope.params = $.extend($scope.params, params);
            $("#k_word").val(params.k_word);
            params.s_type = "";
            $scope.getList(params);

        }



        // list
        $scope.getList = function (params) {
            // 카테고리 toggle
            if (params) {
                $scope.toggleCategory(params.s_type, params.top);
				$scope.params = $.extend($scope.params, params);
				$("#category li a[data-category=" + $scope.params.s_type + "]").addClass("on");
            }
            
            

            $http.get("/api/faq.ashx?t=list", { params: $scope.params }).success(function (result) {
                $scope.list = result.data;
                $scope.total = result.data.length > 0 ? result.data[0].total : 0;
                
            });
            if (params) scrollTo($("#l"));


        }

        $scope.toggleCategory = function (s_type, top) {
            $("#category li a").removeClass("on");

            $("#category li a[data-category=" + s_type + "]").addClass("on");

            $scope.params.page = 1;
        }


        // toggle
        $scope.toggle = function (item) {
            var question = $(".question" + item.f_id);
            var answer = $(".answer" + item.f_id);
            if (answer.is(":visible")) {
                question.removeClass("on");
                answer.slideUp(300);
            } else {
                $(".answer").hide();
                $(".a_wrap").slideUp(300);
                answer.slideDown(300);
                if ($(".q_wrap").hasClass("on")) {
                    $(".q_wrap").removeClass("on");
                }
                question.addClass("on");
            }
        }


        $scope.renderHtml = function (html) {
            return $sce.trustAsHtml(html);
        };
        
        $scope.getList();
        

    });

})();