﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="customer_faq_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

	<script type="text/javascript" src="/customer/faq/default.js"></script>
    <script type="text/javascript">
    </script>

    <script type="text/javascript">
        $(function () {

        })
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>FAQ</em></h1>
				<span class="desc">궁금하신 내용을 FAQ 검색창에서 검색하시거나 해당 카테고리를 클릭해 확인하실 수 있습니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 etc">

			<div class="faqSrch" id="l">
				<div class="w450">
					<div class="srchWrap">
						<label for="k_word" class="hidden">검색어 입력</label>
                        <input type="text" name="k_word" id="k_word" class="srch_input" placeholder="검색어를 입력하세요." ng-enter="search()" maxlength="30"/>
						<a href="#" class="srch_btn" ng-click="search($event)">검색</a>
                                
					</div>
					<ul class="hot clear2">
                        <asp:Repeater runat="server" ID="repeater_keyword">
                            <ItemTemplate>
                                <li><a ng-click="keyword({k_word:'<%#Eval("k_word") %>'})"><%#Eval("k_word") %></a></li>
                            </ItemTemplate>
                        </asp:Repeater>
					</ul>
				</div>
			</div>

            <div class="w980">
                <!-- tab -->
                <ul class="tab_type2 mb60" id="category">

                    <asp:Repeater runat="server" ID="repeater_category">
                        <ItemTemplate>
                            <li style="width:25%"><a ng-click="getList({s_type:'<%#Eval("cd_key") %>', top:-1})" data-category="<%#Eval("cd_key") %>"><%#Eval("cd_value") %></a></li>
                        </ItemTemplate>
                    </asp:Repeater>


                </ul>
                <!--//  -->


				<!-- faq -->
				<div class="faqTop">
					<span>구분</span>
					<span>제목</span>
				</div>
				<div class="faq">
					<!-- set -->
					<div class="faqSet" ng-repeat="item in list" ng-click="toggle(item)">
						<button class="q_wrap question{{item.f_id}}">
							<span class="q_icon">{{item.f_type_name}}</span>
							<span class="q_tit">{{item.f_question}}</span>
						</button>
						<div class="a_wrap answer{{item.f_id}}" ng-bind-html="renderHtml(item.f_answer)"></div>
					</div>
					<!--// -->

					<!--// -->
					<!-- no contents -->
					<div class="noContent" ng-hide="list.length">등록된 글이 없습니다.</div>
					<!--// -->
				</div>
				<!--// faq -->

				<!-- page navigation -->
				<div class="tac mb60">
					<paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  
				</div>
				<!--// page navigation -->

				<div class="conb">
					<p class="s_tit1 mb10">FAQ에서 원하는 답변을 찾지 못하셨나요?</p>
					<p class="s_con6">문의사항을 [1:1 문의하기]를 통해 남겨주시면 답변드리겠습니다.<br />
					더욱 빠른 답변을 원하시면 한국컴패션 대표번호 (02-740-1000/평일 9시~18시)로 연락 주시기 바랍니다.</p>
					<a href="/customer/qna/"><img src="/common/img/page/etc/personal_ask.jpg" alt="1:1문의 바로가기" /></a>
				</div>

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>

</asp:Content>