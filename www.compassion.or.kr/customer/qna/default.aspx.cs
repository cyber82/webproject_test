﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Data;

public partial class customer_qna_default : FrontBasePage {

	const int TABLE_INDEX= 4000;
	
	public override bool RequireLogin{
		get{
			return true;
		}
	}

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);

		// 개인정보
		var userInfo = new UserInfo();
		//lbUserId.Text = userInfo.UserId;
		lbUserName.Text = userInfo.UserName;
		

		// 타입
		foreach (var a in StaticData.Code.GetList(this.Context).Where(p => p.cd_display == true && p.cd_group == "qna_type").OrderBy(p => p.cd_order)) {
			qna_type.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}
		qna_type.Items.Insert(0, new ListItem("선택하세요", ""));

		// 파일 업로드 설정
		upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_qna);


		//개인정보 가져오기

		UserInfo sess = new UserInfo();

		if (sess.Email.EmptyIfNull() != "") {
			email.Text = sess.Email;
		}


		// 국내거주인경우 연락처를 컴파스에서 가져온다
		if (sess.LocationType == "국내") {

			var comm_result = new SponsorAction().GetCommunications();
			if (comm_result.success) {
				SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
				if ((comm_data.Mobile.TranslatePhoneNumber()).EmptyIfNull() != "") {
					phone.Text = comm_data.Mobile.TranslatePhoneNumber();
				}

			}
		}

	}


	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
	}


	protected void btnAdd_Click(object sender, EventArgs e)
    {
		if (base.IsRefresh)
        {
			return;
		}

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            //웹서비스 연결
            CommonLib.WWW6Service.SoaHelperSoap _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

            Object[] objSql = new object[1] { "select max(b_num), max(no_idx) from upboard" };

            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompassionWeb", objSql, "Text", null, null);

            var numMax = Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
            var idxMax = Int32.Parse(ds.Tables[0].Rows[0][1].ToString());

            //var numMax = www6.selectQ<upboard>().Max(p => p.b_num);
            //var idxMax = www6.selectQ<upboard>().Max(p => p.no_idx);

            var b_num = numMax == null ? 1 : numMax + 1;
            var no_idx = idxMax == null ? 1 : idxMax + 1;


            var entity = new upboard()
            {
                table_idx = TABLE_INDEX,
                no_idx = no_idx,
                b_num = b_num,
                @ref = b_num,
                b_name = userInfo.UserName,
                b_title = b_title.Text,
                b_content = b_content.InnerText.EscapeSqlInjection(),
                user_ip = Request.UserHostAddress,
                b_pwd = "compassion_zoppura",
                user_id = userInfo.UserId,
                notice_idx = "N",
                b_email = email.Text,
                b_readnum = 0,
                b_writeday = DateTime.Now,
                re_step = 0,
                re_level = 0,
                APP_ViewYN = 'N',
                tel = phone.Text,
                type = qna_type.SelectedValue,
                b_filename1 = upload_root.Value + file_path.Value
            };

            //dao.upboard.InsertOnSubmit(entity);
            www6.insert(entity);

            //dao.SubmitChanges();

            
            Response.Write("<script>alert('접수가 완료되었습니다.\\n회원님의 문의 내역 및 답변은\\n 마이컴패션>1:1문의 내역에서 언제든 확인하실 수 있습니다.');location.href='/my/qna/';</script>");
            //Response.Write("<script>alert('접수가 완료되었습니다.\\n고객님의 문의 내역 및 답변은\\n 마이컴패션>1:1문의 내역에서 언제든 확인하실 수 있습니다.\\n\\n※연휴 기간 접수되는 1:1 문의는 10/10(화) 이후부터 순차적으로 답변드리겠습니다. 감사합니다.');location.href='/my/qna/';</script>");
        }
    }
}