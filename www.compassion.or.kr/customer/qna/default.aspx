﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="customer_qna_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript">
        $(function () {
            var uploader = attachUploader("btn_file_path");
            uploader._settings.data.fileDir = $("#upload_root").val();
            uploader._settings.data.fileType = "file";

            $("#btn_remove").click(function () {
                $("#file_path").val("");
                $("#lb_file_path").val("");

                return false;
            });

            $("#b_content").textCount($("#content_count"), { limit: 2000 });

            $("#phone").mobileFormat();
            
            $("#qna_type").change(function () {
				if($(this).val() != ""){
            		$("#msg_qna").hide();
				}
            })

            $("#b_title").keyup(function () {
            	if ($("#b_title").val() != "") {
					$("#msg_title").hide();
            	}
            })

            $("#b_content").keyup(function () {
            	if ($("#b_content").val() != "") {
					$("#msg_content").hide();
            	}
            })


            $("#email").keyup(function () {
                if ($("#email").val() != "") {
                    $("#msg_email").hide();
                }
            })


            $("#phone").keyup(function () {
                if ($("#phone").val() != "") {
                    $("#msg_phone").hide();
                }
            })

            $("#lb_file_path").click(function (e) {
            	console.log("test");
            	e.preventDefault();
            	return false;
            })

            setTimeout(function () {
                $("[name=userfile]").attr("accept", ".jpg, .gif, .png, .jpeg")
            }, 300)

        });

        var attachUploader = function (button) {
            return new AjaxUpload(button, {
                action: '/common/handler/upload',
                responseType: 'json',
                onChange: function (file) {
                    var fileName = file.toLowerCase();

                    if (fileName.indexOf(".jpg") != -1 || fileName.indexOf(".jpeg") != -1 || fileName.indexOf(".gif") != -1 || fileName.indexOf(".png") != -1) {
                        return true;
                    } else {
                        alert("첨부파일은 이미지파일(jpg, jpeg, gif, png) 만 가능합니다.");
                        return false;
                    }
                },
                onSubmit: function (file, ext) {
                    this.disable();
                },
                onComplete: function (file, response) {

                    this.enable();

                    if (response.success) {
                        //$("#file_path").val(response.name);
                        $("#file_path").val(response.name.replace(/^.*[\\\/]/, ''));
                        $("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
                        //	$(".temp_file_size").val(response.size);

                    } else
                        alert(response.msg);
                }
            });
        }


        var onSubmit = function () {
            if ($("#qna_type").val() == "") {
                $("#msg_qna").html("문의 유형을 선택하세요").show();
                return false;
            }
            if ($("#b_title").val() == "") {
                $("#msg_title").html("제목을 입력하세요").show();
                return false;
            }

            if ($("#b_title").val().length < 4) {
                $("#msg_title").html("제목은 최소 4~40자 이내로 입력해주세요.").show();
                return false;
            }
            if ($("#b_content").val() == "") {
                $("#msg_content").html("문의내용을 입력하세요").show();
                return false;
            }

            if ($("#b_content").val().length < 10) {
                $("#msg_content").html("문의 내용은 최소 10자 이상 입력해주세요.").show();
                return false;
            }
            //if ($("#email").val() == "") {
            //    $("#msg_email").html("이메일을 입력하세요").show();
            //    return false;
            //}
            
            if (($("#email").val() == "" || $("#email").val() == null) == false
                && !/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($("#email").val())) {
                    $("#msg_email").html("이메일 형식이 맞지 않습니다. 다시 확인해주세요.").show();
                    $("#email").focus();
                    return false;
            }

            //if ($("#phone").val() == "") {
            //    $("#msg_phone").html("휴대폰번호를 입력하세요").show();
            //    return false;
            //}
            
            if (($("#phone").val() == "" || $("#phone").val() == null) == false
                && !/(01[016789])[-](\d{4}|\d{3})[-]\d{4}$/g.test($("#phone").val())) {
                $("#msg_phone").html("휴대폰 번호 양식이 맞지 않습니다. 10자 이상의 숫자만 입력 가능합니다.").show();
                $("#phone").focus();
                return false;
            }
            

            // 중복 등록을 막기 위해 Click 시 버튼 숨김
            $("#btnAdd").hide();
            loading.show("글을 등록 중입니다");
            

        }
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" runat="server" id="upload_root" value="" />
    <input type="hidden" runat="server" id="file_path" value="" />

    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>1:1문의</em></h1>
                <span class="desc">문의주신 내용은 빠른 시간 내 답변 드리도록 하겠습니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents etc">

            <div class="w980">
                <div class="tableWrap1 mb40">
                    <table class="tbl_type2">
                        <caption>1:1문의 테이블</caption>
                        <colgroup>
                            <col style="width: 20%" />
                            <col style="width: 80%" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <label for="qna_type"><span>문의 유형</span></label></th>
                                <td class="typeSel">
                                    <span class="sel_type2" style="width: 400px;">
                                        <asp:DropDownList class="custom_sel" runat="server" ID="qna_type"></asp:DropDownList>

                                    </span>
                                    <span class="guide_comment2 mt5" id="msg_qna" style="display: none">필수 선택 항목입니다.</span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><span>이름</span></th>
                                <td>
                                    <p>
                                        <asp:Literal runat="server" ID="lbUserName" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <label for="b_title"><span>제목</span></label></th>
                                <td class="titbox">
                                    <asp:TextBox runat="server" ID="b_title" MaxLength="40" class="input_type2" placeholder="제목을 입력해 주세요" Style="width: 740px" />
                                    <span class="guide_comment2 mt5" id="msg_title" style="display: none">필수 입력 항목입니다.</span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" class="aCon">
                                    <label for="b_content"><span>문의 내용</span></label></th>
                                <td>
									<textarea Rows="8" maxlength="2000" class="textarea_type2" runat="server" ID="b_content" placeholder="문의하실 내용을 입력해 주세요" ></textarea>
                                    <div class="textCount">
                                        <span class="guide_comment2" id="msg_content" style="display: none">필수 입력 항목입니다.</span>
                                        <span id="content_count">0</span>
                                        <span>/ 2000자</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <label for="photo"><span>사진 첨부</span></label></th>
                                <td>
                                    <div class="btn_attach clear2 relative">
                                        <input type="text" runat="server" id="lb_file_path" class="input_type2 fl mr10"  disabled style="width: 400px;background:#fdfdfd;" placeholder="선택된 파일이 없습니다" />
                                        <a href="#" class="btn_s_type3 fl mr10" id="btn_file_path"><span>파일선택</span></a>
                                        <a href="#" class="btn_s_type2 fl" id="btn_remove"><span>삭제</span></a>
                                    </div>
                                    <p class="pt5"><span class="s_con1">첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</span></p>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <label for="email"><span>이메일</span></label></th>
                                <td>
                                    <asp:TextBox runat="server" ID="email" MaxLength="100" class="input_type2" Style="width: 400px" />
                                    <div>
                                        <span class="guide_comment2 mt5" id="msg_email" style="display: none"></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <label for="phone"><span>휴대폰</span></label></th>
                                <td>
                                    <asp:TextBox runat="server" ID="phone" MaxLength="13" class="input_type2 number_only" Style="width: 400px;" />
                                    <div>
                                        <span class="guide_comment2 mt5" id="msg_phone" style="display: none"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="btn_ac">
                    <div>
                        <asp:LinkButton runat="server" ID="btnAdd" OnClick="btnAdd_Click" OnClientClick="return onSubmit();" class="btn_type1 fl mr10">확인</asp:LinkButton>
                        <a href="/customer/faq/" class="btn_type2 fl mr10">취소</a>
                    </div>
                </div>

            </div>
        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>
    <!--
     아이디
    <asp:Literal runat="server" ID="lbUserId" />
    -->




</asp:Content>
