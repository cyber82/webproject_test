﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="zipcode.aspx.cs" Inherits="popup_zipcode" %>

<!DOCTYPE html>
<html lang="ko" runat="server" id="html_lang">
<head runat="server">
    <title runat="server" id="title">나비엔메이트</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<script type="text/javascript" src="/common/js/jquery/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">
        $(function () {
            zipcode.init();
        });

        var zipcode = {
            apiUrl : 'http://www.juso.go.kr/addrlink/addrLinkUrl.do',
            isCallback: false,
            init: function () {
                isCallback = $("#inputYn").val() == 'Y';
                if (isCallback) {
                    zipcode.callback();
                } else {
                    zipcode.callApi();
                }
            },
            callApi: function () {
                var url = location.href;
                $("#returnUrl").val(url);
                $("#frm").attr("action", zipcode.apiUrl);
                $("#frm").submit();
            },

            callback: function () {
                var roadFullAddr = $("#roadFullAddr").val();
                var roadAddrPart1 = $("#roadAddrPart1").val();
                var addrDetail = $("#addrDetail").val();
                var roadAddrPart2 = $("#roadAddrPart2").val();
                var engAddr = $("#engAddr").val();
                var jibunAddr = $("#jibunAddr").val();
                var zipNo = $("#zipNo").val();
                var admCd = $("#admCd").val();
                var rnMgtSn = $("#rnMgtSn").val();
                var bdMgtSn = $("#bdMgtSn").val();

                var t = $("#t").val();
                var c = $("#c").val();
                opener.zipcodeCallback(roadFullAddr, roadAddrPart1, addrDetail, roadAddrPart2, engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn, t, c);
                window.close();
            }
        }

        
    </script>
</head>
<body>
    <form id="frm" name="frm" runat="server">
    <div>
        
        <input type="hidden" runat="server" id="t" name="t" value="" />
        <input type="hidden" runat="server" id="c" name="c" value="" />

		<input type="hidden" runat="server" id="confmKey" name="confmKey" value=""/>
		<input type="hidden" id="returnUrl" name="returnUrl" value=""/>


        <input type="hidden" runat="server" name="roadFullAddr" id ="roadFullAddr"/>
		<input type="hidden" runat="server" name="roadAddrPart1" id ="roadAddrPart1"/>
		<input type="hidden" runat="server" name="addrDetail" id ="addrDetail"/>
		<input type="hidden" runat="server" name="roadAddrPart2" id ="roadAddrPart2"/>
		<input type="hidden" runat="server" name="engAddr" id ="engAddr"/>
		<input type="hidden" runat="server" name="jibunAddr" id ="jibunAddr"/>
		<input type="hidden" runat="server" name="zipNo" id ="zipNo"/>
		<input type="hidden" runat="server" name="admCd" id ="admCd"/>
		<input type="hidden" runat="server" name="rnMgtSn" id ="rnMgtSn"/>
		<input type="hidden" runat="server" name="bdMgtSn" id ="bdMgtSn"/>
		<input type="hidden" runat="server" name="inputYn" id ="inputYn"/>
    </div>
    </form>
</body>
</html>
