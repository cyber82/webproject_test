﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class popup_zipcode : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e){
        roadFullAddr.Value = Request["roadFullAddr"].EmptyIfNull();
        roadAddrPart1.Value = Request["roadAddrPart1"].EmptyIfNull();
        addrDetail.Value = Request["addrDetail"].EmptyIfNull();
        roadAddrPart2.Value = Request["roadAddrPart2"].EmptyIfNull();
        engAddr.Value = Request["engAddr"].EmptyIfNull();
        jibunAddr.Value = Request["jibunAddr"].EmptyIfNull();
        zipNo.Value = Request["zipNo"].EmptyIfNull();
        admCd.Value = Request["admCd"].EmptyIfNull();
        rnMgtSn.Value = Request["rnMgtSn"].EmptyIfNull();
        bdMgtSn.Value = Request["bdMgtSn"].EmptyIfNull();
        inputYn.Value = Request["inputYn"].EmptyIfNull();

        t.Value = Request["t"].EmptyIfNull();
        c.Value = Request["c"].EmptyIfNull();

        confmKey.Value = ConfigurationManager.AppSettings["zipcode_apikey"];
    }
}