﻿<%@ WebHandler Language="C#" Class="popup_list" %>

using System;
using System.Web;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using drawing = System.Drawing;
using NLog;
using System.Net;
using System.Collections;
using CommonLib;
public class popup_list : IHttpHandler {

    public void ProcessRequest(HttpContext context) {

        var type = context.Request["type"].ValueIfNull("layer");


        JsonWriter.Write(this.getList(type), context);

    }

    List<sp_popup_list_fResult> getList(string type)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //return dao.sp_popup_list_f(type, "pc").ToList();
            Object[] op1 = new Object[] { "p_type", "p_device" };
            Object[] op2 = new Object[] { type, "pc" };
            var list = www6.selectSP("sp_popup_list_f", op1, op2).DataTableToList<sp_popup_list_fResult>();

            return list;
        }

    }

    public bool IsReusable {
        get {
            return false;
        }
    }
}