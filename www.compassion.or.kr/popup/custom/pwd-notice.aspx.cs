﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class popup_custom_pwd_notice : FrontBasePage {


    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();


    }

    protected void btn_pwd_change_Click( object sender, EventArgs e )
    {        
        UserInfo sess = new UserInfo();
        var userId = sess.UserId;
        var userPwd = user_pwd.Value;
        using (AuthDataContext dao = new AuthDataContext())
        {
            //var data = dao.sp_tSponsorMaster_get_f(userId, userPwd, "", "", "", "", "").ToList();
            Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
            Object[] op2 = new Object[] { userId, userPwd, "", "", "", "", "" };
            var data = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

            if (data.Count < 1)
            {
                base.AlertWithJavascript("현재 비밀번호가 일치하지 않습니다.");
            }
            else
            {
                //dao.sp_tSponsorMaster_change_pwd_f(userId, userPwd);
                Object[] op3 = new Object[] { "UserID", "UserPW" };
                Object[] op4 = new Object[] { userId, userPwd };
                www6.selectSPAuth("sp_tSponsorMaster_change_pwd_f", op3, op4);

                base.AlertWithJavascript("비밀번호 변경이 완료 되었습니다.", "window.close()");

            }
        }
    }
}
