﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="info-change.aspx.cs" Inherits="popup_custom_pwd_notice" MasterPageFile="~/top.Master" %>

<%@ MasterType VirtualPath="~/top.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/popup/popupManager.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js"></script>
    <script type="text/javascript">
        $(function () {
            $("header").remove()
            $("footer").remove()
            $(".pageTop").remove()

            $("#btn_close_today").click(function () {
                popupManager.setCookie("info", "1Day", 1);
                window.close();
            });

            $("#btn_pop_close").click(function () {
                window.close();
            });

            $("#my_info").click(function () {
            	opener.parent.location = "/my/";
            	window.close();
            });

        })

    </script>

</asp:Content>
<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    	
	<div class="pop_type1 w400">
		<div class="pop_title">
			<span>개인정보를 확인해주세요</span>
			<button style="display:none" class="pop_close" id="btn_pop_close"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content info_change">
			<p class="con_tit">후원자님의 주소 및 핸드폰,<br />이메일을 다시 한 번 확인해주세요</p>
			<p class="s_con6">
				그동안 후원자님께 보내드린 우편물이 반송되어<br />
				발송이 중지되었거나, 주소 및 핸드폰, 이메일이 미 입력<br />
				또는 잘못 입력되어 있습니다.<br />
				<br />
				마이컴패션에서 후원자님의 정보를<br />
				다시 한 번 확인해주시고, 그 동안 놓치고 있었던<br />
				컴패션의 다양한 후원 서비스를 받아보세요.
			</p>

			<div class="tac">
				<a href="#" id="my_info" class="btn_type1">내 정보 확인하기</a>
			</div>
		</div>
		<div class="bottom info_change">
			<span class="checkbox_ui inpop">
				<input type="checkbox" class="css_checkbox" id="btn_close_today" />
				<label for="btn_close_today" class="css_label font1">오늘 하루 동안 보지 않기</label>
			</span>
		</div>
	</div>
	

</asp:Content>
