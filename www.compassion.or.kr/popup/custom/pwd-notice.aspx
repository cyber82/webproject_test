﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pwd-notice.aspx.cs" Inherits="popup_custom_pwd_notice" MasterPageFile="~/top.Master" %>

<%@ MasterType VirtualPath="~/top.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/popup/popupManager.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js"></script>
    <script type="text/javascript">
        $(function () {
            $("header").remove()
            $("footer").remove()
            $(".pageTop").remove()

            $("#btn_change_next").click(function () {
                popupManager.setCookie("pwdChange", "30Days", 30);
                window.close();
            });

            $("#btn_pop_close").click(function () {
                window.close();
            });

            $("#new_pwd").keyup(function () {
                if ($("new_pwd").val() != "") {
                    $("#msg_pwd_re").hide();
                    $("#msg_pwd").hide();
                }
            });

            $("#new_pwd_re").keyup(function () {
                if ($("new_pwd_re").val() != "") {
                    $("#msg_pwd_re").hide();
                    $("#msg_pwd").hide();
                }
            });

            $("#btn_pwd_change").click(function () {
                
                if ($("#user_pwd").val() == "") {
                    $("#msg_current").show();
                    return false;
                } else {
                    $("#msg_current").hide();
                }


                if ($("#user_pwd").val() == $("#new_pwd").val()) {
                    $("#msg_pwd").show();
                    return false;
                }
                
                var pwd_result = passwordValidation.check($("#new_pwd"), $("#new_pwd_re"), 5, 15);

                if (!pwd_result.result) {

                    $("#new_pwd").focus();
                    $("#msg_pwd_alert").html(pwd_result.msg);
                    $("#msg_pwd_re").show();
                    
                    if (pwd_result.msg.indexOf("확인") == -1) {
                        $("#new_pwd").focus();
                    } else {
                        $("#new_pwd_re").focus();
                    }

                    return false;
                }
                return true;
            });
        })

    </script>

</asp:Content>
<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <!-- 일반팝업 width : 800 -->
    <div class="pop_type1 w800">
        <div class="pop_title">
            <span>비밀번호 변경 안내</span>
            <button class="pop_close" id="btn_pop_close" style="display:none;">
                <span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span>
            </button>
        </div>

        <div class="pop_content pwChange tac">

            <p class="s_tit6">주기적인 비밀번호 변경으로 소중한 개인정보를 지켜 주세요!</p>

            <p class="s_con3">
                후원자님은 6개월 동안 비밀번호를 변경하지 않으셨습니다.<br />
                안전한 개인정보 관리를 위해 <em class="fc_blue">6개월마다 비밀번호를 변경</em>해 주세요.
            </p>

            <div class="input_area">
                <div class="now_pw">
                    <label for="pw" class="hidden">현재비밀번호</label>
                    <input type="password" id="user_pwd" runat="server" class="input_type2 mb10" placeholder="현재비밀번호" style="width: 420px;" />
                    <div class="tal" style="display:none" id="msg_current" runat="server"><span class="guide_comment2 ">현재 비밀번호를 입력해 주세요.</span></div>
                </div>
                <div class="new_pw">
                    <label for="new_pwd" class="hidden">새비밀번호</label>
                    <input type="password" id="new_pwd" runat="server" class="input_type2 pw_input1" placeholder="새비밀번호 (띄어쓰기 없이 영문, 숫자, 특수문자 3가지 조합으로 5~15자)" style="width: 420px;" />
                    <label for="new_pwd_re" class="hidden">비밀번호 재확인</label>
                    <input type="password" id="new_pwd_re" runat="server" class="input_type2 pw_input1" placeholder="비밀번호 재확인" style="width: 420px;" />
                    <div class="tal" style="display:none" id="msg_pwd" runat="server"><span class="guide_comment2 mb10">현재 사용중인 비밀번호입니다.</span></div>
                    <div class="tal" style="display:none" id="msg_pwd_re">
                        <span class="guide_comment2 mb10" id="msg_pwd_alert">띄어쓰기 없이 영문, 숫자, 특수문자 3가지 조합으로<br />5~15자 이내(대소문자 구별)로 입력해 주세요.</span>
                    </div>
                </div>

				<!-- 모든 정보를 입력하지 않았을 때 -->
				<!--<span class="guide_comment2 posA">정보를 입력해 주세요.</span>-->
            </div>

            <div class="btn_ac">
                <a href="#" id="btn_change_next" class="btn_type2 mr8">다음에 변경하기</a>
                <asp:LinkButton class="btn_type1" runat="server" ID="btn_pwd_change" OnClick="btn_pwd_change_Click">비밀번호 변경하기</asp:LinkButton>
                <p><span class="s_con1 mt5">다음에 변경하기를 선택하시면 30일 동안 변경 안내 페이지가 나타나지 않습니다.</span></p>
            </div>

        </div>
    </div>
    <!--// popup -->
</asp:Content>
