﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;


public partial class popup_custom_pwd_notice_check : FrontBasePage
{
    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        if(UserInfo.IsLogin)
        {
            UserInfo sess = new UserInfo();
            using (AuthDataContext dao = new AuthDataContext())
            {
                //var entity = dao.tSponsorMaster.FirstOrDefault(p => p.UserID == sess.UserId);
                var entity = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);
                if (entity != null)
                {
                    // 비번 변경일 체크 (기존가입자는 null)
                    if (entity.PwdDate.HasValue)
                    {
                        if (DateTime.Compare(DateTime.Now.AddMonths(-6), Convert.ToDateTime(entity.PwdDate)) == 1)
                        {
                            pwd_change.Visible = true;
                        }
                        // 수정일 체크
                    }
                    else if (entity.ModifyDate.HasValue)
                    {
                        if (DateTime.Compare(DateTime.Now.AddMonths(-6), Convert.ToDateTime(entity.ModifyDate)) == 1)
                        {
                            pwd_change.Visible = true;
                        }
                        // 가입일 체크
                    }
                    else if (entity.RegisterDate.HasValue)
                    {
                        if (DateTime.Compare(DateTime.Now.AddMonths(-6), Convert.ToDateTime(entity.RegisterDate)) == 1)
                        {
                            pwd_change.Visible = true;
                        }
                    }
                }
            }
        }
    }

}
