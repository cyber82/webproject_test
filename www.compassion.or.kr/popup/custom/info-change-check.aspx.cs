﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text.RegularExpressions;

public partial class popup_custom_info_change_check : FrontBasePage {


    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();

        if(UserInfo.IsLogin) {
            UserInfo sess = new UserInfo();

            if (string.IsNullOrEmpty(sess.SponsorID)) return;

            if(sess.LocationType == "국내") {
                WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

                Object[] objSql3 = new object[1] { " SELECT * FROM tCommitmentMaster " +
                                               " WHERE StopDate IS NULL " +
                                               " AND SponsorID = '" + sess.SponsorID.ToString() + "'" };

                DataSet ds3 = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql3, "Text", null, null);

                if(ds3.Tables[0].Rows.Count > 0) {
                    #region 주소 확인
                    Object[] objSql2 = new object[1] { " SELECT * FROM tSponsorAddress " +
                                                   " WHERE CurrentUse = 'Y' " +
                                                   " AND SponsorID = '" + sess.SponsorID.ToString() + "'" };

                    DataSet ds2 = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "Text", null, null);

                    if(ds2.Tables[0].Rows.Count > 0) {
                        #region 주소가 2개인 사람들은 제외
                        if(ds2.Tables[0].Rows.Count > 1)
                            return;
                        #endregion

                        String Address = ds2.Tables[0].Rows[0]["Address2"].ToString();

                        String tmp = Regex.Replace(Address, @"[~!@\#$%^&*\()\=+|\\/:;?""<>']", "");

                        String pattern_1 = @"^[ㄱ-ㅎ]*$";
                        ;

                        if(!String.IsNullOrEmpty(tmp) && !Regex.IsMatch(Address, pattern_1)) {
                            if(Address.IndexOf("아파트") > -1) {
                                tmp = Regex.Replace(Address, @"\D", "");

                                if(tmp.Length == 0) {
                                    info_change.Visible = true;
                                    return;
                                }
                            }
                        } else {
                            info_change.Visible = true;
                            return;
                        }
                    } else {
                        info_change.Visible = true;
                        return;
                    }

                    #endregion

                    #region SMS 및 Email 캠페인 거부 걸려있는 후원자 제외
                    Object[] objSql4 = new object[1] { " SELECT * FROM tSponsorRefuse " +
                                                   " WHERE CommunicationType IN ('SMS', '이메일', '휴대번호', '전화번호') AND CurrentUse = 'Y' " +
                                                   " AND SponsorID = '" + sess.SponsorID.ToString() + "'" };

                    DataSet ds4 = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql4, "Text", null, null);

                    if(ds4.Tables[0].Rows.Count > 0)
                        return;
                    #endregion

                    #region 이메일, 휴대번호 확인
                    Object[] objSql = new object[1] { " SELECT * FROM tSponsorCommunication " +
                                                  " WHERE CurrentUse = 'Y' AND CommunicationType IN ('이메일', '휴대번호') " +
                                                  " AND SponsorID = '" + sess.SponsorID.ToString() + "'" +
                                                  " ORDER BY CommunicationType " };

                    DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

                    if(ds.Tables[0].Rows.Count > 0) {
                        if(ds.Tables[0].Rows.Count < 2) {
                            info_change.Visible = true;
                            return;
                        } else {
                            String Email = ds.Tables[0].Rows[0]["CommunicationContents"].ToString();

                            if(String.IsNullOrEmpty(Email)) {
                                info_change.Visible = true;
                                return;
                            }

                            //Regex regex = new Regex(@"^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$");
                            Regex regex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,6}$");
                            Match match = regex.Match(Email);

                            if(!match.Success) {
                                info_change.Visible = true;
                                return;
                            }

                            String PhoneNo = ds.Tables[0].Rows[1]["CommunicationContents"].ToString();

                            if(PhoneNo.Length != 10 && PhoneNo.Length != 11) {
                                info_change.Visible = true;
                                return;
                            }
                        }
                    } else {
                        info_change.Visible = true;
                        return;
                    }
                    #endregion

                    #region DM 수신상태 확인
                    Object[] objSql5 = new object[1] { " SELECT * FROM tSponsorRefuseReturn " +
                                                   " WHERE SponsorID = '" + sess.SponsorID.ToString() + "'" };

                    DataSet ds5 = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql5, "Text", null, null);

                    if(ds5.Tables[0].Rows.Count > 0) {
                        info_change.Visible = true;
                        return;
                    }
                    #endregion
                }
            }


        }

    }
}