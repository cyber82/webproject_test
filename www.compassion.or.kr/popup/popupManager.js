
var popupManager = {
    openAll: function (check_cookie, type) {
        $.get("/api/popup.ashx", function (r) {
        	var pop_cookie = [];

        	if (r.data) {
        		$.each(r.data, function () {
        			console.log(this);
        			popupManager.open(this, check_cookie);

        			if (popupManager.getCookie("popup_" + this.p_id) != "") {

        				pop_cookie.push("popup_" + this.p_id);
        			}

        		});

        		if (pop_cookie.length < 0 || r.data.length < 0 || pop_cookie.length == r.data.length) {
        			popupManager.openAdminPop(true, "layer");

        		}
        	}

 
        });

    },

    openAdminPop: function (check_cookie, type) {
        $.get("/popup/list.ashx?type=" + type, function (data) {

            $.each(data, function () {
                console.log(this);
                popupManager.open(this, check_cookie);
            });
        });
    },

    open: function (data, check_cookie) {
        var type = data.p_type;
        var title = data.p_name;
        var left = data.p_left;
        var top = data.p_top;
        var width = data.p_width;
        var height = parseInt(data.p_height);	// 20 = footer
        var pop_id = data.p_id;
        var content = data.p_content;
        var cookieId = "popup_" + pop_id;
        var rd = data.rd ? data.rd : 1;

        var enable = false;
        if (check_cookie) {
            if (popupManager.getCookie(cookieId) == "") {
                enable = true;
            }
        } else {
            enable = true;
        }

        if (enable) {

            if (type == "window") {


            } else if (type == "layer") {

                if (data.p_id == "temp") {
                    $.post("/popup/layer_view?c=" + pop_id, { title: title, cookieId: cookieId, content: popupManager.htmlEscape(content), width: width, height: height, left: left, top: top }, function (r) {
                        $("body").append(r);
                    });
                } else {
                    $.post("/popup/layer_view?c=" + pop_id, { cookieId: cookieId, content: popupManager.htmlEscape(content), width: width, height: height, left: left, top: top, rd: rd }, function (r) {

                        $("body").append(r);
                    });
                }

            } else if (type == "top") {
                /*
				$("#top_notice_section").empty();
				$($("#top_notice_section").parent()).css("height", "auto");
				$("#top_notice_section").append(content);
				$(".top_nomore_today").attr("data-id", cookieId);

				$(".top_nomore_today").unbind("click");
				$(".top_nomore_today").click(function () {

					if (data.p_id != "temp") {
						var cookieId = $(this).attr("data-id");
						var notToday = $(this).attr("data-set-cookie");
						if (notToday == "true") {
							popupManager.setCookie(cookieId, "today", 1);
						}
					}

					$($("#top_notice_section").parent()).animate({ height: "0px" }, 'fast', function () {
						$("#top_notice_section").empty();
					})

				});
				*/
            }

        }

    },

    htmlEscape: function (str) {
        return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    },

    setCookie: function (name, value, expiredays) {
        var todayDate = new Date();
        todayDate.setDate(todayDate.getDate() + expiredays);
        document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
    },

    getCookie: function (name) {
        var nameOfCookie = name + "=";
        var x = 0;
        while (x <= document.cookie.length) {
            var y = (x + nameOfCookie.length);
            if (document.cookie.substring(x, y) == nameOfCookie) {
                if ((endOfCookie = document.cookie.indexOf(";", y)) == -1)
                    endOfCookie = document.cookie.length;
                return unescape(document.cookie.substring(y, endOfCookie));
            }
            x = document.cookie.indexOf(" ", x) + 1;
            if (x == 0)
                break;
        }
        return "";
    }

}
