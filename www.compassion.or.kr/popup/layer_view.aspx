﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="layer_view.aspx.cs" Inherits="popup_layer_view" %>

<div class="layer_popup" data-id="<%=cookieId %>">
    <script type="text/javascript">

        $(function () {
            // 고정팝업 풋터삭제
            if ($(".layer_popup").data("id").length == 13) {
                $(".lp_footer").remove();
            }
            if ($("head").find("script[src='/popup/popupManager.js']").length < 1) {
                var s = $("script");
                s.attr("type", 'text/javascript');
                s.attr("src", '/popup/popupManager.js');
                $("head").append(s);
            }

            $(".nomore_today").unbind("click");
            $(".nomore_today").click(function () {
                
                var cookieId = $(this).attr("data-id");
                var notToday = $(this).attr("data-set-cookie");
                
                if (notToday == "true") {
                    popupManager.setCookie(cookieId, "today",<%:remindDays%>);
                    
                }
                $(".layer_popup[data-id='" + cookieId + "']").remove();

                // 관리자 팝업
                // 관리자 등록 팝업이 아닌 custom 팝업은 13자리 아이디를 가진다.
                if ($(this).data("id").length == 13) {
                    if (!$(".btn_lp_close").hasClass("fix_pop")) {
                        popupManager.openAdminPop(true, "layer");
                    }

                }

            });

            $(".btn_lp_close").unbind("click").bind("click", function () {
                var data_id = $(this).data("id");
                
                $(".layer_popup[data-id=" + data_id + "]").remove();

                // 관리자 팝업
                // 관리자 등록 팝업이 아닌 custom 팝업은 13자리 아이디를 가진다.
                if (!$(".btn_lp_close").hasClass("fix_pop")) {
                    if (data_id.length == 13) {
                        popupManager.openAdminPop(true, "layer");

                    }

                }
            });

        });

    </script>
    <style>
        /* 메인 레이어팝업 */
        .lp_wrap {
            width: auto;
            height: 70px;
            background: transparent;
            /*border: 1px solid #ff6e00;*/
            box-shadow: rgba(0,0,0,0.2) 3px 3px 3px;
        }

            .lp_wrap .lp_tit {
                width: 100%;
                height: 70px;
                background: #ff6e00;
                padding-top: 20px;
                box-sizing: border-box;
            }

                .lp_wrap .lp_tit span {
                    display: inline-block;
                    float: left;
                    font-size: 20px;
                    font-weight: 600;
                    color: #fff;
                    padding-left: 30px;
                }

                .lp_wrap .lp_tit a.lp_close {
                    display: inline-block;
                    float: right;
                    font-size: 20px;
                    font-weight: 600;
                    padding: 0 30px 0 0;
                }

            .lp_wrap .lp_con {
                display: inline-block;
                width: 100%;
                height: auto;
                background: #fff;
                padding: 0;
                text-align: center;
            }

                .lp_wrap .lp_con img {
                    display: inline;
                    vertical-align: middle;
                }

            .lp_wrap .lp_footer {
                width: 100%;
                height: 45px;
                background: #fff;
                font-size: 13px;
                color: #333;
                padding: 12px 0 0 0;
                border-top: 1px solid #e9e9e9;
                box-sizing: border-box;
                position: relative;
            }

                .lp_wrap .lp_footer span {
                    display: inline-block;
                    vertical-align: middle;
                }

                .lp_wrap .lp_footer .btn_lp_close {
                    position: absolute;
                    right: 20px;
                    top: 13px;
                }
    </style>


    <div class="lp_wrap" data-id="<%=cookieId %>" style="position: absolute; width: <%=p_width%>px; height: <%=p_height%>px; left: <%=p_left%>px; top: <%=p_top%>px; z-index: 100000; box-sizing: content-box;">

        <!-- 타이틀 높이값 : 71px -->


        <!-- 컨텐츠 -->
        <div id="container" class="lp_con" style="background-color: #ffffff; border-top-right-radius: 4px; border-top-left-radius: 4px;">
            <%=content %>
        </div>

        <!-- 푸터 높이값 : 45px -->
        <div class="lp_footer">
            <input type="checkbox" class="nomore_today" data-id="<%=cookieId %>" data-set-cookie="true" style="margin: 0 3px 0 20px;" />
            <span><%:msg %></span>
            <a href="#" class="btn_lp_close" data-id="<%=cookieId %>" title="닫기">닫기</a>
        </div>

    </div>

</div>


