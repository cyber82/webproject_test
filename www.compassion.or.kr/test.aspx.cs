﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TCPTModel;
using TCPTModel.Request.Hold.Beneficiary;
using TCPTModel.Request.Supporter;
using TCPTModel.Response.Supporter;

public partial class test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // 문희원 테스트용 추가
        string test = "";
        try
        {
            var sess = new UserInfo();

            //test += sponsorid + " " + childMasterId + " ";

            string childMasterId = Request.QueryString["childmasterid"];
            string sponsorid = Request.QueryString["sponsorid"];

            TCPTService.Service svc = new TCPTService.Service();
            svc.Timeout = 180000;

            test += "결연정보 조회 / ";
            //결연정보 조회
            DataSet ds = svc.GetCommitmentInfoForTCPT(sponsorid, childMasterId);

            if (ds == null || ds.Tables.Count == 0)
            {
                test += "결연정보 조회 실패 / ";
            }
            else
            {
                //결연정보 조회되지 않음
                if (ds.Tables[0].Rows.Count == 0)
                {
                    test += "결연정보 조회 되지 않음 / ";
                }
                else
                {
                    test += "결연정보 조회됨 / ";
                    // GlobalSponsorID  생성
                    DataRow dr = ds.Tables[0].Rows[0];

                    bool regSuccess = false;
                    string result = string.Empty;
                    string userid = "";
                    string userName = "";
                    string GlobalSponsorID = string.Empty;
                    string ChildGlobalID = dr["ChildGlobalID"].ToString();
                    string CommitmentID = dr["CommitmentID"].ToString();
                    string holdID = dr["HoldID"].ToString();
                    string holdUID = dr["HoldUID"].ToString();
                    string holdType = dr["HoldType"].ToString();
                    string conid = "54-" + dr["ConID"].ToString();
                    string firstName = dr["FirstName"].ToString();
                    string lastName = dr["LastName"].ToString();
                    string genderCode = dr["GenderCode"].ToString();
                    if (dr["SponsorGlobalID"].ToString() != "")
                    {
                        GlobalSponsorID = dr["SponsorGlobalID"].ToString();
                        regSuccess = true;
                    }

                    if (genderCode == "M")
                        genderCode = "Male";
                    else if (genderCode == "F")
                        genderCode = "Female";

                    if (string.IsNullOrEmpty(GlobalSponsorID))
                    {
                        test += "Sponsor GlobalID  생성 시작 / ";

                        try
                        {
                            SupporterCreateInSFCI_POST kit = new SupporterCreateInSFCI_POST();
                            SupporterProfile profile = new SupporterProfile();

                            profile.GlobalPartner = "Compassion Korea";
                            profile.CorrespondenceDeliveryPreference = "Digital";
                            profile.FirstName = firstName;
                            profile.LastName = lastName;
                            if (!string.IsNullOrEmpty(genderCode))
                                profile.Gender = genderCode;
                            profile.GPID = "54-" + conid;
                            profile.PreferredName = "";
                            profile.Status = "Active";
                            profile.StatusReason = "New";
                            profile.MandatoryReview = false;

                            kit.SupporterProfile.Add(profile);

                            string json = JsonConvert.SerializeObject(kit);
                            result = svc.SupporterCreateInSFCI_POST(json);
                            TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(result);
                            if (msg.IsSuccessStatusCode)
                            {
                                test += "Sponsor GlobalID  생성 성공 / ";
                                SupporterProfileResponse_Kit response = JsonConvert.DeserializeObject<SupporterProfileResponse_Kit>(msg.RequestMessage.ToString());
                                GlobalSponsorID = response.SupporterProfileResponse[0].GlobalID;

                                test += "Sponsor GlobalID  생성 성공 " + GlobalSponsorID + " / ";
                                test += "Sponsor GlobalID  업데이트 시작 / ";
                                bool b = svc.UpdateSponsorGlobalID(sponsorid, childMasterId);
                                if (b)
                                {
                                    test += "Sponsor GlobalID  업데이트 성공 / ";
                                }
                                else
                                {
                                    test += "Sponsor GlobalID  업데이트 실패 / ";
                                }
                            }
                            else
                            {
                                test += "Sponsor GlobalID  생성 실패 " + msg.RequestMessage.ToString() + " / ";
                                //오류
                            }
                        }
                        catch (Exception ex)
                        {
                            test += "Sponsor GlobalID  생성 오류 " + ex.Message + "/ ";
                        }
                    }


                    test += "no money hold 로 업데이트 시작 / ";
                    //  no money hold 로 업데이트
                    if (!string.IsNullOrEmpty(GlobalSponsorID))
                    {
                        string endDT = DateTime.Now.AddMonths(3).ToString("yyyy-MM-dd");

                        try
                        {
                            BeneficiaryHoldRequestList hKit = new BeneficiaryHoldRequestList();
                            hKit.Beneficiary_GlobalID = ChildGlobalID;
                            hKit.BeneficiaryState = "No Money Hold";
                            hKit.HoldEndDate = endDT + "T23:59:59Z";
                            hKit.IsSpecialHandling = false;
                            hKit.PrimaryHoldOwner = conid;
                            hKit.GlobalPartner_ID = "KR";
                            hKit.HoldID = holdID;

                            string hJson = JsonConvert.SerializeObject(hKit);

                            result = svc.BeneficiaryHoldSingle_PUT(ChildGlobalID, hKit.HoldID, hJson);
                            TCPTResponseMessage updateResult = JsonConvert.DeserializeObject<TCPTResponseMessage>(result);
                            if (!updateResult.IsSuccessStatusCode)
                            {
                                test += "no money hold 로 업데이트 성공 / ";
                                test += "기존 hold  expired  처리 시작 / ";
                                // 기존 hold  expired  처리
                                bool b = svc.UpdateHoldStatus(new Guid(holdUID), holdID, hKit.BeneficiaryState, endDT, "Expired", "2000", "", sponsorid, GlobalSponsorID);

                                if (b)
                                {
                                    test += "기존 hold  expired  처리 성공 / ";
                                }
                                else
                                {
                                    test += "기존 hold  expired  처리 실패 / ";
                                }
                                // 신규  hold insert
                                Guid newHoldUID = Guid.NewGuid();
                                test += "신규 hold insert / ";
                                b = svc.InsertHoldHistory(newHoldUID, holdID, ChildGlobalID, hKit.BeneficiaryState, endDT, sponsorid, "", "2000", "web 어린이 결연");
                                if (b)
                                {
                                    test += "신규 hold insert 성공 / ";
                                }
                                else
                                {
                                    if (b)
                                    {
                                        test += "신규 hold insert 성공 / ";
                                    }
                                    else
                                    {
                                        test += "신규 hold insert 실패 / ";
                                    }
                                }

                                // TCPT_CommitmentTemp 테이블에 insert
                                test += "TCPT_CommitmentTemp 테이블에 insert / ";
                                string s = svc.InsertTCPT_CommitmentTemp(CommitmentID, sponsorid, childMasterId, newHoldUID.ToString(), holdID, sponsorid, "");
                                test += "TCPT_CommitmentTemp 테이블에 insert 결과 : " + s + " / ";

                            }
                            else
                            {
                                // hold update error
                                test += "no money hold 로 업데이트 실패 " + updateResult.RequestMessage.ToString() + " / ";
                            }
                        }
                        catch (Exception ex)
                        {
                            test += "no money hold 로 업데이트 오류 " + ex.Message + " / ";
                        }
                    }

                }
            }
        }
        catch (Exception ex)
        {
            test += "오류 오류 " + ex.Message + " / ";
        }
        txtTest.InnerText = test; 
    }
}