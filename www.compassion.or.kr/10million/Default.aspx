﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="million_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <style type="text/css">   
        ul.photo { overflow:hidden; margin:48px auto 95px; width:960px;height:350px;}
        ul.photo li { position:relative;float:left;padding:42px 22px 0;margin-left:30px;width:200px;height:290px; }
        ul.photo li img {width:246px;height:269px;}
        ul.photo li .phoDeco {position:absolute;top:0;left:0;width:261px;height:273px;background:url("/image/Mobile/with2_photo_bg.png") 0 0 no-repeat; }
        ul.photo li .btnView {background:url("/image/Mobile/paradise_btn.jpg") 0 0 no-repeat;}
    
        li{list-style:none; word-break:keep-all; word-break:break-word;}
    </style>
</head>
<body style="margin:-5px -5px -5px -5px; padding:-5px -5px -5px -5px " >
    <form id="form1" runat="server">
        <div>          
            <div id="child_box1" runat="server" style="position:absolute;margin-top:871px;width:210px;height:350px;left:40px; border:0px solid red;">
                <a href="<%=href1 %>"><img id="child_img1" runat="server" src="<%=img1 %>" alt="" style="width:220px;height:290px; " /></a><br />
                <table id="table_1" runat="server" border="0" style="margin-top:7px;margin-left:5px;width:210px;">
					<tbody>
						<tr>
							<th style="width:40%;"><span style="font-size:80%;">이름</span></th>
							<td><span style="font-size:90%;"><%=name1 %></span></td>
						</tr>
                        <tr>
							<th style="width:40%;"><span style="font-size:80%;">생일</span></th>
							<td><span style="font-size:90%;"><%=birth1 %></span></td>
						</tr>
						<tr>
							<th style="width:40%;"><span style="font-size:80%;">국가</span></th>
							<td><span style="font-size:90%;"><%=country1 %></span></td>
						</tr>
					</tbody>
				</table>
                <a href="<%=href1 %>"><img id="child_btn1" runat="server" src="/image/Mobile/compassion_btn.jpg" alt="" style="width:152px;height:42px;margin-left:30px;margin-top:26px;" /></a>
            </div>

            <div id="child_box2" runat="server" style="position:absolute;margin-top:871px; left:288px;width:250px;height:275px; border:0px solid red;">
                <a href="<%=href2 %>"><img id="child_img2" runat="server" src="<%=img2 %>" alt="" style="width:220px;height:290px; " /></a>
                <table id="table1" runat="server" border="0" style="margin-top:3px;margin-left:5px;width:210px;">
					<tbody>
						<tr>
							<th style="width:40%;"><span style="font-size:80%;">이름</span></th>
							<td><span style="font-size:90%;"><%=name2 %></span></td>
						</tr>
                        <tr>
							<th style="width:40%;"><span style="font-size:80%;">생일</span></th>
							<td><span style="font-size:90%;"><%=birth2 %></span></td>
						</tr>
						<tr>
							<th style="width:40%;"><span style="font-size:80%;">국가</span></th>
							<td><span style="font-size:90%;"><%=country2 %></span></td>
						</tr>
					</tbody>
				</table>
                <a href="<%=href2 %>"><img id="child_btn2" runat="server" src="/image/Mobile/compassion_btn.jpg" alt="" style="width:152px;height:42px;margin-left:30px;margin-top:26px;" /></a>
            </div>

            <div id="child_box3" runat="server" style="position:absolute;margin-top:871px;left:535px;width:250px;height:275px; border:0px solid red;">
                <a href="<%=href3 %>"><img id="child_img3" runat="server" src="<%=img3 %>" alt="" style="width:220px;height:290px; " /></a>
                <table id="table2" runat="server" border="0" style="margin-top:3px;margin-left:5px;width:210px;">
					<tbody>
						<tr>
							<th style="width:40%;"><span style="font-size:80%;">이름</span></th>
							<td><span style="font-size:90%;"><%=name3 %></span></td>
						</tr>
                        <tr>
							<th style="width:40%;"><span style="font-size:80%;">생일</span></th>
							<td><span style="font-size:90%;"><%=birth3 %></span></td>
						</tr>
						<tr>
							<th style="width:40%;"><span style="font-size:80%;">국가</span></th>
							<td><span style="font-size:90%;"><%=country3 %></span></td>
						</tr>
					</tbody>
				</table>
                <a href="<%=href3 %>"><img id="child_btn3" runat="server" src="/image/Mobile/compassion_btn.jpg" alt="" style="width:152px;height:42px;margin-left:30px;margin-top:26px;" /></a>
            </div>

            <img id="imageid" runat="server" src="image/10million_main.jpg" alt="" />
        </div>
    </form>

    <script type="text/javascript">
        var size = {
            width: window.innerWidth || document.body.clientWidth,
            height: window.innerHeight || document.body.clientHeight
        }

        var yourImg = document.getElementById('imageid');

        var width = window.innerWidth;
        var height = width * (1400 / 800);

        if (yourImg && yourImg.style) {
            yourImg.style.width = width + 'px';
        }

        child_box1 = document.getElementById("child_box1");
        child_box1.style.marginTop = (height * 0.64) + "px";
        child_box1.style.width = (width * 0.37) + "px";
        child_box1.style.height = (height * 0.27) + "px";
        child_box1.style.left = (width * 0.09) + "px";

        child_img1 = document.getElementById("child_img1");
        child_img1.style.width = (width * 0.25) + "px";
        child_img1.style.height = (height * 0.19) + "px";

        child_btn1 = document.getElementById("child_btn1");
        child_btn1.style.width = (width * 0.21) + "px";
        child_btn1.style.height = (height * 0.025) + "px";
        child_btn1.style.marginLeft = (width * 0.02) + "px";
        child_btn1.style.marginTop = (height * 0.005) + "px";

        child_box2 = document.getElementById("child_box2");
        child_box2.style.marginTop = (height * 0.64) + "px";
        child_box2.style.width = (width * 0.312) + "px";
        child_box2.style.height = (height * 0.233) + "px";
        child_box2.style.left = (width * 0.375) + "px";

        child_img2 = document.getElementById("child_img2");
        child_img2.style.width = (width * 0.25) + "px";
        child_img2.style.height = (height * 0.19) + "px";

        child_btn2 = document.getElementById("child_btn2");
        child_btn2.style.width = (width * 0.21) + "px";
        child_btn2.style.height = (height * 0.025) + "px";
        child_btn2.style.marginLeft = (width * 0.02) + "px";
        child_btn2.style.marginTop = (height * 0.005) + "px";

        child_box3 = document.getElementById("child_box3");
        child_box3.style.marginTop = (height * 0.64) + "px";
        child_box3.style.width = (width * 0.312) + "px";
        child_box3.style.height = (height * 0.233) + "px";
        child_box3.style.left = (width * 0.655) + "px";

        child_img3 = document.getElementById("child_img3");
        child_img3.style.width = (width * 0.25) + "px";
        child_img3.style.height = (height * 0.19) + "px";

        child_btn3 = document.getElementById("child_btn3");
        child_btn3.style.width = (width * 0.21) + "px";
        child_btn3.style.height = (height * 0.025) + "px";
        child_btn3.style.marginLeft = (width * 0.02) + "px";
        child_btn3.style.marginTop = (height * 0.005) + "px";
    </script>
    
</body>

</html>