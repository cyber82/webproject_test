﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class million_Default : System.Web.UI.Page
{
    protected string img1 = string.Empty;
    protected string img2 = string.Empty;
    protected string img3 = string.Empty;

    protected string href1 = string.Empty;
    protected string href2 = string.Empty;
    protected string href3 = string.Empty;

    protected string ChildMasterID1 = string.Empty;
    protected string ChildMasterID2 = string.Empty;
    protected string ChildMasterID3 = string.Empty;

    protected string ChildDetailID1 = string.Empty;
    protected string ChildDetailID2 = string.Empty;
    protected string ChildDetailID3 = string.Empty;

    protected string name1 = string.Empty;
    protected string name2 = string.Empty;
    protected string name3 = string.Empty;

    protected string birth1 = string.Empty;
    protected string birth2 = string.Empty;
    protected string birth3 = string.Empty;

    protected string country1 = string.Empty;
    protected string country2 = string.Empty;
    protected string country3 = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

        Object[] objSql = new object[1] { "sp_get_t10millionCampaign" };
        Object[] objParam = new object[0] { };
        Object[] objValue = new object[0] { };

        DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 2)
            {
                string ChildKey1 = ds.Tables[0].Rows[0]["ChildKey"].ToString();
                string ChildKey2 = ds.Tables[0].Rows[1]["ChildKey"].ToString();
                string ChildKey3 = ds.Tables[0].Rows[2]["ChildKey"].ToString();

                ChildMasterID1 = ds.Tables[0].Rows[0]["ChildMasterID"].ToString();
                ChildMasterID2 = ds.Tables[0].Rows[1]["ChildMasterID"].ToString();
                ChildMasterID3 = ds.Tables[0].Rows[2]["ChildMasterID"].ToString();

                ChildDetailID1 = ds.Tables[0].Rows[0]["ChildDetailID"].ToString();
                ChildDetailID2 = ds.Tables[0].Rows[1]["ChildDetailID"].ToString();
                ChildDetailID3 = ds.Tables[0].Rows[2]["ChildDetailID"].ToString();

                img1 = "http://ws.compassion.or.kr/Files/Child/" + ChildKey1.ToString().Substring(0, 2) + "/" + ChildKey1.ToString().Trim() + ".jpg";
                img2 = "http://ws.compassion.or.kr/Files/Child/" + ChildKey2.ToString().Substring(0, 2) + "/" + ChildKey2.ToString().Trim() + ".jpg";
                img3 = "http://ws.compassion.or.kr/Files/Child/" + ChildKey3.ToString().Substring(0, 2) + "/" + ChildKey3.ToString().Trim() + ".jpg";

                href1 = "/Mobile/cdspDetail3.aspx?ChildMasterID=" + ChildMasterID1.Trim();
                href2 = "/Mobile/cdspDetail3.aspx?ChildMasterID=" + ChildMasterID2.Trim();
                href3 = "/Mobile/cdspDetail3.aspx?ChildMasterID=" + ChildMasterID3.Trim();

                child_img1.Src = img1;
                child_img2.Src = img2;
                child_img3.Src = img3;

                //lblName1.Text = ds.Tables[0].Rows[0]["ChildName"].ToString();
                name1 = ds.Tables[0].Rows[0]["ChildName"].ToString();
                name2 = ds.Tables[0].Rows[1]["ChildName"].ToString();
                name3 = ds.Tables[0].Rows[2]["ChildName"].ToString();

                //lblBirthDate1.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["BirthDay"].ToString()).ToString("yyyy-MM-dd");
                birth1 = Convert.ToDateTime(ds.Tables[0].Rows[0]["BirthDay"].ToString()).ToString("yyyy-MM-dd");
                birth2 = Convert.ToDateTime(ds.Tables[0].Rows[1]["BirthDay"].ToString()).ToString("yyyy-MM-dd");
                birth3 = Convert.ToDateTime(ds.Tables[0].Rows[2]["BirthDay"].ToString()).ToString("yyyy-MM-dd");

                //lblCountryCode1.Text = ds.Tables[0].Rows[0]["ChildCountry"].ToString();
                country1 = ds.Tables[0].Rows[0]["ChildCountry"].ToString();
                country2 = ds.Tables[0].Rows[1]["ChildCountry"].ToString();
                country3 = ds.Tables[0].Rows[2]["ChildCountry"].ToString();
            }

            else
            {
                //ds = WWWService.Childlist_BirthMonth();

                string ChildKey1 = ds.Tables[0].Rows[0]["ChildKey"].ToString();
                string ChildKey2 = ds.Tables[0].Rows[1]["ChildKey"].ToString();
                string ChildKey3 = ds.Tables[0].Rows[2]["ChildKey"].ToString();

                ChildMasterID1 = ds.Tables[0].Rows[0]["ChildMasterID"].ToString();
                ChildMasterID2 = ds.Tables[0].Rows[1]["ChildMasterID"].ToString();
                ChildMasterID3 = ds.Tables[0].Rows[2]["ChildMasterID"].ToString();

                img1 = "http://ws.compassion.or.kr/Files/Child/" + ChildKey1.ToString().Substring(0, 2) + "/" + ChildKey1.ToString().Trim() + ".jpg";
                img2 = "http://ws.compassion.or.kr/Files/Child/" + ChildKey2.ToString().Substring(0, 2) + "/" + ChildKey2.ToString().Trim() + ".jpg";
                img3 = "http://ws.compassion.or.kr/Files/Child/" + ChildKey3.ToString().Substring(0, 2) + "/" + ChildKey3.ToString().Trim() + ".jpg";

                href1 = "/Mobile/cdspDetail3.aspx?ChildMasterID=" + ChildMasterID1.Trim() + "&EventType=VONVON";
                href2 = "/Mobile/cdspDetail3.aspx?ChildMasterID=" + ChildMasterID2.Trim() + "&EventType=VONVON";
                href3 = "/Mobile/cdspDetail3.aspx?ChildMasterID=" + ChildMasterID3.Trim() + "&EventType=VONVON";

                child_img1.Src = img1;
                child_img2.Src = img2;
                child_img3.Src = img3;
            }
        }
    }
}