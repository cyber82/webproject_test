﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="menu.ascx.cs" Inherits="my_menu" %>
<% 
    bool ADMINCK = (bool)ViewState["ADMINCK"];
    bool OVERSEACK = (bool)ViewState["OVERSEACK"];
%>



<!-- 후원자 이름 -->
<div class="topInfo">
	<div>
		<span class="name"><em><asp:Literal runat="server" ID="userName" /></em> 후원자님</span>
		<asp:PlaceHolder runat="server" ID="ph_conid">
		<span class="bar"></span>
		<span class="txt">후원자 번호</span>
		<span class="number"><asp:Literal runat="server" ID="conid" /></span>
			</asp:PlaceHolder>
	</div>

	<div class="btn">
        <% if (!ADMINCK){ %>
		<a href="/my/qna" class="qna">문의내역<span class="new" style="display:none"></span></a>
        <%} %>
		<a href="/my/account" class="modify">개인정보수정</a>
	</div>
</div>
<!--// -->

<!-- 탭메뉴 -->
<div class="box_type4 myTab">
	<ul class="mt_D1">
		<li><a href="/my/children/">나의 어린이</a></li>
		<li><a href="/my/sponsor/" data-depth2="sponsor">후원관리</a></li>
        <li><a href="/my/letter/" <%if (!ADMINCK){%>data-depth2="letter"<%} %> >편지</a></li>

        <% if (!ADMINCK) { %>
		<li><a href="/my/user-funding/" data-depth2="user-funding">나의 펀딩관리</a></li>
        <%} %>
		<li><a href="/my/activity/" data-depth2="activity">나의 참여/활동</a></li>
        <% if (!ADMINCK){ %>
		<li><a href="/my/store/" data-depth2="store" >스토어 주문내역</a></li>
        <%} %>
	</ul>

	<ul class="mt_D2 padding1" data-depth2="sponsor" style="display:none">
		<li><a href="/my/sponsor/commitment/">정기후원 신청내역</a></li>

		<li><a href="/my/sponsor/payment/">후원금 내역</a></li>
		<li><a href="/my/sponsor/pay-account/">납부방법 변경</a></li>
        <% if (OVERSEACK){ %>
            <li><a href="/my/sponsor/pay-oversea/">해외카드 결제</a></li>
        <%} %>
		<li><a href="/my/sponsor/pay-delay/">지연된 후원금</a></li>
		<li><a href="/my/sponsor/pay-receipt/">기부금 영수증</a></li>
		<li><a href="/my/sponsor/gift-money/">어린이 선물금</a></li>
	</ul>

	<ul class="mt_D2 padding2" data-depth2="letter" style="display:none">
		<% if (!ADMINCK){ %>
        <li><a href="/my/letter/" data-urls="/my/letter/view/~">편지함</a></li>
		<li><a href="/my/letter/write/" data-urls="/my/letter/write-pic/">편지쓰기</a></li>
        <li><a href="/my/letter/smart-letter/">스마트레터 서비스 신청</a></li>
        <%} %>
	</ul>

	<ul class="mt_D2 padding3" data-depth2="user-funding" style="display:none">
		<li><a href="/my/user-funding/join" >참여한 펀딩</a></li>
		<li><a href="/my/user-funding/create">개설한 펀딩</a></li>

	</ul>

	<ul class="mt_D2 padding4" data-depth2="activity" style="display:none">
		<li><a href="/my/activity/cad">CAD 추천 관리</a></li>
		<li><a href="/my/activity/stamp">스탬프 투어</a></li>
        <li><a href="/my/activity/visiontrip">현지방문 관리</a></li>
        <% if (!ADMINCK){ %>
		<li><a href="/my/activity/event">캠페인/이벤트 참여내역</a></li>
        <%} %>
	</ul>

	<ul class="mt_D2 padding5" data-depth2="store" style="display:none">
		<li><a href="/my/store/order/">주문/배송 내역</a></li>
		<li><a href="/my/store/review/" data-urls="/my/store/qna/|/my/store/reviewable/">나의 후기/문의</a></li>
	</ul>
	

</div>

<script type="text/javascript">
	$(function () {

		$(".mt_D1 a").mouseenter(function () {
		
			$(".mt_D2").hide();
			var depth2 = $(this).data("depth2");
			$(".mt_D2[data-depth2='" + depth2 + "']").show();
		});

		$(".myTab").mouseleave(function () {
			$(".mt_D2").hide();
			var depth2 = $(".mt_D1 a.selected").data("depth2");
			$(".mt_D2[data-depth2='" + depth2 + "']").show();
		});

		var url = location.pathname;
		$.each($(".myTab .mt_D1 a"), function (r) {
			if (url.indexOf($(this).attr("href")) > -1) {
				$(this).addClass("on").addClass("selected");

				var depth2 = $(this).data("depth2");
				var d2 = $(".mt_D2[data-depth2='" + depth2 + "']");
				
				if (d2.length > 0) {
					d2.show();

					var selected_d2 = null;
					$.each(d2.find("a"), function (r) {

						if (!selected_d2) {
							if ($(this).attr("href").indexOf(url) > -1) {
								selected_d2 = $(this);
								$(this).addClass("on").addClass("selected");
								return;
							}
						}

							
					})

					if (!selected_d2) {
						$.each(d2.find("a"), function (r) {

							var self = $(this);
							
							var urls = self.data("urls");
							if (!urls){
								return;
							} 
							
							var arr = urls.split('|');
							$.each(arr , function(){
							
								var me = this + '';
								if (me.indexOf(url) > -1) {
									selected_d2 = self;
									self.addClass("on").addClass("selected");
								}
								// ~ 로 끝나면 depth 한단계 들어감
								if (me.endsWith("~")) {
									
									var url2 = url.substr(0, url.lastIndexOf("/")+1);
									
									if (me.indexOf(url2) > -1) {
										selected_d2 = self;
										self.addClass("on").addClass("selected");
									}
									
								}

							})
							

						})
					}
				}

			}
		})
	})

</script>
<!--// -->
