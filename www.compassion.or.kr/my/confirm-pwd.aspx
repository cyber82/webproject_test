﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="confirm-pwd.aspx.cs" Inherits="my_confirm_pwd" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript">
        $(function () {
            $("#btn_submit").click(function () {
                if ($("#pwd").val() == "") {
                    $("#msg_pwd_2").text("비밀번호를 입력해 주세요.").show();
                    return false;
                }
                return true;
            }),
            	$("#pwd").keypress(function (e) {
            	    if (e.keyCode == 13) {
            	        location.href = $("#btn_submit").attr("href");
            	        return false;

            	    }
            	})

            $("#pwd").keyup(function () {
                $("#msg_pwd_2").hide();
                $("#msg_pwd").hide();
            });
        });

                
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="return_url" />
    <!-- sub body -->
    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>마이컴패션</h1>
                <span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents mypage">

            <div class="w980 myInfo">

                <uc:menu runat="server" />

                <!-- content -->
                <div class="box_type4">

                    <!-- 서브타이틀 -->
                    <div class="sub_tit">
                        <p class="tit">비밀번호 확인</p>
                    </div>
                    <!--// -->

                    <div class="modify_pw">
                        <p class="txt">안전한 개인 정보 보호를 위해 비밀번호를 다시 한 번 확인하고 있습니다.</p>
                        <div class="tac">
                            <input type="password" runat="server" id="pwd" class="input_type2" placeholder="비밀번호" style="width: 480px" />
                            <input type="text" class="input_type2" style="display:none"/>
                        </div>
                    </div>

                    <div class="tac">
                        <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="btn_type1 mb30">확인</asp:LinkButton>
                        <p>
                            <span class="guide_comment2" id="msg_pwd" runat="server" visible="false">비밀번호가 일치하지 않습니다. 다시 입력해주세요.</span>
                            <span class="guide_comment2" id="msg_pwd_2" style="display:none">비밀번호가 일치하지 않습니다. 다시 입력해주세요.</span>

                        </p>
                    </div>


                </div>
                <!--// content -->


            </div>

            <div class="h100"></div>
        </div>
        <!--// e: sub contents -->




    </section>
    <!--// sub body -->






</asp:Content>

