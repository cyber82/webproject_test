﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_user_funding_create : FrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		hd_image_domain.Value = ConfigurationManager.AppSettings["domain_file"];
		hd_userpic_upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_user_pic);

	}

}