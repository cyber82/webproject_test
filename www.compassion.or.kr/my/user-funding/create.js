﻿$(function () {
	$page.init();
});

var $page = {

	init : function(){
	
		setDatePicker($(".date"), function () {
			// dateValidate 가 있으면 호출안됨
		});

		$("#date_begin").dateRange({
			buttons: ".dateRange",	// preset range
			end: "#date_end",
			eventBubble: true,
			onClick: function (r) {
			}
		});

		$("#date_begin").dateValidate({
			end: "#date_end",
			onSelect: function () {

			}
		});

		var uploader = $page.attachUploader("btn_UserPic");
		uploader._settings.data.fileDir = $("#hd_userpic_upload_root").val();
		uploader._settings.data.fileType = "image";
		uploader._settings.data.limit = 512;

	},

	updateUserPic : function(path){
		$.post("/api/sponsor.ashx", { t: "update-userpic", path: path });
	} , 

	attachUploader: function (button) {
		return new AjaxUpload(button, {
			action: '/common/handler/upload',
			responseType: 'json',
			onChange: function () {
			},
			onSubmit: function (file, ext) {
				this.disable();
			},
			onComplete: function (file, response) {

				this.enable();

				if (response.success) {
					
					if (button == "btn_UserPic") {
						
						var url = $("#hd_image_domain").val() + response.name;
						$("span.pic").css({"background-image" : "url('"+ url +"')"});

						$page.updateUserPic(response.name);

					}
				} else
					alert(response.msg);
			}
		});
	}

};

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

	
		$scope.total = -1;
		$scope.list = null;
		$scope.ing = null;
		$scope.summary = null;
		$scope.today = new Date();

		$scope.params = {
			page: 1,
			rowsPerPage: 4,
			date_begin: "",
			date_end: ""
		};

		// 기본 날짜 세팅
		var date_begin = new Date();
		date_begin.setMonth(date_begin.getMonth() - 1);
		$scope.date_begin = date_begin;

		$scope.date_end = new Date();
		$("#date_begin").val($filter('date')($scope.date_begin, "yyyy-MM-dd"));
		$("#date_end").val($filter('date')($scope.date_end, "yyyy-MM-dd"));

		$scope.getSummary = function () {

			$http.get("/api/my/user-funding.ashx?t=summary", { params: $scope.params }).success(function (r) {

				if (r.success) {
					$scope.summary = r.data;
					//console.log("summary", $scope.summary);

				} else {
					alert(r.message);
				}
			});
		}

		$scope.getList = function (params) {

			$scope.params = $.extend($scope.params, params);
			$scope.params.date_begin = $("#date_begin").val();
			$scope.params.date_end = $("#date_end").val();

			$http.get("/api/my/user-funding.ashx?t=create-list", { params: $scope.params }).success(function (r) {

				if (r.success) {
					var list = r.data;

					console.log("list", list);

					$.each(list, function () {
						this.uf_date_start = new Date(this.uf_date_start);
						this.uf_date_end = new Date(this.uf_date_end);
						this.uf_regdate = new Date(this.uf_regdate);
						this.uf_type_name = this.uf_type == "normal" ? "양육을 돕는 펀딩" : "어린이 결연 펀딩";
						this.status = new Date() > this.uf_date_end ? "종료" : "진행중";
						
					});

					if (list.length > 0 && $scope.params.page == 1) {
						var entity = $.extend({}, list[0]);
						if (entity.uf_date_end > $scope.today)
							$scope.ing = entity;
					}


					$scope.list = list;
					$scope.total = r.data.length > 0 ? r.data[0].total : 0;
                    console.log($scope.ing)
				} else {
					alert(r.message);
				}
			});
		}

		$scope.search = function ($event) {
			$event.preventDefault();
			$scope.params.page = 1;
			$scope.getList();
		}

		$scope.showChildPop = function ($event, item) {
		    $http.get("/api/tcpt.ashx?t=get-user-funding&childMasterId=" + item.childmasterid).success(function (r) {
                
		        if (r.success) {
		            if ($event) $event.preventDefault();
		            loading.show();
		            var childinfo = r.data;
		            childinfo.birthdate = new Date(r.data.birthdate);
		            
		            popup.init($scope, "/common/child/pop/" + childinfo.countrycode + "/" + childinfo.childmasterid + "/" + childinfo.childkey + "?fn=hide", function (modal) {
		                modal.show();
		                $("#btn_commitment").hide();
		                initChildPop($http, $scope, modal, childinfo);

		            }, { top: 0, iscroll: true, removeWhenClose: true });
		        }
		    })
		}


		$scope.getSummary();
		
		$scope.getList();

	});

})();