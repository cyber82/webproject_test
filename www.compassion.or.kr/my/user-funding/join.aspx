﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="join.aspx.cs" Inherits="my_user_funding_join" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
	<script type="text/javascript" src="/my/user-funding/join.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다.</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980 funding">

				<uc:menu runat="server"  />

				<!-- -->
				<div class="box_type4 mb10">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit">
						<p class="tit noline">참여한 펀딩</p>
					</div>
					<!--// -->

				</div>

				<!-- 펀딩현황 -->
				<div class="mb10 relative">
					<div class="myinfo">
						<span class="pic" background-img="{{summary.creator_image}}" data-default-image="/common/img/page/my/no_pic.png" style="background-repeat:no-repeat;background-size:cover;background-position:center center;"></span>
						<p class="name">{{summary.creator_name}}</p>
						<ul class="clear2">
							<li>
								<span>참여펀딩</span><br /><span class="fs15">{{summary.cnt_join | number:N0}}</span>
							</li>
							<li>
								<span>개설펀딩</span><br /><span class="fs15">{{summary.cnt_create | number:N0}}</span>
							</li>
						</ul>
					</div>

					<div class="box_type4 myFunding">
						
						<p class="s_tit2 relative">최근 참여한 펀딩 현황
							<span class="news">
								<span class="update"><a ng-href="/sponsor/user-funding/view/{{latest.uf_id}}?tab=2">업데이트 소식 ({{(latest.uf_cnt_notice || 0) | number:N0}})</a></span>
							</span>
						</p>

						<!-- 참여한 펀딩이 있는 경우 -->
						<div class="detail clear2" ng-if="total > 0">
							<div class="pic" >
								<fill-img data-image="{{latest.uf_image}}" data-default-image="/common/img/page/my/no_pic.png" />
							</div>
							<div class="con">
								<p class="tit"><a ng-href="/sponsor/user-funding/view/{{latest.uf_id}}"><span style="color:#005dab">{{latest.uf_title}}<span class="d_day">D-{{ latest.uf_date_end | amDifference : today : 'days' }}</span></span></a></p>
								<ul>
									<li><span class="field">펀딩구분</span><span class="data">{{latest.uf_type_name}}</span></li>
									<li><span class="field">진행기간</span><span class="data">{{latest.uf_date_start | date:'yyyy.MM.dd'}} ~ {{latest.uf_date_end | date:'yyyy.MM.dd'}} </span></li>
									<li><span class="field">목표금액</span><span class="data">{{latest.uf_goal_amount | number:N0}}원({{latest.uf_current_amount / latest.uf_goal_amount|percentage:0}})</span></li>
									<li><span class="field">후원금액</span><span class="data">{{latest.uf_current_amount | number:N0}}원</span></li>
								</ul>
								<a ng-href="/sponsor/user-funding/view/{{latest.uf_id}}?tab=2" class="btn_s_type2">업데이트 소식 보기</a>
							</div>
						</div>
						<!--//-->
						
						<!-- 참여한 펀딩이 없는 경우 -->
						
						<div class="no_recent" ng-if="total == 0">
							<span>최근 참여한 나눔펀딩이 없습니다.<br />다양한 나눔 펀딩에 참여해 보시겠어요?</span><br />
							<a href="/sponsor/user-funding/" class="btn_type1 mt20">나눔펀딩 보기</a>
						</div>
						
						<!--//-->
					</div>
				</div>
				<!--// 펀딩현황 -->

				<!-- 누적현황 -->
				<div class="box_type4 accrue mb40">

					<span class="tit">누적 현황</span>&nbsp;&nbsp;<span class="fs15">({{today | date:'yyyy.MM.dd'}} 기준)</span>

					<p class="txt">누적 현황 총 <span class="price">{{summary.amount_join | number:N0}}</span><span class="won">원</span>현재까지 참여한 펀딩의 총 누적금액입니다.</p>
				</div>
				<!--// 누적현황 -->

				<!-- 참여한 펀딩내역 -->
				<p class="s_tit3 mb15">참여한 펀딩 내역</p>
				<div class="box_type4">
					
					<!-- 조회기간선택 -->
					<div class="box_type3 search_period mb40" id="l">
						<span class="tit">조회기간</span>
						
						<span class="radio_ui">
							<input type="radio" id="period_1" name="period" class="css_radio dateRange" checked data-day="0" data-month="1"/>
							<label for="period_1" class="css_label mr30">1개월</label>

							<input type="radio" id="period_2" name="period" class="css_radio dateRange" data-day="0" data-month="2"/>
							<label for="period_2" class="css_label mr30">2개월</label>

							<input type="radio" id="period_3" name="period" class="css_radio dateRange" data-day="0" data-month="3"/>
							<label for="period_3" class="css_label mr30">3개월</label>
						</span>

						<span>
							<input type="text" id="date_begin" class="input_type2 date begin" value="{{date_begin| date:'yyyy-MM-dd'}}" style="width:130px" />
							<button class="calendar btn_calendar" onclick="$('#date_begin').trigger('focus');return false;"></button>
							~&nbsp;&nbsp;
							<input type="text" id="date_end" class="input_type2 date end" value="{{date_end| date:'yyyy-MM-dd'}}" style="width:130px" />
							<button class="calendar btn_calendar " onclick="$('#date_end').trigger('focus');return false;"></button>
						</span>

						<a href="#" class="btn_s_type2 ml5 daterange"  data-from="date_begin" data-end="date_end" ng-click="search($event)">조회</a>
					</div>
					<!--// 조회기간선택 -->

					<div class="tbl_sort mb15">
						<span class="number">총 <em class="fc_blue">{{total}}</em>건</span>

						
					</div>
					<div class="tableWrap1 mb30">
						<table class="tbl_type6 padding1">
							<caption>참여한 펀딩내역 테이블</caption>
							<colgroup>
								<col style="width:18%" />
								<col style="width:34%" />
								<col style="width:16%" />
								<col style="width:16%" />
								<col style="width:16%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">펀딩구분</th>
									<th scope="col">제목</th>
									<th scope="col">후원일자</th>
									<th scope="col">후원금액</th>
									<th scope="col">상태</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in list">
									<td>{{item.uf_type_name}}</td>
									<td class="tit"><a ng-href="/sponsor/user-funding/view/{{item.uf_id}}">{{item.uf_title}}</a></td>
									<td>{{item.uu_regdate | date:'yyyy.MM.dd'}}</td>
									<td class="price">{{item.uu_amount | number:N0}}원</td>
									<td>{{item.status}}<span ng-if="item.uf_current_amount >= item.uf_goal_amount">(후원성공)</span></td>
								</tr>
							
								<tr ng-if="total == 0">
									<td colspan="5" class="no_content">등록된 내역이 없습니다.</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<!-- page navigation -->
					<div class="tac mb80 relative">
						<paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
						<!--
						<a href="#" class="btn_type3 posR">후원 내역 보기</a>
						-->
					</div>
					<!--// page navigation -->

				</div>
				<!--// 참여한 펀딩내역 -->

				
			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>
	
</asp:Content>

