﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
	<script type="text/javascript" src="/my/store/order/default.js"></script>
    

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" id="hdSponsorId" runat="server"  />
	<input type="hidden" id="hdBirthDate" runat="server"  />
	<input type="hidden" id="hdUserName" runat="server"  />
	

     <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다.</span>
                <uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->
    <!-- s: sub contents -->
		<div class="subContents mypage" >

			<div class="w980 myOrder">
                <uc:menu runat="server"  />

				<!-- content -->
				<div class="box_type4">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit mb30"  id="l">
						<p class="tit">주문/배송 내역</p>
					</div>
					<!--// -->
					<!-- 조회기간선택 -->
					<div class="box_type3 search_period mb40">
						<span class="tit">조회기간</span>
						
						<span class="radio_ui">
							<input type="radio" id="period_1" class="css_radio dateRange" name="chk_radio" data-day="0" data-month="1" checked="checked">
							<label for="period_1" class="css_label mr30">1개월</label>

							<input type="radio" id="period_2" class="css_radio dateRange" name="chk_radio"  data-day="0" data-month="2"/>
							<label for="period_2" class="css_label mr30">2개월</label>

							<input type="radio" id="period_3"class="css_radio dateRange" name="chk_radio"  data-day="0" data-month="3"/>
							<label for="period_3" class="css_label mr30">3개월</label>
						</span>

						<span>
							<input type="text" id="date_begin" class="input_type2 date begin" value="{{params.date_begin| date:'yyyy-MM-dd'}}" style="width:130px" />
							<button class="calendar btn_calendar" onclick="$('#date_begin').trigger('focus');return false;"></button>
							~&nbsp;&nbsp;

							<input type="text" id="date_end" class="input_type2 date end" value="{{params.date_end| date:'yyyy-MM-dd'}}" style="width:130px" />
							<button class="calendar btn_calendar" onclick="$('#date_end').trigger('focus');return false;"></button>
						</span>

						<a class="btn_s_type2 ml5 daterange" data-from="date_begin" data-end="date_end" ng-click="getList()">조회</a>
					</div>
					<!--// 조회기간선택 -->
					

					<!-- 주문/배송내역리스트 -->
					<div class="tbl_sort mb15">
						<span class="number">총 <em class="fc_blue">{{total}}</em>건</span>
					</div>
					<div class="tableWrap1 mb30">
						<table class="tbl_type6 order padding1">
							<caption>주문/배송내역리스트 테이블</caption>
							<colgroup>
								<col style="width:20%" />
								<col style="width:13%" />
								<col style="width:31%" />
								<col style="width:18%" />
								<col style="width:18%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">주문일자/번호</th>
									<th scope="col" colspan="2">상품정보</th>
									<th scope="col">결제금액</th>
									<th scope="col">진행상황</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in list">
									<td>{{item.dtreg | date:'yyyy.MM.dd'}}<br /><a ng-click="modal.show(item.orderno)" class="fc_gray2">{{item.orderno}} {{item.repcorrespondenceid == null ? '' : '(선물편지)'}}</a></td>
									<td class="prdt_pic">
										<!-- 제품이미지 사이즈 : 98 * 98 -->
										<div><a target="_blank" ng-click="modal.show(item.orderno)"><img ng-src="{{item.img}}" alt="상품 썸네일 이미지" width="98px;"/></a></div>

									</td>
									<td class="tit">
										<a ng-click="modal.show(item.orderno)" class="prdt_name" target="_blank">{{item.producttitle}}</a>
										<p class="prdt_option">(옵션) {{item.optiondetail}}</p>
									</td>
									<td>{{item.settleprice  | currency : "" : 0}}원</td>
									<td >{{item.statename}}<br /><a ng-href="{{item.href}}" target="_blank" class="btn_s_type3 mt10" ng-show="item.statename == '출고완료' && (item.deliveryco < 4 || item.deliveryco == 5)">배송조회</a></td>
								</tr>
							
								<tr ng-if="total == 0">
									<td colspan="5" class="no_content" >
										<p class="mb20">주문하신 내역이 없습니다.</p>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// 후원금내역 리스트 -->

					<!-- page navigation -->
					<div class="tac mb60">
						<paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
					</div>
					<!--// page navigation -->

					<div class="box_type5 mb30">
						<ul class="delivery_info clear2">
							<li class="step1">
								<span class="icon">주문완료</span>
								<p class="tit">회원님의 주문 및 NHN KCP<br />결제 내역이 확인 되었습니다.</p>
								<p class="con">주문확인 문자발송/결제 영수증<br />메일 발송 오후 3시 30분 당일출고<br />(커피 오후 1시 30분 마감)</p>
							</li>
							<li class="step1">
								<span class="icon">출고완료</span>
								<p class="tit">상품이 출고되어 택배사로<br />전달하였습니다.</p>
								<p class="con">출고 다음 날 배송조회를 통해<br />배송상황을 확인하실 수 있습니다.</p>
							</li>
							<li class="step1">
								<span class="icon">주문취소</span>
								<p class="tit">회원님의 요청으로 주문 및<br />결제가 취소된 경우입니다.</p>
								<p class="con">주문취소 관련 문의는<br />담당자 (02-3668-3434)에게<br />연락 주시기 바랍니다.</p>
							</li>
						</ul>
					</div>

					<div class="box_type3">
						<ul>
							<li><span class="s_con1">주문이 정상적으로 완료되지 않으면 내역 조회가 되지 않습니다.</span></li>
							<li><span class="s_con1">배송조회는 상품이 출고된 다음날부터 가능합니다.</span></li>
							<li><span class="s_con1">NHN KCP는 신용카드 및 계좌이체 결제대행 서비스를 제공하는 회사 이름입니다.</span></li>
							<li><span class="s_con1">배송 / 반품 / 교환 / 환불 안내 <button class="link2 delivery_pop" ng-click="modalDetail.show($event)"><span>[자세히보기]</span></button> 담당자 전화 : 02-3668-3434</span></li>
						</ul>
					</div>

				</div>
				<!--// content -->

				
			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

    </section>
    <!--// sub body -->
    
</asp:Content>

