﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_default : FrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		
		// 비회원결제시 결제한 내역 동기화 
		
		var sess = new UserInfo();
		hdSponsorId.Value = sess.SponsorID;
		//var birth = sess.Birth.Substring(0, 10);
		//hdBirthDate.Value = birth.Replace("-", "");
		hdUserName.Value = sess.UserName;


	}

}