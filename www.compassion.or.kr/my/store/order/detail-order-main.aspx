﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="detail-order-main.aspx.cs" Inherits="my_store_order_detail_order_main"  %>
<div style="background: transparent;" class="fn_pop_container" id="bluebookWrapper">
<div class="pop_type1 w800 fn_pop_content" style="margin-top:50px; margin-bottom: 50px;">
	
    <!-- 일반팝업 width : 800 -->
	<div class="pop_type1 w800">
		<div class="pop_title">
			<span>주문 상세 정보</span>
			<button class="pop_close" ng-click="storeModal.close($event)"><span><img src="/common/img/btn/close_1.png"  alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content">
			
			<div class="tableWrap1">
				<table class="tbl_type6 order padding1">
					<caption>주문/배송내역리스트 테이블</caption>
					<colgroup>
						<col style="width:15%" />
						<col style="width:31%" />
						<col style="width:14%" />
						<col style="width:10%" />
						<col style="width:16%" />
						<col style="width:14%" />
					</colgroup>
					<thead>
						<tr>
							<th scope="col" colspan="2">주문내용</th>
							<th scope="col">판매가</th>
							<th scope="col">수량</th>
							<th scope="col">상품구분</th>
							<th scope="col">구매가격</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="item in storeModal.detail_list">
							<td class="prdt_pic">
								<!-- 제품이미지 사이즈 : 98 * 98 -->
								<div><a ng-click="goView(item.product_idx, $event)" target="_blank"><img ng-src="{{item.name_img}}" width="98px;" alt="상품 썸네일 이미지" /></a></div>

							</td>
							<td class="tit">
								<a ng-click="goView(item.product_idx, $event)" class="prdt_name" target="_blank">{{item.producttitle}}</a>
								<p class="prdt_option">(옵션) {{item.optiondetail}}</p>
                                <p ng-show="item.childname != ''" ng-bind-html="item.childname"></p>
							</td>
							<td class="tar">{{item.price | currency : "" : 0 }}원</td>
							<td>{{item.qty}}</td>
							<td ng-bind-html="item.product_type"></td>
							<td class="tar">{{item.totalprice | currency : "" : 0 }}원</td>
						</tr>
	
					</tbody>
				</table>
			</div>

		</div>
	</div>
	<!--// popup -->

</div>
</div>