﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="detail-delivery.aspx.cs" Inherits="my_store_order_detail_delivery"  %>
<div style="background: transparent;" class="fn_pop_container" id="bluebookWrapper">
<div class="pop_type1 w800 fn_pop_content" style="height:950px;padding-top:50px">
	
    <!-- 일반팝업 width : 800 -->
	<div class="pop_type1 w800">
		<div class="pop_title">
			<span>배송 안내</span>
			<button class="pop_close" ng-click="modalDetail.close($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content delivery">
			
			<div class="category cat1">
				<div class="tit">배송 안내</div>
				<div class="con">
					<ul>
						<li><span class="s_con7">3만원 이상 구매 시 무료배송 / 3만원 미만 구매 시 배송비 2,500원</span></li>
						<li><span class="s_con7">오후 3시 30분 이전 결제가 완료 된 주문은 당일 출고됩니다.</span></li>
						<li><span class="s_con7">컴패션 커피는 오후 1시 30분 마감입니다.<br />미리 볶아두지 않고 주문 받은 후 로스팅하여 출고됩니다. 따라서 평일에만 주문/출고되며,<br />공휴일 전날 주문하시면 공휴일이 지난 다음 날 로스팅하여 출고됩니다.</span></li>
						<li><span class="s_con7">토요일/공휴일 제외한 평균 배송기간은 출고일로부터 1~2일 소요됩니다.<br />(단, 지역별/업체별 상황에 따라 배송 예정일이 변경될 수 있습니다.)</span></li>
						<li><span class="s_con7">도서산간 지역(제주도 포함), 오지 일부 지역은 운임이 추가될 수 있습니다.<br />(해당되신 분들께는 별도 연락을 드립니다.)</span></li>
						<li><span class="s_con7">택배 관련 문제/사고 발생 시 담당자(02-3668-3434)에게 연락 주시기 바랍니다.</span></li>
					</ul>
				</div>
			</div>

			<div class="category cat2">
				<div class="tit">반품/교환 안내</div>
				<div class="con">
					<p>상품수령 후 7일 이내에 신청하실 수 있으며, 반품/교환 신청은 담당자(02-3668-3434)에게 연락주시기 바랍니다. 단, 다음의 경우 해당하는 반품/교환 신청이 불가능 할 수 있습니다.</p>
					<ul>
						<li><span class="s_con7">소비자의 책임 있는 사유로 상품 등이 멸실 또는 훼손된 경우<br />(단지, 상품확인을 위한 포장 훼손 제외)</span></li>
						<li><span class="s_con7">소비자의 사용 또는 소비에 의해 상품 등의 가치가 현저히 감소한 경우</span></li>
						<li><span class="s_con7">시간의 경과에 의해 재판매가 곤란할 정도로 상품 등의 가치가 현저히 감소한 경우</span></li>
						<li><span class="s_con7">복제가 가능한 상품 등의 포장을 훼손한 경우</span></li>
						<li><span class="s_con7">소비자의 주문에 따라 개별적으로 생산되는 상품이 제작에 들어간 경우</span></li>
					</ul>
				</div>
			</div>

			<div class="category cat3">
				<div class="tit">환불 안내</div>
				<div class="con">
					<ul>
						<li><span class="s_con7">주문 취소 등의 사유로 환불이 발생 될 경우 담당자(02-3668-3434)에게 <br />연락 주시기 바랍니다.</span></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<!--// popup -->

</div>