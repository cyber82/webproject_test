﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_store_reviewable_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
    <script type="text/javascript" src="/my/store/reviewable/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <!-- sub body -->
    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>마이컴패션</h1>
                <span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다.</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents mypage">

            <div class="w980 myOrder">

                <!-- 후원자 이름 -->
                <uc:menu runat="server" />
                <!--// -->

                <!-- content -->
                <div class="box_type4">

                    <!-- 서브타이틀 -->
                    <div class="sub_tit mb30" id="l">
                        <p class="tit">나의 후기/문의</p>
                    </div>
                    <!--// -->

                    <!-- 탭메뉴 -->
                    <ul class="tab_type1 mb30">
                        <li style="width: 33%"><a href="/my/store/review/">나의 구매후기</a></li>
                        <li style="width: 34%"><a href="/my/store/qna/">나의 상품문의</a></li>
                        <li style="width: 33%" class="on"><a href="/my/store/reviewable/">후기 작성 가능한 상품내역</a></li>
                    </ul>
                    <!--// -->

                    <!-- 후기작성가능리스트 -->
                    <div class="tbl_sort tar mb15">
                        <span class="txt1">최근 3개월 이내 내역</span>
                    </div>
                    <div class="tableWrap1 mb30">
                        <table class="tbl_type6 padding2">
                            <caption>후기작성가능 리스트 테이블</caption>
                            <colgroup>
                                <col style="width: 20%" />
                                <col style="width: 13%" />
                                <col style="width: 37%" />
                                <col style="width: 15%" />
                                <col style="width: 15%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">주문번호</th>
                                    <th scope="col">등록일</th>
                                    <th scope="col">제목</th>
                                    <th scope="col">결제금액</th>
                                    <th scope="col">상품후기작성</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in list">
                                    <td>{{item.orderno}}</td>
                                    <td>{{item.dtreg  | date:'yyyy.MM.dd'}}</td>
                                    <td class="tit elps"><a href="/store/item/{{item.productidx}}#product_review">{{item.name}} </a></td>
                                    <td class="price">{{item.totalprice | currency : "" : 0}}</td>
                                    <td><a href="/store/item/{{item.productidx}}#product_review" class="btn_s_type3">작성하기</a></td>
                                </tr>

                                <tr ng-if="total == 0">
                                    <td colspan="5" class="no_content">
                                        <p class="mb10">등록된 내역이 없습니다.</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--// 후기작성가능 -->

                    <!-- page navigation -->
                    <div class="tac">
                        <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getlist({page : page})"></paging>
                    </div>
                    <!--// page navigation -->


                </div>
                <!--// content -->


            </div>

            <div class="h100"></div>
        </div>
        <!--// e: sub contents -->

    </section>
    <!--// sub body -->



</asp:Content>
