﻿$(function () {

	
})

var app = angular.module('cps.page', []);
app.controller("defaultCtrl", function ($scope, $http, $filter) {

	$scope.total = -1;
	$scope.list = null;

	$scope.params = {
		page: 1,
		rowsPerPage: 10
	};

	// getReviwList
	$scope.getlist = function (params) {

		$scope.params = $.extend($scope.params, params);
		//console.log($scope.params);

		$http.get("/api/store.ashx?t=qna_list", { params: $scope.params }).success(function (r) {
			
			if (r.success) {

				$scope.list = r.data;

				$.each($scope.list, function () {
					this.reg_date = new Date(this.reg_date);
				})

				$scope.total = r.data.length > 0 ? r.data[0].total : 0;

			} else {
				alert(r.message);
			}
			if (params)
			    scrollTo($("#l"),20);
		});
	}


	$scope.getlist();

});
