﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
    <script type="text/javascript" src="/my/default.js?v=1.1"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/jquery.cookie.js"></script>
    <style>
        .btn_s_type_b{display:inline-block;width:auto;height:34px;font-family:'noto_d';font-size:13px;color:#fff;text-align:center;line-height:34px;background:#fff;text-decoration:none;vertical-align:middle;padding:0 15px;border-radius:4px;transition: 0.5s;-webkit-transition: 0.5s;}
    </style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" id="hdSponsorId" runat="server" />
    <input type="hidden" id="hdBirthDate" runat="server" />
    <input type="hidden" id="hdUserName" runat="server" />
    <input type="hidden" id="data" runat="server" />
    
    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
       <!-- 타이틀 -->
        <div class="page_tit" ng-if="AdminCK == false">
            <div class="titArea">
                <h1>마이컴패션</h1>
                <span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents mypage">

            <div class="w980">

                <uc:menu runat="server" />

                <div class="mb20"></div>

                <!-- 후원중인 어린이 -->
                <div class="box_type4 myChild" ng-show="data && data.child">
                    <div class="picWrap">
                        <!-- 사진사이즈 : 242 * 333 -->


                        <div class="pic">
                            <a href="/my/children/"><span>
                                <img ng-src="{{data.child.pic}}" ng-show="data.child.pic" alt="추천어린이" width="242" /></span></a>
                        </div>


                        <span class="name"><em>{{data.child.ko_name}}</em><br />
                            {{data.child.en_name}}</span>
                    </div>
                    <div class="textWrap">
                        <span class="label">1:1어린이양육</span><br />
                        <span class="title"><em>{{data.sponsorname}}</em> 후원자님, 안녕하세요?</span><br />
                        <p class="con">
                            우리가 함께한 지 <em class="fc_black">{{data.child.days}}</em>일이 지났어요.<br />
                            후원금과 기도, 사랑의 편지 항상 감사드려요.
                        </p>
                        <div class="btn">
                            <span ng-if="AdminCK == false">
                                <a href="#" ng-click="goLetter($event , data.child);" class="on"><span class="letter">편지쓰기</span></a>
                                <a href="#" ng-click="goGift($event , data.child);"><span class="gift">선물금 보내기</span></a>
                            </span>
                            <a href="#" ng-click="showAlbum($event,data.child)"><span class="diary">성장앨범 보기</span></a>
                        </div>

                        <div class="tar" ng-if="data.count_cdsp > 1"><a href="/my/children/" class="btn_s_type4">나의 후원어린이 더 보기</a></div>
                    </div>
                </div>
                <!--// 후원중인 어린이 -->

                <!-- 후원중인 어린이가 없는경우 -->
                <div class="box_type4 myChild none" ng-show="data && !data.child">

                    <div class="childBox">
                        <div class="snsWrap">
                            <span class="day">{{item.waitingdays}}일</span>
                            <span class="sns_ani" ng-if="AdminCK == false">
                                <span class="common_sns_group">
                                    <span class="wrap">
                                        <a href="#" title="페이스북" data-role="sns" data-provider="fb" data-url="" class="sns_facebook" child-item="{{item}}">페이스북</a>
                                        <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" data-url="" class="sns_story" child-item="{{item}}">카카오스토리</a>
                                        <a href="#" title="트위터" data-role="sns" data-provider="tw" data-url="" class="sns_twitter" child-item="{{item}}">트위터</a>
                                        <a href="#" title="url 공유" data-role="sns" data-provider="copy" data-url="" class="sns-copy sns_url" child-item="{{item}}">url 공유</a>
                                    </span>
                                </span>
                                <button class="common_sns_share">공유하기</button>
                            </span>
                        </div>

                        <div class="child_info">

                            <span class="pic" background-img="{{item.pic}}" data-default-image="/common/img/page/my/no_pic.png" style="background: no-repeat center top; background-size: 150px;">양육어린이사진</span>
                            <span class="name">{{item.name}}</span>
                            <p class="info">
                                <span>국가 : {{item.countryname}}</span><br />
                                <span class="bar">생일 : {{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><span>성별 : {{item.gender}}</span>
                            </p>
                        </div>

                        <div class="more" ng-if="AdminCK == false"><a href="#" ng-click="showChildPop($event , item)">더 알아보기</a></div>
                    </div>

                    <div class="textWrap">
                        <span class="title"><em>
                            <asp:Literal runat="server" ID="userName" /></em>님을 기다리고 있는<br />
                            어린이가 있어요.</span><br />
                        <p class="con">
                            1:1 결연이 만드는 사랑의 기적,<br />
                            한 어린이가 꿈을 찾는 양육비는 <em class="fc_blue">매월 4만 5천원</em>입니다.<br />
                            후원금과 기도, 사랑의 편지로 아이에게 꿈을 주세요.<br />
                        </p>
                    </div>
                </div>
                <!--// 후원중인 어린이가 없는경우 -->

                <!-- 대시보드 -->
                <div class="dashboard">
                    <a href="/my/letter/">
                        <div class="box_type4 letter"><span class="tit">편지함</span><span class="num"><em>{{data.count_letter}}</em>개</span><!--<span class="new" runat="server" id="unread_letter"></span>--></div>
                    </a>
                    <a href="/my/children/">
                        <div class="box_type4 board1">
                            <span class="tit">결연중인<br />
                                어린이</span><span class="num"><em>{{data.count_cdsp}}</em>명</span>
                        </div>
                    </a>
                    <a href="/my/sponsor/commitment/">
                        <div class="box_type4 board2">
                            <span class="tit">특별한<br />
                                나눔</span><span class="num"><em>{{data.count_non_cdsp}}</em>개</span>
                        </div>
                    </a>
                    <span ng-if="AdminCK == false">
                        <a href="/my/user-funding/create">
                        <div class="box_type4 board3">
                            <span class="tit">개설한<br />
                                펀딩</span><span class="num"><em>{{data.count_make_funding}}</em>개</span>
                        </div>
                        </a>
                    </span>
                    <span ng-if="AdminCK == true">
                        <div class="box_type4 board3">
                            <span class="tit">개설한<br />
                                펀딩</span><span class="num"><em>{{data.count_make_funding}}</em>개</span>
                        </div>
                    </span>
                    <span ng-if="AdminCK == false">
                        <a href="/my/user-funding/join">
                        <div class="box_type4 board4">
                            <span class="tit">참여한<br />
                                펀딩</span><span class="num"><em>{{data.count_join_funding}}</em>개</span>
                        </div>
                        </a>
                    </span>
                    <span ng-if="AdminCK == true">
                        <div class="box_type4 board4">
                            <span class="tit">참여한<br />
                                펀딩</span><span class="num"><em>{{data.count_join_funding}}</em>개</span>
                        </div>
                    </span>
                </div>
                <!--// 대시보드 -->

                <!-- 후원관리 -->
                <p class="s_tit1 mb15">후원관리</p>

                <div class="realtive myMainSponsor mb40">
                    <div class="clear2 mb10">
                        <div class="box_type4 box1">
                            <!-- 후원내역 있는 경우 -->
                            <div ng-if="commitment_total > 0">
                                <p class="s_tit2 mb10 clear2">후원 신청 내역<a href="/my/sponsor/commitment/" class="btn_s_type3 fr">더보기</a></p>
                                <div class="tableWrap1">
                                    <table class="tbl_type6 padding1">
                                        <caption>후원내역 리스트 테이블</caption>
                                        <colgroup>
                                            <col style="width: 18%" />
                                            <col style="width: 18%" />
                                            <col style="width: 41%" />
                                            <col style="width: 23%" />
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th scope="col">신청일</th>
                                                <th scope="col">후원 방식</th>
                                                <th scope="col">후원금 제목</th>
                                                <th scope="col">후원금액</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in commitment_list">
                                                <td>{{item.startdate | date:'yyyy.MM.dd'}} </td>
                                                <td>{{item.fundingfrequency == '1회' ? "일시후원" : "정기후원" }}</td>
                                                <td class="tal elps">
                                                    <a href="#" ng-click="showDetail($event,item)">{{item.accountclassname}} <span ng-if="item.accountclassname != item.accountclassdetail">- {{item.accountclassdetail}}</span></a>
                                                    <!-- 후원 상세정보 팝업 링크 -->
                                                </td>
                                                <td class="tar fc_black">{{item.amount | number:0}}원</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--// 후원내역 있는 경우 -->

                            <!-- 후원내역 없는 경우 -->

                            <div class="no_content" ng-show="commitment_total == 0">
                                <p class="mb20">신청하신 후원 내역이 없습니다. 첫 후원을 하러 가보시겠어요?</p>
                                <span ng-if="AdminCK == false">
                                <a href="/sponsor/children/" class="btn_s_type3">1:1어린이양육 시작하기</a>
                                    </span>
                            </div>

                            <!--// 후원내역 없는 경우 -->
                        </div>
                        <div class="box_type4 box2">
                            <div class="tac clear2">
                                <p class="tit">이번달 후원금 내역</p>
                                <span class="date">{{thismonth | date:'yyyy.MM'}}</span>
                                <p class="price"><span>{{payment_total | number:0}}</span>&nbsp;원</p>
                                <a href="/my/sponsor/payment/" class="btn_b_type1">이번달 후원금 내역 확인</a>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="box_type4 box3">
                            <p class="tit">CAD 추천 관리</p>
                            <p class="con"> 
                                내가 추천한 어린이의 결연 현황을<br />
                                확인해보세요.
                            </p>
                            <a href="/my/activity/CAD" class="btn_s_type3">결연 현황 보러가기</a>

                            <p class="num">추천 어린이<em>{{data.count_cad}}</em>명</p>
                        </div>
                        <div class="box_type4 box3">
                            <p class="tit">스탬프 투어</p>
                            <p class="con">
                                내가 획득한 스탬프 현황을 확인해<br />
                                보세요.
                            </p>
                            <a href="/my/activity/stamp" class="btn_s_type3">스탬프 현황 보러가기</a>

                            <p class="num">획득 스탬프<em>{{data.count_stamp}}</em>개</p>
                        </div>
                        <div class="box_type4 box3">
                            <p class="tit">이벤트 신청 내역</p>
                            <p class="con">
                                내가 참여한 캠페인/이벤트 내역을<br />
                                확인해보세요.
                            </p>
                            <span ng-if="AdminCK == false"><a href="/my/activity/event" class="btn_s_type3">참여 내역 보러가기</a></span>
                            <span class="btn_s_type_b" ng-if="AdminCK == true"></span>
                            <p class="num">참여현황<em>{{data.count_event}}</em>개</p>
                        </div>
                    </div>
                </div>
                <!--// 후원관리 -->

                <!-- 스토어 주문내역 -->
                <p class="s_tit1 mb15">스토어 주문내역</p>

                <div class="box_type4 box1">
                    <div class="tableWrap1">
                        <table class="tbl_type6 store">
                            <caption>스토어 주문내역 테이블</caption>
                            <colgroup>
                                <col style="width: 21%" />
                                <col style="width: 13%" />
                                <col style="width: 30%" />
                                <col style="width: 18%" />
                                <col style="width: 18%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">주문일자/번호</th>
                                    <th scope="col" colspan="2">상품정보</th>
                                    <th scope="col">결제금액</th>
                                    <th scope="col">진행상황</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in order_list">

                                    <!---- 주문상세정보 팝업 링크 ----->
                                    <td>
                                        <span ng-if="AdminCK == true">{{item.dtreg | date:'yyyy-MM-dd'}}<br />{{item.orderno}}</span>
                                        <span ng-if="AdminCK == false"><a href="#" ng-click="storeModal.show($event ,item.orderno)">
                                        {{item.dtreg | date:'yyyy-MM-dd'}}<br />
                                        {{item.orderno}}</a></span>
                                    </td>
                                    <td class="prdt">
                                        <!-- 제품이미지 사이즈 : 98 * 98 -->
                                         <span class="prdt_pic" ng-if="AdminCK == true"> <img ng-src="{{item.img}}" style="width: 98px" alt="상품 썸네일 이미지" /></a></span>
                                         <span class="prdt_pic" ng-if="AdminCK == false"><a href="/store/item/{{item.productidx}}" target="_blank"><img ng-src="{{item.img}}" style="width: 98px" alt="상품 썸네일 이미지" /></a></span>
                                    </td>
                                    <td class="tal">
                                        <!---- 상품 상세페이지 이동 ---->
                                        <span ng-if="AdminCK == true">{{item.producttitle}}</span>
                                        <span ng-if="AdminCK == false"><a href="/store/item/{{item.productidx}}" class="prdt_name" target="_blank">{{item.producttitle}} </a></span>
                                        <p class="prdt_option">(옵션) {{item.optiondetail}}</p>
                                    </td>
                                    <td>{{item.settleprice | number:0}}원</td>
                                    <td>{{item.statename}}<br />
                                        <a ng-href="{{item.href}}" target="_blank" class="btn_s_type3 mt10" ng-show="item.statename == '출고완료' && (item.deliveryco < 4 || item.deliveryco == 5)">배송조회</a></td>
                                </tr>
                                <tr ng-show="order_list == 0">
                                    <td colspan="5" class="no_content">
                                        <p class="mb20">주문하신 내역이 없습니다.</p>
                                        <a href="/store/" class="btn_s_type3" ng-if="AdminCK == false">스토어 바로가기</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--// 스토어 주문내역 -->

            </div>

            <div class="h100"></div>
        </div>
        <!--// e: sub contents -->




    </section>


</asp:Content>

