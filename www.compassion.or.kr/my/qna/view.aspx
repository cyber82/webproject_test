﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="my_qna_view"  MasterPageFile="~/main.Master"  %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/qna/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" id="key" runat="server" />


	<!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

                <uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->
		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980 withdraw">

                <uc:menu runat="server" />

				<!-- content -->
				<div class="box_type4">

					<!-- 서브타이틀 -->
					<div class="sub_tit mb30">
						<p class="tit">문의내역</p>
					</div>
					<!--// -->

					<!-- 문의내역 상세 -->
					<div class="tar mb15">
						<a href="/customer/faq/" class="btn_s_type2 mr5">FAQ 바로가기</a>
						<a href="/customer/qna/" class="btn_s_type2">1:1 문의하기</a>
					</div>

					<div class="qna_content">
						
						<div class="tit">
							<p runat="server" id="b_title"></p>
							<span class="date" runat="server" id="b_regDate"></span><span runat="server" id="s_confirm"> </span>
						</div>

						<div class="con">
							<div class="question">
								<p>회원 문의</p>
								<span runat="server" id="b_content" style="white-space:pre-line;white-space:pre-line;">
									
								</span>
								<img runat="server" id="b_img" style="max-width:100%;"/>
							</div>
							<div class="answer" runat="server" id="cp_answer">
								<p>한국컴패션 답변</p>
								<span runat="server" id="s_content">
								</span>
							</div>
						</div>

						<div class="tar">
							<a href="#" class="btn_type4" runat="server" id="btnList">목록</a>
						</div>

					</div>
					<!--// 문의내역 상세 -->

				</div>
				<!--// content -->

			</div>

			<div class="h100"></div>
		</div>
		<!--// e: sub contents -->

	</section>
	<!--// sub body -->

	
</asp:Content>

