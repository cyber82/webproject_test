﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_qna_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/qna/default.js?v=1.0"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

	<!-- 타이틀 -->
	<div class="page_tit">
		<div class="titArea">
			<h1>마이컴패션</h1>
			<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

			<uc:breadcrumb runat="server" />
		</div>
	</div>
	<!--// -->

	<div class="subContents mypage">

		<div class="w980 withdraw">

			<uc:menu runat="server"  />

			<!-- content -->
			<div class="box_type4">

				<!-- 서브타이틀 -->
				<div class="sub_tit mb30">
					<p class="tit">문의내역</p>
				</div>
				<!--// -->

				<!-- 문의내역 리스트 -->
				<div class="tbl_sort mb15">
					<span class="number">총 <em class="fc_blue">{{total}}</em>건</span>
					<div class="pos2">
						<a href="/customer/faq/" class="btn_s_type2 mr5">FAQ 바로가기</a>
						<a href="/customer/qna/" class="btn_s_type2">1:1 문의하기</a>
					</div>
				</div>

				<div class="tableWrap1 mb30">
					<table class="tbl_type6 padding2">
						<caption>문의내역 리스트 테이블</caption>
						<colgroup>
							<col style="width:17%" />
							<col style="width:51%" />
							<col style="width:16%" />
							<col style="width:16%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col">문의구분</th>
								<th scope="col">제목</th>
								<th scope="col">등록일</th>
								<th scope="col">답변</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="item in list">
								<td>{{item.type}}</td>
								<td class="tit"><a href="#" ng-click="goView(item.board_idx, $event)">{{item.b_title}}</a></td>
								<td>{{item.b_writeday | date:'yyyy.MM.dd'}}</td>
								<td>{{item.s_confirm}}</td>
							</tr>
							<tr  ng-if="total == 0">
								<td colspan="4" class="no_content">
									<p class="mb20">등록된 내역이 없습니다.</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 문의내역 리스트 -->

				<!-- page navigation -->
				<div class="tac mb30">
					<div class="paging_type1">
						<paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
					</div>
				</div>
				<!--// page navigation -->
					

			</div>
			<!--// content -->


		</div>

		<div class="h100"></div>
	</div>
	<!--// e: sub contents -->

</section>
</asp:Content>

