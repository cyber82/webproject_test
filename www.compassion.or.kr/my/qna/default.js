﻿

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

		$scope.total = -1;
		$scope.list = null;

		$scope.params = {
			page: 1,
			rowsPerPage: 10
		};


		// 뷰에서 돌아올때 넘겨준 파람을 다시 넘겨받는다.
		$scope.params = $.extend($scope.params, paramService.getParameterValues());

		$scope.getList = function (params) {

			$scope.params = $.extend($scope.params, params);
			console.log($scope.params);

			$http.get("/api/my/qna.ashx?t=list", { params: $scope.params }).success(function (r) {
				//console.log(r);
				if (r.success) {

					$scope.list = r.data;
					//console.log(r.data.length)

					$.each($scope.list, function () {

						this.b_writeday = new Date(this.b_writeday);
                        this.s_confirm = (this.s_confirm == "처리완료" ? "답변완료" : "처리중");
					})

					$scope.total = r.data.length > 0 ? r.data[0].total : 0;

				} else {
					alert(r.message);
				}
			});
		}



		// 상세페이지
		$scope.goView = function (idx, $event) {
			location.href = "/my/qna/view/" + idx + "?" + $.param($scope.params);
			$event.preventDefault();
		}

		$scope.getList();


	});

})();