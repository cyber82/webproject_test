﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_pay_delay_default" MasterPageFile="~/main.Master" ValidateRequest="false"  %>
<%@ MasterType VirtualPath="~/main.master" %>
<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/my/sponsor/pay-delay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/my/sponsor/pay-delay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/my/sponsor/pay-delay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/sponsor/pay-delay/default.js"></script>
	<script type="text/javascript">
        $(document).ready(function () {
            if ($('#AdminCK').val() == "true") {
                $(".adminCK").css("display", "none");
            }
        });
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<input type="hidden" id="amount" runat="server"  />
	
	<input type="hidden" runat="server" id="jumin" value="" />		
	<input type="hidden" runat="server" id="ci" value="" />		<!-- 본인인증 CI -->
	<input type="hidden" runat="server" id="di" value="" />		<!-- 본인인증 DI -->
	<asp:HiddenField ID="hidConfirm" runat="server" />
    <input type="hidden" id="AdminCK" value="<%=ViewState["AdminCK"]%>" />

<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit adminCK">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980">

				<uc:menu runat="server"  />

				<!-- 지연된후원금 -->
				<div class="box_type4 mb40" id="exists" runat="server">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit">
						<p class="tit">지연된 후원금</p>
					</div>
					<!--// -->

					<!-- 서브 문구 -->
					<div class="sub_desc">
						<p class="txt2">
							혹시 지난달 후원금을 전달하지 못하셨나요?<br />
							<span class="txt1">
								현재 후원 중인 목록을 선택 후 지난 후원금을 내실 수 있습니다. 당 월 후원금을 예정일에 납부하지 못한 경우,<br />
								재출금일이 있습니다. 후원금 결제 하실 때 확인 후 결제해주세요.
							</span>
						</p>
						
					</div>
					<div class="box_type3 tac mb20">
						<p class="s_con4">납부가 지연된 1:1어린이 양육 후원금은 총 <span class="txt_type2"><asp:Literal runat="server" ID="lblAmountTotal" />원</span>입니다.</p>
					</div>
					<div class="txt_info1 clear2 mb40">
						<div class="inblock">
							<span>재출금일 안내</span>
							<ul>
								<li class="s_con1">자동이체일이 5일, 15일인 경우 : 매월 20일, 27일</li>
								<li class="s_con1">자동이체일이 25일인 경우 : 매월 27일</li>
							</ul>
						</div>
					</div>
					<!--// -->

					
					<!-- 후원 어린이 수 -->
					<div class="tbl_sort mb15">
						<span class="number">후원금이 지연된 어린이는 총 <em class="fc_blue"><asp:Literal runat="server" ID="lblChildTotal" /></em>명입니다.</span>
					</div>
					<div class="tableWrap1">
						<table class="tbl_type6 padding2">
							<caption>후원하는 어린이 수 테이블</caption>
							<colgroup>
								<col style="width:33%" />
								<col style="width:34%" />
								<col style="width:33%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">어린이 수</th>
									<th scope="col">지연된 개월 수</th>
									<th scope="col">지연 후원금액(원)</th>
								</tr>
							</thead>
							<tbody>

								<asp:Repeater ID="grvDeliquent" runat="server">
									<ItemTemplate>
										<tr>
											<td ><%#Eval("ChildCnt")%></td>
											<td ><%#Eval("DeliquentMonth")%>개월</td>
											<td class="price"><%#Convert.ToDecimal(Eval("Amount")).ToString("N0")%></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>

							</tbody>
						</table>
					</div>
					<!--// 후원 어린이 수 -->
					

				</div>
				<!--// 지연된후원금 -->

				<div class="box_type4 mb40" id="nonexists" runat="server">

					<!-- 서브타이틀 -->
					<div class="sub_tit">
						<p class="tit">지연된 후원금</p>
					</div>
					<!--// -->

					<!-- 서브 문구 -->
					<div class="sub_desc">
						<p class="txt2">
							후원자님께서는 지연된 후원금이 없습니다
						</p>
						
					</div>

					
				</div>

				<!-- 결제정보선택 -->
				<div id="pn_payment" runat="server">
				<p class="s_tit3 mb20">결제 정보 선택</p>
				<div class="box_type4">

					<div class="box_type2 select_pay clear2">
						<div class="left">결제하실 어린이(들)의<br /><span class="fc_blue">지연된 후원금</span>을 선택해 주세요.</div>
						<div class="right">
							<span class="sel_type2 mr20 vam" style="width:200px;">
								<label for="ddlSelectMonth" class="hidden">결제월 선택</label>
								<asp:DropDownList ID="ddlSelectMonth" cssClass="custom_sel" runat="server" OnSelectedIndexChanged="ddlSelectMonth_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>

							</span>
							<span class="checkbox_ui vam" runat="server" id="divAddPay">
								<asp:CheckBox ID="cbxAdd" runat="server" Text="" OnCheckedChanged="cbxAdd_CheckedChanged" AutoPostBack="True" />
								<label for="cbxAdd" class="css_label">당월 후원금도 함께 결제 할래요.</label>
							</span>
						</div>

						<div runat="server" id="divAddPay2" class="sub_desc" >
							<div class="clear2"></div>
							<p class="txt2" style="font-size:15px">당월 후원금은 지정된 이체일에 자동으로 결제될 예정이므로 홈페이지에서는 지연된 후원금만 결제하실 수 있습니다.</p>
						</div>
					</div>


					<div class="tableWrap2 mb30">
						<table class="tbl_type2 delay">
							<caption>결제할 후원금 테이블</caption>
							<colgroup>
								<col style="width:35%" />
								<col style="width:65%" />
							</colgroup>
							<tbody>
								<tr style="display:none;">
									<th scope="row">결제할 지연된 후원금</th>
									<td class="tar"><asp:Label ID="lblDeliquentAmount" runat="server" Text=""></asp:Label></td>
								</tr>
								<tr style="display:none;">
									<th scope="row">결제할 당월 후원금</th>
									<td class="tar fc_blue"><asp:Label ID="lblNowAmount" runat="server" Text=""></asp:Label></td>
								</tr>
								<tr class="sum">
									<th scope="row">결제 후원금</th>
									<td class="tar"><asp:Label ID="lblTotalAmount" runat="server" Text=""></asp:Label></td>
								</tr>
							</tbody>
						</table>

					</div>

					<div class="tableWrap1 mb30">
						<table class="tbl_type1 line1">
							<caption>결제정보 선택 테이블</caption>
							<colgroup>
								<col style="width:20%" />
								<col style="width:80%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">결제방법</th>
									<td>
										<span class="radio_ui mb15">
											<input type="radio" id="payment_method_card" runat="server" name="payment_method"  class="css_radio" checked />
											<label for="payment_method_card" class="css_label" style="width:170px">신용카드 결제</label>

											<input type="radio" id="payment_method_cms" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_cms" class="css_label" style="width:170px">실시간 계좌이체</label>

											<input type="radio" id="payment_method_oversea" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_oversea" class="css_label" style="width:170px">해외발급 카드</label>
										</span>
										<span class="radio_ui">
											<input type="radio" id="payment_method_kakao" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_kakao" class="css_label relative" style="width:170px"><img src="/common/img/icon/kakaopay.jpg" class="kakao" alt="kakao pay" onclick="$('#payment_method_kakao').trigger('click')" />kakaopay</label>

											<input type="radio" id="payment_method_payco" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_payco" class="css_label relative" style="width:170px"><img src="/common/img/icon/payco.jpg" class="payco" alt="payco"  onclick="$('#payment_method_payco').trigger('click')" />payco</label>

											<input type="radio" id="payment_method_phone" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_phone" class="css_label" style="width:170px">휴대폰 결제</label>
										</span>
									</td>
								</tr>
								
							</tbody>
						</table>

					</div>

					<div class="tac mb60 adminCK">
						<a href="/my/sponsor/pay-account/?action=virtual-account" class="btn_type3 mr10">가상계좌 발급</a>
						<asp:LinkButton runat="server" ID="btn_submit" CssClass="btn_type1" OnClick="btnPay_Click">바로 결제하기</asp:LinkButton>

					</div>
					
					<div class="box_type3">
						<div class="guide mb30">
							<p>지연된 후원금 결제 안내</p>
							<ul>
								<li><span class="s_con1">후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</span></li>
								<li><span class="s_con1">지연된 후원금을 모두 결제하셔야 당월 후원금 결제가 가능합니다.</span></li>
								<li><span class="s_con1">결제관련 문의 : 후원자 서비스팀 02-740-1000</span></li>
								<li>
									<span class="s_con1">
										1:1리더쉽 결연 후원자님의 지연된 후원금 정보는 이 페이지에서 확인되지 않습니다.<br />
										1:1리더십 결연 관련 문의는 한국컴패션으로 연락 주시기 바랍니다.<br />
										Tel : 02-740-1000  / E-mail : <a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a>
									</span>
								</li>
							</ul>
						</div>
						<div class="guide">
							<p>지연된 후원금을 쉽게 납부하시는 방법</p>
							<ul>
								<li>
									<span class="s_con1">
										<em>한국컴패션 홈페이지 이용</em><br />
										로그인>마이컴패션>후원관리>지연된후원금 메뉴에서 결제방법을 선택하시면 바로 결제하실 수 있습니다.
									</span>
								</li>
								<li>
									<span class="s_con1">
										<em>전화 ARS 이용</em><br />
										02-740-1000로 전화걸기 > 2번(후원금 결제 및 납부 내역 문의) > 1번(지연된 후원금 신용카드 결제)를 통해 지연된 후원금을 바로 결제하실 수 있습니다.
									</span>
								</li>
								<li>
									<span class="s_con1">
										<em>전용 가상계좌 이용</em><br />
										홈페이지 또는 전화를 통해 후원자님만의 전용 가상계좌를 발급 받으셔서 지연된 후원금을 납부하실 수 있습니다.
									</span>
								</li>
							</ul>
						</div>
					</div>

				</div>
				<!--// 결제정보선택-->
				</div>
				
			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

    </section>

</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">

	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="payco_form" />
	<uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kakaopay_form" />

</asp:Content>