﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_pay_delay_default : SponsorPayBasePage {

	public override bool RequireSSL {
		get {
			return utils.chServerState(); //true;
        }
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}
    
    WWWService.Service _wwwService = new WWWService.Service();
	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		cbxAdd.InputAttributes["class"] = "css_checkbox";
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		cbxAdd.InputAttributes["class"] = "css_checkbox";
		

		//선택개월수 바인딩
		DropDownListBind();

		//총미납금을 가져온다.
		getNonPayment();

        //당월후원금 결제  2013-01-03 추가
        //makeAmount();

        //20171129 김은지
        //어드민계정 로그인시 마스터페이지 변경 
        var sess = FrontLoginSession.GetCookie(this.Context);
        ViewState["AdminCK"] = "false";

        if (sess.AdminLoginCheck)
        {
            ViewState["AdminCK"] = "true";
        }

    }

	// 선택개월수 바인딩
	void DropDownListBind() {
		//주석처리 2013-01-03
		//string sjs = string.Empty;
		//UserInfo sess = new UserInfo();
		DataSet ds = new DataSet();
		DataSet dsCountry = new DataSet();
		UserInfo sess = new UserInfo();
		#region //선택 개월수
		try {
			//ds = _wwwService.listSystemCode("SelectMonth", "Y");

			DataTable dtMonth = new DataTable();
			dtMonth.Columns.Add("CodeName");
			dtMonth.Columns.Add("CodeID");
			DataRow dr;

			DataSet dataDPay = _wwwService.DeliquentPayment_List(sess.SponsorID.ToString());

			int iMonthGridRef = 0;
			int iMonthGrid = 0;
			foreach(DataRow drRow in dataDPay.Tables[1].Rows) {
				iMonthGrid = int.Parse(drRow["DeliquentMonth"].ToString());
				if(iMonthGridRef < iMonthGrid)
					iMonthGridRef = iMonthGrid;
			}

			for(int i = 1; i <= iMonthGridRef; i++) {
				dr = dtMonth.NewRow();
				dr["CodeName"] = i.ToString() + "개월분";
				dr["CodeID"] = i.ToString();
				dtMonth.Rows.Add(dr);
			}

			dr = dtMonth.NewRow();
			dr["CodeName"] = "전체";
			dr["CodeID"] = "A";
			dtMonth.Rows.Add(dr);

			ddlSelectMonth.DataTextField = "CodeName";
			ddlSelectMonth.DataValueField = "CodeID";
			ddlSelectMonth.DataSource = dtMonth;
			ddlSelectMonth.DataBind();

			if (dtMonth.Rows.Count > 0)
				ddlSelectMonth.SelectedIndex = dtMonth.Rows.Count - 1;
		} catch(Exception ex) {

			ErrorLog.Write(this.Context, 0, ex.Message);
			base.AlertWithJavascript("선택가능 개월수 정보를 가져오지 못했습니다.", "goBack();");
			return;

		}
		#endregion

	}

	void getNonPayment() {
		string sTotalMoney = string.Empty; //총미납금   
		UserInfo sess = new UserInfo();
		try {
			//미납금 로드
			//수정 2013-01-03
			DataSet dsData = new DataSet();
			dsData = _wwwService.DeliquentPayment_List(sess.SponsorID.ToString());

			//미납금 로드 오류
			if(dsData.Tables[0] == null) {
				//    pnlFirst.Visible = false; //총미납금보여주는 패널
				//    pnlSecond.Visible = true; //미납금정보가 없음을 보여주는 패널
				base.AlertWithJavascript("지연된 후원금 정보를 가져오지 못했습니다.", "goBack();");
				return;
			}
			//미납금 로드 성공 : 미납금이 없을경우
			else if(string.IsNullOrEmpty(dsData.Tables[2].Rows[0][1].ToString()) || dsData.Tables[2].Rows[0][1].ToString() == "0") {

				pn_payment.Visible = false;
				exists.Visible = false;
				//base.AlertWithJavascript("지연된 후원금이 없습니다.");
				return;
			}
			//미납금 로드 성공 : 미납금이 있을경우
			else {
				string vChildCount = dsData.Tables[0].Rows[0][0].ToString();
				
				//lblChildTotal.Text = vChildCount;
				lblChildTotal.Text = dsData.Tables[1].Rows[0]["ChildCnt"].ToString();
				grvDeliquent.DataSource = dsData.Tables[1];
				grvDeliquent.DataBind();

				decimal xTotalAmount = decimal.Parse(dsData.Tables[2].Rows[0][1].ToString());
				string vTotalAmount = xTotalAmount.ToString("###,###,###0");
				lblAmountTotal.Text = vTotalAmount.ToString();
				//grvRearCommit.DataSource = dsData.Tables[3];
				//grvRearCommit.DataBind();


				if(dsData.Tables[4].Rows.Count > 0  
                    && (dsData.Tables[4].Rows[0]["PaymentType"].ToString() == "0003" || dsData.Tables[4].Rows[0]["PaymentType"].ToString() == "0004")) {
					divAddPay.Visible = false;
					cbxAdd.Enabled = false;
					divAddPay2.Visible = true;
				} else {
					exists.Visible = false;
					divAddPay.Visible = true;
					divAddPay2.Visible = false;
					cbxAdd.Enabled = true;
				}

				makeAmount();
			}
		} catch(Exception ex) {
			ErrorLog.Write(this.Context, 0, ex.Message);
			base.AlertWithJavascript("지연된 후원금 정보를 가져오지 못했습니다.", "goBack();");
			return;
		}

	}

	// 당월후원금결제
	void makeAmount() {
		//세션아이디와 쿠키아이디가 다른 경우 업데이트 해주기 2012-12-12
		//주석처리 2013-01-03
		//IDUpdate();
		UserInfo sess = new UserInfo();

		int iMonth = 0;
		int iMonthGrid = 0;
		string xMonth = string.Empty;
		string vMonthGrid = string.Empty;

		xMonth = ddlSelectMonth.SelectedValue.ToString();
		if(xMonth == "A") {
			xMonth = "0";
		}
		iMonth = int.Parse(xMonth);

		//수정 2013-01-03
		DataSet dsData = new DataSet();
		dsData = _wwwService.DeliquentPayment_List(sess.SponsorID.ToString());

		foreach(DataRow drRow in dsData.Tables[1].Rows) {
			//마지막 최고 미납개월수를 입력받는다.
			vMonthGrid = string.Empty;
			vMonthGrid = drRow["DeliquentMonth"].ToString();
			iMonthGrid = int.Parse(vMonthGrid);
		}
		//  선택받은 개월수 가 
		if(iMonth > iMonthGrid) {
			string xMonText = ddlSelectMonth.SelectedItem.Text;
			//lblerr.Text = xMonText.Replace("각", "").Replace("씩", "").Replace("분", "") + " 이상 미납된 후원이 없습니다.";
			nonexists.Visible = true;
			exists.Visible = false;
			ddlSelectMonth.SelectedValue = vMonthGrid;
			xMonth = ddlSelectMonth.SelectedValue.ToString();
		} else if(iMonth == iMonthGrid) {
			exists.Visible = true;
			nonexists.Visible = false;
		} else {
			exists.Visible = true;
			nonexists.Visible = false;
		}

		if(exists.Visible && cbxAdd.Checked) {
			if(iMonth == iMonthGrid || ddlSelectMonth.SelectedValue.ToString() == "A") {
				//처리
			} else if(iMonth < iMonthGrid) {

				base.AlertWithJavascript("지연된 후원금을 모두 결제 하셔야 당월 후원금을 결제 하실 수 있습니다.");
				cbxAdd.Checked = false;
				return;
			}
		}

		int xNowAmount = 0;
		int xTotalNowAmount = 0;
		foreach(DataRow drRow_Now in dsData.Tables[3].Rows) {
			if(cbxAdd.Checked) {
				xNowAmount += int.Parse(drRow_Now["KRDsbAmount"].ToString());
			} else {
				xNowAmount = 0;
			}

			int xKRDsbAmount = 0;
			xKRDsbAmount = int.Parse(drRow_Now["KRDsbAmount"].ToString());
			int xComMonth = 0;
			xComMonth = int.Parse(drRow_Now["DeliquentMonth"].ToString());

			if(xComMonth < 1)
				continue;

			if(xComMonth <= iMonth) {
				xTotalNowAmount += xKRDsbAmount * xComMonth;
			} else {
				xTotalNowAmount += xKRDsbAmount * iMonth;
			}
		}

		string xPaymentMethod = string.Empty;
		if(payment_method_card.Checked == true) {
			xPaymentMethod = "0007";
		}else if(payment_method_cms.Checked == true) {
			xPaymentMethod = "0011";
		} else if(payment_method_oversea.Checked == true) {
			xPaymentMethod = "0021";
		} else if(payment_method_phone.Checked == true) {
			xPaymentMethod = "0019";
		} else if(payment_method_kakao.Checked == true) {
			xPaymentMethod = "0025";
		} else if(payment_method_payco.Checked == true) {
			xPaymentMethod = "0024";
		} else {
			xPaymentMethod = "";
		}

		DataTable xDtPaymentInfo = new DataTable();
		xDtPaymentInfo.TableName = "PaymentInfo";
		xDtPaymentInfo.Columns.Add("PaymentMonth", typeof(string));
		xDtPaymentInfo.Columns.Add("ThisMonth", typeof(string));
		xDtPaymentInfo.Columns.Add("TotalAmount", typeof(string));
		xDtPaymentInfo.Columns.Add("PaymentMethod", typeof(string));
		xDtPaymentInfo.Rows.Add(ddlSelectMonth.SelectedValue.ToString(), cbxAdd.Checked.ToString().ToUpper(), xTotalNowAmount.ToString(), xPaymentMethod);
		dsData.Tables.Add(xDtPaymentInfo);
		xDtPaymentInfo.Dispose();
		settingAmount(iMonth, iMonthGrid, xNowAmount, xTotalNowAmount, dsData);
	}

	// 당월후원금결제
	void settingAmount( int iMonth, int iMonthGrid, int xNowAmount, int xTotalNowAmount, DataSet dsData ) {

		UserInfo sess = new UserInfo();

		if(ddlSelectMonth.SelectedValue.ToString() == "A") {
			//수정 2013-01-25  위치 0행이 없습니다. 수정
			if(dsData != null) {
				if(dsData.Tables[4].Rows.Count > 0) {
					if(dsData.Tables[4].Rows[0]["PaymentType"].ToString() != "0003" && dsData.Tables[4].Rows[0]["PaymentType"].ToString() != "0004") {
						cbxAdd.Enabled = true;
					}
				}
			}

			decimal dDeliquentAmount = 0;
			if(dsData != null) {
				if(dsData.Tables[2].Rows.Count > 0) {
					if(dsData.Tables[2].Rows[0][1].ToString() != "") {
						dDeliquentAmount = decimal.Parse(dsData.Tables[2].Rows[0][1].ToString());
					}
				}
			}
			lblDeliquentAmount.Text = dDeliquentAmount.ToString("###,###,###0");

			if(cbxAdd.Checked) {
				lblNowAmount.Text = xNowAmount.ToString("###,###,###0");
			} else {
				lblNowAmount.Text = "0";
			}

			int xTotalSumAmount = 0;
			if(dsData != null) {
				if(dsData.Tables[2].Rows.Count > 0) {
					if(dsData.Tables[2].Rows[0][1].ToString() != "") {
						xTotalSumAmount = int.Parse(dsData.Tables[2].Rows[0][1].ToString()) + int.Parse(xNowAmount.ToString());
					}
				}
			}
			lblTotalAmount.Text = xTotalSumAmount.ToString("###,###,###0");
		} else {
			if(iMonth == iMonthGrid) {
				//수정 2013-01-25  위치 0행이 없습니다. 수정
				if(dsData != null) {
					if(dsData.Tables[4].Rows.Count > 0) {
						if(dsData.Tables[4].Rows[0]["PaymentType"].ToString() != "0003" && dsData.Tables[4].Rows[0]["PaymentType"].ToString() != "0004") {
							cbxAdd.Enabled = true;
						}
					}
				}

				decimal dDeliquentAmount = 0;
				if(dsData != null) {
					if(dsData.Tables[2].Rows.Count > 0) {
						if(dsData.Tables[2].Rows[0][1].ToString() != "") {
							dDeliquentAmount = decimal.Parse(dsData.Tables[2].Rows[0][1].ToString());
						}
					}
				}
				lblDeliquentAmount.Text = dDeliquentAmount.ToString("###,###,###0");

				if(cbxAdd.Checked) {
					lblNowAmount.Text = xNowAmount.ToString("###,###,###0");
				} else {
					lblNowAmount.Text = "0";
				}

				int xTotalSumAmount = 0;
				if(dsData != null) {
					if(dsData.Tables[2].Rows.Count > 0) {
						if(dsData.Tables[2].Rows[0][1].ToString() != "") {
							xTotalSumAmount = int.Parse(dsData.Tables[2].Rows[0][1].ToString()) + int.Parse(xNowAmount.ToString());
						}
					}
				}
				lblTotalAmount.Text = xTotalSumAmount.ToString("###,###,###0");
			} else {
				//수정 2013-01-25  위치 0행이 없습니다. 수정
				if(dsData != null) {
					if(dsData.Tables[4].Rows.Count > 0) {
						if(dsData.Tables[4].Rows[0]["PaymentType"].ToString() != "0003" && dsData.Tables[4].Rows[0]["PaymentType"].ToString() != "0004") {
							cbxAdd.Enabled = true;
						}
					}
				}

				lblDeliquentAmount.Text = xTotalNowAmount.ToString("###,###,###0");

				if(cbxAdd.Checked) {
					lblNowAmount.Text = xNowAmount.ToString("###,###,###0");
				} else {
					lblNowAmount.Text = "0";
				}

				int xTotalSumAmount = xTotalNowAmount + int.Parse(xNowAmount.ToString());
				lblTotalAmount.Text = xTotalSumAmount.ToString("###,###,###0");
			}

		}

		string xdeliquent = " 지연된 후원금 : " + lblDeliquentAmount.Text + "                               ";
		string xthisMonth = "\n 당월후원금 : " + lblNowAmount.Text + "                               ";
		string xtotal = "\n\n\n  총 결제 후원금 : " + lblTotalAmount.Text + "";

		string xtoday = string.Empty;
		if(cbxAdd.Checked) {
			xtoday = "  당월후원금결제를 선택하셨습니다. 결제 하시겠습니까?";
		} else {
			xtoday = "  결제 하시겠습니까?";
		}

		hidConfirm.Value = xdeliquent + xthisMonth + xtotal + xtoday;

		//추가 2013-01-03
		//	Session.Remove("DataSet_" + sess.SponsorID);
		//	Session["DataSet_" + sess.SponsorID] = dsData;

		this.ViewState["payInfo"] = dsData.ToJson();

	}

	// 선택개월수 변경
	protected void ddlSelectMonth_SelectedIndexChanged( object sender, EventArgs e ) {

		kakaopay_form.Hide();
		kcp_form.Hide();
		payco_form.Hide();

		makeAmount();
	}

	protected void cbxAdd_CheckedChanged( object sender, EventArgs e ) {

		kakaopay_form.Hide();
		kcp_form.Hide();
		payco_form.Hide();

		makeAmount();
	}

	// btnPay_Click 결제하기 버튼
	protected void btnPay_Click( object sender, EventArgs e ) {
		
		kakaopay_form.Hide();
		kcp_form.Hide();
		payco_form.Hide();

		if(base.IsRefresh)
			return;

		UserInfo sess = new UserInfo();
		
		var data = new PayItemSession.Store().Create(this.ViewState["payInfo"].ToString());
		if(data == null) {
			base.AlertWithJavascript("결제정보 임시저장에 실패했습니다.");
			return;
		}

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "PayDelay";  // 결제 페이지 종류
        this.ViewState["payItem"] = data.ToJson();
        this.ViewState["returnUrl"] = "/pay/complete_paydelay/" + data.orderId;
        //this.ViewState["returnUrl"] = "/pay/complete/" + data.orderId;
		
		amount.Value = lblTotalAmount.Text.Replace(",", "");
		this.ViewState["good_mny"] = amount.Value;

		// 결제정보를 세팅

		this.ViewState["used_card_YN"] = "N";
		this.ViewState["overseas_card"] = "N";
        //[jun.heo] 2017-12 : 결제 모듈 통합 : user_name --> buyr_name
        this.ViewState["buyr_name"] = sess.UserName;
        //this.ViewState["user_name"] = sess.UserName;
        this.ViewState["good_name"] = "한국컴패션후원금";
		
		base.SetTemporaryPaymentInfo();

		if(payment_method_payco.Checked) {
			payco_form.Show(this.ViewState);
		} else if(payment_method_kakao.Checked) {
			kakaopay_form.Show(this.ViewState);
		} else {
			kcp_form.Show(this.ViewState);
		}

	}
	

}