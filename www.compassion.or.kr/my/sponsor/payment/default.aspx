﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_payment_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
	<script type="text/javascript" src="/my/sponsor/payment/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		<!-- 타이틀 -->
		<div class="page_tit" ng-if="AdminCK == false">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980">

			<uc:menu runat="server"  />

				<!-- 후원금내역 -->
				<div class="box_type4">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit mb30">
						<p class="tit">후원금 내역</p>
					</div>
					<!--// -->

					<!-- 조회기간선택 -->
					<div class="box_type3 search_period mb40" id="l">
						<span class="tit">조회기간</span>
						
						<span class="radio_ui">
							<input type="radio" id="period_1" name="period" class="css_radio dateRange" checked data-day="0" data-month="1"/>
							<label for="period_1" class="css_label mr30">1개월</label>

							<input type="radio" id="period_2" name="period" class="css_radio dateRange" data-day="0" data-month="2"/>
							<label for="period_2" class="css_label mr30">2개월</label>

							<input type="radio" id="period_3" name="period" class="css_radio dateRange" data-day="0" data-month="3"/>
							<label for="period_3" class="css_label mr30">3개월</label>
						</span>

						<span>
							<input type="text" id="date_begin" class="input_type2 date begin" value="{{date_begin| date:'yyyy-MM-dd'}}" style="width:130px" />
							<button class="calendar btn_calendar" onclick="$('#date_begin').trigger('focus');return false;"></button>
							~&nbsp;&nbsp;
							<input type="text" id="date_end" class="input_type2 date end" value="{{date_end| date:'yyyy-MM-dd'}}" style="width:130px" />
							<button class="calendar btn_calendar " onclick="$('#date_end').trigger('focus');return false;"></button>
						</span>

						<a href="#" class="btn_s_type2 ml5 daterange"  data-from="date_begin" data-end="date_end" ng-click="search($event)">조회</a>
					</div>
					<!--// 조회기간선택 -->
					

					<!-- 후원금내역리스트 -->
					<div class="tbl_sort mb15">
						<span class="number">총 <em class="fc_blue">{{total}}</em>건</span>
						<span class="number fr">후원금 합계 : <em class="fc_blue"> {{totalAmount | number:0}}</em>원</span>
					</div>
					<div class="tableWrap1 mb30">
						<table class="tbl_type6 padding2">
							<caption>후원내역 리스트 테이블</caption>
							<colgroup>
								<col style="width:10%" />
								<col style="width:12%" />
								<col style="width:13%" />
								<col style="width:30%" />
								<col style="width:13%" />
								<col style="width:11%" />
								<col style="width:11%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">납입 월</th>
									<th scope="col">결제일</th>
									<th scope="col">후원 종류</th>
									<th scope="col">후원금 제목</th>
									<th scope="col">후원금액</th>
									<th scope="col">납부방법</th>
									<th scope="col">결제 수단</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in list">
									<td>{{item.paymentdate | date:'M'}}월</td>
									<td>{{item.paymentdate | date:'yyyy.MM.dd'}}</td>
									<td>{{item.accountclassname}}</td>
									<td class="tit">{{item.accountclassdetail}}</td>
									<td class="price">{{item.amount | number:0}}원</td>
									<td>{{item.paymethod}}</td>
									<td>{{item.paymethoddetail}}</td>
								</tr>
								<tr ng-if="total == 0">
									<td colspan="7" class="no_content">
										<p class="mb20">조회기간에 신청하신 후원 내역이 없습니다.</p>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// 후원금내역 리스트 -->

					<!-- page navigation -->
					<div class="tac mb80">
						<paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>   
					</div>
					<!--// page navigation -->

					<div class="box_type3 lineH_22">
						CMS계좌 자동이체의 경우 출금일로부터 1일후, 지로의 경우 납부일로부터 2-5일 후 납부내역이 확인됩니다.<br />
						후원금 납부 내역에 대한 문의사항이 있으시면 한국컴패션으로 연락 주시기 바랍니다.<br />
						후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a>
					</div>

				</div>
				<!--// 후원금내역 -->

				
			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>

</asp:Content>
