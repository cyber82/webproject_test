﻿$(function () {

	setDatePicker($(".date"), function () {
		// dateValidate 가 있으면 호출안됨
	});

	$("#date_begin").dateRange({
		buttons: ".dateRange",	// preset range
		end: "#date_end",
		eventBubble : true ,
		onClick: function (r) {
		}
	});

	$("#date_begin").dateValidate({
		end: "#date_end",
		onSelect: function () {

		}
	});

});

(function () {
var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter) {

		$scope.total = null;
		
		$scope.list = [];
		$scope.params = {
			page: 1,
			rowsPerPage: 10,
			date_begin: "",
			date_end: ""
		};

        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function () {

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();

		// 기본 날짜 세팅
		var date_begin = new Date();
		date_begin.setMonth(date_begin.getMonth() - 1);
		$scope.date_begin = date_begin;

		$scope.date_end = new Date();
		$("#date_begin").val($filter('date')($scope.date_begin, "yyyy-MM-dd"));
		$("#date_end").val($filter('date')($scope.date_end, "yyyy-MM-dd"));

		// list
		$scope.getList = function (params) {

			$scope.params.date_begin = $("#date_begin").val();
			$scope.params.date_end = $("#date_end").val();
			$scope.params = $.extend($scope.params, params);
			console.log($scope.params);

			$http.get("/api/my/payment.ashx?t=history", { params: $scope.params }).success(function (r) {
				console.log(r);
				if (r.success) {

					var list = r.data;
					$scope.totalAmount = 0;
					if (list.length > 0) {
						$scope.totalAmount = r.data[0].totalamount;
					}

					$.each(list, function () {
						this.paymentdate = new Date(this.paymentdate);
					});

					$scope.list = list;
					$scope.total = r.data.length > 0 ? r.data[0].total : 0;

					if (params)
						scrollTo($("#l"), 30);
				} else {

					if (r.action == "not_sponsor") {
						$scope.totalAmount = 0;
						$scope.total = 0;
					}
					//alert(r.message);
				}
			});


		}

		$scope.search = function ($event) {
			$event.preventDefault();
			$scope.params.page = 1;
			$scope.getList();
		}

		$scope.getList();

	});

})();