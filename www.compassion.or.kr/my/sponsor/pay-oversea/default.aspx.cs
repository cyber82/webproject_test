﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_pay_oversea_default : FrontBasePage {

	public override bool RequireSSL {
		get {
			return utils.chServerState(); //false;
        }
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}
    
    protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

        JsonWriter actionResult;
        //2018-03-30 이종진 - #13222 해외카드결제는 신설페이지로 이동
        actionResult = new PaymentAction().CanUseOverseaCard();
        if(!actionResult.success) {
        	base.AlertWithJavascript(actionResult.message);
            return;
        } 

    }

	protected override void loadComplete( object sender, EventArgs e ) {
	
	}
	
	protected void btn_submit_Click( object sender, EventArgs e ) {
		if(base.IsRefresh) {
		//	return;
		}

		var sess = new UserInfo();

		cms_form.Visible = false;
		kcp_form.Visible = false;
		kcp_form_temporary.Visible = false;

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "PayAccount";

		this.ViewState["good_name"] = "한국컴패션후원금";
		this.ViewState["buyr_name"] = sess.UserName;
		this.ViewState["month"] = oversea_pay_month.Value;
		this.ViewState["good_mny"] = (Convert.ToInt32(hd_amount.Value) - Convert.ToInt32(hd_c_amount.Value) + (Convert.ToInt32(oversea_pay_month.Value) * Convert.ToInt32(hd_c_amount.Value))).ToString();
		this.ViewState["pay_method"] = "100000000000";
		this.ViewState["used_card_YN"] = "Y";
		this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_oversea"];
		this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card_Foreign"];
        this.ViewState["paymentType"] = prevPaymentType.Value;

        kcp_form_temporary.Visible = true;
		kcp_form_temporary.Visible = true;
        kcp_form_temporary.Visible = true;
		kcp_form_temporary.Show(this.ViewState);

	}
}