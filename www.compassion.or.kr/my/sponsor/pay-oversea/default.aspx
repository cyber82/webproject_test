﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_pay_oversea_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form_temporary" %>
<%@ Register Src="/pay/kcp_batch_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/cms_form.ascx" TagPrefix="uc" TagName="cms_form" %>
<%--<%@ Register Src="/my/sponsor/pay-account/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form_temporary" %>--%>
<%--<%@ Register Src="/my/sponsor/pay-account/kcp_batch_form.ascx" TagPrefix="uc" TagName="kcp_form" %>--%>
<%--<%@ Register Src="/my/sponsor/pay-account/cms_form.ascx" TagPrefix="uc" TagName="cms_form" %>--%>

<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/my/sponsor/pay-oversea/default.js"></script>
    <script type="text/javascript">
		
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hd_amount" value="" />
    <input type="hidden" runat="server" id="hd_c_amount" value="0" />
	<input type="hidden" runat="server" id="hd_userName" value="" />
	<input type="hidden" runat="server" id="hd_birth" value="" />


	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
        <!-- 타이틀 -->
		<div class="page_tit" ng-if ="AdminCK == false">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980">

				<uc:menu runat="server"  />


				<!-- 결제정보관리 -->
				<div class="box_type4 mb40">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit">
						<p class="tit">해외카드 결제</p>
					</div>
					<!--// -->

                    <!-- 정기결제후원내역 리스트 -->
					<p class="s_tit3 mb20" id="l">정기결제 후원 내역</p>
					<div class="tbl_sort mb15">
						<span class="number">총 <em class="fc_blue">{{total}}</em>건</span>
						
						<span class="number fr">이 달의 후원금액 : <em class="fc_blue">{{currentMonthAmount | number:0}}</em>원</span>
						
						<!-- 해외발급 카드일 경우 
						<span class="number fr">1개월 후원금액 : <em class="fc_blue">160,000</em>원</span>
						-->

					</div>
					<div class="tableWrap1 mb30">
						<table class="tbl_type6 padding2">
							<caption>정기결제 후원내역 테이블</caption>
							<colgroup>
								<col style="width:18%" />
								<col style="width:34%" />
								<col style="width:14%" />
								<col style="width:17%" />
								<col style="width:17%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">후원 종류</th>
									<th scope="col">후원금 제목</th>
									<th scope="col">결제주기</th>
									<th scope="col">후원금액</th>
									<th scope="col">후원 신청일</th>
								</tr>
							</thead>
							<tbody>
								
								<tr ng-repeat="item in list">
									<td>{{item.accountclassname}}</td>
									<td class="tit">{{item.accountclassdetail}}</td>
									<td ng-bind-html="item.fundingfrequency"></td>
									<td class="price">{{item.amount | number:0}}원</td>
									<td>{{item.startdate | date:'yyyy.MM.dd'}}</td>
								</tr>
								<tr ng-if="total == 0">
									<td colspan="5" class="no_content">
										<p class="mb20">정기결제 후원 내역이 없습니다.</p>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// 정기결제후원내역 리스트 -->

					<!-- page navigation -->
					<div class="tac mb30">
						<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>
					</div>
					<!--// page navigation -->

				<%--</div>--%>
				<!--// 결제정보관리 -->

				<!-- 결제정보 -->
				<p class="s_tit3 mb20" ng-show="total > 0">후원금 결제</p>
				<div class="box_type4" ng-show="total > 0">

					<div class="tableWrap1 mb30">
						<table class="tbl_type1 line1">
							<caption>결제방법 선택 테이블</caption>
							<colgroup>
								<col style="width:20%" />
								<col style="width:80%" />
							</colgroup>
							<tbody>
								<!-- 해외발급카드 선택 시 -->
								<tr class="payinfo" data-type="oversea_card">
									<th scope="row">결제금액</th>
									<td>
										<label class="hidden">개월 수 입력</label>
										<input type="text" class="input_type2 mr10 number_only" runat="server" id="oversea_pay_month" value="12"  maxlength="3" style="width:200px" /><span class="fs15 vam">개월</span>
										<span class="txt_type1 ml30 vam" id="oversea_pay_total"></span>

										<p class="pt15"><span class="s_con1">결제관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a></span></p>
                                        <p class="pt15"><span class="s_con1">선물금 결제는 ‘어린이 선물금’ 메뉴를 이용해 주세요.</span></p>
									</td>
								</tr>
								<!--// -->
							</tbody>
						</table>
					</div>
                    
                    <div class="tac mb60 pn_btn_submit">
						<!--<a href="#" class="btn_type1">결제정보 수정</a>-->
                        <input type="hidden" runat="server" id="prevPaymentType" />
                        <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" cssClass="btn_type1"  ng-if ="AdminCK == false">결제</asp:LinkButton>
					</div>

<%--					<div class="box_type3 pl40">
						<p class="mb15">자동이체를 신청하시는 경우 ‘공인인증서’가 필요합니다.</p>

						<ol class="mb20">
							<li class="mb10">
								1. 공인인증서가 있으신 경우
								<ul class="ml20">
									<li><span class="s_con1">CMS 자동이체 신청 후 승인완료까지 최대 일주일이 소요되며, 전사서명처리는 MS Internet Explorer 브라우저에서만 사용 가능합니다.</span></li>
									<li><span class="s_con1">신용카드 자동이체로 후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</span></li>
								</ul>
							</li>
							<li>
								2. 공인인증서가 없으신 경우
								<ul class="ml20">
									<li><span class="s_con1">전화녹취 : 한국 컴패션으로 전화하여 녹취로 신청 (Tel : 02-740-1000)</span></li>
									<li><span class="s_con1">팩스접수 : 자동이체 신청서 작성 후 컴패션으로 송부 (Fax : 02-740-1001)</span></li>
									<li>
										<span class="s_con1">
											우편접수 : 자동이체 신청서 작성 후 한국컴패션으로 우편발송<br />
											<span class="fc_blue">주소 : (04418) 서울시 용산구 한남대로 102-5 (한남동 723-41) 석전빌딩</span>
										</span>
									</li>
								</ul>
							</li>
						</ol>

						<a href="/common/download/automatic_request.zip" class="btn_s_type4">자동이체 신청서 다운로드</a>
					</div>--%>

				</div>
                </div>
				<!--// 결제정보관리 -->
               </div>
				
			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>

</asp:Content>


<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<uc:cms_form runat="server" EnableViewState="true" ViewStateMode="Enabled" ID="cms_form" />
	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" ID="kcp_form" />
	<uc:kcp_form_temporary runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form_temporary" />
</asp:Content>
