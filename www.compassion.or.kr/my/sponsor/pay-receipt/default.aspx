﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_pay_receipt_default" MasterPageFile="~/main.Master" validateRequest="false" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/sponsor/pay-receipt/default.js"></script>
	<script type="text/javascript">
        $(document).ready(function () {
            if ($('#AdminCK').val() == "true") {
                $(".adminCK").css("display", "none");
            }
        });
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<asp:HiddenField ID="hdReceiptContent" runat="server" />
    <asp:HiddenField ID="hdDataInfoChk" runat="server" />
    <asp:HiddenField ID="hdConSSNChk" runat="server" />
	<input type="hidden" id="hdRequestMsg" value="" runat="server"  />
    <input type="hidden" id="AdminCK" value="<%=ViewState["AdminCK"]%>" />
	<!-- 타이틀 -->
	<div class="page_tit adminCK">
		<div class="titArea">
			<h1>마이컴패션</h1>
			<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

			<uc:breadcrumb runat="server" />
		</div>
	</div>
	<!--// -->

	<div class="subContents mypage">

		<div class="w980">

			<uc:menu runat="server"  />

			<!-- 기부금영수증 -->
			<div class="box_type4 mb40">
					
				<!-- 서브타이틀 -->
				<div class="sub_tit">
					<p class="tit">기부금 영수증</p>
				</div>
				<!--// -->

				<!-- 서브 문구 -->
				<div class="sub_desc">
					<p class="txt2">
						기부금 영수증 발급 안내<br />
						<span class="txt1">
							후원자님, 올 한 해 동안 한국 컴패션과 함께해주셔서 감사합니다.<br />
							후원자님께서 사랑으로 보내주신 후원금은 지정기부금(코드 40번)으로 연말정산 시 기부금 공제 혜택을 받으실 수 있습니다.<br />
							기부금 영수증은 ‘국세청 연말정산 간소화 서비스(<a href="http://www.hometax.go.kr">http://www.hometax.go.kr</a> )’또는 한국컴패션 홈페이지를 통해 확인하실 수 있습니다.<br />
							<span class="inblock fc_blue pt10">정확한 기부금 영수증 발급을 위해 후원자님의 개인정보(이름,주민등록번호,주소)를 꼭 확인해 주세요.</span>
						</span>
							
					</p>
						
                    <asp:PlaceHolder runat="server" ID="ph_user_period_search" Visible="true">
					    <div class="box_type2 tac mb20">
						    <span class="s_tit5 mr20 vam">조회기간</span>
						    <span class="sel_type2 mr10 vam" style="width:200px;">
							    <label for="ddlYear" class="hidden">조회기간 선택</label>
							    <asp:DropDownList ID="ddlYear" cssClass="custom_sel" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged"></asp:DropDownList>

						    </span>
						
					    </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="ph_company_period_search" Visible="false">
					    <div class="box_type2 tac mb20">
						    <span class="s_tit5 mr20 vam">조회기간</span>
						    <span class="sel_type2 mr10 vam" style="width:140px;">
							    <asp:DropDownList ID="ddlFromYear" cssClass="custom_sel" runat="server"></asp:DropDownList>
						    </span>
                            <span class="sel_type2 mr10 vam" style="width:140px;">
                                <asp:DropDownList ID="ddlFromMonth" cssClass="custom_sel" runat="server"></asp:DropDownList>
                            </span>
                            <span class="s_tit5 mr20 vam">&nbsp;~</span>
                            <span class="sel_type2 mr10 vam" style="width:140px;">
                                <asp:DropDownList ID="ddlToYear" cssClass="custom_sel" runat="server"></asp:DropDownList>
                            </span>
                            <span class="sel_type2 mr10 vam" style="width:140px;">
                                <asp:DropDownList ID="ddlToMonth" cssClass="custom_sel" runat="server"></asp:DropDownList>  
                            </span>
                            <asp:LinkButton runat="server" class="btn_type1" ID="btn_search" AutoPostBack="True" OnClientClick="" OnClick="btn_search_Click">조 회</asp:LinkButton>
						
					    </div>
                    </asp:PlaceHolder>

				</div>
				<!--// -->

				<!-- 영수증 영역 -->
				<div class="box_type4 receipt" style="overflow:hidden;height:430px">
					<div style="padding-top:20px"></div>
					<iframe id="receipt_frm" style="width:100%;height:390px;padding-left:100px;"></iframe>
					<div style="padding-bottom:20px"></div>
				</div>
				<!--// -->

				<div class="clear2 mb40">
					<a href="/common/download/compassion_사업자등록증.zip" class="btn_s_type6 fl mr10">사업자등록증 다운로드</a>
					<a href="/common/download/compassion_법인설립허가증.zip" class="btn_s_type2 fl">법인설립허가증 다운로드</a>
					<a href="#" class="btn_s_type4 fr" id="btn_receipt">인쇄하기</a>
				</div>
					
				<div class="box_type5 padding1">
					<ul class="receipt_info">
						<li><span class="s_con1">지로의 경우 12월 28일까지 입금에 한해 해당연도 후원금으로 적용됩니다.</span></li>
						<li><span class="s_con1">지정 기부금이 공제 한도를 초과하는 경우, 최대 5년까지이월공제가 가능합니다.</span></li>
						<li><span class="s_con1">기부금 영수증을 사실과 다르게 발급할 경우 소득세법 81조에 따라 법적 처벌을 받게되므로 소득공제를 위한 명의변경은 불가하며, <br />영수증 발급은 후원자님 성함으로만 발급 가능합니다.</span></li>
					</ul>

					<div class="lineH_22">
						<p class="s_con5">전화신청 및 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_blue">info@compassion.or.kr</a></p>
						<%--<span>신청자에 한해 매년 1월에 기부금 영수증이 우편으로 발송됩니다. 법인 기부금 영수증은 기존과 동일하게 매월 우편으로 발송해 드립니다.</span><br />--%>
						<span class="adminCK"><a href="/customer/faq/" class="btn_s_type3 mt5">기부금 영수증 관련 FAQ</a></span>
					</div>
				</div>
					

			</div>
			<!--// 기부금영수증 -->

			<!-- 결제정보선택 -->
			<p class="mb15"><span class="s_tit3 vam">정기 우편발송 신청</span><a href="#" id="btn_post_show" class="btn_s_type2 vam ml20">우편발송 정보보기</a></p>
			<div class="box_type4" ID="pn_post" style="display:none">

				<p class="s_tit5 mb30">받으시는 분</p>

				<div class="tableWrap3 pb20">
					<table class="tbl_type7">
						<caption>우편발송 받는사람 정보 테이블</caption>
						<colgroup>
							<col style="width:12%" />
							<col style="width:88%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">이름</th>
								<td><asp:Literal ID="username" runat="server" Text="" ></asp:Literal></td>
							</tr>
							<tr>
								<th scope="row">주소</th>
								<td>(<asp:Literal ID="txtZip1" runat="server" Text="" ></asp:Literal>)<asp:Literal ID="txtAddrDong" runat="server" Text="" ></asp:Literal> <asp:Literal ID="txtAddrBunji" runat="server" Text="" ></asp:Literal></td>
							</tr>
							<tr>
								<th scope="row">휴대폰번호</th>
								<td><asp:Literal ID="txtPhone1" runat="server" Text="" />-<asp:Literal ID="txtPhone2" runat="server" Text="" />-<asp:Literal ID="txtPhone3" runat="server" Text="" /></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="tac adminCK">
					<span style="float:left;color:red;">신청자에 한해 매년 1월에 기부금 영수증이 우편으로 발송됩니다. 법인 기부금 영수증은 기존과 동일하게 매월 우편으로 발송해 드립니다.</span><br /><br />
					<asp:LinkButton runat="server" ID="btn_request_post" OnClick="AddrRequest" cssclass="btn_s_type1 mr10">신청하기</asp:LinkButton>

					<a href="/my/account/" class="btn_s_type3">회원정보 수정하기</a>
				</div>

			</div>
			<!--// 결제정보선택-->

				
		</div>

		<div class="h100"></div>
	</div>	


</asp:Content>

