﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_pay_receipt_default : FrontBasePage {

	public override bool RequireSSL {
		get {
			return utils.chServerState(); //false;
        }
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}
    
    WWWService.Service _wwwService = new WWWService.Service();
	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		this.GetYearBind();

        this.GetYearMonthBind();

        this.ReceiptPrint();

		this.setSponsorData();

        //20171129 김은지
        ////어드민계정 로그인시 마스터페이지 변경 
        var sess = FrontLoginSession.GetCookie(this.Context);
        ViewState["AdminCK"] = "false";

        if (sess.AdminLoginCheck)
        {
            ViewState["AdminCK"] = "true";
        }
    }
	
	void GetYearBind() {
		//- DropDownList에 현재년도에서 -3년까지 년도를 Bind한다.
		DataTable dtYear = new DataTable();
		dtYear.Columns.Add("Value");
		dtYear.Columns.Add("Text");
		dtYear.Rows.Add(DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("yyyy") + "년");
		dtYear.Rows.Add(DateTime.Now.AddYears(-1).ToString("yyyy"), DateTime.Now.AddYears(-1).ToString("yyyy") + "년");
		dtYear.Rows.Add(DateTime.Now.AddYears(-2).ToString("yyyy"), DateTime.Now.AddYears(-2).ToString("yyyy") + "년");
		dtYear.Rows.Add(DateTime.Now.AddYears(-3).ToString("yyyy"), DateTime.Now.AddYears(-3).ToString("yyyy") + "년");
		dtYear.Rows.Add(DateTime.Now.AddYears(-4).ToString("yyyy"), DateTime.Now.AddYears(-4).ToString("yyyy") + "년");
		dtYear.Rows.Add(DateTime.Now.AddYears(-5).ToString("yyyy"), DateTime.Now.AddYears(-5).ToString("yyyy") + "년");

		ddlYear.DataSource = dtYear;
		ddlYear.DataTextField = "Text";
		ddlYear.DataValueField = "Value";
		ddlYear.DataBind();

		if(DateTime.Now.Month < 7) {
			ddlYear.SelectedValue = DateTime.Now.AddYears(-1).ToString("yyyy");
		} else {
			ddlYear.SelectedValue = DateTime.Now.ToString("yyyy");
		}
	}

    void GetYearMonthBind()
    {
        //- DropDownList에 현재년도에서 -3년까지 년도를 Bind한다.
        DataTable dtYear = new DataTable();
        dtYear.Columns.Add("Value");
        dtYear.Columns.Add("Text");
        dtYear.Rows.Add(DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("yyyy") + "년");
        dtYear.Rows.Add(DateTime.Now.AddYears(-1).ToString("yyyy"), DateTime.Now.AddYears(-1).ToString("yyyy") + "년");
        dtYear.Rows.Add(DateTime.Now.AddYears(-2).ToString("yyyy"), DateTime.Now.AddYears(-2).ToString("yyyy") + "년");
        dtYear.Rows.Add(DateTime.Now.AddYears(-3).ToString("yyyy"), DateTime.Now.AddYears(-3).ToString("yyyy") + "년");
        dtYear.Rows.Add(DateTime.Now.AddYears(-4).ToString("yyyy"), DateTime.Now.AddYears(-4).ToString("yyyy") + "년");
        dtYear.Rows.Add(DateTime.Now.AddYears(-5).ToString("yyyy"), DateTime.Now.AddYears(-5).ToString("yyyy") + "년");

        ddlFromYear.DataSource = dtYear;
        ddlFromYear.DataTextField = "Text";
        ddlFromYear.DataValueField = "Value";
        ddlFromYear.DataBind();

        ddlToYear.DataSource = dtYear;
        ddlToYear.DataTextField = "Text";
        ddlToYear.DataValueField = "Value";
        ddlToYear.DataBind();

        ddlFromYear.SelectedValue = DateTime.Now.AddMonths(-11).ToString("yyyy");
        ddlToYear.SelectedValue = DateTime.Now.ToString("yyyy");
        

        DataTable dtMonth = new DataTable();
        dtMonth.Columns.Add("Value");
        dtMonth.Columns.Add("Text");
        dtMonth.Rows.Add("01", "1월");
        dtMonth.Rows.Add("02", "2월");
        dtMonth.Rows.Add("03", "3월");
        dtMonth.Rows.Add("04", "4월");
        dtMonth.Rows.Add("05", "5월");
        dtMonth.Rows.Add("06", "6월");
        dtMonth.Rows.Add("07", "7월");
        dtMonth.Rows.Add("08", "8월");
        dtMonth.Rows.Add("09", "9월");
        dtMonth.Rows.Add("10", "10월");
        dtMonth.Rows.Add("11", "11월");
        dtMonth.Rows.Add("12", "12월");

        ddlFromMonth.DataSource = dtMonth;
        ddlFromMonth.DataTextField = "Text";
        ddlFromMonth.DataValueField = "Value";
        ddlFromMonth.DataBind();

        ddlToMonth.DataSource = dtMonth;
        ddlToMonth.DataTextField = "Text";
        ddlToMonth.DataValueField = "Value";
        ddlToMonth.DataBind();

        ddlFromMonth.SelectedValue = DateTime.Now.AddMonths(-11).ToString("MM");
        ddlToMonth.SelectedValue = DateTime.Now.ToString("MM");
        
    }

    void ReceiptPrint() {
		UserInfo userInfo = new UserInfo();

		if(string.IsNullOrEmpty(userInfo.SponsorID)) {
			hdReceiptContent.Value = "";
			return;
		}

		DataSet dsFundInfo = new DataSet();
		string sJs = string.Empty;
		string sFromDate = string.Empty;
		string sToDate = string.Empty;
		string sReceiptContent = string.Empty;

		try {
            //납부목록 가져오기
            dsFundInfo = GetFundingInfo();
            //dsFundInfo = _wwwService.listDATPaymentReceipt(userInfo.SponsorID, ddlYear.SelectedValue);

            //수정 2013-01-23  납부내역이 없으면
            if (dsFundInfo != null && dsFundInfo.Tables["tPaymentMonth"] != null) {
				if(dsFundInfo.Tables["tPaymentMonth"].Rows.Count > 0) {
					hdDataInfoChk.Value = "Y";
				} else {
					hdDataInfoChk.Value = "N";
					hdReceiptContent.Value = "";
					return;
				}
			} else {
				hdDataInfoChk.Value = "N";
				hdReceiptContent.Value = "";
				return;
			}
		} catch(Exception ex) {
			ErrorLog.Write(this.Context, 0, ex.Message);
			base.AlertWithJavascript("후원금내역 로드 도중 에러가 발생하였습니다");
		}

		try {
			Receipt myReceipt = new Receipt();

			myReceipt.ConID = userInfo.ConId; //ConID
			myReceipt.ConName = dsFundInfo.Tables["tPaymentInfo"].Rows[0]["SponsorName"].ToString(); //후원자명

			if(userInfo.Jumin != null && userInfo.Jumin != "") {
				myReceipt.ConSSN = dsFundInfo.Tables["tPaymentInfo"].Rows[0]["JuminID"].ToString(); //주민번호

				myReceipt.ConType = "person";
			} else if(userInfo.GroupNo != null && userInfo.GroupNo != "") {
				myReceipt.ConSSN = dsFundInfo.Tables["tPaymentInfo"].Rows[0]["JuminID"].ToString(); //사업자번호
				myReceipt.ConType = "company";
			} else {
				myReceipt.ConSSN = "";
				myReceipt.ConType = "person";
			}

			//추가 2013-01-23  주민번호나 사업자번호가 없으면
			if(myReceipt.ConSSN.ToString() != "" && myReceipt.ConSSN.ToString() != string.Empty) {
				hdConSSNChk.Value = "Y";
			} else {
				hdConSSNChk.Value = "N";
				hdReceiptContent.Value = "";
				return;
			}

			myReceipt.ConAddress = dsFundInfo.Tables["tPaymentInfo"].Rows[0]["AddressDong"].ToString()
									+ " " + dsFundInfo.Tables["tPaymentInfo"].Rows[0]["AddressBunji"].ToString(); //주소


			sReceiptContent = myReceipt.CreateReceipt(dsFundInfo);

			hdReceiptContent.Value = sReceiptContent;

		//	Response.Write(userInfo.SponsorID + " : " + dsFundInfo.Tables["tPaymentInfo"].ToJson());

		} catch(Exception ex) {
			
		}
	}

    private DataSet GetFundingInfo()
    {
        UserInfo userInfo = new UserInfo();

        //일반후원자는 조회기간이 없고, 년도만 선택.
        if (CheckUserClass() != "기업")
        {
            DataSet dsFundingInfo = _wwwService.listDATPaymentReceipt(userInfo.SponsorID, ddlYear.SelectedValue);
            return dsFundingInfo;
        }

        //기업후원자는 조회기간이 from ~ to임
        //from ~ to의 년도 조회를 한 후, 데이터를 맞춰줌.
        DataSet dsResult = new DataSet();

        DataSet dsFromYearInfo = _wwwService.listDATPaymentReceipt(userInfo.SponsorID, ddlFromYear.SelectedValue);
        DataSet dsToYearInfo = _wwwService.listDATPaymentReceipt(userInfo.SponsorID, ddlToYear.SelectedValue);

        //DataSet 합치기
        #region | 1. tPaymentInfo DataTable 넣어주기 |
        if (dsFromYearInfo.Tables["tPaymentInfo"].Rows.Count > 0) 
        {
            dsResult.Tables.Add(dsFromYearInfo.Tables["tPaymentInfo"].Copy());
        }
        else if (dsToYearInfo.Tables["tPaymentInfo"].Rows.Count > 0)
        {
            dsResult.Tables.Add(dsToYearInfo.Tables["tPaymentInfo"].Copy());
        }
        else //데이터가 존재하지 않다면 컬럼만 임의 생성.
        {
            dsResult.Tables.Add("tPaymentInfo");
            dsResult.Tables["tPaymentInfo"].Columns.Add("SponsorID");
            dsResult.Tables["tPaymentInfo"].Columns.Add("ReceiptYear");
            dsResult.Tables["tPaymentInfo"].Columns.Add("MoneySum");
        }
        #endregion

        #region | 2.tPaymentMonth DataTable은 생성 하여, 조회조건에 맞게 데이터를 넣어줌 |
        //2.1 DataTable 생성
        dsResult.Tables.Add("tPaymentMonth");
        dsResult.Tables["tPaymentMonth"].Columns.Add("Month");
        dsResult.Tables["tPaymentMonth"].Columns.Add("Money");

        //2.2 from과 to의 month 차이를 구하여, 컬럼을 만들어줌.
        DateTime from = DateTime.Parse(ddlFromYear.SelectedValue + "-" + ddlFromMonth.SelectedValue);
        DateTime to = DateTime.Parse(ddlToYear.SelectedValue + "-" + ddlToMonth.SelectedValue);
        int diffMonth = ((to.Year - from.Year) * 12) + (to.Month - from.Month) + 1; //조회 종료월도 포함하기위해 +1을 함
        for(int i=0; i< diffMonth; i++) //시작월부터 종료월까지
        {
            DateTime increaseMonth = from.AddMonths(i);
            string month = increaseMonth.ToString("yyyy-MM");

            bool insertChk = false;
            //조회월 데이터가 있으면 넣어줌
            if (insertChk == false)
            {
                foreach (DataRow dr in dsFromYearInfo.Tables["tPaymentMonth"].Rows)
                {
                    if (dr["Month"].ToString() == month)
                    {
                        dsResult.Tables["tPaymentMonth"].Rows.Add(month, dr["Money"].ToString());
                        insertChk=true;
                        continue;
                    }
                }
            }
            if (insertChk == false)
            {
                foreach (DataRow dr in dsToYearInfo.Tables["tPaymentMonth"].Rows)
                {
                    if (dr["Month"].ToString() == month)
                    {
                        dsResult.Tables["tPaymentMonth"].Rows.Add(month, dr["Money"].ToString());
                        insertChk = true;
                        continue;
                    }
                }
            }

        }

        #endregion

        #region | 3.tPaymentMonth 의 값을 합하여, tPaymentInfo.MoneySum에 계산하여 넣어줌 |

        //3.1 값 구하기
        int totMoney = 0;
        foreach (DataRow drMoney in dsResult.Tables["tPaymentMonth"].Rows)
        {
            if (drMoney["Money"] != null && drMoney["Money"].ToString() != "")
            {
                totMoney = totMoney + Int32.Parse(drMoney["Money"].ToString());
            }
        }

        //3.2 값 변경하기
        dsResult.Tables["tPaymentInfo"].Rows[0]["MoneySum"] = totMoney;

        #endregion

        return dsResult;
    }


    protected void ddlYear_SelectedIndexChanged( object sender, EventArgs e ) {
		ReceiptPrint();
	}

    protected void btn_search_Click(object sender, EventArgs e)
    {
        DateTime from = DateTime.Parse(ddlFromYear.SelectedValue + "-" + ddlFromMonth.SelectedValue);
        DateTime to = DateTime.Parse(ddlToYear.SelectedValue + "-" + ddlToMonth.SelectedValue);

        //1. 시작 종료 기간설정체크 
        //양수면 from이 더 크고, 음수면 to가 더 큼. 0이면 동일
        int chk = DateTime.Compare(from, to);
        if(chk > 0)
        {
            base.AlertWithJavascript("종료날짜를 시작날짜보다 이후로 설정해 주세요.");
            return;
        }
        //if (chk == 0)
        //{
        //    base.AlertWithJavascript("기간을 1개월 이상으로 설정해 주세요.");
        //    return;
        //}

        //2. 기간은 12개월이하로 설정해야함.
        chk = DateTime.Compare(from.AddMonths(12), to);
        if(chk < 0) {
            base.AlertWithJavascript("기간을 12개월 이하로 설정해 주세요.");
            return;
        }

        ReceiptPrint();
    }

    //우편신청 등록
    protected void AddrRequest( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

		#region 값 설정
		UserInfo user = new UserInfo();

		DataSet dsState = new DataSet();
		bool state = false;

		string uname = username.Text.ToString();

		//수정 2013-09-12
		string zip = string.Empty;
		
			zip = txtZip1.Text.ToString();

		string addr1 = txtAddrDong.Text.ToString();
		string addr2 = txtAddrBunji.Text.ToString();
		string tel = txtPhone1.Text + "-" + txtPhone2.Text + "-" + txtPhone3.Text;
		#endregion

		//수정 2013-09-03
		try {
			UserInfo sess = new UserInfo();

			string sSponsorID = sess.SponsorID.ToString().Trim();
			if(string.IsNullOrEmpty(sSponsorID)) {
				base.AlertWithJavascript("신청 가능한 기부금 영수증 정보가 없습니다.");
				return;
			}
			DataSet dsSponsor = _wwwService.listSponsor(sSponsorID, "", "", "", "");
			
			if(dsSponsor.Tables[0].Rows.Count > 0) {
				String iResult = String.Empty;

				if(dsSponsor.Tables[0].Rows[0]["juminID"].ToString().Length == 13 && dsSponsor.Tables[0].Rows[0]["juminID"].ToString().Substring(6, 7) != "0000000") {
					dsState = _wwwService.SetInsertDonationReceiptZip(user.ConId, user.UserId, uname, zip, addr1, addr2, tel);
					_wwwService.registerContributionReceipt_Web(user.ConId, uname);

					iResult = "10";
				} else {

					base.AlertWithJavascript("기부금 영수증 발급을 위한 정보가 부족합니다.\\r\\n발급을 원하시면 02)740-1000으로 전화주세요.");
					


					return;
				}

				if(iResult == "10") {
					base.AlertWithJavascript("기부금 영수증 우편 발송 신청이 완료되었습니다");
					
					return;
				}
			} else {
				base.AlertWithJavascript("신청 가능한 기부금 영수증 정보가 없습니다.");
			}

		} catch(Exception ex) {
			ErrorLog.Write(this.Context, 0, ex.Message);
			base.AlertWithJavascript("우편발송 신청 등록하는 중 오류가 발생했습니다");

		}


	}

	private void setSponsorData() {
		#region ===== Define ==========================================

		UserInfo sess = new UserInfo();
		DataSet dsSponsor = new DataSet();  //추가 2013-09-12
		DataSet dsAddress = new DataSet();
		DataSet dsMailing = new DataSet();  //추가 2013-09-12
		DataSet dsCommunication = new DataSet();
		DataSet dsRefuse = new DataSet();   //추가 2013-09-12

		string sSponsorID = sess.SponsorID.ToString().Trim();

		#endregion

		#region ===== getData =========================================

		#region //SponsorMaster Select  //추가 2013-09-12
		try {
			dsSponsor = _wwwService.listSponsor(sSponsorID, "", "", "", "");
		} catch(Exception ex) //Exception Error
		  {
			//Exception Error Insert
			base.AlertWithJavascript("회원님의 정보를 읽어오는 중 오류가 발생했습니다");
			return;
		}
		if(dsSponsor == null) //DB Error
		{

			base.AlertWithJavascript("회원님의 정보를 가져오지 못했습니다.\\r\\n관리자에게 문의해주세요.");
			return;
		}
		if(dsSponsor.Tables[0].Rows[0]["UserClass"].ToString().Trim() == null ||
			dsSponsor.Tables[0].Rows[0]["UserClass"].ToString().Trim() == "") //UserClass체크
		{

			base.AlertWithJavascript("회원님의 가입유형이 존재하지 않습니다. \\r\\n관리자에게 문의해주세요.");
			return;
		}
		#endregion

		if(CheckUserClass() == "기업") {

			hdRequestMsg.Value = "법인 또는 사업자의 경우 기부금영수증이 매월 우편으로 발송됩니다.";
            ph_user_period_search.Visible = false;
            ph_company_period_search.Visible = true;

        }

        #region //SponsorAddress Select
        try {
			dsAddress = _wwwService.getSponsorAddress(sSponsorID, "", "", "Y");
		} catch(Exception ex) {

			ErrorLog.Write(this.Context, 0, ex.Message);
			base.AlertWithJavascript("회원님의 정보를 읽어오는 중 오류가 발생했습니다");
			return;
		}
		if(dsAddress == null)//사용자주소가 null이거나 개수(자택,직장)가 2보다 많으면
							 //|| dsAddress.Tables[0].Rows.Count > 2) 
		{
			
			base.AlertWithJavascript("회원님의 주소를 읽어오는 중 오류가 발생했습니다. \\r\\n관리자에게 문의해주세요");
			return;
		}
		#endregion

		#region //SponsorMailing Select //추가 2013-09-12
		try {
			dsMailing = _wwwService.listDATSponsorMailing(dsSponsor.Tables[0].Rows[0]["SponsorID"].ToString().Trim(), "", "");
		} catch(Exception ex) //Exception Error
		  {

			ErrorLog.Write(this.Context, 0, ex.Message);
			base.AlertWithJavascript("회원님의 우편물수신 정보를 가져오는 중 오류가 발생했습니다");
			return;
		}
		if(dsRefuse == null) //DB Error
		{
			base.AlertWithJavascript("회원님의 우편물수신 정보를 가져오지 못했습니다. \\r\\n관리자에게 문의해주세요");
			return;
		}

		#endregion

		#region //SponsorCommunication
		try {
			dsCommunication = _wwwService.getSponsorCommunication(sSponsorID, "", "", "Y");
		} catch(Exception ex) {

			ErrorLog.Write(this.Context, 0, ex.Message);
			base.AlertWithJavascript("회원님의 정보를 읽어오는 중 오류가 발생했습니다.\\r\\n관리자에게 문의해주세요.");
			return;
		}
		if(dsCommunication == null)//연락처가 null이거나 개수(이메일,집전화,이동전화)가 3보다 많으면
								   //|| dsCommunication.Tables[0].Rows.Count > 3)
		{
			base.AlertWithJavascript("회원님의 연락처를 읽어오는 중 오류가 발생했습니다. \\r\\n관리자에게 문의해주세요.");
			return;
		}
		#endregion

		#region //SponsorRefuse Select  //추가 2013-09-12
		//Compass4 : SponsorCommunication Select
		try {
			dsRefuse = _wwwService.listDATSponsorRefuse(dsSponsor.Tables[0].Rows[0]["SponsorID"].ToString().Trim(), "", "Y");
		} catch(Exception ex) //Exception Error
		  {

			ErrorLog.Write(this.Context, 0, ex.Message);
			base.AlertWithJavascript("회원님의 수신동의 정보를 가져오는 중 오류가 발생했습니다.");
			return;
		}
		if(dsRefuse == null) //DB Error
		{

			base.AlertWithJavascript("회원님의 수신동의 정보를 가져오지 못했습니다. \\r\\n관리자에게 문의해주세요.");
			return;
		}

		#endregion

		#endregion

		#region ===== setData =======================================

		//후원자명
		username.Text = sess.UserName.ToString();
		string sUserClass = sess.UserClass.ToString();

		string homeZip1 = string.Empty;
		string homeZip2 = string.Empty;
		string homeAddrDong = string.Empty;
		string homeAddrBunji = string.Empty;

		string jobZip1 = string.Empty;
		string jobZip2 = string.Empty;
		string jobAddrDong = string.Empty;
		string jobAddrBunji = string.Empty;

		#region # 후원자 주소 정보
		for(int i = dsAddress.Tables[0].Rows.Count - 1; i >= 0; i--) {
			string sCountry = dsAddress.Tables[0].Rows[i]["CountryCode"].ToString();

			if(dsAddress.Tables[0].Rows[i]["AddressType"].ToString() == "수령지" && dsAddress.Tables[0].Rows[i]["CurrentUse"].ToString() != "N") {
				homeZip1 = dsAddress.Tables[0].Rows[i]["ZipCode"].ToString(); //우편번호
				homeAddrDong = dsAddress.Tables[0].Rows[i]["Address1"].ToString(); //집주소 동
				homeAddrBunji = dsAddress.Tables[0].Rows[i]["Address2"].ToString(); //집주소 번지
			}
		}
		#endregion

		#region //기부금영수증 수신동의 주소지 설정    //추가 2013-09-12
		DataTable dtMailing = new DataTable();
		dtMailing.Clear();

		dsMailing.Tables["tSponsorMailing"].DefaultView.RowFilter = "MailingType='기부금영수증'";
		dtMailing = dsMailing.Tables["tSponsorMailing"].DefaultView.ToTable();

		if(dtMailing.Rows.Count == 1 && dtMailing.Rows[0]["AddressType"].ToString() == "집") {
			txtZip1.Text = homeZip1;
			
			txtAddrDong.Text = homeAddrDong;
			txtAddrBunji.Text = homeAddrBunji;
		} else if(dtMailing.Rows.Count == 1 && dtMailing.Rows[0]["AddressType"].ToString() == "직장") {
			txtZip1.Text = jobZip1;
			
			txtAddrDong.Text = jobAddrDong;
			txtAddrBunji.Text = jobAddrBunji;
		} else {
			txtZip1.Text = homeZip1;
			
			txtAddrDong.Text = homeAddrDong;
			txtAddrBunji.Text = homeAddrBunji;
		}
		#endregion

		#region //후원자 연락처
		//-- 후원자연락처(이메일,전화번호,휴대번호)
		for(int i = dsCommunication.Tables[0].Rows.Count - 1; i >= 0; i--) {
			if(dsCommunication.Tables[0].Rows[i]["CommunicationType"].ToString() == "휴대번호") {
				//연락처 타입이 휴대번호면
				txtPhone1.Text = "";
				txtPhone2.Text = "";
				txtPhone3.Text = "";

				string telStr = dsCommunication.Tables[0].Rows[i]["CommunicationContents"].ToString();
				string[] telArr = new string[3];
				int telLen = telStr.Length;
				int rest = telLen - 7;

				if(telLen == 12) {
					telArr[0] = telStr.Substring(0, 4);
					telArr[1] = telStr.Substring(4, 4);
					telArr[2] = telStr.Substring(8, 4);
				} else if(telLen == 11) {
					telArr[0] = telStr.Substring(0, 3);
					telArr[1] = telStr.Substring(3, 4);
					telArr[2] = telStr.Substring(7, 4);
				} else if(telLen == 10) {
					telArr[0] = telStr.Substring(0, 2);
					telArr[1] = telStr.Substring(2, 4);
					telArr[2] = telStr.Substring(6, 4);
				} else {
					telArr[0] = telStr.Substring(0, 3);
					telArr[1] = telStr.Substring(3, 4);
					telArr[2] = telStr.Substring(7, rest);
				}

				txtPhone1.Text = telArr[0].ToString();
				txtPhone2.Text = telArr[1].ToString();
				txtPhone3.Text = telArr[2].ToString();
			}
		}
		#endregion

		#endregion

	}

    private string CheckUserClass ()
    {
        string result = "";
        try
        {
            UserInfo sess = new UserInfo();
            DataSet dsSponsor = new DataSet();  

            string sSponsorID = sess.SponsorID.ToString().Trim();
            dsSponsor = _wwwService.listSponsor(sSponsorID, "", "", "", "");
            if (dsSponsor == null) //DB Error
            {
                return result;
            }

            if (dsSponsor.Tables[0].Rows[0]["ComRegistration"].ToString() != "" || dsSponsor.Tables[0].Rows[0]["UserClass"].ToString() == "기업")
            {
                result = "기업";
            }
            else
            {
                result = "";
            }
            return result;
        }
        catch (Exception ex) //Exception Error
        {
            return result;
        }
    }

}