﻿$(function () {

	$page.init();
	
})

var $page = {

	init : function(){
		$("#btn_post_show").click(function () {
			if ($("#hdRequestMsg").val() != "") {
				alert($("#hdRequestMsg").val());
				return false;
			}
			$("#pn_post").show();
			return false;

		});
		
		// 우편발송신청
		$("#btn_request_post").click(function () {

			var username = $("#username");
			var txtZip1 = $("#txtZip1");
			var txtZip2 = $("#txtZip2");
			var txtAddrDong = $("#txtAddrDong");
			var txtAddrBunji = $("#txtAddrBunji");
			var txtPhone1 = $("#txtPhone1");
			var txtPhone2 = $("#txtPhone2");
			var txtPhone3 = $("#txtPhone3");

			if (username.html() == "") {
				alert("이름이 비었습니다.");
				//username.focus();
				return false;
			}
			
			if (txtAddrDong.html() == "") {
				alert("주소가 비었습니다.");
				//SearchAddress();
				return false;
			}
			if (txtAddrBunji.html() == "") {
				alert("나머지 주소가 비었습니다.");
				//txtAddrBunji.focus();
				return false;
			}
			if (txtPhone1.html() == "") {
				alert("전화가 비었습니다.");
				//txtPhone1.focus();
				return false;
			}
			if (txtPhone2.html() == "") {
				alert("전화가 비었습니다.");
				//txtPhone2.focus();
				return false;
			}
			if (txtPhone3.html() == "") {
				alert("전화가 비었습니다.");
				//txtPhone3.focus();
				return false;
			}

			return true;
		});

		// 영수증출력
		$("#btn_receipt").click(function () {

			//수정 2013-01-23 예외처리 추가
			if ($("#hdDataInfoChk").val() != "Y") {
				alert("후원금 납부 내역이 없습니다.");
				return false;
			}
			else if ($("#hdConSSNChk").val() != "Y") {
				alert("기부금영수증 조회 및 인쇄가 불가능합니다.");
				return false;
			} else if ($("#hdReceiptContent").val() != "") {
				var wopt = 'menubar=no,toolbar=no,location=no,scrollbars=yes,status=no,resizable=yes,width=750,height=860';
				winResult = window.open('about:blank', '', wopt);
				winResult.document.open('text/html', 'replace');
				winResult.document.write($("#hdReceiptContent").val());
				winResult.document.close();

				winResult.fnPrint();
				

				return true;
			}
			else {
				alert("후원하신 기부금내역이 존재 하지 않습니다.");
				return false;
			}

		});

		if ($("#hdDataInfoChk").val() == "Y" && $("#hdConSSNChk").val() == "Y") {

			$("#btn_receipt").show();
			var content = $("#hdReceiptContent").val();
			$("#pn_receipt").show();
			$("#receipt_frm").contents().find('html').html($(content));
			$("#printID", $("#receipt_frm").contents()).hide();

		} else {
			
			$("#receipt_frm").attr("src", "no-receipt.html");
			$("#btn_receipt").hide();
		}

	} 

}
