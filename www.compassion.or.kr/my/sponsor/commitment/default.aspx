﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_commitment_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
	<script type="text/javascript" src="/my/sponsor/commitment/default.js?v=1.0"></script>
    <style>
    .subContents.mypage .sub_desc .em2 {
        display: inline-block;
        font-family: 'noto_r';
        font-size: 15px;
        color: #005dab;
        margin-top:10px;
    }
    </style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" id="hd_auth_domain" runat="server"  />
	<input type="hidden" id="hdSponsorId" runat="server"  />
	<input type="hidden" id="hdBirthDate" runat="server"  />
	<input type="hidden" id="hdUserName" runat="server"  />
	
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
        <!-- 타이틀 -->
		<div class="page_tit" ng-if="AdminCK == false">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980">

				<uc:menu runat="server"  />

				<!-- 후원신청내역 -->
				<div class="box_type4 ">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit " >
						<p class="tit noline">정기후원 신청내역</p>
					</div>
					<!--// -->
                    <asp:PlaceHolder runat="server" ID="ph_change_notice" Visible="true">
						<div class="sub_desc">
						    <p class="txt1 line">
							
							    <em class="em2">후원자님의 정기후원 신청 내역을 확인하실 수 있습니다. <br />
                                일시후원 내역은 '후원금 내역' 메뉴에서 확인해주세요.</em>
						    </p>
					    </div>
					</asp:PlaceHolder>

					<!-- 기업/해외 회원일 경우 해당영역 미노출 -->
					<asp:PlaceHolder runat="server" ID="ph_before_auth" Visible="false">

					<div class="sub_desc">
						<p class="txt1">
							<em class="em1 mb5">혹시 기존 후원내역이 안보이세요?</em><br />
							후원내역을 연결하여 하나의 아이디로 회원님의 후원내역을 관리해보세요.

							<!-- 인증완료 시 문구 -->
							<!--
							
							-->
						</p>
					</div>
					<!--// -->

					<!-- 본인인증 -->
					<div class="cert clear2">

						<!-- 기업/해외 회원일 경우 해당영역 미노출 : s -->

						<!-- 휴대폰인증 -->
						<div class="fl">
							<div class="box phone">
								<p class="tit">휴대폰 인증</p>
								<p class="con">
									본인 명의의 휴대전화번호로 본인인증을<br />
									원하시는 분은 아래 버튼을 클릭해주세요
								</p>

								<a href="#"  id="btn_cert_by_phone" class="btn_s_type2" ng-if="AdminCK == false">휴대폰 인증하기</a>
							</div>
						</div>

						<!-- 아이핀인증 -->
						<div class="fr">
							<div class="box ipin">
								<p class="tit">아이핀(i-PIN) 인증</p>
								<p class="con">
									가상 주민등록번호 아이핀으로 본인인증을<br />
									원하시는 분은 아래 버튼을 클릭해주세요
								</p>

								<a href="#"  id="btn_cert_by_ipin" class="btn_s_type2"  ng-if="AdminCK == false">아이핀 인증하기</a>
							</div>
						</div>
						
						<!-- 기업/해외 회원일 경우 해당영역 미노출 : e -->

					</div>
					<!--// 본인인증-->
					</asp:PlaceHolder>
					<asp:PlaceHolder runat="server" ID="ph_after_auth" Visible="false">
						<div class="sub_desc">
						<p class="txt1">
							
							<em class="em1 mb5">본인 인증이 완료되었습니다.</em><br>
							본인인증 후에도 기존 후원내역이 안보이시는 경우 한국컴패션 후원지원팀으로 문의주세요.<br />
							후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a>
						</p>
					</div>


					</asp:PlaceHolder>

                    

					<!-- 후원내역리스트 -->
					<div class="tbl_sort mt50 mb15" id="l">
						<span class="number">총 <em class="fc_blue" ng-if="total > -1">{{total}}</em>건</span>
						
						<span class="sel_type1 pos1" style="width:150px">
							<label for="year" class="hidden">후원형태 선택</label>
							<select class="custom_sel" ng-model="params.sponsorItemEng" ng-change="changeSponsorItemEng()" >
								<option value="">전체</option>
								<option value="commitment">1:1어린이양육 후원</option>
								<option value="gift">어린이 선물금</option>
                                <option value="funding">특별한 나눔</option>
							</select>
						</span>
					</div>
					<div class="tableWrap1 mb30">
						<table class="tbl_type6 padding1">
							<caption>후원내역 리스트 테이블</caption>
							<colgroup>
								<col style="width:12%" />
								<col style="width:12%" />
								<col style="width:12%" />
								<col style="width:29%" />
								<col style="width:13%" />
								<col style="width:11%" />
								<col style="width:11%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">후원신청일</th>
									<th scope="col">후원 방식</th>
									<th scope="col">후원 종류</th>
									<th scope="col">후원금 제목</th>
									<th scope="col">후원 금액</th>
									<th scope="col">후원 상태</th>
									<th scope="col">상세</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in list">
									<td>{{item.startdate | date:'yyyy.MM.dd'}} </td>
									<td class="fc_blue">{{item.fundingfrequency == '1회' ? "일시후원" : "정기후원" }}</td>
									<td>{{item.accountclassname}}</td>
									<td class="tit">{{item.accountclassdetail}}</td>
									<td class="price">{{item.amount | number:0}}원</td>
									<td><span class="status" ng-class="{'ing' : item.commitstatus == '후원중','done' : item.commitstatus == '후원완료','cancel' : item.commitstatus == '후원취소'}" ng-bind-html="item.commitstatusText"></span></td>
									<td><a href="#" ng-click="showDetail($event,item)" class="btn_s_type3">후원내역</a></td>
								</tr>
								
								<tr ng-if="total == 0">
									<td colspan="7" class="no_content">
										<p class="mb20">신청하신 후원 내역이 없습니다.<br />첫 후원을 하러 가보시겠어요?</p>
										<a href="/sponsor/children/" id="sponsorItemEngLinkURL" class="btn_type1" ng-if="AdminCK == false">1:1 어린이 양육 시작하기</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// -->

					<!-- page navigation -->
					<div class="tac mb80 relative">
						<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>

						<a href="/my/sponsor/pay-account/" ng-if="total > 0" class="btn_type3 posR">결제정보변경</a>
					</div>
					<!--// page navigation -->

					<div class="box_type3 lineH_22">
						
						후원자님께서 보내주시는 후원금은 어린이들이 지적, 사회·정서적, 신체적, 영적으로 온전하게 자라가는 데 큰 도움이 되고 있습니다.<br />
						후원 신청 내역에 대한 문의사항이 있으시면 한국컴패션으로 연락 주시기 바랍니다.<br />
						후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a>
					</div>

				</div>
				<!--// 후원신청내역 -->

				
			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>
</asp:Content>

