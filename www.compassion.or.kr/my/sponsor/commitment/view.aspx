﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="my_commitment_view"  %>


<div style="background:transparent;" class="fn_pop_container" id="modalview">

	<div class="pop_type1 w800 fn_pop_content" style="width:800px;height:1200px;padding-top:50px">
	<div class="pop_title">
		<span>후원 상세정보</span>
		<button class="pop_close" ng-click="modal.hide($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
	</div>

	<div class="pop_content sponsorDetail">
			
		<div class="sub_tit2">
			<span>후원 정보</span>
			<span class="date">
				<span ng-if="entity.fundingfrequency == '1회'">후원일</span>
		<span ng-if="entity.fundingfrequency != '1회'">후원 시작일</span>
				 : {{entity.startdate |  date:'yyyy-MM-dd'}}</span>
		</div>

		<!-- 어린이정보 -->
		<div class="myChild clear2" ng-class="entity.childmasterid == '0000000000' ? 'another' : ''"><!-- 이미지비율5:5일 경우, 'myChild' class에 'another'를 추가해주세요 -->
			<div class="picWrap">
				<!-- 사진사이즈 : 242 * 333 -->
				<div class="pic"><span style="background:url('{{entity.pic}}') no-repeat center top;background-size:cover;"></span></div>

			</div>
			<div class="textWrap">
				<span class="label">{{entity.accountclassname}}</span><br />
				<span class="name">{{entity.accountclassdetail}}</span><br />
					

				<ul class="info" ng-if="entity.childmasterid != '0000000000'">
					<li>
						<span class="field">국가</span>
						<span class="data">{{entity.countryname}}</span>
					</li>
					<li>
						<span class="field">생일</span>
						<span class="data">{{entity.birthdate | date:'yyyy-MM-dd'}} ({{entity.age}}세)</span>
					</li>
					<li>
						<span class="field">성별</span>
						<span class="data">{{entity.gendercode == 'M' ? "남자" : "여자"}}</span>
					</li>
				</ul>

				<div class="tableWrap">
					<p class="tar mb10"  ng-if="entity.fundingfrequency != '1회' && entity.ptd">후원금 최종 적용 월 : <em class="fc_black">{{entity.ptd | date:'yyyy.MM'}}월</em></p>
					<table>
						<caption>후원유형 테이블</caption>
						<colgroup>
							<col style="width:30%" />
							<col style="width:70%" />
						</colgroup>
						<tbody>
							<tr>
								<th>후원유형</th>
								<td>{{entity.fundingfrequency == '1회' ? "일시후원" : "정기후원" }}
									<span ng-if="entity.fundingfrequency == '1회'">/ {{entity.paymentname}}</span>
								</td>
							</tr>
							<tr>
								<th>후원금액</th>
								<td class="fc_blue">{{entity.amount | number:0}} 원 <span ng-if="entity.fundingfrequency != '1회'">/{{entity.fundingfrequency.replace("매" , "")}}</span></td>
							</tr>
						</tbody>
					</table>
				</div>

			</div>
		</div>
		<!--// 어린이정보 -->


		<!-- 납부내역 -->
		<div ng-if="entity.fundingfrequency == '1회'">
		<div class="sub_tit2 nomargin noline">
			<span>납부 내역</span>
		</div>
		<div class="tableWrap2">
			<table class="tbl_type8">
				<caption>일시후원내역 테이블</caption>
				<colgroup>
					<col style="width:50%" />
					<col style="width:50%" />
				</colgroup>
				<tbody>
					<tr>
						<th>{{modal.payments[0].paymentdate }}월</th>
						<td>{{modal.payments[0].totalamount | number:0}} 원</td>
					</tr>
					<tr class="sum">
						<th>총 납부 금액</th>
						<td><em>{{modal.payments[0].totalamount | number:0}}</em> 원</td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>
		<!-- // 납부내역 -->

		<!-- 월별 납부내역 -->
		<div ng-if="entity.fundingfrequency != '1회'">
			
		<div class="sub_tit2 nomargin noline">
			<span>월별 납부 내역</span>
			<span class="posR">
				<span class="sel_type1" style="width:150px">
					<label for="year" class="hidden">년도 선택</label>
					<select class="custom_sel" ng-model="selectedYear" ng-change="modal.changeYear(selectedYear)" data-ng-options="item as item+'년' for item in modal.years">
					</select>
				</span>
			</span>
		</div>
		<div class="tableWrap2">
			<table class="tbl_type8">
				<caption>후원유형 테이블</caption>
				<colgroup>
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
				</colgroup>
				<tbody>
					<tr>
						<th>1월</th>
						<td>{{modal.paymentsByMonth[0]}}</td>
						<th>7월</th>
						<td>{{modal.paymentsByMonth[6]}}</td>
					</tr>
					<tr>
						<th>2월</th>
						<td>{{modal.paymentsByMonth[1]}}</td>
						<th>8월</th>
						<td>{{modal.paymentsByMonth[7]}}</td>
					</tr>
					<tr>
						<th>3월</th>
						<td>{{modal.paymentsByMonth[2]}}</td>
						<th>9월</th>
						<td>{{modal.paymentsByMonth[8]}}</td>
					</tr>
					<tr>
						<th>4월</th>
						<td>{{modal.paymentsByMonth[3]}}</td>
						<th>10월</th>
						<td>{{modal.paymentsByMonth[9]}}</td>
					</tr>
					<tr>
						<th>5월</th>
						<td>{{modal.paymentsByMonth[4]}}</td>
						<th>11월</th>
						<td>{{modal.paymentsByMonth[10]}}</td>
					</tr>
					<tr>
						<th>6월</th>
						<td>{{modal.paymentsByMonth[5]}}</td>
						<th>12월</th>
						<td>{{modal.paymentsByMonth[11]}}</td>
					</tr>
					<tr class="sum">
						<th colspan="2">총 납부 금액</th>
						<td colspan="2"><em>{{modal.totalPayment | number:0}} </em> 원</td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>
		<!--// 월별 납부내역 -->
			
	</div>
</div>

</div>

