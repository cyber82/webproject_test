﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_commitment_default : FrontBasePage {

	public override bool RequireSSL {
		get {
			return utils.chServerState(); //true;
        }
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}
    
    protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		
		// 비회원결제시 결제한 내역 동기화 
		
		this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];
		hd_auth_domain.Value = this.ViewState["auth_domain"].ToString();

		var sess = new UserInfo();
		hdSponsorId.Value = sess.SponsorID;
		
		if(sess.UserClass != "기업" && sess.UserClass != "기타") {
            var birth = sess.Birth.EmptyIfNull().Length > 9 ? sess.Birth.Substring(0, 10) : "";
            hdBirthDate.Value = birth.Replace("-", "");
		}
		hdUserName.Value = sess.UserName;

		var result = new SponsorAction().IsAuthenticated();
	//	Response.Write(result.ToJson());
		if(result.success) {
            //2018-05-23 이종진 - #WO-55 본인인증완료 and 정기후원내역이 존재하면 문구숨김
            var DSList = new CommitmentAction().GetCommitmentList(1, 5, "정기후원");
            DataTable dt = (DataTable)DSList.data;
            if (DSList.success == true && dt.Rows.Count > 0)
            {
                ph_after_auth.Visible = false;
            }
            else
            {
                ph_after_auth.Visible = true;
            }
		} else {
			if(result.action == "sync" && sess.UserClass != "기업" && sess.LocationType == "국내" ) {
			/*
			국외이고 본인인증 안하고 비회원으로 CI등록한(주민번호입력-현금영수증발급한회원) 회원이 연결하기위해서는 조건에서 국내는 빼야함
			if(result.action == "sync" && sess.UserClass != "기업") {
			*/
				
				ph_before_auth.Visible = true;
			}
		}

	}

}