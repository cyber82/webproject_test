﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_pay_account_virtual_account : FrontBasePage {
	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		if(!UserInfo.IsLogin) {
			Response.ClearContent();
			return;
		}

		var actionResult = new CodeAction().Banks();
		DataTable dt = (DataTable)actionResult.data;

		hd_banks.Value = dt.ToJson();

		foreach(DataRow item in dt.Rows) {
			//banks.Items.Add(new ListItem(item["codeName"].ToString() , item["codeId"].ToString()));
		}
		


	}
	
}