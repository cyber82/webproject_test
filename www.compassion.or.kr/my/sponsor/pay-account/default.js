﻿$(function () {

	$("input.payment_method").click(function () {

		if ($(this).attr("id") == "payment_method_oversea") {
			$("#pn_payment_oversea").show();
		} else {
			$("#pn_payment_oversea").hide();
		}
	})

	$("#btn_submit").click(function () {
		if (pay_method == "") {
			alert("후원자님은 현재 설정하신 납부방법이 \n없으므로 변경할 내역이 없습니다.");
			return false;
		}

		var pay_type = $(".payment_method:checked").data("type");
		
		if (pay_type == "cms") {

			if (!checkCMS()) {
				return false;
			}
		    /* 20170317 효성 FMS  작업으로 인해 주석처리 3월20일 월요일 에 주석 삭제
			if (!is_checked_account) {
				alert("계좌 확인을 해주세요");
				return false;
			}
            */

		} else if (pay_type == "oversea_card") {
			if ($("#hd_amount").val() == "") {
				alert("납부예정 금액을 불러오지 못했습니다. ");
				return false;
			}

			var val = $("#oversea_pay_month").val();
			if (val == "" || val == "0" || isNaN(val)) {
				alert("개월수를 입력해 주세요");
				$("#oversea_pay_month").focus();
				return false;
			}
			

		}

		return true;
	})

	$("input.payment_method").click(function () {
		var id = $(this).attr("id");

		var type = $(this).data("type");
		
		if (type == "oversea_card") {
			$("#btn_submit").html("결제");
		} else {
			$("#btn_submit").html("결제정보 수정");
		}

		$(".pn_btn_submit").show();
		$(".payinfo").hide();
		$(".payinfo[data-type='" + type + "']").show();
	});

	var checkedPayType = $("input.payment_method:checked");
	if (checkedPayType.length) {
		$("input.payment_method:checked").trigger("click");
	}

	$("#oversea_pay_month").keyup(function () {

	    var month = $("#oversea_pay_month").val(); //입력한 개월수
	    
	    if (month == "" || isNaN(month)) {
	        $("#oversea_pay_total").html("0<span>&nbsp;&nbsp;원</span>");
	    } else {
	        var val = parseInt(month); //입력한 개월수
	        var now = parseInt($("#hd_amount").val()); //매월 + 매년(현재월일경우) 합계
	        var month = parseInt($("#hd_c_amount").val()); //매월 합계
	        var total = now - month + (month * val);
	        $("#oversea_pay_total").html((total).format() + "<span>&nbsp;&nbsp;원</span>");
	    }
	})
	
	// CMS
	// 계좌정보
	$("input.cms_account_type").click(function () {
		var id = $(this).attr("id");

		$(".cms_account_type1").hide();
		$(".cms_account_type2").hide();

		if (id == "cms_account_type1") {		// 개인
			$(".cms_account_type1").show();
			$("#cms_soc").val("");
		} else {
			$(".cms_account_type2").show();
			$("#cms_birth").val("");
		}
	});

	var checkedCmsAccountType = $("input.cms_account_type:checked");
	if (checkedCmsAccountType.length) {
		$("input.cms_account_type:checked").trigger("click");
	}
	
	// 예금주와의 관계
	$("input.cms_owner_type").click(function () {
		var id = $(this).attr("id");

		if (id == "cms_owner_type1") {		// 개인
			$("#cms_owner").val($("#hd_userName").val());
			$("#cms_birth").val($("#hd_birth").val());
		} else {
			$("#cms_owner").val("");
			$("#cms_birth").val("");
		}
	});

	// 계좌확인
	$("#btn_cms_check_account").click(function () {

		if (checkCMS()) {

			$.post("/api/payment.ashx", {
				t: "check-cms-owner",
				bankCode: $("#hd_cms_bank").val(),
				bankAccount: $("#cms_account").val(),
				birth: ($("#cms_birth").val() == "" ? $("#cms_soc").val() : $("#cms_birth").val()),
				owner: $("#cms_owner").val()
			}, function (r) {

				console.log(r);

				if (r.success) {
					var result = r.data;
					if (result.success) {
						alert("계좌가 확인되었습니다.")
						is_checked_account = true;
					} else {
						alert(result.msg);
					}

				} else {
					alert(r.message)
				}
			});

		}
		return false;
	})

	// CMS 입력값 체크
	var checkCMS = function(){
		
		if ($("#cms_owner").val() == "") {
			alert("이름을 입력해 주세요");
			$("#cms_owner").focus();
			return false;
		}
		if ($("#cms_account_type1").prop("checked")) {
			if ($("#cms_birth").val() == "") {
				alert("생년월일을 입력해 주세요");
				$("#cms_birth").focus();
				return false;
			}

			if ($("#cms_birth").val().length != 6) {
				alert("생년월일을 정확히 입력해 주세요");
				$("#cms_birth").focus();
				return false;
			}
		} else {
			if ($("#cms_soc").val() == "") {
				alert("사업자번호를 입력해 주세요");
				$("#cms_soc").focus();
				return false;
			}

			if ($("#cms_soc").val().length != 10) {
				alert("사업자번호를 정확히 입력해 주세요");
				$("#cms_soc").focus();
				return false;
			}
		}

		if ($(".cms_bank:checked").length < 1 && $("#cms_bank_etc").val() == "") {
			alert("은행을 선택해주세요");
			return false;
		}

		if ($(".cms_bank:checked").length > 0) {
			$("#hd_cms_bank").val($($(".cms_bank:checked")[0]).data("code"));
			$("#hd_cms_bank_name").val($("label[for='" + $($(".cms_bank:checked")[0]).attr("id") + "']").text());
		} else {
			$("#hd_cms_bank").val($("#cms_bank_etc").val());
			$("#hd_cms_bank_name").val($("#cms_bank_etc option:selected").text());
		}


		if ($("#cms_account").val() == "") {
			alert("계좌번호를 입력해 주세요");
			$("#cms_account").focus();
			return false;
		}

		if ($("#cms_account").val().length < 9) {
			alert("계좌번호를 정확히 입력해 주세요");
			$("#cms_account").focus();
			return false;
		}

		return true;
	}

	$(".cms_bank").click(function () {
		$("#cms_bank_etc").val("");
		$("#ibk_bank").hide();
		$("#nong_bank").hide();

		if ($(this).data("code") == 03) {
			$("#ibk_bank").show();
		} else if ($(this).data("code") == 12 || $(this).data("code") == 11) {
			$("#nong_bank").show();
		}
	})

	$("#cms_bank_etc").change(function () {
		if ($("#cms_bank_etc").val() != "") {
			$(".cms_bank").prop("checked", false);
		}
	});

	var cmsBank = $("#hd_cms_bank").val();
	if (cmsBank != "") {

		var checkedCms = false;
		$.each($(".cms_bank"), function () {
			if ($(this).data("code") == cmsBank) {
				$(this).prop("checked", true);
				checkedCms = true;
			}
		})

		if (!checkedCms) {
			$("#cms_bank_etc").val(cmsBank);
		}
	}

	$("#cms_account").keyup(function () {
		
//		if ($("#cms_account").val() != checked_account_val) {
			is_checked_account = false;
//		}
		
	})
})

var checked_account_val = "";
var is_checked_account = false;
var pay_method = "";

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		$scope.requesting = false;
		$scope.total = 0;
		$scope.rowsPerPage = 10;

		$scope.list = null;
		$scope.params = {
			page: 1,
			rowsPerPage: $scope.rowsPerPage
		};


        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function () {

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();

		// 가상계좌발급 팝업
		$scope.modal = {
			instance: null,
			banks: [],
			virtual_accounts: [],

			init: function () {
				popup.init($scope, "/my/sponsor/pay-account/virtual-account", function (modal) {
					$scope.modal.instance = modal;

					if (getParameterByName("action") == "virtual-account")
						$scope.modal.show();

				}, { top: 0, iscroll: true });
			},

			show: function ($event) {

				if ($event)
					$event.preventDefault();

				if (!$scope.modal.instance)
					return;

				$scope.modal.instance.show();

				// 발급된 가상계좌
				$http.get("/api/my/payment.ashx?t=virtual-account", {}).success(function (r) {
					console.log(r);
					if (r.success) {

						$scope.modal.virtual_accounts = r.data;

					} else {
						alert(r.message);
					}
				});

				// 은행목록
				var banks = $.parseJSON($("#hd_banks").val());
				$("#banks").empty();

				$.each(banks, function (r) {
					var codeId = this.CodeID;
					var codeName = this.CodeName;

					$("#banks").append($("<option value='"+ codeId +"'>"+codeName+"</option>"));
				});


				$(".custom_sel").selectbox({});


			},

			hide: function ($event) {
				$event.preventDefault();
				if (!$scope.modal.instance)
					return;
				$scope.modal.instance.hide();

			},

			request: function ($event) {

				$event.preventDefault();

				var bankCode = $("#banks").val();
				if (bankCode == "") return;
				var bankName = $("#banks option:selected").text();
				if ($scope.requesting) return;
				$scope.requesting = true;
				
				
				$http.post("/api/my/payment.ashx?t=set-virtual-account", {bankCode: bankCode, bankName: bankName}).success(function (r) {
					$scope.requesting = false;
					console.log(r);
					alert(r.message);
					if (r.success) {
						$scope.modal.virtual_accounts = r.data;
					} else {
						//alert(r.message);
					}
				});
				
			}
		};
		
		// 정기후원내역
		$scope.getList = function (params) {

			//$scope.list = null;
			//$scope.total = null;

			$scope.params = $.extend($scope.params, params);
			
			$http.get("/api/my/payment.ashx?t=reservation", { params: $scope.params }).success(function (r) {
			//	console.log(r);
				if (r.success) {
					
				    var money = 0;
					var list = r.data;

					console.log("reservation" , list);

					if (list.length > 0) {
						$scope.currentMonthAmount = list[0].currentmonthamount;
						$("#hd_amount").val($scope.currentMonthAmount);

						$.each(list, function () {
						    this.startdate = new Date(this.startdate);
						    this.ptd = new Date(this.ptd);
						    if (this.ptd > new Date()) {
						        //	this.fundingfrequency = "선납<br/>(" + moment(this.ptd).format('YYYY.MM') + ")";
						    }

						    if (this.fundingfrequency == "매월") {
						        money += parseInt(this.amount);
						    }
						});

						$("#hd_c_amount").val(money);

						$("#oversea_pay_month").trigger("keyup");
					}

					$scope.list = list;
					$scope.total = list.length > 0 ? list[0].total : 0;

					if (params)
						scrollTo($("#l"), 30);

				} else {

					if (r.action == "not_sponsor") {
						$scope.currentMonthAmount = 0;
						$scope.total = 0;
					}
				}
			});

		}

		// 자동이체 정보 
		$scope.getPaymentWay = function () {

			$http.get("/api/my/payment.ashx?t=way", {}).success(function (r) {
				console.log(r);
				if (r.success) {

				    //alert(JSON.stringify(r.data));

					$scope.way = r.data;
					pay_method = $scope.way.pay_method;

				    //해외카드 신용카드자동결제 CMS 지로 가상계좌
					var strPayMethod = '';
					if (pay_method == '해외카드' || pay_method == '신용카드자동결제' || pay_method == 'CMS') {
					    if (pay_method == '해외카드' || pay_method == '신용카드자동결제') strPayMethod = 'CARD';
					    else if (pay_method == 'CMS') strPayMethod = 'CMS';
					}
					$('#prevPaymentType').val(strPayMethod);

				} else {

					// 후원회원 아님
					if (r.action == "not_sponsor") {

					}
				}
			});

		};

		// 가상계좌정보
		$scope.getVirtualAccounts = function () {
			// 발급된 가상계좌
			$http.get("/api/my/payment.ashx?t=virtual-account", {}).success(function (r) {
				console.log(r.data);
				if (r.success) {

					$scope.virtual_accounts = r.data;

				} else {
				//	alert(r.message);
				}
			});

		}

		$scope.getPaymentWay();

		$scope.getList();

		$scope.getVirtualAccounts();

		$scope.modal.init();

	});

})();