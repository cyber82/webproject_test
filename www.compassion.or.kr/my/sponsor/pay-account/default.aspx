﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_pay_account_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>

<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form_temporary" %>
<%@ Register Src="/pay/kcp_batch_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/cms_form.ascx" TagPrefix="uc" TagName="cms_form" %>
<%--<%@ Register Src="/my/sponsor/pay-account/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form_temporary" %>--%>
<%--<%@ Register Src="/my/sponsor/pay-account/kcp_batch_form.ascx" TagPrefix="uc" TagName="kcp_form" %>--%>
<%--<%@ Register Src="/my/sponsor/pay-account/cms_form.ascx" TagPrefix="uc" TagName="cms_form" %>--%>

<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/my/sponsor/pay-account/default.js"></script>
    <script type="text/javascript">
		function downloadFilePDF(src){
            var link=document.createElement('a');
            document.body.appendChild(link);
            link.href= src;
            link.download = '';
            link.click();
        }
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hd_amount" value="" />
    <input type="hidden" runat="server" id="hd_c_amount" value="0" />
	<input type="hidden" runat="server" id="hd_userName" value="" />
	<input type="hidden" runat="server" id="hd_birth" value="" />


	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
        <!-- 타이틀 -->
		<div class="page_tit" ng-if ="AdminCK == false">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980">

				<uc:menu runat="server"  />


				<!-- 결제정보관리 -->
				<div class="box_type4 mb40">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit">
						<p class="tit">납부방법 변경</p>
					</div>
					<!--// -->

					<!-- 카드 -->
					<div ng-if="way.pay_method == '해외카드'">
					<div class="sub_desc">
						<p class="txt2">
							후원자님께서 현재 이용하고 계시는 납부방법은<br />
							<em class="em1">해외카드</em>입니다.
						</p>
					</div>
					
					</div>
					<!--// -->

					<!-- 카드 -->
					<div ng-if="way.pay_method == '신용카드자동결제'">
					<div class="sub_desc">
						<p class="txt2">
							후원자님께서 현재 이용하고 계시는 납부방법은<br />
							<em class="em1">신용카드 자동결제</em>입니다.
						</p>
					</div>
					<div class="box_type3 tac mb40">
						<p class="s_con3 mb15">결제정보</p>
						<p class="s_con4"> {{way.pay_name}} / 매월 {{way.pay_day}}일</p>
					</div>
					</div>
					<!--// -->

					<!-- CMS -->
					<div ng-if="way.pay_method == 'CMS'">
					<div class="sub_desc">
						<p class="txt2">
							후원자님께서 현재 이용하고 계시는 납부방법은<br />
							<em class="em1">CMS 자동결제</em>입니다.
						</p>
					</div>
					<div class="box_type3 tac mb40">
						<p class="s_con3 mb15">결제정보</p>
						<p class="s_con4">  {{way.pay_name}} / {{way.pay_account}} / 매월 {{way.pay_day}}일</p>
					</div>
					</div>
					<!--// -->

					<!-- 지로 -->
					<div ng-if="way.pay_method == '지로'">
						<div ng-if="way.pay_title == 'CMS신청 중'">
							<div class="sub_desc" >
								<p class="txt2">후원자님께서 현재 이용하고 계시는 납부방법은  <em class="em1">CMS 신청중</em> 입니다</p>
							</div>
						</div>
						<div ng-if="way.pay_title != 'CMS신청 중'">
							<div class="sub_desc" >
								<p class="txt2">후원자님께서 현재 이용하고 계시는 납부방법은 <em class="em1">지로</em>입니다.</p>
							</div>
							<div class="box_type3 tac mb40">
								<p class="s_con3">받으시는 주소 : {{way.address}}</p>
							</div>
						</div>
					
					</div>
					<!--// -->

					<!-- 가상계좌 -->
					<div ng-if="way.pay_method == '가상계좌'">
					<div class="sub_desc" >
						<p class="txt2">
							후원자님께서 현재 이용하고 계시는 납부방법은 <em class="em1">가상계좌</em>입니다.<br />
							<span class="txt1">신용카드 및 CMS 자동이체 신청을 원하시는 경우 아래에서 결제를 선택해주세요.</span>
						</p>
					</div>
					<div class="v_account_wrap mb40">
						<ul class="v_account" >
							<li ng-repeat="item in way.virtual_accounts"><p class="name">{{item.bankname}}</p><p>{{item.accountnumber}}</p></li>
						</ul>
					</div>
					</div>
					<!--// -->

					<!-- 정기결제 없음 -->
					<div class="sub_desc padding1" ng-if="way && !way.has_detail">
						<p class="txt2">후원자님께서 현재 이용하고 계시는 <em class="em1">정기 결제정보</em>가 없습니다.</p>
					</div>
					<!--// -->
					
					

<%--				<!-- 2018-03-30 이종진 - #13222에 의해 정기결제 후원내역은 신설페이지로 이동. -->	
                    <!-- 정기결제후원내역 리스트 -->
					<p class="s_tit3 mb20" id="l">정기결제 후원 내역</p>
					<div class="tbl_sort mb15">
						<span class="number">총 <em class="fc_blue">{{total}}</em>건</span>
						
						<span class="number fr">이 달의 후원금액 : <em class="fc_blue">{{currentMonthAmount | number:0}}</em>원</span>
						
						<!-- 해외발급 카드일 경우 
						<span class="number fr">1개월 후원금액 : <em class="fc_blue">160,000</em>원</span>
						-->

					</div>
					<div class="tableWrap1 mb30">
						<table class="tbl_type6 padding2">
							<caption>정기결제 후원내역 테이블</caption>
							<colgroup>
								<col style="width:18%" />
								<col style="width:34%" />
								<col style="width:14%" />
								<col style="width:17%" />
								<col style="width:17%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">후원 종류</th>
									<th scope="col">후원금 제목</th>
									<th scope="col">결제주기</th>
									<th scope="col">후원금액</th>
									<th scope="col">후원 신청일</th>
								</tr>
							</thead>
							<tbody>
								
								<tr ng-repeat="item in list">
									<td>{{item.accountclassname}}</td>
									<td class="tit">{{item.accountclassdetail}}</td>
									<td ng-bind-html="item.fundingfrequency"></td>
									<td class="price">{{item.amount | number:0}}원</td>
									<td>{{item.startdate | date:'yyyy.MM.dd'}}</td>
								</tr>
								<tr ng-if="total == 0">
									<td colspan="5" class="no_content">
										<p class="mb20">정기결제 후원 내역이 없습니다.</p>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// 정기결제후원내역 리스트 -->

					<!-- page navigation -->
					<div class="tac mb30">
						<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>
					</div>
					<!--// page navigation -->

					<!-- 지로선납 시 노출 -->
					<div class="box_type5 pl40">
						<p class="lineH_22">
							자동이체를 신청하시면 당월부터 후원금이 자동 납부됩니다. 선납 후원금 적용 후 자동이체를 신청하시면 당월부터 후원금이 자동 납부됩니다.<br />
							선납 후원금 적용 후 자동이체를 원하시면 한국컴패션으로 연락 주시기 바랍니다.<br />
							후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a>
						</p>
					</div>
					<!--// -->--%>

				<%--</div>--%>
				<!--// 결제정보관리 -->

				<!-- 결제정보 -->
				<p class="s_tit3 mb20" ng-show="total > 0">결제 정보</p>
				<div class="box_type4" ng-show="total > 0">

					<div class="mb30">
						<table class="tbl_type1 line1">
							<caption>결제방법 선택 테이블</caption>
							<colgroup>
								<col style="width:20%" />
								<col style="width:80%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">결제방법</th>
									<td>
										<span class="radio_ui">
											<input type="radio" id="payment_method_cms" runat="server" name="payment_method" class="css_radio payment_method" data-type="cms" />
											<label for="payment_method_cms" class="css_label" style="width:170px">CMS 자동이체</label>

											<input type="radio" id="payment_method_card" runat="server" name="payment_method"  class="css_radio payment_method" data-type="card" />
											<label for="payment_method_card" class="css_label mr30">신용카드 자동이체</label>

											<asp:PlaceHolder runat="server" ID="ph_payment_method_oversea" Visible="false">
											<input type="radio" id="payment_method_oversea" runat="server" name="payment_method"  class="css_radio payment_method" data-type="oversea_card" />
											<label for="payment_method_oversea" class="css_label">해외발급 카드 즉시결제</label>
											</asp:PlaceHolder>
										</span>
									</td>
								</tr>
								
								<!-- 자동이체 선택시 -->
								<tr class="payinfo" data-type="cms" style="display:none">
									<th scope="row">예금주와의 관계</th>
									<td>
										<span class="radio_ui">
											<input type="radio" id="cms_owner_type1" value="본인" name="cms_owner_type" runat="server" class="cms_owner_type css_radio" checked="true" />
											<label for="cms_owner_type1" class="css_label" style="width:170px">본인</label>

											<input type="radio" id="cms_owner_type2" value="타인" name="cms_owner_type" runat="server" class="cms_owner_type css_radio" />
											<label for="cms_owner_type2" class="css_label">타인</label>
										</span>
									</td>
								</tr>
								<tr class="payinfo" data-type="cms" style="display:none">
									<th rowspan="2" scope="row">계좌정보</th>
									<td>
										<span class="radio_ui">
											<input type="radio" id="cms_account_type1" name="cms_account_type" runat="server" class="cms_account_type css_radio" checked />
											<label for="cms_account_type1" class="css_label" style="width:170px">개인계좌</label>

											<input type="radio" id="cms_account_type2" name="cms_account_type" runat="server" class="cms_account_type css_radio" />
											<label for="cms_account_type2" class="css_label">기업계좌</label>
										</span>
									</td>
								</tr>
								<tr class="payinfo" data-type="cms" style="display:none">
									<td>
										<label class="hidden">계좌소유주명</label>
										<input type="text" class="input_type2 mb10" id="cms_owner" runat="server" value="" style="width:400px" maxlength="10" placeholder="이름" /><br />

										<label class="hidden">주민등록번호 앞자리</label>
										<input type="text" class="input_type2 mb20 number_only cms_account_type1" id="cms_birth" runat="server" value="" maxlength="6" style="width:400px" placeholder="생년월일(6자리)" /><br class="cms_account_type1" />

										<label class="hidden">사업자번호</label>
										<input type="text" class="input_type2 mb20 number_only cms_account_type2" id="cms_soc" runat="server" value="" maxlength="10" placeholder="사업자번호 (-없이 입력)" style="width:400px" /><br class="cms_account_type2" />
										
										<input type="hidden" runat="server" id="hd_cms_bank" value="" />
										<input type="hidden" runat="server" id="hd_cms_bank_name" value="" />
										<span class="radio_ui mb15">
											<input type="radio" id="bank_1" data-code="004" name="cms_bank" class="cms_bank css_radio"  />
											<label for="bank_1" class="css_label" style="width:120px">국민은행</label>

											<input type="radio" id="bank_2" data-code="020" name="cms_bank" class="cms_bank css_radio" />
											<label for="bank_2" class="css_label" style="width:140px">우리은행</label>

											<input type="radio" id="bank_3" data-code="012" name="cms_bank" class="cms_bank css_radio" />
                                                <label for="bank_3" class="css_label" style="width: 120px">농협회원조합</label>

											<input type="radio" id="bank_4" data-code="011" name="cms_bank" class="cms_bank css_radio" />
											<label for="bank_4" class="css_label" style="width:120px">농협중앙회</label>

										</span>
										<span class="radio_ui mb10">
											<input type="radio" id="bank_5" data-code="088" name="cms_bank" class="cms_bank css_radio" />
											<label for="bank_5" class="css_label" style="width:120px">신한은행</label>

											<input type="radio" id="bank_6" data-code="081" name="cms_bank" class="cms_bank css_radio" />
											<label for="bank_6" class="css_label" style="width:140px">KEB하나은행</label>

											<input type="radio" id="bank_7" data-code="003" name="cms_bank" class="cms_bank css_radio" />
											<label for="bank_7" class="css_label" style="width:120px">기업은행</label>

											<span class="sel_type2" style="width:120px;">
												<label for="cms_bank_etc" class="hidden">그외 은행 선택</label>
												<asp:DropDownList runat="server" ID="cms_bank_etc" cssClass="custom_sel" >
													<asp:ListItem Text="그 외" Value=""></asp:ListItem>
												</asp:DropDownList>
											</span>
										</span>

										 <p class="guide_comment3 mb20" id="ibk_bank" style="display: none">
                                                기업은행 평생계좌(휴대번호 등)은 반드시 모계좌로 등록해주세요.
                                            </p>
                                           
											<div class="guide_comment3" style="display: none" id="nong_bank">
												<em class="fc_blue">농협계좌</em>로 자동이체를 신청하시는 경우 <em class="fc_blue">통장을 잘 확인</em>하여 은행명을 지정해 주세요.
												<ul class="pl30 lineH_22 mb20">
													<li><span class="s_con1">농협대표,농협중앙회,농협은행 ▶ 농협중앙회</span></li>
													<li><span class="s_con1">단위농협,농협회원조합 ▶ 농협회원조합</span></li>
												</ul>
											</div>

										<label class="hidden">계좌번호입력</label>
										<input type="text" class="input_type2 mb10 number_only" maxlength="30" id="cms_account" runat="server" value="" placeholder="계좌번호 (-없이 입력)" style="width:400px" />
										<!-- 20170317 효성 FMS  작업으로 인해 주석처리 3월20일 월요일 에 주석 삭제
                                        <button class="btn_s_type7 ml5 mb10" id="btn_cms_check_account"><span>계좌 확인</span></button>
                                        -->
									</td>
								</tr>
								<tr class="payinfo" data-type="cms" style="display:none">
									<th scope="row">이체일</th>
									<td>
										<span class="radio_ui mb20">
											<input type="radio" id="cmsday_5" value="05" name="cmsday" runat="server" class="css_radio" checked />
											<label for="cmsday_5" class="css_label" style="width:170px">매월 5일</label>

											<input type="radio" id="cmsday_15" value="15" name="cmsday" runat="server" class="css_radio" />
											<label for="cmsday_15" class="css_label" style="width:170px">매월 15일</label>

											<input type="radio" id="cmsday_25" value="25" name="cmsday" runat="server" class="css_radio" />
											<label for="cmsday_25" class="css_label">매월 25일</label>
										</span><br />

										<div class="box_type5 pl40">
											<p class="s_tit4 mb5">재출금일 안내</p>
											<p class="lineH_20">
												자동이체일이 5일, 15일인 경우 : 매월 20일, 27일<br />
												자동이체일이 25일인 경우 : 매월 27일
											</p>
										</div>
									</td>
								</tr>
								<!--// -->

								<!-- 신용카드 선택시 -->
								<tr class="payinfo" data-type="card" style="display:none">
									<th scope="row">카드주</th>
									<td>
										<span class="radio_ui">
											<input type="radio" id="cmsKind1"  name="cardOwner"  class="css_radio" checked />
											<label for="cmsKind1" class="css_label" style="width:170px">본인</label>

											<input type="radio" id="cmsKind2" name="cardOwner" class="css_radio" />
											<label for="cmsKind2" class="css_label">타인</label>
										</span>
									</td>
								</tr>
								<tr class="payinfo" data-type="card" style="display:none">
									<th scope="row">결제일</th>
									<td>
										<span class="radio_ui mb20">
											<input type="radio"  class="css_radio paymentDay" checked id="paymentDay5" value="05" runat="server" name="paymentDay"   />
											<label for="paymentDay5" class="css_label" style="width:170px">매월 5일</label>

											<input type="radio"  class="css_radio paymentDay"  id="paymentDay15" value="15" runat="server" name="paymentDay"  />
											<label for="paymentDay15" class="css_label" style="width:170px">매월 15일</label>

											<input type="radio"  class="css_radio paymentDay"  id="paymentDay25" value="25" runat="server" name="paymentDay"  />
											<label for="paymentDay25" class="css_label">매월 25일</label>
										</span><br />

										<div class="box_type5 pl40">
											<p class="s_tit4 mb5">재결제일 안내</p>
											<p class="lineH_20">
												자동이체일이 5일, 15일인 경우 : 매월 20일, 27일<br />
												자동이체일이 25일인 경우 : 매월 27일
											</p>
										</div>
									</td>
								</tr>
								<!--// -->

								<!-- 해외발급카드 선택 시 -->
								<tr class="payinfo" data-type="oversea_card" style="display:none">
									<th scope="row">결제금액</th>
									<td>
										<label class="hidden">개월 수 입력</label>
										<input type="text" class="input_type2 mr10 number_only" runat="server" id="oversea_pay_month" value="12"  maxlength="3" style="width:200px" /><span class="fs15 vam">개월</span>
										<span class="txt_type1 ml30 vam" id="oversea_pay_total"></span>

										<p class="pt15"><span class="s_con1">결제관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a></span></p>
									</td>
								</tr>
								<!--// -->
							</tbody>
						</table>

					</div>
                    
					<div class="tac mb60 pn_btn_submit" style="display:none">
						<!--<a href="#" class="btn_type1">결제정보 수정</a>-->
                        <input type="hidden" runat="server" id="prevPaymentType" />
                        <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" cssClass="btn_type1"  ng-if ="AdminCK == false">결제정보 수정</asp:LinkButton>
					</div>
					
					<!-- 가상계좌발급 -->
					<div>
						<p class="s_tit5 mb10">가상계좌발급</p>
						<p class="mb10"  ng-if ="AdminCK == false">
                            <span class="fs14 spacing05">가상계좌 발급 신청을 통해서도 납부하실 수 있습니다. 새로운 가상계좌 발급을 원하시는 경우 가상계좌발급 버튼을 클릭해 주세요.</span>
							<a href="#" class="btn_s_type2 ml20" ng-click="modal.show($event)">가상계좌 발급</a>
						</p>

						<div class="v_account_wrap mb30" ng-if="virtual_accounts">
							<p class="s_tit5 tac mb30" >현재 발급된 가상계좌</p>

							<ul class="v_account">
								<li ng-repeat="item in virtual_accounts"><p class="name">{{item.bankname}}</p><p>{{item.accountnumber}}</p></li>
							</ul>
						</div>
					</div>
					<!--// 가상계좌발급 -->

					<div class="box_type3 pl40">
						<p class="mb15">자동이체를 신청하시는 경우 ‘공인인증서’가 필요합니다.</p>

						<ol class="mb20">
							<li class="mb10">
								1. 공인인증서가 있으신 경우
								<ul class="ml20">
									<li><span class="s_con1">CMS 자동이체 신청 후 승인완료까지 최대 일주일이 소요되며, 전사서명처리는 MS Internet Explorer 브라우저에서만 사용 가능합니다.</span></li>
									<li><span class="s_con1">신용카드 자동이체로 후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</span></li>
								</ul>
							</li>
							<li>
								2. 공인인증서가 없으신 경우
								<ul class="ml20">
									<li><span class="s_con1">전화녹취 : 한국 컴패션으로 전화하여 녹취로 신청 (Tel : 02-740-1000)</span></li>
									<li><span class="s_con1">팩스접수 : 자동이체 신청서 작성 후 컴패션으로 송부 (Fax : 02-740-1001)</span></li>
									<li>
										<span class="s_con1">
											우편접수 : 자동이체 신청서 작성 후 한국컴패션으로 우편발송<br />
											<span class="fc_blue">주소 : (04418) 서울시 용산구 한남대로 102-5 (한남동 723-41) 석전빌딩</span>
										</span>
									</li>
								</ul>
							</li>
						</ol>

						<%--<a href="/common/download/automatic_request.pdf" class="btn_s_type4">자동이체 신청서 다운로드</a>--%>
                        <a href="javascript:downloadFilePDF('/common/download/automatic_request.pdf')" class="btn_s_type4">자동이체 신청서 다운로드</a>
                        
					</div>

				</div>
				<!--// 결제정보관리 -->
               </div>
				
			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>

</asp:Content>


<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<uc:cms_form runat="server" EnableViewState="true" ViewStateMode="Enabled" ID="cms_form" />
	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" ID="kcp_form" />
	<uc:kcp_form_temporary runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form_temporary" />
</asp:Content>
