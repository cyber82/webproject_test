﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_pay_account_default : FrontBasePage {

	public override bool RequireSSL {
		get {
			return utils.chServerState(); //false;
        }
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}
    
    protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

        JsonWriter actionResult;
        //2018-03-30 이종진 - #13222 해외카드결제는 신설페이지로 이동
        //actionResult = new PaymentAction().CanUseOverseaCard();
        //if(!actionResult.success) {
        //	base.AlertWithJavascript(actionResult.message);
        //} else {
        //	
        //	var canUseOverseaCard = (bool)actionResult.data;
        //	ph_payment_method_oversea.Visible = canUseOverseaCard;
        //    
        //    //Response.Write(canUseOverseaCard);
        //}

        UserInfo sess = new UserInfo();
		cms_owner.Value = hd_userName.Value = sess.UserName;
		if (!string.IsNullOrEmpty(sess.Birth))
			cms_birth.Value = hd_birth.Value = sess.Birth.Replace("-" , "").Substring(2,6);

		// CMS

		actionResult = new CodeAction().CMSBanks();
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message , "goBack()");
		} else {

			var exceptCodes = new string[] { "004", "011", "012", "088", "020", "081", "003" };        // radio 로 제공하는 은행
			DataTable dt = (DataTable)actionResult.data;
			foreach(DataRow dr in dt.Rows) {
				if(exceptCodes.Contains(dr["codeId"].ToString()))
					continue;
				cms_bank_etc.Items.Add(new ListItem(dr["name"].ToString() , dr["codeId"].ToString()));
			}
		}
        //cms_bank_etc
        
    }

	protected override void loadComplete( object sender, EventArgs e ) {
	
	}
	
	protected void btn_submit_Click( object sender, EventArgs e ) {
		if(base.IsRefresh) {
		//	return;
		}

		if(payment_method_oversea.Checked) {
			//pn_payment_oversea.Style["display"] = "block";
		}

		var sess = new UserInfo();

		cms_form.Visible = false;
		kcp_form.Visible = false;
		kcp_form_temporary.Visible = false;

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "PayAccount";

        if (payment_method_card.Checked) {

			this.ViewState["kcpgroup_id"] = ConfigurationManager.AppSettings["kcpgroup_id"];
			this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_batch_site_cd"];
            this.ViewState["user_name"] = sess.UserName;
            this.ViewState["paymentType"] = prevPaymentType.Value;

            kcp_form.Visible = true;
			kcp_form.Show(this.ViewState);

		} else if(payment_method_oversea.Checked) {

			this.ViewState["good_name"] = "한국컴패션후원금";
			this.ViewState["buyr_name"] = sess.UserName;
			this.ViewState["month"] = oversea_pay_month.Value;
			this.ViewState["good_mny"] = (Convert.ToInt32(hd_amount.Value) - Convert.ToInt32(hd_c_amount.Value) + (Convert.ToInt32(oversea_pay_month.Value) * Convert.ToInt32(hd_c_amount.Value))).ToString();
			this.ViewState["pay_method"] = "100000000000";
			this.ViewState["used_card_YN"] = "Y";
			this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_oversea"];
			this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card_Foreign"];
            this.ViewState["paymentType"] = prevPaymentType.Value;

            kcp_form_temporary.Visible = true;
			kcp_form_temporary.Visible = true;
            kcp_form_temporary.Visible = true;
			kcp_form_temporary.Show(this.ViewState);

		} else if(payment_method_cms.Checked) {
			
			var actionResult = new PaymentAction().GetBankAccount();
			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message);
			} else {

				// 기존에 등록한 계좌와 동일한경우
				var data = (PaymentAction.BankAccount)actionResult.data;
				if(data != null) {
					if(data.code == hd_cms_bank.Value && data.account == cms_account.Value) {
						base.AlertWithJavascript(@"이미 동일한 계좌로 CMS 등록이 되어있습니다."
						+ "\\n동일한 계좌로 재 신청 하시는 경우 후원금이 정상 출금되지 않습니다."
						+ "\\n\\n이체일 변경을 원하시거나 "
						+ "\\n그 밖의 문의가 있으신 경우 한국컴패션으로 전화 주세요."
						+ "\\n전화 : 02-740-1000 (구 02-3668-3400) 평일09:00~18:00/토,일휴무");
						return;
					}
				}
				
				this.ViewState["relation"] = cms_owner_type1.Checked ? cms_owner_type1.Value : cms_owner_type2.Value;
				this.ViewState["cms_owner"] = cms_owner.Value;
				this.ViewState["cms_account"] = cms_account.Value;
				this.ViewState["birth"] = cms_account_type1.Checked ? cms_birth.Value : cms_soc.Value;
				this.ViewState["bank_name"] = hd_cms_bank_name.Value;
				this.ViewState["bank_code"] = hd_cms_bank.Value;
				this.ViewState["cmsday"] = cmsday_5.Checked ? cmsday_5.Value : (cmsday_15.Checked ? cmsday_15.Value : cmsday_25.Value);
                this.ViewState["paymentType"] = prevPaymentType.Value;

                this.ViewState["src"] = string.Format("예금주와의 관계 : {0} /\n예금주 : {1} /\n예금주 생년월일 : {2} /\n은행명 : {3} /\n"
						   + "은행번호 : {4} /\n계좌번호 : {5} /\n이체일 : {6} /\n"
						   , this.ViewState["relation"]
						   , this.ViewState["cms_owner"]
						   , this.ViewState["birth"]
						   , this.ViewState["bank_name"]
						   , this.ViewState["bank_code"]
						   , this.ViewState["cms_account"]
						   , this.ViewState["cmsday"]
						   );


				cms_form.Visible = true;
				cms_form.Show(this.ViewState);


			}

		}

	}
}