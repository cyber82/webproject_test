﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_gift_money_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/sponsor/gift-money/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
        <!-- 타이틀 -->
		<div class="page_tit" ng-if="AdminCK == false">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980 money">

				<uc:menu runat="server"  />

				<!-- 선물금이란 -->
				<div class="box_type4 mb40">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit">
						<p class="tit">어린이 선물금</p>
					</div>
					<!--// -->

					<!-- 서브 문구 -->
					<div class="sub_desc">
						<p class="txt2">선물금이 뭔가요?</p>
					</div>
					<!--// -->

					<div class="bg_info">
						<p>
							<em>후원자님의 마음을 담아 선물금을 보내주세요.</em><br />
							보내주신 선물금으로 어린이와 가족, 어린이센터 교사가 함께 의논해 어린이가 가장 원하고 가장 필요한<br />
							선물을 준비하게 됩니다. 사랑하는 후원자님으로부터 받은 선물금을 통해 어린이는 더 큰 기쁨을 갖게 됩니다.
						</p>
					</div>
					
					<div class="kind">
						<p class="s_tit3 mb15">어떤 선물금을 줄 수 있나요?</p>
						<ul class="clear2">
							<li class="kind1">
								<p class="s_tit5 mb10">생일 선물금</p>
								<span class="txt1">어린이의 생일을 위한 선물금</span><br />
								<span class="txt2">최소 1만 5천원 ~ 최대 10만원까지</span>
							</li>
							<li class="kind2">
								<p class="s_tit5 mb10">어린이 선물금</p>
								<span class="txt1">어린이에게 전하는 특별한 선물금</span><br />
								<span class="txt2">최소 1만 5천원 ~ 최대 10만원까지<br />연에 1~2회를 추천합니다.</span>
							</li>
							<li class="kind3">
								<p class="s_tit5 mb10">가족 선물금</p>
								<span class="txt1">어린이 가족에게 전하는 마음의 선물금</span><br />
								<span class="txt2">최소 3만 5천원 ~ 최대 100만원까지<br />연에 100만원까지 가능합니다.</span>
							</li>
							<li class="kind4">
								<p class="s_tit5 mb10">어린이센터 선물금</p>
								<span class="txt1">어린이가 등록된 어린이센터에 전하는 모두를 위한 선물금</span><br />
								<span class="txt2">최소 15만원 ~ 최대 200만원까지<br />연에 200만원까지 가능합니다.</span>
							</li>
							<li class="kind5">
								<p class="s_tit5 mb10">크리스마스 선물금</p>
								<span class="txt1">컴패션에 등록된 모든 어린이를 위한 크리스마스 선물금</span><br />
								<span class="txt2">한 어린이당 2만원의 선물금을 추천합니다.</span>
							</li>
						</ul>
					</div>
					

				</div>
				<!--// 선물금이란 -->

				<!-- 선물금 예약내역 
                <div style="float:right;padding-right:300px;color:red;">
                        <span>3월 1일, 선물금 예약월이 어린이 생일 1개월 전으로 변경됩니다.<br />예시) 어린이 생일이 7월인 경우, 기존 예약월 4월 -> 변경 예약월 6월</span></p>
                </div>
                    -->
				<p class="mb15"><span class="s_tit3 vam">선물금 예약 내역</span>
                    <a href="/my/sponsor/gift-money/pay/?t=regular" ng-if="giftable && AdminCK == false" runat="server" id="btn_reservation" class="btn_s_type2 vam ml20">선물금 예약하기</a></p>
                    
				<div class="box_type4 mb40">

					<div class="tableWrap1 mb30">
						<table class="tbl_type6 padding1">
							<caption>선물금 예약 내역 테이블</caption>
							<colgroup>
								<col style="width:15%" />
								<col style="width:27%" />
								<col style="width:16%" />
								<col style="width:16%" />
								<col style="width:10%" />
								<col style="width:16%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">신청일</th>
									<th scope="col">어린이 이름</th>
									<th scope="col">선물금 종류</th>
									<th scope="col">금액</th>
									<th scope="col">예약달</th>
									<th scope="col">수정/삭제</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in gr_list">
									<td>{{item.startdate | date:'yyyy.MM.dd'}}</td>
									<td>{{item.namekr}}</td>
									<td>{{item.sponsoritem}}</td>
									<td class="price">{{item.sponsoramount | number:0}}원</td>
									<td>{{item.fundingfrequency}} {{item.sponsormonth}}월</td>
									<td>
										<div class="btn_md">
                                            <button ng-click="goUpdate($event , item);" class="modify" ng-if="AdminCK == false"><span>수정</span></button>
											<span class="bar"></span>
                                            <a ng-click="deleteGiftReservation(item.commitmentid)"><button class="del" ng-if="AdminCK == false"><span>삭제</span></button></a>
										</div>
									</td>
								</tr>
								
								<tr ng-if="gr_total == 0">
									<td colspan="6" class="no_content">등록된 선물금 예약 정보가 없습니다.</td>
								</tr>
							</tbody>
						</table>
					</div>

					<!-- page navigation -->
					<div class="tac">
						<paging class="small" page="gr_page" page-size="gr_rowsPerPage" total="gr_total" show-prev-next="true" show-first-last="true" paging-action="getGiftReservationList(page)"></paging>
					</div>
					<!--// page navigation -->

				</div>
				<!--// 선물금 예약내역-->

				<!-- 어린이 선물금 결제내역 -->
				<p class="mb15"><span class="s_tit3 vam">선물금 결제 내역</span><a href="/my/sponsor/gift-money/pay/?t=temporary"  class="btn_s_type2 vam ml20"  ng-if="AdminCK == false">선물금 보내기</a></p>
				<div class="box_type4 mb20">

					<div class="tableWrap1 mb30">
						<table class="tbl_type6 padding1">
							<caption>어린이 선물금 결제내역 테이블</caption>
							<colgroup>
								<col style="width:15%" />
								<col style="width:33%" />
								<col style="width:18%" />
								<col style="width:17%" />
								<col style="width:17%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">결제일</th>
									<th scope="col">어린이 이름</th>
									<th scope="col">선물금 종류</th>
									<th scope="col">금액</th>
									<th scope="col">결제방법</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in gp_list">
									<td>{{item.paymentdate | date:'yyyy.MM.dd'}}</td>
									<td>{{item.namekr}}</td>
									<td>{{item.sponsoritem}}</td>
									<td class="price">{{item.sponsoramount | number:0}}원</td>
									<td>{{item.paymentname}}</td>
								</tr>
							
								<tr ng-if="gp_total == 0">
									<td colspan="5" class="no_content">등록된 선물금 내용이 없습니다.<br />어린이에게 선물을 한 번 보내보세요.</td>
								</tr>
							</tbody>
						</table>
					</div>

					<!-- page navigation -->
					<div class="tac relative mb20">
						<paging class="small" page="gp_page" page-size="gp_rowsPerPage" total="gp_total" show-prev-next="true" show-first-last="true" paging-action="getGiftPaymentList(page)"></paging>

						<a href="/my/sponsor/payment" class="btn_type3 posR">전체후원내역보기</a>
					</div>
					<!--// page navigation -->

				</div>
				<!--// 어린이 선물금 결제내역-->

				<div class="box_type4 tac">
					<p class="s_con3">기타 문의는 한국컴패션으로 연락 주시기 바랍니다. 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a></p>
				</div>

				
			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>


</asp:Content>
