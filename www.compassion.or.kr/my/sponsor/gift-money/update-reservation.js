﻿$(function () {

	$page.init();

});

var goBack = function () {
	location.href = "/my/sponsor/gift-money/";
}

var $page = {

	init: function () {

		$("#pay_amount").change(function () {

			if ($(this).val() == "custom") {
				$("#pay_amount_custom").show();
				$("#pay_amount_custom").focus();
			} else {
				$("#pay_amount_custom").val("");
				$("#pay_amount_custom").hide();
			}
		});

		$("#pay_amount").trigger("change");

		$("#btn_submit").click(function () {
			return $page.onSubmit();
		})

		if ($("#gift_type").val() == "BG") {
			$(".pn_BG").show();
		} else if ($("#gift_type").val() == "GG") {
			$(".pn_GG").show();
		} else if ($("#gift_type").val() == "FG") {
			$(".pn_FG").show();
		} else if ($("#gift_type").val() == "PG") {
		    $(".pn_PG").show();
		} else if ($("#gift_type").val() == "CF") {
		    $(".pn_CF").show();
		}

		if ($("#pay_amount_custom").val() != "")
			$(".pn_pay_amount_custom").show();

	},

	// 확인
	onSubmit: function () {

		if ($("#pay_amount").val() == "") {
			alert("금액을 선택해주세요.");
			return false;
		}

		if ($("#pay_amount").val() == "custom") {
			var obj = $("#pay_amount_custom");
			var val = obj.val();
			var min = $("#pay_amount option:eq(1)").val();
			var min_text = $("#pay_amount option:eq(1)").text();
			var max = $("#pay_amount option:eq(" + ($("#pay_amount option").size() - 2) + ")").val();
			var max_text = $("#pay_amount option:eq(" + ($("#pay_amount option").size() - 2) + ")").text();

			if (val == "") {
				alert("금액을 입력해주세요");
				obj.focus();
				return false;
			}

			if (isNaN(val) || parseInt(val) < min) {
				alert(min_text + " 이상 입력해주세요");
				obj.val("");
				obj.focus();
			}
			if (parseInt(val) > max && $("#gift_type").val() != "CF") {
				alert("최대 " + max_text + " 이하 입력해주세요");
				obj.val("");
				obj.focus();
				return false;
			}

			if (parseInt(val) % 1000 > 0) {
				alert("천원단위로 입력해 주세요");
				obj.val("");
				obj.focus();
				return false;
			}

		}

		return true;
	}

};
