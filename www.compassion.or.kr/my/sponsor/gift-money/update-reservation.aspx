﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update-reservation.aspx.cs" Inherits="my_gift_money_update_reservation" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/sponsor/gift-money/update-reservation.js"></script>
	<script type="text/javascript">
        $(document).ready(function () {
            if ($('#AdminCK').val() == "true") {
                $(".adminCK").css("display", "none");
            }
        });
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" id="AdminCK" value="<%=ViewState["AdminCK"]%>" />
	<input type="hidden" runat="server" id="gift_type" />
	<section class="sub_body" >

		<!-- 타이틀 -->
		<div class="page_tit adminCK">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980 money">

				<uc:menu runat="server"  />

				<!-- 선물금예약 -->
				<div class="box_type4 mb40">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit mb30">
						<p class="tit">선물금 예약 수정</p>
					</div>
					<!--// -->

					<!-- 어린이 -->
					<p class="s_tit5 mb20">선물금 종류 : <span class="fc_blue"><asp:Literal runat="server" ID="lb_type"></asp:Literal></span></p>
					<div class="sel_child mb40" runat="server" id="pn_child">

						<div class="tableWrap4">
							<table class="tbl_child reserve">
								<caption>선물금 예약 수정할 어린이 정보 테이블</caption>
								<colgroup>
									<col style="width:2%" />
									<col style="width:12%" />
									<col style="width:60%" />
									<col style="width:26%" />
								</colgroup>
								<tbody>
									<tr>
										<td></td>
										<td class="tac">
											<span class="pic" style="background:url('<asp:Literal runat="server" ID="img" />') no-repeat center top;background-size:cover;">어린이사진</span>
										</td>
										<td class="detail">
											<p class="s_tit5 mb5"><asp:Literal runat="server" ID="c_namekr" /></p>
											<span class="field">어린이ID</span><span class="data"><asp:Literal runat="server" ID="c_key" /></span><span class="field fd2">국가</span><span class="data"><asp:Literal runat="server" ID="c_country" /></span><br />
											<span class="field">생일</span><span class="data"><asp:Literal runat="server" ID="c_birth" /> (<asp:Literal runat="server" ID="c_age" />세)</span><span class="field fd2">성별</span><span class="data"><asp:Literal runat="server" ID="c_gender" /></span>
										</td>
										<td class="tar pr20">
											<span class="s_con6">결연일 : <asp:Literal runat="server" ID="lb_startdate"></asp:Literal></span><br />
											<!--<a href="#" class="btn_view">이 어린이 선물금 내역 보기</a>-->
										</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>
					<!--// 어린이 -->

					<!-- 입력테이블 -->
					<div>
						<div class="tableWrap1 mb30">
							<table class="tbl_type1 line1">
								<caption>결제방법 선택 테이블</caption>
								<colgroup>
									<col style="width:20%" />
									<col style="width:80%" />
								</colgroup>
								<tbody>
									<asp:PlaceHolder runat="server" ID="ph_month" Visible="false">
									<tr>
										<th scope="row">선물금 예약 월</th>
										

										<td>
											<span class="fs15 vam mr10">매년</span>
											<span class="sel_type2 mr20" style="width:158px;">
												<label for="ddlMonth" class="hidden">예약 월 선택</label>
	
												<asp:DropDownList ID="ddlMonth" CssClass="custom_sel" runat="server" Enabled="true" >
													<asp:ListItem Selected="True" Value="01">1월</asp:ListItem>     
													<asp:ListItem Value="02">2월</asp:ListItem>
													<asp:ListItem Value="03">3월</asp:ListItem>      
													<asp:ListItem Value="04">4월</asp:ListItem>
													<asp:ListItem Value="05">5월</asp:ListItem>
													<asp:ListItem Value="06">6월</asp:ListItem>
													<asp:ListItem Value="07">7월</asp:ListItem>
													<asp:ListItem Value="08">8월</asp:ListItem>
													<asp:ListItem Value="09">9월</asp:ListItem>
													<asp:ListItem Value="10">10월</asp:ListItem>
													<asp:ListItem Value="11">11월</asp:ListItem>
													<asp:ListItem Value="12">12월</asp:ListItem>
												</asp:DropDownList>
												
											</span>
											<p class="pt15 pn_BG" ><span class="s_con1">
												예약월 변경 시 중복 출금이 될 수 있습니다. 선물금 결제 내역을 확인하시고, 변경해주세요.
												</span>
													</p>
										</td>
									</tr>
									</asp:PlaceHolder>
									<tr>
										<th scope="row">금액</th>
										<td>
											<span class="sel_type2 mr20" style="width:200px;">
												<label for="pay_amount" class="hidden">금액 선택</label>
												<asp:DropDownList runat="server" ID="pay_amount" CssClass="custom_sel"></asp:DropDownList>

											</span>

											<label class="hidden pn_pay_amount_custom" style="display:none;">개월 수 입력</label>
											<input type="text" runat="server" id="pay_amount_custom" class="number_only input_type2 mr10 pay_amount_custom fc_money pn_pay_amount_custom" style="display:none;width:300px" placeholder="금액을 입력해 주세요" />
											<span class="fs15 vam pn_pay_amount_custom" style="display:none;">원</span>

											<p class="pt15 pn_BG" style="display:none;"><span class="s_con1">생일선물금은 최소 1만 5천원~ 최대 10만원까지를 추천합니다.</span></p>
                                            <p class="pt15 pn_GG" style="display:none;"><span class="s_con1">어린이에게 전하는 특별한 선물금은 최소 1만 5천원~ 최대 10만원까지 연에 1~2회를 추천합니다.</span></p>
                                            <p class="pt15 pn_FG" style="display:none;"><span class="s_con1">어린이 가족에게 전하는 마음의 선물금은 최소 3만 5천원~ 최대 100만원까지 권장합니다. (연에 100만원까지 가능합니다.)</span></p>
                                            <p class="pt15 pn_PG" style="display:none;"><span class="s_con1">어린이가 등록된 어린이센터에 전하는 모두를 위한 선물금은 최소 15만원~ 최대 200만원까지 권장합니다. (연에 200만원까지 가능합니다.)</span></p>
                                            <p class="pt15 pn_CF" style="display:none;"><span class="s_con1">크리스마스 선물금은 컴패션에 등록된 모든 어린이를 위한 선물금으로, 한 어린이당 2만원의 선물금을 추천합니다.</span></p>

										</td>
									</tr>
								</tbody>
							</table>

						</div>

						<div class="tac mb60 adminCK"><asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" cssclass="btn_type1">수정하기</asp:LinkButton></div>
					</div>
					<!--// 입력테이블 -->
					<!--
					<div class="box_type3">
						<ul>
							<li><span class="s_con1">선물금은 최소 15,000원 이상부터 보내실 수 있습니다.</span></li>
							<li><span class="s_con1">예약하신 선물금은 후원금 자동이체 계좌, 신용카드에서 출금됩니다.</span></li>
						</ul>
					</div>
						-->
				</div>
				<!--// 선물금예약 -->

			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>

</asp:Content>
