﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_gift_money_default : FrontBasePage {

	public override bool RequireSSL {
		get {
			return utils.chServerState(); //false;
        }
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}
    
    protected override void OnBeforePostBack() {

		UserInfo sess = new UserInfo();
		// 정기결제 수단이 예약가능한 결제수단인지 
		DataSet dsPaymentType = null;
		try {
			WWWService.Service _wwwService = new WWWService.Service();
			dsPaymentType = _wwwService.selectUsingPaymentAccount(sess.SponsorID, "");

			if(dsPaymentType.Tables[0].Rows.Count > 0) {

				var paymentName = dsPaymentType.Tables[0].Rows[0]["PaymentName"].ToString().Trim();
				
				if(paymentName == "해외카드" || paymentName == "미주") {
					btn_reservation.Visible = false;
					return;
				}

				btn_reservation.Visible = true;
			}
			
		} catch(Exception ex) {
			ErrorLog.Write(this.Context, 0, ex.Message);
			btn_reservation.Visible = false;
		}


	}


}