﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pay.aspx.cs" Inherits="my_gift_money_pay" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/sponsor/pay/temporary/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/sponsor/pay/temporary/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/sponsor/pay/temporary/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/my/sponsor/gift-money/pay.js?v=1"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" id="hdCompleted" runat="server" value="N" />
	<input type="hidden" id="hdChildren" runat="server" />
	<input type="hidden" id="hdCountries" runat="server" />
	<input type="hidden" id="hdChildMasterIds" runat="server" value="" />

	<input type="hidden" runat="server" id="hash" value="" />
	<input type="hidden" runat="server" id="gubun" value="" />
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />
	<input type="hidden" runat="server" id="baby_gender" value="" />

	<input type="hidden" id="gender" runat="server"  />
	<input type="hidden" id="hd_auth_domain" runat="server"  />
	<input type="hidden" id="hd_age" runat="server"  />

	<input type="hidden" runat="server" id="jumin" value="" />		
	<input type="hidden" runat="server" id="ci" value="" />		<!-- 본인인증 CI -->
	<input type="hidden" runat="server" id="di" value="" />		<!-- 본인인증 DI -->
	<input type="hidden" runat="server" id="cert_gb" value="" />		<!-- 본인인증 수단 -->
	
	<input type="hidden" runat="server" id="user_class" value="20세이상" />
	<input type="hidden" runat="server" id="hdState" value="" />
	<input type="hidden" runat="server" id="hdLocation" value="" />
	<input type="hidden" runat="server" id="hdManageType" value="" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />		<!-- 회원 기본주소와 동기화 하는 기능이 있는경우 hfAddressType 가 필요함 -->
	<input type="hidden" runat="server" id="hdCI" value="" />	<!-- 기존 등록된 본인인증 CI -->
	
	
	<input type="hidden" runat="server" id="exist_account" value="N" />		<!-- 정기결제 계좌가 있는지 여부 -->
	<input type="hidden" runat="server" id="gift_type" value="" />		<!-- 선택된 선물금 종류 -->
	<input type="hidden" runat="server" id="amount" value="0" />		<!-- 선택된 금액 -->
	<input type="hidden" runat="server" id="child_count" value="0" />		
	<input type="hidden" runat="server" id="ready_count" value="0" />		
	<input type="text" runat="server" id="user_name" style="display:none" />		<!-- 결제시 사용 -->

		<!-- 주소지 , 국내,국외로 분기되는 부분을 위해서 hidden 으로 유지한다. -->
	<asp:PlaceHolder runat="server" Visible="false">
		<input type="checkbox" runat="server" id="addr_overseas" />해외에 거주하고 있어요
		연말정산영수증 신청 <input type="checkbox" id="p_receipt_pub_ok" name="p_receipt_pub" runat="server" checked="true" /> 

	</asp:PlaceHolder>

	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		<!-- 타이틀 -->
		<div class="page_tit" ng-if="AdminCK == false">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980 money">

				<uc:menu runat="server"  />

				<!-- 선물금보내기 -->
				<div class="box_type4 mb40">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit"  id="l">
						<p class="tit">선물금 보내기</p>
					</div>
					<!--// -->

					<!-- 서브 문구 -->
					<div class="sub_desc">
						<p class="txt2">
							<span class="txt1">
								생일선물금의 경우 어린이 생일 <em class="fc_blue">1개월 전</em>에 보내주시기를 권해드립니다.<br /> 
								권장 시기 이후에 납부하시더라도 ‘생일 선물금’으로 어린이에게 정확히 전해드립니다. <br />
								크리스마스 선물금의 경우 <em class="fc_blue">9월</em>에 예약해 주시면 어린이가 크리스마스에 맞추어 선물을 전달받아 행복한 크리스마스를 보낼 수 있습니다.

							</span>

						</p>
					</div>
					<!--// -->
					
					<asp:LinkButton runat="server" ID="btn_select_type" onClick="gift_type_Click"></asp:LinkButton>
					<!-- 선물금 종류 선택 -->
					<div class="sel_money mb40">
						<p class="s_tit3 mb15">선물금 종류 선택</p>
						<ul class="clear2 gift_type">
							<li class="sel1" data-type="BG" data-text="생일 선물금">
								<p class="s_tit5">생일 선물금</p>
							</li>
							<li class="sel2" data-type="GG" data-text="어린이 선물금">
								<p class="s_tit5">어린이 선물금</p>
							</li>
							<li class="sel3" data-type="FG" data-text="가족 선물금">
								<p class="s_tit5">가족 선물금</p>
							</li>
							<li class="sel4" data-type="PG" data-text="어린이센터 선물금">
								<p class="s_tit5">어린이센터 선물금</p>
							</li>
							<li class="sel5" data-type="CF" data-text="크리스마스 선물금">
								<p class="s_tit5">크리스마스 선물금</p>
							</li>
						</ul>
					</div>
					<!--//-->
					<div style="display:none">
						<asp:Literal ID="lblTypeStr" runat="server" Text=""></asp:Literal>
						<asp:Literal ID="lblTypeStr2" runat="server" Text=""></asp:Literal>
						<asp:Literal ID="lblFirstNo" runat="server" Text="어린이에게 보내시는 선물금은 첫 후원금이 납부된 이후에 결제하실 수 있습니다."></asp:Literal>
					</div>

					<!-- 어린이 선택 -->
					<div runat="server" id="pn_children">
					<p class="s_tit3 mb15">어린이 선택</p>
					<div class="sel_child mb60">

						<div class="tableWrap1 mb30">
							<table class="tbl_child">
								<caption>후원중인 어린이 선택 테이블</caption>
								<colgroup>
									<col style="width:7%" />
									<col style="width:12%" />
									<col style="width:52%" />
									<col style="width:26%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" colspan="4">
											<span class="checkbox_ui all">
												<input type="checkbox" class="css_checkbox"  id="chkAll" ng-click="chkAll()"/>
												<label for="chkAll" class="css_label font2">후원중인 모든 어린이 선택</label>
											</span>

											<div class="selBox" ng-show="total > 0">
												<span class="sel_type1 fl mr10" style="width:150px">
													<label for="country" class="hidden">국가 선택</label>
													<select class="custom_sel" data-refresh="true" id="country" ng-model="country" ng-change="changeCountry()" 
													data-ng-options="item.country as item.country for item in countries">
														<option value="">국가</option>
													</select>
												</span>
												<span class="sel_type1 fl mr5" style="width:190px">
													<label for="child" class="hidden">어린이 선택</label>
													<select class="custom_sel" data-refresh="true" id="child" ng-model="child" ng-change="changeChild()" 
													data-ng-options="item.childKey as item.nameKr for item in children">
														<option value="">어린이이름</option>
													</select>
												</span>
												<!--<a href="#" class="btn_type9">검색</a>-->
											</div>
										
										</th>
									</tr>
								</thead>
								<tbody>
								
									<style>
										.checkbox_ui input[type=checkbox].css_checkbox_gift {position:absolute; z-index:-1000; left:-1000px; overflow: hidden; clip: rect(0 0 0 0); height:1px; width:1px; margin:-1px; padding:0; border:0}
										.checkbox_ui input[type=checkbox].css_checkbox_gift + label.css_label {width:auto;height:auto;min-height:20px;padding:0 0 0 25px;cursor:pointer;background-position:0 1px;}
										.checkbox_ui input[type=checkbox].css_checkbox_gift:focus + label.css_label {outline-style:inherit !important;}
										.checkbox_ui input[type=checkbox].css_checkbox_gift.selected + label.css_label {background-position:0 -19px;}
										.checkbox_ui.default input[type=checkbox].css_checkbox_gift + label.css_label {width:17px;height:17px;min-height:18px;text-indent:-9999px;margin:0;padding:0;}
									</style>
									<tr ng-repeat="item in list">
										<td class="tac">
											<span class="checkbox_ui default">
												<input type="checkbox" id="chk{{$index}}" ng-click="chk(item)"  ng-class="item.checked ? 'selected' : ''" ng-checked="{{item.checked}}" class="chk css_checkbox_gift" data-exist-year="{{item.exist_year}}" data-commit-count="{{item.commit_count}}" data-childMasterId="{{item.ChildMasterID}}"  data-name="{{item.NameKr}}" />
												<label for="chk{{$index}}" class="css_label">어린이 선택</label>
											</span>
										</td>
										<td class="tac">
											<span class="pic" style="background:url('{{item.pic}}') no-repeat center;background-size:cover;background-position-y:0">어린이사진</span>
										</td>
										<td class="detail">
											<p class="s_tit5 mb5">{{item.NameKr}}<span class="fs13 fc_blue" ng-if="item.exist_year == 'Y'">&nbsp;(선물금 예약 중)</span></p>
											<span class="field">어린이ID</span><span class="data">{{item.ChildKey}}</span><span class="field fd2">국가</span><span class="data">{{item.CountryCode}}</span><br />
											<span class="field">생일</span><span class="data">{{item.BirthDate}} ({{item.age}}세)</span><span class="field fd2">성별</span><span class="data">{{item.Gender}}</span>
										</td>
										<td class="tar pr20">
											<span class="s_con6">결연일 : {{item.commit_date | date:'yyyy-MM-dd'}}</span><br />
											<!--<a href="#" class="btn_view">이 어린이 선물금 내역 보기</a>-->
										</td>
									</tr>
									

								</tbody>
							</table>
						</div>

						<div class="tac"><button class="btn_s_type3" ng-show="params.page * params.rowsPerPage < total" ng-click="more($event);"><span>더 많은 어린이들 보기<span class="btn_arr1"></span></span></button></div>

					</div>
					</div>
					<!--// 어린이 선택 -->

					<!-- 선물금 결제 -->
					<div>
						<div class="tableWrap1 mb30">
							<table class="tbl_type1 line1">
								<caption>결제방법 선택 테이블</caption>
								<colgroup>
									<col style="width:20%" />
									<col style="width:80%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">선물금 결제</th>
										<td>
											<span class="radio_ui">
												<input type="radio" runat="server" id="rdoOnce" class="raGubun css_radio"  name ="raGubun"/>
												<label for="rdoOnce" class="css_label mr30">바로결제</label>
												<span runat="server" id="pn_regular">
												<input type="radio" runat="server" id="rdoYear" class="raGubun css_radio" checked="true"  name ="raGubun"/>
												<label for="rdoYear" class="css_label mr30">매년결제예약</label>
												</span>
											</span>

											<span class="sel_type2" style="width:100px;" runat="server" id="month_container">
												<label for="ddlMonth" class="hidden">개월 수 선택</label>
												<asp:DropDownList ID="ddlMonth" runat="server" Enabled="true" Visible="false" CssClass="custom_sel">
													<asp:ListItem Selected="True" Value="01">1월</asp:ListItem>     
													<asp:ListItem Value="02">2월</asp:ListItem>
													<asp:ListItem Value="03">3월</asp:ListItem>      
													<asp:ListItem Value="04">4월</asp:ListItem>
													<asp:ListItem Value="05">5월</asp:ListItem>
													<asp:ListItem Value="06">6월</asp:ListItem>
													<asp:ListItem Value="07">7월</asp:ListItem>
													<asp:ListItem Value="08">8월</asp:ListItem>
													<asp:ListItem Value="09">9월</asp:ListItem>
													<asp:ListItem Value="10">10월</asp:ListItem>
													<asp:ListItem Value="11">11월</asp:ListItem>
													<asp:ListItem Value="12">12월</asp:ListItem>
												</asp:DropDownList>

											</span>
											<p class="pt15">
												<span class="s_con1 pn_payYear" style="display:none">예약하신 선물금은 후원금 자동이체 계좌, 신용카드에서 출금됩니다. </span>
                                                <span class="s_con1 pn_payYear" style="display:none">생일 선물금은 어린이 생일 1개월 전, 크리스마스 선물금은 9월로 자동예약됩니다.</span>
											</p>
										</td>
									</tr>
									<tr>
										<th scope="row">금액</th>
										<td>
											<!-- 선택한 선물금 종류에 따라 금액과 하단 문구가 바뀜 -->
											<span class="sel_type2 mr20" style="width:200px;">
												<label for="pay_amount" class="hidden">금액 선택</label>
												<asp:DropDownList runat="server" ID="pay_amount" CssClass="custom_sel"><asp:ListItem Value="" Text="선택하세요"></asp:ListItem></asp:DropDownList>
											</span>
											
											<label class="hidden pn_pay_amount_custom" style="display:none;">개월 수 입력</label>
											<input type="text" runat="server" id="pay_amount_custom" maxlength="9" class="input_type2 mr10 pay_amount_custom fc_money pn_pay_amount_custom number_only"  value="" placeholder="직접입력" style="width:300px;display:none;" /><span class="fs15 vam pn_pay_amount_custom">원</span>
											<p class="pt15 pn_BG" style="display:none;"><span class="s_con1">생일선물금은 최소 1만 5천원~ 최대 10만원까지를 추천합니다.</span></p>
                                            <p class="pt15 pn_GG" style="display:none;"><span class="s_con1">어린이에게 전하는 특별한 선물금은 최소 1만 5천원~ 최대 10만원까지 연에 1~2회를 추천합니다.</span></p>
                                            <p class="pt15 pn_FG" style="display:none;"><span class="s_con1">어린이 가족에게 전하는 마음의 선물금은 최소 3만 5천원~ 최대 100만원까지 권장합니다. (연에 100만원까지 가능합니다.)</span></p>
                                            <p class="pt15 pn_PG" style="display:none;"><span class="s_con1">어린이가 등록된 어린이센터에 전하는 모두를 위한 선물금은 최소 15만원~ 최대 200만원까지 권장합니다. (연에 200만원까지 가능합니다.)</span></p>
                                            <p class="pt15 pn_CF" style="display:none;"><span class="s_con1">크리스마스 선물금은 컴패션에 등록된 모든 어린이를 위한 선물금으로, 한 어린이당 2만원의 선물금을 추천합니다.</span></p>
											</div>
										</td>
									</tr>
									<tr id="pn_payment" runat="server" style="display:none">
										<th scope="row">결제방법</th>
										<td>
										<span class="radio_ui mb15">
											<input type="radio" id="payment_method_card" runat="server" name="payment_method"  class="css_radio" checked />
											<label for="payment_method_card" class="css_label" style="width:170px">신용카드 결제</label>

											<input type="radio" id="payment_method_cms" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_cms" class="css_label" style="width:170px">실시간 계좌이체</label>

											<input type="radio" id="payment_method_oversea" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_oversea" class="css_label" style="width:170px">해외발급 카드</label>
										</span>
										<span class="radio_ui">
											<input type="radio" id="payment_method_kakao" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_kakao" class="css_label relative" style="width:170px"><img src="/common/img/icon/kakaopay.jpg" class="kakao" alt="kakao pay" onclick="$('#payment_method_kakao').trigger('click')" />kakaopay</label>

											<input type="radio" id="payment_method_payco" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_payco" class="css_label relative" style="width:170px"><img src="/common/img/icon/payco.jpg" class="payco" alt="payco" onclick="$('#payment_method_payco').trigger('click')" />payco</label>

											<input type="radio" id="payment_method_phone" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_phone" class="css_label" style="width:170px">휴대폰 결제</label>
										</span>
									</td>
									</tr>
								</tbody>
							</table>

						</div>

						<div class="tac" ng-if="AdminCK==false"><asp:LinkButton runat="server" ID="btn_submit" ng-click="onSubmit($event);" OnClick="btn_submit_Click" cssClass="btn_type1">결제하기</asp:LinkButton></div>
						<!--
						<div class="tac"><a href="#" class="btn_type1">예약하기</a><a href="#" class="btn_type1">결제하기</a></div>
							-->
					</div>
					<!--// 선물금 결제 -->

				</div>
				<!--// 선물금보내기 -->

			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>




</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="payco_form" />
	<uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kakaopay_form" />
</asp:Content>