﻿$(function () {

	$page.init();

});

var goBack = function () {
	location.href = "/my/sponsor/gift-money/";
}

var $page = {

	init: function () {

		//if ($("#exist_account").val() == "N" && $("#child_count").val() == "0") {
		if ($("#child_count").val() == "0") {
			/*
			$(".sel_money li[data-type=BG]").hide();
			$(".sel_money li[data-type=GG]").hide();
			$(".sel_money li[data-type=FG]").hide();
			$(".sel_money li[data-type=PG]").hide();
			*/
		}

		$(".sel_money li").click(function () {
			//if ($("#exist_account").val() == "N" && $("#child_count").val() == "0") {
			if ($("#child_count").val() == "0") {
				var type = $(this).data("type");
				if (type != "CF") {
					alert("후원 어린이가 없습니다.");
				}
				return false;
			}
			$("#gift_type").val($(this).data("type"));
			location.href = $("#btn_select_type").attr("href");
		});

		if ($("#gift_type").val() != "") {
			$(".gift_type li[data-type='" + $("#gift_type").val() + "']").addClass("on");
		}

		if ($("#hash").val() != "") {
			location.href = "#" + $("#hash").val();
			//	scrollTo($("#l"));
		}

		/*
		$("#chkAll").click(function () {
			var checked = $(this).prop("checked");
			$(".chk").prop("checked", checked);
		});

		$(".chk").click(function () {
			var checked = $(this).prop("checked");
			var exist_year = $(this).data("exist-year");		// 예약여부 , Y or N
			var commit_count = $(this).data("commit-count");		// 결제여부 , tcommitmentMaster 에 등록된 횟수

			if ($("#rdoOnce").prop("checked")) {
				if (commit_count >= 2) {
					alert("예약을 포함하여 2건만 결제가능합니다.");
					return false;
				}
			} else if ($("#rdoYear").prop("checked")) {
				if (exist_year == "Y") {
					alert("이미 예약된 어린이 입니다.");
					return false;
				}
			}

			if (checked) {
				$(this).parent().parent().parent().css({ "background": "#fbfbfb" });
			} else {
				$(this).parent().parent().parent().css({ "background": "#fff" });
			}

		})
		*/

		$(".raGubun").change(function () {
			var id = $(".raGubun:checked").attr("id");
			if (id == "rdoOnce") {
				$(".pn_payYear").hide();
				$("#pn_payment").show();
				$("#btn_submit").html("결제하기");

				$("#month_container").hide();

			} else {
				$(".pn_payYear").show();
				$("#pn_payment").hide();
				$("#btn_submit").html("예약하기");

				$("#month_container").show();
			}

			if ($("#hdCompleted").val() == "N") {
				$(".chk").each(function () {

					var checked = $(this).prop("checked");
					var exist_year = $(this).data("exist-year");		// 예약여부 , Y or N
					var commit_count = $(this).data("commit-count");		// 결제여부 , tcommitmentMaster 에 등록된 횟수
					if (checked && $("#gift_type").val() != "CF") {
						if ($("#rdoOnce").prop("checked")) {
							if (commit_count >= 2) {
								alert("예약을 포함하여 2건만 결제가능합니다.");
								$(this).prop("checked", false);
								return false;
							}
						} else if ($("#rdoYear").prop("checked")) {
							if (exist_year == "Y") {
								alert("이미 예약된 어린이 입니다.");
								$(this).prop("checked", false);
								return false;
							}
						}

					}
				});
			}

		})

		$(".raGubun").trigger("change");


		if ($("#rdoOnce").prop("checked")) {
			$("#pn_payment").show();
		}

		$("#pay_amount").change(function () {

			if ($(this).val() == "custom") {
				$(".pn_pay_amount_custom").show();
				$("#pay_amount_custom").focus();
			} else {
				$(".pn_pay_amount_custom").hide();
				$("#pay_amount_custom").val("");
				
			}
		});

		if ($("#gift_type").val() == "BG") {
			$(".pn_BG").show();
		} else if ($("#gift_type").val() == "GG") {
		    $(".pn_GG").show();
		} else if ($("#gift_type").val() == "FG") {
		    $(".pn_FG").show();
		} else if ($("#gift_type").val() == "PG") {
		    $(".pn_PG").show();
		} else if ($("#gift_type").val() == "CF") {
		    $(".pn_CF").show();
		}

		$("#pay_amount").trigger("change");

		
	}

};

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		$scope.total = -1;
		
		$scope.data = [];
		$scope.list = null;
		$scope.countries = null;
		$scope.children = null;
		$scope.country = "";
		$scope.child = "";

		$scope.params = {
			page: 1,
			rowsPerPage: 4
		};

        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function () {

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();

		// list
		$scope.getList = function (params) {
			
				
			var data = $.extend($scope.data, $.parseJSON($("#hdChildren").val()));

			// 어린이가 지정된 경우 , 선물금 가능한 어린이만 선택되기 때문에, 선물금이 불가한 경우는 전체 목록 노출
			var childMasterId = getParameterByName("childMasterId");

			if (childMasterId) {
				var data2 = $.grep(data, function (r) {
					return r.ChildMasterID == childMasterId;
				});

				if (data2.length > 0) {
					data2[0].checked = true;
					data = data2;
				}
			}

			$scope.data = data;

			$scope.total = $scope.data.length > 0 ? $scope.data[0].total : 0;
			console.log($scope.data);
			$.each($scope.data, function () {
				/*	
				var yyyy = this.CommitmentID.substr(0, 4);
				var mm = this.CommitmentID.substr(4, 2);
				var dd = this.CommitmentID.substr(6, 2);
				this.commit_date = new Date(mm + "/" + dd + "/" + yyyy);
					*/

				this.commit_date = new Date(this.startdate.replace(/-/g, '/').substring(0, 10));
			});

			var countries = $.parseJSON($("#hdCountries").val());
			$scope.countries = countries;
			console.log("countries", $scope.countries);

			setTimeout(function () {
				$("#country").selectbox({});
			},300)
			
			$scope.showList(true);


		}

		$scope.showList = function (reload) {
			var begin = ($scope.params.page - 1) * $scope.params.rowsPerPage;
			var end = ($scope.params.page) * $scope.params.rowsPerPage;
			var list = $scope.data.slice(begin, end);
			if (reload) {
				$scope.list = [];
				$scope.total = $scope.data.length;
			}

			$scope.list = $.merge($scope.list, list);

		};

		$scope.more = function ($event) {
			$event.preventDefault();
			$scope.params.page++;
			$scope.showList(false);
		};

		$scope.search = function () {
			
			if (($scope.country == null || $scope.country == "") && ($scope.child == null || $scope.child == "")) {
				$scope.params.page = 1;
				$scope.showList(true);
				return;
			}

			var children = $.grep($scope.data, function (r) {
				var country = true , child = true;
				if ($scope.country && $scope.country != "") {
					country = r.CountryCode == $scope.country
				};

				if ($scope.child && $scope.child != "") {
					child = r.ChildKey == $scope.child
				};
				
				return country && child;
			});
			
			$scope.list = children;
			$scope.total = children.length;

		} , 

		$scope.changeCountry = function () {
			
			var children = $.grep($scope.countries, function (r) {
				return r.country == $scope.country;
			});

			$scope.children = children.length < 1 ? "" : children[0].children;

			setTimeout(function () {
				$("#child").selectbox({});
			}, 300)

			$scope.search();
		};

		$scope.changeChild = function () {
			
			$scope.search();
		};

		$scope.chkAll = function () {
			var checked = $("#chkAll").prop("checked");
			$.each($scope.data, function () {
				this.checked = checked;
			})
			
		}

		$scope.chk = function (item) {

			var checked = !item.checked;
			if (checked) {
				var exist_year = item.exist_year;
				var commit_count = item.commit_count;

				if ($("#rdoOnce").prop("checked")) {
					if (commit_count >= 2) {
						alert("예약을 포함하여 2건만 결제가능합니다.");
						return false;
					}
				} else if ($("#rdoYear").prop("checked")) {
					if (exist_year == "Y") {
						alert("이미 예약된 어린이 입니다.");
						return false;
					}
				}
			}

			item.checked = !item.checked;

		}

		$scope.onSubmit = function ($event) {

			var list = $.grep($scope.data, function (r) {
				return r.checked;
			});

			if ($("#gift_type").val() == "") {
				alert("선물금 종류를 선택해 주세요");
				$event.preventDefault();
				return;
			}
			if (list.length < 1 && $("#gift_type").val() != "CF") {
				alert("선물금을 보내실 어린이를 선택해주세요");
				$event.preventDefault();
				return;
			}

			if ($("#pay_amount").val() == "") {
				alert("금액을 선택해주세요.");
				$event.preventDefault();
				return;
			}

			if ($("#pay_amount").val() == "custom") {
				var obj = $("#pay_amount_custom");
				var val = obj.val();
				var min = $("#pay_amount option:eq(1)").val();
				var min_text = $("#pay_amount option:eq(1)").text();
				var max = $("#pay_amount option:eq(" + ($("#pay_amount option").size() - 2) + ")").val();
				var max_text = $("#pay_amount option:eq(" + ($("#pay_amount option").size() - 2) + ")").text();
				var gift_name = $("ul.gift_type li.on").data("text");

				if (val == "") {
					alert("금액을 입력해주세요");
					obj.focus();
					$event.preventDefault();
					return;
				}

				if (isNaN(val) || parseInt(val) < min) {
					alert(gift_name + "은 최소 " + min_text + " 이상 입력해주세요");
					obj.val("");
					obj.focus();
					$event.preventDefault();
					return;
				}

				if (parseInt(val) > max && $("#gift_type").val() != "CF") {
					alert(gift_name + "은 최대 " + max_text + " 이하 입력해주세요");
					obj.val("");
					obj.focus();
					$event.preventDefault();
					return;
				}

				if (parseInt(val) % 1000 > 0) {
					alert("천원단위로 입력해 주세요");
					obj.val("");
					obj.focus();
					$event.preventDefault();
					return;
				}

			}

			// 정기 조건 
			var childMasterIds = "";
			if ($("#rdoYear").prop("checked")) {

				var targets = "";
				$.each(list, function () {
					var reservation = this.exist_year;
					if (reservation == "Y") {
						targets += "," + this.NameKr;
					} else {
						childMasterIds += "," + this.ChildMasterID;

					}

				})

				

				if (targets != "") {
					alert(targets.substring(1) + " 어린이는 이미 선물금이 예약되어 있습니다.");
					$event.preventDefault();
					return;
				}
			}

			
			// 일시 조건
			if ($("#rdoOnce").prop("checked")) {

				var targets = "";
				$.each(list, function () {

					var cnt = this.commit_count;
					if (cnt && cnt >= 2) {
						targets += "," + this.NameKr;
					} else {
						childMasterIds += "," + this.ChildMasterID;
					}
				})

				if (targets != "") {
					alert(targets.substring(1) + " 어린이는 선물금 결제횟수를 초과했습니다.(예약포함)");
					$event.preventDefault();
					return;
				}
			}

			if ($("#gift_type").val() == "CF") {
				$("#hdChildMasterIds").val("0000000000");
			} else {
				$("#hdChildMasterIds").val(childMasterIds.substring(1));
			}

			
			
		}

		

		$scope.getList();

	});

})();
