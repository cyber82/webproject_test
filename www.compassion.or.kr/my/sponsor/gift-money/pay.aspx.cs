﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class my_gift_money_pay : SponsorPayBasePage
{

    public override bool RequireSSL
    {
        get
        {
            return utils.chServerState(); //true;
        }
    }

    public override bool NoCache
    {
        get
        {
            return true;
        }
    }

    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    protected override void OnBeforePostBack()
    {

        var payInfo = new PayItemSession.Entity() { type = PayItemSession.Entity.enumType.GIFT_MONEY, campaignId = "", frequency = "일시", group = "GIFT", codeId = "", codeName = "", amount = 0 };
        PayItemSession.SetCookie(this.Context, payInfo);

        // 결제정보를 디비에 저장하고 이후 결제프로세스는 디비의 값을 참조한다.
        var data = new PayItemSession.Store().Create(payInfo);
        if (data == null)
        {
            base.AlertWithJavascript("후원정보 임시저장에 실패했습니다.", "goBack()");
            return;
        }
        this.ViewState["payItem"] = data.ToJson();
        UserInfo sess = new UserInfo();

        var payment_type = Request.QueryString["t"].ValueIfNull("temporary");
        rdoOnce.Checked = rdoYear.Checked = false;
        if (payment_type == "temporary")
        {
            rdoOnce.Checked = true;
        }
        else
        {
            rdoYear.Checked = true;
        }

        var giftType = Request.QueryString["giftType"].ValueIfNull("BG");

        base.OnBeforePostBackTemporary(payInfo);

        if (!this.ResoluteFirstNo(sess.SponsorID))
        {
            return;
        }

        if (!this.GetChildren())
        {
            base.AlertWithJavascript("후원어린이 목록을 가져오지 못했습니다.", "goBack()");
            return;
        }


        this.CheckPaymentAccount();

        if (giftType == "CF")
        {
            //pn_regular.Style["display"] = "none";
            gift_type.Value = "CF";
            gift_type_Click(null, null);
            hash.Value = "";
        }
        else
        {
            if (exist_account.Value == "Y")
            {        // 정기결제수단이 있는경우

                if (child_count.Value == "0")
                {      // 후원어린이가 없는경우 
                    gift_type.Value = "CF";
                    gift_type_Click(null, null);
                    hash.Value = "";
                }


            }
            else
            {

                if (payment_type == "regular")
                {
                    base.AlertWithJavascript("정기결제 수단이 등록되어 있지 않습니다.", "goBack()");
                    return;
                }

                pn_regular.Style["display"] = "none";
                gift_type.Value = "CF";
                gift_type_Click(null, null);
                hash.Value = "";
            }
        }



    }

    // 첫 후원금 납부가 안된 수
    bool ResoluteFirstNo(string sSponsorID)
    {
        string result = string.Empty;

        try
        {
            result = base._wwwService.getResoluteFirstNo(sSponsorID);
            ready_count.Value = result;
            return true;
        }
        catch (Exception ex)
        {
            ErrorLog.Write(this.Context, 0, ex.Message);
            base.AlertWithJavascript("회원님의 결연목록을 찾는 중 오류가 발생했습니다.", "goBack()");
            return false;
        }

    }

    // 정기결제 정보가 있는지 여부확인
    bool CheckPaymentAccount()
    {

        UserInfo sess = new UserInfo();

        exist_account.Value = "N";
        DataSet dsPaymentType = null;
        try
        {
            WWWService.Service _wwwService = new WWWService.Service();
            dsPaymentType = _wwwService.selectUsingPaymentAccount(sess.SponsorID, "");

            if (dsPaymentType.Tables[0].Rows.Count > 0)
            {

                var paymentName = dsPaymentType.Tables[0].Rows[0]["PaymentName"].ToString().Trim();

                if (paymentName == "미주" || paymentName == "해외카드")
                {
                    return true;
                }

                exist_account.Value = "Y";
            }

            return true;
        }
        catch (Exception ex)
        {
            ErrorLog.Write(this.Context, 0, ex.Message);
            base.AlertWithJavascript("납부방법을 가져오는 중 오류가 발생했습니다.", "goBack()");
            return false;
        }


    }

    // 선물금 예약내역
    private DataTable GetFundReservationList(string sSponsorID, string sItemType)
    {
        DataSet ds = null;

        try
        {
            WWWService.Service _wwwService = new WWWService.Service();

            ds = _wwwService.listupFundingReservationList(sSponsorID, sItemType, "FundT");

            if (ds != null)
            {
                return ds.Tables["FundT"];
            }
        }
        catch (Exception ex)
        {
            ErrorLog.Write(this.Context, 0, ex.Message);
            base.AlertWithJavascript("선물금 예약목록을 가져오지 못했습니다.", "goBack()");
            return null;
        }

        return null;
    }

    // 선물금 결제내역
    private DataTable GetCommitmentList(string sSponsorID, string sItemType)
    {
        DataSet ds = null;

        try
        {
            // 예약포함 선택된 선물금 타입의 결연수
            Object[] objParam = new object[] { "itemType", "sponsorId" };
            Object[] objValue = new object[] { sItemType, sSponsorID };
            Object[] objSql = new object[] { "sp_web_present_payment_count_list_f" };
            ds = base._www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);

            if (ds != null)
            {
                return ds.Tables[0];
            }
        }
        catch (Exception ex)
        {
            ErrorLog.Write(this.Context, 0, ex.Message);
            base.AlertWithJavascript("선물금 결제목록을 가져오지 못했습니다.", "goBack()");
            return null;
        }

        return null;
    }

    // 선물금 예약내역이 있는지 여부가 포함됨
    private bool GetChildren()
    {

        UserInfo sess = new UserInfo();

        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            base.AlertWithJavascript("웹회원은 선물금을 보내실 수 없습니다.", "goBack();");
            return false;
        }

        DataTable children = null;
        DataTable reservations = null;
        DataTable commitments = null;
        if (gift_type.Value != "")
        {

            // 예약내역
            reservations = this.GetFundReservationList(sess.SponsorID, gift_type.Value);
            if (reservations == null)
                return false;

            commitments = this.GetCommitmentList(sess.SponsorID, gift_type.Value);
            if (commitments == null)
                return false;
        }

        if (this.ViewState["children"] == null)
        {
            try
            {

                var actionResult = new ChildAction().GetGitfableList();
                if (!actionResult.success)
                {
                    base.AlertWithJavascript("후원어린이 목록을 가져오지 못했습니다.", "goBack();");
                    return false;
                }

                var data = (Dictionary<string, object>)actionResult.data;
                children = (DataTable)data["list"];

                children.Columns.Add("exist_year");
                children.Columns.Add("commit_count");

                child_count.Value = children.Rows.Count.ToString();

                this.ViewState["children"] = children;
                this.ViewState["countries"] = data["children"].ToJson();

            }
            catch (Exception ex)
            {
                ErrorLog.Write(this.Context, 0, ex.Message);
                base.AlertWithJavascript("후원어린이 목록을 가져오지 못했습니다.", "goBack();");
                return false;
            }
        }

        children = (DataTable)this.ViewState["children"];

        foreach (DataRow dr in children.Rows)
        {
            // 예약내역이 있는 경우 체크
            if (reservations != null)
            {
                reservations.DefaultView.RowFilter = string.Format("ChildMasterID='{0}'", dr["ChildMasterId"].ToString());
                dr["exist_year"] = (reservations.DefaultView.Count > 0) ? "Y" : "N";
            }

            // 결연내역이 있는 경우 체크
            if (commitments != null)
            {
                commitments.DefaultView.RowFilter = string.Format("ChildMasterID='{0}'", dr["ChildMasterId"].ToString());
                dr["commit_count"] = (commitments.DefaultView.Count > 0) ? commitments.DefaultView[0]["cnt"] : "0";
            }
        }


        hdChildren.Value = children.ToJson();
        hdCountries.Value = this.ViewState["countries"].ToString();
        /*
		repeater_children.DataSource = children;
		repeater_children.DataBind();
		*/



        return true;



    }

    // 결제
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        kakaopay_form.Hide();
        kcp_form.Hide();
        payco_form.Hide();


        if (base.IsRefresh)
            return;

        amount.Value = pay_amount.SelectedValue == "custom" ? pay_amount_custom.Value : pay_amount.SelectedValue;

        var giftType = gift_type.Value;
        var sponsorAmount = Convert.ToInt32(amount.Value);

        if (giftType == "")
        {
            base.AlertWithJavascript("선물금 종류를 선택해주세요");
            return;
        }

        if (giftType == "BG" && (sponsorAmount < 15000 || sponsorAmount > 100000))
        {
            base.AlertWithJavascript("생일선물금은 15.000~100.000원까지 가능하십니다.");
            return;
        }
        else if (giftType == "GG" && (sponsorAmount < 15000 || sponsorAmount > 100000))
        {
            base.AlertWithJavascript("어린이선물금은 15.000~100.000원까지 가능하십니다.");
            return;
        }
        else if (giftType == "FG" && (sponsorAmount < 35000 || sponsorAmount > 1000000))
        {
            base.AlertWithJavascript("가족선물금은 35.000~1.000.000원까지 가능하십니다.");
            return;
        }
        else if (giftType == "PG" && (sponsorAmount < 150000 || sponsorAmount > 2000000))
        {
            base.AlertWithJavascript("어린이센터선물금은 150.000~2.000.000원까지 가능하십니다.");
            return;
        }
        else if (giftType == "CF" && (sponsorAmount < 20000))
        {
            base.AlertWithJavascript("크리스마스선물금은 20.000원 이상 가능하십니다.");
            return;
        }

        if (rdoYear.Checked)
        {

            if (exist_account.Value == "N")
            {
                base.AlertWithJavascript("정기결제정보가 없습니다.");
                return;
            }

            PayRegular();

        }
        else
        {

            PayTemporary();

        }
    }

    void PayTemporary()
    {


        var childMasterIds = hdChildMasterIds.Value;
        var total = Convert.ToInt32(amount.Value);
        total = total * childMasterIds.Split(',').Length;

        var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
        var payInfo = payItem.data.ToObject<PayItemSession.Entity>();
        payInfo.childMasterId = childMasterIds;
        payInfo.amount = Convert.ToInt32(amount.Value);
        payInfo.codeId = gift_type.Value;
        payInfo.codeName = "";


        // 결제정보를 세팅

        if (payment_method_card.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card;
        }
        else if (payment_method_cms.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.cms;
        }
        else if (payment_method_oversea.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card_oversea;
        }
        else if (payment_method_phone.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.phone;
        }
        else if (payment_method_payco.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.payco;
        }
        else if (payment_method_kakao.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.kakao;
        }

        var orderId = PayItemSession.Store.GetNewOrderID();
        payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo, orderId);
        //payItem.data = payInfo.ToJson();
        this.ViewState["payItem"] = payItem.ToJson();
        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "GiftMoney";
        this.ViewState["buyr_name"] = user_name.Value;
        this.ViewState["returnUrl"] = "/pay/complete_giftmoney/" + orderId;
        //this.ViewState["returnUrl"] = "/my/sponsor/gift-money/complete/" + orderId;



        // 결제정보를 세팅
        base.SetTemporaryPaymentInfo();
        this.ViewState["good_mny"] = total.ToString();



        if (payment_method_payco.Checked)
        {
            payco_form.Show(this.ViewState);
        }
        else if (payment_method_kakao.Checked)
        {
            kakaopay_form.Show(this.ViewState);
        }
        else
        {
            kcp_form.Show(this.ViewState);
        }

    }

    void PayRegular()
    {
        UserInfo sess = new UserInfo();
        var childMasterIds = hdChildMasterIds.Value;

        var month = ddlMonth.SelectedValue;
        if (gift_type.Value == "CF")
        {
            if (DateTime.Now.Month == 9)
            {
                month = "10";
            }
            else
            {
                month = "09";
            }
        }

        var result = new CommitmentAction().DoGIFT(sess.SponsorID, childMasterIds, amount.Value, "", month, "정기", gift_type.Value);
        if (result.success)
        {
            hdCompleted.Value = "Y";
            base.AlertWithJavascript("선물금 예약이 완료 되었습니다.", "goBack()");

            this.GetChildren();
        }
        else
        {
            base.AlertWithJavascript(result.message);
            return;
        }

    }

    // 선물금 선택
    protected void gift_type_Click(object sender, EventArgs e)
    {
        kakaopay_form.Hide();
        kcp_form.Hide();
        payco_form.Hide();
        //lblFirstNo.Visible = lblTypeStr.Visible = lblTypeStr2.Visible = false;
        lblFirstNo.Visible = false;

        ddlMonth.Enabled = true;
        pn_children.Style["display"] = "block";

        if (gift_type.Value == "BG")
        {
            ddlMonth.Visible = false;
            //	lblTypeStr.Visible = true;
        }
        else if (gift_type.Value == "CF")
        {     // 크리스마스
            month_container.Visible = ddlMonth.Visible = false;
            //	lblTypeStr2.Visible = true;
            pn_children.Style["display"] = "none";

        }
        else
        {
            ddlMonth.Visible = true;
            lblFirstNo.Visible = true;

        }

        if (ready_count.Value != "0")
        {
            lblFirstNo.Visible = true;
        }

        if (rdoOnce.Checked)
        {
            month_container.Style["display"] = "none";
        }

        this.BindAmount(gift_type.Value);

        if (gift_type.Value != "CF")
        {
            this.GetChildren();
        }

        hash.Value = "l";
    }

    // 종류별 금액

    void BindAmount(string type)
    {

        List<int> price = null;
        if (type == "BG")
        {
            price = new List<int> { 15000, 30000, 50000, 70000, 100000 };
        }
        else if (type == "GG")
        {   // 어린이
            price = new List<int> { 15000, 30000, 50000, 70000, 100000 };
        }
        else if (type == "FG")
        {   // 가족
            price = new List<int> { 35000, 50000, 100000, 300000, 500000, 1000000 };
        }
        else if (type == "PG")
        {   // 어린이센터
            price = new List<int> { 150000, 200000, 300000, 500000, 1000000, 2000000 };
        }
        else if (type == "CF")
        {   // 크리스마스
            price = new List<int> { 20000, 30000, 40000, 50000 };
        }

        pay_amount.Items.Clear();
        pay_amount.Items.Add(new ListItem("선택하세요", ""));

        foreach (var p in price)
        {
            var item = new ListItem(p.ToString("N0") + "원", p.ToString());

            pay_amount.Items.Add(item);
        }

        pay_amount.Items.Add(new ListItem("직접입력", "custom"));

    }
}