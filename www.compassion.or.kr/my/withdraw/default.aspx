﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_withdraw_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript">
	    $(function () {

	        $("#btn_submit").click(function () {
	            if (!$("#confirm").prop("checked")) {
	                alert("탈퇴 안내 확인에 체크해 주세요");
	                return false;
	            }
	            if (confirm("탈퇴하시겠습니까?")) {
	                
	                return true;
	            }
	            return false;
	        });
	    })


		
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
    <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다.</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->
		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980 withdraw">

				<uc:menu runat="server" />
				<!-- content -->
				<div class="box_type4">

					<!-- 서브타이틀 -->
					<div class="sub_tit">
						<p class="tit">회원탈퇴</p>
					</div>
					<!--// -->

					<div class="sub_desc">
						<p class="txt2"><em class="em1">한국컴패션</em> 웹사이트를<br />이용해주신 회원님께 감사드립니다.</p>

					</div>

					<div class="box_type5 withdraw_info">
						<ul class="">
							<li><span>회원탈퇴 시, 정기후원자가 아닌 웹회원인 경우, 탈퇴와 동시에 입력하신 개인정보는 모두 즉시 삭제됩니다.</span></li>
							<li><span>탈퇴 후 기존 정보의 복구 또한 불가능하게 됩니다.</span></li>
							<li><span>기존에 사용하신 아이디로 재가입이 불가능합니다.</span></li>
							<li>
								<span>
									<em>회원탈퇴는 한국컴패션 온라인 서비스 탈퇴로, 탈퇴하더라도 후원이 중지되는 것은 아닙니다.<br />
										단, 웹사이트를 통해 후원내역을 확인하실 수는 없습니다.<br />
									</em>
									(후원 관련 문의는 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr 으로 연락 주시기 바랍니다)
								</span>
							</li>
						</ul>
					</div>
					<p class="agree">
						<span class="checkbox_ui">
							<input type="checkbox" class="css_checkbox" id="confirm" runat="server"/>
							<label for="confirm" class="css_label font2">위의 내용을 확인하였습니다.</label>
						</span>
					</p>

					<div class="tac">
						<a href="../" class="btn_type2 mr10">취소</a>
                        <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="btn_type1">확인</asp:LinkButton>
					</div>

				</div>
				<!--// content -->


			</div>

			<div class="h100"></div>
		</div>
		<!--// e: sub contents -->


	</section>
	<!--// sub body -->

	<asp:PlaceHolder runat="server" ID="logout" Visible="false">
	<img runat="server" id="img" />
		<script type="text/javascript">
			alert("한국컴패션 웹회원 탈퇴가 완료되었습니다.한국컴패션 홈페이지를 이용해주셔서 감사합니다.");
			location.href = "/";
		</script>
	</asp:PlaceHolder>
	

</asp:Content>

