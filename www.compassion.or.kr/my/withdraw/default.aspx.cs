﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_withdraw_default : FrontBasePage {

    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();

        if(this.Session["pwd_confirm"] == null)
            Response.Redirect("/my/confirm-pwd/?r=" + Request.RawUrl);

        this.ViewState["pwd"] = this.Session["pwd_confirm"].ToString();
        this.Session["pwd_confirm"] = null;
		
		// 회원탈퇴
		img.Src = ConfigurationManager.AppSettings["domain_auth"] + "/logout";

	}


    protected void btn_submit_Click( object sender, EventArgs e ) {

        var result = new SponsorAction().Withdraw(this.ViewState["pwd"].ToString());
        if(result.success) {
			
            logout.Visible = true;
       
        } else {
            base.AlertWithJavascript(result.message);
        }





    }
}