﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


	public partial class my_menu : System.Web.UI.UserControl {

	protected override void OnLoad( EventArgs e ) {

		base.OnLoad(e);

		if(!IsPostBack) {

			var sess = new UserInfo();
			userName.Text = sess.UserName;
			conid.Text = sess.ConId;

			ph_conid.Visible = !string.IsNullOrEmpty(sess.ConId) && sess.ConId.Length <= 6;


            //20171130 김은지
            //백오피스 기능으로 마이페이지 로그인시 어드민 체크 코드 
            var UserCookie = FrontLoginSession.GetCookie(this.Context);
            this.ViewState["ADMINCK"] = UserCookie.AdminLoginCheck;
            //ViewState["AdminCK"] = "true";

            //2018-03-28 이종진 - #13222요청으로 해외카드결제는 해외카드결제 메뉴신설. 납부방법이 해외카드인 사용자만 보이도록함
            this.ViewState["OVERSEACK"] = false;
            var actionResult = new PaymentAction().CanUseOverseaCard();
            if(actionResult.success) {
            	
            	var canUseOverseaCard = (bool)actionResult.data;
                this.ViewState["OVERSEACK"] = canUseOverseaCard;
                
                //Response.Write(canUseOverseaCard);
            }

        }

    }

		

	}
