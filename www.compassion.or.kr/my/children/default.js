﻿$(function () {
	

});

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		$scope.total = -1;
		$scope.list = [];
		$scope.countries = null;
		$scope.children = null;
		$scope.country = "";
		$scope.child = "";

		$scope.params = {
			page: 1,
			rowsPerPage: 4
		};
        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function () {

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();

		// 국가별 어린이 목록
		$scope.getCountries = function () {
			
			$http.get("/api/my/child.ashx?t=get-child-country", { params: {} }).success(function (r) {
				console.log(r);
				if (r.success) {
					
					$scope.countries = r.data;
					$scope.changeCountry();
					setTimeout(function () {

						$(".custom_sel").selectbox("detach");
						$(".custom_sel").selectbox({});
					}, 500)

				} else {
					alert(r.message);
				}
			});
		
		}

		// list
		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);

			if ($scope.params.page == 1) {
				showList( $.parseJSON($("#data").val()));
			} else {

				$http.get("/api/my/child.ashx?t=list", { params: $scope.params }).success(function (r) {
					if (r.success) {
						showList(r.data);
					} else {
						alert(r.message);
					}
				});
			}

		}

		var showList = function (list) {

			$.each(list, function () {
				this.commitmentdate = new Date(this.commitmentdate);
				this.birthdate = new Date(this.birthdate);
			});

			console.log("showList",list);

			$scope.list = $.merge($scope.list, list);
			//	console.log($scope.data, r.data.children);
			$scope.total = $scope.list.length > 0 ? $scope.list[0].total : 0;
			//var countries = data.children;

			//$scope.countries = countries;

			setTimeout(function () {
				$(".custom_sel").selectbox("detach");
				$(".custom_sel").selectbox({});
			}, 500)

		}

		$scope.showMore = function ($event) {
			$event.preventDefault();
			$scope.params.page++;
			$scope.getList({});
		}

		$scope.changeCountry = function (country) {
			
			$scope.country = country;

			if (country == null) {
				entity = []
				$.each($scope.countries, function (i) {
				    //entity.push(this.children[0])
				    angular.forEach(this.children, function (item) {
				        entity.push(item);
				    });
				});
				$scope.children = entity;

				console.log('$scope.children', $scope.children)
			}else{
				var children = $.grep($scope.countries, function (r) {
					return r.country == country;
				})[0];
				$scope.children = children.children;
			}
			setTimeout(function () {

				$(".custom_sel").selectbox("detach");
				$(".custom_sel").selectbox({});
			}, 500)

		}

		$scope.changeChild = function (childKey) {
			if (!childKey) return;
			$scope.child = childKey;

			console.log(childKey);

			//var childmasterid = $.grep($scope.list, function (r) {
			//	return r.childkey = childKey;
		    //})[0].childmasterid;

			var childmasterid = $.grep($scope.children, function (r) {
			    return r.childkey == childKey;
			})[0].childmasterid;

			location.href = "/my/children/view/" + childKey + "?childMasterId=" + childmasterid;
		}


		// 상세페이지
		$scope.goView = function ($event, item) {
			$event.preventDefault();
			location.href = "/my/children/view/" + item.childkey + "?childMasterId=" + item.childmasterid;
		}


		$scope.getCountries();
		// 첫페이지는 codebehind에서 처리
		$scope.getList();

		$scope.showAlbum = function ($event, item) {
			$event.preventDefault();
			loading.show();
			popup.init($scope, "/common/child/album/" + item.childmasterid, function (modal) {
				modal.show();
				activateAlbum($scope, item.childkey, modal);
			}, { removeWhenClose : true});
		}

		// 편지쓰기
		$scope.goLetter = function ($event , item) {

			$event.preventDefault();
			if (!item.paid) {
			    //alert("첫 후원금 납부 후 어린이에게 편지를 보내실 수 있습니다.\n납부 관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr ");
			    //return;

			    if (confirm("첫 후원금 납부 후 어린이에게 편지를 보내실 수 있습니다.\n납부를 진행하시겠습니까?")) {
			        $http.get("/sponsor/pay-gateway.ashx?t=go-cdsp-letter", { params: { childMasterId: item.childmasterid } }).success(function (r) {

			            if (r.success) {
			                //alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
			                location.href = r.data;
			            } else {
			                if (r.action == "nonpayment") {		// 미납금

			                    if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
			                        location.href = "/my/sponsor/pay-delay/";
			                    }

			                } else {
			                    alert(r.message);
			                }
			            }

			        });
			    } else {
			        return;
			    }

			}else{

			    if (!item.canletter) {
				    alert("후원자님께서는 어린이와 편지를 주고 받지 않으시는 머니 후원 중이십니다. 감사합니다.");
				    return;
			    }
			    location.href = "/my/letter/write?c=" + item.childmasterid;
			}
		}

		// 편지쓰기
		$scope.goGift = function ($event , item) {

			$event.preventDefault();
			if (!item.paid) {
                /*
				alert("첫 후원금 납부 후 어린이에게 선물금을 보내실 수 있습니다.\n납부 관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr ");
				return;
                */
			    if (confirm("첫 후원금 납부 후 어린이에게 선물금을 보내실 수 있습니다.\n납부를 진행하시겠습니까?")) {

			        $http.get("/sponsor/pay-gateway.ashx?t=go-cdsp-letter", { params: { childMasterId: item.childmasterid } }).success(function (r) {

			            if (r.success) {
			               // alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
			                location.href = r.data;
			            } else {
			                if (r.action == "nonpayment") {		// 미납금

			                    if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
			                        location.href = "/my/sponsor/pay-delay/";
			                    }

			                } else {
			                    alert(r.message);
			                }
			            }

			        });


			    } else {
			        return;
			    }
			}

			if (!item.cangift) {
				alert("편지후원자의 경우는 선물금보내기 기능이 제한됩니다.");
				return;
			}
			location.href = "/my/sponsor/gift-money/pay/?t=temporary&childMasterId=" + item.childmasterid;

		}

		
	});

})();