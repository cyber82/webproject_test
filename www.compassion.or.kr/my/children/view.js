﻿$(function () {
	

});

var lat , lng;

function appendGoogleMapApi() {
	if (typeof google === 'object' && typeof google.maps === 'object') {
		initMap();
	} else {
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "https://maps.googleapis.com/maps/api/js?key="+ ($("#googleMapApiKey").val()) +"&callback=initMap";
		document.body.appendChild(script);
	}
}

function initMap() {

	map = new google.maps.Map(document.getElementById('map'), {
		center: { lat: lat, lng: lng },
		zoom: 5,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		icon: "/common/img/icon/pin.png",
		map: map
	});
		
};

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

		$scope.giftable = false;		// 선물금가능한지 여부

		$scope.entity = null;
		$scope.countries = null;
		$scope.children = null;
		$scope.country = "";
		$scope.child = "";

        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function () {

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();

		// 국가별 어린이 목록
		$scope.getCountries = function () {

			$http.get("/api/my/child.ashx?t=get-child-country", { params: {} }).success(function (r) {
				console.log(r);
				if (r.success) {

					$scope.countries = r.data;

					$scope.changeCountry($scope.entity.countryname);

					setTimeout(function () {

						$(".custom_sel").selectbox("detach");
						$(".custom_sel").selectbox({});
					}, 500)
				} else {
					alert(r.message);
				}
			});

		}

		// 어린이정보
		$scope.getDetail = function () {
			$http.get("/api/my/child.ashx?t=detail", { params: { childKey: $("#childKey").val() } }).success(function (r) {

				console.log(r);
				if (r.success) {
					
					$scope.entity = $.extend({} , r.data);
					$scope.entity.birthdate = new Date($scope.entity.birthdate);
					$scope.entity.commitmentdate = new Date($scope.entity.commitmentdate);
					var bio = $scope.entity.biokr;
					bio = bio.replace(/(?:\r\n|\r|\n)/g, '<br />');
                    $scope.entity.biokr = bio;
					
					$scope.getCountryInfo($scope.entity.countrycode);

					$scope.country = $scope.entity.countryname;
					$scope.child = $scope.entity.childkey;
					
					$scope.getCountries();

					$scope.getLetterList(1);

					$scope.checkGiftable();

					$scope.getGiftReservationList(1);

					$scope.getGiftPaymentList(1);

					if ($scope.entity.cognitive.split('no activitie').length > 1 || $scope.entity.cognitive == "")
					    $('.cognitive').hide();
					if ($scope.entity.socioemotional.split('no activitie').length > 1 || $scope.entity.socioemotional == "")
					    $('.socioemotional').hide();
					if ($scope.entity.spiritual.split('no activitie').length > 1 || $scope.entity.spiritual == "")
					    $('.spiritual').hide();
					if ($scope.entity.physical.split('no activitie').length > 1 || $scope.entity.physical == "")
					    $('.physical').hide();


					if ($scope.entity.projectinfo == "" || $scope.entity.projectinfo == null) {
					    // ICP 정보 조회되지 않을 경우 지움
					    $(".compassionCenter").html("");
					}
					else
					    $(".compassionCenter").show();

					setTimeout(function () {
						$(".custom_sel").selectbox("detach");
						$(".custom_sel").selectbox({});
					}, 500)

					if (paramService.getParameter("album") == "Y")
						$scope.showAlbum(null, $scope.entity);

				} else {
					alert(r.message);
					location.href = "/my/children/";
				}
			});
		}

		$scope.changeCountry = function (country) {
			$scope.country = country;

			if (country == null) {
				$scope.children = [];
			} else {
				var children = $.grep($scope.countries, function (r) {
					return r.country == country;
				})[0];
				$scope.children = children.children;
			}
			setTimeout(function () {

				$(".custom_sel").selectbox("detach");
				$(".custom_sel").selectbox({});
			}, 500)

		}

		$scope.changeChild = function (childKey) {
			if (!childKey) return;
			$scope.child = childKey;

			$.each($scope.countries, function () {
				
				for (i = 0 ; i < this.children.length ; i++) {

					if (childKey == this.children[i].childkey) {
						location.href = "/my/children/view/" + childKey + "?childMasterId=" + this.children[i].childmasterid;
						break;
					}
				}

			});

		}

		// 레터내역
		$scope.letter_total = -1;
		$scope.letter_page = 1;
		$scope.letter_rowsPerPage = 4;
		$scope.letter_list = [];
		
		$scope.getLetterList = function (page) {
			
			var params = { childMasterId: $("#childMasterId").val(), type: "all", page: page, rowsPerPage: $scope.letter_rowsPerPage };

			$http.get("/api/my/letter.ashx?t=list&include-ready=N", { params: params }).success(function (r) {
				console.log("편지" , r);
				if (r.success) {

					var list = $.extend([],r.data.list);
				
					$.each(list, function () {
						this.display_date = new Date(this.displaydate);
					});

					// 더보기인경우 merge
					$scope.letter_list = $.merge($scope.letter_list, list);
					$scope.letter_total = list.length > 0 ? list[0].total : 0;
					
					$scope.letter_page = params.page;		// 현재 페이지 갱신해야 함
					
				} else {
					alert(r.message);
				}
			});

		}

		// 선물금예약/결제 가능한지 여부 확인
		$scope.checkGiftable = function () {

			$http.get("/api/my/payment.ashx?t=gift-giftable", { params: {} }).success(function (r) {

				if (r.success) {
					if (r.data > 0)
						$scope.giftable = true;

				} else {
					alert(r.message);
				}
			});

		}

		// 선물금예약내역
		$scope.gr_total = -1;
		$scope.gr_page = 1;
		$scope.gr_rowsPerPage = 3;
		$scope.gr_list = [];

		$scope.getGiftReservationList = function (page) {

			var params = { childMasterId: $("#childMasterId").val(), page: page, rowsPerPage: $scope.gr_rowsPerPage };

			$http.get("/api/my/payment.ashx?t=gift-reservation", { params: params }).success(function (r) {

				if (r.success) {

					var list = $.extend([], r.data);

					$.each(r.data, function () {
						this.startdate = new Date(this.startdate);
					});

					// 더보기인경우 merge
					$scope.gr_list = list;
					$scope.gr_total = list.length > 0 ? list[0].total : 0;

					$scope.gr_page = params.page;		// 현재 페이지 갱신해야 함

				} else {
					alert(r.message);
				}
			});

		}

		// 선물금 결제내역
		$scope.gp_total = -1;
		$scope.gp_page = 1;
		$scope.gp_rowsPerPage = 3;
		$scope.gp_list = [];
		$scope.getGiftPaymentList = function (page) {
			
			var params = { childMasterId: $("#childMasterId").val(), page: page, rowsPerPage: $scope.gp_rowsPerPage };

			$http.get("/api/my/payment.ashx?t=gift-payment", { params: params }).success(function (r) {
				
				if (r.success) {

					var list = $.extend([], r.data);
					console.log("payment", list);

					$.each(r.data, function () {
						this.paymentdate = new Date(this.paymentdate);
					});

					// 더보기인경우 merge
					$scope.gp_list = list;
					$scope.gp_total = list.length > 0 ? list[0].total : 0;

					$scope.gp_page = params.page;		// 현재 페이지 갱신해야 함

				} else {
					alert(r.message);
				}
			});

		}

		$scope.deleteGiftReservation = function ($event , commitmentid) {

			$event.preventDefault();
			if (!confirm("선물금 예약을 취소하고 예약정보를 삭제하시겠어요?")) {
				return false;
			}
	
			$http.post("/api/my/payment.ashx?t=gift-delete-reservation", { c: commitmentid }).success(function (r) {

				if (r.success) {

					$scope.getGiftReservationList(1);

				} else {
					alert(r.message);
				}
			});

		}

		$scope.modifyGiftReservation = function ($event, commitmentid) {

			$event.preventDefault();
			
			location.href = "/my/sponsor/gift-money/update-reservation/?c=" + commitmentid;

		}

		// 국가정보, 날씨 , 기온 , 시간
		$scope.countryInfo = null;
		$scope.getCountryInfo = function (code) {

			$http.get("/api/country.ashx?t=info", { params: { code: code } }).success(function (r) {

				if (r.success) {
					$scope.countryInfo = r.data;
					var local_time = new Date();
					local_time.setSeconds(local_time.getTimezoneOffset() * 60 + r.data.c_timeoffset);
					
					$scope.countryInfo.time = local_time;
					
					lat = $scope.countryInfo.c_lat;
					lng = $scope.countryInfo.c_lng;

					appendGoogleMapApi();

					console.log($scope.countryInfo);
				} else {
					alert(r.message);
				}
			});

		}

		$scope.getDetail();

	

		// 성장일기
		$scope.showAlbum = function ($event, item) {
			loading.show();
			if ($event) $event.preventDefault();
			popup.init($scope, "/common/child/album/" + item.childmasterid, function (modal) {
				modal.show();
				activateAlbum($scope, item.childkey, modal);
			}, { removeWhenClose : true});
		}
		

		// 편지쓰기
		$scope.goLetter = function ($event, item) {

			$event.preventDefault();
			if (!item.paid) {
			    //alert("첫 후원금 납부 후 어린이에게 편지를 보내실 수 있습니다.\n납부 관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr ");
			    if (confirm("첫 후원금 납부 후 어린이에게 편지를 보내실 수 있습니다.\n납부를 진행하시겠습니까?")) {
			        $http.get("/sponsor/pay-gateway.ashx?t=go-cdsp-letter", { params: { childMasterId: item.childmasterid } }).success(function (r) {

			            if (r.success) {
			                //alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
			                location.href = r.data;
			            } else {
			                if (r.action == "nonpayment") {		// 미납금

			                    if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
			                        location.href = "/my/sponsor/pay-delay/";
			                    }

			                } else {
			                    alert(r.message);
			                }
			            }

			        });
			    } else {
			        return;
			    }
			}

			if (!item.canletter) {
				alert("후원자님께서는 어린이와 편지를 주고 받지 않으시는 머니 후원 중이십니다. 감사합니다.");
				return;
			}
			location.href = "/my/letter/write?c=" + item.childmasterid;

		}

		// 선물금
		$scope.goGift = function ($event, item) {

			$event.preventDefault();
			if (!item.paid) {
				//alert("첫 후원금 납부 후 어린이에게 선물금을 보내실 수 있습니다.\n납부 관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr ");
			    if (confirm("첫 후원금 납부 후 어린이에게 선물금을 보내실 수 있습니다.\n납부를 진행하시겠습니까?")) {

			        $http.get("/sponsor/pay-gateway.ashx?t=go-cdsp-letter", { params: { childMasterId: item.childmasterid } }).success(function (r) {

			            if (r.success) {
			                // alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
			                location.href = r.data;
			            } else {
			                if (r.action == "nonpayment") {		// 미납금

			                    if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
			                        location.href = "/my/sponsor/pay-delay/";
			                    }

			                } else {
			                    alert(r.message);
			                }
			            }

			        });


			    } else {
			        return;
			    }
			}

			if (!item.cangift) {
				alert("편지후원자의 경우는 선물금보내기 기능이 제한됩니다.");
				return;
			}
			location.href = "/my/sponsor/gift-money/pay/?t=temporary&childMasterId=" + item.childmasterid;

		}

		// 어린이 이미지
		$scope.getChildImage = function () {

			$http.get("/api/tcpt.ashx?t=get-child-image", { params: { childMasterId: $("#childMasterId").val(), childKey: $("#childKey").val() } }).success(function (r) {

				if (r.success) {
					$("#img_child").attr("src", r.data);
					$("#img_child").show();
				} 
			});

		}

		$scope.getChildImage();

	});

})();