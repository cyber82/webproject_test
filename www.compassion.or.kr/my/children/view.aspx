﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="my_children_view" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/my/children/view.js?v=1"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <style>
        .mynation{
            font-size: 22px;
            color: #333;
            margin-bottom: 15px;
            /*display: block;
            font-family: 'noto_m';
            font-weight: normal;
            font-size: 16px;
            line-height: 20px;
            color: #333;
            padding-bottom: 18px;*/
        }

    </style>
    <input type="hidden" id="childKey" runat="server" />
    <input type="hidden" id="childMasterId" runat="server" />
    <input type="hidden" id="googleMapApiKey" runat="server" />

    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit" ng-if="AdminCK == false">
            <div class="titArea">
                <h1>마이컴패션</h1>
                <span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents mypage">

            <div class="w980">

                <!-- 후원자 이름 -->
                <uc:menu runat="server" />
                <!--// -->

                <div class="mb20"></div>

                <!-- 나의어린이 -->
                <div class="box_type4 myChild view">

                    <!-- 서브타이틀 -->
                    <div class="sub_tit mb70">
                        <p class="tit">나의 어린이</p>
                    </div>
                    <!--// -->

                    <!-- 어린이정보 -->
                    <div class="clear2 mb30">
                        <div class="picWrap">
                            <!-- 사진사이즈 : 242 * 333 -->
                            <div class="pic">
                                <span>
                                    <img id="img_child" alt="추천어린이" style="display: none" width="242" /></span>
                            </div>
                            <span class="name"><em>{{entity.name}} {{entity.personalnameeng}}</em></span>
                        </div>
                        <div class="textWrap">

                            <!-- 어린이이름검색 -->
                            <div class="childname_search" ng-if="countries.length > 1">
                                <span class="wrap">
                                    <span class="sel_type1 mr5" style="width: 140px">
                                        <label for="country" class="hidden">국가 선택</label>
                                        <select class="custom_sel" id="country" ng-model="country" ng-change="changeCountry(country)" data-ng-options="item.country as item.country for item in countries">
                                            <option value="">국가</option>
                                        </select>
                                    </span>
                                    <span class="sel_type1 mr5" style="width: 200px">
                                        <label for="child" class="hidden">어린이 선택</label>
                                        <select class="custom_sel" id="child" ng-model="child" ng-change="changeChild(child)" data-ng-options="item.childkey as item.namekr for item in children">
                                            <option value="">어린이이름</option>
                                        </select>
                                    </span>
                                </span>
                            </div>
                            <!--// -->

                            <span class="label_meet">우리가 만난 날 {{entity.commitmentdate | date:'yyyy.MM.dd'}}</span><br />
                            <span class="title"><em><%:this.ViewState["userName"].ToString() %></em> 후원자님, 안녕하세요?</span><br />
                            <p class="con">
                                우리가 함께한 지 <em class="fc_black">{{entity.commitmentdays| number:0}}</em>일이 지났어요.<br />
                                후원금과 기도, 사랑의 편지 항상 감사드려요.
                            </p>
                            <style>
                                .subContents.mypage .myChild.view .textWrap .detail_info { height: 140px; }
                            </style>
                            <div class="detail_info">
                                <span class="field">어린이 이름</span><span>{{entity.namekr}} {{entity.nameen}}</span><br />
                                <span class="field">어린이 애칭</span><span>{{entity.name}} {{entity.personalnameeng}}</span><br />
                                <span class="field">어린이ID</span><span class="data">{{entity.childkey}}</span><span class="field fd2">국가</span><span class="data">{{entity.countryname}}</span><br />
                                <span class="field">생일</span><span class="data">{{entity.birthdate | date:'yyyy.MM.dd'}} ({{entity.age}}세)</span><span class="field fd2">성별</span><span class="data">{{entity.gender}}</span>
                            </div>

                            <div class="btn">
                                <a href="#" ng-click="goLetter($event , entity);" class="on" ng-if="AdminCK == false"><span class="letter">편지쓰기</span></a>
                                <a href="#" ng-click="goGift($event , entity)" ng-if="AdminCK == false"><span class="gift">선물금 보내기</span></a>
                                <a href="#" ng-click="showAlbum($event , entity);"><span class="diary">성장앨범 보기</span></a>
                            </div>

                        </div>
                    </div>
                    <!--// 어린이정보 -->

                    <!-- 어린이스토리 -->
                    <div class="box_type3 story">
                        <!--
						<span ng-if="entity.casestudy.family">저희 가족은요, <em>{{entity.casestudy.family}}</em> <em>형제 2명</em>이 있어요.</span>
						<span ng-if="entity.casestudy.familyduties">저는 집에서 가족과 함께 <em>{{entity.casestudy.familyduties}}</em>을(를) 해요.<br /></span>
						<span ng-if="entity.casestudy.christianactivities">제가 참여하는 컴패션 활동은요 <em>{{entity.casestudy.christianactivities}}</em>이예요. </span>
						<span ng-if="entity.casestudy.hobby">제 취미와 운동은 <em>{{entity.casestudy.hobby}}</em> 예요.<br /></span>
						<span ng-if="entity.casestudy.schooling"><em>{{entity.casestudy.schooling}}</em>에 다니고 있고, </span>
						<span ng-if="entity.casestudy.family">좋아하는 과목은 <em>{{entity.casestudy.family}}</em>이예요. <br />-->
                        <span ng-if="entity.biokr != ''" ng-bind-html="entity.biokr"></span>
                        <span ng-if="entity.biokr == ''" >현재 어린이 정보를 번역 중입니다.</span>
                        <br />

                        <span ng-if="entity.casestudy.health">(<em>{{entity.casestudy.health}}</em>)</span>

                    </div>
                    <!--// -->

                    <!-- 지도,날씨 -->
                    <div class="mapWrap clear2">
                        <p class="mynation">지금 {{countryInfo.c_name}}는(은)요,<br /></p>
                        <!-- 지도사이트 : 448 * 296 -->
                        <div class="map" id="map"></div>

                        <div class="weather">
                            <div class="wrap">
                                <img ng-show="countryInfo" ng-src="/common/img/page/sponsor/weather/{{countryInfo.c_weather_icon}}.png" class="inline" alt="흐림" />

                                <span class="time">{{countryInfo.time | date:'HH:mm'}}</span>
                                <span class="day">{{countryInfo.time | date:'MMMM dd'}}</span>
                                <span class="temperature">{{countryInfo.c_temperature | number:0}}˚C</span>
                                <span class="state">{{countryInfo.c_weather}}</span>
                                <span class="nation">{{countryInfo.c_name}}</span>
                            </div>
                        </div>
                    </div>
                    <!--// -->

                    <!-- 그래프 -->
                    <div class="gdp_wrap" ng-show="countryInfo">
                        <img ng-src="/common/img/page/sponsor/gdp/{{countryInfo.c_id}}.jpg" class="inline" alt="1인당 GDP그래프" />
                    </div>
                    <!--// -->

                    <!-- 센터설명 -->
                    <div class="compassionCenter" style="display: none">
                        <p class="tit">{{entity.name}}이(가) 다니는 어린이센터는,</p>
                        <div class="centerDetail">
                            <span ng-bind-html="entity.projectinfo"></span>
                            <br />
                            <span>■ 어린이센터 활동</span>
                            <div class="box_icp">
                                <div class="box_icp_detail cognitive">
                                    <span>□ 지적 영역</span>
                                    <div class="box_type9" ng-bind-html="entity.cognitive">
                                    </div>
                                </div>
                                <div class="box_icp_detail socioemotional">
                                    <span>□ 사회정서적 영역</span>
                                    <div class="box_type9" ng-bind-html="entity.socioemotional">
                                    </div>
                                </div>
                                <div class="box_icp_detail spiritual">
                                    <span>□ 영적 영역</span>
                                    <div class="box_type9" ng-bind-html="entity.spiritual">
                                    </div>
                                </div>
                                <div class="box_icp_detail physical">
                                    <span>□ 신체적 영역</span>
                                    <div class="box_type9" ng-bind-html="entity.physical">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!--// -->

                <!--// 나의어린이 -->

                <!-- 편지함 -->
                <div ng-show="entity.canletter">
                    <p class="s_tit1 mb15">편지함</p>

                    <div class="mailbox">

                        <div class="clear2" ng-show="letter_total > 0">
                            <div class="box_type4">
                                <ul class="list">
                                    <li ng-repeat="item in letter_list" ng-class="{'send' : item.direction == 'send' ,'receive' : item.direction == 'receive'}">
                                        <span ng-if="item.direction == 'send'" class="pic" style="background: url('/common/img/common/no_img3.png') center top">어린이사진</span>
                                        <span ng-if="item.direction != 'send'" class="pic" style="background: url('{{item.pic}}') center top">어린이사진</span>

                                        <span class="listCon">
                                            <span class="name">{{item.direction == 'send' ? '후원자님의 편지' : item.namekr + ' 어린이 편지'}}</span><span class="new" ng-if="item.checkstatusyn == 'N'">new</span><br />
                                            <span class="date">{{item.display_date | date:'yyyy.MM.dd'}}</span>
                                            <span class="btns">
                                                <span class="txt_status" ng-if="item.is_smart == 'True' && item.direction == 'send'">발송완료(스마트레터)</span>
                                                <span class="txt_status" ng-if="item.is_smart == 'False' && item.direction == 'send'">발송완료</span>
                                                <a href="/my/letter/write/?c={{item.childmasterid}}" class="btn_reply" ng-if="(item.direction == 'receive') &&  (AdminCK == false)">답장쓰기</a>
                                                <span class="bar" ng-if="(item.direction == 'receive' || item.direction == 'send') || AdminCK == false"></span>
                                                <a href="/my/letter/view/{{item.corrid}}" class="btn_letter" ng-if="item.direction == 'receive' || item.direction == 'send'">편지보기</a>

                                            </span>
                                            <span class="arr"></span>
                                        </span>
                                    </li>

                                </ul>

                                <div class="tac" ng-if="letter_total > 4"><a href="/my/letter/" class="btn_s_type3">더보기<span class="btn_arr1"></span></a></div>
                            </div>

                            <div class="bnr_write">
                                <p class="tit">어린이에게 편지란?</p>
                                <span class="bar"></span>
                                <p class="con">
                                    컴패션에서 어린이가 받는 편지는<br />
                                    어린이가 양육 받을 수 있는 또 다른<br />
                                    통로입니다. 편지를 통해 어린이에게<br />
                                    사랑을 전해주세요.
                                </p>
                                <a href="/my/letter/write" class="btn_b_type3" ng-if="AdminCK == false">편지쓰기</a>
                            </div>
                        </div>

                        <!-- 내용없을경우 -->
                        <div class="box_type4 no_content" ng-show="letter_total == 0">
                            <p>
                                앗, 편지함에 편지가 없어요.<br />
                                어린이에게 편지를 한번 써보시겠어요?
                            </p>
                            <a href="/my/letter/write" ng-show="AdminCK == false" class="btn_type5">편지쓰기</a>
                        </div>
                    </div>
                    <!--// 편지함 -->
                </div>

                <!-- 선물금 예약내역 -->
                <div ng-show="entity.cangift">
                    <p class="mb15">
                        <span class="s_tit3 vam">선물금 예약 내역</span>
                        <a href="/my/sponsor/gift-money/pay/?t=regular" ng-if="giftable && AdminCK == false" class="btn_s_type2 vam ml20">선물금 예약하기</a></span>
                    </p>
                    <div class="box_type4 mb40">

                        <div class="tableWrap1 mb30">
                            <table class="tbl_type6 padding1">
                                <caption>선물금 예약 내역 테이블</caption>
                                <colgroup>
                                    <col style="width: 18%" />
                                    <col style="width: 23%" />
                                    <col style="width: 25%" />
                                    <col style="width: 15%" />
                                    <col style="width: 18%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">신청일</th>
                                        <th scope="col">선물금 종류</th>
                                        <th scope="col">금액</th>
                                        <th scope="col">예약 월</th>
                                        <th scope="col">수정/삭제</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in gr_list">
                                        <td>{{item.startdate | date:'yyyy.MM.dd'}}</td>
                                        <td>{{item.sponsoritem}}</td>
                                        <td class="price">{{item.sponsoramount | number:0}}원</td>
                                        <td>{{item.fundingfrequency}} {{item.sponsormonth}}월</td>
                                        <td>
                                            <div class="btn_md">
                                                <button class="modify" ng-click="modifyGiftReservation($event,item.commitmentid)" ng-if="AdminCK == false">
                                                <span>수정</span></button>
                                                <span class="bar" ng-if="AdminCK == false"></span>
                                                <button class="del" ng-click="deleteGiftReservation($event,item.commitmentid)" ng-if="AdminCK == false">
                                                <span>삭제</span></button>
                                            </div>
                                        </td>
                                    </tr>

                                    <!-- 내용없을경우 -->
                                    <tr>
                                        <td colspan="5" class="no_content icon1" ng-if="gr_total == 0">
                                            <p class="mb20">등록된 선물금 예약 정보가 없습니다.</p>
                                            <a href="/my/sponsor/gift-money/pay/?t=regular" ng-if="giftable && AdminCK == false" class="btn_type1">선물금 예약하기</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- page navigation -->
                        <div class="tac">
                            <paging class="small" page="gr_page" page-size="gr_rowsPerPage" total="gr_total" show-prev-next="true" show-first-last="true" paging-action="getGiftReservationList(page)"></paging>
                        </div>
                        <!--// page navigation -->

                    </div>
                    <!--// 선물금 예약내역-->

                    <!-- 어린이 선물금 결제내역 -->
                    <p class="mb15">
                        <span class="s_tit3 vam">어린이 선물금 결제 내역</span>
                        <a href="/my/sponsor/gift-money/pay/?t=temporary" ng-if="giftable && AdminCK == false" class="btn_s_type2 vam ml20">선물금 보내기</a></p></span>
                    <div class="box_type4 mb40">

                        <div class="tableWrap1 mb30">
                            <table class="tbl_type6 padding1">
                                <caption>어린이 선물금 결제내역 테이블</caption>
                                <colgroup>
                                    <col style="width: 33%" />
                                    <col style="width: 34%" />
                                    <col style="width: 33%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">결제일</th>
                                        <th scope="col">선물금 종류</th>
                                        <th scope="col">금액</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in gp_list">
                                        <td>{{item.paymentdate | date:'yyyy.MM.dd'}}</td>
                                        <td>{{item.sponsoritem}}</td>
                                        <td class="price">{{item.sponsoramount | number:0}}원</td>
                                    </tr>

                                    <!-- 내용없을경우 -->
                                    <tr>
                                        <td colspan="3" class="no_content icon2" ng-if="gp_total == 0">
                                            <p class="mb20">등록된 선물금 내용이 없습니다. 어린이에게 선물을 한 번 보내보세요.</p>
                                            <a href="/my/sponsor/gift-money/pay/?t=temporary" ng-if="giftable" class="btn_type1">선물금 보내기</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- page navigation -->
                        <div class="tac">
                            <paging class="small" page="gp_page" page-size="gp_rowsPerPage" total="gp_total" show-prev-next="true" show-first-last="true" paging-action="getGiftPaymentList(page)"></paging>
                        </div>
                        <!--// page navigation -->

                    </div>
                    <!--// 어린이 선물금 결제내역-->
                </div>

                <!-- 선물하기 -->
                <p class="mb15">
                    <span class="s_tit3" ng-show="AdminCK == false">직접 고른 선물을 하고싶으신가요?</span>
                </p>
                <div>
                 <a href="/store/?gift_flag=1" ng-show="AdminCK == false">
                 <img src="/common/img/page/my/btn_banner.jpg" alt="선물 고르러 스토어로 가기" /></a>
                </div>
                <!--// 선물하기-->

            </div>
        </div>

        <div class="h100"></div>
        <!--// e: sub contents -->

    </section>


</asp:Content>
