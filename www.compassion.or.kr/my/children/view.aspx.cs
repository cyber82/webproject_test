﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using System.IO;


public partial class my_children_view : FrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}
    
    protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var requests = Request.GetFriendlyUrlSegments();
		if(requests.Count < 1) {
			Response.Redirect("/my/", true);
		}
		
		childKey.Value = requests[0];      // childKey
        if (Request["childMasterId"] != null)
        {
            childMasterId.Value = Request["childMasterId"];
        }
        else
        {
            if (requests.Count > 1)
                childMasterId.Value = requests[1];
        }

		googleMapApiKey.Value = ConfigurationManager.AppSettings["googleMapApiKey"];
		this.ViewState["userName"] = new UserInfo().UserName;
		
	}


}