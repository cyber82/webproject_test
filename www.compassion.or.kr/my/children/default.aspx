﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_children_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/children/default.js?v=1.0"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
<input type="hidden" id="data" runat="server" />

<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
    	<!-- 타이틀 -->
		<div class="page_tit" ng-if="AdminCK == false">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980">

				<!-- 후원자 이름 -->
				<uc:menu runat="server"  />

				<div class="mb20"></div>

				<!-- 나의어린이 -->
				<div class="box_type4 myChild view">

					<!-- 서브타이틀 -->
					<div class="sub_tit mb20">
						<p class="tit">나의 어린이</p>
					</div>
					<!--// -->

					<!-- 어린이검색 -->
					<div class="tar mb20" ng-if="countries.length > 1">
						<span class="sel_type1 mr5" style="width:140px">
							<label for="country" class="hidden">국가 선택</label>
							<select class="custom_sel" id="country" ng-model="country" ng-change="changeCountry(country)" data-ng-options="item.country as item.country for item in countries">
								<option value="">국가</option>
							</select>
						</span>
						<span class="sel_type1 mr5" style="width:200px">
							<label for="child" class="hidden">어린이 선택</label>
							<select class="custom_sel" id="child" ng-model="child" ng-change="changeChild(child)" data-ng-options="item.childkey as item.namekr for item in children">
								<option value="">어린이이름</option>
							</select>
						</span>
						<!--<a href="#" class="btn_type9">검색</a>-->
						
					</div>
					<!--// -->

					<!-- 어린이 리스트 -->
					<div class="overTwo">

						<!-- set -->
						<div class="childBox" ng-repeat="item in list">
							<div class="textWrap">
								<span class="label_meet">우리가 만난 날 {{item.commitmentdate | date:'yyyy.MM.dd'}}</span>
							</div>

							<div class="child_info">
								<a href="#" ng-click="goView($event,item)">
								<span class="pic" style="background:url('{{item.pic}}') no-repeat center top; background-size:cover;">양육어린이사진</span>
								<span class="name">{{item.name == '' ? item.personalnameeng : item.name}}</span>
								</a>
								<p class="info mb30">
									<span class="bar">어린이ID : {{item.childkey}}</span><span>국가 : {{item.countryname}}</span><br />
									<span class="bar">생일 : {{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><span>성별 : {{item.gender}}</span>
								</p>

								<div class="btn">
                                    <span ng-if="AdminCK == false">
									<a href="#" ng-click="goLetter($event , item);" class="on"><span class="letter">편지쓰기</span></a>
									<a href="#" ng-click="goGift($event , item)"><span class="gift">선물금 보내기</span></a>
                                    </span>
									<a href="#" ng-click="showAlbum($event,item)"><span class="diary">성장앨범 보기</span></a>
								</div>
								
							</div>
						</div>
						<!--// set -->

					</div>

					<button class="btn_com_more" ng-show="total > list.length" ng-click="showMore($event)"><span>더 보기</span></button>
					<!--// 어린이 리스트 -->


				</div>
				<!--// 나의어린이 -->

				
			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>

</asp:Content>
