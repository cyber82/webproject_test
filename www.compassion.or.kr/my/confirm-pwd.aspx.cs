﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_confirm_pwd : FrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		return_url.Value = Request.QueryString["r"].ValueIfNull("/my/");

	}


	protected void btn_submit_Click( object sender, EventArgs e )
    {
		var sess = new UserInfo();

        using (AuthDataContext dao = new AuthDataContext())
        {
            //var data = dao.sp_tSponsorMaster_get_f(sess.UserId, pwd.Value, "", "", "", "", "").ToList();
            Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
            Object[] op2 = new Object[] { sess.UserId, pwd.Value, "", "", "", "", "" };
            var data = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<CommonLib.sp_tSponsorMaster_get_fResult>();

            if (data.Count < 1)
            {
                //AlertWithJavascript("비밀번호가 일치하지 않습니다. 다시 입력해주세요.");
                msg_pwd.Visible = true;
                return;
            }

        }

		this.Session["pwd_confirm"] = pwd.Value;
		Response.Redirect(return_url.Value);
	}
}