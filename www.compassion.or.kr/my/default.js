﻿$(function () {

	$("#date_begin").dateRange({
		buttons: ".dateRange",	// preset range
		end: "#date_end",
		onClick: function () {

		}
	});

	$("#date_begin").dateValidate({
		end: "#date_end",
		onSelect: function () {

		}
    });

    

});

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {
        
		$scope.thismonth = new Date();
		$scope.commitment_list = null;
		$scope.commitment_total = -1;
		$scope.payment_total = null;
		$scope.order_total = -1;
		$scope.order_list = null;
		$scope.data = null;
		$scope.select_orderNo = "";
        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function(){

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();

		// 데이타
		$scope.getData = function () {

			$http.get("/api/my/main.ashx?t=data", { params: {} }).success(function (r) {
			//	console.log(r);
				if (r.success) {

					$scope.data = r.data;
					//console.log("main", $scope.data);
					
					if (r.data.child) {
						var name = r.data.child.name;
						var regex = /\(([^)]+)\)/;
						var matches = regex.exec(name);
						var en_name = matches[1];
						var ko_name = name.replace("(" + matches[1] + ")", "");
						r.data.child.en_name = en_name;
						r.data.child.ko_name = ko_name;
						if (r.data.child.pic == "" || r.data.child.pic == null || r.data.child.pic == undefined)
						    $scope.getChildImage(r.data.child);
					}


				} else {
					if (r.action == "not_sponsor") {
						$scope.data = r.data;
					}
					//alert(r.message);
				}
			});

		}

		// 정기후원내역
		$scope.getCommitmentList = function () {

			$http.get("/api/my/commitment.ashx?t=list", { params: {page : 1 , rowsPerPage : 3} }).success(function (r) {
				
				if (r.success) {
					
					var list = r.data;
					
					$.each(list, function () {
						this.startdate = new Date(this.startdate);
						this.icon = getIcon(this.accountclass);
					});
					$scope.commitment_list = list;
					$scope.commitment_total = list.length;
				} else {

					
				}
			});

			var getIcon = function (accountclass) {
				switch (accountclass) {
					default: return "ic-gift3";	// 특별한후원
					case "DS": case "DSADD": case "LS": case "LSADD": return "ic-gift1";
					case "DF": return "ic-gift2";
					case "BG": case "PG": case "GG": case "FG": return "ic-gift4";
				}
			};
		}

		// 이번달 결제금액
		$scope.getPaymentList = function () {

			var date_begin = $filter('date')(new Date(), 'yyyy-MM-01');
			var date_end = $filter('date')(new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0), 'yyyy-MM-dd');
			//console.log(date_begin, date_end);

			$http.get("/api/my/payment.ashx?t=history", { params: { page: 1, rowsPerPage: 1, date_begin: date_begin, date_end: date_end } }).success(function (r) {

				console.log(r);
				if (r.success) {

					if (r.data.length > 0) {
						$scope.payment_total = r.data[0].totalamount;
					} else {
						$scope.payment_total = 0;
					}

				} else {
					//	alert(r.message);
					if (r.action == "not_sponsor") {
						$scope.payment_total = 0;
					}

				}
			});
		}

		// 이번달 주문 내역 
		$scope.getOrderList = function () {

			$http.get("/api/store.ashx?t=order_list", { params: { page: 1, rowsPerPage: 5} }).success(function (r) {

				if (r.success) {
				    var list = r.data;

                    
					if (r.data.length > 0) {
						$scope.order_total = r.data[0].totalamount;
					}

					$.each(list, function () {
						this.dtreg = new Date(this.dtreg);

						if (this.statename == "출고완료") {
							if (this.deliveryco == 1) {
								this.href = 'https://trace.epost.go.kr/xtts/servlet/kpl.tts.common.svl.SttSVL?ems_gubun=E&sid1=' + this.deliverynum + '&POST_CODE=&mgbn=trace&traceselect=1&target_command=kpl.tts.tt.epost.cmd.RetrieveOrderConvEpostPoCMD&JspURI=%2Fxtts%2Ftt%2Fepost%2Ftrace%2FTrace_list.jsp&postNum=' + this.deliverynum;
							} else if (this.deliveryco == 2) {
								this.href = "http://d2d.ilogen.com/d2d/delivery/invoice_tracesearch_quick.jsp?slipno=" + this.deliverynum;
							} else if (this.deliveryco == 3) {
								this.href = 'http://www.hlc.co.kr/hydex/jsp/tracking/trackingViewCus.jsp?InvNo=' + this.deliverynum;
							} else if (this.deliveryco == 5) {
								this.href = 'http://nplus.doortodoor.co.kr/web/detail.jsp?slipno=' + this.deliverynum;
							} else {
								this.href = "";
							}

						}
					});
				    //console.log(list);


					$scope.order_list = list;

				} else {
					alert(r.message);
				}
			});
		}
        
		$scope.getData();

		$scope.getCommitmentList();

		$scope.getPaymentList();

		$scope.getOrderList();

		// 후원어린이가 없는 경우
		$scope.item = $.parseJSON($("#data").val());
		$scope.item.birthdate = new Date($scope.item.birthdate);

		$scope.showChildPop = function ($event, item) {
			loading.show();
			if ($event) $event.preventDefault();
			popup.init($scope, "/common/child/pop/" + item.countrycode + "/" + item.childmasterid + "/" + item.childkey, function (modal) {

				modal.show();

				initChildPop($http, $scope, modal, item);

			}, { top: 0, iscroll: true, removeWhenClose: true });
		}

		$scope.showAlbum = function ($event, item) {
			console.log(item)
			loading.show();
			$event.preventDefault();
			popup.init($scope, "/common/child/album/" + item.childmasterid, function (modal) {
				modal.show();
				activateAlbum($scope, item.childkey, modal);
			}, { removeWhenClose: true });
		}

		$scope.modal = {
		    instance: null,
		    payments: [],
		    paymentsByMonth: [],
		    totalPayment: 0,
		    years: [],

		    init: function () {

		        for (var i = 0 ; i < 3 ; i++) {
		            this.years.push(new Date().getFullYear() - i);
		        }
		        popup.init($scope, "/my/sponsor/commitment/view", function (modal) {
		            $scope.modal.instance = modal;

		        }, { top: 0, iscroll: true });
		    },

		    show: function (item) {

		        if (!$scope.modal.instance)
		            return;



		        $scope.entity = item;
		        $scope.entity.birthdate = new Date($scope.entity.birthdate);
		        $scope.entity.ptd = new Date($scope.entity.ptd);
		        if ($scope.entity.ptd.getFullYear() < 2000) {
		            $scope.entity.ptd = null;
		        }

		        $scope.selectedYear = new Date().getFullYear();
		        $("#view_year").val("number:" + $scope.selectedYear);

		        $scope.modal.instance.show();

		        this.changeYear(item.fundingfrequency == '1회' ? "" : $scope.selectedYear);

		        setTimeout(function () {
		            $(".custom_sel").selectbox({});
		        }, 300)

		    },

		    hide: function ($event) {
		        $event.preventDefault();
		        if (!$scope.modal.instance)
		            return;
		        $scope.modal.instance.hide();

		    },

		    changeYear: function (val) {
		        $scope.modal.payments = [];
		        $scope.modal.paymentsByMonth = [];
		        $scope.modal.totalPayment = 0;
		        // 월별결제내역
		        $http.get("/api/my/payment.ashx?t=get-monthly-history-by-commitmentId", { params: { c: $scope.entity.commitmentid, year: val } }).success(function (r) {
		            console.log(r);
		            if (r.success) {

		                $scope.modal.payments = r.data;

		                // 정기는 월별로 데이타세팅
		                if ($scope.entity.fundingfrequency != '1회') {

		                    $scope.modal.paymentsByMonth = [];
		                    for (var i = 0; i < 12; i++) {
		                        $scope.modal.paymentsByMonth[i] = "-";

		                        $.each(r.data, function () {
		                            if (parseInt(this.paymentdate) == (i + 1)) {

		                                $scope.modal.totalPayment += this.totalamount;
		                                $scope.modal.paymentsByMonth[i] = this.totalamount.format() + " 원";
		                            }
		                        });
		                    }

		                }

		            } else {
		                alert(r.message);
		            }
		        });
		    }
		};
		$scope.modal.init();
        

		$scope.showDetail = function ($event, item) {
		    // childmasterid : 0000000000
		    console.log(item);
		    $event.preventDefault();
		    $scope.modal.show(item);


		}

		$scope.storeModal = {
		    instance: null,
		    banks: [],
		    detail_list: [],

		    init: function () {
		        popup.init($scope, "/my/store/order/detail-order-main", function (storeModal) {
		            $scope.storeModal.instance = storeModal;
		            //$scope.storeModal.show();
		        }, { top: 0, iscroll: true });
		    },

		    show: function ($event,orderNo) {
		    	console.log(orderNo)
		    	$event.preventDefault();
		        if (!$scope.storeModal.instance)
		            return;

		        $scope.storeModal.instance.show();

		        // 상세 주문 내역 

		        $http.get("/api/store.ashx?t=order_detail_list&orderno=" + orderNo, {}).success(function (r) {
		            //$http.get("/api/store.ashx?t=order_detail_list", { orderno : orderNo }).success(function (r) {

		            if (r.success) {

		                var list = r.data;

		                $.each(list, function () {
		                    this.dtreg = new Date(this.dtreg);
		                    if (this.childname == "") {
		                        this.product_type = "일반상품";
		                    } else {
		                        this.product_type = "어린이에게<br> 보내는 선물";
		                        this.childname = "선물받을 어린이 : " + this.childname;
		                    }

		                });
		                $scope.storeModal.detail_list = list;
		            } else {
		                alert(r.message);
		            }
		        });
		    },
		    close: function ($event) {
		        $event.preventDefault();
		        if (!$scope.storeModal.instance)
		            return;
		        $scope.storeModal.instance.hide();

		    }

		};

		$scope.storeModal.init();

		$scope.goView = function (idx, $event) {
		    location.href = "/store/item/" + idx;
		    $event.preventDefault();

		}

		$scope.showChildDetail = function ($event, item) {
		    $http.get("/api/tcpt.ashx?t=get&childMasterId=" + item.childmasterid).success(function (r) {

		        if (r.success) {
		            if ($event) $event.preventDefault();
		            loading.show();
		            var childinfo = r.data;
		            childinfo.birthdate = new Date(r.data.birthdate);

		            popup.init($scope, "/common/child/pop/" + childinfo.countrycode + "/" + childinfo.childmasterid + "/" + childinfo.childkey + "?fn=hide" , function (modalDetail) {
		                modalDetail.show();

		                initChildPop($http, $scope, modalDetail, childinfo);

		            }, { top: 0, iscroll: true, removeWhenClose: true });
		        }
		    })
		}

		// 편지쓰기
		$scope.goLetter = function ($event, item) {

			$event.preventDefault();
			if (!item.paid) {
			    //alert("첫 후원금 납부 후 어린이에게 편지를 보내실 수 있습니다.\n납부 관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr ");
			    if (confirm("첫 후원금 납부 후 어린이에게 편지를 보내실 수 있습니다.\n납부를 진행하시겠습니까?")) {
			        $http.get("/sponsor/pay-gateway.ashx?t=go-cdsp-letter", { params: { childMasterId: item.childmasterid } }).success(function (r) {

			            if (r.success) {
			                //alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
			                location.href = r.data;
			            } else {
			                if (r.action == "nonpayment") {		// 미납금

			                    if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
			                        location.href = "/my/sponsor/pay-delay/";
			                    }

			                } else {
			                    alert(r.message);
			                }
			            }

			        });
			    } else {
			        return;
			    }
			}

			if (!item.canletter) {
				alert("후원자님께서는 어린이와 편지를 주고 받지 않으시는 머니 후원 중이십니다. 감사합니다.");
				return;
			}
			location.href = "/my/letter/write?c=" + item.childmasterid;

		}

		// 선물금
		$scope.goGift = function ($event, item) {

			$event.preventDefault();
			if (!item.paid) {
				//alert("첫 후원금 납부 후 어린이에게 선물금을 보내실 수 있습니다.\n납부 관련 문의 : 후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr ");
			    if (confirm("첫 후원금 납부 후 어린이에게 선물금을 보내실 수 있습니다.\n납부를 진행하시겠습니까?")) {

			        $http.get("/sponsor/pay-gateway.ashx?t=go-cdsp-letter", { params: { childMasterId: item.childmasterid } }).success(function (r) {

			            if (r.success) {
			                // alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
			                location.href = r.data;
			            } else {
			                if (r.action == "nonpayment") {		// 미납금

			                    if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
			                        location.href = "/my/sponsor/pay-delay/";
			                    }

			                } else {
			                    alert(r.message);
			                }
			            }

			        });


			    } else {
			        return;
			    }
			}

			if (!item.cangift) {
				alert("편지후원자의 경우는 선물금보내기 기능이 제한됩니다.");
				return;
			}
			location.href = "/my/sponsor/gift-money/pay/?t=temporary&childMasterId=" + item.childmasterid;

		}

		// 어린이 이미지
		$scope.getChildImage = function (item) {

			$http.get("/api/tcpt.ashx?t=get-child-image", { params: { childMasterId: item.childmasterid, childKey: item.childkey } }).success(function (r) {
				console.log("어린이 이미지" , r);
				if (r.success) {

					item.pic = r.data;
					

				}
			});

		}


	});

})();