﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="visiontrip.aspx.cs" Inherits="my_activity_visiontrip" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <style type="text/css"> 
        .pd20{padding:20px 0px !important;}
    </style>
    <script type="text/javascript" src="/my/activity/visiontrip.js?v=1.0"></script>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript">

        function printWindowOpen() {
            var wopt = 'menubar=no,toolbar=no,location=no,scrollbars=yes,status=yes,resizable=yes,width=750,height=860';
            winResult = window.open('about:blank', '', wopt);
            winResult.document.open('text/html', 'replace');
            winResult.document.write(document.getElementById("hdnPrintContents").value);
            winResult.document.close();
            return false;
        }
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <asp:HiddenField runat="server" ID="hdnFileRoot" />
        <asp:HiddenField runat="server" ID="hdnPrintContents" />
        <!-- 타이틀 -->
        <div class="page_tit" ng-if="AdminCK == false">
            <div class="titArea">
                <h1>마이컴패션</h1>
                <span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>
                
				<uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents mypage">

            <div class="w980 activity">

                <uc:menu runat="server" />

                <!-- content -->
                <div class="box_type4 mb40">

                    <!-- 서브타이틀 -->
                    <div class="sub_tit mb30">
                        <p class="tit">현지방문 관리</p>
                    </div>
                    <!--// -->
                    <div class="tab_info fr">
                        <span class="s_con8">2018년 진행되는 트립부터 확인이 가능합니다. </span>
                    </div>
                    <ul class="tab_type1 trip_list font15">
                        <li style="width: 33%" class="on" ng-click="getVisionTripList()"><a href="javascript:void(0);">나의 비전트립</a></li>
                        <li style="width: 33%" ng-click="getIndividualTripList()"><a href="javascript:void(0);">나의 개인방문</a></li>
                        <li style="width: 34%" ng-click="getTripHistoryList()"><a href="javascript:void(0);">다녀온 트립/체험확인서</a></li>
                    </ul>

                    <script type="text/javascript">
                        $(function () {
                            $(".tab_type1.trip_list > li").click(function () {
                                var idx = $(this).index() + 1;
                                $(".tab_type1.trip_list > li").removeClass("on");
                                $(this).addClass("on");
                                $(".tc").hide();
                                $(".tc" + idx).show();

                                return false;
                            });
                        })
                    </script>
                    
                    <!-- 나의 비전트립 목록 tableWrap1-->
                    <style>
                        .btnblue { background-color: #005dab; color:#ffffff; }
                        .btnorange { background-color: rgb(244, 180, 56) !important; color:#ffffff; }
                        .btnlightgray { background-color: rgba(118, 118, 118, .5) !important; color:#ffffff; }
                    </style>
                    <div class="tab_contents">
                        <div class="mb30 tc tc1">
                            <table class="tbl_type12 mb30">
                                <caption>나의 비전트립 목록</caption>
                                <%--<colgroup>
								<col style="width:17%" />
								<col style="width:35%" />
								<col style="width:16%" />
								<col style="width:16%" />
								<col style="width:16%" />
							</colgroup>--%>
                                <thead>
                                    <tr>
                                        <%--<th scope="col" rowspan="2">비전트립<br />
                                            신청현황</th>--%>
                                        <th scope="col" rowspan="2">트립일정</th>
                                        <th scope="col" rowspan="2">방문국가</th>
                                        <th scope="col" rowspan="2">온라인<br />
                                            신청서</th>
                                        <th scope="col" rowspan="2">여권사본</th>
                                        <th scope="col" rowspan="2">기타서류</th>
                                        <th scope="col" rowspan="2">신청비<br />
                                            납부현황</th>
                                        <th class="border-right" scope="col" colspan="2">참가비용<span style="font-size:12px;">(트립비용-신청비)</span></th>
                                        <th class="" scope="col" colspan="2">후원어린이만남</th> 
                                        <%--<th class="border-none" scope="col" rowspan="2">트립안내</th>--%>
                                    </tr>
                                    <tr> 
                                        <th class="border-right" scope="col">참가비용</th>
                                        <th class="border-right" scope="col">납부현황</th>
                                        <th class="border-right" scope="col">만남비용</th>
                                        <th class="" scope="col">납부현황</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in vtlist">
                                        <%--<td>{{item.apply_state_display }}</td>--%>
                                        <td>{{item.s_date | date:'yy.MM.dd'}} ~ {{item.e_date | date:'yy.MM.dd'}}</td>
                                        <td>{{item.visit_country }}</td>
                                        <td><a ng-click="tripApplyModal.init($event, item, 'plan', item.apply_id)" class="btn_type8 btnblue">확인</a></td>

                                        <td><a ng-click="tripAttachModal.show($event, item, 'passport')" class="btn_type8 {{item.passprot_name == null ? 'btnorange' : 'btnlightgray'}}" ng-show="item.tripnotice_view && AdminCK == false">{{item.passprot_name == null ? '첨부하기' : '첨부완료'}}</a></td>
                                        <td><a ng-click="tripAttachModal.show($event, item, 'etc')" class="btn_type8 {{item.etc_name == null ? 'btnorange' : 'btnlightgray'}}" ng-show="item.tripnotice_view && AdminCK == false">{{item.etc_name == null ? '첨부하기' : '첨부완료'}}</a></td>

                                        <td><%--"paymentModal.show($event, 'r', item);"--%>
                                            <a ng-click="payment(item.apply_id, item.plandetail_id, 'R')" class="btn_type8 {{item.virtualaccountpaymentcount == 0 ? 'btnorange' : 'btnlightgray'}}" ng-hide="item.virtualaccountpaymentcount > 0 || AdminCK == true">{{item.virtualaccountpaymentcount > 0 ? '납부완료' : '납부하기'}}</a>
                                            <span ng-show="item.virtualaccountpaymentcount > 0">{{item.request_pay_yn}}</span>
                                        </td> 
                                        <td class="border-right">{{item.trip_cost}}</td>
                                        <td class="border-right">
                                            <a ng-click="payment(item.apply_id, item.plandetail_id, 'T')" class="btn_type8 {{item.virtualaccountpaymentcountt == 0 ? 'btnorange' : 'btnlightgray'}}" ng-show="item.item.trip_cost != '0' && AdminCK == false">{{item.virtualaccountpaymentcountt > 0 ? '납부완료' : '납부하기'}}</a>
                                            <%--<span ng-hide="item.trip_pay_yn == 'N'">{{item.trip_pay_yn }}</span>--%>
                                           
                                        </td>
                                        <td class="border-right">{{item.childmeet_cost}}</td>
                                        <td>
                                            <a ng-click="payment(item.apply_id, item.plandetail_id, 'C')" class="btn_type8 {{item.virtualaccountpaymentcountc == 0 ? 'btnorange' : 'btnlightgray'}}" ng-show="item.childmeet_cost != '0' && AdminCK == false">{{item.virtualaccountpaymentcountc > 0 ? '납부완료' : '납부하기'}}</a>
                                            <%--<span ng-hide="item.childmeet_pay_yn == 'N'">{{item.childmeet_pay_yn }}</span>--%>
                                            
                                        </td>

                                        <%--<td class="border-none"><a ng-click="tripNoticeModal.show($event, item, 'p')" class="btn_type8" ng-show="item.tripnotice_view">보기</a></td>--%>
                                    </tr>
                                    <tr ng-hide="vtlist.length">
                                        <td colspan="10" class="border-none">데이터가 없습니다.</td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- page navigation -->
                            <div class="tac mb30">
                                <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getVisionTripList({page : page})"></paging>
                            </div>
                            <!--// page navigation -->

                            <div class="mb30">
                                <div style="margin: 0 auto; text-align: center;">
                                    <a class="btn_s_type1" ng-href="/participation/visiontrip/schedule/" ng-if="AdminCK == false">비전트립 신청 바로가기</a>
                                </div>
                            </div>
                            <div class="box_type5 pl40">
                                <ul>
                                    <li><span class="s_con2">신청비, 참가비용, 후원어린이 만남 비용은 각 각 납부해 주셔야 합니다. </span></li>
                                    <li><span class="s_con2">신청서류, 여권사본 제출 및 신청비를 입금하셔야 신청이 완료됩니다. </span></li>
                                    <li><span class="s_con2">여권사본 및 기타서류 첨부가 어려운 경우, FAX(02-3668-3501) 또는 이메일(visiontrip@compassion.or.kr)로 제출해 주세요.</span></li>
                                    <li><span class="s_con2">기타서류는 해당자에 한합니다(미성년자, 한국 외 국적자 등).</span></li>
                                </ul>
                            </div>
                        </div>


                        <div class="mb30 tc tc2" style="display: none;">
                            <table class="tbl_type12 mb30">
                                <caption>나의 개인방문 신청 목록</caption>
                                <colgroup>
								<%--<col style="width:10%" />--%>
								<col style="width:15%" />
								<col style="width:15%" />
								<col style="width:40%" />
								<col style="width:15%" />
								<col style="width:15%" />
							</colgroup>
                                <thead>
                                    <tr>
                                        <%--<th scope="col" class="pd20">개인방문<br />
                                            신청현황</th>--%>
                                        <th scope="col" class="pd20">방문일정</th>
                                        <th scope="col" class="pd20">방문국가</th>
                                        <th scope="col" class="pd20">후원어린이<br />
                                            정보</th> 
                                        <th scope="col" class="pd20">온라인<br />
                                            신청서확인</th>
                                        <th scope="col" class="border-none pd20">방문안내</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in ilist">
                                        <%--<td>신청완료</td>--%>
                                        <td>{{item.visit_date | date:'yy.MM.dd'}} </td>
                                        <td>{{item.visit_country }}</td>
                                        <td><div ng-bind-html="item.child_key"></div>
                                            <%--<span>{{item.child_key}}</span>--%>
                                        </td> 
                                        <td><a ng-click="tripApplyModal.init($event, item, 'individual', item.apply_id)" class="btn_type8">신청서</a></td>
                                        <td class="border-none"><a ng-click="tripNoticeModal.show($event ,item, 'i')" class="btn_type8" ng-show="item.tripnotice_view">보기</a></td>
                                    </tr>
                                    <tr ng-hide="ilist.length">
                                        <td colspan="6" class="border-none">데이터가 없습니다.</td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- page navigation -->
                            <div class="tac mb30">
                                <paging class="small" page="i_page" page-size="i_rowsPerPage" total="i_total" show-prev-next="true" show-first-last="true" paging-action="getIndividualTripList({page : page})"></paging>
                            </div>
                            <!--// page navigation -->

                            <div class="mb30" ng-if="AdminCK == false">
                                <div style="margin: 0 auto; text-align: center;">
                                    <a class="btn_s_type1" ng-href="/participation/visiontrip/form/apply_individual/">개인방문 신청 바로가기</a>
                                </div>
                            </div>
                            <div class="box_type5 pl40">
                                <ul>
                                    <li><span class="s_con2">개인방문 신청서 접수 후, 현지를 통하여 방문 일정 및 예상비용을 확인하여 후원자님께 안내 드립니다.</span></li>
                                    <li><span class="s_con2">개인방문과 관련하여 발생한 비용은 현지 직원의 안내에 따라 현지에서 현지 화폐로 직접 지불합니다.</span></li>
                                    <li><span class="s_con2">기타서류 첨부가 어려운 경우, FAX(02-3668-3501) 또는 이메일(visiontrip@compassion.or.kr)로 제출해 주세요.</span></li>
                                </ul>
                            </div>
                        </div>


                        <div class="mb30 tc tc3" style="display: none;">
                            <div class="mt10 mb10">
                                <%--  <div class="fr">
                                    <a href="javascript:void(0);" id="main_recmd1_link" class="btn_s_type4" ng-click="getTripHistoryList({ page : 1, tripType : 'Plan' })">비전트립</a>
                                    <a href="javascript:void(0);" class="btn_s_type4" ng-click="getTripHistoryList({ page : 1, tripType : 'Individual' })">개인방문</a> 
                                </div>--%>


                                <span class="sel_type2" style="width: 173px;">
                                    <label for="ddlVisitCountry" class="hidden">신청서 구분</label>

                                    <asp:DropDownList runat="server" ID="ddlApplyType" class="custom_sel" Width="200">
                                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="비전트립" Value="Plan"></asp:ListItem>
                                        <asp:ListItem Text="개인방문" Value="Individual"></asp:ListItem>
                                    </asp:DropDownList>
                                </span>

                            </div>
                            <table class="tbl_type12 mb30">
                                <caption>다녀온 트립/체험확인서 목록</caption>
                                <thead>
                                    <tr>
                                        <th scope="col" class="pd20">구분</th>
                                        <th scope="col" class="pd20">트립일정</th>
                                        <th scope="col" class="pd20">방문국가</th>
                                        <th scope="col" class="pd20">체험확인서</th>
                                        <th scope="col" class="border-none pd20">후기보러가기</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in hlist" class="item.apply_type">
                                        <td>{{item.apply_type == 'Plan'? '비전트립' : '개인방문'}}</td>
                                        <td><span ng-show="item.is_plan">{{item.s_date | date:'yy.MM.dd'}} ~ {{item.e_date | date:'yy.MM.dd'}} </span>
                                            <span ng-hide="item.is_plan">{{item.visit_date | date:'yy.MM.dd'}}</span></td>
                                        <td>{{item.visit_country }}</td> <%--ng-click="popupOpen()" --%>
                                        <td><a class="btn_type8" ng-show="item.print_view" runat="server" onserverclick="btnExportPDF_Click" ng-click="setPrintValue(item.apply_id,  item.apply_type)">보기
                                            </a></td>
                                        <td class="border-none"><a ng-href="{{item.trip_review}}" target="_blank" class="btn_type8" ng-show="item.trip_review">보기</a></td>
                                    </tr>
                                    <tr ng-hide="hlist.length">
                                        <td colspan="5" class="border-none">데이터가 없습니다.</td>
                                    </tr>
                                </tbody>
                            </table>
                                            <asp:HiddenField runat="server" ID="hdnTripID" />
                                            <asp:HiddenField runat="server" ID="hdnTripType" />
                            <!-- page navigation -->
                            <div class="tac mb30">
                                <paging class="small" page="h_page" page-size="h_rowsPerPage" total="h_total" show-prev-next="true" show-first-last="true" paging-action="getTripHistoryList({ page : page })"></paging>
                            </div>
                            <!--// page navigation -->

                            <div class="box_type5 pl40">
                                <ul>
                                    <li><span class="s_con2">컴패션비전트립은 컴패션의 양육 현장을 방문하여 컴패션 사역을 이해하고 비전을 확인하며 현지를 체험하는 일정으로 봉사활동은 진행되지 않습니다. VMS, 1365 등 자원봉사포털을 통해 발급되는 봉사확인서가 아님을 참고해주시기 바랍니다.</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//   --> 

                    

                    

                </div>
                <!--// content -->


            </div>

            <div class="h100"></div>
        </div>
        <!--// e: sub contents -->




    </section>

</asp:Content>

