﻿$(function () {


});

(function () {

    var app = angular.module('cps.page', []);

    app.directive('onFinishRender', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                //if (scope.$first === true) {
                //    window.alert('First thing about to render');
                //}
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit(attr.onFinishRender);
                    }, 1);
                }
            }
        };
    });

    app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

        //나의 비전트립
        $scope.total = 0;
        $scope.page = 1;
        $scope.rowsPerPage = 10;

        $scope.params = {
            page: $scope.page,
            rowsPerPage: $scope.rowsPerPage
        };


        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function () {

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();

        $scope.getVisionTripList = function (params) {
            $scope.params = $.extend($scope.params, params);
            $http.get("/api/visiontrip.ashx?t=my_visiontriplist", { params: $scope.params }).success(function (r) {
                console.log(r);
                if (r.success) {
                    //var list = $.parseJSON(r.data);
                    var list = r.data;

                    if (list.length > 0) {
                        $.each(list, function () {
                            //this.regdate = new Date(this.regdate); 
                            this.apply_state_display = this.apply_state == "A" ? "신청중" : "신청완료";

                            this.request_pay_yn = this.r_cost_yn != "Y" ? "N" : $scope.numberWithCommas(this.request_cost);  

                            if (this.trip_cost == null) {
                                this.trip_pay_yn = "";
                                this.trip_cost = "추후안내";
                            } else {
                                if (this.trip_cost == "0" || this.trip_cost == "")
                                    this.trip_pay_yn = "";
                                else
                                this.trip_pay_yn = this.t_cost_yn != "Y" ? "N" : $scope.numberWithCommas(this.trip_cost);
                                this.trip_cost = $scope.numberWithCommas(this.trip_cost);
                            }

                            if (this.childmeet_cost == null) {
                                this.childmeet_pay_yn = "";
                                this.childmeet_cost = "추후안내";
                            } else {
                                if (this.childmeet_cost == "0" || this.childmeet_cost == "")
                                    this.childmeet_pay_yn = "";
                                else
                                    this.childmeet_pay_yn = this.cm_cost_yn != "Y" ? "N" : this.childmeet_cost;
                                this.childmeet_cost = $scope.numberWithCommas(this.childmeet_cost);
                            }

                            this.tripnotice_view = this.trip_notice != "" || this.trip_notice_detail != "" ? true : false;
                        });
                    }
                    $scope.vtlist = list;
                    $scope.total = r.data.length > 0 ? r.data[0].total : 0;
                } else {
                    alert(r.message);
                }
            });
        }
        $scope.numberWithCommas = function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }


        $scope.getVisionTripList();


        //나의 개인방문
        $scope.i_total = 0;
        $scope.i_page = 1;
        $scope.i_rowsPerPage = 5;

        $scope.i_params = {
            page: $scope.i_page,
            rowsPerPage: $scope.i_rowsPerPage
        };

        $scope.getIndividualTripList = function (params) {
            $scope.i_params = $.extend($scope.i_params, params);
            $http.get("/api/visiontrip.ashx?t=my_individualtriplist", { params: $scope.i_params }).success(function (r) {
                console.log(r);
                if (r.success) {
                    //var list = $.parseJSON(r.data);
                    var list = r.data;

                    if (list.length > 0) {
                        $.each(list, function () {
                            this.visit_date = new Date(this.visit_date);
                            this.tripnotice_view = this.tripnotice != "" ? true : false;
                            this.child_name = this.child_name; 
                            this.child_key = this.child_key != null ? this.child_key.replace(/, /g, ', <br/>') : '';
                        });

                    }
                    $scope.ilist = list;
                    $scope.i_total = r.data.length > 0 ? r.data[0].total : 0;
                } else {
                    alert(r.message);
                }
            });
        }
        // end - 나의 개인방문


        //다녀온 트립 체험확인서
        $scope.h_total = 0;
        $scope.h_page = 1;
        $scope.h_rowsPerPage = 5;
        $scope.h_type = "";
        $scope.h_params = {
            page: $scope.h_page,
            rowsPerPage: $scope.h_rowsPerPage,
            tripType: $scope.h_type
        };
        $scope.getTripHistoryList = function (params) {
            $scope.h_params = $.extend($scope.h_params, params);
            $http.get("/api/visiontrip.ashx?t=my_historylist", { params: $scope.h_params }).success(function (r) {
                console.log(r);
                if (r.success) {
                    //var list = $.parseJSON(r.data);
                    var list = r.data;

                    if (list.length > 0) {
                        $.each(list, function () {
                            this.is_plan = this.apply_type == "Plan" ? true : false;
                            //this.visit_date = new Date(this.visit_date);
                            //this.tripnotice_view = this.tripnotice != "" ? true : false;

                            if (this.trip_review != null) {
                                if (this.trip_review.length == 7 && this.trip_review.indexOf("http://") == 0) {
                                    this.trip_review = "";
                                } 
                            }

                            this.print_view = false;
                            var today = new Date();
                            this.checkdate = new Date(this.apply_type == "Plan" ? this.s_date.replace(/-/g, '/') : this.visit_date.replace(/-/g, '/'));
                            if (today > this.checkdate)
                                this.print_view = true;
                        });

                    }
                    $scope.hlist = list;
                    $scope.h_total = r.data.length > 0 ? r.data[0].total : 0;
                } else {
                    alert(r.message);
                }
            });
        }
        //end - 다녀온 트립 체험확인서

        $scope.setPrintValue = function(id, type) {

            $("#hdnTripID").val(id);
            $("#hdnTripType").val(type);

        }

        $("#ddlApplyType").change(function () {
            var selectedValue = $("#ddlApplyType option:selected").val();
            $scope.getTripHistoryList({ page: 1, tripType: selectedValue }); 
        });


        //$scope.paymentModal =  {
        //    instance: null,
        //    cost: "",
        //    paymentType: "",
        //    init: function () {
        //        popup.init($scope, "/my/activity/popup/payment_visiontrip", function (paymentModal) {
        //            $scope.paymentModal.instance = paymentModal;
        //            $scope.paymentModal.paymentType = "";
        //            $scope.paymentModal.cost = "";
        //            //$scope.paymentModal.show();
        //        }, {
        //            top: 10, iscroll: true, backgroundClick: 'n'
        //        });
        //    },
        //    show: function ($event, paymentType, item) {
        //        $event.preventDefault();
        //        if (!$scope.paymentModal.instance)
        //            return;
        //        switch(paymentType){
        //            case "r":
        //                $scope.paymentModal.paymentType = "신청비";
        //                $scope.paymentModal.cost = item.request_cost;
        //                break;
        //            case "t":
        //                $scope.paymentModal.paymentType = "참가비용 (트립비용-신청비)";
        //                $scope.paymentModal.cost = item.trip_cost;
        //                break;
        //            case "c":
        //                $scope.paymentModal.paymentType = "후원어린이 만남 비용";
        //                $scope.paymentModal.cost = item.childmeet_cost;
        //                break;
        //        }

        //        //$scope.paymentModal.paymentType = paymentType == "r" ? "신청비" : paymentType == "t" ? "참가비용 (트립비용-신청비)" : paymentType == "c" ? "후원어린이 만남 비용" : "";
        //        //$scope.paymentModal.item = item;

        //        $scope.paymentModal.instance.show(); 
        //    },
        //    close: function ($event) {
        //        $event.preventDefault();
        //        if (!$scope.paymentModal.instance)
        //            return;
        //        $scope.paymentModal.instance.hide();
        //    }
        //};
        //$scope.paymentModal.init();


        $scope.payment = function (applyid, plandetailid, type) {
            location.href = "/my/activity/pay/payment_visiontrip/" + applyid + "/" + plandetailid + "/" + type;

            //$http.post("/my/activity/popup/payment_visiontripl", "applyid=" + applyid);
        };

        //트립안내 popup
        $scope.tripNoticeModal = {
            instance: null,
            init: function () {
                popup.init($scope, "/my/activity/popup/tripnotice", function (tripNoticeModal) {
                    $scope.tripNoticeModal.instance = tripNoticeModal;
                    //$scope.tripnoticeModal.show();
                }, {
                    top: 0, iscroll: true, backgroundClick: 'n'
                });
            },
            show: function ($event, item, type) {
                $event.preventDefault();
                if (!$scope.tripNoticeModal.instance)
                    return;

                $scope.tripNoticeModal.instance.show();

                // 트립안내
                $scope.tripNoticeModal.tripnotice = item.trip_notice;
                $("#divTripNotice").html(item.trip_notice);

                $scope.tripNoticeModal.detailShow = false;
                $scope.tripNoticeModal.tripnoticeDetail = "";

                //if (type == "p") { 
                var noticeDetail = item.trip_notice_detail.replace("http://", "").trim() != "" ? item.trip_notice_detail : "";
                $scope.tripNoticeModal.detailShow = (noticeDetail != "");

                $scope.tripNoticeModal.tripnoticeDetail = item.trip_notice_detail;
                //}
            },
            close: function ($event) {
                $event.preventDefault();
                if (!$scope.tripNoticeModal.instance)
                    return;
                $scope.tripNoticeModal.instance.hide();
            }
        };

        $scope.tripNoticeModal.init();
        //end - 트립안내 popup
        
        //첨부파일 popup
        $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
            if ($scope.tripAttachModal.input_fils.length > 0) {
                var idx = $scope.tripAttachModal.input_fils[$scope.tripAttachModal.input_fils.length - 1].key;
                $scope.tripAttachModal.setFileUploader(idx);
            }
        });

        $scope.tripAttachModal = {
            instance: null,
            banks: [],
            detail_list: [],
            init: function () {
                popup.init($scope, "/my/activity/popup/attach", function (tripAttachModal) {
                    $scope.tripAttachModal.instance = tripAttachModal;
                }, {
                    top: 10, iscroll: true, backgroundClick: 'n'
                });
            },
            show: function ($event, item, type) {
                $event.preventDefault();
                if (!$scope.tripAttachModal.instance)
                    return;

                $scope.tripAttachModal.instance.show();
                $scope.tripAttachModal.item = item; //apply data

                $scope.tripAttachModal.showChangePassport = false; //여권 수정 버튼		        
                $scope.tripAttachModal.showAttachEtc = false;  //기타첨부 추가
                $scope.tripAttachModal.showAttachPassport = false; //여권첨부 

                $scope.tripAttachModal.showPassport = false;
                $scope.tripAttachModal.showEtc = false;
                 
                $scope.tripAttachModal.passportYN = true;

                $scope.tripAttachModal.readOnly = true;
                if ($scope.tripAttachModal.item.apply_state == "A") {
                    //신청중일 경우에만 수정 가능
                    $scope.tripAttachModal.showChangePassport = true;
                    $scope.tripAttachModal.showAttachEtc = true;

                    $scope.tripAttachModal.readOnly = false;
                }

                $scope.tripAttachModal.attach_type = type;

                if (type == "passport") {
                    $scope.tripAttachModal.showPassport = true;
                    $scope.tripAttachModal.showEtc = false;
                    //여권 첨부
                    //if (item.passport_path != "" && !$scope.tripAttachModal.readOnly) {
                    //    $scope.tripAttachModal.showChangePassport = false;
                    //}
                    //else {
                    //    $scope.tripAttachModal.showChangePassport = true;
                    //}passportChangeButton

                    if (item.passport_path == "" || item.passport_path == null) {
                        $scope.tripAttachModal.passportYN = false;
                        $scope.tripAttachModal.showChangePassport = false;
                        $scope.tripAttachModal.showAttachPassport = true;
                    }
                    else {
                        $scope.tripAttachModal.passportYN = true;
                        $scope.tripAttachModal.showChangePassport = true;
                        $scope.tripAttachModal.showAttachPassport = false;
                    }
                    if ($scope.tripAttachModal.readOnly) {
                        $scope.tripAttachModal.showChangePassport = false;
                        $scope.tripAttachModal.showAttachPassport = false;
                    }


                    //첨부파일 - 여권
                    var uploader_passport = $scope.tripAttachModal.attachUploader("btn_passportfile");
                    uploader_passport._settings.data.fileDir = $("#hdnFileRoot").val() + $scope.tripAttachModal.item.schedule_id + "/" + common.getUserId() + "/";
                    uploader_passport._settings.data.limit = 2048;
                }
                else {
                    $scope.tripAttachModal.showChangePassport = false;
                    if (!$scope.tripAttachModal.readOnly)
                        $scope.tripAttachModal.showAttachEtc = true;

                    $scope.tripAttachModal.showPassport = false;
                    $scope.tripAttachModal.showEtc = true;

                    $scope.tripAttachModal.a_params = {
                        apply_id: $scope.tripAttachModal.item.apply_id,
                        attach_type: "etc"
                    };
                    $scope.tripAttachModal.a_params = $.extend($scope.tripAttachModal.a_params, null);
                    $http.get("/api/visiontrip.ashx?t=attachlist", { params: $scope.tripAttachModal.a_params }).success(function (r) {
                        console.log(r);
                        if (r.success) {
                            var list = r.data;
                            $scope.tripAttachModal.etcattach_list = list;
                        } else {
                            console.log(r);
                        }
                    });
                    //기타 파일
                    $scope.tripAttachModal.input_fils = [{
                        key: 0, filename: '', filepath: ''
                    }];
                    $scope.tripAttachModal.newItemNo = 0;
                }
            },
            attachShow: function ($event) {
                //attach passport add - show/hide
                $event.preventDefault();
                if (!$scope.tripAttachModal.instance)
                    return;
                $scope.tripAttachModal.showAttachPassport = true;
            },
            attachUploader: function (button) {
                //$event.preventDefault();
                //if (!$scope.tripAttachModal.instance)
                //    return;

                return new AjaxUpload(button, {
                    action: '/common/handler/upload',
                    responseType: 'json',
                    onChange: function (file) {
                        var fileName = file.toLowerCase();
                    },
                    onSubmit: function (file, ext) {
                        this.disable();
                    },
                    onComplete: function (file, response) {

                        this.enable();

                        console.log(file, response);
                        if (response.success) {
                            $("#path_" + button).val(response.name);
                            $("[data-id=path_" + button + "]").val(response.name.replace(/^.*[\\\/]/, ''));

                            console.log($("#path_" + button).val()); //filepullpath
                            console.log($("[data-id=path_" + button + "]").val());  //filename 

                        } else
                            alert(response.msg);
                    }
                });
            },

            addInputFiles: function () {
                $scope.tripAttachModal.newItemNo++;
                $scope.tripAttachModal.input_fils.push({
                    key: $scope.tripAttachModal.newItemNo, filename: '', filepath: ''
                });
            },
            setFileUploader: function (idx) {
                var uploader_etc = $scope.tripAttachModal.attachUploader("btn_etcfile" + idx);
                uploader_etc._settings.data.fileDir = $("#hdnFileRoot").val() + $scope.tripAttachModal.item.schedule_id + "/" + common.getUserId() + "/";
                uploader_etc._settings.data.rename = "y";
                uploader_etc._settings.data.limit = 2048;
            },
            removeInputFiles: function (index) {
                $scope.tripAttachModal.input_fils.splice(index, 1);
            },
            deleteAttach: function ($event, attachid, index) {
                var param = {
                    attachID: attachid,
                    applyID: $scope.tripAttachModal.item.apply_id
                };

                $http.post("/api/visiontrip.ashx?t=delete_my_attach", param).success(function (r) {
                    if (r.success) {
                        alert("파일 삭제되었습니다.");
                        $scope.tripAttachModal.etcattach_list.splice(index, 1);
                        console.log(r.data);
                    } else {
                        alert(r.message);
                    }

                }).error(function (e) {
                    console.log(e);
                })

            },
            request: function ($event) {
                //console.log($scope.tripAttachModal.attach_type);

                var AttachFile = [];
                if ($scope.tripAttachModal.attach_type == "passport") {
                    if ($("#path_btn_passportfile").val() == "") {
                        alert("선택된 파일이 없습니다.");
                        return;
                    }
                    AttachFile.push({
                        type: "passport", name: $("[data-id=path_btn_passportfile]").val(), path: $("#path_btn_passportfile").val()
                    });
                }
                else {
                    for (var f in $scope.tripAttachModal.input_fils) {
                        if ($("#path_btn_etcfile" + $scope.tripAttachModal.input_fils[f].key).val() != "") {
                            AttachFile.push({
                                type: "etc", name: $("[data-id=path_btn_etcfile" + $scope.tripAttachModal.input_fils[f].key + "]").val(), path: $("#path_btn_etcfile" + $scope.tripAttachModal.input_fils[f].key).val()
                            });
                        }
                    }

                    if (AttachFile.length == 0) {
                        alert("선택된 파일이 없습니다.");
                        return;
                    }
                }
                //console.log(AttachFile);

                var param = {
                    attachType: $scope.tripAttachModal.attach_type,
                    applyID: $scope.tripAttachModal.item.apply_id,
                    attachFile: JSON.stringify(AttachFile)
                };

                $http.post("/api/visiontrip.ashx?t=update_my_attach", param).success(function (r) {
                    if (r.success) {
                        alert("파일 첨부되었습니다.");
                        $scope.tripAttachModal.close($event);
                        $scope.getVisionTripList();
                        console.log(r.data)

                    } else {
                        alert(r.message);
                        $scope.tripAttachModal.close($event);
                    }

                }).error(function (e) {
                    console.log(e);
                })

            },
            close: function ($event) {
                $event.preventDefault();
                if (!$scope.tripAttachModal.instance)
                    return;
                $scope.tripAttachModal.instance.hide();
            }

        };

		$scope.tripAttachModal.init();
        //end 첨부파일 popup


        $scope.download = function (url) {
            //cert_setDomain(); document.domain +
            //location.href = url;
            var openNewWindow = window.open("about:blank");
            openNewWindow.location.href = url;
        },


        //신청서 popup
        $scope.tripApplyModal = {
            instance: null,
            banks: [],
            detail_list: [],
            init: function ($event, item, type, applyId) {
                popup.init($scope, "/my/activity/popup/apply_" + type + "/" + applyId, function (tripApplyModal) {
                    $scope.tripApplyModal.instance = tripApplyModal;
                    $scope.tripApplyModal.show($event, item, applyId);
                }, {
                    top: 10, iscroll: true, backgroundClick: 'n'
                });
            },
            show: function ($event, item, applyId) {
                $event.preventDefault();
                if (!$scope.tripApplyModal.instance)
                    return;
                $scope.tripApplyModal.instance.show();

                $scope.tripApplyModal.item = item;

                //기타 파일 
                $scope.tripApplyModal.params = {
                    apply_id: applyId,
                    attach_type: "etc"
                };
                $scope.tripApplyModal.params = $.extend($scope.tripApplyModal.params, null);
                $http.get("/api/visiontrip.ashx?t=attachlist", { params: $scope.tripApplyModal.params }).success(function (r) {
                    console.log(r);
                    if (r.success) {
                        var list = r.data;
                        $scope.tripApplyModal.etcattach_list = list;
                    } else {
                        console.log(r);
                    }
                });
                //기타 파일 

                //후원어린이 만남   
                $http.get("/api/visiontrip.ashx?t=childmeetlist", { params: { apply_id: applyId } }).success(function (r) {
                    console.log(r);
                    if (r.success) {
                        var list = r.data;
                        $scope.tripApplyModal.childmeet_list = list;
                    } else {
                        console.log(r);
                    }
                });
                // 
                //약관 popup
                $scope.agreementModal = {
                    instance: null,
                    banks: [],
                    detail_list: [],

                    init: function () {
                        popup.init($scope, "/participation/visiontrip/form/term", function (agreementModal) {
                            $scope.agreementModal.instance = agreementModal;
                            //$scope.tripnoticeModal.show();
                        }, { top: 0, iscroll: true });
                    },

                    show: function ($event1, type, title) {
                        //console.log(orderNo)
                        $event1.preventDefault();
                        if (!$scope.agreementModal.instance)
                            return;

                        $scope.agreementModal.instance.show();

                        $scope.agreementModal.title = title;

                        $scope.agreementModal.type1 = type == 1;
                        $scope.agreementModal.type2 = type == 2;
                        $scope.agreementModal.type3 = type == 3;
                        $scope.agreementModal.type4 = type == 4;
                        $scope.agreementModal.type5 = type == 5;
                        $scope.agreementModal.type6 = type == 6;
                        $scope.agreementModal.type7 = type == 7;
                        //$scope.agreementModal.type8 = type == 8;

                        $scope.agreementModal.typei1 = type == 11;
                        $scope.agreementModal.typei2 = type == 12;
                        $scope.agreementModal.typei3 = type == 13;
                        $scope.agreementModal.typei4 = type == 14;
                        $scope.agreementModal.typei5 = type == 15;
                        $scope.agreementModal.typei6 = type == 16;

                    },
                    close: function ($event1) {
                        $event1.preventDefault();
                        if (!$scope.agreementModal.instance)
                            return;
                        $event1.agreementModal.instance.hide();
                    }
                };

                $scope.agreementModal.init();
            },
            close: function ($event) {
                $event.preventDefault();
                if (!$scope.tripApplyModal.instance)
                    return;
                $scope.tripApplyModal.instance.hide();
            }
        };






        //end 신청서 popup
           
        $scope.popupOpen = function(){ 
            var popUrl = "/my/activity/popup/certificate";	//팝업창에 출력될 페이지 URL 
            var popOption = "width=830, height=700, resizable=no, scrollbars=yes, status=no, menubar=no;";    //팝업창 옵션(optoin) 
            window.open(popUrl,"",popOption); 
        }

    });

})();


$page = {
    payment: function () { 
    
    } 
};

