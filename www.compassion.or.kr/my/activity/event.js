﻿$(function () {


});

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

	
		$scope.total = -1;
		$scope.list = null;
	
		$scope.params = {
			page: 1,
			rowsPerPage: 5
		};


		// 이번달 주문 내역 

		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);
			$http.get("/api/my/event.ashx?t=list", { params: $scope.params }).success(function (r) {
				

				if (r.success) {
					var list = r.data;
					
					$.each(list, function () {
						this.e_close = new Date(this.e_close); // 마감일
						this.e_announce = this.e_announce ? new Date(this.e_announce) : "-"; // 발표일 
						this.er_regdate = new Date(this.er_regdate); // 등록일 
						//this.state = new Date(this.e_closed).getTime() < new Date().getTime() ? "종료" : "진행중";
					    //this.state = this.event_closed ? "종료" : "진행중";
						this.state = "신청완료";


						if (this.e_announce != "-" && this.e_announce < new Date()) {
						    this.winner = this.ew_id != null ? "당첨" : "미당첨"

						} else {
						    this.winner = "-";
						}
					});

					console.log(list);

					$scope.list = list;
					$scope.total = r.data.length > 0 ? r.data[0].total : 0;

				} else {
					alert(r.message);
				}
			});
		}

		$scope.getList();

	});

})();