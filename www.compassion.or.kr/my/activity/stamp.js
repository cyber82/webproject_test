﻿$(function () {


});

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

	
		$scope.years = null;
		$scope.year = (new Date).getFullYear() + '';
		$scope.online_list = null;
		$scope.offline_list = null;

        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function () {

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();

		// 년도정보
		$scope.getYears = function () {

			$scope.online_list = null;
			$scope.offline_list = null;

			$http.get("/api/my/stamp.ashx?t=years", { params: [] }).success(function (r) {

				if (r.success) {
					var list = r.data;
					$scope.years = list;

					setTimeout(function () {
						$(".custom_sel").selectbox("detach");
						$(".custom_sel").selectbox();
					},500)
					

					//console.log(list);

				} else {
					alert(r.message);
				}
			});
		}

		// 리스트
		$scope.getList = function () {

			$scope.online_list = null;
			$scope.offline_list = null;

			$http.get("/api/my/stamp.ashx?t=list", { params: {year : $scope.year} }).success(function (r) {

				if (r.success) {
					var list = r.data;
					
					$.each(list, function () {
						if (this.su_regdate)
						    this.su_regdate = new Date(this.su_regdate);

						if (this.is_earning == "Y") {
						    this.addClass = "on";
						}

						if (this.s_id == "store") {
						    this.url = "/store/";
						    this.do = "스토어 상품 보기";
						}

						if (this.s_id == "ufj") {
						    this.url = "/sponsor/user-funding/";
						    this.do = "나눔펀딩 만나기";
						}

						if (this.s_id == "ufc") {
						    this.url = "/sponsor/user-funding/create/";
						    this.do = "나눔펀딩 만들기";
						}
						if (this.s_id == "sf") {
						    this.url = "/sponsor/special/";
						    this.do = "특별한 나눔 보기";
						}
						if (this.s_id == "cad") {
						    this.url = "/my/activity/cad/";
						    this.do = "CAD 추천가기";
						}
						if (this.s_id == "mail") {
						    this.url = "/my/letter/write/";
						    this.do = "편지쓰러 가기";
						}
						if (this.s_id == "cdsp") {
						    this.url = "/sponsor/children/";
						    this.do = "결연하러 가기";
						}

					});

					$scope.online_list = $.grep(list, function (r) {
						return r.is_online == 'Y';
					})

					$scope.offline_list = $.grep(list, function (r) {
						return r.is_online == 'N';
					})

					

					$.each($scope.offline_list, function () {
					    if (this.is_earning == "Y") {
					        $scope.view = true;
					    }
					})
					//console.log($scope.view);
					//console.log(list);
				} else {
					alert(r.message);
				}
			});
		}

		$scope.changeYear = function (year) {
			$scope.year = year;

			$scope.getList();
		}

		$scope.getYears();

		$scope.getList();

	});

})();