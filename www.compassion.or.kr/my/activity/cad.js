﻿$(function () {


});

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

	
		$scope.total = -1;
		$scope.list = null;
	
		$scope.params = {
			page: 1,
			rowsPerPage: 5
		};

        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function () {

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();

		// 이번달 주문 내역 
		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);
			$http.get("/api/cad.ashx?t=list", { params: $scope.params }).success(function (r) {
				console.log(r);
				if (r.success) {
					//var list = $.parseJSON(r.data);
					var list = r.data;
					
					if (list.length > 0) {
						$.each(list, function () {
							this.regdate = new Date(this.regdate);
							this.startdate = new Date(this.startdate);
							this.birthdate = new Date(this.birthdate);

						});

					} 
					$scope.list = list;
					$scope.total = r.data.length > 0 ? r.data[0].total : 0;
				} else {
					alert(r.message);
				}
			});
		}

		$scope.getList();

	});

})();