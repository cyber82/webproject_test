﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using System.IO;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser; 

using System.Text;
using System.Text.RegularExpressions;
using System.Net; 

public partial class my_activity_visiontrip : FrontBasePage
{ 
    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        hdnFileRoot.Value = Uploader.GetRoot(Uploader.FileGroup.file_visiontrip) + "plan/";
    }
    
    private string CreateReceiptHtml()
    {
        UserInfo sess = new UserInfo();
        string strUserID = sess.UserId;
        string strSponserID = sess.SponsorID; 

        int iApplyID = Convert.ToInt32(hdnTripID.Value); 

        string strName = string.Empty, strNameEng = string.Empty, strBirth = string.Empty, 
            strPhone = string.Empty, strLocation = string.Empty, strLocationEng = string.Empty,
            strStartdate = string.Empty, strEndDate = string.Empty, strVisitDate = string.Empty, 
            strType = string.Empty;
        
        string strDate = string.Empty;
        string strTime = strType == "Plan" ? "40 " : "6";

        strName = sess.UserName;
        strBirth = sess.Birth != "" ? sess.Birth.Substring(0, 10) : "";
        strType = hdnTripType.Value; 
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var applyData = dao.tVisionTripApply.First(p => p.ApplyID == iApplyID && p.UserID == strUserID && p.CurrentUse == 'Y');
            var applyData = www6.selectQF<CommonLib.tVisionTripApply>("ApplyID", iApplyID, "UserID", strUserID, "CurrentUse", "Y");
            strNameEng = applyData.SponsorNameEng;
            strPhone = applyData.Tel.Decrypt(); 

            if(strType == "Plan")
            {
                //var scheduleData = dao.tVisionTripSchedule.FirstOrDefault(p => p.ScheduleID == applyData.ScheduleID && p.CurrentUse == 'Y');
                var scheduleData = www6.selectQF<CommonLib.tVisionTripSchedule>("ScheduleID", applyData.ScheduleID, "CurrentUse", "Y");
                strLocation = scheduleData.VisitCountry;
                strLocationEng = scheduleData.VisitCountryEng;
                strStartdate = scheduleData.StartDate;
                strEndDate = scheduleData.EndDate;

                strDate = strStartdate + " ~ " + strEndDate;
            }
            else
            {
                //ar applyDetailData = dao.tVisionTripIndividualDetail.FirstOrDefault(p => p.ApplyID == iApplyID);
                var applyDetailData = www6.selectQF<CommonLib.tVisionTripIndividualDetail>("ApplyID", iApplyID);
                strVisitDate = applyDetailData.VisitDate1;
                strLocationEng = applyDetailData.VisitCountryEng;

                //var coutry = dao.country.FirstOrDefault(p => p.c_id == applyDetailData.VisitCountry.ToString());
                var coutry = www6.selectQF<CommonLib.country>("c_id", applyDetailData.VisitCountry.ToString());
                strLocation = coutry.c_name.ToString();

                strDate = strVisitDate;
            }
        } 



        string sHtml = string.Empty;  

        #region Html
        sHtml += "<!DOCTYPE html> ";
        sHtml += "<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>";
        sHtml += "<head>";
        sHtml += "    <meta charset='utf-8' /> ";

        //var interfaceProps = new Dictionary<string, Object>();
        //var ih = new ImageHander() { BaseUri = "http://www.compassionkr.com/common/img/system/compassion_logo.gif" };  
        //interfaceProps.Add(HTMLWorker.IMG_PROVIDER, ih); 
        //var ih2 = new ImageHander() { BaseUri = "http://www.compassionkr.com/images/visiontrip/img002.jpg" };

        var title = new ImageHander() { BaseUri = Server.MapPath("~/common/img/system/visiontrip_title.PNG") };
        var sign = new ImageHander() { BaseUri = Server.MapPath("~/common/img/system/visiontrip_sign.PNG") };

        sHtml += "    <title></title>";
        sHtml += "</head>";
        sHtml += "<body style='font-family: 맑은 고딕;'>";
        sHtml += "    <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>";
        //sHtml += "        <tr>";
        //sHtml += "            <td><img src='"+ ih.BaseUri + "' height='100' /></td>";
        //sHtml += "        </tr> "; 
        //sHtml += "        <tr><td height='80' align='center' class='title' style='width:100%;position:absolute;top:50px;text-align:center;'><span style='font-size: 19pt; font-weight:bold;'>체 험 확 인 서</span></td></tr>";
        //sHtml += "        <tr><td height='80' align='center'  style='width:100%;position:absolute;top:80px;text-align:center;'><span style='font-size: 15pt; font-weight:bold;color:gray;'>Certification of Valuntary Service</span></td></tr>";

        //sHtml += "<tr><td><table><tr><td><img class='imgLogo' src='" + ih.BaseUri + "'  width='200' height='75'  /></td>";
        //sHtml += "        <td><p style='font-size: 18pt; font-weight:bold;padding-left:100px;padding-top:10px;line-height:15px;'>체 험 확 인 서</p>";
        //sHtml += "            <p style='font-size: 13pt; font-weight:bold;color:gray;padding-left:30px;line-height:15px;'>Certification of Valuntary Service</p></td>";
        //sHtml += "    </tr></table></td></tr> ";
        sHtml += "      <tr><td align='center'><img class='imgLogo' src='" + title.BaseUri + "' height='90'  /></td></tr>";

        sHtml += "        <tr><td height='60' align='center'>&nbsp;</td>";
        sHtml += "        </tr>";
        sHtml += "        <tr>";
        sHtml += "            <td>";
        sHtml += "                <table width='100%' border='1' cellpadding='0' cellspacing='0' bordercolor='#000000' style='border-collapse: collapse;'>";
        sHtml += "                    <tr><td height='40' colspan='4' bgcolor='#CCCCCC' align='center'><span style='font-size: 11pt; font-weight:bold;'>&nbsp;&nbsp;참가자 인적 사항</span> &nbsp;<span style='font-size:10pt;color:gray'>Personal Information</span></td>";
        sHtml += "                    </tr>";
        sHtml += "                    <tr>";
        sHtml += "                        <td width='30%' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>성 명</span>&nbsp;<span style='font-size:10pt;color:gray'>Name</span></td>";
        sHtml += "                        <td width='20%' height='40' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>&nbsp;"+ strName + "</span>&nbsp;<span style='font-size:10pt;color:gray'>&nbsp;" + strNameEng + "</span></td>";
        sHtml += "                        <td width='30%' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>생년월일</span>&nbsp;<span style='font-size:10pt;color:gray'>Date of Birth</span></td>";
        sHtml += "                        <td width='20%' height='40' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>&nbsp;"+ strBirth + "</span></td>";
        sHtml += "                    </tr>";
        sHtml += "                    <tr>";
        sHtml += "                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>연락처</span>&nbsp;<span style='font-size:10pt;color:gray'>Phone</span></td>";
        sHtml += "                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>&nbsp;" + strPhone + "</span></td>";
        sHtml += "                    </tr>";
        sHtml += "                    <tr>";
        sHtml += "                        <td height='40' colspan='4' bgcolor='#CCCCCC' align='center'><span style='font-size: 11pt; font-weight:bold;'>&nbsp;&nbsp;활동내역</span> &nbsp;<span style='font-size:10pt;color:gray'>The Details of Service</span></td>";
        sHtml += "                    </tr>";
        sHtml += "                    <tr>";
        sHtml += "                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>봉사기관명</span>&nbsp;<span style='font-size:10pt;color:gray'>Affiliation</span></td>";
        sHtml += "                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>&nbsp;사회복지법인 한국컴패션</span>&nbsp;<span style='font-size:10pt;color:gray'>&nbsp;Compassion Korea</span></td>";
        sHtml += "                    </tr>";
        sHtml += "                    <tr>";
        sHtml += "                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>방문국가</span>&nbsp;<span style='font-size:10pt;color:gray'>Location</span></td>";
        sHtml += "                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>&nbsp;"+ strLocation + "</span>&nbsp;<span style='font-size:10pt;color:gray'>&nbsp;" + strLocationEng + "</span></td>";
        sHtml += "                    </tr>";
        sHtml += "                    <tr>";
        sHtml += "                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>방문기간</span>&nbsp;<span style='font-size:10pt;color:gray'>Period of Service</span></td>";
        sHtml += "                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>&nbsp;"+ strDate +"</span></td>";
        sHtml += "                    </tr>";
        sHtml += "                    <tr>";
        sHtml += "                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>봉사시간</span>&nbsp;<span style='font-size:10pt;color:gray'>Service Time</span></td>";
        sHtml += "                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>&nbsp;" + strTime + " 시간</span>&nbsp;<span style='font-size:10pt;color:gray'>&nbsp;" + strTime + " hours </span></td>";
        sHtml += "                    </tr>";
        sHtml += "                    <tr>";
        sHtml += "                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>봉사내용</span>&nbsp;<span style='font-size:10pt;color:gray'>Details of Service</span></td>";
        sHtml += "                        <td height='40' colspan='3' bgcolor='#FFFFFF'>";
        sHtml += "                            <div style='font-size: 10pt;padding:7px;'>";
        sHtml += "                                <span>";
        sHtml += "                                    <span style='font-size:10pt;'>";
        sHtml += "                                        &nbsp;&#183;&nbsp;컴패션국가사무실 방문, 컴패션어린이센터 방문 및 컴패션사역 이해";
        sHtml += "                                        <br /><span style='color:gray'>";
        sHtml += "                                            &nbsp;&nbsp;&nbsp;To visit Country office of Compassion, Compassion Children’s";
        sHtml += "                                            Center and understand Compassion ministry";
        sHtml += "                                        </span>";
        sHtml += "                                    </span>";
        sHtml += "                                </span><br />";

        if(strType == "Plan")
        {
            sHtml += "                                <span>";
            sHtml += "                                    <span style='font-size:10pt;'>";
            sHtml += "                                        &nbsp;&#183;&nbsp;어린이들을 위한 봉사활동 (풍선아트, 페이스페인팅 등)";
            sHtml += "                                        <br /><span style='color:gray'>";
            sHtml += "                                            &nbsp;&nbsp;&nbsp;Voluntary activities for children (Balloon Art, Bubble Play etc.)";
            sHtml += "                                        </span>";
            sHtml += "                                    </span>";
            sHtml += "                                </span><br />";
        }

        sHtml += "                                <span>";
        sHtml += "                                    <span style='font-size:10pt;'>";
        sHtml += "                                        &nbsp;&#183;&nbsp;어린이 가정, 지역사회 방문을 통한 현지 문화 및 빈곤 실태 간접 체험";
        sHtml += "                                        <br /><span style='color:gray'>";
        sHtml += "                                            &nbsp;&nbsp;&nbsp;To visit homes/families and witness the poverty of the community";
        sHtml += "                                        </span>";
        sHtml += "                                    </span>";
        sHtml += "                                </span>";
        sHtml += "                            </div>";
        sHtml += "                        </td>";
        sHtml += "                    </tr>";
        sHtml += "                    <tr>";
        sHtml += "                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>담 당 자</span>&nbsp;<span style='font-size:10pt;color:gray'>Person in charge</span></td>";
        sHtml += "                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 10pt;'>&nbsp;김지은</span></td>";
        sHtml += "                    </tr>";
        sHtml += "                </table>";
        sHtml += "            </td>";
        sHtml += "        </tr>";
        sHtml += "        <tr>";
        //sHtml += "            <td height='20' class='blankTD'>&nbsp;</td>";
        sHtml += "        </tr>";
        sHtml += "        <tr>";
        sHtml += "            <td align='center'>";
        sHtml += "                <span style='font-size: 10pt;'>상기 명기된 사람은 본 기관에서 진행한 비전트립을 통하여 <br />봉사활동을 성실히 수행하였음을 위와 같이 증명합니다.</span><br />";
        sHtml += "                <span style='font-size: 10pt;color:gray;'>This is certify that the student above has taken part<br />in volunteer activities at Compattion Korea.</span><br />";
        sHtml += "                <span style='font-size: 10pt;color:gray;'>"+DateTime.Now.ToString("yyyy.MM.dd")+"</span>";
        sHtml += "            </td>";
        sHtml += "        </tr>";  
       // sHtml += "        <tr><td height='20'>&nbsp;</td></tr>";
        sHtml += "        <tr>";
        sHtml += "            <td align='center' style='width:100%;'>";
        ////sHtml += "                       <span style='font-size:18px;'>사회복지법인 한국컴패션 대표 서정인 (인)</span>";
        ////sHtml += "                        <br /><span style='color:gray;'>JUSTIN JUNG-IN SUH, CEO<br />COMPASSION KOREA</span>";
        //sHtml += "                <div style='display:inline-block;position:relative;'>";
        //// sHtml += "                    <div style='position: relative; top:-10px; left: 50px; font-size:13pt; font-weight: bold; float:left;'>";
        //sHtml += "                        <div><span style='font-size:18px;'>사회복지법인 한국컴패션 대표 서정인 (인)</span>";
        //sHtml += "                        <br /><span style='color:gray;'>JUSTIN JUNG-IN SUH, CEO<br />COMPASSION KOREA</span>";
        //sHtml += "                    </div>";
        //sHtml += "                </div>";
       // sHtml += "                <img src='"+ ih2.BaseUri + "' width='88' height='84' />";
        sHtml += "                <img src='" + sign.BaseUri + "' height='110' />";
        sHtml += "            </td>";
        sHtml += "        </tr>";
        sHtml += "        <tr><td height='20'>&nbsp;</td></tr>";
        sHtml += "        <tr><td>&nbsp;</td></tr>";
        sHtml += "    </table> ";
        sHtml += "</body>";
        sHtml += "</html>";
        #endregion
        
        return sHtml;
    }

    protected void btnExportPDF_Click(object sender, EventArgs e)
    { 
        string HTMLCode = string.Empty;
        ConvertHTMLToPDF(CreateReceiptHtml()); 
    }  

    protected void ConvertHTMLToPDF(string HTMLCode)
    { 
        BaseFont bf = BaseFont.CreateFont(Server.MapPath("~/common/font/malgun.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font font = new Font(bf, 12, Font.BOLD | Font.UNDERLINE, iTextSharp.text.pdf.CMYKColor.CYAN); 

        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition",
         "attachment;filename=체험확인서_"+ DateTime.Now.ToString("yyyyMMddhhmmssfff") + ".pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);  

        HttpContext context = HttpContext.Current;

        //Render PlaceHolder to temporary stream
        System.IO.StringWriter stringWrite = new StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        StringReader reader = new StringReader(HTMLCode);

        //Create PDF document
        Document doc = new Document(PageSize.A4);
        
        HTMLWorker parser = new HTMLWorker(doc);
        //PdfWriter.GetInstance(doc, new FileStream(Server.MapPath("~") + "/App_Data/HTMLToPDF.pdf", FileMode.Create));

        PdfWriter.GetInstance(doc, Response.OutputStream);

        doc.Open();

        /********************************************************************************/
        var interfaceProps = new Dictionary<string, Object>();
        var ih = new ImageHander() { BaseUri = Request.Url.ToString() };

        interfaceProps.Add(HTMLWorker.IMG_PROVIDER, ih);

        //Paragraph contents = new Paragraph(HTMLCode.ToString(), font); 
        FontFactory.Register(Server.MapPath("~/common/font/malgun.ttf"));
        StyleSheet style = new StyleSheet();
        style.LoadTagStyle("body", "font-family", "맑은 고딕");
        style.LoadTagStyle("body", "encoding", BaseFont.IDENTITY_H);
        //style.LoadTagStyle(HtmlTags.TD, HtmlTags.FONTSIZE, "9");
        style.LoadTagStyle(HtmlTags.TD, HtmlTags.AFTER, "10");
        style.LoadTagStyle(HtmlTags.TD, HtmlTags.PADDINGLEFT, "30"); 
        style.LoadStyle("blankTD", "height", "30px"); 

        parser.SetStyleSheet(style);

        foreach (IElement element in HTMLWorker.ParseToList(
        new StringReader(HTMLCode), style))
        { 
            doc.Add(element);
        }
        doc.Close();
        Response.End(); 
        /********************************************************************************/ 
    }
     
    public class ImageHander : IImageProvider
    {
        public string BaseUri;
        public iTextSharp.text.Image GetImage(string src,
        IDictionary<string, string> h,
        ChainedProperties cprops,
        IDocListener doc)
        {
            string imgPath = string.Empty;

            if (src.ToLower().Contains("http://") == false)
            {
                imgPath = HttpContext.Current.Request.Url.Scheme + "://" +

                HttpContext.Current.Request.Url.Authority + src;
            }
            else
            {
                imgPath = src;
            }

            return iTextSharp.text.Image.GetInstance(imgPath);
        }
    }
}


