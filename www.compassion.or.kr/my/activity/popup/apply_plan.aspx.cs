﻿using Microsoft.AspNet.FriendlyUrls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Data;

public partial class my_activity_popup_apply_plan : FrontBasePage
{    
    const string listPath = "/participation/visiontrip/schedule";

    private string strUserID = string.Empty;
    private string strSponserID = string.Empty;
    private int iApplyID = 0;
     
    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();
        //this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];

        if (!UserInfo.IsLogin)
        {
            Response.ClearContent();
            return;
        } 

        UserInfo sess = new UserInfo();
        strUserID = sess.UserId;
        strSponserID = sess.SponsorID;
        
        // 이름
        txtName.Value = sess.UserName;
        txtBirth.Value = sess.Birth != "" ? sess.Birth.Substring(0, 10) : "";
        txtEmail.Value = sess.Email;
        //txtPhone.Value = sess.Phone;

        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1)
        {
            Response.Redirect(listPath, true);
        }
        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);

        //if (!requests[0].CheckNumeric())
        //{
        //    Response.Redirect(listPath, true);
        //}

        //parameter
        base.PrimaryKey = requests[0];

        
        bool isNum = Int32.TryParse(PrimaryKey.ToString(), out iApplyID);

        //id.Value = PrimaryKey.ToString();
        //strApplyID = id.Value;

        //GetVisionTripSchedule();
        GetApply();

        //hdnFileRoot.Value = Uploader.GetRoot(Uploader.FileGroup.file_visiontrip)+"plan/"+id.Value+"/"+strUserID+"/";

    }
    
    protected void GetApply()
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var applyData = dao.tVisionTripApply.First(p => p.ApplyID == iApplyID && p.UserID == strUserID && p.CurrentUse == 'Y');
            var applyData = www6.selectQF<tVisionTripApply>("ApplyID", iApplyID, "UserID", strUserID, "CurrentUse", "Y");

            if (applyData != null)
            {
                //var scheduleData = dao.tVisionTripSchedule.FirstOrDefault(p => p.ScheduleID == applyData.ScheduleID && p.CurrentUse == 'Y');
                var scheduleData = www6.selectQF<tVisionTripSchedule>("ScheduleID", applyData.ScheduleID, "CurrentUse", "Y");

                if (scheduleData != null)
                {
                    txtTripSchedule.Value = scheduleData.StartDate.ToString() + " ~ " + scheduleData.EndDate.ToString();
                    txtVisitCountry.Value = scheduleData.VisitCountry.ToString();
                }

                //var applyDetailData = dao.tVisionTripPlanDetail.FirstOrDefault(p => p.ApplyID == iApplyID );
                var applyDetailData = www6.selectQF<tVisionTripPlanDetail>("ApplyID", iApplyID);

                ddlScheduleCheck.Value = applyDetailData.ScheduleAgreeYN.ToString();

                txtNameEng.Value = applyData.SponsorNameEng;
                txtPhone.Value = applyData.Tel.Decrypt();
                txtEmail.Value = applyData.Email.Decrypt();

                #region 주소 관련 
                locationType.Value = applyData.LocationType;

                addr_domestic.Checked = applyData.LocationType == "국내";
                addr_oversea.Checked = applyData.LocationType != "국내";

                var addr_split = "";
                var addr_country = "";
                if (applyData.Address1.Decrypt().Contains("$"))
                {
                    string[] addr_location = applyData.Address1.Decrypt().Split('$');
                    addr_split = addr_location[0];
                    addr_country = addr_location[1];
                }

                pn_addr_domestic.Visible = false;
                pn_addr_overseas.Visible = false;
                if (applyData.LocationType == "국내")
                {
                    pn_addr_domestic.Visible = true;
                    if (addr_split.Contains("//"))
                    {
                        string splitnew = addr_split.Replace("//", "^");

                        string[] addr_gubn = splitnew.Split('^');
                        addr_road.InnerText = "[도로명주소] (" + applyData.ZipCode.Decrypt() + ") " + addr_gubn[0] + applyData.Address2.Decrypt().ToString();
                        addr_jibun.InnerText = "[지번주소] (" + applyData.ZipCode.Decrypt() + ") " + addr_gubn[1] + applyData.Address2.Decrypt().ToString();
                        zipcode.Value = applyData.ZipCode.Decrypt().ToString();

                    }
                }
                else
                {
                    pn_addr_overseas.Visible = true;

                    addr_overseas_country.Value = addr_country.ToString();
                    addr_overseas_zipcode.Value = applyData.ZipCode.Decrypt();
                    addr_overseas_addr1.InnerText = addr_split;
                    addr_overseas_addr2.InnerText = applyData.Address2.Decrypt().ToString();
                } 
                #endregion


                ddlInfoChange.Value = applyData.ChangeYN.ToString();

                ddlNation.Value = applyDetailData.Nation.ToString();
                ddlReligion.Value = applyData.ReligionType.ToString();
                txtChurch.Value = applyData.ChurchName.ToString();
                if (applyData.ReligionType.ToString() != "Christian")
                    txtChurch.Visible = false;

                ddlEnglishLevel.Value = applyDetailData.EnglishLevel.ToString();
                ddlVTHistory.Value = applyDetailData.VisionTripHistory.ToString();
                txtJob.Value = applyDetailData.Job.ToString();

                ddlMilitary.Value = applyDetailData.MilitaryYN.ToString();

                txtEmergencyContactName.Value = applyData.EmergencyContactName.ToString();
                txtEmergencyContactTel.Value = applyData.EmergencyContactTel.ToString();
                txtEmergencyContactRelation.Value = applyData.EmergencyContactRelation.ToString();

                ddlRoomType.Value = applyDetailData.RoomType.ToString();
                txtRoomDetail.Value = applyDetailData.RoomDetail.ToString();
                ddlChildMeetYn.Value = applyData.ChildMeetYN.ToString();

                ddlCashReceiptType.Value = applyDetailData.CashReceiptType.ToString();

                txtCashReceiptName.Value = applyDetailData.CashReceiptName.ToString();
                txtCashReceiptTel.Value = applyDetailData.CashReceiptTel.ToString();

                txtCashReceiptRelation.Visible = false;
                if (applyDetailData.CashReceiptType.ToString() != "M")
                {
                    txtCashReceiptRelation.Visible = true;
                    txtCashReceiptRelation.Value = applyDetailData.CashReceiptRelation.ToString();
                }

                if (applyDetailData.AcceptTerms != null && applyDetailData.AcceptTerms != "0000000")
                {
                    //alert(ReviewSubmit.ETHICSCHECK);
                    if (applyDetailData.AcceptTerms.Substring(0, 1).IndexOf("1") > -1) chkAgree1.Checked = true; else chkAgree1.Checked = false;
                    if (applyDetailData.AcceptTerms.Substring(1, 2).IndexOf("1") > -1) chkAgree2.Checked = true; else chkAgree2.Checked = false;
                    if (applyDetailData.AcceptTerms.Substring(2, 3).IndexOf("1") > -1) chkAgree3.Checked = true; else chkAgree3.Checked = false;
                    if (applyDetailData.AcceptTerms.Substring(3, 4).IndexOf("1") > -1) chkAgree4.Checked = true; else chkAgree4.Checked = false;
                    if (applyDetailData.AcceptTerms.Substring(4, 3).IndexOf("1") > -1) chkAgree5.Checked = true; else chkAgree5.Checked = false;
                    if (applyDetailData.AcceptTerms.Substring(5, 2).IndexOf("1") > -1) chkAgree6.Checked = true; else chkAgree6.Checked = false;
                    if (applyDetailData.AcceptTerms.Substring(6, 1).IndexOf("1") > -1) chkAgree7.Checked = true; else chkAgree7.Checked = false; 
                     
                    chkAgree1.Disabled = true; 
                    chkAgree2.Disabled = true;
                    chkAgree3.Disabled = true;
                    chkAgree4.Disabled = true;
                    chkAgree5.Disabled = true;
                    chkAgree6.Disabled = true;
                    chkAgree7.Disabled = true;
                }
                chkAgreeFinal.Checked = (applyDetailData.ApplyAgree == 'Y') ? true : false;
                chkAgreeFinal.Disabled = true;

            }

        }

    }



    //protected void GetVisionTripSchedule()
    //{
    //    using (FrontDataContext dao = new FrontDataContext())
    //    { 
    //        var list = dao.tVisionTripSchedule.Where(p => p.ScheduleID == Convert.ToInt32(id.Value)).ToList();
    //        if (list != null)
    //        {
    //            txtTripSchedule.Value = (list.ToArray())[0].StartDate.ToString() + " ~ " + (list.ToArray())[0].EndDate.ToString();
    //            txtVisitNation.Value = (list.ToArray())[0].VisitNation.ToString();
    //        }
    //    }
    //}
      

}