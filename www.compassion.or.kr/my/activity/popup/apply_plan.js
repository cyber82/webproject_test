﻿

function jusoCallback(zipNo, addr1, addr2, jibun) {
    // 실제 저장 데이타
    $("#addr1").val(addr1 + "//" + jibun);
    $("#addr2").val(addr2);

    // 화면에 표시
    $("#zipcode").val(zipNo);

    $("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
    $("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);

};
function cert_setDomain() {
    //return;
    //if (location.hostname.startsWith("auth")) return;
    if (location.hostname.indexOf("compassionko.org") > -1)
        document.domain = "compassionko.org";
    else if (location.hostname.indexOf("compassionkr.com") > -1)
        document.domain = "compassionkr.com";
    else
        document.domain = "compassion.or.kr";
}

function checkAcceptterms(chkID) { 
    $("#hdnAgree" + chkID).val("1");
    $("[data-id=check_agree]").hide();
    $(".sp_agree" + chkID).removeClass("agree_check");
}
function agree_checkbox(chkID) {
    if ($("#hdnAgree" + chkID).val() == "") {
        scrollTo("#hdnAgree" + chkID);
        $("[data-id=check_agree]").html("약관전문보기를 하셔야 동의 체크 가능합니다.").addClass("guide_comment2").show();
        $("#chkAgree" + chkID).prop({ "checked": false });
        $(".sp_agree" + chkID).addClass("agree_check");
        return false;
    }
    else
        $(".sp_agree" + chkID).removeClass("agree_check");
}

(function () {

    var app = angular.module('cps.page', ['ui.multiselect']); 

    app.directive('onFinishRender', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                //if (scope.$first === true) {
                //    window.alert('First thing about to render');
                //}
                if (scope.$last === true) {
                    $timeout(function () { 
                        scope.$emit(attr.onFinishRender);
                    },1);
                }
            }
        };
    });

    app.controller('defaultCtrl', function ($scope, $http, popup, $location, paramService) {

        $scope.name = 'Child';
        $scope.mychild = [{ id: 1, name: '' }, { id: 2, name: '' }, { id: 1, name: '' }];
        $scope.selectedChild = [];

        $http.get("/api/visiontrip.ashx?t=childlist", { params: {} }).success(function (result) { 
            $scope.list = [];

            $.each(result.data, function () {
                $scope.list.push({ id: this.childkey, name: this.name });
            });
            $scope.name = 'World'; 
            $scope.mychild = $scope.list;

        });
        
        //후원어린이선택
        $scope.child_inputs = [];
        $scope.addInput = function () {
            if ($scope.child_inputs.length < 3) { //최대 3개 까지만 가능
                $scope.child_inputs.push({ id: '', name: '' });
            }
        } 
        $scope.removeInput = function (index) {
            $scope.child_inputs.splice(index, 1);
        }
        $scope.$on('ngRepeatFinished_Child', function (ngRepeatFinishedEvent) { 
            $(".childinput").setHangulBan();
        });
        //end 후원어린이선택

        //동반인 
        $scope.companion_inputs = [{ id: '', name_kor: '', name_eng: '', birth: '', gender: '' }];
        $scope.addCompanionInput = function () {
            if ($scope.companion_inputs.length < 6) { //최대 7개 까지만 가능
                $scope.companion_inputs.push({ id: '', name_kor: '', name_eng: '', birth: '', gender: '' });
            }
        }
        $scope.removeCompanionInput = function (index) {
            $scope.companion_inputs.splice(index, 1);
        }
        //end 동반인 

        //기타 파일
        $scope.input_fils = [{ key: 0, filename: '', filepath:'' }];
        var newItemNo = 0;
        $scope.addInputFiles = function ($index) {
            newItemNo++;
            //newItemNo = $scope.input_fils.length + 1; 
            $scope.input_fils.push({ key: newItemNo, filename: '', filepath: '' });
        }

        $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
            if ($scope.input_fils.length > 0) {
                var idx = $scope.input_fils[$scope.input_fils.length - 1].key;
                $scope.setFileUploader(idx);
            }
        });
        $scope.setFileUploader = function (idx) {
            var uploader_passport = $page.attachUploader("btn_etcfile" + idx);
            uploader_passport._settings.data.fileDir = $("#hdnFileRoot").val();
            uploader_passport._settings.data.rename = "y"; 
            uploader_passport._settings.data.limit = 2048; 
        };

        $scope.removeInputFiles = function (index) {
            $scope.input_fils.splice(index, 1);
        }
        //end 기타파일

        //주소 검색 팝업
        $scope.addrpopup = function ($event) {
            $event.preventDefault();
            cert_setDomain();
            var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");
        }

        //방문국가 
        if ($("#hdnApplyType").val() == "Individual") {
           
            $scope.countries = null;
            $scope.country = "";
            $http.get("/api/visiontrip.ashx?t=country", { params: {} }).success(function (r) {
                $scope.countries = r.data;
                setTimeout(function () {
                    $(".custom_sel").selectbox("detach");
                    $(".custom_sel").selectbox({});
                }, 500) 
            });
        }

        //일반트립 신청서 저장
        $scope.applysubmit = function () {

            if (!$page.onSubmit())
                return false;

            var ScheduleID = $("#id").val();
            var ScheduleAgreeYN = $("#ddlScheduleCheck").val(); //스케줄확인여부
            var Nation = $("#ddlNation").val();// 국적
            var EnglishLevel = $("#ddlEnglishLevel").val(); //영어회화능력
            var VisionTripHistory = $("#ddlVTHistory").val();//비전트립 참가이력
            var Job = $("#txtJob").val();//직업
            var Military = $("#ddlMilitary").val(); //병역
            var RoomType = $("#ddlRoomType").val(); //방배정
            var RoomDetail = $("#txtRoomDetail").val(); //방배정상세
            var CashReceiptType = $("#ddlCashReceiptType").val(); //현금영수증 구분
            var CashReceiptName = $("#txtCashReceiptName").val(); //현금영수증 성함
            var CashReceiptTel = $("#txtCashReceiptTel").val(); //현금영수정 번호
            var CashReceiptRelation = $("#txtCashReceiptRelation").val();//현금영수증 참가자와의 관계
            var AcceptTerms = ($("#chkAgree1").is(":checked") ? "1" : "0") + ($("#chkAgree2").is(":checked") ? "1" : "0") +
                        ($("#chkAgree3").is(":checked") ? "1" : "0") + ($("#chkAgree4").is(":checked") ? "1" : "0") +
                        ($("#chkAgree5").is(":checked") ? "1" : "0") + ($("#chkAgree6").is(":checked") ? "1" : "0") +
                        ($("#chkAgree7").is(":checked") ? "1" : "0") + ($("#chkAgree8").is(":checked") ? "1" : "0"); //약관동의 

            var ApplyAgree = $("#chkAgreeFinal").is(":checked") ? "Y" : "N";  //약관동의 최종확인

            var ApplyType = "Plan"; //비전트립 구분 - 기획/개인방문
            var SponsorNameEng = $("#txtNameEng").val();  //신청자 영문명
            var Tel = $("#txtPhone").val(); //신청자 전화번호
            var Email = $("#txtEmail").val(); //신청자 이메일
            var Address1 = $("#addr1").val(); //신청자 주소
            var Address2 = $("#addr2").val();
            var ZipCode = $("#zipcode").val();
            var ChangeYN = $("#ddlInfoChange").val(); //기존정보 변경 여부
            var Religion = $("#ddlReligion").val(); //종교
            var Church = $("#ddlReligion").val() == "Christian" ? $("#txtChurch").val() : ""; //교회명
            var EmergencyContactName = $("#txtEmergencyContactName").val(); //비상연락처
            var EmergencyContactTel = $("#txtEmergencyContactTel").val();
            var EmergencyContactRelation = $("#txtEmergencyContactRelation").val();
            var ChildMeetYN = $("#ddlChildMeetYn").val(); //후원어린이 만남여부

            var ChildList = [];

            for (var k in $scope.selectedChild) {
                ChildList.push($scope.selectedChild[k]);
            }
            for (var k in $scope.child_inputs) {
                ChildList.push($scope.child_inputs[k]);
            }

            var AttachFile = [];
            if ($("#path_btn_passportfile").val() != "") {
                AttachFile.push({ type: "passport", name: $("[data-id=path_btn_passportfile]").val(), path: $("#path_btn_passportfile").val() });
            }
            for (var f in $scope.input_fils) {
                if ($("#path_btn_etcfile" + $scope.input_fils[f].key).val() != "") {
                    AttachFile.push({ type: "etc", name: $("[data-id=path_btn_etcfile" + $scope.input_fils[f].key + "]").val(), path: $("#path_btn_etcfile" + $scope.input_fils[f].key).val() });
                }
            }

            var param = {
                t: "add_planapply",
                scheduleID: ScheduleID,
                applyType: "Plan",
                sponsorNameEng: SponsorNameEng,
                tel: Tel,
                email: Email,
                address1: Address1,
                address2: Address2,
                zipcode: ZipCode,
                changeYn: ChangeYN,
                religion: Religion,
                church: Church,
                emergencycontactName: EmergencyContactName,
                emergencycontactTel: EmergencyContactTel,
                emergencycontactRelation: EmergencyContactRelation,
                childmeetYn: ChildMeetYN,
                scheduleagreeYn: ScheduleAgreeYN,
                nation: Nation,
                englishLevel: EnglishLevel,
                visiontripHistory: VisionTripHistory,
                job: Job,
                military: Military,
                roomType: RoomType,
                roomDetail: RoomDetail,
                cashreceiptType: CashReceiptType,
                cashreceiptName: CashReceiptName,
                cashreceiptTel: CashReceiptTel,
                cashreceiptRelation: CashReceiptRelation,
                acceptTerms: AcceptTerms,
                applyAgree: ApplyAgree,
                childList: JSON.stringify(ChildList),
                attachFile: JSON.stringify(AttachFile)
            }

            $http.post("/api/visiontrip.ashx", param).success(function (result) {
                console.log(result);
                if (result.success) {
                    location.href = "/participation/visiontrip/complete/plan/" + ScheduleID;
                } else {
                    if (result.message == "") {
                        alert("신청 실패하였습니다. 관리자에게 문의하시기 바랍니다.");
                    } else {
                        alert(result.message);
                    }
                }
            });
        };

        //개인방문 신청서 저장
        $scope.applysubmit_individual = function () {

            var ChildList = []; 
            for (var k in $scope.selectedChild) {
                ChildList.push($scope.selectedChild[k]);
            }
            for (var k in $scope.child_inputs) {
                ChildList.push($scope.child_inputs[k]);
            } 
             
            if (!$page.onSubmit_individual(ChildList, $scope.companion_inputs))
                return false;

            var Nation = $("#ddlNation").val();// 국적 

            var AcceptTerms = ($("#chkAgree1").is(":checked") ? "1" : "0") + ($("#chkAgree2").is(":checked") ? "1" : "0") +
                        ($("#chkAgree3").is(":checked") ? "1" : "0"); //약관동의 

            var ApplyType = "Individual"; //비전트립 구분 - 기획/개인방문
            var SponsorNameEng = $("#txtNameEng").val();  //신청자 영문명
            var Tel = $("#txtPhone").val(); //신청자 전화번호
            var Email = $("#txtEmail").val(); //신청자 이메일
            var Address1 = $("#addr1").val(); //신청자 주소
            var Address2 = $("#addr2").val();
            var ZipCode = $("#zipcode").val();
            var ChangeYN = $("#ddlInfoChange").val(); //기존정보 변경 여부
            var Religion = $("#ddlReligion").val(); //종교
            var Church = $("#ddlReligion").val() == "Christian" ? $("#txtChurch").val() : ""; //교회명
            var EmergencyContactName = $("#txtEmergencyContactName").val(); //비상연락처
            var EmergencyContactTel = $("#txtEmergencyContactTel").val();
            var EmergencyContactRelation = $("#txtEmergencyContactRelation").val();
            var VisitType = $("#ddlVisitType").val(); //방문유형 
            var VisitDate1 = $("#txtVisitDate1").val(); //방문일정1
            var VisitDate2 = $("#txtVisitDate2").val(); //방문일정2
            var VisitNation = $("#ddlVisitNation").val(); // 방문국가
            var LocalAccommodation = $("#txtLocalAccommodation").val(); //현지정보
            var LocalTel = $("#txtLocalTel").val();
            var LocalAddress = $("#txtLocalAddress").val();
            var DepartureDate = $("#txtDepartureDate").val(); //출국일
            var ReturnDate = $("#txtReturnDate").val(); //귀국일



            var ChildList = [];

            for (var k in $scope.selectedChild) {
                ChildList.push($scope.selectedChild[k]);
            }
            for (var k in $scope.child_inputs) {
                ChildList.push($scope.child_inputs[k]);
            }

            var CompanionList = [];
            for (var c in $scope.companion_inputs) {
                CompanionList.push($scope.companion_inputs[c]);
            }

            var AttachFile = []; 
            for (var f in $scope.input_fils) {
                if ($("#path_btn_etcfile" + $scope.input_fils[f].key).val() != "") {
                    AttachFile.push({ type: "etc", name: $("[data-id=path_btn_etcfile" + $scope.input_fils[f].key + "]").val(), path: $("#path_btn_etcfile" + $scope.input_fils[f].key).val() });
                }
            }

            var param = {
                t: "add_individualapply", 
                applyType: ApplyType,
                sponsorNameEng: SponsorNameEng,
                tel: Tel,
                email: Email,
                address1: Address1,
                address2: Address2,
                zipcode: ZipCode,
                changeYn: ChangeYN,
                religion: Religion,
                church: Church,
                emergencycontactName: EmergencyContactName,
                emergencycontactTel: EmergencyContactTel,
                emergencycontactRelation: EmergencyContactRelation,

                visitType: VisitType,
                visitDate1: VisitDate1,
                visitDate2: VisitDate2,
                visitNation: VisitNation,
                localAccommodation: LocalAccommodation,
                localTel: LocalTel,
                localAddress: LocalAddress,
                departureDate: DepartureDate,
                returnDate: ReturnDate,

                acceptTerms: AcceptTerms,

                childList: JSON.stringify(ChildList),
                companionList: JSON.stringify(CompanionList),
                attachFile: JSON.stringify(AttachFile)
            }

            $http.post("/api/visiontrip.ashx", param).success(function (result) {
                console.log(result);
                if (result.success) {
                    location.href = "/participation/visiontrip/complete/individual/";
                } else {
                    if (result.message == "") {
                        alert("신청 실패하였습니다. 관리자에게 문의하시기 바랍니다.");
                    } else {
                        alert(result.message);
                    }
                }
            });
        };

    });  
})();

var $page = {

    timer: null,

    init: function () {
        $("#txtNameEng").setHangulBan(); //한글 X 

        // ddl - 트립 일정과 방문 국가를 확인했습니다
        $("#ddlScheduleCheck").change(function () {
            if ($("#ddlScheduleCheck").val() == "Y") {
                $("[data-id=check_tripschedule]").hide();
            }
        });
        //영문명
        $("#txtNameEng").keyup(function () {
            if ($("#txtNameEng").val() != "") {
                $("[data-id=check_engname]").hide();
            }
        });
        //전화번호
        $("#txtPhone").keyup(function () {
            if ($("#txtPhone").val() != "") {
                $("[data-id=check_phone]").hide();
            }
        });
        //주소
        $("#addr_road").change(function () {
            if ($("addr_road").val() != "") {
                $("[data-id=check_addr]").hide();
            }
        });
        //ddl - 기존정보변경여부
        $("#ddlInfoChange").change(function () {
            if ($("#ddlInfoChange").val() != "") {
                $("[data-id=check_infochange]").hide();
            }
        });
        //ddl - 국적
        $("#ddlNation").change(function () {
            if ($("#ddlNation").val() != "") {
                $("[data-id=check_nation]").hide();
            }
        });
        //ddl - 종교
        $("#ddlReligion").change(function () {
            if ($("#ddlReligion").val() != "") {
                $("[data-id=check_religion]").hide();
            }
            if ($("#ddlReligion").val() == "Christian" && $("#txtChurch").val() != "") {
                $("[data-id=check_church]").hide();
            }
        });

        //소속 교회
        $("#txtChurch").keyup(function () {
            if ($("#txtChurch").val() != "") {
                $(this).val($(this).val().replace(' ', '')); //공백제거
                $("[data-id=check_church]").hide();
            }
        });

        //ddl - 영어회화능력
        $("#ddlEnglishLevel").change(function () {
            if ($("#ddlEnglishLevel").val() != "") {
                $("[data-id=check_englishlevel]").hide();
            }
        });
        //ddl - 비전트립 참가이력 
        $("#ddlVTHistory").change(function () {
            if ($("#ddlVTHistory").val() != "") {
                $("[data-id=check_visiontriphistory]").hide();
            }
        });
        //직업
        $("#txtJob").keyup(function () {
            if ($("#txtJob").val() != "") {
                $("[data-id=check_job]").hide();
            }
        });

        //ddl - 병역
        $("#ddlMilitary").change(function () {
            if ($("#ddlMilitary").val() != "") {
                $("[data-id=check_military]").hide();
            }
        });


        //비상연락처
        $("#txtEmergencyContactName").keyup(function () {
            if ($("#txtEmergencyContactName").val() != "") {
                $("[data-id=check_emergencycontactname]").hide();
            }
        });
        $("#txtEmergencyContactTel").keyup(function () {
            if ($("#txtEmergencyContactTel").val() != "") {
                $("[data-id=check_emergencycontacttel]").hide();
            }
        });
        $("#txtEmergencyContactRelation").keyup(function () {
            if ($("#txtEmergencyContactRelation").val() != "") {
                $("[data-id=check_emergencycontactrelation]").hide();
            }
        });
        //ddl - 방배정
        $("#ddlRoomType").change(function () {
            if ($("#ddlRoomType").val() != "") {
                $("[data-id=check_roomtype]").hide();
            }
        });
        //ddl - 후원어린이 만남 여부
        $("#ddlChildMeetYn").change(function () {
            if ($("#ddlChildMeetYn").val() != "") {
                $("[data-id=check_childmeetyn]").hide();
            }
        });
        //ddl - 현금영수증 발급 선택
        $("#ddlCashReceiptType").change(function () {
            if ($("#ddlCashReceiptType").val() != "") {
                $("[data-id=check_cashreceipttype]").hide();
            }
        });
        $("#txtCashReceiptName").keyup(function () {
            if ($("#txtCashReceiptName").val() != "") {
                $("[data-id=check_cashreceiptname]").hide();
            }
        });
        $("#txtCashReceiptTel").keyup(function () {
            if ($("#txtCashReceiptTel").val() != "") {
                $("[data-id=check_cashreceipttel]").hide();
            }
        });
        $("#txtCashReceiptRelation").keyup(function () {
            if ($("#txtCashReceiptRelation").val() != "") {
                $("[data-id=check_cashreceiptrelation]").hide();
            }
        });



        $(".agreecheck_change").change(function () {

            if ($("#chkAgree1").is(":checked") && $("#chkAgree2").is(":checked") &&
                $("#chkAgree3").is(":checked") && $("#chkAgree4").is(":checked") &&
                $("#chkAgree5").is(":checked") && $("#chkAgree6").is(":checked") &&
                $("#chkAgree7").is(":checked") && $("#chkAgree8").is(":checked")
                && $("#chkAgreeFinal").is(":checked")
                ) {
                $("[data-id=check_agree]").hide();
            }
        });




    },

    init_individual: function () {

        $("#txtNameEng").setHangulBan();
        
        $(".childinput").keyup(function (e) {
            $(this).val($(this).val().replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, ''));
        });




        // ddl - 트립 일정과 방문 국가를 확인했습니다
        $("#ddlVisitType").change(function () {
            if ($("#ddlVisitType").val() == "") {
                $("[data-id=check_visittype]").hide();
            }
        });
        //방문일자  
        $('#txtVisitDate1').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function () {
                $("[data-id=check_visitdate1]").hide();
            }
        });
        $('#txtVisitDate2').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function () { 
                $("[data-id=check_visitdate2]").hide();
            }
        });
        //방문국가
        $("#ddlVisitNation").change(function () {
            if ($("#ddlVisitNation").val() != "") {
                $("[data-id=check_visitnation]").hide();
            }
        });

        //영문명
        $("#txtNameEng").keyup(function () {
            if ($("#txtNameEng").val() != "") {
                $("[data-id=check_engname]").hide();
            }
        });
        //전화번호
        $("#txtPhone").keyup(function () {
            if ($("#txtPhone").val() != "") {
                $("[data-id=check_phone]").hide();
            }
        });
        //주소
        $("#addr_road").change(function () {
            if ($("addr_road").val() != "") {
                $("[data-id=check_addr]").hide();
            }
        });
        //ddl - 기존정보변경여부
        $("#ddlInfoChange").change(function () {
            if ($("#ddlInfoChange").val() != "") {
                $("[data-id=check_infochange]").hide();
            }
        }); 
        //ddl - 종교
        $("#ddlReligion").change(function () {
            if ($("#ddlReligion").val() != "") {
                $("[data-id=check_religion]").hide();
            }
            if ($("#ddlReligion").val() == "Christian" && $("#txtChurch").val() != "") {
                $("[data-id=check_church]").hide();
            }
        });

        //소속 교회
        $("#txtChurch").keyup(function () {
            if ($("#txtChurch").val() != "") {
                $(this).val($(this).val().replace(' ', '')); //공백제거
                $("[data-id=check_church]").hide();
            }
        });

        //어린이정보
        $("multiselect").click(function () {
            $("[data-id=check_childlist]").hide();
        });
        $(".divChildMeet a").click(function () {
            $("[data-id=check_childlist]").hide();
        });

        // 현지정보
        $("#txtLocalAccommodation").keyup(function () {
            if ($("#txtLocalAccommodation").val() != "") {
                $("[data-id=check_localaccommodation]").hide();
            }
        });
        $("#txtLocalTel").keyup(function () {
            if ($("#txtLocalTel").val() != "") {
                $("[data-id=check_localtel]").hide();
            }
        });
        $("#txtLocalAddress").keyup(function() {
            if ($("#txtLocalAddress").val() != "") {
                $("[data-id=check_localaddress]").hide();
            }
        });


        //$(".companion_name_kor, .companion_name_eng, .companion_birth, .companion_gender").keyup(function () {
        //    $("[data-id=check_companion]").hide();
        //});



        //비상연락처
        $("#txtEmergencyContactName").keyup(function () {
            if ($("#txtEmergencyContactName").val() != "") {
                $("[data-id=check_emergencycontactname]").hide();
            }
        });
        $("#txtEmergencyContactTel").keyup(function () {
            if ($("#txtEmergencyContactTel").val() != "") {
                $("[data-id=check_emergencycontacttel]").hide();
            }
        });
        $("#txtEmergencyContactRelation").keyup(function () {
            if ($("#txtEmergencyContactRelation").val() != "") {
                $("[data-id=check_emergencycontactrelation]").hide();
            }
        }); 

        //출입국정보
        $('#txtDepartureDate').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function () {
                $("[data-id=check_departuredate]").hide();
            }
        });
        $('#txtReturnDate').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function () {
                $("[data-id=check_returndate]").hide();
            }
        });

        $(".agreecheck_change").change(function () { 
            if ($("#chkAgree1").is(":checked") && $("#chkAgree2").is(":checked") &&
                $("#chkAgree3").is(":checked")
                ) {
                $("[data-id=check_agree]").hide();
            }
        });  
    },

    // 확인
    onSubmit: function () {

        var count = 0;

        // ddl - 트립 일정과 방문 국가를 확인했습니다
        if ($("#ddlScheduleCheck").val() != "Y") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_tripschedule]").html("트립 일정 확인해주세요.").addClass("guide_comment2").show();
            count++;
            //return false;
        }
        // 영문명
        if ($("#txtNameEng").val() == "") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_engname]").html("영문명을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }


        // 전화번호
        if ($("#txtPhone").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_phone]").html("전화번호를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 이메일
        if ($("#txtEmail").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_engname]").html("이메일을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 주소
        //if ($("#addr_road").text() == "") {
        //    if (count == 0)
        //        scrollTo($(".divUserInfo"));
        //    $("[data-id=check_addr]").html("주소를 입력해주세요.").addClass("guide_comment2").show();
        //    //return false;
        //    count++;
        //}
        // ddl - 기존정보변경여부
        if ($("#ddlInfoChange").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_infochange]").html("정보변경 여부 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 국적
        if ($("#ddlNation").val() == "") {
            if (count == 0)
                scrollTo($(".divNation"));
            $("[data-id=check_nation]").html("국적을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 종교
        if ($("#ddlReligion").val() == "") {
            if (count == 0)
                scrollTo($(".divReligion"));
            $("[data-id=check_religion]").html("종교를 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 소속 교회
        if ($("#ddlReligion").val() == "Christian" && $("#txtChurch").val() == "") {
            if (count == 0)
                scrollTo($(".divReligion"));
            $("[data-id=check_church]").html("소속교회를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // ddl - 영어회화능력
        if ($("#ddlEnglishLevel").val() == "") {
            if (count == 0)
                scrollTo($(".divGroupField"));
            $("[data-id=check_englishlevel]").html("영어회화능력을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 비전트립 참가이력 
        if ($("#ddlVTHistory").val() == "") {
            if (count == 0)
                scrollTo($(".divGroupField"));
            $("[data-id=check_visiontriphistory]").html("비전트립 참가이력을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 직업
        if ($("#txtJob").val() == "") {
            if (count == 0)
                scrollTo($(".divGroupField"));
            $("[data-id=check_job]").html("직업을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 병역
        if ($("#ddlMilitary").val() == "") {
            if (count == 0)
                scrollTo($(".divMilitary"));
            $("[data-id=check_military]").html("병역 구분을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }


        // 비상연락처 성함
        if ($("#txtEmergencyContactName").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontactname]").html("비상연락처 성함을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 비상연락처 휴대번호
        if ($("#txtEmergencyContactTel").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontacttel]").html("비상연락처를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 비상연락처 참가자와의 관계
        if ($("#txtEmergencyContactRelation").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontactrelation]").html("참가자와의 관계를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // ddl - 방배정
        if ($("#ddlRoomType").val() == "") {
            if (count == 0)
                scrollTo($(".divRoom"));
            $("[data-id=check_roomtype]").html("방배정 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // ddl - 후원어린이 만남 여부
        if ($("#ddlChildMeetYn").val() == "") {
            if (count == 0)
                scrollTo($(".divChildMeet"));
            $("[data-id=check_childmeetyn]").html("후원어린이 만남 여부를 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 현금영수증 발급 선택
        if ($("#ddlCashReceiptType").val() == "") {
            if (count == 0)
                scrollTo($(".divCashReceipt"));
            $("[data-id=check_cashreceipttype]").html("현금영수증 발급 구분을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }


        // 현금영수증 발급자 성함
        if ($("#txtCashReceiptName").val() == "") {
            if (count == 0)
                scrollTo($(".divCashReceipt"));
            $("[data-id=check_cashreceiptname]").html("현금영수증 발급자 성함을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 현금영수증 발급자 휴대번호
        if ($("#txtCashReceiptTel").val() == "") {
            if (count == 0)
                scrollTo($(".divCashReceipt"));
            $("[data-id=check_cashreceipttel]").html("현금영수증 발급자 휴대번호을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 현금영수증 - 참가자와의 관계
        if ($("#txtCashReceiptRelation").val() == "") {
            if (count == 0)
                scrollTo($(".divCashReceipt"));
            $("[data-id=check_cashreceiptrelation]").html("참가자와의 관계를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        if (!($("#chkAgree1").is(":checked") &&
            $("#chkAgree2").is(":checked") &&
            $("#chkAgree3").is(":checked") &&
            $("#chkAgree4").is(":checked") &&
            $("#chkAgree5").is(":checked") &&
            $("#chkAgree6").is(":checked") &&
            $("#chkAgree7").is(":checked") &&
            $("#chkAgree8").is(":checked")
            &&
            $("#chkAgreeFinal").is(":checked")
            )) {
            if (count == 0)
                scrollTo($(".divAgreement"));
            $("[data-id=check_agree]").html("약관동의를 모두 선택해주세요.").addClass("guide_comment2").show();
        }

        if (count > 0) {
            count = 0;
            return false;
        }

        return true;
    },

    // 확인
    onSubmit_individual: function (childList, companionList) {

        var count = 0;

        // ddl - 방문유형
        if ($("#ddlVisitType").val() == "") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_visittype]").html("방문유형을 선택해주세요.").addClass("guide_comment2").show();
            count++;
            //return false;
        }
        // 방문일자 - 1
        if ($("#txtVisitDate1").val() == "") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_visitdate1]").html("방문날짜(1지망)을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 방문일자 - 2
        if ($("#txtVisitDate2").val() == "") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_visitdate2]").html("방문날짜(2지망)을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 방문국가
        if ($("#ddlVisitNation").val() == "") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_visitnation]").html("방문국가를 선택해주세요.").addClass("guide_comment2").show();
            count++;
            //return false;
        }


        // 영문명
        if ($("#txtNameEng").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"))
            $("[data-id=check_engname]").html("영문명을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // 전화번호
        if ($("#txtPhone").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_phone]").html("전화번호를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 이메일
        if ($("#txtEmail").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_engname]").html("이메일을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 주소
        //if ($("#addr_road").text() == "") {
        //    if (count == 0)
        //        scrollTo($(".divUserInfo"));
        //    $("[data-id=check_addr]").html("주소를 입력해주세요.").addClass("guide_comment2").show();
        //    //return false;
        //    count++;
        //}
        // ddl - 기존정보변경여부
        if ($("#ddlInfoChange").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_infochange]").html("정보변경 여부 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // ddl - 종교
        if ($("#ddlReligion").val() == "") {
            if (count == 0)
                scrollTo($(".divReligion"));
            $("[data-id=check_religion]").html("종교를 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 소속 교회
        if ($("#ddlReligion").val() == "Christian" && $("#txtChurch").val() == "") {
            if (count == 0)
                scrollTo($(".divReligion"));
            $("[data-id=check_church]").html("소속교회를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        //var companion_text = "동반인 정보 ";
        //var companion_count = 0;
        //var companion_name_kor = $('.companion_name_kor').filter(function () {
        //    return this.value == ''
        //}); 
        //// 동반인정보
        //if (companion_name_kor.length) {
        //    if (count == 0)
        //        scrollTo($(".divCompanion"));
        //    companion_text += "한글성함"
        //    companion_count++;     //return false;
        //    count++;
        //}
        //var companion_name_eng = $('.companion_name_eng').filter(function () {
        //    return this.value == ''
        //}); 
        //if (companion_name_eng.length) {
        //    if (count == 0)
        //        scrollTo($(".divCompanion"));
        //    companion_text += companion_count > 0 ? ", 영문성함" : "영문성함"
        //    companion_count++;
        //}
        //var companion_birth = $('.companion_birth').filter(function () {
        //    return this.value == ''
        //});
        //if (companion_birth.length) {
        //    if (count == 0)
        //        scrollTo($(".divCompanion"));
        //    companion_text += companion_count > 0 ? ", 생년월일" : "생년월일"
        //    companion_count++; 
        //}
        //var companion_gender = $('.companion_gender').filter(function () {
        //    return this.value == ''
        //});
        //if (companion_gender.length) {
        //    if (count == 0)
        //        scrollTo($(".divCompanion"));
        //    companion_text += companion_count > 0 ? ", 성별" : "성별"
        //    companion_count++; 
        //}
        //if (companion_count > 0) { 
        //    $("[data-id=check_companion]").html(companion_text + "을 입력해주세요.").addClass("guide_comment2").show();

        //    count++;
        //}

        //어린이정보
        if (childList.length == 0) {
            if (count == 0)
                scrollTo($(".divChildMeet"));
            $("[data-id=check_childlist]").html("어린이정보를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // 현지정보
        if ($("#txtLocalAccommodation").val() == "") {
            if (count == 0)
                scrollTo($(".divLocal"));
            $("[data-id=check_localaccommodation]").html("현지 숙소명을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        } 
        if ($("#txtLocalTel").val() == "") {
            if (count == 0)
                scrollTo($(".divLocal"));
            $("[data-id=check_localtel]").html("현지 숙소 연락처를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        } 
        if ($("#txtLocalAddress").val() == "") {
            if (count == 0)
                scrollTo($(".divLocal"));
            $("[data-id=check_localaddress]").html("현지 숙소 주소를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }


        if ($("#txtDepartureDate").val() == "") {
            if (count == 0)
                scrollTo($(".divDate"));
            $("[data-id=check_departuredate]").html("출국일을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        if ($("#txtReturnDate").val() == "") {
            if (count == 0)
                scrollTo($(".divDate"));
            $("[data-id=check_returndate]").html("귀국일을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // 비상연락처 성함
        if ($("#txtEmergencyContactName").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontactname]").html("비상연락처 성함을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 비상연락처 휴대번호
        if ($("#txtEmergencyContactTel").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontacttel]").html("비상연락처를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 비상연락처 참가자와의 관계
        if ($("#txtEmergencyContactRelation").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontactrelation]").html("참가자와의 관계를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
 

        if (!($("#chkAgree1").is(":checked") &&
            $("#chkAgree2").is(":checked") &&
            $("#chkAgree3").is(":checked") )) {
            if (count == 0)
                scrollTo($(".divAgreement"));
            $("[data-id=check_agree]").html("약관동의를 모두 선택해주세요.").addClass("guide_comment2").show();
        }

        if (count > 0) {
            count = 0;
            return false;
        }

        return true;
    },

    attachUploader: function (button) {
        return new AjaxUpload(button, {
            action: '/c/handler/upload',
            responseType: 'json',
            onChange: function (file) {
                var fileName = file.toLowerCase();
            },
            onSubmit: function (file, ext) {
                this.disable();
            },
            onComplete: function (file, response) {

                this.enable();

                console.log(file, response);
                if (response.success) {
                    //$("#passport_path").val(file);
                    //$("#lb_passport_path").val(file);
                    $("#path_" + button).val(response.name);
                    $("[data-id=path_" + button + "]").val(response.name.replace(/^.*[\\\/]/, ''));

                    console.log($("#path_" + button).val()); //filepullpath
                    console.log($("[data-id=path_" + button + "]").val());  //filename
                    //$("#passport_path").val(response.name);
                    //$("[data-id=passport_path]").val(response.name.replace(/^.*[\\\/]/, ''));
                    //	$(".temp_file_size").val(response.size);

                } else
                    alert(response.msg);
            }
        });
    }

};