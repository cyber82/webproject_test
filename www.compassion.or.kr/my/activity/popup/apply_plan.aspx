﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="apply_plan.aspx.cs" Inherits="my_activity_popup_apply_plan" %>

<div style="background: transparent;" class="fn_pop_container" id="tripApply">
    <div class="pop_type1 w980 fn_pop_content" style="margin-top: 50px; margin-bottom: 50px;">
        <script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="/assets/jquery/jquery-ui/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/assets/jquery/wisekit/function.js"></script>
        <script type="text/javascript" src="/common/js/site/common.js"></script>
        <script type="text/javascript" src="/common/js/jquery/placeholders.min.js"></script>
        <script type="text/javascript" src="/common/js/util/jquery.selectbox-0.2/jquery.selectbox-0.2-wisekit.js"></script>

        <style type="text/css">

            .login_input{width:700px !important;}
             
            .subContents.member .input_div .box {
                width: 500px;
                text-align: center;
                background: #fdfdfd;
                border: 1px solid #e8e8e8;
                padding: 20px 20px 0px;
            }

                .subContents.member .input_div .box p {
                    text-align: center;
                    line-height: 22px;
                    margin-bottom: 20px;
                }


            .tbl_agreement {
                width: 100%;
            }

                .tbl_agreement tr td {
                    height: 44px;
                    vertical-align: middle;
                }

                .tbl_agreement .confirm {
                    font-size: 15px;
                    color: #333;
                    text-align: left;
                }

            .agree_check {
                color: red;
            }
        </style>


      
        <div class="pop_type1 w980">
            <div class="pop_title">
                <span>컴패션비전트립 참가신청ㆍ동의서</span>
                <button class="pop_close" ng-click="tripApplyModal.close($event)">
                    <span>
                        <img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
            </div>

            <div class="pop_content">
                <div class="pop_common"> 
                    
        <script type="text/javascript">

            $(function () {
                //$page.init();

                $("input [type='text']").prop({ "readonly": true });
                $("input [type='checkbox']").prop({ "disabled": true });
                $('.custom_sel').selectbox('disable');


                if ($("#ddlRoomType option:selected").val() == "S") {
                    $("#txtRoomDetail").hide();
                } else {
                    $("#txtRoomDetail").show();
                }
                if ($("#ddlCashReceiptType option:selected").val() == "M") {
                    $("#txtCashReceiptRelation").hide(); 
                } else {
                    $("#txtCashReceiptRelation").show(); 
                }
            });
        </script>
         
                    <%--<div class="page_tit">
                        <div class="titArea">
                            <h1><em>컴패션비전트립 참가신청ㆍ동의서</em></h1>
                            <span class="desc">컴패션어린이들과 함께할 설레는 시간을 계획해 보세요</span>
                        </div>
                    </div>--%>

                    <div class="subContents member">
                        <div class="w800 trip_sch">
                            <div class="tab_info">
                                <span class="s_con3 fc_red mb20">작성된 내용을 수정하거나 신청을 취소하기 원하실 경우 컴패션으로 직접 연락을 주셔야 합니다.
                                </span>
                                <span class="nec_info">표시는 필수입력 사항입니다.</span>
                            </div>

                            <div class="input_div">
                                <div class="login_field">
                                    <span>참가트립</span>
                                </div>
                                <div class="login_input">
                                    <table class="tbl_join">
                                        <caption>신청서 정보입력</caption>
                                        <colgroup> 
                                            <col style="width: /" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input type="text" id="txtTripSchedule" runat="server" maxlength="10" class="input_type2" value="" style="width: 200px;" readonly="readonly" disabled="disabled" />
                                                    <input type="text" id="txtVisitCountry" runat="server" maxlength="10" class="input_type2" value="" style="width: 200px;" readonly="readonly" disabled="disabled" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div>
                                                        <span class="s_con5">트립 일정과 방문 국가를 확인했습니다.</span>
                                                        <span class="sel_type2" style="width: 100px;">
                                                            <label for="s_country" class="hidden">일정확인</label>
                                                            <select runat="server" id="ddlScheduleCheck" class="custom_sel" style="width:100px;">
                                                                <option text="--선택--" value="" disabled></option>
                                                                <option text="Yes" value="Y" disabled></option>
                                                                <option text="No" value="N" disabled></option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <span class="guide_comment2" data-id="check_tripschedule" style="display: none"></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="input_div">
                                <div class="login_field">
                                    <span>참가자정보</span>
                                </div>
                                <div class="login_input">
                                    <table class="tbl_join">
                                        <caption>신청서 정보입력</caption>
                                        <colgroup> 
                                            <col style="width: /" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div>
                                                        <input type="text" id="txtName" runat="server" class="input_type2" value="" style="width: 150px;" readonly="readonly" disabled="disabled" />
                                                        <input type="text" id="txtNameEng" runat="server" maxlength="50" class="input_type2" value="" style="width: 150px;" placeholder="영문명(여권과 동일)" readonly="readonly"/>
                                                        <input type="text" id="txtBirth" runat="server" maxlength="10" class="input_type2" value="" style="width: 150px;" readonly="readonly" disabled="disabled" />
                                                        <p class="s_con2">영문명 기재 시 여권과 동일, 알파벳 대문자로 기재해 주세요(성+이름)</p>
                                                        <p class="s_con2">
                                                            미성년자(출발일 기준 만 19세 미만)의 경우 부모동의서, 위임장, 영문주민등록등본(원본)을 첨부/제출하셔야 합니다(추후 첨부/제출 가능).</p>
                                                        <p>
                                                            <a class="btn_s_type4" ng-click="download('/files/visiontrip/doc/미성년자_비전트립 참가 부모동의서.pdf')" target="_blank"><span class="ic_down"></span>부모동의서</a>
                                                            <a class="btn_s_type4" ng-click="download('/files/visiontrip/doc/미성년자_비전트립 위임장.pdf')" target="_blank"><span class="ic_down"></span>위임장</a>
                                                        </p>
                                                    </div>
                                                    <span class="guide_comment2" data-id="check_engname" style="display: none"></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="input_div divUserInfo">
                                <div class="login_field">
                                    <span>주소 및 연락처</span>
                                </div>
                                <div class="login_input">
                                    <table class="tbl_join">
                                        <caption>신청서 정보입력 - 주소 및 연락처</caption>
                                        <colgroup>
                                            <%--<col style="width: 400px" />--%>
                                            <col style="width: /" />
                                        </colgroup>
                                        <tbody>

                                            <tr>
                                                <td>
                                                    <p class="s_con9">해당 정보는  비전트립 관련 안내에 한하여 사용되며 웹 등록 정보와 불일치 하여도 무관합니다. </p>
                                                    <input type="text" id="txtPhone" runat="server" maxlength="13" class="input_type2 number_only" value="" style="width: 400px;" readonly="readonly"/>
                                                    <span class="guide_comment2" data-id="check_phone" style="display: none"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" id="txtEmail" runat="server" maxlength="50" class="input_type2" value="" style="width: 400px;"  readonly="readonly"/>
                                                    <span class="guide_comment2" data-id="check_email" style="display: none"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div>
                                                        <input type="hidden" runat="server" id="locationType" />
                                                        <input type="hidden" runat="server" id="hfAddressType" value="" />

                                                        <input type="hidden" runat="server" id="zipcode" />
                                                        <input type="hidden" id="addr1" runat="server" />
                                                        <input type="hidden" id="addr2" runat="server" />
                                                        <input type="hidden" runat="server" id="dspAddrJibun" value="" />
                                                        <input type="hidden" runat="server" id="dspAddrDoro" value="" />

                                                    </div>


                                                    <span class="radio_ui">
                                                        <input type="radio" runat="server" class="rd_addr css_radio" id="addr_domestic" name="addr_location" disabled="disabled" />
                                                        <label for="addr_domestic" class="css_label">국내</label>

                                                        <input type="radio" runat="server" class="rd_addr css_radio" id="addr_oversea" name="addr_location" disabled="disabled" />
                                                        <label for="addr_oversea" class="css_label ml20">해외</label>

                                                    </span>

                                                    <span id="pn_addr_domestic" runat="server">
                                                        <input type="hidden" id="addr_domestic_zipcode" />
                                                        <input type="hidden" id="addr_domestic_addr1" />
                                                        <input type="hidden" id="addr_domestic_addr2" />

                                                        <p id="addr_road" class="mt15" runat="server"></p>
                                                        <p id="addr_jibun" class="mt10" runat="server"></p>
                                                    </span>

                                                    <!-- 해외주소 체크 시 -->
                                                    <div id="pn_addr_overseas" runat="server" style="width: 400px;" class="mt15">
                                                        <span>
                                                            <label for="addr_overseas_zipcode" class="hidden">국가</label>
                                                            <input type="text" id="addr_overseas_country" runat="server" class="input_type2 " value="" style="width: 195px" placeholder="국가" />
                                                        </span>
                                                        <span>
                                                            <label for="addr_overseas_zipcode" class="hidden">우편번호</label>
                                                            <input type="text" id="addr_overseas_zipcode" runat="server" class="input_type2" value="" style="width: 195px" placeholder="우편번호" />
                                                        </span>

                                                        <p id="addr_overseas_addr1" class="mt15" runat="server"></p>
                                                        <p id="addr_overseas_addr2" class="mt10" runat="server"></p>
                                                    </div>
                                                    <!--// --> 

                                                    <div class="mt10">
                                                        <span class="s_con5">후원과 관련된 모든 안내/정보를 위 정보로 변경하기 원하십니까?</span>
                                                        <span class="sel_type2" style="width: 100px;">
                                                            <label for="s_country" class="hidden">기존정보변경</label>
                                                            <select runat="server" id="ddlInfoChange" class="custom_sel" width="100">
                                                                <option text="--선택--" value=""></option>
                                                                <option text="Yes" value="Y"></option>
                                                                <option text="No" value="N"></option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <span class="guide_comment2" data-id="check_infochange" style="display: none"></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="input_div divNation">
                                <div class="login_field">
                                    <span>국적</span>
                                </div>
                                <div class="login_input ">
                                    <table class="tbl_join">
                                        <caption>신청서 정보입력 - 국적</caption>
                                        <colgroup>
                                            <%--<col style="width: 400px" />--%>
                                            <col style="width: /" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div>
                                                        <span class="sel_type2" style="width: 120px;">
                                                            <label for="s_country" class="hidden">국적 선택</label>
                                                            <select runat="server" id="ddlNation" class="custom_sel" width="120">
                                                                <option text="--선택--" value=""></option>
                                                                <option text="대한민국" value="K"></option>
                                                                <option text="기타" value="E"></option>
                                                            </select>
                                                        </span>

                                                        <span class="guide_comment2" data-id="check_nation" style="display: none"></span>
                                                    </div>
                                                    <p class="s_con2">외국인 후원자의 경우, 외국인거소증 사본(앞, 뒷면)을 첨부/제출해 주셔야 합니다.</p>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="input_div divReligion">
                                <div class="login_field">
                                    <span>종교</span>
                                </div>
                                <div class="login_input">
                                    <table class="tbl_join">
                                        <caption>신청서 정보입력 - 종교</caption>
                                        <colgroup>
                                            <col style="width: 120px" />
                                            <col style="width: /" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div>
                                                        <span class="sel_type2" style="width: 120px;">
                                                            <label for="s_country" class="hidden">종교선택</label>
                                                            <select runat="server" id="ddlReligion" class="custom_sel" width="120">
                                                                <option text="--선택--" value=""></option>
                                                                <option text="기독교" value="Christian"></option>
                                                                <option text="불교" value="Buddhist"></option>
                                                                <option text="천주교" value="Catholic"></option>
                                                                <option text="무교" value="Atheism"></option>
                                                                <option text="기타" value="Etc"></option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <input type="text" id="txtChurch" runat="server" maxlength="25" class="input_type2" value="" style="width: 400px;" placeholder="교회명" readonly="readonly"/>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <span class="guide_comment2" data-id="check_religion" style="display: none"></span>
                                    <span class="guide_comment2" data-id="check_church" style="display: none"></span>
                                </div>
                            </div>

                            <div class="input_div divGroupField">
                                <table style="width: 100%">
                                    <colgroup>
                                        <col style="width: 33%;" />
                                        <col style="width: 33%;" />
                                        <col style="width: 33%;" />
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="login_field">
                                                    <span>영어회화 능력</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="login_field">
                                                    <span>비전트립 참가이력</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="login_field">
                                                    <span>직업</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div>
                                                    <span class="sel_type2" style="width: 120px;">
                                                        <label for="s_country" class="hidden">영어회화 능력 선택</label>
                                                        <select runat="server" id="ddlEnglishLevel" class="custom_sel" style="width:120px">
                                                            <option text="--선택--" value=""></option>
                                                            <option text="상" value="T"></option>
                                                            <option text="중" value="M"></option>
                                                            <option text="하" value="B"></option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <span class="guide_comment2 mt5" data-id="check_englishlevel" style="display: none"></span>
                                            </td>

                                            <td>
                                                <div>
                                                    <span class="sel_type2" style="width: 120px;">
                                                        <label for="s_country" class="hidden">비전트립 참가이력 선택</label>
                                                        <select runat="server" id="ddlVTHistory" class="custom_sel" style="width:120px">
                                                            <option text="--선택--" value=""></option>
                                                            <option text="처음" value="Z"></option>
                                                            <option text="1회" value="F"></option>
                                                            <option text="2회" value="S"></option>
                                                            <option text="3회 이상" value="T"></option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <span class="guide_comment2 mt5" data-id="check_visiontriphistory" style="display: none"></span>
                                            </td>
                                            <td>
                                                <div>
                                                    <input type="text" id="txtJob" runat="server" maxlength="100" class="input_type2 mb5" value="" style="width: 200px;" placeholder="직업" readonly="readonly"/>
                                                </div>
                                                <span class="guide_comment2 mt5" data-id="check_job" style="display: none"></span>
                                            </td>
                                        </tr>
                                </table>
                            </div>
                            <div class="input_div divMilitary">
                                <div class="login_field">
                                    <span>병역</span>
                                </div>
                                <div class="login_input">
                                    <table class="tbl_join">
                                        <caption>신청서 정보입력 - 병역</caption>
                                        <colgroup> 
                                            <col style="width: /" />
                                        </colgroup>
                                        <tbody>
                                            <tr>

                                                <td>
                                                    <div>
                                                        <span class="sel_type2" style="width: 120px;">
                                                            <label for="s_country" class="hidden">병역 선택</label>
                                                            <select runat="server" id="ddlMilitary" class="custom_sel" width="120">
                                                                <option value="">--선택--</option>
                                                                <option value="F">필</option>
                                                                <option value="U">미필</option>
                                                                <option value="N">해당사항없음</option>
                                                            </select>
                                                        </span>
                                                        <span class="guide_comment2" data-id="check_military" style="display: none"></span>
                                                    </div>
                                                    <p class="s_con2">
                                                        병역미필자의 경우 병무청에서 허가를 받으셔야 하며, 개인마다 경우가 다르므로 미리 확인하시기 바랍니다. 
                                                        <br />
                                                        (<a href="http://www.mma.go.kr/index.do">병무청 사이트 바로가기</a>)
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="input_div divEmergencyContact">
                                <div class="login_field">
                                    <span>비상연락처</span>
                                </div>
                                <div class="login_input">
                                    <table class="tbl_join">
                                        <caption>신청서 정보입력 - 비상연락처</caption>
                                        <colgroup>
                                            <%--<col style="width: 400px" />--%>
                                            <col style="width: /" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input type="text" id="txtEmergencyContactName" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="비상연락처 - 성함" />
                                                    <span class="guide_comment2" data-id="check_emergencycontactname" style="display: none"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" id="txtEmergencyContactTel" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="비상연락처 - 휴대번호" />

                                                    <span class="guide_comment2" data-id="check_emergencycontacttel" style="display: none"></span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" id="txtEmergencyContactRelation" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="비상연락처 - 참가자와의 관계" />

                                                    <span class="guide_comment2" data-id="check_emergencycontactrelation" style="display: none"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="input_div divRoom">
                                <div class="login_field">
                                    <span>방배정</span>
                                </div>
                                <div class="login_input">
                                    <table class="tbl_join">
                                        <caption>신청서 정보입력 - 방배정</caption>
                                        <colgroup>
                                            <col style="width: 120px" />
                                            <col style="width: /" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <span class="sel_type2" style="width: 120px;">
                                                        <label for="s_country" class="hidden">방배정 선택</label>
                                                        <select runat="server" id="ddlRoomType" class="custom_sel" width="120">
                                                            <option text="--선택--" value=""></option>
                                                            <option text="싱글" value="S"></option>
                                                            <option text="트윈" value="T"></option>
                                                        </select>
                                                    </span>
                                                </td>
                                                <td>
                                                    <input type="text" id="txtRoomDetail" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="원하시는 룸메이트가 있는 경우 룸메이트의 성함을 기재해 주세요" />
                                                    <span class="guide_comment2" data-id="check_roomdetail" style="display: none"></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div><span class="guide_comment2" data-id="check_roomtype" style="display: none"></span></div>
                                    <p class="s_con2">
                                        신청 순서대로 숙소가 배정되어 트윈룸을 신청하셨어도 싱글룸으로 배정될 수 있으며, 싱글룸 사용시 20~30만원 정도 추가됩니다
                                    </p>
                                </div>
                            </div>
                            <div class="input_div divChildMeet">
                                <div class="login_field">
                                    <span>후원어린이
                            <br />
                                        만남</span>
                                </div>
                                <div class="login_input">
                                    <table class="tbl_join">
                                        <caption>신청서 정보입력 - 후원어린이만남</caption>
                                        <colgroup>
                                            <col style="width: 120px" />
                                            <col style="width: /" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <span class="sel_type2" style="width: 120px;">
                                                        <label for="s_country" class="hidden">후원어린이 만남 여부 선택</label>
                                                        <select runat="server" id="ddlChildMeetYn" class="custom_sel" width="120">
                                                            <option text="--선택--" value=""></option>
                                                            <option text="예" value="Y"></option>
                                                            <option text="아니오" value="N"></option>
                                                        </select>
                                                    </span>

                                                </td>
                                                <td> 
                                                    <div ng-repeat="item in tripApplyModal.childmeet_list" class="mb5">
                                                        <input type="text" class="input_type2" style="width: 150px;" value="{{item.childkey}}" readonly="readonly"/>
                                                        <input type="text" class="input_type2" style="width: 150px;" value="{{item.childname}}" readonly="readonly"/> 
                                                    </div> 
                                                </td>
                                            </tr> 
                                        </tbody>
                                    </table>
                                    <div><span class="guide_comment2" data-id="check_childmeetyn" style="display: none"></span></div>
                                    <p class="s_con2">
                                        방문국가에 거주하는 후원어린이만남만 가능합니다.
                                    </p>
                                    <p class="s_con2">
                                        후원어린이만남을 위한 추가비용(어린이/보호자/어린이센터 선생님의 교통, 숙박, 식사 비용 등)은 후원자님께서 부담하시게 됩니다.
                                    </p>
                                    <p class="s_con2">
                                        컴패션 규정에 따라 본인 또는 가족이 후원하는 어린이 만남만 가능합니다. 가족 후원자의 어린이를 만나실 경우 후원자 동의서와 가족관계증명서류를 첨부/제출하셔야 합니다(추후 첨부/제출 가능).
                                        <a class="btn_s_type4" ng-click="download('/files/visiontrip/doc/후원어린이만남 동의서_비전트립.pdf')"><span class="ic_down"></span>후원자 동의서</a> 
                                    </p>
                                </div>
                            </div>
                            <div class="input_div divCashReceipt">
                                <div class="login_field">
                                    <span>현금영수증<br />
                                        발급</span>
                                </div>
                                <div class="login_input">
                                    <table class="tbl_join">
                                        <caption>신청서 정보입력 - 현금영수증 발급</caption>
                                        <colgroup> 
                                            <col style="width: /" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <span class="sel_type2" style="width: 120px;">
                                                        <label for="s_country" class="hidden">현금영수증 발급 선택</label>
                                                        <select runat="server" id="ddlCashReceiptType" class="custom_sel" width="120">
                                                            <option value="M">본인</option>
                                                            <option value="O">타인</option>
                                                        </select>
                                                    </span><span class="guide_comment2" data-id="check_cashreceipttype" style="display: none"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" id="txtCashReceiptName" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="현금영수증 발급자 성함" />
                                                    <span class="guide_comment2" data-id="check_cashreceiptname" style="display: none"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" id="txtCashReceiptTel" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="현금영수증 발급자 휴대번호" />
                                                    <span class="guide_comment2" data-id="check_cashreceipttel" style="display: none"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" id="txtCashReceiptRelation" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="참자가와의 관계" />
                                                    <span class="guide_comment2" data-id="check_cashreceiptrelation" style="display: none"></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p class="s_con2">
                                        왕복항공권에 한하여 현금영수증 발급이 가능하며, 현지화로 환전하여 사용하는 금액은 현금영수증 발급이 불가합니다.
                                    </p>
                                    <p class="s_con2">
                                        현금영수증 발급자는 참가자 본인이 아니어도 가능하며 서류 제출 후 변경은 불가합니다.
                                    </p>
                                </div>
                            </div>
                <!-- 약관동의 -->
                <div class="input_div divAgreement">
                    <div class="login_field"><span>약관동의</span></div>
                    <div class="login_input">
                        <span class="guide_comment2 mb5" data-id="check_agree" style="display: none"></span>
                        <div class="box mb5" style="height: 368px;">
                            <table class="tbl_agreement">
                                <caption>신청서 정보입력 - 약관동의</caption>
                                <colgroup>
                                    <col style="width: 270px" />
                                    <col style="width: 120px" />
                                    <col style="width: /" />
                                </colgroup>
                                <tbody>
                                    <%--<tr>
                                        <td><span class="sp_agree1">비전트립 가이드자료</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(1)" ng-click="agreementModal($event, 1)">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree1">
                                                <input type="hidden" id="hdnAgree1" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree1" type="checkbox" runat="server" onclick="agree_checkbox(1)">
                                                <label class="css_label font1" for="chkAgree1"></label>
                                            </span>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td><span class="sp_agree1">어린이보호서약서</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(1)" ng-click="agreementModal.show($event, 1, '어린이보호서약서')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree1">
                                                <input type="hidden" id="hdnAgree1" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree1" type="checkbox" runat="server" onclick="agree_checkbox(1)">
                                                <label class="css_label font1" for="chkAgree1"></label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree2">개인정보의 수집 및 활용</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(2)" ng-click="agreementModal.show($event, 2, '개인정보의 수집 및 활용')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree2">
                                                <input type="hidden" id="hdnAgree2" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree2" type="checkbox" runat="server" onclick="agree_checkbox(2)">
                                                <label class="css_label font1" for="chkAgree2"></label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree3">사진 및 영상자료 공유 </span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(3)" ng-click="agreementModal.show($event, 3, '사진 및 영상자료 공유')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree3">
                                                <input type="hidden" id="hdnAgree3" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree3" type="checkbox" runat="server" onclick="agree_checkbox(3)">
                                                <label class="css_label font1" for="chkAgree3"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree4">일정변경 또는 취소</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(4)" ng-click="agreementModal.show($event, 4, '일정변경 또는 취소')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree4">
                                                <input type="hidden" id="hdnAgree4" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree4" type="checkbox" runat="server" onclick="agree_checkbox(4)">
                                                <label class="css_label font1" for="chkAgree4"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree5">환불규정</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(5)" ng-click="agreementModal.show($event, 5, '환불규정')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree5">
                                                <input type="hidden" id="hdnAgree5" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree5" type="checkbox" runat="server" onclick="agree_checkbox(5)">
                                                <label class="css_label font1" for="chkAgree5"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree6">예방접종</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(6)" ng-click="agreementModal.show($event, 6, '예방접종')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree6">
                                                <input type="hidden" id="hdnAgree6" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree6" type="checkbox" runat="server" onclick="agree_checkbox(6)">
                                                <label class="css_label font1" for="chkAgree6"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree7">기타사항</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(7)" ng-click="agreementModal.show($event, 7, '기타사항')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree7">
                                                <input type="hidden" id="hdnAgree7" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree7" type="checkbox" runat="server" onclick="agree_checkbox(7)">
                                                <label class="css_label font1" for="chkAgree7"></label>
                                            </span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="box divAgreementConfirm">
                            <table class="tbl_agreement">
                                <caption>신청서 정보입력 - 약관동의</caption>
                                <colgroup>
                                    <col style="width: 380px" />
                                    <col style="width: /" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="confirm">
                                            <p>
                                                상기 본인은 컴패션 비전트립 모든 안내 및 동의사항을<br />
                                                숙지하고 이를 준수하고 따를 것을 동의하며
                                                <br />
                                                해당 비전트립 참가를 신청합니다.
                                            </p>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgreeFInal">
                                                <input class="css_checkbox agreecheck_change" id="chkAgreeFinal" type="checkbox" runat="server" ><label class="css_label font1" for="chkAgreeFinal"></label>
                                            </span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--// -->


                            <div class="input_div">
                                <p class="s_con3 ml30 mb20">
                                    여권 첨부가 어려우신 경우 아래 ‘제출하기’ 버튼을 클릭하여 작성된 신청서를 제출하여 주시고, 추후 여권사본을 첨부 또는 제출하여 주시기를 부탁 드립니다.
                                </p>
                                <div>
                                    <div class="login_field no">
                                        <label for="attach">여권사본첨부</label>
                                    </div>
                                    <div class="login_input">  
                                        <table class="tbl_join">
                                            <caption>첨부파일 등록 테이블</caption>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="btn_attach clear2 relative">
                                                            <a ng-hide="(tripApplyModal.item.passprot_name == undefined)" ng-click="download(tripApplyModal.item.passport_path)" ng-href="{{tripApplyModal.item.passport_path}}" class="btn_s_type4 mr10" href="{{tripApplyModal.item.passport_path}}" target="_blank">{{tripApplyModal.item.passprot_name}}</a>
                                                            <div ng-show="tripApplyModal.item.passprot_name == undefined">등록된 첨부파일이 없습니다.</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table> 
                                    </div>
                                </div>
                                <div>
                                    <div class="login_field no">
                                        <label for="attach">기타파일첨부</label>
                                    </div>
                                    <div class="login_input">
                                        <table class="tbl_join">
                                            <caption>첨부파일 등록 테이블</caption>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div ng-repeat="item in tripApplyModal.etcattach_list">
                                                             <a ng-click="download(item.attachpath)" ng-href="{{item.attachpath}}" target="_blank" class="btn_s_type4 mb5">{{item.attachname}}</a>
                                                        </div> 
                                                        <div ng-hide="tripApplyModal.etcattach_list.length">등록된 첨부파일이 없습니다.</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p class="s_con2">여권만료일은 비전트립 후 한국 입국일 기준 6개월 이상, 여권 잔여 페이지는 최소 3페이지 이상 남아있어야 합니다. </p>
                                        <p class="s_con2">외국인국적자, 만 15세 이상 미성년자, 가족 후원어린이만남의 경우 추가 서류를 첨부하여 주세요. </p>
                                        <p class="s_con2">첨부가 어려우신 경우, 이메일(visiontrip@compassion.or.kr) 또는 팩스(02-3668-3501)로 제출해 주세요.</p>
                                    </div>
                                </div>
                            </div>


                            <div class="input_div agreement">
                                <p class="confirm">비전트립 가이드자료 및 약관을 잘 숙지하고 이해하였으며, 위 내용에 동의합니다.</p>

                                <div class="tac">
                                    <%--<a class="btn_type1" id="btn_submit" ng-click="applysubmit()">제출하기</a>--%>
                                </div>
                            </div>

                            <div class="contact mb10"><span>회원가입이 어려우시거나 본인확인/인증이 안되시는 경우 한국컴패션으로 연락 주시기 바랍니다. <em>후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</em></span></div>

                        </div>

                    </div>
                                         
                </div>
            </div>
        </div>


    </div>
</div>
