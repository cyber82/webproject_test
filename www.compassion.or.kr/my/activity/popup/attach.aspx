﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="attach.aspx.cs" Inherits="my_activity_popup_attach" %>

<div style="background: transparent;" class="fn_pop_container" id="tripAttach">
    <div class="pop_type1 w600 fn_pop_content" style="margin-top: 50px; margin-bottom: 50px;">

        <!-- 일반팝업 width : 800 -->
        <div class="pop_type1 w600">
            <div class="pop_title">
                <span>첨부 파일</span>
                <button class="pop_close" ng-click="tripAttachModal.close($event)">
                    <span>
                        <img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
            </div>
            <script type="text/javascript">
                $(function () {

                });
            </script>
            <style>
                /*.regist_img {background:url('/common/img/btn/del_4.png') no-repeat;}*/
                .regist_img{ width:34px;height:34px;text-indent:-9999px;background:url('/common/img/btn/del.png') no-repeat;}
            </style>
            <div class="pop_content"> 
                <div class="pop_common">
                    <div ng-show="tripAttachModal.showPassport">
                        <p class="sub_tit">여권사본</p>
                        <table class="tbl_type7">
                            <tr>
                                <td>
                                    <a   ng-show="tripAttachModal.passportYN" ng-click="download(tripAttachModal.item.passport_path)" ng-href="{{tripAttachModal.item.passport_path}}" class="btn_s_type4 mr10" href="{{tripAttachModal.item.passport_path}}" target="_blank">{{tripAttachModal.item.passprot_name}}</a>
                                    <a href="javascript:void(0);" class="btn_type8" id="btnPassportModify" ng-show="tripAttachModal.showChangePassport" ng-click="tripAttachModal.attachShow($event)"><span>여권사본 수정</span></a>
                                </td>
                            </tr>
                            <tr ng-show="tripAttachModal.showAttachPassport">
                                <td>
                                    <div class="" ng-show="tripAttachModal.showAttachPassport"> 
                                        <div class="btn_attach clear2 relative">
                                            <input type="text" runat="server" data-id="path_btn_passportfile" value="" class="input_type2 fl mr10" style="width: 350px;" readonly="readonly" />
                                            <label for="lb_passport_path" class="hidden"></label>
                                            <input type="hidden" runat="server" id="path_btn_passportfile" value="" />
                                            <a href="javascript:void(0);" class="btn_type8 fl" id="btn_passportfile"><span>파일선택</span></a>
                                            <a href="javascript:void(0);" class="btn_type8 fl ml5" id="btnPassportDel"><span>삭제</span></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table> 
                    </div>
                </div>

                <div ng-show="tripAttachModal.showEtc">
                    <p class="sub_tit">기타서류</p> 

                    <table class="tbl_type7 padding1">
                        <caption>신청서 첨부파일 테이블</caption>
                        <tbody>
                            <tr ng-repeat="attach_item in tripAttachModal.etcattach_list">
                                <td ng-id="{{attach_item.attachid}}">
                                    <div>
                                        <a ng-click="download(attach_item.attachpath)" ng-href="{{attach_item.attachpath}}" target="_blank" class="btn_s_type4 mr10">{{attach_item.attachname}}</a>
                                        <button ng-show="tripAttachModal.showAttachEtc" class="regist_img" ng-class="del" ng-click="tripAttachModal.deleteAttach($event, attach_item.attachid, $index)"><span>삭제</span></button>


                                        <%--<a href="javascript:void(0);" class="btn_type8 ml5" ng-show="tripAttachModal.showAttachEtc" ng-click="tripAttachModal.deleteAttach($event, attach_item.attachid, $index)"><span>삭제</span></a>--%>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <%--ng-click="download(attach_item.attachpath)"--%> 
                    <div ng-show="tripAttachModal.showAttachEtc" class="mb30">
                        <a ng-click="tripAttachModal.addInputFiles()" ng-href="" class="btn_type8 mb5">+첨부파일추가</a>
                        <div ng-repeat="input in tripAttachModal.input_fils" class="mb5" on-finish-render="ngRepeatFinished">
                            <input type="text" runat="server" ng-model="input.filename" data-id="path_btn_etcfile{{input.key}}" value="" class="input_type2 fl mr10" style="width: 350px;" readonly="readonly" />
                            <input type="hidden" id="path_btn_etcfile{{input.key}}" ng-model="input.filepath" value="" />

                            <a href="javascript:void(0);" class="btn_type8 fl" id="btn_etcfile{{input.key}}"><span>파일선택</span></a>
                            <a ng-click="tripAttachModal.removeInputFiles($index)" class="btn_type8 ml5">삭제</a>
                        </div>
                    </div>
                </div> 

                <div class="tac">
                    <a href="javascript:void(0);" ng-click="tripAttachModal.request($event)" ng-hide="tripAttachModal.readOnly" class="btn_type1">제출하기</a>
                </div>
            </div>
        </div>
        <!--// popup -->
    </div>
</div>
