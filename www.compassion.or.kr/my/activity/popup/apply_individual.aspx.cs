﻿using Microsoft.AspNet.FriendlyUrls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CommonLib;

public partial class participation_visiontrip_form_apply_individual : FrontBasePage
{
    const string listPath = "/participation/visiontrip/individual";

    private string strUserID = string.Empty;
    private string strSponserID = string.Empty;
    private int iApplyID = 0;

    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        if (!UserInfo.IsLogin)
        {
            Response.ClearContent();
            return;
        }

        UserInfo sess = new UserInfo();
        strUserID = sess.UserId;
        strSponserID = sess.SponsorID;

        // 이름
        txtName.Value = sess.UserName;
        txtBirth.Value = sess.Birth != "" ? sess.Birth.Substring(0, 10) : "";
        txtEmail.Value = sess.Email;
        //txtPhone.Value = sess.Phone;

        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1)
        {
            Response.Redirect(listPath, true);
        }
        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);

        //parameter
        base.PrimaryKey = requests[0];

        bool isNum = Int32.TryParse(PrimaryKey.ToString(), out iApplyID); 

        GetApply(); 
    }


    protected void GetApply()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var applyData = dao.tVisionTripApply.First(p => p.ApplyID == iApplyID && p.UserID == strUserID && p.CurrentUse == 'Y');
            var applyData = www6.selectQF<tVisionTripApply>("ApplyID", iApplyID, "UserID", strUserID, "CurrentUse", "Y");

            if (applyData != null)
            {
                //var applyDetailData = dao.tVisionTripIndividualDetail.FirstOrDefault(p => p.ApplyID == iApplyID);
                var applyDetailData = www6.selectQF<tVisionTripIndividualDetail>("ApplyID", iApplyID);
                
                ddlVisitType.SelectedValue = applyDetailData.VisitType.ToString(); 
                txtVisitDate1.Value = applyDetailData.VisitDate1.ToString();
                txtVisitDate2.Value = applyDetailData.VisitDate2.ToString();

                //var coutry = dao.country.FirstOrDefault(p => p.c_id == applyDetailData.VisitCountry.ToString());
                var coutry = www6.selectQF<country>("c_id", applyDetailData.VisitCountry.ToString());
                txtVisitCountry.Value = coutry.c_name.ToString();
                
                txtNameEng.Value = applyData.SponsorNameEng;
                txtPhone.Value = applyData.Tel.Decrypt();
                txtEmail.Value = applyData.Email.Decrypt();

                #region 주소 관련 
                locationType.Value = applyData.LocationType;

                addr_domestic.Checked = applyData.LocationType == "국내";
                addr_oversea.Checked = applyData.LocationType != "국내";

                var addr_split = "";
                var addr_country = "";
                if (applyData.Address1.Decrypt().Contains("$"))
                {
                    string[] addr_location = applyData.Address1.Decrypt().Split('$');
                    addr_split = addr_location[0];
                    addr_country = addr_location[1];
                }

                pn_addr_domestic.Visible = false;
                pn_addr_overseas.Visible = false;
                if (applyData.LocationType == "국내")
                {
                    pn_addr_domestic.Visible = true;
                    if (addr_split.Contains("//"))
                    {
                        string splitnew = addr_split.Replace("//", "^");

                        string[] addr_gubn = splitnew.Split('^');
                        addr_road.InnerText = "[도로명주소] (" + applyData.ZipCode.Decrypt() + ") " + addr_gubn[0] + applyData.Address2.Decrypt().ToString();
                        addr_jibun.InnerText = "[지번주소] (" + applyData.ZipCode.Decrypt() + ") " + addr_gubn[1] + applyData.Address2.Decrypt().ToString();
                        zipcode.Value = applyData.ZipCode.Decrypt().ToString();

                    }
                }
                else
                {
                    pn_addr_overseas.Visible = true;

                    addr_overseas_country.Value = addr_country.ToString();
                    addr_overseas_zipcode.Value = applyData.ZipCode.Decrypt();
                    addr_overseas_addr1.InnerText = addr_split;
                    addr_overseas_addr2.InnerText = applyData.Address2.Decrypt().ToString();
                }
                #endregion
                //zipcode.Value = applyData.ZipCode.ToString();
                //addr1.Value = applyData.Address1.ToString();
                //addr2.Value = applyData.Address2.ToString();


                ddlInfoChange.SelectedValue = applyData.ChangeYN.ToString();
                 
                ddlReligion.SelectedValue = applyData.ReligionType.ToString();
                txtChurch.Value = applyData.ChurchName.ToString();
                if (applyData.ReligionType.ToString() != "Christian")
                    txtChurch.Visible = false;


                txtLocalAccommodation.Value = applyDetailData.LocalAccommodation.ToString();
                txtLocalTel.Value = applyDetailData.LocalTel.ToString();
                txtLocalAddress.Value = applyDetailData.LocalAddress.ToString(); 

                txtDepartureDate.Value = applyDetailData.DepartureDate.ToString();
                txtReturnDate.Value = applyDetailData.ReturnDate.ToString(); 

                txtEmergencyContactName.Value = applyData.EmergencyContactName.ToString();
                txtEmergencyContactTel.Value = applyData.EmergencyContactTel.ToString();
                txtEmergencyContactRelation.Value = applyData.EmergencyContactRelation.ToString();

                //동반인 정보
                //var list = dao.tVisionTripCompanion.Where(p => p.IndividualDetailID == applyDetailData.IndividualDetailID).ToList();
                var list = www6.selectQ<tVisionTripCompanion>("IndividualDetailID", applyDetailData.IndividualDetailID);
                repeater.DataSource = list;
                repeater.DataBind();


                if (applyDetailData.AcceptTerms != null && applyDetailData.AcceptTerms != "000000")
                {
                    //alert(ReviewSubmit.ETHICSCHECK);
                    if (applyDetailData.AcceptTerms.Substring(0, 1).IndexOf("1") > -1) chkAgree1.Checked = true; else chkAgree1.Checked = false;
                    if (applyDetailData.AcceptTerms.Substring(1, 2).IndexOf("1") > -1) chkAgree2.Checked = true; else chkAgree2.Checked = false;  
                    if (applyDetailData.AcceptTerms.Substring(2, 3).IndexOf("1") > -1) chkAgree3.Checked = true; else chkAgree3.Checked = false;
                    if (applyDetailData.AcceptTerms.Substring(3, 3).IndexOf("1") > -1) chkAgree4.Checked = true; else chkAgree4.Checked = false;
                    if (applyDetailData.AcceptTerms.Substring(4, 2).IndexOf("1") > -1) chkAgree5.Checked = true; else chkAgree5.Checked = false;
                    if (applyDetailData.AcceptTerms.Substring(5, 1).IndexOf("1") > -1) chkAgree6.Checked = true; else chkAgree6.Checked = false;

                    chkAgree1.Disabled = true;
                    chkAgree2.Disabled = true;
                    chkAgree3.Disabled = true;
                    chkAgree4.Disabled = true;
                    chkAgree5.Disabled = true;
                    chkAgree6.Disabled = true;
                } 

            }

        }

    }
}