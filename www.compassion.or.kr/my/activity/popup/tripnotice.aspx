﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tripnotice.aspx.cs" Inherits="my_activity_popup_tripnotice"  %>
<div style="background: transparent;" class="fn_pop_container" id="tripNotice">
<div class="pop_type1 w600 fn_pop_content" style="margin-top:50px; margin-bottom: 50px;">
	
    <!-- 일반팝업 width : 800 -->
	<div class="pop_type1 w600">
		<div class="pop_title">
			<span>안내보기</span>
			<button class="pop_close" ng-click="tripNoticeModal.close($event)"><span><img src="/common/img/btn/close_1.png"  alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content">
			
			<div class="pop_common">
				<table class="tbl_type1">
                    <colgroup>
                        <col width="500" />
                    </colgroup>
                    <tr>
                        <td width="500" style="border-bottom:none;">
                            <div id="divTripNotice" style="width: 100%; overflow-x: auto;"></div> 
                        </td>
                    </tr>
                    <tr ng-show="tripNoticeModal.detailShow">
                        <td align="center">
                            <a ng-click="download(tripNoticeModal.tripnoticeDetail)" class="btn_s_type1">세부내용보기</a>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
	</div>
	<!--// popup -->
</div>
</div>