﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="certificate.aspx.cs" Inherits="my_activity_popup_certificate" %>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <script language='javascript' type='text/javascript'>
        function setPrint() {
            //if (factory.printing == null) {
            //    alert('출력시 필요한 ActiveX가 설치되어있지 않습니다.\\r\\n출력시 필요한 ActiveX를 설치해주세요.\\r\\n설치하지 않을 경우 출력시 이상이 있을 수 있습니다.');
            //    return;
            //}
            //factory.printing.header = '';
            //factory.printing.footer = '';
            //factory.printing.portrait = true;
            //factory.printing.leftMargin = 10;
            //factory.printing.topMargin = 10;
            //factory.printing.rightMargin = 10;
            //factory.printing.bottomMargin = 10;
            ////+ "     factory.printing.printBackground = true;
            //agree = confirm('현재 페이지를 출력하시겠습니까?');
            //if (agree) factory.printing.Print(false, window)
        }
    </script>
    <style type='text/css'>
        /*body { margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;}*/

        img {
            border: none;
        }

        body, td {
            font-family: '돋움', '바탕', '굴림';
            font-size: 10pt;
            color: #000000;
        }

            /* 소득세법 */
            td.CodeTitle {
                font-size: 10pt;
                padding-bottom: 5px;
            }

            td.Code {
                font-size: 8pt;
                padding-bottom: 5px;
            }
            /* 기부내용 HEAD */

            td.ContentHeadType {
                width: 96px;
                height: 25px;
                text-align: center;
                background-color: silver;
            }

            td.ContentHeadCode {
                width: 46px;
                text-align: center;
                background-color: silver;
            }

            td.ContentHeadDate {
                width: 105px;
                text-align: center;
                background-color: silver;
            }

            td.ContentHeadDesc {
                width: 105px;
                text-align: center;
                background-color: silver;
            }

            td.ContentHeadAmount {
                width: 98px;
                text-align: center;
                background-color: silver;
            }
            /* 기부내용 CONTENT */

            td.ContentItem {
                border-top: gray 0px dotted;
                border-left: gray 0px dotted;
                border-right: gray 1px solid;
                border-bottom: gray 1px dotted;
            }

            td.ContentItemBottom {
                border-top: gray 0px dotted;
                border-left: gray 0px dotted;
                border-right: gray 1px solid;
                border-bottom: gray 1px solid;
            }
        /* Title */

        .Title {
            font-size: 12pt;
            font-weight: bold;
            border-bottom: gray 1px solid;
        }
        /*  */

        .Gray_Table {
            border-color: gray;
            border-style: solid;
            border-top-width: 1px;
            border-left-width: 2px;
            border-right-width: 2px;
            border-bottom-width: 1px;
        }

        .Gray_Head {
            text-align: center;
            border-color: gray;
            border-style: solid;
            border-top-width: 0px;
            border-left-width: 0px;
            border-right-width: 0px;
            border-bottom-width: 1px;
        }

        .Gray_Head2 {
            text-align: left;
            border-color: gray;
            border-style: solid;
            border-top-width: 0px;
            border-left-width: 0px;
            border-right-width: 0px;
            border-bottom-width: 1px;
        }

        .Gray_Item {
            border-color: gray;
            border-style: solid;
            border-top-width: 0px;
            border-left-width: 0px;
            border-right-width: 0px;
            border-bottom-width: 1px;
        }
    </style>
    <title></title>
</head> 
<body style='font-family: 맑은 고딕;' onload="setPrint()">
     
        <object id="factory" style="display:none" classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" codebase='http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,429,14'></object>
<%--<object id=factory style='display:none' classid=clsid:1663ed61-23eb-11d2-b92f-008048fdd814 codebase=../Files/ScriptX.cab#Version=6,4,438,06 VIEWASTEXT>  
</object>   --%>

    <table width='800' border='0' align='center' cellpadding='0' cellspacing='0'>
        <tr>
            <td>
                <img src='/images/visiontrip/img001_1.jpg' height='100' /></td>
        </tr>
        <!--<tr>
            <td height='60'>
                <table width='750' border='0' align='center' cellpadding='0' cellspacing='0'>
                    <tr>
                        <td><span style='font-size: 9pt; font-weight:bold;'>발급번호 : " + DateTime.Now.ToString("yyyyMMddhhmmssfff") + " </span></td>

                    </tr>

                </table>
            </td>

        </tr>-->

        <tr>
            <td height='80' align='center' style='width: 800px; position: absolute; top: 50px; text-align: center;'><span style='font-size: 19pt; font-weight: bold;'>체 험 확 인 서</span></td>
        </tr>
        <tr>
            <td height='80' align='center' style='width: 800px; position: absolute; top: 80px; text-align: center;'><span style='font-size: 15pt; font-weight: bold; color: gray;'>Certification of Valuntary Service</span></td>
        </tr>

        <tr>
            <td height='60' align='center'>&nbsp;</td>

        </tr>

        <tr>
            <td>
                <table width='800' border='1' cellpadding='0' cellspacing='0' bordercolor='#000000' style='border-collapse: collapse;'>
                    <tr>
                        <td height='40' colspan='4' bgcolor='#CCCCCC' align='center'><span style='font-size: 11pt; font-weight: bold;'>&nbsp;&nbsp;참가자 인적 사항</span> &nbsp;<span style='font-size: 11px; color: gray'>Personal Information</span></td>

                    </tr>

                    <tr>
                        <td width='160' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>성 명</span>&nbsp;<span style='font-size: 11px; color: gray'>Name</span></td>

                        <td width='200' height='40' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;" + this._SponsorName + "</span></td>

                        <td width='160' height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>생년월일</span>&nbsp;<span style='font-size: 11px; color: gray'></span></td>

                        <td width='275' height='40' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;" + this._BirthDate + "</span></td>

                    </tr>

                    <tr>
                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>연락처</span>&nbsp;<span style='font-size: 11px; color: gray'>Phone</span></td>

                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;" + this._SponsorAddress + "</span></td>

                    </tr>
                    <tr>
                        <td height='40' colspan='4' bgcolor='#CCCCCC' align='center'><span style='font-size: 11pt; font-weight: bold;'>&nbsp;&nbsp;활동내역</span> &nbsp;<span style='font-size: 11px; color: gray'>The Details of Service</span></td>
                    </tr>
                    <tr>
                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>봉사기관명</span>&nbsp;<span style='font-size: 11px; color: gray'>Affiliation</span></td>
                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;사회복지법인 한국컴패션</span>&nbsp;<span style='font-size: 11px; color: gray'>Compassion Korea</span></td>
                    </tr>
                    <tr>
                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>방문국가</span>&nbsp;<span style='font-size: 11px; color: gray'>Location</span></td>
                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;</span></td>
                    </tr>
                    <tr>
                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>방문기간</span>&nbsp;<span style='font-size: 11px; color: gray'>Period of Service</span></td>
                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;</span></td>
                    </tr>
                    <tr>
                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>봉사시간</span>&nbsp;<span style='font-size: 11px; color: gray'>Service Time</span></td>
                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;</span></td>
                    </tr>
                    <tr>
                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>봉사내용</span>&nbsp;<span style='font-size: 11px; color: gray'>Details of Service</span></td>
                        <td height='40' colspan='3' bgcolor='#FFFFFF'>
                            <div style='font-size: 11pt; padding: 7px;'>
                                <div>
                                    <span style='font-size: 11px;'>&#183;&nbsp;컴패션국가사무실 방문, 컴패션어린이센터 방문 및 컴패션사역 이해
                                        <br />
                                        <span style='color: gray'>&nbsp;&nbsp;To visit Country office of Compassion, Compassion Children’s
                                            Center and understand Compassion ministry
                                        </span>
                                    </span>
                                </div>
                                <div>
                                    <span style='font-size: 11px;'>&#183;&nbsp;어린이들을 위한 봉사활동 (풍선아트, 페이스페인팅 등)
                                        <br />
                                        <span style='color: gray'>&nbsp;&nbsp;Voluntary activities for children (Balloon Art, Bubble Play etc.)
                                        </span>
                                    </span>
                                </div>
                                <div>
                                    <span style='font-size: 11px;'>&#183;&nbsp;어린이 가정, 지역사회 방문을 통한 현지 문화 및 빈곤 실태 간접 체험
                                        <br />
                                        <span style='color: gray'>&nbsp;&nbsp;To visit homes/families and witness the poverty of the community
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height='40' align='center' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>담 당 자</span>&nbsp;<span style='font-size: 11px; color: gray'>Person in charge</span></td>
                        <td height='40' colspan='3' bgcolor='#FFFFFF'><span style='font-size: 11pt;'>&nbsp;</span></td>
                    </tr>
                </table>
            </td>

        </tr>

        <tr>
            <td height='30'>&nbsp;</td>
        </tr>
        <tr>
            <td align='center' height='50'>
                <span style='font-size: 11pt;'>상기 명기된 사람은 본 기관에서 진행한 비전트립을 통하여
                    <br />
                    봉사활동을 성실히 수행하였음을 위와 같이 증명합니다.</span>
            </td>
        </tr>
        <tr>
            <td align='center' height='30'>
                <span style='font-size: 11pt; color: gray;'>This is certify that the student above has taken part<br />
                    in volunteer activities at Compattion Korea.</span>
            </td>
        </tr>
        <tr>
            <td align='center'>
                <span style='font-size: 11pt; color: gray;'>2017.08.21</span>
            </td>
        </tr>
        <tr>
            <td height='30'>&nbsp;</td>
        </tr>
        <tr>
            <td align='center'>
                <div style='display: inline-block; position: relative;'>
                    <div style='position: relative; top: -10px; left: 50px; font-size: 13pt; font-weight: bold; float: left;'>
                        <span style='font-size: 25px;'>사회복지법인 한국컴패션 대표 서정인 (인)</span>
                        <br />
                        <span style='color: gray;'>JUSTIN JUNG-IN SUH, CEO<br />
                            COMPASSION KOREA</span>
                    </div>
                </div>
                <img src='/images/visiontrip/img002.jpg' width='118' height='114'>
            </td>
        </tr>
        <tr>
            <td height='30'>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <!--<tr>
            <td height='20' style='padding-top:20px;'><span style='font-size: 9pt;'><font color='#0033FF'><b>한국컴패션</b></font> (04418) 서울시 용산구 한남대로 102-5 (한남동 723-41) 석전빌딩</span></td>

        </tr>

        <tr>
            <td height='20'><span style='font-size: 9pt;'><font color='#0033FF'><b>후원상담/안내 :</b></font> 02-740-1000 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>팩스 :</b></font> 02-740-1001 <font color='#0033FF'>&nbsp;&nbsp;&nbsp;<b>이메일 :</b></font> info@compassion.or.kr &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>www.</b></font>compassion.or.kr</span></td>

        </tr>

        <tr>
            <td>&nbsp;</td>

        </tr>
        <tr>
            <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>Compassion USA Korea Office&nbsp;&nbsp;</b></font> 14515 Valley View Ave. Ste. #C Santa Fe Springs, CA90670</span></td>

        </tr>

        <tr>
            <td height='20'><span style='font-size: 10pt;'><font color='#0033FF'><b>TEL :</b></font> 562-483-4300 &nbsp;&nbsp;&nbsp;<font color='#0033FF'><b>EMAIL :</b></font> info_us@compassion.or.kr</span></td>

        </tr>-->

    </table>
</body>
</html>
