﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cad.aspx.cs" Inherits="my_activity_cad" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/my/activity/cad.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

		<!-- 타이틀 -->
		<div class="page_tit" ng-if="AdminCK == false">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980 activity">

				 <uc:menu runat="server"  />

				<!-- content -->
				<div class="box_type4 mb40">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit mb30">
						<p class="tit">CAD 추천 관리</p>
					</div>
					<!--// -->

					<!-- CAD 추천관리리스트 -->
					
					<div class="tableWrap1 mb30">
						<table class="tbl_type6 padding2">
							<caption>CAD 추천관리 테이블</caption>
							<colgroup>
								<col style="width:17%" />
								<col style="width:35%" />
								<col style="width:16%" />
								<col style="width:16%" />
								<col style="width:16%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">추천일</th>
									<th scope="col">추천어린이</th>
									<th scope="col">추천어린이 ID</th>
									<th scope="col">결연현황</th>
									<th scope="col">결연일</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in list">
									<td>{{item.regdate | date:'yyyy.MM.dd'}}</td>
									<td class="tit">{{item.namekr }}</td>
									<td>{{item.childkey}}</td>
									<td>{{item.status}}</td>
									<td><span ng-show="item.status == '대기' || item.status == '종료'">-</span> <span ng-show="item.status == '결연'">{{item.startdate| date:'yyyy.MM.dd' }}</span></td>
								</tr>
								
								<tr ng-show="total == 0">
									<td colspan="5" class="no_content">
										등록된 내역이 없습니다.
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// CAD 추천관리리스트 -->

					<!-- page navigation -->
					<div class="tac mb30">
						<paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
					</div>
					<!--// page navigation -->

					<div class="box_type5 pl40">
						<ul>
							<li><span class="s_con2">CAD 추천 서비스는 컴패션 모바일 홈페이지를 통해 이용하실 수 있습니다.</span></li>
						</ul>
					</div>

				</div>

				<div class="cad_info" ng-if="AdminCK == false">
					<p class="s_tit3 mb15">하루 한 명, 소중한 사람들에게 컴패션 어린이를 추천해 보세요.</p>

					<a href="/advocate/together/compassion-a-day/"><img src="/common/img/page/my/btn_banner_cad.jpg" alt="CAD 소개 페이지 가기" /></a>
				</div>
				<!--// content -->

				
			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>

</asp:Content>

