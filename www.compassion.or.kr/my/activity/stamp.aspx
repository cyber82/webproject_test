﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="stamp.aspx.cs" Inherits="my_activity_stamp" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript" src="/my/activity/stamp.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit" ng-if="AdminCK == false">
            <div class="titArea">
                <h1>마이컴패션</h1>
                <span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents mypage">

            <div class="w980 activity">
                <uc:menu runat="server" />

                <!-- content -->
                <div class="box_type4 mb40">

                    <!-- 서브타이틀 -->
                    <div class="sub_tit mb30 relative">
                        <p class="tit">스탬프 투어</p>

                        <span class="sel_type1 sel_stamp" style="width: 180px">
                            <label for="year" class="hidden">년도 선택</label>
                            <select id="year" ng-model="year" ng-change="changeYear(year)" class="custom_sel" data-ng-options="item.s_year as item.s_year for item in years">
                            </select>
                        </span>
                    </div>
                    <!--// -->

                    <!-- 온라인 스탬프투어 -->
                    <div class="tbl_sort mb15">
                        <span class="s_tit3">온라인 스탬프 투어</span>


                    </div>

                    <div class="stampWrap mb30">
                        <ul class="online clear2">
                            <li class="{{item.s_id}} {{item.addClass}}" ng-repeat="item in online_list | orderBy : $index : true">
                                <span class="img"></span>
                                <br />
                                <span class="subject">{{item.s_name}}</span><br />
                                <a href="{{item.url}}" class="btn_s_type3" ng-hide="item.addClass == 'on'" ng-if ="AdminCK == false">{{item.do}}</a>
                            </li>

                        </ul>
                    </div>
                    <!--// 온라인 스탬프투어 -->

                    <!-- 출석체크 스탬프 : 출석체크 없는 경우 미노출 -->
                    <div class="tbl_sort mb15">
                        <span class="s_tit3">출석체크 스탬프</span>
                    </div>

                    <div class="stampWrap">
                        <ul class="attendance clear2">
                            <li class="common {{item.addClass}}" ng-show="item.is_earning == 'Y'" ng-repeat="item in offline_list">
                                <span class="img"></span>
                                <br />
                                <span class="subject">{{item.s_name}}</span>
                            </li>
                            
							<!-- 출석 스탬프 없을때 -->
							<li class="no_content" ng-show="!view">
								참여한 오프라인 출석 스탬프가 없습니다.
							</li>
                            

                        </ul>
                    </div>
                    <!--// 출석체크 스탬프 -->

                </div>
                <!--// content -->


            </div>

            <div class="h100"></div>
        </div>
        <!--// e: sub contents -->

    </section>

</asp:Content>

