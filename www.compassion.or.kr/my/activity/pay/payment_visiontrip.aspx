﻿<%@ Page Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeFile="payment_visiontrip.aspx.cs" Inherits="my_activity_pay_payment_visiontrip" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%--<%@ Register Src="/my/activity/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>--%>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/activity/pay/payment_visiontrip.js"></script> 
    <script>
        $(function () {


        });

        var onSubmit = function () {
            if (!($("#payment_method_cms").is(":checked") || $("#payment_method_virtual").is(":checked"))) {
                alert("결제방법을 선택해주세요.");
                return false;
            }
            else {
                return true;
            }
        };
    </script>
</asp:Content>
<asp:Content runat="server" ID="body" ContentPlaceHolderID="body"> 

	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

        <asp:HiddenField runat="server" ID="hdnApplyID" />
        <asp:HiddenField runat="server" ID="hdnPlanDetailID" />
        <asp:HiddenField runat="server" ID="hdnAmount" />
        <asp:HiddenField runat="server" ID="hdnAmountType" />
        <asp:HiddenField runat="server" ID="hdnAmountTypeCode" /> 

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>마이컴패션</h1>
                <span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents mypage">
            <div class="w980 activity">

                <uc:menu runat="server" />

                <!-- content -->
                <div class="box_type4 mb40">

                    <!-- 서브타이틀 -->
                    <div class="sub_tit mb30">
                        <p class="tit">나의 현지방문 보러가기 
                            (<asp:Literal runat="server" ID="ltTitle_type" /> 
                            납부)</p>
                    </div>
                    <!--// -->

                    
				<!-- 결제정보 -->
				<p class="table_tit" id="deliveryTitle">
					<span class="tit">결제정보</span>
					<span class="nec_info">표시는 필수입력 사항입니다.</span>
				</p> 

                    <div class="tableWrap2 mb30">
                        <table class="tbl_type1">
                            <caption>결제정보 테이블</caption>
                            <colgroup>
                                <col style="width: 20%">
                                <col style="width: 80%">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <th>구분</th>
                                    <th>
                                        <asp:Label runat="server" ID="lbAmountType"></asp:Label></th>
                                </tr>
                                <tr>
                                    <th>결제금액</th>
                                    <%--<th>{{ paymentModal.cost | number }}</th>--%>
                                    <th>
                                        <asp:Label runat="server" ID="lbAmount"></asp:Label>
                                    </th>
                                </tr>
                                <tr>
                                    <th scope="row"><span class="nec">결제방법</span></th>
                                    <td> 
                                        <span class="radio_ui">
                                            <input type="radio" id="payment_method_cms" name="payment_method" runat="server" class="css_radio radioPayment" />
                                            <label for="payment_method_cms" class="css_label" style="margin-right: 30px;">실시간 계좌이체</label> 

                                            <input type="radio" id="payment_method_virtual" name="payment_method" runat="server" class="css_radio radioPayment" />
                                            <label for="payment_method_virtual" class="css_label">가상계좌</label> 
                                        </span>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <th scope="row">발급된 가상계좌 정보</th>
                                    <td>

                                        <div class="tac">
                                            <ul class="v_account">
                                                <li ng-repeat="item in virtual_accounts">
                                                    <p class="name">{{item.bankname}}</p>
                                                    <p>{{item.account}}</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>

                                </tr>

                            </tbody>
                        </table>

                    </div>
                    <div style="display:none;">  
                        가상계좌 발급일로부터 3일간 유효합니다.
                    </div>


                    <div class="tac mb60">
                        <a runat="server" id="btnPay" onserverclick="btn_submit_Click" class="btn_type1" onclick="return onSubmit()">납부하기</a>
                        <%--<asp:LinkButton runat="server" ID="btnPay" OnClick="btn_submit_Click" class="btn_type1" OnClientClick="">납부하기</asp:LinkButton>
                        <a href="javascript:void(0);" runat="server" id="btnVirtual" class="btn_type1" style="display: none;" ng-click="modal.show($event)">납부하기</a>--%>
                    </div>


                </div>
                <!--// content -->

                <%--<div class="mb30">
                        <div style="margin: 0 auto; text-align: center;">
                            <a class="btn_s_type1" ng-href="/my/activity/visiontrip">나의 현지방문 보러가기</a>
                        </div>
                    </div>--%>
                <div class="h100"></div>

            </div>
            <!--// e: sub contents -->
        </div>

    </section>  
     
</asp:Content>
<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
    <uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" ID="kcp_form" />
</asp:Content>
