﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="virtual-account.aspx.cs" Inherits="my_activity_pay_virtual_account"  %>
	<div style="background:transparent;" class="fn_pop_container" id="modalview">

	<div class="pop_type1 w800 fn_pop_content" style="width:800px;height:1200px;padding-top:50px">
		<input type="hidden" runat="server" id="hd_banks" />
		<div class="pop_title">
			<span>가상계좌 발급</span>
			<button class="pop_close" ng-click="modal.hide($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content virtual">

            <div class="box_type3 mb20">
                <div ng-show="modal.virtual_accounts.length > 0">
                    <p class="tit">현재 발급된 <em class="fc_blue">비전트립 {{amountTypeName}} 가상계좌</em>는 아래와 같습니다.</p>

                    <div class="tac">
                        <ul class="v_account">
                            <li ng-repeat="item in modal.virtual_accounts">
                                <p class="name">{{item.bankname}}</p>
                                <p>{{item.account}}</p>
                            </li>
                        </ul>
                    </div>
                </div>
				<div class="sel_bank">
					<p>자주 거래하시는 은행을 선택해주세요.</p>

					<span class="sel_type2 mb20" style="width:400px">
						<label for="banks" class="hidden">은행 선택</label>

						<select name="banks" id="banks" class="custom_sel"></select> 
					</span>
					<br />
					
					<a href="#" ng-show="modal.instance" ng-click="modal.request($event)" class="btn_type1 mb20">가상계좌 발급신청</a>
				</div>

			</div>
			
			<%--<div class="box_type5 pl40">
				<p class="tit">가상 계좌로 후원금을 납부하실 때 꼭 기억해주세요!</p>

				<ul>
					<li><span class="s_con1">후원금 단위에 맞게 보내주셔야 후원금이 정확하게 처리됩니다.</span></li>
					<li>
						<span class="s_con1">
							선물금이나 특정 용도로 후원금을 보내시는 경우 계좌 이체 시 이체 내역에 후원금 용도를 기록해 주시거나<br />
							납부 후 한국컴패션으로 연락 주시기 바랍니다.<br />
							후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a>
						</span>
					</li>
				</ul>
			</div>--%>

		</div>
	</div>
	<!--// popup -->
	</div>

