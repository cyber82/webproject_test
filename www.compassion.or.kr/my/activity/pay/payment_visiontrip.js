﻿
(function () {
    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

        $scope.requesting = false;
        //$(".radioPayment").change(function () {  
        //    if ($("#payment_method_virtual").is(":checked")) {
        //        $("#btnPay").hide();
        //        $("#btnVirtual").show();
        //    }
        //    else {
        //        $("#btnPay").show();
        //        $("#btnVirtual").hide();
        //    }
        //})


        $scope.planDetailID = $("#hdnPlanDetailID").val();
        $scope.amountType = $("#hdnAmountTypeCode").val(); 
        $scope.amountTypeName = $("#hdnAmountType").val();


        $scope.virtual_accounts = [];
        // 발급된 가상계좌
        $http.get("/api/visiontrip.ashx?t=virtual-account", { params: { planDetailID: $scope.planDetailID, amountType: $scope.amountType } }).success(function (r) {
            console.log(r);
            if (r.success) {
                $scope.virtual_accounts = r.data;
            } else {
                alert(r.message);
            }
        });



        // 가상계좌발급 팝업
        $scope.modal = {
            instance: null,
            banks: [],
            virtual_accounts:[],
            init: function () {
                popup.init($scope, "/my/activity/pay/virtual-account", function (modal) {
                    $scope.modal.instance = modal;

                    if (getParameterByName("action") == "virtual-account")
                        $scope.modal.show();

                }, { top: 0, iscroll: true });
            },

            show: function ($event) {

                if ($event)
                    $event.preventDefault();

                if (!$scope.modal.instance)
                    return;

                $scope.modal.instance.show();
                $scope.modal.virtual_accounts = [];

                // 발급된 가상계좌
                $http.get("/api/visiontrip.ashx?t=virtual-account", { params: { planDetailID: $scope.planDetailID, amountType: $scope.amountType } }).success(function (r) {
                    console.log(r);
                    if (r.success) {
                        $scope.modal.virtual_accounts = r.data;
                    } else {
                        alert(r.message);
                    }
                });

                // 은행목록
                var banks = $.parseJSON($("#hd_banks").val());
                $("#banks").empty();

                $.each(banks, function (r) {
                    var codeId = this.CodeID;
                    var codeName = this.CodeName;

                    $("#banks").append($("<option value='" + codeId + "'>" + codeName + "</option>"));
                });


                $(".custom_sel").selectbox({});


            },

            hide: function ($event) {
                $event.preventDefault();
                if (!$scope.modal.instance)
                    return;
                $scope.modal.instance.hide();

            },

            request: function ($event) {

                $event.preventDefault();
                if ($scope.modal.virtual_accounts.length > 0) {
                    if (confirm("이미 발급된 가상 계좌가 있습니다. 재발급 받으시겠습니까?")) {

                        var bankCode = $("#banks").val();
                        if (bankCode == "") return;
                        var bankName = $("#banks option:selected").text();
                        if ($scope.requesting) return;
                        $scope.requesting = true;

                        $scope.planDetailID = $("#hdnPlanDetailID").val();
                        $scope.amount = $("#hdnAmount").val();
                        $scope.amountType = $("#hdnAmountTypeCode").val();

                        $http.post("/api/visiontrip.ashx?t=set-virtual-account", {
                            bankCode: bankCode, bankName: bankName,
                            planDetailID: $scope.planDetailID, amountType: $scope.amountType, amount: $scope.amount
                        }).success(function (r) {
                            $scope.requesting = false;
                            console.log(r);
                            alert(r.message);
                            if (r.success) {
                                $scope.modal.virtual_accounts = r.data;
                            } else {
                                //alert(r.message);
                            }
                        });
                    }
                } else {
                    var bankCode = $("#banks").val();
                    if (bankCode == "") return;
                    var bankName = $("#banks option:selected").text();
                    if ($scope.requesting) return;
                    $scope.requesting = true;

                    $scope.planDetailID = $("#hdnPlanDetailID").val();
                    $scope.amount = $("#hdnAmount").val();
                    $scope.amountType = $("#hdnAmountTypeCode").val();

                    $http.post("/api/visiontrip.ashx?t=set-virtual-account", {
                        bankCode: bankCode, bankName: bankName,
                        planDetailID: $scope.planDetailID, amountType: $scope.amountType, amount: $scope.amount
                    }).success(function (r) {
                        $scope.requesting = false;
                        console.log(r);
                        alert(r.message);
                        if (r.success) {
                            $scope.modal.virtual_accounts = r.data;
                        } else {
                            //alert(r.message);
                        }
                    });
                }

            }
        };


        $scope.modal.init();








    });  
})();

var $page = {

    timer: null,

    init: function () {
        $("#txtNameEng").setHangulBan(); //한글 X 

        // ddl - 트립 일정과 방문 국가를 확인했습니다
        $("#ddlScheduleCheck").change(function () {
            if ($("#ddlScheduleCheck").val() == "Y") {
                $("[data-id=check_tripschedule]").hide();
            }
        });
        //영문명
        $("#txtNameEng").keyup(function () {
            if ($("#txtNameEng").val() != "") {
                $("[data-id=check_engname]").hide();
            }
        });
        //전화번호
        $("#txtPhone").keyup(function () {
            if ($("#txtPhone").val() != "") {
                $("[data-id=check_phone]").hide();
            }
        });
        //주소
        $("#addr_road").change(function () {
            if ($("addr_road").val() != "") {
                $("[data-id=check_addr]").hide();
            }
        });
        //ddl - 기존정보변경여부
        $("#ddlInfoChange").change(function () {
            if ($("#ddlInfoChange").val() != "") {
                $("[data-id=check_infochange]").hide();
            }
        });
        //ddl - 국적
        $("#ddlNation").change(function () {
            if ($("#ddlNation").val() != "") {
                $("[data-id=check_nation]").hide();
            }
        });
        //ddl - 종교
        $("#ddlReligion").change(function () {
            if ($("#ddlReligion").val() != "") {
                $("[data-id=check_religion]").hide();
            }
            if ($("#ddlReligion").val() == "Christian" && $("#txtChurch").val() != "") {
                $("[data-id=check_church]").hide();
            }
        });

        //소속 교회
        $("#txtChurch").keyup(function () {
            if ($("#txtChurch").val() != "") {
                $(this).val($(this).val().replace(' ', '')); //공백제거
                $("[data-id=check_church]").hide();
            }
        });

        //ddl - 영어회화능력
        $("#ddlEnglishLevel").change(function () {
            if ($("#ddlEnglishLevel").val() != "") {
                $("[data-id=check_englishlevel]").hide();
            }
        });
        //ddl - 비전트립 참가이력 
        $("#ddlVTHistory").change(function () {
            if ($("#ddlVTHistory").val() != "") {
                $("[data-id=check_visiontriphistory]").hide();
            }
        });
        //직업
        $("#txtJob").keyup(function () {
            if ($("#txtJob").val() != "") {
                $("[data-id=check_job]").hide();
            }
        });

        //ddl - 병역
        $("#ddlMilitary").change(function () {
            if ($("#ddlMilitary").val() != "") {
                $("[data-id=check_military]").hide();
            }
        });


        //비상연락처
        $("#txtEmergencyContactName").keyup(function () {
            if ($("#txtEmergencyContactName").val() != "") {
                $("[data-id=check_emergencycontactname]").hide();
            }
        });
        $("#txtEmergencyContactTel").keyup(function () {
            if ($("#txtEmergencyContactTel").val() != "") {
                $("[data-id=check_emergencycontacttel]").hide();
            }
        });
        $("#txtEmergencyContactRelation").keyup(function () {
            if ($("#txtEmergencyContactRelation").val() != "") {
                $("[data-id=check_emergencycontactrelation]").hide();
            }
        });
        //ddl - 방배정
        $("#ddlRoomType").change(function () {
            if ($("#ddlRoomType").val() != "") {
                $("[data-id=check_roomtype]").hide();
            }
        });
        //ddl - 후원어린이 만남 여부
        $("#ddlChildMeetYn").change(function () {
            if ($("#ddlChildMeetYn").val() != "") {
                $("[data-id=check_childmeetyn]").hide();
            }
        });
        //ddl - 현금영수증 발급 선택
        $("#ddlCashReceiptType").change(function () {
            if ($("#ddlCashReceiptType").val() != "") {
                $("[data-id=check_cashreceipttype]").hide();
            }
        });
        $("#txtCashReceiptName").keyup(function () {
            if ($("#txtCashReceiptName").val() != "") {
                $("[data-id=check_cashreceiptname]").hide();
            }
        });
        $("#txtCashReceiptTel").keyup(function () {
            if ($("#txtCashReceiptTel").val() != "") {
                $("[data-id=check_cashreceipttel]").hide();
            }
        });
        $("#txtCashReceiptRelation").keyup(function () {
            if ($("#txtCashReceiptRelation").val() != "") {
                $("[data-id=check_cashreceiptrelation]").hide();
            }
        });



        $(".agreecheck_change").change(function () {

            if ($("#chkAgree1").is(":checked") && $("#chkAgree2").is(":checked") &&
                $("#chkAgree3").is(":checked") && $("#chkAgree4").is(":checked") &&
                $("#chkAgree5").is(":checked") && $("#chkAgree6").is(":checked") &&
                $("#chkAgree7").is(":checked") && $("#chkAgree8").is(":checked")
                && $("#chkAgreeFinal").is(":checked")
                ) {
                $("[data-id=check_agree]").hide();
            }
        });




    },

    init_individual: function () {

        $("#txtNameEng").setHangulBan();
        
        $(".childinput").keyup(function (e) {
            $(this).val($(this).val().replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, ''));
        });




        // ddl - 트립 일정과 방문 국가를 확인했습니다
        $("#ddlVisitType").change(function () {
            if ($("#ddlVisitType").val() == "") {
                $("[data-id=check_visittype]").hide();
            }
        });
        //방문일자  
        $('#txtVisitDate1').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function () {
                $("[data-id=check_visitdate1]").hide();
            }
        });
        $('#txtVisitDate2').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function () { 
                $("[data-id=check_visitdate2]").hide();
            }
        });
        //방문국가
        $("#ddlVisitNation").change(function () {
            if ($("#ddlVisitNation").val() != "") {
                $("[data-id=check_visitnation]").hide();
            }
        });

        //영문명
        $("#txtNameEng").keyup(function () {
            if ($("#txtNameEng").val() != "") {
                $("[data-id=check_engname]").hide();
            }
        });
        //전화번호
        $("#txtPhone").keyup(function () {
            if ($("#txtPhone").val() != "") {
                $("[data-id=check_phone]").hide();
            }
        });
        //주소
        $("#addr_road").change(function () {
            if ($("addr_road").val() != "") {
                $("[data-id=check_addr]").hide();
            }
        });
        //ddl - 기존정보변경여부
        $("#ddlInfoChange").change(function () {
            if ($("#ddlInfoChange").val() != "") {
                $("[data-id=check_infochange]").hide();
            }
        }); 
        //ddl - 종교
        $("#ddlReligion").change(function () {
            if ($("#ddlReligion").val() != "") {
                $("[data-id=check_religion]").hide();
            }
            if ($("#ddlReligion").val() == "Christian" && $("#txtChurch").val() != "") {
                $("[data-id=check_church]").hide();
            }
        });

        //소속 교회
        $("#txtChurch").keyup(function () {
            if ($("#txtChurch").val() != "") {
                $(this).val($(this).val().replace(' ', '')); //공백제거
                $("[data-id=check_church]").hide();
            }
        });

        //어린이정보
        $("multiselect").click(function () {
            $("[data-id=check_childlist]").hide();
        });
        $(".divChildMeet a").click(function () {
            $("[data-id=check_childlist]").hide();
        });

        // 현지정보
        $("#txtLocalAccommodation").keyup(function () {
            if ($("#txtLocalAccommodation").val() != "") {
                $("[data-id=check_localaccommodation]").hide();
            }
        });
        $("#txtLocalTel").keyup(function () {
            if ($("#txtLocalTel").val() != "") {
                $("[data-id=check_localtel]").hide();
            }
        });
        $("#txtLocalAddress").keyup(function() {
            if ($("#txtLocalAddress").val() != "") {
                $("[data-id=check_localaddress]").hide();
            }
        });


        //$(".companion_name_kor, .companion_name_eng, .companion_birth, .companion_gender").keyup(function () {
        //    $("[data-id=check_companion]").hide();
        //});



        //비상연락처
        $("#txtEmergencyContactName").keyup(function () {
            if ($("#txtEmergencyContactName").val() != "") {
                $("[data-id=check_emergencycontactname]").hide();
            }
        });
        $("#txtEmergencyContactTel").keyup(function () {
            if ($("#txtEmergencyContactTel").val() != "") {
                $("[data-id=check_emergencycontacttel]").hide();
            }
        });
        $("#txtEmergencyContactRelation").keyup(function () {
            if ($("#txtEmergencyContactRelation").val() != "") {
                $("[data-id=check_emergencycontactrelation]").hide();
            }
        }); 

        //출입국정보
        $('#txtDepartureDate').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function () {
                $("[data-id=check_departuredate]").hide();
            }
        });
        $('#txtReturnDate').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function () {
                $("[data-id=check_returndate]").hide();
            }
        });

        $(".agreecheck_change").change(function () { 
            if ($("#chkAgree1").is(":checked") && $("#chkAgree2").is(":checked") &&
                $("#chkAgree3").is(":checked")
                ) {
                $("[data-id=check_agree]").hide();
            }
        });  
    },

    // 확인
    onSubmit: function () {

        var count = 0;

        // ddl - 트립 일정과 방문 국가를 확인했습니다
        if ($("#ddlScheduleCheck").val() != "Y") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_tripschedule]").html("트립 일정 확인해주세요.").addClass("guide_comment2").show();
            count++;
            //return false;
        }
        // 영문명
        if ($("#txtNameEng").val() == "") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_engname]").html("영문명을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }


        // 전화번호
        if ($("#txtPhone").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_phone]").html("전화번호를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 이메일
        if ($("#txtEmail").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_engname]").html("이메일을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 주소
        //if ($("#addr_road").text() == "") {
        //    if (count == 0)
        //        scrollTo($(".divUserInfo"));
        //    $("[data-id=check_addr]").html("주소를 입력해주세요.").addClass("guide_comment2").show();
        //    //return false;
        //    count++;
        //}
        // ddl - 기존정보변경여부
        if ($("#ddlInfoChange").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_infochange]").html("정보변경 여부 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 국적
        if ($("#ddlNation").val() == "") {
            if (count == 0)
                scrollTo($(".divNation"));
            $("[data-id=check_nation]").html("국적을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 종교
        if ($("#ddlReligion").val() == "") {
            if (count == 0)
                scrollTo($(".divReligion"));
            $("[data-id=check_religion]").html("종교를 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 소속 교회
        if ($("#ddlReligion").val() == "Christian" && $("#txtChurch").val() == "") {
            if (count == 0)
                scrollTo($(".divReligion"));
            $("[data-id=check_church]").html("소속교회를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // ddl - 영어회화능력
        if ($("#ddlEnglishLevel").val() == "") {
            if (count == 0)
                scrollTo($(".divGroupField"));
            $("[data-id=check_englishlevel]").html("영어회화능력을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 비전트립 참가이력 
        if ($("#ddlVTHistory").val() == "") {
            if (count == 0)
                scrollTo($(".divGroupField"));
            $("[data-id=check_visiontriphistory]").html("비전트립 참가이력을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 직업
        if ($("#txtJob").val() == "") {
            if (count == 0)
                scrollTo($(".divGroupField"));
            $("[data-id=check_job]").html("직업을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 병역
        if ($("#ddlMilitary").val() == "") {
            if (count == 0)
                scrollTo($(".divMilitary"));
            $("[data-id=check_military]").html("병역 구분을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }


        // 비상연락처 성함
        if ($("#txtEmergencyContactName").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontactname]").html("비상연락처 성함을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 비상연락처 휴대번호
        if ($("#txtEmergencyContactTel").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontacttel]").html("비상연락처를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 비상연락처 참가자와의 관계
        if ($("#txtEmergencyContactRelation").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontactrelation]").html("참가자와의 관계를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // ddl - 방배정
        if ($("#ddlRoomType").val() == "") {
            if (count == 0)
                scrollTo($(".divRoom"));
            $("[data-id=check_roomtype]").html("방배정 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // ddl - 후원어린이 만남 여부
        if ($("#ddlChildMeetYn").val() == "") {
            if (count == 0)
                scrollTo($(".divChildMeet"));
            $("[data-id=check_childmeetyn]").html("후원어린이 만남 여부를 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 현금영수증 발급 선택
        if ($("#ddlCashReceiptType").val() == "") {
            if (count == 0)
                scrollTo($(".divCashReceipt"));
            $("[data-id=check_cashreceipttype]").html("현금영수증 발급 구분을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }


        // 현금영수증 발급자 성함
        if ($("#txtCashReceiptName").val() == "") {
            if (count == 0)
                scrollTo($(".divCashReceipt"));
            $("[data-id=check_cashreceiptname]").html("현금영수증 발급자 성함을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 현금영수증 발급자 휴대번호
        if ($("#txtCashReceiptTel").val() == "") {
            if (count == 0)
                scrollTo($(".divCashReceipt"));
            $("[data-id=check_cashreceipttel]").html("현금영수증 발급자 휴대번호을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 현금영수증 - 참가자와의 관계
        if ($("#txtCashReceiptRelation").val() == "") {
            if (count == 0)
                scrollTo($(".divCashReceipt"));
            $("[data-id=check_cashreceiptrelation]").html("참가자와의 관계를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        if (!($("#chkAgree1").is(":checked") &&
            $("#chkAgree2").is(":checked") &&
            $("#chkAgree3").is(":checked") &&
            $("#chkAgree4").is(":checked") &&
            $("#chkAgree5").is(":checked") &&
            $("#chkAgree6").is(":checked") &&
            $("#chkAgree7").is(":checked") &&
            $("#chkAgree8").is(":checked")
            &&
            $("#chkAgreeFinal").is(":checked")
            )) {
            if (count == 0)
                scrollTo($(".divAgreement"));
            $("[data-id=check_agree]").html("약관동의를 모두 선택해주세요.").addClass("guide_comment2").show();
        }

        if (count > 0) {
            count = 0;
            return false;
        }

        return true;
    },

    // 확인
    onSubmit_individual: function (childList, companionList) {

        var count = 0;

        // ddl - 방문유형
        if ($("#ddlVisitType").val() == "") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_visittype]").html("방문유형을 선택해주세요.").addClass("guide_comment2").show();
            count++;
            //return false;
        }
        // 방문일자 - 1
        if ($("#txtVisitDate1").val() == "") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_visitdate1]").html("방문날짜(1지망)을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 방문일자 - 2
        if ($("#txtVisitDate2").val() == "") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_visitdate2]").html("방문날짜(2지망)을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // ddl - 방문국가
        if ($("#ddlVisitNation").val() == "") {
            if (count == 0)
                scrollTo($(".w980"))
            $("[data-id=check_visitnation]").html("방문국가를 선택해주세요.").addClass("guide_comment2").show();
            count++;
            //return false;
        }


        // 영문명
        if ($("#txtNameEng").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"))
            $("[data-id=check_engname]").html("영문명을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // 전화번호
        if ($("#txtPhone").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_phone]").html("전화번호를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 이메일
        if ($("#txtEmail").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_engname]").html("이메일을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 주소
        //if ($("#addr_road").text() == "") {
        //    if (count == 0)
        //        scrollTo($(".divUserInfo"));
        //    $("[data-id=check_addr]").html("주소를 입력해주세요.").addClass("guide_comment2").show();
        //    //return false;
        //    count++;
        //}
        // ddl - 기존정보변경여부
        if ($("#ddlInfoChange").val() == "") {
            if (count == 0)
                scrollTo($(".divUserInfo"));
            $("[data-id=check_infochange]").html("정보변경 여부 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // ddl - 종교
        if ($("#ddlReligion").val() == "") {
            if (count == 0)
                scrollTo($(".divReligion"));
            $("[data-id=check_religion]").html("종교를 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 소속 교회
        if ($("#ddlReligion").val() == "Christian" && $("#txtChurch").val() == "") {
            if (count == 0)
                scrollTo($(".divReligion"));
            $("[data-id=check_church]").html("소속교회를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        //var companion_text = "동반인 정보 ";
        //var companion_count = 0;
        //var companion_name_kor = $('.companion_name_kor').filter(function () {
        //    return this.value == ''
        //}); 
        //// 동반인정보
        //if (companion_name_kor.length) {
        //    if (count == 0)
        //        scrollTo($(".divCompanion"));
        //    companion_text += "한글성함"
        //    companion_count++;     //return false;
        //    count++;
        //}
        //var companion_name_eng = $('.companion_name_eng').filter(function () {
        //    return this.value == ''
        //}); 
        //if (companion_name_eng.length) {
        //    if (count == 0)
        //        scrollTo($(".divCompanion"));
        //    companion_text += companion_count > 0 ? ", 영문성함" : "영문성함"
        //    companion_count++;
        //}
        //var companion_birth = $('.companion_birth').filter(function () {
        //    return this.value == ''
        //});
        //if (companion_birth.length) {
        //    if (count == 0)
        //        scrollTo($(".divCompanion"));
        //    companion_text += companion_count > 0 ? ", 생년월일" : "생년월일"
        //    companion_count++; 
        //}
        //var companion_gender = $('.companion_gender').filter(function () {
        //    return this.value == ''
        //});
        //if (companion_gender.length) {
        //    if (count == 0)
        //        scrollTo($(".divCompanion"));
        //    companion_text += companion_count > 0 ? ", 성별" : "성별"
        //    companion_count++; 
        //}
        //if (companion_count > 0) { 
        //    $("[data-id=check_companion]").html(companion_text + "을 입력해주세요.").addClass("guide_comment2").show();

        //    count++;
        //}

        //어린이정보
        if (childList.length == 0) {
            if (count == 0)
                scrollTo($(".divChildMeet"));
            $("[data-id=check_childlist]").html("어린이정보를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // 현지정보
        if ($("#txtLocalAccommodation").val() == "") {
            if (count == 0)
                scrollTo($(".divLocal"));
            $("[data-id=check_localaccommodation]").html("현지 숙소명을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        } 
        if ($("#txtLocalTel").val() == "") {
            if (count == 0)
                scrollTo($(".divLocal"));
            $("[data-id=check_localtel]").html("현지 숙소 연락처를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        } 
        if ($("#txtLocalAddress").val() == "") {
            if (count == 0)
                scrollTo($(".divLocal"));
            $("[data-id=check_localaddress]").html("현지 숙소 주소를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }


        if ($("#txtDepartureDate").val() == "") {
            if (count == 0)
                scrollTo($(".divDate"));
            $("[data-id=check_departuredate]").html("출국일을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        if ($("#txtReturnDate").val() == "") {
            if (count == 0)
                scrollTo($(".divDate"));
            $("[data-id=check_returndate]").html("귀국일을 선택해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }

        // 비상연락처 성함
        if ($("#txtEmergencyContactName").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontactname]").html("비상연락처 성함을 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 비상연락처 휴대번호
        if ($("#txtEmergencyContactTel").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontacttel]").html("비상연락처를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
        // 비상연락처 참가자와의 관계
        if ($("#txtEmergencyContactRelation").val() == "") {
            if (count == 0)
                scrollTo($(".divEmergencyContact"));
            $("[data-id=check_emergencycontactrelation]").html("참가자와의 관계를 입력해주세요.").addClass("guide_comment2").show();
            //return false;
            count++;
        }
 

        if (!($("#chkAgree1").is(":checked") &&
            $("#chkAgree2").is(":checked") &&
            $("#chkAgree3").is(":checked") )) {
            if (count == 0)
                scrollTo($(".divAgreement"));
            $("[data-id=check_agree]").html("약관동의를 모두 선택해주세요.").addClass("guide_comment2").show();
        }

        if (count > 0) {
            count = 0;
            return false;
        }

        return true;
    },

    attachUploader: function (button) {
        return new AjaxUpload(button, {
            action: '/c/handler/upload',
            responseType: 'json',
            onChange: function (file) {
                var fileName = file.toLowerCase();
            },
            onSubmit: function (file, ext) {
                this.disable();
            },
            onComplete: function (file, response) {

                this.enable();

                console.log(file, response);
                if (response.success) {
                    //$("#passport_path").val(file);
                    //$("#lb_passport_path").val(file);
                    $("#path_" + button).val(response.name);
                    $("[data-id=path_" + button + "]").val(response.name.replace(/^.*[\\\/]/, ''));

                    console.log($("#path_" + button).val()); //filepullpath
                    console.log($("[data-id=path_" + button + "]").val());  //filename
                    //$("#passport_path").val(response.name);
                    //$("[data-id=passport_path]").val(response.name.replace(/^.*[\\\/]/, ''));
                    //	$(".temp_file_size").val(response.size);

                } else
                    alert(response.msg);
            }
        });
    }

};