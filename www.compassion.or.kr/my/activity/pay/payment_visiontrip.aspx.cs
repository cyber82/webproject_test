﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
using Microsoft.AspNet.FriendlyUrls;

public partial class my_activity_pay_payment_visiontrip : FrontBasePage
{
    public override bool NoCache
    {
        get
        {
            return true;
        }
    }
    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }
    public override bool RequireSSL
    {
        get
        {
            return utils.chServerState(); //true;
        }
    }
    protected int iApplyID;
    protected int iPlanDetailID;
    protected string sAmountType; //비용 구분 : 신청비/트립비/후원어린이 만남비
    protected string sAmount; //비용  

    const string listPath = "/my/activity/visiontrip";
    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();
        if (!UserInfo.IsLogin)
        {
            Response.ClearContent();
            return;
        }
        string strType = string.Empty;

        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1)
        {
            Response.Redirect(listPath, true);
        }
        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);

        //parameter
        base.PrimaryKey = requests[0];
        iApplyID = Convert.ToInt16(PrimaryKey);
        iPlanDetailID = Convert.ToInt16(requests[1]);
        strType = requests[2];
        //bool isNum = Int32.TryParse(PrimaryKey.ToString(), out iApplyID);


        UserInfo sess = new UserInfo();
        string strUserID = sess.UserId.ToString();


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var applyData = dao.tVisionTripApply.First(p => p.ApplyID == iApplyID && p.UserID == strUserID && p.CurrentUse == 'Y');
            var applyData = www6.selectQF<tVisionTripApply>("ApplyID", iApplyID, "UserID", strUserID, "CurrentUse", "Y");
            if (applyData != null)
            {
                //var detailData = dao.tVisionTripPlanDetail.First(p => p.ApplyID == applyData.ApplyID && p.PlanDetailID == iPlanDetailID);
                var detailData = www6.selectQF<tVisionTripPlanDetail>("ApplyID", applyData.ApplyID, "PlanDetailID", iPlanDetailID);

                var scheduleData = www6.selectQF<tVisionTripSchedule>("ScheduleID", applyData.ScheduleID);

                if (detailData != null)
                {

                    //string.Format("{0:#.00}", strType == "R" ? detailData.RequestCost : strType == "T" ? detailData.TripCost : detailData.ChildMeetCost / 100);
                    //sAmount = Convert.ToString(strType == "R" ? detailData.RequestCost : strType == "T" ? detailData.TripCost : detailData.ChildMeetCost);
                    sAmount = string.Format("{0:#}", strType == "R" ? detailData.RequestCost : strType == "T" ? detailData.TripCost : detailData.ChildMeetCost );
                    string sAmountDisplay = Convert.ToDecimal(strType == "R" ? detailData.RequestCost : strType == "T" ? detailData.TripCost : detailData.ChildMeetCost).ToString("C"); //

                    //sAmountType = strType == "R" ? "신청비" : strType == "T" ? "트립비" : "후원어린이 만남비";
                    sAmountType = scheduleData.CostCenter; //#13729 

                    lbAmountType.Text = sAmountType;
                    ltTitle_type.Text = sAmountType;
                    lbAmount.Text = sAmountDisplay;

                    hdnApplyID.Value = iApplyID.ToString();
                    hdnPlanDetailID.Value = iPlanDetailID.ToString();
                    hdnAmount.Value = sAmount.ToString();
                    hdnAmountType.Value = sAmountType;
                    hdnAmountTypeCode.Value = strType;
                }
            }

            //var virtualAccount = dao.tVisionTripPayment.Where(p => p.PlanDetailID == iPlanDetailID && p.PaymentName == "가상계좌").OrderBy(p=> p.RegisterDate );



        }


    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        UserInfo sess = new UserInfo();

        if (base.IsRefresh)
        {
            return;
        }

        //기획트립 ID
        this.ViewState["plandetail_id"] = hdnPlanDetailID.Value;
        this.ViewState["amount_type"] = hdnAmountTypeCode.Value;

        string sOrderItem = "";
        sOrderItem += "{";
        sOrderItem += "ApplyID : " + hdnApplyID.Value + ",";
        sOrderItem += "PlandetailID : " + hdnPlanDetailID.Value + ",";
        sOrderItem += "AmountType : " + hdnAmountType.Value + ",";
        sOrderItem += "Amount : " + hdnAmount.Value + "}";

        this.ViewState["order_item"] = sOrderItem.ToJson();
        this.ViewState["good_mny"] = hdnAmount.Value;
        this.ViewState["good_name"] = hdnAmountType.Value;
        this.ViewState["buyr_name"] = sess.UserName.ToString();
        this.ViewState["ordr_idxx"] = StringExtension.GetRandomString(3) + DateTime.Now.ToString("yyyyMMddHHmmss");

        this.ViewState["used_card_YN"] = "N";
        this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_domestic"];

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "VisionTrip";  // 결제 페이지 종류

        //kcp_no_form.Hide(); 
        kcp_form.Hide();

        string payMethod = string.Empty;
        if (payment_method_cms.Checked)
        {
            this.ViewState["pay_method"] = "010000000000";
            this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_visiontrip_account"];
            kcp_form.Show(this.ViewState);
        }
        else if (payment_method_virtual.Checked)
        {
            this.ViewState["pay_method"] = "001000000000";
            this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_visiontrip_virtual"];
            kcp_form.Show(this.ViewState);
            //payMethod = PayItemSession.Entity.enumPayMethod.virtualAccount.ToString();
        }
        ///base.ConfirmWithJavascript("aaa", "alert('aaaaa')");
        // DateTime.Now.ToString("yyyyMMddHHmmssff"); ;
        //kcp_form.Show(this.ViewState);
    }
}