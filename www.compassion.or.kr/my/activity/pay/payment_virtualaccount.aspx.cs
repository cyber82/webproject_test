﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
using System.IO;
using System.Text;

public partial class my_activity_pay_payment_virtualaccount : System.Web.UI.Page
{
    string[,] m_straResData;
    Int32 m_nResDataCnt;

    protected void Page_Load(object sender, EventArgs e)
    {
        //----- 리로드면 타지않도록
        if (IsPostBack == false)
        {
            GetData(sender, e);
        }
    }

    public void GetData(Object sender, System.EventArgs e)
    {
        StringBuilder sSql = new StringBuilder();

        string strResult = string.Empty;                    //-- DB저장 결과코드
        string strMSG = string.Empty;

        string site_cd = Request.Params["site_cd"];         // 사이트 코드
        string tno = Request.Params["tno"];                 // KCP 거래번호
        string order_no = Request.Params["order_no"];       // 주문번호
        string tx_cd = Request.Params["tx_cd"];             // 업무처리 구분 코드
        string tx_tm = Request.Params["tx_tm"];             // 업무처리 완료 시간

        string ipgm_name = string.Empty;                    // 주문자명
        string remitter = string.Empty;                     // 입금자명
        string ipgm_mnyx = string.Empty;                    // 입금 금액
        string bank_code = string.Empty;                    // 은행코드 : 은행명도..
        string account = string.Empty;                      // 가상계좌 입금계좌번호
        string op_cd = string.Empty;                        // 처리구분 코드 : processcode
        string noti_id = string.Empty;                      // 통보 아이디 : notificationid
        string cash_a_no = string.Empty;                    // 현금영수증 승인번호 : cashid
        string sEuckr_Total = string.Empty;


        //1회성 가상계좌
        string site_cd_virtual = ConfigurationManager.AppSettings["g_conf_site_cd_visiontrip_account"];
        //고정 가상계좌
        string site_cd_fix_virtual = ConfigurationManager.AppSettings["g_conf_site_cd_visiontrip_fix_virtual"];
         

        /* = -------------------------------------------------------------------------- = */
        /* =   02-1. 가상계좌 입금 통보 데이터 받기                                     = */
        /* = -------------------------------------------------------------------------- = */
        if (tx_cd == "TX00")
        {
            //-- 한글 Encoding
            try
            {
                byte[] buffer = Request.BinaryRead(Request.TotalBytes);
                sEuckr_Total = HttpUtility.UrlDecode(buffer, System.Text.Encoding.GetEncoding("euc-kr"));

                getPostDate(sEuckr_Total);

            }
            catch (Exception ex)
            {
                string sMsg = ex.Message;
            }

            //ipgm_name = Request.Params["ipgm_name"];
            //remitter = Request.Params["remitter"];

            ipgm_name = getResPost("ipgm_name");
            remitter = getResPost("remitter");

            ipgm_mnyx = Request.Params["ipgm_mnyx"];
            bank_code = Request.Params["bank_code"];
            account = Request.Params["account"];
            op_cd = Request.Params["op_cd"];
            noti_id = Request.Params["noti_id"];
            cash_a_no = Request.Params["cash_a_no"];
        }
        /* = -------------------------------------------------------------------------- = */
        /* =   02-2. 모바일안심결제 통보 데이터 받기                                    = */
        /* = -------------------------------------------------------------------------- = 
        else if (tx_cd == "TX08")
        {
            ipgm_mnyx = Request.Params["ipgm_mnyx"];
            bank_code = Request.Params["bank_code"];
        }*/
        /* ============================================================================== */


        #region 로그남기기

        /*입금 통보 로그        */
        lblMsg.Text = site_cd + tno + order_no + tx_cd + tx_tm;
        string sFilePath = CreateFaxFolder(DateTime.Now.ToString("yyyyMM")+"_VisionTrip_Virtual", DateTime.Now);
        string sFileName = sFilePath + "\\"+ DateTime.Now.ToString("yyyyMMdd") + "Payment_Account_IN_Log.txt";
        if (File.Exists(sFileName))
        {
            StreamWriter sw = File.AppendText(sFileName);
            sw.WriteLine();
            sw.WriteLine("\r\n");
            sw.Write("Log [ " + lblMsg.Text);
            sw.Write("tno:" + tno + "|");
            sw.Write("ipgm_name:" + ipgm_name + "|");
            sw.Write("remitter:" + remitter + "|");
            sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
            sw.Write("bank_code:" + bank_code + "|");
            sw.Write("account:" + account + "|");
            sw.Write("op_cd:" + op_cd + "|");
            sw.Write("noti_id:" + noti_id + "|");
            sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
            sw.Write("bank_code:" + bank_code + "|");
            sw.Write("strResult:" + strResult + "|");
            sw.Write("strMSG:" + strMSG + "|");
            sw.Write("] " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " Log End");
            sw.Close();
        }
        else
        {
            StreamWriter sw = File.CreateText(sFileName);
            sw.WriteLine();
            sw.WriteLine("\r\n");
            sw.Write("Log [ " + lblMsg.Text);
            sw.Write("tno:" + tno + "|");
            sw.Write("ipgm_name:" + ipgm_name + "|");
            sw.Write("remitter:" + remitter + "|");
            sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
            sw.Write("bank_code:" + bank_code + "|");
            sw.Write("account:" + account + "|");
            sw.Write("op_cd:" + op_cd + "|");
            sw.Write("noti_id:" + noti_id + "|");
            sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
            sw.Write("bank_code:" + bank_code + "|");
            sw.Write("strResult:" + strResult + "|");
            sw.Write("strMSG:" + strMSG + "|");
            sw.Write("] " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " Log End");
            sw.Close();
        }


        #endregion


        /* ============================================================================== */
        /* =   03. 공통 통보 결과를 업체 자체적으로 DB 처리 작업하시는 부분입니다.      = */
        /* = -------------------------------------------------------------------------- = */
        /* =   통보 결과를 DB 작업 하는 과정에서 정상적으로 통보된 건에 대해 DB 작업을  = */
        /* =   실패하여 DB update 가 완료되지 않은 경우, 결과를 재통보 받을 수 있는     = */
        /* =   프로세스가 구성되어 있습니다. 소스에서 result 라는 Form 값을 생성 하신   = */
        /* =   후, DB 작업이 성공 한 경우, result 의 값을 "0000" 로 세팅해 주시고,      = */
        /* =   DB 작업이 실패 한 경우, result 의 값을 "0000" 이외의 값으로 세팅해 주시  = */
        /* =   기 바랍니다. result 값이 "0000" 이 아닌 경우에는 재통보를 받게 됩니다.   = */
        /* = -------------------------------------------------------------------------- = */

        /* = -------------------------------------------------------------------------- = */
        /* =   03-1. 가상계좌 입금 통보 데이터 DB 처리 작업 부분                        = */
        /* = -------------------------------------------------------------------------- = */
        
        if (tx_cd == "TX00")
        {
            //DB처리 코드 삽입

            //-- 코드명을 받아오기 위한 데이터셋
            DataSet dsPaymentName = new DataSet();
            DataSet dsBankName = new DataSet();
            //-- 가상계좌 결제타입 : 001000000000 
            string strPaymentType = "001000000000";

            strResult = "10";
            strMSG = "OK";
            string xstrMSG = string.Empty;
            try
            {
                if (site_cd == site_cd_virtual) //1회성
                {
                    using(FrontDataContext dao = new FrontDataContext())
                    {
                        //var payment = dao.tVisionTripPayment.Where(p => p.KCPTNo == tno && p.OrderNo == order_no && p.CurrentUse == 'Y').FirstOrDefault();
                        //var payment = www6.selectQF<tVisionTripPayment>("KCPTNo", tno, "OrderNo", order_no, "CurrentUse", "Y");
                        var payment = www6.selectQF<tVisionTripPayment>("KCPTNo", tno, "OrderNo", order_no);

                        if (payment != null)
                        {

                            var d = DateTime.ParseExact(tx_tm, "yyyyMMddHHmmss", null);

                            payment.PaymentDate = d;// Convert.ToDateTime(tx_tm.ToDateFormat("yyyy-MM-dd hh:mm:ss"));
                            payment.Depositor = remitter;
                            payment.ProcessCode = op_cd;
                            payment.NotificationID = noti_id;
                            payment.CashID = cash_a_no;

                            payment.ModifyDate = DateTime.Now;
                            payment.ModifyID = "System";
                            payment.ModifyName = "System";
                            payment.PaymentResultYN = 'Y';

                            payment.CurrentUse = 'Y';

                            //dao.SubmitChanges();
                            
                            www6.update(payment);
                            
                            
                        }
                    }

                }
                else if (site_cd == site_cd_fix_virtual)
                { //고정 가상계좌의 경우 insert
                    decimal dAmount = new decimal();
                    bool check = decimal.TryParse(ipgm_mnyx, out dAmount);

                    var payment = new tVisionTripPayment();
                    if (payment != null)
                    {
                        using (FrontDataContext dao = new FrontDataContext())
                        {
                            payment.PaymentName = "고정가상계좌";
                            payment.PaymentType = strPaymentType;
                            payment.Amount = dAmount;
                            payment.BankCode = bank_code;
                            payment.Account = account;

                            payment.PaymentDate = Convert.ToDateTime(tx_tm);
                            payment.Depositor = remitter;
                            payment.ProcessCode = op_cd;
                            payment.NotificationID = noti_id;
                            payment.CashID = cash_a_no;
                            payment.CurrentUse = 'Y';
                            payment.RegisterDate = DateTime.Now;
                            payment.RegisterID = "System";
                            payment.RegisterName = "System";
                            payment.PaymentResultYN = 'Y';

                            //dao.tVisionTripPayment.InsertOnSubmit(payment);
                            www6.insert(payment);
                            //dao.SubmitChanges();
                        }
                        
                    }
                }

                //-- _COMService로 데이터 저장 (WebDB에 저장)
                //strResult = _COMService.registerPayment_VirtualAccount(
                //            strPaymentType
                //            , strResult
                //            , strMSG
                //            , site_cd
                //            , tno
                //            , order_no
                //            , tx_cd
                //            , tx_tm
                //            , ipgm_name
                //            , remitter
                //            , ipgm_mnyx
                //            , bank_code
                //            , account
                //            , op_cd
                //            , noti_id
                //            , cash_a_no
                //            , "Y"
                //            , "System");

            }
            catch (Exception ex)
            {
                xstrMSG = strResult;
                strResult = "30";
                strMSG = ex.Message;
            }

            if (strResult == "10")
            {
                #region 로그남기기

                /*입금 통보 로그        */
                lblMsg.Text = site_cd + tno + order_no + tx_cd + tx_tm;
                sFileName = sFilePath + "\\Payment_Account_IN_OK_Log.txt";
                if (File.Exists(sFileName))
                {
                    StreamWriter sw = File.AppendText(sFileName);
                    sw.WriteLine();
                    sw.WriteLine("\r\n");
                    sw.Write("Log [ " + lblMsg.Text);
                    sw.Write("tno:" + tno + "|");
                    sw.Write("ipgm_name:" + ipgm_name + "|");
                    sw.Write("remitter:" + remitter + "|");
                    sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
                    sw.Write("bank_code:" + bank_code + "|");
                    sw.Write("account:" + account + "|");
                    sw.Write("op_cd:" + op_cd + "|");
                    sw.Write("noti_id:" + noti_id + "|");
                    sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
                    sw.Write("bank_code:" + bank_code + "|");
                    sw.Write("strResult:" + strResult + "|");
                    sw.Write("strMSG:" + strMSG + "|");
                    sw.Write("] " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " Log End");
                    sw.Close();
                }
                else
                {
                    StreamWriter sw = File.CreateText(sFileName);
                    sw.WriteLine();
                    sw.WriteLine("\r\n");
                    sw.Write("Log [ " + lblMsg.Text);
                    sw.Write("tno:" + tno + "|");
                    sw.Write("ipgm_name:" + ipgm_name + "|");
                    sw.Write("remitter:" + remitter + "|");
                    sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
                    sw.Write("bank_code:" + bank_code + "|");
                    sw.Write("account:" + account + "|");
                    sw.Write("op_cd:" + op_cd + "|");
                    sw.Write("noti_id:" + noti_id + "|");
                    sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
                    sw.Write("bank_code:" + bank_code + "|");
                    sw.Write("strResult:" + strResult + "|");
                    sw.Write("strMSG:" + strMSG + "|");
                    sw.Write("] " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " Log End");
                    sw.Close();
                }


                #endregion
            }
            else
            {
                #region 로그남기기

                /*입금 통보 로그        */
                lblMsg.Text = site_cd + tno + order_no + tx_cd + tx_tm;
                sFileName = sFilePath + "\\Payment_Account_IN_Err_Log.txt";
                if (File.Exists(sFileName))
                {
                    StreamWriter sw = File.AppendText(sFileName);
                    sw.WriteLine();
                    sw.WriteLine("\r\n");
                    sw.Write("Log [ " + lblMsg.Text);
                    sw.Write("tno:" + tno + "|");
                    sw.Write("ipgm_name:" + ipgm_name + "|");
                    sw.Write("remitter:" + remitter + "|");
                    sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
                    sw.Write("bank_code:" + bank_code + "|");
                    sw.Write("account:" + account + "|");
                    sw.Write("op_cd:" + op_cd + "|");
                    sw.Write("noti_id:" + noti_id + "|");
                    sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
                    sw.Write("bank_code:" + bank_code + "|");
                    sw.Write("strResult:" + strResult + "|");
                    sw.Write("strMSG:" + strMSG + xstrMSG + "|");
                    sw.Write("] " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " Log End");
                    sw.Close();
                }
                else
                {
                    StreamWriter sw = File.CreateText(sFileName);
                    sw.WriteLine();
                    sw.WriteLine("\r\n");
                    sw.Write("Log [ " + lblMsg.Text);
                    sw.Write("tno:" + tno + "|");
                    sw.Write("ipgm_name:" + ipgm_name + "|");
                    sw.Write("remitter:" + remitter + "|");
                    sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
                    sw.Write("bank_code:" + bank_code + "|");
                    sw.Write("account:" + account + "|");
                    sw.Write("op_cd:" + op_cd + "|");
                    sw.Write("noti_id:" + noti_id + "|");
                    sw.Write("ipgm_mnyx:" + ipgm_mnyx + "|");
                    sw.Write("bank_code:" + bank_code + "|");
                    sw.Write("strResult:" + strResult + "|");
                    sw.Write("strMSG:" + strMSG + xstrMSG + "|");
                    sw.Write("] " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " Log End");
                    sw.Close();
                }


                #endregion
            } 
        } 

    }
    /* =============================================================== */
    /* +    응답 DATA 분석                                           + */
    /* - ----------------------------------------------------------- - */
    private void getPostDate(string sEuckr_Total)
    {
        string[] straResDataList;
        string[] straResData;
        Int32 nInx;

        straResDataList = sEuckr_Total.Split('&');

        m_nResDataCnt = straResDataList.Length;

        m_straResData = new string[m_nResDataCnt, 2];

        for (nInx = 0; nInx < m_nResDataCnt; nInx++)
        {
            straResData = straResDataList[nInx].Split('=');

            m_straResData[nInx, 0] = straResData[0];
            m_straResData[nInx, 1] = straResData[1];
        }
    }
    /* =============================================================== */

    /* =============================================================== */
    /* +    응답 DATA                                                + */
    /* - ----------------------------------------------------------- - */
    public string getResPost(string parm_strDataName)
    {
        string strRT;
        Int32 nInx;

        for (nInx = 0, strRT = ""; nInx < m_nResDataCnt; nInx++)
        {
            if (m_straResData[nInx, 0].Equals(parm_strDataName))
            {
                strRT = m_straResData[nInx, 1];

                break;
            }
        }


        return strRT;
    }
    /* =============================================================== */


    /// <summary>
    /// 로그가 저장될 폴더를 만든다.
    /// </summary>
    /// <param name="UserAccount">계정에 따른 폴더</param>
    /// <param name="CreateTime">월별 폴더</param>
    /// <returns></returns>
    private string CreateFaxFolder(string UserAccount, DateTime CreateTime)
    {
        DirectoryInfo DefaultFolder = new DirectoryInfo(ConfigurationManager.AppSettings["g_kcp_log_path"].ToString());

        if (DefaultFolder.Exists == false)
            DefaultFolder.Create();

        DirectoryInfo UserAccountFolder = new DirectoryInfo(DefaultFolder.FullName + "\\" + UserAccount);

        if (UserAccountFolder.Exists == false)
            UserAccountFolder.Create();

        return UserAccountFolder.FullName;

    }


}