﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="event.aspx.cs" Inherits="my_activity_event" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/my/activity/event.js?v=1.1"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	 <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980 activity">

				<uc:menu runat="server"  />

				<!-- content -->
				<div class="box_type4 mb40">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit mb30">
						<p class="tit">캠페인/이벤트 참여내역</p>
					</div>
					<!--// -->

					<!-- CAD 추천관리리스트 -->
					<div class="tbl_sort mb15">
						<span class="number">총 <em class="fc_blue">{{total}}</em>건</span>
					</div>
					<div class="tableWrap1 mb30">
						<table class="tbl_type6 padding2">
							<caption>CAD 추천관리 테이블</caption>
							<colgroup>
								<col style="width:16%" />
								<col style="width:40%" />
								<col style="width:16%" />
								<col style="width:14%" />
								<col style="width:14%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">참여일</th>
									<th scope="col">제목</th>
									<th scope="col">당첨자 발표일</th>
									<th scope="col">진행상태</th>
									<th scope="col">당첨여부</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in list">
									<td>{{item.er_regdate | date:'yyyy.MM.dd'}}</td>
									<td class="tit"><a href="/participation/event/view/{{item.er_e_id}}">{{item.e_title}}</a></td>
									<td>{{item.e_announce | date:'yyyy.MM.dd'}}</td>
									<td>{{item.state}}</td>
									<td>{{item.winner}}</td>
								</tr>
								<tr ng-if="total == 0">
									<td colspan="5" class="no_content">
										<p class="mb10">등록된 내역이 없습니다.</p>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// CAD 추천관리리스트 -->

					<!-- page navigation -->
					<div class="tac">
						<paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
					</div>
					<!--// page navigation -->

				</div>

				<!--// content -->

				
			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

</asp:Content>

