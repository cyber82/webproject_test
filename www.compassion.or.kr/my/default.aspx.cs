﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_default : FrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}
    
    protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
        
        // 비회원결제시 결제한 내역 동기화 

        var sess = new UserInfo();
		hdSponsorId.Value = sess.SponsorID;
		if(sess.UserClass != "기업" && sess.UserClass != "기타") {

			var birth = sess.Birth.EmptyIfNull().Length > 9 ? sess.Birth.Substring(0, 10) : "";
			hdBirthDate.Value = birth.Replace("-", "");
			
		}
		hdUserName.Value = sess.UserName;
		
		var actionResult = new LetterAction().GetUnreadCount();
		if(actionResult.success) {
			var count = ((LetterAction.UnreadEntity)actionResult.data).cnt;
			unread_letter.Visible = count > 0;
		} else {
			unread_letter.Visible = false;
		}

		// 후원어린이 없는 경우 데이타

		userName.Text = new UserInfo().UserName;

		actionResult = new ChildAction().GetChildren(null, null, null, null, null, 18, null, null, 1, 10, "waiting", -1, -1);
		if(actionResult.success) {

			var list = (List<ChildAction.ChildItem>)actionResult.data;
			Random rnd = new Random();
            var index = 0;
            if (list.Count > 0)
            {
                index = rnd.Next(list.Count);
                var item = list[index];
                data.Value = item.ToLowerCaseJson();
            }
			//Response.Write(list.ToJson());
		}

	}

}