﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_account_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/password.js?v=1"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/email.js"></script>
    <script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
    <script type="text/javascript" src="/my/account/default.js?v=1"></script>


    <script type="text/javascript">
        $(function () {
            
            if ($(".religion:checked").val() == "기독교") {
                $("#direct_input").show();
                $("#pn_church").show();
            } else {
                $("#direct_input").hide();
                $("#pn_church").hide();
            }

            $(".religion").click(function () {
                if ($("#religion1").prop("checked")) {
                    $("#direct_input").show();
                    $("#pn_church").show();
                } else {
                    $("#direct_input").hide();
                    $("#pn_church").hide();
                }
            })
        })
        $(document).ready(function () {
            if ($('#AdminCK').val() == "true") {
                $(".adminCK").css("display", "none");
            }
        });
    </script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    
    <input type="hidden" id="AdminCK" value="<%=ViewState["AdminCK"]%>" />
    <input type="hidden" runat="server" id="hd_auth_domain" />
    <input type="hidden" runat="server" id="jumin" />
    <input type="hidden" runat="server" id="gender" />
    <input type="hidden" runat="server" id="di" />
    <input type="hidden" runat="server" id="ci" />
    <input type="hidden" runat="server" id="cert_gb" />
    <input type="hidden" runat="server" id="zipcode" />
    <input type="hidden" runat="server" id="addr1" />
    <input type="hidden" runat="server" id="addr2" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
    <input type="hidden" runat="server" id="user_name" />

    <input type="hidden" runat="server" id="hd_mobile" />
    <input type="hidden" runat="server" id="hd_email" />
    <input type="hidden" runat="server" id="hd_mobile_auth" value="Y" />
    <input type="hidden" runat="server" id="hd_email_auth" value="Y" />

    <input type="hidden" runat="server" id="hfAddressType" value="" />
    <input type="hidden" runat="server" id="hdOrganizationID" value="" />
    <input type="hidden" runat="server" id="password_popup" value="N" />

    <!-- 집전화 -->


    <!-- sub body -->
    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit adminCK">
            <div class="titArea">
                <h1>마이컴패션</h1>
                <span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents member mypage">

            <div class="w980 myInfo">


                <uc:menu runat="server" />

                <!-- content -->
                <div class="box_type4">

                    <!-- 서브타이틀 -->
                    <div class="sub_tit mb30">
                        <p class="tit">개인정보 수정</p>
                    </div>
                    <!--// -->



                    <!-- id/pw -->
                    <div class="input_div">
                        <div class="login_field"><span>ID / PW</span></div>
                        <div class="login_input">

                            <table class="tbl_join">
                                <colgroup>
                                    <col style="width: 400px" />
                                    <col style="width: *" />
                                </colgroup>
                                <caption>비밀번호 수정 테이블</caption>
                                <tbody>
                                    <tr>
                                        <td class="tit">
                                            <span class="field">아이디</span>
                                            <span class="data">
                                                <asp:Literal runat="server" ID="userid" /></span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <buton class="change_pw">비밀번호 변경하기</buton>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr class="pw_hidden">
                                        <td colspan="2">
                                            <label for="cur_pwd" class="hidden">현재 비밀번호</label>
                                            <input type="password" runat="server" id="cur_pwd" class="input_type2" style="width: 400px" placeholder="현재 비밀번호" />
                                            <p><span class="guide_comment2" id="cur_user_password" style="display: none"></span></p>
                                        </td>
                                    </tr>
                                    <tr class="pw_hidden">
                                        <td colspan="2">
                                            <label for="pwd" class="hidden">신규 비밀번호</label>
                                            <input type="password" runat="server" id="pwd" class="input_type2" style="width: 400px" placeholder="신규 비밀번호" />
                                            <p><span class="guide_comment2" data-id="msg_user_pwd" id="new_user_password" style="display: none">띄어쓰기 없이 영문, 숫자, 특수문자 3가지 조합으로 5~15자 이내(대소문자 구별)로 입력해주세요.</span></p>
                                        </td>
                                    </tr>
                                    <tr class="pw_hidden">
                                        <td colspan="2">
                                            <label for="re_pwd" class="hidden">비밀번호 확인</label>
                                            <input type="password" runat="server" id="re_pwd" class="input_type2" style="width: 400px" placeholder="비밀번호 확인" />
                                            <p><span class="guide_comment2" style="display: none" id="chk_user_password">비밀번호가 일치하지 않습니다.</span></p>
                                        </td>
                                    </tr>
                                    <tr class="pw_hidden">
                                        <td colspan="2">
                                            <p><span class="s_con1">띄어쓰기 없이 영문, 숫자, 특수문자 3가지 조합으로 5~15자 이내(대소문자 구별)로 입력해주세요.</span></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!--// -->

                    <!-- 기본정보 -->
                    <div class="input_div">
                        <div class="login_field"><span>기본정보</span></div>
                        <div class="login_input">

                            <table class="tbl_join">
                                <colgroup>
                                    <col style="width: 400px" />
                                    <col style="width: *" />
                                </colgroup>
                                <caption>기본정보 입력 테이블</caption>
                                <tbody>
                                    <tr>
                                        <td class="tit">
                                            <span class="field">이름</span>
                                            <span class="data">
                                                <asp:Literal runat="server" ID="username" /></span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <span class="form_disabled fl mr10">
                                                <input type="text" runat="server" id="birth_year" style="background-color: transparent" readonly="readonly" /></span>
                                            <span class="form_disabled fl mr10">
                                                <input type="text" runat="server" id="birth_mm" style="background-color: transparent" readonly="readonly" /></span>
                                            <span class="form_disabled">
                                                <input type="text" runat="server" id="birth_dd" style="background-color: transparent" readonly="readonly" /></span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="form_disabled cert">본인확인<span class="posR"><asp:Literal runat="server" ID="auth_method" /></span></span>
                                        </td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td>

                                            <label for="mobile" class="hidden">휴대폰번호</label>
                                            <span class="relative">
                                                <input type="text" runat="server" id="mobile" maxlength="11" class="number_only input_type2 tar" />
                                                <span class="posL">휴대폰번호</span>
                                            </span>
                                            <p><span class="guide_comment2" id="msg_chk_mobile" style="display: none"></span></p>
                                        </td>
                                        <td><span class="pos1">
                                            <button class="btn_s_type8 mobile_auth" id="btn_auth_mobile" style="display: none">번호변경 인증</button></span></td>
                                    </tr>
                                    <tr class="mobile_auth" style="display: none">
                                        <td>
                                            <label for="mobile_auth_no" class="hidden" runat="server">인증번호 입력</label>
                                            <input type="text" id="mobile_auth_no" class="input_type2" placeholder="인증번호" />
                                            <p><span class="guide_comment1" id="auth_result" style="display: none"></span></p>
                                        </td>
                                        <td><span class="pos1">
                                            <button class="btn_s_type8" id="btn_auth_confirm_mobile">확인</button></span></td>
                                    </tr>

                                    <tr>
                                        <td>

                                            <label for="email" class="hidden">이메일</label>
                                            <span class="relative">
                                                <input type="text" runat="server" id="email" class="input_type2 tar" />
                                                <span class="posL">이메일</span>
                                            </span>
                                            <p><span class="guide_comment2" id="msg_chk_email" style="display: none"></span></p>
                                        </td>
                                        <td><span class="pos1">
                                            <button class="btn_s_type8 email_auth" id="btn_auth_email" style="display: none">인증</button></span></td>
                                    </tr>
                                    <tr class="email_auth" style="display: none" id="chk_mail_main">
                                        <td class="hide_mail">
                                            <p>
                                                <span class="guide_comment1" id="mgs_chk_email_result" style="display: none"></span>
                                            </p>
                                        </td>
                                        <td class="hide_mail"><span class="pos1">
                                            <button class="btn_s_type8" id="btn_auth_confirm_email" style="display: none">확인</button></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>

                                            <label for="phone" class="hidden">전화번호</label>
                                            <span class="relative">
                                                <input type="text" runat="server" id="phone" maxlength="12" class="number_only input_type2 tar" />
                                                <span class="posL">전화번호</span>
                                            </span>
                                        </td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td>

                                            <span class="checkbox_ui">
                                                <input type="checkbox" runat="server" id="agree_receive" class="css_checkbox" />
                                                <label for="agree_receive" class="css_label font2">이메일/SMS 수신 동의 (선택)</label>
                                            </span>
                                            <span class="info_guide">
                                                <button class="open">가이드보기</button>
                                                <!-- tooltip -->
                                                <span class="tooltip">
                                                    <span class="tit">이메일/SMS 수신여부에 동의하신 분에게는</span><br />
                                                    <ul>
                                                        <li>컴패션 후원과 관련한 필수 안내사항과, 매월 발행되는
															‘이메일 뉴스레터', 연 1회 발행되는 '오프라인 뉴스레터'
															의 PDF파일을 메일로 보내드립니다.
                                                        </li>
                                                        <li>휴대폰으로 이벤트 및 행사 알림 메시지를 보내드립니다.
															(일반 전화번호 제외, 휴대폰번호로만 보내드립니다.)
                                                        </li>
                                                    </ul>
                                                    <button class="close">닫기</button>
                                                    <span class="arr"></span>
                                                </span>
                                                <!--// -->
                                            </span>
                                        </td>
                                        <td></td>
                                    </tr>

                                    <tr>

                                        <td class="line">
                                            <span class="fl">
                                                <label for="lastname" class="hidden">영문성</label>
                                                <span class="relative">
                                                    <input type="text" runat="server" id="lastname" class="input_type2 tar" style="width: 195px" />
                                                    <span class="posL">영문성</span>
                                                </span>
                                            </span>
                                            <span class="fr">
                                                <label for="firstname" class="hidden">영문이름</label>
                                                <span class="relative">
                                                    <input type="text" runat="server" id="firstname" class="input_type2 tar" style="width: 195px" />
                                                    <span class="posL">영문이름</span>
                                                </span>
                                            </span>
                                        </td>
                                        <td></td>
                                    </tr>

                                    <div runat="server" id="pn_dat_data">
                                        <asp:PlaceHolder runat="server" ID="ph_has_regular_commitment" Visible="false">
                                            <!-- 해당되는 후원정보를 입력한 경우만 노출 -->
                                            <tr runat="server" id="ph_receipt2">
                                                <td class="padding2">
                                                    <span class="checkbox_ui">
                                                        <input type="checkbox" runat="server" id="is_receipt" class="css_checkbox" checked>
                                                        <label for="is_receipt" class="css_label font2">국세청 연말정산 영수증 신청</label>
                                                    </span>
                                                </td>
                                                <td></td>
                                            </tr>

                                            <tr runat="server" id="ph_receipt">
                                                <td class="line">
                                                    <label for="receipt" class="hidden">주민등록번호 입력</label>
                                                    <input type="text" runat="server" id="reveipt" data-id="jumin" maxlength="13" class="func_name_check input_type2 number_only" placeholder="주민등록번호 (-없이 입력)" />
                                                    <p><span class="guide_comment1" id="msg_name_check" style="display: none">정보 확인이 완료되었습니다.</span></p>
                                                    <ul class="mt10 func_name_check">
                                                        <li><span class="s_con1">후원자님의 주민등록번호를 입력해주세요. (타인 주민등록번호 입력 불가)</span></li>
                                                        <li><span class="s_con1">입력하신 주민등록번호로 국세청 연말정산 간소화 서비스에 등록됩니다.</span></li>
                                                    </ul>
                                                </td>
                                                <td><span class="pos2 func_name_check adminCK">
                                                    <button class="btn_s_type8" id="btn_name_check">실명인증</button></span></td>
                                            </tr>


                                            <tr>
                                                <td class="tit line">
                                                    <div class="clear2 mb15">
                                                        <span class="field">종교</span>
                                                        <span class="data">

                                                            <span class="radio_ui">
                                                                <input type="radio" runat="server" id="religion1" name="religion" class="religion css_radio" value="기독교" checked />
                                                                <label for="religion1" class="css_label">기독교</label>

                                                                <input type="radio" runat="server" id="religion2" name="religion" class="religion css_radio" value="천주교" />
                                                                <label for="religion2" class="css_label ml20">천주교</label>

                                                                <input type="radio" runat="server" id="religion3" name="religion" class="religion css_radio" value="불교" />
                                                                <label for="religion3" class="css_label ml20">불교</label>

                                                                <input type="radio" runat="server" id="religion4" name="religion" class="religion css_radio" value="무교" />
                                                                <label for="religion4" class="css_label ml20">없음</label>
                                                            </span>

                                                        </span>
                                                    </div>
                                                    <span id="direct_input" style="display:none;">
                                                        <input type="text" runat="server" id="church_name" placeholder="교회명 직접입력" class="input_type2" style="border:0;" disabled /></span>
                                                </td>
                                                <td id="pn_church" style="display:none;"><span class="pos2">
                                                    <button class="btn_s_type2" id="btn_find_church">교회찾기</button></span></td>
                                            </tr>
                                        </asp:PlaceHolder>

                                        <asp:PlaceHolder runat="server" ID="ph_has_address" Visible="false">
                                            <!-- 주소 -->
                                            <tr>


                                                <td class="tit line">

                                                    <div class="clear2 mb15">
                                                        <span class="field">주소</span>
                                                        <span class="data">
                                                            <span class="radio_ui">
                                                                <input type="radio" runat="server" class="rd_addr css_radio" id="addr_domestic" name="addr_location" checked />
                                                                <label for="addr_domestic" class="css_label">국내</label>

                                                                <input type="radio" runat="server" class="rd_addr css_radio" id="addr_oversea" name="addr_location" />
                                                                <label for="addr_oversea" class="css_label ml20">해외</label>

                                                            </span>

                                                        </span>
                                                    </div>

                                                   <span id="pn_addr_domestic" runat="server" style="display: none">
                                                        <input type="hidden" id="addr_domestic_zipcode"/>
                                                        <input type="hidden" id="addr_domestic_addr1"/>
                                                        <input type="hidden" id="addr_domestic_addr2"/>

														<p id="addr_road" class="mt15"></p>
														<p id="addr_jibun" class="mt10"></p>
                                                    </span>


                                                    <!-- 해외주소 체크 시 -->
                                                    <div id="pn_addr_overseas" runat="server" style="display: none">
                                                        <span class="sel_type2 fl" style="width: 195px;">
                                                            <label for="ddlHouseCountry" class="hidden">국가 선택</label>
                                                            <asp:DropDownList runat="server" ID="ddlHouseCountry" class="custom_sel"></asp:DropDownList>

                                                        </span>
                                                        <label for="addr_overseas_zipcode" class="hidden">우편번호</label>

                                                        <input type="text" id="addr_overseas_zipcode" class="input_type2 fr" value="" style="width: 195px" placeholder="우편번호" />
                                                        <input type="text" id="addr_overseas_addr1" class="input_type2 mt10" placeholder="주소" />
                                                        <input type="text" id="addr_overseas_addr2" class="input_type2 mt10" placeholder="상세주소" />
                                                        <p class="overseas_comment"><span class="s_con1">우편비 절감을 위해 어린이 정보 및 편지 등 후원 관련 내용은 홈페이지를 이용해 주세요!</span></p>
                                                        <p class="overseas_comment"><span class="s_con1">국외 주소의 경우, 영문만 입력 가능합니다.</span></p>
													<div class="overseas_faq adminCK"><a href="/customer/faq?k_word=해외" class="btn_arr_type1">국외 후원자 안내 FAQ 바로가기<span></span></a></div>
                                                    </div>
                                                    <!--// -->

                                                </td>
                                                <td><span class="pos2">
                                                    <button class="btn_s_type1" id="btn_find_addr">주소찾기</button></span></td>
                                            </tr>
                                        </asp:PlaceHolder>

                                        <!-- // 주소 -->
                                        <asp:PlaceHolder runat="server" ID="ph_has_regular_commitment2" Visible="false">

                                            <tr>
                                                <td class="tit line">
                                                    <!-- 본인인증하지 않은 회원 -->
                                                    <div runat="server" id="ph_cert">
                                                        <p class="mb10 pt10"><span class="s_con1">기존 후원 내역 확인을 위해 본인인증을 진행해주세요.</span></p>
                                                        <div id="func_cert">
                                                            <button style="width: 195px" class="btn_s_type2 fl" id="btn_cert_by_phone"><span>휴대폰 인증</span></button>
                                                            <button style="width: 195px" class="btn_s_type2 fr" id="btn_cert_by_ipin"><span>아이핀 인증</span></button>
                                                        </div>
                                                        <div id="msg_cert" class="guide_comment1" style="display: none"></div>
                                                    </div>
                                                    <!--// -->
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="tit line">
                                                    <div class="clear2 mb10">
                                                        <span class="field">후원정보수신</span>
                                                        <span class="data">
                                                            <span class="radio_ui">

                                                                <input type="radio" runat="server" id="sponsor_info_y" name="sponsor_info" class="css_radio" checked />
                                                                <label for="sponsor_info_y" class="css_label">예</label>

                                                                <input type="radio" runat="server" id="sponsor_info_n" name="sponsor_info" class="css_radio" />
                                                                <label for="sponsor_info_n" class="css_label ml20">아니오</label>

                                                            </span>

                                                        </span>
                                                    </div>
                                                    <span class="s_con1">입력하신 주소로 후원과 관련한 우편물을 수신하시겠습니까?</span>
													<span class="s_con1">단, 후원과 관련하여 후원자님께 중요하게 안내되어야 하는 우편물은<br />수신 여부와 상관없이 발송됩니다.</span>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder runat="server" ID="ph_send_info" Visible="false">
                                            <tr>
                                                <td class="tit line">
                                                    <div class="clear2 mb10">
                                                        <span class="field">후원정보수신</span>
                                                        <span class="data">
                                                            <span class="radio_ui">

                                                                <input type="radio" runat="server" id="sponsor_info_yes" name="sponsor_info" class="css_radio" checked />
                                                                <label for="sponsor_info_yes" class="css_label">예</label>

                                                                <input type="radio" runat="server" id="sponsor_info_no" name="sponsor_info" class="css_radio" />
                                                                <label for="sponsor_info_no" class="css_label ml20">아니오</label>

                                                            </span>

                                                        </span>
                                                    </div>
                                                    <span class="s_con1">입력하신 주소로 후원과 관련한 우편물을 수신하시겠습니까?</span>
                                                    <span class="s_con1">단, 후원과 관련하여 후원자님께 중요하게 안내되어야 하는 우편물은<br />
                                                        수신 여부와 상관없이 발송됩니다.</span>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </asp:PlaceHolder>

                                    </div>
                                    <!--// 해당되는 후원정보를 입력한 경우만 노출 -->
                                    <tr runat="server" id="ph_translate" visible="false">
                                        <td class="tit line">
                                            <div class="clear2 mb5">
                                                <span class="field">어린이 편지 번역 여부</span>
                                                <span class="data">
                                                    <span class="radio_ui">
                                                        <input type="radio" runat="server" id="translate_n" name="translate" class="css_radio" checked />
                                                        <label for="translate_n" class="css_label">영문</label>
                                                        <input type="radio" runat="server" id="translate_y" name="translate" class="css_radio" />
                                                        <label for="translate_y" class="css_label ml20 mr10">한글</label>

                                                    </span>

                                                    <span class="info_guide">
                                                        <button class="open">가이드보기</button>
                                                        <!-- tooltip -->
                                                        <span class="tooltip layer2">
                                                            <ul>
                                                                <li>영문으로 선택하시면 어린이편지를 더 빨리 받으실 수 있습니다.</li>
                                                                <li>어린이에게 편지를 보내실 때는 위 선택과 관계없이 한글 또는 영문으로 작성하실 수 있습니다.</li>
                                                            </ul>
                                                            <button class="close">닫기</button>
                                                            <span class="arr"></span>
                                                        </span>
                                                        <!--// -->
                                                    </span>
                                                </span>
                                            </div>
                                        </td>
                                        <td></td>
                                    </tr>

                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!--// 기본정보 -->

                    <div class="tac mb30 adminCK">
                        <a href="/my/" class="btn_type2 mr10">취소</a>
                        <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="btn_type1">확인</asp:LinkButton>
                    </div>
                    <p class="tac mb30"><span class="guide_comment2" id="msg_must_item" style="display: none">필수 항목을 입력해주세요.</span></p>

                    <div class="box_type3 adminCK">
                        한국컴패션을 더 이상 이용하지 않는다면<a href="/my/withdraw/" class="btn_withdraw">회원탈퇴 바로가기</a>
                    </div>

                </div>
                <!--// content -->
            </div>

            <div class="h100"></div>
        </div>
        <!--// e: sub contents -->

    </section>
    <!--// sub body -->



</asp:Content>
