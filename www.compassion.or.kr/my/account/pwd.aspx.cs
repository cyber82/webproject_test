﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_account_pwd : FrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}

	public override bool RequireSSL {
		get {
			return utils.chServerState(); //true;
        }
	}


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		redirect.Value = Request.QueryString["r"].EmptyIfNull();

	}


	protected void btn_submit_Click( object sender, EventArgs e ) {

		UserInfo sess = new UserInfo();
		string userId = sess.UserId;
		string userPwd = pwd.Value;

		using(AuthDataContext dao = new AuthDataContext()) {
            //var data = dao.sp_tSponsorMaster_get_f(userId, userPwd, "", "", "", "", "").ToList();
            Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
            Object[] op2 = new Object[] { userId, userPwd, "", "", "", "", "" };
            var data = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<CommonLib.sp_tSponsorMaster_get_fResult>();

            if (data.Count < 1) {
				base.AlertWithJavascript("비밀번호가 일치하지 않습니다.");
				return;
			}
			// 
			Session["my_pwd_accept"] = true;
			if(!string.IsNullOrEmpty(redirect.Value)) {
				Response.Redirect(redirect.Value);
				return;
			}
		}


	}
}