﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
public partial class my_account_email_confirm : FrontBasePage {
	
	protected override void OnBeforePostBack() {

		var code = Request.QueryString["code"].EmptyIfNull();

		if(string.IsNullOrEmpty(code))
			return;

        using (AuthDataContext dao = new AuthDataContext())
        {
            var exist = www6.selectQAuth<user_email_auth>("ue_id", Guid.Parse(code));
            //if (dao.user_email_auth.Any(p => p.ue_id == Guid.Parse(code)))
            if(exist.Any())
            {
                //var entity = dao.user_email_auth.First(p => p.ue_id == Guid.Parse(code));
                var entity = www6.selectQFAuth<user_email_auth>("ue_id", Guid.Parse(code));
                entity.ue_active = true;
                //dao.SubmitChanges();
                www6.update(entity);

                Response.Write("<script>alert('인증되었습니다.');window.close();</script>");

            }
        }


	
	}

}