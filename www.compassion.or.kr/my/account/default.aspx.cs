﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class my_account_default : FrontBasePage {

	public override bool RequireLogin {
		get {
			return true;
		}
	}

	public override bool RequireSSL {
		get {
            return utils.chServerState(); //true;
		}
	}

    
    WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
	
	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

        this.ViewState["AdminCK"] = "false";
        this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];

		UserInfo sess = new UserInfo();

		if (sess.UserClass == "기업" ) {
			Response.Redirect("/my/account/company/");
			return;
		}
        // 20171204 김은지
        // 어드민 로그인시 바로 페이지 이동
        var UserCookie = FrontLoginSession.GetCookie(this.Context);
        if (UserCookie.AdminLoginCheck)
        {
            this.ViewState["pwd"] = "ADMINCHECK";
            this.ViewState["AdminCK"] = "true";
        }
        else{

            user_name.Value = sess.UserName;
            hd_auth_domain.Value = ConfigurationManager.AppSettings["domain_auth"];

            if (this.Session["pwd_confirm"] == null)
                Response.Redirect("/my/confirm-pwd/?r=" + Request.RawUrl);

            this.ViewState["pwd"] = this.Session["pwd_confirm"].ToString();
        }
		this.Session["pwd_confirm"] = null;
        
		if(!this.GetWebInfo())
			return;

		if(!this.GetDatInfo())
			return;

	}

	// 웹회원정보
	bool GetWebInfo() {

		UserInfo sess = new UserInfo();

        var UserCookie = FrontLoginSession.GetCookie(this.Context);

        using (AuthDataContext dao = new AuthDataContext())
        {
            if (UserCookie.AdminLoginCheck)
            {
                sp_tSponsorMaster_get_f_ADMINResult user = null;
                //var admindata = dao.sp_tSponsorMaster_get(sess.UserId).ToList();
                //var admindata = dao.sp_tSponsorMaster_get_f(sess.UserId, this.ViewState["pwd"].ToString(), "", "", "", "", "").ToList();
                Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile"};
                Object[] op2 = new Object[] { sess.UserId, this.ViewState["pwd"].ToString(), "", "", "", "", "" };
                var admindata = www6.selectSPAuth("sp_tSponsorMaster_get_f_ADMIN", op1, op2).DataTableToList<sp_tSponsorMaster_get_f_ADMINResult>();

                if (admindata.Count < 1)
                {
                    base.AlertWithJavascript("회원정보를 찾을 수 없습니다.");
                    return false;
                }

                user = admindata.First();
                //Response.Write(user.ToJson());

                userid.Text = user.UserId;
                username.Text = user.SponsorName;

                var birthDate = user.BirthDate;
                if (!string.IsNullOrEmpty(birthDate) && birthDate.Length >= 8)
                {
                    birth_year.Value = birthDate.Substring(0, 4);
                    birth_mm.Value = birthDate.Substring(4, 2);
                    birth_dd.Value = birthDate.Substring(6, 2);
                }

                // 본인인증 정보가 있으면 
                if (user.CertifyDate.HasValue)
                {
                    auth_method.Text = "본인인증";
                }
                else
                {
                    if (user.AuthMethod == "phone")
                    {
                        auth_method.Text = "휴대폰 인증";


                    }
                    else if (user.AuthMethod == "email")
                    {
                        auth_method.Text = "이메일 인증";


                    }
                    else
                    {
                        // 인증안됨
                    }
                }

                hd_mobile.Value = mobile.Value = user.Mobile;
                hd_email.Value = email.Value = user.Email;
                phone.Value = user.Phone;

                if (user.AgreeEmail.HasValue && user.AgreePhone.HasValue)
                {
                    agree_receive.Checked = user.AgreeEmail.Value && user.AgreePhone.Value;
                }

                var str_addr1 = user.Addr1.EmptyIfNull().Split('$')[0];
                var str_location = user.Addr1.EmptyIfNull().Split('$').Length > 1 ? user.Addr1.EmptyIfNull().Split('$')[1] : "한국";

                addr_domestic.Checked = user.LocationType == "국내";
                addr_oversea.Checked = user.LocationType == "국외";
                zipcode.Value = user.ZipCode;
                addr1.Value = str_addr1;
                addr2.Value = user.Addr2;

                // 국외인경우 국가목록
                var actionResult = new CodeAction().Countries();
                if (!actionResult.success)
                {
                    base.AlertWithJavascript(actionResult.message);
                    return false;
                }

                ddlHouseCountry.DataSource = actionResult.data;
                ddlHouseCountry.DataTextField = "CodeName";
                ddlHouseCountry.DataValueField = "CodeName";
                ddlHouseCountry.DataBind();
                ddlHouseCountry.SelectedValue = str_location;

            }
            else
            {
                sp_tSponsorMaster_get_fResult user = null;

                //var data = dao.sp_tSponsorMaster_get_f(sess.UserId, this.ViewState["pwd"].ToString(), "", "", "", "", "").ToList();
                Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                Object[] op2 = new Object[] { sess.UserId, this.ViewState["pwd"].ToString(), "", "", "", "", "" };
                var data = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

                if (data.Count < 1)
                {
                    base.AlertWithJavascript("회원정보를 찾을 수 없습니다.");
                    return false;
                }
                user = data.First();
                //Response.Write(user.ToJson());

                userid.Text = user.UserId;
                username.Text = user.SponsorName;

                var birthDate = user.BirthDate;
                if (!string.IsNullOrEmpty(birthDate) && birthDate.Length >= 8)
                {
                    birth_year.Value = birthDate.Substring(0, 4);
                    birth_mm.Value = birthDate.Substring(4, 2);
                    birth_dd.Value = birthDate.Substring(6, 2);
                }

                // 본인인증 정보가 있으면 
                if (user.CertifyDate.HasValue)
                {
                    auth_method.Text = "본인인증";
                }
                else
                {
                    if (user.AuthMethod == "phone")
                    {
                        auth_method.Text = "휴대폰 인증";


                    }
                    else if (user.AuthMethod == "email")
                    {
                        auth_method.Text = "이메일 인증";


                    }
                    else
                    {
                        // 인증안됨
                    }
                }

                hd_mobile.Value = mobile.Value = user.Mobile;
                hd_email.Value = email.Value = user.Email;
                phone.Value = user.Phone;

                if (user.AgreeEmail.HasValue && user.AgreePhone.HasValue)
                {
                    agree_receive.Checked = user.AgreeEmail.Value && user.AgreePhone.Value;
                }

                var str_addr1 = user.Addr1.EmptyIfNull().Split('$')[0];
                var str_location = user.Addr1.EmptyIfNull().Split('$').Length > 1 ? user.Addr1.EmptyIfNull().Split('$')[1] : "한국";

                addr_domestic.Checked = user.LocationType == "국내";
                addr_oversea.Checked = user.LocationType == "국외";
                zipcode.Value = user.ZipCode;
                addr1.Value = str_addr1;
                addr2.Value = user.Addr2;

                // 국외인경우 국가목록
                var actionResult = new CodeAction().Countries();
                if (!actionResult.success)
                {
                    base.AlertWithJavascript(actionResult.message);
                    return false;
                }

                ddlHouseCountry.DataSource = actionResult.data;
                ddlHouseCountry.DataTextField = "CodeName";
                ddlHouseCountry.DataValueField = "CodeName";
                ddlHouseCountry.DataBind();
                ddlHouseCountry.SelectedValue = str_location;
            }
        }

		

        return true;
	}

	// 후원회원정보
	bool GetDatInfo() {
		UserInfo sess = new UserInfo();
      
		// sponsorID가 없으면 후원회원 아님
		
		SponsorAction sponsorAction = new SponsorAction();
		JsonWriter actionResult;
		DataSet dsSponsor;
		try {

			// 주소
			actionResult = sponsorAction.GetAddress();
			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message, "goBack()");
				return false;
			}

			SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)actionResult.data;
			zipcode.Value = addr_data.Zipcode;
			addr1.Value = addr_data.Addr1;
			addr2.Value = addr_data.Addr2;
			hfAddressType.Value = addr_data.AddressType;
            dspAddrDoro.Value = addr_data.DspAddrDoro;
            dspAddrJibun.Value = addr_data.DspAddrJibun;

            //if(addr_data.ExistAddress || !string.IsNullOrEmpty(sess.SponsorID)) {
            //if (addr_data.ExistAddress ) {
            //	ph_has_address.Visible = true;
            //} else { 
            //	pn_addr_domestic.Style["display"] = "block";
            //}
            ph_has_address.Visible = true;

            if (string.IsNullOrEmpty(sess.SponsorID)) {
				//	pn_dat_data.Style["display"] = "none";
					return false;
			}

			// 휴대폰/이메일/집전화
			actionResult = sponsorAction.GetCommunications();
			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message, "goBack()");
				return false;
			}
			SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)actionResult.data;
			hd_email.Value = email.Value = comm_data.Email;
			phone.Value = comm_data.Phone;
			hd_mobile.Value = mobile.Value = comm_data.Mobile;

            // 컴파스 회원정보
            // #13223 : CurrentUser 체크 하지 않도록 수정
            //Object[] objSql = new object[1] { "SELECT top 1 * FROM tSponsorMaster with(nolock) WHERE CurrentUse = 'Y' and sponsorId = '" + sess.SponsorID + "'" };
            Object[] objSql = new object[1] { "SELECT top 1 * FROM tSponsorMaster with(nolock) WHERE sponsorId = '" + sess.SponsorID + "'" };
			dsSponsor = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

			if(dsSponsor == null || dsSponsor.Tables.Count < 1) {
				base.AlertWithJavascript("후원회원 정보를 가져오는 중에 에러가 발생했습니다.", "goBack()");
				return false;
			}

			if(dsSponsor.Tables[0].Rows.Count < 1) {
				base.AlertWithJavascript("후원회원 정보를 찾을 수 없습니다.", "goBack()");
				return false;
			}


		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			base.AlertWithJavascript("후원회원 정보를 가져오는 중에 에러가 발생했습니다.", "goBack()");
			return false;
		}

		DataTable dtSponsor = dsSponsor.Tables[0];
		addr_domestic.Checked = dtSponsor.Rows[0]["locationType"].ToString() == "국내";
		addr_oversea.Checked = dtSponsor.Rows[0]["locationType"].ToString() == "국외";
		firstname.Value = dtSponsor.Rows[0]["firstname"].ToString();
		lastname.Value = dtSponsor.Rows[0]["lastname"].ToString();
		translate_y.Checked = dtSponsor.Rows[0]["TranslationFlag"].ToString() != "N";
		translate_n.Checked = dtSponsor.Rows[0]["TranslationFlag"].ToString() == "N";
		jumin.Value = dtSponsor.Rows[0]["juminId"].ToString();
		ci.Value = dtSponsor.Rows[0]["ci"].ToString();
		di.Value = dtSponsor.Rows[0]["di"].ToString();


		// 연말정산 실명인증 노출여부
		ph_receipt2.Visible = ph_receipt.Visible = (string.IsNullOrEmpty(jumin.Value) || jumin.Value.LastIndexOf("0000000") > -1);
		

		ph_has_regular_commitment2.Visible = string.IsNullOrEmpty(ci.Value) && sess.LocationType == "국내";

        

        var religionType = dsSponsor.Tables[0].Rows[0]["ReligionType"].ToString();
		if(religionType == "기독교" || religionType == "") {
			religion1.Checked = true;
			church_name.Value = dsSponsor.Tables[0].Rows[0]["ChurchName"].ToString();

			if(church_name.Value != null) {
				actionResult = sponsorAction.GetChurch();
				if(!actionResult.success) {
					base.AlertWithJavascript(actionResult.message, "goBack()");
					return false;
				}

				SponsorAction.ChurchResult church_data = (SponsorAction.ChurchResult)actionResult.data;
				hdOrganizationID.Value = church_data.organizationId;
			}

		} else if(religionType == "천주교") {
			religion2.Checked = true;
		} else if(religionType == "불교") {
			religion3.Checked = true;
		} else {
			religion4.Checked = true;
		}

		// CI가 없으면 본인인증 노출
		ph_cert.Visible = string.IsNullOrEmpty(ci.Value);
		ph_cert.Visible = true;

		// 어린이 편지 번역여부
		if(dtSponsor.Rows[0]["TranslationFlag"].ToString() == "Y") {
			translate_y.Checked = true;
		} else {
			translate_n.Checked = true;
		}

		// 후원정보 수신
		actionResult = sponsorAction.GetReceiveSponsorInfo();
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message);
			return false;
		}

		if((bool)actionResult.data) {
			sponsor_info_y.Checked = true;
            sponsor_info_yes.Checked = true;
        } else {
			sponsor_info_n.Checked = true;
            sponsor_info_no.Checked = true;
        }
        ph_send_info.Visible = !string.IsNullOrEmpty(sess.SponsorID) && sess.LocationType == "국내" && !ph_has_regular_commitment2.Visible;

        // 이메일/SMS 수신여부
        actionResult = sponsorAction.GetAgreeCommunications();
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message);
			return false;
		}

		SponsorAction.AgreeCommunicationResult agree_data = (SponsorAction.AgreeCommunicationResult)actionResult.data;
		//Response.Write(agree_data.ToJson());
		agree_receive.Checked = agree_data.Email || agree_data.SMS;

		// 연말정산 영수증
		actionResult = sponsorAction.IsPayReceipt();
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message);
			return false;
		}
		
		is_receipt.Checked = (bool)actionResult.data;
		
		
		//
		// 어린이편지 번역 노출여부 , 1:1결연정보가 있는 경우에만 노출
		actionResult = sponsorAction.HasCDSP();
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message, "goBack()");
			return false;
		}
		ph_translate.Visible = ((DataTable)actionResult.data).Rows.Count > 0;

		ph_has_regular_commitment.Visible = !string.IsNullOrEmpty(sess.SponsorID);
        
        // 정기결제 내역이 있는 경우
        //ph_has_regular_commitment.Visible = sponsorAction.HasRegularCommitment().success;

        return true;
	}

	// 수정
	protected void btn_submit_Click( object sender, EventArgs e ) {
        this.Session["pwd_confirm"] = this.ViewState["pwd"].ToString();
    //    if(base.IsRefresh) {
	//		return;
	//	}
		UserInfo sess = new UserInfo();
		SponsorAction sponsorAction = new SponsorAction();
		JsonWriter actionResult;

		string p_translate = translate_y.Checked ? "Y" : "N";
		string p_firstName = firstname.Value.EscapeSqlInjection();
		string p_lastName = lastname.Value.EscapeSqlInjection();
		string p_juminId = jumin.Value;
		string p_location = addr_domestic.Checked ? "국내" : "국외";
		string p_country = addr_domestic.Checked ? "한국" : ddlHouseCountry.SelectedValue;
		string p_addressType = hfAddressType.Value;
		string p_zipcode = zipcode.Value;
		string p_addr1 = addr1.Value;
		string p_addr2 = addr2.Value;
		bool? p_agree_receive = null;
		bool? p_is_receipt = null;
		bool? p_sponsor_info = null;

		var religion = "";
		if(religion1.Checked)
			religion = religion1.Value;
		else if(religion2.Checked)
			religion = religion2.Value;
		else if(religion3.Checked)
			religion = religion3.Value;
		else if(religion4.Checked)
			religion = religion4.Value;
		//string sResult;
		string sChurchName = "";
		string sOrganizationID = "";

		if(!religion1.Checked) {
			church_name.Value = "";
			sChurchName = "";
		} else {
			if(church_name.Value.Trim().Length > 0 && hdOrganizationID.Value.ToString().Trim().Length > 0) {
				sChurchName = church_name.Value.Trim();
				sOrganizationID = hdOrganizationID.Value.ToString().Trim();
			} else if(church_name.Value.Trim().Length > 0)
				sChurchName = church_name.Value.Trim();

		}

        if(ph_has_regular_commitment.Visible) {
            p_agree_receive = agree_receive.Checked;
            p_is_receipt = is_receipt.Checked;
            if(ph_send_info.Visible) {
                p_sponsor_info = sponsor_info_yes.Checked;

            } else {
                p_sponsor_info = sponsor_info_y.Checked;
            }
        }


        // 어린이편지 번역여부
        if(ph_translate.Visible) {
                p_translate = translate_y.Checked ? "Y" : "N";
            }

        if(password_popup.Value == "Y") {
            if(string.IsNullOrEmpty(cur_pwd.Value)) {
                base.AlertWithJavascript("현재 비밀번호를 입력해주세요");
                return;
            }

            if(string.IsNullOrEmpty(pwd.Value)) {
                base.AlertWithJavascript("신규 비밀번호를 입력해주세요");
                return;
            }

            if(pwd.Value != re_pwd.Value) {
                base.AlertWithJavascript("신규 비밀번호와 비밀번호 확인정보가 일치하지 않습니다. 다시한번 확인해주세요");
                return;
            }
        }
		if (string.IsNullOrEmpty(mobile.Value) && string.IsNullOrEmpty(email.Value)){
			base.AlertWithJavascript("휴대폰번호와 이메일주소 둘중에 하나는 입력해주세요.");
			return;
		}

		if(!string.IsNullOrEmpty(sess.SponsorID)) {

			if (string.IsNullOrEmpty(firstname.Value)) {
				base.AlertWithJavascript("영문이름(성)을 입력해주세요");
				return;
			}

			if(string.IsNullOrEmpty(lastname.Value)) {
				base.AlertWithJavascript("영문이름(이름)을 입력해주세요");
				return;
			}

            if(!(string.IsNullOrEmpty(zipcode.Value) && string.IsNullOrEmpty(addr1.Value) && string.IsNullOrEmpty(addr2.Value))) {
                if(string.IsNullOrEmpty(zipcode.Value)) {
                    base.AlertWithJavascript("우편번호를 입력해주세요");
                    return;
                }

                if(string.IsNullOrEmpty(addr1.Value)) {
                    base.AlertWithJavascript("주소를 입력해주세요");
                    return;
                }

                if(string.IsNullOrEmpty(addr2.Value)) {
                    base.AlertWithJavascript("상세주소를 입력해주세요");
                    return;
                }
            }

        }


        using (AuthDataContext dao = new AuthDataContext())
        {
            if (password_popup.Value == "Y")
            {
                //var data = dao.sp_tSponsorMaster_get_f(sess.UserId, cur_pwd.Value, "", "", "", "", "").ToList();
                Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                Object[] op2 = new Object[] { sess.UserId, cur_pwd.Value, "", "", "", "", "" };
                var data = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

                if (data.Count < 1)
                {
                    base.AlertWithJavascript("비밀번호가 일치하지 않습니다.");
                    return;
                }

                //dao.sp_tSponsorMaster_change_pwd_f(sess.UserId, pwd.Value);
                Object[] op3 = new Object[] { "UserID", "UserPW"};
                Object[] op4 = new Object[] { sess.UserId, pwd.Value };
                www6.selectSPAuth("sp_tSponsorMaster_change_pwd_f", op3, op4);

                this.Session["pwd_confirm"] = null;

            }
        }

        actionResult = sponsorAction.UpdateSponsor(sess.SponsorID, p_firstName, p_lastName, "", p_translate, mobile.Value, email.Value, phone.Value, p_location, p_country,
		p_addressType, p_zipcode, p_addr1, p_addr2 , religion , sChurchName , sOrganizationID ,  null , null , p_agree_receive, p_is_receipt, p_sponsor_info);

        // 영문이름만 업데이트 추가 문희원 2017-05-31-
        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            var objSql = new object[1] { " UPDATE  " +
                                            " kr_compass4.dbo.tSponsorMaster  " +
                                         " SET FirstName = '"+p_firstName+"', LastName = '"+p_lastName+"'  " +
                                         " WHERE UserID = '" + sess.UserId + "'" };
            _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
        }
		
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message);
			return;
		}
        
        base.AlertWithJavascript("성공적으로 수정되었습니다." , "location.href='/my/account'");
		
	}
}