﻿$(function () {

    $page.init();
    // 비밀번호 수정입력란 show/hide
    $(".tbl_join .change_pw").click(function () {
        $(".tbl_join .pw_hidden").toggle();

        if ($(this).hasClass("on")) {
            $(this).removeClass("on");
            $("#password_popup").val("N");
            $("#cur_pwd, #pwd, #re_pwd").val("");
        }
        else {
            $(this).addClass("on");
            $("#password_popup").val("Y");
        }
    })
    // 툴팁
    $("button.open").click(function () {
        $(this).next(".tooltip").fadeIn("fast");
        return false;
    });
    $("button.close").click(function () {
        $(this).parent(".tooltip").fadeOut("fast");
        return false;
    });
})

function jusoCallback(zipNo, addr1, addr2, jibun) {
	// 실제 저장 데이타
	$("#addr_domestic_zipcode").val(zipNo);
	$("#addr_domestic_addr1").val(addr1 + "//" + jibun);
	$("#addr_domestic_addr2").val(addr2);

	// 화면에 표시
	$("#addr_domestic_1").val(zipNo);
	//$("#addr_domestic_2").val(addr2);

	$("#addr_road").text("[도로명주소] (" + zipNo + ") " + addr1 + " " + addr2);
	$("#addr_jibun").text("[지번주소] (" + zipNo + ") " + jibun + " " + addr2);
}

var $page = {

	timer: null,

	init: function () {

		// 국내 , 국외선택
		$(".rd_addr").change(function () {
			if ($("#addr_domestic").prop("checked")) {
				$(".hide_overseas").show();
				$("#pn_addr_domestic").show();
				$("#pn_addr_overseas").hide();
				$("#btn_find_addr").show();
			} else {
				$(".hide_overseas").hide();
				$("#pn_addr_domestic").hide();
				$("#pn_addr_overseas").show();
				$("#btn_find_addr").hide();
			}
		})

		$("#btn_submit").click(function () {
			return $page.onSubmit();
		})

		if ($(".religion").val() == "기독교") {
		    $("#direct_input").show();
		}
		$(".religion").change(function () {
		    if ($("#religion1").prop("checked")) {
		        $("#direct_input").show();
		    } else {
		        $("#direct_input").hide();
		    }

		})

		$("#btn_find_church").click(function () {

			cert_setDomain();
			addr = "/common/popup/PopOrganization.aspx?Type=Church&hdOrganizationID=hdOrganizationID"
                    + "&txtOrganizationName=church_name&SearchTxt=" + $("#church_name").val();

			window.open(addr, "교회검색", "width=600,height=405,resizable=yes");

			return false;
		})


		$("#btn_find_addr").click(function () {
			cert_setDomain();
			var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");

			return false;
		})

		$("#church_name").focus(function () {
			$("#church_name").val("");
			$("#hdOrganizationID").val("");
		})

		if ($("#addr_domestic").prop("checked")) {

			$("#pn_addr_domestic").show();
			$("#pn_addr_overseas").hide();
			$("#btn_find_addr").show();


			$("#addr_domestic_zipcode").val($("#zipcode").val());
			$("#addr_domestic_addr1").val($("#addr1").val());
			$("#addr_domestic_addr2").val($("#addr2").val());

			// 컴파스의 데이타를 불러오는경우 
			if ($("#dspAddrDoro").val() != "") {
				$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
				if ($("#dspAddrJibun").val() != "") {
					$("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
				}

			} else if ($("#addr1").val() != "") {
				addr_array = $("#addr1").val().split("//");
				$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + addr_array[0] + " " + $("#addr2").val());
				if (addr_array[1]) {
					$("#addr_jibun").text("[지번주소] (" + $("#zipcode").val() + ") " + addr_array[1] + " " + $("#addr2").val());
				}
			}

		} else {
			$("#pn_addr_domestic").hide();
			$("#pn_addr_overseas").show();
			$("#btn_find_addr").hide();

			$("#addr_overseas_zipcode").val($("#zipcode").val());
			$("#addr_overseas_addr1").val($("#addr1").val());
			$("#addr_overseas_addr2").val($("#addr2").val());

		}

		$(".rd_addr").trigger("change");

		$page.setIdentificationByPhoneEvent();

		$page.setIdentificationByEmailEvent();

	},

	// 보인인증-휴대폰 이벤트
	setIdentificationByPhoneEvent: function () {

		// 휴대폰 번호를 변경하면 인증창 노출
		$("#mobile").keyup(function () {
			if ($("#mobile").val() == $("#hd_mobile").val()) {
				$("#hd_mobile_auth").val("Y");
				$(".mobile_auth").hide();
				$("#msg_chk_mobile").hide();
				$("#msg_must_item").hide();
			} else {
				$("#hd_mobile_auth").val("N");
				$(".mobile_auth").show();
			}

			if ($("#mobile").val() == "") {
				$("#hd_mobile_auth").val("Y");
				$("#hd_mobile").val("");
				$(".mobile_auth").hide();
			}
		});

		// 인증요청
		$("#btn_auth_mobile").click(function () {

			var phone = $("#mobile").val();
			var name = $("#user_name").val();
			
			if (!/^[0-9.]{10,11}$/.test(phone)) {
			    $("#msg_chk_mobile").removeClass("guide_comment1");
			    $("#msg_chk_mobile").html("10자 이상의 숫자만 입력 가능합니다").addClass("guide_comment2").show();
				$("#mobile").focus();
				return false;
			}

			if ($page.timer != null)
				window.clearTimeout($page.timer);

			// 연속클릭방지
			$page.timer = window.setTimeout(function () {
				
				$.get("/api/my/account.ashx", { t: "send-phone-nocheck", c: phone, n: name }, function (r) {

					$page.timer = null;
					if (r.success) {
					    $("#msg_chk_mobile").removeClass("guide_comment2");
					    $("#msg_chk_mobile").html("인증번호가 발송되었습니다.").addClass("guide_comment1").show();
					} else {
					    $("#msg_chk_mobile").removeClass("guide_comment1");
					    $("#msg_chk_mobile").html(r.message).addClass("guide_comment2").show();
					}

				});

			}, 1000);

			return false;
		});

		// 인증확인
		$("#btn_auth_confirm_mobile").click(function () {

			var code = $("#mobile_auth_no").val();

			$.get("/api/my/account.ashx", { t: "check-phone-nocheck", c: code }, function (r) {

				if (r.success) {
					var phone = r.data;
					$("#hd_mobile").val(phone);
					$("#hd_mobile_auth").val("Y");

					$("#auth_result").removeClass("guide_comment2");
					$("#auth_result").html("인증이 완료되었습니다.").addClass("guide_comment1").show();
					$("#mobile_auth_no, #msg_chk_mobile, #btn_auth_confirm_mobile").hide();
				} else {
				    $("#auth_result").removeClass("guide_comment1");
				    $("#auth_result").html("인증 번호가 일치하지 않습니다. 다시 입력해주세요.").addClass("guide_comment2").show();
				}

			});

			return false;
		});

	},

	// 보인인증-이메일 이벤트
	setIdentificationByEmailEvent: function () {
		// 이메일을 변경하면 인증창 노출
		$("#email").keyup(function () {
			if ($("#email").val() == $("#hd_email").val()) {
				$("#hd_email_auth").val("Y");
				$("#btn_auth_email").hide();
				$("#btn_auth_confirm_email").hide();
				$("#mgs_chk_email_result").hide();
				$("#msg_chk_email").hide();
				$("#msg_must_item").hide();
				$(".hide_mail").hide();
			} else {
				$("#hd_email_auth").val("N");
				$("#btn_auth_email").show();
				$("#btn_auth_confirm_email").show();
				$("#mgs_chk_email_result").show();
				$("#msg_must_item").show();
				$(".hide_mail").show();
			}

			if ($("#email").val() == "") {
			    $("#hd_email_auth").val("Y");

				$("#hd_email").val("");
				$("#btn_auth_email").hide();
			}

		});


		// 인증요청
		$("#btn_auth_email").click(function () {

			var email = $("#email").val();
			var name = $("#user_name").val();
			var birth_yyyy = $("#birth_year").val();
			var birth_mm = $("#birth_mm").val();
			var birth_dd = $("#birth_dd").val();
			var birth = birth_yyyy + "-" + birth_mm + "-" + birth_dd;

			if (!emailValidation.checkWithAlert($("#email"))) {
				$("#email").focus();
				return false;
			}

			if ($page.timer != null)
				window.clearTimeout($page.timer);

			// 연속클릭방지
			$page.timer = window.setTimeout(function () {

				$.get("/api/my/account.ashx", { t: "send-email-nocheck", c: email, n: name, b: birth }, function (r) {

					$page.timer = null;
					if (r.success) {

					    $("#msg_chk_email").removeClass("guide_comment2");
					    $("#mgs_chk_email_result").removeClass("guide_comment2");
					    $("#msg_chk_email").html("메일을 발송하였습니다.").addClass("guide_comment1").show();
					    $("#mgs_chk_email_result").html("입력하신 이메일에서 인증코드를 클릭하셨으면 [확인] 버튼을<br />클릭해주세요.").addClass("guide_comment1").show();
					    $("#chk_mail_main, #btn_auth_confirm_email").show();
					} else {
					    $("#msg_chk_email").removeClass("guide_comment1");
					    $("#msg_chk_email").html(r.message).addClass("guide_comment2").show();
					}

				});

			}, 1000);

			return false;
		});

		// 인증확인
		$("#btn_auth_confirm_email").click(function () {

			var email = $("#email").val();

			$.get("/api/my/account.ashx", { t: "check-email-nocheck", c: email }, function (r) {

				if (r.success) {
					var email = r.data;
					$("#hd_email").val(email);
					$("#hd_email_auth").val("Y");

					$("#mgs_chk_email_result").removeClass("guide_comment2");
					$("#mgs_chk_email_result").html("인증이 완료 되었습니다.").addClass("guide_comment1").show();
					$("#msg_chk_email, #btn_auth_confirm_email").hide();
				} else {
				    $("#mgs_chk_email_result").removeClass("guide_comment1");
				    $("#mgs_chk_email_result").html("인증이 완료 되지 않았습니다").addClass("guide_comment2").show();
				}

			});

			return false;
		});

	},

	onSubmit : function(){
	    if ($("#password_popup").val() == "Y") {
	        if ($("#cur_pwd").val() == "") {
	            $("#cur_user_password").html("현재비밀번호를 입력해주세요").show();
	            $("#cur_pwd").focus();
	            return false;
	        }

	        var pwd_result = passwordValidation.check($("#pwd"), $("#re_pwd"), 5, 15);
	        if (!pwd_result.result) {
	            $("#pwd").focus();
	            $("#chk_user_password").html(pwd_result.msg).show();
	            return false;
	        }
	    }
		if ($("#email").val() != "" && !/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test($("#email").val())) {
		    $("#msg_must_item").html("이메일주소가 올바르지 않습니다.").show();
			$("#email").focus();
			return false;
		}

		if ($("#mobile").val() == "" && $("#email").val() == "") {
		    $("#msg_must_item").html("휴대폰번호와 이메일주소 둘중에 하나는 입력해주세요.").show();
			return false;
		}

		if ( $("#hd_mobile_auth").val() == "N") {
		    $("#msg_must_item").html("휴대폰 인증을 해주세요").show();
			return false;
		}

		if ( $("#hd_email_auth").val() == "N") {
		    $("#msg_must_item").html("이메일 인증을 해주세요").show();
			return false;
		}


		if ($("#firstname").length > 0 && $("#firstname").val() == "") {
		    $("#msg_must_item").html("영문이름(성)을 입력해주세요").show();
			$("#firstname").focus();
			return false;
		}

		if ($("#lastname").length > 0 && $("#lastname").val() == "") {
		    $("#msg_must_item").html("영문이름(이름)을 입력해주세요").show();
			$("#lastname").focus();
			return false;
		}
		

		if ($("#addr_domestic").length > 0 && $("#addr_domestic").prop("checked")) {
		    if ($("#addr_domestic_zipcode").val() == "") {
			    $("#msg_must_item").html("주소를 등록해주세요").show();
				return false;
			}

		    $("#zipcode").val($("#addr_domestic_zipcode").val());
		    $("#addr1").val($("#addr_domestic_addr1").val());
		    $("#addr2").val($("#addr_domestic_addr2").val());
		}

		if ($("#addr_oversea").length > 0 && $("#addr_oversea").prop("checked")) {
			if ($("#addr_overseas_zipcode").val() == "") {
			    $("#msg_must_item").html("우편번호를 입력해주세요").show();
				$("#addr_overseas_zipcode").focus();
				return false;
			}
			if ($("#addr_overseas_addr1").val() == "") {
			    $("#msg_must_item").html("주소를 입력해주세요").show();
				$("#addr_overseas_addr1").focus();
				return false;
			}
			if ($("#addr_overseas_addr2").val() == "") {
			    $("#msg_must_item").html("상세주소를 입력해주세요").show();
				$("#addr_overseas_addr2").focus();
				return false;
			}

			$("#zipcode").val($("#addr_overseas_zipcode").val());
			$("#addr1").val($("#addr_overseas_addr1").val());
			$("#addr2").val($("#addr_overseas_addr2").val());
		}

	}
}
