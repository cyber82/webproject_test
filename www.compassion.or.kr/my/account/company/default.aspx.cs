﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class my_account_company_default : FrontBasePage {

	public override bool RequireLogin {
		get {
			return true;
		}
	}

	public override bool RequireSSL {
		get {
			return utils.chServerState(); //true;
        }
	}

	WWWService.Service _wwwService = new WWWService.Service();
	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
	
	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		

		this.ViewState["firstname"] = "";
		this.ViewState["lastname"] = "";
		this.ViewState["zipcode"] = "";
		this.ViewState["zipcode"] = "";
		this.ViewState["zipcode"] = "";

		this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];

		UserInfo sess = new UserInfo();
		if(sess.UserClass != "기업") {
			Response.Redirect("/my/account/");
			return;
		}

		user_name.Value = sess.UserName;
		
		hd_auth_domain.Value = ConfigurationManager.AppSettings["domain_auth"];

		if(this.Session["pwd_confirm"] == null)
			Response.Redirect("/my/confirm-pwd/?r=" + Request.RawUrl);

		this.ViewState["pwd"] = this.Session["pwd_confirm"].ToString();
		//this.Session["pwd_confirm"] = null;

		if(!this.GetWebInfo())
			return;

		if(!this.GetDatInfo())
			return;

	}

	// 웹회원정보
	bool GetWebInfo()
    {

		UserInfo sess = new UserInfo();

		sp_tSponsorMaster_get_fResult user = null;
        using (AuthDataContext dao = new AuthDataContext())
        {
            //var data = dao.sp_tSponsorMaster_get_f(sess.UserId, this.ViewState["pwd"].ToString(), "", "", "", "", "").ToList();
            Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
            Object[] op2 = new Object[] { sess.UserId, this.ViewState["pwd"].ToString(), "", "", "", "", "" };
            var data = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

            if (data.Count < 1)
            {
                base.AlertWithJavascript("회원정보를 찾을 수 없습니다.");
                return false;
            }

            user = data.First();
        }

			//Response.Write(user.ToJson());

		userid.Text = user.UserId;
		username.Text = user.SponsorName;
		comRegistrationNo.Text = user.ComRegistration;
		managerName.Text = user.ManagerName;


		hd_mobile.Value = mobile.Value = user.Mobile;
		hd_email.Value = email.Value = user.Email;
		phone.Value = user.Phone;

		if(user.AgreeEmail.HasValue && user.AgreePhone.HasValue) {
			agree_receive.Checked = user.AgreeEmail.Value && user.AgreePhone.Value;
		}

        var str_addr1 = user.Addr1.EmptyIfNull().Split('$')[0];
        var str_location = user.Addr1.EmptyIfNull().Split('$').Length > 1 ? user.Addr1.EmptyIfNull().Split('$')[1] : "한국";


        addr_domestic.Checked = user.LocationType == "국내";
        addr_oversea.Checked = user.LocationType == "국외";
        this.ViewState["zipcode"] = zipcode.Value = user.ZipCode;
		this.ViewState["addr1"] = addr1.Value = str_addr1;
		this.ViewState["addr2"] = addr2.Value = user.Addr2;

        // 국외인경우 국가목록
       var actionResult = new CodeAction().Countries();
        if (!actionResult.success) {
            base.AlertWithJavascript(actionResult.message);
            return false;
        }

        ddlHouseCountry.DataSource = actionResult.data;
        ddlHouseCountry.DataTextField = "CodeName";
        ddlHouseCountry.DataValueField = "CodeName";
        ddlHouseCountry.DataBind();
        ddlHouseCountry.SelectedValue = str_location;

        return true;
	}

	// 후원회원정보
	bool GetDatInfo() {

		UserInfo sess = new UserInfo();
        SponsorAction sponsorAction = new SponsorAction();
        JsonWriter actionResult;
        DataSet dsSponsor;


        // 주소
        actionResult = new SponsorAction().GetAddress();
        if(!actionResult.success) {
            base.AlertWithJavascript(actionResult.message, "goBack()");
            return false;
        }

        SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)actionResult.data;
		this.ViewState["zipcode"] = zipcode.Value = addr_data.Zipcode;
		this.ViewState["addr1"] = addr1.Value = addr_data.Addr1;
		this.ViewState["addr2"] = addr2.Value = addr_data.Addr2;
		dspAddrDoro.Value = addr_data.DspAddrDoro;
		dspAddrJibun.Value = addr_data.DspAddrJibun;
		hfAddressType.Value = addr_data.AddressType;
        
        if(addr_data.ExistAddress) {
			if(!string.IsNullOrEmpty(addr_data.Country)) {
				ddlHouseCountry.SelectedValue = addr_data.Country;
			}
			ph_has_address.Visible = true;
        } else {
            pn_addr_domestic.Style["display"] = "block";
        }

        // sponsorID가 없으면 후원회원 아님
        if(string.IsNullOrEmpty(sess.SponsorID)) {
			pn_dat_data.Style["display"] = "none";
			return false;
		}

        // 휴대폰/이메일/집전화
        actionResult = sponsorAction.GetCommunications();
        if(!actionResult.success) {
            base.AlertWithJavascript(actionResult.message, "goBack()");
            return false;
        }
        SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)actionResult.data;
        email.Value = comm_data.Email;
        phone.Value = comm_data.Phone;
        mobile.Value = comm_data.Mobile;


        try {

		

			// 컴파스 회원정보
			Object[] objSql = new object[1] { "SELECT top 1 * FROM tSponsorMaster WHERE CurrentUse = 'Y' and sponsorId = '" + sess.SponsorID + "'" };
			dsSponsor = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

			if(dsSponsor == null || dsSponsor.Tables.Count < 1) {
				base.AlertWithJavascript("후원회원 정보를 가져오는 중에 에러가 발생했습니다.", "goBack()");
				return false;
			}

			if(dsSponsor.Tables[0].Rows.Count < 1) {
				base.AlertWithJavascript("후원회원 정보를 찾을 수 없습니다.", "goBack()");
				return false;
			}


		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			base.AlertWithJavascript("후원회원 정보를 가져오는 중에 에러가 발생했습니다.", "goBack()");
			return false;
		}

		DataTable dtSponsor = dsSponsor.Tables[0];
		addr_domestic.Checked = dtSponsor.Rows[0]["locationType"].ToString() == "국내";
		addr_oversea.Checked = dtSponsor.Rows[0]["locationType"].ToString() == "국외";
		this.ViewState["firstname"] = firstname.Value = dtSponsor.Rows[0]["firstname"].ToString();
		this.ViewState["lastname"] = lastname.Value = dtSponsor.Rows[0]["lastname"].ToString();
		translate_y.Checked = dtSponsor.Rows[0]["TranslationFlag"].ToString() != "N";
		translate_n.Checked = dtSponsor.Rows[0]["TranslationFlag"].ToString() == "N";
		
		var religionType = dsSponsor.Tables[0].Rows[0]["ReligionType"].ToString();
		if(religionType == "기독교" || religionType == "") {
			religion1.Checked = true;
			church_name.Value = dsSponsor.Tables[0].Rows[0]["ChurchName"].ToString();

			if(church_name.Value != null) {
				actionResult = sponsorAction.GetChurch();
				if(!actionResult.success) {
					base.AlertWithJavascript(actionResult.message, "goBack()");
					return false;
				}

				SponsorAction.ChurchResult church_data = (SponsorAction.ChurchResult)actionResult.data;
				hdOrganizationID.Value = church_data.organizationId;
			}

		} else if(religionType == "천주교") {
			religion2.Checked = true;
		} else if(religionType == "불교") {
			religion3.Checked = true;
		} else {
			religion4.Checked = true;
		}
		
		// 어린이 편지 번역여부
		if(dtSponsor.Rows[0]["TranslationFlag"].ToString() == "Y") {
			translate_y.Checked = true;
		} else {
			translate_n.Checked = true;
		}

		// 후원정보 수신
		actionResult = sponsorAction.GetReceiveSponsorInfo();
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message);
			return false;
		}

		if((bool)actionResult.data) {
			sponsor_info_y.Checked = true;
		} else {
			sponsor_info_n.Checked = true;
		}

		// 이메일/SMS 수신여부
		actionResult = sponsorAction.GetAgreeCommunications();
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message);
			return false;
		}

		SponsorAction.AgreeCommunicationResult agree_data = (SponsorAction.AgreeCommunicationResult)actionResult.data;
		//Response.Write(agree_data.ToJson());
		agree_receive.Checked = agree_data.Email || agree_data.SMS;


		//
		// 어린이편지 번역 노출여부 , 1:1결연정보가 있는 경우에만 노출
		actionResult = sponsorAction.HasCDSP();
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message, "goBack()");
			return false;
		}
		ph_translate.Visible = ((DataTable)actionResult.data).Rows.Count > 0;

		// 정기결제 내역이 있는 경우
		ph_has_regular_commitment.Visible = sponsorAction.HasRegularCommitment().success;
        ph_has_regular_commitment2.Visible = sponsorAction.HasRegularCommitment().success;

        return true;
	}

	// 수정
	protected void btn_submit_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

		UserInfo sess = new UserInfo();

		SponsorAction sponsorAction = new SponsorAction();
		JsonWriter actionResult;

		string p_translate = translate_y.Checked ? "Y" : "N";
		string p_firstName = firstname.Value.EscapeSqlInjection();
		string p_lastName = lastname.Value.EscapeSqlInjection();
		string p_juminId = jumin.Value;
		string p_location = addr_domestic.Checked ? "국내" : "국외";
		string p_country = addr_domestic.Checked ? "한국" : ddlHouseCountry.SelectedValue;
		string p_addressType = hfAddressType.Value;
		string p_zipcode = zipcode.Value;
		string p_addr1 = addr1.Value;
		string p_addr2 = addr2.Value;
		bool? p_agree_receive = null;
		bool? p_sponsor_info = null;

		var religion = "";
		if(religion1.Checked)
			religion = religion1.Value;
		else if(religion2.Checked)
			religion = religion2.Value;
		else if(religion3.Checked)
			religion = religion3.Value;
		else if(religion4.Checked)
			religion = religion4.Value;
		//string sResult;
		string sChurchName = "";
		string sOrganizationID = "";

		if(!religion1.Checked) {
			church_name.Value = "";
			sChurchName = "";
		} else {
			if(church_name.Value.Trim().Length > 0 && hdOrganizationID.Value.ToString().Trim().Length > 0) {
				sChurchName = church_name.Value.Trim();
				sOrganizationID = hdOrganizationID.Value.ToString().Trim();
			} else if(church_name.Value.Trim().Length > 0)
				sChurchName = church_name.Value.Trim();

		}

		if(ph_has_regular_commitment.Visible) {
			p_agree_receive = agree_receive.Checked;
			
			p_sponsor_info = sponsor_info_y.Checked;
		}


		// 어린이편지 번역여부
		if(ph_translate.Visible) {
			p_translate = translate_y.Checked ? "Y" : "N";
		}
        if(password_popup.Value == "Y") {
            if(string.IsNullOrEmpty(cur_pwd.Value)) {
                base.AlertWithJavascript("현재 비밀번호를 입력해주세요");
                return;
            }

            if(string.IsNullOrEmpty(pwd.Value)) {
                base.AlertWithJavascript("신규 비밀번호를 입력해주세요");
                return;
            }

            if(pwd.Value != re_pwd.Value) {
                base.AlertWithJavascript("신규 비밀번호와 비밀번호 확인정보가 일치하지 않습니다. 다시한번 확인해주세요");
                return;
            }
        }
		if (string.IsNullOrEmpty(mobile.Value) && string.IsNullOrEmpty(email.Value)){
			base.AlertWithJavascript("휴대폰번호와 이메일주소 둘중에 하나는 입력해주세요.");
			return;
		}

		if(!string.IsNullOrEmpty(sess.SponsorID)) {

			if (this.ViewState["firstname"].ToString() != "" && string.IsNullOrEmpty(firstname.Value)) {
				base.AlertWithJavascript("영문이름(성)을 입력해주세요");
				return;
			}

			if(this.ViewState["lastname"].ToString() != "" && string.IsNullOrEmpty(lastname.Value)) {
				base.AlertWithJavascript("영문이름(이름)을 입력해주세요");
				return;
			}

			if(this.ViewState["zipcode"].ToString() != "" && string.IsNullOrEmpty(zipcode.Value)) {
				base.AlertWithJavascript("우편번호를 입력해주세요");
				return;
			}

			if(this.ViewState["addr1"].ToString() != "" && string.IsNullOrEmpty(addr1.Value)) {
				base.AlertWithJavascript("주소를 입력해주세요");
				return;
			}

			if(this.ViewState["addr2"].ToString() != "" && string.IsNullOrEmpty(addr2.Value)) {
				base.AlertWithJavascript("상세주소를 입력해주세요");
				return;
			}

		}

        using (AuthDataContext dao = new AuthDataContext())
        {
            //var data = dao.sp_tSponsorMaster_get_f(sess.UserId, cur_pwd.Value, "", "", "", "", "").ToList();
            Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
            Object[] op2 = new Object[] { sess.UserId, this.ViewState["pwd"].ToString(), "", "", "", "", "" };
            var data = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<sp_tSponsorMaster_get_fResult>();

            if (password_popup.Value == "Y")
            {
                if (data.Count < 1)
                {
                    base.AlertWithJavascript("비밀번호가 일치하지 않습니다.");
                    return;
                }
                //dao.sp_tSponsorMaster_change_pwd_f(sess.UserId, pwd.Value);
                Object[] op3 = new Object[] { "UserID", "UserPW" };
                Object[] op4 = new Object[] { sess.UserId, pwd.Value };
                www6.selectSPAuth("sp_tSponsorMaster_change_pwd_f", op3, op4);
            }
        }
        actionResult = sponsorAction.UpdateSponsor(sess.SponsorID ,  p_firstName, p_lastName, "", p_translate, mobile.Value, email.Value, phone.Value, p_location, p_country,
		p_addressType, p_zipcode, p_addr1, p_addr2 , religion , sChurchName , sOrganizationID ,  null , null , p_agree_receive, null, p_sponsor_info);
		
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message);
			return;
		}


		base.AlertWithJavascript("성공적으로 수정되었습니다.");
		
	}
}