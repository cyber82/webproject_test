﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pwd.aspx.cs" Inherits="my_account_pwd" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/paging.ascx" TagPrefix="uc" TagName="paging" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/my/account/pwd.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" runat="server" id="redirect" value="" />
	<input type="password" runat="server" id="pwd" />
	<asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click">확인</asp:LinkButton>
	

</asp:Content>
