﻿
var selectedSampleItem = null;

$(function () {
	$("#btn_file_remove").hide();
	var uploader = attachUploader("btn_file_path");
	uploader._settings.data.fileDir = $("#upload_root").val();
	uploader._settings.data.fileType = "image";
	uploader._settings.data.rename = "y";
	uploader._settings.data.limit = 2048;

    // 삭제 문희원 2017-04-26
	//$("#lang_ko").click(function () {
	//	alert("한글 편지는 번역 과정을 거치기 때문에, \n영어로 작성하시면 더 빨리 전달됩니다");

	//})

	if ($("#file_path").val() != "") {
		$("#btn_file_remove").show();
	}

	$("#btn_file_remove").click(function () {

		$.post("/api/my/letter.ashx", { t: "file-delete", file_path: $("#file_path").val() }, function (r) {
			if (r.success) {

				$("#file_path").val("");
				$("#lb_file_path").val("");
				$("#btn_file_remove").hide();
				$(".attach_img").html("");

			} else {
				alert(r.message)
			}
		})

		return false;
	})

	//$("#letterComment").textCount($("#comment_count"), { limit: 1000 });
	$("#letterComment").byteCount($("#comment_count"), { limit: 2000 });

	$("#letterComment").keyup(function () {
		if ($("#comment_count").text() >= 2000) {
			alert("2000byte 까지 입력할 수 있습니다.");
		}
	});

});

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

		$scope.total = -1;

		$scope.data = [];
		$scope.list = [];
		$scope.params = {
			page: 1,
			rowsPerPage: 5
		};

		$scope.selectType = null;
		$scope.selectTypeName = null;
		$scope.selectContent = null;
		$scope.sample_letters = null;
		$scope.bg_class = null;
		$scope.c = getParameterByName("c");
		$scope.samplelettercode = "";

		$scope.listParams = paramService.getParameterValues();

		// 답장여부
		$scope.is_reply = getParameterByName("c") != "";

		// list
		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);

			if ($scope.data.length) {
				$scope.showList();
			} else {
				$http.get("/api/my/child.ashx?t=list", { params: { page: 1, rowsPerPage: 1000 } }).success(function (r) {
					if (r.success) {
						$scope.data = $.extend($scope.data, r.data);
						console.log($scope.data);

						var bCanLetter = false;
						$.each($scope.data, function () {
							this.checked = false;
							this.birthdate = new Date(moment(this.birthdate, "YYYY-MM-DD"));

							if (this.canletter) { bCanLetter = true; }
						});

						if ($scope.is_reply) {
							$scope.data = $.grep($scope.data, function (r) {
								return r.childmasterid == getParameterByName("c")
							});

							if ($scope.data.length < 1) {
								alert("후원중인 어린이가 아닙니다.");
								goList();
								return;
							}

							if (!$scope.data[0].paid) {
							    if (confirm("첫 후원금 납부 후 어린이에게 편지를 보내실 수 있습니다.\n납부를 진행하시겠습니까?")) {
							        $http.get("/sponsor/pay-gateway.ashx?t=go-cdsp-letter", { params: { childMasterId: $scope.data[0].childmasterid } }).success(function (r) {

							            if (r.success) {
							                //alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
							                location.href = r.data;
							            } else {
							                if (r.action == "nonpayment") {		// 미납금

							                    if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
							                        location.href = "/my/sponsor/pay-delay/";
							                    }

							                } else {
							                    alert(r.message);
							                }
							            }

							        });
							    } else {
							        return;
							    }
							}

						    //if (!$scope.data[0].canletter) {
							if (!bCanLetter) {
								alert("후원자님께서는 어린이와 편지를 주고 받지 않으시는 머니 후원 중이십니다. 감사합니다.");
								goList();
								return;
							}

							$scope.total = 1;
							$scope.data[0].checked = true;
						} else {

						    var data = $.grep($scope.data, function (r) {
						        return r.canletter;
						    });

						    var total = data.length > 0 ? data[0].total : 0;

                            // 편지 쓰기 가능 어린이 수가 0 이면 전체 선택 도 안되게 
						    if (total <= 0) {
						        $scope.readonly = true;
						    }
						    else
						        $scope.readonly = false;

						    $.each($scope.data, function () {
						        if (!this.canletter)
						            this.readonly = true;
						    });

							$scope.total = $scope.data.length > 0 ? $scope.data[0].total : 0;
						}
						$scope.showList();
					} else {
						if (r.action == "not_sponsor") {

							$scope.total = 0;
						}
					}
				});
			}

		}

		$scope.showList = function () {
			var begin = ($scope.params.page - 1) * $scope.params.rowsPerPage;
			var end = ($scope.params.page) * $scope.params.rowsPerPage;
			$scope.list = $scope.data.slice(begin, end);
		};

		// 어린이 선택
		$scope.checkChild = function (item) {
		    if (item.canletter)
		        item.checked = !item.checked;
		}

		$scope.checkChildLabel = function (item) {
		    if (!item.canletter) {
		        item.checked = false;
		        alert("이 어린이는 후원자님께서 머니후원 중이신 어린이입니다. 감사합니다.");
		    }

		    else if (!item.paid) {
		        item.checked = false;
		        if (confirm("첫 후원금 납부 후 어린이에게 편지를 보내실 수 있습니다.\n납부를 진행하시겠습니까?")) {
		            $http.get("/sponsor/pay-gateway.ashx?t=go-cdsp-letter", { params: { childMasterId: item.childmasterid } }).success(function (r) {

		                if (r.success) {
		                    //alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
		                    location.href = r.data;
		                } else {
		                    if (r.action == "nonpayment") {		// 미납금

		                        if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
		                            location.href = "/my/sponsor/pay-delay/";
		                        }

		                    } else {
		                        alert(r.message);
		                    }
		                }

		            });
		        }
		        else {
		            item.checked = false;
		        }
		    }
		    else {
		        if (item.checked)
		            item.checked = false;
		        else 
		            item.checked = true;
		    }
		}

		$scope.checkAllChild = function (sender) {

			if (sender == "chk_all2") {
				$("#chk_all").prop("checked", $("#chk_all2").prop("checked"));
			} else {
				$("#chk_all2").prop("checked", $("#chk_all").prop("checked"));
			}
			$.each($scope.data, function () {
			    if (this.canletter && this.paid)
				    this.checked =  $("#chk_all2").prop("checked");
			});
		}

		// type1 : 3자리 , type2 : 6자리
		$scope.selectSampleType = function (type1, type2, title) {
            // 언어 선택해야 예문 가져올 수 있음 추가 문희원 2017-04-07
		    if ($("input[name=lang]:checked").length < 1) {
		        alert("언어를 선택해주세요.");
		        return;
		    }

			if (type1 == "BBI") {
			//	alert("후원어린이의 성장 사진과 소식이 담긴 보고서에 \r\n대한 답장 편지일 경우 선택해주세요.");
			}

            // 기존 선택 샘플타입이랑 다르면 초기화
			if (selectedSampleItem != null && selectedSampleItem.samplelettertype != type1)
			    selectedSampleItem = null;

			$scope.bg_class = "bg_" + type1;

			$scope.selectType = type2;
			$scope.selectTypeName = title;
			// 예문데이타 로딩
			$http.get("/api/my/letter.ashx?t=sample-letter", { params: { type: type1 } }).success(function (r) {
				if (r.success) {
					console.log(r.data);

					$scope.sample_letters = r.data;
					$.each($scope.sample_letters, function () {
						this.modifydate = new Date(this.modifydate);
						this.registerdate = new Date(this.registerdate);

                        // 예문 선택 된 언어만 가져오기 위해 추가 문희원 2017-04-07
						this.lan = $("input[name=lang]:checked").val();
						this.lan_ko = $("input[name=lang]:checked").val() == "ko" ? "한글(예문)" : "English(예문)";

						//this.modifydate = new Date();
						this.new = moment(new Date()).diff(this.registerdate, "days") < 30;
						this.samplelettercodename = this.samplelettercodename.replace("[", '<span class="fc_blue">[').replace("]", ']</span>');
						//
					})

					$scope.SetSampleLan();

				} else {
					alert(r.message);
				}
			});

		}

		$scope.SetSampleLan = function () {

		    if (selectedSampleItem != null) {
		        var lan = $("input[name=lang]:checked").val();
		        $scope.samplelettercode = selectedSampleItem.samplelettercode;

		        if (lan == "ko") {
		            $scope.selectContent = selectedSampleItem.contextkor;
		        } else {
		            $scope.selectContent = selectedSampleItem.contexteng;
		        }
		    }
		    else
		        $scope.selectContent = "";
		}

        // 편지언어클릭
		$scope.languageClick = function () {
		    if ($("input[name=letterType]:checked").length == 0) {
		        return;
		    }
		    else {
		        var id = $("input[name=letterType]:checked")[0].id;

		        setTimeout(function () {
		            angular.element('#' + id).trigger('click');
		            $scope.clicked = true;
		        }, 0);
		    }
		}

		$scope.selectSample = function ($event, item, lang) {
		    $event.preventDefault();

		    selectedSampleItem = item;

			$scope.samplelettercode = item.samplelettercode;

			if (lang == "ko") {
				$scope.selectContent = item.contextkor;
			} else {
				$scope.selectContent = item.contexteng;
			}

		}

		$scope.goList = function ($event) {
			$event.preventDefault();
			location.href = "/my/letter/?" + $.param($scope.listParams);
		},

		$scope.submit = function ($event, status) {
			$event.preventDefault();
			var children = [];
			//var checkChildKey = true;
			if ($scope.data) {
				$.each($scope.data, function () {
				    if (this.checked) {
				        //if (typeof this.childkey == "undefined" || this.childkey == "undefined" || this.childkey == null || this.childkey == "") {
				        //    checkChildKey = false;
				        //}
						children.push({ childMasterId: this.childmasterid, childKey: this.childkey });
					}
				});
			}

			//if (!checkChildKey) {
			//    alert("어린이키에 문제가 있습니다. 관리자에게 문의해주세요.");
			//    return false;
			//}

			if (children.length < 1) {
				alert("편지를 보내실 어린이를 선택해주세요");
				return false;
			}

			if ($("input[name=lang]:checked").length < 1) {
				alert("편지에 사용하실 언어를 선택해 주세요.");
				return false;
			}

			if ($("input[name=letterType]:checked").length < 1) {
				alert("편지종류를 선택해 주세요.");
				return false;
			}

			if ($("#letterComment").val().trim().length < 1) {
				alert("편지내용을 입력해 주세요.");
				return false;
			}


			var lang = $("input[name=lang]:checked").val();
			var comment = $("#letterComment").val();
            var isTempChange = 0;
            
		    // 한글 선택 예문이 변경되지 않았다면 영문으로 설정할 수 있도록
            if (selectedSampleItem != undefined && selectedSampleItem != null && selectedSampleItem.contextkor == $("#letterComment").val() && $("input[name=lang]:checked").val() == "ko" && status != "T") {
                isTempChange = 1;
            }
            else {
                if (status == "N" && !confirm("[언어 : " + $("input[name=lang]:checked").data("text") + " / 편지타입 : " + $scope.selectTypeName + "] 로 발송 하시겠습니까?"))
                    return false;
            }

			var param = {
				t: "add",
				samplelettercode: $scope.samplelettercode,
				file_path: $("#file_path").val(),
				file_size: $("#file_size").val(),
				lang: lang,
				status: status,
				letter_comment: comment,
				letter_type: $scope.selectType,
				children: $.toJSON(children),
			    isTempChange: isTempChange,

			};

			console.log(param);
			//	return;

			$http.post("/api/my/letter.ashx", param).success(function (r) {
				if (r.success) {

					if (status == "T") {
						alert("임시 저장이 완료되었습니다.");
						location.href = "/my/letter/?type=send_temp"
						return;
					}

					alert("편지가 등록되었습니다. 등록된 편지는 당일 24시 전까지 수정/취소가 가능합니다.");
					location.href = "/my/letter/?type=send"
				} else {
					alert(r.message);
				}
			});

		}

		$scope.getList();

		// 영상 팝업
		$scope.modalVideo = {
			instance: null,
			init: function () {
				// 팝업

				popup.init($scope, "/my/letter/letter-video", function (modalVideo) {
					$scope.modalVideo.instance = modalVideo;
				});
			},

			show: function () {

				$scope.modalVideo.instance.show();


			},

			close: function ($event) {
				$event.preventDefault();
				if (!$scope.modalVideo.instance)
					return;
				// 영상재상 초기화
				$("#letter_movie").attr("src", $("#letter_movie").attr("src"));
				$scope.modalVideo.instance.hide();

			},
		}
		$scope.modalVideo.init();

	});

})();

var attachUploader = function (button) {

	return new AjaxUpload(button, {
		action: '/common/handler/upload',
		responseType: 'json',
		onChange: function () {
		},
		onSubmit: function (file, ext) {
			this.disable();
		},
		onComplete: function (file, response) {

			this.enable();

			if (response.success) {

				$("#btn_file_remove").show();
				alert("첨부 파일에 내용이 있는 경우 번역이 되지 않습니다");

				$("#file_path").val(response.name);
				$("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
				$("#file_size").val(response.size);

				$(".attach_img").html($("<img src='" + ($("#domain_image").val() + response.name) + "' alt='이미지'/>"));

			} else
				alert(response.msg);
		}
	});
}