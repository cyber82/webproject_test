﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="my_letter_view" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/my/letter/view.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="upload_root" value="" />
	<input type="hidden" runat="server" id="corrId" value="" />

<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

		<!-- 타이틀 -->
		<div class="page_tit" ng-if="AdminCK == false" >
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980 letter">

				 <uc:menu runat="server"  />

				<!-- contents -->
				<div class="box_type4 mb40">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit mb30">
						<p class="tit">편지함</p>
					</div>
					<!--// -->

					<!-- 편지상세 -->
					<div class="tableWrap1 mb30">
						<table class="tbl_child reserve">
							<caption>편지함 테이블</caption>
							<colgroup>
								<col style="width:2%" />
								<col style="width:12%" />
								<col style="width:60%" />
								<col style="width:26%" />
							</colgroup>
						
							<tbody>
								<tr>
									<td></td>
									<td class="tac">
										<span class="relative">
											<span class="pic" style="background:url('{{data.basedata[0].pic}}') no-repeat center top;background-size:cover;">어린이사진</span>
											<span class="status" ng-class="{'send' : data.basedata[0].direction == 'send' , 'receive' : data.basedata[0].direction == 'receive'}"></span>
										</span>
									</td>
									<td class="detail">
										<p class="s_tit5">{{data.basedata[0].namekr}}<span class="ic_new" ng-if="data.basedata[0].checkstatusyn == 'N'"></span></p>
										<span class="field">어린이ID</span><span class="data">{{data.basedata[0].childkey}}</span><span class="field fd2">국가</span><span class="data">{{data.basedata[0].countryname}}</span>
									</td>
									<td class="tar">
										<span class="txt_status mr10"><span ng-if="data.basedata[0].direction == 'send'">{{data.basedata[0].state}}</span>
                                        <span ng-if="data.basedata[0].is_smart == 'True'">(스마트레터)</span></span><!--span class="s_con6 vam">{{//data.basedata[0].display_date | date:'yyyy.MM.dd'}}</span--><br />
										<div class="mb10"></div>
										<a href="#" class="btn_s_type3 ml10" ng-repeat="item in images" ng-click="download($event,item)">다운로드{{$index+1}}</a>
									</td>
								</tr>
								<tr ng-if="images.length > 0">
									<td colspan="4">
										<%--<p class="tar mb20">--%>
                                            <div  class="tar mb20" style="margin-bottom:40px;">
                                                <div style="float: left; color: #005dab; font-size: 14pt; font-weight:bold; margin-top: -5px;">한글 번역을 신청하신 경우 편지 이미지 하단에서 확인하실 수 있습니다.</div>
											    <div style="float:right;">
                                                    <button class="letter_prev"  ng-click="prev($event)" style="width: 25px; height: 25px; background-size: cover;">이전</button>
											        <span class="fc_blue" style="font-size:12pt;">{{cur_page}}</span><span style="font-size:12pt;">&nbsp;/ {{tot_page}}</span>
											        <button class="letter_next" ng-click="next($event)" style="width: 25px; height: 25px; background-size: cover;">다음</button>
											    </div>
                                            </div>
                                            
                                            
										<%--</p>--%>

										<div class="letter_detail" style="overflow-x:hidden">
											<img ng-src="{{item}}" alt="편지이미지" ng-repeat="item in images" ng-show="$index + 1 == cur_page" style="max-width:910px" />
										</div>
                                        
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div style="padding-bottom:20px" ng-bind-html="data.sendcontent"></div>
                    <!-- 20170406 번역여부와 관계없이 스크리닝 텍스트가 있으면 표시-->
					<!--div ng-if="data.translatecontent && data.detaildata[0].translationneed == 'Y'"></div -->
                    <div ng-if="data.translatecontent"></div>
					<!--div style="padding-bottom:20px" ng-if="data.translatecontent && data.detaildata[0].translationneed == 'Y'" ng-bind-html="data.translatecontent"></div> -->
                    <div style="padding-bottom:20px" ng-if="data.translatecontent" ng-bind-html="data.translatecontent"></div>
                    <p ng-if="data.translationname != '' && data.translationstatus == 'END'" style="text-align:right; margin:30px;">이 편지는 Compassion Mate <span ng-bind="data.translationname"></span>님께서 번역해 주셨습니다.</p>

					<div style="padding-bottom:20px" ng-if="data.attachment">
						<img ng-src="{{data.attachment}}" style="max-width:910px" />
					</div>

					<div class="tac relative mb80">
						<a ng-href="/my/letter/write/?c={{data.basedata[0].childmasterid}}&type=receive" ng-if="data.basedata[0].direction == 'receive'" ng-show="AdminCK == false" class="btn_type1">어린이에게 답장하기</a>
						

						<a href="#" ng-click="goList($event)" class="btn_type4 posR">목록</a>
					</div>
					<!--// 편지상세 -->

				</div>
				<!--// contents -->


			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>

</asp:Content>

