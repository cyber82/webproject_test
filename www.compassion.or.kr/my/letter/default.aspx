﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_letter_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/letter/default.js?v=1.8"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
<style>
.tac > .relative > .pic { background-size:88px !important; }
.tac >.relative >.pic.big { 
    position: absolute !important;
    z-index: 1;
    border-radius: 10px !important;
    background-Size: 200px 300px !important; 
    border: 1px solid #888888;
    box-shadow:5px 5px 5px rgba(100,100,100,0.8);
    text-indent:initial !important;
    margin-left:10px;
}
</style>	
<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

		<!-- 타이틀 -->
		<div class="page_tit" ng-if="AdminCK == false">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980 letter">

				 <uc:menu runat="server"  />

				<!-- contents -->
				<div class="box_type4 mb40">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit mb30">
						<p class="tit">편지함</p>
					</div>
					<!--// -->

					<!-- 국가선택 -->

					<div class="table_sel clear2" ng-if="params.type != 'send_temp' && total > 0">
						<div class="fr">
							<span class="sel_type1 fl mr10" style="width:150px">
								<label for="country" class="hidden">국가 선택</label>
								<select class="custom_sel" id="country" data-refresh="true" ng-model="params.country" ng-change="changeCountry()"  data-ng-options="item.countryid as item.countryname for item in countries">
								</select>
							</span>
							<span class="sel_type1 fl mr5" style="width:280px">
								<label for="child" class="hidden">어린이 선택</label>
								<select class="custom_sel" id="child" data-refresh="true" ng-model="params.childMasterId" ng-change="changeChild()"  data-ng-options="item.childmasterid as item.namekr for item in children">
								</select>
							</span>
							<!--<a href="#" class="btn_type9">검색</a>-->
						</div>
					</div>
					<!--// 국가선택 -->

					<!-- 편지리스트 -->
					<div class="tableWrap1 mb30">
						<table class="tbl_child letter">
							<caption>편지함 테이블</caption>
							<colgroup>
								<col style="width:2%" />
								<col style="width:12%" />
								<col style="width:60%" />
								<col style="width:26%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col" colspan="4">
										<div class="topLink1">
											<a href="#" ng-class="params.type == 'all' ? 'on' : ''" ng-click="setType($event,'all')">전체편지({{total_all}})</a>
											<a href="#" ng-class="params.type == 'receive' ? 'on' : ''" ng-click="setType($event,'receive')">받은편지({{total_receive}})</a>
											<a href="#" ng-class="params.type == 'send' ? 'on' : ''" ng-click="setType($event,'send')">보낸편지({{total_send + total_ready}})</a>
                                            <a href="#" ng-class="params.type == 'send_temp' ? 'on' : ''" ng-click="setType($event,'send_temp')" ng-if="AdminCK == false">임시저장({{total_temp}})</a>
										</div>

										<div class="topLink2" ng-if="AdminCK == false">
											<a href="/customer/faq/?s_type=E" class="faq">편지 FAQ</a>
											<a href="#" ng-click="modalVideo.show($event)" class="what">편지 영상 보기</a>
										</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in list">
									<td></td>
									<td class="tac">
										<span class="relative">

											<%--<span class="pic" background-img="{{item.pic}}" data-default-image="/common/img/page/my/no_pic.png" style="background-position:center 0px;background:no-repeat;background-size:88px;" onclick="fnPicClick(this, {{$index}})">어린이사진</span>--%>
                                            <span class="pic" background-img="{{item.pic}}" data-default-image="/common/img/page/my/no_pic.png" style="background-position:center 0px;background:no-repeat;background-size:88px !important; cursor:pointer;" ng-click="childPicClick($event, item)">어린이사진</span>

											<span class="status" ng-class="{'send' : item.direction == 'send' , 'receive' : item.direction == 'receive' , 'temp' : item.direction == 'send_temp' , 'standby' : item.direction == 'send_ready'}"></span>
										</span>
									</td>
									<td class="detail">
										<p class="s_tit5">{{item.namekr}}<span class="ic_new" ng-if="item.checkstatusyn == 'N'"></span></p>
										<span class="field">어린이ID</span><span class="data">{{item.childkey}}</span><span class="field fd2">국가</span><span class="data">{{item.countryname}}</span><span class="data">{{item.CorrID}}</span>
									</td>
									<td class="tar">
										<p class="s_con6 mb10">{{item.display_date | date:'yyyy.MM.dd'}}</p>
											
										<span ng-if="item.direction == 'send' && item.is_smart == 'True'" class="txt_status">스마트레터</span><span class="bar"  ng-if="item.direction == 'send' && item.is_smart == 'True'" ></span>
										<span ng-if="item.direction == 'send' && item.is_smart == 'False'" class="txt_status"></span><span class="bar"  ng-if="item.direction == 'send' && item.is_smart == 'False'" ></span>
										<span ng-if="item.direction == 'send_temp'" class="txt_status">임시저장</span><span class="bar"  ng-if="item.direction == 'send_temp'" ></span>

                                        <%-- 2018-05-03 [이종진] 선물편지의 경우. 유료(SPNPRE)는 수정/삭제불가능. 무료(SPNPPR)는 삭제만가능. 당일에만 수정/삭제한점은 동일 --%>
										<!-- 받은/보낸 편지 -->
										<a href="#" ng-click="goReply($event , item)" class="btn_reply" ng-if="item.direction == 'receive'" ng-show="AdminCK == false">답장쓰기</a>
                                        <span class="bar" ng-if="item.direction == 'receive'" ng-show="AdminCK == false"></span>
										<a href="#" ng-click="goView($event , item)" class="btn_letter" ng-if="item.direction == 'receive' || item.direction == 'send'">편지보기</a>

										<!-- 등록대기 편지 -->
                                        <span ng-if="item.direction == 'send_ready'" class="txt_status">
                                            <a href="#" ng-click="goUpdate($event,item)" ng-show="AdminCK == false">등록대기중 </a>
                                            <a class="btn_cancel" href="#" ng-if="item.is_editable == 'True' && item.correspondencetype != 'SPNPRE'" ng-click="cancel($event,item.correspondenceid)" ng-show="AdminCK == false">[편지삭제]</a>
										</span>
										<span class="bar" ng-if="item.direction == 'send_ready' && item.is_editable == 'True'"></span>
                                        <a href="#" class="btn_letter" ng-click="goUpdate($event,item)" ng-if="item.direction == 'send_ready' && item.is_editable == 'True'" ng-show="AdminCK == false">편지수정</a>

                                        <!-- 임시저장 -->
                                        <a href="#" ng-click="goUpdate($event,item)" ng-if="item.direction == 'send_temp'" class="btn_modify" ng-show="AdminCK == false">수정</a>
                                        <span class="bar" ng-if="item.direction == 'send_temp'" ng-show="AdminCK == false"></span>
                                        <a href="#" ng-click="cancel($event,item.correspondenceid)" class="btn_del" ng-if="item.direction == 'send_temp'" ng-show="AdminCK == false">삭제</a>
									</td>
								</tr>
								
								<tr ng-if="total == 0">

									<td colspan="4" class="no_content">
										<p>편지함이 비어 있습니다.<br />어린이에게 후원자님의 따뜻한 마음을 전해 보세요!</p>
                                        <div class="btn_ac" ng-if ="AdminCK == false"><a href="/my/letter/write/" data-urls="/my/letter/write-pic/" class="btn_type1">편지쓰기</a></div>
									</td>



								</tr>

							</tbody>
						</table>
                        
                        
					</div>
					<!--// 편지리스트 -->

					<div class="tac"><button class="btn_s_type3" ng-show="params.page * params.rowsPerPage < total" ng-click="more($event);"><span>더보기<span class="btn_arr1"></span></span></button></div>

				</div>
				<!--// contents -->


				<!-- 편지상태 -->
				<p class="s_tit3 mb20">편지 상태가 궁금해요</p>
				<div class="box_type4 letter_status clear2">

					<div class="receive">
						<p>받은 편지</p>
						<span>어린이로부터 받은 편지로,<br />편지를 읽고 답장을<br />작성하실 수 있습니다.</span>
					</div>

					<div class="send">
						<p>보낸 편지</p>
						<span>어린이에게 보낸 편지로<br />편지 작성은 직접 작성하거나<br />스마트레터를 통해 주기적으로<br />자동 발송하실 수 있습니다.</span>
					</div>

					<div class="temp">
						<p>임시저장</p>
						<span>임시저장한 편지는<br />수정/삭제하실 수 있습니다.</span>
					</div>

					<div class="standby">
						<p>등록대기</p>
						<span>발송한 편지는 당일 24시<br />이전에 수정하실 수 있습니다.</span>
					</div>

				</div>
				<!--// 편지상태 -->

			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>

</asp:Content>

