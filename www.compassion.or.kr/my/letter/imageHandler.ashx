﻿<%@ WebHandler Language="C#" Class="imageHandler" %>

using System;
using System.Web;

public class imageHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        context.Response.Write("Hello World");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}