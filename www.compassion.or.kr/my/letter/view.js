﻿$(function () {
	
});

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

		$scope.cur_page = 1;
		$scope.tot_page = 1;
		$scope.data = null;

		$scope.listParams = paramService.getParameterValues();

        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function () {

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();

		// 레터내역
		$scope.getData = function () {
			
			$http.get("/api/my/letter.ashx?t=detail", { params: { corrId: $("#corrId").val(), attach_path: $("#upload_root").val() } }).success(function (r) {
				
				if (r.success) {
					
					$scope.data = $.extend([], r.data);
					console.log("data" , $scope.data);
		
					if ($scope.data.basedata.length > 0) {
					    $scope.data.basedata[0].display_date = new Date($scope.data.basedata[0].displaydate);
						var childmasterid = $scope.data.basedata[0].childmasterid;
						var correspondenceid = $scope.data.basedata[0].correspondenceid;

						// 읽음처리
						$http.post("/api/my/letter.ashx?t=set-read", { childmasterid: childmasterid, correspondenceid: correspondenceid });
						
					}

					$scope.tot_page = $scope.data.images.length;
					if ($scope.tot_page > 1) {

					}
					$scope.images = $scope.data.images;

				} else {
					alert(r.message);
				}
			});

		}

		$scope.download = function ($event,url) {
			$event.preventDefault();
			location.href = "/api/my/letter.ashx?t=file-download&url=" + encodeURIComponent(url);
			return false;
		}

		$scope.prev = function ($event) {
			$event.preventDefault();
			var val = $scope.cur_page - 1;
			if (val < 1) {
				return;
			}

			$scope.cur_page--;
		}

		$scope.next = function ($event) {
			$event.preventDefault();
			var val = $scope.cur_page + 1;
			if (val > $scope.tot_page) {
				return;
			}

			$scope.cur_page++;
		}

		$scope.goList = function ($event) {
			$event.preventDefault();
			location.href = "/my/letter/?" + $.param($scope.listParams);
		};

		$scope.getData();

	});

	

})();