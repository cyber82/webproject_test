﻿$(function () {


});

(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

        $scope.requesting = false;
        $scope.total = -1;
        $scope.list = [];
        $scope.countries = null;
        $scope.children = null;
        $scope.total_ready = 0;
        $scope.total_receive = 0;
        $scope.total_send = 0;
        $scope.total_temp = 0;
        $scope.total_all = 0;

        var rowsPerPage = 5;
        $scope.params = {
            page: 1,
            rowsPerPage: rowsPerPage,
            type: "all",
            country: "",
            childMasterId: ""
        };

        // 뷰에서 돌아올때 넘겨준 파람을 다시 넘겨받는다.
        $scope.params = $.extend($scope.params, paramService.getParameterValues());

        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function () {

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();
        // 레터내역
        $scope.getList = function (params, cb) {

            $scope.params = $.extend($scope.params, params);
            console.log($scope.params);
            $http.get("/api/my/letter.ashx?t=list", { params: $scope.params }).success(function (r) {

                if (r.success) {
                    setTimeout(function () {
                        $(".custom_sel").selectbox("detach");
                        $(".custom_sel").selectbox();
                    }, 500)
                    var list = r.data.list;
                    console.log(r.data);
                    // 전체편지 1페이지인경우 타입별 갯수 가져온다.
                    // 전체편지에서 임시저장데이타를 제외할경우 임시저장 데이타는 갯수조회가 안된다. 
                    if ($scope.params.page == 1 && list.length > 0) {
                        $scope.total_ready = parseInt(r.data.total_ready);
                        $scope.total_receive = parseInt(r.data.total_receive);
                        $scope.total_send = parseInt(r.data.total_send);
                        $scope.total_temp = parseInt(r.data.total_temp);

                        $scope.total_all = $scope.total_ready + $scope.total_receive + $scope.total_send;
                    } else {
                        $scope.total_temp = parseInt(r.data.total_temp);
                    }

                    $.each(list, function () {

                        this.display_date = new Date(this.display_date.substring(0, 10).replace(/-/g, '/'));
                    });

                    $scope.list = $.merge($scope.list, list);

                    $scope.total = list.length > 0 ? list[0].total : 0;

                    /*
					// 임시저장에서 더보기를 없애기 위해서 1로 설정
					if ($scope.params.type == "send_temp" && $scope.list.length > 0)
						$scope.total = 1;
					*/

                    if (cb)
                        cb();
                } else {

                    $scope.total = 0;
                    if (r.action == "not_sponsor") {


                    }
                }
                /*
				console.log("total",$scope.total)
				console.log("total all", $scope.total_all)
				console.log("total ready", $scope.total_ready)
				console.log("total receive", $scope.total_receive)
				console.log("total send", $scope.total_send)
				*/
            });

        }

        $scope.more = function ($event) {
            $event.preventDefault();
            $scope.getList({ page: $scope.params.page + 1 });

        };

        // 어린이의 국가
        $scope.getCountries = function () {
            $scope.countries = [];
            $scope.countries.splice(0, 0, { countryid: "", countryname: "조회중" })

            $http.get("/api/my/letter.ashx?t=countries", { params: { type: $scope.params.type } }).success(function (r) {

                if (r.success) {
                    var list = r.data;
                    list.splice(0, 0, { countryid: "", countryname: "국가선택" })
                    $scope.countries = list;

                    setTimeout(function () {
                        $("#country").selectbox({});
                        $("#child").selectbox({});
                    }, 300)

                } else {
                    $scope.countries = null;
                }
            });

        }

        // 선택된 국가의 어린이
        $scope.getChildren = function () {

            $scope.children = [];


            $scope.children.splice(0, 0, { childmasterid: "", namekr: "조회중" })
            $http.get("/api/my/letter.ashx?t=children", { params: { type: $scope.params.type, country: $scope.params.country } }).success(function (r) {

                if (r.success) {
                    var list = r.data;
                    list.splice(0, 0, { childmasterid: "", namekr: "어린이선택" })
                    $scope.children = list;

                    console.log("testsetse ", r.data);

                    setTimeout(function () {
                        $("#child").selectbox({});
                    }, 300)

                } else {
                    //alert(r.message);
                }
            });

        }

        $scope.setType = function ($event, type) {
            if ($event) $event.preventDefault();

            //	$(".topLink1 a").removeClass("on")
            //	angular.element($event.currentTarget).addClass("on");


            $scope.params.type = type;
            $scope.params.country = "";
            $scope.params.childMasterId = "";
            $scope.list = [];

            $scope.getChildren();

            setTimeout(function () {
                $("#country").selectbox({});
            }, 300)

            $scope.getList({ page: 1 });
        }


        $scope.cancel = function ($event, correspondenceId) {
            $event.preventDefault();
            if (!confirm("삭제하시겠습니까?")) return;

            $http.post("/api/my/letter.ashx?t=cancel", { c: correspondenceId }).success(function (r) {

                alert(r.message);

                if (r.success) {
                    $scope.list = [];
                    $scope.getList({ page: 1 });

                }
            });

            return false;
        }

        $scope.changeCountry = function () {

            var reload = $scope.params.childMasterId != "";

            $scope.params.childMasterId = "";

            $scope.getChildren();

            if (reload)
                $scope.getList({ page: 1 });

        }

        $scope.changeChild = function () {
            $scope.list = [];
            $scope.getList({ page: 1 });

        }

        $scope.goUpdate = function ($event, item) {

            //correspondencetype

            $event.preventDefault();
            if (item.is_pic_letter == "Y") {
                if (item.correspondencetype == 'SPNPRE' || item.correspondencetype == 'SPNLTR' || item.correspondencetype == 'SPNPPR') {
                    //location.href = "/my/letter/update-gift/" + item.correspondenceid + "?" + $.param($scope.params);
                    //2018 - 05 - 03[이종진] 선물편지의 경우.유료(SPNPRE)는 수정 / 삭제불가능.무료(SPNPPR)는 삭제만가능.당일에만 수정 / 삭제한점은 동일
                    alert("선물편지는 수정할 수 없습니다.");
                    return false;
                }
                else {
                    location.href = "/my/letter/update-pic/" + item.correspondenceid + "?" + $.param($scope.params);
                }
            }
            else {
                location.href = "/my/letter/update/" + item.correspondenceid + "?" + $.param($scope.params);
            }

            return false;
        }

        $scope.goReply = function ($event, item) {
            $event.preventDefault();
            location.href = "/my/letter/write/?c=" + item.childmasterid + "&" + $.param($scope.params);
        }

        $scope.goView = function ($event, item) {
            $event.preventDefault();

            location.href = "/my/letter/view/" + item.corrid + "?" + $.param($scope.params);

        }

        $scope.getCountries();

        $scope.getChildren();

        // 뷰에서 돌아왔을때 처리
        if ($scope.params.page > 1) {
            var page = $scope.params.page;
            $scope.params.rowsPerPage = rowsPerPage * page;
            $scope.params.page = 1;
            $scope.getList({}, function () {
                $scope.params.rowsPerPage = rowsPerPage;
                $scope.params.page = parseInt(page);
            });

        } else {
            $scope.getList({ page: 1 });
        }

        // 영상 팝업
        $scope.modalVideo = {
            instance: null,
            init: function () {
                // 팝업

                popup.init($scope, "/my/letter/letter-video", function (modalVideo) {
                    $scope.modalVideo.instance = modalVideo;
                });
            },

            show: function ($event) {
                $event.preventDefault();

                $scope.modalVideo.instance.show();


            },

            close: function ($event) {
                $event.preventDefault();
                if (!$scope.modalVideo.instance)
                    return;
                // 영상재상 초기화
                $("#letter_movie").attr("src", $("#letter_movie").attr("src"));
                $scope.modalVideo.instance.hide();

            },
        }
        $scope.modalVideo.init();

        // 어린이사진 크게보기
        $scope.childPicClick = function ($event, item) {
            $event.preventDefault();

            //console.log(idx);
            var bView = false;
            if ($('.tbl_child.letter .pic.big').length == 0) {
                bView = true;
            }
            else {
                var prevIdx = $('.tbl_child.letter .pic.big').attr('idx');
                $('.tbl_child.letter .pic.big').remove();
                if (prevIdx != item.idx) {
                    bView = true;
                }
            }

            if (bView) {
                var obj = $event.currentTarget;
                var bigImg = $(obj).clone();
                $(bigImg).attr('idx', item.idx);
                $(bigImg).addClass('big');
                $(bigImg).click(function () { $('.tbl_child.letter .pic.big').remove(); });
                $(bigImg).html('<div style="float:right; background-color:rgba(255,255,255,0.5); color:black; cursor:pointer; margin:5px; border-radius:3px !important; text-indent:12px;">X 닫기</div>');
                $(obj).after(bigImg);
                $(bigImg).animate({
                    position: 'absolute',
                    width: '200px',
                    height: '300px',
                    backgroundSize: '200px 300px'
                }, 300, function () {
                    // Animation complete.
                });
            }

        }

    });

})();