﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;

public partial class my_letter_view : FrontBasePage {


	public override bool RequireLogin {
		get {
			return true;
		}
	}
    
    WWWService.Service _wwwService = new WWWService.Service();
	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		var requests = Request.GetFriendlyUrlSegments();
		if(requests.Count < 1) {
			Response.Redirect("/my/letter/", true);
		}
		
		upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_letter);
		corrId.Value = requests[0];      // CorrID
		
	}
	
}