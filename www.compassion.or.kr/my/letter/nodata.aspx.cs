﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using System.IO;


public partial class my_letter_nodata : FrontBasePage {
	
	public override bool RequireLogin {
		get {
			return true;
		}
	}
    
    protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		userName.Text = new UserInfo().UserName;

		var actionResult = new ChildAction().GetChildren(null, null, null, null, null, 18, null, null, 1, 10, "waiting", -1, -1);
		if(actionResult.success) {

			var list = (List<ChildAction.ChildItem>)actionResult.data;
			Random rnd = new Random();
			var index = rnd.Next(list.Count);
			//Response.Write(list.ToJson());

			var item = list[index];
			data.Value = item.ToLowerCaseJson();

		}
	}

}