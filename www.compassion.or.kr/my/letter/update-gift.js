﻿$(function () {



    //$("#btn_file_remove").hide();
    //var uploader = attachUploader("btn_file_path");
    //uploader._settings.data.fileDir = $("#upload_root").val();
    //uploader._settings.data.fileType = "image";
    //uploader._settings.data.rename = "y";
    //uploader._settings.data.limit = 2048;		// 2MB

    //$("#lang_ko").click(function () {
    //    alert("한글으로는 번역 과정을 거치기 때문에,\n\r영어로 사용하시면 편지가 더 빨리 전달됩니다.");
    //})

    //if ($("#file_path").val() != "") {
    //    $("#btn_file_remove").show();
    //    $(".attach_img").html($("<img src='" + ($("#domain_image").val() + $("#file_path").val()) + "' alt='이미지'/>"));
    //}

    //$("#btn_file_remove").click(function () {

    //    $.post("/api/my/letter.ashx", { t: "file-delete", file_path: $("#file_path").val() }, function (r) {
    //        if (r.success) {

    //            $("#file_path").val("");
    //            $("#lb_file_path").val("");
    //            $("#btn_file_remove").hide();
    //            $(".attach_img").html("");

    //        } else {
    //            alert(r.message)
    //        }
    //    })

    //    return false;
    //})

    //$("#letterComment").textCount($("#comment_count"), { limit: 50 });

    //$("#letterComment").keyup(function () {
    //    if ($("#comment_count").text() >= 50) {
    //        alert("50자 까지 입력할 수 있습니다.");
    //    }
    //});
});

(function () {

    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

        $scope.requesting = false;
        $scope.total = -1;


        $scope.data = [];
        $scope.list = [];
        $scope.params = {
            page: 1,
            rowsPerPage: 5
        };

        $scope.selectChilden = [];
        $scope.repCorrespondenceID = "";

        $scope.listParams = paramService.getParameterValues();

        // list
        $scope.getList = function (params) {
            $scope.params = $.extend($scope.params, params);

            if ($scope.data.length) {

                $scope.showList();

            } else {

                $http.get("/api/my/child.ashx?t=list", { params: { letter: 1, page: 1, rowsPerPage: 1000 } }).success(function (r) {
                    if (r.success) {

                        $scope.data = $.extend($scope.data, r.data);
                        console.log($scope.data);

                        $scope.data = $.grep($scope.data, function (r) {

                            // 선택된 어린이면 
                            if ($.grep($scope.selectChilden, function (r2) {
								return r2.childMasterId == r.childmasterid;
                            }).length > 0) {
                                return true;
                            }

                            return false;

                        });
                        $scope.total = $scope.data.length;

                        $scope.showList();
                    } else {
                        alert(r.message);
                    }
                });

            }

        }

        $scope.showList = function () {
            var begin = ($scope.params.page - 1) * $scope.params.rowsPerPage;
            var end = ($scope.params.page) * $scope.params.rowsPerPage;
            $scope.list = $scope.data.slice(begin, end);
        };

        $scope.goList = function ($event) {
            if ($event) $event.preventDefault();
            location.href = "/my/letter/?" + $.param($scope.listParams);
        };

        $scope.submit = function ($event, status) {
            $event.preventDefault();
            if ($scope.requesting) return false;

            var children = $scope.selectChilden;

            //var checkChildKey = true;
            //if (children) {
            //    $.each(children, function () {
            //        if (typeof this.childKey == "undefined" || this.childKey == "undefined" || this.childKey == null || this.childKey == "") {
            //            checkChildKey = false;
            //        }
            //    });
            //}

            //if (!checkChildKey) {
            //    alert("어린이키에 문제가 있습니다. 관리자에게 문의해주세요.");
            //    return false;
            //}

            if (children.length < 1) {
                alert("편지를 보내실 어린이를 선택해주세요");
                return false;
            }

            //if ($("input[name=lang]:checked").length < 1) {
            //    alert("편지에 사용하실 언어를 선택해 주세요.");
            //    return false;
            //}

            //if ($("#lb_file_path").val() == "") {
            //    alert("사진을 선택해주세요.");
            //    return false;
            //}

            //if (!confirm("[언어 : " + $("input[name=lang]:checked").data("text") + "] 로 등록 하시겠습니까?"))
            //    return false;

            if ($("input[name=giftSeq]:checked").length < 1) {
                alert("보내실 선물을 선택해주세요");
                return false;
            }

            var giftSeq = $("input[name=giftSeq]:checked").val();
            var giftIsStoreItem = $("input[name=giftSeq]:checked").attr('data-value');// == 'Y' ? true : false;
            var giftName = $("input[name=giftSeq]:checked").attr('data-text');

            if (giftIsStoreItem == 'Y') {
                $scope.modalChild.reInit();
            }
            //if (giftIsStoreItem == 'Y') {
            //    var isCORRES = false;
            //    $.each(children, function () {
            //        if (this.checked && this.sponsortypeeng == 'CORRES') {
            //            isCORRES = true;
            //            this.checked = false;
            //        }
            //    });

            //    if (isCORRES) {
            //        alert('유료선물의 경우는 편지결연 어린이는 선택하실 수 없습니다.');
            //        return false;
            //    }
            //}

            var saveData = $.parseJSON($("#save_data").val());
            if (giftSeq == saveData.giftSeq) {
                if ($scope.status == "N") {
                    location.href = "/my/letter/?type=send"
                } else {
                    location.href = "/my/letter/?type=send_temp"
                }
            }
            else {
                $('#newFile').val('Y');
                if (giftIsStoreItem == 'Y') {
                    $scope.modalChild.show($event);
                }
                else {
                    $.post("/common/handler/uploadImage.ashx", {
                        giftSeq: giftSeq,
                        path: $('#upload_root').val()
                    }, function (res) {
                        if (res.success) {
                            var param = {
                                t: "update",
                                repCorrespondenceID: $scope.repCorrespondenceID,
                                samplelettercode: $scope.samplelettercode,
                                file_path: res.name,
                                file_size: res.size,
                                lang: $("input[name=lang]:checked").val(),
                                status: status,
                                letter_comment: giftIsStoreItem == 'Y' ? giftName : '',
                                letter_type: giftIsStoreItem == 'Y' ? 'SPNPRE' : 'SPNLTR',
                                children: $.toJSON(children),
                                is_pic_letter: "Y",
                                giftSeq: giftSeq,
                                giftIsStoreItem: giftIsStoreItem
                            };

                            $http.post("/api/my/letter.ashx", param).success(function (r) {
                                $scope.requesting = false;
                                if (r.success) {
                                    console.log(r.data);
                                    if ($scope.status == "N") {

                                        location.href = "/my/letter/?type=send"
                                    } else {
                                        location.href = "/my/letter/?type=send_temp"
                                    }
                                } else {
                                    alert(r.message);
                                }
                            });
                        }
                        else {
                            alert(res.msg);
                        }
                    });
                }
            }
        }

        $scope.loadData = function () {
            var data = $.parseJSON($("#save_data").val());
            console.log(data);

            $scope.status = data.status;
            $scope.repCorrespondenceID = data.repCorrespondenceID;
            $scope.selectChilden = data.children;

            $("#file_path").val(data.fileNameWithPath);
            //$("#lb_file_path").val(data.fileName);
            //if (data.lang == "ko") {
            //    $($("input[name=lang]")[0]).prop("checked", true);
            //} else {
            //    $($("input[name=lang]")[1]).prop("checked", true);
            //}

            $('input:radio[name=giftSeq]:input[value=' + data.giftSeq + ']').attr("checked", true);

            $scope.selectContent = data.letterComment;

            $scope.editable = $("#editable").val() === 'true';

            $scope.getList();

            $scope.modalChild.init();
        }

        $scope.cancel = function ($event, correspondenceId) {
            $event.preventDefault();

            var msg = ($scope.status == "N") ? "발송취소 하시겠습니까?" : "삭제 하시겠습니까?";

            if (!confirm(msg)) return;

            $http.post("/api/my/letter.ashx?t=cancel", { c: correspondenceId }).success(function (r) {

                //alert(r.message);

                if (r.success) {
                    $scope.goList();

                }
            });

            return false;
        }



        // 어린이선물
        $scope.modalChild = {

            total: 0,
            page: 1,
            rowsPerPage: 2,
            data: [],
            list: null,
            container: null,
            processing: false,
            total_ea: 0,
            total_amount: 0,
            child_count: 0,
            item_count: 0,
            title: "",
            image: "",
            instance: null,
            hd_inventory: '',
            price: 0,
            idx: 0,

            init: function () {
                // 팝업

                popup.init($scope, "/store/item-child-gift-letter", function (modal) {
                    $scope.modalChild.instance = modal;
                    $scope.modalChild.container = $("#childWrapper");

                    var obj = $scope.modalChild.container;
                    $scope.modalChild.title = '';

                    $scope.modalChild.image = '';

                    //var option = obj.find("[data-id=option]");
                    //option.empty();
                    //option.append($("#option > option").clone());
                    //option.change(function () {
                    //    $scope.modalChild.calculate();
                    //})

                    //option.selectbox({

                    //    onOpen: function (inst) {
                    //    },

                    //    onChange: function (val, inst) {
                    //        $scope.modalChild.calculate();
                    //    }
                    //});

                    $('.btn_ac .btn_type10.fl.mr10').css('display', 'none');

                }, { top: 0, iscroll: true });

            },

            reInit: function () {
                var obj = $scope.modalChild.container;

                var giftSeq = $("input[name=giftSeq]:checked").val();
                var giftIsStoreItem = $("input[name=giftSeq]:checked").attr('data-value');// == 'Y' ? true : false;
                var giftName = $("input[name=giftSeq]:checked").attr('data-text');

                $scope.modalChild.title = giftName;
                $scope.modalChild.image = $("input[name=giftSeq]:checked").parent().parent().find('img').attr("src");//$("#image").attr("src");
                $scope.modalChild.idx = $("input[name=giftSeq]:checked").attr('data-idx');

                var optionsData = $.parseJSON($("input[name=giftSeq]:checked").attr('data-option'));

                var option = obj.find("[data-id=option]");
                option.empty();
                //option.append($("#option > option").clone());
                $.each(optionsData, function () {
                    option.append("<option value='" + this.value + "' data-price='" + this.price + "'>" + this.text + "</option>");
                })

                option.change(function () {
                    $scope.modalChild.calculate();
                })

                option.selectbox('detach');

                option.selectbox({

                    onOpen: function (inst) {
                    },

                    onChange: function (val, inst) {
                        $scope.modalChild.calculate();
                    }
                });

                $scope.modalChild.hd_inventory = $("input[name=giftSeq]:checked").attr('data-hd_inventory');
                $scope.modalChild.price = $("input[name=giftSeq]:checked").attr('data-price');
            },

            show: function ($event) {
                $event.preventDefault();
                if (!common.checkLogin()) {
                    return false;
                }


                if (!$scope.modalChild.instance)
                    return;


                $scope.modalChild.getChildren();


            },

            hide: function ($event) {
                if ($event) $event.preventDefault();
                if (!$scope.modalChild.instance)
                    return;
                $scope.modalChild.instance.hide();

            },

            getChildren: function () {

                $http.get("/api/store.ashx?t=get_children", { params: {} }).success(function (r) {

                    if (r.success) {

                        $scope.modalChild.data = $.extend($scope.modalChild.data, r.data);

                        if (r.data.length < 1) {
                            alert("한국컴패션 후원자만 어린이에게 선물하실 수 있습니다.");
                            $scope.modalChild.hide();
                            return;
                        }

                        if ($scope.modalChild.EAInit() == false) {
                            alert('재고량이 부족 합니다.');
                            return;
                        }

                        $scope.modalChild.instance.show();

                        $scope.modalChild.total = r.data.length;

                        $scope.modalChild.getList({ page: 1 });

                    } else {

                        if (r.action == "not_sponsor") {
                            alert("한국컴패션 후원자만 어린이에게 선물하실 수 있습니다.");
                        } else if (!r.data) {
                            alert("후원가능한 어린이가 없습니다.");
                            return;
                        } else {
                            alert(r.message);
                        }
                    }
                });

            },

            EAInit: function () {
                $scope.modalChild.total_ea = 0;
                $.each($scope.modalChild.data, function () {
                    this.birthdate = new Date(this.birthdate);
                    var childmasterid = this.childmasterid;
                    var isChecked = false;

                    //2018-05-02 이종진 - 선택한 어린이는 수량을 1로 셋팅해줌.
                    if ($scope.list) {
                        $.each($scope.list, function () {
                            if (this.childmasterid == childmasterid) {
                                isChecked = true;
                                $scope.modalChild.total_ea++;
                            }
                        });
                    }
                    this.ea = isChecked ? 1 : 0;

                });

                var inventory = parseInt($scope.modalChild.hd_inventory);
                var total = $scope.modalChild.total_ea;
                if (total > inventory) {
                    return false;
                }

                var price = parseInt($scope.modalChild.price);
                var ea = $scope.modalChild.total_ea;
                $scope.modalChild.total_amount = price * ea;
                var count = 0;
                $.each($scope.modalChild.data, function () {
                    if (this.ea > 0) {
                        count++;
                    }
                });
                $scope.modalChild.child_count = count;
                $scope.modalChild.item_count = $scope.modalChild.total_ea.format();

                return true;
            },

            getList: function (param) {
                $scope.modalChild.page = param.page;
                var begin = ($scope.modalChild.page - 1) * $scope.modalChild.rowsPerPage;
                var end = ($scope.modalChild.page) * $scope.modalChild.rowsPerPage;

                $scope.modalChild.list = $scope.modalChild.data.slice(begin, end);
            },

            calculate: function () {

                // 선택된 상품수
                $scope.modalChild.item_count = $scope.modalChild.total_ea.format();

                var obj = $scope.modalChild.container;
                var price = parseInt($scope.modalChild.price);
                var ea = $scope.modalChild.total_ea;
                var opt_price = obj.find("[data-id=option] option:selected").data("price") || 0;


                $scope.modalChild.total_amount = ((opt_price + price) * ea);
                //console.log($scope.modalChild.total_amount);

                // 선택된 어린이수
                /*
                var count = 0;
                $.each(obj.find("[data-id=ea]"), function () {
                    if ($(this).val() != "0") {
                        count++;
                    }
                })
                */
                var count = 0;
                $.each($scope.modalChild.data, function () {
                    if (this.ea > 0) {
                        count++;
                    }
                });

                $scope.modalChild.child_count = count;

            },

            setBasket: function (action) {
                var childrenLetter = [];
                if ($scope.data) {
                    $.each($scope.data, function () {
                        //if (this.checked) {
                        //if (typeof this.childkey == "undefined" || this.childkey == "undefined" || this.childkey == null || this.childkey == "") {
                        //    checkChildKey = false;
                        //}
                        childrenLetter.push({ childMasterId: this.childmasterid, childKey: this.childkey });
                        //}
                    });
                }

                var giftSeq = $("input[name=giftSeq]:checked").val();
                var giftIsStoreItem = $("input[name=giftSeq]:checked").attr('data-value');// == 'Y' ? true : false;
                var giftName = $("input[name=giftSeq]:checked").attr('data-text');

                $.post("/common/handler/uploadImage.ashx", {
                    giftSeq: giftSeq,
                    path: $('#upload_root').val()
                }, function (res) {
                    if (res.success) {
                        var param = {
                            t: "update",
                            repCorrespondenceID: $scope.repCorrespondenceID,
                            samplelettercode: "",
                            file_path: res.name,
                            file_size: res.size,
                            lang: $("input[name=lang]:checked").val(),
                            status: 'N',
                            letter_comment: giftIsStoreItem == 'Y' ? giftName : '',
                            letter_type: giftIsStoreItem == 'Y' ? 'SPNPRE' : 'SPNLTR',
                            children: $.toJSON(childrenLetter),
                            is_pic_letter: "Y",
                            giftSeq: giftSeq,
                            giftIsStoreItem: giftIsStoreItem
                        };
                        var repCorrespondenceId = '';
                        var correspondenceWebID = '';
                        $http.post("/api/my/letter.ashx", param).success(function (r) {
                            if (r.success) {
                                repCorrespondenceId = r.data;
                                correspondenceWebID = r.message;
                                var obj = $scope.modalChild.container;

                                var json = {};
                                json.user_id = $("#hd_user_id").val();
                                json.item_id = $scope.modalChild.idx;//$("#hd_item_id").val();
                                json.option_name = obj.find("[data-id=option] option:selected").text();
                                json.option = obj.find("[data-id=option]").val();
                                json.option_price = obj.find("[data-id=option] option:selected").data("price") || 0;
                                json.quantity = $scope.modalChild.total_ea;
                                json.repCorrespondenceId = repCorrespondenceId;
                                json.CorrespondenceWebID = correspondenceWebID;

                                var children = [];

                                $.each($scope.modalChild.data, function () {
                                    if (this.ea > 0) {
                                        var child = {};
                                        child.childId = this.childkey;
                                        child.childName = this.namekr;
                                        child.ea = this.ea;
                                        children.push(child);
                                    }
                                })

                                json.children = children;

                                var jsonStr = $.toJSON(json);

                                //	console.log(json);
                                //	return;
                                $http.post("/api/store.ashx", { t: "set_basket", data: jsonStr }).success(function (r) {

                                    $scope.modalChild.processing = false;
                                    //	console.log(r);
                                    if (r.success) {
                                        //if (action == "cart") {
                                        //    if (!confirm('장바구니로 이동하시겠습니까?')) {
                                        //        $scope.modalChild.hide();
                                        //        return;
                                        //    }
                                        //}

                                        alert("편지가 등록되었습니다. 결제를 위해 장바구니로 이동하겠습니다.");

                                        location.href = '/store/cart';

                                    } else {
                                        alert(r.message);
                                    }

                                });

                            } else {
                                alert(r.message);
                            }
                        });
                    }
                    else {
                        alert(res.msg);
                    }
                });

                return;
            },

            buy: function (action) {

                if ($scope.modalChild.processing) return false;

                var index = $('[data-id=option] option').index($('[data-id=option] option:selected'));
                if ($('[data-id=option] option').size() > 1 && index < 1) {
                    alert('옵션을 선택해주세요');
                    $("[data-id=option]").focus();
                    return false;
                }

                if ($scope.modalChild.total_ea < 1) {
                    alert("상품갯수를 선택해주세요");
                    return false;
                }

                //	$scope.modalChild.processing = true;
                $scope.modalChild.setBasket(action);

            },

            plusEA: function ($event, item) {
                $event.preventDefault();

                var obj = $scope.modalChild.container;

                var group = item.childkey;
                var inventory = parseInt($scope.modalChild.hd_inventory);
                var total = $scope.modalChild.total_ea + 1;
                var ea = parseInt(obj.find("[data-id=ea][data-group='" + group + "']").val()) + 1;
                item.ea = ea;
                if (total > inventory) {
                    alert('재고량이 부족 합니다.');
                    return false;
                }

                $scope.modalChild.total_ea = total;
                obj.find("[data-id=ea][data-group='" + group + "']").val(ea);
                $scope.modalChild.calculate();
            },

            minusEA: function ($event, item) {
                $event.preventDefault();

                var obj = $scope.modalChild.container;
                var group = item.childkey;
                var total = $scope.modalChild.total_ea - 1;
                var ea = parseInt(obj.find("[data-id=ea][data-group='" + group + "']").val()) - 1;

                if (ea < 1) {
                    alert('수량은 1개 이상만 가능합니다.');
                    return;
                }

                item.ea = ea;
                $scope.modalChild.total_ea = total;
                obj.find("[data-id=ea][data-group='" + group + "']").val(ea);
                $scope.modalChild.calculate();

            }


        }

        $scope.loadData();
    });

})();

//var attachUploader = function (button) {
//    return new AjaxUpload(button, {
//        action: '/common/handler/upload',
//        responseType: 'json',
//        onChange: function () {
//        },
//        onSubmit: function (file, ext) {
//            this.disable();
//        },
//        onComplete: function (file, response) {

//            this.enable();

//            if (response.success) {
//                $('#newFile').val('Y');
//                $("#btn_file_remove").show();
//                alert("사진을 첨부하실 경우 편지는 일반우편과 동일한방식으로 배송됩니다.\n\r첨부 파일이 텍스트인 경우 번역이 되지 않으니 이 점 유의해 주세요.");

//                $("#file_path").val(response.name);
//                $("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
//                $("#file_size").val(response.size);

//                $(".attach_img").html($("<img src='" + ($("#domain_image").val() + response.name) + "' alt='이미지'/>"));

//            } else
//                alert(response.msg);
//        }
//    });
//}