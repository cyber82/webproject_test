﻿$(function () {



    $("#btn_file_remove").hide();
    var uploader = attachUploader("btn_file_path");
    uploader._settings.data.fileDir = $("#upload_root").val();
    uploader._settings.data.fileType = "image";
    uploader._settings.data.rename = "y";
    uploader._settings.data.limit = 2048;		// 2MB

    $("#lang_ko").click(function () {
        alert("한글으로는 번역 과정을 거치기 때문에,\n\r영어로 사용하시면 편지가 더 빨리 전달됩니다.");
    })

    if ($("#file_path").val() != "") {
        $("#btn_file_remove").show();
        $(".attach_img").html($("<img src='" + ($("#domain_image").val() + $("#file_path").val()) + "' alt='이미지'/>"));
    }

    $("#btn_file_remove").click(function () {

        $.post("/api/my/letter.ashx", { t: "file-delete", file_path: $("#file_path").val() }, function (r) {
            if (r.success) {

                $("#file_path").val("");
                $("#lb_file_path").val("");
                $("#btn_file_remove").hide();
                $(".attach_img").html("");

            } else {
                alert(r.message)
            }
        })

        return false;
    })

    $("#letterComment").textCount($("#comment_count"), { limit: 50 });

    $("#letterComment").keyup(function () {
        if ($("#comment_count").text() >= 50) {
            alert("50자 까지 입력할 수 있습니다.");
        }
        //if (getByteLength($(this).val()) >= 100) {
        //    alert("한글 50자(영문일 경우 100자) 까지 입력할 수 있습니다.");
        //}
    });
});

(function () {

    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

        $scope.requesting = false;
        $scope.total = -1;


        $scope.data = [];
        $scope.list = [];
        $scope.params = {
            page: 1,
            rowsPerPage: 5
        };

        $scope.selectChilden = [];
        $scope.repCorrespondenceID = "";

        $scope.listParams = paramService.getParameterValues();

        // list
        $scope.getList = function (params) {
            $scope.params = $.extend($scope.params, params);

            if ($scope.data.length) {

                $scope.showList();

            } else {

                $http.get("/api/my/child.ashx?t=list", { params: { letter: 1, page: 1, rowsPerPage: 1000 } }).success(function (r) {
                    if (r.success) {

                        $scope.data = $.extend($scope.data, r.data);
                        console.log($scope.data);

                        $scope.data = $.grep($scope.data, function (r) {

                            // 선택된 어린이면 
                            if ($.grep($scope.selectChilden, function (r2) {
								return r2.childMasterId == r.childmasterid;
                            }).length > 0) {
                                return true;
                            }

                            return false;

                        });
                        $scope.total = $scope.data.length;

                        $scope.showList();
                    } else {
                        alert(r.message);
                    }
                });

            }

        }

        $scope.showList = function () {
            var begin = ($scope.params.page - 1) * $scope.params.rowsPerPage;
            var end = ($scope.params.page) * $scope.params.rowsPerPage;
            $scope.list = $scope.data.slice(begin, end);
        };

        $scope.goList = function ($event) {
            if ($event) $event.preventDefault();
            location.href = "/my/letter/?" + $.param($scope.listParams);
        };

        $scope.submit = function ($event, status) {
            $event.preventDefault();
            if ($scope.requesting) return false;

            var children = $scope.selectChilden;

            //var checkChildKey = true;
            //if (children) {
            //    $.each(children, function () {
            //        if (typeof this.childKey == "undefined" || this.childKey == "undefined" || this.childKey == null || this.childKey == "") {
            //            checkChildKey = false;
            //        }
            //    });
            //}

            //if (!checkChildKey) {
            //    alert("어린이키에 문제가 있습니다. 관리자에게 문의해주세요.");
            //    return false;
            //}

            if (children.length < 1) {
                alert("편지를 보내실 어린이를 선택해주세요");
                return false;
            }

            if ($("input[name=lang]:checked").length < 1) {
                alert("편지에 사용하실 언어를 선택해 주세요.");
                return false;
            }

            if ($("#lb_file_path").val() == "") {
                alert("사진을 선택해주세요.");
                return false;
            }

            if (!confirm("[언어 : " + $("input[name=lang]:checked").data("text") + "] 로 등록 하시겠습니까?"))
                return false;

            var param = {
                t: "update",
                repCorrespondenceID: $scope.repCorrespondenceID,
                samplelettercode: $scope.samplelettercode,
                file_path: $("#file_path").val(),
                file_size: $("#file_size").val(),
                lang: $("input[name=lang]:checked").val(),
                letter_comment: $("#letterComment").val(),
                letter_type: "SPNLTR",
                children: $.toJSON(children),
                status: status

            };

            console.log(param);
            //	return;

            $scope.requesting = true;
            $http.post("/api/my/letter.ashx", param).success(function (r) {
                $scope.requesting = false;
                if (r.success) {
                    console.log(r.data);
                    if ($scope.status == "N") {

                        location.href = "/my/letter/?type=send"
                    } else {
                        location.href = "/my/letter/?type=send_temp"
                    }
                } else {
                    alert(r.message);
                }
            });

        }

        $scope.loadData = function () {
            var data = $.parseJSON($("#save_data").val());
            console.log(data);

            $scope.status = data.status;
            $scope.repCorrespondenceID = data.repCorrespondenceID;
            $scope.selectChilden = data.children;

            $("#file_path").val(data.fileNameWithPath);
            $("#lb_file_path").val(data.fileName);
            if (data.lang == "ko") {
                $($("input[name=lang]")[0]).prop("checked", true);
            } else {
                $($("input[name=lang]")[1]).prop("checked", true);
            }

            $scope.selectContent = data.letterComment;

            $scope.editable = $("#editable").val() === 'true';

            $scope.getList();
        }

        $scope.cancel = function ($event, correspondenceId) {
            $event.preventDefault();

            var msg = ($scope.status == "N") ? "발송취소 하시겠습니까?" : "삭제 하시겠습니까?";

            if (!confirm(msg)) return;

            $http.post("/api/my/letter.ashx?t=cancel", { c: correspondenceId }).success(function (r) {

                //alert(r.message);

                if (r.success) {
                    $scope.goList();

                }
            });

            return false;
        }

        $scope.loadData();

    });

})();

var attachUploader = function (button) {
    return new AjaxUpload(button, {
        action: '/common/handler/upload',
        responseType: 'json',
        onChange: function () {
        },
        onSubmit: function (file, ext) {
            this.disable();
        },
        onComplete: function (file, response) {

            this.enable();

            if (response.success) {
                $('#newFile').val('Y');
                $("#btn_file_remove").show();
                alert("사진을 첨부하실 경우 편지는 일반우편과 동일한방식으로 배송됩니다.\n\r첨부 파일이 텍스트인 경우 번역이 되지 않으니 이 점 유의해 주세요.");

                $("#file_path").val(response.name);
                $("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
                $("#file_size").val(response.size);

                $(".attach_img").html($("<img src='" + ($("#domain_image").val() + response.name) + "' alt='이미지'/>"));

            } else
                alert(response.msg);
        }
    });
}