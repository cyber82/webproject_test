﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_letter_write_pic : FrontBasePage
{


    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        domain_image.Value = ConfigurationManager.AppSettings["domain_file"];
        upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_letter);

        // 선물편지사용여부
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
        Object[] objSql = new object[1] { string.Format("select MEM_NAME_LANG0 from ETS_TC_CODE_MEMBER where Dim_Code = 'GIFT_LETTER' and MEM_Code = 'USE'") };
        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);
        if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
        {
            giftLetterUseYN.Value = "N";
        }
        else
        {
            giftLetterUseYN.Value = ds.Tables[0].Rows[0][0].ToString().ValueIfNull("");
        }
    }



}