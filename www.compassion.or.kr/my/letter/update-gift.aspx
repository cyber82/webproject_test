﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update-gift.aspx.cs" Inherits="my_letter_update_gift" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<%--<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>--%>
	<script type="text/javascript" src="/my/letter/update-gift.js?v=1.2"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" runat="server" id="save_data" value="" />
	<input type="hidden" runat="server" id="domain_image" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" id="file_size" value="" />
    <input type="hidden" id="newFile" value="N" />
	<input type="hidden" id="giftLetterUseYN" runat="server" value="" />
    <input type="hidden" id="hd_user_id" runat="server" />
    <input type="hidden" runat="server" id="editable" value="" />
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

		<div class="page_tit">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>

		<div class="subContents mypage">

			<div class="w980 letter">

				 <uc:menu runat="server"  />

				<!-- contents -->
				<div class="box_type4 mb40">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit mb30">
						<p class="tit noline">선물 편지 <span class="tit noline" ng-bind="editable ? '수정' : ' - 등록대기중'"></span></p>
					</div>
					<!--// -->

					<!-- 어린이 -->
					<div class="sel_child mb40">

						<div class="tableWrap4">
							<table class="tbl_child reserve">
								<caption>편지 수정할 어린이 정보 테이블</caption>
								<colgroup>
									<col style="width:2%" />
									<col style="width:12%" />
									<col style="width:60%" />
									<col style="width:26%" />
								</colgroup>
								<tbody>
									<tr ng-repeat="item in list">
										<td></td>
										<td class="tac">
											<span class="relative">
												<span class="pic" style="background:url('{{item.pic}}') no-repeat center top;background-size:cover;">어린이사진</span>
												<span class="status" ng-class="{ 'temp' : status == 'T' , 'standby' : status == 'N'}"></span>
											</span>
										</td>
										<td class="detail">
											<p class="s_tit5 mb10">{{item.name}} ({{item.personalnameeng}})</p>
											<span class="field fd2">국가</span><span class="data">{{item.countryname}}</span><span class="field">어린이ID</span><span class="data">{{item.childkey}}</span>
										</td>
										<td class="tar pr20">
											<span class="s_con6 inblock mb10">{{item.birth | date:'yyyy.MM.dd'}}</span><br />
											<span class="txt_status"><span ng-if="status == 'N'">등록대기중</span><span ng-if="status == 'T'">임시저장</span> 
												
											</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>

					<div class="tac mb40" ng-if="total > params.rowsPerPage ">
						 <paging class="small" page="params.page"  page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>   
					</div>
					<!--// 어린이 -->

					<!-- 수정테이블 -->
					<div>
						<div class="tableWrap1 mb30">
							<%--<table class="tbl_type1 line1">
								<caption>편지 수정 테이블</caption>
								<colgroup>
									<col style="width:17%" />
									<col style="width:83%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">언어선택</th>
										<td>
											<span class="radio_ui">
												<input type="radio" class="css_radio" name="lang" id="lang_ko" value="ko" data-text="한글" checked />
												<label for="lang_ko" class="css_label mr30" >한글</label>

												<input type="radio" id="lang_en" class="css_radio"  name="lang" value="en" data-text="영어"/>
												<label for="lang_en" class="css_label mr30">영어</label>
											</span>
											
											<p class="pt15"><span class="s_con1">사용하실 언어를 선택해주세요.</span></p>
										</td>
									</tr>
									
									<tr>
										<th scope="row">첨부파일</th>
										<td>
											<div class="mb10">
												<div class="btn_attach clear2 fl relative">
													
													<input type="text" runat="server" id="lb_file_path" class="input_type1 fl mr10" style="width:400px; background:#fdfdfd;border:1px solid #d8d8d8;" disabled  />
													
													<a href="#" id="btn_file_path" class="btn_type8 fl"><span>파일찾기</span></a>
												</div>
												<a href="#" id="btn_file_remove" class="btn_type9 ml10">삭제</a>
											</div>
											<span class="s_con1 mb20">첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</span>

											<div class="attach_img"></div>
										</td>
									</tr>

									<tr>
										<th scope="row"><label for="msg">메세지<br />(선택)</label></th>
										<td>
											<textarea name="letterComment" id="letterComment" class="letterComment textarea_type2 msg" ng-model="selectContent"></textarea>
											<span class="s_con1">후원 어린이에게 전할 메시지는 글자수 한글 50자로 제한되어 있습니다. (<span id="comment_count"></span>/50자)</span>
										</td>
									</tr>

								</tbody>
							</table>--%>
							<div style="font-size: 16px; color: #121212; margin:20px 0;">선물선택</div>
                            <asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
				                <ItemTemplate>
					                <div style="width: 223px; height:300px; display:inline-block; margin:2px;">
                                        <div style="margin-bottom:5px; text-overflow:ellipsis;">
                                            <input type="radio" name="giftSeq" id="rd<%#Eval("SEQ") %>" value="<%#Eval("SEQ") %>" class="css_radio" data-value="<%#Eval("IsStoreItem") %>" data-text="<%#Eval("Title") %>" ng-click="checkChildSponsorType('<%#Eval("IsStoreItem") %>')" data-price="<%#Eval("price") %>" data-option='<%#Eval("option") %>' data-hd_inventory="<%#Eval("hd_inventory") %>" data-idx="<%#Eval("idx") %>"/>
										    <label for="rd<%#Eval("SEQ") %>"><div title="<%#Eval("Title") %>" style="white-space: nowrap; text-overflow: ellipsis; max-width: 200px; overflow: hidden; display: inline-block;"><%#Eval("Title") %></div></label>
                                        </div>
                                        <asp:Image ID="imgGift" runat="server" style="width:223px; height:auto;" oncontextmenu="return false;" />
					                </div>
				                </ItemTemplate>
				                <FooterTemplate>
					                <tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
						                <td colspan="5">데이터가 없습니다.</td>
					                </tr>
				                </FooterTemplate>

			                </asp:Repeater>		
						</div>

						<div class="tac relative">
							<a href="#" ng-if="status == 'N'" class="btn_type2 mr5" ng-click="cancel($event,repCorrespondenceID)" ng-show="editable">발송취소</a>
							<a href="#" ng-if="status == 'T'" class="btn_type2 mr5" ng-click="cancel($event,repCorrespondenceID)" ng-show="editable">삭제</a>

							<a href="#" class="btn_type3 mr5" ng-if="status == 'T'" ng-click="submit($event,'T')" ng-show="editable">임시저장</a>
							<a href="#" class="btn_type5" ng-if="status == 'T'" ng-click="submit($event,'N')" ng-show="editable">편지 보내기</a>
							<a href="#" class="btn_type1" ng-if="status == 'N'" ng-click="submit($event,'N')" ng-show="editable">수정완료</a>

							<a href="#" class="btn_type4 posR" ng-click="goList($event)">목록</a>
						</div>
					</div>
					<!--// 수정테이블 -->

				</div>
				<!--// contents -->

			</div>

			<div class="h100"></div>
		</div>	
	</section>

</asp:Content>

