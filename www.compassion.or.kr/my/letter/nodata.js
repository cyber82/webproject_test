﻿$(function () {
	

});


(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		
		$scope.item = $.parseJSON($("#data").val());
		$scope.item.birthdate = new Date($scope.item.birthdate);

        $scope.AdminCK = "";

        //20171206 김은지
        //백오피스 기능 추가, 로그인 타입 가져오기
        $scope.getAdminCK = function () {

            $http.get("/api/userInfo.ashx?t=get").success(function (r) {
                console.log(r);
                if (r.success) {
                    var entity = $.parseJSON(r.data);
                    $scope.AdminCK = entity.AdminLoginCheck;
                } else {

                }
            });
        }

        $scope.getAdminCK();

		$scope.showChildPop = function ($event, item) {
			loading.show();
			if ($event) $event.preventDefault();
			popup.init($scope, "/common/child/pop/" + item.countrycode + "/" + item.childmasterid + "/" + item.childkey, function (modal) {
				
				modal.show();

				initChildPop($http , $scope, modal, item);

			}, { top: 0, iscroll: true, removeWhenClose: true });
		}


	    // list
		$scope.getList = function (params) {

		    $http.get("/api/my/child.ashx?t=list", { params: { page: 1, rowsPerPage: 1000 } }).success(function (r) {
		        if (r.success) {
		            $scope.data = $.extend($scope.data, r.data);
		            $scope.total = $scope.data.length > 0 ? $scope.data[0].total : 0;
		            if ($scope.data.length == undefined)
		                $scope.total = $scope.data[0].total;

		            if ($scope.total > 0)
		                $("#sp_NoData").html("<em class='fc_blue'>후원자님께서는 현재 어린이와<br />편지를 주고받지 않으시는 머니후원 중이십니다. <br />어린이의 꿈을 후원해 주셔서 감사합니다.");

		            else
		                $("#sp_NoData").html("<em class='fc_blue'>후원중인 어린이</em>가 없습니다.<br />어린이의 미소를 후원해주세요.");

		        }
		    });
		}
		$scope.getList();


	});

})();