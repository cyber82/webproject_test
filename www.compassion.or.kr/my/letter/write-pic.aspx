﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="write-pic.aspx.cs" Inherits="my_letter_write_pic" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
	<script type="text/javascript" src="/my/letter/write-pic.js?v=1.6"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" runat="server" id="upload_root" value="" />
	<input type="hidden" runat="server" id="domain_image" value="" />
	<input type="hidden" id="file_path" value="" />
	<input type="hidden" id="file_size" value="" />
	<input type="hidden" id="giftLetterUseYN" runat="server" value="" />
<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage"  >

			<div class="w980 letter">

				<uc:menu runat="server"  />

				<!-- contents -->
				<div class="box_type4 mb40" id="t">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit">
						<p class="tit">편지쓰기</p>
					</div>
					<!--// -->

					<div class="sub_desc">
						<p class="txt2"><em class="em1">나의 어린이와 소중한 추억을 만들고 싶으세요?</em></p>
					</div>

					<script type="text/javascript">
						$(function () {
							$(".sel_letter .box").click(function () {
								$(".sel_letter .box").removeClass("on");
								$(this).addClass("on");
							})
						})
					</script>

					<!-- 편지선택 -->
					<div class="sel_letter clear2 mb60" >
                        <% if (giftLetterUseYN.Value == "N") { %>
						<a href="/my/letter/write/?c={{c}}#t"><div class="box box1">
							<span class="icon"></span>
							<div class="txt">
								<span class="icon2"></span><span>편지 쓰기</span>
							</div>
						</div></a>

						<div class="box box2 on">
							<span class="icon"></span>
							<div class="txt">
								<span class="icon2"></span><span>사진 편지 쓰기</span>
							</div>
						</div>
                        <% } else { %>
                        <a href="/my/letter/write/?c={{c}}#t"><div class="box box1" style="width:300px;">
							<span class="icon"></span>
							<div class="txt">
								<span class="icon2"></span><span>편지 쓰기</span>
							</div>
						</div></a>

						<div class="box box2 on" style="float:left; width:300px; margin-left:9px;">
							<span class="icon"></span>
							<div class="txt">
								<span class="icon2"></span><span>사진 편지 쓰기</span>
							</div>
						</div>
                        <a href="/my/letter/write-gift/?c={{c}}#t"><div class="box box2" style="float:right; width:300px;">
							<span class="icon"></span>
							<div class="txt">
								<span class="icon2"></span><span>선물 편지 쓰기</span>
							</div>
						</div></a>
                        <% } %>
					</div>
					<!--// 편지선택 -->

					<div class="vi_info" id="l">
						<p class="txt1">잠깐만요! 여러명의 어린이를 후원하고 계신가요?</p>
						<p class="txt2">
							한번의 편지작성으로 선택한 후원어린이 모두에게 편지를 보내실 수 있습니다.<br />
							이 경우, 편지에 특정어린이의 이름을 쓰지 않으셔야 정상적으로 발송될 수 있습니다.
						</p>
					</div>

					<!-- 어린이리스트 -->
					<div class="tbl_sort mb15" >
						<span class="number">
							나의 결연어린이 <em class="fc_blue">{{total}}</em>명&nbsp;&nbsp;&nbsp;
							<span class="checkbox_ui">
								<input type="checkbox" class="css_checkbox" id="chk_all2" ng-disabled="readonly" ng-click="checkAllChild('chk_all2')" ng-hide="is_reply" />
								<label for="chk_all2" class="css_label font2 fs15" ng-hide="is_reply" >후원중인 모든 어린이 선택</label>
							</span>
						</span>

						<div class="topLink2">
							<a href="#" class="faq">편지 FAQ</a>
							<a href="#" class="what">편지 영상 보기</a>
						</div>
					</div>
					<div class="tableWrap1 mb30" >
                        <style>
                            .tbl_type6.padding1 th, .tbl_type6.padding1 td { padding:10px 5px; }
                        </style>
						<table class="tbl_type6 padding1">
							<caption>어린이리스트 테이블</caption>
							<colgroup>
								<col style="width:5%" ng-hide="is_reply"/>
								<col style="width:21%" />
								<col style="width:25%" />
								<col style="width:19%" />
								<col style="width:15%" />
								<col style="width:15%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col" ng-hide="is_reply" >
										<span class="checkbox_ui default">
											<input type="checkbox" class="css_checkbox" id="chk_all" ng-disabled="readonly" ng-click="checkAllChild('chk_all')" />
											<label for="chk_all" class="css_label">결연어린이 모두 선택</label>
										</span>
									</th>
									<th scope="col">국가</th>
									<th scope="col">이름</th>
									<th scope="col">어린이ID</th>
									<th scope="col">성별</th>
									<th scope="col">생일</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in list">
									<td ng-hide="is_reply" >
										<span class="checkbox_ui default">
											<input type="checkbox" id="cb{{$index}}" ng-model="item.checked" ng-checked="item.checked" class="css_checkbox" ng-click="checkChild(item)" />
											<label for="cb{{$index}}" class="css_label" ng-click="checkChildLabel(item)">어린이 선택</label>
										</span>
									</td>
									<td>{{item.countryname}}</td>
									<td>{{item.name}}<br />{{item.personalnameeng}}</td>
									<td>{{item.childkey}}</td>
									<td>{{item.gender}}</td>
									<td>{{item.birthdate | date:'yyyy.MM.dd'}}</td>
								</tr>
								<tr ng-if="total == 0">
									<td colspan="6">후원중인 어린이가 없습니다.</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// 어린이리스트 -->

					<!-- page navigation -->
					<div class="tac mb40" ng-if="total > params.rowsPerPage ">
						 <paging class="small" page="params.page"  page-size="params.rowsPerPage"  total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>   
					</div>
					<!--// page navigation -->

					<!-- 쓰기테이블 -->
					<div>
						<p class="mb5"><span class="s_con1 fc_blue">여러명의 어린이에게 보내실 경우 어린이 이름은 제외하고 작성해주세요.</span></p>

						<div class="tableWrap1 mb30">
							<table class="tbl_type1 line1">
								<caption>편지 수정 테이블</caption>
								<colgroup>
									<col style="width:17%" />
									<col style="width:83%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">언어선택</th>
										<td>
											<span class="radio_ui">
												<input type="radio" name="lang" id="lang_ko" value="ko" data-text="한글" class="css_radio" checked />
												<label for="lang_ko" class="css_label mr30">한글</label>

												<input type="radio" name="lang" value="en" id="lang_en" data-text="영어" class="css_radio" />
												<label for="lang_en" class="css_label mr30">영어</label>
											</span>
											
											<p class="pt15"><span class="s_con1">사용하실 언어를 선택해주세요.</span></p>
										</td>
									</tr>
									
									<tr>
										<th scope="row">첨부파일</th>
										<td>
											<div class="mb10">
												<div class="btn_attach clear2 fl relative">
													
													<input type="text" runat="server" id="lb_file_path" class="input_type1 fl mr10" style="width:400px;background:#fdfdfd;border:1px solid #d8d8d8;" disabled  />
													
													<a href="#" id="btn_file_path" class="btn_type8 fl"><span>파일선택</span></a>
												</div>
												<a href="#" id="btn_file_remove" class="btn_type9 ml10">삭제</a>
											</div>
											<span class="s_con1 mb20">첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</span>

											<div class="attach_img"></div>
										</td>
									</tr>

									<tr>
										<th scope="row"><label for="msg">메세지<br />(선택)</label></th>
										<td>
											<textarea name="letterComment" id="letterComment" class="letterComment textarea_type2 msg" ></textarea>
											<span class="s_con1">후원 어린이에게 전할 메시지는 글자수 한글 50자로 제한되어 있습니다. (<span id="comment_count"></span>/50자)</span>
										</td>
									</tr>

								</tbody>
							</table>
							
						</div>

						<div class="tac relative mb80" >
							<a href="#" ng-if="total > 0" ng-click="submit($event,'N')" class="btn_type5 mr5" >편지 보내기</a>
							<a href="#" ng-if="total > 0" ng-click="submit($event,'T')" class="btn_type3">임시저장</a>
							
							<a href="#" ng-click="goList($event)" class="btn_type4 posR ">목록</a>
						</div>
					</div>
					<!--// 쓰기테이블 -->

				</div>
				<!--// contents -->

			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>

</asp:Content>

