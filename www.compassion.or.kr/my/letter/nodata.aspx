﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="nodata.aspx.cs" Inherits="my_letter_nodata" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/my/letter/nodata.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" id="data" runat="server" />
	
<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
	
		<!-- 타이틀 -->
		<div class="page_tit" ng-if="AdminCK == false">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980">

				<!-- 후원자 이름 -->
				<uc:menu runat="server"  />
				<!--// -->

				<div class="mb20"></div>

				<!-- 후원중인 어린이 -->
				<div class="box_type4 no_child">
					<span id="sp_NoData">
						<em class="fc_blue">후원중인 어린이</em>가 없습니다.<br />어린이의 미소를 후원해주세요.
					</span>
				</div>
				<!--// 후원중인 어린이 -->

				<!-- 후원중인 어린이가 없는경우 -->
				<div class="box_type4 myChild none">
					
					<div class="childBox">
						<div class="snsWrap">
							<span class="day">{{item.waitingdays}}일</span>
							<span class="sns_ani" ng-if ="AdminCK == false">
								<span class="common_sns_group">
									<span class="wrap">
										<a href="#" title="페이스북" data-role="sns" data-provider="fb" data-url="" class="sns_facebook" child-item="{{item}}">페이스북</a>
										<a href="#" title="카카오스토리" data-role="sns" data-provider="ks" data-url="" class="sns_story" child-item="{{item}}">카카오스토리</a>
										<a href="#" title="트위터" data-role="sns" data-provider="tw" data-url="" class="sns_twitter" child-item="{{item}}">트위터</a>
										<a href="#" title="url 공유" data-role="sns" data-provider="copy" data-url="" class="sns-copy sns_url" child-item="{{item}}">url 공유</a>
									</span>
								</span>
								<button class="common_sns_share">공유하기</button>
							</span>
						</div>

						<div class="child_info">
							<span class="pic" background-img="{{item.pic}}" data-default-image="/common/img/page/my/no_pic.png" style="background: no-repeat center; background-size: 150px; background-position-y: 0">양육어린이사진</span>
							<span class="name">{{item.name}}</span>
							<p class="info">
								<span>국가 : {{item.countryname}}</span><br />
								<span class="bar">생일 : {{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><span>성별 : {{item.gender}}</span>
							</p>
						</div>

						<div class="more" ng-if="AdminCK == false"><a href="#" ng-click="showChildPop($event , item)" >더 알아보기</a></div>
					</div>
					
					<div class="textWrap">
						<span class="title"><em><asp:Literal runat="server" ID="userName" /></em>님을 기다리고 있는<br />어린이가 있어요.</span><br />
						<p class="con">
							1:1 결연이 만드는 사랑의 기적,<br />
							한 어린이가 꿈을 찾는 양육비는 <em class="fc_blue">매월 4만 5천원</em>입니다.<br />
							후원금과 기도, 사랑의 편지로 아이에게 꿈을 주세요.<br />
						</p>
					</div>
				</div>
				<!--// 후원중인 어린이가 없는경우 -->

			</div>
			<div class="h100"></div>
		</div>

		
		

    </section>


</asp:Content>
