﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="my_letter_smart_letter_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript">

        var noChild = "<%= nSponCount.ToString() %>"

		$(function () {

		});

	    (function () {

		    var app = angular.module('cps.page', []);
		    app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {
   	        // list
		        $scope.getList = function (params) {

		            $http.get("/api/my/child.ashx?t=list", { params: { page: 1, rowsPerPage: 1000 } }).success(function (r) {
		                if (r.success) {
		                    $scope.data = $.extend($scope.data, r.data);
		                    $scope.total = $scope.data.length > 0 ? $scope.data[0].total : 0;
		                    if ($scope.data.length == undefined)
		                        $scope.total = $scope.data[0].total;

		                    var canLetterCnt = 0;
		                    var total = 0;
		                    $.each($scope.data, function () {
		                        total++;
		                        if (this.canletter)
		                            canLetterCnt += 1;
		                    });

		                    $("#hfChildCnt").val(total);
		                    $("#hfLetterCnt").val(canLetterCnt);

		                    if (canLetterCnt == 0) {
		                        alert("후원자님께서는 현재 어린이와 \r\n편지를 주고받지 않으시는 머니후원 중이십니다. \r\n어린이의 꿈을 후원해 주셔서 감사합니다.");
		                        //alert("후원자님께서 머니 후원 중이신 일부 어린이에게는 \r\n편지가 전달되지 않습니다. \r\n어린이의 꿈을 후원해 주셔서 감사합니다.");
		                        location.href = "/my/letter/nodata";
		                    }
		                    
		                }
		            });
		        }

		        //if (noChild == '0') {
		        //    alert("후원자님께서는 현재 어린이와 \r\n편지를 주고받지 않으시는 머니후원 중이십니다. \r\n어린이의 꿈을 후원해 주셔서 감사합니다.");
		        //    //alert("후원자님께서 머니 후원 중이신 일부 어린이에게는 \r\n편지가 전달되지 않습니다. \r\n어린이의 꿈을 후원해 주셔서 감사합니다.");
		        //    location.href = "/my/letter/nodata";
		        //}
		        //else if (noChild == '1')
		            $scope.getList();
		    });
		})();

		function Validate5() {
			return confirm("스마트레터 서비스를 신청하시겠습니까?");
		}

		function Validate6() {
		    if (!document.getElementById("select2").checked && !document.getElementById("select3").checked) {
				alert('신청하실 서비스를 선택해 주세요.');
				return false;
			}
			return confirm("스마트레터 서비스를 신청하시겠습니까?");
		}

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
    <asp:HiddenField runat="server" ID="hfChildCnt" Value="0" />
    <asp:HiddenField runat="server" ID="hfLetterCnt" Value="0" />

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents mypage">

			<div class="w980 letter">

				<uc:menu runat="server"  />

				<!-- contents -->
				<div class="box_type4">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit">
						<p class="tit">스마트레터 서비스 신청</p>
					</div>
					<!--// -->

					<!-- 서브 문구 -->
					<div class="sub_desc">
						<p class="txt2">스마트레터 서비스란?</p>
					</div>
					<!--// -->

					<div class="bg_info">
						<p runat="server" id="letter_more5" visible="false">
							<em>어린이에게 편지 한 장이라도 써주고 싶지만 미루고 미루시다 깜빡 잊어버리실까 염려되세요?</em><br />
							스마트레터 서비스를 신청해주시면 컴패션에서 매년 후원자님을 대신하여<br />
							<!-- 5명 이하 후원시 노출 -->
							후원 어린이에게 크리스마스 카드와 어린이날 카드를 전해드립니다.
					
							
						</p>

                        <p runat="server" id="letter_more6" visible="false">
                            <em>어린이에게 편지 한 장이라도 써주고 싶지만 미루고 미루시다 깜빡 잊어버리실까 염려되세요?</em><br />
                            스마트레터 서비스를 신청해주시면 컴패션에서 매년 후원자님을 대신하여<br />
							<!-- 6명 이상 후원시 노출 -->
                            후원 어린이에게 생일 카드, 크리스마스 카드, 어린이날 카드 및 편지를 전해드립니다.
							
						</p>
					</div>

					<!-- 스마트레터 신청 -->
					<div id="submit5" runat="server">
						<div class="box_type5 smart_con">

							<div>
								<p class="tit">카드 서비스</p>

								<span class="radio_ui">
									<input type="radio" id="card_1" name="select" class="css_radio"  checked />
									<label for="card_1" class="css_label mr30">매년 크리스마스 카드와 어린이날 축하 카드를 어린이에게 대신 보내드려요.</label>
								</span>
								<!--<p class="con">(어린이날 축하카드는 만 13세 이하 어린이에게 보내지는 편지입니다.)</p>-->
							</div>
						</div>

						<div class="tac mb60">
							
							<asp:LinkButton runat="server" CssClass="btn_type1" ID="btnSave5" runat="server" OnClientClick="return Validate5()"  onclick="btnSave5_Click">스마트레터 서비스 신청하기</asp:LinkButton>

						</div>
					</div>

                    <div id="exist5" runat="server">
                        <div class="box_type5 smart_con">

                            <p class="tit">이용중인 서비스</p>
                            <span class="checkbox_ui">
                                <input type="checkbox" class="css_checkbox" id="cb_SingleUseCard" runat="server" />
                                <label for="cb_SingleUseCard" class="css_label mr30">후원자님께서는 현재 스마트레터 카드 서비스를 이용중입니다.</label>
                            </span>
                        </div>
                        <div class="tac mb60">
                            <asp:LinkButton runat="server" CssClass="btn_type1" ID="btn_cancel" OnClick="btn_cancel_Click">스마트레터 서비스 취소하기</asp:LinkButton>
                        </div>
                    </div>

                     <div id="exist6" runat="server">
                        <div class="box_type5 smart_con">

                            <p class="tit">이용중인 서비스</p>
                            <span class="checkbox_ui" visible="false" id="exist6_letter" runat="server">
                                <input type="checkbox" class="css_checkbox" id="cb_MultiUseLetter" runat="server" />
                                <label for="cb_MultiUseLetter" class="css_label">후원자님께서는 현재 스마트레터 편지 서비스를 이용중입니다.</label>
                            </span>
                            <br />
                            <span class="checkbox_ui" visible="false" id="exist6_card" runat="server">
                                <input type="checkbox" class="css_checkbox" id="cb_MultiUseCard" runat="server" />
                                <label for="cb_MultiUseCard" class="css_label">후원자님께서는 현재 스마트레터 카드 서비스를 이용중입니다.</label>
                            </span>
                        </div>
                        <div class="tac mb60">
                            <asp:LinkButton runat="server" CssClass="btn_type1" ID="btn_cancel2" OnClick="btn_cancel_Click">스마트레터 서비스 취소하기</asp:LinkButton>
                        </div>
                    </div>

					<div id="submit6" runat="server">
						<div class="box_type5 smart_con">

							<!-- 6명 이상 후원시 노출 -->
							<div class="mb40 pt10" runat="server" id="submit6_letter">
								<!--
								<span class="radio_ui">
									<input type="radio" name="select" id="select1" class="css_radio" runat="server"/>
									<label for="select1" class="css_label mr30">한장 써서 모두에게
								</span>
								<p class="con">
									후원자님께서 편지를 <strong>한장만 써서 컴패션에 보내주시면 모든 어린이들에게  동일한 내용</strong>으로 편지를 보내드려요. (컴패션에서 보내드린 '한장써서 모두에게' 전용편지를 이용해주세요.)
								</p>
								-->
								<p class="tit">편지 서비스</p>
								<span class="checkbox_ui">
									<input type="checkbox" class="css_checkbox" id="select2" runat="server"/>
									<label for="select2" class="css_label">일년에 두 번, 후원자님을 대신해 사랑이 듬뿍 담긴 정성스러운 편지를 어린이들에게 전해드려요.</label>
								</span>

								<p class="con"></p>
								<!--<p class="con">일년에 두 번, 후원자님을 대신해 사랑이 듬뿍 담긴 정성스러운 편지를 어린이들에게 전해드려요.</p>-->
							</div>
							<!--// -->

							<div  runat="server" id="submit6_card">
								<p class="tit">카드 서비스</p>
								<span class="checkbox_ui">
									<input type="checkbox" class="css_checkbox" id="select3" runat="server"/>
									<label for="select3" class="css_label">매년 생일, 크리스마스, 어린이날 축하 카드를 어린이에게 대신 보내드려요.</label>
								</span>

								<p class="con"></p>
							</div>
						</div>

						<div class="tac mb60">
							<asp:LinkButton runat="server" CssClass="btn_type1" ID="btnSave6" OnClientClick="return Validate6()"  onclick="btnSave6_Click">스마트레터 서비스 신청하기</asp:LinkButton>
                        </div>
                    </div>


					<div class="box_type3" runat="server" id="more5">
						<div class="guide">
							<p>더 알고 싶어요!</p>
							<ul>
								<li>
									<span class="s_con1">
										매년 새로운 내용의 카드를 후원 어린이에게 전해드립니다.<br />
										(크리스마스/어린이날이 지난 후에 신청하시면 그 다음해부터 발송 됩니다.)
									</span>
								</li>
								<li><span class="s_con1">어린이날 카드는 만 13세 이하 어린이에게만 보내지는 카드입니다.</span></li>
								<li><span class="s_con1">여러명의 어린이들을 후원하고 있어도 염려 마세요. 정성 듬뿍 담은 카드를 후원 어린이 모두에게 보내드립니다.</span></li>
								<li><span class="s_con1">스마트레터 서비스를 통해 어린이에게 보낸 카드는 홈페이지 > 마이컴패션 > 편지 > 편지함이나 애플리케이션에서 확인하실 수 있습니다.</span></li>
								<li><span class="s_con1">스마트레터 서비스를 이용하시더라도, 어린이에게 직접 편지와 카드를 작성하실 수 있습니다.</span></li>
							</ul>
						</div>
					</div>
				
				</div>
				<!--// contents -->

			</div>

			<div class="h100"></div>
		</div>	
		<!--// e: sub contents -->

		
		

    </section>


</asp:Content>
