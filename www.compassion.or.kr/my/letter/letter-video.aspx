﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="letter-video.aspx.cs" Inherits="my_letter_letter_video" %>

<div style="background: transparent; width: 800px;">

    <div class="pop_type1 w800">
		<div class="pop_title">
			<span>편지 영상 보기</span>
			<button class="pop_close" type="button" ng-click="modalVideo.close($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content">
			<iframe width="740" height="415" id="letter_movie" src="https://www.youtube.com/embed/hgVb-T5KPSQ?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

			<div class="tac pt30">
				<a href="/my/letter/write/" class="btn_type5">어린이에게 편지쓰기</a>
			</div>
		</div>
	</div>

</div>
