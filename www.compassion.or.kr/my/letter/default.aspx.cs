﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class my_letter_default : FrontBasePage {

	public override bool RequireLogin {
		get {
			return true;
		}
	}
    
    protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var sponsorType = new UserInfo().SponsorType;
		//	if (sponsorType == "머니" || sponsorType == "머니/일반") {
		if(!string.IsNullOrEmpty(sponsorType) && sponsorType.IndexOf("결연") < 0 && sponsorType.IndexOf("편지") < 0 ) {
			Response.Redirect("/my/letter/nodata");
		}

	}

}