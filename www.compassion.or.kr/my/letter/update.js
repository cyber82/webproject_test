﻿var selectedSampleItem = null;

$(function () {

    $("#btn_file_remove").hide();
    var uploader = attachUploader("btn_file_path");
    uploader._settings.data.fileDir = $("#upload_root").val();
    uploader._settings.data.fileType = "image";
    uploader._settings.data.rename = "y";
    uploader._settings.data.limit = 2048;

    $("#lang_ko").click(function () {
        alert("한글으로는 번역 과정을 거치기 때문에,\n\r영어로 사용하시면 편지가 더 빨리 전달됩니다.");
    })

    if ($("#file_path").val() != "") {
        $("#btn_file_remove").show();
        $(".attach_img").html($("<img src='" + ($("#domain_image").val() + $("#file_path").val()) + "' alt='이미지'/>"));
    }

    $("#btn_file_remove").click(function () {

        $.post("/api/my/letter.ashx", { t: "file-delete", file_path: $("#file_path").val() }, function (r) {
            if (r.success) {

                $("#file_path").val("");
                $("#lb_file_path").val("");
                $("#btn_file_remove").hide();
                $(".attach_img").html("");

            } else {
                alert(r.message)
            }
        })

        return false;
    })


    $("#letterComment").textCount($("#comment_count"), { limit: 2000 });

    $("#letterComment").keyup(function () {
        //if ($("#comment_count").text() >= 1000) {
        //    alert("1000자 까지 입력할 수 있습니다.");
        //}
        if (getByteLength($(this).val()) >= 2000) {
            alert("한글 1000자(영문일 경우 2000자) 까지 입력할 수 있습니다.");
        }
    });


});

(function () {

    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

        $scope.requesting = false;
        $scope.total = -1;

        $scope.data = [];
        $scope.list = [];
        $scope.params = {
            page: 1,
            rowsPerPage: 5
        };

        $scope.selectType = null;
        $scope.selectTypeName = null;
        $scope.selectContent = null;
        $scope.sample_letters = null;

        $scope.samplelettercode = "";
        $scope.selectChilden = [];
        $scope.repCorrespondenceID = "";

        $scope.listParams = paramService.getParameterValues();

        // list
        $scope.getList = function (params) {
            $scope.params = $.extend($scope.params, params);

            if ($scope.data.length) {
                $scope.showList();

            } else {

                $http.get("/api/my/child.ashx?t=list", { params: { letter: 1, page: 1, rowsPerPage: 1000 } }).success(function (r) {
                    if (r.success) {
                        $scope.data = $.extend($scope.data, r.data);
                        console.log($scope.data);

                        $scope.data = $.grep($scope.data, function (r) {

                            // 선택된 어린이면 
                            if ($.grep($scope.selectChilden, function (r2) {
								return r2.childMasterId == r.childmasterid;
                            }).length > 0) {
                                return true;
                            }

                            return false;

                        });
                        $scope.total = $scope.data.length;

                        $scope.showList();
                    } else {
                        alert(r.message);
                    }
                });

            }

        }

        $scope.showList = function () {
            var begin = ($scope.params.page - 1) * $scope.params.rowsPerPage;
            var end = ($scope.params.page) * $scope.params.rowsPerPage;
            $scope.list = $scope.data.slice(begin, end);
        };

        $scope.goList = function ($event) {
            if ($event) $event.preventDefault();
            location.href = "/my/letter/?" + $.param($scope.listParams);
        };

        // type1 : 3자리 , type2 : 6자리
        $scope.selectSampleType = function (type1, type2, title, skip_alert, isConSet) {
            if (type1 == "BBI" && !skip_alert) {
                alert("후원어린이의 성장 사진과 소식이 담긴 보고서에 \r\n대한 답장 편지일 경우 선택해주세요.");
            }

            $scope.bg_class = "bg_" + type1;

            $scope.selectType = type2;
            $scope.selectTypeName = title;
            // 예문데이타 로딩
            $http.get("/api/my/letter.ashx?t=sample-letter", { params: { type: type1 } }).success(function (r) {
                if (r.success) {
                    $scope.sample_letters = r.data;

                    $.each(r.data, function () {
                        // 예문 선택 된 언어만 가져오기 위해 추가 문희원 2017-04-07
                        this.lan = $("input[name=lang]:checked").val();
                        this.lan_ko = $("input[name=lang]:checked").val() == "ko" ? "한글(예문)" : "English(예문)";
                        if (this.samplelettercode == $scope.samplelettercode) {
                            selectedSampleItem = this;
                        }
                    });

                    $scope.SetSampleLan(isConSet);
                } else {
                    alert(r.message);
                }
            });
        }

        $scope.SetSampleLan = function (isConSet) {

            if (selectedSampleItem != null) {
                var lan = $("input[name=lang]:checked").val();
                $scope.samplelettercode = selectedSampleItem.samplelettercode;
                if (isConSet) {
                    if (lan == "ko") {
                        $scope.selectContent = selectedSampleItem.contextkor;
                    } else {
                        $scope.selectContent = selectedSampleItem.contexteng;
                    }
                }
            }
            else
                if (isConSet)
                    $scope.selectContent = "";
        }

        // 편지언어클릭
        $scope.languageClick = function () {
            if ($("input[name=letterType]:checked").length == 0) {
                return;
            }
            else {
                var id = $("input[name=letterType]:checked")[0].id;

                setTimeout(function () {
                    angular.element('#' + id).trigger('click');
                    $scope.clicked = true;
                }, 0);
            }
        }

        $scope.selectSample = function ($event, item, lang) {
            $event.preventDefault();

            $scope.samplelettercode = item.samplelettercode;
            selectedSampleItem = item;
            if (lang == "ko") {
                $scope.selectContent = item.contextkor;
            } else {
                $scope.selectContent = item.contexteng;
            }


        }

        $scope.submit = function ($event, status) {
            $event.preventDefault();
            if ($scope.requesting) return false;

            var children = $scope.selectChilden;

            //var checkChildKey = true;
            //if (children) {
            //    $.each(children, function () {
            //        if (typeof this.childKey == "undefined" || this.childKey == "undefined" || this.childKey == null || this.childKey == "") {
            //            checkChildKey = false;
            //        }
            //    });
            //}

            //if (!checkChildKey) {
            //    alert("어린이키에 문제가 있습니다. 관리자에게 문의해주세요.");
            //    return false;
            //}

            if (children.length < 1) {
                alert("편지를 보내실 어린이를 선택해주세요");
                return false;
            }

            if ($("input[name=lang]:checked").length < 1) {
                alert("편지에 사용하실 언어를 선택해 주세요.");
                return false;
            }

            if ($("input[name=letterType]:checked").length < 1) {
                alert("편지종류를 선택해 주세요.");
                return false;
            }

            if ($("#letterComment").val().trim().length < 1) {
                alert("편지내용을 입력해 주세요.");
                return false;
            }

            var lang = $("input[name=lang]:checked").val();
            var comment = $("#letterComment").val();
            var isTempChange = 0;

            // 한글 선택 예문이 변경되지 않았다면 영문으로 설정할 수 있도록
            if (selectedSampleItem != undefined && selectedSampleItem != null && selectedSampleItem.contextkor == $("#letterComment").val() && $("input[name=lang]:checked").val() == "ko" && status != "T") {
                isTempChange = 1;
            }
            else {
                if (status == "N" && !confirm("[언어 : " + $("input[name=lang]:checked").data("text") + " / 편지타입 : " + $scope.selectTypeName + "] 로 발송 하시겠습니까?"))
                    return false;
            }

            var param = {
                t: "update",
                repCorrespondenceID: $scope.repCorrespondenceID,
                samplelettercode: $scope.samplelettercode,
                file_path: $("#file_path").val(),
                file_size: $("#file_size").val(),
                lang: lang,
                letter_comment: comment,
                letter_type: $scope.selectType,
                children: $.toJSON(children),
                status: status,
                isTempChange: isTempChange
            };

            $scope.requesting = true;
            $http.post("/api/my/letter.ashx", param).success(function (r) {
                $scope.requesting = false;
                if (r.success) {
                    console.log(r.data);
                    if ($scope.status == "N") {

                        location.href = "/my/letter/?type=send"
                    } else {
                        location.href = "/my/letter/?type=send_temp"
                    }
                } else {
                    alert(r.message);
                }
            });

        }

        $scope.loadData = function () {
            var data = $.parseJSON($("#save_data").val());
            console.log("loadData", data);

            //	$("input[name=letterType]." + data.samplelettercode).prop("checked", true);

            $scope.status = data.status;
            $scope.repCorrespondenceID = data.repCorrespondenceID;
            $scope.selectChilden = data.children;
            $("#file_path").val(data.fileNameWithPath);
            $("#lb_file_path").val(data.fileName);
            if (data.lang == "ko") {
                $($("input[name=lang]")[0]).prop("checked", true);
            } else {
                $($("input[name=lang]")[1]).prop("checked", true);
            }

            var letter_type = $("input[name=letterType][data-code6='" + data.letterType + "']");
            var letter_code3 = letter_type.data("code3");
            var letter_code6 = letter_type.data("code6");
            var letter_title = letter_type.data("title");
            letter_type.prop("checked", true);
            $scope.samplelettercode = data.samplelettercode;
            $scope.selectSampleType(letter_code3, letter_code6, letter_title, true, false);
            $scope.selectContent = data.letterComment;

            $scope.editable = $("#editable").val() === 'true';

            $scope.getList();
        }

        $scope.cancel = function ($event, correspondenceId) {
            $event.preventDefault();

            var msg = ($scope.status == "N") ? "발송취소 하시겠습니까?" : "삭제 하시겠습니까?";

            if (!confirm(msg)) return;

            $http.post("/api/my/letter.ashx?t=cancel", { c: correspondenceId }).success(function (r) {

                //alert(r.message);

                if (r.success) {
                    $scope.goList();

                }
            });

            return false;
        }

        $scope.loadData();

    });

})();

var attachUploader = function (button) {
    return new AjaxUpload(button, {
        action: '/common/handler/upload',
        responseType: 'json',
        onChange: function () {
        },
        onSubmit: function (file, ext) {
            this.disable();
        },
        onComplete: function (file, response) {

            this.enable();

            if (response.success) {

                $("#btn_file_remove").show();
                alert("첨부 파일에 내용이 있는 경우 번역이 되지 않습니다");

                $("#file_path").val(response.name);
                $("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
                $("#file_size").val(response.size);

                $(".attach_img").html($("<img src='" + ($("#domain_image").val() + response.name) + "' alt='이미지'/>"));

            } else
                alert(response.msg);
        }
    });
}