﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="my_letter_update" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/my/menu.ascx" TagPrefix="uc" TagName="menu" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
	<script type="text/javascript" src="/my/letter/update.js?v=1.2"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" runat="server" id="save_data" value="" />
	<input type="hidden" runat="server" id="domain_image" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" id="file_size" value="" />
    <input type="hidden" runat="server" id="editable" value="" />
	
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

		<div class="page_tit">
			<div class="titArea">
				<h1>마이컴패션</h1>
				<span class="desc">후원내역을 확인하고 사랑하는 어린이를 위해 편지를 쓸 수 있는 후원자님만의 공간입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>

		<div class="subContents mypage" >

			<div class="w980 letter">

				 <uc:menu runat="server"  />

				<!-- contents -->
				<div class="box_type4 mb40">
					
					<!-- 서브타이틀 -->
					<div class="sub_tit mb30">
						<p class="tit noline">편지 <span class="tit noline" ng-bind="editable ? '수정' : ' - 등록대기중'"></span></p>
					</div>
					<!--// -->

					<!-- 어린이 -->
					<div class="sel_child mb40">

						<div class="tableWrap4">
							<table class="tbl_child reserve">
								<caption>편지 수정할 어린이 정보 테이블</caption>
								<colgroup>
									<col style="width:2%" />
									<col style="width:12%" />
									<col style="width:60%" />
									<col style="width:26%" />
								</colgroup>
								<tbody>
									<tr ng-repeat="item in list">
										<td></td>
										<td class="tac">
											<span class="relative">
												<span class="pic" style="background:url('{{item.pic}}') no-repeat center top;background-size:cover;">어린이사진</span>
												<span class="status" ng-class="{ 'temp' : status == 'T' , 'standby' : status == 'N'}"></span>
											</span>
										</td>
										<td class="detail">
											<p class="s_tit5 mb10">{{item.name}} ({{item.personalnameeng}})</p>
											<span class="field">어린이ID</span><span class="data">{{item.childkey}}</span><span class="field fd2">국가</span><span class="data">{{item.countryname}}</span>
										</td>
										<td class="tar pr20">
											<span class="s_con6 inblock mb10">{{item.birth | date:'yyyy.MM.dd'}}</span><br />
											<span class="txt_status"><span ng-if="status == 'N'">등록대기중</span><span ng-if="status == 'T'">임시저장</span> 
												
											</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>

					<div class="tac mb40" ng-if="total > params.rowsPerPage ">
						 <paging class="small" page="params.page"  page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>   
					</div>
					<!--// 어린이 -->

					<!-- 수정테이블 -->
					<div>
						<div class="tableWrap1 mb30">
							<table class="tbl_type1 line1">
								<caption>편지 수정 테이블</caption>
								<colgroup>
									<col style="width:17%" />
									<col style="width:83%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">언어선택</th>
										<td>
											<span class="radio_ui">
												<input type="radio" class="css_radio" name="lang" id="lang_ko" value="ko" data-text="한글" ng-click="languageClick()" ng-disabled="!editable" checked />
												<label for="lang_ko" class="css_label mr30" >한글</label>

												<input type="radio" id="lang_en" class="css_radio"  name="lang" value="en" data-text="영어" ng-click="languageClick()" ng-disabled="!editable" />
												<label for="lang_en" class="css_label mr30">영어</label>
											</span>
											
											<p class="pt15" ng-show="editable"><span class="s_con1">사용하실 언어를 선택해주세요.</span></p>
										</td>
									</tr>
									<tr>
										<th scope="row">편지종류</th>
										<td>
											<ul class="letter_thumbList clear2">
												<asp:Repeater ID="repeater_types" runat="server" >
													<ItemTemplate>
														<li>
															<span class="img"><img src="/common/img/page/my/letter_thumb_<%#Eval("code3") %>.png" alt="편지지 샘플" /></span>
															<span class="radio_ui">
																<input type="radio" name="letterType" id="rd<%#Container.ItemIndex %>" class="css_radio" data-code3="<%#Eval("code3") %>" data-code6="<%#Eval("code6") %>" data-title="<%#Eval("title") %>" ng-click="selectSampleType('<%#Eval("code3") %>' , '<%#Eval("code6") %>', '<%#Eval("title") %>', true, true)" />
																<label for="rd<%#Container.ItemIndex %>" class="css_label mr30"><asp:Literal runat="server" ID="text" Text=<%#Eval("title") %>/></label>
															</span>
														</li>
													</ItemTemplate>
												</asp:Repeater>
											</ul>
										</td>
									</tr>
									<tr ng-show="selectType && editable" >
										<th scope="row">편지예문</th>
										<td>
											<p class="mb5" ng-show="editable"><span class="s_con1 fc_blue">편지 종류를 먼저 선택해주세요. 각 종류에 맞는 편지예문을 보실 수 있습니다.</span></p>

											<div ng-show="sample_letters">
												<div class="clear2 mb10" ng-repeat="item in sample_letters">
													<span class="s_con3 fl link1 relative"><span ng-bind-html="item.samplelettercodename"></span><span ng-if="item.used == 'Y'">(사용)</span><span class="sample_new" ng-if="item.new == true"></span></span>
													<span class="s_con3 fr relative">
														<button class="s_con3 link1" ng-click="selectSample($event,item,item.lan)"><span ng-bind-html="item.lan_ko"></span></button>
													</span>
												</div>												
											</div>
										</td>
									</tr>
									<tr ng-show="selectType">
										<th scope="row"><label for="write" class="hidden">편지작성</label></th>
										<td>
											<div class="box_type5 pl40 mb20" ng-show="editable">
												<ul>
													<li><span class="s_con1">예문을 클릭하시면 보다 쉽고 간편하게 편지를 작성하실 수 있습니다.</span></li>
													<li><span class="s_con1">화면상의 편지 배경과 실제 편지의 배경은 차이가 있습니다.</span></li>
												</ul>
											</div>

											<!-- 편지쓰기 -->
											<div class="letter_paper" ng-class="bg_class" id="letterType" >
												<textarea name="letterComment" id="letterComment" class="letterComment textarea_type2" ng-model="selectContent" ng-disabled="!editable"></textarea>

											</div>
											<!--// 편지쓰기 -->

											<span class="s_con1" ng-show="editable">편지 본문 내용의 글자수는 1000자로 제한되어 있습니다. (<span id="comment_count"></span>/1000자)</span>
										</td>
									</tr>
									<tr ng-show="selectType">
										<th scope="row">첨부파일</th>
										<td>
											<div class="mb10" ng-show="editable">
												<div class="btn_attach clear2 fl relative">
													
													<input type="text" runat="server" id="lb_file_path" class="input_type1 fl mr10" style="width:400px;background:#fdfdfd;border:1px solid #d8d8d8;" disabled  />
													
													<a href="#" id="btn_file_path" class="btn_type8 fl"><span>파일선택</span></a>
												</div>
												<a href="#" id="btn_file_remove" class="btn_type9 ml10">삭제</a>
											</div>
											<span class="s_con1 mb20" ng-show="editable">첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</span>

											<div class="attach_img"></div>
										</td>
									</tr>
								</tbody>
							</table>
							
						</div>

						<div class="tac relative">
							<a href="#" ng-if="status == 'N'" class="btn_type2 mr5" ng-click="cancel($event,repCorrespondenceID)" ng-show="editable">발송취소</a>
							<a href="#" ng-if="status == 'T'" class="btn_type2 mr5" ng-click="cancel($event,repCorrespondenceID)" ng-show="editable">삭제</a>

							<a href="#" class="btn_type3 mr5" ng-if="status == 'T'" ng-click="submit($event,'T')" ng-show="editable">임시저장</a>
							<a href="#" class="btn_type5" ng-if="status == 'T'" ng-click="submit($event,'N')" ng-show="editable">편지 보내기</a>
							<a href="#" class="btn_type1" ng-if="status == 'N'" ng-click="submit($event,'N')" ng-show="editable">수정완료</a>

							<a href="#" class="btn_type4 posR" ng-click="goList($event)">목록</a>
						</div>
					</div>
					<!--// 수정테이블 -->

				</div>
				<!--// contents -->

			</div>

			<div class="h100"></div>
		</div>	
	</section>

</asp:Content>

