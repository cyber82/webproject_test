﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class my_letter_update_gift : FrontBasePage
{
    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1)
        {
            Response.Redirect("/my/letter/", true);
        }
        base.PrimaryKey = requests[0];      // correspondenceID

        domain_image.Value = ConfigurationManager.AppSettings["domain_file"];
        upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_letter);


        this.LoadData();

        //// 선물편지사용여부
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
        //Object[] objSql = new object[1] { string.Format("select MEM_NAME_LANG0 from ETS_TC_CODE_MEMBER where Dim_Code = 'GIFT_LETTER' and MEM_Code = 'USE'") };
        //DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "TEXT", null, null);
        //if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
        //{
        //    giftLetterUseYN.Value = "N";
        //}
        //else
        //{
        //    giftLetterUseYN.Value = ds.Tables[0].Rows[0][0].ToString().ValueIfNull("");
        //}
        //if (giftLetterUseYN.Value == "N")
        //{
        //    base.AlertWithJavascript("선물편지관리에서 사용하지 않도록 설정되어있습니다.", "location.href='/my/letter'");
        //}

        //  선물리스트
        Object[] objSql2 = new object[1] { "Corr_S_GiftLetter" };
        Object[] objParam = new object[] { "DIV", "USE" };
        Object[] objValue = new object[] { "", 1 };
        var giftList = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql2, "SP", objParam, objValue).Tables[0];

        giftList.Columns.Add("price", typeof(String));
        giftList.Columns.Add("delivery_charge", typeof(String));
        giftList.Columns.Add("option", typeof(String));
        giftList.Columns.Add("hd_inventory", typeof(String));

        foreach (DataRow dr in giftList.Rows)
        {


            if (dr["IsStoreItem"].ToString().Equals("Y"))
            {
                using (StoreDataContext dao = new StoreDataContext())
                {
                    var idx = Convert.ToInt32(dr["idx"]);
                    //var entity = dao.product.First(p => p.idx == idx && p.display_flag == true);
                    var entity = www6.selectQFStore<product>("idx", idx, "display_flag", 1);

                    //var opts = dao.product_option.Where(p => p.product_idx == idx && p.option_level == 0 && p.del_flag == false && p.display == true).ToList();
                    var opts = www6.selectQStore<product_option>("product_idx", idx, "option_level", 0, "del_flag", 0, "display", 1);

                    //var imgs = dao.product_image.Where(p => p.product_idx == idx).ToList();
                    //btn_show_child.Visible = entity.gift_flag.Value;
                    //hd_title.Value = title.Text = entity.name;

                    //hd_price.Value = entity.selling_price.ToString();
                    dr["price"] = entity.selling_price.ToString();

                    //price.Text = entity.selling_price.ToString("N0");
                    //status.Text = entity.inventory > 0 ? "판매중" : "품절";

                    //hd_inventory.Value = entity.inventory.ToString();
                    dr["hd_inventory"] = entity.inventory.ToString();

                    //product_detail.Text = entity.product_detail;

                    //hd_delivery_charge.Value = entity.delivery_charge.ToString().ValueIfNull("0");
                    dr["delivery_charge"] = entity.delivery_charge.ToString().ValueIfNull("0");

                    string option = "[{\"text\":\"옵션 없음\",\"value\":\"0\"}]";
                    if (opts.Count > 0)
                    {
                        string optTemp = "{\"text\":\"옵션을 선택하세요\",\"value\":\"\"}";
                        foreach (var opt in opts)
                        {
                            var text = opt.option_name + (opt.option_price > 0 ? "(+" + opt.option_price.Value.ToString("N0") + "원)" : "");
                            optTemp += ",{\"text\":\"" + text + "\",\"value\":\"" + opt.idx.ToString() + "\",\"price\":\"" + opt.option_price.Value.ToString() + "\"}";
                        }
                        option = "[" + optTemp + "]";
                    }
                    else
                    {
                        //option.Items.Add(new ListItem("옵션 없음", "0"));
                    }
                    dr["option"] = option;

                    //this.ViewState["image"] = "";

                    //if (imgs.Count != 0)
                    //{
                    //    this.ViewState["image"] = (Uploader.GetRoot(Uploader.FileGroup.image_shop) + imgs[0].saved_file_name).WithFileServerHost();
                    //}

                    //repeater_image.DataSource = imgs;
                    //repeater_image.DataBind();

                    //if (UserInfo.IsLogin)
                    //{
                    //    hd_user_id.Value = new UserInfo().UserId;
                    //}

                    //hd_item_id.Value = this.PrimaryKey.ToString();
                }
            }
        }

        repeater.DataSource = giftList;

        repeater.DataBind();

        if (UserInfo.IsLogin)
        {
            hd_user_id.Value = new UserInfo().UserId;
        }
    }

    public void LoadData()
    {
        try
        {

            LetterAction.SaveEntity entity = new LetterAction.SaveEntity();

            CommonLib.WWWService.ServiceSoap _wwwService = new CommonLib.WWWService.ServiceSoapClient();

            var sRepCorrespondenceID = this.PrimaryKey.ToString();
            //DataSet
            DataTable dtChildInfo = (DataTable)new LetterAction().GetTemporaryDetail(sRepCorrespondenceID).data;

            //sponCount.Text = dtChildInfo.Rows.Count.ToString();

            //	Response.Write(dtChildInfo.ToJson());
            foreach (DataRow dr in dtChildInfo.Rows)
            {
                entity.children.Add(new LetterAction.SaveEntity.child()
                {
                    childKey = dr["childKey"].ToString(),
                    childMasterId = dr["childMasterId"].ToString()
                });
            }

            if (dtChildInfo.Rows.Count > 0)
            {
                //변수선언
                string sCorrespondenceWebID = string.Empty;         //CorrespondenceWebID
                string vChildMasterID = string.Empty;               //ChildMasterID
                string sChildKey = string.Empty;                    //ChildKey
                string sCorrespondenceType = string.Empty;          //편지종류
                string sCorrespondenceContext = string.Empty;       //편지내용
                string sAttachYn = string.Empty;                    //첨부파일여부
                string sLanguageType = string.Empty;                //언어타입
                string sOrder = string.Empty;                       //편지종류 순번 2012-10-18 추가

                //바인딩
                sCorrespondenceWebID = dtChildInfo.Rows[0]["CorrespondenceWebID"].ToString();
                vChildMasterID = dtChildInfo.Rows[0]["ChildMasterID"].ToString();
                sChildKey = dtChildInfo.Rows[0]["ChildKey"].ToString();
                sCorrespondenceType = dtChildInfo.Rows[0]["CorrespondenceType"].ToString();
                sCorrespondenceContext = dtChildInfo.Rows[0]["CorrespondenceContext"].ToString();
                sAttachYn = dtChildInfo.Rows[0]["AttachYn"].ToString();
                sLanguageType = dtChildInfo.Rows[0]["LanguageType"].ToString();
                entity.status = dtChildInfo.Rows[0]["corrStatus"].ToString();


                if (sLanguageType == "한글")
                {
                    entity.lang = "ko";
                }
                else
                {
                    entity.lang = "en";
                }
                entity.letterComment = sCorrespondenceContext;
                entity.repCorrespondenceID = this.PrimaryKey.ToString();

                //첨부파일이 있을경우
                if (sAttachYn.Equals("Y"))
                {
                    DataSet dsAttachFileYn = new DataSet();

                    dsAttachFileYn = _wwwService.getWebMailFileName(sRepCorrespondenceID);

                    if (dsAttachFileYn.Tables[0].Rows.Count > 0)
                    {
                        entity.fileNameWithPath = (upload_root.Value + dsAttachFileYn.Tables[0].Rows[0]["FileName"].ToString());
                    }

                }

                if (sCorrespondenceType.Trim().Equals("XCSLTR") || sCorrespondenceType.Trim().Equals("XCSATM") || sCorrespondenceType.Trim().Equals("SPNATM"))
                {
                    sCorrespondenceType = "SPNLTR";
                }
                //생일카드
                else if (sCorrespondenceType.Trim().Equals("XCSBDC"))
                {
                    sCorrespondenceType = "SPNBDC";
                }
                //어린이날카드
                else if (sCorrespondenceType.Trim().Equals("XCSBBC"))
                {
                    sCorrespondenceType = "SPNBBC";
                }
                //크리스마스카드
                else if (sCorrespondenceType.Trim().Equals("XCSCC"))
                {
                    sCorrespondenceType = "SPNCMS";
                }
                //추가 2012-10-18 편지종류 추가
                //첫편지
                else if (sCorrespondenceType.Trim().Equals("XCSBIO"))
                {
                    sCorrespondenceType = "SPNBIO";
                }
                //성장보고 답장편지
                else if (sCorrespondenceType.Trim().Equals("XCSBBI"))
                {
                    sCorrespondenceType = "SPNBBI";
                }


                entity.letterType = sCorrespondenceType;

                entity.giftSeq = dtChildInfo.Rows[0]["giftSeq"].ToString();

                string corrStatus = dtChildInfo.Rows[0]["corrStatus"].ToString();
                var bEditable = corrStatus == "T" || (corrStatus == "N" && dtChildInfo.Rows[0]["RegisterDate"].ToString().Substring(0, 10) == DateTime.Now.ToString("yyyy-MM-dd"));
                editable.Value = bEditable.ToString().ToLower();

            }

            save_data.Value = entity.ToJson();

        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
            base.AlertWithJavascript("데이타를 불러오는 도중 에러가 발생했습니다.", "goBack()");
            return;
        }
    }

    protected void ListBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
            return;

        DataRowView drv = (DataRowView)e.Item.DataItem;
        string img = drv["Contents"].ToString();
        ((System.Web.UI.WebControls.Image)e.Item.FindControl("imgGift")).ImageUrl = "data:image/png;charset=utf-8;base64," + img;
    }

}