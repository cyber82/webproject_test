﻿
$(function () {
	Sympathy.init($("#sympathy_visual"), ".list", $("#sympathy_visual_right"), $("#sympathy_visual_left"), 2);
});

var Sympathy = {

	lock: false,
	shadow_count : 2 ,	// 좌우측 각각 갯수
	visible_count: 2,
	obj_container: null,
	items: [],
	item_width: null,
	opacity: 0.3,
	init: function (obj_container, items_class, btn_left, btn_right, visible_count) {
	
		this.items = obj_container.find(items_class);
		
		if (this.items.length > 0) {
			/*
			좌우측 fadeIn/Out 되는 영역의 효과를 처리하기위해 
			양쪽 사이드에 2개씩의 아이템이 오도록 갯수 관리 
			visible item 을 최소 4개로 설정했기때문에, visible_count * 2개를 최소 아이템 갯수로 잡는다.
			*/

			var item_size = this.items.length;
			while (this.items.length < item_size * Sympathy.shadow_count) {
				$.each(this.items, function () {
					//var div = $(this).wrapAll("<div/>").parent().html();
					var div = $(this).clone();
					obj_container.append(div);

				})
				this.items = obj_container.find(items_class);
			}

		}

		this.visible_count = visible_count;
		this.obj_container = obj_container;
		this.item_width = parseInt($(this.items[0]).css("width").replace("px", ""));

		// click left
		btn_left.click(function () {
			if (Sympathy.lock) return;
			Sympathy.beforeMove("left");
			var offset = "-=" + $(Sympathy.items[0]).css("width").replace("px", "");
			Sympathy.obj_container.animate({ left: offset }, 500, function () {
				Sympathy.afterMove("left");
			});
		});

		// click right
		btn_right.click(function () {
			if (Sympathy.lock) return;
			Sympathy.beforeMove("right");
			var offset = "+=" + $(Sympathy.items[0]).css("width").replace("px", "");
			Sympathy.obj_container.animate({ left: offset }, 500, function () {
				Sympathy.afterMove("right");
			});
		});

		$.each(this.items, function (i, val) {
			var obj = $(val);
			var index = 0;

			if (i > (Sympathy.visible_count + Sympathy.shadow_count)) {
				index = i - Sympathy.items.length;
				//index = Sympathy.visible_count + Sympathy.shadow_count - i;
				//index = -((Sympathy.shadow_count+2) + index);
			} else {
				index = i;
			}
			obj.attr("id", "index_" + index);
			//obj.css({ left: ((index + 1) * Sympathy.item_width) + "px", display: "none" });
			obj.css({ left: ((index + 1) * Sympathy.item_width) + "px" });
		//	obj.hide();
		});

		Sympathy.setVisible('none');

		$("#sympathy_visual").show();
	},

	beforeMove: function (direction) {
		Sympathy.lock = true;
		Sympathy.setVisible(direction);
	},

	afterMove: function (direction) {

		if (direction == "left") {

			var maxLeft = 0;
			$.each(Sympathy.items, function (i, val) {
				var obj = $(val);
				var left = parseInt(obj.css("left").replace("px", ""));
				if (left > maxLeft)
					maxLeft = left;
			});

			$.each(Sympathy.items, function (i, val) {
				var obj = $(val);
				var index = Sympathy.getIndex(obj);
				index = index - 1;

				if (index == -(Sympathy.shadow_count+2)) {		// 가장 마지막 아이템의 index를 가장 높은 index로 변경
					index = Sympathy.items.length - Sympathy.shadow_count-2;
					obj.css("left", (maxLeft + Sympathy.item_width) + "px");
			
				//	console.log("minLeft", (maxLeft + Sympathy.item_width));
				}

				$(this).attr("id", "index_" + index);

			});
		} else if (direction == "right") {
		
			var minIndex = -Sympathy.shadow_count-1;

			var minLeft = 10000;
			$.each(Sympathy.items, function (i, val) {
				var obj = $(val);
				var left = parseInt(obj.css("left").replace("px", ""));
				if (left < minLeft)
					minLeft = left;
			});

			$.each(Sympathy.items, function (i, val) {
				var obj = $(val);
				var index = Sympathy.getIndex(obj);
				index = index + 1;

				if (index == Sympathy.items.length - Sympathy.shadow_count-1) {
					index = minIndex;
					obj.css("left", (minLeft - Sympathy.item_width) + "px");
				}

				$(this).attr("id", "index_" + index);
			});

		}
		Sympathy.lock = false;
	},

	setVisible: function (direction) {

		$.each(Sympathy.items, function (i, val) {
			var obj = $(val);
			var index = Sympathy.getIndex(obj);

			obj.off("mouseenter");

			if (direction == 'none') {

				if ((index < 0 && Math.abs(index) <= Sympathy.shadow_count) || (index >= Sympathy.visible_count && (index - Sympathy.visible_count < Sympathy.shadow_count))) {
					obj.fadeTo(0.0, Sympathy.opacity);
					obj.find("img").fadeTo(0.0, Sympathy.opacity);
				} else if (index > -1 && index < Sympathy.visible_count) {
					obj.show();
					Sympathy.setShowEvent(obj);
				} else {
					obj.hide();
				}

			} else if (direction == 'left') {
				console.log(index);
				if (index == -Sympathy.shadow_count) {
					obj.fadeTo("slow", 0.0);
				} else if (index == 0) {
					obj.fadeTo("slow", Sympathy.opacity);
					obj.find("img").fadeTo("slow", Sympathy.opacity);
				} else if (index == Sympathy.visible_count) {
					obj.fadeTo("fast", 1);
					obj.find("img").fadeTo("fast", 1);
					Sympathy.setShowEvent(obj);
				} else if (index >= Sympathy.visible_count && index <= (Sympathy.visible_count + Sympathy.shadow_count+1)) {
					obj.fadeTo("slow", Sympathy.opacity);
					obj.find("img").fadeTo("slow", Sympathy.opacity);
				} else if (index > 0 && index <= Sympathy.visible_count) {
					obj.show();
					obj.find("img").show();
					Sympathy.setShowEvent(obj);
				} else {
					//obj.hide();
				}
			} else if (direction == 'right') {
				if (index == -1) {
					obj.fadeTo("fast", 1);
					obj.find("img").fadeTo("fast", 1);
					Sympathy.setShowEvent(obj);
				} else if (index >= -(Sympathy.shadow_count+1) && index < 0) {
					obj.fadeTo("slow", Sympathy.opacity);
					obj.find("img").fadeTo("slow", Sympathy.opacity);
				} else if (index >= Sympathy.visible_count - 1 && index < Sympathy.visible_count + Sympathy.shadow_count-1) {
					obj.fadeTo("slow", Sympathy.opacity);
					obj.find("img").fadeTo("slow", Sympathy.opacity);
				} else if (index == Sympathy.visible_count + Sympathy.shadow_count-1) {
					obj.fadeTo("slow", 0.0);
					obj.find("img").fadeTo("slow", 0.0);
				} else if (index > -1 && index <= Sympathy.visible_count - 1) {
					obj.show();
					obj.find("img").show();
					Sympathy.setShowEvent(obj);
				} else {
			//		obj.hide();
				}
			}
		})

	},

	setShowEvent: function (obj) {

		obj.on("mouseenter", function () {
			$(this).addClass("selected");
		}).on("mouseleave", function () {
			$(this).removeClass("selected");
		});
	},

	getIndex: function (obj) {
		return parseInt(obj.attr("id").split("_")[1]);
	}
}