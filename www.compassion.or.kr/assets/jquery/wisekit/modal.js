
function modalShow(obj, onAppended, isCancelWhenClickBackground, isCancelWhenScroll, onCancelWhenClickBackground) {
	
	$("body").append("<div id=mask></div>");
	$("body").css({ "overflow": "hidden" })

	obj = $(obj);
	// html 을 새로 생성할지 재활용 할지 여부 , instance , static
	var modal_type = obj.attr("data-modal-type");
	if (!modal_type) {
		modal_type = "instance";
	}

	var modal_position = obj.data("position") || "auto";
	
	var mask_index = obj.attr("data-mask-index") || "auto";

	var mask_content = modal_type == "instance" ? obj.clone() : obj;
	mask_content.attr("data-id", "mask_content");
	mask_content.attr("data-modal-type", modal_type);

	$("body").append(mask_content);
	
	$(window).resize(function () {
		
		$('#mask').css({ 'width': $(document).width(), 'height': $(document).height() });
		
		if ($("#mask_content").length > 0) {
			var top = (($(window).height() - parseInt($("#mask_content").css("height").replace("px", ""))) / 2);
			var left = ($(window).width() - parseInt($("#mask_content").css("width").replace("px", ""))) / 2;

			$("#mask_content").css({ top: top, left: left });
		}
	});

	$(document).bind('mousewheel', function (e) {
		if (isCancelWhenScroll) {
			closeModal();
		}
	});
	
	if (onAppended)
		onAppended(mask_content);

	var contentH = mask_content.height();
	var contentHR = Math.round(contentH, 0);
	
	var contentTop = $(window).scrollTop() + ($(window).height() / 2) - (contentHR / 2);
	var contentLeft = ($(document).width() / 2) - (mask_content.width() / 2);
	var maskWidth = $(document).width();
	
	var maskIndex = mask_index == "auto" ? (parseInt(mask_content.css("z-index")) - 1) : mask_index;
	$('#mask').css({ 'width': $(document).width(), 'height': $(document).height(), 'position': 'absolute', 'opacity': 0.0, 'left': '0px', 'top': '0px', 'background': '#000', 'z-index': maskIndex });
	if (modal_position == "auto")
		mask_content.css({ 'top': contentTop, 'left': contentLeft, 'opacity': 0.0 });
	
	
	if (!navigator.userAgent.match(/SHW-/i)) {
		$("#mask").fadeTo(300, 0.7);
	}
	mask_content.fadeTo(300, 1.0);
	
	if (isCancelWhenClickBackground) {
		$('#mask').unbind("click");
		$('#mask').click(function () {
			closeModal(onCancelWhenClickBackground);
		});
	}

};

function closeModal(cb) {

	if ($("[data-id='mask_content']").length > 0) {
		$("[data-id='mask_content']").fadeTo(300, 0.0, function () {
			if ($("[data-id='mask_content']").attr("data-modal-type") == "instance")
				$("[data-id='mask_content']").remove();
			else
				$("[data-id='mask_content']").hide();

		});
	}

	if ($("#mask").length > 0) {
		$("#mask").fadeTo(300, 0.0, function () {
			$("#mask").remove();
		});
	}

	
	$("body").css({ "overflow": "auto" })
	if (cb) cb();
};
