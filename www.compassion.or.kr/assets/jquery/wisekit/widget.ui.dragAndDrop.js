/*
whilestomach , 2014-05-30

2015-04-09 : 한페이지에 여러개 위젯을 사용할때 option 에 들어가 있는 dropContainer 등의 pointer 가 가장 마지막을 가리키고 있어서 ,
해당 문제 수정(data 사용으로 변경)
*/

(function ($) {

	$.widget("ui.dragAndDrop", {

		_init: function () {
			var self = this, o = this.options;
			this.data = $.data(this);

			o.zIndexStart = 1000;
			o.zIndexEnd = 100;
			o.colsPerLine = parseInt(o.width / o.item_width);
			//o.dropContainer = self.element.find(".dropContainer");
			//o.dragContainer = self.element.find(".dragContainer");
			o.mode = o.mode ? o.mode : "slide";
		
			$.data(this, "dropContainer", self.element.find(".dropContainer"));
			$.data(this, "dragContainer", self.element.find(".dragContainer"));


			o.rows = parseInt(self.data.dropContainer.find(".item").length / o.colsPerLine) + (self.data.dropContainer.find(".item").length % o.colsPerLine > 0 ? 1 : 0)
			o.height = o.item_height * o.rows;
			self.element.css("height", o.height + "px");
			
			o.hasEmptyFirst = self.data.dropContainer.find(".first").length;
			o.hasEmptyLast = self.data.dropContainer.find(".last").length;
			o.removeItems = [];

			this._initDroppable(self);

			$.each(self.data.dragContainer.find(".item"), function (i) {

				var index = o.hasEmptyFirst ? i + 1 : i;
				var top = parseInt(index / o.colsPerLine) * o.item_height;
				var left = index % o.colsPerLine * o.item_width;

				$(this).css({ "left": left + "px", "top": top + "px" });
				$(this).attr("data-index", index);
			})

			this._setEvents(self);

		},

		_initDroppable: function (self) {
			o = self.options;

			$.each(self.data.dropContainer.find(".item"), function (i) {
				var top = parseInt(i / o.colsPerLine) * o.item_height;
				var left = i % o.colsPerLine * o.item_width;

				$(this).css({ "left": left + "px", "top": top + "px" });
				$(this).attr("data-index", i);
			})
		},

		_setEvents: function (self) {
			o = self.options;

			if (o.onclick) {
				self.element.find(".draggable").unbind("click");
				self.element.find(".draggable").click(function () {
					if (o.drag) return;
					var src = $(this).find("img").attr("src");
					o.onclick(src);
				});
			}

			self.element.find(".draggable").draggable({
				start: function (e, ui) {
					clearTimeout($.data(self, "drag"));
					o.drag = true;
					o.dropped = false;
					$(this).css("z-index", o.zIndexStart)
					o.current = this;
					o.current_left = $(this).css("left");
					o.current_top = $(this).css("top");
					
				},
				stop: function (e, ui) {
					// click 이벤트를 방지하기 위함
					$.data(self, "drag", setTimeout(function () {
						o.drag = false;
					}, 10));

					$(this).css("z-index", o.zIndexEnd)
					if (!o.dropped) {
						$(this).css({ "top": o.current_top, "left": o.current_left });
					}
				}
			});

			self.element.find(".droppable").droppable({
				drop: function (e, ui) {

					$(this).removeClass("over");

					o.dropped = true;
					$(o.current).css({ "top": $(this).css("top"), "left": $(this).css("left") })
					
					//	대상과 현재움직이고 있는 element 의 data-index 를 변경

					if ($(this).hasClass("remove")) {
						
						o.removeItems.push($(o.current).clone());
						$(o.current).remove();
						$(self.data.dropContainer.find(".item")[o.hasEmptyFirst ? 1 : 0]).remove();

					} else {
						var droppable_data_index = parseInt($(this).attr("data-index"));
						var draggable_data_index = parseInt($(o.current).attr("data-index"));
						var direction = draggable_data_index < droppable_data_index ? "right" : "left";
						
						/*
						option1 : 서로 위치만 맞바꾸는 경우
						*/
						if (o.mode == "eachother") {
							var target_el = self.data.dragContainer.find(".item[data-index='" + droppable_data_index + "']");
							target_el.css({ "left": o.current_left, "top": o.current_top });
							target_el.attr("data-index", $(o.current).attr("data-index"));
							$(o.current).attr("data-index", droppable_data_index);
						} else {
							/* option2 : 범위안에 있는 아이템을 밀어주는 경우 */
							var targets = $.grep(self.data.dragContainer.find(".draggable"),
								function (obj) {
									if (direction == "right") {
										return parseInt($(obj).attr("data-index")) <= droppable_data_index && parseInt($(obj).attr("data-index")) > draggable_data_index
									} else {
										return parseInt($(obj).attr("data-index")) >= droppable_data_index && parseInt($(obj).attr("data-index")) < draggable_data_index
									}
								});
							
							$.each(targets, function () {
								var item = $(this);
								if (direction == "right") {
									item.attr("data-index", parseInt(item.attr("data-index")) - 1);
								} else {
									console.log(item);
									item.attr("data-index", parseInt(item.attr("data-index")) + 1);
								}
							})
							$(o.current).attr("data-index", droppable_data_index);
						}
					}

				//	if ($(this).hasClass("last") || $(this).hasClass("first") || $(this).hasClass("remove")) {
						self.reset(self);
				//	}

				},
				over: function (e, ui) {
					$(this).addClass("over");
				},
				out: function (e, ui) {
					$(this).removeClass("over");
				}
			});
		},
		// item 재정렬
		reset: function (self) {

			var o = self.options;
			var sortedList = [];

			o.rows = parseInt(self.data.dropContainer.find(".item").length / o.colsPerLine) + (self.data.dropContainer.find(".item").length % o.colsPerLine > 0 ? 1 : 0)
			o.height = o.item_height * o.rows;
			self.element.css("height", o.height + "px");

			// data-index 기준으로 재정렬
			self._initDroppable(self);

			for (i = 0 ; i < self.data.dropContainer.find(".item").length ; i++) {
				var item = self.data.dragContainer.find(".draggable[data-index='" + i + "']");
				if (item.hasClass("draggable") && item.length) {
				//	console.log(">>" + item.attr("data-index") + " : " + item.attr("data-name"));
					sortedList.push(item);
				}
			}

			$.each(sortedList, function (i) {

				var index = o.hasEmptyFirst ? (i + 1) : i;
				var item = $(this);
				var top = parseInt(index / o.colsPerLine) * o.item_height;
				var left = index % o.colsPerLine * o.item_width;

				item.css({ "left": left + "px", "top": top + "px" });
				item.attr("data-index", index);
				//console.log(item.attr("data-index") + " " + item.attr("data-name"));
			})

			return sortedList;
		},

		// 
		getElements: function () {
			var self = this, o = this.options;
			return this.reset(self);
		},

		getRemoveElements : function(){
			var self = this, o = this.options;
			return o.removeItems;
		} , 

		append: function (droppable, draggable) {

			var self = this, o = this.options;

			var draggable = $(draggable);
			draggable.attr("data-index", self.data.dragContainer.find(".draggable").length + (o.hasEmptyLast ? 1 : 0));
			$(droppable).insertBefore(self.data.dropContainer.find("div.new").length > 0 ? self.data.dropContainer.find("div.new") : self.data.dropContainer.find("div.remove"));
			self.data.dragContainer.append(draggable);

	//		this._initDroppable(self);

			this._setEvents(self);

			this.reset(self);

		}

	});

})(jQuery);