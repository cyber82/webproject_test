﻿var idValidation = {

	checkWithAlert: function (obj, min, max) {
		var result = idValidation.check(obj, min, max);

		if (!result.result) {
			alert(result.msg);
			obj1.focus();
			return false;
		}

		return true;
	},

	check: function (obj, min, max) {

		var val = obj.val();

		if (obj.val() == "") {
			obj.focus();
			return { result: false, msg: "아이디는 공백없이 입력해주세요." };
		}

		if (val.length < min || val.length > max) {
			return { result: false, msg: min + "~" + max + "자 이내의 영문 소문자,숫자 조합으로 입력해주세요" };
		}

		if ((mval = val.match(/([a-z0-9]+)/))) {
			if (mval[0].length != val.length) {
				return { result: false, msg: min + "~" + max + "자 이내의 영문 소문자,숫자 조합으로 입력해주세요" };
			}
		} else {
			return { result: false, msg: min + "~" + max + "자 이내의 영문 소문자,숫자 조합으로 입력해주세요" };
		}

		if (!/[0-9]+/.test(val)) {
			return { result: false, msg: min + "~" + max + "자 이내의 영문 소문자,숫자 조합으로 입력해주세요" };
		}

		if (!/[a-z]+/.test(val)) {
			return { result: false, msg: min + "~" + max + "자 이내의 영문 소문자,숫자 조합으로 입력해주세요" };
		}


		return { result: true, msg: "" };

	}

}
