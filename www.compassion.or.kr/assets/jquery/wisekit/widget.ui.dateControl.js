﻿
var dateControl = {

	init: function (obj, callback) {
		obj.keydown(function (e) {

			if (e.keyCode == 8) {
				$(this).val("");
				$(this).datepicker("hide");
			}
			return false;
		});

		obj.datepicker(
			{
				dateFormat: 'yy-mm-dd',
				timeFormat: '',
				numberOfMonths: 1,
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
				showTime: false,
				showHour: false,
				showMinute: false,
				closeText: '닫기',
				currentText: '오늘',
				showOn: 'both',
				buttonImageOnly: true,
				buttonImage: "/common/image/icon/icon_calendar.png",
				changeYear: true,
				onSelect: function (text, e) {

					$(this).datepicker("hide");

					if (callback) {
						callback();
					}
				}
			}
		);


	}

}

