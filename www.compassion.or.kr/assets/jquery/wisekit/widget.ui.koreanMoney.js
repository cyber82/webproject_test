﻿
(function ($) {

	$.widget("ui.koreanMoney", {

		_init: function () {
			var self = this, o = this.options;
			
			if (this.element.val() != "") {
				self._update(self);
			}

			this.element.bind("keyup , focus", function (e) {
				self._update(self);
			});

		},

		_update: function (self) {
			var o = self.options;

			var v = self.element.val().replace(/,/g, "")

			var val = v.match(/[0-9]+/g);
			if (v != "" && v != val) {
				alert("숫자만 입력가능합니다.");
				self.element.val("");
				o.label.html("");
				
			} else {
				if (v.length > 12) {
					o.label.html("");
					return;
				}

				val = self.convert(v);
				o.label.html(val);
			}
		} ,

		update : function(){
			this._update(this);
		} , 

		convert : function(val) {
			
			/* 
			fn 출처
			http://spoo79.tistory.com/6
			*/
			var index = 0;
			var i = 0;
			var result = "";
			var newResult = "";
			var money = val;
			su = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
			km = new Array("영", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구");
			danwi = new Array("", "십", "백", "천", "만", "십", "백", "천", "억", "십", "백", "천", "조");
			for (j = 1; j <= money.length; j++) {
				for (index = 0; index < 10; index++) {
					money = money.replace(su[index], km[index]);
				}
			}

			for (index = money.length; index > 0; index = index - 1) {
				result = money.substring(index - 1, index);
				if (result == "영") {
					if (i < 4 || i > 8) {
						result = "";

					} else if (i >= 4 && i < 8 && newResult.indexOf("만") < 0) {
						result = "만";

					} else if (i >= 8 && i < 12 && newResult.indexOf("억") < 0) {
						result = "억";
					}
				} else {
					result = result + danwi[i];
				}
				i++;
				newResult = result + newResult;
			}

			for (j = 1; j < newResult.length; j++) {
				newResult = newResult.replace("영", "");
			}

			if ((newResult.indexOf("만") - newResult.indexOf("억")) == 1)
				newResult = newResult.replace("만", "");
			if ((newResult.indexOf("억") - newResult.indexOf("조")) == 1)
				newResult = newResult.replace("억", "");

			return newResult + "원";
		}
	});


})(jQuery);



