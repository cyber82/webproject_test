﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title>
	<meta name="keywords" content="한국컴패션,compassion,어린이꽃이피었습니다,꽃들의이야기">
	<meta name="description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<meta property="og:title" content="한국컴패션">
	<meta property="og:url" content="http://www.compassion.or.kr/flower/">
	<meta property="og:image" content="images/logo.gif">
	<meta property="og:description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<link rel="shortcut icon" href="favicon.ico" >	
	<script src="common/js/jquery-1.10.2.min.js"></script>
	<script src="common/js/bx/jquery.bxslider.min.js"></script>
	<link rel="stylesheet" href="common/css/common.css">
	<link rel="stylesheet" href="common/js/bx/jquery.bxslider.css">
	<!--[if lt IE 9]>
		<script src="common/js/html5shiv.js"></script>
	<![endif]-->
</head>
<body>
<!--  LOG corp Web Analitics & Live Chat  START -->
<script  type="text/javascript">
//<![CDATA[
function logCorpAScript_full(){
	HTTP_MSN_MEMBER_NAME="";/*member name*/
	var prtc=(document.location.protocol=="https:")?"https://":"http://";
	var hst=prtc+"asp6.http.or.kr";
	var rnd="r"+(new Date().getTime()*Math.random()*9);
	this.ch=function(){
		if(document.getElementsByTagName("head")[0]){logCorpAnalysis_full.dls();}else{window.setTimeout(logCorpAnalysis_full.ch,30)}
	}
	this.dls=function(){
		var h=document.getElementsByTagName("head")[0];
		var s=document.createElement("script");s.type="text/jav"+"ascript";try{s.defer=true;}catch(e){};try{s.async=true;}catch(e){};
		if(h){s.src=hst+"/HTTP_MSN/UsrConfig/compassion/js/ASP_Conf.js?s="+rnd;h.appendChild(s);}
	}
	this.init= function(){
		document.write('<img src="'+hst+'/sr.gif?d='+rnd+'" style="width:1px;height:1px;position:absolute;display:none" onload="logCorpAnalysis_full.ch()" alt="" />');
	}
}
if(typeof logCorpAnalysis_full=="undefined"){var logCorpAnalysis_full=new logCorpAScript_full();logCorpAnalysis_full.init();}
//]]>
</script>
<noscript><img src="http://asp6.http.or.kr/HTTP_MSN/Messenger/Noscript.php?key=compassion" style="display:none;width:0;height:0;" alt="" /></noscript>
<!-- LOG corp Web Analitics & Live Chat END -->
<!--NSmart Track Tag Script-->
        <script type='text/javascript'>
            callbackFn = function() {};
            var _nsmart = _nsmart || [];
            _nsmart.host = _nsmart.host || (('https:' == document.location.protocol) ? 'https://' : 'http://');
            _nsmart.push([12490, 32060]); // 캠페인 번호와 페이지 번호를 배열 객체로 전달
            //document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            // document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            /* v 1.4 ---------------------------------*
            *  Nasmedia Track Object, version 1.4
            *  (c) 2009 nasmedia,mizcom (2009-12-24)
            *---------------------------------------*/
            var nasmedia = {};
            var NTrackObj = null;
            nasmedia.track = function(){
                this.camp, this.img = null;
            };
            nasmedia.track.prototype = {
                callTrackTag : function(p, f, c){
                    if(Object.prototype.toString.call(_nsmart) != '[object Array]' || _nsmart.host == undefined || _nsmart.host == '') return false;
                    if(f != undefined && typeof(f) == 'function') {
                        this.img.onload = this.img.onerror = this.img.onabort = f;
                        var no = ((parseInt(c, 10) % 100) < 10 ? '0' + (parseInt(c, 10) % 100): (parseInt(c, 10) % 100));
                        this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + c + '&page=' + p + '&r=' + Math.random();
                    }
                    else this.callNext(0);
                    return false;
                },
                callNext : function(i) {
                    if(i == this.camp.length) return false;
                    this.img.onload = this.img.onerror = this.img.onabort = function(o, i){return function(){o.callNext(i);};}(this, i+1);
                    var no = ((parseInt(this.camp[i][0], 10) % 100) < 10 ? '0' + (parseInt(this.camp[i][0], 10) % 100): (parseInt(this.camp[i][0], 10) % 100));
                    this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + this.camp[i][0] + '&page=' + this.camp[i][1] + '&r=' + Math.random();
                },
                init : function(c) {
                    this.camp = c, this.img = new Image;
                }
            };
            (function(){
                NTrackObj = new nasmedia.track();
            })();
            NTrackObj.init(_nsmart);
        NTrackObj.callTrackTag();
        </script>
        <!--NSmart Track Tag Script End..-->

<div id="wrap" class="lighten">
	<!-- header -->
	<header id="header" class="header">
		<div class="row">
			<h1 class="logo">
                <script>
                    function getHost() {
                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
                    else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
                    return host;
                }

                function goLink(param) {
                    var host = getHost();
                    if (host != '') location.href = 'http://' + host + param ;
                }
                </script>
				<a href="javascript:goLink('');" onclick="javascript:NTrackObj.callTrackTag('32061', callbackFn, 12490);">
					<img src="images/logo.gif" alt="Compassion">
				</a>
			</h1>
			<ul class="gnb">
				<li><a href="/flower/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>"><span>꽃들의 이야기</span></a></li>
				<li><a href="people.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>"><span>꽃 피우는 사람들</span></a></li>
				<li><a href="donation.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>"><span>꽃으로 전하는 행복</span></a></li>
			</ul>
			<ul class="snb">
				<li><a class="sprite snb__item snb__item--fb" href="https://www.facebook.com/compassion.Korea/" target="_blank">컴패션 페이스북</a></li>
				<li><a class="sprite snb__item snb__item--in" href="https://www.instagram.com/compassionkorea/" target="_blank">컴패션 인스타그램</a></li>
				<li><a class="sprite snb__item snb__item--na" href="http://happylog.naver.com/compassion.do" target="_blank">컴패션 네이버</a></li>
			</ul>
			<a class="btn btn-top" href="#" style="display:block;">
				<img src="images/btn_top.png" alt="TOP">
			</a>
		</div>
	</header>
	<!-- //header -->
	<div id="container" class="fixLeft">
		<a href="http://www.compassion.or.kr/sponsor/children/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_WEB_direct&utm_source=WEB_direct&utm_campaign=2017flower​&utm_medium=WEB" %>&#anchor_childlist" class="btn-maindonation" onclick="javascript:NTrackObj.callTrackTag('32062', callbackFn, 12490);">
			<img src="images/btn_maindonation.png" alt="1:1어린이 후원하기">
		</a>
		<section class="content content-people">
			<!-- 사람들 비주얼 -->
			<article class="cp-intro">
				메마른 땅에서도 피어나는 아름다운 꽃처럼 존재 자체만으로 존귀하고
				사랑받아 마땅한  어린이들이 꽃처럼 활짝 피어날 수 있도록 모인 사람들이 있습니다.
				카네이션   꽃 한 송은이  꽃범수
				이들과 함께하는 어린이 꽃피우기, 시작합니다!
			</article>
			<!-- 션	 -->
			<article class="cp-sean">
				<div class="cp-sean__content">
					<div class="cp-sean__video">
						<%--<img src="images/img_seanvideo.jpg" alt="1일 플로리스트 션 정혜영 부부와의 만남을 공개합니다.">--%>
                        <iframe width="614" height="342" src="https://www.youtube.com/embed/c5-AwGNv3sY" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</article>
			<!-- 게스트	 -->
			<article class="cp-guest">
				<div class="cp-guest__content">
					<div class="cp-guest__top">
						어린이 꽃을 피우기 위해 
						꽃다발 라이더 송은이, 김범수가 당신을 만나러 갑니다!
					</div>
					<div class="cp-guest__video">
						<div class="cp-guest__videotit">
							· 김범수가 꽃을 들고 교실에 찾아간 이유는? ·
						</div>
						<div class="cp-guest__videowrap">
							<div class="l">
								<div class="b">
									<img src="images/img_big.jpg" alt="김범수">
								</div>
							</div>
							<div class="r">
								<div class="i">
									<a href="#none" class="p p1" data-video='<iframe src="https://www.youtube.com/embed/SZwUGe6iAZU" frameborder="0" allowfullscreen></iframe>'>
										<img src="images/img_people1.jpg" alt="김범수">
									</a>
								</div>
								<div class="i">
									<a href="#none" class="p p2" data-video='<iframe src="https://www.youtube.com/embed/3OxXtheMTIY" frameborder="0" allowfullscreen></iframe>'>
										<img src="images/img_people2.jpg" alt="송은이" >
									</a>
								</div>
							</div>
						</div>
						<div class="cp-guest__btn">
							<a href="donation.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">
								<img src="images/btn_guest.png" alt="자세히보기">
							</a>
						</div>
					</div>
				</div>
			</article>
			<!-- 콘서트 -->
			<article class="cp-concert">
				<img src="images/img_concert.jpg" alt="어린이 꽃 피우기 좋은 한 여름 밤에 찾아갑니다! 문의 9:00am~18:00pm  |  02-740-1000">
				<!-- 꽃서트 링크버튼 -->
				<div class="cp-concert__btn">
					<a href="http://www.compassion.or.kr/participation/event/view/50?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flowercert_WEB_direct&utm_source=WEB_direct&utm_campaign=2017flower​cert&utm_medium=WEB" %>">
						<img src="images/btn_peopledetail.png" alt="꽃서트 자세히 보기">
					</a>
				</div>
			</article>
			<!-- 후원 -->
			<article class="cp-donation">
				<div class="cp-donation__content">
					<div class="table">
						<div class="cell">
							<img src="images/tit_peopledonation.png" alt="어린이 꽃에게 당신의 손길을 내어 주세요"><br><br>
							<a href="http://www.compassion.or.kr/sponsor/children/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_WEB_direct&utm_source=WEB_direct&utm_campaign=2017flower​&utm_medium=WEB" %>&#anchor_childlist" target="_blank" onclick="javascript:NTrackObj.callTrackTag('32043', callbackFn, 12490);">
								<img src="images/btn_peopledonation.png" alt="어린이 후원하기">
							</a>
						</div>
					</div>
				</div>
			</article>
		</section>
		<script>
		$(function () {
			// $('.cp-sean__video img').click(function () {
			// 	$('.cp-sean__video').html('<iframe width="614" height="342" src="https://www.youtube.com/embed/got7vAV1jgg?autoplay=1" frameborder="0" allowfullscreen></iframe>');
			// 	$(this).fadeOut();
			// })
			$('[data-video]').click(function (e) {				
				var con = $(this).attr('data-video');
				var tit = $(this).attr('data-tit');
				$('.cp-guest__videowrap .b').html(con);
				$('.cp-guest__videotit').html(tit);
				$(this).addClass('active');
				$('[data-video]').not($(this)).removeClass('active');				
				$('.p2').removeClass('active');
				e.preventDefault();
			})
			$('.p2').click(function () {
				$(this).addClass('active');
				$('[data-video]').not($(this)).removeClass('active');
			})
		});
		</script>
	</div>
	<!-- footer -->
	<footer id="footer" class="footer">
		<div class="row">
			<div class="footer__logo">
				<img src="images/logo_footer.png" alt="Compassion">
			</div>
			<div class="footer__info">
				<div class="footer__addr">
					한국컴패션 사업자등록번호 : 108-82-05789     |    대표자 : 서정인<br>
					(04418) 서울시 용산구 한남대로 102-5 (한남동 723-41) 석전빌딩<br>
					후원상담/안내 : 02-740-1000(평일 09:00 ~ 18:00)팩스 : 02-740-1001이메일 : info@compassion.or.kr
				</div>
				<div class="footer__copyright">
					© COMPASSION KOREA All Rights Reserved. Contact us for more information					
				</div>
			</div>
		</div>
	</footer>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '235171356890594'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=235171356890594&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

	<!-- //footer -->
</div>
</body>
</html>