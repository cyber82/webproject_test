﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title>
	<meta name="keywords" content="한국컴패션,compassion,어린이꽃이피었습니다,꽃들의이야기">
	<meta name="description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<meta property="og:title" content="한국컴패션">
	<meta property="og:url" content="http://www.compassion.or.kr/flower/">
	<meta property="og:image" content="images/logo.gif">
	<meta property="og:description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<link rel="shortcut icon" href="favicon.ico" >	
	<script src="common/js/jquery-1.10.2.min.js"></script>
	<script src="common/js/bx/jquery.bxslider.min.js"></script>
	<link rel="stylesheet" href="common/css/common.css">
	<link rel="stylesheet" href="common/js/bx/jquery.bxslider.css">
	<!--[if lt IE 9]>
		<script src="common/js/html5shiv.js"></script>
	<![endif]-->
</head>
<body>
<!--  LOG corp Web Analitics & Live Chat  START -->
<script  type="text/javascript">
//<![CDATA[
function logCorpAScript_full(){
	HTTP_MSN_MEMBER_NAME="";/*member name*/
	var prtc=(document.location.protocol=="https:")?"https://":"http://";
	var hst=prtc+"asp6.http.or.kr";
	var rnd="r"+(new Date().getTime()*Math.random()*9);
	this.ch=function(){
		if(document.getElementsByTagName("head")[0]){logCorpAnalysis_full.dls();}else{window.setTimeout(logCorpAnalysis_full.ch,30)}
	}
	this.dls=function(){
		var h=document.getElementsByTagName("head")[0];
		var s=document.createElement("script");s.type="text/jav"+"ascript";try{s.defer=true;}catch(e){};try{s.async=true;}catch(e){};
		if(h){s.src=hst+"/HTTP_MSN/UsrConfig/compassion/js/ASP_Conf.js?s="+rnd;h.appendChild(s);}
	}
	this.init= function(){
		document.write('<img src="'+hst+'/sr.gif?d='+rnd+'" style="width:1px;height:1px;position:absolute;display:none" onload="logCorpAnalysis_full.ch()" alt="" />');
	}
}
if(typeof logCorpAnalysis_full=="undefined"){var logCorpAnalysis_full=new logCorpAScript_full();logCorpAnalysis_full.init();}
//]]>
</script>
<noscript><img src="http://asp6.http.or.kr/HTTP_MSN/Messenger/Noscript.php?key=compassion" style="display:none;width:0;height:0;" alt="" /></noscript>
<!-- LOG corp Web Analitics & Live Chat END -->
<!--NSmart Track Tag Script-->
        <script type='text/javascript'>
            callbackFn = function() {};
            var _nsmart = _nsmart || [];
            _nsmart.host = _nsmart.host || (('https:' == document.location.protocol) ? 'https://' : 'http://');
            _nsmart.push([12490, 32063]); // 캠페인 번호와 페이지 번호를 배열 객체로 전달
            //document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            // document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            /* v 1.4 ---------------------------------*
            *  Nasmedia Track Object, version 1.4
            *  (c) 2009 nasmedia,mizcom (2009-12-24)
            *---------------------------------------*/
            var nasmedia = {};
            var NTrackObj = null;
            nasmedia.track = function(){
                this.camp, this.img = null;
            };
            nasmedia.track.prototype = {
                callTrackTag : function(p, f, c){
                    if(Object.prototype.toString.call(_nsmart) != '[object Array]' || _nsmart.host == undefined || _nsmart.host == '') return false;
                    if(f != undefined && typeof(f) == 'function') {
                        this.img.onload = this.img.onerror = this.img.onabort = f;
                        var no = ((parseInt(c, 10) % 100) < 10 ? '0' + (parseInt(c, 10) % 100): (parseInt(c, 10) % 100));
                        this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + c + '&page=' + p + '&r=' + Math.random();
                    }
                    else this.callNext(0);
                    return false;
                },
                callNext : function(i) {
                    if(i == this.camp.length) return false;
                    this.img.onload = this.img.onerror = this.img.onabort = function(o, i){return function(){o.callNext(i);};}(this, i+1);
                    var no = ((parseInt(this.camp[i][0], 10) % 100) < 10 ? '0' + (parseInt(this.camp[i][0], 10) % 100): (parseInt(this.camp[i][0], 10) % 100));
                    this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + this.camp[i][0] + '&page=' + this.camp[i][1] + '&r=' + Math.random();
                },
                init : function(c) {
                    this.camp = c, this.img = new Image;
                }
            };
            (function(){
                NTrackObj = new nasmedia.track();
            })();
            NTrackObj.init(_nsmart);
        NTrackObj.callTrackTag();
        </script>
        <!--NSmart Track Tag Script End..-->


<div id="wrap" class="lighten">
	<!-- header -->
	<header id="header" class="header">
		<div class="row">
			<h1 class="logo">
                <script>
                    function getHost() {
                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
                    else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
                    return host;
                }

                function goLink(param) {
                    var host = getHost();
                    if (host != '') location.href = 'http://' + host + param ;
                }
                </script>
				<a href="javascript:goLink('');" onclick="javascript:NTrackObj.callTrackTag('32064', callbackFn, 12490);">
					<img src="images/logo.gif" alt="Compassion">
				</a>
			</h1>
			<ul class="gnb">
				<li><a href="/flower/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>"><span>꽃들의 이야기</span></a></li>
				<li><a href="people.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>"><span>꽃 피우는 사람들</span></a></li>
				<li><a href="donation.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>"><span>꽃으로 전하는 행복</span></a></li>
			</ul>
			<ul class="snb">
				<li><a class="sprite snb__item snb__item--fb" href="https://www.facebook.com/compassion.Korea/" target="_blank">컴패션 페이스북</a></li>
				<li><a class="sprite snb__item snb__item--in" href="https://www.instagram.com/compassionkorea/" target="_blank">컴패션 인스타그램</a></li>
				<li><a class="sprite snb__item snb__item--na" href="http://happylog.naver.com/compassion.do" target="_blank">컴패션 네이버</a></li>
			</ul>
			<a class="btn btn-top" href="#" style="display:block;">
				<img src="images/btn_top.png" alt="TOP">
			</a>
		</div>
	</header>
	<!-- //header -->
	<div id="container" class="fixLeft"> 
        <a href="javascript:goLink('/sponsor/children/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_WEB_direct&utm_source=WEB_direct&utm_campaign=2017flower&utm_medium=WEB" %>&#anchor_childlist');" class="btn-maindonation" onclick="javascript:NTrackObj.callTrackTag('32065', callbackFn, 12490);"> 
        <img src="images/btn_maindonation.png" alt="1:1어린이 후원하기"> 
        </a> 
        <section class="content content-donation">
			<!-- 후원하기 비주얼 -->
			<article class="cd-intro">
				<div class="cd-intro__wrap">
					<div class="cd-intro__meta">
						<h2 class="cd-intro__tit">
							<img src="images/tit_donation.png" alt="행복이 피어나길">
						</h2>
						<p class="cd-intro__desc">
							플라워 서브스크립션 브랜드 모이 <span class="sprite"></span>에서<br>
							한국 컴패션 &lt;어린이 꽃이 피었습니다&gt; 캠페인을 응원합니다.
						</p>
					</div>
				</div>
			</article>
			<!-- //후원하기 비주얼 -->
			<!-- 후원하기 정보 -->
			<article class="cd-info">
				<div class="cd-info__present">
					<img src="images/img_donationinfo.png" alt="후원을 시작하신 모든 분들께 Blooming wisher box를 드립니다. 후원신청시 이 캠페인을 통해 후원을 시작하셨다고 체크하신 분들 해당/2017년 6월 30일 ~ 8월 31일/후원 감사 꽃다발,엽서카드/후원 완료 후, 2주 이내 배송 예정">
					<a href="" data-open="#layer-form" onclick="javascript:NTrackObj.callTrackTag('32070', callbackFn, 12490);">
						<img src="images/btn_story.png" alt="나만의 사연 응모">
					</a>
				</div>
				<div class="cd-info__find">
					<img src="images/img_donationfind.png" alt="어린이 후원을 결심하게 된 이유가 무엇인가요? 사연을 남겨주신 분들 중 추첨을 통해  송은이, 김범수가 직접 꽃다발을 배달해드립니다. 사연응모 클릭->개인정보 입력후 후원 결심 이유 남기기 ->당첨자 개별연락">
					<a href="http://www.compassion.or.kr/sponsor/children/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_WEB_direct&utm_source=WEB_direct&utm_campaign=2017flower​&utm_medium=WEB" %>&#anchor_childlist" onclick="javascript:NTrackObj.callTrackTag('32035', callbackFn, 12490);" >
						<img src="images/btn_donation1.png" alt="1:1어린이 후원하기">
					</a>
				</div>
				<!-- 나만의 사연응모 레이어 -->
				<div id="layer-form" class="layer layer-form">
					<div class="layer-wrap">
						<div class="layer-header">
							사연을 남기면 송은이, 김범수가 간다!
						</div>
						<div class="layer-body">					
						</div>
						<button class="btn btn-layerclose" data-close='#layer-form'>X</button>
					</div>
				</div>
				<!-- //나만의 사연응모 레이어 -->				
			</article>
			<!-- //후원하기 정보 -->
		</section>
		<script>
		$(function () {
			$('[data-open]').bind('click',function (e) {
				var target = $(this).attr('data-open');
				$(target).find('.layer-body').html('<iframe src="https://goo.gl/forms/Z3nQdG8aceMKJAvg2" frameborder="0"></iframe>');
				$(target).fadeIn();
				e.preventDefault();
			})
			$('[data-close]').bind('click',function (e) {
				var target = $(this).attr('data-close');				
				$(target).fadeOut();
				$(target).find('.layer-body').empty();
			});
		});
		</script>
	</div>
	<!-- footer -->
	<footer id="footer" class="footer">
		<div class="row">
			<div class="footer__logo">
				<img src="images/logo_footer.png" alt="Compassion">
			</div>
			<div class="footer__info">
				<div class="footer__addr">
					한국컴패션 사업자등록번호 : 108-82-05789     |    대표자 : 서정인<br>
					(04418) 서울시 용산구 한남대로 102-5 (한남동 723-41) 석전빌딩<br>
					후원상담/안내 : 02-740-1000(평일 09:00 ~ 18:00)팩스 : 02-740-1001이메일 : info@compassion.or.kr
				</div>
				<div class="footer__copyright">
					© COMPASSION KOREA All Rights Reserved. Contact us for more information					
				</div>
			</div>
		</div>
	</footer>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '235171356890594'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=235171356890594&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


	<!-- //footer -->
</div>
</body>
</html>