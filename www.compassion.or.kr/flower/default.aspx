﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title>
	<meta name="keywords" content="한국컴패션,compassion,어린이꽃이피었습니다,꽃들의이야기">
	<meta name="description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<meta property="og:title" content="한국컴패션">
	<meta property="og:url" content="http://www.compassion.or.kr/flower/">
	<meta property="og:image" content="images/logo.gif">
	<meta property="og:description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<link rel="shortcut icon" href="favicon.ico" >	
	<script src="common/js/jquery-1.10.2.min.js"></script>
	<script src="common/js/bx/jquery.bxslider.min.js"></script>
    
	<script src="common/js/jquery.easing.1.3.min.js"></script>
	<link rel="stylesheet" href="common/css/common.css">
	<link rel="stylesheet" href="common/js/bx/jquery.bxslider.css">
	<!--[if lt IE 9]>
		<script src="common/js/html5shiv.js"></script>
	<![endif]-->
<script type="text/javascript">
var tmpUser = navigator.userAgent;
if (tmpUser.indexOf("iPhone") > 0 || tmpUser.indexOf("iPod") > 0 || tmpUser.indexOf("Android ") > 0 )
{
location.href = "http://m.compassion.or.kr/flower/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>";
}
</script>
<!--NSmart Track Tag Script-->
        <script type='text/javascript'>
            callbackFn = function() {};
            var _nsmart = _nsmart || [];
            _nsmart.host = _nsmart.host || (('https:' == document.location.protocol) ? 'https://' : 'http://');
            _nsmart.push([12490, 32029]); // 캠페인 번호와 페이지 번호를 배열 객체로 전달
            //document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            // document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            /* v 1.4 ---------------------------------*
            *  Nasmedia Track Object, version 1.4
            *  (c) 2009 nasmedia,mizcom (2009-12-24)
            *---------------------------------------*/
            var nasmedia = {};
            var NTrackObj = null;
            nasmedia.track = function(){
                this.camp, this.img = null;
            };
            nasmedia.track.prototype = {
                callTrackTag : function(p, f, c){
                    if(Object.prototype.toString.call(_nsmart) != '[object Array]' || _nsmart.host == undefined || _nsmart.host == '') return false;
                    if(f != undefined && typeof(f) == 'function') {
                        this.img.onload = this.img.onerror = this.img.onabort = f;
                        var no = ((parseInt(c, 10) % 100) < 10 ? '0' + (parseInt(c, 10) % 100): (parseInt(c, 10) % 100));
                        this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + c + '&page=' + p + '&r=' + Math.random();
                    }
                    else this.callNext(0);
                    return false;
                },
                callNext : function(i) {
                    if(i == this.camp.length) return false;
                    this.img.onload = this.img.onerror = this.img.onabort = function(o, i){return function(){o.callNext(i);};}(this, i+1);
                    var no = ((parseInt(this.camp[i][0], 10) % 100) < 10 ? '0' + (parseInt(this.camp[i][0], 10) % 100): (parseInt(this.camp[i][0], 10) % 100));
                    this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + this.camp[i][0] + '&page=' + this.camp[i][1] + '&r=' + Math.random();
                },
                init : function(c) {
                    this.camp = c, this.img = new Image;
                }
            };
            (function(){
                NTrackObj = new nasmedia.track();
            })();
            NTrackObj.init(_nsmart);
        NTrackObj.callTrackTag();
        </script>
        <!--NSmart Track Tag Script End..-->

</head>
<body>
<!--  LOG corp Web Analitics & Live Chat  START -->
<script  type="text/javascript">
//<![CDATA[
function logCorpAScript_full(){
	HTTP_MSN_MEMBER_NAME="";/*member name*/
	var prtc=(document.location.protocol=="https:")?"https://":"http://";
	var hst=prtc+"asp6.http.or.kr";
	var rnd="r"+(new Date().getTime()*Math.random()*9);
	this.ch=function(){
		if(document.getElementsByTagName("head")[0]){logCorpAnalysis_full.dls();}else{window.setTimeout(logCorpAnalysis_full.ch,30)}
	}
	this.dls=function(){
		var h=document.getElementsByTagName("head")[0];
		var s=document.createElement("script");s.type="text/jav"+"ascript";try{s.defer=true;}catch(e){};try{s.async=true;}catch(e){};
		if(h){s.src=hst+"/HTTP_MSN/UsrConfig/compassion/js/ASP_Conf.js?s="+rnd;h.appendChild(s);}
	}
	this.init= function(){
		document.write('<img src="'+hst+'/sr.gif?d='+rnd+'" style="width:1px;height:1px;position:absolute;display:none" onload="logCorpAnalysis_full.ch()" alt="" />');
	}
}
if(typeof logCorpAnalysis_full=="undefined"){var logCorpAnalysis_full=new logCorpAScript_full();logCorpAnalysis_full.init();}
//]]>
</script>
<noscript><img src="http://asp6.http.or.kr/HTTP_MSN/Messenger/Noscript.php?key=compassion" style="display:none;width:0;height:0;" alt="" /></noscript>
<!-- LOG corp Web Analitics & Live Chat END -->
<div id="wrap">
	<!-- header -->
	<header id="header" class="header">
		<div class="row">
			<h1 class="logo">
                <script>
                    function getHost() {
                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
                    else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
                    return host;
                }

                function goLink(param) {
                    var host = getHost();
                    if (host != '') location.href = 'http://' + host + param ;
                }
                </script>
				<a href="javascript:goLink('')" onclick="javascript:NTrackObj.callTrackTag('32030', callbackFn, 12490);">
					<img src="images/logo.gif" alt="Compassion">
				</a>
			</h1>
			<ul class="gnb">
				<li><a href="/flower/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>"><span>꽃들의 이야기</span></a></li>
				<li><a href="people.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>"><span>꽃 피우는 사람들</span></a></li>
				<li><a href="donation.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>"><span>꽃으로 전하는 행복</span></a></li>
			</ul>
			<ul class="snb">
				<li><a class="sprite snb__item snb__item--fb" href="https://www.facebook.com/compassion.Korea/" target="_blank">컴패션 페이스북</a></li>
				<li><a class="sprite snb__item snb__item--in" href="https://www.instagram.com/compassionkorea/" target="_blank">컴패션 인스타그램</a></li>
				<li><a class="sprite snb__item snb__item--na" href="http://happylog.naver.com/compassion.do" target="_blank">컴패션 네이버</a></li>
			</ul>
			<a class="btn btn-top" href="#">
				<img src="images/btn_top.png" alt="TOP">
			</a>
			<div class="lnb">
				<a href="#container" class="active">메인 비주얼</a>
				<a href="#main-story1">스토리1</a>
				<a href="#main-story2">스토리2</a>
				<a href="#main-story3">스토리3</a>
			</div>
		</div>
	</header>
	<!-- //header -->
	<div id="container">
        <!-- main video -->
		<section class="main-video" id="main-video">
			<div class="main-video__wrap">
				<div class="main-video__top">
					<h2 class="main-video__tit">
						
					</h2>
					<a class="main-video__close" href="#main-video">
						<img src="images/btn_bannerclose.png" alt="메인 비디오 닫기">
					</a>
				</div>
				<div class="main-video__frame">
					<iframe width="700" height="394" src="https://www.youtube.com/embed/3OxXtheMTIY?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="main-video__btn">
<a href="javascript:goLink('/sponsor/children/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_WEB_direct&utm_source=WEB_direct&utm_campaign=2017flower​&utm_medium=WEB" %>&#anchor_childlist');"><img src="images/btn_donation.jpg"></a>
					<!--a href="donation.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">
						<img src="images/btn_popup1.png">
					</a>
					<a href="people.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">
						<img src="images/btn_popup2.png">
					</a-->
				</div>
			</div>
		</section>
		<!-- main intro -->
		<section class="main-intro">
			<div class="main-intro__videowrap active">
				<video class="main-intro__video"  autoplay preload="none" poster="images/img_videoplay1.jpg">
					<source src="images/video_main1.mp4" type="video/mp4">
					<img src="images/img_videono1.jpg">
				</video>
				<div class="main-intro__videodesc">
					희망이 없는 메마른 땅에도 어린이 꽃은 피어납니다.<br>
					연약하지만 너무나도 소중하고 눈부신 생명인 어린이 꽃에게 <br>
					당신의 손길을 내어주세요. 
					<div class="main-intro__btn">
						<a href="#" class="sprite btn btn-scrolldown">Scroll Down</a>
					</div>
				</div>
			</div>				
			<div class="main-intro__videowrap">
				<video  class="main-intro__video"  preload="none" poster="images/img_videoplay2.jpg">
					<source src="images/video_main2.mp4" type="video/mp4">
					<img src="images/img_videono2.jpg">
				</video>
				<div class="main-intro__videodesc">
					희망이 없는 메마른 땅에도 어린이 꽃은 피어납니다.<br>
					연약하지만 너무나도 소중하고 눈부신 생명인 어린이 꽃에게 <br>
					당신의 손길을 내어주세요. 
					<div class="main-intro__btn">
						<a href="#" class="sprite btn btn-scrolldown">Scroll Down</a>
					</div>
				</div>
			</div>
			<div class="main-intro__videowrap">
				<video class="main-intro__video"  preload="none" poster="images/img_videoplay3.jpg">
					<source src="images/video_main3.mp4" type="video/mp4">
					<img src="images/img_videono3.jpg">
				</video>
				<div class="main-intro__videodesc">
					희망이 없는 메마른 땅에도 어린이 꽃은 피어납니다.<br>
					연약하지만 너무나도 소중하고 눈부신 생명인 어린이 꽃에게 <br>
					당신의 손길을 내어주세요. 
					<div class="main-intro__btn">
						<a href="#" class="sprite btn btn-scrolldown">Scroll Down</a>
					</div>
				</div>
			</div>				
			<div class="main-intro__videowrap">
				<video class="main-intro__video"  preload="none" poster="images/img_videoplay4.jpg">
					<source src="images/video_main4.mp4" type="video/mp4">
					<img src="images/img_videono4.jpg">
				</video>
				<div class="main-intro__videodesc">
					희망이 없는 메마른 땅에도 어린이 꽃은 피어납니다.<br>
					연약하지만 너무나도 소중하고 눈부신 생명인 어린이 꽃에게 <br>
					당신의 손길을 내어주세요. 
					<div class="main-intro__btn">
						<a href="#" class="sprite btn btn-scrolldown">Scroll Down</a>
					</div>
				</div>
			</div>
			<a href="javascript:goLink('/sponsor/children/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_WEB_direct&utm_source=WEB_direct&utm_campaign=2017flower​&utm_medium=WEB" %>&#anchor_childlist');" class="btn-maindonation" onclick="javascript:NTrackObj.callTrackTag('32031', callbackFn, 12490);">
				<img src="images/btn_maindonation.png" alt="1:1어린이 후원하기">
			</a>
            <%--<div class="main-banner" style="display:block;">
				<a href="javascript:goLink('/flower/donation?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "&" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>');">
                   <img src="images/img_banner.jpg" alt="사연을 남기면 송은이, 김범수가 당신을 만나러 갑니다!">
                </a>
				<div class="main-banner__close">
					<img src="images/btn_bannerclose.png" alt="">
				</div>
				<div class="main-banner__no" onclick="todaycloseWin();">
					오늘 하루 그만보기
				</div>

			</div>--%>
		</section>
		<!-- //main intro -->
		<!-- main story 1 -->
		<!-- [D] 스토리 컬러에 따라 클래스 추가 -->
		<section class="main-story main-story--red" id="main-story1">
			<div class="main-story__inner">
				<div class="main-story__item main-story__item--01" data-show="1">
					<img src="images/img01.png" alt="벨린과 렐린 - 태어나 제일 먼저 본 것은 구멍 난 천장에서 스며 들어오는 가느다란 햇빛뿐이었어요">
				</div>
				<div class="main-story__item main-story__item--02" data-show="2">
					<img src="images/img02.png" alt="홍수가 나고, 마을 전체가 산산조각이 났어요">
				</div>
				<div class="main-story__item main-story__item--03" data-show="3">
					<img src="images/img03.png" alt="저희 가족에게 희망이 찾아왔어요">
				</div>
				<img src="images/img_line.png" alt="라인" class="main-story__line main-story__line--red" data-line="1">
			</div>
		</section>
		<!-- //main story 1 -->
		<!-- main bg -->
		<section class="main-slogan main-slogan--red" data-bg="1">
			<div class="main-slogan__desc">
				<div class="table">
					<div class="cell">
						<img src="images/desc_slogan1.png" alt="저희와 같이 희망을 찾는 소녀들에게 후원자님의 손길을 내어주세요.">
					</div>
				</div>
			</div>
		</section>
		<!-- //main bg -->
		<!-- main donation -->
		<section class="main-donation main-donation--red">
			<h2 class="main-donation__tit">
				<img src="images/tit_spon1.png" alt="1:1 어린이 후원하기 , 키움과 돌봄의 감동적인 1:1만남, 지금 시작하세요">
			</h2>
			<!-- [D] 첫번째 후원 리스트 들어갈 곳 -->
			<div class="row">
				<iframe id="ifrm1" src="" frameborder="0" class="main-donation__iframe"></iframe>
                <script>
                    var host = getHost();
                    $('#ifrm1').attr('src', 'http://'+host+'/flower/list1?gender=F&minAge=3&maxAge=10&<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>');
                </script>
            </div>
		</section>
		<!-- //main donation -->
		<!-- main story 2 -->
		<!-- [D] 스토리 컬러에 따라 클래스 추가 -->
		<section class="main-story main-story--blue"  id="main-story2">
			<div class="main-story__inner">
				<div class="main-story__item main-story__item--04" data-show="4">
					<img src="images/img04.png" alt="9살의 렌스 - 몇 년을 집에서 누워만 있었어요">
				</div>
				<div class="main-story__item main-story__item--05" data-show="5">
					<img src="images/img05.png" alt="1년 후, 저는 건강하고 통통한 9살이 되었답니다 ">
				</div>
				<div class="main-story__item main-story__item--06" data-show="6">
					<img src="images/img06.png" alt="아픈 친구들을 도와주는 의사선생님 이 될 거에요">
				</div>
				<img src="images/img_line2.png" alt="라인" class="main-story__line main-story__line--blue" data-line="2">
			</div>
		</section>
		<!-- //main story 2 -->
		<!-- main bg -->
		<section class="main-slogan main-slogan--blue" data-bg="2">
			<div class="main-slogan__desc">
				<div class="table">
					<div class="cell">
						<img src="images/desc_slogan2.png" alt="제 친구들도 후원자님을 만나 꿈을 키워갈 수 있었으면 좋겠어요!">
					</div>
				</div>
			</div>
		</section>
		<!-- //main bg -->
		<!-- main donation2 -->
		<section class="main-donation main-donation--blue">
			<h2 class="main-donation__tit">
				<img src="images/tit_spon2.png" alt="1:1 어린이 후원하기 , 키움과 돌봄의 감동적인 1:1만남, 지금 시작하세요">
			</h2>
			<!-- [D] 두번째 후원 리스트 들어갈 곳 -->
			<div class="row">
				<iframe id="ifrm2" src="" frameborder="0" class="main-donation__iframe"></iframe>
                <script>
                    var host = getHost();
                    $('#ifrm2').attr('src', 'http://'+host+'/flower/list2?gender=M&minAge=3&maxAge=10&<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>');
                </script>
			</div>
		</section>
		<!-- //main donation2 -->
		<!-- main story 3 -->
		<!-- [D] 스토리 컬러에 따라 클래스 추가 -->
		<section class="main-story main-story--yellow" >
			<div class="main-story__inner" id="main-story3">
				<div class="main-story__item main-story__item--07" data-show="7">
					<img src="images/img07.png" alt="육상선수가 된 소녀, 베사">
				</div>
				<div class="main-story__item main-story__item--08" data-show="8">
					<img src="images/img08.png" alt="2살엄마는 저를 떠났어요">
				</div>
				<div class="main-story__item main-story__item--09" data-show="9">
					<img src="images/img09.png" alt="저의 재능을 알게 되었어요!">
				</div>
				<img src="images/img_line3.png" alt="라인" class="main-story__line main-story__line--yellow" data-line="3">
			</div>
		</section>
		<!-- //main story 3 -->
		<!-- main bg -->
		<section class="main-slogan main-slogan--yellow" data-bg="3">
			<div class="main-slogan__desc">
				<div class="table">
					<div class="cell">
						<img src="images/desc_slogan3.png" alt="무한한 가능성을 가진 친구들에게 어서 후원자님이 나타나길 기도해요.">
					</div>
				</div>
			</div>
		</section>
		<!-- //main bg -->
		<!-- main donation3 -->
		<section class="main-donation main-donation--yellow">
			<h2 class="main-donation__tit">
				<img src="images/tit_spon3.png" alt="1:1 어린이 후원하기 , 키움과 돌봄의 감동적인 1:1만남, 지금 시작하세요">
			</h2>
			<!-- [D] 세번째 후원 리스트 들어갈 곳 -->
			<div class="row">
				<iframe id="ifrm3" src="" frameborder="0" class="main-donation__iframe"></iframe>
                <script>
                    var host = getHost();
                    $('#ifrm3').attr('src', 'http://'+host+'/flower/list3?minAge=8&<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>');
                </script>
			</div>
		</section>
		<!-- //main donation3 -->
		<script>
		$(function () {
			//배너
			var banner = {
				c : function () {
					$('.main-video').fadeOut().remove();
					$('body').removeClass('modal-open');
				},
				bind : function () {
					$('.main-video__close').bind('click',function (e) {
						banner.c();
						e.preventDefault();
					});
				}
			}
			banner.bind();
			//인디케이터
			var lnb = {
				action : function (link) {
					var target = link.attr('href'),
					top1 = $(target).offset().top;
					$('html,body').animate({
						'scrollTop':top1
					},800,'easeInOutExpo');	
				},
				init : function () {
					$('.lnb a').removeClass('active');
					$('.lnb a').eq(0).addClass('active');
				}
			}

			$('.lnb a').bind('click',function (e) {
				lnb.action($(this));
				e.preventDefault();
			});
			// 스크롤 다운
			$('.btn-scrolldown').bind('click',function (e) {
				var top2 = $('.main-story--red').position().top;
				$('html,body').animate({
					'scrollTop' : top2
				},800,'easeInOutExpo');
				e.preventDefault();
			})
			// 메인 슬라이더 자동
			setInterval(function () {
				var current = $('.main-intro__videowrap.active'),
				next = current.next('.main-intro__videowrap');				
				if (!next.length) {
					$('.main-intro__videowrap').eq(0).addClass('active');
					$('.main-intro__videowrap').eq(0).find('video')[0].play();
				} else {
					next.addClass('active');	
					next.find('video')[0].play();
				}
				current.removeClass('active');
			},4000);
			var $window = $(window);
			// 줄기모션
			$.fn.move = function (type,number) {
				var $this = $(this),
				wheight = $window.height();
				function update() {
					var top3 = $window.scrollTop(),
					otop = $this.offset().top,
					chk = 200;
					if (top3 > 400) {$('.btn-top').fadeIn();} else {$('.btn-top').fadeOut();lnb.init();}
					if (type=='bg') {chk-=200;}
					if (top3+(wheight / 2)+chk < otop) {
						$this.removeClass('active');
						return false;
					} else {
						if (number ) {
							$('.lnb a').removeClass('active');
							$('.lnb a').eq(number).addClass('active');
						} 
						switch (type) {
							case 'fade' :
								$this.addClass('active');
								break;
							case 'line' :
								var value = top3-otop+700;
								$this.css('clip','rect(0,1366px,'+value+'px,0px)');
								$this.removeClass('active');
								break;
							case 'bg' :
								$this.css('backgroundPosition','50%' +' ' +Math.round((otop - top3) * 0.5) + 'px');
								$this.removeClass('active');								
								break;
						}
					}
				}
				$window.bind('scroll',update);
				update();
			}
			$('[data-show=1]').move('fade',1);
			$('[data-show=2]').move('fade');
			$('[data-show=3]').move('fade');
			$('[data-show=4]').move('fade',2);
			$('[data-show=5]').move('fade');
			$('[data-show=6]').move('fade');
			$('[data-show=7]').move('fade',3);
			$('[data-show=8]').move('fade');
			$('[data-show=9]').move('fade');
			$('[data-line=1]').move('line');
			$('[data-line=2]').move('line');
			$('[data-line=3]').move('line');
			$('[data-bg=1]').move('bg');
			$('[data-bg=2]').move('bg');
			$('[data-bg=3]').move('bg');
		})
		</script>
	</div>
	<!-- footer -->
	<footer id="footer" class="footer">
		<div class="row">
			<div class="footer__logo">
				<img src="images/logo_footer.png" alt="Compassion">
			</div>
			<div class="footer__info">
				<div class="footer__addr">
					한국컴패션 사업자등록번호 : 108-82-05789     |    대표자 : 서정인<br>
					(04418) 서울시 용산구 한남대로 102-5 (한남동 723-41) 석전빌딩<br>
					후원상담/안내 : 02-740-1000(평일 09:00 ~ 18:00)팩스 : 02-740-1001이메일 : info@compassion.or.kr
				</div>
				<div class="footer__copyright">
					© COMPASSION KOREA All Rights Reserved. Contact us for more information					
				</div>
			</div>
		</div>
	</footer>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '235171356890594'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=235171356890594&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<!-- Google 리마케팅 태그 코드 --> 
<!-------------------------------------------------- 
리마케팅 태그를 개인식별정보와 연결하거나 민감한 카테고리와 관련된 페이지에 추가해서는 안 됩니다. 리마케팅 태그를 설정하는 방법에 대해 자세히 알아보려면 다음 페이지를 참조하세요. http://google.com/ads/remarketingsetup 
---------------------------------------------------> 
<script type="text/javascript"> 
/* <![CDATA[ */ 
var google_conversion_id = 845672798; 
var google_custom_params = window.google_tag_params; 
var google_remarketing_only = true; 
/* ]]> */ 
</script> 
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"> 
</script> 
<noscript> 
<div style="display:inline;"> 
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/845672798/?guid=ON&amp;script=0"/> 
</div> 
</noscript>

	<!-- //footer -->
</div>
</body>
</html>