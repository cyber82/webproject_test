﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="list.aspx.cs" Inherits="_default" MasterPageFile="~/top_without_header.Master" %>
<%@ MasterType VirtualPath="~/top_without_header.master" %>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
    
    <script type="text/javascript" src="/default.js?v=1"></script>
    <%--<script type="text/javascript" src="/default.sympathy.js"></script>--%>
    <script type="text/javascript" src="/assets/jquery/wisekit/cookie.js"></script>
    <script type="text/javascript" src="/assets/jquery/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="/popup/popupManager.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<meta charset="UTF-8">
	<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title>
	<link rel="shortcut icon" href="favicon.ico" >	
	<link rel="stylesheet" href="/flower/common/css/list.css">
        <asp:PlaceHolder runat="server" ID="ph_sec4" Visible="true">
<!--NSmart Track Tag Script-->
        <script type='text/javascript'>
            callbackFn = function() {};
            var _nsmart = _nsmart || [];
            _nsmart.host = _nsmart.host || (('https:' == document.location.protocol) ? 'https://' : 'http://');
            //document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            // document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            /* v 1.4 ---------------------------------*
            *  Nasmedia Track Object, version 1.4
            *  (c) 2009 nasmedia,mizcom (2009-12-24)
            *---------------------------------------*/
            var nasmedia = {};
            var NTrackObj = null;
            nasmedia.track = function(){
                this.camp, this.img = null;
            };
            nasmedia.track.prototype = {
                callTrackTag : function(p, f, c){
                    if(Object.prototype.toString.call(_nsmart) != '[object Array]' || _nsmart.host == undefined || _nsmart.host == '') return false;
                    if(f != undefined && typeof(f) == 'function') {
                        this.img.onload = this.img.onerror = this.img.onabort = f;
                        var no = ((parseInt(c, 10) % 100) < 10 ? '0' + (parseInt(c, 10) % 100): (parseInt(c, 10) % 100));
                        this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + c + '&page=' + p + '&r=' + Math.random();
                    }
                    else this.callNext(0);
                    return false;
                },
                callNext : function(i) {
                    if(i == this.camp.length) return false;
                    this.img.onload = this.img.onerror = this.img.onabort = function(o, i){return function(){o.callNext(i);};}(this, i+1);
                    var no = ((parseInt(this.camp[i][0], 10) % 100) < 10 ? '0' + (parseInt(this.camp[i][0], 10) % 100): (parseInt(this.camp[i][0], 10) % 100));
                    this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + this.camp[i][0] + '&page=' + this.camp[i][1] + '&r=' + Math.random();
                },
                init : function(c) {
                    this.camp = c, this.img = new Image;
                }
            };
            (function(){
                NTrackObj = new nasmedia.track();
            })();
            NTrackObj.init(_nsmart);
        NTrackObj.callTrackTag();
        </script>
        <!--NSmart Track Tag Script End..-->

<ul class="main-donation__list main-donation__list--yellow">
                            <asp:Repeater runat="server" ID="repeater_children">
                                <ItemTemplate>
	<li>
		<div class="main-donation__list-item">
			<div class="main-donation__list-dday"><%#Eval("waitingdays")%>일						
			</div>
			<div class="main-donation__list-img" style="background:url(<%#Eval("pic")%>) no-repeat center top;">
				<%#Eval("name")%>
			</div>
			<div class="main-donation__list-name"><%#Eval("name")%></div>
			<div class="main-donation__list-nation"><%#Eval("countryName")%></div>
			<div class="main-donation__list-info"><%#Eval("birthDate" , "{0:yyyy.MM.dd}")%> <%#Eval("age")%>세, <%#Eval("gender")%></div>
<a href="/sponsor/children/?c=<%#Eval("childmasterid") %>&<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_WEB_direct&utm_source=WEB_direct&utm_campaign=2017flower​&utm_medium=WEB" %>" class="btn btn-donation" target="_blank" onclick="javascript:NTrackObj.callTrackTag('32034', callbackFn, 12490);" >후원하기</a>
		</div>
	</li>
                                </ItemTemplate>
                            </asp:Repeater>
</ul>
        </asp:PlaceHolder>     
</asp:Content>