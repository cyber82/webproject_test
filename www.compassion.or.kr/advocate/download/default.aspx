﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_notice_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="default.js"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>자료실</em></h1>
				<span class="desc">소중한 사람들에게 컴패션을 전할 수 있는 다양한 모임별 패킷과 이미지를 다운로드 받으세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 advocate">

			<div class="dataRoom tac w980">
				
				<!-- 모임별 패킷 -->
				<div class="stitle">
					<h2>모임별 패킷</h2>
				</div>
				<ul class="packetList tal mb15">
					<li ng-repeat="item in list" ng-click="">
						<!-- 이미지 사이즈 : 160 * 120 -->
						<div class="img"><img ng-src="{{item.b_thumb}}" width="160" alt="패킷 이미지" /></div>
						<div class="list_con">
							<p class="s_tit6">{{item.b_title}}</p>
							<p class="s_con3">
								{{item.b_summary}}
							</p>
						</div>
						<div class="download_bt"><a class="btn_s_type4" ng-click="download(item.b_file)" >Download</a></div>
					</li>
		
				</ul>
				<!--// 모임별 패킷 -->


				<!-- 사진 -->
				<div class="stitle">
					<h2>사진</h2>
				</div>

				<div class="data_photo clear2 mt30">
					<!-- 백그라운드 이미지 사이즈 : 629 * 468 -->
					<div class="bigImg" style="background:url('{{main}}') no-repeat center;" ng-if="main">
						<a ng-href="{{main}}" class="btn_b_type2">Download</a>
					</div>
					<ul class="thumbImg">
						<!-- 백그라운드 이미지 사이즈 : 262 * 196 -->
						<li ng-class="{on: $index == selectedIndex}" style="background:url('{{item.b_thumb}}') no-repeat center;" ng-repeat="item in picList" ng-click="changeMain(item, $index)">
							<span class="over"></span>
						</li>
					
					</ul>



				</div>
				<!--// 사진 -->


				<!-- 위젯 -->
				<div class="widgetWrap relative">
					<div class="stitle">
						<h2>위젯</h2>
						<span class="s_con3">원하시는 위젯의 코드를 복사해 블로그에 붙여 넣어 사용하세요.</span>
					</div>

					<ul class="dataWidget clear2">
						<li ng-repeat="item in widgetList">
							<!-- 이미지 사이즈 : 170 * 600 -->
							<img ng-src="{{item.w_image}}" alt="컴패션 위젯 이미지" />
							<div class="wg_btn mt20"><button><span class="btn_s_type3" ng-click="copyclip(item)">위젯 사용</span></button></div>
						</li>
						
					</ul>

					<!-- 배너코드 박스 -->
					<div class="bnrCodeBox tal">
						<p>이 배너의 코드입니다. Ctrl+C를 눌러 복사해 주세요.</p>
						<input type="text" name="" value="{{linkaddr}}" class="input_type2" style="width:380px" />
						<button class="close">닫기</button>
					</div>

				</div>				
				<!--// 위젯 -->

				<div class="contact_qna">
					<p class="tit">문의</p>
					<div class="clear2">
						<div class="tel"><span class="icon"><span class="txt">전화 : </span>02) 3668-3450</span></div>
						<div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:ebaek@compassion.or.kr">ebaek@compassion.or.kr</a></span></div>
					</div>
				</div>



			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>


    
</asp:Content>