﻿(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		$scope.total = -1;
		$scope.page = 1;
		$scope.rowsPerPage = 7;

		$scope.list = [];
		$scope.picList = [];
		$scope.widgetList = [];


		// 모임별 패킷 list
		$scope.getList = function (params) {

			$http.get("/api/board.ashx?t=file_list", { params: {page : 1, rowsPerPage:7, b_type : 'download_packet'} }).success(function (result) {
				$scope.list = result.data;
			});

		}
		$scope.getList();


		// 사진
		$scope.getPicList = function (params) {
			$http.get("/api/board.ashx?t=file_list", { params: { page: 1, rowsPerPage: 999, b_type: 'download_picture' } }).success(function (result) {
				$scope.picList = result.data;
				$scope.main = result.data.length > 0 ? result.data[0].b_thumb : "";
			});
		}
		$scope.changeMain = function (item, index) {
		    $scope.main = item.b_thumb;
		    $scope.selectedIndex = index;
            
		}
		$scope.getPicList();



		// 위젯
		$scope.getWidgetList = function (params) {
			$http.get("/api/advocate.ashx?t=widget_list", { params: { page: 1, rowsPerPage: 999} }).success(function (result) {
				$scope.widgetList = result.data;
			});
		}

		$scope.copyclip = function (item) {
            
		    var b = $(".dataWidget li .wg_btn");
		    var idx = 0;
		    b.click(function () {
		        idx = $(this).parent("li").index();

		        $(".bnrCodeBox").hide();
		        $(".bnrCodeBox").fadeIn();

		        $(".dataWidget li").each(function (e) {
		            $(".bnrCodeBox").removeClass("pos" + e);
		        })

		        $(".bnrCodeBox").addClass("pos" + idx);
		    })
		    $(".bnrCodeBox .close").click(function () {
		        $(".bnrCodeBox").fadeOut();
		    })

		    $scope.linkaddr = "<a href='"+item.w_link+"' target='_blank'><img src='"+item.w_image+"' border=0></a>";

		}
		$scope.getWidgetList();


		$scope.download = function (url) {
			location.href = "/api/advocate.ashx?t=download&url=" + encodeURIComponent(url);
		}

	});

})();

