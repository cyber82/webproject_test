﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_share_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="default.js"></script>
    <script>
        $(function () {
        	$(".notice_type li[data-type=online]").css("width", "15%");
        	$(".notice_type li[data-type=shop]").css("width", "15%");
        });
    </script>
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>활동공유</em></h1>
				<span class="desc">애드보킷 활동을 하며 겪은 이야기를 자유롭게 나눠 주세요</span>
				
				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 advocate">

			<!-- 베스트 -->
			<div class="share_best">
				<h2 class="tit">BEST 게시물</h2>
				<div class="best_wrap">
					<ul class="best3">
						<li ng-repeat="item in top" ng-click="goView(item.ub_id)">
							<!-- 이미지 사이즈 : 308 * 226 -->
							<span class="img_wh" style="background:url('{{item.ub_thumb}}') no-repeat center;"></span>
							<span class="tit_box">
								<span class="tit">[{{item.type}}] {{item.ub_title}}</span>
								<span class="name">{{item.ub_user_id}}</span>
								<span class="bar"></span>
								<span class="day">{{parseDate(item.ub_regdate) | date:'yyyy.MM.dd'}}</span>
								<span class="bar"></span>
								<span class="hit">{{item.ub_hits}}</span>
							</span>

							<!-- 마우스 오버 시 -->
							<a href="#" class="over"><span class="btn_view1">VIEW</span></a>
						</li>
					</ul>
				</div>
			</div>
			<!-- //베스트 -->

			<div class="shareList w980">		

				<!-- 탭메뉴 -->
				<ul class="tab_type1 notice_type" id="l">
					<asp:Repeater runat="server" ID="repeater">
						<ItemTemplate>
							<li style="width:14%" ng-click="sorting('<%#Eval("cd_key") %>')" data-type="<%#Eval("cd_key") %>"><a><%#Eval("cd_value") %></a></li>
						</ItemTemplate>
					</asp:Repeater>
				</ul>
				<!--// 탭메뉴 -->

				<!-- 게시판 리스트 -->
				<div class="boardList_1">

					<div class="sortWrap">
						<div class="fr relative mt50">
							<label for="k_word" class="hidden">검색어 입력</label>
							<input type="text" name="k_word" id="k_word" ng-enter="search()" class="input_search1" style="width:245px" placeholder="검색어를 입력해 주세요" />
							<a ng-click="search()" class="search_area1">검색</a>
						</div>
					</div>

					<ul class="list">
						<li ng-repeat="item in list" ng-click="goView(item.ub_id)">
							<!-- 이미지 사이즈 : 308 * 226 -->
							<span class="img_wh" style="background:url('{{item.ub_thumb}}') no-repeat center;"></span>
							<span class="tit_box">
								<span class="tit tit2">[{{item.type}}] {{item.ub_title}}</span>
								<span class="name">{{item.ub_user_id}}</span>
								<span class="bar"></span>
								<span class="day">{{parseDate(item.ub_regdate) | date:'yyyy.MM.dd'}}</span> 
								<span class="bar"></span>
								<span class="hit">{{item.ub_hits}}</span>
							</span>

							<!-- 마우스 오버 시 -->
							<a href="#" class="over"><span class="btn_view1">VIEW</span></a>
						</li>
						
						<!-- 검색결과 없을때 -->
						<li class="no_result" ng-if="total == 0 && isSearch">검색 결과가 없습니다.</li>
						<!--//  -->

						<!-- 게시글 없을때 -->
						<li class="no_content" ng-if="total == 0 && !isSearch">등록된 글이 없습니다.</li>
						<!--//  -->

					</ul>

				</div>
				<!--// 게시판 리스트 -->
				<!-- page navigation -->
				<div class="tac mb60">
					<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
				</div>
				<!--//page navigation -->


				<div class="relative tar"><a ng-click="goWrite();" class="btn_type1">글쓰기</a></div>

			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

    
</asp:Content>