﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="write.aspx.cs" Inherits="advocate_share_write" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
    <script type="text/javascript">
        var image_path = "";

        var getImagePath = function () {
            return image_path;
        }

        var oEditors = [];
        // 추가 글꼴 목록
        //var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];

        var initEditor = function (ref, holder) {
            nhn.husky.EZCreator.createInIFrame({
                oAppRef: ref,
                elPlaceHolder: holder,
                sSkinURI: "/common/smartEditor/SmartEditor2Skin.html",
                htParams: {
                    bUseToolbar: true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
                    bUseVerticalResizer: true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
                    bUseModeChanger: true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
                    //aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
                    fOnBeforeUnload: function () {
                        //alert("완료!");
                    }
                }, //boolean
                fOnAppLoad: function () {
                    //oEditors.getById["content"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
                },
                fCreator: "createSEditor2"
            });
        }


        $(function () {
            image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
        	initEditor(oEditors, "ub_content");

        	$("input[data-id=" + $("#ub_category").val() + "]").prop("checked", true);

        	$("input[name=ub_category]").click(function () {
        		val = $(this).val();
        		$("#ub_category").val(val);
        	})

		});


        function onSubmit() {

            oEditors.getById["ub_content"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용됩니다.

        	/*
            if ($("#ub_category input:checked").length < 1) {
                alert("소속을 선택해주세요.");
                $("#ub_category input").focus();
                return false;
            }
			*/

            if (!validateForm([
				{ id: "#ub_title", msg: "제목을 입력해주세요" },
            ])) {
                return false;
            }

            
            if ($("#ub_content").val() == "<p>&nbsp;</p>") {
                alert("내용을 입력해 주세요")
                oEditors.getById["ub_content"].exec('FOCUS', []);
                return false;
            }


            // 에디터의 등록
            var $editor = $($("#ub_content").val());
            $("#ub_thumb").val($editor.find("img").attr("src"));

            return true;
        }

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
    <!-- sub body -->
	<section class="sub_body">
		 
		<input type="hidden" runat="server" id="ub_thumb"/>
		<input type="hidden" runat="server" id="ub_category" value="online"/>

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>활동공유</em></h1>
				<span class="desc">애드보킷 활동을 하며 겪은 이야기를 자유롭게 나눠 주세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="sharingW w980">
				
				<!-- 공유쓰기 테이블 -->
				<div class="tableWrap1">
					<table class="tbl_type1 mb40">
						<caption>활동공유 쓰기 테이블</caption>
						<colgroup>
							<col style="width:18%" />
							<col style="width:82%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><label for="ub_title">제목</label></th>
								<td><input type="text" runat="server" ID="ub_title" class="input_type2" style="width:77%" maxlength="100" /></td>
							</tr>
							<tr>
								<th scope="row">소속</th>
								<td>
									<span class="radio_ui">
										<asp:Repeater runat="server" ID="repeater">
											<ItemTemplate>
												<input type="radio" name="ub_category" id="<%#Eval("cd_key")%>" data-id="<%#Eval("cd_key")%>" class="css_radio" value="<%#Eval("cd_key") %>" />
												<label for="<%#Eval("cd_key") %>" class="css_label mr30"><%#Eval("cd_value") %></label>
											</ItemTemplate>
										</asp:Repeater>
									</span>
								</td>
							</tr>
							<tr class="padding3">
								<td colspan="2">
									<textarea name="ub_content" id="ub_content" runat="server" style="display: none;"></textarea>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 공유쓰기 테이블 -->

				<div class="btn_ac">
					<div>
						<a runat="server" id="btnList" class="btn_type2 fl mr10">취소</a>
						<asp:LinkButton runat="server" ID="btnAdd" CssClass="btn_type1 fl" OnClick="btnAdd_Click" OnClientClick="return onSubmit()" >확인</asp:LinkButton>
					</div>
				</div>


			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->



  
	





</asp:Content>
