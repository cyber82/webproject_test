﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;
using System.Configuration;
using Microsoft.AspNet.FriendlyUrls;

public partial class advocate_share_write : FrontBasePage {

	const string listPath = "/advocate/share/";


	public override bool RequireLogin{
		get{
			return true;
		}
	}

	protected override void OnBeforePostBack()
    {
		base.OnBeforePostBack();
	    btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		var userInfo = new UserInfo();

        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<advocate_user>("au_user_id", userInfo.UserId);
            //if (!dao.advocate_user.Any(p => p.au_user_id == userInfo.UserId))
            if(!exist.Any())
            {
                var isAdvocate = new VOCAction().CheckUser();
                if (!isAdvocate.success)
                {
                    base.AlertWithJavascript("애드보킷 회원만 이용가능합니다. 온라인 애드보킷 지원페이지를 이용하세요.", "goBack()");
                    return;
                }
            }
        }


		var radio_entity = StaticData.Code.GetList( this.Context, true ).Where( p => p.cd_display == true && p.cd_group == "board_sub" && p.cd_key != "all" ).OrderBy( p => p.cd_order );

		repeater.DataSource = radio_entity;
		repeater.DataBind();

		/*
		foreach(var entity in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "board_sub" && p.cd_key != "all").OrderBy(p => p.cd_order)) {
			ub_category.Items.Add(new ListItem(entity.cd_value, entity.cd_key));
		}
		*/



		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count > 0) {
			base.PrimaryKey = requests[0];

            using (FrontDataContext dao = new FrontDataContext())
            {
                //var entity = dao.user_board.First(p => p.ub_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<user_board>("ub_id", Convert.ToInt32(PrimaryKey));

                var exist = www6.selectQ<user_board>("ub_id", Convert.ToInt32(PrimaryKey), "ub_display", 1, "ub_deleted", 0);
                //if (!dao.user_board.Any(p => p.ub_id == Convert.ToInt32(PrimaryKey) && p.ub_display && !p.ub_deleted))
                if(!exist.Any())
                {
                    Response.Redirect(listPath, true);
                }

                var user = new UserInfo();
                // 자신글 확인
                if (entity.ub_user_id != user.UserId)
                {
                    Response.Redirect(listPath, true);
                }

                ub_title.Value = entity.ub_title;
                ub_content.InnerHtml = entity.ub_content;
                ub_category.Value = entity.ub_category;
            }
		}
	}


	protected override void loadComplete(object sender, EventArgs e) {
		
		
	}

	protected void btnAdd_Click(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var user = new UserInfo();

            if (PrimaryKey == null)
            {
                var entity = new user_board()
                {
                    ub_type = "advocate",
                    ub_category = ub_category.Value,
                    ub_title = ub_title.Value,
                    ub_content = ub_content.InnerHtml.ToHtml(),
                    ub_thumb = ub_thumb.Value == "" ? ConfigurationManager.AppSettings["advocate_share_default_image"] : ub_thumb.Value,
                    ub_user_id = user.UserId,
                    ub_user_name = user.UserName,
                    ub_top = false,
                    ub_hits = 0,
                    ub_regdate = DateTime.Now,
                    ub_deleted = false,
                    ub_ip = Request.ServerVariables["REMOTE_ADDR"].ToString(),
                    ub_display = true
                };

                //dao.user_board.InsertOnSubmit(entity);
                www6.insert(entity);
                //dao.SubmitChanges();

                base.AlertWithJavascript("등록이 완료되었습니다. 후원자님의 귀한 섬김에 언제나 감사드립니다.", "goList()");
            }
            else
            {
                //var entity = dao.user_board.First(p => p.ub_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<user_board>("ub_id", Convert.ToInt32(PrimaryKey));

                entity.ub_category = ub_category.Value;
                entity.ub_title = ub_title.Value;
                entity.ub_content = ub_content.InnerHtml.ToHtml();
                entity.ub_thumb = ub_thumb.Value == "" ? ConfigurationManager.AppSettings["advocate_share_default_image"] : ub_thumb.Value;

                //dao.SubmitChanges();
                //string wClause = string.Format("ub_id = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);

                base.AlertWithJavascript("수정되었습니다.", "location.href='/advocate/share/view/" + base.PrimaryKey + "?" + this.ViewState["q"].ToString() + "'");
            }


        }
	}
}