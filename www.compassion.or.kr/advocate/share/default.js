﻿(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		$scope.total = -1;
		$scope.page = 1;
		$scope.isSearch = false;
		$scope.rowsPerPage = 9;
		$scope.isFirst = true;

		$scope.top = [];
		$scope.list = [];
		$scope.params = {
			page: $scope.page,
			rowsPerPage: $scope.rowsPerPage,
			type: 'advocate',
			category: paramService.getParameter("sorting") == "" ? 'all' : paramService.getParameter("sorting"),
			k_word: ""
		};


		// 파라미터 초기화
		$scope.params = $.extend($scope.params, paramService.getParameterValues());
		if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);

		// 검색
		$scope.search = function (params) {
			$scope.params = $.extend($scope.params, params);
			$scope.params.k_word = $("#k_word").val();
			$scope.getList();
		}


		$scope.sorting = function (type) {

			$(".notice_type li").removeClass("on");
			if (type == "") type = "all";
			$(".notice_type li[data-type=" + type + "]").addClass("on");

			$scope.params.page = 1;
			$scope.params.category = type == "all" ? "" : type;
			$("#k_word").val("");

			if ($scope.isFirst) { $scope.isFirst = false; return; }
			$scope.search();
			
		}

		// list
		$scope.getTop = function (params) {
			$http.get("/api/advocate.ashx?t=share_list&type=advocate&page=1&rowsPerPage=3&top=1").success(function (result) {
				$scope.top = result.data;
				if (params)
					scrollTo($("#l"), 10);

				$.each($scope.top, function () {
					if (this.ub_category == "online") {
						this.type = "온라인 애드보킷"
					} else if (this.ub_category == "share") {
						this.type = "나눔별"
					} else if (this.ub_category == "shop") {
						this.type = "프렌즈샵"
					} else {
						this.type = this.ub_category.toUpperCase();
					}
				})
			});

		}

		// list
		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);


			$http.get("/api/advocate.ashx?t=share_list", { params: $scope.params }).success(function (result) {
				$scope.list = result.data;
				$scope.total = result.data.length > 0 ? result.data[0].total : 0;

				$scope.isSearch = false;
				if ($scope.params.k_word != "") {
					$scope.isSearch = true;
				}

				$.each($scope.list, function () {
					if (this.ub_category == "online") {
						this.type = "온라인 애드보킷"
					} else if (this.ub_category == "share") {
						this.type = "나눔별"
					} else if (this.ub_category == "shop") {
						this.type = "프렌즈샵"
					} else {
						this.type = this.ub_category.toUpperCase();
					}
				})



				if (params)
					scrollTo($("#l"), 10);
			});

		}


		// 상세페이지
		$scope.goView = function (id) {
			$http.post("/api/advocate.ashx?t=hits&type=share&id=" + id).then().finally(function () {
				location.href = "/advocate/share/view/" + id + "?" + $.param($scope.params);
			});
		}

		$scope.parseDate = function (datetime) {
			return new Date(datetime);
		}

		$scope.getTop();
		$scope.getList();

		if (!$scope.params.category) {
			$scope.params.category = "";
		}

		$scope.sorting($scope.params.category);

		$scope.goWrite = function () {

		    $http.post("/api/advocate.ashx?t=check_user").success(function (r) {
		        if (!r.success) {
		            alert(r.message);
		            return;
		        }
		        location.href = "/advocate/share/write/";
		    });
		    
		};

	});

})();

