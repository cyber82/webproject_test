﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="voc.aspx.cs" Inherits="advocate_intro_voc" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>VOC(Voice of Compassion)</em></h1>
				<span class="desc">가난한 어린이들의 목소리가 되어 활동하는 컴패션 애드보킷을 소개합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="voc tac">

				<div class="w980 intro">

					<p class="advo_tit"><em>VOC</em>는 컴패션 일반인 홍보대사로서 가난으로</p>
					<p class="advo_con">고통받는 어린이의 목소리가 되어 적극적으로 그들을 대변하고 더 많은 어린이가 후원자의<br />
					손을 잡을 수 있도록 돕는 컴패션 후원자를 부르는 말입니다.</p>
					<div class="mt40">
						<iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/Ixcft2x7Xus?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
					</div>
					<p class="s_con3">VOC는 현재 전 세계 12개 컴패션 후원국에서 동일하게 어린이들을 위한 일반인 컴패션 홍보대사로 활발한 활동을 진행하고 있으며, 한국은 2007년 말<br />
                        ‘후원자의 밤’행사를 기점으로 발족되었습니다. 이후 2008년 서울 VOC 1기, 2009년 부산 VOC 1기를 시작으로 대전충청, 대구, 인천, 동해 등 전국에서
                        <br />어린이들을 위한 컴패션 사역에 동참하고 어린이들의 필요를 채우는 사명을 완수해 왔습니다.</p>
				</div>

				<div class="voc_role">					
					<h2 class="sub_tit">VOC의 역할</h2>
					<span class="bar"></span>
					<ul class="s_con3">
						<li>컴패션의 전인적인 양육 프로그램을 정확히 이해하여 컴패션을 대신해서 교회와 학교, 직장 등 본인이 속한 곳에서 컴패션 사역을 알리고<br />
						도움이 필요한 어린이들이 새로운 후원자를 만나도록 적극 활동합니다.</li>
						<li>자신이 속한 곳에서 전 세계 도움이 필요한 어린이들을 대변하는 어린이들의 목소리가 되어줍니다.</li>
						<li>컴패션이 함께하는 행사와 집회 등에서 진행을 돕습니다.</li>
						<li>VOC가 주최가 되어 컴패션 홍보행사를 직접 기획하고 진행합니다.</li>
						<li>컴패션 사역과 어린이를 위한 기도 후원자가 됩니다.</li>
						<li>자신을 통해 후원하게 된 후원자를 관리하고 이들이 어린이를 더욱 사랑할 수 있도록 힘씁니다.</li>
					</ul>
				</div>

				<div class="voc_act w980">
					<h2 class="sub_tit">VOC 주요 활동</h2>
					<ul>
						<li>							
							<h3 class="s_tit6">VOC 정기모임</h3>
							<p class="s_con3">
								컴패션 사역과 VOC 역할를 이해할 수 있는 신규 교육을 제공하며<br />
                                조별 모임을 통해 각자의 삶의 영역에서 어떻게 어린이들의 목소리가 될 것인가 아이디어를 나누고 실행합니다.
							</p>
						</li>
						<li>
							<h3 class="s_tit6">VOC Act For Compassion</h3>
							<p class="s_con3">
								VOC 각자의 삶의 자리에서 다양한 아이디어로 어린이들의 목소리가 됩니다.<br />
                                어린이들의 결연을 위해 컴패션 선데이로 연결, 마라톤(Running For Children), VOC 콰이어, 걷기 프로젝트, 재능기부를 통한<br />
                                원데이 클라스, 사진전, 바자회 등 다양한 형태로 컴패션을 알리고 행사를 진행 합니다.
							</p>
						</li>
						<li>
							<h3 class="s_tit6">VOC 결연행사</h3>
							<p class="s_con3">
								기획부터 마무리까지 VOC가 자발적으로 기금을 들여 준비합니다. 각 지인을 초청하여 컴패션을 소개하고<br />
								파티를 통해 새로운 어린이가 후원자를 만나 후원 받게 할 수 있습니다.
							</p>
						</li>
						<li>
							<h3 class="s_tit6">VOC 비전트립</h3>
							<p class="s_con3">
								컴패션 사역을 눈으로 확인하고 비전을 공유하기 위한 VOC 비전트립이 매년 여름 진행됩니다.<br />
								어린이들과의 만남을 통해 컴패션의 비전을 나눌 뿐 아니라 함께 참여한 VOC들과 가족처럼 가까워지는 시간이 됩니다.
							</p>
						</li>
					</ul>					
				</div>

				<div class="voc_guide" style="height:960px; background-repeat: repeat; background-position-y: top;">
					<h2 class="sub_tit">VOC 신청 안내</h2>
					<span class="bar"></span>
					<p class="s_con3">VOC는 1년에 정해진 기간에 따라 지역별 일정을 가지고 선발됩니다.</p>
					<div class="license">
						<h3 class="tit">VOC의 활동 자격</h3>
						<ul class="s_con3">
							<li>1:1어린이양육을 통해 어린이를 후원하고 계신 20세 이상 후원자</li>
							<li>컴패션 사역의 중요성을 이해하고 이를 세상에 알리기 원하는 후원자</li>
							<li>홍보대사 필수 교육 및 VOC 활동에 성실하게 참여할 수 있는 후원자</li>
						</ul>
					</div>

                    <div class="howTo">
						<h3 class="tit">신청 방법</h3>
						<p class="s_con3"><br>
                        <a href="http://bit.ly/2BCE5Y2" class="btn_s_type5" style="color:#666;" target="_blank">VOC 신청하기</a><br><br> 
                        해당링크로 연결하여 온라인 지원해주세요.<br /><br />
                        1차 서류 합격자는 개별 연락 드리며, 인터뷰 및 추후 일정을 안내 드립니다.<br />
                    </div>

					<div class="howTo">
						<%--<p class="s_con3">
							아래의 지원서를 다운로드받아 작성하고 이메일로 지원해 주세요.&nbsp;
							<a href="/common/download/VOC_ApplicationForm.zip" class="download">지원서 다운받기</a><br />
							서울 VOC : <a href="mailto:voc@compassion.or.kr">voc@compassion.or.kr</a>  부산 VOC : <a href="mailto:busan@compassion.or.kr">busan@compassion.or.kr</a>
						</p>
						<div class="how">
							<div class="step1"><span>지원서 다운로드 후 작성</span></div>
							<div class="step2"><span>메일로 전송하면 지원 완료!</span></div>
						</div>--%>
                        <%--<p class="s_con3">
                            아래의 링크를 통해 신청서를 작성해 주세요.<br />
                            <a href="http://bit.ly/2BCE5Y2" class="download">http://bit.ly/2BCE5Y2</a>
                        </p>--%>
					</div>
					<ul class="info clear2">
						<%--<li style="width:25%;">
							<h3 class="tit">명단 발표</h3>
							<p class="s_con3">홈페이지 및 개별 문자 공지</p>
						</li>--%>
						<li style="width:100%;">
							<h3 class="tit">VOC 정기모임 안내</h3>
							<p class="s_con3">
								서울: (월 2회) 화요일 저녁 7시 30분 / 한국컴패션 2층 스완슨홀<br />
                                부산: (월 2회) 월요일 저녁 7시30분 / 부산 초량교회<br />
                                대전충청: (월 1회) 셋째주 토요일 오후 3시 / 대덕교회<br />
                                대구: (월 2회) 토요일 오후 5시 / 대구삼덕교회<br />
                                인천: (월 2회) 토요일 오후 2시 / 석남은혜교회<br />
                                동해: (월 1회) 셋째주 수요일 오후 7시 / 메르시마마<br />
                                <br />
                                *날짜와 장소는 변경될 수 있으니 관심 있는 분들은 문의 바랍니다.
                                <br />
                            </p>
						</li>
						<%--<li style="width:25%;">
							<h3 class="tit">활동 기간</h3>
							<p class="s_con3">1년 단위로 활동하며<br />매년 초 재신청을 받습니다.</p>
						</li>--%>
					</ul>
                   <%-- <div class="license">
						<h3 class="tit">컴패션 애드보킷 온라인 커뮤니티</h3>
                        <p class="s_con3">
						    애드보킷 온라인 커뮤니티를 통해 애드보킷 행사, 활동 등 다양한 내용을 접할 수 있습니다.<br />
                            http://cafe.naver.com/compassionadv 애드보킷 페이지 접속<br />
                            QR코드로 접속할 수 있어요!
                            <img src="/common/img/page/advocate/compassion_qrcode.gif" alt="cafe.naver.com/compassionadv" style="width:100px; height:100px; margin:20px auto;">
                        </p>
					</div>--%>
				</div>

				<!-- 문의 -->
				<div class="v_contact_qna w980">
					<p class="tit">문의</p>
					<div class="clear2">
						<%--<div class="seoul">
							<p class="part">서울VOC</p>
							<div class="con">
								<span class="tel"><span class="txt">전화 : </span>02)3668-3516,&nbsp;3494</span><br />
								<span class="email"><span class="txt">이메일 : </span><a href="mailto:voc@compassion.or.kr">voc@compassion.or.kr</a></span>
							</div>
						</div>
						<div class="busan">
							<p class="part">부산VOC</p>
							<div class="con">
								<span class="tel"><span class="txt">전화 : </span>051)461-0303</span><br />
								<span class="email"><span class="txt">이메일 : </span><a href="mailto:busan@compassion.or.kr">busan@compassion.or.kr</a></span>
							</div>
						</div>--%>
                        <div class="seoul" style="height:80px;">
							<div class="con">
								<span class="tel"><span class="txt">전화 : </span>02)3668-3494</span><br />
							</div>
						</div>
						<div class="busan" style="height:80px;">
							<div class="con">
								<span class="email"><span class="txt">이메일 : </span><a href="mailto:voc@compassion.or.kr">voc@compassion.or.kr</a></span>
							</div>
						</div>
					</div>
				</div>
				<!--//  -->
				
				<!-- 공지/공유 바로가기 -->
				<div class="act_news w980">
					<p class="tit">VOC의 자세한 활동 소식이 궁금하신가요?</p>
					<div class="board clear2 tal">
						<%--<div class="box_type4 fl">
							<p class="s_tit1">공지 게시판</p>
							<p class="con">VOC 활동 공지 게시판 입니다.</p>
							<a href="/advocate/notice/?b_sub_type=voc" class="btn_s_type3">바로 가기</a>
						</div>
						<div class="box_type4 fr">
							<p class="s_tit1">공유 게시판</p>
							<p class="con">VOC 활동 공유 게시판 입니다.</p>
							<a href="/advocate/share/?category=voc" class="btn_s_type3">바로 가기</a>
						</div>--%>
                        <div class="box_type4 fl" style="width:100%;">
							<p class="s_tit1">컴패션 애드보킷 온라인 커뮤니티</p>
							<p class="con">애드보킷 온라인 커뮤니티를 통해 애드보킷 행사, 활동 등 다양한 내용을 접할 수 있습니다.</p>
							<p style="text-align:center;"><a href="http://cafe.naver.com/compassionadv" class="btn_s_type3">바로 가기</a></p>
						</div>
					</div>
				</div>
				<!--//  -->

			</div>
		</div>
		

		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

	
    
</asp:Content>