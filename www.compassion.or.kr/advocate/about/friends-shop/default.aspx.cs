﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using System.Configuration;

public partial class advocate_about_friends_shop : FrontBasePage {


    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();

        this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];
        upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_advocate);

        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if(requests.Count > 0 && requests[0].CheckNumeric()) {
            // sns등 id를 가지고 온 경우 상세 팝업 호출
            id.Value = requests[0];
        }

    }

    protected override void loadComplete( object sender, EventArgs e )
    {
        var index = Request["c"].EmptyIfNull();
        if(!string.IsNullOrEmpty(index))
        {
            using (StoreDataContext dao = new StoreDataContext())
            {
                //var entity = dao.friends_shop.First(p => p.idx == Convert.ToInt32(index));
                var entity = www6.selectQFStore<CommonLib.friends_shop>("idx", Convert.ToInt32(index));

                share_list.Value = entity.ToJson();
                // SNS
                this.ViewState["meta_url"] = Request.UrlEx();
                this.ViewState["meta_title"] = string.Format("[한국컴패션-컴패션프렌즈샵] {0}", entity.shop_name);

                // optional(설정 안할 경우 주석)
                //this.ViewState["meta_description"] = "";
                //this.ViewState["meta_keyword"] = "";
                // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,FrontBasePage)
                this.ViewState["meta_image"] = entity.shop_img.WithFileServerHost();


            }
        }

    }


}
