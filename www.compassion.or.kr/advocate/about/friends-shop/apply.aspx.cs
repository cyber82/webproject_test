﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class advocate_about_friends_shop_apply : FrontBasePage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		if (!UserInfo.IsLogin) {
			Response.ClearContent();
			return;
		}


		UserInfo sess = new UserInfo();
		// 이름
		writer.Value = sess.UserName;
		writer_email.Value = sess.Email;


		if (sess.LocationType == "국내") {

			var addr_result = new SponsorAction().GetAddress();
			if (!addr_result.success) {
				base.AlertWithJavascript(addr_result.message, "goBack()");
				return;
			}

			var comm_result = new SponsorAction().GetCommunications();
			if (!comm_result.success) {
				base.AlertWithJavascript(comm_result.message, "goBack()");
				return;
			}

			SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
			writer_tel.Value = comm_data.Mobile.TranslatePhoneNumber();
		}
	}
	
}