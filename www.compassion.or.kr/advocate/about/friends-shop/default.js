﻿


function jusoCallback(zipNo, addr1, addr2, jibun) {
	// 실제 저장 데이타
	$("#addr1").val(addr1);
	$("#addr2").val(addr2);

	// 화면에 표시
	$(".zipcode").val(zipNo);

	$("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
	$("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);

};

(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, popup, paramService) {

        $scope.requesting = false;
        $scope.total = -1;
        $scope.rowsPerPage = 9;

        $scope.list = [];
        $scope.nationList = [];
        $scope.params = {
            page: 1,
            rowsPerPage: $scope.rowsPerPage
        };

        // 파라미터 초기화
        $scope.params = $.extend($scope.params, paramService.getParameterValues());
        if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);
        if ($scope.params.type) $("#type").val($scope.params.type);	// 지역
        if ($scope.params.addr) $("#addr").val($scope.params.addr);	// 업종


        // 검색
        $scope.search = function (params) {
            $scope.params = $.extend($scope.params, params);
            $scope.params.page = 1;
            $scope.params.k_word = $("#k_word").val();
            $scope.params.addr = $("#addr").val();
            $scope.params.type = $("#type").val();

            $scope.getList();
        }


        // list
        $scope.getList = function (params) {
        	$scope.params = $.extend($scope.params, params);

            $http.get("/api/advocate.ashx?t=friends_shop_list", { params: $scope.params }).success(function (result) {
            	$scope.list = result.data;

            	$.each($scope.list, function () {
            		this.shop_type = "#" + this.shop_type;
            		if (!this.is_online && this.shop_addr != "온라인샵" && this.shop_addr != "온라인") {
            			this.shop_location = "#" + this.shop_addr.substring(0, 2) + ","
            		}
            	})

            	$scope.total = result.data.length > 0 ? result.data[0].total : 0;
            	//console.log($scope.list)
            });

            if (params) {
                scrollTo($("#l"), 10);
            }
            
        }
        $scope.getList();

        // 프렌즈 샵 신청
        $scope.modal = {
            instance: null,

            init: function () {
                // 팝업
            	popup.init($scope, "/advocate/about/friends-shop/apply", function (modal) {
            		$scope.modal.instance = modal;

            		$("#writer_tel").mobileFormat();
            		
            		var uploader = attachUploader("btnImage");
            		uploader._settings.data.fileDir = $("#upload_root").val();
            		uploader._settings.data.fileType = "image";


            	}, { top: 0, iscroll: true, backgroundClick: 'n' });

            },

            show: function () {
                if (!$scope.modal.instance)
                	return;
				
                if (common.checkLogin()) {
                	$scope.modal.instance.show();

                	setNumberOnly();

					/*
                	$("#online").unbind("click");
                	$("#online").click(function () {
                		var is = $(this).is(":checked");
                		$("#search_addr").prop("disabled", is);
                		$("#shop_zip").prop("disabled", is);
                		$("#addr1").prop("disabled", is);
                		$("#addr2").prop("disabled", is);

                		if (is) {
                			$("#shop_zip").val(" ")
                			$("#addr1").val("")
                			$("#addr2").val(" ")

                			$("#addr_road").text("");
                			$("#addr_jibun").text("");
                		} else {
                			$("#addr1").val("")
                			$("#addr2").val("")
                		}

                	})
					*/
                }
            },

            request: function ($event) {

                if (!validateForm([
					{ id: "#writer", msg: "이름을 입력하세요." },
					{ id: "#writer_tel", msg: "연락처를 입력하세요.", type: "phone" },
					{ id: "#writer_email", msg: "이메일을 입력하세요." },
					{ id: "#shop_name", msg: "프렌즈샵 명을 입력하세요." }
                ])) {
                    return;
                }

                if ($("input[name=shop_type]:checked").length < 1) {
                	alert("업종을 선택하세요");
                	$("input[name=shop_type]").eq(0).focus();
                	return false;
                }

                if (!validateForm([
					{ id: "#shop_tel", msg: "샵 대표번호를 입력하세요." },
					{ id: "#shop_zip", msg: "주소를 입력하세요."},
					{ id: "#addr1", msg: "주소를 입력하세요." },
					{ id: "#addr2", msg: "주소를 입력하세요." }
                ])) {
                	return;
                }


                if ($("input[name=part]:checked").length < 1) {
                	alert("참여가능 영역을 선택하세요");
                	$("input[name=shop_type]").eq(0).focus();
                	return false;
                }


                if (!validateForm([
					{ id: "#shop_intro", msg: "샵 소개를 입력하세요." }
                ])) {
                	return;
                }

                if ($("#btnImage").attr("data-image") != "1") {
                	alert("샵이미지를 선택하세요.");
                	$("#shop_img").focus();
                	return false;
                }


                if ($scope.requesting) return;
                $scope.requesting = true;


                var param = {
                	writer: $("#writer").val(),
                	writer_tel: $("#writer_tel").val(),
                	writer_email: $("#writer_email").val(),
                	shop_name: $("#shop_name").val(),
                	shop_type: $("input[name=shop_type]:checked").val(),
                	shop_tel: $("#shop_tel").val(),
                	shop_zip: $("#shop_zip").val(),
                	shop_addr: $("#addr1").val() + " " + $("#addr2").val(),
                	shop_homepage: $("#shop_homepage").val(),
                	part: $.map($('input[name=part]:checked'), function(n, i){return n.value;}).join(','),
                	shop_intro: $("#shop_intro").val(),
                	shop_img: $("#shop_img").val(),
                	online: $("#online").is(":checked")?"1" : "0"
                };
                $http.post("/api/advocate.ashx?t=friends_shop_apply", param).success(function (r) {
                    $scope.requesting = false;
                    if (r.success) {
                        alert("신청이 완료되었습니다.\n컴패션 담당자가 연락드릴 예정입니다.\n후원자님의 귀한 섬김에 언제나 감사드립니다.");
                    } else {
                        alert(r.message);
                    }
                    $scope.modal.close($event);

                });
            },

            close: function ($event) {

                if (!$scope.modal.instance)
                    return;

                $scope.modal.instance.hide();
                $event.preventDefault();
            },

            popup: function ($event) {
            	cert_setDomain();
            	var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");
                $event.preventDefault();
            }

        }
        $scope.modal.init();

        // 신청안내
        $scope.modalInfo = {
            instance: null,
            init: function () {
                // 팝업
                popup.init($scope, "/advocate/about/friends-shop/guide", function (modalInfo) {
                    $scope.modalInfo.instance = modalInfo;
                }, { top: 0, iscroll: true, backgroundClick: 'n' });
            },

            show: function () {
                if (!$scope.modalInfo.instance)
                    return;
                $scope.modalInfo.instance.show();

            },

            close: function ($event) {
                $event.preventDefault();
                if (!$scope.modalInfo.instance)
                    return;
                $scope.modalInfo.instance.hide();

            }
        }
        $scope.modalInfo.init();

    	// 상세정보
        $scope.view = {
        	instance: null,
        	item: null,

        	show: function (id, item, $event) {

        	    $scope.view.item = item;
        	    $scope.view.item.url = location.protocol + "//" + location.host + "/advocate/about/friends-shop/" + "?c=" + id;

        	    //console.log($scope.view.item)

        	    popup.init($scope, "/advocate/about/friends-shop/view/" + id , function (view) {
        			$scope.view.instance = view;
        			$scope.view.instance.show();
                    
        			common.bindSNSAction();
        			$("a[data-role='sns']").attr("data-url", $scope.view.item.url).attr("data-title", "[한국컴패션-컴패션프렌즈샵] " + $scope.view.item.shop_name).attr("data-picture", $scope.view.item.shop_img)

        	    }, { removeWhenClose : true});
        	    if ($event) {
        	        $event.preventDefault();
        	    }
        	},

        	close: function ($event) {

        		if (!$scope.view.instance)
        			return;

        		$scope.view.instance.hide();
        		$event.preventDefault();
        	}

        }


        $scope.shareAction = function () {

            var paramValue = paramService.getParameter("c");
            //console.log(paramValue)
            //console.log($("#share_list").val());

            if (paramValue) {

            var share_list = $.parseJSON($("#share_list").val());
                $scope.view.show(paramValue, share_list);

            }
        }

        $scope.shareAction();
        
    });
    

})();

var attachUploader = function (button) {
	return new AjaxUpload(button, {
		action: '/common/handler/upload',
		responseType: 'json',
		onChange: function () {
		},
		onSubmit: function (file, ext) {
			this.disable();
		},
		onComplete: function (file, response) {

			this.enable();

			if (response.success) {
				$("#shop_img").val(response.name);
				$("#pathImage").val(response.name.replace(/^.*[\\\/]/, ''));
				$("#btnImage").attr("data-image", 1);
			} else {
				$("#btnImage").attr("data-image", 0);
				alert(response.msg);
			}
		}
	});
}

