﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_about_friends_shop" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
   
    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript" src="/advocate/about/friends-shop/default.js"></script>


</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

        <input type="hidden" id="id" runat="server" />
        <input type="hidden" id="upload_root" runat="server" />
        <input type="hidden" id="share_list" runat="server" />


		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션프렌즈샵(Compassion Friends Shop)</em></h1>
				<span class="desc">가난한 어린이들의 목소리가 되어 활동하는 컴패션 애드보킷을 소개합니다</span>

				<uc:breadcrumb runat="server"/><br />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 advocate">

			<div class="friendsShop">
				<div class="visual_friends tac">
					<p class="tit">아이들의 꿈을 후원합니다</p>
					<span class="bar"></span>
					<p class="s_con3">
						프렌즈샵은 샵(매장)을 운영하는 컴패션 후원자가 본인의 샵을 활용하여 고객들에게 컴패션을 알리고<br />
						어린이들의 꿈을 후원하도록 독려하고 응원하는 공간입니다.
					</p>
					<a ng-click="modalInfo.show()" class="pop_btn">신청안내</a>
					<a ng-click="modal.show()" class="pop_btn">신청하기</a>
				</div>

				<!-- 검색영역 -->
				<div class="shop_search w980 clear2" id="l">
					<div class="search_bar1">
						<label for="k_word" class="label_tit">업체 명 검색</label>
						<input type="text" id="k_word" class="input_type2" ng-enter="search()" style="width:200px" />
						<a ng-click="search()" class="btn_s_type2">검색</a>
					</div>
					<div class="search_bar2">
						<p class="label_tit">업체 명 검색</p>
						<span class="sel_type2" style="width:180px">
							<label for="addr" class="hidden">지역 선택</label>
							<select class="custom_sel" name="" id="addr">
								<option value="">지역</option>
									<option value="서울">서울</option>
									<option value="부산">부산</option>
									<option value="경기">경기</option>
									<option value="인천">인천</option>
									<option value="세종">세종</option>
									<option value="제주">제주</option>
									<option value="대전">대전</option>
									<option value="충청">충청</option>
									<option value="강원">강원</option>
									<option value="경상">경상</option>
									<option value="대구">대구</option>
									<option value="울산">울산</option>
									<option value="광주">광주</option>
									<option value="전라">전라</option>
							</select>
						</span>

						<span class="sel_type2" style="width:138px">
							<label for="type" class="hidden">업종 선택</label>
							<select class="custom_sel" name="" id="type">
								<option value="">업종</option>
								<option value="외식">외식</option>
								<option value="문화,생활">문화,생활</option>
								<option value="쇼핑">쇼핑</option>
								<option value="여가스포츠">여가스포츠</option>
								<option value="의료">의료</option>
								<option value="숙박">숙박</option>
								<option value="교육">교육</option>
								<option value="온라인">온라인</option>
								<option value="기타">기타</option>
							</select>
						</span>
						<a ng-click="search()" class="btn_s_type2">검색</a>
					</div>
				</div>
				<!--// 검색영역 -->

				<!--게시판 리스트-->
				<div class="w980 boardList_1">
					<ul class="list">
						<li ng-repeat="item in list" >
							<span class="img_wh" style="background:url('{{item.shop_img}}') no-repeat center"></span>
							<span class="tit_box">
								<span class="tit">{{item.shop_name}}</span>
                                
								<span class="keyword">{{item.shop_location}} {{item.shop_type}}</span>
							</span>

							<!-- 마우스 오버 시 -->
							<a class="over"><span class="btn_view1" ng-click="view.show(item.idx, item, $event)">VIEW</span></a>
						</li>
											
						<li ng-if="total == 0" class="no_content">검색 결과가 없습니다.</li>
					</ul>
				</div>
				<!--// 게시판 리스트 -->

				<!-- page navigation -->
				<div class="tac mb60">
					<div class="paging_type1">
						<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
					</div>
				</div>
				<!--// page navigation -->

			</div><!--// friendsShop -->

		</div>
		<!--// e: sub contents -->
			
		<div class="h40"></div>
		

    </section>
    <!--// sub body -->

</asp:Content>
