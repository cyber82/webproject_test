﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="advocate_about_friends_shop_guide"  %>

<div style="background:transparent;width:800px;">
	
	<div class="pop_type1 w800">
		<div class="pop_title">
			<span>[<asp:Literal id="shop_type" runat="server"/>]<asp:Literal id="shop_name_title" runat="server"/></span>
			<button class="pop_close" ng-click="view.close($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content shopDetail">

			<div id="shop_pic" class="shop_img" style="background:url('{{view.item.shop_img}}') no-repeat center"></div>

			<div class="tit_area clear2">
				<p class="s_tit5 fl"><asp:Literal id="shop_name" runat="server"/></p>

				<!-- sns 공유하기 -->
				<div class="share_box fl">
					<span class="sns_ani">
						<span class="common_sns_group">
							<span class="wrap">
                                <a href="#" title="페이스북" data-role="sns" data-provider="fb" data-url="" data-title="" data-picture="" class="sns_facebook">페이스북</a>
                                <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" data-url="" data-title="" data-picture="" class="sns_story">카카오스토리</a>
                                <a href="#" title="트위터" data-role="sns" data-provider="tw" data-url="" data-title="" class="sns_twitter">트위터</a>
                                <a href="#" title="url 공유" data-role="sns" data-provider="copy" data-url="" class="sns_url">url 공유</a>
							</span>
						</span>
						<button><span class="common_sns_share">공유하기</span></button>
					</span>
				</div>
				<!--// -->
			</div>

			<p class="s_con3"><asp:Literal id="shop_intro" runat="server" /></p>

			<div class="info_area tac">
                <asp:placeHolder runat="server" id="phAddress" visible="false">
				    <p class="address"><span><asp:Literal id="shop_addr" runat="server" /></span></p>
                </asp:placeHolder>
				<span class="tel mr10"><asp:Literal id="shop_tel" runat="server" /></span>
				<a href="#" target="_blank" class="homepage" id="go_shop" runat="server"><asp:Literal id="shop_homepage" runat="server" /></a>
			</div>

		</div>
	</div>
	

    
</div>
