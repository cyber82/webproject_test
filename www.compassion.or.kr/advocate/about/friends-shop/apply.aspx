﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="apply.aspx.cs" Inherits="advocate_about_friends_shop_apply"  %>

<div style="background:transparent;" class="fn_pop_container" id="popup">
	<!-- 일반팝업 width : 800 -->
	<div class="pop_type1 w800 fn_pop_content" style="height:1492px;padding-top:50px; margin-bottom:50px;">
		<div class="pop_title">
			<span>프렌즈샵 신청</span>
			<button type="button" class="pop_close" ng-click="modal.close($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content shopApply">

			<p class="intro s_con3">후원자님과 같은 마음으로 어린이를 후원하고 계신 후원자님에게 따뜻한 마음을 베풀어 주세요.</p>

			<!-- 등록인 정보 -->
			<div class="input_div">
				<div class="login_field"><span>등록인 정보</span></div>
				<div class="login_input">
					<div class="tableWrap2 mb60">
						<table class="tbl_type1">
							<caption>정보입력 테이블</caption>
							<colgroup>
								<col style="width:17%" />
								<col style="width:83%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="writer">이름</label></th>
									<td><input type="text" id="writer" runat="server"  class="input_type2" value="" style="width:150px;" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="writer_tel">연락처</label></th>
									<td><input type="text" id="writer_tel" runat="server" class="input_type2" value="" maxlength="13" style="width:400px;" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="writer_email">이메일</label></th>
									<td><input type="text" id="writer_email" runat="server" class="input_type2" value="" style="width:400px;" /></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!--// 등록인 정보 -->

			<!-- 샵 정보 -->
			<div class="input_div">
				<div class="login_field"><span>샵 정보</span></div>
				<div class="login_input">
					<div class="tableWrap2 mb60">
						<table class="tbl_type1 line1">
							<caption>정보입력 테이블</caption>
							<colgroup>
								<col style="width:17%" />
								<col style="width:83%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="shop_name">프렌즈샵 명</label></th>
									<td><input type="text" id="shop_name" class="input_type2" value="" style="width:150px;" /></td>
								</tr>
								<tr>
									<th scope="row"><span>업종</span></th>
									<td>
										<span class="radio_ui bizType">
											<input type="radio" id="type1" name="shop_type" value="외식" class="css_radio" checked />
											<label for="type1" class="css_label">외식</label>

											<input type="radio" id="type2" name="shop_type" value="문화생활" class="css_radio" />
											<label for="type2" class="css_label ml40">문화/생활</label>

											<input type="radio" id="type3" name="shop_type" value="쇼핑" class="css_radio" />
											<label for="type3" class="css_label ml40">쇼핑</label>

											<input type="radio" id="type4" name="shop_type" value="여가스포츠" class="css_radio" />
											<label for="type4" class="css_label ml40">여가/스포츠</label>

											<input type="radio" id="type5" name="shop_type" value="의료" class="css_radio" />
											<label for="type5" class="css_label ml40">의료</label>

											<input type="radio" id="type6" name="shop_type" value="교육" class="css_radio" />
											<label for="type6" class="css_label ml40">교육</label>

											<input type="radio" id="type7" name="shop_type" value="숙박" class="css_radio" />
											<label for="type7" class="css_label ml40">숙박</label>

											<input type="radio" id="type8" name="shop_type" value="온라인" class="css_radio" />
											<label for="type8" class="css_label ml40">온라인</label>

											<input type="radio" id="type9" name="shop_type" value="기타" class="css_radio" />
											<label for="type9" class="css_label ml40">기타</label>
										</span>
									</td>
								</tr>
								<tr>
									<th scope="row"><label for="shop_tel">샵 대표번호</label></th>
									<td><input type="text" id="shop_tel" class="input_type2 number_only" value="" style="width:400px;" /></td>
								</tr>
								<tr>
									<th scope="row"><span>주소</span></th>									
									<td>
										<div class="onshop clear2">
											
											<input type="hidden" id="addr1" />
											<input type="hidden" id="addr2" />

											<div class="codeNum">
												<label for="shop_zip" class="hidden">주소찾기</label>
												<input type="text" id="shop_zip" ng-click="modal.popup($event)" class="zipcode input_type2" value="" style="width:150px;background:#fdfdfd;border:1px solid #d8d8d8;" readonly/>
												<button id="search_addr" href="#" ng-click="modal.popup($event)" class="btn_s_type2 ml5">주소찾기</button>
											</div>
											<div class="checkbox_ui">
												<input type="checkbox" class="css_checkbox" id="online"/>
												<label for="online" class="css_label font1">온라인 샵</label>
											</div>
										</div>
										<p class="s_con9 mt5">온라인샵으로 신청하시는 경우, 홈페이지에 사업장 주소가 노출되지 않습니다.</p>
										
										<p id="addr_road" class="fs14 mt10"></p>
										<p id="addr_jibun" class="fs14 mt10"></p>

									</td>									
								</tr>
								<tr>
									<th scope="row"><label for="shop_homepage">홈페이지</label></th>
									<td><input type="text" id="shop_homepage" class="input_type2" value="" style="width:400px;" /></td>
								</tr>
								<tr>
									<th scope="row"><span>참여가능 영역</span></th>
									<td>
										<span class="checkbox_ui">
											<input type="checkbox" class="css_checkbox" id="pros1" name="part" value="컴패션 홍보 물 비치" />
											<label for="pros1" class="css_label font1 mr10">컴패션 홍보물 비치</label>

											<input type="checkbox" class="css_checkbox" id="pros2" name="part" value="후원자 혜택 제공" />
											<label for="pros2" class="css_label font1 mr10">후원자 혜택 제공</label>

											<input type="checkbox" class="css_checkbox" id="pros3" name="part" value="블루보드 설치" />
											<label for="pros3" class="css_label font1 mr10">블루보드 설치</label>

											<input type="checkbox" class="css_checkbox" id="pros4" name="part" value="컴패션 후원자 소규모 행사 장소" />
											<label for="pros4" class="css_label font1">컴패션 후원자 소규모 행사 장소</label>
										</span>
									</td>
								</tr>
								<tr>
									<th scope="row"><label for="shop_intro">샵 소개</label></th>
									<td><textarea id="shop_intro" maxlength="300" class="textarea_type2" rows="18"></textarea></td>
								</tr>
								<tr>
									<th scope="row"><label for="shopPhoto">샵 이미지</label></th>
									<td>
										<div class="btn_attach clear2 relative">
											<input type="hidden" id="shop_img" />

											<input type="text" id="pathImage" class="input_type2 fl mr10" style="width:400px; background:#fdfdfd;border:1px solid #d8d8d8;" disabled />
											<a href="#" id="btnImage" class="btn_s_type3 fl"><span>파일선택</span></a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!--// 샵 정보 -->


			<div class="tac">
				<a href="#" ng-click="modal.request($event)" class="btn_type1">등록하기</a>
			</div>

		</div>
	</div>
	<!--// popup -->
</div>