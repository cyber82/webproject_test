﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

using Microsoft.AspNet.FriendlyUrls;

public partial class advocate_about_friends_shop_guide : FrontBasePage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.ClearContent();
			return;
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context);

		if (!requests[0].CheckNumeric()) {
			Response.ClearContent();
			return;
		}

		base.PrimaryKey = requests[0];

		GetData();
	}


	protected void GetData()
    {
        using (StoreDataContext dao = new StoreDataContext())
        {
            //var entity = dao.friends_shop.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQFStore<CommonLib.friends_shop>("idx", Convert.ToInt32(PrimaryKey));

            shop_name.Text = entity.shop_name;
            shop_intro.Text = entity.shop_intro;
            shop_addr.Text = entity.shop_addr;
            shop_tel.Text = entity.shop_tel;
            shop_homepage.Text = entity.shop_homepage;
            shop_name_title.Text = entity.shop_name;
            shop_type.Text = entity.shop_type;
            //shop_pic.Src = entity.shop_img.WithFileServerHost();

            //go_shop.HRef = "http://" + entity.shop_homepage;
            go_shop.HRef = "http://" + entity.shop_homepage.Replace("http://", "");



            if (entity.shop_homepage == "")
            {
                go_shop.Visible = false;
            }

            phAddress.Visible = !entity.is_online;


            // SNS
            //this.ViewState["meta_url"] = Request.UrlWithoutQueryString() + entity.idx;
            //this.ViewState["meta_title"] = string.Format("[한국컴패션-컴패션프렌즈샵] {0}", entity.shop_name);

            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = "";
            //this.ViewState["meta_keyword"] = "";
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,FrontBasePage)
            //this.ViewState["meta_image"] = entity.thumb.WithFileServerHost();


        }
    }
	
}