﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_intro_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>애드보킷이란?</em></h1>
				<span class="desc">가난한 어린이들의 목소리가 되어 활동하는 컴패션 애드보킷을 소개합니다</span>

				 <uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 advocate">

			<div class="intro tac">

				<div class="notice_bnr" id="recruit_div" runat="server">
					<p><span class="ing">모집중</span><a href="#" id="recruit_text" runat="server">서울 VOC 9기 / 부산 VOC 8기 신청 일정</a></p>
				</div>
				
				<div class="w980 intro_top pt60">
					<div class="advocate_movie">
						<iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/pbb8eZn-16E?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="meaning">
						<h2 class="tit">컴패션 애드보킷이란?</h2>
						<p class="s_con3 pt30">
							가난으로 고통 당하는 전 세계 어린이들을 대신하여 그들의 필요를 적극 알리고,<br />
							컴패션  양육을 통해 꿈을 잃은  어린이들이 꿈을 찾을 수 있도록 자원하여 돕는  후원자들을 가리킵니다.
						</p>
						<p class="mean">애드보킷(Advocate)은‘옹호자’,‘지지자’라는 뜻을 가지고 있습니다.</p>
					</div>
				</div>

				<div class="visual_intro">
					<h2 class="tit">컴패션 애드보킷 파트별 소개</h2>
					<span class="bar"></span>
				</div>

				<div class="w980 intro_bottom">
					<div class="online_advocate mb60">
						<h3 class="s_tit2">컴패션 온라인 애드보킷(Compassion Online Advocate)</h3>
						<p class="s_con6 pt20">
							연약한 어린이들의 목소리가 되어 온라인 상에서 캠페인 참여, 컴패션 활동 공유, SNS 홍보 등을 통하여<br />
							적극적으로 컴패션 사역을 알리는 후원자를 말합니다.
						</p>
						<a href="/advocate/about/apply/" class="btn_s_type6 mt40">온라인 애드보킷 자세히 알아보기</a>
					</div>
					<ul class="advocate_part clear2">
						<li>
							<div class="img"><img src="/common/img/page/advocate/part_FOC1.jpg" alt="FOC이미지" /></div>
							<div class="desc">
								<h3>FOC ( Friends of Compassion )</h3>
								<p>
									각 분야의 CEO, 리더로 구성되며 정기모임, FOC 파티, 바자회 등<br />
									다양한 네트워크와 전문적인 재능을 통해 컴패션 사역을 알립니다.
								</p>
								<a href="foc.aspx" class="btn_s_type6">FOC 자세히 알아보기</a>
							</div>
						</li>
						<li>
							<div class="img"><img src="/common/img/page/advocate/part_VOC1.jpg" alt="VOC이미지" /></div>
							<div class="desc">
								<h3>VOC ( Voice of Compassion )</h3>
								<p>
									컴패션 일반인 홍보대사로서 가난으로 고통 받는 어린이들의 필요를<br />
									알리고 더 많은 어린이가 후원자의 손을 잡을 수 있도록 정기모임, 결연행사,<br />
									결연캠페인, 조별프로젝트 등 일상의 아이디어를 사용하여 어린이들의<br />
									목소리가 되어주는 후원자들입니다.
								</p>
								<a href="voc.aspx" class="btn_s_type6">VOC 자세히 알아보기</a>
							</div>
						</li>
						<li>
							<div class="img"><img src="/common/img/page/advocate/part_YVOC1.jpg" alt="YVOC이미지" /></div>
							<div class="desc">
								<h3>YVOC ( Youth Voice of Compassion )</h3>
								<p>
									한국컴패션 청소년홍보대사로서 정기모임, 다양한 재능기부를 이용한<br />
									나눔캠페인 컴패션동아리 등 자발적이고 창의적인 활동을 통해<br />
									어린이들의 목소리가 됩니다.
								</p>
								<a href="yvoc.aspx" class="btn_s_type6">YVOC 자세히 알아보기</a>
							</div>
						</li>
						<li>
							<div class="img"><img src="/common/img/page/advocate/part_nanumstar1.jpg" alt="컴패션나눔별이미지" /></div>
							<div class="desc">
								<h3>컴패션나눔별</h3>
								<p>
									컴패션나눔별은 어린이집, 유치원, 초/중/고등학생, 동아리 등<br />
									학생들이 힘을 모아 한 어린이를 후원합니다.
								</p>
								<a href="share-star.aspx" class="btn_s_type6">컴패션나눔별 자세히 알아보기</a>
							</div>
						</li>
						<li>
							<div class="img"><img src="/common/img/page/advocate/part_band1.jpg" alt="컴패션밴드이미지" /></div>
							<div class="desc">
								<h3>컴패션밴드(Compassion Band)</h3>
								<p>
									한국컴패션의 후원자 중 공연과 관련된 재능을 가진 사람들이 모여<br />
									더 많은 어린이들이 사랑으로 양육될 수 있도록, 공연과 여러가지 봉사활동으로<br />
									재능을 나누는 자원봉사 모임입니다.
								</p>
								<a href="/advocate/about/band/" class="btn_s_type6">컴패션밴드 자세히 알아보기</a>
							</div>
						</li>
						<li>
							<div class="img"><img src="/common/img/page/advocate/part_friends_shop1.jpg" alt="컴패션프렌즈샵이미지" /></div>
							<div class="desc">
								<h3>컴패션프렌즈샵(Compassion Friends Shop)</h3>
								<p>
									매장을 운영하는 후원자가 자신의 샵을 활용하여 고객들에게 컴패션을 알리고<br />
									어린이들의 꿈을 후원하도록 독려하는 공간입니다.
								</p>
								<a href="/advocate/about/friends-shop/" class="btn_s_type6">컴패션프렌즈샵 자세히 알아보기</a>
							</div>
						</li>
					</ul>
				</div>


			</div>
			
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->
    
</asp:Content>