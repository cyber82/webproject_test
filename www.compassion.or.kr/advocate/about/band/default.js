﻿


(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, popup, paramService) {

        // 블루북 신청
        $scope.modal = {
            instance: null,

            init: function () {
                // 팝업
            	popup.init($scope, "/advocate/about/band/apply", function (modal) {
                	$scope.modal.instance = modal;

					
                	var uploader = attachUploader("btnDoc");
                	uploader._settings.data.fileDir = $("#upload_root").val();
                	uploader._settings.data.fileType = "file";
                	uploader._settings.data.limit = 10240;
                	
                	uploader = attachUploader("btnMovie");
                	uploader._settings.data.fileDir = $("#upload_root").val();
                	uploader._settings.data.fileType = "movie";
                	uploader._settings.data.limit = 20480;
					
                	$(".custom_sel").selectbox({

                	})

                });
            },


            show: function () {
                if (!$scope.modal.instance)
                    return;

                if (common.checkLogin()) {
                    $scope.modal.instance.show();
                }

            },

            request: function ($event) {

            	if ($("#up_file2").val() == "") {
            		$("#up_file2_msg").show();
            		$("#up_file2_msg").focus();
            		return false;
            	} else {
            		$("#up_file2_msg").hide();
            	}

				
            	if ($("#ucc").val() == "") {
            		$("#ucc_msg").show();
            		$("#ucc_msg").focus();
            		return false;
            	} else {
            		$("#ucc_msg").hide();
            	}
				

                if ($scope.requesting) return;
                $scope.requesting = true;

                var param = {
                	up_file2: $("#up_file2").val(),
                	ucc: $("#ucc").val(),
                	tel_call_time: $("#tel_call_time").val()
                };

                $http.post("/api/advocate.ashx?t=band_apply", param).success(function (r) {
                	console.log(r);
                    $scope.requesting = false;
                    if (r.success) {
                    	alert($("#name").val() + "님의 컴패션 밴드 지원이 완료되었습니다.\n추후 면접 대상자에 한해 개별 연락드릴 예정입니다.\n별도의 불합격 안내는 없는 점 양해 말씀드립니다.\n컴패션밴드를 향한 관심과 사랑에 늘 감사 드립니다.");
                    } else {
                    	alert(r.message);
                    }
                    $scope.modal.close($event);

                });
            },

            close: function ($event) {

                if (!$scope.modal.instance)
                    return;

                $scope.modal.instance.hide();
                $event.preventDefault();
            }

        }
        $scope.modal.init();

    });

})();

var attachUploader = function (button) {
	return new AjaxUpload(button, {
		action: '/common/handler/upload',
		responseType: 'json',
		onChange: function () {
		},
		onSubmit: function (file, ext) {
			this.disable();
		},
		onComplete: function (file, response) {

			this.enable();

			if (response.success) {

				var type = $("#" + button).attr("data-type");
				if (type == "doc") {
					$("#up_file2").val(response.name);
					$("#file_doc").val(response.name.replace(/^.*[\\\/]/, ''));
				} else {
					$("#ucc").val(response.name);
					$("#file_movie").val(response.name.replace(/^.*[\\\/]/, ''));
				}
			} else
				alert(response.msg);
		}
	});
}