﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="apply.aspx.cs" Inherits="advocate_band_apply"  %>

<div style="background: transparent; width:800px;" class="fn_pop_container" id="popup">
<!-- 일반팝업 width : 800 -->
	<div class="pop_type1 w800 fn_pop_content" >
		<div class="pop_title">
			<span>컴패션밴드 지원하기</span>
			<button class="pop_close" ng-click="modal.close($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content combandApply">
			<div class="pop_common">
				<div class="tableWrap2 mb30">
					<table class="tbl_type1">
						<caption>정보입력 테이블</caption>
						<colgroup>
							<col style="width:17%" />
							<col style="width:83%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><label for="name">지원자 이름</label></th>
								<td><div class="s_con3"><asp:Literal runat="server" id="lbName" /><input type="hidden" runat="server" id="name" maxlength="10" /></div></td>
							</tr>
							<tr>
								<th scope="row"><label for="apply">지원서 올리기</label></th>
								<td>
									<div class="btn_attach clear2 relative">
										<input type="hidden" id="up_file2" runat="server" />
										<input type="text" runat="server" id="file_doc" disabled maxlength="10" value="선택된 파일 없음" class="input_type2 fl mr10" style="width:266px;background:#fdfdfd;" />
										<a href="#" id="btnDoc" data-type="doc" class="btn_s_type3 fl"><span>파일선택</span></a>
									</div>
									<div class="s_con1">파일 용량을 10MB 이하로 올려 주세요.</div>
									<p id="up_file2_msg" class="mt5" style="display:none;"><span class="guide_comment2">밴드 신청서를 올려주세요.</span></p>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="source">첨부자료<br />올리기</label></th>
								<td>
									<div class="btn_attach clear2 relative">
										<input type="hidden" id="ucc" runat="server" />
										<input type="text" runat="server" id="file_movie" disabled maxlength="10" value="선택된 파일 없음" class="input_type2 fl mr10" style="width:266px;background:#fdfdfd;" />
										<a href="#" id="btnMovie" data-type="movie" class="btn_s_type3 fl"><span>파일선택</span></a>
									</div>
									<div class="s_con1">ZIP, MP4, WMV, AVI만 가능합니다. 20MB 이하의 파일만 올려 주세요.</div>
									<p id="ucc_msg" class="mt5" style="display:none;"><span class="guide_comment2">파일을 첨부해 주세요.</span></p>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="time">연락 가능 시간</label></th>
								<td>
									<span class="sel_type2" style="width:150px">
										<select class="custom_sel" id="tel_call_time" name="tel_call_time">
											<option value="AM10:00">10:00 AM</option>
											<option value="AM11:00">11:00 AM</option>
											<option value="PM12:00">12:00 PM</option>
											<option value="PM01:00">01:00 PM</option>
											<option value="PM02:00">02:00 PM</option>
											<option value="PM03:00">03:00 PM</option>
											<option value="PM04:00">04:00 PM</option>
											<option value="PM05:00">05:00 PM</option>
											<option value="PM06:00">06:00 PM</option>
										</select>
									</span>
								</td>
							</tr>
						</tbody>
					</table>
                <div class="s_con1">지원자의 소중한 개인정보는 본 목적 외에 사용하지 않을 것을 약속 드립니다.</div>
				</div>
			</div>

			<div class="tac">
				<a ng-click="modal.request($event)" class="btn_type1">제출</a>
			</div>

		</div>
	</div>
	<!--// popup -->
</div>