﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_band_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript" src="/advocate/about/band/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	
    <!-- sub body -->
    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
        <input type="hidden" runat="server" id="upload_root" value="" />
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션밴드(Compassion Band)</em></h1>
				<span class="desc">가난한 어린이들의 목소리가 되어 활동하는 컴패션 애드보킷을 소개합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="band tac">

				<div class="w980 intro">

					<p class="advo_tit">어린이들의 희망을 노래하는 <em>컴패션밴드</em></p>
					<p class="advo_con">컴패션밴드는 국제어린이양육기구 한국컴패션의 후원자 중<br />
					공연과 관련된 재능을 가진 사람들이 모여 더 많은 어린이들이 사랑으로 양육될 수 있도록,<br />
					공연과 여러가지 봉사활동으로 재능을 나누는 <em class="fc_blue">100% 자원봉사 모임</em>입니다.</p>
					<div class="mt40">
						<iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/TtHGCtcD8IA?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
					</div>
					<ul class="band_act">
						<li>
							<p class="s_tit7">
								<span class="icon"></span>
								그 시작은 미약하였으나…
							</p>
							<p class="s_con3">
								2006년 4월, 배우 차인표 후원자를 비롯한 6명으로 시작해 지금은 다양한 재능을 가진 수 많은 사람들이 전 세계 어린이들을 향한 마음 하나로 모였습니다.<br />
								연예인, 프로듀서, 엔지니어, 안무가 등 직업은 다양하지만 어린 생명을 살리고, 함께 꿈을 키워가는 기쁨을 전하기 위해 춤추고 노래합니다.
							</p>
						</li>
						<li>
							<p class="s_tit7">
								<span class="icon"></span>
								예수님의 사랑을 전하는 사람들!
							</p>
							<p class="s_con3">
								컴패션밴드는 가난으로 꿈꿀 수 없었던 어린이들을 가슴에 품고 섬기며 컴패션선데이 등 컴패션의 각종 행사와 자원봉사,<br />
								비전트립을 통해 더 많은 어린이들이 후원자님의 큰 사랑과 기도 안에서 자랄 수 있기를 기도합니다.
							</p>
						</li>
						<li class="mb60">
							<p class="s_tit7">
								<span class="icon"></span>
								가장 낮은 곳에서 가장 높이 찬양합니다.
							</p>
							<p class="s_con3">
								우리가 그리스도 안에서 한 형제, 자매임을 믿고 서로 사랑하며<br />
								귀한 섬김으로 마음을 더해줄 열정 가득한 신입단원을 모집합니다.
							</p>
						</li>
					</ul>
				</div>


				<div class="band_recruit bg_bottom">

					<div class="bgContent">
						<h2 class="sub_tit">컴패션밴드 신입단원 수시모집</h2>
						<span class="bar"></span>
						<h3 class="s_tit7">지원 자격</h3>
						<p class="s_con3 fc_sky">
							공연을 통해 자신의 재능을 헌신할 수 있는 분<br />
							컴패션 사역의 가치를 이해하고 섬기고자 하시는 분<br />
							현재 혹은 향후 컴패션 후원을 통해 어린이를 돕고자 하는 분
						</p>
					</div>

					<div class="w980 band_apply">
						<h3 class="s_tit7">지원방법</h3>

						<div class="howTo">
							<ul class="clear2">
								<li class="s_con3">
									<span>STEP 1</span>
									<p>밴드 지원서 파일을 다운받아<br />양식에 맞게 입력해 주세요.</p>
									<a href="/common/download/compassion_band.zip" class="btn_s_type4">지원서 양식 및 지정곡 다운로드</a>
								</li>
								<li class="s_con3">
									<span>STEP 2</span>
									<p>아래의 [컴패션밴드 지원하기] 버튼을 눌러<br />본인이 노래하거나 공연하는 모습이 담긴<br />동영상 파일을 첨부해 주세요.<br />영상 파일을 반드시 함께 제출해 주셔야<br />접수가 정상적으로 완료됩니다.</p>
								</li>
								<li class="s_con3">
									<span>STEP 3</span>
									<p>보컬 파트 지원자의 경우 자유곡 1곡과<br />컴패션밴드 1,2집 앨범에 수록된 아래<br />지정곡 중 1곡을 반드시 첨부해 주세요.</p>
								</li>
							</ul>
						</div>

						<div class="song_wrap">
							<div class="song">
								<p class="tit">지정곡</p>
								<p class="con">
									남성 지정곡 – 사랑하기 때문에, 혼자가 아니에요, 리카에게, Just a minute, 어딘가요<br />
									여성 지정곡 – 사랑하기 때문에, 새로운 날, 리카에게, Just a minute, 우리 다시 만난다면, MAMA SONG
								</p>
							</div>
						</div>


						<h3 class="s_tit7">심사 및 결과 안내</h3>
						<p class="s_con3">지원서 확인 후 컴패션밴드 리더들의 회의를 거쳐, 면접 대상자로 선정되신 분에 한해 추후 개별 연락드립니다.</p>
						<p><span class="s_con1">면접 대상자는 지원자료 검토 후, 컴패션밴드의 목적과 팀원 구성에 적합하다고 판단되는 분입니다.</span></p>

						<a ng-click="modal.show()" class="btn_type1 mt20">컴패션밴드 지원하기</a>
					</div>
				</div>


			</div>
		</div>
		

		<!--// e: sub contents -->

		

    </section>
    <!--// sub body -->

    </section>

    
</asp:Content>