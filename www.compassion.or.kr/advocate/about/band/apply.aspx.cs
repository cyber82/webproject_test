﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class advocate_band_apply : FrontBasePage {
	


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		if (!UserInfo.IsLogin) {
			Response.ClearContent();
			return;
		}


		UserInfo sess = new UserInfo();
		// 이름
		if (sess.UserName != "") {
			lbName.Text = sess.UserName;
			name.Value = sess.UserName;
		}

	}
}