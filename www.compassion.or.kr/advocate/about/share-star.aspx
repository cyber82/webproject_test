﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="share-star.aspx.cs" Inherits="advocate_intro_star" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	
				
	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션나눔별</em></h1>
				<span class="desc">가난한 어린이들의 목소리가 되어 활동하는 컴패션 애드보킷을 소개합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="star tac">

				<div class="w980 intro">

					<p class="advo_tit">우리 반에 <em>별 하나</em>를 달아주세요!</p>
					<p class="advo_con">꿈을 잃은 어린이에게 보내는 사랑이 한 어린이의 별이 될 때, 우리 반 학생들의 마음에도 별 하나가 뜹니다.<br />사랑, 그 이상의 나눔</p>
					<div class="mt40">
						<iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/6iH-Ipmzl6k?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
					</div>
					<p class="advo_con padding">컴패션 나눔별은 유치원, 초/중/고등학생, 동아리 등 학생들이 힘을 모아 한 어린이를 도울 수 있는 방법입니다.</p>
					
					<ul class="star_act clear2">
						<li>
							<p class="s_tit7">
								<span class="icon"></span>
								한 어린이의 삶을 바꾸는<br />
								<em>사랑의 실천</em>
							</p>
							<p class="s_con3">
								반/동아리별로, 매월 4만 5천 원의 후원금을<br />후원 어린이에게 보냅니다.
							</p>
						</li>
						<li>
							<p class="s_tit7">
								<span class="icon"></span>
								편지로 주고 받는<br />
								<em>꿈과 희망의 이야기</em>
							</p>
							<p class="s_con3">
								후원 어린이와의 편지 교환을 통해<br />꿈과 희망을 함께 찾아갑니다.
							</p>
						</li>
						<li>
							<p class="s_tit7">
								<span class="icon"></span>
								‘1:1 어린이 양육’ 이라는<br />
								<em>특별한 가치 체험</em>
							</p>
							<p class="s_con3">
								후원 어린이와의 만남으로 가난의 현실을<br />돌아보고, 가난에 맞서는 컴패션의 방법과<br />가치를 통해 비전을 발견합니다.
							</p>
						</li>
					</ul>
				</div>


				<div class="star_apply">					
					<h2 class="sub_tit">컴패션나눔별을 시작해 주세요!</h2>
					<span class="bar"></span>
					<ul class="howTo clear2">
						<li>
							<span>STEP 1</span>
							<div class="tit">동의</div>
							<p>선생님/친구들에게 컴패션<br />나눔별을 소개하시고,<br />결연 동의 여부를 확인해 주세요.</p>
						</li>
						<li>
							<span>STEP 2</span>
							<div class="tit">신청</div>
							<p>홈페이지에서 신청서를 내려 받은 뒤,<br />우편이나 팩스로 보내주세요.</p>
						</li>
						<li>
							<span>STEP 3</span>
							<div class="tit">후원</div>
							<p>즐거운 후원 활동이 시작됩니다.<br />편지는 가급적 한 장에 모아 영어로 작성해 주세요.<br />편지를 주고 받는 시간이 훨씬 빨라집니다.</p>
						</li>
					</ul>
					<div class="bt_area clear2">
						<a href="/common/download/Compassion_Star_Application.zip" class="downBt">컴패션나눔별 신청서 다운로드</a>
						<a href="/common/download/Compassion_Star_Introduction.zip" class="downBt">컴패션나눔별 소개서 다운로드</a>
					</div>
					<p class="con">신청서를 작성하신 후, 한국컴패션 나눔별 담당자 앞으로 우편이나 팩스로 보내주세요.</p>
				</div>


				<!-- 문의 -->
				<div class="contact_qna w980 div3">
					<p class="tit">컴패션나눔별 신청/문의</p>
					<div class="clear2">
						<div class="tel"><span class="icon"><span class="txt">전화 : </span>02) 3668-3436</span></div>
						<div class="fax"><span class="icon"><span class="txt">팩스 : </span>02) 3668-3501</span></div>
						<div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:sham@compassion.or.kr">sham@compassion.or.kr</a></span></div>
					</div>
				</div>
				<!--//  -->

				
				<!-- 공지/공유 바로가기 -->
				<div class="act_news w980">
					<p class="tit">나눔별의 자세한 활동 소식이 궁금하신가요?</p>
					<div class="board clear2 tal">
						<div class="box_type4 fl">
							<p class="s_tit1">공지 게시판</p>
							<p class="con">나눔별 활동 공지 게시판 입니다.</p>
							<a href="/advocate/notice/?b_sub_type=share" class="btn_s_type3">바로 가기</a>
						</div>
						<div class="box_type4 fr">
							<p class="s_tit1">공유 게시판</p>
							<p class="con">나눔별 활동 공유 게시판 입니다.</p>
							<a href="/advocate/share/?category=share" class="btn_s_type3">바로 가기</a>
						</div>
					</div>
				</div>
				<!--//  -->

			</div>
		</div>
		

		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

    
</asp:Content>