﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CommonLib;

public partial class advocate_intro_apply_finish : FrontBasePage {
	


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		if (base.IsRefresh) {
			return;
		}

		if (Request.UrlReferrer == null || Request.UrlReferrer.ToString().IndexOf("/advocate/about/apply/step3") < 0)
        {
            Response.Redirect("/advocate/about/apply/step1");
		} else
        {
            CommonLib.WWW6Service.SoaHelperSoap _www6Service;
            _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

            // 3번까지 다 푼 회원 애드보킷 등록
            using (FrontDataContext dao = new FrontDataContext())
            {
                UserInfo sess = new UserInfo();
                var exist = www6.selectQ<advocate_user>("au_user_id", sess.UserId);
                //if (!dao.advocate_user.Any(p => p.au_user_id == sess.UserId))
                if(!exist.Any())
                {
                    var entity = new advocate_user()
                    {
                        au_user_id = sess.UserId,
                        au_from = CodeAction.ChannelType,
                        au_regdate = DateTime.Now
                    };

                    //dao.advocate_user.InsertOnSubmit(entity);
                    www6.insert(entity);
                    //dao.SubmitChanges();
                }
            }
		}
	}
}