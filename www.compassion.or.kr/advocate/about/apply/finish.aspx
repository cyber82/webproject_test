﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="finish.aspx.cs" Inherits="advocate_intro_apply_finish" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />


	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션 온라인 애드보킷 지원</em></h1>
				<span class="desc">온라인 애드보킷이 되어 컴패션의 가치를 알려주세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="on_advocate tac">
				<div class="apply">					
					<div class="w980 apply_intro">
						<p class="advo_tit"><em>온라인 애드보킷</em>이 되신 걸 축하드립니다!</p>
						<p class="advo_con">다양한 활동으로 컴패션의 양육가치를 널리 알려 주세요</p>
						<span class="img"></span>
					</div>

					<div class="on_advocate_role">
						<p class="tit">온라인 애드보킷은</p>
						<span class="bar"></span>

						<ul class="relative w980">
							<li class="r1">
								<p class="tit">Compassion A Day</p>
								<p class="con">소중한 컴패션어린이를 친구에게<br />
								추천할 수 있어요.</p>
								<a href="/advocate/together/compassion-a-day/" class="btn_s_type3">바로가기</a>
							</li>
							<li class="r2">
								<p class="tit">컴패션블루보드</p>
								<p class="con">회사, 사업장, 교회에 컴패션을<br />
								소개할 수 있는 자료가 담긴 컴패션<br />
								블루보드를 제공받을 수 있어요. </p>
								<a href="/advocate/together/blueboard/" class="btn_s_type3">바로가기</a>
							</li>
							<li class="r3">
								<p class="tit">컴패션 DIY</p>
								<p class="con">직접 후원 프로그램을 기획하고<br />
								진행할 수 있어요.</p>
								<a href="/advocate/together/diy/" class="btn_s_type3">바로가기</a>
							</li>
							<li class="r4">
								<p class="tit">컴패션 애드보킷 캠페인</p>
								<p class="con">시즌별로 진행되는 캠페인 행사를<br />
								직접 기획하고 주최할 수 있어요.</p>
								<a href="/advocate/together/campaign/" class="btn_s_type3">바로가기</a>
							</li>
							<li class="r5">
								<p class="tit">컴패션 천만 명에게 알리기</p>
								<p class="con">많은 사람들에게 컴패션의 가치를<br />
								공유할 수 있어요.</p>
								<a href="/advocate/together/notify/" class="btn_s_type3">바로가기</a>
							</li>
						</ul>
					</div>

					<div class="we_com w980">
						<p>우리는 모두 <em>컴패션</em>입니다</p>
					</div>
				</div>
			</div>
			
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

    
</asp:Content>