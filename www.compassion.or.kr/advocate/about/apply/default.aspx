﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_intro_apply_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션 온라인 애드보킷</em></h1>
				<span class="desc">가난한 어린이들의 목소리가 되어 활동하는 컴패션 애드보킷을 소개합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="on_advocate tac">
				<div class="w980 intro">
					<p class="advo_tit"><em>온라인 애드보킷은</em> 연약한 어린이들의 목소리가 되어</p>
					<p class="advo_con">온라인 상에서 캠페인 참여, 컴패션 활동 공유, SNS 홍보 등을 통하여<br />
					적극적으로 컴패션 사역을 알리는 후원자를 말합니다.</p>
					<div class="img"></div>
				</div>

				<div class="bgContent act_eligible">
					<div class="w980">
						<p class="tit">온라인 애드보킷 활동 자격</p>
						<span class="bar"></span>
						<p class="con">
							어린이의 목소리가 되고자 하는 뜨거운 마음이 있어야 합니다.<br /><br />
							후원자님의 이름으로 컴패션에 후원한 이력이 있는 분이어야 합니다. (온라인 가입 시 확인 가능합니다.)<br /><br />
							온라인으로 진행되는 동영상 교육 과정을 이수해야 합니다. 이 과정은 필수입니다.
						</p>
						<div class="con2">
							<div class="s_con1">컴패션을  배울 수 있는 3개의 동영상 시청으로 진행되며, 20~25분 정도 소요됩니다.</div>
							<div class="s_con1">각 단계별 영상시청 후 간단한 퀴즈를 통해 컴패션 이해 정도를 확인합니다.</div>
							<div class="s_con1">모든 교육이 끝나면 바로 온라인 애드보킷으로 활동(온라인 CAD, 활동 패킷 신청 등)하실 수 있습니다.</div>
						</div>
						<a href="/advocate/about/apply/step1" class="btn_b_type2">온라인 애드보킷 지원하기</a>
					</div>
				</div>
				
				<div class="act_news w980">
					<p class="tit">온라인 애드보킷의 자세한 활동 소식이 궁금하신가요?</p>
					<div class="board clear2 tal">
						<div class="box_type4 fl">
							<p class="s_tit1">공지 게시판</p>
							<p class="con">온라인 애드보킷 활동 공지 게시판 입니다.</p>
							<a href="/advocate/notice/" class="btn_s_type3">바로 가기</a>
						</div>
						<div class="box_type4 fr">
							<p class="s_tit1">공유 게시판</p>
							<p class="con">온라인 애드보킷 활동 공유 게시판 입니다.</p>
							<a href="/advocate/share/?sorting=online" class="btn_s_type3">바로 가기</a>
						</div>
					</div>
				</div>

			</div>
			
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

    
</asp:Content>