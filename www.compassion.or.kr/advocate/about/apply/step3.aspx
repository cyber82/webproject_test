﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="step3.aspx.cs" Inherits="advocate_intro_apply_step3" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        $(function () {

            $("#btnAnswer").click(function () {
                if ($("input[name=answer]:checked").length < 1) {
                    alert("답을 선택해 주세요.");
                    return false;
                }

                if ($("input[name=answer]:checked").val() != "answer3") {
                    $("#answer_message").show();
                    return false;
                }

                location.href = "/advocate/about/apply/finish";
            });
            
        })

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션 온라인 애드보킷 지원</em></h1>
				<span class="desc">온라인 애드보킷이 되어 컴패션의 가치를 알려주세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="on_advocate tac w980">
				<div class="quiz">
					<div class="quiz_intro">
						<p class="advo_tit"><em>컴패션의 가치</em>를 전하는 온라인 애드보킷이</p>
						<p class="advo_con">꼭 갖춰야 할 배경 지식을 학습하는 과정입니다.</p>
						<span class="bar"></span>
						<p class="s_con3">“ 간단한 퀴즈를 통해 온라인 애드보킷으로서의 걸음을 시작해 보아요! ”</p>
					</div>

					<!-- step -->
					<div class="stepWrap">
						<ul class="clear2">
							<li class="arr">
								<span class="bg bg1"></span>
								<span class="txt">STEP 01<br /><span class="txt2">컴패션의 시작</span></span>
							</li>
							<li class="arr">
								<span class="bg bg2"></span>
								<span class="txt">STEP 02<br /><span class="txt2">컴패션의 투명성</span></span>
							</li>
							<li class="on">
								<span class="bg bg3"></span>
								<span class="txt">STEP 03<br /><span class="txt2">어린이 양육</span></span>
							</li>
						</ul>
					</div>
					<!--// step -->

					<div class="quiz_movie">
						<iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/bz7LX8gOXoc?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
					</div>

					<div class="quiz_area">
						<div class="question">
							<p class="s_tit7">컴패션에서 후원을 받게 된 세 친구의 삶은 어떻게 바뀌었나요?</p>
						</div>
						<div class="answer_box mb40">
							<div class="answer3">
								<span class="radio_ui">
									<input type="radio" name="answer" value="answer1" id="answer1" class="css_radio" />
									<label for="answer1" class="css_label mb10">가정에 경제적인 지원이 이루어졌으며 학교에 갈 수 있게 되었다. </label>

									<input type="radio" name="answer" value="answer2" id="answer2" class="css_radio" />
									<label for="answer2" class="css_label mb10">계속 광산에 나가서 일을 해야 하지만 컴패션어린이센터 양육프로그램에 정기적으로 참석하고 있다. </label>

									<input type="radio" name="answer" value="answer3" id="answer3" class="css_radio" />
									<label for="answer3" class="css_label">더 이상 광산에서 일하지 않으며 컴패션어린이센터 양육프로그램, 정기 건강검진, 소득창출교육을 받게 되었다.</label>
								</span>
							</div>							
						</div>
						<a id="btnAnswer" class="btn_type1 mb40">확인</a>
						<div id="answer_message" style="display:none;"><span class="comment">앗!  정답이 아닙니다.  다른 답을 입력해 보세요~</span></div>
					</div>					
				</div>
			</div>
			
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

    
</asp:Content>