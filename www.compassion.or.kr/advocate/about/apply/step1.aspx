﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="step1.aspx.cs" Inherits="advocate_intro_apply_step1" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        $(function () {
            $("#answer").keydown(function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    if (onSubmit()) {
                        eval($("#btnAnswer").attr("href").replace("javascript:", ""));
                    }
                }
            })
        });

        var onSubmit = function () {
            if ($("#answer").val() == "") {
                alert("답을 입력해주세요.");
                $("#answer").focus();
                return false;
            }

            if ($("#answer").val() != "한국") {
                $("#answer_message").show();
                return false;
            }

            return true;
        }
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />


	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션 온라인 애드보킷 지원</em></h1>
				<span class="desc">온라인 애드보킷이 되어 컴패션의 가치를 알려주세요</span>

				   <uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="on_advocate tac w980">
				<div class="quiz">
					<div class="quiz_intro">
						<p class="advo_tit"><em>컴패션의 가치</em>를 전하는 온라인 애드보킷이</p>
						<p class="advo_con">꼭 갖춰야 할 배경 지식을 학습하는 과정입니다.</p>
						<span class="bar"></span>
						<p class="s_con3">“ 간단한 퀴즈를 통해 온라인 애드보킷으로서의 걸음을 시작해 보아요! ”</p>
					</div>

					<!-- step -->
					<div class="stepWrap">
						<ul class="clear2">
							<li class="arr on">
								<span class="bg bg1"></span>
								<span class="txt">STEP 01<br /><span class="txt2">컴패션의 시작</span></span>
							</li>
							<li class="arr">
								<span class="bg bg2"></span>
								<span class="txt">STEP 02<br /><span class="txt2">컴패션의 투명성</span></span>
							</li>
							<li>
								<span class="bg bg3"></span>
								<span class="txt">STEP 03<br /><span class="txt2">어린이 양육</span></span>
							</li>
						</ul>
					</div>
					<!--// step -->

					<div class="quiz_movie">
						<iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/cNX_DzClfZ4?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
					</div>

					<div class="quiz_area">
						<div class="question">
							<p class="s_tit7">컴패션이 시작된 나라는 어디일까요?</p>
						</div>
						<div class="answer_box mb40">
							<div class="answer1">
								<span class="txt_bg">컴패션은 1952년,</span>
								<label for="quiz1_a" class="hidden">정답 입력</label>
								<input type="text" id="answer" maxlength="2" class="input_type1 fs16" style="width:153px" />
								<span>의 전쟁고아를 본 에버렛 스완슨 목사의 결단으로 시작되었다.</span>
							</div>							
						</div>
						<asp:LinkButton id="btnAnswer" runat="server" OnClick="btnAnswer_Click" OnClientClick="return onSubmit()" class="btn_type1 mb40">확인</asp:LinkButton>
						<div id="answer_message" style="display:none;"><span class="comment">앗!  정답이 아닙니다.  다른 답을 입력해 보세요~</span></div>
					</div>					
				</div>
			</div>
			
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

</asp:Content>