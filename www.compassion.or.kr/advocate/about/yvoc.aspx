﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="yvoc.aspx.cs" Inherits="advocate_intro_yvoc" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" runat="server" id="hfAddressType" value="" />

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>YVOC(Youth Voice of Compassion)</em></h1>
				<span class="desc">가난한 어린이들의 목소리가 되어 활동하는 컴패션 애드보킷을 소개합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="yvoc tac">

				<div class="w980 intro">

					<p class="advo_tit"><em>YVOC는 컴패션 청소년 홍보대사로서</em></p>
					<p class="advo_con">
						가난으로 고통 받는 어린이들의 필요를 적극적으로 알립니다. 나눔에 있어, 모든 영역에서 끝까지 양육하는<br />
						컴패션 사역의 중요성을 전하며, 다음 세대에 컴패션무브먼트를 일으켜 건전한 나눔문화를 선도합니다.<br />
						이를 통하여 어린이들이 더 많이 양육받을 수 있도록  돕습니다.
					</p>
					<div class="mt40">
						<iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/ysU2DYy6bu4?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
					</div>

					<h2 class="sub_tit">YVOC의 활동 자격</h2>
					<p class="sub_con">
						예수 그리스도의 사랑을 품고 가난으로 고통받는 어린이들의 필요를 적극적으로 알리고자 하며,<br />
						아래 항목 중 1개 이상, 그리고 필수조건에 해당하는 대한민국 거주 중1~고3학생
					</p>

					<ul class="license clear2">
						<li><span class="s_con3">필수조건: 매월 첫째, 셋째주 토요일 오전 YVOC 정기모임에 참석이 가능한자</span></li>
						<li><span class="s_con7">컴패션의 자원봉사자(컴패션메이트)로 참여하고 있는 중 &middot; 고등학생</span></li>
						<li><span class="s_con7">컴패션을 통해 본인 또는 가족이 어린이를 후원하고 있는 중 &middot; 고등학생</span></li>
						<li><span class="s_con7">컴패션나눔별 동아리 참여 중인 중 &middot; 고등학생</span></li>
						<li><span class="s_con7">컴패션을 더 알고 나눔의 마음을 가진 리더로 성장하고 싶은 중 &middot; 고등학생</span></li>
						<li><span class="s_con7">컴패션 비전트립에 참여한 중 &middot; 고등학생</span></li>
					</ul>
				</div>

				<div class="yvoc_role">					
					<h2 class="sub_tit">YVOC의 역할</h2>
					<span class="bar"></span>
					<ul class="s_con3">
						<li>본인이 속한 학교에서 가난으로 고통받는 어린이들의 실상을 알리고 이를 해결하기 위해 친구들의 동참을 이끌어 냅니다.</li>
						<li>후원자들이 도움이 필요한 어린이들의 손을 잡을 수 있도록 컴패션 사역 홍보 및 모금활동을 YVOC 자체적으로 기획/진행합니다.</li>
						<li>컴패션 사역과 어린이들의 기도 후원자 역할을 합니다.</li>
					</ul>
				</div>

				<div class="yvoc_term w980">
					<h2 class="sub_tit">YVOC의 활동 기간</h2>
					<span class="bar"></span>
					<p class="s_con3">
						매 년 초 오리엔테이션을 시작으로 <em>정기프로그램 일정이 끝나는 다음 해 2월까지</em><br />
						한국컴패션의 청소년 홍보대사 자격을 부여합니다.
					</p>
				</div>

				<div class="yvoc_organize">
					<div class="visual">
						<h2 class="sub_tit">YVOC 조직</h2>
						<span class="bar"></span>
						<p class="s_con3">
							거주지역에 따라 나눠진 6개 지부 모임을 중심으로 운영되며, 본인의 재능 및 취미에 따라 선택적으로 참여할 수 있는<br />
							팀 활동을 통해 다양한 교제와 재능기부의 기회가 주어집니다.  또한, 각 지부 및 팀에 YVOC 대학생 멘토가 지정되어 있어 YVOC사역을 지원할 뿐 아니라<br />
							개인의 진로와 인생에 대한 고충을 함께 고민하고 중보해주는 멘토링을 지원합니다.
						</p>
					</div>
					<div class="sect clear2 w980">
						<div class="local">
							<div class="img"></div>
							<div class="txt_area">
								<p class="s_tit2">YVOC 지부</p>
								<p class="s_con6">서울 남부/ 북부/동부/서부 지부, 경기 남부/서부,<br />연합지부(기타지역)</p>
							</div>
						</div>
						<div class="team">
							<div class="img"></div>
							<div class="txt_area">
								<p class="s_tit2">YVOC 팀</p>
								<p class="s_con6">음악팀, 공연팀(안무, 연극), 미술팀, 영상팀,<br />YVOC 기자단, YVOC 스피커팀</p>
							</div>
						</div>
					</div>
				</div>


				<div class="yvoc_act w980">
					<h2 class="sub_tit">YVOC 주요 활동</h2>
					<ul>
						<li>							
							<h3 class="s_tit6">VOC 정기모임 및 비전트립</h3>
							<p class="s_con3">
								컴패션의 철학과 가치를 바탕으로 전인적 양육 커리큘럼을 축약해 재구성한 정기모임 프로그램을 통해 컴패션 사역과 가난의<br />
								실상에 대해 이해하고 배웁니다. 또한  수혜국 양육현장인 컴패션어린이센터를 방문하는 비전트립을 통해 컴패션의 전인적 양육을 실제로 보고<br />
								어린이와의 만남을 통해 섬김과 나눔의 마음을 가진 글로벌리더로 성장할 수 있는 기회를 갖게 됩니다.
							</p>
						</li>
						<li>
							<h3 class="s_tit6">YVOC 나눔프로젝트</h3>
							<p class="s_con3">
								컴패션을 알리고 어린이들의 1:1양육을 돕기 위한 다양한 나눔 프로젝트를 매년 직접 기획하고 진행하는 창의적 체험활동을 통해<br />
								각자의 재능을 발견/활용할 기회가 주어지며 기획력과 책임감, 더불어 함께하는 사역을 통해 낮아지며 섬기는 리더십을 훈련 받게 됩니다.
							</p>
							<div class="project">
								<div class="clear2">
									<p class="tit">YVOC 주요 행사</p>
									<p class="con">YVOC Family day / YVOC 외부 캠페인 / YVOC 플래시몹 / YVOC 버스킹</p>
								</div>
								<div class="clear2">
									<p class="tit">YVOC 주요 프로젝트</p>
									<p class="con">
										2012 대한민국 청소년박람회 여성가족부 장관상 수상<br />
										2013 YVOC 자작곡 음원 제작 ‘One Act Song’<br />
										2014 동인도 도서관짓기 프로젝트<br />
										2015 YVOC 나눔별 연합 ‘필리핀교실 지어주기’ 프로젝트<br />
										2016 YVOC 컴패션나눔별 연합 ‘컴패션, 희망의 교실 보내주기’ 프로젝트 <br />
                                        2017 YVOC ‘태아영아생존’ 프로젝트<br />
                                        2017 YVOC 콘서트 ‘작은예수’<br />
									</p>
								</div>
							</div>
						</li>
						<li>
							<h3 class="s_tit6 mt40">YVOC 동아리(컴패션나눔별)</h3>
							<p class="s_con3">
								컴패션을 학교에 알리고 같은 마음으로 동참할 친구를 모집하여 YVOC 동아리 모임을 진행합니다. 모임에서 나눔별 후원을 통해 현지 어린이를 함께 후원하거나,<br />재능기부로 자체적인 결연 및 홍보캠페인을 진행하고 YVOC와 연합으로 진행되는 행사 또는 프로젝트에 참여하는 청소년들의 자발적 후원활동입니다.
							</p>
						</li>
					</ul>					
				</div>

				<div class="yvoc_guide">
					<h2 class="sub_tit">YVOC 신청 안내</h2>
					<span class="bar"></span>
					<div class="howTo">
						<h3 class="tit">신청 방법</h3>
                        <p class="s_con3"><br><a href="http://goo.gl/mGshFo" class="btn_s_type5" style="color:#666;" target="_blank">YVOC 지원하기</a><br><br> 
                        해당링크로 연결하여 온라인 지원해주세요.<br><br>
                        1차 합격자는 홈페이지를 통해 공지하며, 인터뷰에 필참해야합니다. (대상자 및 일정 추후공지)<br>
                        추천서(담임교사/담당목회자/학부모 중 1분께 받은 추천서)는 온라인지원시 첨부해주세요.<br>
                        <a href="/common/download/한국컴패션 청소년홍보대사 추천서 양식.zip" class="download" target="_blank">YVOC추천서 양식 다운받기</a><br><br></p>

						<%--<p class="s_con3">컴패션 홈페이지 공고 / YVOC 및 후원자, 문의자 이메일 발송</p>
						<h3 class="tit">모집 공고</h3>
						<p class="s_con3">
							아래의 지원서를 다운로드받아 작성하고 이메일로 지원해 주세요.&nbsp;
							<a href="/common/download/2016YVOC.zip" class="download">지원서 다운받기</a><br />
							지원서 통과자는 홈페이지를 통해 공지하며, 지원서 통과 후에는 인터뷰에 필참해야 합니다. (대상자 및 시간 추후 공지)<br />
							지원서, 추천서(담임교사/담당목회자/학부모 중 1분께 받은 추천서)
						</p>
						<div class="how">
							<div class="step1">
								<span><em>지원서 작성하고 추천서 받기</em><br />
								담임교사/담당목회자/학부모 중 1분께 받은 추천서</span></div>
							<div class="step2">
								<span><em>메일로 전송하면 지원 완료!</em><br />
								<a href="mailto:yvoc@compassion.or.kr">yvoc@compassion.or.kr</a></span></div>
						</div>--%>
					</div>
					<ul class="info clear2">
						<li>
							<h3 class="tit">명단 발표</h3>
							<p class="s_con3">홈페이지 및 개별 문자 공지</p>
						</li>
						<li>
							<h3 class="tit">모임 장소</h3>
							<p class="s_con3">
								한국컴패션 사옥 2층 스완슨홀<br />
								(서울시 용산구 한남대로 102-5)
							</p>
						</li>
						<li>
							<h3 class="tit">활동 기간</h3>
							<p class="s_con3">1년 단위로 활동하며 매년 초<br />신청 및 재서약을 받습니다.</p>
						</li>
					</ul>
				</div>

				
				<!-- 공지/공유 바로가기 -->
				<div class="act_news w980 mb60">
					<p class="tit">YVOC의 자세한 활동 소식이 궁금하신가요?</p>
					<div class="board clear2 tal">
						<div class="box_type4 fl">
							<p class="s_tit1">공지 게시판</p>
							<p class="con">YVOC 활동 공지 게시판 입니다.</p>
							<a href="/advocate/notice/?b_sub_type=yvoc" class="btn_s_type3">바로 가기</a>
						</div>
						<div class="box_type4 fr">
							<p class="s_tit1">공유 게시판</p>
							<p class="con">YVOC 활동 공유 게시판 입니다.</p>
							<a href="/advocate/share/?category=yvoc" class="btn_s_type3">바로 가기</a>
						</div>
					</div>
				</div>
				<!--//  -->

				<div class="banner w980">
					<!-- 고객사 확인용 임시url -->
                    <script>
                        function getHost() {
                            var host = '';
                            var loc = String(location.href);
                            if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
                            else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
                            else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
                            return host;
                        }

                        function goLink() {
                            var host = getHost();
                            if (host != '') location.href = 'http://' + host + '/sympathy/view/sponsor/1444?page=1&rowsPerPage=9&s_type=sponsor&s_column=reg_date';
                        }
                    </script>
					<a href="javascript:goLink()"><img src="/common/img/page/advocate/YVOC_banner.jpg" alt="YVOC이야기 바로가기 이미지" /></a>
				</div>

			</div>
		</div>
		

		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->



    
</asp:Content>