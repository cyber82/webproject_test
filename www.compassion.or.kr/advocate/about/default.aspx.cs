﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class advocate_intro_default : FrontBasePage {
	


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);
	}



	protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.board.OrderByDescending(p => p.b_id).FirstOrDefault(p => p.b_main == true && p.b_type == "advocate");
            var entity = www6.selectQF<board>("b_main", 1, "b_type", "advocate", "b_id");

            if (entity == null)
            {
                recruit_div.Visible = false;
            }
            else
            {
                recruit_text.InnerText = entity.b_title;
                recruit_text.HRef = "/advocate/notice/view/" + entity.b_id;
            }

        }
	}
}