﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="foc.aspx.cs" Inherits="advocate_intro_foc" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>FOC(Friends of Compassion)</em></h1>
				<span class="desc">가난한 어린이들의 목소리가 되어 활동하는 컴패션 애드보킷을 소개합니다</span>

				 <uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="foc tac">

				<div class="w980 intro">

					<p class="advo_tit"><em>FOC</em>는 꿈을 잃은 어린이들에게 그리스도의 사랑을 전하는</p>
					<p class="advo_con">자원봉사자 그룹으로 각 분야의 CEO, 리더로 구성되며 다양한 네트워크와<br />
					전문적인 재능을 통해 컴패션의 사역을 알립니다.</p>
					<div class="img"></div>

					<h2 class="sub_tit">FOC의 활동 자격</h2>
					<p class="sub_con">공통 : 어린이 1명 이상 후원을 통해 컴패션의 가치를 이해하고 있는 후원자</p>
					
					<ul class="license clear2">
						<li><span class="s_con7">각 기업의 CEO 및 리더그룹, 개인 사업자</span></li>
						<li><span class="s_con7">다수 후원자 (6명 이상) 혹은 지속적인 기부 후원자</span></li>
						<li><span class="s_con7">각 분야의 네트워크 전문가</span></li>
						<li><span class="s_con7">FOC 파티의 호스트로 행사를 진행한 자</span></li>
					</ul>

				</div>

				<div class="foc_role">					
					<h2 class="sub_tit">FOC의 역할</h2>
					<span class="bar"></span>
					<p class="s_con3">
						컴패션 사역과 어린이들을 위한 기도 후원자가 됩니다.<br />
						FOC 성경공부에 참석해 컴패션의 가치가 드러나는 삶을 살기 위해 예배하고 교제합니다.<br />
						컴패션 비전트립에 참여해 컴패션 사역 현장을 직접 확인하고 지인들에게 소개합니다.<br />
						FOC가 주최하는 컴패션 FOC 파티를 직접 기획하고 진행합니다.<br />
						FOC 기업이나 지인의 기업에 컴패션을 소개합니다.
					</p>
				</div>

				<div class="foc_act w980">
					<h2 class="sub_tit">FOC 주요 활동</h2>
					<ul>
						<li>							
							<h3 class="s_tit6">FOC 정기모임 (성경공부)</h3>
							<p class="s_con3">
								매주 구성원들이 함께 모여 성경공부와 조별나눔을 갖는 목요 정기모임을 진행합니다. 하나님 안에서 나눔과 삶을 위한 강의가 계속되며,<br />
								이를 통해 컴패션의 가치가 드러나는 삶을 살기 위해 예배하고 교제합니다.
							</p>
							<table class="info_box">
								<caption>FOC정기모임 날짜와 장소</caption>
								<colgroup>
									<col width="33%" />
									<col width="34%" />
									<col width="33%" />
								</colgroup>
								<tbody>
									<tr>
										<td><span class="s_con7">상반기 : 매년 3월~ 6월 (매주 목요일)</span></td>
										<td><span class="s_con7">하반기 : 매년 9월~ 12월 (매주 목요일)</span></td>
										<td>
											<span class="s_con7">
												장소 : 한국컴패션 사옥 2층 스완슨홀<br />
												(서울시 용산구 한남대로 102-5)
											</span>
										</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li>
							<h3 class="s_tit6">FOC 파티</h3>
							<p class="s_con3">
								파티의 호스트가 되어 지인들을 초대하고 컴패션을 알리는 행사를 주최해 컴패션을 소개합니다.<br />
								FOC 파티는 형식에 제한 받지 않고 호스트가 장소, 음식, 공연 등을 자발적으로 준비하며 컴패션이 프로그램의 소스를 제공합니다.
							</p>
						</li>
						<li>
							<h3 class="s_tit6">FOC 바자회</h3>
							<p class="s_con3">
								FOC는 매년 FOC 자체 바자회를 진행하거나 유명 잡지사와 함께 바자회를 진행합니다.<br />
								이 때 FOC는 행사에 직접 참여하거나 자신들의 네트워크를 통해 바자에 많은 브랜드가 참여할 수 있도록 돕습니다.<br />
								해당 수익금은 컴패션 양육보완프로그램(CIV)를 통해 도움이 필요한 곳에 쓰여집니다.
							</p>
						</li>
						<li>
							<h3 class="s_tit6">FOC 비전트립</h3>
							<p class="s_con3">
								컴패션 사역을 눈으로 확인하고 비전을 공유하기 위한 FOC 비전트립이 매년 진행됩니다.<br />
								현지 어린이들과의 만남을 통해 컴패션의 비전을 나눌 뿐 아니라 함께 참여한 FOC들과 가족처럼 가까워지는 시간이 될 것입니다.
							</p>
						</li>
						<li>
							<h3 class="s_tit6">FOC 기업 연결 프로그램</h3>
							<p class="s_con3">
								FOC의 네트워크를 통해 기업에 컴패션을 소개하고 연결하는 프로그램입니다.<br />
								기업 담당자를 통해 직원 후원, 상품 콜라보레이션, 공동 프로모션/이벤트, 매칭 기프트 등의 프로그램을 진행할 수 있습니다.
							</p>
						</li>
					</ul>					
				</div>

				<!-- 문의 -->
				<div class="contact_qna w980">
					<p class="tit">컴패션 프렌즈 관련 문의</p>
					<div class="clear2">
						<div class="tel"><span class="icon"><span class="txt">전화 : </span>02) 3668-3486</span></div>
						<div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:echoi@compassion.or.kr">echoi@compassion.or.kr</a></span></div>
					</div>
				</div>
				<!--//  -->
				
				<!-- 공지/공유 바로가기 -->
				<div class="act_news w980">
					<p class="tit">FOC의 자세한 활동 소식이 궁금하신가요?</p>
					<div class="board clear2 tal">
						<div class="box_type4 fl">
							<p class="s_tit1">공지 게시판</p>
							<p class="con">FOC 활동 공지 게시판 입니다.</p>
							<a href="/advocate/notice/?b_sub_type=foc" class="btn_s_type3">바로 가기</a>
						</div>
						<div class="box_type4 fr">
							<p class="s_tit1">공유 게시판</p>
							<p class="con">FOC 활동 공유 게시판 입니다.</p>
							<a href="/advocate/share/?category=foc" class="btn_s_type3">바로 가기</a>
						</div>
					</div>
				</div>
				<!--//  -->

			</div>
		</div>
		

		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->
	
	
	    
</asp:Content>