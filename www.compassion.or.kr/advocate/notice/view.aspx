﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="advocate_notice_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    
    <script type="text/javascript" src="/advocate/notice/view.js"></script>
    <script type="text/javascript" >
        $(function () {
            $("#reply").textCount($("#count"), { limit: 300 });
        });

    </script>
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    
    <!-- sub body -->
	<section class="sub_body"  ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
        <input type="hidden" id="id" runat="server" />


		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>활동공지</em></h1>
				<span class="desc">애드보킷 활동에 필요한 공지사항을 알려드립니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="notice w980">
				
				<!-- 게시판 상세 -->
				<div class="boardView_1">
					<div class="tit_box noline">
						<span class="tit"><asp:Literal runat="server" ID="b_title" /></span>
						<span class="txt">
							<span><asp:Literal runat="server" ID="b_regdate" /></span>
							<span class="bar"></span>
							<span class="hit"><asp:Literal runat="server" ID="b_hits" /></span>
						</span>
					</div>
					<div class="view_contents">
						<asp:Literal runat="server" ID="b_content" />
					</div>

					<!-- sns 공유하기 -->
					<div class="share_box">
						<span class="sns_ani">
							<span class="common_sns_group">
								<span class="wrap">
									<a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
                                    <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
                                    <a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
                                    <a href="#" title="url 공유" data-role="sns" data-provider="copy" class="sns_url">url 공유</a>
								</span>
							</span>
							<button class="common_sns_share">공유하기</button>
						</span>
					</div>
					<!--// -->
					<!-- 댓글 -->
					<div class="comment">
						<fieldset>
							<legend>댓글 입력창</legend>
							<span class="tit">댓글 {{total}}</span>
							<span id="count" style="position: absolute;top:10px;right: 165px">0</span>
							<span style="position: absolute;top:10px;right:120px">/ 300자</span>
							<div class="clear2">
								<label for="reply" class="hidden">댓글입력</label>
								<textarea id="reply" name="content" ng-model="content" class="textarea_type1" maxlength="300"></textarea>

								<button type="button" ng-click="add()" class="registration">등록</button>
							</div>
						</fieldset>
					</div>




					<ul class="comment_lst mb20">
						<li ng-repeat="item in list" ng-click="goView(item.idx)">
							<div class="clear2">
								<div class="id_box">
									<span class="id">{{item.c_user_id}}</span>
									<span class="txt">{{item.c_content}}</span>
								</div>
								<div class="modify_box">
									<span ng-show="item.is_owner">
										<a ng-click="toggleEvent(item.c_id)" class="modify">수정</a>
										<span class="bar"></span>
										<a ng-click="remove(item.c_id)" class="delete">삭제</a>
									</span>
									<span class="day">{{parseDate(item.c_regdate) | date : 'yyyy-MM-dd HH:mm:ss' }}</span>
								</div>
							</div>
							<!-- 수정하기 -->
                            <div class="modifyArea" ng-show="item.is_owner" data-idx="{{item.c_id}}" style="display:none;">
                                <label for="modify_{{item.c_id}}" class="hidden">댓글 수정하기</label>
                                <div class="tar mb10"><span class="modify_count">0</span>/300자</div>
                                <textarea id="modify_{{item.c_id}}" class="textarea_type1 mb10" style="width: 100%;" maxlength="300" rows="4">{{item.c_content}}</textarea>

                                <div class="tac">
                                    <button type="button" class="btn_type1 mr5" ng-click="update(item.c_id)"><span>수정하기</span></button>
                                    <button type="button" class="btn_type2" ng-click="toggleEvent(item.c_id)"><span>수정취소</span></button>
                                </div>
                            </div>
                            <!--// -->
						</li>
					</ul>
					<!--// 댓글-->
					<!-- page navigation -->
					<div class="tac mb60">
						<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  
					</div> 
					<!--// -->
					
				</div>
				<!--// 게시판 상세 -->

				<div class="tar"><a  runat="server" id="btnList" class="btn_type4 ">목록</a></div>


			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->


    
</asp:Content>