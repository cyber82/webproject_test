﻿(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		$scope.total = -1;
		$scope.page = 1;
		$scope.isSearch = false;
		$scope.rowsPerPage = 10;
		$scope.isFirst = true;

		$scope.list = [];
		$scope.params = {
			page: $scope.page,
			rowsPerPage: $scope.rowsPerPage,
			b_type: 'advocate',
			b_sub_type: 'all,online'
		};

		
		

		// 파라미터 초기화
		$scope.params = $.extend($scope.params, paramService.getParameterValues());
		if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);


		// 검색
		$scope.search = function (params) {
			$scope.params = $.extend($scope.params, params);
			$scope.params.k_word = $("#k_word").val();
			$scope.getList();
		}


		$scope.sorting = function (type) {
			$(".notice_type li").removeClass("on");
			$(".notice_type li[data-type=" + type+ "]").addClass("on");

			$scope.params.page = 1;
			$scope.params.b_sub_type = "all,"+type;
			$("#k_word").val("");

			if ($scope.isFirst) { $scope.isFirst = false; return; }

			$scope.search();
		}


		// list
		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);

			$http.get("/api/board.ashx?t=file_list", { params: $scope.params }).success(function (result) {
				$scope.list = result.data;
				$scope.total = result.data.length > 0 ? result.data[0].total : 0;

				$scope.isSearch = false;
				if ($scope.params.k_word != "") {
					$scope.isSearch = true;
				}

				$.each($scope.list, function () {
					this.li_class = this.b_file ? "thumb" : "";
					this.title_class = this.b_sub_type_name == '전체공지' ? "lst_tit" : "lst_tit2"
				})

				if (params)
					scrollTo($("#l"), 10);
			});

		}


		// 상세페이지
		$scope.goView = function (id) {
			$http.post("/api/board.ashx?t=hits&id=" + id).then().finally(function () {
				location.href = "/advocate/notice/view/" + id + "?" + $.param($scope.params);
			});
		}

		$scope.parseDate = function (datetime) {
			return new Date(datetime);
		}

		$scope.getList();


		// sort
		type = $scope.params.b_sub_type.split(",")
		type = type[type.length - 1];
		$scope.sorting(type);


	});

})();

