﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class advocate_notice_default : FrontBasePage {


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		var list = StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "board_sub" && p.cd_key != "all").OrderBy(p => p.cd_order);

		repeater.DataSource = list;
		repeater.DataBind();
	}

}