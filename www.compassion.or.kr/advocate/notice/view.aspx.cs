﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class advocate_notice_view : FrontBasePage {

	const string listPath = "/advocate/notice/";


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}
		base.PrimaryKey = requests[0];
		id.Value = PrimaryKey.ToString();
	}


	protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<board>("b_id", Convert.ToInt32(PrimaryKey) , "b_display", 1);
            //if (!dao.board.Any(p => p.b_id == Convert.ToInt32(PrimaryKey) && p.b_display))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));

            b_title.Text = entity.b_title;
            b_regdate.Text = entity.b_regdate.ToString("yyyy.MM.dd");
            b_hits.Text = entity.b_hits.ToString("N0");
            b_content.Text = entity.b_content;

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-활동공지] {0}", entity.b_title);

            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = "";
            //this.ViewState["meta_keyword"] = "";
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,FrontBasePage)
            //this.ViewState["meta_image"] = entity.thumb.WithFileServerHost();

        }
	} 


}