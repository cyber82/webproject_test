﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_notice_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="default.js"></script>
    <script>
        $(function () {
        	//$(".notice_type li[data-type=online]").addClass("on");
        	$(".notice_type li[data-type=foc]").css("width", "16%");
        	$(".notice_type li[data-type=voc]").css("width", "16%");
        });
    </script>
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

 
	
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>활동공지</em></h1>
				<span class="desc">애드보킷 활동에 필요한 공지사항을 알려드립니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="notice w980">
				
				<!-- 탭메뉴 -->
				<ul class="tab_type1 notice_type" id="l">
					<asp:Repeater runat="server" ID="repeater">
						<ItemTemplate>
							<li style="width:17%" ng-click="sorting('<%#Eval("cd_key") %>')" data-type="<%#Eval("cd_key") %>"><a><%#Eval("cd_value") %></a></li>
						</ItemTemplate>
					</asp:Repeater>
				</ul>
				<!--// 탭메뉴 -->

				<div class="sortWrap clear2 mb20">
					<div class="fr relative mt50">
						<label for="k_word" class="hidden">검색어 입력</label>
						<input type="text" name="k_word" id="k_word" ng-enter="search()"  class="input_search1" style="width:245px" placeholder="검색어를 입력해 주세요" />
						<a ng-click="search()" class="search_area1">검색</a>
					</div>
				</div>

				<div class="boardList_2">
					<ul class="list">

						<li ng-repeat="item in list" class="{{item.li_class}}">
							<!-- 썸네일 이미지 사이즈 : 190 * 120 -->
							<span class="lst_img" ng-if="item.li_class == 'thumb' ">
								<span class="img_wh"><img ng-src="{{item.b_file}}" alt="썸네일 이미지" /></span>
							</span>
							<!--// -->
							<span class="lst_box">
								<span class="whole_box">
									<span class="whole_notice" ng-if="item.b_sub_type == 'all'">[전체공지]</span>
									<a ng-click="goView(item.b_id)" class="{{item.title_class}}">{{item.b_title}}</a>
								</span>
								<span class="lst_txt">
									<span>{{parseDate(item.b_regdate) | date:'yyyy.MM.dd'}}</span>
									<span class="lst_bar"></span>
									<span class="lst_hit">{{item.b_hits}}</span>
								</span>
								<a href="#" class="list_link">{{item.b_summary}}</a>
							</span>
						</li>
						
						<!-- 검색결과 없을때 -->
						<li class="no_result" ng-if="total == 0 && isSearch">검색 결과가 없습니다.</li>
						<!--//  -->

						<!-- 게시글 없을때 -->
						<li class="no_content"  ng-if="total == 0 && !isSearch">등록된 글이 없습니다.</li>
						<!--//  -->

					</ul>
				</div>

				<!-- page navigation -->
				 <div class="tac mb60">
					<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
				</div>
				<!--// page navigation -->


			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->
    
</asp:Content>