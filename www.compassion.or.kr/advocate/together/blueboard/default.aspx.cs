﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;
using System.Configuration;
using System.Data;
using CommonLib;

public partial class advocate_together_blueboard_default : FrontBasePage {

	protected override void OnBeforePostBack()
    {
		this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];
		base.OnBeforePostBack();

		

		isAdvocate.Value = IsAdvocate().ToString();

		if (UserInfo.IsLogin)
        {
			var userInfo = new UserInfo();

            CommonLib.WWW6Service.SoaHelperSoap _www6Service;
            _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

            using (FrontDataContext dao = new FrontDataContext())
            {
                //var checkDay = dao.blue_board_apply.Where(p => p.bba_user_id == userInfo.UserId).OrderByDescending(p => p.bba_regdate).ToList();

                string dbType = "Text";
                string dbName = "SqlCompassionWeb";

                Object[] objSql_checkDay = new object[1] { "select * from blue_board_apply where bba_user_id = '" + userInfo.UserId + "' order by bba_regdate desc" };
                DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql_checkDay, dbType, null, null);
                List<blue_board_apply> checkDay = ds.Tables[0].DataTableToList<blue_board_apply>();

                if (checkDay.Count > 0)
                {
                    if (DateTime.Now.Subtract(checkDay[0].bba_regdate).TotalDays < 1)
                    {
                        todayApply.Value = "true";
                    }
                }
            }
		}
	}


	protected bool IsAdvocate()
    {
		bool result = false;
		if (UserInfo.IsLogin)
        {
			var userInfo = new UserInfo();

            using (FrontDataContext dao = new FrontDataContext())
            {
                string dbType = "Text";
                string dbName = "SqlCompassionWeb";

                var exist = www6.selectQ<advocate_user>("au_user_id", userInfo.UserId);
                //if (dao.advocate_user.Any(p => p.au_user_id == userInfo.UserId))
                if (exist.Any())
                {
                    result = true;
                }
                else
                {
                    result = new VOCAction().CheckUser().success;
                }
            }
		}

			return result;
	}

}