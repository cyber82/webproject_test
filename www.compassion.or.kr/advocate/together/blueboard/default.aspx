﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_together_blueboard_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
    <script type="text/javascript" src="/advocate/together/blueboard/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    
    <input type="hidden" id="id" runat="server"/>
    <input type="hidden" id="isAdvocate" runat="server" />
    <input type="hidden" id="todayApply" runat="server" />
 
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션블루보드(Blue Board)</em></h1>
				<span class="desc">컴패션어린이 정보가 담긴 블루보드로 컴패션을 소개해 보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 advocate">

			<div class="blueBoard tac">

				<div class="visual_blue">
					<h2 class="sub_tit">소중한 사람들에게 컴패션을 알립니다</h2>
					<span class="bar"></span>
					<p class="s_con3">컴패션블루보드란 컴패션 어린이 정보가 담긴 블루보드를 활용하여<br />
					후원자님이 속한 곳(회사, 사업장, 교회 등)에서 소중한 사람들에게 자연스럽게 컴패션을 알리고 소개하는 활동입니다</p>
				</div>

				<div class="exemple w980"></div>

				<div class="act_guide">
					<h2 class="sub_tit">활동 가이드</h2>
					<div class="mt40">
						<iframe width="802" height="451" title="영상" src="https://www.youtube.com/embed/RnLcOyj99vk?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

				<ul class="blue_conb w980">
					<li class="consist">
						<h3 class="s_tit6">컴패션블루보드 구성</h3>
						<ul class="clear2">
							<li><span class="s_con8">결연 안내 보드</span></li>
							<li><span class="s_con8">컴패션 브로슈어</span></li>
							<li><span class="s_con8">결연서 / 반송봉투</span></li>
						</ul>
						<p><span class="s_con1">현장에서 바로 결연이 이루어질 수 있도록, 온라인에 공개되지 않은 어린이 3명의 정보가 함께 발송됩니다.</span></p>
						<p><span class="s_con1">소중한 어린이의 정보가 담기는 자료인 만큼, 기도하는 마음으로 보다 신중하게 신청해 주세요.</span></p>
					</li>
					<li>
						<h3 class="s_tit6">신청 및 발송</h3>
						<p class="s_con3">아래  [패킷 신청] 버튼을 통해 패킷 활용 계획을 알려 주시면, 담당자의 승인 절차를 거쳐 2일 내 발송됩니다.</p>
					</li>
					<li>
						<h3 class="s_tit6">신청 조건</h3>
						<p class="s_con3">블루보드 패킷은 하루 최대 1회, 컴패션 애드보킷만 신청 가능합니다.<br />
						더 많은 어린이 정보가 필요하시거나 결연 행사를 계획하고 계신 후원자님은 DIY신청이나 담당자에게 직접 연락 바랍니다.</p>
					</li>
				</ul>

				<div class="w980"><a href="#" ng-click="modal.show($event)" class="btn_type1 mt40">패킷 신청</a></div>

				<!-- 문의 -->
				<div class="contact_qna w980 div3">
					<p class="tit">온라인 애드보킷 블루보드 신청/문의</p>
					<div class="clear2">
						<div class="tel"><span class="icon"><span class="txt">전화 : </span>02) 3668-3516</span></div>
						<div class="fax"><span class="icon"><span class="txt">팩스 : </span>02) 3668-3501</span></div>
						<div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:echa@compassion.or.kr">echa@compassion.or.kr</a></span></div>
					</div>
				</div>
				<!--//  -->

			</div>

		</div>
		

		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

    
</asp:Content>