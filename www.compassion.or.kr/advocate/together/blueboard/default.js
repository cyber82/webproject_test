﻿

function jusoCallback(zipNo, addr1, addr2, jibun) {
	// 실제 저장 데이타
	$("#addr1").val(addr1 + "//" + jibun);
	$("#addr2").val(addr2);

	// 화면에 표시
	$("#zipcode").val(zipNo);

	$("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
	$("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);

};


(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		// 패킷 신청
		$scope.modal = {
			instance: null,

			init: function () {
				// 팝업
				popup.init($scope, "/advocate/together/blueboard/write", function (modal) {
					$scope.modal.instance = modal;

					$("#phone").mobileFormat();

				});
			},

			show: function ($event) {
				$event.preventDefault();
				if (!$scope.modal.instance)
					return;

				if (common.checkLogin()) {
					if ($("#locationType").val() != "국내") {
						alert("국내거주 회원만 신청가능합니다.");
						return;
					}

					if ($("#isAdvocate").val() == "false") {
						alert("애드보킷 회원만 이용가능합니다. 온라인 애드보킷 지원페이지를 이용하세요.");
						return;
					}

					if ($("#todayApply").val() == "true") {
						alert('패킷은 하루 1회만 신청 가능합니다.');
						return;
					}

					$http.post("/api/advocate.ashx?t=check_user").success(function (r) {
					    if (!r.success) {
					        alert(r.message);
					        return;
					    }
					    $scope.modal.instance.show();
					});

                }

			},

			request: function ($event) {
				
				if (!validateForm([
					{ id: "#name", msg: "이름을 입력하세요" },
					{ id: "#phone", msg: "휴대폰번호를 입력하세요", type: "phone" },
					{ id: "#zipcode", msg: "우편번호를 입력하세요" },
					{ id: "#addr1", msg: "주소를 입력하세요" },
					{ id: "#addr2", msg: "주소를 입력하세요" },
					{ id: "#plan", msg: "활동계획을 입력하세요" }
				])) {
					$(".guide_comment2").show();
					return;
				} else {
					$(".guide_comment2").hide();
				}

				if ($scope.requesting) return;
				$scope.requesting = true;


				var name = $("#name").val();
				var phone = $("#phone").val();
				var zipcode = $("#zipcode").val();
				var addr1 = $("#addr1").val();
				var addr2 = $("#addr2").val();
				var plan = $("#plan").val();
				var hfAddressType = $("#hfAddressType").val()
				
				var param = {
					name: name,
					phone: phone,
					zipcode: zipcode,
					addr1: addr1,
					addr2: addr2,
					plan: plan,
					hfAddressType: hfAddressType
				};

				console.log(param);

				$http.post("/api/advocate.ashx?t=blueboard_apply", param).success(function (r) {
					$scope.requesting = false;
					if (r.success) {
						alert("신청이 완료되었습니다.\n후원자님의 귀한 섬김에 언제나 감사드립니다.");
					    location.reload();
                        console.log(r.data)

					} else {
						alert(r.message);
						$scope.modal.close($event);
					}

				}).error(function(e){
					console.log(e);
				})
			},

			close: function ($event) {

				if (!$scope.modal.instance)
					return;

				$scope.modal.instance.hide();
				$event.preventDefault();
			},

			popup: function ($event) {
				$event.preventDefault();
				cert_setDomain();
				var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");

			}

		}
		$scope.modal.init();


	});

})();