﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="write.aspx.cs" Inherits="advocate_together_diy_write"  %>

<div style="background:transparent;width:600px;">

    <script>
    	$(function () {
    		// 컴파스의 데이타를 불러오는경우 
    		if ($("#dspAddrDoro").val() != "") {
    			$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
    			if ($("#dspAddrJibun").val() != "") {
    				$("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
    			}

    		} else {
    			if ($("#addr1").val() != "") {
    				var addr_array = $("#addr1").val().split("//");
    				$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + addr_array[0] + " " + $("#addr2").val());
    				if (addr_array[1]) {
    					$("#addr_jibun").text("[지번주소] (" + $("#zipcode").val() + ") " + addr_array[1] + " " + $("#addr2").val());
    				}
    			}
    		}
        });
    </script>
    
    <input type="hidden" runat="server" id="locationType" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	<input type="hidden" id="addr1" runat="server"/>
	<input type="hidden" id="addr2" runat="server" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
	
	<div class="pop_type1 w600">
		<div class="pop_title">
			<span>컴패션블루보드(Blue Board) 패킷 신청</span>
			<button type="button" ng-click="modal.close($event)" class="pop_close"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content blueBrdApply">

			<div class="pop_common">
				<table class="tbl_type1">
					<caption>정보입력 테이블</caption>
					<colgroup>
						<col style="width:15%" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th scope="row"><label for="name">이름</label></th>
							<td><input type="text" id="name" runat="server" maxlength="10" class="input_type2" value="" style="width:150px;" /></td>
						</tr>
						<tr>
							<th scope="row"><label for="phone">휴대폰</label></th>
							<td><input type="text" id="phone" runat="server" maxlength="13" class="input_type2 number_only" value="" style="width:400px;" /></td>
						</tr>
						<tr>
							<th scope="row"><span>주소</span></th>
							<td>
								
								<label for="zipcode" class="hidden">주소찾기</label>
								<input type="text" runat="server" name="zipcode" id="zipcode" maxlength="5"  class="input_type2 zipcode" value="" style="width:150px;" readonly="readonly" />
								<a href="#" class="btn_s_type2 ml5" runat="server" id="popup" ng-click="modal.popup($event)">주소찾기</a>
								
								<p id="addr_road" class="fs14 mt15"></p>
								<p id="addr_jibun" class="fs14 mt10"></p>

							</td>
						</tr>
						<tr>
							<th scope="row"><label for="plan">활동 계획</label></th>
							<td><textarea id="plan" maxlength="1000" class="textarea_type2" rows="18"></textarea></td>
						</tr>
					</tbody>
				</table>

				<!-- 경고문구 -->
				<div class="warning">
					<span class="guide_comment2" style="display:none;">필수 정보를 입력해 주세요.</span>
				</div>
				<!--// 경고문구 -->

				<p class="s_con3">
					블루보드는 현장에서 바로 결연이 이루어질 수 있도록<br />
					온라인에 공개되지 않은 어린이 3명의 정보가 함께 발송됩니다.<br />
					소중한 어린이의 정보가 담기는 자료인 만큼, 보다 신중하게 신청해 주세요.<br />
				</p>


				<div class="tac">
					<a href="#" ng-click="modal.request($event)" class="btn_type1">신청하기</a>
				</div>
			</div>


		</div>
	</div>
</div>