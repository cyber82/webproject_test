﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class advocate_together_default : FrontBasePage {
	
	protected override void OnBeforePostBack() {
		Response.Redirect( "/advocate/together/compassion-a-day/" );

	}

}