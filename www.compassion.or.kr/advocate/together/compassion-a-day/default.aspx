﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_intro_together" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>Compassion A Day</em></h1>
				<span class="desc">하루 한 번, 친구나 지인들에게 컴패션어린이를 소개해 보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 advocate">

			<div class="aday tac">

				<div class="visual_aday">
					<h2 class="sub_tit">하루에 한 번, 어린이를 향한 사랑고백</h2>
					<span class="bar"></span>
					<p class="s_con3">Compassion A Day 캠페인이란 하루에 한 번, 친구나 지인들에게 컴패션을 소개하고 도움이 필요한 어린이를 알리는 캠페인입니다.<br />
					하루에 한 번 컴패션을 전해 주시면 한 어린이를 향한 놀라운 기적이 시작됩니다.</p>
				</div>

				<div class="w980">
					<iframe width="980" height="551" title="영상" class="mb60" src="https://www.youtube.com/embed/fQjCrHEYV2E?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
				</div>

				<div class="bgContent">
					<div class="childRecom w980">
						<h2 class="sub_tit">어린이 추천 과정</h2>
						<span class="bar"></span>

						<!-- 어린이추천과정 배경이미지 -->
						<div class="recom_img clear2">
							<div class="img i1"></div>
							<div class="arr"></div>
							<div class="img i2"></div>
							<div class="arr"></div>
							<div class="img i3"></div>
						</div>
						<!--//  -->

						<ul class="howTo clear2">
							<li>
								<span>STEP 1</span>
								<div class="tit">어린이 유형 선택</div>
								<p class="s_con3">추천하고 싶은 어린이 유형을 선택하세요.<br />
								컴패션에서는 유형에 맞는 한 명의 어린이를<br />
								후원자님만의 편지에 담아 드립니다.</p>
							</li>
							<li>
								<span>STEP 2</span>
								<div class="tit">추천 메세지 쓰기</div>
								<p class="s_con3">후원자님의 진심 어린 메시지는 한 어린이가<br />
								밝은 내일을 맞이할 수 있는 힘이 됩니다.<br />
								신중한 마음으로 함께해주세요.</p>
							</li>
							<li>
								<span>STEP 3</span>
								<div class="tit">어린이 패킷 발송 완료</div>
								<p class="s_con3">수신인은 후원자님의 메시지와 함께 모바일에서<br />
								확인 가능한 어린이 정보를 받게 됩니다.<br />
								어린이의 양육 신청이 가능한 7일 동안<br />
								후원자님도 기도로 함께해주세요!</p>
							</li>
						</ul>
					</div>
				</div>

				<div class="aday_conb w980">
					<p class="sub_tit">하루 한 명, 소중한 사람들에게 <em>컴패션 어린이를 추천</em>해 보세요</p>
					<p class="s_con3">CAD 추천 서비스는 컴패션 모바일 홈페이지를 통해 이용하실 수 있습니다.</p>
					<div class="shortcut">
						<span class="s_con3">추천 후에는 내가 추천한 어린이의 결연 현황을 <em>마이컴패션</em>에서 확인하실 수 있습니다.</span>
						<a href="/my/activity/cad" class="btn_s_type2 ml10">CAD 추천 현황 보기</a>
					</div>
				</div>


			</div>
		</div>
		

		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

	
	
	
	    
</asp:Content>