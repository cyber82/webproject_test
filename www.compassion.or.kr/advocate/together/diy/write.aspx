﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="write.aspx.cs" Inherits="advocate_together_diy_write"  %>

<div style="background:transparent; width:800px;" class="fn_pop_container" id="popup">
	<!-- 일반팝업 width : 800 -->
	<div class="pop_type1 w800 fn_pop_content" style="height:864px; padding-top:25px; margin-bottom:25px;" >
		<div class="pop_title">
			<span>컴패션 DIY 사연 신청</span>
			<button type="button" class="pop_close" ng-click="modal.close($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content StoryApply">
			
			<div class="pop_common">
				<p class="intro">후원자님만의 방법으로 <em>사랑을 나눌 계획</em>을 알려 주세요.</p>

				<div class="tableWrap2">
					<table class="tbl_type1 line1">
						<caption>정보입력 테이블</caption>
						<colgroup>
							<col style="width:21%" />
							<col style="width:auto" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><label for="ds_name">이름</label></th>
								<td><input type="text" id="ds_name" runat="server"  class="input_type2" value="홍길동" style="width:150px;" /></td>
							</tr>
							<tr>
								<th scope="row"><label for="ds_phone">연락 가능한 번호</label></th>
								<td><input type="text" id="ds_phone" runat="server" class="input_type2" maxlength="13" value="010-1234-5678" style="width:400px;" /></td>
							</tr>
							<tr>
								<th scope="row"><label for="ds_content">사연 및 실천<br />방안 소개</label></th>
								<td><textarea id="ds_content" maxlength="1000" class="textarea_type2" rows="18"></textarea></td>
							</tr>
						</tbody>
					</table>
				</div>

				<!-- 경고문구 -->
				<div class="warning" >
					<span class="guide_comment2" style="display:none;">필수 정보를 입력해 주세요.</span>
				</div>
				<!--// 경고문구 -->

				<p class="s_con3">
					후원자님께서 작성해 주신 내용을 바탕으로 담당자의 검토가 이루어질 예정이며,<br />
					프로그램 진행에 문제가 없을 경우 유선으로 자세한 후원 진행 방법을 안내해 드리겠습니다.
				</p>


				<div class="tac">
					<a ng-click="modal.request($event)" class="btn_type1">신청하기</a>
				</div>
			</div>


		</div>
	</div>
	<!--// popup -->

</div>