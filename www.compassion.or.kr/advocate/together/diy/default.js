﻿(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, popup, paramService) {

        $scope.requesting = false;
        $scope.total = -1;
        $scope.isSearch = false;
        $scope.rowsPerPage = 9;

        $scope.list = [];
        $scope.nationList = [];
        $scope.params = {
            page: 1,
            rowsPerPage: $scope.rowsPerPage,
        	k_word : ""
        };

        // 파라미터 초기화
        $scope.params = $.extend($scope.params, paramService.getParameterValues());
        if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);

        // 검색
        $scope.search = function () {
            $scope.params.page = 1;
            $scope.params.k_word = $("#k_word").val();

            $scope.getList();
        }

        // list
        $scope.getList = function (params) {

            $http.get("/api/advocate.ashx?t=diy_list", { params: $scope.params }).success(function (result) {
                $scope.list = result.data;
                $scope.total = result.data.length > 0 ? result.data[0].total : 0;

                $scope.isSearch = false;
                if ($scope.params.k_word != "") {
                	$scope.isSearch = true;
                }

            });

            if (params)
                scrollTo($("#l"), 10);
        }

        // 상세페이지
        $scope.goView = function (id, $event) {
        	$http.post("/api/advocate.ashx?t=hits&type=diy&id=" + id).then().finally(function () {
        		location.href = "/advocate/together/diy/view/" + id + "?" + $.param($scope.params);
        	});
        	$event.preventDefault();
        }
        $scope.getList();

        // 사연 신청
        $scope.modal = {
            instance: null,

            init: function () {
                // 팝업
                popup.init($scope, "/advocate/together/diy/write", function (modal) {
                	$scope.modal.instance = modal;
                	$("#ds_phone").mobileFormat();

                }, { top: 0, iscroll: true, backgroundClick: 'n' });
            },

            show: function ($event) {
            	$event.preventDefault();

                if (!$scope.modal.instance)
                    return;

                if (common.checkLogin()) {
                    $http.post("/api/advocate.ashx?t=check_user").success(function (r) {
                        if (!r.success) {
                            alert(r.message);
                            return;
                        }
                        $scope.modal.instance.show();
                    });
                }

            },

            request: function ($event) {
				/*
                if (!validateForm([
					{ id: "#ds_name", msg: "이름을 입력하세요" },
					{ id: "#ds_phone", msg: "휴대폰번호를 입력하세요", type: "phone" },
					{ id: "#ds_content", msg: "사연 및 실천방안 소개를 입력하세요" }
                ])) {
                	$(".guide_comment2").show();
                	return;
                } else {
                	$(".guide_comment2").hide();
                }
				*/

                if ($("#ds_name").val() == "" || $("#ds_phone").val() == "" || $("#ds_content").val() == "") {
                	$(".guide_comment2").show();
                	return;
				}else{
					$(".guide_comment2").hide();
				}

                if ($scope.requesting) return;
                $scope.requesting = true;

                var param = {
                	ds_name: $("#ds_name").val(),
                	ds_phone: $("#ds_phone").val(),
                	ds_content: $("#ds_content").val()
                }

                $http.post("/api/advocate.ashx?t=add_story", param).success(function (r) {
                    $scope.requesting = false;
                    if (r.success) {
                    	alert("신청이 완료되었습니다.\n후원자님의 귀한 섬김에 언제나 감사드립니다.");
                    	location.reload();
                    } else {
                        alert(r.message);
						$scope.modal.close($event);
                    }

                });
            },

            close: function ($event) {

                if (!$scope.modal.instance)
                    return;

                $scope.modal.instance.hide();
                $event.preventDefault();
            }

        }
        $scope.modal.init();


        $scope.parseDate = function (datetime) {
        	return new Date(datetime);
        }

    });

})();