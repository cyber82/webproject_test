﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_together_diy_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/diy/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션 DIY(Donate It Your way)</em></h1>
				<span class="desc">다 함께 나누는 기쁨! 후원자님이 직접 만드는 특별한 후원 프로그램으로 사랑을 전해보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 advocate">

			<div class="visual_diy">
				<p class="tit">후원자님만 할 수 있는 특별한 나눔</p>
				<span class="bar"></span>
				<p class="s_con3">
					DIY는 후원자님께서 직접 후원 프로그램을 만들고, 함께 참여할 분들을 모집하는 활동입니다.<br />
					후원자님만 할 수 있는 특별한 나눔을 실천해 보세요!
				</p>
				<a href="#" ng-click="modal.show($event)" class="btn_b_type2">프로그램 사연 신청</a>
			</div>

			<div class="diyList w980">

				<!-- 게시판 리스트 -->
				<div class="boardList_1">

					<div class="sortWrap">
						<div class="fr relative">
							<label for="k_word" class="hidden">검색어 입력</label>
							<input type="text" id="k_word" ng-enter="search()" name="k_word"  class="input_search1" style="width:245px" placeholder="검색어를 입력해 주세요" />
							<a ng-click="search()" class="search_area1">검색</a>
						</div>
					</div>
					


					<ul class="list">
						<li ng-repeat="item in list">
							<!-- 이미지 사이즈 : 308 * 226 -->
							<span class="img_wh"><img ng-src="{{item.db_thumb}}" alt="DIY 이미지" /></span>
							<span class="tit_box">
								<span class="tit">{{item.db_title}}</span>
								<span class="name">{{item.db_name}} 후원자님</span>
								<span class="day">{{parseDate(item.db_regdate)  | date:'yyyy.MM.dd' }}</span> 
								<span class="bar"></span>
								<span class="hit">{{item.db_hits}}</span>
							</span>

							<!-- 마우스 오버 시 -->
							<a href="#" class="over" ng-click="goView(item.db_id , $event)"><span class="btn_view1">VIEW</span></a>
						</li>

						<!-- 검색결과 없을때 -->
						<li class="no_result" ng-if="total == 0 && isSearch">검색 결과가 없습니다.</li>
						<!--//  -->

						<!-- 게시글 없을때 -->
						<li class="no_content" ng-if="total == 0 && !isSearch">등록된 글이 없습니다.</li>
						<!--//  -->

					</ul>
				</div>
				<!--// 게시판 리스트 -->
				<!-- page navigation -->
				<div class="tac">
					<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  
				</div>
				<!--//page navigation -->

				<div class="contact_qna">
					<p class="tit">문의</p>
					<div class="clear2">
						<div class="tel"><span class="icon"><span class="txt">전화 : </span>02) 3668-3450</span></div>
						<div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:ebaek@compassion.or.kr">ebaek@compassion.or.kr</a></span></div>
					</div>
				</div>

			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->






</asp:Content>
