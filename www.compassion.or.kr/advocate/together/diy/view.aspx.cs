﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class advocate_together_diy_view : FrontBasePage {

	const string listPath = "/advocate/together/diy";


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}
		base.PrimaryKey = requests[0];

		id.Value = PrimaryKey.ToString();
	}


	protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<diy_board>("db_id", Convert.ToInt32(PrimaryKey), "db_display", 1);
            //if (!dao.diy_board.Any(p => p.db_id == Convert.ToInt32(PrimaryKey) && p.db_display))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var entity = dao.diy_board.First(p => p.db_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<diy_board>("db_id", Convert.ToInt32(PrimaryKey));

            db_title.Text = entity.db_title;
            db_regdate.Text = entity.db_regdate.ToString("yyyy.MM.dd");
            db_hits.Text = entity.db_hits.ToString("N0");
            db_content.Text = entity.db_content;

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-컴패션 DIY] {0}", entity.db_title);

            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = "";
            //this.ViewState["meta_keyword"] = "";
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,FrontBasePage)
            this.ViewState["meta_image"] = entity.db_thumb.WithFileServerHost();

        }
	} 


}