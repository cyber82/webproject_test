﻿(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http) {

		$scope.isLogin = common.isLogin();
		$scope.userId = common.getUserId();

		$scope.total = 0;
		$scope.page = 1;
		$scope.rowsPerPage = 10;


		$scope.list = [];
		$scope.params = {
			id: $("#id").val(),
			page: $scope.page,
			rowsPerPage: $scope.rowsPerPage,
			type: "diy"
		};

		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);
			$http.get("/api/comment.ashx?t=list", { params: $scope.params }).success(function (result) {
				if (result.success) {
					$scope.list = result.data;
					$scope.total = result.data.length > 0 ? result.data[0].total : 0;
				}
			});
		}

		$scope.add = function () {

			if ($scope.content == "") {
				alert("댓들을 입력해주세요.");
				$("#reply").focus();
				return false;
			}

			// 신규 등록
			$http.post("/api/comment.ashx", { t: 'add', id: $("#id").val(), content: $scope.content, type: $scope.params.type }).success(function (result) {
				if (result.success) {
					$scope.getList({ page: 1 });
					$("#count").text(0);
				} else {
					alert(result.message);
				}
			})

			$scope.content = "";
		}

		$scope.update = function (idx) {
			//console.log($scope.content);
			target = $(".modifyArea[data-idx=" + idx + "]").find(".textarea_type1")
			content = target.val();

			if (content == "") {
				alert("댓들을 입력해주세요.");
				target.focus();
				return false;
			}

			$http.post('/api/comment.ashx', { t: 'update', id: idx, content: content }).success(function (result) {
				if (result.success) {
					$scope.getList();
				} else {
					alert(result.message);
				}
			})

			$scope.toggleEvent(idx);

		}

		$scope.toggleEvent = function (idx) {
			//$event.preventDefault();
			all = $(".modifyArea");
			target = $(".modifyArea[data-idx=" + idx + "]");

			if (target.css("display") == "none") {
				all.hide();
				target.show();

				var textarea = target.find(".textarea_type1");
				var count = target.find(".modify_count");

				textarea.unbind("keyup");
				textarea.keyup(function () {
					count.text($(this).val().length)
				})
				textarea.trigger("keyup");

			} else {
				target.hide();
			}

		}

		$scope.remove = function (id) {

			if (!confirm('삭제하시겠습니까?')) {
				return false;
			}


			$http.post("/api/comment.ashx?t=remove&id=" + id).success(function (result) {
				if (result.success) {
					alert("삭제되었습니다.");
					$scope.getList();
				} else {
					alert(result.message);
				}
			});
		}

		$scope.parseDate = function (datetime) {
			return new Date(datetime);
		}

		$scope.getList();
	})
	;

})();