﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="notify.aspx.cs" Inherits="advocate_intro_notify" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
 

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	
				
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션 천만 명에게 알리기</em></h1>
				<span class="desc">감동적인 스토리로 소중한 사람들에게 컴패션을 소개해 보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 advocate">

			<div class="inform tac">

				<div class="visual_inform">
					<h2 class="sub_tit">천만 명에게 컴패션 알리기, 함께하는 방법을 알려드립니다</h2>
					<span class="bar"></span>
					<p class="s_con3">이미지와 짧은 영상으로 구성된 컴패션의 감동스러운 스토리들을<br />
					소중한 사람들과 함께 나눠 보세요.</p>
				</div>

				<div class="inform_is w980">
					<h2 class="sub_tit">컴패션 천만 명에게 알리기란?</h2>
					<p class="s_con3"><!--18px-->
						문자 메시지 및 모바일 메신저(카카오톡, 라인)를 이용하여 지인에게 컴패션의 스토리를 전달하는 프로젝트입니다.
					</p>
					<ul>
						<li><span class="s_con8">천만 명 알리기 프로젝트는 1인이 세 번에 걸쳐 지인에게 메시지를 통해 컴패션을 알리는 방식으로 진행됩니다.</span></li>
						<li><span class="s_con8">스토리 컨텐츠는 모바일 전용입니다. 링크를 통해 모바일 버전의 스토리를 확인하실 수 있습니다.</span></li>
					</ul>
				</div>

				<div class="course">
					<h2 class="sub_tit">참여 과정</h2>
					<span class="bar"></span>
					<ul class="howTo clear2">
						<li><span class="s_con3">STEP 1<br />후원자 스토리 알리기</span></li>
						<li><span class="s_con3">STEP 2<br />컴패션 스토리 알리기</span></li>
						<li><span class="s_con3">STEP 3<br />후원 추천 카드 보내기</span></li>
					</ul>
				</div>

				<div class="procedure w980">
					<div class="stepBox step1">
						<span class="curcle">Step1</span>
						<p class="s_tit6 mb15">친구에게 메시지와 함께 <em class="fc_blue">‘컴패션 후원자 스토리’</em> 링크를 보내 주세요</p>
						<p class="s_con3 mb35">첫 번째 스토리는 총 4개로 구성되어 있습니다. 스토리 내용을 확인하시고, 지인의 상황에 맞는 스토리를 한 가지만 선택하여 메시지와 함께<br />지인에게 공유해 주세요.</p>

						<div class="sample_box clear2">
							<div class="sample">
								<p class="s_tit5">후원자 스토리 알리기 메시지 예시</p>
								<img src="/common/img/page/advocate/sample_img_01.png" alt="후원자 스토리 알리기 메시지 예시이미지" />
							</div>

							<div class="story">
								<p class="s_tit5">스토리 미리보기<span>원하는 스토리를 선택하시면 모바일 스토리 페이지로 이동합니다.</span></p>
								<ul class="shortcut clear2">
									<li>
										<a onclick="winpop('/10million/shoeshine_01.html')">
											<div class="img">
												<img src="/common/img/page/advocate/story_img_01.png" alt="스토리 미리보기 이미지" />
											</div>
											<p><span class="st_tit">김정하 구두닦이 목사님</span></p>
										</a>
									</li>
									<li>
										<a onclick="winpop('/10million/cfc.html')">
											<div class="img">
												<img src="/common/img/page/advocate/story_img_02.png" alt="스토리 미리보기 이미지" />
											</div>
											<p><span class="st_tit">Cycling For Compassion</span></p>
										</a>
									</li>
									<li>
										<a onclick="winpop('/10million/neldi.html')">
											<div class="img">
												<img src="/common/img/page/advocate/story_img_03.png" alt="스토리 미리보기 이미지" />
											</div>
											<p><span class="st_tit">세진이와 넬디 이야기</span></p>
										</a>
									</li>
									<li>
										<a onclick="winpop('/10million/shoeshine_02.html')">
											<div class="img">
												<img src="/common/img/page/advocate/story_img_04.png" alt="스토리 미리보기 이미지" />
											</div>
											<p><span class="st_tit">전용출 후원자 이야기</span></p>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="stepBox step2">
						<span class="curcle">Step2</span>
						<p class="s_tit6 mb15">두 번째 메시지와 함께 <em class="fc_blue">‘컴패션 더 알아가기’</em> 영상 링크를 보내 주세요</p>
						<p><span class="s_con8">첫번째 스토리 공유 후 2-3일 후에 지인에게 다시 한 번 스토리를 공유해 주세요.</span></p>
						<p class="mb35"><span class="s_con8">두 번째 스토리는 컴패션에 대해 더 깊게 알아볼 수 있는 이야기들로 구성되어 있습니다.</span></p>

						<div class="sample_box clear2">
							<div class="sample">
								<p class="s_tit5">후원자 스토리 알리기 메시지 예시</p>
								<img src="/common/img/page/advocate/sample_img_02.png" alt="후원자 스토리 알리기 메시지 예시이미지" />
							</div>

							<div class="story">
								<p class="s_tit5">스토리 미리보기<span>원하는 스토리를 선택하시면 모바일 스토리 페이지로 이동합니다.</span></p>
								<ul class="shortcut">
									<li>
										<a onclick="winpop('/10million/history.html')">
											<div class="img">
												<img src="/common/img/page/advocate/story_img_05.png" alt="스토리 미리보기 이미지" />
											</div>
											<p><span class="st_tit">컴패션 히스토리</span></p>
										</a>
									</li>
									<li>
										<a onclick="winpop('/10million/finance.html')">
											<div class="img">
												<img src="/common/img/page/advocate/story_img_06.png" alt="스토리 미리보기 이미지" />
											</div>
											<p><span class="st_tit">양육의 투명성</span></p>
										</a>
									</li>
									<li>
										<a onclick="winpop('/10million/miriam.html')">
											<div class="img">
												<img src="/common/img/page/advocate/story_img_07.png" alt="스토리 미리보기 이미지" />
											</div>
											<p><span class="st_tit">미리암의 노래</span></p>
										</a>
									</li>
									<li>
										<a onclick="winpop('/10million/mine.html')">
											<div class="img">
												<img src="/common/img/page/advocate/story_img_08.png" alt="스토리 미리보기 이미지" />
											</div>
											<p><span class="st_tit">금 캐는 아이들</span></p>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="stepBox step3">
						<span class="curcle">Step3</span>
						<p class="s_tit6 mb15"><em class="fc_blue">‘내 안에 컴패션’</em>의 마음을 담아 후원 추천 카드 보내기</p>
						<p class="s_con3 mb35">셋째 날은 온라인 애드보킷 분들이 가지고 계신 컴패션의 마음을 전달하는 날입니다.<br />
						직접 마음을 담아 메시지를 작성해 주시고, 어린이의 손을 잡아 줄 수 있는 링크를 함께 공유해 주세요~</p>

						<div class="sample_box clear2">
							<div class="sample">
								<p class="s_tit5">후원 추천 메시지 예시</p>
								<img src="/common/img/page/advocate/sample_img_03.png" alt="후원자 스토리 알리기 메시지 예시이미지" />
							</div>

							<div class="story">
								<p class="s_tit5">후원 추천 카드 미리보기<span>원하는 카드를 선택하시면 후원 카드 페이지로 이동합니다.</span></p>
								<ul class="shortcut">
									<li>
										<a onclick="winpop('/10million/letter_03.html')">
											<img src="/common/img/page/advocate/story_img_09.png" alt="후원 추천 카드 이미지" />
										</a>
									</li>
									<li>
										<a onclick="winpop('/10million/letter_04.html')">
											<img src="/common/img/page/advocate/story_img_10.png" alt="후원 추천 카드 이미지" />
										</a>
									</li>									
								</ul>
							</div>
						</div>
					</div>
					
				</div>
				<script type="text/javascript">
				    function getHost() {
				        var host = '';
				        var loc = String(location.href);
				        if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
				        else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
				        else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
				        return host;
				    }

				    function winpop(page) {
				        var host = getHost();
						var url = 'http://' + host + page;
						window.open(url, '', 'toolbar=no,location=yes,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=450,height=700,left=400px,top=50');
					}
				</script>

				<div class="conb">
					<p class="sub_tit">당신의 작은 행동이 가난 가운데 있는 어린이들에게<br />
					큰 힘이 될 것입니다</p>
					<span class="bar"></span>
					<p class="con">Releasing children from poverty in Jesus’ name</p>
					<span class="img_logo"></span>
				</div>
			</div>

		</div>

		<!--// e: sub contents -->

    </section>
    <!--// sub body -->

</asp:Content>