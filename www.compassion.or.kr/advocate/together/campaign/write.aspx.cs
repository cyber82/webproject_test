﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class advocate_together_diy_write : FrontBasePage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		if (!UserInfo.IsLogin) {
			Response.ClearContent();
			return;
		}


		UserInfo sess = new UserInfo();
		// 이름
		name.Value = sess.UserName;

		locationType.Value = sess.LocationType;
		if (sess.LocationType == "국내") {

			var addr_result = new SponsorAction().GetAddress();
			if (!addr_result.success) {
				base.AlertWithJavascript(addr_result.message, "goBack()");
				return;
			}

			SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;
			hfAddressType.Value = addr_data.AddressType;
			zipcode.Value = addr_data.Zipcode;
			addr1.Value = addr_data.Addr1;
			addr2.Value = addr_data.Addr2;
			dspAddrDoro.Value = addr_data.DspAddrDoro;
			dspAddrJibun.Value = addr_data.DspAddrJibun;

			var comm_result = new SponsorAction().GetCommunications();
			if (!comm_result.success) {
				base.AlertWithJavascript(comm_result.message, "goBack()");
				return;
			}

			SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
			phone.Value = comm_data.Mobile.TranslatePhoneNumber();
		}
	}

}