﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="advocate_together_campaign_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/advocate/together/campaign/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">


	
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션 애드보킷 캠페인</em></h1>
				<span class="desc">캠페인별로 제공되는 다양한 프로그램과 패킷을 활용하여 나눔의 기쁨을 느껴 보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 advocate">

				<div class="visual_campaign">
					<p class="tit">다양한 캠페인을 만나보세요</p>
					<span class="bar"></span>
					<p class="s_con3">
						컴패션 애드보킷 캠페인이란, 시즌별 컨셉에 맞는 홍보 활동 및 결연 행사를 진행하는 캠페인입니다.<br />
						캠페인별로 제공되는 다양한 프로그램과 패킷을 활용하여 나눔의 기쁨을 느껴 보세요!
					</p>
				</div>

				<div class="w980 campaign">

					<h2>진행 중인 캠페인</h2>

					<!-- 페이지 당 노출갯수 : 5개 -->
					<ul class="campaignList">
				
						<li  ng-repeat="item in list" >
							<!-- 이미지사이즈 : 410 * 170 -->
							<span class="img"><a href="#"><img ng-src="{{item.sc_thumb}}" alt="이벤트 이미지" /></a></span>
							<span class="con">
								<a ng-click="goView(item.sc_id)" class="tit">{{item.sc_title}}</a>
								<p class="txt" ng-if="item.sc_announce">
									참여 기간 : {{parseDate(item.sc_begin) | date:'yyyy년 MM월 dd일'}} ~ {{parseDate(item.sc_end) | date:'yyyy년 MM월 dd일'}}<br />
									당첨자 발표 : {{parseDate(item.sc_announce) | date:'yyyy년 MM월 dd일'}}<br />
									제공 아이템 : {{item.sc_offer}}
								</p>
                                <p class="txt" ng-if="!item.sc_announce">
									참여 기간 : {{parseDate(item.sc_begin) | date:'yyyy년 MM월 dd일'}} ~ {{parseDate(item.sc_end) | date:'yyyy년 MM월 dd일'}}<br />
									제공 아이템 : {{item.sc_offer}}
								</p>
							</span>
							<span class="status">
								<span class="ing" ng-if="!item.is_end">진행 중</span>
								<span class="end" ng-if="item.is_end">마감</span>
							</span>
						</li>
						
						<li class="no_content" ng-if="total == 0">등록된 글이 없습니다.</li>
					</ul>

					<!-- page navigation -->
					<div class="tac mb60">
						<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  
					</div>
					<!--// page navigation -->

				</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->







</asp:Content>
