﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="advocate_together_campaign_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
    <script type="text/javascript" src="/advocate/together/campaign/view.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
 
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<input type="hidden" runat="server" id="locationType" />
	    <input type="hidden" id="id" runat="server"/>
        <input type="hidden" id="alreadyApply" runat="server" value="false"/>

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션 애드보킷 캠페인</em></h1>
				<span class="desc">캠페인별로 제공되는 다양한 프로그램과 패킷을 활용하여 나눔의 기쁨을 느껴 보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents advocate">

			<div class="campaignV w980">
				
				<!-- 게시판 상세 -->
				<div class="boardView_1 line1">
					<div class="tit_box noline">
						<span class="tit"><asp:Literal runat="server" ID="sc_title" /></span>
						<span class="txt">
							<span>참여 기간 : <asp:Literal runat="server" ID="sc_begin" /> ~ <asp:Literal runat="server" ID="sc_end" /></span>
                            <asp:PlaceHolder ID="lb_sc_announce" runat="server" Visible="false">
							<span class="bar"></span>
							<span>당첨자 발표 : <asp:Literal runat="server" ID="sc_announce" /></span>
                            </asp:PlaceHolder>
							<span class="bar"></span>
							<span>제공 아이템 : <asp:Literal runat="server" ID="sc_offer" /></span>
						</span>
					</div>
					<div class="view_contents">
						<asp:Literal runat="server" ID="sc_content" />
					</div>

					<!-- sns 공유하기 -->
					<div class="share_box">
						<span class="sns_ani">
							<span class="common_sns_group">
								<span class="wrap">
									<a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
                                    <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
                                    <a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
                                    <a href="#" title="url 공유" data-role="sns" data-provider="copy" class="sns_url">url 공유</a>
								</span>
							</span>
							<button class="common_sns_share">공유하기</button>
						</span>
					</div>
					<!--// -->
					
				</div>
				<!--// 게시판 상세 -->

				<div class="btn_ac relative">
					<a ng-click="modal.show()" class="btn_type1" id="btn_request" runat="server">신청</a>
					<a runat="server" id="btnList" class="btn_type4 posR">목록</a>			
				</div>

			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

</asp:Content>