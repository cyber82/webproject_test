﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text;
using System.IO;

public partial class TopMaster : System.Web.UI.MasterPage {

	protected void Page_Load(object sender, EventArgs e) {
        this.ViewState["isMetaOgImg"] = "N";
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        head_meta.RenderControl(hw);
        if (sb.ToString().Contains("og:image")) {
            this.ViewState["isMetaOgImg"] = "Y";
        }
        //if (Request.Url.ToString().Contains("/participation/event/view/")) {
        //    this.ViewState["isMetaOgImg"] = "N";
        //}

        //

        if (!IsPostBack) {
            
			this.OnBeforePostBack();
		}

        /*
		var userAddr = Request.UserHostAddress;
		if (!(userAddr.IndexOf("192.168") > -1
			|| userAddr == "127.0.0.1"
			|| userAddr.IndexOf("10.10.100") > -1		//vpn
			|| userAddr == "183.98.89.248"		// 개발서버
			|| userAddr.IndexOf("211.35.74") > -1   // 펜타내부
			)) {
			Response.Clear();
			Response.End();
		}
		*/

        //	if (!IsPostBack) {
        /*
		is_login.Value = "N";
		if(UserInfo.IsLogin) {
			var sess = new UserInfo();
			ph_after_login.Visible = true;
			is_login.Value = "Y";
			_userid.Value = sess.UserId;
			user_id.Text = string.Format("{0}님&nbsp;" , sess.UserName);

			var actionResult = new LetterAction().GetUnreadCount();
			if(actionResult.success) {
				var count = ((LetterAction.UnreadEntity)actionResult.data).cnt;
				letter_count.Visible = count > 0;
				letter_count.InnerHtml = count > 99 ? "99+" : count.ToString();
			} else {
				letter_count.Visible = false;
			}
			

		} else {
			ph_before_login.Visible = true;
		}
		*/

        //	_loggedin.Value = FrontLoginSession.HasCookie(this.Context) ? "1" : "0";
        //	this.OnBeforePostBack();
        //	}
        #region GA Tag Manager Data
        string dimension1 = "", dimension2 = "", dimension3 = "", dimension4 = "X", dimension5 = "X", dimension6 = "X",
            dimension7 = "X", dimension8 = "X", dimension9 = "X", dimension10 = "X", dimension11 = "X", dimension12 = "X";

        dimension1 = UserInfo.IsLogin ? "Y" : "N";  //고객_로그인여부    Y / N

        var isApp = AppSession.HasCookie(this.Context);
        var WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0)) { WebMode = "M"; }
        else { WebMode = "WWW"; }
        dimension3 = isApp ? "APP" : WebMode;   //채널_액세스유형    WWW / M / APP

        if (UserInfo.IsLogin)
        {
            UserInfo sess = new UserInfo();

            dimension2 = sess.SponsorID.EmptyIfNull();  //고객_ID   20030701000450900
            dimension4 = sess.Birth.EmptyIfNull().Length >= 4 ? sess.Birth.Substring(0, 4) : "X";  //고객_생년   1979 / X

            var genderCode = "";
            switch (sess.GenderCode.EmptyIfNull())
            {
                case "M": genderCode = "남"; break;
                case "F": genderCode = "여"; break;
                case "C": genderCode = "단체"; break;
                case "E": genderCode = "기타"; break;
                case "": genderCode = "X"; break;
                default: genderCode = "X"; break;
            }
            dimension5 = genderCode;    //dimension5    고객_성별   남 / 여 / 단체 / 기타 / X

            dimension6 = sess.ReligionType.EmptyIfNull().Equals("") ? "X" : sess.ReligionType;   //고객_종교   기독교, 무교, 불교, 천주교, 기타, X
            dimension7 = sess.ChannelType.EmptyIfNull().Equals("") ? "X" : sess.ChannelType;   //고객_가입경로 APP, EMAIL, FAX, IVR, mapp, mWeb, Web 기타, 방문, 우편, 이메일, 전화, 행사집계, X - 최초 가입 경로 (ChannelType)
            dimension8 = sess.AgreeEmail.EmptyIfNull().Equals("") ? "X" : sess.AgreeEmail;   //고객_메일 수신여부  Y / N / X
            dimension9 = sess.UserDate.EmptyIfNull().Equals("") ? "X" : sess.UserDate;   //고객_웹가입일     2017 - 11 - 20 / X
            dimension10 = sess.RegisterDate.EmptyIfNull().Equals("") ? "X" : sess.RegisterDate;   //고객_컴파스등록일   2017 - 11 - 20 / X - 최초 후원계정데이터 생성일(RegisterDate)
            dimension11 = sess.SponsorType.EmptyIfNull().Equals("") ? "X" : sess.SponsorType;   //고객_후원유형     결연 , 결연 / 머니 , 결연 / 머니 / 일반 , 결연 / 일반 , 결연 / 편지 , 결연 / 편지 / 일반 , 머니 , 머니 / 일반 , 일반 , 편지 , 편지 / 일반 , X
            dimension12 = sess.LastPaymentDate.EmptyIfNull().Equals("") ? "X" : sess.LastPaymentDate;   //고객_마지막납부일   2017 - 11 - 20 / X
        }

        this.ViewState["dimension1"] = dimension1;
        this.ViewState["dimension2"] = dimension2;
        this.ViewState["dimension3"] = dimension3;
        this.ViewState["dimension4"] = dimension4;
        this.ViewState["dimension5"] = dimension5;
        this.ViewState["dimension6"] = dimension6;
        this.ViewState["dimension7"] = dimension7;
        this.ViewState["dimension8"] = dimension8;
        this.ViewState["dimension9"] = dimension9;
        this.ViewState["dimension10"] = dimension10;
        this.ViewState["dimension11"] = dimension11;
        this.ViewState["dimension12"] = dimension12;
        #endregion
    }

    protected virtual void OnBeforePostBack() {

        this.ViewState["is_login"] = "N";
		this.ViewState["_userid"] = "";

		if(UserInfo.IsLogin) {
			var sess = new UserInfo();
			ph_after_login.Visible = true;
            this.ViewState["is_login"] = "Y";
            this.ViewState["_userid"] = sess.UserId; 

            user_id.Text = string.Format("{0}님&nbsp;", sess.UserName);

			var actionResult = new LetterAction().GetUnreadCount();
			if(actionResult.success) {
				var count = ((LetterAction.UnreadEntity)actionResult.data).cnt;
				letter_count.Visible = count > 0;
				letter_count.InnerHtml = count > 99 ? "99+" : count.ToString();
			} else {
				letter_count.Visible = false;
			}


		} else {
			ph_before_login.Visible = true;
		}


        using (FrontDataContext dao = new FrontDataContext())
        {
            Object[] op1 = new Object[] { "count", "position"};
            Object[] op2 = new Object[] { 3, "main_gnb_sponsor" };
            var list = www6.selectSP("sp_mainpage_list_f", op1, op2).DataTableToList<CommonLib.sp_mainpage_list_fResult>();

            //repeater_gnb_banner_sponsor.DataSource = dao.sp_mainpage_list_f(3, "main_gnb_sponsor");
            repeater_gnb_banner_sponsor.DataSource = list;
            repeater_gnb_banner_sponsor.DataBind();

            Object[] op3 = new Object[] { "count", "position" };
            Object[] op4 = new Object[] { 3, "main_gnb_participation" };
            var list2 = www6.selectSP("sp_mainpage_list_f", op3, op4).DataTableToList<CommonLib.sp_mainpage_list_fResult>();

            //repeater_gnb_banner_participation.DataSource = dao.sp_mainpage_list_f(3, "main_gnb_participation");
            repeater_gnb_banner_participation.DataSource = list2;
            repeater_gnb_banner_participation.DataBind();

            Object[] op5 = new Object[] { "count", "position" };
            Object[] op6 = new Object[] { 3, "main_gnb_sympathy" };
            var list3 = www6.selectSP("sp_mainpage_list_f", op5, op6).DataTableToList<CommonLib.sp_mainpage_list_fResult>();

            //repeater_gnb_banner_sympathy.DataSource = dao.sp_mainpage_list_f(3, "main_gnb_sympathy");
            repeater_gnb_banner_sympathy.DataSource = list3;
            repeater_gnb_banner_sympathy.DataBind();

            Object[] op7 = new Object[] { "count", "position" };
            Object[] op8 = new Object[] { 3, "main_gnb_about-us" };
            var list4 = www6.selectSP("sp_mainpage_list_f", op7, op8).DataTableToList<CommonLib.sp_mainpage_list_fResult>();

            //repeater_gnb_banner_about_us.DataSource = dao.sp_mainpage_list_f(3, "main_gnb_about-us");
            repeater_gnb_banner_about_us.DataSource = list4;
            repeater_gnb_banner_about_us.DataBind();

            Object[] op9 = new Object[] { "count", "position" };
            Object[] op10 = new Object[] { 3, "main_gnb_activity" };
            var list5 = www6.selectSP("sp_mainpage_list_f", op9, op10).DataTableToList<CommonLib.sp_mainpage_list_fResult>();

            //repeater_gnb_banner_activity.DataSource = dao.sp_mainpage_list_f(3, "main_gnb_activity");
            repeater_gnb_banner_activity.DataSource = list5;
            repeater_gnb_banner_activity.DataBind();
        }


	}
    
    public virtual ContentPlaceHolder Content
	{
		get
		{
			return this.body;
		}
	}

	public virtual string Title
	{
		get
		{
			return title.InnerText;
		}
		set
		{
			title.InnerText = value;
		}
	}
    
}
