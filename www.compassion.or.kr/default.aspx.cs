﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CommonLib;
using Newtonsoft.Json.Linq;
using TCPTModel;
using TCPTModel.Response.Supporter;
using Newtonsoft.Json;
using System.Text;
using TCPTModel.Request.Beneficiary;
using TCPTModel.Response.Beneficiary;
using System.Configuration;

public partial class _default : FrontBasePage {

    public WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

    protected override void OnBeforePostBack() {

        //Availabilityquery();

        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;

        //김태형 2017.06.01
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }
        

        base.OnBeforePostBack();


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.content.First(p => p.c_id == "main");
            var entity = www6.selectQF<content>("c_id", "main");

            var data = JObject.Parse(entity.c_text.ToString());

            children.Value = data["child"].ToString();
            main_content.Text = data["content"].ToString();

        }

        bool hasCDSP = false;

        if(UserInfo.IsLogin) {

            UserInfo sess = new UserInfo();

            DataTable childData = null;
			var actionResult = new SponsorAction().HasCDSP();
			if(actionResult.success) {

				childData = (DataTable)actionResult.data;
				if(childData.Rows.Count > 0) {
					hasCDSP = true;

					//	Response.Write(childData.ToJson());
					var row = childData.Rows[0];
                    //childPic.Src = ChildAction.GetPicByChildKey(row["childKey"].ToString());
                    //childPic.Src = new ChildAction().GetChildImage(row["childmasterId"].ToString(), row["childKey"].ToString());
                    childPic.ImageUrl = new ChildAction().GetChildImage(row["childmasterId"].ToString(), row["childKey"].ToString());
                    ViewState["childMasterId"] = row["childmasterId"].ToString();
                    userName.Text = sess.UserName;
					childName.Text = row["namekr"].ToString();
					btn_write_letter.HRef = btn_write_letter.HRef.Replace("{childmasterid}", row["childmasterId"].ToString());
					btn_album.HRef = btn_album.HRef.Replace("{childmasterid}", row["childmasterId"].ToString()).Replace("{childkey}", row["childkey"].ToString());


                    //childAge.Text = row["childAge"].ToString();

                    //2018-04-04 이종진 - 편지쓰기 권한 체크하여 편지쓰기 버튼 숨김 추가 #13832
                    JsonWriter childDetail = new ChildAction().MyChildren(row["childkey"].ToString(), 1, 1);
                    if(childDetail.success)
                    {
                        var children = (List<ChildAction.MyChildItem>)childDetail.data;
                        if (children.Count > 0)
                        {
                            if(!children[0].PAID || !children[0].CanLetter)
                            {
                                btn_write_letter.Visible = false;
                            }
                        }
                    }

                }
			}

			if(hasCDSP) {
				ph_sec1.Visible = ph_sec2.Visible = ph_sec5.Visible = ph_sec6.Visible = ph_sec8.Visible = ph_sec9.Visible = ph_sec12.Visible = ph_sec13.Visible = true;
			} else {
				ph_sec3.Visible = ph_sec4.Visible = ph_sec5.Visible = ph_sec6.Visible = ph_sec7.Visible = ph_sec8.Visible = ph_sec9.Visible = ph_sec10.Visible = ph_sec11.Visible = true;
			}

		} else {

			ph_sec3.Visible = ph_sec4.Visible = ph_sec5.Visible = ph_sec7.Visible = ph_sec8.Visible = ph_sec9.Visible = ph_sec10.Visible = ph_sec11.Visible = true;

		}

		this.GetPrayer(hasCDSP);

		this.GetSympathy();

		this.GetEvent();

		if(ph_sec3.Visible) {

			Random rnd = new Random();
			var index = rnd.Next(3);
			this.GetMainVisualChild(index);
		}

		if(ph_sec4.Visible) {
			this.GetChildren();
		}

		try {
			this.GetUserFunding();

			this.GetSpecialFunding();
		} catch { }
	}

	void GetSympathy()
    {

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_sympathy_list_latest_f();
            Object[] op1 = new Object[] {  };
            Object[] op2 = new Object[] {  };
            var list = www6.selectSP("sp_sympathy_list_latest_f", op1, op2).DataTableToList<sp_sympathy_list_latest_fResult>();

            repeater_sympathy.DataSource = list;
            repeater_sympathy.DataBind();
        }
	}

	void GetEvent() {

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_mainpage_list_f(10, "main_event");
            Object[] op1 = new Object[] { "count", "position" };
            Object[] op2 = new Object[] { 10, "main_event" };
            var list = www6.selectSP("sp_mainpage_list_f", op1, op2).DataTableToList<sp_mainpage_list_fResult>();

            repeater_event.DataSource = list;
            repeater_event.DataBind();
        }
	}

	void GetPrayer( bool hasCDSP )
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.prayer.FirstOrDefault(p => p.p_date == DateTime.Today);
            var entity = www6.selectQF<prayer>("p_date", DateTime.Today);

            if (entity == null)
            {
                pn_prayer.Visible = pn_prayer2.Visible = false;
                return;
            }

            prayer_content2.InnerHtml = prayer_content.InnerHtml = entity.p_content.ToHtml();
            prayer_date2.InnerText = prayer_date.InnerText = entity.p_date.ToString("M월 d일 (ddd)");

            if (hasCDSP)
            {
                prayer_check2.Visible = prayer_check.Visible = entity.p_check;
            }
            else
            {
                //pn_prayer.Style.Add("height", "125px;");
                //pn_prayer2.Style.Add("height", "125px;");
            }
        }
	}

	// 비로그인 나오는 어린이 추천 3종 , 일별 캐싱
	void GetMainVisualChild( int index ) {
		if(index == 0) {
			if(this.GetMainVisualType1()) {
				main_recmd1.Visible = true;
			} else {
				this.GetMainVisualType3();
				main_recmd3.Visible = true;
			}
		}else if(index == 1) {
			if(this.GetMainVisualType2()) {
				main_recmd2.Visible = true;
			} else {
				this.GetMainVisualType3();
				main_recmd3.Visible = true;
			}
		} else {
			this.GetMainVisualType3();
			main_recmd3.Visible = true;
		}

		
	}

	// 생일이 어제
	bool GetMainVisualType1() {
		
		var action = new ChildAction();
		var yesterday = DateTime.Now.AddDays(-1);
		var month = Convert.ToInt32(yesterday.ToString("MM"));
		var day = Convert.ToInt32(yesterday.ToString("dd"));

        //		var actionResult = action.GetChildren(null, null, month, day, null, 20, null, null, 1, 3, "new", -1, -1);
        //        var actionResult = action.GetChildrenGp(null, null, month, day, null, 20, null, null, 1, 3, "new", -1, -1);

        var actionResult = new JsonWriter();
        if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            actionResult = action.GetChildren(null, null, month, day, null, 20, null, null, 1, 3, "new", -1, -1);
        }
        else if (ConfigurationManager.AppSettings["dbgp_kind"] == "2")
        {
            actionResult = action.GetChildrenGp(null, null, month, day, null, 20, null, null, 1, 3, "new", -1, -1);
        }

        //Response.Write("actionResult.success = " + actionResult.success);


        if (actionResult.success) {
			Random rnd = new Random();
			var items = (List<ChildAction.ChildItem>)actionResult.data;
			if(items.Count < 1)
				return false;
			var index = rnd.Next(items.Count);

			var item = items[index];
			
			main_recmd1_pic.Src = item.Pic;
			main_recmd1_country.Text = main_recmd1_country2.Text = item.CountryName;
			main_recmd1_age.Text = main_recmd1_age2.Text = item.Age.ToString();
			main_recmd1_gender.Text = item.Gender;
			main_recmd1_waitingdays.Text = item.WaitingDays.ToString();
			main_recmd1_name.Text = item.Name;
			main_recmd1_name2.Text = item.Name.Length > 5 ? item.Name.Substring(0, 5) + ".." : item.Name;
			main_recmd1_link.HRef = main_recmd1_link.HRef.Replace("{childMasterId}", item.ChildMasterId);

			return true;
		} else {
			return false;
		}
	}

	// 부모님이 안계신
	bool GetMainVisualType2() {

		var action = new ChildAction();

        //		var actionResult = action.GetChildren(null, null, null, null, null, 20, true, null, 1, 3, "new", -1, -1);
        //        var actionResult = action.GetChildrenGp(null, null, null, null, null, 20, true, null, 1, 3, "new", -1, -1);

        var actionResult = new JsonWriter();
        if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            actionResult = action.GetChildren(null, null, null, null, null, 20, true, null, 1, 3, "new", -1, -1);
        }
        else if (ConfigurationManager.AppSettings["dbgp_kind"] == "2")
        {
            actionResult = action.GetChildrenGp(null, null, null, null, null, 20, true, null, 1, 3, "new", -1, -1);
        }

        if (actionResult.success) {
			Random rnd = new Random();
			//Response.Write(actionResult.data.ToJson());
			var items = (List<ChildAction.ChildItem>)actionResult.data;
			if(items.Count < 1)
				return false;
			var index = rnd.Next(items.Count);

			var item = items[index];

			main_recmd2_pic.Src = item.Pic;
			main_recmd2_country.Text = main_recmd2_country2.Text = item.CountryName;
			main_recmd2_age.Text = item.Age.ToString();
			main_recmd2_gender.Text = item.Gender;
			main_recmd2_waitingdays.Text = item.WaitingDays.ToString();
			main_recmd2_name.Text = item.Name;
			main_recmd2_name2.Text = item.Name.Length > 5 ? item.Name.Substring(0, 5) + ".." : item.Name;
			main_recmd2_link.HRef = main_recmd2_link.HRef.Replace("{childMasterId}", item.ChildMasterId);

			return true;
		} else {
			return false;
		}
	}

	// 랜덤
	bool GetMainVisualType3() {

	
		var action = new ChildAction();

        //		var actionResult = action.GetChildren(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);
        //        var actionResult = action.GetChildrenGp(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);

        var actionResult = new JsonWriter();
        if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            actionResult = action.GetChildren(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);
        }
        else if (ConfigurationManager.AppSettings["dbgp_kind"] == "2")
        {
            actionResult = action.GetChildrenGp(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);
        }

        if (actionResult.success) {
			Random rnd = new Random();
			var items = (List<ChildAction.ChildItem>)actionResult.data;
			if(items.Count < 1)
				return false;
			var index = rnd.Next(items.Count);

			var item = items[index];
			
			main_recmd3_pic.Src = item.Pic;
			main_recmd3_country.Text = main_recmd3_country2.Text = item.CountryName;
			main_recmd3_age.Text = item.Age.ToString();
			main_recmd3_gender.Text = item.Gender;
			main_recmd3_waitingdays.Text = item.WaitingDays.ToString();
			main_recmd3_name.Text = item.Name;
			main_recmd3_name2.Text = item.Name.Length > 5 ? item.Name.Substring(0, 5) + ".." : item.Name;
			main_recmd3_link.HRef = main_recmd3_link.HRef.Replace("{childMasterId}", item.ChildMasterId);
			return true;
		} else {
			return false;
		}


	}

	// 1:1 어린이 양육 목록 , 일별 캐싱
	void GetChildren() {
		 
		var action = new ChildAction();

        //var actionResult = action.GetChildren(null, null, null, null, null, 6, null, null, 1, 16, "waiting", -1, -1);
        /*20160207 후원>1:1어린이양육 기준으로 검색조건 수정*/

//       var actionResult = action.GetChildren(null, null, null, null, null, 20, null, null, 1, 16, "new", -1, -1);
       var actionResult = new JsonWriter();
        if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            actionResult = action.GetChildren(null, null, null, null, null, 20, null, null, 1, 16, "new", -1, -1);
        }
        else if (ConfigurationManager.AppSettings["dbgp_kind"] == "2")
        {
            actionResult = action.GetChildrenGp(null, null, null, null, null, 20, null, null, 1, 16, "new", -1, -1);
        }

        if (actionResult.success) {

			var items = (List<ChildAction.ChildItem>)actionResult.data;

			if(items.Count > 0) {
				Random rnd = new Random();
				var start_index = rnd.Next(items.Count);
                var end_index = 3;
                List<ChildAction.ChildItem> selected_list = new List<ChildAction.ChildItem>();

                if (items.Count < 3)
                {
                    end_index = items.Count;
                }

				for(int i = 0; i < end_index; i++) {

					var index = start_index + i;
					if(index >= items.Count) {
						index = index - items.Count;
					}

					selected_list.Add(items[index]);
				}

				repeater_children.DataSource = selected_list;
				repeater_children.DataBind();
			}
		}
		
	}

	// 특별한나눔 , 시별 캐싱
	void GetSpecialFunding() {

		var today = DateTime.Now.ToString("yyyyMMddHH");
		var action = new ChildAction();

		{

			var exist = false;
			if(Application["main_special_funding"] != null) {
				var data = (KeyValuePair<string, object>)Application["main_special_funding"];
				if(data.Key == today) {
					exist = true;
				} else {
					Application["main_special_funding"] = null;
				}
			}

			if(!exist)
            {
                using (FrontDataContext dao = new FrontDataContext())
                {
                    //var items = dao.sp_tSpecialFunding_list_f("", "", "N", "", "Y").ToList();
                    Object[] op1 = new Object[] { "AccountClassGroup", "AccountClass", "fixedYN", "ufYN", "pic" };
                    Object[] op2 = new Object[] { "", "", "N", "", "Y" };
                    var items = www6.selectSP("sp_tSpecialFunding_list_f", op1, op2).DataTableToList<sp_tSpecialFunding_list_fResult>();
                    //Response.Write( items.ToJson() );

                    var data = new KeyValuePair<string, object>(today, items);
                    Application["main_special_funding"] = data;


                }

			}

			if(Application["main_special_funding"] != null) {
				var data = (KeyValuePair<string, object>)Application["main_special_funding"];
				var items = (List<sp_tSpecialFunding_list_fResult>)data.Value;

                if (items.Count > 0)
                {
                    Random rnd = new Random();
                    var start_index = rnd.Next(items.Count);

                    List<sp_tSpecialFunding_list_fResult> selected_list = new List<sp_tSpecialFunding_list_fResult>();
                    for (int i = 0; i < 2; i++)
                    {

                        var index = start_index + i;
                        if (index >= items.Count)
                        {
                            index = index - items.Count;
                        }
                        
                        Object[] objParam = new object[] { "CampaignID" };
                        Object[] objValue = new object[] { items[index].CampaignID };
                        Object[] objSql = new object[] { "sp_S_Campaign_PaymentTotal" };
                        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["TOTAL"] != DBNull.Value)
                            {
                                long total = Convert.ToInt32(ds.Tables[0].Rows[0]["TOTAL"]);
                                items[index].sf_current_amount = total;
                            }
                            else
                            {
                                items[index].sf_current_amount = 0;
                            }
                        }
                        

                        selected_list.Add(items[index]);
                    }
                    repeater_special_funding.DataSource = selected_list;
                    repeater_special_funding.DataBind();
                }                  

			}
		}


	}

	// 나눔펀딩 , 시간별 캐싱
	void GetUserFunding() {

		var today = DateTime.Now.ToString("yyyyMMddHH");
		var action = new ChildAction();

		{

			var exist = false;
			if(Application["main_user_funding"] != null) {
				var data = (KeyValuePair<string, object>)Application["main_user_funding"];
				if(data.Key == today) {
					exist = true;
				} else {
					Application["main_user_funding"] = null;
				}
			}

			if(!exist)
            {
                using (FrontDataContext dao = new FrontDataContext())
                {
                    //var items = dao.sp_tUserFunding_pick_list_f().ToList();
                    Object[] op1 = new Object[] {  };
                    Object[] op2 = new Object[] {  };
                    var items = www6.selectSP("sp_tUserFunding_pick_list_f", op1, op2).DataTableToList<sp_tUserFunding_pick_list_fResult>();

                    var data = new KeyValuePair<string, object>(today, items);
                    Application["main_user_funding"] = data;
                }
			}

			if(Application["main_user_funding"] != null) {
				var data = (KeyValuePair<string, object>)Application["main_user_funding"];
				var items = (List<sp_tUserFunding_pick_list_fResult>)data.Value;

                if (items.Count > 0)
                {
                    Random rnd = new Random();
                    var start_index = rnd.Next(items.Count);

                    List<sp_tUserFunding_pick_list_fResult> selected_list = new List<sp_tUserFunding_pick_list_fResult>();
                    for (int i = 0; i < 2; i++)
                    {

                        var index = start_index + i;
                        if (index >= items.Count)
                        {
                            index = index - items.Count;
                        }

                        selected_list.Add(items[index]);
                        if (items.Count == 1)
                        {
                            break;
                        }
                    }
                    repeater_user_funding.DataSource = selected_list;
                    repeater_user_funding.DataBind();
                }
				

            }
        }


	}


    void Availabilityquery()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("IsCheck");
        dt.Columns.Add("BeneficiaryState");
        dt.Columns.Add("HoldingGlobalPartnerID");
        dt.Columns.Add("Beneficiary_LocalID");
        dt.Columns.Add("FullName");
        dt.Columns.Add("Gender");
        dt.Columns.Add("BirthDate");
        dt.Columns.Add("Age", typeof(int));
        dt.Columns.Add("IsOrphan");
        dt.Columns.Add("ICP_Country");
        dt.Columns.Add("ICP_ID");

        dt.Columns.Add("Beneficiary_GlobalID");
        dt.Columns.Add("HangulName");
        dt.Columns.Add("FieldOffice_Name");

        dt.Columns.Add("PreferredName");
        dt.Columns.Add("WaitingSinceDate");
        dt.Columns.Add("MinDaysWaiting", typeof(int));
        dt.Columns.Add("PriorityScore", typeof(float));
        dt.Columns.Add("HoldExpirationDate");
        dt.Columns.Add("IsInHIVAffectedArea");
        dt.Columns.Add("IsSpecialNeeds");

        BeneficiaryAvailabilityQueryRequest_POST REQ = new BeneficiaryAvailabilityQueryRequest_POST();

        //// 어린이 키
        //if (!string.IsNullOrEmpty(tbxChildKey.Text))
        //{
        //    List<string> value = new List<string>();
        //    value.Add(tbxChildKey.Text);
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("BeneficiaryLocalId", "is", value));
        //}

        //// 어린이 이름
        //if (!string.IsNullOrEmpty(tbxChildName.Text))
        //{
        //    List<string> value = new List<string>();
        //    value.Add(tbxChildName.Text);
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("BeneficiaryName", "is", value));
        //}

        //// Global ID
        //if (!string.IsNullOrEmpty(txtGlobalID.Text) && !string.IsNullOrEmpty(txtGlobalID.Text))
        //{
        //    List<string> value = new List<string>();
        //    value.Add(txtGlobalID.Text);
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("BeneficiaryGlobalId", "is", value));
        //}

        ////ICPID
        //if (!string.IsNullOrEmpty(txtICPID.Text))
        //{
        //    List<string> value = new List<string>();
        //    value.Add(txtICPID.Text);
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("ICPId", "anyof", value));
        //}

        ////국가
        //if (ddlChildArea.Text != "전체")
        //{
        //    List<string> value = new List<string>();

        //    if (ddlCountry.SelectedValue.ToString().Trim() != "" && ddlCountry.SelectedValue.ToString().Trim() != "전체")
        //    {
        //        string code = ((DataRowView)ddlCountry.Items[ddlCountry.SelectedIndex]).Row[1].ToString();
        //        value.Add(code);
        //    }

        //    else
        //    {
        //        for (int i = 0; i < ddlCountry.Items.Count; i++)
        //        {
        //            string code = ((DataRowView)ddlCountry.Items[i]).Row[1].ToString();

        //            if (code != "전체")
        //                value.Add(code);
        //        }
        //    }

        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("FieldOffice", "anyof", value));
        //}

        //else if (ddlCountry.SelectedValue.ToString().Trim() != "" && ddlCountry.SelectedValue.ToString().Trim() != "전체")
        //{
        //    List<string> value = new List<string>();
        //    value.Add(ddlCountry.SelectedValue.ToString());
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("FieldOffice", "anyof", value));
        //}

        //// 나이
        //if (!string.IsNullOrEmpty(tbxAgeFrom.Text) && !string.IsNullOrEmpty(tbxAgeTo.Text))
        //{
        //    List<string> value = new List<string>();
        //    value.Add(tbxAgeFrom.Text);
        //    value.Add(tbxAgeTo.Text);
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("Age", "between", value));
        //}

        //else if (!string.IsNullOrEmpty(tbxAgeFrom.Text) && string.IsNullOrEmpty(tbxAgeTo.Text))
        //{
        //    List<string> value = new List<string>();
        //    value.Add(tbxAgeFrom.Text);
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("Age", "equalto", value));
        //}


        ////생일 
        //if (!string.IsNullOrEmpty(tbxBirthYear.Text.Trim()))
        //{
        //    List<string> value = new List<string>();
        //    value.Add(tbxBirthYear.Text.Trim());
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("BirthYear", "equalto", value));
        //}

        //if (!string.IsNullOrEmpty(tbxBirthMonth.Text.Trim()))
        //{
        //    List<string> value = new List<string>();
        //    value.Add(tbxBirthMonth.Text.Trim());
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("BirthMonth", "equalto", value));
        //}

        //if (!string.IsNullOrEmpty(tbxBirthDay.Text.Trim()))
        //{
        //    List<string> value = new List<string>();
        //    value.Add(tbxBirthDay.Text.Trim());
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("BirthDay", "equalto", value));
        //}


        ////장애
        //if (cbxHandicapped_Y.Checked && !cbxHandicapped_N.Checked)
        //{
        //    List<string> value = new List<string>();
        //    value.Add("T");
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("PhysicalDisabilities", "is", value));
        //}

        //else if (!cbxHandicapped_Y.Checked && cbxHandicapped_N.Checked)
        //{
        //    List<string> value = new List<string>();
        //    value.Add("F");
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("PhysicalDisabilities", "is", value));
        //}

        ////고아
        //if (cbxOrphan_Y.Checked && !cbxOrphan_N.Checked)
        //{
        //    List<string> value = new List<string>();
        //    value.Add("T");
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("IsOrphan", "is", value));
        //}

        //else if (!cbxOrphan_Y.Checked && cbxOrphan_N.Checked)
        //{
        //    List<string> value = new List<string>();
        //    value.Add("F");
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("IsOrphan", "is", value));
        //}


        ////성별
        //if (cbxMale.Checked && !cbxFemale.Checked)
        //{
            List<string> value = new List<string>();
            value.Add("M");
        Filter f = new Filter();
        f.Field = "Gender";
        f.Value = value;
        f.Operator = "anyof";
            REQ.BeneficiarySearchRequestList.Filter.Add(f);
        //}

        //else if (!cbxMale.Checked && cbxFemale.Checked)
        //{
        //    List<string> value = new List<string>();
        //    value.Add("F");
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("Gender", "anyof", value));
        //}

        ////어린이 상태
        //if (ddlBeneficiaryStatus.Text != "전체")
        //{
        //    List<string> value = new List<string>();
        //    value.Add(ddlBeneficiaryStatus.Text);
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("BeneficiaryAMState", "anyof", value));
        //}

        ////Hold Partner
        //if (ddlHoldPartner.Text != "전체")
        //{
        //    List<string> value = new List<string>();
        //    value.Add(ddlHoldPartner.Text);
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("HoldingGlobalPartner", "anyof", value));
        //}

        //// hold 시작일
        //if (cbHoldStart.Checked)
        //{
        //    List<string> value = new List<string>();
        //    value.Add(dtHoldStartSDT.Text);
        //    value.Add(dtHoldStartEDT.Text);
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("HoldStartDate", "within", value));
        //}

        //// hold 종료일
        //if (cbHoldExpireDate.Checked)
        //{
        //    List<string> value = new List<string>();
        //    value.Add(dtHoldExpiredSDT.Text);
        //    value.Add(dtHoldExpiredEDT.Text);
        //    REQ.BeneficiarySearchRequestList.Filter.Add(Common.SetSearchFilter("HoldExpirationDate", "within", value));
        //}


        REQ.BeneficiarySearchRequestList.NumberOfBeneficiaries = 100;

        //if (cbx1000.Checked)
        //    REQ.BeneficiarySearchRequestList.NumberOfBeneficiaries = 1000;

        string json = JsonConvert.SerializeObject(REQ);
        TCPTService.Service _OffRampService = new TCPTService.Service();


        string result = _OffRampService.BeneficiaryAvailabilityQueryRequest_POST(json);

        TCPTModel.TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(result);

        if (msg.IsSuccessStatusCode)
        {
            TCPTModel.Response.Beneficiary.BeneficiarySearchResponseList_Kit kit = new TCPTModel.Response.Beneficiary.BeneficiarySearchResponseList_Kit();
            kit = JsonConvert.DeserializeObject<BeneficiarySearchResponseList_Kit>(msg.RequestMessage.ToString());

            //lblGridViewCount.Text = kit.BeneficiarySearchResponseList.Count.ToString();

            if (kit.BeneficiarySearchResponseList == null || kit.BeneficiarySearchResponseList.Count <= 0)
            {
                //MessageBox.Show("조회된 어린이가 없습니다.");
            }

            else
            {
                foreach (BeneficiarySearchResponseList item in kit.BeneficiarySearchResponseList)
                {
                    DataRow dr = dt.NewRow();

                    dr["IsCheck"] = false;
                    if (item.Beneficiary_GlobalID != null)
                        dr["Beneficiary_GlobalID"] = item.Beneficiary_GlobalID;
                    if (item.Beneficiary_LocalID != null)
                        dr["Beneficiary_LocalID"] = item.Beneficiary_LocalID;
                    if (item.BeneficiaryState != null)
                        dr["BeneficiaryState"] = item.BeneficiaryState;
                    if (item.Age != null)
                        dr["Age"] = item.Age;
                    if (item.FullName != null)
                        dr["FullName"] = item.FullName;
                    if (item.HangulName != null)
                        dr["HangulName"] = item.HangulName;
                    if (item.PreferredName != null)
                        dr["PreferredName"] = item.PreferredName;
                    if (item.BirthDate != null)
                        dr["BirthDate"] = item.BirthDate;
                    if (item.ICP_Country != null)
                        dr["ICP_Country"] = item.ICP_Country;
                    if (item.Gender != null)
                        dr["Gender"] = item.Gender;
                    if (item.IsOrphan != null)
                        dr["IsOrphan"] = item.IsOrphan;
                    if (item.WaitingSinceDate != null)
                        dr["WaitingSinceDate"] = item.WaitingSinceDate;
                    if (item.FieldOffice_Name != null)
                        dr["FieldOffice_Name"] = item.FieldOffice_Name;
                    if (item.ICP_ID != null)
                        dr["ICP_ID"] = item.ICP_ID;
                    if (item.IsOrphan != null)
                        dr["IsOrphan"] = item.IsOrphan;
                    if (item.IsInHIVAffectedArea != null)
                        dr["IsInHIVAffectedArea"] = item.IsInHIVAffectedArea;
                    if (item.IsSpecialNeeds != null)
                        dr["IsSpecialNeeds"] = item.IsSpecialNeeds;
                    if (item.HoldExpirationDate != null)
                        dr["HoldExpirationDate"] = item.HoldExpirationDate;
                    if (item.HoldingGlobalPartnerID != null)
                        dr["HoldingGlobalPartnerID"] = item.HoldingGlobalPartnerID;
                    if (item.MinDaysWaiting != null)
                        dr["MinDaysWaiting"] = item.MinDaysWaiting;
                    if (item.PriorityScore != null)
                        dr["PriorityScore"] = item.PriorityScore;

                    dt.Rows.Add(dr);
                }
            }
        }

        else
        {
            //MessageBox.Show("어린이 조회에 실패했습니다.");
        }

        //grvChildInfo.DataSource = dt;
    }

}
