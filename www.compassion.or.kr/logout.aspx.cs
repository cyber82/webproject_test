﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class logout : FrontBasePage
{


    protected override void OnBeforePostBack()
    {

        var isLocal = Request.Url.AbsoluteUri.IndexOf("local") > -1;
        string domainAuth = isLocal ? "https://auth.local.compassionkr.com" : ConfigurationManager.AppSettings["domain_auth"];


        var url = "";
        if (Request.UrlReferrer != null)
            url = Request.UrlReferrer.AbsoluteUri;
        Response.Redirect(domainAuth + "/logout/?r=" + HttpUtility.UrlEncode(url.Replace("default.aspx", "").Replace(".aspx", "")));
    }

}