﻿$(function () {

	$("#btn_goCDSP").click(function () {

		var childmasterid = $(this).data("childmasterid");

		$.get("/sponsor/pay-gateway.ashx?t=go-cdsp", { childMasterId: childmasterid }).success(function (r) {

			if (r.success) {
				location.href = r.data;
			} else {
				if (r.action == "nonpayment") {		// 미납금

					if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
						location.href = "/my/sponsor/pay-delay/";
					}

				} else {
					alert(r.message);
				}
			}
		});

	});

	$("#btn_login").click(function () {

		var userId = $("#user_id").val();
		var userPwd = $("#user_pwd").val();

		if (userId == "") {
			alert("아이디를 입력해주세요");
			$("#user_id").focus();
			return false;
		}

		if (userPwd == "") {
			alert("비밀번호를 입력해주세요");
			$("#user_pwd").focus();
			return false;
		}

		$("#userId").val(userId);
		$("#userPwd").val(userPwd);

		$("#exfrm").attr("action", $("#exfrm").attr("action") + "?action=REFER");
		$("#exfrm").submit();

		return false;

	});

	$("#btn_member").click(function () {
		$(".loginBox").height("376");
		$(".pn_add_form").hide();
		$(".pn_login").show();
		$("#user_id").focus();
		return false;
	})

	$("#btn_nonmember").click(function () {
		$(".loginBox").height("200");
		$(".pn_add_form").hide();
		$(".pn_nonmember").show();
		return false;
	})

	$("#btn_noname").click(function () {
		$(".loginBox").height("200");
		$(".pn_add_form").hide();
		$(".pn_noname").show();
		return false;
	})


	$("#user_id").keypress(function (e) {
		if (e.keyCode == 13) {
			$("#user_pwd").focus();
		}
	})

	$("#user_pwd").keypress(function (e) {
		if (e.keyCode == 13) {
			$("#btn_login").trigger("click");
		}
	})


	if ($(".subContents.sponsor .payment .tbl_child .info .label").text() == "특별한 나눔") {
		$(".subContents.sponsor .payment").addClass("temporary");
	}

});

