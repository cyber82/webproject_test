﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pop-pay.aspx.cs" Inherits="sponsor_pop_pay"  %>
<div class="pop_type1 w800" style="width:800px;">
	<script type="text/javascript">

		$(function () {

			setCurrenyFormat();

			$(".pay_amount").click(function () {

				var amount = $(this).data("amount").format();
				$("#amount").val(amount);
				$(".pay_amount_custom").val(amount);
				return false;

			});

			$(".pay_amount_custom").focus(function () {
				$(this).val("");
			}).blur(function () {
				var val = $(this).val().replace(/\,/g , "");
				if (isNaN(val) || parseInt(val) < 1000) {
					$(".guide_comment2").show();
					$("#amount").val("");
					$(this).val("");
					$(this).focus();
					return;
				} else {
					$(".guide_comment2").hide();
				}

				if (parseInt(val) % 1000 > 0) {
					alert("천원단위로 입력해 주세요");
					$("#amount").val("");
					$(this).val("");
					$(this).focus();
					return;
				}

				$("#amount").val(val);
			})

			$("input[name=pop_frequency]").click(function () {
				var val = $(this).val();
				$("#frequency").val(val);
			})
			
			if ($("input[name=pop_frequency]").length > 0) {
			    var idx = $("input[name=pop_frequency]").length > 1 ? 1 : 0;
			    $($("input[name=pop_frequency]")[idx]).prop("checked", true);
			}
			//$("#frequency").val($($("input[name=pop_frequency]")[0]).val());
		})
	</script>


	<div class="pop_title">
		<span>후원하기</span>
		<button class="pop_close" ng-click="modal.close($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
	</div>

	<div class="pop_content dir_sponsor">
			
		<!--// 신청정보 -->
		<p class="fs15 fc_blue mb15">후원정보를 선택해주세요.</p>

		<div class="tableWrap2 mb30">
			<table class="tbl_type1 marriage">
				<caption>신청정보 입력 테이블</caption>
				<colgroup>
					<col style="width:25%" />
					<col style="width:75%" />
				</colgroup>
				<tbody>
					<tr>
						<th scope="row"><span>후원유형</span></th>
						<td>
							<span class="radio_ui">
								<asp:PlaceHolder runat="server" ID="ph_regular" Visible="false" >
								<input type="radio" id="type_1" value="정기" name="pop_frequency" class="css_radio" />
								<label for="type_1" class="css_label mr30">정기후원</label>
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="ph_temporary" Visible="false" >
								<input type="radio" id="type_2" value="일시" name="pop_frequency" class="css_radio" />
								<label for="type_2" class="css_label mr30">일시후원</label>
								</asp:PlaceHolder>
							</span>

						</td>
					</tr>
						
					<tr>
						<th scope="row"><span>후원금액</span></th>
						<td>
							<div class="relative">
								<button class="btn_s_type4 btn_amount pay_amount" data-amount="10000"><span>10,000원</span></button>
								<button class="btn_s_type4 btn_amount pay_amount" data-amount="20000"><span>20,000원</span></button>
								<button class="btn_s_type4 btn_amount pay_amount" data-amount="30000"><span>30,000원</span></button><br />
								<button class="btn_s_type4 btn_amount pay_amount" data-amount="50000"><span>50,000원</span></button>
								<button class="btn_s_type4 btn_amount pay_amount" data-amount="100000"><span>100,000원</span></button><br />

								<label for="pay_amount_custom" class="hidden">후원금액 직접입력</label>
								<input type="text" id="pay_amount_custom" class="input_type2 pay_amount_custom number_only use_digit" maxlength="10" value="" placeholder="직접입력(천원이상 입력 가능)" style="width:400px" />

								<p class="pt10"><span class="guide_comment2" style="display:none;">후원금은 1,000원 이상 입력 가능합니다.</span></p>
							</div>
						</td>
					</tr>
						
				</tbody>
			</table>

		</div>
		<!--// 신청정보 -->

		<div class="tac"><a href="#" ng-click="modal.request($event)" class="btn_type1">등록</a></div>

	</div>
</div>
<!--// popup -->
	