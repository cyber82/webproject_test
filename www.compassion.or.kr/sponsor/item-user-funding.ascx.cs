﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using CommonLib;

public partial class sponsor_item_user_funding : System.Web.UI.UserControl {

	protected override void OnLoad(EventArgs e) {
		
		base.OnLoad(e);
		
		if(!PayItemSession.HasCookie(this.Context)) {
			return;
		}

	
	}

	public override bool Visible {
		get {
			return base.Visible;
		}
		set {
			if(value) {
				var payInfo = PayItemSession.GetCookie(this.Context);
				this.GetData(payInfo);
			}
			base.Visible = value;
		}
	}

	void GetData( PayItemSession.Entity payInfo)
    {
		sp_tUserFunding_get_fResult entity;
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tUserFunding_get_f(payInfo.relation_key).ToList();
            Object[] op1 = new Object[] { "uf_id" };
            Object[] op2 = new Object[] { payInfo.relation_key };
            var list = www6.selectSP("sp_tUserFunding_get_f", op1, op2).DataTableToList<sp_tUserFunding_get_fResult>();

            if (list.Count < 1)
            {
                return;
            }

            entity = list[0];
        }
		
		img.Text = entity.uf_image.WithFileServerHost();
		title.Text = entity.uf_title;
		typeName.Text = payInfo.TypeName;
		frequency.Text = payInfo.frequency;
		amount2.Text = amount.Text = payInfo.amount.ToString("N0");

		if(payInfo.frequency == "정기") {
			amount2_unit.InnerHtml = "원/월";
			amount.Text += " 원 /월";
		} else {
			amount2_unit.InnerHtml = "원";
			amount.Text += " 원 ";
		}

	}
}
