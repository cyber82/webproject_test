﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;

public partial class sso_logout : System.Web.UI.Page {
	
	protected void Page_Load( object sender, EventArgs e ) {
		
		new UserInfo().ClearCookie();

	}

}