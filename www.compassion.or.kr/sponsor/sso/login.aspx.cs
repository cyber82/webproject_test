﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using CommonLib;

public partial class sso_login : System.Web.UI.Page {

	string auth_domain = ConfigurationManager.AppSettings["domain_auth"];
	protected void Page_Load( object sender, EventArgs e ) {
		
		return;
		//http://cps.m.wisekit2.co/sso/login/?token=ABLO1vk+1gptU/O2q5js8b4ATx03W+vkNhQb1HrCJJSPyOPKHr3m7dnXIkPqlT3aVd9EaYV0tl00cNKmBmr2WD6DpS7yI0hCRrpL8Bj4P6Q=
		
		// SSO 사이트에서 온 요청이 아니면 무시
		/*
		if(Request.UrlReferrer == null ||
			!Request.UrlReferrer.AbsoluteUri.StartsWith(auth_domain)) {
			return;
		}
		*/
		var token = Request.QueryString["token"];
		
		
		/*
		var values = token.Decrypt().Split('|');
		var uuid = values[0];
		var ip = values[1];
		var date = DateTime.Parse(values[2]);

		Response.Write(token.Decrypt() + "<br>");
		Response.Write(string.Format("uuid={0},ip={1},date={2}" , uuid , ip , date) + "<br>");
		return;
		*/

		Authentication.Result result = authenticate(token);
		/*
		Response.Write(result.message + " : " + result.success.ToString() + " : " + result.errCode.ToString() + " : ");
		if(result.data != null)
			Response.Write(result.data.ToString());
			*/
		Response.Write(result.ToJson());
		if(result.success) {
			
			HttpCookie cookie = new HttpCookie("token");
			cookie.Value = token;
			cookie.Path = "/";
			//cookie.Domain = ConfigurationManager.AppSettings["cookie_domain"];
			Response.Cookies.Set(cookie);
		}

	}

	Authentication.Result authenticate( string token) {

		using(WebClient wc = new WebClient()) {
			wc.Encoding = System.Text.Encoding.UTF8;
			wc.Headers.Add("cps-token", token);
			wc.Headers.Add("cps-host", Request.Url.Host);
			wc.Headers.Add("Accept", "application/json");

			var url = string.Format("{0}/authenticate/", auth_domain);
			
			string data = wc.DownloadString(url);
			
			return data.ToObject<Authentication.Result>();
		}

	}
}