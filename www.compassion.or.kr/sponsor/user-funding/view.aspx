﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="sponsor_user_funding_view" MasterPageFile="~/main.Master"%>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/common/js/site/moveToMobile.js"></script>
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
	
	<script type="text/javascript" src="/sponsor/user-funding/view.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" id="uf_id" runat="server" />
	<input type="hidden" id="campaignId" runat="server" />
	<input type="hidden" id="amount" runat="server" />
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />
	<input type="hidden" runat="server" id="hd_percent" value="" />
	<input type="hidden" runat="server" id="hd_is_end" value="" />
	
	
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>나눔<em>펀딩</em></h1>
				<span class="desc">후원이 끊긴 어린이, 어린이센터 건축, 깨끗한 식수 지원, 직업교육, 의료지원, 엄마와 아이지원 등 다양한 곳에 후원해보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 sponsor">

			<!-- 비주얼 -->
			<div class="bgContent shareVisual" style="background:url('<%:entity.uf_image.WithFileServerHost()%>') no-repeat center -120px;">
				
				<div class="bg_dim">
					<div class="w980">
						<p class="tit"><asp:Literal runat="server" ID="uf_title" /></p>
						<span class="bar"></span>
						<p class="con"><asp:Literal runat="server" ID="uf_summary" /></p>

					</div>
				</div>

			</div>
			<!--// 비주얼 -->

			<!-- 참여인원 -->
			<div class="part_member">
				<div class="w980 relative">

					<div class="count clear2">
						<span class="txt">지금까지 <em><asp:Literal runat="server" ID="uf_cnt_user" /></em>명이 참여했어요</span>

						<script type="text/javascript">
							$(function () {
								// 바로후원하기, sns공유버튼 스크롤 fixed
								$(window).scroll(function () {
									if ($(this).scrollTop() > 900) {
										$(".btn_dir_sponsor").addClass("fixed");
									} else {
										$(".btn_dir_sponsor").removeClass("fixed");
									}
								});
							})
						</script>

						<div class="btns">
							<span class="btn_dir_sponsor">
								<a href="#" class="btn_b_type2 btn_sponsor" ng-click="showPay($event)">바로 후원하기</a>

								<!-- 공유하기버튼 -->
								<span class="sns_ani down">
									<button class="common_sns_share">공유하기</button>
									<span class="common_sns_group">
										<span class="wrap">
											<a href="#" title="페이스북" data-role="sns" data-provider="fb" data-url="<%:Request.UrlWithoutQueryString() %>" class="sns_facebook">페이스북</a>
											<a href="#" title="카카오스토리" data-role="sns" data-provider="ks" data-url="<%:Request.UrlWithoutQueryString() %>" class="sns_story">카카오스토리</a>
											<a href="#" title="트위터" data-role="sns" data-provider="tw" data-url="<%:Request.UrlWithoutQueryString() %>" class="sns_twitter">트위터</a>
											<a href="#" title="url 공유" data-role="sns" data-provider="copy" data-url="<%:Request.UrlWithoutQueryString() %>" class="sns-copy sns_url">url 공유</a>
										</span>
									</span>
								</span>
								<!--// -->
							</span>
						</div>
					</div>

					<!-- graph -->
					<div class="commonGraph_wrap white" style="display:<%:percent < 0 ? "none" : "block"%>">
						<span class="heart" style="left:<%:percent %>%">
							<span class="txt day"><em><%:ori_percent %>%</em> <%:percent >= 100 ? "달성" : "달성중" %><asp:Literal runat="server" ID="days" /></span>
						</span>
						<div class="graph">
							<span class="bg"></span>
							<span class="per" style="width:<%:percent %>%"></span>
						</div>
						<div class="sum clear2">
							<span class="ing"><%:entity.uf_current_amount.ToString("N0") %>원</span>
							<span class="goal"><%:entity.uf_goal_amount.ToString("N0") %>원</span>
						</div>

					</div>

					<!--// -->

				</div>
			</div>
			<!--// 참여인원 -->

			<!-- 크리에이터 -->
			<div class="visual_creater">

				<p class="name">나눔 크리에이터<em><asp:Literal runat="server" ID="creator" /></em></p>
				<span class="pic" style="background:url('<%:entity.UserPic.WithFileServerHost()%>') no-repeat center top;background-size:cover;"></span>

			</div>
			<!--// 크리에이터 -->
			
			<div class="sf_view">
				<div class="w980">
					
					<div class="main_tit">
						<asp:PlaceHolder runat="server" ID="ph_go_update" Visible="false" >
						<a href="/sponsor/user-funding/update/<%:entity.uf_id%>" class="btn_s_type2">내용 수정하기</a>
						</asp:PlaceHolder>
						<!-- 이야기펀딩일 경우 -->
						<asp:PlaceHolder runat="server" ID="ph_campaign" Visible="false" >
						<div>
							<div class="mb20">
								<span class="label">이야기 펀딩</span><span class="label_txt">제가 함께 하고 싶은 후원은요,</span>
							</div>

							<p class="tit"><em><%:entity.uf_title%></em></p>
							<p class="desc"><%:entity.uf_summary%></p>

						</div>
						</asp:PlaceHolder>

						<!--// -->

						<!-- 어린이후원일 경우 -->
						<asp:PlaceHolder runat="server" ID="ph_child" Visible="false" >
						<div>
							<div class="mb10">
								<span class="label">어린이 후원</span><span class="label_txt">제가 함께 하고 싶은 어린이는요,</span>
							</div>

							<p class="tit child">안녕하세요, 제 이름은 <em><asp:Literal runat="server" ID="c_name" /></em> 예요.</p>
							
							<div class="child">
								<span class="pic" style="background:url('<%:this.ViewState["c_img"].ToString()%>') no-repeat center;background-position-y:0"></span>
								<span class="story">
									나이는 <asp:Literal runat="server" ID="c_age" />살 이고요, <asp:Literal runat="server" ID="c_country" />에 살고 있어요.<br />
									제 생일은 <asp:Literal runat="server" ID="c_birth" /> 이고요, 제가 좋아하는 것은  <asp:Literal runat="server" ID="c_hobby"/>이예요.<br />
									제게도 후원자님이 생겨서 컴패션 친구들과 함께 배우고 싶어요.
								</span>

							</div>
						</div>
						</asp:PlaceHolder>
						<!--// -->
					</div>

				<!-- 탭메뉴 -->
					<ul class="tab_type1" id="tab">
						<li style="width:33%" ng-class="{'on' : tab == '1'}"><a href="#" ng-click="changeTab($event,'1')">나눔 스토리</a></li>
						<li style="width:34%" ng-class="{'on' : tab == '2'}"><a href="#" ng-click="changeTab($event,'2')">업데이트 소식</a></li>
						<li style="width:33%" ng-class="{'on' : tab == '3'}"><a href="#" ng-click="changeTab($event,'3')">참여자</a></li>
					</ul>
					<!--// 탭메뉴 -->

				</div>
			</div>

			<!------- 나눔스토리 -------->
			<div class="sf_view_tab story" ng-show="tab == '1'">

				<div class="w980">
					
					<!-- 영상 -->
					<asp:PlaceHolder runat="server" ID="ph_movie" Visible="false">
					<div class="story_movie">
					
						<asp:Repeater runat="server" ID="repeater_movie">
							<ItemTemplate>
								<iframe src="<%#Container.DataItem.ToString().Replace("https://youtu.be/", "https://www.youtube.com/embed/") + "?rel=0&showinfo=0&wmode=transparent" %>" visible="false" width="980" height="551" title="영상" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
							</ItemTemplate>
						</asp:Repeater>
					</div>
					</asp:PlaceHolder>
					<!--// -->

					<!-- 본문내용 -->
					<div class="story_content" style="word-break:break-all;">
						<asp:Literal runat="server" ID="uf_content" />
					</div>
					<!--// -->

					<!-- 본문이미지 -->
					<asp:PlaceHolder runat="server" ID="ph_content_image" Visible="false">
					<div class="story_image">
						<asp:Repeater runat="server" ID="repeater_content_image">
							<ItemTemplate>
								<img src="<%#Container.DataItem.ToString() %>" style="max-width:980px"/><br />
							</ItemTemplate>
						</asp:Repeater>
					</div>
					</asp:PlaceHolder>
					<!--// -->
                    <asp:Literal runat="server" ID="sf_content_user" Visible="false"/>
				</div>
				

				<!-- 캠페인 컨텐츠 인클루드영역 -->
				<div class="sf_cam_static">
						
					<asp:PlaceHolder runat="server" ID="ph_campaign_content" Visible="false">
                        
                        <div class="special north">
						<asp:Literal runat="server" ID="sf_content_fix" />
                        </div>
                        
					</asp:PlaceHolder>

					<!-- 다운로드 -->
					<div class="report" ng-if="report.total > 0">
						<div class="w980">

						<p class="tit">후원 결과 레포트/뉴스레터</p>
						
						<div class="tableWrap1">
							<table class="tbl_type1 campaign mb40">
								<caption>자료 다운로드 테이블</caption>
								<colgroup>
									<col style="width:83%" />
									<col style="width:17%" />
								</colgroup>
								<tbody>
									<tr ng-repeat="item in report.list">
										<td>{{item.sr_title}} <span class="icon_new" ng-show="item.is_new">new</span></td>
										<td><a ng-href="{{item.sr_file}}" class="btn_s_type4"><span class="ic_down"></span>Download</a></td>
									</tr>
									
								</tbody>
							</table>
							</div>
						</div>
                        
						<button class="btn_com_more" ng-click="report.showMore($event)" ng-show="report.total > report.list.length">더보기</button>

					</div>

					<!--// 다운로드 -->

				</div>
				<!--// 캠페인 컨텐츠 인클루드영역 -->
				
				<div class="w980 pt40">

					<div class="box_type3 shareInfo">
						<p class="fc_blue">나눔펀딩 안내</p>
						<ul>
							<li>1. 나눔펀딩으로 모금된 후원금의 집행은 한국컴패션에서 진행됩니다.</li>
							<li>2. 목표금액을 달성하지 못한 모금액과 목표금액을 초과한 모금액 모두 현재 모금중인 캠페인에 기부됩니다.<br />
							&nbsp;&nbsp;&nbsp;&nbsp;(1:1어린이양육을 위한 모금일 경우 해당 부분은 후원자님을 만나지 못한 어린이를 위한 후원금으로 사용됩니다.)</li>
							<li>3. 1:1어린이양육을 위한 목표금액이 달성되면 일주일 이내에 개설자가 어린이의 후원자로 등록되어 편지쓰기, 선물하기 등이 가능하며 후원 취소는 하실 수 없습니다.</li>
							<li>4. 나눔펀딩으로 1:1 후원하게 된 후원어린이가 어린이의 사정으로 컴패션을 떠나게 되는 경우, 잔여 모금액은 후원자님을 만나지 못한 어린이를 위한 후원금으로 사용됩니다.</li>
						</ul>
					</div>

					<div class="donation">
						<p class="tit">지금 바로 이 나눔펀딩에 기부할래요</p>

						<div class="input_wrap clear2">
							<div class="inblock">
								<span class="inputArea">
									<span class="txt">금액입력</span>
									<label for="direct_amount" class="hidden">후원금액 입력</label>
									<input type="text" id="direct_amount" value="20,000" maxlength="10" class="number_only use_digit" />
									<span class="txt">원</span>
								</span>
								<a href="#" class="btn_b_type2 fl btn_sponsor" ng-click="goPay($event)">후원하기</a>
							</div>
						</div>

						<p class="pt15"><span id="direct_amount_err" class="guide_comment4" style="display:none;">후원금은 1천원 이상 입력 가능합니다.</span></p>
					</div>

					<!-- 응원댓글 -->
					<div class="boardView_1" ng-show="reply.total > -1">

						<p class="tab_tit">응원 댓글 <span>(<em>{{reply.total}}</em>)</span></p>

						<div class="comment">
							<fieldset>
								<legend>댓글 입력창</legend>
								<span class="tit"></span>
								<span id="reply_comment_count" style="position: absolute;top:10px;right: 165px">0</span>
								<span style="position: absolute;top:10px;right:120px">/ 300자</span>
								<div class="clear2">
									<label for="reply_comment" class="hidden">댓글입력</label>
									<textarea id="reply_comment" class="textarea_type1" maxlength="300" ng-model="reply.comment" placeholder=""></textarea>

									<button type="submit" class="registration" ng-click="reply.add($event)">등록</button>
								</div>
							</fieldset>
						</div>

						<ul class="comment_lst mb40">

							<li class="reply_row" ng-repeat="item in reply.list" data-idx="{{item.ur_id}}">
								<div class="clear2">
									<div class="id_box">
										<span class="id">{{item.userid}}</span>
										<span class="txt" ng-bind-html="item.ur_content"></span>
									</div>
									<div class="modify_box">
										<span ng-if="item.is_owner">
											<a href="#" class="modify" ng-click="reply.toggleUpdate(item.ur_id , $event)">수정</a>
											<span class="bar"></span>
											<a href="#" class="delete" ng-click="reply.delete(item.ur_id , $event)">삭제</a>
										</span>
										<span class="day">{{item.ur_regdate | date:'yyyy.MM.dd HH:mm:ss' }}</span>
									</div>
								</div>
								<!-- 수정하기 -->
								<div class="reply_modify_row modifyArea" data-idx="{{item.ur_id}}" ng-if="item.is_owner" style="display:none;" >
									<label for="re_modify_{{item.ur_id}}" class="hidden">댓글 수정하기</label>
									<!--수정하기 댓글 카운트-->
									<div class="tar mb5">
										<span class="count">0</span><span>/ 300자</span>
									</div>
									<!--//-->
									<textarea id="re_modify_{{item.ur_id}}" maxlength="300" class="textarea_type1 mb20 reply_modify" style="width:100%;" rows="4">{{item.ur_content}}</textarea>

									<div class="tac">
										<button class="btn_type2 mr5" onclick="$('.modifyArea').slideUp(300);"><span>수정취소</span></button>
										<button class="btn_type1" ng-click="reply.update(item.ur_id , $event)"><span>수정하기</span></button>
									</div>
								</div>
								<!--// -->
							</li>
							
							<!-- 댓글이 없을때 -->
							<li class="no_content" ng-if="reply.total == 0">등록된 댓글이 없습니다.<br />첫 댓글로 나눔펀딩을 응원해보세요.</li>
							<!--//  -->

						</ul>

						<button class="btn_com_more" ng-show="reply.total > reply.list.length" ng-click="reply.showMore($event)"><span>더 보기</span></button>
					</div>
					<!-- 응원댓글 -->

				</div>

			</div>

			<!-------// 나눔스토리 -------->

			<!------- 업데이트소식 -------->
			<div class="sf_view_tab update" ng-show="tab == '2'">
				<div class="w980" ng-show="notice.total > -1">
					
					<p class="tab_tit mb40">업데이트 소식 <span>(<em>{{notice.total}}</em>)</span></p>

					<div class="boardView_1">
						<asp:PlaceHolder runat="server" id="ph_notice_add" Visible="false" >
						<div class="comment mb10">
							<fieldset>
								<legend>댓글 입력창</legend>
								<span class="tit"></span>
								<span id="notice_comment_count" style="position: absolute;top:10px;right: 165px">0</span>
								<span style="position: absolute;top:10px;right:120px">/ 300자</span>
								<div class="clear2">
									<label for="notice_comment" class="hidden">댓글입력</label>
									<textarea id="notice_comment" class="textarea_type1" ng-model="notice.comment" placeholder="최근 나눔펀딩의 진행소식을 전해주세요."></textarea>

									<button type="submit" class="registration" ng-click="notice.add($event)">등록</button>
								</div>
							</fieldset>
						</div>
						<div class="mb30">
							<div class="btn_attach clear2 relative">
								<label for="lb_file_path" class="hidden">이미지 첨부</label>
								<input type="text" id="lb_file_path" class="input_type1 fl mr10" placeholder="이미지 파일을 선택해주세요." style="width:864px;background:#fdfdfd;border:1px solid #d8d8d8;" disabled />
								<a href="#" class="btn_type8 fl" id="btn_file_path" ><span>이미지 찾기</span></a>
							</div>
						</div>
						</asp:PlaceHolder>

						<table class="tbl_update mb40">
							<caption></caption>
							<colgroup>
								<col style="width:13%" />
								<col style="width:15%" />
								<col style="width:60%" />
								<col style="width:12%" />
							</colgroup>
							<tbody>
								<tr class="notice_row" data-idx="{{item.un_id}}" ng-repeat-start="item in notice.list">
									<td class="tac">
										<span class="pic" background-img="{{item.userpic}}" data-default-image="/common/img/page/my/no_pic.png" style="background:no-repeat center;background-position-y:0;background-size:80px;"></span>
									<td class="timeline"><span class="date">{{item.un_regdate | date:'yyyy.MM.dd'}}</span><br /><span class="time">{{item.un_regdate | date:'HH:mm:ss'}}</span></td>
									<td class="content">
										<div ng-bind-html="item.un_content"></div>
										<div class="update_img" ng-show="item.un_image"><img ng-src="{{item.un_image}}" alt="업데이트 이미지" style="max-width:600px" /></div>
									</td>
									<td class="tar" ng-if="item.is_owner">
										<a href="#" class="modify" ng-click="notice.toggleUpdate(item.un_id , $event)">수정</a>
										<span class="bar"></span>
										<a href="#" class="delete" ng-click="notice.delete(item.un_id , $event)">삭제</a>
									</td>
								</tr>
								<!-- 수정하기 -->
								<tr class="notice_modify_row" ng-if="item.is_owner" data-idx="{{item.un_id}}" ng-repeat-end style="display:none;">
									<td colspan="4" class="modifyArea">
										<p class="tar mb5">(<span id="no_modify_count_{{item.un_id}}">0</span>/300자)</p>
										<label for="no_modify_{{item.un_id}}" class="hidden">댓글 수정하기</label>
										<textarea id="no_modify_{{item.un_id}}" class="textarea_type1 mb10 notice_modify" maxlength="300" style="width:100%;" rows="4">{{item.un_content}}</textarea>

										<div class="mb20">
											<div class="btn_attach clear2 relative">
												<label for="file_attach2" class="hidden">이미지 첨부</label>
												<input type="text" id="" class="input_type1 fl mr10" placeholder="이미지 파일을 선택해주세요." value="{{item.or_image}}" style="width:740px;background:#fdfdfd;border:1px solid #d8d8d8;"  disabled/>
												<a href="#" class="btn_type8 fl" id="btn_modify_file_path_{{item.un_id}}"><span>이미지 찾기</span></a>
												<a href="#" class="btn_type7 fl ml10" id="btn_delete_file_{{item.un_id}}"><span>삭제</span></a>
											</div>
										</div>

										<div class="tac">
											<button class="btn_type2 mr5" ng-click="notice.toggleUpdate(item.un_id , $event)"><span>수정취소</span></button>
											<button class="btn_type1" ng-click="notice.update(item.un_id , $event)"><span>수정하기</span></button>
										</div>
									</td>
								</tr>
								<!--// -->
								
								<!-- 업데이트소식 없을 경우 -->
								<tr ng-if="notice.total == 0">
									<td colspan="4" class="no_content">등록된 업데이트 소식이 없습니다.</td>
								</tr>
								<!--//-->

							</tbody>
						</table>

						<button class="btn_com_more" ng-show="notice.total > notice.list.length" ng-click="notice.showMore($event)"><span>더 보기</span></button>
					</div>


				</div>

			</div>
			<!-------// 업데이트소식 -------->

			<!------- 참여자 -------->
			<div class="sf_view_tab participant" ng-show="tab == '3'">
				<div class="w980" ng-show="user.total > -1">

					<p class="tab_tit mb40">참여자 <span>(<em>{{user.total}}</em>)</span></p>

					<table class="tbl_part mb40">
						<caption></caption>
						<colgroup>
							<col style="width:22%" />
							<col style="width:58%" />
							<col style="width:20%" />
						</colgroup>
						<tbody>
							<tr ng-repeat="item in user.list">
								<th scope="row">{{item.userid}}</th>
								<td class="content"><span>{{item.uu_amount | number:0}}</span>원이 후원되었어요.</td>
								<td class="day">{{item.uu_regdate | date:'yyyy.MM.dd HH:mm:ss' }}</td>
							</tr>
							
							<!-- 참여자가 없을 때 -->
							<tr ng-if="user.total == 0">
								<td colspan="3" class="no_content">아직 이 나눔펀딩에 참여자가 없습니다.<br />참여를 통해 어린이들의 꿈을 응원해주세요.</td>
							</tr>
							<!--//  -->

						</tbody>
					</table>

					<button class="btn_com_more" ng-show="user.total > user.list.length" ng-click="user.showMore($event)"><span>더 보기</span></button>
					
				</div>
			</div>
			<!-------// 참여자 -------->

			<div class="ShareFunding">
				<div class="w980">
					<div class="newFunding">
						<em class="fs_m fc_blue">새로운 나눔펀딩</em>을 <em class="fs_m fc_blue">개설</em>해 보시겠어요? 단계에 따라 손쉽게 생성하실 수 있습니다.
						<a href="/sponsor/user-funding/create/" class="btn_s_type1 ml20">새로운 나눔펀딩 만들기</a>
					</div>
				</div>
			</div>


		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>


</asp:Content>
