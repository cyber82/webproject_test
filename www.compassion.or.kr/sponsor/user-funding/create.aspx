﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="create.aspx.cs" Inherits="sponsor_user_funding_create" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
	<script type="text/javascript" src="/sponsor/pay/regular/default.js"></script>
	<script type="text/javascript" src="/sponsor/user-funding/create.js?v=1.5"></script>
	<script type="text/javascript" src="/common/js/site/motive.js"></script>
	
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>

	<script type="text/javascript">
		
	</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" id="hd_upload_root" runat="server"  />
	<input type="hidden" id="hd_userpic_upload_root" runat="server"  />
	<input type="hidden" id="hd_image_domain" runat="server"  />
	<input type="hidden" id="hd_campaigns" runat="server" value="[]"  />
	<input type="hidden" id="hd_uf_image" runat="server" value=""/>
	<input type="hidden" id="hd_user_pic" runat="server" value=""/>
	<input type="hidden" id="hd_childMasterId" runat="server" value="" />
	<input type="hidden" id="hd_childName" runat="server" value="" />
	<input type="hidden" id="hd_childKey" runat="server" value="" />
	<input type="hidden" id="hd_uf_type" runat="server" value="child" />
	<input type="hidden" id="hd_uf_movie" runat="server" value="" />
	<input type="hidden" id="hd_uf_content_image" runat="server" value="" />

	<input type="hidden" id="user_name" runat="server"  />
	<input type="hidden" id="gender" runat="server"  />
	<input type="hidden" id="hd_auth_domain" runat="server"  />
	<input type="hidden" id="hd_age" runat="server"  />
	<input type="hidden" runat="server" id="jumin" value="" />		
	<input type="hidden" runat="server" id="ci" value="" />		<!-- 본인인증 CI -->
	<input type="hidden" runat="server" id="di" value="" />		<!-- 본인인증 DI -->
	<input type="hidden" runat="server" id="cert_gb" value="" />		<!-- 본인인증 수단 -->
	
	<input type="hidden" runat="server" id="user_class" value="20세이상" />
	<input type="hidden" runat="server" id="parent_cert" value="" />
	<input type="hidden" runat="server" id="parent_name" value="" />
	<input type="hidden" runat="server" id="parent_juminId" value="" />
	<input type="hidden" runat="server" id="parent_mobile" value="" />
	<input type="hidden" runat="server" id="parent_email" value="" />

	<input type="hidden" runat="server" id="exist_account" value="" />
	<input type="hidden" runat="server" id="hdManageType" value="" />
	<input type="hidden" runat="server" id="hdState" value="" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	<input type="hidden" runat="server" id="hdLocation" value="" />
	<input type="hidden" runat="server" id="hdOrganizationID" value="" />
	<input type="hidden" runat="server" id="hdSponsorId" value="" />
	<input type="hidden" runat="server" id="hdBirthDate" value="" />
	<input type="hidden" runat="server" id="zipcode" />
	<input type="hidden" runat="server" id="addr1" /> 
	<input type="hidden" runat="server" id="addr2" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
	<input type="hidden" runat="server" id="hdCI" value="" />	<!-- 기존 등록된 본인인증 CI -->
	<input type="hidden" runat="server" id="dmYN" value="Y" />
	<input type="hidden" runat="server" id="translationYN" value="N" />
    <!-- [이종진] 애칭저장 -->
    <input type="hidden" runat="server" id="hdChildPersonalName" value="N" />

	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>나눔<em>펀딩</em></h1>
				<span class="desc">후원이 끊긴 어린이, 어린이센터 건축, 깨끗한 식수 지원, 직업교육, 의료지원, 엄마와 아이지원 등 다양한 곳에 후원해보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 sponsor">

			<div class="sf_regist">
				
				<!-- 펀딩타입 선택 -->
				<script type="text/javascript">
				    $(function () {
				        // 펀딩타입 선택
				        $(".funding_type button").click(function () {
				            $(".funding_type button").removeClass("on");
				            $(this).addClass("on");
				        });
				    })
				</script>
				<div class="funding_type">

					<div class="w980">
						<p class="tit">어떤 <em>나눔펀딩</em>을 만들고 싶으세요?</p>

						<div class="clear2">
							<button class="child" ng-class="{'on':uf_type == 'child'}" ng-click="changeType($event,'child')"><span>1:1어린이양육을 위한<br />나눔펀딩을 만들고 싶어요</span></button>
							<button class="amount" ng-class="{'on':uf_type == 'normal'}" ng-click="changeType($event,'normal')"><span>금액을 목표로 양육을 돕는<br />나눔펀딩을 만들고 싶어요</span></button>
						</div>

						<div class="tar"><a href="#" ng-click="detail.show($event)" class="btn_s_type1">나눔펀딩 더 자세히 알기</a></div>
					</div>

				</div>
				<!--// 펀딩타입 선택 -->

				<div class="w980 pt20">

					<!-- 펀딩내용등록 -->
					<table class="tbl_type1 mb60">
						<caption>펀딩내용등록 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr ng-show="uf_type == 'child' && children.list">
								<th scope="row"><span class="nec">어린이 선택</span></th>
								<td>
									<!-- 어린이리스트 -->

									<div ng-show="child == null">
										<table class="tbl_child mb20">
											<caption>후원중인 어린이 선택 테이블</caption>
											<colgroup>
												<col style="width:12%" />
												<col style="width:59%" />
												<col style="width:20%" />
												<col style="width:6%" />
											</colgroup>

											<tbody>
												<tr ng-class="{'padding1' : $index == 0}" ng-repeat="item in children.list">
													<td class="tac">
														<span class="pic" style="background:url('{{item.pic}}') no-repeat center;background-size:cover;background-position-y:0">어린이사진</span>
													</td>
													<td class="detail">
														<p class="s_tit5 mb10">{{item.name}}</p>
														<span class="field fd2">국가</span><span class="data">{{item.countryname}}</span><br />
														<span class="field fd2">생일</span><span class="data">{{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><span class="field fd2">성별</span><span class="data">{{item.gender}}</span>
													</td>
													<td><a href="#" class="fs15 link1" ng-click="showChildPop($event , item)">이 어린이 더 만나보기</a></td>
													<td>
														<span class="radio_ui yellow2">
															<input type="radio" id="sel_child_{{$index}}" name="child" class="css_radio" ng-click="children.select($event , item)"/>
															<label for="sel_child_{{$index}}" class="css_label">어린이 선택</label>
														</span>
													</td>
												</tr>
											
											</tbody>
										</table>

										<!-- page navigation -->
										<div class="tac mb30">
											<paging class="small" page="children.params.page" page-size="children.params.rowsPerPage" total="children.total" show-prev-next="true" show-first-last="true" paging-action="children.getList({page : page})"></paging> 
										</div>
										<!--// page navigation -->

										<!-- 선택버튼 -->
										<div class="txt_guide1" ng-show="children.item">
											선택하신 어린이가 성인이 될 때까지 필요한 예상 금액은 <em>{{children.item.remainamount | number:N0}}</em>원 입니다.<br />
											이 금액으로 나눔펀딩을 진행합니다.
										</div>
										<div class="tac mb20" ng-show="children.item">
											<button ng-click="children.confirm($event)"><span class="btn_type1">선택</span></button>
										</div>
										<!-- 선택버튼 -->

									</div>
									<!--// 어린이리스트 -->

									<!-- 선택된 어린이 -->
									<div ng-show="child != null" id="selected_child">
										<table class="tbl_child mb20">
											<caption>후원중인 어린이 선택 테이블</caption>
											<colgroup>
												<col style="width:12%" />
												<col style="width:59%" />
												<col style="width:20%" />
												<col style="width:6%" />
											</colgroup>

											<tbody>
												<tr class="padding1">
													<td class="tac">
														<span class="pic" style="background:url('{{child.pic}}') no-repeat center;background-size:cover;background-position-y:0">어린이사진</span>
													</td>
													<td class="detail">
														<p class="s_tit5 mb10">{{child.name}}</p>
														<span class="field fd2">국가</span><span class="data">{{child.countryname}}</span><br />
														<span class="field fd2">생일</span><span class="data">{{child.birthdate | date:'yyyy.MM.dd'}} ({{child.age}}세)</span><span class="field fd2">성별</span><span class="data">{{child.gender}}</span>
													</td>
													<td></td>
													<td><button ng-click="children.cancel($event);"><span><img src="/common/img/btn/del_3.png" alt="삭제" /></span></button></td>
												</tr>
											</tbody>
										</table>

										<div class="txt_guide1 tal">
											선택하신 어린이가 성인이 될 때까지 필요한 예상 금액은 <em>{{child.remainamount | number:N0}}</em>원 입니다.<br />
											이 금액으로 나눔펀딩을 진행합니다.
										</div>

									</div>
									<!--// 선택된 어린이 -->

								</td>
							</tr>
							<tr ng-show="uf_type == 'normal'">
								<th scope="row"><span class="nec"><label for="CampaignID">후원금 사용처</label></span></th>
								<td>
									<span class="sel_type1 fl mr10" style="width:400px">
										<asp:DropDownList runat="server" ID="CampaignID" class="custom_sel" style="z-index: 3147483583"></asp:DropDownList>

									</span>
								</td>
							</tr>
							<tr>
								<th scope="row"><span class="nec">나눔펀딩<br />모금기간</span></th>
								<td>
									<span class="radio_ui mb10">
									

										<input type="radio" id="period_1" class="css_radio date_range" name="chk_radio" data-day="0" data-month="1" checked="checked">
										<label for="period_1" class="css_label mr30">1개월</label>

										<input type="radio" id="period_2" class="css_radio date_range" name="chk_radio"  data-day="0" data-month="2"/>
										<label for="period_2" class="css_label mr30">2개월</label>

										<input type="radio" id="period_3"class="css_radio date_range" name="chk_radio"  data-day="0" data-month="3"/>
										<label for="period_3" class="css_label mr30">3개월</label>

									</span>
									<br />

									<span class="search_period">
										<label for="uf_date_start" class="hidden">시작일</label>
										<input type="text"  id="uf_date_start" runat="server" class="input_type2 calendar_date date begin" placeholder="시작일" style="width:173px" />
										<button class="calendar btn_calendar"  onclick="$('#uf_date_start').trigger('focus');return false;"></button>
										~&nbsp;&nbsp;
										<label for="uf_date_end" class="hidden">종료일</label>
										<input type="text"  id="uf_date_end" runat="server" class="input_type2 calendar_date date end" placeholder="종료일" style="width:173px" />
										<button class="calendar btn_calendar"  onclick="$('#uf_date_end').trigger('focus');return false;"></button>
									</span>
									<br />

									<p class="pt10"><span class="guide_comment2" id="date_comment" style="display:none;">모금 기간은 최소 1개월 이상 최대 3개월 까지만 설정 가능합니다.</span></p>
								</td>
							</tr>
							<tr ng-show="uf_type == 'normal'">
								<th scope="row"><span class="nec"><label for="goal_amount">목표금액</label></span></th>
								<td class="target_money">
									<div class="relative">
										<input type="text" runat="server" id="uf_goal_amount" maxlength="4" class="number_only input_type2" value="" placeholder="목표금액" style="width:400px" />
										<p class="pt10"><span class="guide_comment2" id="amount_comment" style="display:none;">모금 금액은 1만원 이상 입력 가능합니다.</span></p>

										<div class="amount_sum">선택된 캠페인의 모금액<span><em id="goal_amount_msg">0</em>/<span id="total_amount_msg">0</span></span>만원</div>
									</div>
									
								</td>
							</tr>
							<tr>
								<th scope="row"><span class="nec">나눔펀딩 이미지</span></th>
								<td>
									<div class="visual_img" style="background:no-repeat center;" id="btn_uf_image">

										<!-- 이미지 등록 시, 아래 <span> 태그는 display:none 시키고 상위 <div>에 백그라운드 이미지 삽입. -->

										<span class="img_guide">
											<span class="guide1">대표 이미지를 등록해 주세요.</span><br />
											<span class="guide2">최대 1MB 첨부가능 / JPEG,PNG,GIF<br />1280*960이상 / 4:3비율권장</span>
										</span>

										<!-- (추가) 이미지 등록 시, 아래 <button class="img_del"> 노출 (디폴트는 display:none) -->
										<button class="img_del" id="btn_uf_image_del" style="z-index: 3147483583"><span>삭제</span></button>

									</div>
								</td>
							</tr>
							<tr>
								<th scope="row"><span>추가영상<br />(선택사항)</span></th>
								<td>

									<!-- + 버튼 클릭 시 등록폼 추가, 버튼 이미지는 regist_img 클래스에 'del' 추가 -->
									<div class="clear2 mb5" ng-repeat="item in form.movie.list">
										<label for="youtube_url_{{$index}}" class="hidden">유투브영상 url 입력</label>
										<input type="text" id="youtube_url_{{$index}}" name="uf_movie" class="input_type2 mb10" value="{{item.val}}" placeholder="유투브 등에 올라가있는 대표 영상 URL을 올려주세요" style="width:740px" />
										<button ng-hide="$index == 0 && form.movie.list.length > 2" class="regist_img" ng-class="{'del' : $index > 0}" ng-click="form.movie.control($event , $index)"><span>추가</span></button>
									</div>
									<p><span class="s_con1">유투브 영상 업로드 안내 : <br />공유하실 유투브 영상 하단의 공유하기 버튼을 클릭하신 뒤 https://youtu.be/compassion과 같은 주소를 복사하여 붙여넣어주세요.</span></p>
								</td>
							</tr>
							<tr>
								<th scope="row"><span class="nec">나눔펀딩<br />주요정보</span></th>
								<td>
									<label for="uf_title" class="hidden">나눔펀딩 제목 입력</label>
									<input type="text" id="uf_title" runat="server" maxlength="20" class="input_type2 mb10 mr10" value="" placeholder="나눔펀딩 제목 20자" style="width:400px" /><span><span id="uf_title_count">0</span>/20</span>

									<label for="uf_summary" class="hidden">나눔펀딩 목적, 내용 입력</label>
									<input type="text"  id="uf_summary" runat="server" maxlength="40"  class="input_type2 mr10" value="" placeholder="나눔펀딩의 목적, 다루고자하는 내용 한줄설명 40자" style="width:400px" /><span><span id="uf_summary_count">0</span>/40</span>
								</td>
							</tr>
							<tr>
								<th scope="row"><span class="nec">나눔펀딩<br />상세 스토리</span></th>
								<td>
									<label for="uf_content" class="hidden">나눔펀딩 내용 입력</label>
									<textarea class="textarea_type2 mb10" rows="10" id="uf_content" runat="server" maxlength="2000" placeholder="나눔펀딩의 상세 내용을 10자이상 입력해주세요."></textarea>
									<p class="tar"><span id="uf_content_count"></span>/2000</p>


								</td>
							</tr>
							<tr>
								<th scope="row"><span>본문 이미지<br />(선택사항)</span></th>
								<td>
									<!-- 이미지 등록 시, <span> 태그는 display:none 시키고 상위 <div>에 백그라운드 이미지 삽입. -->
									<!-- + 버튼 클릭 시 등록폼 추가, 버튼 이미지는 regist_img 클래스에 'del' 추가 -->
									<div class="clear2 mb10" ng-repeat="item in form.image.list">
										<div class="visual_img con uf_content_image" style="background:no-repeat center;" data-url="{{item.val}}" id="uf_content_image_{{$index}}">
											
											<span class="img_guide">
												<span class="guide1">본문 내에 들어갈 이미지를 등록해 주세요.</span><br />
												<span class="guide2">최대 1MB 첨부가능 / JPEG,JPG,PNG,GIF</span>
											</span>

											<!-- (추가) 첫번째 본문이미지만 아래 <button class="conimg_del"> 노출 (디폴트는 display:none) -->
											<button class="conimg_del" ng-if="$first" id="uf_content_image_del" style="z-index: 3147483583"><span>삭제</span></button>
										</div>
										<button ng-hide="$index == 0 && form.image.list.length > 5" class="regist_img" ng-class="{'del' : $index > 0}" ng-click="form.image.control($event , $index)"><span>추가</span></button>
									</div>

								</td>
							</tr>
						</tbody>
					</table>
					<!--// 펀딩내용등록 -->

					<!-- 후원자정보등록 -->
					<div class="table_tit">
						<p class="tit">후원자 정보</p>
					</div>

					<script type="text/javascript">
					    $(function () {
					        // table 내 탭메뉴 on, off
					        $(".tab_select button").click(function () {
					            $(this).parent().find("button").removeClass("on");
					            $(this).addClass("on");
					        });
					    })
					</script>

					<div class="table_tit">
						<span class="checkbox_ui mt10">
							<input type="checkbox" class="css_checkbox" id="addr_overseas" runat="server"/>
							<label for="addr_overseas" class="css_label font2">해외에 거주하고 있어요</label>
						</span>
						<span class="nec_info">표시는 필수입력 사항입니다.</span>
					</div>

					<div class="tableWrap2 payment mb40">
						<table class="tbl_type1">
							<caption>후원자정보등록 테이블</caption>
							<colgroup>
								<col style="width:20%" />
								<col style="width:80%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><span class="nec">개설자 정보</span></th>
									<td>
										<div class="btn_attach clear2 relative">
											<label for="picture"></label>
											<input type="text" id="image_UserPic" runat="server" class="input_type1 fl mr10" disabled placeholder="개설자 사진을 등록해 주세요" style="width:400px;background:#fdfdfd;border:1px solid #d8d8d8;" />
											<a href="#" class="btn_type8 fl" id="btn_UserPic"><span>파일선택</span></a>
											<a href="#" class="btn_type7 fl ml10" id="btn_remove_userPic" style="display:none">삭제</a>
										</div>
										<p class="pt5"><span class="s_con1">최대 1MB 첨부가능 / JPEG,JPG,PNG,GIF 정비율권장</span></p>

									</td>
								</tr>
								<tr >
									<th scope="row"><span class="nec">본인인증</span></th>
									<td>
										<asp:PlaceHolder runat="server" ID="ph_no_jumin" Visible="false">
										<p class="mb10">
											<span class="checkbox_ui">
												<input type="checkbox" class="css_checkbox"  id="p_receipt_pub_ok" name="p_receipt_pub" runat="server"  checked />
												<label for="p_receipt_pub_ok" class="css_label font2">국세청 연말정산 영수증 신청</label>
											</span>
										</p>

										<div id="func_name_check">
											<label class="hidden">주민등록번호입력</label>
											<input type="text" class="input_type2 mb10 number_only" value="" data-id="jumin1" runat="server" id="jumin1" maxlength="13" placeholder="주민등록번호(-없이 입력)" style="width:400px" />
											<button class="btn_s_type7 ml5 mb10" id="btn_name_check"><span>실명인증</span></button>
										</div>

										<p id="msg_name_check" style="display:none"><span class="guide_comment1"></span></p>
										</asp:PlaceHolder>


										<asp:PlaceHolder runat="server" ID="ph_yes_jumin" Visible="false">
										<p><span class="guide_comment1">인증이 완료되었습니다.</span></p>
										</asp:PlaceHolder>
										<div class="hide_overseas">
										<asp:PlaceHolder runat="server" ID="ph_cert" Visible="false" >
										<p class="fs14 mb20 func_cert">온/오프라인 기존 후원정보 확인을 위해 본인 인증을 진행해 주세요.</p>

										<!-- 휴대폰,아이핀인증 -->
										<div class="cert clear2 mb20 func_cert">
											<div class="fl">
												<div class="box phone">
													<p class="tit">휴대폰 인증</p>
													<p class="con">
														본인 명의의 휴대전화번호로 본인인증을<br />
														원하시는 분은 아래 버튼을 클릭해주세요
													</p>

													<a href="#" id="btn_cert_by_phone" class="btn_s_type2">휴대폰 인증하기</a>
												</div>
											</div>

											<div class="fr">
												<div class="box ipin">
													<p class="tit">아이핀(i-PIN) 인증</p>
													<p class="con">
														가상 주민등록번호 아이핀으로 본인인증을<br />
														원하시는 분은 아래 버튼을 클릭해주세요
													</p>

													<a href="#" id="btn_cert_by_ipin" class="btn_s_type2">아이핀 인증하기</a>
												</div>
											</div>
										</div>

										<p id="msg_cert" style="display:none"><span class="guide_comment1">정보 확인이 완료되었습니다.</span></p>
										
										<!--// 휴대폰,아이핀인증 -->
										

										<ul class="func_cert">
											<li><span class="s_con1">본인명의의 휴대폰으로만 본인인증이 가능합니다.</span></li>
											<li><span class="s_con1">아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다.</span></li>
										</ul>
										</asp:PlaceHolder>
										</div>
									</td>
								</tr>

                                <asp:PlaceHolder runat="server" ID="ph_parent_cert" Visible="false">
                                    <tr class="hide_overseas">
                                        <th scope="row"><span class="nec">보호자 동의</span></th>
                                        <td>
                                            <p class="fs14 mb20">만 19세 미만은 법률에 의거하여 결제 시 보호자(법적대리인)의 동의가 필요합니다.</p>
                                            <div class="clear2 mb10 hide_parent_cert">
                                                <button style="width: 195px" class="btn_s_type2 fl mr10" id="btn_parent_cert_by_phone"><span>휴대폰 인증</span></button>
                                                <button style="width: 195px" class="btn_s_type2 fl" id="btn_parent_cert_by_ipin"><span>아이핀 인증</span></button>
                                            </div>
                                            <p id="msg_parent_cert" style="display: none"><span class="guide_comment1">보호자 동의 인증이 완료되었습니다.</span></p>
                                        </td>
                                    </tr>
                                </asp:PlaceHolder>

								<tr>
									<th scope="row"><span class="nec">영문이름</span></th>
									<td>
										<span class="fl mr10">
											<span class="relative">
												<label for="last_name" class="hidden">영문성</label>
												<input type="text" maxlength="20" runat="server" id="last_name" class="input_type2" value="" style="width:150px" placeholder="영문 성" />
											</span>
										</span>
										<span class="fl">
											<span class="relative">
												<label for="first_name" class="hidden">영문이름</label>
												<input type="text" maxlength="20" runat="server"  id="first_name" class="input_type2" value="" style="width:240px" placeholder="영문 이름" />
											</span>
										</span>
									</td>
								</tr>
								<tr>
									<th scope="row"><span class="nec">종교</span></th>
									<td>
										<span class="radio_ui mb20">
											<input type="radio" runat="server" id="religion1" name="religion" class="css_radio religion" value="기독교"  checked />
											<label for="religion1" class="css_label">기독교</label>

											<input type="radio" runat="server" id="religion2" name="religion" value="천주교"  class="css_radio religion" />
											<label for="religion2" class="css_label ml40">천주교</label>

											<input type="radio" runat="server" id="religion3" name="religion" value="불교"  class="css_radio religion"  />
											<label for="religion3" class="css_label ml40">불교</label>

											<input type="radio" runat="server" id="religion4" name="religion" value="무교"  class="css_radio religion" />
											<label for="religion4" class="css_label ml40">없음</label>
										</span>
										<br />
										<div id="pn_church" runat="server">
										<label for="church_name" class="hidden">교회명 입력</label>
										<input type="text" runat="server" id="church_name" class="input_type2" maxlength="30" value="" placeholder="교회명 직접입력" style="width:400px" />
										<button class="btn_s_type2 ml5" id="btn_find_church"><span>교회찾기</span></button>
										</div>
									</td>
								</tr>
								<tr id="pn_addr_domestic" runat="server" style="display:none">
									<th scope="row"><span class="nec">주소</span></th>
									<td>
										<input type="hidden" id="addr_domestic_zipcode" />
										<input type="hidden" id="addr_domestic_addr1" />
										<input type="hidden" id="addr_domestic_addr2" />

										<label for="addr_domestic_1" class="hidden">주소</label>
										<input type="text" id="addr_domestic_1" disabled class="input_type2" value="" placeholder="주소" style="width:400px;background:#fdfdfd;border:1px solid #d8d8d8;" />
										<button class="btn_s_type1 ml5" id="btn_find_addr"><span>주소찾기</span></button>
										
										<p id="addr_road" class="fs14 mt15"></p>
										<p id="addr_jibun" class="fs14 mt10"></p>

									</td>
								</tr>
								<tr id="pn_addr_overseas"  runat="server" style="display:none">
									<th scope="row"><span class="nec">주소(해외)</span></th>
									<td>
										<div class="clear2 mb10">
											<span class="sel_type2 fl mr10" style="width:195px;">
												<label for="ddlHouseCountry" class="hidden">국가</label>
												<asp:DropDownList runat="server" ID="ddlHouseCountry" cssclass="custom_sel"></asp:DropDownList>
											</span>
											<label for="addr_overseas_zipcode" class="hidden">우편번호</label>
											<input type="text" id="addr_overseas_zipcode" maxlength="10" class="input_type2 fl" value="" placeholder="우편번호" style="width:195px" />
										</div>
										<label for="addr_overseas_1" class="hidden">주소</label>
										<input type="text" id="addr_overseas_addr1" maxlength="100" class="input_type2 mb10" value="" placeholder="주소" style="width:400px" />
										
										<label for="addr_overseas_2" class="hidden">주소</label>
										<input type="text" id="addr_overseas_addr2" maxlength="100" class="input_type2 mb10" value="" placeholder="주소" style="width:400px" />
									</td>
								</tr>
								<tr class="hide_overseas">
									<th scope="row"><span class="nec">후원정보 수신</span></th>
									<td>
										<script type="text/javascript">
										    $(function () {
										        $(".tab_select button").click(function () {
										            $(this).parent().find("button").removeClass("on");
										            $(this).addClass("on");
										        });
										    })
										</script>

										<p class="mb20">
											<span class="fs14 s_con1">입력하신 주소로 후원과 관련한 우편물을 수신하시겠습니까?</span>
											<span class="fs14 s_con1">단, 후원과 관련하여 후원자님께 중요하게 안내되어야 하는 우편물은 수신 여부와 상관없이 발송됩니다.</span>
										</p>
										<div class="tab_type3 tab_select" style="width:400px">
											<button style="width:200px" class="dmYN" data-value="Y"><span>예</span></button>
											<button style="width:200px" class="dmYN" data-value="N"><span>아니오</span></button>
										</div>
									</td>
								</tr>
								<tr ng-show="uf_type == 'child'">
									<th scope="row"><span class="nec">어린이 편지<br />번역 여부</span></th>
									<td>
										<div class="tab_type3 tab_select mb20" style="width:400px">
											<button style="width:200px" class="translationYN" data-value="N" class="on"><span>영문</span></button>
											<button style="width:200px" class="translationYN" data-value="Y" ><span>한글</span></button>
										</div>
										<p>
											<ul>
												<li><span class="s_con1">영문으로 선택하시면 어린이편지를 더 빨리 받으실 수 있습니다.</span></li>
												<li><span class="s_con1">어린이에게 편지를 보내실 때는 위 선택과 관계없이 한글 또는 영문으로 작성하실 수 있습니다.</span></li>
											</ul>
										</p>
									</td>
								</tr>
								<tr style="display:">
									<th scope="row"><span class="nec">후원 계기</span></th>
									<td>
										<div class="clear2">
											<input type="hidden" runat="server" id="motiveCode" value="" />
											<input type="hidden" runat="server" id="motiveName" value=""/>

											<span class="sel_type2 fl mr10" style="width:165px;">
												<label for="motive1" class="hidden">미디어 선택</label>
												<select class="custom_sel" id="motive1">
												</select>
											</span>
											<span class="sel_type2 fl mr10" style="width:325px;">
												<label for="motive2" class="hidden">알게된 경로</label>
												<select class="custom_sel" id="motive2">
													<option value="">선택하세요</option>
												</select>
											</span>
										</div>
										<div class="clear2" style="margin-top:5px;display:none" id="pn_motive2_etc">
												<input type="text"  class="input_type1" id="motive2_etc" style="width:500px" maxlength="30" placeholder="기타 후원계기를 입력해주세요"/>

											</div>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<p class="guide_tit">나눔펀딩 개설 안내 동의</p>
										<ul class="guide_agree">
											<li>1. 등록된 나눔펀딩은 수정/삭제가 되지 않습니다. 등록 버튼을 누르기 전에 꼭 확인해주세요.</li>
											<li>2. 나눔펀딩으로 모금된 후원금의 집행은 한국컴패션에서 진행됩니다.</li>
											<li>3. 목표금액을 달성하지 못한 모금액과 목표금액을 초과한 모금액 모두 현재 모금중인 캠페인에 기부됩니다.<br />
												&nbsp;&nbsp;&nbsp;&nbsp;(1:1어린이양육을 위한 모금일 경우 해당 부분은 후원자님을 만나지 못한 어린이를 위한 후원금으로 사용됩니다.)</li>
											<li>4. 1:1어린이양육을 위한 목표금액이 달성되면 일주일 이내에 개설자가 어린이의 후원자로 등록되어 편지쓰기, 선물하기 등이 가능하며 후원 취소는 하실 수 없습니다.</li>
											<li>5. 나눔펀딩으로 1:1 후원하게 된 후원어린이가 어린이의 사정으로 컴패션을 떠나게 되는 경우, 잔여 모금액은 후원자님을 만나지 못한 어린이를 위한 후원금으로 사용됩니다.</li>
											<li>6. 모금중이신 나눔펀딩이 있으실 경우 추가 혹은 중복으로 개설하실 수 없습니다.</li>
										</ul>
									</td>
								</tr>
								<tr>
									<td colspan="2" class="tac">
										<span class="checkbox_ui">
											<input type="checkbox" class="css_checkbox" id="agree" />
											<label for="agree" class="css_label font2">위 내용에 동의합니다.</label>
										</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// 후원자정보등록 -->

					<div class="tac">
						<asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" CssClass="btn_type1">나눔펀딩 등록</asp:LinkButton>
					</div>

				</div>
			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>



</asp:Content>
