﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class sponsor_user_funding_complete : FrontBasePage {

	public int uf_id;
	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var requests = Request.GetFriendlyUrlSegments();
		if(requests.Count < 1) {
			Response.Redirect("/", true);
		}

	//	Response.Write(new UserInfo().ToJson());
		uf_id = Convert.ToInt32(requests[0]);

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.tUserFunding.FirstOrDefault(p => p.uf_id == uf_id && p.UserID == new UserInfo().UserId && p.uf_display == true);
            var entity = www6.selectQF<tUserFunding>("uf_id", uf_id, "UserID", new UserInfo().UserId, "uf_display", 1);
            if (entity == null)
            {
                Response.Redirect("/", true);
            }

        }

	}
	
}