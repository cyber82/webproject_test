﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="sponsor_user_funding_update" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript" src="/sponsor/user-funding/update.js"></script>
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
	
	
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" id="hd_image_domain" runat="server"  />
	<input type="hidden" id="hd_upload_root" runat="server"  />
	<input type="hidden" id="hd_uf_image" runat="server" value=""/>
	<input type="hidden" id="hd_uf_content_image" runat="server" value=""/>
	<input type="hidden" id="hd_uf_movie" runat="server" value=""/>
	
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>나눔<em>펀딩 수정</em></h1>
				<span class="desc">후원이 끊긴 어린이, 어린이센터 건축, 깨끗한 식수 지원, 직업교육, 의료지원, 엄마와 아이지원 등 다양한 곳에 후원해보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents sponsor">

			<div class="sf_regist">
				
				
				<div class="w980">

					<!-- 펀딩내용수정 -->
					<table class="tbl_type1 mb40">
						<caption>펀딩내용수정 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><span class="nec">나눔펀딩 이미지</span></th>
								<td>
									<div class="visual_img" style="background:no-repeat center;" id="btn_uf_image">

										<!-- 이미지 등록 시, 아래 <span> 태그는 display:none 시키고 상위 <div>에 백그라운드 이미지 삽입. -->

										<span class="img_guide">
											<span class="guide1">대표 이미지를 등록해 주세요.</span><br />
											<span class="guide2">최대 1MB 첨부가능 / JPEG,PNG,GIF<br />1280*960이상 / 4:3비율권장</span>
										</span>
										
										<!-- (추가) 이미지 등록 시, 아래 <button class="img_del"> 노출 (디폴트는 display:none) -->
										<button class="img_del" id="btn_uf_image_del" style="z-index: 2147483467"><span>삭제</span></button>
									</div>
								</td>
							</tr>
							<tr>
								<th scope="row"><span>추가영상<br />(선택사항)</span></th>
								<td>

									<!-- + 버튼 클릭 시 등록폼 추가, 버튼 이미지는 regist_img 클래스에 'del' 추가 -->
									<div class="clear2 mb5" ng-repeat="item in form.movie.list">
										<label for="youtube_url_{{$index}}" class="hidden">유투브영상 url 입력</label>
										<input type="text" id="youtube_url_{{$index}}" name="uf_movie" class="input_type2 mb10" value="{{item.val}}" placeholder="유투브 등에 올라가있는 대표 영상 URL을 올려주세요" style="width:740px" />
										<button ng-hide="$index == 0 && form.movie.list.length > 2" class="regist_img" ng-class="{'del' : $index > 0}" ng-click="form.movie.control($event , $index)"><span>추가</span></button>
									</div>
									<p><span class="s_con1">유투브 영상 업로드 안내 : <br />공유하실 유투브 영상 하단의 공유하기 버튼을 클릭하신 뒤 https://youtu.be/compassion과 같은 주소를 복사하여 붙여넣어주세요.</span></p>
								</td>
							</tr>
							<tr>
								<th scope="row"><span class="nec">나눔펀딩<br />주요정보</span></th>
								<td>
									<label for="uf_title" class="hidden">나눔펀딩 제목 입력</label>
									<input type="text" id="uf_title" runat="server" maxlength="20" class="input_type2 mb10 mr10" value="" placeholder="나눔펀딩 제목 20자" style="width:400px" /><span><span id="uf_title_count">0</span>/20</span>

									<label for="uf_summary" class="hidden">나눔펀딩 목적, 내용 입력</label>
									<input type="text"  id="uf_summary" runat="server" maxlength="40"  class="input_type2 mr10" value="" placeholder="나눔펀딩의 목적, 다루고자하는 내용 한줄설명 40자" style="width:400px" /><span><span id="uf_summary_count">0</span>/40</span>
								</td>
							</tr>
							<tr>
								<th scope="row"><span class="nec">나눔펀딩<br />상세 스토리</span></th>
								<td>
									<label for="uf_content" class="hidden">나눔펀딩 내용 입력</label>
									<textarea class="textarea_type2 mb10" rows="10" id="uf_content" runat="server" maxlength="2000"></textarea>
									<p class="tar"><span id="uf_content_count"></span>/2000</p>


								</td>
							</tr>
							<tr>
								<th scope="row"><span>본문 이미지<br />(선택사항)</span></th>
								<td>
									<!-- 이미지 등록 시, <span> 태그는 display:none 시키고 상위 <div>에 백그라운드 이미지 삽입. -->
									<!-- + 버튼 클릭 시 등록폼 추가, 버튼 이미지는 regist_img 클래스에 'del' 추가 -->
									<div class="clear2 mb10" ng-repeat="item in form.image.list">
										<div class="visual_img con uf_content_image" style="background:url('{{item.val}}') no-repeat center;" data-url="{{item.val}}" id="uf_content_image_{{$index}}">
											
											<span class="img_guide" ng-hide="{{item.val != ''}}">
												<span class="guide1">본문 내에 들어갈 이미지를 등록해 주세요.</span><br />
												<span class="guide2">최대 1MB 첨부가능 / JPEG,PNG,GIF</span>
											</span>

											<!-- (추가) 첫번째 본문이미지만 아래 <button class="conimg_del"> 노출 (디폴트는 display:none) -->
											<button class="conimg_del"><span>삭제</span></button>
										</div>
										<button ng-hide="$index == 0 && form.image.list.length > 5" class="regist_img" ng-class="{'del' : $index > 0}" ng-click="form.image.control($event , $index)"><span>추가</span></button>
									</div>

								</td>
							</tr>
						</tbody>
					</table>
					<!--// 펀딩내용등록 -->

					<div class="tac">
						<a href="#" id="btn_cancel" runat="server" class="btn_type2 mr10">취소</a>
						<asp:LinkButton cssclass="btn_type1" runat="server" ID="btn_submit" OnClick="btn_submit_Click">수정</asp:LinkButton>
					</div>

				</div>
			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>


</asp:Content>
