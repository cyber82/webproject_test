﻿$(function () {

	$page.init();

});

var $page = {

	init: function () {
	
		$("#reply_comment").textCount($("#reply_comment_count"), { limit: 300 });
		if ($("#notice_comment").length > 0) $("#notice_comment").textCount($("#notice_comment_count"), { limit: 300 });


		if ($("#btn_file_path").length > 0) {
			var uploader = attachUploader("btn_file_path");
			uploader._settings.data.fileDir = $("#upload_root").val();
			uploader._settings.data.fileType = "image";
			uploader._settings.data.limit = 5120;
		}


		//if ($("#hd_percent").val() >= 100 || $("#hd_is_end").val() == "True") {
		if ($("#hd_is_end").val() == "True") {
			$(".btn_sponsor").hide();
			return false;
		}
	}

}

var goBack = function () {
	if (location.pathname.indexOf("/sponsor/user-funding/view") > -1)
		location.href = "/";
	else
		history.back();
}

var app = angular.module('cps.page', []);
app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

	$scope.tab = paramService.getParameter("tab"); 
	if (!$scope.tab) $scope.tab = "1";


	$scope.changeTab = function ($event, tab) {
		$event.preventDefault();
		$scope.tab = tab;

		scrollTo($("#tab"), 30);

	}

	// 댓글
	$scope.reply = {
		total : -1 , 
		list : [] , 
		params : {
			page : 1 , 
			rowsPerPage : 5 , 
			uf_id: $("#uf_id").val()
		},
		comment : "" ,
	
		getList : function (params , reload) {
			$scope.reply.params = $.extend($scope.reply.params, params);

			$http.get("/api/user-funding.ashx?t=reply-list", { params: $scope.reply.params }).success(function (r) {
				//console.log(r);
				if (r.success) {
					var list = r.data;

					$.each(list, function () {
						//this.ur_regdate = moment(this.ur_regdate).fromNow();
						this.ur_regdate = new Date(this.ur_regdate);
					});

					// 더보기인경우 merge
					if (reload) {
						$scope.reply.list = list;
					} else {
						$scope.reply.list = $.merge($scope.reply.list, list);
					}
					$scope.reply.total = list.length > 0 ? list[0].total : 0;
					

				} else {
					alert(r.message);
				}
			});

		} , 
		
		add: function ($event) {

			$event.preventDefault();
			if (!common.checkLogin()) {
				return;
			}

			if ($("#reply_comment").val().length < 10) {
				alert("10자 이상 입력해야 등록가능합니다.")
				return;
			}
			
			$http.post("/api/user-funding.ashx", { t: 'add-reply', uf_id: $("#uf_id").val(), comment: $scope.reply.comment }).success(function (result) {
				if (result.success) {
					console.log(result);
					$scope.reply.comment = "";
					$("#reply_comment_count").text("0");
					$scope.reply.getList({page : 1}, true);
				} else {
					alert(result.message);
				}
			})
			
		},

		showMore: function ($event) {
			$event.preventDefault();

			$scope.report.params.page++;
			$scope.report.getList();
		},

		toggleUpdate: function (idx, $event) {

			target = $(".reply_modify_row[data-idx='" + idx + "']");

			if (target.css("display") == "none") {
				$(".reply_modify_row").slideUp(300);

				if (target.find(".reply_modify").length > 0) target.find(".reply_modify").textCount(target.find(".count"), { limit: 300 });

				target.slideDown(300);
			} else {
				target.slideUp(300);
			}

			$event.preventDefault();
		},

		update: function (idx, $event) {
			if (common.checkLogin()) {

				var content = $("#re_modify_" + idx ).val();


				if (content.length < 10) {
					alert("10자 이상 입력해야 등록가능합니다.")
					return false;
				}
				
				$http.post("/api/user-funding.ashx", { t: 'update-reply', id: idx, content: content }).success(function (result) {
					console.log(result);
					if (result.success) {
						$scope.reply.getList({} , true);
						target.slideDown(300);

					} else {
						alert(result.message);
					}
				})
			}

			$event.preventDefault();
		},

		delete: function (idx, $event) {
			if (common.checkLogin()) {
				if (confirm("삭제하시겠습니까?")) {
					$http.post("/api/user-funding.ashx", { t: 'delete-reply', id: idx, uf_id: $("#uf_id").val() }).success(function (result) {
						if (result.success) {
							alert("삭제 되었습니다.");
							$scope.reply.getList({}, true);
						} else {
							alert(result.message);
						}
					})
				}
			}

			$event.preventDefault();
		}
	};

	$scope.reply.getList();

	// 업데이트 소식
	$scope.notice = {
		total: -1,
		list: [],
		params: {
			page: 1,
			rowsPerPage: 3,
			uf_id: $("#uf_id").val()
		},
		comment: "",

		hasNew : false ,	// 신규글이 있는지 여부

		getList: function (params, reload) {
			$scope.notice.params = $.extend($scope.notice.params, params);

			$http.get("/api/user-funding.ashx?t=notice-list", { params: $scope.notice.params }).success(function (r) {
				if (r.success) {
					console.log(r.data);
					var list = r.data;

					$.each(list, function () {
						//this.un_regdate = moment(this.un_regdate).fromNow();
						this.un_regdate = new Date(this.un_regdate);
						var split = this.un_image.split("/")
						this.or_image = split[split.length - 1]
						this.update_content = this.un_content.replace("<br>", "");
						//$scope.notice.setUploadEvent(this.un_id);
					});

					// 더보기인경우 merge
					if (reload) {
						$scope.notice.list = list;
					} else {
						$scope.notice.list = $.merge($scope.notice.list, list);
					}
					$scope.notice.total = list.length > 0 ? list[0].total : 0;

					if ($scope.notice.params.page == 1 && $scope.notice.list.length > 0) {
						var elapsed = (new Date() - $scope.notice.list[0].un_regdate) / (1000 * 60 * 60 * 24);		// days
						if (elapsed < 14) {
							$scope.notice.hasNew = true;
						}
					}

				} else {
					alert(r.message);
				}
			});

		},

		add: function ($event) {

			$event.preventDefault();
			if (!common.checkLogin()) {
				return;
			}

			
			if ($scope.notice.comment.length < 10) {
				alert("10자 이상 입력해야 등록가능합니다.")
				return;
			}
		

			var file = "";
			if ($("#file_path").val() != "") {
				file = $("#upload_root").val() + $("#file_path").val();
			}

			$http.post("/api/user-funding.ashx", { t: 'add-notice', uf_id: $("#uf_id").val(), comment: $scope.notice.comment, file: file }).success(function (result) {
				if (result.success) {
					console.log(result);
					$scope.notice.comment = "";
					$("#notice_comment_count").text("0");
					$("#lb_file_path").val("");
					$("#file_path").val("");
					$scope.notice.getList({ page: 1 }, true);
				} else {
					alert(result.message);
				}
			})

		},

		showMore: function ($event) {
			$event.preventDefault();

			$scope.notice.params.page++;
			$scope.notice.getList();
		},
		
		toggleUpdate: function (idx, $event) {

			$scope.notice.setUploadEvent(idx);
			target = $(".notice_modify_row[data-idx='" + idx + "']");

			if (target.css("display") == "none") {
				$(".notice_modify_row").hide(300);
				$("#file_path").val("");
				target.show(300);

			} else {
				target.hide(300);
			}

			$event.preventDefault();
		},

		update: function (idx, $event) {

			$event.preventDefault();
			if (!common.checkLogin()) {
				return;
			}

			var content = $(".notice_modify_row[data-idx='" + idx + "'] textarea")
			var fileName = $(".notice_modify_row[data-idx='" + idx + "'] input[type='text']");

			if (content < 10) {
				alert("10자 이상 입력해야 등록가능합니다.")
				return;
			}

			var file = "";
			
			if (fileName.val() != "") {
				file = $("#upload_root").val() + fileName.val();
			}

			console.log(fileName);
			if ($("#file_path").val() != "") {
				file = $("#upload_root").val() + $("#file_path").val();
			}
			
			console.log(fileName);
			$http.post("/api/user-funding.ashx", { t: 'update-notice', uf_id: $("#uf_id").val(), "id": idx, comment: content.val(), file: file }).success(function (result) {
				if (result.success) {
					console.log(result);
					content = "";
					fileName.val("");
					$("#file_path").val("");
					$scope.notice.getList({ page: 1 }, true);
				} else {
					alert(result.message);
				}
			})
			

		},

		delete: function (idx, $event) {
			if (common.checkLogin()) {
				if (confirm("삭제하시겠습니까?")) {
					
					$http.post("/api/user-funding.ashx", { t: 'delete-notice', uf_id: $("#uf_id").val(), id: idx }).success(function (result) {
						if (result.success) {
							alert("삭제 되었습니다.");
							$scope.notice.getList({}, true);
						} else {
							alert(result.message);
						}
					})
					
				}
			}

			$event.preventDefault();
		},

		setUploadEvent: function (idx) {
			//이미지 업로드 
			if ($("#btn_modify_file_path_" + idx).length > 0) {
				setTimeout(function () {
					var uploader = attachUploader("btn_modify_file_path_" + idx, idx);
					uploader._settings.data.fileDir = $("#upload_root").val();
					uploader._settings.data.fileType = "image";
					uploader._settings.data.limit = 5120;
				}, 600);
			}

			// 삭제버튼
			delete_target = $("#btn_delete_file_" + idx);
			if (delete_target.length > 0) {
				delete_target.unbind("click");
				delete_target.click(function () {
					$("#file_path").val("");
					$(".notice_modify_row[data-idx='" + idx + "'] input[type='text']").val("");
					return false;
				})

			}

			//카운트 
			var content = $(".notice_modify_row[data-idx='" + idx + "'] textarea")
			var count = $("#no_modify_count_" + idx);
			if (content.length > 0) content.textCount(count, { limit: 300 });



		}
	};

	$scope.notice.getList();

	// 참여자
	$scope.user = {
		total: -1,
		list: [],
		params: {
			page: 1,
			rowsPerPage: 10,
			uf_id: $("#uf_id").val()
		},
		comment: "",

		getList: function (params, reload) {
			$scope.user.params = $.extend($scope.user.params, params);

			$http.get("/api/user-funding.ashx?t=user-list", { params: $scope.user.params }).success(function (r) {
				if (r.success) {
					var list = r.data;

					$.each(list, function () {
						//this.un_regdate = moment(this.un_regdate).fromNow();
						this.uu_regdate = new Date(this.uu_regdate);
					});

					// 더보기인경우 merge
					if (reload) {
						$scope.user.list = list;
					} else {
						$scope.user.list = $.merge($scope.user.list, list);
					}
					$scope.user.total = list.length > 0 ? list[0].total : 0;


				} else {
					alert(r.message);
				}
			});

		},

		showMore: function ($event) {
			$event.preventDefault();

			$scope.user.params.page++;
			$scope.user.getList();
		}

	};

	$scope.user.getList();

	// 양육을 돕는 뉴스레터
	$scope.report = {
		total: -1,
		list: [],
		params: {
			page: 1,
			rowsPerPage: 3,
			campaignId: $("#campaignId").val()
		},
		
		getList: function (params, reload) {
			$scope.report.params = $.extend($scope.report.params, params);

			$http.get("/api/special-funding.ashx?t=report", { params: $scope.report.params }).success(function (r) {
				if (r.success) {
					//console.log(r.data);
					var list = r.data;

					$.each(list, function () {
						this.sr_regdate = new Date(this.sr_regdate);
						var elapsed = (new Date() - this.sr_regdate) / (1000 * 60 * 60 * 24);		// days
						this.is_new = elapsed < 14;

					});

					// 더보기인경우 merge
					if (reload) {
						$scope.report.list = list;
					} else {
						$scope.report.list = $.merge($scope.report.list, list);
					}
					$scope.report.total = list.length > 0 ? list[0].total : 0;


				} else {
					alert(r.message);
				}
			});

		},

		showMore: function ($event) {
			$event.preventDefault();
			
			$scope.report.params.page++;
			$scope.report.getList();
		}

	};

	$scope.report.getList();

	// 바로 후원하기
	$scope.showPay = function ($event) {
		$event.preventDefault();
		/*
		if (!$scope.checkEnd()) {
			return false;
		}
		*/
		$scope.modal.show();
	}

	// 페이지하단 팝업없이 기부페이지로
	$scope.goPay = function ($event) {
		$event.preventDefault();
		
		/*
		if (!$scope.checkEnd()) {
			return false;
		}
		*/
		$scope.goPayGateway($("#direct_amount").val());

	}

	$scope.checkEnd = function () {
		console.log($("#hd_percent").val());
		if ($("#hd_percent").val() >= 100) {
			alert("이미 달성된 나눔펀딩입니다.");
			return false;
		}
		return true;
	}

	// 레이어 팝업 - 후원금액설정 팝업
	$scope.modal = {
		instance: null,

		init: function () {
			// 팝업
			popup.init($scope, "/sponsor/pop-pay/NY", function (modal) {
				$scope.modal.instance = modal;
			});
		},

		show: function () {
			if (!$scope.modal.instance)
				return;

			setNumberOnly();

			$scope.modal.instance.show();

		},

		request: function ($event) {
			
			$event.preventDefault();
			if ($("#amount").val() == "") {
				alert("금액을 선택해 주세요");
				return false;

			}

			if ($scope.requesting) return;
			$scope.requesting = true;

			$scope.goPayGateway($("#amount").val());

		},

		close: function ($event) {
			$event.preventDefault();
			if (!$scope.modal.instance)
				return;

			$scope.modal.instance.hide();
		}

	}
	$scope.modal.init();

	$scope.goPayGateway = function (amount) {

		var amount = amount.replace(/\,/g, '');
		if (isNaN(amount)) {
			alert("금액을 정확히 입력해주세요");
			return;
		}

		if (parseInt(amount) < 1000) {
			alert("후원금은 1,000원 이상 입력 가능합니다.");
			$("#direct_amount_err").show();
			$("#direct_amount").focus();
			return;
		} else {
			$("#direct_amount_err").hide();
		}


		if (parseInt(amount) % 1000 > 0) {
			alert("천원단위로 입력해 주세요");
			
			$("#direct_amount").focus();
			return;
		}


		var params = {
			uf_id: $("#uf_id").val(),
			amount: amount
		}
		$.post("/sponsor/pay-gateway.ashx?t=go-user-funding", params).success(function (r) {

			if (r.success) {
				location.href = r.data;
			} else {
				alert(r.message);
			}
		});

	};
});

var attachUploader = function (button , idx) {
	return new AjaxUpload(button, {
		action: '/common/handler/upload',
		responseType: 'json',
		onChange: function () {
		},
		onSubmit: function (file, ext) {
			this.disable();
		},
		onComplete: function (file, response) {

			this.enable();

			if (response.success) {

				$("#file_path").val(response.name.replace(/^.*[\\\/]/, ''));

				if (button == "btn_file_path") {
					$("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
				} else {
					$(".notice_modify_row[data-idx='"+idx+"'] input[type='text']").val(response.name.replace(/^.*[\\\/]/, ''));
				}

			} else
				alert(response.msg);
		}
	});
}