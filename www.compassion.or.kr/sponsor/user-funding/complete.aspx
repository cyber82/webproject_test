﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="sponsor_user_funding_complete" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>나눔<em>펀딩</em></h1>
				<span class="desc">후원이 끊긴 어린이, 어린이센터 건축, 깨끗한 식수 지원, 직업교육, 의료지원, 엄마와 아이지원 등 다양한 곳에 후원해보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents sponsor">

			<div class="sf_regist_complete">
				<div class="w980">
					
					<div class="sub_tit mb40">
						<h2 class="tit blue"><em>나눔펀딩 등록</em>이 완료되었습니다.</h2>
						<p class="mb50"><em class="fs18">개설하신 나눔펀딩에서 첫 나눔 후원을 진행해보세요.</em></p>

						<p class="regist_complete">
							등록하신 나눔펀딩은 <a href="/my/user-funding/create" class="link2">마이컴패션 > 나눔펀딩관리</a>에서<br />
							새로운 업데이트 소식을 관리하거나 후원현황을 확인하실 수 있습니다.
						</p>
					</div>

					<div class="tac">
						<a href="/my/user-funding/create" class="btn_type3 mr10">내 나눔펀딩 보기</a>
						<a href="/sponsor/user-funding/view/<%:uf_id %>" class="btn_type1">첫 후원하기</a>
					</div>

				</div>
			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
</asp:Content>
