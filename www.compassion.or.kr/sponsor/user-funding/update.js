﻿$(function () {

	$page.init();


	$(".wrap").css("z-index", 2147483468)
	$(".gnbsub_wrap").css("z-index", 2147483468)

	$("#btn_uf_image_del").click(function () {
		$("#hd_uf_image").val("");
		$("#btn_uf_image").css({ "background-image": "url('')" });
		$("#btn_uf_image").find(".img_guide").show();
		$("#btn_uf_image_del").hide();
	});

});


var $page = {

	init: function () {

		$page.setFundingEvent();

		$("#btn_submit").click(function () {
			return $page.onSubmit();
		})
	},

	// 확인
	onSubmit: function () {

		if (!validateForm([
				{ id: "#hd_uf_image", msg: "대표이미지를 선택해 주세요" },
				{ id: "#uf_title", msg: "제목을 입력해 주세요" },
				{ id: "#uf_summary", msg: "한줄설명을 입력해 주세요" },
				{ id: "#uf_content", msg: "상세스토리를 입력해 주세요" }
		])) {
			return false;
		}

		if ($("#uf_title").val().length < 4) { alert("제목을 4자 이상 입력해 주세요"); $("#uf_title").focus(); return false; }
		if ($("#uf_summary").val().length < 4) { alert("한줄설명을 4자 이상 입력해 주세요"); $("#uf_summary").focus(); return false; }
		if ($("#uf_content").val().length < 10) { alert("상세스토리를 10자 이상 입력해 주세요"); $("#uf_content").focus(); return false; }


		if (!confirm("수정하시겠습니까?")) return false;



		$page.updateMovieData();

		$page.updateContentImageData();

		return true;
	},

	setFundingEvent: function () {

		$("#uf_title").textCount($("#uf_title_count"), { limit: 20 });
		$("#uf_summary").textCount($("#uf_summary_count"), { limit: 40 });
		$("#uf_content").textCount($("#uf_content_count"), { limit: 2000 });

		$("#uf_title_count").text($("#uf_title").val().length);
		$("#uf_summary_count").text($("#uf_summary").val().length);
		$("#uf_content_count").text($("#uf_content").val().length);



		{
			var uploader = $page.attachUploader("btn_uf_image");
			uploader._settings.data.fileDir = $("#hd_upload_root").val();
			uploader._settings.data.fileType = "image";
			uploader._settings.data.limit = 1024;
		}

		if ($("#hd_uf_image").val() != "") {
			$("#btn_uf_image").css({ "background-image": "url('" + $("#hd_image_domain").val() + $("#hd_uf_image").val() + "')" });
			$("#btn_uf_image").find(".img_guide").hide();
			$("#btn_uf_image").find("#btn_uf_image_del").show();

		}


	},

	attachUploader: function (button) {
		return new AjaxUpload(button, {
			action: '/common/handler/upload',
			responseType: 'json',
			onChange: function () {
			},
			onSubmit: function (file, ext) {
				this.disable();
			},
			onComplete: function (file, response) {

				this.enable();

				if (response.success) {
					if (button == "btn_uf_image") {
						$("#hd_uf_image").val(response.name);
						$("#btn_uf_image").find(".img_guide").hide();
						$("#btn_uf_image").find("#btn_uf_image_del").show();
						$("#btn_uf_image").css({ "background-image": "url('" + $("#hd_image_domain").val() + $("#hd_uf_image").val() + "')" });

					} else if (button.indexOf("uf_content_image") > -1) {
						$("#" + button).attr("data-url", response.name);
						$("#" + button).find(".img_guide").hide();
						$("#btn_uf_image").find("#btn_uf_image_del").show();
						$("#" + button).css({ "background-image": "url('" + $("#hd_image_domain").val() + response.name + "')" });

						$page.updateContentImageData();
					}
				} else
					alert(response.msg);
			}
		});
	},

	updateContentImageData: function () {

		var data = "";
		$.each($(".uf_content_image"), function () {
			var url = $(this).attr("data-url");
			if (url != "") {
				data += "|" + url;
				//	console.log("url>" + url);
			}

		});
		if (data.length > 0) {
			data = data.substring(1);
		}

		$("#hd_uf_content_image").val(data);
		console.log($(".uf_content_image").length, data);

	},

	updateMovieData: function () {

		var data = "";
		$.each($("input[name=uf_movie]"), function () {
			var val = $(this).val();
			if (val != "") {
				data += "|" + val;
				//	console.log("url>" + url);
			}

		});
		if (data.length > 0) {
			data = data.substring(1);
		}
		$("#hd_uf_movie").val(data);

	}


};

var goBack = function () {
	if (location.pathname.indexOf("/sponsor/user/update/") > -1)
		location.href = "/";
	else
		history.back();
};

(function () {

	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {
		// form
		$scope.form = {

			init: function () {
				$scope.form.movie.init();
				$scope.form.image.init();
			},

			movie: {
				list: [],

				init: function () {

					var val = $("#hd_uf_movie").val();
					if (val == "") {
						$scope.form.movie.list.push({ index: $scope.form.movie.list.length, val: "" });
					} else {
						for (i = 0 ; i < val.split('|').length ; i++) {
							var m = val.split('|')[i];
							if (m == "") continue;

							$scope.form.movie.list.push({ index: $scope.form.movie.list.length, val: m });
						}
					}

				},

				control: function ($event, $index) {
					console.log($index);
					if ($index > 0) {	// 삭제
						console.log($scope.form.movie.list);
						$scope.form.movie.list.splice($index, 1);
					} else {

						$scope.form.movie.list.push({ index: $scope.form.movie.list.length, val: "" });
					}
				}
			},

			image: {
				list: [],

				init: function () {

					var val = $("#hd_uf_content_image").val();
					if (val == "") {
						var index = $scope.form.image.list.length;
						$scope.form.image.list.push({ index: index, val: "" });
						$scope.form.image.setUploadEvent(index);

					} else {
						for (i = 0 ; i < val.split('|').length ; i++) {
							var m = val.split('|')[i];
							if (m == "") continue;

							var index = $scope.form.image.list.length;
							$scope.form.image.list.push({ index: index, val: m });
							$scope.form.image.setUploadEvent(index);
						}

					}

				},

				control: function ($event, $index) {
					console.log($index);
					if ($index > 0) {	// 삭제
						console.log($scope.form.image.list);
						$scope.form.image.list.splice($index, 1);
					} else {

						var index = $scope.form.image.list.length;
						$scope.form.image.list.push({ index: index, val: "" });
						$scope.form.image.setUploadEvent(index);
					}
				},

				setUploadEvent: function (index) {
					setTimeout(function () {
						var uploader = $page.attachUploader("uf_content_image_" + index);
						uploader._settings.data.fileDir = $("#hd_upload_root").val();
						uploader._settings.data.fileType = "image";
						uploader._settings.data.limit = 1024;

						$page.updateContentImageData();

					}, 300);
				}
			}

		};

		$scope.form.init();
	});

})();
