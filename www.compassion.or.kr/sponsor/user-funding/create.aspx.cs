﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
using TCPTModel.Request.Hold.Beneficiary;
using Newtonsoft.Json;
using TCPTModel;
using TCPTModel.Response.Beneficiary;
using TCPTModel.Response.Hold.Beneficiary;

public partial class sponsor_user_funding_create : SponsorPayBasePage
{

    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    public override bool RequireSSL
    {
        get
        {
            return true;
        }
    }


    protected override void OnBeforePostBack()
    {

        base.OnBeforePostBackRegular(new PayItemSession.Entity());

        UserInfo sess = new UserInfo();

        this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];
        hd_image_domain.Value = ConfigurationManager.AppSettings["domain_file"];
        hd_auth_domain.Value = this.ViewState["auth_domain"].ToString();
        hd_upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_user_funding);
        hd_userpic_upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_user_pic);

        if (sess.GenderCode == "C")
        {
            base.AlertWithJavascript("개인회원만 해당 서비스를 이용하실 수 없습니다.", "goBack()");
            return;
        }


        if (!AdminLoginSession.HasCookie(this.Context))
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                // 펀딩 개설 가능한지 여부 
                //var result = dao.sp_tUserFunding_can_create_f(sess.UserId).First();
                Object[] op1 = new Object[] { "userID" };
                Object[] op2 = new Object[] { sess.UserId };
                var result = www6.selectSP("sp_tUserFunding_can_create_f", op1, op2).DataTableToList<sp_tUserFunding_can_create_fResult>().First();

                if (result.result == "N")
                {
                    base.AlertWithJavascript(result.msg, "goBack()");

                    return;
                }

            }
        }

        if (!this.GetCampaignInfo())
        {
            return;
        }

        //	this.GetUserInfo();

        //		this.GetSponsorInfo();


    }

    bool GetCampaignInfo()
    {

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tSpecialFunding_list_f("", "", "", "Y", "").ToList();
            Object[] op1 = new Object[] { "accountClassGroup", "accountClass", "fixedYN", "ufYN", "pick" };
            Object[] op2 = new Object[] { "", "", "", "Y", "" };
            var list = www6.selectSP("sp_tSpecialFunding_list_f", op1, op2).DataTableToList<sp_tSpecialFunding_list_fResult>();

            hd_campaigns.Value = list.ToJson();
            CampaignID.Items.Add(new ListItem("후원금 사용처를 선택해 주세요", ""));
            foreach (var entity in list)
            {
                CampaignID.Items.Add(new ListItem(entity.CampaignName, entity.CampaignID));
            }
        }

        return true;
    }

    protected bool GetUserInfo()
    {

        if (!UserInfo.IsLogin)
        {
            return false;
        }

        var sess = new UserInfo();

        using (AuthDataContext dao = new AuthDataContext())
        {
            //var pic = dao.tSponsorMaster.First(p => p.UserID == sess.UserId).UserPic;
            var pic = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId).UserPic;
            if (!string.IsNullOrEmpty(pic))
            {
                image_UserPic.Value = pic;
                hd_user_pic.Value = pic.Substring(pic.LastIndexOf("/"));
            }
        }

        return true;

    }


    protected void btn_submit_Click(object sender, EventArgs e)
    {

        if (base.IsRefresh)
        {
            //		return;
        }

        try
        {
            if (!base.UpdateRegularUserInfo())
                return;
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, "나눔펀딩 UpdateRegularUserInfo 에러 : " + ex.Message);
        }

        //Response.Write(new UserInfo().ToJson());
        //return;
        if (!this.AddUserFunding())
        {
            return;
        }

        Response.Redirect(string.Format("/sponsor/user-funding/complete/{0}", this.PrimaryKey.ToString()));
    }

    bool AddUserFunding()
    {
        string test = "";
        #region 폼값 체크
        if (hd_uf_type.Value == "child")
        {
            if (hd_childMasterId.Value == "")
            {
                base.AlertWithJavascript("후원하실 어린이를 선택해주세요");
                return false;
            }
        }
        else
        {
            if (CampaignID.SelectedValue == "")
            {
                base.AlertWithJavascript("후원금 사용처를 선택해 주세요");
                return false;
            }
        }

        if (string.IsNullOrEmpty(uf_date_start.Value))
        {
            base.AlertWithJavascript("시작일을 선택해 주세요");
            return false;
        }

        if (string.IsNullOrEmpty(uf_date_end.Value))
        {
            base.AlertWithJavascript("종료일을 선택해 주세요");
            return false;
        }

        if (string.IsNullOrEmpty(uf_goal_amount.Value))
        {
            base.AlertWithJavascript("목표금액을 입력해 주세요");
            return false;
        }

        if (string.IsNullOrEmpty(hd_uf_image.Value))
        {
            base.AlertWithJavascript("대표이미지를 선택해 주세요");
            return false;
        }

        if (string.IsNullOrEmpty(uf_title.Value))
        {
            base.AlertWithJavascript("제목을 입력해 주세요");
            return false;
        }

        if (string.IsNullOrEmpty(uf_summary.Value))
        {
            base.AlertWithJavascript("한줄설명을 입력해 주세요");
            return false;
        }

        if (string.IsNullOrEmpty(uf_content.Value))
        {
            base.AlertWithJavascript("상세스토리를 입력해 주세요");
            return false;
        }

        if (string.IsNullOrEmpty(hd_user_pic.Value))
        {
            base.AlertWithJavascript("개설자 사진을 선택해 주세요");
            return false;
        }
        #endregion

        //[이종진] 기존방법
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();
        if (strdbgp_kind == "1")
        {
            return AddUserFunding_DB();
        }
        //[이종진] 신규방법 (Global Pool 조회)
        else
        {
            return AddUserFunding_GP();
        }
        
    }

    bool AddUserFunding_DB()
    {
        string test = "";

        string childMasterId = hd_childMasterId.Value;

        UserInfo sess = new UserInfo();
        var action = new UserFundingAction();
        using (FrontDataContext dao = new FrontDataContext())
        {
            if (!AdminLoginSession.HasCookie(this.Context))
            {
                //var result = dao.sp_tUserFunding_can_create_f(sess.UserId).First();
                Object[] op1 = new Object[] { "UserId" };
                Object[] op2 = new Object[] { sess.UserId };
                var result = www6.selectSP("sp_tUserFunding_can_create_f", op1, op2).DataTableToList<sp_tUserFunding_can_create_fResult>().First();

                if (result.result == "N")
                {
                    base.AlertWithJavascript(result.msg);
                    return false;
                }
            }
            
            Guid newHoldUID = Guid.NewGuid();
            var campaignId = "";
            var goal_amount = Convert.ToInt32(uf_goal_amount.Value);
            if (hd_uf_type.Value == "child")
            {
                //캠페인 등록 후, 캠페인ID 리턴
                var actionResult = action.AddCampaign(childMasterId, hd_childKey.Value, hd_childName.Value, goal_amount, uf_date_start.Value, uf_date_end.Value);
                if (!actionResult.success)
                {
                    base.AlertWithJavascript(actionResult.message);
                    return false;
                }
                campaignId = actionResult.data.ToString();

                // 어린이 hold 정보 조회
                Object[] objParam = new object[] { "ChildMasterID" };
                Object[] objValue = new object[] { childMasterId };
                Object[] objSql = new object[] { "sp_S_ChildHoldStatus" };
                DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

                // 어린이 홀드 정보 조회 오류
                if (dt == null || dt.Rows.Count == 0)
                {
                    base.AlertWithJavascript("어린이 정보 조회에 실패했습니다.");
                    ErrorLog.Write(HttpContext.Current, 0, "어린이 정보 조회에 실패했습니다.");
                    return false;
                }

                DataRow dr = dt.Rows[0];

                // Hold Update
                string endDT = DateTime.ParseExact(uf_date_end.Value, "yyyy-MM-dd", null).AddMonths(1).ToString("yyyy-MM-dd"); ;
                bool updateHold = false;
                try
                {
                    TCPTService.Service svc = new TCPTService.Service();
                    svc.Timeout = 180000;

                    BeneficiaryHoldRequestList hKit = new BeneficiaryHoldRequestList();
                    hKit.Beneficiary_GlobalID = dr["Beneficiary_GlobalID"].ToString();
                    hKit.BeneficiaryState = "Consignment Hold";
                    hKit.HoldEndDate = endDT + "T23:59:59Z";
                    hKit.IsSpecialHandling = false;
                    hKit.PrimaryHoldOwner = sess.UserId;
                    hKit.GlobalPartner_ID = "KR";
                    hKit.HoldID = dr["HoldID"].ToString();
                    string HoldUID = dr["HoldUID"].ToString();

                    string hJson = JsonConvert.SerializeObject(hKit);
                    string holdResult = svc.BeneficiaryHoldSingle_PUT(hKit.Beneficiary_GlobalID, hKit.HoldID, hJson);
                    TCPTResponseMessage updateResult = JsonConvert.DeserializeObject<TCPTResponseMessage>(holdResult);
                    if (updateResult.IsSuccessStatusCode)
                    {
                        test += "hold 업데이트 성공 / ";
                        test += "기존 hold  expired  처리 시작 / ";
                        // 기존 hold  expired  처리
                        bool b = svc.UpdateHoldStatus(new Guid(HoldUID), hKit.HoldID, hKit.BeneficiaryState, endDT, "Expired", "2000", "나눔펀딩 Hold로 Expired 처리", sess.UserId, sess.UserId);

                        if (b)
                        {
                            test += "기존 hold  expired  처리 성공 / ";
                        }
                        else
                        {
                            test += "기존 hold  expired  처리 실패 / ";
                        }
                        // 신규  hold insert
                        test += "신규 hold insert / ";
                        b = svc.InsertHoldHistory(newHoldUID, hKit.HoldID, hKit.Beneficiary_GlobalID, hKit.BeneficiaryState, endDT, sess.UserId, "", "2000", "나눔펀딩 Hold");
                        updateHold = true;
                    }
                    else
                    {
                        // hold update error
                        test += "Consignment Hold 업데이트 실패 " + updateResult.RequestMessage.ToString() + " / ";
                        base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다. 다른 어린이를 선택 해주세요.");
                        ErrorLog.Write(HttpContext.Current, 0, test);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    test += "Consignment Hold 로 업데이트 오류 " + ex.Message + " / ";
                    base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다. 다른 어린이를 선택 해주세요.");
                    ErrorLog.Write(HttpContext.Current, 0, test);
                    return false;
                }

                if (!updateHold)
                {
                    base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다.");
                    ErrorLog.Write(HttpContext.Current, 0, test);
                    return false;
                }

                try
                {
                    // 신규 캠페인에 홀드 정보 insert
                    Object[] objParam2 = new object[] { "CampaignID", "ChildMasterID", "RegisterID", "RegisterName", "HoldUID" };
                    Object[] objValue2 = new object[] { campaignId, childMasterId, sess.UserId, sess.UserName, newHoldUID.ToString() };
                    Object[] objSql2 = new object[] { "sp_I_CampaignConsign" };
                    int n = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2);
                }
                catch (Exception ex)
                {
                    base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다.");
                    ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                    return false;
                }
            }
            else
            {
                goal_amount = goal_amount * 10000;
            }

            var entity = new tUserFunding()
            {
                CampaignID = hd_uf_type.Value == "normal" ? CampaignID.SelectedValue : campaignId,
                ChildMasterID = hd_uf_type.Value == "normal" ? "0000000000" : childMasterId,
                ChildName = hd_childName.Value,
                ChildKey = hd_childKey.Value,
                uf_cnt_user = 0,
                uf_cnt_view = 0,
                uf_deleted = false,
                uf_cnt_notice = 0,
                uf_cnt_reply = 0,
                uf_content = uf_content.Value.EscapeSqlInjection(),
                uf_current_amount = 0,
                uf_date_end = Convert.ToDateTime(uf_date_end.Value + " 23:59:59"),
                uf_date_start = Convert.ToDateTime(uf_date_start.Value),
                uf_display = true,
                uf_goal_amount = goal_amount,
                uf_image = hd_uf_image.Value,
                uf_moddate = DateTime.Now,
                uf_regdate = DateTime.Now,
                uf_movie = hd_uf_movie.Value,
                uf_content_image = hd_uf_content_image.Value,
                uf_summary = uf_summary.Value,
                uf_title = uf_title.Value,
                uf_type = hd_uf_type.Value,
                UserID = sess.UserId
            };

            //dao.tUserFunding.InsertOnSubmit(entity);
            www6.insert(entity);

            try
            {
                //dao.SubmitChanges();
            }
            catch (Exception ex)
            {
                string msg = "";
                msg += hd_uf_type.Value == "normal" ? CampaignID.SelectedValue : campaignId;
                msg += hd_uf_type.Value == "normal" ? "0000000000" : childMasterId;
                msg += hd_childName.Value;
                msg += hd_childKey.Value;
                msg += "0";
                msg += "0";
                msg += "false";
                msg += "0";
                msg += "0";
                msg += uf_content.Value.EscapeSqlInjection();
                msg += "0";
                msg += Convert.ToDateTime(uf_date_end.Value + " 23:59:59");
                msg += Convert.ToDateTime(uf_date_start.Value);
                msg += "true";
                msg += goal_amount.ToString();
                msg += hd_uf_image.Value;
                msg += DateTime.Now.ToString("yyyy-MM-dd");
                msg += DateTime.Now.ToString("yyyy-MM-dd");
                msg += hd_uf_movie.Value;
                msg += hd_uf_content_image.Value;
                msg += uf_summary.Value;
                msg += uf_title.Value;
                msg += hd_uf_type.Value;
                msg += sess.UserId.ToString();


                ErrorLog.Write(HttpContext.Current, 0, msg);
                throw ex;
            }
            this.PrimaryKey = entity.uf_id.ToString();

            /*         이벤트 정보 등록  -- 20170327 이정길       */
            string sponsorType = "나눔펀딩(개설)"; ;
            string sponsorTypeEng = "USER_FUNDING_CREATE";
            var eventID = Session["eventID"];
            var eventSrc = Session["eventSrc"];

            if (eventID == null)
            {
                if (Request.Cookies["e_id"] != null)
                {
                    eventID = Request.Cookies["e_id"].Value;
                    eventSrc = Request.Cookies["e_src"].Value;
                }
            }

            try
            {
                using (FrontDataContext dda = new FrontDataContext())
                {
                    if (eventID != null && eventSrc != null && !eventID.Equals(""))
                    {
                        //dda.sp_tEventApply_insert_f(int.Parse(eventID.ToString()),
                        //                            new UserInfo().UserId,
                        //                            new UserInfo().SponsorID,
                        //                            new UserInfo().ConId,
                        //                            eventSrc.ToString(),
                        //                            sponsorType,
                        //                            sponsorTypeEng,
                        //                            goal_amount,
                        //                            "");
                        //dao.SubmitChanges();

                        Object[] op1 = new Object[] { "evt_id", "UserID", "SponsorID", "ConID", "evt_route", "SponsorType", "SponsorTypeEng", "SponsorAmount", "CommitmentID" };
                        Object[] op2 = new Object[] { int.Parse(eventID.ToString()), new UserInfo().UserId, new UserInfo().SponsorID, new UserInfo().ConId, eventSrc.ToString(), sponsorType, sponsorTypeEng, goal_amount, "" };
                        www6.selectSP("sp_tEventApply_insert_f", op1, op2);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, "나눔펀딩 생성 이벤트 정보 등록 error :: " + ex.Message);
            }

            // tSponsorCall 등록
            if (hd_uf_type.Value == "child")
            {
                action.AddSponsorCall(entity.uf_id, "후원신청", "결연후원", "신규결연", "나눔펀딩 모금함 개설",
                    string.Format("나눔펀딩 모금함 개설 \n캠페인 : {0} \n결연 예정 어린이 : {1} \n모금 기간 : {2} ~ {3}", entity.CampaignID, entity.ChildKey, entity.uf_date_start.ToString("yy년MM월dd일"), entity.uf_date_end.ToString("yy년MM월dd일")));
            }
            else
            {
                action.AddSponsorCall(entity.uf_id, "후원신청", "양육보완후원", "신규후원", "나눔펀딩 모금함 개설",
                    string.Format("나눔펀딩 모금함 개설 \n캠페인 : {0}\n모금 기간 : {1} ~ {2}", entity.CampaignID, entity.uf_date_start.ToString("yy년MM월dd일"), entity.uf_date_end.ToString("yy년MM월dd일")));
            }

            this.SendMail(entity);

            new StampAction().Add("ufc");
        }

        // 회원사진 업데이트
        using (AuthDataContext dao = new AuthDataContext())
        {
            //var user = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
            var user = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);
            user.UserPic = hd_user_pic.Value;

            //dao.SubmitChanges();
            www6.updateAuth(user);

        }

        return true;
    }

    bool AddUserFunding_GP()
    {
        string test = "";
        //[이종진] ChildMasterID선언
        //신규 방법의 경우는, hd_childMasterId -> "09" + ChildGlobalId이기 때문에, 어린이정보 체크 및 insert 후, childmasterid를 다시 받아온다.
        string childMasterId = hd_childMasterId.Value;

        UserInfo sess = new UserInfo();
        var action = new UserFundingAction();
        using (FrontDataContext dao = new FrontDataContext())
        {
            if (!AdminLoginSession.HasCookie(this.Context))
            {
                //var result = dao.sp_tUserFunding_can_create_f(sess.UserId).First();
                Object[] op1 = new Object[] { "UserId" };
                Object[] op2 = new Object[] { sess.UserId };
                var result = www6.selectSP("sp_tUserFunding_can_create_f", op1, op2).DataTableToList<sp_tUserFunding_can_create_fResult>().First();

                if (result.result == "N")
                {
                    base.AlertWithJavascript(result.msg);
                    return false;
                }
            }

            Guid newHoldUID = Guid.NewGuid();
            var campaignId = "";
            var goal_amount = Convert.ToInt32(uf_goal_amount.Value);
            
            if (hd_uf_type.Value == "child")
            {
                //[이종진]ConsignmentHold 
                #region | 어린이 ConsignmentHold |

                // Hold Update
                string endDT = DateTime.ParseExact(uf_date_end.Value, "yyyy-MM-dd", null).AddMonths(1).ToString("yyyy-MM-dd"); ;
                bool updateHold = false;
                
                try
                {
                    // TCPT Hold 처리용 킷 생성
                    BeneficiaryHoldRequestList item = new BeneficiaryHoldRequestList();
                    item.Beneficiary_GlobalID = childMasterId.Substring(2);
                    item.BeneficiaryState = "Consignment Hold";
                    item.HoldEndDate = endDT+ "T23:59:59Z";
                    item.IsSpecialHandling = false;
                    item.PrimaryHoldOwner = sess.UserId; 
                    item.GlobalPartner_ID = "KR";
                    
                    string json = JsonConvert.SerializeObject(item);
                    string strHoldResult = string.Empty;

                    CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();

                    // 홀드요청
                    strHoldResult = _OffRampService.BeneficiaryHoldRequest_Single_POST(childMasterId.Substring(2), json);
                    TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(strHoldResult);
                    //실패
                    if (!msg.IsSuccessStatusCode)
                    {
                        // hold update error
                        test += "Consignment Hold 실패 " + msg.RequestMessage.ToString() + " / ";
                        base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다. 다른 어린이를 선택 해주세요.");
                        ErrorLog.Write(HttpContext.Current, 0, test);
                        return false;
                    }

                    // 성공
                    BeneficiaryHoldRequestResponse_Kit response = JsonConvert.DeserializeObject<BeneficiaryHoldRequestResponse_Kit>(msg.RequestMessage.ToString());
                    BeneficiaryHoldResponseList_Kit res = JsonConvert.DeserializeObject<BeneficiaryHoldResponseList_Kit>(msg.RequestMessage.ToString());
                    response = res.BeneficiaryHoldResponseList[0];
                    
                    // 신규  hold insert TCPT_Beneficiary_HOLD
                    test += "신규 hold insert / ";
                    updateHold = new ChildAction().InsertHoldHistory(newHoldUID, response.HoldID, childMasterId.Substring(2), item.BeneficiaryState, endDT, sess.UserId, sess.UserName, "2000", "나눔펀딩 Hold");
                    
                }
                catch (Exception ex)
                {
                    test += "Consignment Hold 생성 오류 " + ex.Message + " / ";
                    base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다. 다른 어린이를 선택 해주세요.");
                    ErrorLog.Write(HttpContext.Current, 0, test);
                    return false;
                }

                if (!updateHold)
                {
                    base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다.");
                    ErrorLog.Write(HttpContext.Current, 0, test);
                    return false;
                }

                #endregion

                //[이종진]어린이 정보 insert
                #region | 어린이 정보 insert |

                //beneficiary kit(어린이 상세정보) 조회
                JsonWriter childDetail = new JsonWriter();
                ChildAction childAction = new ChildAction();
                childDetail = childAction.Beneficiary_GET(childMasterId);
                if (!childDetail.success)
                {
                    base.AlertWithJavascript("어린이 상세정보 조회에 실패하였습니다. 다른 어린이를 선택해주세요.");
                    ErrorLog.Write(HttpContext.Current, 0, test);
                    return false;
                }

                //조회 후, 어린이 정보 및 TCPT_Bene 정보 insert
                //신규조회방법일 경우, 초기 ChildMasterID가 "09"+ChildGlobalID임. -> 존재하는 어린이면 존재하는 어린이의 ChildMasterID를 return함
                JsonWriter insertResult = new JsonWriter();
                insertResult = childAction.InsertTCPTBene_tChild(childDetail.data, childMasterId, hd_childKey.Value);
                if (insertResult.success)
                {
                    childMasterId = insertResult.data.ToString();
                }
                else
                {
                    base.AlertWithJavascript("어린이 정보 생성이 실패하였습니다. 다른 어린이를 선택해주세요.");
                    ErrorLog.Write(HttpContext.Current, 0, test);
                    return false;
                }

                #endregion

                //캠페인등록
                var actionResult = action.AddCampaign(childMasterId, hd_childKey.Value, hd_childName.Value, goal_amount, uf_date_start.Value, uf_date_end.Value);
                if (!actionResult.success)
                {
                    base.AlertWithJavascript(actionResult.message);
                    return false;
                }
                campaignId = actionResult.data.ToString();
                
                try
                {
                    // 신규 캠페인에 홀드 정보 insert
                    Object[] objParam2 = new object[] { "CampaignID", "ChildMasterID", "RegisterID", "RegisterName", "HoldUID" };
                    Object[] objValue2 = new object[] { campaignId, childMasterId, sess.UserId, sess.UserName, newHoldUID.ToString() };
                    Object[] objSql2 = new object[] { "sp_I_CampaignConsign" };
                    int n = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2);
                }
                catch (Exception ex)
                {
                    base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다.");
                    ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                    return false;
                }
            }
            else
            {
                goal_amount = goal_amount * 10000;
            }

            var entity = new tUserFunding()
            {
                CampaignID = hd_uf_type.Value == "normal" ? CampaignID.SelectedValue : campaignId,
                ChildMasterID = hd_uf_type.Value == "normal" ? "0000000000" : childMasterId,
                ChildName = hd_childName.Value,
                ChildKey = hd_childKey.Value,
                uf_cnt_user = 0,
                uf_cnt_view = 0,
                uf_deleted = false,
                uf_cnt_notice = 0,
                uf_cnt_reply = 0,
                uf_content = uf_content.Value.EscapeSqlInjection(),
                uf_current_amount = 0,
                uf_date_end = Convert.ToDateTime(uf_date_end.Value + " 23:59:59"),
                uf_date_start = Convert.ToDateTime(uf_date_start.Value),
                uf_display = true,
                uf_goal_amount = goal_amount,
                uf_image = hd_uf_image.Value,
                uf_moddate = DateTime.Now,
                uf_regdate = DateTime.Now,
                uf_movie = hd_uf_movie.Value,
                uf_content_image = hd_uf_content_image.Value,
                uf_summary = uf_summary.Value,
                uf_title = uf_title.Value,
                uf_type = hd_uf_type.Value,
                UserID = sess.UserId
            };

            //dao.tUserFunding.InsertOnSubmit(entity);
            www6.insert(entity);

            try
            {
                //dao.SubmitChanges();
            }
            catch (Exception ex)
            {
                string msg = "";
                msg += hd_uf_type.Value == "normal" ? CampaignID.SelectedValue : campaignId;
                msg += hd_uf_type.Value == "normal" ? "0000000000" : childMasterId;
                msg += hd_childName.Value;
                msg += hd_childKey.Value;
                msg += "0";
                msg += "0";
                msg += "false";
                msg += "0";
                msg += "0";
                msg += uf_content.Value.EscapeSqlInjection();
                msg += "0";
                msg += Convert.ToDateTime(uf_date_end.Value + " 23:59:59");
                msg += Convert.ToDateTime(uf_date_start.Value);
                msg += "true";
                msg += goal_amount.ToString();
                msg += hd_uf_image.Value;
                msg += DateTime.Now.ToString("yyyy-MM-dd");
                msg += DateTime.Now.ToString("yyyy-MM-dd");
                msg += hd_uf_movie.Value;
                msg += hd_uf_content_image.Value;
                msg += uf_summary.Value;
                msg += uf_title.Value;
                msg += hd_uf_type.Value;
                msg += sess.UserId.ToString();


                ErrorLog.Write(HttpContext.Current, 0, msg);
                throw ex;
            }
            this.PrimaryKey = entity.uf_id.ToString();

            /*         이벤트 정보 등록  -- 20170327 이정길       */
            string sponsorType = "나눔펀딩(개설)"; ;
            string sponsorTypeEng = "USER_FUNDING_CREATE";
            var eventID = Session["eventID"];
            var eventSrc = Session["eventSrc"];

            if (eventID == null)
            {
                if (Request.Cookies["e_id"] != null)
                {
                    eventID = Request.Cookies["e_id"].Value;
                    eventSrc = Request.Cookies["e_src"].Value;
                }
            }

            try
            {
                using (FrontDataContext dda = new FrontDataContext())
                {
                    if (eventID != null && eventSrc != null && !eventID.Equals(""))
                    {
                        //dda.sp_tEventApply_insert_f(int.Parse(eventID.ToString()),
                        //                            new UserInfo().UserId,
                        //                            new UserInfo().SponsorID,
                        //                            new UserInfo().ConId,
                        //                            eventSrc.ToString(),
                        //                            sponsorType,
                        //                            sponsorTypeEng,
                        //                            goal_amount,
                        //                            "");
                        //dao.SubmitChanges();

                        Object[] op1 = new Object[] { "evt_id", "UserID", "SponsorID", "ConID", "evt_route", "SponsorType", "SponsorTypeEng", "SponsorAmount", "CommitmentID" };
                        Object[] op2 = new Object[] { int.Parse(eventID.ToString()), new UserInfo().UserId, new UserInfo().SponsorID, new UserInfo().ConId, eventSrc.ToString(), sponsorType, sponsorTypeEng, goal_amount, "" };
                        www6.selectSP("sp_tEventApply_insert_f", op1, op2);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, "나눔펀딩 생성 이벤트 정보 등록 error :: " + ex.Message);
            }

            // tSponsorCall 등록
            if (hd_uf_type.Value == "child")
            {
                action.AddSponsorCall(entity.uf_id, "후원신청", "결연후원", "신규결연", "나눔펀딩 모금함 개설",
                    string.Format("나눔펀딩 모금함 개설 \n캠페인 : {0} \n결연 예정 어린이 : {1} \n모금 기간 : {2} ~ {3}", entity.CampaignID, entity.ChildKey, entity.uf_date_start.ToString("yy년MM월dd일"), entity.uf_date_end.ToString("yy년MM월dd일")));
            }
            else
            {
                action.AddSponsorCall(entity.uf_id, "후원신청", "양육보완후원", "신규후원", "나눔펀딩 모금함 개설",
                    string.Format("나눔펀딩 모금함 개설 \n캠페인 : {0}\n모금 기간 : {1} ~ {2}", entity.CampaignID, entity.uf_date_start.ToString("yy년MM월dd일"), entity.uf_date_end.ToString("yy년MM월dd일")));
            }

            this.SendMail(entity);

            new StampAction().Add("ufc");
        }

        // 회원사진 업데이트
        using (AuthDataContext dao = new AuthDataContext())
        {
            //var user = dao.tSponsorMaster.First(p => p.UserID == sess.UserId);
            var user = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);
            user.UserPic = hd_user_pic.Value;

            //dao.SubmitChanges();
            www6.updateAuth(user);

        }

        return true;
    }

    // 메일발송
    void SendMail(tUserFunding entity)
    {

        try
        {

            var sess = new UserInfo();

            var actionResult = new SponsorAction().GetCommunications();
            if (!actionResult.success)
            {
                ErrorLog.Write(this.Context, 0, actionResult.message);
                return;
            }

            var email = ((SponsorAction.CommunicationResult)actionResult.data).Email;
            if (string.IsNullOrEmpty(email))
                return;

            var from = ConfigurationManager.AppSettings["emailUserFunding"];

            var args = new Dictionary<string, string> {
					{"{userName}" ,  sess.UserName} ,
					{"{typeName}" , hd_uf_type.Value == "child" ? "어린이 결연 펀딩" : "양육을 돕는 펀딩"} ,
					{"{duration}" , string.Format("{0} ~ {1}" , entity.uf_date_start.ToString("yyyy.MM.dd") , entity.uf_date_end.ToString("yyyy.MM.dd")) } ,
					{"{goal_amount}" , entity.uf_goal_amount.ToString("N0") } ,
					{"{title}" , entity.uf_title} ,
					{"{uf_id}" , entity.uf_id.ToString()} 
				};

            Email.Send(HttpContext.Current, from, new List<string>() { email },
                "[한국컴패션] 나눔펀딩 개설이 완료되었습니다.", " /mail/uf-create.html",
                args, null);

        }
        catch (Exception e)
        {
            ErrorLog.Write(HttpContext.Current, 0, e.Message);
            throw e;
        }

    }

}