﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="sponsor_user_funding_default" MasterPageFile="~/main.Master" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
	<script type="text/javascript" src="/sponsor/user-funding/default.js?v=1"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>나눔<em>펀딩</em></h1>
				<span class="desc">후원이 끊긴 어린이, 어린이센터 건축, 깨끗한 식수 지원, 직업교육, 의료지원, 엄마와 아이지원 등 다양한 곳에 후원해보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 sponsor">

			<div class="visual_funding">
				<div class="slide_wrap" id="special_funding_visual">
					<!-- sliding set / size : 100% * 500px -->
					 <asp:Repeater runat="server" ID="repeater" >
                        <ItemTemplate>
							<div class="item img" style="background:url('<%# Eval("uf_image").ToString()%>') no-repeat center -90px">
								<div class="bgContent">
									<div class="bg_dim">
										<p class="tit"><%# Eval("uf_title").ToString()%></p>
										<span class="bar"></span>
										<p class="con">
											<%# Eval("uf_summary").ToString()%>
										</p>
										<a href="#" ng-click="goDetail($event,<%# Eval("uf_id").ToString()%>)" class="btn_b_type2 mt20">자세히 보기</a>
									</div>
								</div>
							</div>

						</ItemTemplate>
                    </asp:Repeater>
					<!--// -->
				</div>

				<div class="btn_group" style="z-index:10">
					<div class="indi_wrap"></div>
					<button class="prev"><img src="/common/img/btn/prev_1.png" alt="previous" /></button>
					<button class="next"><img src="/common/img/btn/next_1.png" alt="next" /></button>
				</div>

			</div>
			
			<div class="ShareFunding">
                

				<div class="w980">
                    <!-- 검색 -->
					<div class="boardList_1">
						<div class="sortWrap clear2">
							<div class="sort">
								<a href="#" class="recent" ng-class="{'on':params.sort == 'new'}" ng-click="sort($event,'new')">최신순</a>
								<span class="bar"></span>
								<a href="#" class="interest" ng-class="{'on':params.sort == 'popular'}" ng-click="sort($event,'popular')">인기순</a>
								<span class="bar"></span>
								<a href="#" class="finish" ng-class="{'on':params.sort == 'deadline'}" ng-click="sort($event,'deadline')">종료임박</a>
							</div>
							<div class="fr relative">


								<label for="k" class="hidden">검색어 입력</label>
								<input type="text" id="k" class="input_search1" ng-enter="search()" ng-model="params.keyword" style="width:245px" placeholder="검색어를 입력해 주세요" />
								<a href="#" class="search_area1" ng-click="search($event)">검색</a>
							</div>
						</div>
					</div>
					<!--// -->

					<!-- 탭메뉴 -->
					<ul class="tab_type1 mb40">
						<li style="width:33%" ng-class="{'on':params.type == ''}"><a href="#" ng-click="changeType($event,'')">전체 나눔펀딩</a></li>
						<li style="width:34%" ng-class="{'on':params.type == 'child'}"><a href="#" ng-click="changeType($event,'child')">어린이 결연 펀딩</a></li>
						<li style="width:33%" ng-class="{'on':params.type == 'normal'}"><a href="#" ng-click="changeType($event,'normal')">양육을 돕는 편딩</a></li>
					</ul>
					<!--// 탭메뉴 -->
					
					<p class="result_txt">현재까지 "<em>{{total}}</em>"개의 나눔펀딩이 개설되었습니다.</p>
					<div class="sf_list">
						<ul class="clear2">
							<li ng-repeat="item in list" class="{{ ((item.uf_date_end | amDifference : today : 'days') <= 0)  ? 'end' : '' }}">
								
								<!-- 모금 중 -->
								<div class="top" ng-if="((item.uf_date_end | amDifference : today : 'days') > 0)">
									<!-- 대표이미지 사이즈 470 * 352-->
									<div class="img" style="background:url('{{item.uf_image}}') no-repeat center;"></div>
									<div class="bg_gradient"></div>

									<span class="d_day">D-{{ item.uf_date_end | amDifference : today : 'days' }}</span>
									
									<!-- graph -->
									<div class="commonGraph_wrap white">
										<span class="heart" ng-style="{left: (item.uf_current_amount > item.uf_goal_amount ? 100 : item.uf_current_amount / item.uf_goal_amount * 100) + '%'}">
											<span class="txt"><em>{{item.uf_current_amount / item.uf_goal_amount|percentage:0}}</em> <span ng-if="item.uf_current_amount < item.uf_goal_amount">달성중</span><span ng-if="item.uf_current_amount >= item.uf_goal_amount">달성</span></span>
										</span>
										<div class="graph" >
											<span class="bg"></span>
											<span class="per" ng-style="{width:item.uf_current_amount / item.uf_goal_amount * 100 + '%'}"></span>
										</div>
										<div class="sum clear2">
											<span class="ing">{{item.uf_current_amount| number:N0}}원</span>
											<span class="goal">{{item.uf_goal_amount | number:N0}}원</span>
										</div>

									</div>
									<!--// -->
								</div>

								<!-- 모금 완료 -->
								<div class="top" ng-if="((item.uf_date_end | amDifference : today : 'days') <= 0)" ng-click="goDetail($event,item)" style="cursor:pointer;">
									<!-- 대표이미지 사이즈 470 * 352-->
									<div class="img" style="background:url('{{item.uf_image}}') no-repeat center;"></div>
									<div class="bg_gradient"></div>

									<div class="txt_end">모금이 완료되었습니다</div>
									
								</div>

								<div class="bottom">
									<p class="type">{{item.uf_type_name}}</p>
									<p class="tit elps">{{item.uf_title}}</p>
									<p>
										<span class="writer">By {{item.sponsorname}}</span>
										<span class="comment">댓글 <em class="fc_blue">{{item.uf_cnt_reply}}</em></span>
									</p>
								</div>

								<!-- 오버시 레이어 -->
								<div class="over" ng-if="((item.uf_date_end | amDifference : today : 'days') > 0)">
									<p>{{item.uf_summary}}</p>
									<a href="#" class="btn_b_type2" ng-click="goDetail($event,item)">자세히 보기</a>
								</div>
								<!--// -->

								<!-- 공유버튼 -->
								<span class="sns_ani" ng-if="item.uf_current_amount < item.uf_goal_amount">
									<span class="common_sns_group">
										<span class="wrap">
											<a href="#" title="페이스북" data-role="sns" data-provider="fb" data-url="" class="sns_facebook" uf-item="{{item}}">페이스북</a>
											<a href="#" title="카카오스토리" data-role="sns" data-provider="ks" data-url="" class="sns_story" uf-item="{{item}}">카카오스토리</a>
											<a href="#" title="트위터" data-role="sns" data-provider="tw" data-url="" class="sns_twitter" uf-item="{{item}}">트위터</a>
											<a href="#" title="url 공유" data-role="sns" data-provider="copy" data-url="" class="sns-copy sns_url" uf-item="{{item}}">url 공유</a>
										</span>
									</span>
									<button class="common_sns_share">공유하기</button>
								</span>
								<!--// -->
							</li>
							
							<li class="no_content" ng-if="total == 0 && isSearch">검색된 결과가 없습니다.</li>
							<li class="no_content end" ng-if="total == 0 && !isSearch">
								<p class="s_tit5 mb20">앗!, 기존 나눔펀딩이 모두 종료되어 진행중인 나눔펀딩이 없어요.</p>
								<p class="lineH_24 mb30">새로운 나눔펀딩을 개설해 보시겠어요?<br />단계에 따라 손쉽게 생성하실 수 있습니다.</p>
								<a href="/sponsor/user-funding/create" class="btn_s_type1">새로운 나눔펀딩 만들기</a>
							</li>
						</ul>
						<button class="btn_com_more" ng-show="list.length < total" ng-click="showMore($event)"><span></span></button>
					</div>
					
					<div class="newFunding">
						<em class="fs_m fc_blue">새로운 나눔펀딩</em>을 <em class="fs_m fc_blue">개설</em>해 보시겠어요? 단계에 따라 손쉽게 생성하실 수 있습니다.
						
						<asp:LinkButton runat="server" ID="btn_create" OnClick="btn_create_Click" class="btn_s_type1 ml20">새로운 나눔펀딩 만들기</asp:LinkButton>

					</div>

				</div>

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>


</asp:Content>
