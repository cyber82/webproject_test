﻿
(function () {

	var app = angular.module("cps.page", []);
	app.controller("defaultCtrl", function ($scope, $http, popup) {

		$scope.total = -1;
		$scope.list = [];
		$scope.requesting = false;

		$scope.params = {
			page: 1,
			rowsPerPage: 3,
			campaignId: $("#campaignId").val()
		};

		$scope.getList = function (params) {

			$scope.params = $.extend($scope.params, params);

			$http.get("/api/special-funding.ashx?t=report", { params: $scope.params }).success(function (r) {
				console.log(r);

				if (r.success) {

					var list = r.data;

					if (r.data.length > 0) {
						$scope.total = r.data[0].total;
						$scope.list = $.merge($scope.list, list);
						console.log($scope.list, $scope.total);

					} else {
						$scope.total = 0;
					}

				} else {
					alert(r.message);
				}
			});
		};

		$scope.showMore = function () {
			$scope.getList({ page: $scope.params.page + 1 });
		};

		$scope.item = $.parseJSON($("#data").val());
		console.log($scope.item);

		if ($scope.item.EndDate == null || new Date() - new Date($scope.item.EndDate)) {
			$scope.getList();
		}

		$scope.showPay = function ($event) {
			$event.preventDefault();
			$scope.modal.show();
		}

		// 레이어 팝업
		$scope.modal = {
			instance: null,

			init: function () {
				// 팝업

				var payType = "";
				payType += ($scope.item.sf_is_regular) ? "Y" : "N";
				payType += ($scope.item.sf_is_temporary) ? "Y" : "N";

				popup.init($scope, "/sponsor/pop-pay/" + payType, function (modal) {
					$scope.modal.instance = modal;

					$("#frequency").val($($("input[name=pop_frequency]")[0]).val());

				});
			},

			show: function () {
				if (!$scope.modal.instance)
					return;

				setNumberOnly();

				if ($("#dsp_amount").val() != "") {
					$(".pay_amount_custom").val($("#dsp_amount").val());
					$("#amount").val($("#dsp_amount").val());

					// 금액 유효성 체크
					$(".pay_amount_custom").trigger("blur");
				}

				$scope.modal.instance.show();
			},

			request: function ($event) {
				$event.preventDefault();
				if ($("#amount").val() == "") {
					alert("금액을 선택해 주세요");
					return false;
					
				}

				if ($scope.requesting) return;
				$scope.requesting = true;

				var params = {
					campaignId: $("#campaignId").val(),
					frequency: $("#frequency").val(),
					amount: $("#amount").val().replace(/\,/g, "")
				}
				$.post("/sponsor/pay-gateway.ashx?t=go-special", params).success(function (r) {

					if (r.success) {
						location.href = r.data;
					} else {
						
						if (r.action == "nonpayment") {		// 미납금

							if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
								location.href = "/my/sponsor/pay-delay/";
							}
							
						} else {
							alert(r.message);
						}
					}
				});

			},

			close: function ($event) {
				$event.preventDefault();
				if (!$scope.modal.instance)
					return;

				$scope.modal.instance.hide();
			}

		}
		$scope.modal.init();
	});

})();