﻿
$(function () {

    //브라우저 체크 
    function browerCheck(type) {

        // 재활용을 위해 "hackerc_Agent" 에 정보를 저장.
        var hackerc_Agent = navigator.userAgent;
        var result = hackerc_Agent.match('LG | SAMSUNG | Samsung | iPhone | iPod | Android | Windows CE | BlackBerry | Symbian | Windows Phone | webOS | Opera Mini | Opera Mobi | POLARIS | IEMobile | lgtelecom | nokia | SonyEricsson');

        var pc = 'http://www.';
        var mobile = 'http://m.';

        if (type == 'pc') {
            if (result == null) {
                link = location.href.replace(mobile, pc)
                location.href = link;
                return false;
            }
        } else if (type == 'mobile') {
            if (result != null) {
                //link = location.href.replace(pc, mobile)
                //location.href = link;


                (function () {

                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'http://m.compassion.or.kr/';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'http://m.compassionkr.com/';
                    else if (loc.indexOf('localhost') != -1) host = 'http://localhost:35563/';


                    var http_us = host;// "http://m.compassion.or.kr/"; //모바일이동URL
                    var http_param = [];
                    var str_param = "";
                    if (document.location.search != "") {
                        http_param.push(document.location.search.replace(/^\?/, ""));
                    }
                    if (document.referrer != "" && !/OV_REFFER/.test(document.location.search)) {
                        http_param.push("OV_REFFER=" + document.referrer);
                    }
                    if (http_param.length > 0) {
                        str_param = (/\?/.test(http_us) ? "&" : "?") + http_param.join("&");
                    } else {
                        str_param = "";
                    }
                    location.href = http_us + str_param;
                })();

                return false;
            }
        }

    }


    //모바일로 접속시 모바일 홈페이지로 이동 
    if (getParameterByName("pc") != "ok") {
        if (cookie.get("locationMobile") != 1) {
            browerCheck('mobile');
        }
    } else {
        cookie.set("locationMobile", 1, 1);
    }


    $page.init();

});


var $page = {

    init: function () {

        $('input[type="radio"].frequency').click(function () {

            var self = $(this);
            var codeId = self.data("codeid");
            var amount = $(".amount[data-codeId='" + codeId + "']");

            //	amount.prop("readonly", self.val() == "정기");

            if (self.val() == "정기") {
                amount.val("20,000");
            } else {
                amount.val("20,000");
            }
        })

        $('a[data-action="pay"]').each(function () {

            var self = $(this);

            self.click(function () {

                var codeId = self.data("codeid");
                var campaignId = self.data("campaignid");

                var frequency = $(".frequency[data-codeId='" + codeId + "']:checked").val();
                var amount = $(".amount[data-codeId='" + codeId + "']").val().replace(/\,/g, "");

                if (amount < 1000) {
                    alert("후원금은 천원 이상 입력 가능합니다.");
                    return;
                }

                if (amount % 1000 > 0) {
                    alert("죄송합니다.\n천원단위로 후원 가능합니다.");
                    return;
                }

                $.post("/sponsor/pay-gateway.ashx?t=go-special", { campaignId: campaignId, frequency: frequency, amount: amount }).success(function (r) {

                    if (r.success) {
                        location.href = r.data;
                    } else {
                        if (r.action == "nonpayment") {		// 미납금

                            if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
                                location.href = "/my/sponsor/pay-delay/";
                            }

                        } else {
                            alert(r.message);
                        }
                    }
                });


                return false;
            })

        })

        this.setMainVisual();


    },

    setMainVisual: function () {

        var root = $(".visual_special");

        // 메인비주얼
        if (root.length < 1) {
            root.find(".btn_group").hide();
            return;
        }

        if (root.find(".slide_wrap").find(".item").length < 2) {
            root.find(".btn_group").hide();
            return;
        }

        $("#special_funding_visual").slidesjs({
            width: $(window).width(),
            height: 750,

            play: {

                active: true,
                effect: "slide",
                interval: 5000,
                auto: true,
                swap: true,
                pauseOnHover: false,
                restartDelay: 2500
            },

            effect: {
                slide: {
                    speed: 1000
                },
                fade: {
                    speed: 300,
                    crossfade: true
                }
            },

            callback: {


                loaded: function () {
                },
                start: function (number) {

                },
                complete: function (number) {
                    root.find(".indi_wrap button").removeClass("on");
                    $(root.find(".indi_wrap button")[number - 1]).addClass("on")
                }
            }

        });

        $.each(root.find(".slide_wrap").find(".item"), function (i) {
            var item = $('<button' + (i == 0 ? ' class="on"' : '') + ' type="button">O</button>');
            item.click(function () {
                root.find('a[data-slidesjs-item=' + i + ']').trigger("click");		// 이동
            })
            root.find(".indi_wrap").append(item);
        })

        var btn_pause = $('<button type="button" class="pause">pause</button>');
        var btn_play = $('<button type="button" class="play">play</button>');
        root.find(".indi_wrap").append(btn_pause);
        root.find(".indi_wrap").append(btn_play);

        btn_pause.click(function () {
            root.find(".slidesjs-stop").trigger("click");
            return false;
        });

        btn_play.click(function () {
            root.find(".slidesjs-play").trigger("click");
            return false;
        });

        root.find(".prev").click(function () {
            root.find(".slidesjs-previous").trigger("click");
            return false;
        });

        root.find(".next").click(function () {
            root.find(".slidesjs-next").trigger("click");
            return false;
        });



        root.mouseenter(function () {
            btn_pause.trigger("click");
        }).mouseleave(function () {
        })
    }

};


(function () {
    var app = angular.module('cps.page', []);
    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

        // 완료된 캠페인보기
        $scope.showCompleteCampaign = function ($event) {
            $event.preventDefault();

            $scope.modalComplete.show();
        }

        // 어린이선물
        $scope.modalComplete = {

            instance: null,
            list: null,
            total: -1,
            params: {
                page: 1,
                rowsPerPage: 10
            },

            show: function () {

                $scope.modalComplete.item = $.extend({}, $scope.child);
                popup.init($scope, "/sponsor/special/pop-complete-campaign?v=1", function (modal) {
                    $scope.modalComplete.instance = modal;

                    modal.show();

                    $scope.modalComplete.getList();
                    //	setTimeout(function () {

                    //	}, 500);


                }, { top: 0, iscroll: true, removeWhenClose: true });


            },

            hide: function ($event) {
                $event.preventDefault();
                if (!$scope.modalComplete.instance)
                    return;
                $scope.modalComplete.instance.hide();

            },

            goView: function ($event, item) {
                $event.preventDefault();
                location.href = "/sponsor/special/view/" + item.campaignid;
            },

            getList: function (params) {
                $scope.modalComplete.params = $.extend($scope.modalComplete.params, params);

                $http.get("/api/special-funding.ashx?t=complete-list", { params: $scope.modalComplete.params }).success(function (r) {
                    console.log(r);
                    if (r.success) {

                        var list = r.data;
                        $.each(list, function () {
                            this.startdate = new Date(this.startdate);
                            this.enddate = new Date(this.enddate);
                        });

                        $scope.modalComplete.list = r.data;
                        $scope.modalComplete.total = r.data.length > 0 ? r.data[0].total : 0;

                    } else {
                        alert(r.message);
                    }

                });

            }

        }

    });

})();
