﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="sponsor_special_view" MasterPageFile="~/main.Master"  %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/common/js/site/moveToMobile.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
	<script type="text/javascript" src="/sponsor/special/view.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="campaignId" value="" />
	<input type="hidden" runat="server" id="frequency" value="" />
	<input type="hidden" runat="server" id="amount" value="" />
	<input type="hidden" runat="server" id="data" value="" />

	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em><%--<%:entity.CampaignName %>--%>특별한 나눔</em></h1>
				<span class="desc">주제별 다양한 나눔을 선택하여 후원하실 수 있습니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 sponsor">

			<!-- 비주얼 -->
			<div class="bgContent sf_cam_visual" style="background:url('<%:entity.sf_image.WithFileServerHost()%>') no-repeat center; background-size:cover !important;">

				<div class="bg_dim">
					<div class="w980">
						<p class="tit"><%:entity.CampaignName %></p>
						<span class="bar"></span>
						<p class="con"><%:entity.sf_summary %></p>

					</div>
				</div>

			</div>
			<!--// 비주얼 -->

			<!-- 그래프영역 -->
			<div class="sf_cam_graph">
				<div class="w980 clear2 relative">

					<div class="graphArea" >
						<!-- graph -->
						<div class="commonGraph_wrap white" style="display:<%:percent < 0 ? "none" : "block"%>">
							<span class="heart" style="left:<%:percent %>%">
								<span class="txt day"><em><%:percent %>%</em> <%:percent >= 100 || remain_days < 0 ? "달성" : "달성중" %><asp:Literal runat="server" ID="days" /></span>
							</span>
							<div class="graph">
								<span class="bg"></span>
								<span class="per" style="width:<%:percent %>%"></span>
							</div>
							<div class="sum clear2">
								<span class="ing"><%:entity.sf_current_amount.ToString("N0") %>원</span>
								<span class="goal"><%:entity.sf_goal_amount.ToString("N0") %>원</span>
							</div>

						</div>
						<!--// -->
					</div>

					<script type="text/javascript">
						$(function () {
							// 바로후원하기, sns공유버튼 스크롤 fixed
							$(window).scroll(function () {
								if ($(this).scrollTop() > 900) {
									$(".btn_dir_sponsor").addClass("fixed");
								} else {
									$(".btn_dir_sponsor").removeClass("fixed");
								}
							});
						})
					</script>

					<div class="amountWrap">
					<asp:PlaceHolder runat="server" ID="ph_amount">
						<div class="inblock clear2">
							
							<span class="amount">
								<label for="dsp_amount" class="hidden">금액 입력</label>
								<input type="text" id="dsp_amount" value="20,000" class="input_amount number_only use_digit" maxlength="10" />
								<span class="won">원</span>
							</span>
							
							
							<span class="btn_dir_sponsor">
								<a href="#" class="btn_b_type2" ng-click="showPay($event)" >바로 후원하기</a>

								<!-- 공유하기버튼 -->
								<span class="sns_ani down">
									<button class="common_sns_share">공유하기</button>
									<span class="common_sns_group">
										<span class="wrap">
											<a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
											<a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
											<a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
											<a href="#" title="url 공유" data-role="sns" data-provider="copy" class="sns-copy sns_url">url 공유</a>
										</span>
									</span>
								</span>
								<!--// -->
							</span>
							
						</div>
					</asp:PlaceHolder>

						<!-- 완료되었을 때 -->
						
						<div class="complete_comment" id="complete_comment" runat="server" >
							모금이 완료되었습니다.
						</div>
					
						<!--//  -->

					</div>



				</div>
			</div>
			<!--// 참여인원 -->
			
			<!-- 관리자등록 컨텐츠 -->
			<div class="sf_cam_detail">

				<div class="content">
					<asp:Literal runat="server" ID="content" />

					
					<br />
                    <!--
					<img src="" runat="server" id="sf_image" />
                    -->
				</div>

				<div class="w980">

					<div class="report" ng-if="total > 0">
						<p class="tit">후원 결과 레포트</p>
						
						<div class="tableWrap1">
							<table class="tbl_type1 mb40">
								<caption>자료 다운로드 테이블</caption>
								<colgroup>
									<col style="width:83%" />
									<col style="width:17%" />
								</colgroup>
								<tbody>
									<tr ng-repeat="item in list">
										<td>{{item.sr_title}}</td>
										<td><a ng-href="{{item.sr_file}}" class="btn_s_type4"><span class="ic_down"></span>Download</a></td>
									</tr>

									<!-- 등록된 레포트가 없을때 -->
									<tr ng-if="total == 0">
										<td colspan="2" class="no_content">등록된 레포트가 없습니다.</td>
									</tr>
									<!--//  -->
								</tbody>
							</table>
						</div>

						<button class="btn_com_more" ng-click="showMore()" ng-show="total > list.length">더보기</button>

					</div>
				</div>
			</div>
			<!-- 관리자등록 컨텐츠 -->
			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->

		
    </section>



</asp:Content>
