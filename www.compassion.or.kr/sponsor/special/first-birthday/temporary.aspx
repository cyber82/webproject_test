﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="temporary.aspx.cs" Inherits="sponsor_special_first_birthday_temporary" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/sponsor/pay/temporary/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/sponsor/pay/temporary/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/sponsor/pay/temporary/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
	<script type="text/javascript" src="/sponsor/pay/temporary/default.js"></script>
	<script type="text/javascript" src="/sponsor/special/first-birthday/temporary.js?v=1"></script>
	
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>

	<script type="text/javascript">
	    $(function () {

	        if ($("#map").length > 0) {
	            appendGoogleMapApi();

	            $(".btn_loc").click(function () {
	                var num = $(this).index() + 1;

	                $(".btn_loc").removeClass("on");
	                $(this).addClass("on");

	                $(".location .loc1").hide();
	                $(".location .loc2").hide();
	                $(".location .loc" + num).show();

	                if (num == 1) {
	                    setPosition(37.537970, 127.005080);
	                } else {
	                    setPosition(35.116564, 129.040821);
	                }
	            })

	        }

	    });

	    function appendGoogleMapApi() {
	        if (typeof google === 'object' && typeof google.maps === 'object') {
	            initMap();
	        } else {
	            var script = document.createElement("script");
	            script.type = "text/javascript";
	            script.src = "https://maps.googleapis.com/maps/api/js?language=ko&region=kr&key=<%:this.ViewState["googleMapApiKey"].ToString() %>&callback=initMap";
				document.body.appendChild(script);
            }
        }

        function setPosition(lat, lng) {
            map.setCenter(new google.maps.LatLng(lat, lng));

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: "/common/img/icon/pin.png",
                map: map
            });

        }

        function initMap() {

            lat = 37.537970;
            lng = 127.005080;

            map = new google.maps.Map(document.getElementById('map'), {
                center: { lat: lat, lng: lng },
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: "/common/img/icon/pin.png",
                map: map
            });


        };


	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" runat="server" id="gubun" value="" />
    <input type="hidden" runat="server" id="bluedog" value="" />
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />
	<input type="hidden" runat="server" id="baby_gender" value="M" />
	<input type="hidden" runat="server" id="visit_region" value="S" />
	
	<input type="hidden" id="gender" runat="server"  />
	<input type="hidden" id="hd_auth_domain" runat="server"  />
	<input type="hidden" id="hd_age" runat="server"  />

	<input type="hidden" id="amount" runat="server"  />
	<input type="hidden" runat="server" id="jumin" value="" />		
	<input type="hidden" runat="server" id="ci" value="" />		<!-- 본인인증 CI -->
	<input type="hidden" runat="server" id="di" value="" />		<!-- 본인인증 DI -->
	<input type="hidden" runat="server" id="cert_gb" value="" />		<!-- 본인인증 수단 -->
	
	<input type="hidden" runat="server" id="user_class" value="20세이상" />
	<input type="hidden" runat="server" id="hdState" value="" />
	<input type="hidden" runat="server" id="hdLocation" value="" />
	<input type="hidden" runat="server" id="hdManageType" value="" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />		<!-- 회원 기본주소와 동기화 하는 기능이 있는경우 hfAddressType 가 필요함 -->
	<input type="hidden" runat="server" id="hdCI" value="" />	<!-- 기존 등록된 본인인증 CI -->
	
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />

	<div style="display:none">
	<input type="checkbox" runat="server" id="addr_overseas" />해외에 거주하고 있어요
	<input type="text" runat="server" id="user_name" />		<!-- 결제시 사용 -->
	</div>

	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>첫 생일 <em>첫 나눔</em></h1>
				<span class="desc">우리 아기의 첫 나눔이 또 다른 귀한 생명의 희망이 됩니다</span>

				<uc:breadcrumb runat="server"/>

			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents sponsor">

			<div class="special payment">

				<div class="w980">

					<!-- visual -->
					<div class="bgContent birth_online">

						<p class="tit">
							<span>태아·영아생존 일시후원 (<%=this.ViewState["bluedog"].ToString() == "Y" ? "30" : "10" %>만원 이상)</span><br />
							태국 '보 깨오 어린이센터' 엄마와 아기 살리기
						</p>
						<span class="bar"></span>
						<p class="con">
							태국 보 깨오 어린이 센터에 있는 엄마와 아기의 생명을 살리고 건강하게 양육합니다.<br />
							가장 연약한 존재인 아기들에게 소중한 생명을 선물해주세요.<br />
							<span>
								<span class="field">위치</span><span class="mr60">태국 보 깨오 어린이센터</span>
								<span class="field">수혜자</span><span>엄마와 아기</span>
							</span>
						</p>
						
					</div>
					<!--// visual -->
					
					<!-- 입력테이블 -->
					<div class="table_tit" style="height:30px;">
						<span class="nec_info">표시는 필수입력 사항입니다.</span>
					</div>

					<script type="text/javascript">
					    $(function () {
					        $(".tab_select button").click(function () {
					            $(this).parent().find("button").removeClass("on");
					            $(this).addClass("on");
					        });
					    })
					</script>

					<div class="tableWrap2 mb40">
						<table class="tbl_type1">
							<caption>신청정보 입력 테이블</caption>
							<colgroup>
								<col style="width:20%" />
								<col style="width:80%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><span class="nec">우리아기 정보</span></th>
									<td>
										<div class="tab_type3 tab_select mb10" style="width:400px">
											<button style="width:200px" id="btn_babyGender_M" class="btn_babyGender"><span>아들</span></button>
											<button style="width:200px" id="btn_babyGender_F" class="btn_babyGender"><span>딸</span></button>
										</div>

										<label for="txtBabyName" class="hidden">아기이름 입력</label>
										<asp:TextBox ID="txtBabyName" runat="server" MaxLength="25" placeholder="아기이름" cssclass="input_type2 mb10" style="width:400px"></asp:TextBox>

										
										<div class="clear2 mb10">
											<span class="sel_type2 fl mr10" style="width:180px;">
												<label for="baby_birth_yyyy" class="hidden">아기생년 선택</label>
												 <asp:DropDownList cssclass="custom_sel"  runat="server" ID="baby_birth_yyyy"></asp:DropDownList>
											</span>

											<span class="sel_type2 fl mr10" style="width:100px;">
												<label for="baby_birth_mm" class="hidden">월 선택</label>
												<asp:DropDownList cssclass="custom_sel"  runat="server" ID="baby_birth_mm"></asp:DropDownList>
											</span>

											<span class="sel_type2 fl" style="width:100px;">
												<label for="baby_birth_dd" class="hidden">일 선택</label>
												<asp:DropDownList cssclass="custom_sel"  runat="server" ID="baby_birth_dd"></asp:DropDownList>
											</span>
										</div>

										<asp:PlaceHolder runat="server" ID="ph_v1">
										<div class="btn_attach clear2 relative">
											<input type="text" runat="server" id="lb_file_path" disabled class="input_type1 fl mr10" style="width:400px;background:#fdfdfd;border:1px solid #d8d8d8;" />
											<a href="#" class="btn_type8 fl" id="btn_file_path"><span>파일선택</span></a>
										</div>
										<p class="pt20">
											<span class="fc_black">사진 첨부 시 주의사항</span><br />
											<span class="s_con1">세로 사진을 올려주세요. 세로 사진이 기념증서에 알맞게 제작됩니다.</span><br />
											<span class="s_con1">400KB 이상의 사진이 예쁘게 제작됩니다. 핸드폰 사진은 다소 해상도, 색상이 떨어질 수 있습니다.</span>
											<span class="s_con1">첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</span>
										</p>
										</asp:PlaceHolder>

									</td>
								</tr>
								<tr>
									<th scope="row"><span class="nec">신청자 정보</span></th>
									<td>
										<label for="txtSponsorName" class="hidden">신청자이름 입력</label>
										<asp:TextBox ID="txtSponsorName" runat="server" MaxLength="25" placeholder="이름" cssclass="input_type2 mb10" style="width:400px"></asp:TextBox>

										<label for="txtBabyRelation" class="hidden">아기와의 관계 입력</label>
										<asp:TextBox ID="txtBabyRelation" runat="server" MaxLength="25" placeholder="아기와의 관계" cssclass="input_type2 mb10" style="width:400px"></asp:TextBox>
										
										<div class="clear2 relative">
										<label for="txtPhone" class="hidden">휴대폰번호 입력</label>
										<asp:TextBox ID="txtPhone" runat="server" MaxLength="10" placeholder="휴대폰번호(-없이 입력)" cssclass="input_type2 mb10 number_only" style="width:200px"></asp:TextBox>
										</div>
										<div class="clear2 relative">
										<label for="txtEmail" class="hidden">이메일 입력</label>
										<asp:TextBox ID="txtEmail" runat="server" MaxLength="1000" placeholder="이메일" cssclass="input_type2 mb10" style="width:400px"></asp:TextBox>
										</div>

									</td>
								</tr>

								<asp:PlaceHolder runat="server" ID="ph_gubun_V" Visible="false">
								<!--tr class="noline">
									<th scope="row"><span class="nec">방문장소</span></th>
									<td>
										<div class="tab_type3 tab_select mb10" style="width:400px">
											<button style="width:200px" class="btn_region" runat="server" id="btn_region_S" enableviewstate="true" onserverclick="btn_region_ServerClick"><span>서울 본사</span></button>
											<button style="width:200px" class="btn_region" runat="server" id="btn_region_B" enableviewstate="true" onserverclick="btn_region_ServerClick"><span>부산 지사</span></button>
										</div>
									</td>
								</tr-->
								<tr>
									<th scope="row"><span class="nec">방문 일자</span></th>
									<td>
										<div class="clear2 mb10">
											<span class="sel_type2 fl" style="width:400px;">
												<label for="ddlVisitSelect" class="hidden">방문일자 선택</label>
												<asp:DropDownList ID="ddlVisitSelect" cssclass="custom_sel" runat="server"></asp:DropDownList>
											</span>

											<!--span class="fr">
												<button class="btn_s_type4 btn_loc mr5 on"><span>서울 본사 위치보기</span></button>
												<button class="btn_s_type4 btn_loc"><span>부산 지사 위치보기</span></button>
											</span-->
										</div>

										<!-- 서울 본사 -->
										<div class="location">
											<div class="map" id="map"></div>
											<p class="pt10 tar"><span class="guide_comment2">주차장이 협소하니, 가급적 대중교통을 이용해 주시기 바랍니다.</span></p>

											<div class="loc1">
												<p class="addr">
													<span class="tit">주소</span><br />
													<span class="con">서울시 용산구 한남대로 102-5 석진빌딩</span>
												</p>

												<p class="traffic">
													<span class="tit">오시는 길</span><br />
													<span class="con"><em>지하철</em>6호선 한강진역 2번 출구, 도보 10분~15분/중앙선 한남역 1번 출구, 도보 10분~15분</span><br />
													<span class="con"><em>버스</em>한남동 '순천향대학병원' 정류장</span><br />
													<span class="con">
														<span>[간선/지선]</span><br />
														<span class="mb15">110A,110B,140,142,144,400,402, 405,407,408,420,471,472,3011,6211,N13,N37</span><br />

														<span>[직행/급행]</span><br />
														<span>1005-1,1005-2,1150,1500,5000,5005,5007,5500,5500-1,5500-2,6030,7900,8100,8130,8150,8200,8800,9000,9001,9003,9007,9300,9401,9401B,9409</span>
													</span>
												</p>
											</div>

											<div class="loc2" style="display:none">
												<p class="addr">
													<span class="tit">주소</span><br />
													<span class="con">부산시 동구 중앙대로 216 교원빌딩 17층</span>
												</p>

												<p class="traffic">
													<span class="tit">오시는 길</span><br />
													<span class="con"><em>지하철</em>1호선 부산역 10번 출구 초량동 방향으로 도보 5분~10분</span><br />
													<span class="con"><em>버스</em>초량동 '부산역' 정류장</span><br />
													<span class="con">
														<span>[일반]</span><br />
														<span class="mb15">2,13,17,26,27,39,40,41,43,48,61,67,81,82,85,87,88,101,103,139,167,190,508</span><br />

														<span>[급행]</span><br />
														<span>1000,1001,1003,1004</span>
													</span>
												</p>

											</div>
										</div>
										<!--// -->

									

									</td>
								</tr>
								</asp:PlaceHolder>

								<asp:PlaceHolder runat="server" ID="ph_gubun_P" Visible="true">
								<tr>
									<th scope="row"><span class="nec"><%=gubun.Value == "P" ? "배송지 정보" : "기념증서 배송지 정보" %></span></th>
									<td>
										<input type="hidden" id="addr_domestic_zipcode" runat="server" />
										<input type="hidden" id="addr_domestic_addr1" runat="server" /> 
										<input type="hidden" id="addr_domestic_addr2" runat="server" />	


										<label for="post" class="hidden">주소찾기</label>
										<input type="text" id="post" disabled class="input_type2 mr10" placeholder="주소" style="width:483px; background:#fdfdfd;border:1px solid #d8d8d8;" />
										<a href="#" class="btn_s_type1 " id="btn_find_addr"><span>주소찾기</span></a>
										
										
										<p id="addr_road" class="fs14 mt15"></p>
										<p id="addr_jibun" class="fs14 mt10"></p>

									</td>
								</tr>
								</asp:PlaceHolder>
								<tr>
									<th scope="row"><span class="nec">후원금액</span></th>
									<td>
										<div class="relative">
                                            <% if (this.ViewState["bluedog"].ToString() == "Y") { %>
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="300000"><span>300,000원</span></button>
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="400000"><span>400,000원</span></button>
                                            <% } else { %>
                                            <button class="btn_s_type4 btn_amount pay_amount" data-amount="100000"><span>100,000원</span></button>
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="300000"><span>300,000원</span></button>
                                            <% } %>
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="500000"><span>500,000원</span></button><br />
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="1000000"><span>1,000,000원</span></button>
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="2000000"><span>2,000,000원</span></button>
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="3650000"><span>3,650,000원</span></button><br />

											<label for="pay_amount_custom" class="hidden">후원금액 직접입력</label>

											<input type="text" id="pay_amount_custom" class="pay_amount_custom number_only input_type2" maxlength="8" value="" placeholder="직접입력(<%=this.ViewState["bluedog"].ToString() == "Y" ? "30" : "10" %>만원이상 입력 가능)" style="width:400px" />
											<p class="pt10"><span class="guide_comment2 pay_coment" style="display:none;">'첫 생일 첫 나눔'을 통한 후원금은 <%=this.ViewState["bluedog"].ToString() == "Y" ? "30" : "10" %>만원 이상 입력 가능합니다.</span></p>

											<div class="amount_sum">총 결제 금액<em id="txt_amount">0</em>원</div>
										</div>
									</td>
								</tr>
								<tr class="hide_overseas">
									<th scope="row"><span class="nec">본인인증</span></th>
									<td>
										<asp:PlaceHolder runat="server" ID="ph_no_jumin" Visible="false">
										<p class="mb10">
											<span class="checkbox_ui">
												<input type="checkbox" class="css_checkbox"  id="p_receipt_pub_ok" name="p_receipt_pub" runat="server"  checked />
												<label for="p_receipt_pub_ok" class="css_label font2">국세청 연말정산 영수증 신청</label>
											</span>
										</p>

										<div id="func_name_check">
											<label class="hidden">주민등록번호입력</label>
											<input type="text" class="input_type2 mb10 number_only" value="" data-id="jumin1" runat="server" id="jumin1" maxlength="13" placeholder="주민등록번호(-없이 입력)" style="width:400px" />
											<button class="btn_s_type7 ml5 mb10" id="btn_name_check"><span>실명인증</span></button>
										</div>

										<p id="msg_name_check" style="display:none"><span class="guide_comment1"></span></p>
										</asp:PlaceHolder>
										<asp:PlaceHolder runat="server" ID="ph_yes_jumin" Visible="false">
										<div class="receipt_infoBox">
											<p class="txt1">기존에 등록하신 <em class="fc_blue">국세청 연말정산 영수증 신청 정보</em>가 있습니다.</p>
											<p>기존 정보에 연말정산 정보가 추가로 등록됩니다.</p>
										</div>
										</asp:PlaceHolder>

										<asp:PlaceHolder runat="server" ID="ph_cert" Visible="false" >
										<p class="fs14 mb20 func_cert">온/오프라인 기존 후원정보 확인을 위해 본인 인증을 진행해 주세요.</p>

										<!-- 휴대폰,아이핀인증 -->
										<div class="cert clear2 mb20 func_cert">
											<div class="fl">
												<div class="box phone">
													<p class="tit">휴대폰 인증</p>
													<p class="con">
														본인 명의의 휴대전화번호로 본인인증을<br />
														원하시는 분은 아래 버튼을 클릭해주세요
													</p>

													<a href="#" id="btn_cert_by_phone" class="btn_s_type2">휴대폰 인증하기</a>
												</div>
											</div>

											<div class="fr">
												<div class="box ipin">
													<p class="tit">아이핀(i-PIN) 인증</p>
													<p class="con">
														가상 주민등록번호 아이핀으로 본인인증을<br />
														원하시는 분은 아래 버튼을 클릭해주세요
													</p>

													<a href="#" id="btn_cert_by_ipin" class="btn_s_type2">아이핀 인증하기</a>
												</div>
											</div>
										</div>

										<p id="msg_cert" style="display:none"><span class="guide_comment1">정보 확인이 완료되었습니다.</span></p>
										
										<!--// 휴대폰,아이핀인증 -->
										

										<ul class="func_cert">
											<li><span class="s_con1">본인명의의 휴대폰으로만 본인인증이 가능합니다.</span></li>
											<li><span class="s_con1">아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다.</span></li>
										</ul>
										</asp:PlaceHolder>
									</td>
								</tr>
								<tr>
									<th scope="row"><span class="nec">결제방법</span></th>
									<td>
										<span class="radio_ui mb15">
											<input type="radio" id="payment_method_card" runat="server" name="payment_method"  class="css_radio" checked />
											<label for="payment_method_card" class="css_label" style="width:170px">신용카드 결제</label>

											<input type="radio" id="payment_method_cms" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_cms" class="css_label" style="width:170px">실시간 계좌이체</label>

											<input type="radio" id="payment_method_oversea" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_oversea" class="css_label" style="width:170px">해외발급 카드</label>
										</span>
										<span class="radio_ui">
											<input type="radio" id="payment_method_kakao" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_kakao" class="css_label relative" style="width:170px"><img src="/common/img/icon/kakaopay.jpg" class="kakao" alt="kakao pay" onclick="$('#payment_method_kakao').trigger('click')"/>kakaopay</label>

											<input type="radio" id="payment_method_payco" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_payco" class="css_label relative" style="width:170px"><img src="/common/img/icon/payco.jpg" class="payco" alt="payco" onclick="$('#payment_method_payco').trigger('click')"/>payco</label>

											<input type="radio" id="payment_method_phone" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_phone" class="css_label" style="width:170px">휴대폰 결제</label>
										</span>
									</td>
								</tr>
								
							</tbody>
						</table>

					</div>
					<!--// 입력테이블 -->
					
					<div class="box_type4 padding1 mb40">
						<ul>
							<li><span class="s_con1">후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</span></li>
							<li><span class="s_con1">결제관련 문의 : <em class="fc_blue">후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_blue">info@compassion.or.kr</a></em></span></li>
						</ul>
					</div>
					
					<div class="tac"><asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" cssclass="btn_type1">신청 및 결제</asp:LinkButton></div>

				</div>

			</div>
			<div class="h100"></div>
			
		</div>	
		<!--// e: sub contents -->

		
    </section>


</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="payco_form" />
	<uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kakaopay_form" />
</asp:Content>