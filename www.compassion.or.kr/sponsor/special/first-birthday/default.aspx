﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="sponsor_special_first_birthday_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	

    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/info.js"></script>
    <script type="text/javascript">
        function fnCheckLogin() {
            if ('<%=UserInfo.IsLogin%>'.toLowerCase() != 'true') {
                alert('로그인 후 신청이 가능합니다.');
            }
        }
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<style type="text/css">
    .campaign_info .camp {position:relative ;}
    .campaign_info .camp .logo {position:absolute ; right:30px ; bottom:30px ;}
    .campaign_info .camp .logo img {display:inline-block ; margin-left:10px ;}
    .campaign_info .camp .tit em {color:#2F2D53 ;}
    </style>
	<section class="sub_body">
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>첫 생일 <em>첫 나눔</em></h1>
				<span class="desc">우리 아기의 첫 나눔이 또 다른 귀한 생명의 희망이 됩니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 sponsor">

			<div class="special first">
				
				<!-- visual -->
				<div class="bgContent birth">

					<div class="w980 relative">
						<p class="tit"><span>Happy 1<span style="vertical-align:super;font-size:17px;">st</span> Birthday</span><br />첫 생일을 맞아 시작하는 첫 나눔</p>
						<span class="bar"></span>
						<p class="con">일 년간 건강하게 자라준 아기의 첫 생일을 축하합니다!<br />첫 생일 첫 나눔을 통해 더 많은 생명을 살리는 일에 동참해주세요!</p>

						<!-- 공유하기버튼 -->
						<span class="sns_ani">
							<span class="common_sns_group">
								<span class="wrap">
									<a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
                                    <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
                                    <a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
                                    <a href="#" title="url 공유" data-role="sns" data-provider="copy" class="sns_url">url 공유</a>
								</span>
							</span>
							<button class="common_sns_share">공유하기</button>
						</span>
						<!--// -->
					</div>

				</div>
				<!--// visual -->

				<!-- 박스정보 -->
				<div class="campaign_info">
					<div class="w980">

						<div class="clear2">
                            <div class="camp fl">
								<p class="tit">첫 생일 첫 나눔</p>

								<p class="con">
									<span class="fc_blue">컴패션과 함께 첫 나눔을 시작해보세요.</span><br />
									사옥 방문이나 온라인을 통해 아기의 첫 생일을 기념하세요.<br />컴패션이 준비한 아기의 사진이 담긴 첫 나눔 기념증서를 드립니다.
								</p>
								<a href="/sponsor/special/first-birthday/temporary/?g=V" class="btn_s_type1" onclick="fnCheckLogin()">컴패션 방문</a>
                                <a href="/sponsor/special/first-birthday/temporary/?g=P" class="btn_s_type1" onclick="fnCheckLogin()">온라인 나눔</a>
							</div>

							<div class="camp fr">
								<p class="tit">첫 생일 첫 나눔 | <em>BLUEDOG</em></p>

								<p class="con">
									<span class="fc_blue">컴패션 첫 생일 첫 나눔, 블루독과 함께 해보세요.</span><br />
									컴패션과 블루독이 함께 준비한 특별한 선물과 기념증서를 드립니다.<br />
									자세한 안내는 페이지 하단에서 확인하세요.
								</p>
								<a href="/sponsor/special/first-birthday/temporary/?g=V&bluedog=Y" class="btn_s_type1" onclick="fnCheckLogin()">컴패션 방문</a>
                                <a href="/sponsor/special/first-birthday/temporary/?g=P&bluedog=Y" class="btn_s_type1" onclick="fnCheckLogin()">온라인 나눔</a>
                                <p class="logo"><img src="/common/img/page/sponsor/compassion_logo.png" alt="compassion" />  <img src="/common/img/page/sponsor/bluedog_logo.png" alt="bluedog" /></p>
							</div>
						</div>

						<p class="guide">* 본 캠페인은 국내 회원만 신청 가능합니다. 외국에서 후원을 원하시는 분은 ‘1:1 문의’를 통해 문의해주세요.</p>
                        <p class="guide" style="padding-top:10px;">* 방문은 매주 금요일 오후에 진행됩니다.</p>
					</div>
				</div>
				<!--// 박스정보 -->

				<!-- 함께합니다 -->
				<div class="together">
					<div class="w980">

						<div class="sub_tit mb40">
							<h2 class="tit"><em>블루독에서 첫 생일 첫 나눔과 함께 합니다!</em></h2>
							<p>컴패션 후원기업 블루독에서 아기를 위한 특별한 선물을 드립니다.</p>
						</div>

						<ul class="present clear2">
							<li><span class="s_con7"><em class="fc_blue">기념증서</em> : 아기의 사진이 담긴 기념 증서</span></li>
							<li><span class="s_con7"><em class="fc_blue">사진촬영</em> : 방문 시, 돌상 앞에서 사진 촬영</span></li>
							<li><span class="s_con7"><em class="fc_blue">후원금 사용처</em> : 후원금 사용처 소개서</span></li>
							<li><span class="s_con7"><em class="fc_blue">기부금 영수증</em> : 후원금 결제 시 로그인된 사용자 명의로 기부금 영수증 발급</span></li>
						</ul>

						<div class="mb20"><img src="/common/img/page/sponsor/first_birth_01.jpg" alt="기념증서_사업성과보고서용, 애플비사진, 첫생일 첫나눔_후원금사용처" /></div>
						<p>* 블루독에서 준비한 선물은 컴패션 ‘첫 생일 첫 나눔’ 일반선물과 중복되지 않습니다.</p>
					</div>
				</div>
				<!--// 함께합니다 -->
                <!-- 함께합니다 -->
				<div class="together">
					<div class="w980">

						<div class="sub_tit mb40">
							<h2 class="tit"><em>컴패션에서 소중한 첫 나눔을 함께 하겠습니다!</em></h2>
							<p>아기의 첫 나눔을 오랫동안 기억할 수 있는 선물을 드립니다.</p>
						</div>

						<ul class="present clear2">
							<li><span class="s_con7"><em class="fc_blue">기념증서</em> : 아기의 사진이 담긴 기념 증서</span></li>
							<li><span class="s_con7"><em class="fc_blue">사진촬영</em> : 방문 시, 돌상 앞에서 사진 촬영</span></li>
							<li><span class="s_con7"><em class="fc_blue">후원금 사용처</em> : 후원금 사용처 소개서</span></li>
							<li><span class="s_con7"><em class="fc_blue">기부금 영수증</em> : 후원금 결제 시 로그인된 사용자 명의로 기부금 영수증 발급</span></li>
						</ul>

						<div class="mb20"><img src="/common/img/page/sponsor/first_birth02_1.jpg" alt="기념증서_사업성과보고서용, 애플비사진, 첫생일 첫나눔_후원금사용처" /></div>
						<p>* 컴패션에서 준비한 선물은 ‘블루독과 함께하는 첫 생일 첫 나눔’ 선물과 중복되지 않습니다.</p>
					</div>
				</div>
				<!--// 함께합니다 -->
				<!-- 후원금 사용 -->
				<div class="use">
					<div class="w980">

						<div class="sub_tit mb40">
							<h2 class="tit"><em>후원금은 이렇게 사용됩니다!</em></h2>
							<p>후원금은 태국 '보 깨오 어린이센터' 엄마와 아기들의 태아&middot;영아생존 후원에 사용됩니다.</p>
						</div>

						<a target="_blank" href="http://www.srook.net/compassion/635974665239422185" class="btn_type1">컴패션어린이센터 소개 보기</a>
					</div>
				</div>
				<!--// 후원금 사용 -->

			</div>
			<div class="h100"></div>
			
		</div>	
		<!--// e: sub contents -->

		
    </section>

</asp:Content>

