﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_special_first_birthday_temporary : SponsorPayBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}


	protected override void OnBeforePostBack() {
        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;
        //Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        //Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        
        //김태형 2017.06.01
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else
        {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }


        this.ViewState["googleMapApiKey"] = ConfigurationManager.AppSettings["googleMapApiKey"];
		gubun.Value = Request["g"].ValueIfNull("P");    // P : 온라인신청 , V : 방문신청
        bluedog.Value = Request["bluedog"].ValueIfNull("N");
        this.ViewState["bluedog"] = bluedog.Value;

        if (gubun.Value == "P") {
			ph_gubun_P.Visible = true;
		} else {
			ph_v1.Visible = false;
			ph_gubun_V.Visible = true;
			this.ShowVisitDate();
		}
		
		upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_cpsdol);

		var payInfo = new PayItemSession.Entity() { type = PayItemSession.Entity.enumType.CSP_DOL, campaignId = "20131106162453202", frequency = "일시", group = "CSP", codeId = "CSPF", codeName = "첫생일첫나눔", amount = 0 };
		PayItemSession.SetCookie(this.Context, payInfo);
		var data = new PayItemSession.Store().Create(payInfo);
		if(data == null) {
			base.AlertWithJavascript("후원정보 임시저장에 실패했습니다.", "goBack()");
			return;
		}
		this.ViewState["payItem"] = data.ToJson();

		UserInfo sess = new UserInfo();
		
		base.OnBeforePostBackTemporary(payInfo);
		
		// 국외주소 회원은 신청제한
		if(hdLocation.Value != "국내") {
			base.AlertWithJavascript("국외주소지 회원은 해당 서비스를 이용하실 수 없습니다." , "goBack()");
			return;
		}

		if(sess.GenderCode == "C") {
			base.AlertWithJavascript("첫 생일 첫 나눔과 결혼 첫 나눔 후원은 개인회원이신 경우 신청하실 수 있습니다", "goBack()");
			return;
		}

		var addr_result = new SponsorAction().GetAddress();
		if(!addr_result.success) {
			base.AlertWithJavascript(addr_result.message, "goBack()");
			return;
		}
		
		SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;
		addr_domestic_zipcode.Value = addr_data.Zipcode;
		addr_domestic_addr1.Value = addr_data.Addr1;
		addr_domestic_addr2.Value = addr_data.Addr2;
		dspAddrDoro.Value = addr_data.DspAddrDoro;
		dspAddrJibun.Value = addr_data.DspAddrJibun;
		hfAddressType.Value = addr_data.AddressType;
		
		var comm_result = new SponsorAction().GetCommunications();
		if(!comm_result.success) {
			base.AlertWithJavascript(comm_result.message, "goBack()");
			return;
		}

		SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
		txtEmail.Text = comm_data.Email;
		txtPhone.Text = comm_data.Mobile;

		txtSponsorName.Text = sess.UserName;
		amount.Value = payInfo.amount.ToString();
        

        for (int i = DateTime.Now.Year; i >= DateTime.Now.Year - 3; i--) {
			baby_birth_yyyy.Items.Add(new ListItem(i.ToString(), i.ToString()));
		}
		baby_birth_yyyy.Items.Insert(0, new ListItem("아기생년 선택", ""));

		for(int i = 1; i < 13; i++) {
			baby_birth_mm.Items.Add(new ListItem(i.ToString(), i.ToString().PadLeft(2, '0')));
		}
		baby_birth_mm.Items.Insert(0, new ListItem("월 선택", ""));

		for(int i = 1; i < 32; i++) {
			baby_birth_dd.Items.Add(new ListItem(i.ToString(), i.ToString().PadLeft(2, '0')));
		}
		baby_birth_dd.Items.Insert(0, new ListItem("일 선택", ""));
		
	}

	void ShowVisitDate()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.tDolVisitSelectDate.Where(p => p.UseYN == 'N' && p.DeleteYN == 'N' && p.Region == visit_region.Value).OrderBy(p => p.VisitOrder);
            //var list = www6.selectQ<tDolVisitSelectDate>("UseYN", "N", "DeleteYN", "N", "Region", visit_region.Value, "VisitOrder");

            //#13429 : 정수진 과장 요청으로 관리자페이지의 방문일자순서를 사용하지 않고 입력된 순서대로 정렬 변경
            var list = www6.selectQ<tDolVisitSelectDate>("UseYN", "N", "DeleteYN", "N", "Region", visit_region.Value, "Idx");
            ddlVisitSelect.DataTextField = "VisitDate";
            ddlVisitSelect.DataValueField = "Idx";
            ddlVisitSelect.DataSource = list;
            ddlVisitSelect.DataBind();
        }

		ddlVisitSelect.Items.Insert(0, new ListItem("선택해 주세요", "0"));
	}

	// 결제
	protected void btn_submit_Click( object sender, EventArgs e ) {

        

        kakaopay_form.Hide();
		kcp_form.Hide();
		payco_form.Hide();

		if(base.IsRefresh) {
			return;
		}

		if(!base.UpdateTemporaryUserInfo()) {
			return;
		}

		var sess = new UserInfo();
		var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

		var action = new CommitmentAction();
		
		var visitIdx = 0;
		var visitDate = "";
		if (gubun.Value == "V") {
			visitIdx = Convert.ToInt32(ddlVisitSelect.SelectedValue);
			visitDate = ddlVisitSelect.SelectedItem.Text;
            //addr_domestic_zipcode.Value = addr_domestic_addr1.Value = addr_domestic_addr2.Value = "";

            // 최초 등록하는 주소인경우 기본정보(주소) 업데이트
            new SponsorAction().UpdateAddress(false, hfAddressType.Value, sess.LocationType, "한국", addr_domestic_zipcode.Value, addr_domestic_addr1.Value, addr_domestic_addr2.Value);
        }
        else {
			visitIdx = 0;
			visitDate = "";

			// 최초 등록하는 주소인경우 기본정보(주소) 업데이트
			new SponsorAction().UpdateAddress(false, hfAddressType.Value, sess.LocationType , "한국", addr_domestic_zipcode.Value, addr_domestic_addr1.Value, addr_domestic_addr2.Value);

		}

		var babyBirth = string.Format("{0}-{1}-{2}", baby_birth_yyyy.SelectedValue, baby_birth_mm.SelectedValue, baby_birth_dd.SelectedValue);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var result = dao.INS_tCspDol(Convert.ToChar(gubun.Value), sess.SponsorID, txtSponsorName.Text, Convert.ToDecimal(amount.Value), txtBabyName.Text, Convert.ToChar(baby_gender.Value),
            //    babyBirth, txtBabyRelation.Text, file_path.Value, "", addr_domestic_zipcode.Value, "", addr_domestic_addr1.Value, addr_domestic_addr2.Value, txtEmail.Text, txtPhone.Text,
            //    visitIdx, visitDate, 'N', Convert.ToChar(bluedog.Value)).First();
            Object[] op1 = new Object[] { "gubun", "sponsorID", "sponsorName", "payAmount", "babyName", "babyGender", "babyBirth", "babyRelation", "babyImage", "babyCertificate", "zip1", "zip2", "address1", "address2", "email", "tel", "visitIdx", "visitDate", "paymentYN", "bluedogYN" };
            Object[] op2 = new Object[] { Convert.ToChar(gubun.Value), sess.SponsorID, txtSponsorName.Text, Convert.ToDecimal(amount.Value), txtBabyName.Text, Convert.ToChar(baby_gender.Value), babyBirth, txtBabyRelation.Text, file_path.Value, "", addr_domestic_zipcode.Value, "", addr_domestic_addr1.Value, addr_domestic_addr2.Value, txtEmail.Text, txtPhone.Text, visitIdx, visitDate, 'N', Convert.ToChar(bluedog.Value) };
            var result = www6.selectSP("INS_tCspDol", op1, op2).DataTableToList<INS_tCspDolResult>().First();

            if (result.Result == 'N')
            {
                base.AlertWithJavascript(result.Result_String);
                return;
            }

            payInfo.relation_key = result.idx.Value;
            payInfo.amount = Convert.ToInt32(amount.Value);
            this.ViewState["good_mny"] = payInfo.amount.ToString();
            if (payment_method_card.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card;
            }
            else if (payment_method_cms.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.cms;
            }
            else if (payment_method_oversea.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card_oversea;
            }
            else if (payment_method_phone.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.phone;
            }
            else if (payment_method_payco.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.payco;
            }
            else if (payment_method_kakao.Checked)
            {
                payInfo.payMethod = PayItemSession.Entity.enumPayMethod.kakao;
            }

            payInfo.extra = gubun.Value + "," + bluedog.Value;


            var orderId = PayItemSession.Store.GetNewOrderID();

            payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo, orderId);
            //payItem.data = payInfo.ToJson();
            this.ViewState["payItem"] = payItem.ToJson();

            //[jun.heo] 2017-12 : 결제 모듈 통합
            this.ViewState["payPageType"] = "FirstBirthday";  // 결제 페이지 종류
            this.ViewState["buyr_name"] = user_name.Value;
            this.ViewState["returnUrl"] = "/pay/complete_firstbirthday/" + orderId;
            //this.ViewState["returnUrl"] = "/sponsor/special/first-birthday/complete/" + orderId;

        }

			// 결제정보를 세팅
		base.SetTemporaryPaymentInfo();
        
        if (payment_method_payco.Checked) {
			payco_form.Show(this.ViewState);
		} else if(payment_method_kakao.Checked) {
			kakaopay_form.Show(this.ViewState);
		} else {
			kcp_form.Show(this.ViewState);
		}

	}

	protected void btn_region_ServerClick( object sender, EventArgs e ) {

		var obj = sender as HtmlButton;
		if (obj.ID == "btn_region_S") {
			visit_region.Value = "S";
		} else {
			visit_region.Value = "B";
		}

		this.ShowVisitDate();
	}
}