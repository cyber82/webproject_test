﻿$(function () {

	$extPage.init();

});

function jusoCallback(zipNo, addr1, addr2, jibun) {
	// 실제 저장 데이타
	$("#addr_domestic_zipcode").val(zipNo);
	$("#addr_domestic_addr1").val(addr1 + "//" + jibun);
	$("#addr_domestic_addr2").val(addr2);

	// 화면에 표시
	$("#post").val(zipNo);
	$("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
	$("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);

};



var $extPage = {

	init: function () {

		$page.init();

		if ($("#btn_file_path").length > 0) {
			var uploader = attachUploader("btn_file_path");
			uploader._settings.data.fileDir = $("#upload_root").val();
			uploader._settings.data.fileType = "image";
			uploader._settings.data.limit = 2048;
		}

		$("#post").val($("#addr_domestic_zipcode").val());

		
		// 컴파스의 데이타를 불러오는경우 
		if ($("#dspAddrDoro").val() != "") {
			$("#addr_road").text("[도로명주소] (" + $("#addr_domestic_zipcode").val() + ") " + $("#dspAddrDoro").val());
			if ($("#dspAddrJibun").val() != "") {
				$("#addr_jibun").text("[지번] (" + $("#addr_domestic_zipcode").val() + ") " + $("#dspAddrJibun").val());
			}
		} else if ($("#addr_domestic_addr1").val() != "") {
			if ($("#addr_domestic_addr1").length) {
				addr_array = $("#addr_domestic_addr1").val().split("//");
				if (addr_array[0] != "") {
					$("#addr_road").text("[도로명주소] " + addr_array[0] + " " + $("#addr_domestic_addr2").val());
				}
				if (addr_array[1]) {
					$("#addr_jibun").text("[지번주소] " + addr_array[1] + " " + $("#addr_domestic_addr2").val());
				}
			}
		}



		$("#btn_babyGender_" + $("#baby_gender").val()).addClass("on");
		$("#btn_region_" + $("#visit_region").val()).addClass("on");

		$(".btn_babyGender").click(function () {

			var id = $(this).attr("id");
			$(".btn_babyGender").removeClass("on");
			$(this).addClass("on");

			if (id == "btn_babyGender_M") {
				$("#baby_gender").val("M");
			} else {
				$("#baby_gender").val("F");
			}
			return false;
		})

		$(".pay_amount").click(function () {

			var amount = parseInt($(this).data("amount"));
			
			$("#amount").val(amount);
			$("#txt_amount").html(amount.format());

			return false;

		});

		$(".pay_amount_custom").focus(function () {
			$(this).val("");
		}).blur(function () {
		    var val = $(this).val();

		    var maxV = $('#bluedog').val() == 'Y' ? 300000 : 100000;

		    if (isNaN(val) || parseInt(val) < maxV) {
				//alert("10만원이상 입력해 주세요");
				$(".pay_coment").show();
				$("#amount").val("");
				$(this).val("");
				$(this).focus();
				return;
			} else {
				$(".pay_coment").hide();
			}

			if (parseInt(val) % 1000 > 0) {
				alert("천원단위로 입력해 주세요");
				$("#amount").val("");
				$(this).val("");
				$(this).focus();
				return;
			}

			$("#amount").val(val);
			$("#txt_amount").html(val.format());
		})

		// 버튼
		$("#btn_submit").unbind("click");
		$("#btn_submit").click(function () {

			return $extPage.onSubmit();

		});

		// postback
		if ($("#amount").val() != 0) {
			$("#txt_amount").html($("#amount").val().format());
		} else {

		    var idx = $('#bluedog').val() == 'Y' ? 0 : 1;

			$($(".pay_amount")[idx]).trigger("click");		// default : 30만원
		}
		
	},

	// 확인
	onSubmit: function () {

		if (!$page.onSubmit()) {
			return false;
		}

		if (!$("#btn_babyGender_M").hasClass("on") && !$("#btn_babyGender_F").hasClass("on")) {
			alert("아기의 성별을 선택해주세요");
			return false;
		}

		$("#post").val($("#addr_domestic_zipcode").val());
		
		$("#baby_gender").val($("#btn_babyGender_M").hasClass("on") ? "M" : "F");
		
		if (!validateForm([
				{ id: "#txtBabyName", msg: "아기이름을 입력해 주세요" },
				{ id: "#baby_birth_yyyy", msg: "아기생일(년)을 선택해 주세요" },
				{ id: "#baby_birth_mm", msg: "아기생일(월)을 선택해 주세요" },
				{ id: "#baby_birth_dd", msg: "아기생일(일)을 선택해 주세요" },
				{ id: "#txtSponsorName", msg: "신청자이름을 입력해 주세요" },
				{ id: "#txtBabyRelation", msg: "아기와의 관계를 입력해 주세요" },
				{ id: "#txtPhone", msg: "휴대전화번호를 입력해 주세요", type: "phone" },
				{ id: "#txtEmail", msg: "이메일주소를 입력해 주세요", type: "email" }
		])) {
			return false;
		}

		if ($("#lb_file_path").length > 0) {
			if (!validateForm([
				{ id: "#lb_file_path", msg: "아기사진을 선택해 주세요" }
			])) {
				return false;
			}
		}

		if ($("#gubun").val() == "P") {		// 온라인신청

			if (!validateForm([
				{ id: "#addr_domestic_zipcode", msg: "배송지정보를 선택해 주세요" },
				{ id: "#addr_domestic_addr2", msg: "배송지 상세주소를 입력해주세요" }
			])) {
				return false;
			}

		} else {

			if ($("#ddlVisitSelect").val() == "0") {
				alert("방문일자를 선택해 주세요");
				$("#ddlVisitSelect").focus();
				return false;
			}

		}

		if ($("#amount").val() == "") {
			alert("후원금액을 선택해주세요");
			return false;
		}
		
		if (isNaN($("#amount").val()) || parseInt($("#amount").val()) < 1) {
			$(".pay_coment").show();
			//alert("후원금액은 10만원 이상 지정가능합니다.");
			return false;
		} else {
			$(".pay_coment").hide();
		}

		return true;
	}


}

var attachUploader = function (button) {
	return new AjaxUpload(button, {
		action: '/common/handler/upload',
		responseType: 'json',
		onChange: function () {
		},
		onSubmit: function (file, ext) {
			this.disable();
		},
		onComplete: function (file, response) {

			this.enable();

			if (response.success) {
				//$("#file_path").val(response.name);
				$("#file_path").val(response.name.replace(/^.*[\\\/]/, ''));
				$("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
				//	$(".temp_file_size").val(response.size);

			} else
				alert(response.msg);
		}
	});
}