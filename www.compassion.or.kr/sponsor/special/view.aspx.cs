﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class sponsor_special_view : FrontBasePage {
	
	public override bool RequireSSL {
		get {
			return false;
		}
	}

	public int percent;
	public int remain_days;
	public sp_tSpecialFunding_get_fResult entity;
    public WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

    protected override void OnBeforePostBack() {

        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;
        Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));

        base.OnBeforePostBack();

		var requests = Request.GetFriendlyUrlSegments();

		complete_comment.Visible = false;

		if (requests.Count > 0) {
			campaignId.Value = requests[0];
		}
		
	}


	protected override void loadComplete( object sender, EventArgs e )
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //entity = dao.sp_tSpecialFunding_get_f(campaignId.Value).FirstOrDefault();
            Object[] op1 = new Object[] { "campaignId" };
            Object[] op2 = new Object[] { campaignId.Value };
            
            entity = www6.selectSP("sp_tSpecialFunding_get_f", op1, op2).DataTableToList<sp_tSpecialFunding_get_fResult>().FirstOrDefault();

            if (entity == null)
                Response.Redirect("/sponsor/special/");

            Object[] objParam = new object[] { "CampaignID" };
            Object[] objValue = new object[] { entity.CampaignID };
            Object[] objSql = new object[] { "sp_S_Campaign_PaymentTotal" };
            DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["TOTAL"] != DBNull.Value)
                {
                    long total = Convert.ToInt32(ds.Tables[0].Rows[0]["TOTAL"]);
                    entity.sf_current_amount = total;
                }
                else
                {
                    entity.sf_current_amount = 0;
                }
            }

            data.Value = entity.ToJson();
            content.Text = entity.sf_content;

            if (entity.sf_image_m != null)
            {
                sf_image.Src = entity.sf_image.WithFileServerHost();
            }
            else
            {
                sf_image.Visible = false;
            }

            remain_days = Convert.ToInt32(entity.EndDate.Value.Subtract(DateTime.Now).TotalDays);

            // SNS
            this.ViewState["meta_url"] = Request.Url.AbsoluteUri;
            this.ViewState["meta_title"] = string.Format("특별한 나눔-{0}", entity.CampaignName);
            // optional(설정 안할 경우 주석)
            this.ViewState["meta_description"] = entity.sf_summary;
            this.ViewState["meta_keyword"] = entity.sf_meta_keyword;
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,FrontBasePage)
            this.ViewState["meta_image"] = entity.sf_thumb.WithFileServerHost();

            // days
            if (entity.EndDate.HasValue)
                days.Text = string.Format("<span class='d_day'>D-{0}</span>", remain_days.ToString("N0"));

            if (entity.sf_goal_amount < 1)
            {
                percent = -1;

            }
            else
            {
                if (entity.sf_current_amount < 1)
                    percent = 0;
                else
                    percent = Convert.ToInt32(entity.sf_current_amount * 1.0 / entity.sf_goal_amount * 100);

                if (percent > 100)
                    percent = 100;
            }

            if (remain_days < 0)
            {
                ph_amount.Visible = false;
                days.Visible = false;
                complete_comment.Visible = true;
            }
        }


	}


	
}