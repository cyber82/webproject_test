﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="sponsor_special_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
    <script type="text/javascript" src="/sponsor/special/default.js"></script>
	<script type="text/javascript" src="/common/js/site/moveToMobile.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>특별한 <em>나눔</em></h1>
                <span class="desc">주제별 다양한 나눔을 선택하여 후원하실 수 있습니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 sponsor">

            <div class="visual_special">
                <div class="slide_wrap" id="special_funding_visual">

                    <asp:Repeater runat="server" ID="repeater" OnItemDataBound="repeater_ItemDataBound">
                        <ItemTemplate>
                            <!-- sliding set / size : 100% * 750px -->
                            <div class="item img" style="background: url('<%# Eval("sf_image").ToString()%>') no-repeat center top">
                                <div class="visual_dim"></div>
                                <div class="wrap">

                                    <!-- floating layer -->
                                    <!-- 레이어 height에 따라 위치값(top position) 변경 되어야 함 -->
                                    <div class="box_type1 layer_banner" id="layer_banner" runat="server">
                                        <!-- 공유하기버튼 -->
                                        <span class="sns_ani mb20">
                                            <span class="common_sns_group">
                                                <span class="wrap">

                                                    <a href="#" title="페이스북" data-role="sns" data-provider="fb" data-url="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="sns_facebook">페이스북</a>
                                                    <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" data-url="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="sns_story">카카오스토리</a>
                                                    <a href="#" title="트위터" data-role="sns" data-provider="tw" data-title="특별한후원-<%#Eval("campaignName") %>" data-url="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="sns_twitter">트위터</a>
                                                    <a href="#" title="url 공유" data-role="sns" data-provider="copy" data-url="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="sns-copy sns_url">url 공유</a>

                                                </span>
                                            </span>
                                            <button class="common_sns_share">공유하기</button>
                                        </span>
                                        <!--// -->

                                        <a href="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="tit"><%#Eval("campaignName") %></a>
                                        <a href="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="desc"><%#Eval("sf_summary") %></a>
                                        <a href="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="btn_arr_type1 mb30">자세한 내용 더보기<span></span></a>

                                        <!-- graph -->
                                        <div class="specialMainGraph" id="graph" runat="server">
                                            <div class="commonGraph_wrap">
                                                <span class="heart" runat="server" id="heart">
                                                    <span class="txt" runat="server" id="txt"></span>
                                                </span>
                                                <div class="graph">
                                                    <span class="bg"></span>
                                                    <span class="per" runat="server" id="per"></span>
                                                </div>
                                                <div class="sum clear2">
                                                    <span class="ing"><%#Eval("sf_current_amount" , "{0:N0}") %>원</span>
                                                    <span class="goal"><%#Eval("sf_goal_amount" , "{0:N0}") %>원</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--// -->
                                    </div>
                                    <!--// floating layer -->

                                </div>
                            </div>
                            <!--// -->
                        </ItemTemplate>
                    </asp:Repeater>

                </div>

                <div class="btn_group" style="z-index: 10">
                    <div class="indi_wrap"></div>
                    <button class="prev">
                        <img src="/common/img/btn/prev_1.png" alt="previous" /></button>
                    <button class="next">
                        <img src="/common/img/btn/next_1.png" alt="next" /></button>

                </div>
                <div class="btn_view" style="z-index: 100"><a href="#" ng-click="showCompleteCampaign($event)" class="btn_b_type2">완료된 캠페인 보기</a></div>
            </div>

            <div class="special main">
                <div class="w980">

                    <div class="sub_tit mb40">
                        <h2 class="tit"><em>양육에 가치를 더하는 또 다른 방법</em></h2>
                        <span class="bar"></span>
                        <p>
                            <em>긴급 수술, 재해 복구, 학대 예방, 직업교육 등<br />
                                어린이들 앞에 놓여진 많은  장애물들을 제거하여 지속적이고 효과적인 양육을 받을 수 있도록 돕습니다.</em>
                        </p>
                    </div>

                    <script type="text/javascript">
                        $(function () {

                            var a = $(".special_category .tab");
                            a.mouseenter(function () {
                            	var num = $(this).parent("li").index() + 1;

                            	a.removeClass("on");
                            	$(this).addClass("on");

                            	$(".category_con").hide();
                            	$(".cat_con" + num).show();
                            })
                        })
                    </script>

                    <!-- 탭 레이어 -->
                    <div class="special_category">
                        <ul class="clear2">
                            <!-- 생존 -->
                            <li>
                                <button class="tab tab1 on"><span>태아&middot;영아 생존</span></button>
                                <div class="category_con cat_con1">
                                    <div class="txt_area">
                                        <p class="tit">삶과 죽음이 결정되는 영•유아 시기</p>
                                        <p class="desc">태아&middot;영아 생존 / Survival</p>
                                        <p class="con">
                                            유아 사망률이 높은 지역에서 태어난 아기들은 생후 1년 안에 삶과 죽음이 결정됩니다.<br />
                                            가장 연약한 존재들인 아기들에게 생명을 선물해주세요.
                                        </p>
                                        <a href="/sponsor/special/fixed/CSPF" class="btn_arr_type1">자세한 내용 더보기<span></span></a>
                                    </div>
									<%
										string campaignID = "20160801175336172";
										var campaign = fix_list.FirstOrDefault(p => p.CampaignID == campaignID);
										if(campaign != null) {		
									%>
                                    <div class="input_area">
                                        <span class="radio_ui mb15">
											<%if(campaign.sf_is_regular) { %>
                                            <input type="radio" id="frequency_1" name="frequency_1" data-codeid="CSPF" value="정기" class="css_radio frequency" checked />
                                            <label for="frequency_1" class="css_label">정기후원</label>
											<%} %>
											<%if(campaign.sf_is_temporary) { %>
                                            <input type="radio" id="frequency_2" name="frequency_1" data-codeid="CSPF" value="일시" class="css_radio frequency" <%if(!campaign.sf_is_regular) { %>checked<%} %> />
                                            <label for="frequency_2" class="css_label ml40">일시후원</label>
											<%} %>
                                        </span>
                                        <br />
                                        <div class="tac">
                                            <div class="inblock">
                                                <span class="input fl">
                                                    <label for="amount" class="hidden">후원금액 입력</label>
                                                    <input type="text" value="20,000" data-codeid="CSPF" class="amount number_only use_digit" maxlength="10" />
                                                    <span class="won">원</span>
                                                </span>
                                                <a href="#" data-action="pay" data-codeid="CSPF" data-campaignid="20160801175336172" class="btn_type1 fl">바로 후원하기</a>
                                            </div>
                                        </div>
                                    </div>
									<%} %>
                                </div>
                            </li>
                            <!--// -->
                            <!-- 재난 -->
                            <li>
                                <button class="tab tab2"><span>재난 구호</span></button>
                                <div class="category_con cat_con2" style="display: none">
                                    <div class="txt_area">
                                        <p class="tit">재난으로 고통 받는 어린이 후원</p>
                                        <p class="desc">재난 구호 Disaster Relief</p>
                                        <p class="con">
                                            한 순간에 모든 것을 휩쓸어가는 자연 재해는 어른들이 견디기에도 힘든 일입니다.<br />
                                            재난이 주는 가혹한 현실 앞에, 어린이들이 다시 희망의 끈을 붙잡을 수 있도록 도와주세요.
                                        </p>
                                        <a href="/sponsor/special/fixed/RF" class="btn_arr_type1">자세한 내용 더보기<span></span></a>
                                    </div>
									<%
										campaignID = "20160801175221172";
										campaign = fix_list.FirstOrDefault(p => p.CampaignID == campaignID);
										if(campaign != null) {		
									%>

                                    <div class="input_area">
                                        <span class="radio_ui mb15">
											<%if(campaign.sf_is_regular) { %>
                                            <input type="radio" id="frequency_21" name="frequency_2" data-codeid="RF" value="정기" class="css_radio frequency " checked />
                                            <label for="frequency_21" class="css_label">정기후원</label>
											<%} %>
											<%if(campaign.sf_is_temporary) { %>
                                            <input type="radio" id="frequency_22" name="frequency_2" data-codeid="RF" value="일시" class="css_radio frequency" <%if(!campaign.sf_is_regular) { %>checked<%} %> />
                                            <label for="frequency_22" class="css_label ml40">일시후원</label>
											<%} %>
                                        </span>
                                        <br />
                                        <div class="tac">
                                            <div class="inblock">
                                                <span class="input fl">
                                                    <label for="amount" class="hidden">후원금액 입력</label>
                                                    <input type="text" value="20,000" data-codeid="RF" class="amount number_only use_digit" maxlength="10" />
                                                    <span class="won">원</span>
                                                </span>
                                                <a href="#" data-action="pay" data-codeid="RF" data-campaignid="20160801175221172" class="btn_type1 fl">바로 후원하기</a>
                                            </div>
                                        </div>
                                    </div>
									<%} %>
                                </div>
                            </li>
                            <!--// -->
                            <!-- 교육 -->
                            <li>
                                <button class="tab tab3"><span>교육 지원</span></button>
                                <div class="category_con cat_con3" style="display: none">
                                    <div class="txt_area">
                                        <p class="tit">가난의 악순환을 끊는 효과적인 방법</p>
                                        <p class="desc">교육 지원 Education Needs</p>
                                        <p class="con">
                                            컴패션은 어린이들의 잠재력과 가능성을 바라봅니다.<br />
                                            직업훈련, 비정규교육, 입학 시험 등 기본적인 양육 외 추가적인 교육을 지원하여 스스로의 힘으로 가난에서 벗어날 수 있도록 함께해주세요.
                                        </p>
                                        <a href="/sponsor/special/fixed/EDU" class="btn_arr_type1">자세한 내용 더보기<span></span></a>
                                    </div>
									<%
										campaignID = "20160801175450682";
										campaign = fix_list.FirstOrDefault(p => p.CampaignID == campaignID);
										if(campaign != null) {		
									%>
                                    <div class="input_area">
                                        <span class="radio_ui mb15">
											<%if(campaign.sf_is_regular) { %>
                                            <input type="radio" id="frequency_31" name="frequency_3" data-codeid="EDU" value="정기" class="css_radio frequency " checked />
                                            <label for="frequency_31" class="css_label">정기후원</label>
											<%} %>
											<%if(campaign.sf_is_temporary) { %>
                                            <input type="radio" id="frequency_32" name="frequency_3" data-codeid="EDU" value="일시" class="css_radio frequency" <%if(!campaign.sf_is_regular) { %>checked<%} %> />
                                            <label for="frequency_32" class="css_label ml40">일시후원</label>
											<%} %>
                                        </span>
                                        <br />
                                        <div class="tac">
                                            <div class="inblock">
                                                <span class="input fl">
                                                    <label for="amount" class="hidden">후원금액 입력</label>
                                                    <input type="text" value="20,000" data-codeid="EDU" class="amount number_only use_digit" maxlength="10" />
                                                    <span class="won">원</span>
                                                </span>
                                                <a href="#" data-action="pay" data-codeid="EDU" data-campaignid="20160801175450682" class="btn_type1 fl">바로 후원하기</a>
                                            </div>
                                        </div>
                                    </div>
									<%} %>
                                </div>
                            </li>
                            <!--// -->
                            <!-- 의료 -->
                            <li>
                                <button class="tab tab4"><span>의료 지원</span></button>
                                <div class="category_con cat_con4" style="display: none">
                                    <div class="txt_area">
                                        <p class="tit">아파도 병원에 갈 수 없는 어린이들</p>
                                        <p class="desc">의료 지원 Medical Needs</p>
                                        <p class="con">
                                            아파도 병원에 갈 수 없는 어린이들이 있습니다. 갑자기 발생하는 병원비는 이들이 감당하기에 너무 큰 금액입니다.<br />
                                            어린이들이 적절한 치료를 받고 일상으로 빠르게 복귀할 수 있도록 응원해주세요.
                                        </p>
                                        <a href="/sponsor/special/fixed/MDA" class="btn_arr_type1">자세한 내용 더보기<span></span></a>
                                    </div>
									<%
										campaignID = "20160801175747672";
										campaign = fix_list.FirstOrDefault(p => p.CampaignID == campaignID);
										if(campaign != null) {		
									%>
                                    <div class="input_area">
                                        <span class="radio_ui mb15">
											<%if(campaign.sf_is_regular) { %>
                                            <input type="radio" id="frequency_41" name="frequency_4" data-codeid="MDA" value="정기" class="css_radio frequency " checked />
                                            <label for="frequency_41" class="css_label">정기후원</label>
											<%} %>
											<%if(campaign.sf_is_temporary) { %>
                                            <input type="radio" id="frequency_42" name="frequency_4" data-codeid="MDA" value="일시" class="css_radio frequency" <%if(!campaign.sf_is_regular) { %>checked<%} %> />
                                            <label for="frequency_42" class="css_label ml40">일시후원</label>
											<%} %>
                                        </span>
                                        <br />
                                        <div class="tac">
                                            <div class="inblock">
                                                <span class="input fl">
                                                    <label for="amount" class="hidden">후원금액 입력</label>
                                                    <input type="text" value="20,000" data-codeid="MDA" class="amount number_only use_digit" maxlength="10" />
                                                    <span class="won">원</span>
                                                </span>
                                                <a href="#" data-action="pay" data-codeid="MDA" data-campaignid="20160801175747672" class="btn_type1 fl">바로 후원하기</a>
                                            </div>
                                        </div>
                                    </div>
									<%} %>
                                </div>
                            </li>
                            <!--// -->
                            <!-- 취약 -->
                            <li>
                                <button class="tab tab5"><span>취약 어린이 지원</span></button>
                                <div class="category_con cat_con5" style="display: none">
                                    <div class="txt_area">
                                        <p class="tit">‘위험’과의 불안한 동거</p>
                                        <p class="desc">취약어린이 지원 Highly Vulnerable Children</p>
                                        <p class="con">
                                            세상 어떤 어린이도 버림받거나, 학대나 착취를 경험해선 안됩니다.<br />
                                            각별한 관심과 보호가 필요한 어린이들에게 ‘너는 사랑 받아 마땅한 아이’라고 말해주세요.
                                        </p>
                                        <a href="/sponsor/special/fixed/OVC" class="btn_arr_type1">자세한 내용 더보기<span></span></a>
                                    </div>
									<%
										campaignID = "20160801175802342";
										campaign = fix_list.FirstOrDefault(p => p.CampaignID == campaignID);
										if(campaign != null) {		
									%>
                                    <div class="input_area">
                                        <span class="radio_ui mb15">
											<%if(campaign.sf_is_regular) { %>
                                            <input type="radio" id="frequency_51" name="frequency_5" data-codeid="OVC" value="정기" class="css_radio frequency " checked />
                                            <label for="frequency_51" class="css_label">정기후원</label>
											<%} %>
											<%if(campaign.sf_is_temporary) { %>
                                            <input type="radio" id="frequency_52" name="frequency_5" data-codeid="OVC" value="일시" class="css_radio frequency" <%if(!campaign.sf_is_regular) { %>checked<%} %> />
                                            <label for="frequency_52" class="css_label ml40">일시후원</label>
											<%} %>
                                        </span>
                                        <br />
                                        <div class="tac">
                                            <div class="inblock">
                                                <span class="input fl">
                                                    <label for="amount" class="hidden">후원금액 입력</label>
                                                    <input type="text" value="20,000" data-codeid="OVC" class="amount number_only use_digit" maxlength="10" />
                                                    <span class="won">원</span>
                                                </span>
                                                <a href="#" data-action="pay" data-codeid="OVC" data-campaignid="20160801175802342" class="btn_type1 fl">바로 후원하기</a>
                                            </div>
                                        </div>
                                    </div>
									<%} %>
                                </div>
                            </li>
                            <!--// -->
                            <!-- 위생 -->
                            <li>
                                <button class="tab tab6"><span>에이즈 예방 및 퇴치</span></button>
                                <div class="category_con cat_con6" style="display: none">
                                    <div class="txt_area">
                                        <p class="tit">에이즈만큼 무서운 ‘사람들의 시선’</p>
                                        <p class="desc">에이즈 예방 및 퇴치 HIV and AIDS Initiative</p>
                                        <p class="con">
                                            매일 600명의 어린이들이 HIV/AIDS 바이러스에 감염되고 있지만, 에이즈 환자로 낙인 찍히는 것이 두려워 검사나 치료를 거부하는<br />
                                            경우도 있습니다. 질병과 두려움으로 싸우고 있는 어린이들에게 도움의 손길을 내밀어주세요.
                                        </p>
                                        <a href="/sponsor/special/fixed/AIDS" class="btn_arr_type1">자세한 내용 더보기<span></span></a>
                                    </div>
									<%
										campaignID = "20160801181135252";
										campaign = fix_list.FirstOrDefault(p => p.CampaignID == campaignID);
										if(campaign != null) {		
									%>
                                    <div class="input_area">
                                        <span class="radio_ui mb15">
											<%if(campaign.sf_is_regular) { %>
                                            <input type="radio" id="frequency_61" name="frequency_6" data-codeid="HH" value="정기" class="css_radio frequency " checked />
                                            <label for="frequency_61" class="css_label">정기후원</label>
											<%} %>
											<%if(campaign.sf_is_temporary) { %>
                                            <input type="radio" id="frequency_62" name="frequency_6" data-codeid="HH" value="일시" class="css_radio frequency" <%if(!campaign.sf_is_regular) { %>checked<%} %> />
                                            <label for="frequency_62" class="css_label ml40">일시후원</label>
											<%} %>
                                        </span>
                                        <br />
                                        <div class="tac">
                                            <div class="inblock">
                                                <span class="input fl">
                                                    <label for="amount" class="hidden">후원금액 입력</label>
                                                    <input type="text" value="20,000" data-codeid="HH" class="amount number_only use_digit" maxlength="10" />
                                                    <span class="won">원</span>
                                                </span>
                                                <a href="#" data-action="pay" data-codeid="HH" data-campaignid="20160801181135252" class="btn_type1 fl">바로 후원하기</a>
                                            </div>
                                        </div>
                                    </div>
									<%} %>
                                </div>
                            </li>
                            <!--// -->
                        </ul>
                    </div>
                    <!--// 탭 레이어 -->

                </div>

                <!-- 더 특별한 후원 -->
                <div class="bgContent moreSpecial">

                    <div class="w980 relative">
                        <p class="tit">기념일엔 더 특별한 후원을</p>
                        <span class="bar"></span>
                        <p class="con">내 아이의 첫 생일, 결혼기념일 등 특별한 날을 오래 기억할 수 있도록<br />
                            여러분이 받은 사랑과 축복을 어린이들에게 나눠주세요.</p>

                        <ul class="clear2">
                            <li class="icon1">
                                <span>첫 생일 첫 나눔</span>
                                <a href="/sponsor/special/first-birthday/" class="btn_b_type5">바로가기</a>
                            </li>
                            <li class="icon2">
                                <span>결혼 첫 나눔</span>
                                <a href="/sponsor/special/wedding/" class="btn_b_type5">바로가기</a>
                            </li>
                            <li class="icon3">
                                <span>믿음의 유산</span>
                                <a href="/sponsor/special/fixed/legacy" class="btn_b_type5">바로가기</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!--// 더 특별한 후원 -->

                <!-- 북한사역 -->
                <div class="bgContent north_intro">

                    <div class="w980 relative">
                        <p class="tit">컴패션북한사역</p>
                        <span class="bar"></span>
                        <p class="con">북한을 변화시킬 희망의 씨앗이 될 북한어린이들에게<br />
                            전인적인 양육을 제공하기 위한 준비사역에 재정적으로 함께해주세요.</p>

                        <a href="/sponsor/special/fixed/NK" class="btn_b_type2 mb60">자세한 내용 더보기</a><br />
						<!-- /sponsor/special/fixed/NK?campaignId=20171103165547447 -->

                        <!-- 공유하기버튼 -->
                        <span class="sns_ani">
                            <span class="common_sns_group">
                                <span class="wrap">

                                    <a href="#" title="페이스북" data-role="sns" data-provider="fb" data-url="<%:Request.Domain() + "/sponsor/special/fixed/NK?campaignId=20160801175832532" %>" class="sns_facebook">페이스북</a>
                                    <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" data-url="<%:Request.Domain() + "/sponsor/special/fixed/NK?campaignId=20160801175832532" %>" class="sns_story">카카오스토리</a>
                                    <a href="#" title="트위터" data-role="sns" data-provider="tw" data-title="특별한후원-컴패션북한사역" data-url="<%:Request.Domain() + "/sponsor/special/fixed/NK?campaignId=20160801175832532" %>" class="sns_twitter">트위터</a>
                                    <a href="#" title="url 공유" data-role="sns" data-provider="copy" data-url="<%:Request.Domain() + "/sponsor/special/fixed/NK?campaignId=20160801175832532" %>" class="sns-copy sns_url">url 공유</a>

                                </span>
                            </span>
                            <button class="common_sns_share">공유하기</button>
                        </span>
                        <!--// -->
                    </div>

                </div>
                <!--// 북한사역 -->

            </div>
        </div>
        <!--// e: sub contents -->


    </section>

</asp:Content>
