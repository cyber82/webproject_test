﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pop-complete-campaign.aspx.cs" Inherits="sponsor_special_pop_complete_campaign"  %>


<div style="background:transparent;" class="fn_pop_container" id="childModalview">

	<div class="pop_type1 w800 fn_pop_content" style="width:800px;height:850px;padding-top:50px;">
	<!-- 일반팝업 width : 800 -->
	
		<div class="pop_title">
			<span>완료된 캠페인</span>
			<button class="pop_close" ng-click="modalComplete.hide($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content campaign_endList">
			
			<!-- 나의 어린이 리스트 -->
			<div class="tableWrap3 mb30">
				<table class="tbl_type4">
					<caption>완료된 캠페인 리스트</caption>
					<colgroup>
						<col style="width:65%">
						<col style="width:35%">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">캠페인 제목</th>
							<th scope="col">캠페인 기간</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="item in modalComplete.list">
							<td class="tit"><a href="#" ng-click="modalComplete.goView($event,item)">{{item.campaignname}}</a></td>
							<td>{{item.startdate | date:'yyyy.MM.dd'}} ~ {{item.enddate | date:'yyyy.MM.dd'}}</td>
						</tr>

						<!-- 완료된 캠페인 없을때 -->
						<tr ng-if="modalComplete.total == 0">
							<td colspan="2" class="no_content">완료된 캠페인이 없습니다.</td>
						</t>						
					</tbody>
				</table>
			</div>
			<!--// 나의 어린이 리스트 -->

			<!-- page navigation -->
			<div class="tac mb60">
				<paging class="small" page="modalComplete.params.page" page-size="modalComplete.params.rowsPerPage" total="modalComplete.total" show-prev-next="true" show-first-last="true" paging-action="modalComplete.getList({page : page});"></paging>
			</div>
			<!--// page navigation -->

		</div>
	</div>
	<!--// popup -->
</div>
