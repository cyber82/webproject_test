﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NK.aspx.cs" Inherits="sponsor_special_fixed_nk" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/common/js/site/moveToMobile.js"></script>
	<script type="text/javascript" src="/sponsor/special/fixed/fixed.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="campaignId" />
	<input type="hidden" runat="server" id="frequency" />
	<input type="hidden" runat="server" id="amount" />
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>컴패션<em>북한사역</em></h1>
				<span class="desc">북한어린이를 향한 소망의 여정에 함께해주세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 sponsor">
			<div class="special north">

			<!-- visual -->
			<div class="bgContent">
				<div class="w980">
					<span class="bar"></span>
					<p class="con">
						<span>
							컴패션은 지난 60여 년 동안 도움이 필요한 어린이들에게 하나님께서 품으시는 함께 아파하는 마음으로<br />
							전 세계 교회와 협력하여 1:1 어린이 양육에 주력해 왔습니다. 그리고 이제 북한 어린이를 품기 원하시는 하나님의 마음에 응답하고자 합니다.
						</span>
					</p>
				</div>
			</div>
			<!--// visual -->

			<!-- 그래프영역 -->
			<div class="sf_cam_graph static">
				<div class="w980 clear2 relative">

					<div class="sel_type">
						<span class="radio_ui">
							<asp:PlaceHolder runat="server" ID="ph_sf_is_regular" Visible="false">
							<span class="box">
								<input type="radio" id="sf_is_regular"  value="정기" name="frequency" class="css_radio" />
								<label for="sf_is_regular" class="css_label">정기후원</label>
							</span>
							</asp:PlaceHolder>
							<asp:PlaceHolder runat="server" ID="ph_sf_is_temporary" Visible="false">
							<span class="box">
								<input type="radio" id="sf_is_temporary" value="일시" name="frequency" class="css_radio" />
								<label for="sf_is_temporary" class="css_label">일시후원</label>
							</span>
							</asp:PlaceHolder>
						</span>
					</div>

					<script type="text/javascript">
						$(function () {
							// 바로후원하기, sns공유버튼 스크롤 fixed
							$(window).scroll(function () {
								if ($(this).scrollTop() > 850) {
									$(".btn_dir_sponsor").addClass("fixed");
								} else {
									$(".btn_dir_sponsor").removeClass("fixed");
								}
							});

						
						})
					</script>

					
					<div class="amountWrap">

						<div class="inblock clear2">
							
							<span class="amount">
								<label for="dsp_amount" class="hidden">금액 입력</label>
								<input type="text" id="dsp_amount" value="20,000" class="input_amount number_only use_digit" maxlength="10" />
								<span class="won">원</span>
							</span>
							
							<div class="btn_dir_sponsor">
								<a href="#" class="btn_b_type2" ng-click="showPay($event)">바로 후원하기</a>

								
								<!-- 공유하기버튼 -->
								<span class="sns_ani down">
									<button class="common_sns_share">공유하기</button>
									<span class="common_sns_group">
										<span class="wrap">
											<a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
											<a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
											<a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
											<a href="#" title="url 공유" data-role="sns" data-provider="copy" class="sns-copy sns_url">url 공유</a>
										</span>
									</span>
									
								</span>
								<!--// -->
							</div>
						</div>
					</div>

					

				</div>
			</div>
			<!--// 그래프영역 -->
			
		    <%=sf_content %>


			</div>

			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->

		
    </section>


</asp:Content>
