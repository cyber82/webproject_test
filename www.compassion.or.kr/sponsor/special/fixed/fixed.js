﻿$(function () {

	$page.init();

});


var $page = {


	init: function () {
		
	}

};

(function () {

	var app = angular.module("cps.page", []);
	app.controller("defaultCtrl", function ($scope, $http , popup) {

		if ($("input[name=frequency]").length > 0) {
			$($("input[name=frequency]")[0]).prop("checked", true);
		}

		// 뉴스레터
		$scope.report = {
			total: -1,
			list: [],
			params: {
				page: 1,
				rowsPerPage: 3,
				campaignId: $("#campaignId").val()
			},

			getList: function (params, reload) {
				$scope.report.params = $.extend($scope.report.params, params);

				$http.get("/api/special-funding.ashx?t=report", { params: $scope.report.params }).success(function (r) {
					if (r.success) {
						console.log(r.data);
						var list = r.data;

						$.each(list, function () {
							this.sr_regdate = new Date(this.sr_regdate);
							var elapsed = (new Date() - this.sr_regdate) / (1000 * 60 * 60 * 24);		// days
							this.is_new = elapsed < 20;

						});

						// 더보기인경우 merge
						if (reload) {
							$scope.report.list = list;
						} else {
							$scope.report.list = $.merge($scope.report.list, list);
						}
						$scope.report.total = list.length > 0 ? list[0].total : 0;


					} else {
						alert(r.message);
					}
				});

			},

			showMore: function ($event) {
				$event.preventDefault();

				$scope.report.params.page++;
				$scope.report.getList();
			}

		};

		$scope.report.getList();

		// 바로 후원하기
		$scope.showPay = function ($event) {
			$event.preventDefault();
			$scope.modal.show();
		}

		// 레이어 팝업 - 후원금액설정 팝업
		$scope.modal = {
			instance: null,

			init: function () {
				
				var frequency = "";
				frequency += ($("#sf_is_regular").length > 0) ? "Y" : "N";
				frequency += ($("#sf_is_temporary").length > 0) ? "Y" : "N";

				popup.init($scope, "/sponsor/pop-pay/" + frequency, function (modal) {
					$scope.modal.instance = modal;
				});
			},

			show: function () {
				if (!$scope.modal.instance)
					return;

				setNumberOnly();

				if ($("#dsp_amount").val() != "") {
					$(".pay_amount_custom").val($("#dsp_amount").val());
					$("#amount").val($("#dsp_amount").val().replace(/\,/g , ""));
				}

				var frequency = $("input[name=frequency]:checked").val();
				$("input[name=pop_frequency][value='" + frequency + "']").prop("checked" , true);
				$("#frequency").val(frequency);

				$scope.modal.instance.show();
			},

			request: function ($event) {
				$event.preventDefault();
				if ($("#amount").val() == "") {
					alert("금액을 선택해 주세요");
					return false;

				}

				if ($scope.requesting) return;
				$scope.requesting = true;

				var params = {
					campaignId: $("#campaignId").val(),
					frequency: $("#frequency").val(),
					amount: $("#amount").val().replace(/\,/g , "")
				}
				$.post("/sponsor/pay-gateway.ashx?t=go-special", params).success(function (r) {

					if (r.success) {
						location.href = r.data;
					} else {
						if (r.action == "nonpayment") {		// 미납금

							if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
								location.href = "/my/sponsor/pay-delay/";
							}

						} else {
							alert(r.message);
						}
					}
				});

			},

			close: function ($event) {
				$event.preventDefault();
				if (!$scope.modal.instance)
					return;

				$scope.modal.instance.hide();
			}

		}
		$scope.modal.init();

	});





})();
