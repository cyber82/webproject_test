﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="legacy.aspx.cs" Inherits="sponsor_special_fixed_legacy" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/common/js/site/moveToMobile.js"></script>
	<script type="text/javascript" src="/sponsor/special/fixed/fixed.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="campaignId" />
	<input type="hidden" runat="server" id="frequency" />
	<input type="hidden" runat="server" id="amount" />
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
			<h1>믿음의 <em>유산</em></h1>
				<span class="desc">후원자님의 삶과 사랑이 믿음의 유산으로 전해집니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 sponsor">

			<div class="special inherit">
				
				<!-- visual -->
				<div class="bgContent visual">

					<div class="w980">
						<p class="tit">믿음의 유산, 사랑이 남습니다.</p>
						<span class="bar"></span>
						<p class="con">
							믿음으로 하루하루를 성실하게 보낸 후원자님의 삶은 전해지고 이어져야 할 소중한 자산입니다.<br />
							유산기부를 통해 전해질 믿음, 소망, 사랑은 어린이들이 꿈을 향해 일어설 수 있는<br />
							희망의 이유가 될 것입니다.
						</p>

						<!-- 공유하기버튼 -->
						<span class="sns_ani">
							<span class="common_sns_group">
								<span class="wrap">
									<a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
									<a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
									<a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
									<a href="#" title="url 공유" data-role="sns" data-provider="copy" class="sns_url">url 공유</a>
								</span>
							</span>
							<button class="common_sns_share">공유하기</button>
						</span>
						<!--// -->
					</div>

				</div>
				<!--// visual -->

				<!-- 신청방법 -->
				<div class="bgContent apply">
					<div class="w980">
						<div class="">
							<p class="tit">신청방법</p>
							<span class="bar"></span>
							<p class="con">
								'믿음의 유산' 후원 상담을 원하시면 '문의하기' 클릭 후 성함과 연락처를 남겨주세요.<br />
								담당자 확인 후 연락 드리겠습니다.
							</p>
						</div>
					</div>
				</div>
				<!--// 신청방법 -->

				<!-- 컨텐츠 -->
				<div class="method">
					<div class="w980">
						<ul>
							<li>
								<p class="subject">유언을 통한 <em class="fc_blue">기부</em></p>
								<p class="con">
									가장 일반적인 형태의 유산 기부로, 민법에서 정한 유언 공증의 방식에 따라 유산을 기부하실 수 있습니다.<br />
									후원자님이 남기신 유언에 따라 추후 유산이 집행되어, 지정하신 용처에 맞게 후원금이 사용됩니다.<br />
									<span class="s_con1">유산기부 상담 > 법률, 세무 검토 > 유언공증 및 서약서 작성 > 사후 유언 집행 > 후원금 처리</span>
								</p>
							</li>
							<li>
								<p class="subject">상속 유산<em class="fc_blue">기부</em></p>
								<p class="con">
									부모님, 가족으로부터 상속받은 유산을 컴패션 유산기부 프로그램 '믿음의 유산'에 기부하실 수 있습니다.
									<span class="s_con1">유산기부 상담 > 후원금 용처 선택 > 후원금 처리 > 후원 결과 보고서</span>
								</p>
							</li>
							<li>
								<p class="subject">그 외 유산<em class="fc_blue">기부</em></p>
								<p class="con">
									뜻 깊은 나눔을 실천하고 나눔의 열매를 경험하고자 할 경우 유산기부 프로그램 '믿음의 유산'을 통해<br />
									미리 기부를 준비하실 수 있습니다.
								</p>
							</li>
							<li>
								<p class="con">※ 법무법인 광장의 사회공헌위원회에서 유산기부에 관련된 법률 자문을 제공합니다.</p>
							</li>
						</ul>
					</div>
				</div>
				<!--// 컨텐츠 -->

				
				<!-- 후원금 사용 -->
				<div class="use">
					<div class="w980">

						<div class="sub_tit mb40">
							<h2 class="tit"><em>후원금은 이렇게 사용됩니다!</em></h2>
							<p>상담을 통해 기부금이 가장 소중하게 쓰여질 사용처를 지정하실 수 있습니다.</p>
						</div>

						<ul class="list clear2">
							<li>
								<p class="subject">미결연 어린이 후원금 지원</p>
								<p class="con">컴패션어린이센터에는 아직 후원자들을<br />만나지 못한 어린이들이 있습니다.<br />결연을 기다리고 있는 어린이들을 도와주세요.</p>
							</li>
							<li>
								<p class="subject">어린이센터 설립 후원</p>
								<p class="con">컴패션어린이센터를 통해 어린이들은 전인적인 양육을 받습니다.<br />더 많은 어린이들이 혜택을 받을 수 있도록 신규 어린이센터<br />설립을 지원해주세요.</p>
							</li>
							<li>
								<p class="subject">양육보완 프로그램 후원</p>
								<p class="con">의료지원, 재난구호, 환경 개선 등의<br />꼭 필요한 부분을 지원해주세요.</p>
							</li>
							<li>
								<p class="subject">태아&middot;영아생존프로그램 후원</p>
								<p class="con">태아&middot;영아 사망률이 높은 지역에 거주하는 아기와 엄마를 살리고,<br />건강한 삶을 영위할 수 있도록 도와주세요.</p>
							</li>
						</ul>

						<a href="https://app.smartsheet.com/b/form?EQBCT=f55cd9f7170f4e029c07099fbb2c401b" target="_blank" class="btn_type1">문의하기</a>
					</div>
				</div>
				<!--// 후원금 사용 -->


			</div>
			
			<!-- 캠페인 컨텐츠 -->
			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->

		
    </section>


</asp:Content>
