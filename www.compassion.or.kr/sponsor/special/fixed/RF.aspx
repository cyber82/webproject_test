﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RF.aspx.cs" Inherits="sponsor_special_fixed_rf" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/common/js/site/moveToMobile.js"></script>
	<script type="text/javascript" src="/sponsor/special/fixed/fixed.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="campaignId" />
	<input type="hidden" runat="server" id="frequency" />
	<input type="hidden" runat="server" id="amount" />
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>재난 <em>구호</em></h1>
				<span class="desc">주제별 다양한 나눔을 선택하여 후원하실 수 있습니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 sponsor">

		<!-- 비주얼 -->
			<div class="bgContent sf_cam_visual" style="background:url('/common/img/page/sponsor/campaign2_visual.jpg') no-repeat center;">

				<div class="bg_dim padding1">
					<div class="w980">
						<p class="tit">재난으로 고통받는 어린이 후원</p>
						<span class="bar"></span>
						<p class="con">재난 구호 Disaster Relief</p>

					</div>
				</div>

			</div>
			<!--// 비주얼 -->

			<!-- 그래프영역 -->
			<div class="sf_cam_graph static">
				<div class="w980 clear2 relative">

					<div class="sel_type">
						<span class="radio_ui">
							<asp:PlaceHolder runat="server" ID="ph_sf_is_regular" Visible="false">
							<span class="box">
								<input type="radio" id="sf_is_regular"  value="정기" name="frequency" class="css_radio" />
								<label for="sf_is_regular" class="css_label">정기후원</label>
							</span>
							</asp:PlaceHolder>
							<asp:PlaceHolder runat="server" ID="ph_sf_is_temporary" Visible="false">
							<span class="box">
								<input type="radio" id="sf_is_temporary" value="일시" name="frequency" class="css_radio" />
								<label for="sf_is_temporary" class="css_label">일시후원</label>
							</span>
							</asp:PlaceHolder>
						</span>
					</div>

					<script type="text/javascript">
						$(function () {
							// 바로후원하기, sns공유버튼 스크롤 fixed
							$(window).scroll(function () {
								if ($(this).scrollTop() > 850) {
									$(".btn_dir_sponsor").addClass("fixed");
								} else {
									$(".btn_dir_sponsor").removeClass("fixed");
								}
							});

						
						})
					</script>

					
					<div class="amountWrap">

						<div class="inblock clear2">
							
							<span class="amount">
								<label for="dsp_amount" class="hidden">금액 입력</label>
								<input type="text" id="dsp_amount" value="20,000" class="input_amount number_only use_digit" maxlength="10" />
								<span class="won">원</span>
							</span>
							
							<div class="btn_dir_sponsor">
								<a href="#" class="btn_b_type2" ng-click="showPay($event)">바로 후원하기</a>

								
								<!-- 공유하기버튼 -->
								<span class="sns_ani down">
									<button class="common_sns_share">공유하기</button>
									<span class="common_sns_group">
										<span class="wrap">
											<a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
											<a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
											<a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
											<a href="#" title="url 공유" data-role="sns" data-provider="copy" class="sns-copy sns_url">url 공유</a>
										</span>
									</span>
									
								</span>
								<!--// -->
							</div>
						</div>
					</div>

					

				</div>
			</div>
			<!--// 그래프영역 -->
			
			<!-- 캠페인 컨텐츠 -->
			<div class="sf_cam_static">
                <style>
                    .sf_cam_static .disaster { margin-top:50px; text-align:center; }
                    .sf_cam_static .disaster .direction { display:inline-block; vertical-align: top; padding-top: 70px; }
                    .sf_cam_static .disaster .direction.prev { margin-right:20px; }
                    .sf_cam_static .disaster .direction.next { margin-left:20px; }
                    .sf_cam_static .disaster .content { border:1px solid #666666; text-align:center; width:800px; display:inline-block; max-height: 190px; overflow:hidden; position:relative; height:190px; }
                    .sf_cam_static .disaster .content .container { width:800px; position:absolute; }
                    .sf_cam_static .disaster .content .container .tit { font-size:22px; margin-top:20px; }
                    .sf_cam_static .disaster .content .container .con { overflow-y:auto; max-height:96px; margin-bottom:20px; }
                </style>
                <script>
                    var currRFPage = 0;
                    function fn_Move(obj, dir) {
                        var sl = parseInt($('.disaster .content').scrollLeft(), 10) + (dir == 'prev' ? -800 : 800);
                        $('.disaster .content').animate({ scrollLeft: sl }, 500, function () {
                            var pos = parseInt($('.disaster .content').scrollLeft(), 10);
                            var cnt = parseInt('<%=ViewState["rowCount"].ToString()%>', 10);
                            //alert(pos + "," + (pos >= 800 * (cnt - 1)));
                            var type = $(obj).attr("alt");
                            if (type == "previous") { // 이전
                                //음수가 안나오게 0보다 클때만 -1함.
                                if (currRFPage > 0) currRFPage = currRFPage - 1;
                            } else { // 다음
                                //최대 cnt-1 값만큼만 올림
                                if (currRFPage < (cnt - 1)) currRFPage = currRFPage + 1; 
                            }
                            //전체 개수가 1개면 이벤트 일으키지안음
                            if (cnt > 1) {
                                //첫번째 페이지면, 이전 버튼 비활성화
                                if (currRFPage == 0) {
                                    $(obj).attr('src', '/common/img/btn/prev_1.png').css('cursor', 'default');
                                }
                                else {
                                    $('.direction.prev >img').attr('src', '/common/img/btn/prev_7.png').css('cursor', 'pointer');;
                                }
                                //페이지가 마지막 페이지면 다음 버튼 비활성화
                                if (currRFPage == (cnt - 1)) {
                                    $(obj).attr('src', '/common/img/btn/next_1.png').css('cursor', 'default');
                                }
                                else {
                                    $('.direction.next >img').attr('src', '/common/img/btn/next_7.png').css('cursor', 'pointer');;
                                }
                            }
                            //if (pos < 800) {
                            //    $(obj).attr('src', '/common/img/btn/prev_1.png').css('cursor', 'default');
                            //}
                            //else {
                            //    $('.direction.prev >img').attr('src', '/common/img/btn/prev_7.png').css('cursor', 'pointer');;
                            //}

                            //if (pos >= 800 * (cnt - 1)) {
                            //    $(obj).attr('src', '/common/img/btn/next_1.png').css('cursor', 'default');
                            //}
                            //else {
                            //    $('.direction.prev >img').attr('src', '/common/img/btn/prev_7.png').css('cursor', 'pointer');;
                            //    $('.direction.next >img').attr('src', '/common/img/btn/next_7.png').css('cursor', 'pointer');;
                            //}
                            //if (pos == 0) {
                            //    $(obj).attr('src', '/common/img/btn/prev_1.png').css('cursor', 'default');
                            //}
                            //else if (pos >= 800 * (cnt - 1)) {
                            //    $(obj).attr('src', '/common/img/btn/next_1.png').css('cursor', 'default');
                            //}
                            //else {
                            //    $('.direction.prev >img').attr('src', '/common/img/btn/prev_7.png').css('cursor', 'pointer');;
                            //    $('.direction.next >img').attr('src', '/common/img/btn/next_7.png').css('cursor', 'pointer');;
                            //}
                        });
                    }
                </script>
                <div class="w980 disaster">
                    <p class="sub_tit">재난&#149;재해소식</p>
                    <div class="direction prev">
                        <img src="/common/img/btn/prev_1.png" alt="previous" onclick="fn_Move(this, 'prev')" style="cursor:default;">
                    </div>
					<div class="content">
                        <asp:Repeater runat="server" ID="repeater">
						    <ItemTemplate>
                                <div class="container" style="top:0; left:<%# Container.ItemIndex * 800 %>px;">
                                    <p class="sub_tit tit"><%#Eval("subject") %></p>
					                <p class="sub_con con"><%# ((string)Eval("content")).Replace("\n", "<br/>") %></p>
                                </div>
						    </ItemTemplate>
					    </asp:Repeater>
					</div>
                    <div class="direction next">
                        <% 
                            if(ViewState["rowCount"] == null || (int)ViewState["rowCount"] < 2)
                            {
                        %>
                            <img src="/common/img/btn/next_1.png" alt="next" onclick="fn_Move(this, 'next')" style="cursor:default;">
                        <%
                            } else { 
                        %>
                            <img src="/common/img/btn/next_7.png" alt="next" onclick="fn_Move(this, 'next')" style="cursor:pointer;">
                        <%  } %>
                    </div>
				</div>
    
                <%=sf_content %>

				<!-- 다운로드 -->
				<div class="report" ng-if="report.total > 0">
					<div class="w980">
						<p class="tit">생명을 살리는 뉴스레터</p>

						<div class="tableWrap1">
							<table class="tbl_type1 campaign mb40">
								<caption>자료 다운로드 테이블</caption>
								<colgroup>
									<col style="width:83%" />
									<col style="width:17%" />
								</colgroup>
								<tbody>
									<tr ng-repeat="item in report.list">
										<td>{{item.sr_title}} <span class="icon_new" ng-show="item.is_new">new</span></td>
										<td><a ng-href="{{item.sr_file}}" class="btn_s_type4"><span class="ic_down"></span>Download</a></td>
									</tr>
									<!-- 등록된 뉴스레터가 없을때 -->
									<tr ng-if="report.total == 0">
										<td colspan="2" class="no_content">등록된 뉴스레터가 없습니다.</td>
									</tr>
									<!--//  -->
								</tbody>
							</table>
						</div>

						<button class="btn_com_more" ng-click="report.showMore($event)" ng-show="report.total > report.list.length">더보기</button>

					</div>
				</div>
				<!--// 다운로드 -->

			</div>
			<!-- 캠페인 컨텐츠 -->
			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->

		
    </section>


</asp:Content>
