﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="temporary.aspx.cs" Inherits="sponsor_special_wedding_temporary" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/sponsor/pay/temporary/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/sponsor/pay/temporary/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/sponsor/pay/temporary/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
	<script type="text/javascript" src="/sponsor/pay/temporary/default.js"></script>
	<script type="text/javascript" src="/sponsor/special/wedding/temporary.js"></script>
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />
	
	<input type="hidden" id="gender" runat="server"  />
	<input type="hidden" id="hd_auth_domain" runat="server"  />
	<input type="hidden" id="hd_age" runat="server"  />

	<input type="hidden" id="amount" runat="server"  />
	
	<input type="hidden" runat="server" id="jumin" value="" />		
	<input type="hidden" runat="server" id="ci" value="" />		<!-- 본인인증 CI -->
	<input type="hidden" runat="server" id="di" value="" />		<!-- 본인인증 DI -->
	<input type="hidden" runat="server" id="cert_gb" value="" />		<!-- 본인인증 수단 -->
	
	<input type="hidden" runat="server" id="user_class" value="20세이상" />
	<input type="hidden" runat="server" id="hdState" value="" />
	<input type="hidden" runat="server" id="hdLocation" value="" />
	<input type="hidden" runat="server" id="hdManageType" value="" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />		<!-- 회원 기본주소와 동기화 하는 기능이 있는경우 hfAddressType 가 필요함 -->
	<input type="hidden" runat="server" id="hdCI" value="" />	<!-- 기존 등록된 본인인증 CI -->
	
	<div style="display:none">
	<input type="checkbox" runat="server" id="addr_overseas" />해외에 거주하고 있어요
	<input type="text" runat="server" id="user_name" />		<!-- 결제시 사용 -->
	</div>

	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>결혼 <em>첫 나눔</em></h1>
				<span class="desc">나눔으로 시작한 결혼으로 특별한 결혼을 기념하세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents sponsor">

			<div class="special payment">

				<div class="w980">

					<!-- visual -->
					<div class="bgContent birth_online marriage3">

						<p class="tit">
							<span>태아&middot;영아생존프로그램 일시후원 (10만 원 이상)</span><br />
							아이티 '까르푸 센터' 49 가정의 엄마와 아기 살리기
						</p>
						<span class="bar"></span>
						<p class="con">
							아이티 까르푸(Carrefour) 태아&middot;영아생존센터에 있는 엄마와 아기의 생명을 살리고 건강하게 양육합니다.<br />
							가장 연약한 존재인 아기들에게 소중한 생명을 선물해주세요.<br />
							<span>
								<span class="field">위치</span><span class="mr60">아이티 까르푸 지역</span>
								<span class="field">프로그램 시작년도</span><span class="mr60">2010년</span>
								<span class="field">수혜자</span><span>엄마와 아기 49 가정</span>
							</span>
						</p>

					</div>
					<!--// visual -->

					<div class="table_tit" style="height:30px;">
						<span class="nec_info">표시는 필수입력 사항입니다.</span>
					</div>

					<script type="text/javascript">
						$(function () {
							// 탭메뉴 on/off
							$(".tab_select button").click(function () {
								$(this).parent().find("button").removeClass("on");
								$(this).addClass("on");
							});
						})
					</script>


					<!--// 신청정보 -->
					<div class="tableWrap2 mb40">
						<table class="tbl_type1 marriage">
							<caption>신청정보 입력 테이블</caption>
							<colgroup>
								<col style="width:20%" />
								<col style="width:80%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><span class="nec">신청자 정보</span></th>
									<td>

										<label for="txtBrideName" class="hidden">신부이름 입력</label>
										<asp:TextBox ID="txtBrideName" runat="server" MaxLength="25" placeholder="신부이름" cssclass="input_type2 mb10" style="width:400px"></asp:TextBox>

										<label for="txtGroomName" class="hidden">신랑이름 입력</label>
										<asp:TextBox ID="txtGroomName" runat="server" MaxLength="25" placeholder="신랑이름" cssclass="input_type2 mb10" style="width:400px"></asp:TextBox>

										<div class="clear2 relative">
										<label for="txtWeddingDate" class="hidden">결혼기념일</label>
										<asp:TextBox ID="txtWeddingDate" runat="server" MaxLength="10" placeholder="결혼기념일" cssclass="input_type2 mb10" style="width:200px"></asp:TextBox>
										</div>
										<div class="clear2 relative">
										<label for="txtPhone" class="hidden">휴대폰번호 입력</label>
										<asp:TextBox ID="txtPhone" runat="server" MaxLength="11" placeholder="휴대폰번호(-없이 입력)" cssclass="input_type2 mb10 number_only" style="width:200px"></asp:TextBox>
										</div>
										<div class="clear2 relative">
										<label for="txtEmail" class="hidden">이메일 입력</label>
										<asp:TextBox ID="txtEmail" runat="server" MaxLength="100" placeholder="이메일" cssclass="input_type2 mb10" style="width:400px"></asp:TextBox>
										</div>
										<div class="btn_attach clear2 relative">
											<input type="text" runat="server" id="lb_file_path" class="input_type1 fl mr10" disabled value="" placeholder="결혼사진" style="width:400px;background:#fdfdfd;border:1px solid #d8d8d8;"  />
											<label for="lb_file_path" class="hidden"></label>
											<a href="#" class="btn_type8 fl" id="btn_file_path"><span>파일선택</span></a>
										</div>
										<p class="pt20">
											<span class="fc_black">사진 첨부 시 주의사항</span><br />
											<span class="s_con1">
												세로 사진을 올려주세요. 세로 사진이 기념증서에 알맞게 제작됩니다. (1024x768 해상도 이상의 세로 사진을 권장합니다.)
											</span><br />
											<span class="s_con1">400KB 이상의 사진이 예쁘게 제작됩니다. 핸드폰 사진은 다소 해상도, 색상이 떨어질 수 있습니다.</span><br />
											<span class="s_con1">첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</span><br />
											<span class="s_con1 fc_red">기념증서 제작 및 발송은 약 2주정도 소요됩니다.</span>
										</p>


									</td>
								</tr>
								<tr>
									<th scope="row"><span class="nec">배송지 정보</span></th>
									<td>
										<input type="hidden" id="addr_domestic_zipcode" runat="server" />
										<input type="hidden" id="addr_domestic_addr1" runat="server" /> 
										<input type="hidden" id="addr_domestic_addr2" runat="server" />	


										<label for="post" class="hidden">주소찾기</label>
										<input type="text" id="post" disabled class="input_type2 mr10" placeholder="주소" style="width:483px; background:#fdfdfd;border:1px solid #d8d8d8;" />
										<a href="#" class="btn_s_type1" id="btn_find_addr"><span>주소찾기</span></a>
										

										<p id="addr_road" class="fs14 mt15"></p>
										<p id="addr_jibun" class="fs14 mt10"></p>

									</td>
								</tr>
								<tr>
									<th scope="row"><span class="nec">후원금액</span></th>
									<td>
										<div class="relative">
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="100000"><span>100,000원</span></button>
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="300000"><span>300,000원</span></button>
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="500000"><span>500,000원</span></button><br />
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="1000000"><span>1,000,000원</span></button>
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="2000000"><span>2,000,000원</span></button>
											<button class="btn_s_type4 btn_amount pay_amount" data-amount="3650000"><span>3,650,000원</span></button><br />

											<label for="pay_amount_custom" class="hidden">후원금액 직접입력</label>
											<input type="text" id="pay_amount_custom" class="pay_amount_custom number_only input_type2" maxlength="8" value="" placeholder="직접입력(10만원이상 입력 가능)" style="width:400px" />

											<p class="pt10"><span class="guide_comment2 pay_coment" style="display:none;">'결혼 첫나눔'을 통한 후원금은 10만원 이상 입력 가능합니다.</span></p>

											<div class="amount_sum">총 결제 금액<em id="txt_amount">0</em>원</div>
										</div>
									</td>
								</tr>
								<tr class="hide_overseas">
									<th scope="row"><span class="nec">본인인증</span></th>
									<td>
										<asp:PlaceHolder runat="server" ID="ph_no_jumin" Visible="false">
										<p class="mb10">
											<span class="checkbox_ui">
												<input type="checkbox" class="css_checkbox"  id="p_receipt_pub_ok" name="p_receipt_pub" runat="server"  checked />
												<label for="p_receipt_pub_ok" class="css_label font2">국세청 연말정산 영수증 신청</label>
											</span>
										</p>

										<div id="func_name_check">
											<label class="hidden">주민등록번호입력</label>
											<input type="text" class="input_type2 mb10 number_only" value="" data-id="jumin1" runat="server" id="jumin1" maxlength="13" placeholder="주민등록번호(-없이 입력)" style="width:400px" />
											<button class="btn_s_type7 ml5 mb10" id="btn_name_check"><span>실명인증</span></button>
										</div>

										<p id="msg_name_check" style="display:none"><span class="guide_comment1"></span></p>
										</asp:PlaceHolder>
										<asp:PlaceHolder runat="server" ID="ph_yes_jumin" Visible="false">
										<div class="receipt_infoBox">
											<p class="txt1">기존에 등록하신 <em class="fc_blue">국세청 연말정산 영수증 신청 정보</em>가 있습니다.</p>
											<p>기존 정보에 연말정산 정보가 추가로 등록됩니다.</p>
										</div>
										</asp:PlaceHolder>

										<asp:PlaceHolder runat="server" ID="ph_cert" Visible="false" >
										<p class="fs14 mb20 func_cert">온/오프라인 기존 후원정보 확인을 위해 본인 인증을 진행해 주세요.</p>

										<!-- 휴대폰,아이핀인증 -->
										<div class="cert clear2 mb20 func_cert">
											<div class="fl">
												<div class="box phone">
													<p class="tit">휴대폰 인증</p>
													<p class="con">
														본인 명의의 휴대전화번호로 본인인증을<br />
														원하시는 분은 아래 버튼을 클릭해주세요
													</p>

													<a href="#" id="btn_cert_by_phone" class="btn_s_type2">휴대폰 인증하기</a>
												</div>
											</div>

											<div class="fr">
												<div class="box ipin">
													<p class="tit">아이핀(i-PIN) 인증</p>
													<p class="con">
														가상 주민등록번호 아이핀으로 본인인증을<br />
														원하시는 분은 아래 버튼을 클릭해주세요
													</p>

													<a href="#" id="btn_cert_by_ipin" class="btn_s_type2">아이핀 인증하기</a>
												</div>
											</div>
										</div>

										<p id="msg_cert" style="display:none"><span class="guide_comment1">정보 확인이 완료되었습니다.</span></p>
										
										<!--// 휴대폰,아이핀인증 -->
										

										<ul class="func_cert">
											<li><span class="s_con1">본인명의의 휴대폰으로만 본인인증이 가능합니다.</span></li>
											<li><span class="s_con1">아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다.</span></li>
										</ul>
										</asp:PlaceHolder>
									</td>
								</tr>
								<tr>
									<th scope="row"><span class="nec">결제방법</span></th>
									<td>
										<span class="radio_ui mb15">
											<input type="radio" id="payment_method_card" runat="server" name="payment_method"  class="css_radio" checked />
											<label for="payment_method_card" class="css_label" style="width:170px">신용카드 결제</label>

											<input type="radio" id="payment_method_cms" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_cms" class="css_label" style="width:170px">실시간 계좌이체</label>

											<input type="radio" id="payment_method_oversea" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_oversea" class="css_label" style="width:170px">해외발급 카드</label>
										</span>
										<span class="radio_ui">
											<input type="radio" id="payment_method_kakao" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_kakao" class="css_label relative" style="width:170px"><img src="/common/img/icon/kakaopay.jpg" class="kakao" alt="kakao pay" onclick="$('#payment_method_kakao').trigger('click')"/>kakaopay</label>

											<input type="radio" id="payment_method_payco" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_payco" class="css_label relative" style="width:170px"><img src="/common/img/icon/payco.jpg" class="payco" alt="payco" onclick="$('#payment_method_payco').trigger('click')"/>payco</label>

											<input type="radio" id="payment_method_phone" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_phone" class="css_label" style="width:170px">휴대폰 결제</label>
										</span>
									</td>
								</tr>
							</tbody>
						</table>

					</div>
					<!--// 신청정보 -->

					<div class="box_type4 padding1 mb40">
						<p class="mb5 fc_black">※ 결제 전 꼭 확인해주세요!</p>
						<ul>
							<li><span class="s_con1">후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</span></li>
							<li><span class="s_con1">결제관련 문의 : <em class="fc_blue">후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_blue">info@compassion.or.kr</a></em></span></li>
						</ul>
					</div>

					<div class="tac"><asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" cssclass="btn_type1">신청 및 결제</asp:LinkButton></div>

				</div>

			</div>
			<div class="h100"></div>
			
		</div>	
		<!--// e: sub contents -->

		
    </section>

</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">

	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="payco_form" />
	<uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kakaopay_form" />
</asp:Content>