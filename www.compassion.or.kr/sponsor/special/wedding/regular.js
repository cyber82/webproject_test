﻿$(function () {

	$extPage.init();

});

function jusoCallback(zipNo, addr1, addr2, jibun) {
	// 실제 저장 데이타
	$("#addr_domestic_zipcode").val(zipNo);
	$("#addr_domestic_addr1").val(addr1 + "//" + jibun);
	$("#addr_domestic_addr2").val(addr2);

	// 화면에 표시
	$("#post").val(zipNo);
	$("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
	$("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);
}
var $extPage = {

	timer: null,
	cert_target: "",		// me , parent
	checked_account_val: "",
	is_checked_account: false,

	init: function () {

		$motive.init($("#motiveCode"), $("#motiveName"), $("#motive1"), $("#motive2"));
		
		$page.init();
		
		$("#post").val($("#addr_domestic_zipcode").val());
		
		// 컴파스의 데이타를 불러오는경우 
		if ($("#dspAddrDoro").val() != "") {
			$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
			if ($("#dspAddrJibun").val() != "") {
				$("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
			}
		} else if ($("#addr_domestic_addr1").val() != "") {
			if ($("#addr_domestic_addr1").length) {

				addr_array = $("#addr_domestic_addr1").val().split("//");
				if (addr_array[0] != "") {
					$("#addr_road").text("[도로명주소] " + addr_array[0] + " " + $("#addr_domestic_addr2").val());
				}
				if (addr_array[1]) {
					$("#addr_jibun").text("[지번주소] " + addr_array[1] + " " + $("#addr_domestic_addr2").val());
				}
			}
		}

		var uploader = attachUploader("btn_file_path");
		uploader._settings.data.fileDir = $("#upload_root").val();
		uploader._settings.data.fileType = "image";
		uploader._settings.data.limit = 5120;

		$('#txtWeddingDate').datepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: '',
			numberOfMonths: 1,
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
			showTime: false,
			showHour: false,
			showMinute: false,
			closeText: '닫기',
			currentText: '오늘',

			buttonImageOnly: false,
			changeYear: true,
			onSelect: function (text, e) {
				$(this).datepicker("hide");

				$("#pn_child").show();

			}
		});


		//실명인증등
		$("#btn_name_check").click(function () {
			cert_nameCheck($("#user_name"), $("[data-id=jumin1]"), null, $("#hd_auth_domain").val());

			return false;
		})

		$("#txtWeddingDate").keydown(function (e) {
			if (e.keyCode == 8) {
				$("#pn_child").hide();
				$(this).val("");
				$(this).datepicker("hide");
			}
			return false;
		});

		/*
		$("#btn_find_addr").unbind("click");
		$("#btn_find_addr").click(function () {
			cert_setDomain();
				var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");

			return false;
		})
		*/

		// 가입하기 버튼
		$("#btn_submit").unbind("click");
		$("#btn_submit").click(function () {

			return $extPage.onSubmit();

		});


	},

	// 확인
	onSubmit: function () {
		if (!$page.onSubmit()) {
			return false;
		}

		if (!validateForm([
			{ id: "#txtGroomName", msg: "신랑이름을 입력해 주세요" },
			{ id: "#txtBrideName", msg: "신부이름을 입력해 주세요" },
			{ id: "#txtWeddingDate", msg: "결혼기념일을 선택해 주세요" },
			{ id: "#lb_file_path", msg: "결혼사진을 선택해 주세요" },
			{ id: "#txtPhone", msg: "휴대전화번호를 입력해 주세요", type: "phone" },
			{ id: "#txtEmail", msg: "이메일주소를 입력해 주세요", type: "email" },
			{ id: "#hdChildMasterId", msg: "후원하실 아동을 검색해주세요" }
		])) {
			return false;
		}

		return true;
	}


};

var attachUploader = function (button) {
	return new AjaxUpload(button, {
		action: '/common/handler/upload',
		responseType: 'json',
		onChange: function () {
		},
		onSubmit: function (file, ext) {
			this.disable();
		},
		onComplete: function (file, response) {

			this.enable();
			console.log(file, response);
			if (response.success) {

				//$("#file_path").val(response.name);
				$("#file_path").val(file);
				$("#lb_file_path").val(file);
				//$("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
				//	$(".temp_file_size").val(response.size);

			} else
				alert(response.msg);
		}
	});
};

(function () {
	var app = angular.module('cps.page', []);
	app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		$scope.child = null;
		$scope.searchChild = function ($event) {
			$event.preventDefault();

		    //[이종진]로딩추가
			loading.show();
			$http.get("/api/tcpt.ashx?t=wedding-child", { params: { birth: $("#txtWeddingDate").val() } }).success(function (r) {

				if (r.success) {
					if (r.message != "" && r.message) {
						alert(r.message);
					}

					$scope.child = $.extend({}, r.data[0]);
					$scope.child.birthdate = new Date($scope.child.birthdate);
					$("#hdChildMasterId").val($scope.child.childmasterid);
				    //[이종진] 추가
					$("#hdChildKey").val($scope.child.childkey);
					$("#hdChildGlobalId").val($scope.child.childglobalid);
					console.log($scope.child);
				} else {
				    alert(r.message);
				}
			    //[이종진]로딩추가
				loading.hide();
			});
		    
		}

		// postback 처리
		if ($("#hdChildMasterId").val() != "") {
			$http.get("/api/tcpt.ashx?t=get", { params: { childmasterid: $("#hdChildMasterId").val() } }).success(function (r) {

				console.log("hdChildMasterId", r);
				if (r.success) {
					$scope.child = $.extend({}, r.data);
					$scope.child.birthdate = new Date($scope.child.birthdate);
				} else {
					alert(r.message);
				}
			});
		}

	});

})();