﻿$(function () {

	$extPage.init();

});


function jusoCallback(zipNo, addr1, addr2, jibun) {
	// 실제 저장 데이타
	$("#addr_domestic_zipcode").val(zipNo);
	$("#addr_domestic_addr1").val(addr1 + "//" + jibun);
	$("#addr_domestic_addr2").val(addr2);

	// 화면에 표시
	$("#post").val(zipNo);
	$("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
	$("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);

};

var $extPage = {

	init: function () {

		$page.init();

		var uploader = attachUploader("btn_file_path");
		uploader._settings.data.fileDir = $("#upload_root").val();
		uploader._settings.data.fileType = "image";
		uploader._settings.data.limit = 2048;

		$("#post").val($("#addr_domestic_zipcode").val());

		// 컴파스의 데이타를 불러오는경우 
		if ($("#dspAddrDoro").val() != "") {
			$("#addr_road").text("[도로명주소] (" + $("#addr_domestic_zipcode").val() + ") " + $("#dspAddrDoro").val());
			if ($("#dspAddrJibun").val() != "") {
				$("#addr_jibun").text("[지번] (" + $("#addr_domestic_zipcode").val() + ") " + $("#dspAddrJibun").val());
			}

		} else if ($("#addr_domestic_addr1").val() != "") {

			if ($("#addr_domestic_addr1").length) {

				addr_array = $("#addr_domestic_addr1").val().split("//");
				if (addr_array[0] != "") {
					$("#addr_road").text("[도로명주소] " + addr_array[0] + " " + $("#addr_domestic_addr2").val());
				}
				if (addr_array[1]) {
					$("#addr_jibun").text("[지번주소] " + addr_array[1] + " " + $("#addr_domestic_addr2").val());
				}
			}
		}

		$(".pay_amount").click(function () {

			var amount = parseInt($(this).data("amount"));
			
			$("#amount").val(amount);
			$("#txt_amount").html(amount.format());
			
			return false;

		});

		$(".pay_amount_custom").focus(function () {
			$(this).val("");
		}).blur(function () {
			var val = $(this).val();
			if (isNaN(val) || parseInt(val) < 100000) {
				$(".pay_coment").show();
				//alert("10만원이상 입력해 주세요");
				$("#amount").val("");
				$(this).val("");
				$(this).focus();
				return;
			} else {
				$(".pay_coment").hide();
			}

			if (parseInt(val) % 1000 > 0) {
				alert("천원단위로 입력해 주세요");
				$("#amount").val("");
				$(this).val("");
				$(this).focus();
				return;
			}

			$("#amount").val(val);
			$("#txt_amount").html(val.format());
		})

		$('#txtWeddingDate').datepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: '',
			numberOfMonths: 1,
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
			showTime: false,
			showHour: false,
			showMinute: false,
			closeText: '닫기',
			currentText: '오늘',

			buttonImageOnly: false,
			changeYear: true,
			onSelect: function (text, e) {
				$(this).datepicker("hide");


			}
		});

		// 버튼
		$("#btn_submit").unbind("click");
		$("#btn_submit").click(function () {

			return $extPage.onSubmit();

		});
		

		// postback
		if ($("#amount").val() != 0) {

			$("#txt_amount").html($("#amount").val().format());
		} else {
			$($(".pay_amount")[1]).trigger("click");		// default : 30만원
		}

	},

	// 확인
	onSubmit: function () {

		if (!$page.onSubmit()) {
			return false;
		}

		if (!validateForm([
				{ id: "#txtGroomName", msg: "신랑이름을 입력해 주세요" },
				{ id: "#txtBrideName", msg: "신부이름을 입력해 주세요" },
				{ id: "#txtWeddingDate", msg: "결혼기념일을 선택해 주세요" },
				{ id: "#lb_file_path", msg: "결혼사진을 선택해 주세요" },
				{ id: "#txtPhone", msg: "휴대전화번호를 입력해 주세요", type: "phone" },
				{ id: "#txtEmail", msg: "이메일주소를 입력해 주세요", type: "email" },
				{ id: "#addr_domestic_zipcode", msg: "배송지정보를 선택해 주세요" },
				{ id: "#addr_domestic_addr2", msg: "배송지 상세주소를 입력해주세요" }
		])) {
			return false;
		}


		if ($("#amount").val() == "") {
			alert("후원금액을 선택해주세요");
			return false;
		}
		
		if (isNaN($("#amount").val()) || parseInt($("#amount").val()) < 1) {
			$(".pay_coment").show();
			//alert("후원금액은 10만원 이상 지정가능합니다.");
			return false;
		} else {
			$(".pay_coment").hide();
		}

		return true;
	}


}

var attachUploader = function (button) {
	return new AjaxUpload(button, {
		action: '/common/handler/upload',
		responseType: 'json',
		onChange: function () {
		},
		onSubmit: function (file, ext) {
			this.disable();
		},
		onComplete: function (file, response) {

			this.enable();
			console.log(file,response);
			if (response.success) {
				
				//$("#file_path").val(response.name);
				$("#file_path").val(file);
				$("#lb_file_path").val(file);
				//$("#lb_file_path").val(response.name.replace(/^.*[\\\/]/, ''));
				//	$(".temp_file_size").val(response.size);

			} else
				alert(response.msg);
		}
	});
}