﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="regular.aspx.cs" Inherits="sponsor_special_wedding_regular" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form_temporary" %>
<%@ Register Src="/pay/kcp_batch_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/cms_form.ascx" TagPrefix="uc" TagName="cms_form" %>
<%--<%@ Register Src="/sponsor/pay/regular/kcp_batch_form.ascx" TagPrefix="uc" TagName="kcp_form" %>--%>
<%--<%@ Register Src="/sponsor/pay/temporary/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form_temporary" %>--%>
<%--<%@ Register Src="/sponsor/pay/regular/cms_form.ascx" TagPrefix="uc" TagName="cms_form" %>--%>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
	<script type="text/javascript" src="/sponsor/pay/regular/default.js"></script>
	<script type="text/javascript" src="/common/js/site/motive.js"></script>
	<script type="text/javascript" src="/sponsor/special/wedding/regular.js"></script>
	
	<script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" id="hd_amount" runat="server"  />
	<input type="hidden" runat="server" id="file_path" value="" />
	<input type="hidden" runat="server" id="upload_root" value="" />

	<input type="hidden" id="user_name" runat="server"  />
	<input type="hidden" id="gender" runat="server"  />
	<input type="hidden" id="hd_auth_domain" runat="server"  />
	<input type="hidden" id="hd_age" runat="server"  />

	<input type="hidden" runat="server" id="jumin" value="" />		
	<input type="hidden" runat="server" id="ci" value="" />		<!-- 본인인증 CI -->
	<input type="hidden" runat="server" id="di" value="" />		<!-- 본인인증 DI -->
	<input type="hidden" runat="server" id="cert_gb" value="" />		<!-- 본인인증 수단 -->
	
	<input type="hidden" runat="server" id="user_class" value="20세이상" />
	<input type="hidden" runat="server" id="parent_cert" value="" />
	<input type="hidden" runat="server" id="parent_name" value="" />
	<input type="hidden" runat="server" id="parent_juminId" value="" />
	<input type="hidden" runat="server" id="parent_mobile" value="" />
	<input type="hidden" runat="server" id="parent_email" value="" />

	<input type="hidden" runat="server" id="exist_account" value="" />
	<input type="hidden" runat="server" id="hdManageType" value="" />
	<input type="hidden" runat="server" id="hdState" value="" />
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	<input type="hidden" runat="server" id="hdLocation" value="" />
	<input type="hidden" runat="server" id="hdOrganizationID" value="" />
	<input type="hidden" runat="server" id="hdSponsorId" value="" />
	<input type="hidden" runat="server" id="hdBirthDate" value="" />
	<input type="hidden" runat="server" id="hdChildMasterId" value="" />		<!-- 선택된 아동 아이디 -->
    <input type="hidden" runat="server" id="hdChildKey" value="" />		<!-- 선택된 아동 아이디 -->
    <input type="hidden" runat="server" id="hdChildGlobalId" value="" />		<!-- 선택된 아동 아이디 -->
	<input type="hidden" runat="server" id="zipcode" />
	<input type="hidden" runat="server" id="addr1" /> 
	<input type="hidden" runat="server" id="addr2" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
	<input type="hidden" runat="server" id="hdCI" value="" />	<!-- 기존 등록된 본인인증 CI -->
	<input type="hidden" runat="server" id="dmYN" value="Y" />
	<input type="hidden" runat="server" id="translationYN" value="N" />

	<asp:Panel runat="server" style="display:none">
		<input type="checkbox" runat="server" id="addr_overseas" />해외에 거주하고 있어요
		<asp:DropDownList runat="server" ID="ddlHouseCountry"></asp:DropDownList>
	</asp:Panel>

	<section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>결혼 <em>첫 나눔</em></h1>
				<span class="desc">나눔으로 시작한 결혼으로 특별한 결혼을 기념하세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents sponsor">

			<div class="special payment">

				<div class="w980">

					<!-- visual -->
					<div class="bgContent birth_online marriage2">

						<p class="tit">
							<span>월 45,000원씩 정기후원</span><br />
							결혼 기념일에 태어난 어린이 1:1 양육하기
						</p>
						<span class="bar"></span>
						<p class="con">
							후원자와 어린이가 1:1 결연을 맺어 어린이가 자립 가능한 성인이 될 때까지 후원하는 프로그램입니다.<br />
							후원자님께서 어린이의 손을 잡는 순간, 어린이의 삶에는 기적이 시작됩니다.<br />
						</p>

					</div>
					<!--// visual -->

					<div class="table_tit" style="height:30px;">
						<span class="nec_info">표시는 필수입력 사항입니다.</span>
					</div>

					<script type="text/javascript">
						$(function () {
							// 
							$(".tab_select button").click(function () {
								$(this).parent().find("button").removeClass("on");
								$(this).addClass("on");
							});
						})
					</script>

					<!--// 신청정보 -->
					<div class="tableWrap2 mb40">
						<table class="tbl_type1 marriage">
							<caption>신청정보 입력 테이블</caption>
							<colgroup>
								<col style="width:20%" />
								<col style="width:80%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><span class="nec">신청자 정보</span></th>
									<td>

										<label for="txtBrideName" class="hidden">신부이름 입력</label>
										<asp:TextBox ID="txtBrideName" runat="server" MaxLength="25" placeholder="신부이름" cssclass="input_type2 mb10" style="width:400px"></asp:TextBox>

										<label for="txtGroomName" class="hidden">신랑이름 입력</label>
										<asp:TextBox ID="txtGroomName" runat="server" MaxLength="25" placeholder="신랑이름" cssclass="input_type2 mb10" style="width:400px"></asp:TextBox>

										<div class="clear2 relative">
										<label for="txtWeddingDate" class="hidden">결혼기념일</label>
										<asp:TextBox ID="txtWeddingDate" runat="server" MaxLength="10" placeholder="결혼기념일" cssclass="input_type2 mb10" style="width:200px"></asp:TextBox>
										</div>
										<div class="clear2 relative">
										<label for="txtPhone" class="hidden">휴대폰번호 입력</label>
										<asp:TextBox ID="txtPhone" runat="server" MaxLength="11" placeholder="휴대폰번호(-없이 입력)" cssclass="input_type2 mb10 number_only" style="width:200px"></asp:TextBox>
										</div>
										<div class="clear2 relative">
										<label for="txtEmail" class="hidden">이메일 입력</label>
										<asp:TextBox ID="txtEmail" runat="server" MaxLength="1000" placeholder="이메일" cssclass="input_type2 mb10" style="width:400px"></asp:TextBox>
										</div>
										<div class="btn_attach clear2 relative">
											<input type="text" runat="server" id="lb_file_path" class="input_type1 fl mr10" disabled value="" placeholder="결혼사진" style="width:400px;background:#fdfdfd;border:1px solid #d8d8d8;"  />
											<label for="lb_file_path" class="hidden"></label>
											<a href="#" class="btn_type8 fl" id="btn_file_path"><span>파일선택</span></a>
										</div>
										<p class="pt20">
											<span class="fc_black">사진 첨부 시 주의사항</span><br />
											<span class="s_con1">
												세로 사진을 올려주세요. 세로 사진이 기념증서에 알맞게 제작됩니다. (1024x768 해상도 이상의 세로 사진을 권장합니다.)
											</span><br />
											<span class="s_con1">400KB 이상의 사진이 예쁘게 제작됩니다. 핸드폰 사진은 다소 해상도, 색상이 떨어질 수 있습니다.</span><br />
											<span class="s_con1">첨부파일 용량을 2MB이하로 해주세요. 첨부파일은 이미지파일(jpg, jpeg, gif, png)만 가능합니다.</span><br />
											<span class="s_con1 fc_red">기념증서 제작 및 발송은 약 2주정도 소요됩니다.</span>
										</p>


									</td>
								</tr>
								<tr>
									<th scope="row"><span class="nec">배송지 정보</span></th>
									<td>
										<input type="hidden" id="addr_domestic_zipcode" />
										<input type="hidden" id="addr_domestic_addr1"  /> 
										<input type="hidden" id="addr_domestic_addr2" />

										<label for="post" class="hidden">주소찾기</label>
										<input type="text" id="post" disabled class="input_type2 mr10" placeholder="주소" style="width:483px; background:#fdfdfd;border:1px solid #d8d8d8;" />
										<a href="#" class="btn_s_type1" id="btn_find_addr"><span>주소찾기</span></a>
										
										
										<p id="addr_road" class="fs14 mt15"></p>
										<p id="addr_jibun" class="fs14 mt10"></p>

									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// 신청정보 -->

					<div class="tac mb40" id="pn_child" style="display:none">
						<button class="btn_type1" ng-click="searchChild($event)"><span>우리 결혼기념일에 태어난 어린이를 검색합니다.</span></button>
					</div>

					<!-- 어린이검색 시 보여짐 -->
					<div class="complete" ng-if="child">

						<p class="wedding_child">우리 결혼기념일에 태어난 어린이</p>

						<table class="tbl_child mb40">
							<caption>어린이정보 및 후원금액 테이블</caption>
							<colgroup>
								<col style="width:30%" />
								<col style="width:70%" />
							</colgroup>
							<tbody>
								<tr>
									<td class="pic">
										<!-- 이미지사이즈 : 200 * 200 -->
										<span background-img="{{child.pic}}" data-default-image="/common/img/page/my/no_pic.png" style="background:no-repeat center;background-size:cover;background-position-y:0"></span>
									</td>
									<td class="info">
										<span class="label">1:1 어린이양육</span><span class="label">결혼 첫 나눔</span>
										<p class="name">{{child.namekr}}</p>

										<div>
											<span class="field">국가</span>
											<span class="data">{{child.countryname}}</span>
										</div>
										<div>
											<span class="field">생일</span>
											<span class="data">{{child.birthdate | date:'yyyy.MM.dd'}} ({{child.age}}세)</span>
										</div>
										<div>
											<span class="field">성별</span>
											<span class="data">{{child.gender}}</span>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="2" class="amount">
										<span class="field">총 결제 금액</span><span class="sum">45,000</span><span class="won">원/월</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// 어린이검색 시 보여짐 -->

					<!-- 결제정보 -->
					<div class="tableWrap2 mb30">
						<table class="tbl_type1">
							<caption>결제방법 선택 테이블</caption>
							<colgroup>
								<col style="width:20%" />
								<col style="width:80%" />
							</colgroup>
							<tbody>

								<tr class="hide_overseas">
									<th scope="row"><span class="nec">본인인증</span></th>
									<td>
										<asp:PlaceHolder runat="server" ID="ph_no_jumin" Visible="false">
										<p class="mb10">
											<span class="checkbox_ui">
												<input type="checkbox" class="css_checkbox"  id="p_receipt_pub_ok" name="p_receipt_pub" runat="server"  checked />
												<label for="p_receipt_pub_ok" class="css_label font2">국세청 연말정산 영수증 신청</label>
											</span>
										</p>

										<div id="func_name_check">
											<label class="hidden">주민등록번호입력</label>
											<input type="text" class="input_type2 mb10 number_only" value="" data-id="jumin1" runat="server" id="jumin1" maxlength="13" placeholder="주민등록번호(-없이 입력)" style="width:400px" />
											<button class="btn_s_type7 ml5 mb10" id="btn_name_check"><span>실명인증</span></button>
										</div>

										<p id="msg_name_check" style="display:none"><span class="guide_comment1"></span></p>
										</asp:PlaceHolder>
										<asp:PlaceHolder runat="server" ID="ph_yes_jumin" Visible="false">
										<div class="receipt_infoBox">
											<p class="txt1">기존에 등록하신 <em class="fc_blue">국세청 연말정산 영수증 신청 정보</em>가 있습니다.</p>
											<p>기존 정보에 연말정산 정보가 추가로 등록됩니다.</p>
										</div>
										</asp:PlaceHolder>

										<asp:PlaceHolder runat="server" ID="ph_cert" Visible="false" >
										<p class="fs14 mb20 func_cert">온/오프라인 기존 후원정보 확인을 위해 본인 인증을 진행해 주세요.</p>

										<!-- 휴대폰,아이핀인증 -->
										<div class="cert clear2 mb20 func_cert">
											<div class="fl">
												<div class="box phone">
													<p class="tit">휴대폰 인증</p>
													<p class="con">
														본인 명의의 휴대전화번호로 본인인증을<br />
														원하시는 분은 아래 버튼을 클릭해주세요
													</p>

													<a href="#" id="btn_cert_by_phone" class="btn_s_type2">휴대폰 인증하기</a>
												</div>
											</div>

											<div class="fr">
												<div class="box ipin">
													<p class="tit">아이핀(i-PIN) 인증</p>
													<p class="con">
														가상 주민등록번호 아이핀으로 본인인증을<br />
														원하시는 분은 아래 버튼을 클릭해주세요
													</p>

													<a href="#" id="btn_cert_by_ipin" class="btn_s_type2">아이핀 인증하기</a>
												</div>
											</div>
										</div>

										<p id="msg_cert" style="display:none"><span class="guide_comment1">정보 확인이 완료되었습니다.</span></p>
										
										<!--// 휴대폰,아이핀인증 -->
										

										<ul class="func_cert">
											<li><span class="s_con1">본인명의의 휴대폰으로만 본인인증이 가능합니다.</span></li>
											<li><span class="s_con1">아이핀(i-PIN)ID가 없는 경우 한국신용정보㈜에서 발급 후 사용 가능합니다.</span></li>
										</ul>
										</asp:PlaceHolder>
									</td>
								</tr>

								<tr>
									<th scope="row"><span class="nec">영문이름</span></th>
									<td>
										<span class="fl mr10">
											<span class="relative">
												<label for="last_name" class="hidden">영문성</label>
												<input type="text" maxlength="20" runat="server" id="last_name" class="input_type2" value="" style="width:150px" placeholder="영문 성" />
											</span>
										</span>
										<span class="fl">
											<span class="relative">
												<label for="first_name" class="hidden">영문이름</label>
												<input type="text" maxlength="20" runat="server"  id="first_name" class="input_type2" value="" style="width:240px" placeholder="영문 이름" />
											</span>
										</span>
									</td>
								</tr>
								<tr>
									<th scope="row"><span class="nec">종교</span></th>
									<td>
										<span class="radio_ui mb20">
											<input type="radio" runat="server" id="religion1" name="religion" class="css_radio religion" value="기독교"  checked />
											<label for="religion1" class="css_label">기독교</label>

											<input type="radio" runat="server" id="religion2" name="religion" value="천주교"  class="css_radio religion" />
											<label for="religion2" class="css_label ml40">천주교</label>

											<input type="radio" runat="server" id="religion3" name="religion" value="불교"  class="css_radio religion"  />
											<label for="religion3" class="css_label ml40">불교</label>

											<input type="radio" runat="server" id="religion4" name="religion" value="무교"  class="css_radio religion" />
											<label for="religion4" class="css_label ml40">없음</label>
										</span>
										<br />
										<div id="pn_church" runat="server">
										<label for="church_name" class="hidden">교회명 입력</label>
										<input type="text" runat="server" id="church_name" class="input_type2" maxlength="30" value="" placeholder="교회명 직접입력" style="width:400px" />
										<button class="btn_s_type2 ml5" id="btn_find_church"><span>교회찾기</span></button>
										</div>
									</td>
								</tr>
								
								<tr>
									<th scope="row"><span class="nec">후원정보 수신</span></th>
									<td>
										<script type="text/javascript">
											$(function () {
												$(".tab_select button").click(function () {
													$(this).parent().find("button").removeClass("on");
													$(this).addClass("on");
												});
											})
										</script>

										<p class="mb20">
											<span class="fs14 s_con1">입력하신 주소로 후원과 관련한 우편물을 수신하시겠습니까?</span>
											<span class="fs14 s_con1">단, 후원과 관련하여 후원자님께 중요하게 안내되어야 하는 우편물은 수신 여부와 상관없이 발송됩니다.</span>
										</p>
										<div class="tab_type3 tab_select" style="width:400px">
											<button style="width:200px" class="dmYN" data-value="Y"><span>예</span></button>
											<button style="width:200px" class="dmYN" data-value="N"><span>아니오</span></button>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row"><span class="nec">어린이 편지<br />번역 여부</span></th>
									<td>
										<div class="tab_type3 tab_select mb20" style="width:400px">
											<button style="width:200px" class="translationYN" data-value="N" class="on"><span>영문</span></button>
											<button style="width:200px" class="translationYN" data-value="Y" ><span>한글</span></button>
										</div>
										<p>
											<ul>
												<li><span class="s_con1">영문으로 선택하시면 어린이편지를 더 빨리 받으실 수 있습니다.</span></li>
												<li><span class="s_con1">어린이에게 편지를 보내실 때는 위 선택과 관계없이 한글 또는 영문으로 작성하실 수 있습니다.</span></li>
											</ul>
										</p>
									</td>
								</tr>
								<tr>
									<th scope="row"><span class="nec">후원 계기</span></th>
									<td>
										<div class="clear2">
											<input type="hidden" runat="server" id="motiveCode" />
											<input type="hidden" runat="server" id="motiveName" />

											<span class="sel_type2 fl mr10" style="width:165px;">
												<label for="motive1" class="hidden">미디어 선택</label>
												<select class="custom_sel" id="motive1" data-refresh="true">
												</select>
											</span>
											<span class="sel_type2 fl mr10" style="width:325px;">
												<label for="motive2" class="hidden">알게된 경로</label>
												<select class="custom_sel" id="motive2" data-refresh="true">
												</select>
											</span>
										</div>
										<div class="clear2" style="margin-top:5px;display:none" id="pn_motive2_etc">
												<input type="text"  class="input_type1" id="motive2_etc" style="width:500px" maxlength="30" placeholder="기타 후원계기를 입력해주세요"/>

											</div>
									</td>
								</tr>

								<asp:PlaceHolder runat="server" ID="ph_new_pay" >

								<tr>
									<th scope="row"><span class="nec">결제방법</span></th>
									<td>
										<span class="radio_ui mb15">
											<asp:PlaceHolder runat="server" ID="ph_payment_method_cms">
											<input type="radio" id="payment_method_cms" runat="server" name="payment_method" class="css_radio payment_method" data-type="cms" />
											<label for="payment_method_cms" class="css_label" style="width:170px">CMS 자동이체</label>
											</asp:PlaceHolder>
											<asp:PlaceHolder runat="server" ID="ph_payment_method_card">
											<input type="radio" id="payment_method_card" runat="server" name="payment_method"  class="css_radio payment_method" data-type="card" />
											<label for="payment_method_card" class="css_label mr30">신용카드 자동이체</label>
											</asp:PlaceHolder>
											<asp:PlaceHolder runat="server" ID="ph_payment_method_oversea">
											<input type="radio" id="payment_method_oversea" runat="server" name="payment_method"  class="css_radio payment_method" data-type="oversea_card" />
											<label for="payment_method_oversea" class="css_label">해외발급 카드 즉시결제</label>
											</asp:PlaceHolder>

											<asp:PlaceHolder runat="server" ID="ph_hidden_payment_method" Visible="false">
													<input type="radio" id="payment_method_giro" runat="server" />
													<input type="radio" id="payment_method_virtualaccount" runat="server" />
												</asp:PlaceHolder>
										</span>
										<!--
										<div>
											<span class="s_con1 mr20">해외카드를 이용하시는 후원자님께서는 즉시결제를 선택해주세요.</span>
											<a href="#" class="btn_s_type3">결제방법 보기</a>
										</div>
										-->
									</td>
								</tr>
								<!-- CMS 선택시 -->
								<tr class="payinfo" data-type="cms" style="display:none">
									<th scope="row"><span class="nec">예금주와의 관계</span></th>
									<td>
										<span class="radio_ui">
											<input type="radio" id="cms_owner_type1" value="본인" name="cms_owner_type" runat="server" class="cms_owner_type css_radio" checked="true" />
											<label for="cms_owner_type1" class="css_label" style="width:170px">본인</label>

											<input type="radio" id="cms_owner_type2" value="타인" name="cms_owner_type" runat="server" class="cms_owner_type css_radio" />
											<label for="cms_owner_type2" class="css_label">타인</label>
										</span>
									</td>
								</tr>
								<tr class="payinfo" data-type="cms" style="display:none">
									<th rowspan="2" scope="row">계좌정보</th>
									<td>
										<span class="radio_ui">
											<input type="radio" id="cms_account_type1" name="cms_account_type" runat="server" class="cms_account_type css_radio" checked />
											<label for="cms_account_type1" class="css_label" style="width:170px">개인계좌</label>

											<input type="radio" id="cms_account_type2" name="cms_account_type" runat="server" class="cms_account_type css_radio" />
											<label for="cms_account_type2" class="css_label">기업계좌</label>
										</span>
									</td>
								</tr>
								<tr class="payinfo" data-type="cms" style="display:none">
									<td>
										<label class="hidden">계좌소유주명</label>
										<input type="text" class="input_type2 mb10" id="cms_owner" runat="server" value="" style="width:400px" maxlength="10" placeholder="이름" /><br />

										<label class="hidden">주민등록번호 앞자리</label>
										<input type="text" class="input_type2 mb20 number_only cms_account_type1" id="cms_birth" runat="server" value="" maxlength="6" style="width:400px" placeholder="생년월일(6자리)" /><br class="cms_account_type1" />

										<label class="hidden">사업자번호</label>
										<input type="text" class="input_type2 mb20 number_only cms_account_type2" id="cms_soc" runat="server" value="" maxlength="10" placeholder="사업자번호 (-없이 입력)" style="width:400px" /><br class="cms_account_type2" />
										
										<input type="hidden" runat="server" id="hd_cms_bank" value="" />
										<input type="hidden" runat="server" id="hd_cms_bank_name" value="" />
										<span class="radio_ui mb15">
											<input type="radio" id="bank_1" data-code="004" name="cms_bank" class="cms_bank css_radio"  />
											<label for="bank_1" class="css_label" style="width:120px">국민은행</label>

											<input type="radio" id="bank_2" data-code="020" name="cms_bank" class="cms_bank css_radio" />
											<label for="bank_2" class="css_label" style="width:140px">우리은행</label>

											<input type="radio" id="bank_3" data-code="012" name="cms_bank" class="cms_bank css_radio" />
                                                <label for="bank_3" class="css_label" style="width: 120px">농협회원조합</label>

											<input type="radio" id="bank_4" data-code="011" name="cms_bank" class="cms_bank css_radio" />
											<label for="bank_4" class="css_label" style="width:120px">농협중앙회</label>
										</span>
										<span class="radio_ui mb10">
											<input type="radio" id="bank_5" data-code="088" name="cms_bank" class="cms_bank css_radio" />
											<label for="bank_5" class="css_label" style="width:120px">신한은행</label>

											<input type="radio" id="bank_6" data-code="081" name="cms_bank" class="cms_bank css_radio" />
											<label for="bank_6" class="css_label" style="width:140px">KEB하나은행</label>

											<input type="radio" id="bank_7" data-code="003" name="cms_bank" class="cms_bank css_radio" />
											<label for="bank_7" class="css_label" style="width:120px">기업은행</label>

											<span class="sel_type2" style="width:150px;">
												<label for="cms_bank_etc" class="hidden">그외 은행 선택</label>
												<asp:DropDownList runat="server" ID="cms_bank_etc" cssClass="custom_sel" >
													<asp:ListItem Text="그 외" Value=""></asp:ListItem>
												</asp:DropDownList>
											</span>
										</span>

										 <p class="guide_comment3 mb20" id="ibk_bank" style="display: none">
                                                기업은행 평생계좌(휴대번호 등)은 반드시 모계좌로 등록해주세요.
                                            </p>
                                           
											<div class="guide_comment3" style="display: none" id="nong_bank">
												<em class="fc_blue">농협계좌</em>로 자동이체를 신청하시는 경우 <em class="fc_blue">통장을 잘 확인</em>하여 은행명을 지정해 주세요.
												<ul class="pl30 lineH_22 mb20">
													<li><span class="s_con1">농협대표,농협중앙회,농협은행 ▶ 농협중앙회</span></li>
													<li><span class="s_con1">단위농협,농협회원조합 ▶ 농협회원조합</span></li>
												</ul>
											</div>

										<br />
										<label class="hidden">계좌번호입력</label>
										<input type="text" class="input_type2 mb10 number_only" maxlength="30" id="cms_account" runat="server" value="" placeholder="계좌번호 (-없이 입력)" style="width:400px" />
										<button class="btn_s_type7 ml5 mb10" id="btn_cms_check_account"><span>계좌 확인</span></button>
									</td>
								</tr>
								<tr class="payinfo" data-type="cms" style="display:none">
									<th scope="row">이체일</th>
									<td>
										<span class="radio_ui mb20">
											<input type="radio" id="cmsday_5" value="05" name="cmsday" runat="server" class="css_radio" checked />
											<label for="cmsday_5" class="css_label" style="width:170px">매월 5일</label>

											<input type="radio" id="cmsday_15" value="15" name="cmsday" runat="server" class="css_radio" />
											<label for="cmsday_15" class="css_label" style="width:170px">매월 15일</label>

											<input type="radio" id="cmsday_25" value="25" name="cmsday" runat="server" class="css_radio" />
											<label for="cmsday_25" class="css_label">매월 25일</label>
										</span><br />

										<div class="box_type5 pl40">
											<p class="s_tit4 mb5">재출금일 안내</p>
											<p class="lineH_20">
												자동이체일이 5일, 15일인 경우 : 매월 20일, 27일<br />
												자동이체일이 25일인 경우 : 매월 27일
											</p>
										</div>
									</td>
								</tr>
								<tr class="payinfo" data-type="cms" style="display:none">
									<th scope="row"><span class="nec">신청동의</span></th>
									<td>
										<span class="checkbox_ui mr20">
											<input type="checkbox" class="css_checkbox" id="agree_cms" checked />
											<label for="agree_cms" class="css_label font2">한국컴패션에 CMS 자동이체를 이용하여 후원금을 납부하는 것에 동의합니다.</label>
										</span>

										<script type="text/javascript">
											$(function () {
												$(".info_guide .open").mouseenter(function () {
													$(".tooltip").fadeIn("fast");
												}).mouseleave(function () {
													$(".tooltip").fadeOut("fast");
												});
											})
										</script>

										<span class="info_guide relative">
											<a href="#" onclick="return false" class="btn_s_type3 open">전문 보기</a>
											<!-- tooltip -->
											<span class="tooltip">
												<ul>
													<li>본인은 한국컴패션에 자동이체를 이용해 후원금을 납부하는 것에 동의합니다.</li>
													<li>금융결제원, 효성FMS에 금융거래 정보를 제공하는 것에 동의합니다.</li>
													<li>후원금으로 납부해야 할 금액에 대해 본인이 지정한 계좌에서 한국컴패션으로 지정 출금일(휴일일 경우 다음영업일)에 출금하는데 동의합니다.</li>
													<li>만일 출금금액에 이의가 있을 경우 한국컴패션과 협의해서 조정하며 금융기관에는 이의를 제기하지 않을 것임을 동의합니다.</li>
												</ul>
												<span class="arr"></span>
											</span>
											<!--// -->
										</span>
									</td>
								</tr>
								<!--// -->

								<!-- 신용카드 선택시 -->
								<tr class="payinfo" data-type="card" style="display:none">
									<th scope="row">카드주</th>
									<td>
										<span class="radio_ui">
											<input type="radio" id="cmsKind1"  name="cardOwner"  class="css_radio" checked />
											<label for="cmsKind1" class="css_label" style="width:170px">본인</label>

											<input type="radio" id="cmsKind2" name="cardOwner" class="css_radio" />
											<label for="cmsKind2" class="css_label">타인</label>
										</span>
									</td>
								</tr>
								<tr class="payinfo" data-type="card" style="display:none">
									<th scope="row">결제일</th>
									<td>
										<span class="radio_ui mb20">
											<input type="radio"  class="css_radio paymentDay" checked id="paymentDay5" value="05" runat="server" name="paymentDay"   />
											<label for="paymentDay5" class="css_label" style="width:170px">매월 5일</label>

											<input type="radio"  class="css_radio paymentDay"  id="paymentDay15" value="15" runat="server" name="paymentDay"  />
											<label for="paymentDay15" class="css_label" style="width:170px">매월 15일</label>

											<input type="radio"  class="css_radio paymentDay"  id="paymentDay25" value="25" runat="server" name="paymentDay"  />
											<label for="paymentDay25" class="css_label">매월 25일</label>
										</span><br />

										<div class="box_type5 pl40">
											<p class="s_tit4 mb5">재결제일 안내</p>
											<p class="lineH_20">
												자동이체일이 5일, 15일인 경우 : 매월 20일, 27일<br />
												자동이체일이 25일인 경우 : 매월 27일
											</p>
										</div>
									</td>
								</tr>
								<!--// -->

								<!-- 해외발급카드 선택 시 -->
								<tr class="payinfo" data-type="oversea_card" style="display:none">
									<th scope="row">결제금액</th>
									<td>
										<label class="hidden">개월 수 입력</label>
										<input type="text" class="input_type2 mr10 number_only" runat="server" id="oversea_pay_month" value="12"  maxlength="3" style="width:200px" /><span class="fs15 vam">개월</span>
										<span class="txt_type1 ml30 vam" id="oversea_pay_total"></span>

										<p class="pt15"><span class="s_con1">결제관련 문의 : 후원자 서비스팀 02-740-1000</span></p>
									</td>
								</tr>
								<!--// -->
								</asp:PlaceHolder>

								<!-- 기존 결제정보가 있을 경우 -->
								<asp:PlaceHolder runat="server" ID="ph_exist_pay" Visible="false">
								<tr>
									<td colspan="2" class="booked">
										<p class="txt1"><asp:Literal ID="lb_exist_pay_msg1" runat="server"/></p>
										<p><asp:Literal ID="lb_exist_pay_msg2" runat="server" Text = ""/></p>
									</td>
								</tr>
								</asp:PlaceHolder>
								<!--// -->
							</tbody>
						</table>

					</div>
					
					<!--// 결제정보 -->

					<!-- CMS선택 시 보여지는 문구 -->
					<div class="box_type3 pl40 mb40" class="payinfo" data-type="cms" style="display:none">
						<p class="mb5 fc_black">※ 결제 전 꼭 확인해주세요!</p>
						<ul>
							<li><span class="s_con1">후원신청 후 해당 은행에서 자동이체 등록승인 문자가 중복 안내될 수 있습니다.</span></li>
							<li><span class="s_con1">한국컴패션은 금융결제원과 효성FMS 시스템을 사용하고 있습니다.</span></li>
							<li><span class="s_con1">결제관련 문의 : <em class="fc_blue">후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_blue">info@compassion.or.kr</a></em></span></li>
						</ul>
					</div>
					<!--// CMS선택 시 보여지는 문구 -->

					<!-- 카드 결제 시 보여지는 문구 -->
					<div class="box_type3 pl40 mb40" class="payinfo" data-type="card" style="display:none">
						<p class="mb5 fc_black">※ 결제 전 꼭 확인해주세요!</p>
						<ul>
							<li><span class="s_con1">후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</span></li>
							<li><span class="s_con1">결제관련 문의 : <em class="fc_blue">후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_blue">info@compassion.or.kr</a></em></span></li>
						</ul>
					</div>
					<!--// 카드 결제 시 보여지는 문구 -->

					<!-- 해외발급 카드 시 보여지는 문구 -->
					<div class="box_type3 pl40 mb40" class="payinfo" data-type="oversea_card" style="display:none">
						<p class="mb5 fc_black">※ 결제 전 꼭 확인해주세요!</p>
						<ul>
							<li><span class="s_con1">결제관련 문의 : <em class="fc_blue">후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_blue">info@compassion.or.kr</a></em></span></li>
							<li><span class="s_con1">추가 금액은 마이컴패션>후원관리에서 결제하실 수 있습니다.</span></li>
						</ul>
					</div>
					<!--// 해외발급 카드 시 보여지는 문구 -->

					<asp:PlaceHolder runat="server" ID="ph_exist_pay2" Visible="false">
					<!-- 기존 결제정보가 있을 때 보여지는 문구 -->
					<div class="box_type3 pl40 mb40">
						<p class="mb5 fc_black">※ 결제 전 꼭 확인해주세요!</p>
						<span class="s_con1">
							<asp:Literal runat="server" ID="lt_exist_pay2">아래 결제 버튼을 누르시면 첫 후원금은 즉시 결제되며, 다음달부터 기존 결제일에 선택하신 후원 금액정보가 추가되어 결제됩니다.</asp:Literal>								
						</span>
					</div>
					<!--// 기존 결제정보가 있을 때 보여지는 문구 -->
						<input type="hidden" id="confirmMsg" value="기존 결제정보에 추가로 결제금액을 등록하시겠습니까?" />
					</asp:PlaceHolder>

					<div class="tac"><asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" cssclass="btn_type1">신청 및 결제</asp:LinkButton></div>

				</div>

			</div>
			<div class="h100"></div>
			
		</div>	
		<!--// e: sub contents -->

		
    </section>

</asp:Content>


<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:kcp_form_temporary runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form_temporary" />	
	<uc:cms_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="cms_form" />
</asp:Content>