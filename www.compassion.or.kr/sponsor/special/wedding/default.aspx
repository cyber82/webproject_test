﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="sponsor_special_wedding_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/info.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>결혼 <em>첫 나눔</em></h1>
				<span class="desc">나눔으로 시작한 결혼으로 특별한 결혼을 기념하세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 sponsor">

			<div class="special first">
				
				<!-- visual -->
				<div class="bgContent marriage">

					<div class="w980 relative">
						<p class="tit">나눔으로 시작하는 동행, 결혼 첫 나눔</p>
						<span class="bar"></span>
						<p class="con">두 사람이 하나가 되는 결혼을 기념하여,<br />도움이 필요한 어린 생명에게 사랑을 나눠주세요.<br />
						* 결혼 첫나눔 신청은 기념증서 제작 및 발송을 위해, 결혼 예정일 4주 전에 신청해주세요.</p>

						<!-- 공유하기버튼 -->
						<span class="sns_ani">
							<span class="common_sns_group">
								<span class="wrap">
									<a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
                                    <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
                                    <a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
                                    <a href="#" title="url 공유" data-role="sns" data-provider="copy"  class="sns_url">url 공유</a>
								</span>
							</span>
							<button class="common_sns_share">공유하기</button>
						</span>
						<!--// -->
					</div>

				</div>
				<!--// visual -->

				<!-- 박스정보 -->
				<div class="campaign_info marriage">
					<div class="w980">

						<div class="bgContent">
							<p class="tit">신청방법</p>
							<span class="bar"></span>
							<p class="con">1:1어린이양육 또는 태아&middot;영아생존프로그램을 선택해서 신청 가능합니다.</p>
						</div>

						<div class="clear2">
							<div class="camp fl">
								<p class="tit">결혼기념일에<br />태어난 어린이 1:1 양육하기</p>

								<p class="con">
									<span class="fc_blue">월 45,000원씩 정기후원</span>
								</p>
								<a href="/sponsor/special/wedding/regular/" class="btn_s_type1">신청하기</a>
							</div>

							<div class="camp fr">
								<p class="tit">아이티 '까르푸 센터'<br />49 가정의 엄마와 아기 살리기</p>

								<p class="con">
									<span class="fc_blue">태아&middot;영아생존프로그램 일시후원 (10만 원 이상)</span>
								</p>
								<a href="/sponsor/special/wedding/temporary/" class="btn_s_type1">신청하기</a>
							</div>
						</div>

						<p class="guide">* 본 캠페인은 국내 회원만 신청 가능합니다. 외국에서 후원을 원하시는 분은 ‘1:1 문의’을 통해 문의해주세요.</p>

					</div>
				</div>
				<!--// 박스정보 -->

				<!-- 함께합니다 -->
				<div class="together marriage">
					<div class="w980">

						<div class="sub_tit mb40">
							<h2 class="tit"><em>컴패션에서 함께하겠습니다!</em></h2>
							<p>나눔으로 시작한 결혼을 특별하게 기억할 수 있는 선물을 준비해 드립니다.</p>
						</div>

						<ul class="present clear2">
							<li class="per50"><span class="s_con7"><em class="fc_blue">기념증서</em> : 부부의 사진이 담긴 기념 증서</span></li>
							<li class="per50"><span class="s_con7"><em class="fc_blue">테이블 배너</em> : 축의금 접수처에 세워둘 수 있는 미니배너</span></li>
							<li><span class="s_con7"><em class="fc_blue">'결혼 첫 나눔' 안내문구</em> : 청첩장에 삽입할 수 있는 결혼 첫 나눔 안내문구 및 로고</span></li>
							<li><span class="s_con7"><em class="fc_blue">어린이 정보/ 후원금 사용처</em> : 후원 어린이 정보(1:1어린이양육 시) 또는 후원금 사용처 소개서 (태아·영아생존프로그램 선택 시)</span></li>
							<li class="padding1">
								<span class="s_con7"><em class="fc_blue">기부금 영수증</em> : 후원금 결제 시 로그인된 사용자 명의로 기부금 영수증 발급</span><br />
								(기부금 영수증은 ‘국세청 연말정산 간소화 서비스’ 또는 ‘한국 컴패션 홈페이지’를 통해 확인 하실 수 있습니다.)
							</li>
						</ul>

						<div class="mb20"><img src="/common/img/page/sponsor/marriage_img2.jpg" alt="기념증서, 테이블 배너 샘플 이미지" /></div>
						
					</div>
				</div>
				<!--// 함께합니다 -->

				<!-- 후원금 사용 -->
				<div class="use marriage">
					<div class="w980">

						<div class="sub_tit mb40">
							<h2 class="tit"><em>후원금은 이렇게 사용됩니다!</em></h2>
							<p>
								'일시 후원'을 통해 모인 후원금은 아이티 '까르푸 어린이센터' 태아&middot;영아생존프로그램의 엄마와 아기들을 위해 사용됩니다.<br />

								<span class="fs13">* 어린이를 결연하신 경우, 어린이 정보가 담긴 후원자 가이드를 보내드립니다.</span>
							</p>
						</div>

						<a href="http://www.srook.net/compassion/635974653145277309" class="btn_type1">컴패션어린이센터 소개 보기</a>

					</div>
				</div>
				<!--// 후원금 사용 -->


			</div>
			<div class="h100"></div>
			
		</div>	
		<!--// e: sub contents -->

		
    </section>

</asp:Content>

