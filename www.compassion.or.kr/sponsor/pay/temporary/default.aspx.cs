﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_temporary_default : SponsorPayBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBackTemporary();

		var payInfo = PayItemSession.GetCookie(this.Context);
		this.ViewState["good_mny"] = payInfo.amount.ToString();
		
		if(!UserInfo.IsLogin) {

			if(Request["noname"] == "Y") {
				noname.Checked = true;
				p_receipt_pub_ok.Checked = false;
			} else { 
				ph_notuser1.Style["display"] = "table-row";
			}
		}
		
		// 후원내역
		var childAction = new ChildAction();
		var actionResult = new JsonWriter();
        var childKey = "";

        if (!payInfo.IsEmptyChildMasterId) {
			actionResult = childAction.GetEnsures();
			if(actionResult.success) {

				var dt = (DataTable)actionResult.data;
				DataRow dr;

				if(dt.Rows.Count > 0) {
					if(dt.Rows.Count < 2) {
						dr = dt.Rows[0];
					} else {
						if(dt.Rows[0]["ChildMasterID"].ToString() == payInfo.childMasterId) {
							dr = dt.Rows[1];
						} else {
							dr = dt.Rows[0];
						}
					}

					childKey = dr["ChildId"].ToString();
					this.ViewState["pic"] = ChildAction.GetPicByChildKey(childKey);
					this.ViewState["childmasterid"] = dr["childmasterid"].ToString();
					//ph_child.Visible = true;
				}

			}
		}

	//	Response.Write(payInfo.ToJson());

		if(payInfo.type == PayItemSession.Entity.enumType.CDSP) {

			view_child.Visible = true;

			actionResult = childAction.Ensure();
			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message, "goBack()");
				return;
			}

		} else if(payInfo.type == PayItemSession.Entity.enumType.SPECIAL_FUNDING) {
			view_sf.Visible = true;
		} else if(payInfo.type == PayItemSession.Entity.enumType.USER_FUNDING) {

			view_uf.Visible = true;
			/*
			if(payInfo.IsEmptyChildMasterId) {
				view_uf.Visible = true;
			} else {
				view_child.Visible = true;
			}
			*/
		}


		// 결제정보를 디비에 저장하고 이후 결제프로세스는 디비의 값을 참조한다.
		var data = new PayItemSession.Store().Create();
		if (data == null) {
			base.AlertWithJavascript("후원정보 임시저장에 실패했습니다." , "goBack()");
			return;
		}

		this.ViewState["payItem"] = data.ToJson();
	}
	
	// 결제
	protected void btn_submit_Click( object sender, EventArgs e ) {
		
		if(base.IsRefresh) {
			return;
		}

		if(!base.UpdateTemporaryUserInfo()) {
			return;
		}
		
		JsonWriter result = new JsonWriter();
		var sess = new UserInfo();
		var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();
		
		var action = new CommitmentAction();
		
		action.ValidateCIV(sess.SponsorID, payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.campaignId, ref result);

		if(!result.success) {
			base.AlertWithJavascript(result.message);
			return;
		}

		// 경고메세지를 띄우는 경우
		if(result.action != null && result.action == "confirm") {
			base.ConfirmWithJavascript(result.message, "goBack()");
		}

		// 결제정보를 세팅
		base.SetTemporaryPaymentInfo();

		if(payment_method_card.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card;
		} else if(payment_method_cms.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.cms;
		} else if(payment_method_oversea.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card_oversea;
		} else if(payment_method_phone.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.phone;
		} else if(payment_method_payco.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.payco;
		} else if(payment_method_kakao.Checked) {
			payInfo.payMethod = PayItemSession.Entity.enumPayMethod.kakao;
		}

		var orderId = PayItemSession.Store.GetNewOrderID();

        if (!UserInfo.IsLogin)
        {
            //20161206 추가
            if (noname.Checked)
            {
                user_name.Value = "무기명";
            }
            else
            {
                //#13062
                user_name.Value = "비회원";
            }
        }
		this.ViewState["buyr_name"] = user_name.Value;

		payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo , orderId);
		//payItem.data = payInfo.ToJson();
		this.ViewState["payItem"] = payItem.ToJson();

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "Temporary";  // 결제 페이지 종류 : /sponsor/pay/temporary/
        this.ViewState["returnUrl"] = "/pay/complete_sponsor/" + payItem.orderId;
        //this.ViewState["returnUrl"] = "/sponsor/pay/complete/" + payItem.orderId;

        kakaopay_form.Hide();
		kcp_form.Hide();
		payco_form.Hide();
		if(payment_method_payco.Checked) {
			payco_form.Show(this.ViewState);
		}else if(payment_method_kakao.Checked) {
			kakaopay_form.Show(this.ViewState);
		} else {
			kcp_form.Show(this.ViewState);
		}
		
	}
}