﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="sponsor_pay_complete" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script>

        <!--  문희원 추가 -->

	    $(document).ready(function () {
	        
	        var childmasterid = $("#<%= hfChildMasterID.ClientID%>").val();
	        var sponsorid = $("#<%= hfSponsorID.ClientID%>").val();
            // Global CommitmentID 생성 / No Money Hold 처리
	        if (childmasterid != '' && childmasterid != null && sponsorid != '' && sponsorid != null) {
            

	          //  $.get("/sponsor/tcpt.ashx?t=regCommit", { childMasterId: childmasterid, sponsorid: sponsorid }).success(function (r) {

                    // 후원자 경고 없이 진행
	          //      if (r.success) {
	                    //alert(r.message);
	         //       } else {
	                        //alert('1');
	         //       }
	        //    });
                
	        }

	});

	(function () {
		var app = angular.module('cps.page', []);
		app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		});

	})();

</script>

</asp:Content>


<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
<section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>후원/결제</em></h1>
				<span class="desc">후원자님 사랑에 감사드립니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents sponsor">

			<div class="payment">
				
				<div class="w980">

					<div class="complete">
						
						<!-- 후원정보 -->
							<div class="sub_tit mb40">
							<h2 class="tit blue"><em><asp:Literal runat="server" ID="typeName2" /></em>이 정상적으로 신청되었습니다.</h2>
							<p>후원하신 정보는 <a href="/my/sponsor/commitment/"><em>마이컴패션 > 후원관리</em></a> 메뉴에서 확인하실 수 있습니다.</p>
						</div>

						<table class="tbl_child mb40">
							<caption>어린이정보 및 후원금액 테이블</caption>
							<colgroup>
								<asp:PlaceHolder runat="server" ID="ph_child_devide" Visible="false">
								<col style="width:30%" />
								<col style="width:70%" />
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="ph_default_devide">
								<col style="width:50%" />
								<col style="width:50%" />
								</asp:PlaceHolder>
							</colgroup>
							<tbody>
								<tr>
									<td class="pic">
										<!-- 1:1어린이양육 : 이미지사이즈 : 200 * 200 -->
										<asp:PlaceHolder runat="server" ID="ph_child_devide2" Visible="false">
										<span class="img" style="background:url('<%:this.ViewState["pic"].ToString() %>') no-repeat center top;background-size:cover;"></span>
										</asp:PlaceHolder>

										<!-- 특별한 나눔 : 이미지사이즈 : 400 * 300 --><!-- 특별한 나눔일 경우 클래스 "sp"를 붙여주세요 -->
										<asp:PlaceHolder runat="server" ID="ph_default_devide2">
										<span class="img sp" style="background:url('<%:this.ViewState["pic"].ToString() %>') no-repeat center top;background-size:cover;"></span>
										</asp:PlaceHolder>
									</td>
									<td class="info">
										<span class="label"><asp:Literal runat="server" ID="typeName" /></span>
										<p class="name"><asp:Literal runat="server" ID="title" /></p>
										
										<div>
											<span class="field2">후원유형</span>
											<span class="data"><asp:Literal runat="server" ID="frequency" />후원</span>
										</div>
										<div>
											<span class="field2">납부방법</span>
											<span class="data"><asp:Literal runat="server" ID="payMethod" /></span>
										</div>
										<div>
											<span class="field2">후원금액</span>
											<span class="data2"><asp:Literal runat="server" ID="amount" /></span>
										</div>

										<!-- 첫 후원금 납부가 없을때 표시 -->
										<!--<p class="first mt5">※ 첫 후원금을 납부하시면 첫 편지 작성과 선물금 전달이 가능합니다.</p>-->

									</td>
								</tr>
								<tr>
									<td colspan="2" class="amount">
										<span class="field">총 후원 금액</span><span class="sum"><asp:Literal runat="server" ID="amount2"/></span><span class="won" runat="server" id="amount2_unit">원/월</span>
									</td>
								</tr>
							</tbody>
						</table>

						<!--// 후원정보 -->

						<asp:PlaceHolder runat="server" ID="ph_cdsp_child" Visible="false">
						<div class="sub_tit mb40">
							<h2 class="tit"><em>지금 바로 아이에게 첫 편지를 쓰러 가보시겠어요?</em></h2>
							<p>
								1:1 어린이 양육을 시작하신 것을 축하드립니다.<br />
								아이에게 쓰는 편지는 아이가 정서적으로 사랑을 느끼는데 중요한 역할을 합니다.
							</p>
						</div>

						<div class="hello_box">
							<div class="tit">
								<span><em><asp:Literal runat="server" ID="userName"></asp:Literal> 후원자</em>님 안녕하세요?</span>
								<a href="/my/letter/write/?c=<%:this.ViewState["childmasterid"].ToString() %>" class="btn_s_type1 mr5">편지쓰러 가기</a>
								<a href="/my/sponsor/gift-money/pay/?t=temporary&childMasterId=<%:this.ViewState["childmasterid"].ToString() %>" class="btn_s_type1">선물금 보내기</a>
							</div>
							<div class="child_info">
								<span class="pic" background-img="<%:this.ViewState["pic"].ToString() %>" data-default-image="/common/img/page/my/no_pic.png"  style="background:no-repeat center top;background-size:200px"></span>

								<p class="name">제 이름은 <em><asp:Literal runat="server" ID="childName" /></em>예요.</p>
								<p class="con">후원자님과 함께하게 되어서 너무 기뻐요.<br />저와 같이 편지를 해보시지 않으실래요?</p>
							</div>
						</div>
						</asp:PlaceHolder>

						<div class="sub_tit mb40">
							<asp:PlaceHolder runat="server" ID="ph_first_sponsor" Visible="false">
							<h2 class="tit"><em>첫 후원을 축하드립니다.</em></h2>
							</asp:PlaceHolder>

							<asp:PlaceHolder runat="server" ID="ph_stamp1" Visible="false">
							<p class="mb40">
								<%:this.ViewState["stampTitle"] .ToString()%> 스탬프가 발급되었습니다.<br />
								달성된 스탬프는 <a href="/my/activity/stamp"><em>마이컴패션 > 나의 참여/활동 > 스탬프 투어</em></a> 페이지에서 확인하실 수 있습니다.
							</p>
							</asp:PlaceHolder>
						</div>
						

						<!-- 스탬프 -->
						<!--
							cdsp , mail , cad , sf , ufc , ufj , store , common
							-->
						<asp:PlaceHolder runat="server" ID="ph_stamp2" Visible="false">
						<div class="stampImg_wrap">
							<span class="stampImg <%:this.ViewState["stampId"] .ToString()%> on"></span>

							<div class="otherStamp">
								<span>다른 스탬프는 어떤 스탬프가 있는지 확인해보세요.</span>
								<a href="/my/activity/stamp/" class="btn_s_type2 ml20">스탬프 투어</a>
							</div>

						</div>
						</asp:PlaceHolder>
						<!--// 스탬프 -->

						<div class="tac">
							<a href="/" class="btn_type1">메인</a>
						</div>

					</div>
					

					

				</div>
				
			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
    <!--  문희원 추가 -->
    <asp:HiddenField runat="server" ID="hfSponsorID" />
    <asp:HiddenField runat="server" ID="hfChildMasterID" />

    </section>

</asp:Content>
