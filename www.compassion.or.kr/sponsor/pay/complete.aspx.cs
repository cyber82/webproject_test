﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;


public partial class sponsor_pay_complete : FrontBasePage {
    
	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		var requests = Request.GetFriendlyUrlSegments();
		var orderId = "";
        string sponsorType = "";
        string sponsorTypeEng = "";



        if (requests.Count > 0) {
			orderId = requests[0];
		}

		if(!string.IsNullOrEmpty(orderId)) {

			var payItem = new PayItemSession.Store().Get(orderId);
			
			if(payItem == null) {
				Response.Redirect("/");
			}
			
			var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

			//userName.Text = payInfo.sponsorName;
			if(UserInfo.IsLogin)
				userName.Text = new UserInfo().UserName;
			typeName2.Text = typeName.Text = payInfo.TypeName;
			frequency.Text = payInfo.frequency;
			payMethod.Text = payInfo.PayMethodName;
			amount.Text = payInfo.amount.ToString("N0") + ((payInfo.frequency == "정기") ? "원 /월" : "원");
			amount2.Text = payInfo.amount.ToString("N0");
			if(payInfo.frequency == "정기") {
				amount2_unit.InnerHtml = "원/월";
			} else {
				amount2_unit.InnerHtml = "원";
			}

			if(payInfo.type == PayItemSession.Entity.enumType.CDSP) {
                sponsorType = "어린이후원";
                sponsorTypeEng = "CDSP";
                ph_cdsp_child.Visible = true;
				ShowItemChild(payInfo);

				if(!payItem.is_completed)
					new CDSP().AfterWork(payInfo);

			} else if(payInfo.type == PayItemSession.Entity.enumType.SPECIAL_FUNDING) {
                sponsorType = "특별한나눔";
                sponsorTypeEng = "SPECIAL_FUNDING";
                ShowItemSpecialFunding(payInfo);
				if(!payItem.is_completed)
					DoSpecialFundingWork(payInfo);
			} else if(payInfo.type == PayItemSession.Entity.enumType.USER_FUNDING) {
                sponsorType = "나눔펀딩";
                sponsorTypeEng = "USER_FUNDING";
                if (!payItem.is_completed)
					DoUserFundingWork(payInfo);

				ShowItemUserFunding(payInfo);
				
			}

            /*         이벤트 정보 등록  -- 20170327 이정길       */
            var eventID = Session["eventID"];
            var eventSrc = Session["eventSrc"];

            if (eventID == null)
            {
                if (Request.Cookies["e_id"] != null)
                {
                    eventID = Request.Cookies["e_id"].Value;
                    eventSrc = Request.Cookies["e_src"].Value;
                }
            }

            //ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 eventID == "+ eventID);
            //ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 eventSrc == " + eventSrc);

            try
           {
                using (FrontDataContext dao = new FrontDataContext())
                {
                    if (eventID != null && eventSrc != null && !eventID.Equals(""))
                    {
                        /*
                        ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 UserId == " + new UserInfo().UserId);
                        ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 SponsorID == " + new UserInfo().SponsorID);
                        ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 ConId == " + new UserInfo().ConId);
                        ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 sponsorType == " + sponsorType);
                        ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 sponsorTypeEng == " + sponsorTypeEng);
                        ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 amount == " + payInfo.amount);
                        ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 commitmentId == " + payInfo.commitmentId);
                        */
                        //dao.sp_tEventApply_insert_f(int.Parse(eventID.ToString()),
                        //                            new UserInfo().UserId,
                        //                            new UserInfo().SponsorID,
                        //                            new UserInfo().ConId,
                        //                            eventSrc.ToString(),
                        //                            sponsorType+"("+ payInfo.frequency + ")",
                        //                            sponsorTypeEng,
                        //                            payInfo.amount,
                        //                            payInfo.commitmentId);
                        //dao.SubmitChanges();

                        Object[] op1 = new Object[] { "evt_id", "UserID", "SponsorID", "ConID", "evt_route", "SponsorType", "SponsorTypeEng", "SponsorAmount", "CommitmentID" };
                        Object[] op2 = new Object[] { int.Parse(eventID.ToString()), new UserInfo().UserId, new UserInfo().SponsorID, new UserInfo().ConId, eventSrc.ToString(), sponsorType+"("+ payInfo.frequency + ")", sponsorTypeEng, payInfo.amount, payInfo.commitmentId };
                        var list = www6.selectSP("sp_tEventApply_insert_f", op1, op2).DataTableToList<sp_tEventApply_insert_fResult>();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 error :: "+ex.Message);
            }

            // 후원횟수
            var actionResult = new CommitmentAction().GetCommitmentList(1, 1, "");
			if(actionResult.success) {
				var dt = (DataTable)actionResult.data;
				if (dt.Rows.Count > 0) {
					var total = Convert.ToInt32(dt.Rows[0]["total"]);
					if (total == 1) {
						ph_first_sponsor.Visible = true;
					}
				}
			}

			new PayItemSession.Store().Complete(orderId);
		}

	}

	// 어린이후원
	void ShowItemChild(PayItemSession.Entity payInfo) {
		ph_child_devide.Visible = ph_child_devide2.Visible = true;
		ph_default_devide.Visible = ph_default_devide2.Visible = false;
		string ChildMasterID = payInfo.childMasterId;


		if(string.IsNullOrEmpty(ChildMasterID))
			return;

		var actionResult = new ChildAction().GetChild(ChildMasterID);

        


        if (!actionResult.success) {
			throw new Exception(actionResult.message);
		}

        


        var entity = (ChildAction.ChildItem)actionResult.data;
		this.ViewState["pic"] = entity.Pic;
		this.ViewState["childmasterid"] = entity.ChildMasterId;
		title.Text = entity.NameKr;
		childName.Text = entity.NameKr;

        // TCPTR4 추가 문희원
        hfSponsorID.Value = new UserInfo().SponsorID;
        hfChildMasterID.Value = entity.ChildMasterId;
        // TCPTR4 추가 끝

		this.ViewState["stampTitle"] = "1:1어린이결연 기념";
		this.ViewState["stampId"] = "cdsp";
		var added = new StampAction().Add("cdsp").success;
		if(added) {
			ph_stamp1.Visible = ph_stamp2.Visible = true;
		}

	}
    
    // 나눔펀딩
    void ShowItemUserFunding( PayItemSession.Entity payInfo )
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tUserFunding_get_f(payInfo.relation_key).ToList();
            Object[] op1 = new Object[] { "uf_id" };
            Object[] op2 = new Object[] { payInfo.relation_key };
            var list = www6.selectSP("sp_tUserFunding_get_f", op1, op2).DataTableToList<sp_tUserFunding_get_fResult>();

            if (list.Count < 1)
            {
                return;
            }

            var entity = list[0];
            this.ViewState["pic"] = entity.uf_image.WithFileServerHost();
            title.Text = entity.uf_title;

        }

		this.ViewState["stampTitle"] = "나눔펀딩 후원 기념";
		this.ViewState["stampId"] = "ufj";
		var added = new StampAction().Add("ufj").success;
		if(added) {
			ph_stamp1.Visible = ph_stamp2.Visible = true;
		}

	}

	// 특별한나눔
	void ShowItemSpecialFunding(PayItemSession.Entity payInfo)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tSpecialFunding_get_f(payInfo.campaignId).ToList();
            Object[] op1 = new Object[] { "campaignId" };
            Object[] op2 = new Object[] { payInfo.campaignId };
            var list = www6.selectSP("sp_tSpecialFunding_get_f", op1, op2).DataTableToList<sp_tSpecialFunding_get_fResult>();

            if (list.Count < 1)
            {
                return;
            }

            var entity = list[0];
            this.ViewState["pic"] = entity.sf_thumb.WithFileServerHost();
            title.Text = entity.CampaignName;
        }

		this.ViewState["stampTitle"] = "특별한 나눔 달성 기념";
		this.ViewState["stampId"] = "sf";
		var added = new StampAction().Add("sf").success;
		if(added) {
			ph_stamp1.Visible = ph_stamp2.Visible = true;
		}
	}

	// 나눔펀딩
	void DoUserFundingWork( PayItemSession.Entity payInfo )
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            if (payInfo.relation_key > 0 && !string.IsNullOrEmpty(payInfo.commitmentId))
            {
                //dao.sp_tUserFundingUser_insert_f(payInfo.relation_key, payInfo.userId, payInfo.sponsorId, payInfo.sponsorName, payInfo.commitmentId, payInfo.amount);
                Object[] op1 = new Object[] { "uu_uf_id", "userID", "sponsorID", "sponsorName", "commitmentID", "uu_amount" };
                Object[] op2 = new Object[] { payInfo.relation_key, payInfo.userId, payInfo.sponsorId, payInfo.sponsorName, payInfo.commitmentId, payInfo.amount };
                www6.selectSP("sp_tUserFundingUser_insert_f", op1, op2);
                //dao.SubmitChanges();

                // 메일발송 및 기타처리
                new UserFunding().AfterWork(payInfo);
            }

        }

	}

	// 특별한 나눔
	void DoSpecialFundingWork( PayItemSession.Entity payInfo )
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            if (payInfo.relation_key > 0)
            {
                //dao.sp_tSpecialFunding_update_amount_f(payInfo.relation_key, payInfo.amount);
                Object[] op1 = new Object[] { "sf_id", "amount" };
                Object[] op2 = new Object[] { payInfo.relation_key, payInfo.amount };
                www6.selectSP("sp_tSpecialFunding_update_amount_f", op1, op2);
                //dao.SubmitChanges();

                new SpecialFunding().AfterWork(payInfo);
            }

        }

	}


    // 특별한후원 메일
    class SpecialFunding
    {
		public void AfterWork( PayItemSession.Entity payInfo )
        {
			var sf_id = payInfo.relation_key;
            //var entity = dao.sp_tSpecialFunding_get_f(payInfo.campaignId).FirstOrDefault();
            Object[] op1 = new Object[] { "campaignId" };
            Object[] op2 = new Object[] { payInfo.campaignId };
            var entity = www6.selectSP("sp_tSpecialFunding_get_f", op1, op2).DataTableToList<sp_tSpecialFunding_get_fResult>().FirstOrDefault();

            if (entity != null && UserInfo.IsLogin) {

				this.SendMailPayComplete(entity , payInfo);
			}
		}

		void SendMailPayComplete( sp_tSpecialFunding_get_fResult entity , PayItemSession.Entity payInfo ) {

			try {

				var actionResult = new SponsorAction().GetCommunications();
				if(!actionResult.success) {
					return;
				}

				var email = ((SponsorAction.CommunicationResult)actionResult.data).Email;
				if(string.IsNullOrEmpty(email))
					return;

				var from = ConfigurationManager.AppSettings["emailSender"];

				string amountMonth = "원"; 
				if(payInfo.frequency == "정기") {
					amountMonth = "원/월";
				
				}

				var args = new Dictionary<string, string> {
					{"{pic}" , entity.sf_thumb.WithFileServerHost() } ,
					{"{title}" , entity.CampaignName} ,
					{"{userName}" , new UserInfo().UserName} ,
					{"{frequency}" , payInfo.frequency } ,
					{"{pay_method}" , payInfo.PayMethodName } ,
					{"{amount}" , payInfo.amount.ToString("N0") } ,
					{"{amountMonth}" , amountMonth }

				};

				Email.Send(HttpContext.Current, from, new List<string>() { email },
				string.Format("[한국컴패션] 후원 신청이 완료되었습니다"),
				"/mail/sf-pay-complete.html",
				args
			, null);

			} catch(Exception e) {
				ErrorLog.Write(HttpContext.Current, 0, e.Message);
				throw e;
			}

		}

	}

	// 어린이후원 메일
	class CDSP
    {
		public void AfterWork( PayItemSession.Entity payInfo )
        {	
			this.SendMailPayComplete(payInfo);

			// CAD 추천인 푸시 발송
			try
            {
                using (FrontDataContext dao = new FrontDataContext())
                {
                    //var target = dao.sp_cad_recommender_f(payInfo.childMasterId).FirstOrDefault();
                    Object[] op1 = new Object[] { "childMasterId" };
                    Object[] op2 = new Object[] { payInfo.childMasterId };
                    var target = www6.selectSP("sp_cad_recommender_f", op1, op2).DataTableToList<sp_cad_recommender_fResult>().FirstOrDefault();
                    if (target != null)
                    {
                        ErrorLog.Write(HttpContext.Current, 0, "CAD push발송 to : " + target.userid);
                        Pushpia.Send("3ae8ba7e813647e9a09ab97df1c39405", "CAD를 통해 후원이 이루어졌어요!", "후원자님이 보낸 CAD 메시지를 통해 한 어린이가 양육의 기회를 얻게 되었어요!", "", target.userid);
                    }
                }

			} catch {

			}

		}

		void SendMailPayComplete( PayItemSession.Entity payInfo ) {

			try {
				
				string ChildMasterID = payInfo.childMasterId;

				if(string.IsNullOrEmpty(ChildMasterID))
					return;

				var actionResult = new ChildAction().GetChild(ChildMasterID);
				if(!actionResult.success) {
					return;
				}

				var entity = (ChildAction.ChildItem)actionResult.data;

				actionResult = new SponsorAction().GetCommunications();
				if(!actionResult.success) {
					return;
				}

				var email = ((SponsorAction.CommunicationResult)actionResult.data).Email;
				if(string.IsNullOrEmpty(email))
					return;

				var from = ConfigurationManager.AppSettings["emailSender"];

				var args = new Dictionary<string, string> {
					{"{pic}" , entity.Pic } ,
					{"{childName}" , entity.NameKr} ,
					{"{userName}" , new UserInfo().UserName} ,
					{"{frequency}" , payInfo.frequency } ,
					{"{pay_method}" , payInfo.PayMethodName } ,
					{"{amount}" , payInfo.amount.ToString("N0") }

				};

				Email.Send(HttpContext.Current, from, new List<string>() { email },
				string.Format("[한국컴패션] 후원 신청이 완료되었습니다"),
				"/mail/cdsp-pay-complete.html",
				args
			, null);

			} catch(Exception e) {
				ErrorLog.Write(HttpContext.Current, 0, e.Message);
				throw e;
			}

		}
    }
	
	class UserFunding
    {
		public void AfterWork( PayItemSession.Entity payInfo )
        {
			var uf_id = payInfo.relation_key;
            //var entity = dao.sp_tUserFunding_get_f(uf_id).FirstOrDefault();
            Object[] op1 = new Object[] { "uf_id" };
            Object[] op2 = new Object[] { uf_id };
            var entity = www6.selectSP("sp_tUserFunding_get_f", op1, op2).DataTableToList<sp_tUserFunding_get_fResult>().FirstOrDefault();

            if (entity != null)
            {	
				// 후원완료메일
				if (UserInfo.IsLogin)
					this.SendMailPayComplete(entity, payInfo);
				
				// 100%달성시
				if((entity.uf_current_amount / entity.uf_goal_amount * 100) >= 100)
                {
                    //var email = dao.sp_message_log_create(uf_id, "나눔펀딩100%달성", "email").First();
                    Object[] op3 = new Object[] { "ml_ref_id", "ml_kind", "ml_method" };
                    Object[] op4 = new Object[] { uf_id, "나눔펀딩100%달성", "email" };
                    var email = www6.selectSP("sp_message_log_create", op3, op4).DataTableToList<sp_message_log_createResult>().First();

                    if (email.canSend == "Y")
                    {
						// tSponsorCall 등록
						new UserFundingAction().AddSponsorCall(entity.uf_id, "", "", "", "나눔펀딩 모금 결과", "모금 완료");
						
						this.SendMailSuccess(entity);

                        //dao.message_log.First(p => p.ml_id == email.ml_id).ml_is_send = true;
                        var entity2 = www6.selectQF<message_log>("ml_id", email.ml_id);
                        entity2.ml_is_send = true;

                        //dao.SubmitChanges();
                        www6.update(entity);
                    }
				}
				
			}

			
		}

		void SendMailPayComplete( CommonLib.sp_tUserFunding_get_fResult entity, PayItemSession.Entity payInfo ) {

			try {

				var actionResult = new SponsorAction().GetCommunications();
				if(!actionResult.success) {
					return;
				}

				var email = ((SponsorAction.CommunicationResult)actionResult.data).Email;
				if(string.IsNullOrEmpty(email))
					return;

				var from = ConfigurationManager.AppSettings["emailSender"];

				var args = new Dictionary<string, string> {
					{"{pic}" , entity.uf_image.WithFileServerHost() } ,
					{"{title}" , entity.uf_title} ,
					{"{user_name}" , new UserInfo().UserName} ,
					{"{typeName}" , entity.uf_type == "child"? "어린이 결연 펀딩" : "양육을 돕는 펀딩" } ,
					{"{pay_method}" , payInfo.PayMethodName } ,
					{"{amount}" , payInfo.amount.ToString("N0") }
					
				};

				Email.Send(HttpContext.Current, from, new List<string>() { email },
				string.Format("[한국컴패션] 나눔펀딩 참여가 완료되었습니다."),
				"/mail/uf-pay-complete.html",
				args
			, null);

			} catch(Exception e) {
				ErrorLog.Write(HttpContext.Current, 0, e.Message);
				throw e;
			}

		}

		void SendMailSuccess( CommonLib.sp_tUserFunding_get_fResult entity ) {

			try {

				var actionResult = new SponsorAction().GetCommunications(entity.SponsorID);
				if(!actionResult.success) {
					return;
				}

				var email = ((SponsorAction.CommunicationResult)actionResult.data).Email;
				if(string.IsNullOrEmpty(email))
					return;

				var from = ConfigurationManager.AppSettings["emailUserFunding"];

				var args = new Dictionary<string, string> {
					{"{pic}" , entity.uf_image.WithFileServerHost() } ,
					{"{title}" , entity.uf_title} ,
					{"{typeName}" , entity.uf_type == "child"? "어린이 결연 펀딩" : "양육을 돕는 펀딩" } ,
					{"{duration}" , string.Format("{0} ~ {1}" , entity.uf_date_start.ToString("yyyy.MM.dd") , entity.uf_date_end.ToString("yyyy.MM.dd")) } ,
					{"{goal_amount}" , entity.uf_goal_amount.ToString("N0") } ,
					{"{current_amount}" , entity.uf_current_amount.ToString("N0") } ,
					{"{creator_name}" , entity.SponsorName} ,
					{"{creator_id}" , entity.UserID} ,
					{"{creator_conid}" , entity.ConID} ,
					{"{creator_sponsorid}" , entity.SponsorID} ,
					{"{childname}" , entity.ChildName} ,
					{"{childkey}" , entity.ChildKey} ,
					{"{childDisplay}" , entity.uf_type == "child" ? "block" : "none"} ,
                    {"{AccountClass}" , entity.AccountClass.EmptyIfNull()} 
                };
                

				Email.Send(HttpContext.Current, from, new List<string>() { email },
				"[한국컴패션]나눔펀딩 목표금액 달성에 성공했습니다.", "/mail/uf-success.html", args, null);

				//Email.Send(HttpContext.Current, from, new List<string>() { "jhkim@compassion.or.kr" , "wjo@compassion.or.kr" , "shlee@compassion.or.kr" },
				//"[한국컴패션]나눔펀딩 목표금액 달성에 성공했습니다.", "/mail/uf-success-admin.html", args, null);

                Email.Send(HttpContext.Current, from, new List<string>() { "Wjo@compassion.or.kr", "jhshin@compassion.or.kr", "yjeong@compassion.or.kr", "shlee@compassion.or.kr" },
                "[한국컴패션]나눔펀딩 목표금액 달성에 성공했습니다.", "/mail/uf-success-admin.html", args, null);

            } catch(Exception e) {
				ErrorLog.Write(HttpContext.Current, 0, e.Message);
				throw e;
			}

		}

	}
}