﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="american-member.aspx.cs" Inherits="sponsor_pay_regular_american_member" %>
<div style="background: transparent;height:603px;width:800px;" class="fn_pop_container" id="popup">
	<div class="pop_type1 w800 fn_pop_content">
		<div class="pop_title">
			<span>미주후원자 서비스 변경 안내</span>
			<button class="pop_close" ng-click="modal.close($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content american tac">

			<div class="logo"><img src="/common/img/common/logo_2.png" alt="Compassion 로고" /></div>

			<p class="s_tit6"><span>{{sponsor}}</span> 후원자님</p>

			<p class="s_con3">안녕하세요, 한국컴패션입니다.<br /><br />
			국제컴패션 서비스가 변경되어 앞으로는 미국컴패션의 한글 번역 기능을 통해<br />
			후원 관리 및 어린이와의 편지 서비스를 이용하실 수 있습니다.<br /><br />
			아래의 버튼을 눌러 미국컴패션으로 이동해 주세요.<br />
			언제나 후원자님의 귀한 섬김에 감사드립니다.</p>

			<div class="btn_ac">
				<a href="http://www.compassion.com/" target="_blank" class="btn_type1">미국컴패션 바로 가기</a>
			</div>

		</div>
	</div>
</div>

