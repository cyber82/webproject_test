﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_regular_default : SponsorPayBasePage {
    public override bool RequireSSL
    {
        get
        {
            return true;
        }
    }

    public override bool NoCache
    {
        get
        {
            return true;
        }
    }
    protected override void OnBeforePostBack() {
        try
        {
            base.OnBeforePostBackPayAgain();
            //base.OnBeforePostBackTemporary();
            var payInfo = PayItemSession.GetCookie(this.Context);
            this.ViewState["overseas_card"] = "N";
            this.ViewState["good_mny"] = payInfo.amount.ToString();

            motiveCode.Value = Request.QueryString["motiveCode"].EmptyIfNull();
            motiveName.Value = Request.QueryString["motiveName"].EmptyIfNull();

            hd_amount.Value = payInfo.amount.ToString();

            ErrorLog.Write(HttpContext.Current, 0, "pay_again group===" + payInfo.group);
            ErrorLog.Write(HttpContext.Current, 0, "pay_again type===" + payInfo.type);

            if (payInfo.type == PayItemSession.Entity.enumType.CDSP)
            {

                if (string.IsNullOrEmpty(payInfo.childMasterId))
                {
                    Response.Redirect("/sponsor/children/");
                    return;

                }

                // 후원내역
                var childAction = new ChildAction();
                var actionResult = childAction.GetEnsures();
                if (actionResult.success)
                {

                    var dt = (DataTable)actionResult.data;
                    DataRow dr;

                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows.Count < 2)
                        {
                            dr = dt.Rows[0];
                        }
                        else
                        {
                            if (dt.Rows[0]["ChildMasterID"].ToString() == payInfo.childMasterId)
                            {
                                dr = dt.Rows[1];
                            }
                            else
                            {
                                dr = dt.Rows[0];
                            }
                        }

                        var childKey = dr["ChildId"].ToString();
                        this.ViewState["pic"] = ChildAction.GetPicByChildKey(childKey);
                        this.ViewState["childmasterid"] = dr["childmasterid"].ToString();
                        //ph_child.Visible = true;
                    }

                }

                view_child.Visible = true;

            }
            else if (payInfo.type == PayItemSession.Entity.enumType.SPECIAL_FUNDING)
            {

                //pn_letter.Style["display"] = "none";

                view_sf.Visible = true;

            }
            else
            {


                Response.Redirect("/sponsor/children/");
            }


            // 결제정보를 디비에 저장하고 이후 결제프로세스는 디비의 값을 참조한다.
            var data = new PayItemSession.Store().Create();
            if (data == null)
            {
                base.AlertWithJavascript("후원정보 임시저장에 실패했습니다.", "goBack()");
                return;
            }

            this.ViewState["payItem"] = data.ToJson();
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, "pay_again " + ex.Message );
            base.AlertWithJavascript("오류가 발생했습니다.", "goBack()");
            return;
        }		
	}
	
	// 결제
	protected void btn_submit_Click( object sender, EventArgs e ) {
        if (base.IsRefresh)
        {
            return;
        }

        if (!base.UpdateAgainUserInfo())
        {
            return;
        }

        JsonWriter result = new JsonWriter();
        var sess = new UserInfo();
        var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
        var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

        var action = new CommitmentAction();

        //action.ValidateCIV(sess.SponsorID, payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.campaignId, ref result);

        //if (!result.success)
        //{
        //    base.AlertWithJavascript(result.message);
        //    return;
        //}

        //// 경고메세지를 띄우는 경우
        //if (result.action != null && result.action == "confirm")
        //{
        //    base.ConfirmWithJavascript(result.message, "goBack()");
        //}

        // 결제정보를 세팅
        this.ViewState["jumin"] = jumin.Value;
        this.ViewState["ci"] = ci.Value;
        this.ViewState["di"] = di.Value;

        this.ViewState["used_card_YN"] = "Y";
        this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_domestic"];

        if (payment_method_card.Checked)
        {
            this.ViewState["pay_method"] = "100000000000";
            this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card"];

        }
        else if (payment_method_cms.Checked)
        {
            this.ViewState["pay_method"] = "010000000000";
            this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_account"];

        }
        else if (payment_method_phone != null && payment_method_phone.Checked)
        {
            this.ViewState["pay_method"] = "000010000000";
            this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_phone"];

        }
        else if (payment_method_oversea.Checked)
        {
            this.ViewState["pay_method"] = "100000000000";
            this.ViewState["overseas_card"] = "Y";
            this.ViewState["used_card_YN"] = "Y";
            this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_oversea"];
            this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card_Foreign"];
        }

        if (payment_method_card.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card;
        }
        else if (payment_method_cms.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.cms;
        }
        else if (payment_method_oversea.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card_oversea;
        }
        else if (payment_method_phone.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.phone;
        }
        else if (payment_method_payco.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.payco;
        }
        else if (payment_method_kakao.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.kakao;
        }

        var orderId = PayItemSession.Store.GetNewOrderID();

        
        this.ViewState["buyr_name"] = user_name.Value;

        payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo, orderId);
        //payItem.data = payInfo.ToJson();
        this.ViewState["payItem"] = payItem.ToJson();
        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "Temporary";  // 결제 페이지 종류 : /sponsor/pay/temporary/
        this.ViewState["returnUrl"] = "/pay/complete_sponsor/" + payItem.orderId;
        //this.ViewState["returnUrl"] = "/sponsor/pay/complete/" + payItem.orderId;
        this.ViewState["firstPay"] = "1"; 

        kakaopay_form.Hide();
        kcp_form.Hide();
        payco_form.Hide();
        if (payment_method_payco.Checked)
        {
            payco_form.Show(this.ViewState);
        }
        else if (payment_method_kakao.Checked)
        {
            kakaopay_form.Show(this.ViewState);
        }
        else
        {
            kcp_form.Show(this.ViewState);
        }

 
    }
     
}