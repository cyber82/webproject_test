﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;
using TCPTModel.Request.Supporter;
using Newtonsoft.Json;
using TCPTModel;
using TCPTModel.Response.Supporter;
using TCPTModel.Request.Hold.Beneficiary;

public partial class sponsor_regular_default : SponsorPayBasePage {

    private delegate void DoStuff(); //delegate for the action
    public string SponsorID;
    public string ChildMasterID;
    public string CommitmentID;

    protected override void OnBeforePostBack() {
		base.OnBeforePostBackRegular();
		
		var payInfo = PayItemSession.GetCookie(this.Context);

		hd_amount.Value = payInfo.amount.ToString();

		if(payInfo.type == PayItemSession.Entity.enumType.CDSP) {

			if(string.IsNullOrEmpty(payInfo.childMasterId)) {
				Response.Redirect("/sponsor/children/");
				return;

			}

			// 후원내역
			var childAction = new ChildAction();
			var actionResult = childAction.GetEnsures();
			if(actionResult.success) {

				var dt = (DataTable)actionResult.data;
				DataRow dr;

				if(dt.Rows.Count > 0) {
					if(dt.Rows.Count < 2) {
						dr = dt.Rows[0];
					} else {
						if(dt.Rows[0]["ChildMasterID"].ToString() == payInfo.childMasterId) {
							dr = dt.Rows[1];
						} else {
							dr = dt.Rows[0];
						}
					}

					var ChildKey = dr["ChildId"].ToString();
					this.ViewState["pic"] = ChildAction.GetPicByChildKey(ChildKey);
					this.ViewState["childmasterid"] = dr["childmasterid"].ToString();
					//ph_child.Visible = true;
				}

			}

			view_child.Visible = true;

			actionResult = childAction.Ensure();
			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message, "goBack()");
				return;
			}

		} else if(payInfo.type == PayItemSession.Entity.enumType.SPECIAL_FUNDING) {
			
			pn_letter.Style["display"] = "none";
			
			view_sf.Visible = true;
			/*
		} else if(payInfo.type == PayItemSession.Entity.enumType.CSP_WEDDING) {

			if(payInfo.frequency == "정기") {
				view_child.Visible = true;
				
			} else {
				Response.Write("일시처리해야함");
			}
		}
		*/
		} else {
			Response.Redirect("/sponsor/children/");
		}


		// 결제정보를 디비에 저장하고 이후 결제프로세스는 디비의 값을 참조한다.
		var data = new PayItemSession.Store().Create();
		if(data == null) {
			base.AlertWithJavascript("후원정보 임시저장에 실패했습니다.", "goBack()");
			return;
		}
		 
		this.ViewState["payItem"] = data.ToJson();
		
		cms_owner.Value = user_name.Value;
		if (!string.IsNullOrEmpty(hdBirthDate.Value))
			cms_birth.Value = hdBirthDate.Value.Substring(2);
	}
	
	// 결제
	protected void btn_submit_Click( object sender, EventArgs e ) {
        
        cms_form.Visible = false;
		kcp_form.Visible = false;
		kcp_form_temporary.Visible = false;

  //      var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
  //      var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

  //      var orderId = PayItemSession.Store.GetNewOrderID();
		//payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo , orderId);

        //        Response.Redirect("/sponsor/pay/complete/" + payItem.orderId);

        ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 결제 시작");

        if (base.IsRefresh)
        {
            return;
        }

        //신규 GlobalPool 조회방법인지 체크
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();

        ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 결제 시작 isRefresh");

        if (!base.UpdateRegularUserInfo())
            return;

        ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 결제 시작 UpdateRegularUserInfo");


        UserInfo sess = new UserInfo();
        var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
        var payInfo = payItem.data.ToObject<PayItemSession.Entity>();


        var sponsorId = sess.SponsorID;

        // 첫결연이고 본인인증을 하지 않는 경우(기업,국외) sponsorId 예외처리
        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            if (this.ViewState["sponsorId"] == null)
            {
                base.AlertWithJavascript("후원회원 정보가 없습니다.");
                return;
            }
            else
            {
                sponsorId = this.ViewState["sponsorId"].ToString();


            }
        }

        var result = new JsonWriter();

        ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 결제 시작 payment_method_card.Checking");

        if (payment_method_card.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card;
        }
        else if (payment_method_cms.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.cms;
        }
        else if (payment_method_oversea.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.card_oversea;

            // 국외회원 첫 결연시 sponsorID를 업데이트 시 얻어 오기때문에 다시 Keep 한다.
            // card , cms 는 form 에서 ensure 처리
            if (payInfo.type == PayItemSession.Entity.enumType.CDSP)
            {
                var result2 = new ChildAction().Keep(sponsorId, payInfo.childMasterId);
                if (!result2.success)
                {
                    base.AlertWithJavascript(result2.message, "goBack()");
                    return;
                }
            }

        }
        else if (payment_method_giro.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.jiro;
        }
        else if (payment_method_virtualaccount.Checked)
        {
            payInfo.payMethod = PayItemSession.Entity.enumPayMethod.virtualAccount;
        }

        var orderId = PayItemSession.Store.GetNewOrderID();
        payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo, orderId);

        ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 결제 시작 orderId===" + orderId);

        this.ViewState["payItem"] = payItem.ToJson();
        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "Regular";
        this.ViewState["returnUrl"] = "/pay/complete_sponsor/" + payItem.orderId;
        //this.ViewState["returnUrl"] = "/sponsor/pay/complete/" + payItem.orderId;
        this.ViewState["failUrl"] = "/pay/fail_sponsor/?r=" + HttpUtility.UrlEncode("/sponsor/pay/regular");
        //this.ViewState["failUrl"] = "/sponsor/pay/fail/?r=" + HttpUtility.UrlEncode("/sponsor/pay/regular");
        //이종진 2018-03-12 - 결제실패 시, pay-again 페이지로 가기위한 url을 들고다님
        this.ViewState["againUrl"] = "/sponsor/pay/regular/pay_again?motiveCode=" + motiveCode.Value + "&motiveName=" + HttpUtility.JavaScriptStringEncode(motiveName.Value);

        if (payInfo.group == "CDSP")
        {


            result = this.DoPayCDSP(sponsorId);
            
            // 신규 결연이 아니고 자동 결제 정보가 있을 경우 TCPT 관련 SponsorGlobalID 생성 및 어린이 Hold 상태 변경 및 Kit 발송 테이블에 Insert
            if (result.success)
            {
                if (exist_account.Value == "Y")
                {
                    //[이종진] 2017-12-20 : 신규방법(GlobalPool)이면 DoPayCDSD() -> Pay() 에서 이미 처리 완료 
                    if (strdbgp_kind == "1")
                    {
                        RegistCommitmentTemp(sponsorId, sess.UserName, payInfo.childMasterId);
                    }
                }
            }

        }
        else
        {
            result = this.DoPayCIV(sponsorId);
        }

        ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 결제 시작 DoPay result=========" + result.success);

        if (result.success)
        {
            if (payment_method_card.Checked)
            {
                kcp_form.Visible = true;
                kcp_form.Show(this.ViewState);

            }
            else if (payment_method_oversea.Checked)
            {

                payInfo.month = Convert.ToInt32(oversea_pay_month.Value);
                payItem.data = payInfo.ToJson();
                payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo, null);
                //payItem.data = payInfo.ToJson();
                this.ViewState["payItem"] = payItem.ToJson();


                this.ViewState["campaignId"] = "";
                this.ViewState["jumin"] = "";
                this.ViewState["ci"] = "";
                this.ViewState["di"] = "";
                this.ViewState["good_mny"] = (payInfo.month * payInfo.amount).ToString();
                this.ViewState["pay_method"] = "100000000000";
                this.ViewState["overseas_card"] = "Y";
                this.ViewState["used_card_YN"] = "Y";
                this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_oversea"];
                this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card_Foreign"];
                kcp_form_temporary.Visible = true;
                kcp_form_temporary.Show(this.ViewState);
            }
            else if (payment_method_cms.Checked)
            {

                var actionResult = new PaymentAction().GetBankAccount();
                if (!actionResult.success)
                {
                    base.AlertWithJavascript(actionResult.message);
                }
                else
                {

                    this.ViewState["relation"] = cms_owner_type1.Checked ? cms_owner_type1.Value : cms_owner_type2.Value;
                    this.ViewState["cms_owner"] = cms_owner.Value;
                    this.ViewState["cms_account"] = cms_account.Value;
                    this.ViewState["birth"] = cms_account_type1.Checked ? cms_birth.Value : cms_soc.Value;
                    this.ViewState["bank_name"] = hd_cms_bank_name.Value;
                    this.ViewState["bank_code"] = hd_cms_bank.Value;
                    this.ViewState["cmsday"] = cmsday_5.Checked ? cmsday_5.Value : (cmsday_15.Checked ? cmsday_15.Value : cmsday_25.Value);

                    this.ViewState["src"] = string.Format("예금주와의 관계 : {0} /\n예금주 : {1} /\n예금주 생년월일 : {2} /\n은행명 : {3} /\n"
                               + "은행번호 : {4} /\n계좌번호 : {5} /\n이체일 : {6} /\n"
                               , this.ViewState["relation"]
                               , this.ViewState["cms_owner"]
                               , this.ViewState["birth"]
                               , this.ViewState["bank_name"]
                               , this.ViewState["bank_code"]
                               , this.ViewState["cms_account"]
                               , this.ViewState["cmsday"]
                               );

                    cms_form.Visible = true;
                    cms_form.Show(this.ViewState);


                }
            }
        }
    }

	// 정기후원 즉시결제
	bool Pay( pay_item_session payItem , string commitmentId) {

        
        var payInfo = payItem.data.ToObject<PayItemSession.Entity>();
        
        // 신용카드, CMS 인 경우 즉시결제 처리
        UserInfo sess = new UserInfo();
        //for (int i = 0; i < 50; i++)
        //{
            var actionResult = new BatchPay().Pay(commitmentId, payItem.orderId, payInfo.amount, payInfo.group);

            //[이종진] 2017-12-20 : 주석처리 문희원
            // -> 이종진 다시 주석 품. 일단 기존처리 방식이면 실행하도록 해놓음
            //신규 GlobalPool 조회방법인지 체크하여 GlobalSponsorID생성 및 NomoneyHold (기존소스 유지한것임)
            //신규방법은 Pay() 에서 GlobalSponsorID생성 및 Compass4에있는 NomoneyHold및 임시결연정보 삭제하고, GlobalCommitment생성
            string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();
            if (this.ViewState["childmasterid"] != null && strdbgp_kind == "1")
                RegistCommitmentTemp(sess.SponsorID, sess.UserName, this.ViewState["childmasterid"].ToString());

            if (actionResult.success)
            {
                if (actionResult.message != null && actionResult.message.Contains("즉시결제 대상은행이 아님"))
                {
                    string msg = "정기 후원 신청이 완료되었으나, 첫 후원금 결제가 정상적으로 처리되지 않았습니다. 첫 후원금을 다시 결제하시겠습니까?";
                    base.ConfirmWithJavascript(
                        msg,
                        "location.href= '" + this.ViewState["returnUrl"].ToString() + "'",
                            "location.href='" + this.ViewState["againUrl"].ToString() + "'");
                    return false;
                }
                else if (actionResult.message != null && actionResult.message.Contains("어린이를 다시 확인해주세요"))
                {
                    string msg = actionResult.message;
                    base.AlertWithJavascript(msg);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                //i++;
                //continue;

                /*
                    base.AlertWithJavascript(
                        string.Format(@"정기 후원 신청이 완료되었으나,{0}첫 후원금 결제가 정상적으로 처리되지 못했습니다. 1~3일 내에 등록하신 연락처로 전화 드려 후원금 납부를 도와 드리도록 하겠습니다.",
                        string.IsNullOrEmpty(actionResult.message) ? "" : string.Format("{0}의 사유로 ", actionResult.message)), 
                        string.Format("location.href='{0}'" , this.ViewState["returnUrl"].ToString()));
                */

                PayItemSession.SetCookie(this.Context, new PayItemSession.Entity() { type = payInfo.type, frequency = payInfo.frequency, group = payInfo.type.ToString(), codeId = payInfo.codeId, amount = payInfo.amount, childMasterId = payInfo.childMasterId, campaignId = payInfo.campaignId });

                if (payment_method_giro.Checked || payment_method_virtualaccount.Checked)
                {

                    base.ConfirmWithJavascript(
                            "후원 신청이 완료되었습니다.\\n\\r첫 후원금 결제를 바로 진행하시겠습니까?",
                            "location.href= '" + this.ViewState["returnUrl"].ToString() + "'",
                            "location.href='" + this.ViewState["againUrl"].ToString() + "'"
                            );
                    return false;
                }
                else
                {
                    base.ConfirmWithJavascript(
                            //"정기 후원 신청이 완료되었으나, [" + actionResult.message + "]의 사유로 첫 후원금 결제가 정상적으로 처리되지 못했습니다. 첫 후원금을 다시 결제하시겠습니까?",
                            "정기 후원 신청이 완료되었으나, 첫 후원금 결제가 정상적으로 처리되지 않았습니다. 첫 후원금을 다시 결제하시겠습니까?",
                            "location.href= '" + this.ViewState["returnUrl"].ToString() + "'",
                            "location.href='" + this.ViewState["againUrl"].ToString() + "'"
                            );

                    return false;
                }

            }
        //}
		
		//return true;
	}
    

    JsonWriter DoPayCDSP(string sponsorId) {
		var sess = new UserInfo();
		var action = new CommitmentAction();
		JsonWriter result = new JsonWriter();
		var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();

		/*
		이미 자동결제 정보가 있는 경우 추가 승인번호를 요청하지 않는다.
		*/
		if(exist_account.Value == "Y") {
			
            //어린이 정보 Insert(tChildMaster, tChildDetail, tChildHistory) 후, tCommitmentMaster Insert
            //어린이 정보 Insert는 신규 GP 조회방식 일 경우만 실행함. (web.config 에 dbgp = "2")
			result = action.DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName,motiveCode.Value, motiveName.Value);
            
            ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 DoPayCDSP result=========" + result.message);

            if (!result.success) {
				base.AlertWithJavascript(result.message);
				return result;
			}

			var commitmentId = result.data.ToString();

            ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 PAY commitmentId=========" + commitmentId);
            ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 PAY result.data.ToString()=========" + result.data.ToString());

            //ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 PAY RETURN URL=========" + this.Pay(payItem, result.data.ToString()));

            //결제
            if (this.Pay(payItem , result.data.ToString() )) {
                if (payment_method_giro.Checked || payment_method_virtualaccount.Checked)
                {
                    PayItemSession.SetCookie(this.Context, new PayItemSession.Entity() { type = payInfo.type, frequency = payInfo.frequency, group = payInfo.type.ToString(), codeId = payInfo.codeId, amount = payInfo.amount, childMasterId = payInfo.childMasterId, campaignId = payInfo.campaignId });

                    base.ConfirmWithJavascript(
                            "후원 신청이 완료되었습니다.\\n\\r첫 후원금 결제를 바로 진행하시겠습니까?",
                            "location.href= '" + this.ViewState["returnUrl"].ToString() + "'",
                            "location.href='" + this.ViewState["againUrl"].ToString() + "'"
                            );
                }else
                {
                    Response.Redirect(this.ViewState["returnUrl"].ToString());
                }
			} else {
                ErrorLog.Write(HttpContext.Current, 0, "1:1 결연 PAY RETURN URL=========" + this.ViewState["returnUrl"].ToString());

                // 즉시결제 실패해도 결연 성공으로 처리 
                // 2016-10-24 이석호 과장 요청
                /* 테스트를 위해 잠시 삭제 주석 제거..20170313

                action.DeleteCommitment(commitmentId);
                */
                result.success = false;
				result.message = "결제실패";
				return result;
			}
			
		} else {

            action.ValidateCDSP(payInfo.childMasterId, payInfo.campaignId, payInfo.codeName, sponsorId, ref result);
            if (!result.success)
            {
                base.AlertWithJavascript(result.message);
                return result;
            }

            result.success = true;
        }

		return result;
	}

	JsonWriter DoPayCIV(string sponsorId) {
		var sess = new UserInfo();
		var action = new CommitmentAction();
		JsonWriter result = new JsonWriter();
		var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
		var payInfo = payItem.data.ToObject<PayItemSession.Entity>();


		/*
		이미 자동결제 정보가 있는 경우 추가 승인번호를 요청하지 않는다.
		*/
		if(exist_account.Value == "Y") {
			
			result = action.DoCIV(sponsorId, payInfo.amount , 1 , "", payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.childMasterId , payInfo.campaignId, motiveCode.Value, motiveName.Value);
			
			if(!result.success) {
				base.AlertWithJavascript(result.message);
				return result;
			}

			var commitmentId = result.data.ToString();

			// 경고메세지를 띄우는 경우
			if(result.action != null && result.action == "confirm") {
				base.ConfirmWithJavascript(result.message, "goBack()" , "location.href='"+ this.ViewState["returnUrl"].ToString() + "'");
				result.success = false;
				return result;
			}

			if(this.Pay(payItem , result.data.ToString())) {
                if (payment_method_giro.Checked || payment_method_virtualaccount.Checked)
                {
                    base.ConfirmWithJavascript(
                            "정기후원 신청이 완료되었습니다.\\n\\r첫 후원금 결제를 바로 진행하시겠습니까?",
                            "location.href= '" + this.ViewState["returnUrl"].ToString() + "'",
                            "location.href='" + this.ViewState["againUrl"].ToString() + "'"
                            );
                }
                else
                {
                    Response.Redirect(this.ViewState["returnUrl"].ToString());
                }
            } else {
                /* 테스트를 위해 잠시 삭제 주석 제거..20170313
                action.DeleteCommitment(commitmentId);
                */
                result.success = false;
				result.message = "결제실패";
				return result;
			}
			
		} else {
			
			// CIV 의 경우
			// action = confirm 인경우 javascript confirm 후 false 면 history.back
			action.ValidateCIV(sponsorId, payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.campaignId, ref result);
			
			if(!result.success) {
				base.AlertWithJavascript(result.message);
				return result;
			}

			// 경고메세지를 띄우는 경우
			if(result.action != null && result.action == "confirm") {
				base.ConfirmWithJavascript(result.message, "goBack()");
			}
			
		}

		return result;
	}
}
