﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pay_again.aspx.cs" Inherits="sponsor_regular_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>

<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/sponsor/item-child-payagain.ascx" TagPrefix="uc" TagName="child" %>
<%@ Register Src="/sponsor/item-special.ascx" TagPrefix="uc" TagName="sf" %>
<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/sponsor/pay/temporary/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/sponsor/pay/temporary/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/sponsor/pay/temporary/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
    <script type="text/javascript" src="/sponsor/pay/regular/pay_again.js"></script>
    <script type="text/javascript" src="/common/js/site/motive.js"></script>
    <script type="text/javascript">
        $(function () {
            $page.init();

            $motive.init($("#motiveCode"), $("#motiveName"), $("#motive1"), $("#motive2"));

            

        });

    </script>


</asp:Content>

<asp:Content runat="server" ID="head_middle" ContentPlaceHolderID="head_middle">
    <!-- 최근 확인한 어린이 -->
    <asp:PlaceHolder runat="server" ID="ph_child" Visible="false">
        <div class="recent_child_wrap">
            <div class="recent_child">
                <a href="#" id="btn_goCDSP" data-childmasterid="<%:this.ViewState["childmasterid"].ToString()%>"><span class="img" style="background: url('<%:this.ViewState["pic"].ToString()%>') no-repeat center; background-size: cover; background-size: 104px; background-position-y: 0"></span></a>

                <span class="tooltip" style="display: block; z-index: 10">
                    <span class="tit">최근 확인하신 어린이 정보가 있습니다.</span>
                    <span class="con">해당 어린이 확인을 원하시면 이미지를 클릭해 주세요.<br />
                        클릭 시 후원 결제 페이지로 이동합니다.
                    </span>
                    <button class="close" onclick="$('.tooltip').hide();return false;">닫기</button>
                    <span class="arr"></span>
                </span>
            </div>
        </div>
    </asp:PlaceHolder>
    <!--// 최근 확인한 어린이 -->
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" id="user_name" runat="server" />
    <input type="hidden" id="gender" runat="server" />
    <input type="hidden" id="hd_auth_domain" runat="server" />
    <input type="hidden" id="hd_age" runat="server" />
    <input type="hidden" id="hd_amount" runat="server" />

    <input type="hidden" runat="server" id="jumin" value="" />
    <input type="hidden" runat="server" id="ci" value="" />
    <!-- 본인인증 CI -->
    <input type="hidden" runat="server" id="di" value="" />
    <!-- 본인인증 DI -->
    <input type="hidden" runat="server" id="cert_gb" value="" />
    <!-- 본인인증 수단 -->

    <input type="hidden" runat="server" id="user_class" value="20세이상" />
    <input type="hidden" runat="server" id="parent_cert" value="" />
    <input type="hidden" runat="server" id="parent_name" value="" />
    <input type="hidden" runat="server" id="parent_juminId" value="" />
    <input type="hidden" runat="server" id="parent_mobile" value="" />
    <input type="hidden" runat="server" id="parent_email" value="" />


    <input type="hidden" runat="server" id="exist_account" value="" />
    <input type="hidden" runat="server" id="hdManageType" value="" />
    <input type="hidden" runat="server" id="hdState" value="" />
    <input type="hidden" runat="server" id="hfAddressType" value="" />
    <input type="hidden" runat="server" id="hdLocation" value="" />
    <input type="hidden" runat="server" id="hdOrganizationID" value="" />
    <input type="hidden" runat="server" id="hdSponsorId" value="" />
    <input type="hidden" runat="server" id="hdBirthDate" value="" />
    <input type="hidden" runat="server" id="zipcode" />
    <input type="hidden" runat="server" id="addr1" />
    <input type="hidden" runat="server" id="addr2" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
    
    <input type="hidden" runat="server" id="hdCI" value="" />
    <!-- 기존 등록된 본인인증 CI -->
    <input type="hidden" runat="server" id="dmYN" value="Y" />
    <input type="hidden" runat="server" id="translationYN" value="N" />

    <!-- 후원자 정보 -->
    <input type="hidden" runat="server" id="last_name" />
    <input type="hidden" runat="server" id="first_name" />
    <input type="hidden" runat="server" id="religion" />
    <input type="hidden" runat="server" id="church_name" />

    <input type="hidden" runat="server" id="addr_domestic_zipcode" />
    <input type="hidden" runat="server" id="addr_domestic_addr1" />
    <input type="hidden" runat="server" id="addr_domestic_addr2" />

    <input type="hidden" runat="server" id="addr_overseas_zipcode" />
    <input type="hidden" runat="server" id="addr_overseas_addr1" />
    <input type="hidden" runat="server" id="addr_overseas_addr2" />

    <input type="hidden" runat="server" id="motiveCode" />
    <input type="hidden" runat="server" id="motiveName" />

    <!-- 후원자 정보 끝-->


    <section class="sub_body" ng-app="cps">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>첫 후원금</em></h1>
                <span class="desc">후원자님 사랑에 감사드립니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents sponsor">
            <div class="payment">

                <div class="w980">

                    <!-- 후원정보 -->

                    <uc:child runat="server" ID="view_child" Visible="false" />

                    <uc:sf runat="server" ID="view_sf" Visible="false" />


                    <!--// 후원정보 -->

                    <!-- 결제 테이블 -->
                    <input type="checkbox" class="css_checkbox" id="p_receipt_pub_ok" name="p_receipt_pub"  visible="false" runat="server" checked/>
                    <input type="checkbox" class="css_checkbox" id="addr_overseas" visible="false" runat="server" ng-click="changeValue()" />

                    <!-- 결제정보 -->
                    <div class="table_tit">
                        <span class="tit">결제 정보</span>
                        <span class="nec_info">표시는 필수입력 사항입니다.</span>
                    </div>

                    <div class="tableWrap2 mb30">
                        <table class="tbl_type1">
                            <caption>결제방법 선택 테이블</caption>
                            <colgroup>
                                <col style="width: 20%" />
                                <col style="width: 80%" />
                            </colgroup>
                            <tbody>
								<tr>
									<th scope="row"><span class="nec">결제방법</span></th>
									<td>
										<span class="radio_ui mb15">
											<input type="radio" id="payment_method_card" runat="server" name="payment_method"  class="css_radio" checked />
											<label for="payment_method_card" class="css_label" style="width:170px">신용카드 결제</label>

											<input type="radio" id="payment_method_cms" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_cms" class="css_label" style="width:170px">실시간 계좌이체</label>

											<input type="radio" id="payment_method_oversea" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_oversea" class="css_label" style="width:170px">해외발급 카드</label>
										</span>
										<span class="radio_ui">
											<input type="radio" id="payment_method_kakao" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_kakao" class="css_label relative" style="width:170px;"><img src="/common/img/icon/kakaopay.jpg" class="kakao" style="pointer-events:none" alt="kakao pay" onclick="$('#payment_method_kakao').trigger('click')" />kakaopay</label>

											<input type="radio" id="payment_method_payco" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_payco" class="css_label relative" style="width:170px;"><img src="/common/img/icon/payco.jpg" class="payco" style="pointer-events:none" alt="payco" onclick="$('#payment_method_payco').trigger('click')" />payco</label>

											<input type="radio" id="payment_method_phone" runat="server" name="payment_method"  class="css_radio" />
											<label for="payment_method_phone" class="css_label" style="width:170px">휴대폰 결제</label>
										</span>
									</td>
								</tr>
								
							</tbody>
                        </table>

                    </div>

                    <!--// 결제 테이블 -->

					<div class="box_type4 padding1 mb40">
						<p class="mb5 fc_black">※ 결제 전 꼭 확인해주세요!</p>
						<ul>
							<li><span class="s_con1">후원금 결제 시 일부 카드사의 경우 결제 대행업체인 [NHN KCP 한국 사이버결제]로 승인 SMS가 전송됩니다.</span></li>
							<li><span class="s_con1">결제관련 문의 : <em class="fc_blue">후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / <a href="mailto:info@compassion.or.kr" class="fc_blue">info@compassion.or.kr</a></em></span></li>
						</ul>
					</div>

					<div class="tac mb60"><asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click" cssclass="btn_type1">결제</asp:LinkButton></div>

					<div class="box_type3">
						<span class="s_con1">회원가입이 어려우시거나 본인확인(휴대폰/이메일인증)이 안되시는 경우 한국컴패션으로 연락주시기 바랍니다.<br />
						<em class="fc_blue">후원자 서비스팀 (02-740-1000 / info@compassion.or.kr)로 문의해주세요.</em></span>
					</div>

                </div>

            </div>
        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>
        <!--KCP 설치 메세지창 ,사용안함-->
		<div id="display_setup_message" style="display:none"></div>

    </section>



</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
    <uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" ID="kcp_form" />
	<uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="payco_form" />
	<uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kakaopay_form" />


</asp:Content>
