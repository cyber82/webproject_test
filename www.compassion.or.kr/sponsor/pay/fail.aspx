﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fail.aspx.cs" Inherits="sponsor_pay_fail" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script>

	(function () {
		var app = angular.module('cps.page', []);
		app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

		});

	})();

</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
<section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>후원/결제</em></h1>
				<span class="desc">후원자님 사랑에 감사드립니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<!-- s: sub contents -->
		<div class="subContents sponsor">

			<div class="payment">
				
				<div class="w980">

					<!-- 후원정보 -->
					<div class="failure">
						
						<span class="txt1">다시 결제해주세요.</span>
						<span class="txt2"><em><asp:Literal runat="server" ID="lb_title" /> 결제</em>가 실패하였습니다.</span>

					</div>

					<div class="tac">
						<a href="#" runat="server" id="goPay" class="btn_type1">다시 결제하기</a>
					</div>
					<!--// 후원정보 -->

					
				</div>
				
			</div>
		</div>	
		<!--// e: sub contents -->

		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
</asp:Content>
