﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="item-user-funding.ascx.cs" Inherits="sponsor_item_user_funding" %>

<script>

	(function () {
		//	var app= angular.module('cps.page', []);
		if (!app)
			app = angular.module('cps.page', []);
		app.controller("cdspCtrl", function ($scope, $http, $filter, popup) {

			$scope.cancel = function ($event) {
				$event.preventDefault();
				
				$http.get("/sponsor/pay-gateway.ashx?t=cancel", { params: {} }).success(function (r) {
					
					if (r.success) {
						location.href = r.data;
					} else {
						alert(r.message);
					}
				});

			}

		});

	})();

</script>

<div class="funding_info" ng-app="cps" ng-cloak ng-controller="cdspCtrl">
	<p class="s_tit1 mb15">후원 정보</p>

	<table class="tbl_child mb40">
		<caption>어린이정보 및 후원금액 테이블</caption>
		<colgroup>
			<col style="width:50%" />
			<col style="width:50%" />
		</colgroup>
		<tbody>
			<tr>
				<td rowspan="2" class="pic">
					<!-- 이미지사이즈 : 400 * 300 -->
					<span style="background:url('<asp:Literal runat="server" ID="img" />') no-repeat center;background-size:cover;"></span>
				</td>
				<td class="info">
					<div class="btn_cancel"><a href="#" ng-click="cancel($event)">후원취소</a></div>
					<span class="label"><asp:Literal runat="server" ID="typeName" /></span>
					<p class="name"><asp:Literal runat="server" ID="title" /></p>

				</td>
			</tr>
			<tr>
				<td class="info">
					<span class="radio_ui mb20">
						<span class="field2">후원유형</span>
						<span class="data"><asp:Literal runat="server" ID="frequency"/>후원</span>
					</span>
					<div>
						<span class="field2">후원금액</span>
						<span class="data2"><asp:Literal runat="server" ID="amount"/></span>
					</div>
				
				</td>
			</tr>
			<tr>
				<td colspan="2" class="amount">
					<span class="field">총 결제 금액</span><span class="sum"><asp:Literal runat="server" ID="amount2"/></span><span class="won" runat="server" id="amount2_unit">원/월</span>
				</td>
			</tr>
		</tbody>
	</table>
</div>
