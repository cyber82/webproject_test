﻿<%@ WebHandler Language="C#" Class="sponsor_pay_gateway" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;

public class sponsor_pay_gateway : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "go-cdsp") {
            this.GoCDSPPay(context);
        }else if(t == "go-cdsp-new") {
            this.GoCDSPPayNew(context);
        }else if(t == "go-special") {
            this.GoSpecialFundingPay(context);
        }else if(t == "go-user-funding") {
            this.GoUserFundingPay(context);
        }else if(t == "cancel") {
            this.Cancel(context);
        }else if(t == "go-cdsp-letter") {
            this.GoCDSPLetterPay(context);
        }

    }

    void GoUserFundingPay( HttpContext context ) {

        var amount = Convert.ToInt32(context.Request["amount"].EmptyIfNull().EscapeSqlInjection());
        var uf_id = context.Request["uf_id"].EmptyIfNull().EscapeSqlInjection();

        JsonWriter result = new JsonWriter() {
            success = false
        };

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.sp_tUserFunding_get_f(Convert.ToInt32(uf_id)).FirstOrDefault();
            Object[] op1 = new Object[] { "uf_id" };
            Object[] op2 = new Object[] { Convert.ToInt32(uf_id) };
            var entity = www6.selectSP("sp_tUserFunding_get_f", op1, op2).DataTableToList<sp_tUserFunding_get_fResult>().FirstOrDefault();

            if (entity == null)
            {
                result.message = "나눔펀딩정보를 찾을 수 없습니다.";
                result.Write(context);
                return;
            }

            var group = "";
            var codeId = "";
            var codeName = "";
            if (entity.uf_type == "child")
            {
                group = "CIV";
                codeId = "DF";
                codeName = "1:1어린이양육프로그램";
            }
            else
            {
                //var sf = dao.sp_tSpecialFunding_get_f(entity.CampaignID).FirstOrDefault();
                Object[] op3 = new Object[] { "CampaignID" };
                Object[] op4 = new Object[] { entity.CampaignID };
                var sf = www6.selectSP("sp_tSpecialFunding_get_f", op3, op4).DataTableToList<sp_tSpecialFunding_get_fResult>().FirstOrDefault();
                if (sf == null)
                {
                    result.message = "캠페인정보가 없습니다.";
                    result.Write(context);
                    return;
                }

                group = sf.AccountClassGroup;
                codeId = sf.AccountClass;
                codeName = sf.CampaignName;

            }

            //var childMasterId = entity.ChildMasterID;
            var childMasterId = "0000000000";
            PayItemSession.SetCookie(context, new PayItemSession.Entity()
            {
                type = PayItemSession.Entity.enumType.USER_FUNDING,
                campaignId = entity.CampaignID,
                frequency = "일시",
                group = group,
                codeId = codeId,
                codeName = codeName,
                amount = Convert.ToInt32(amount),
                childMasterId = childMasterId,
                relation_key = entity.uf_id
            });

        }

        if(UserInfo.IsLogin) {

            result.data = "/sponsor/pay/temporary/";

        } else {
            result.data = "/sponsor/info/";
        }

        result.success = true;
        result.Write(context);

    }

    void GoSpecialFundingPay( HttpContext context ) {

        var frequency = context.Request["frequency"].EmptyIfNull().EscapeSqlInjection();        // 정기 , 일시
        var amount = Convert.ToInt32(context.Request["amount"].EmptyIfNull().EscapeSqlInjection());
        var campaignId = context.Request["campaignId"].EmptyIfNull().EscapeSqlInjection();

        JsonWriter result = new JsonWriter() {
            success = false
        };

        /*
        미납금 체크는 CDSP만 한다.
        2016-10-25 정홍은 요청사항 
    if(frequency == "정기") {
        var checkResult = this.CheckRegularPay();
        if(!checkResult.success) {
            checkResult.Write(context);
            return;
        }
    }
    */

        using (FrontDataContext dao = new FrontDataContext())
        {

            //var entity = dao.sp_tSpecialFunding_get_f(campaignId).FirstOrDefault();
            Object[] op1 = new Object[] { "campaignId" };
            Object[] op2 = new Object[] { campaignId };
            var entity = www6.selectSP("sp_tSpecialFunding_get_f", op1, op2).DataTableToList<sp_tSpecialFunding_get_fResult>().FirstOrDefault();

            if (entity == null)
            {
                result.message = "캠페인정보를 찾을 수 없습니다.";
                result.Write(context);
                return;
            }

            PayItemSession.SetCookie(context, new PayItemSession.Entity()
            {
                type = PayItemSession.Entity.enumType.SPECIAL_FUNDING,
                relation_key = entity.sf_id,
                campaignId = campaignId,
                frequency = frequency,
                group = entity.AccountClassGroup,
                codeId = entity.AccountClass,
                codeName = entity.CampaignName,
                amount = amount
            });
        }

        if(UserInfo.IsLogin) {
            //if (frequency == "정기")
            //	result.data = "/sponsor/pay/regular/";
            //else
            //	result.data = "/sponsor/pay/temporary/";

            UserInfo sess = new UserInfo();
            if (frequency == "정기" || frequency == "regular")
            {
                result.data = "/sponsor/pay/regular/";
                ErrorLog.Write(HttpContext.Current, 0, "특별한나눔 후원하기(mobile) - UserID:" + sess.UserId + " CampaignId:" + campaignId + " 결제:정기");
            }

            else
            {
                result.data = "/sponsor/pay/temporary/";
                ErrorLog.Write(HttpContext.Current, 0, "특별한나눔 후원하기(mobile) - UserID:" + sess.UserId + " CampaignId:" + campaignId + " 결제:일시");
            }

        } else {
            result.data = "/sponsor/info/";
        }

        result.success = true;
        result.Write(context);

    }

    void Cancel( HttpContext context ) {
        JsonWriter result = new JsonWriter() {
            success = false
        };

        if(!PayItemSession.HasCookie(context)) {
            //result.message = "TEST_001";
            result.data = "/sponsor/";
        } else {
            var payItem = PayItemSession.GetCookie(context);
            var type = payItem.type;
            //result.message = "TEST_002 type : " + type.ToString();

            switch(type) {
                default:
                    result.data = "/sponsor/";
                    break;
                case PayItemSession.Entity.enumType.CDSP:
                    result.data = "/sponsor/children/";
                    if (!string.IsNullOrEmpty(payItem.childMasterId))
                    {
                        result.message = new ChildAction().Release(payItem.childMasterId);
                    }
                    break;
                case PayItemSession.Entity.enumType.USER_FUNDING:
                    result.data = "/sponsor/user-funding/";
                    break;
                case PayItemSession.Entity.enumType.SPECIAL_FUNDING:
                    result.data = "/sponsor/special/";
                    break;
                case PayItemSession.Entity.enumType.CSP_DOL:
                    result.data = "/sponsor/special/first-birthday/";
                    break;
                case PayItemSession.Entity.enumType.CSP_WEDDING:
                    result.data = "/sponsor/special/wedding/";
                    break;
            }
        }

        PayItemSession.ClearCookie(context);
        result.success = true;
        result.Write(context);
    }

    void GoCDSPPay( HttpContext context ) {
        var childMasterId = context.Request["childMasterId"].EmptyIfNull().EscapeSqlInjection();        // 어린이아이디
        var childKey = context.Request["childKey"].EmptyIfNull().EscapeSqlInjection();
        //[이종진] 추가 - childmasterId = '09' + GlobalId 이므로 GlobalID는 substring(2) 
        var childGlobalId = context.Request["childMasterId"].EmptyIfNull().EscapeSqlInjection().Substring(2);
        var Pic = context.Request["Pic"].EmptyIfNull().EscapeSqlInjection();

        var IsOrphan = context.Request["IsOrphan"].EmptyIfNull().EscapeSqlInjection();
        var IsHandicapped = context.Request["IsHandicapped"].EmptyIfNull().EscapeSqlInjection();
        var WaitingDays = context.Request["WaitingDays"].EmptyIfNull().EscapeSqlInjection();
        var Age = context.Request["Age"].EmptyIfNull().EscapeSqlInjection();
        var BirthDate = context.Request["BirthDate"].EmptyIfNull().EscapeSqlInjection();
        var CountryCode = context.Request["CountryCode"].EmptyIfNull().EscapeSqlInjection();
        var Gender = context.Request["Gender"].EmptyIfNull().EscapeSqlInjection();
        var HangulName = context.Request["HangulName"].EmptyIfNull().EscapeSqlInjection();
        var HangulPreferredName = context.Request["HangulPreferredName"].EmptyIfNull().EscapeSqlInjection();
        var FullName = context.Request["FullName"].EmptyIfNull().EscapeSqlInjection();
        var PreferredName = context.Request["PreferredName"].EmptyIfNull().EscapeSqlInjection();

        JsonWriter result = new JsonWriter() {
            success = false
        };

        //        result.message = "001";

        var checkResult = this.CheckRegularPay();
        if(!checkResult.success) {
            checkResult.Write(context);
            return;
        }
        //        result.message = "002";

        var childAction = new ChildAction();

        var actionResult = new JsonWriter();
            
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();
        if (strdbgp_kind == "1") // 기존 방식 -어린이 리스트 DB조회
        {
            actionResult = childAction.Ensure(childMasterId);
        }
        else if (strdbgp_kind == "2") // 신규 방식 -어린이 리스트 Global Pool조회
        {
            actionResult = childAction.EnsureGp(childMasterId, childKey, Pic, IsOrphan, IsHandicapped, WaitingDays, Age, BirthDate, CountryCode, Gender, HangulName, HangulPreferredName, FullName, PreferredName);

            //if (!actionResult.success) {
            //    result.message = actionResult.message;
            //    try
            //    {
            //        result.Write(context);
            //    }
            //    catch (Exception ex)
            //    {
            //    }
            //    return;
            //}
        }

        if(!actionResult.success) {
            //2017-12-14 이종진 - 에러메시지를 사용자에게 보여줌
            result.message = actionResult.message;
            result.Write(context);
            return;
        }

        PayItemSession.SetCookie(context, new PayItemSession.Entity() {  type = PayItemSession.Entity.enumType.CDSP , frequency = "정기" , group = "CDSP" , codeId = "DS" , amount = 45000, childMasterId = childMasterId, childKey = childKey, childGlobalId = childGlobalId });
        if(UserInfo.IsLogin) {
            result.data = "/sponsor/pay/regular/";
        } else {
            result.data = "/sponsor/info/";
        }

        result.message = "childMasterId : " + childMasterId;
        result.message += " childKey : " + childKey;
        result.message += " Pic : " + Pic;
        result.message += " IsOrphan : " + IsOrphan;
        result.message += " IsHandicapped : " + IsHandicapped;
        result.message += " WaitingDays : " + WaitingDays;
        result.message += " Age : " + Age;
        result.message += " BirthDate : " + BirthDate;
        result.message += " CountryCode : " + CountryCode;
        result.message += " Gender : " + Gender;
        result.message += " HangulName : " + HangulName;
        result.message += " HangulPreferredName : " + HangulPreferredName;
        result.message += " FullName : " + FullName;
        result.message += " PreferredName : " + PreferredName;

        //        result.message = "00008";

        result.success = true;
        result.Write(context);

    }

    //Keep 체크시 sessionID 체크 안함
    void GoCDSPPayNew( HttpContext context ) {
        var childMasterId = context.Request["childMasterId"].EmptyIfNull().EscapeSqlInjection();        // 어린이아이디
        var childGlobalId = context.Request["childMasterId"].EmptyIfNull().EscapeSqlInjection().Substring(2);
        var childKey = context.Request["childKey"].EmptyIfNull().EscapeSqlInjection();

        JsonWriter result = new JsonWriter() {
            success = false
        };

        var checkResult = this.CheckRegularPay();
        if(!checkResult.success) {
            checkResult.Write(context);
            return;
        }

        var childAction = new ChildAction();
        var actionResult = childAction.EnsureNew(childMasterId);
        if(!actionResult.success) {
            result.message = actionResult.message;
            result.Write(context);
            return;
        }
        //PayItemSession.SetCookie(context, new PayItemSession.Entity() {  type = PayItemSession.Entity.enumType.CDSP , frequency = "정기" , group = "CDSP" , codeId = "DS" , amount = 45000, childMasterId = childMasterId });
        PayItemSession.SetCookie(context, new PayItemSession.Entity() {  type = PayItemSession.Entity.enumType.CDSP , frequency = "정기" , group = "CDSP" , codeId = "DS" , amount = 45000, childMasterId = childMasterId, childKey = childKey, childGlobalId = childGlobalId });
        if(UserInfo.IsLogin) {
            result.data = "/sponsor/pay/regular/";
        } else {
            result.data = "/sponsor/info/";
        }

        result.success = true;
        result.Write(context);

    }

    void GoCDSPLetterPay( HttpContext context ) {
        var childMasterId = context.Request["childMasterId"].EmptyIfNull().EscapeSqlInjection();        // 어린이아이디

        JsonWriter result = new JsonWriter() {
            success = false
        };

        var checkResult = this.CheckRegularPay();
        if(!checkResult.success) {
            checkResult.Write(context);
            return;
        }

        var childAction = new ChildAction();
        /*
		var actionResult = childAction.Ensure(childMasterId);
		if(!actionResult.success) {
			result.message = actionResult.message;
			result.Write(context);
			return;
		}
        */
        PayItemSession.SetCookie(context, new PayItemSession.Entity() {  type = PayItemSession.Entity.enumType.CDSP , frequency = "정기" , group = "CDSP" , codeId = "DS" , amount = 45000, childMasterId = childMasterId });
        if(UserInfo.IsLogin) {
            result.data = "/sponsor/pay/regular/pay_again";
        } else {
            result.data = "/sponsor/info/";
        }

        result.success = true;
        result.Write(context);

    }

    JsonWriter CheckRegularPay() {
        // 미납급 체크
        UserInfo sess = new UserInfo();
        var result = new JsonWriter() { success = true };

        if(!string.IsNullOrEmpty(sess.SponsorID)) {
            new CommitmentAction().GetNonPayment(sess.SponsorID, ref result);

            if(!result.success) {
                result.message = result.message.Replace("\\n" , "\n");
                result.action = "nonpayment";
                return result;
            }
        }

        return result;
    }

}