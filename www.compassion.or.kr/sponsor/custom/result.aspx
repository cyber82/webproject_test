﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="result.aspx.cs" Inherits="sponsor_custom_result" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        $(function () {
        	$("#btn_goCDSP").click(function () {
                $.get("/sponsor/pay-gateway.ashx?t=go-cdsp", { childMasterId: $("#childmasterid").val() }).success(function (r) {
                    if (r.success) {
                        location.href = r.data;
                    } else {
                        alert(r.message);
                    }
                });
            })
        });
    </script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="childmasterid" />
    <section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>맞춤 <em>후원</em></h1>
				<span class="desc">간단한 질문을 통해, 후원자님의 소중한 가족이 될 어린이를 소개해드립니다</span>

			<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents sponsor">

			<div class="custom">
				<div class="w980">

					<div class="sub_tit mb40">
						<h2 class="tit blue"><em>맞춤 후원</em> 결과</h2>
						<p class="mb50"><em class="fs18">후원자님의 소중한 가족이 될 어린이를 찾았습니다!</em></p>
					</div>

					<div class="result">
						<span class="child" style="background:url('<%:this.ViewState["childPic"].ToString()%>') no-repeat center top;">어린이사진</span>

						<p>
							<em><asp:Literal runat="server" ID="country_name"/></em>에 사는
                             <em><asp:Literal runat="server" ID="age"/>살 <asp:Literal runat="server" ID="name"/></em>에게 말해주세요. 앞으로의 중요한 순간들을 함께하겠다고.<br />
							후원자님의 사랑으로 이 어린이의 삶이 변화됩니다.
						</p>
					</div>

					<div class="tac relative">
						<a href="#" class="btn_type1" id="btn_goCDSP">후원하러 가기</a>

						<a href="/sponsor/custom/" class="btn_restart">다시하기</a>
					</div>

				</div>

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>


</asp:Content>
