﻿$(function () {

    $(".btn_step01").change(function () {
        setTimeout(function () {

            $(".step01").hide();
            $(".step02").show();

            $(".step_circle1").removeClass("on");
            $(".step_circle2").addClass("on");

            scrollTo($("#l"));
        }, 500);

        $("#min_age").val($(this).data("min_age"))
        $("#max_age").val($(this).data("max_age"))
        //console.log($("#min_age").val())
        //console.log($("#max_age").val())

    })

    $(".btn_step02").change(function () {
        setTimeout(function () {
            $(".step02").hide();
            $(".step03").show();

            $(".step_circle2").removeClass("on");
            $(".step_circle3").addClass("on");

            scrollTo($("#l"));
        }, 500);
        $("#gender").val($(this).val());
    })

    $(".btn_step03").change(function () {
        var selectStep = ".step04_" + $(this).val();

        setTimeout(function () {
            $(".step03").hide();
            $(".step04").show();

            $(".step_circle3").removeClass("on");
            $(".step_circle4").addClass("on");

            $(selectStep).show();

            scrollTo($("#l"));
        }, 500);
    })

    $(".btn_step04").change(function () {
        $("#country").val($(this).val());
        setTimeout(function () {
            location.href = "/sponsor/custom/result/?min_age=" + $("#min_age").val() + "&max_age=" + $("#max_age").val() + "&gender=" + $("#gender").val() + "&country=" + $("#country").val();
        }, 500);
    })

    // 이미지 클릭시
    $(".img").click(function () {

        $(this).next($(".radio_ui")).children($("input")).attr("checked", true);

        if ($(this).next($(".radio_ui")).children($("input")).hasClass("btn_step01")) {
            setTimeout(function () {
                $(".step01").hide();
                $(".step02").show();

                $(".step_circle1").removeClass("on");
                $(".step_circle2").addClass("on");

                scrollTo($("#l"));
            }, 500);

            $("#min_age").val($(this).next($(".radio_ui")).children($("input")).data("min_age"))
            $("#max_age").val($(this).next($(".radio_ui")).children($("input")).data("max_age"))
            //console.log(1, $("#min_age").val())
            //console.log(2, $("#max_age").val())

        } else if ($(this).next($(".radio_ui")).children($("input")).hasClass("btn_step02")) {
            setTimeout(function () {
                $(".step02").hide();
                $(".step03").show();

                $(".step_circle2").removeClass("on");
                $(".step_circle3").addClass("on");

                scrollTo($("#l"));
            }, 500);
            $("#gender").val($(this).next($(".radio_ui")).children($("input")).val());

        } else if ($(this).next($(".radio_ui")).children($("input")).hasClass("btn_step03")) {
            var selectStep = ".step04_" + $(this).next($(".radio_ui")).children($("input")).val();

            setTimeout(function () {
                $(".step03").hide();
                $(".step04").show();

                $(".step_circle3").removeClass("on");
                $(".step_circle4").addClass("on");

                $(selectStep).show();

                scrollTo($("#l"));
            }, 500);

        } else if ($(this).next($(".radio_ui")).children($("input")).hasClass("btn_step04")) {
            $("#country").val($(this).next($(".radio_ui")).children($("input")).val());
            setTimeout(function () {
                location.href = "/sponsor/custom/result/?min_age=" + $("#min_age").val() + "&max_age=" + $("#max_age").val() + "&gender=" + $("#gender").val() + "&country=" + $("#country").val();
            }, 500);
        }

    })

});

