﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="sponsor_custom_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/sponsor/custom/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="min_age" />
    <input type="hidden" runat="server" id="max_age" />
    <input type="hidden" runat="server" id="gender" />
    <input type="hidden" runat="server" id="country" />

    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>맞춤 <em>후원</em></h1>
                <span class="desc">간단한 질문을 통해, 후원자님의 소중한 가족이 될 어린이를 소개해드립니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 sponsor">

            <div class="custom" id="l">
                <div class="w980">

                    <!-- step -->
                    <ul class="stepWrap clear2">
                        <li class="on step_circle1"><span class="bg"><span class="step">STEP</span><span class="num">01</span></span></li>
                        <li class="step_circle2"><span class="bg"><span class="step">STEP</span><span class="num">02</span></span></li>
                        <li class="step_circle3"><span class="bg"><span class="step">STEP</span><span class="num">03</span></span></li>
                        <li class="step_circle4"><span class="bg"><span class="step">STEP</span><span class="num">04</span></span></li>
                    </ul>
                    <!--// step -->

                </div>

                <div class="tit_bg">
                    <div class="w980">

                        <!-- 단계에 따른 문구 -->
                        <p class="txt_step ts1 step01">
                            <span class="txt1">타임머신을 발견한 당신,</span><br />
                            <span class="txt2">어린 시절로 돌아갈 수 있다면 어느 때로 가고 싶으신가요?</span>
                        </p>
                        <p class="txt_step ts2 step02" style="display: none">
                            <span class="txt1">타임머신에서 내린 당신은<br />
                                두근거리는 마음으로 교실에 들어갔습니다.</span><br />
                            <span class="txt2">내 짝꿍은 누구일까요?</span>
                        </p>
                        <p class="txt_step ts3 step03" style="display: none">
                            <span class="txt1">당신은 학생의 역할을 멋지게 해내고<br />
                                방학을 맞이해 가족여행을 가게 됐습니다.</span><br />
                            <span class="txt2">방학을 어느 곳에서 보내실 건가요?</span>
                        </p>
                        <p class="txt_step ts4 step04" style="display: none">
                            <span class="txt1">즐거웠던 방학을 마무리하며 사진을 정리하던 중,<br />
                                왠지 마음에 끌리는 사진을 발견했습니다.</span><br />
                            <span class="txt2">그 사진은 무엇인가요?</span>
                        </p>
                        <!--// 단계에 따른 문구 -->

                    </div>
                </div>

                <div class="w980 stepImg">

                    <!-- step1 -->
                    <div class="wrap stepImg1 step01">
                        <ul class="clear2">
                            <li>
                                <span class="img img1_1 btn_item">호기심 많은<br />
                                    유치원생</span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus1_1" data-min_age="3" data-max_age="5" class="css_radio btn_step01" />
                                    <label for="cus1_1" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img1_2">활발한 매력이<br />
                                    넘치는 초등학생</span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus1_2" class="css_radio btn_step01" data-min_age="6" data-max_age="12" />
                                    <label for="cus1_2" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img1_3">감수성이 풍부한<br />
                                    중&middot;고등학생</span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus1_3" class="css_radio btn_step01" data-min_age="13" />
                                    <label for="cus1_3" class="css_label">선택</label>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <!--// -->

                    <!-- step2 -->
                    <div class="wrap stepImg2 step02" style="display: none">
                        <ul class="clear2">
                            <li>
                                <span class="img img2_1">눈웃음이 매력적인<br />
                                    여학생</span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus2_1" class="css_radio btn_step02" value="F" />
                                    <label for="cus2_1" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img2_2">장난기 가득한<br />
                                    남학생</span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus2_2" class="css_radio btn_step02" value="M" />
                                    <label for="cus2_2" class="css_label">선택</label>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <!--// -->

                    <!-- step3 -->
                    <div class="wrap stepImg3 step03" style="display: none">
                        <ul class="clear2">
                            <li>
                                <span class="img img3_1">사방이 탁 트인<br />
                                    높은 산으로 올라갈래요</span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus3_1" class="css_radio btn_step03" value="1" />
                                    <label for="cus3_1" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img3_2">야생동물로 가득한<br />
                                    국립공원에 놀러갈래요</span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus3_2" class="css_radio btn_step03" value="2" />
                                    <label for="cus3_2" class="css_label">선택</label>
                                </span>
                            </li>
                        </ul>
                        <ul class="clear2">
                            <li>
                                <span class="img img3_3">금강산도 식후경!<br />
                                    맛집 투어를 시작할래요</span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus3_3" class="css_radio btn_step03" value="3" />
                                    <label for="cus3_3" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img3_4">에메랄드빛<br />
                                    바닷가를 걸어볼래요</span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus3_4" class="css_radio btn_step03" value="4" />
                                    <label for="cus3_4" class="css_label">선택</label>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <!--// -->

                    <!-- step4_1 -->
                    <div class="wrap stepImg4_1 step04_1" style="display: none">
                        <ul class="clear2">
                            <li>
                                <span class="img img4_1_1">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_1_1" class="css_radio btn_step04" value="BO" />
                                    <label for="cus4_1_1" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_1_2">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_1_2" class="css_radio btn_step04" value="BR" />
                                    <label for="cus4_1_2" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_1_3">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_1_3" class="css_radio btn_step04" value="EC" />
                                    <label for="cus4_1_3" class="css_label">선택</label>
                                </span>
                            </li>
                        </ul>
                        <ul class="clear2">
                            <li>
                                <span class="img img4_1_4">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_1_4" class="css_radio btn_step04" value="PE" />
                                    <label for="cus4_1_4" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_1_5">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_1_5" class="css_radio btn_step04" value="CO" />
                                    <label for="cus4_1_5" class="css_label">선택</label>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <!--// -->

                    <!-- step4_2 -->
                    <div class="wrap stepImg4_2 step04_2" style="display: none">
                        <ul class="clear2">
                            <li>
                                <span class="img img4_2_1">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_2_1" class="css_radio btn_step04" value="TG" />
                                    <label for="cus4_2_1" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_2_2">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_2_2" class="css_radio btn_step04" value="TZ" />
                                    <label for="cus4_2_2" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_2_3">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_2_3" class="css_radio btn_step04" value="GH" />
                                    <label for="cus4_2_3" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_2_4">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_2_4" class="css_radio btn_step04" value="BF" />
                                    <label for="cus4_2_4" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_2_5">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_2_5" class="css_radio btn_step04" value="RW" />
                                    <label for="cus4_2_5" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_2_6">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_2_6" class="css_radio btn_step04" value="KE" />
                                    <label for="cus4_2_6" class="css_label">선택</label>
                                </span>
                            </li>
                        </ul>
                        <ul class="clear2">
                            <li>
                                <span class="img img4_2_7">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_2_7" class="css_radio btn_step04" value="UG" />
                                    <label for="cus4_2_7" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_2_8">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_2_8" class="css_radio btn_step04" value="ET" />
                                    <label for="cus4_2_8" class="css_label">선택</label>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <!--// -->

                    <!-- step4_3 -->
                    <div class="wrap stepImg4_3 step04_3" style="display: none">
                        <ul class="clear2">
                            <li>
                                <span class="img img4_3_1">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_3_1" class="css_radio btn_step04" value="BD" />
                                    <label for="cus4_3_1" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_3_2">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_3_2" class="css_radio btn_step04" value="PH" />
                                    <label for="cus4_3_2" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_3_3">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_3_3" class="css_radio btn_step04" value="TH" />
                                    <label for="cus4_3_3" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_3_4">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_3_4" class="css_radio btn_step04" value="IO" />
                                    <label for="cus4_3_4" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_3_5">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_3_5" class="css_radio btn_step04" value="LK" />
                                    <label for="cus4_3_5" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_3_6">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_3_6" class="css_radio btn_step04" value="IN" />
                                    <label for="cus4_3_6" class="css_label">선택</label>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <!--// -->

                    <!-- step4_4 -->
                    <div class="wrap stepImg4_4 step04_4" style="display: none">
                        <ul class="clear2">
                            <li>
                                <span class="img img4_4_1">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_4_1" class="css_radio btn_step04" value="HO" />
                                    <label for="cus4_4_1" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_4_2">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_4_2" class="css_radio btn_step04" value="ES" />
                                    <label for="cus4_4_2" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_4_3">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_4_3" class="css_radio btn_step04" value="DR" />
                                    <label for="cus4_4_3" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_4_4">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_4_4" class="css_radio btn_step04" value="GU" />
                                    <label for="cus4_4_4" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_4_5">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_4_5" class="css_radio btn_step04" value="HA" />
                                    <label for="cus4_4_5" class="css_label">선택</label>
                                </span>
                            </li>
                            <li>
                                <span class="img img4_4_6">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_4_6" class="css_radio btn_step04" value="ME" />
                                    <label for="cus4_4_6" class="css_label">선택</label>
                                </span>
                            </li>
                        </ul>
                        <ul class="clear2">
                            <li>
                                <span class="img img4_4_7">
                                    <br />
                                </span>
                                <span class="radio_ui yellow3">
                                    <input type="radio" name="temp" id="cus4_4_7" class="css_radio btn_step04" value="NI" />
                                    <label for="cus4_4_7" class="css_label">선택</label>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <!--// -->

                </div>


            </div>
        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>


</asp:Content>


