﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="info.aspx.cs" Inherits="sponsor_info" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/sponsor/item-child.ascx" TagPrefix="uc" TagName="child" %>
<%@ Register Src="/sponsor/item-user-funding.ascx" TagPrefix="uc" TagName="uf" %>
<%@ Register Src="/sponsor/item-special.ascx" TagPrefix="uc" TagName="sf" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/info.js"></script>
    <!--NSmart Track Tag Script-->
        <script type='text/javascript'>
            callbackFn = function() {};
            var _nsmart = _nsmart || [];
            _nsmart.host = _nsmart.host || (('https:' == document.location.protocol) ? 'https://' : 'http://');
//document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            // document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            /* v 1.4 ---------------------------------*
            *  Nasmedia Track Object, version 1.4
            *  (c) 2009 nasmedia,mizcom (2009-12-24)
            *---------------------------------------*/
            var nasmedia = {};
            var NTrackObj = null;
            nasmedia.track = function(){
                this.camp, this.img = null;
            };
            nasmedia.track.prototype = {
                callTrackTag : function(p, f, c){
                    if(Object.prototype.toString.call(_nsmart) != '[object Array]' || _nsmart.host == undefined || _nsmart.host == '') return false;
                    if(f != undefined && typeof(f) == 'function') {
                        this.img.onload = this.img.onerror = this.img.onabort = f;
                        var no = ((parseInt(c, 10) % 100) < 10 ? '0' + (parseInt(c, 10) % 100): (parseInt(c, 10) % 100));
                        this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + c + '&page=' + p + '&r=' + Math.random();
                    }
                    else this.callNext(0);
                    return false;
                },
                callNext : function(i) {
                    if(i == this.camp.length) return false;
                    this.img.onload = this.img.onerror = this.img.onabort = function(o, i){return function(){o.callNext(i);};}(this, i+1);
                    var no = ((parseInt(this.camp[i][0], 10) % 100) < 10 ? '0' + (parseInt(this.camp[i][0], 10) % 100): (parseInt(this.camp[i][0], 10) % 100));
                    this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + this.camp[i][0] + '&page=' + this.camp[i][1] + '&r=' + Math.random();
                },
                init : function(c) {
                    this.camp = c, this.img = new Image;
                }
            };
            (function(){
                NTrackObj = new nasmedia.track();
            })();
            NTrackObj.init(_nsmart);
        NTrackObj.callTrackTag();
        </script>
        <!--NSmart Track Tag Script End..-->
</asp:Content>


<asp:Content runat="server" ID="head_middle" ContentPlaceHolderID="head_middle">
	<!-- 최근 확인한 어린이 -->
	<asp:PlaceHolder runat="server" ID="ph_child" Visible="false" >
	<div class="recent_child_wrap" style="display:block">
		<div class="recent_child">
			<a href="#" id="btn_goCDSP" data-childmasterid="<%:this.ViewState["childmasterid"].ToString()%>"><span class="img" style="background:url('<%:this.ViewState["pic"].ToString()%>') no-repeat center;background-size:cover;background-size:104px;background-position-y:0"></span></a>

			<span class="tooltip" style="display:block;z-index:10">
				<span class="tit">최근 확인하신 어린이 정보가 있습니다.</span>
				<span class="con">
					해당 어린이 확인을 원하시면 이미지를 클릭해 주세요.<br />
					클릭 시 후원 결제 페이지로 이동합니다.
				</span>
				<button class="close" onclick="$('.tooltip').hide();return false;">닫기</button>
				<span class="arr"></span>
			</span>
		</div>
	</div>
</asp:PlaceHolder>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<section class="sub_body" ng-app="cps">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>후원/결제</em></h1>
				<span class="desc">후원자님 사랑에 감사드립니다</span>

				 <uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents sponsor">

		

			<div class="payment" runat="server" id="detail_view">
				
				<div class="w980">

					<!-- 후원정보 -->

					<uc:child runat="server" ID="view_child" Visible="false" />

					<uc:uf runat="server" ID="view_uf" Visible="false" />

					<uc:sf runat="server" ID="view_sf" Visible="false" />

					<!--// 후원정보 -->

					<!-- 로그인박스 -->
					<div class="loginBox" >
						<p class="txt">컴패션 회원이시면 로그인을 하신 뒤 후원을 진행해주세요.<br />정기후원의 경우 회원가입 후 후원이 가능합니다.</p>
						<div class="btnGroup1">
							<a href="#" id="btn_member" class="btn_member btn_tab">회원후원</a>
							<asp:PlaceHolder runat="server" ID="ph_frequency_temporary" Visible="false">
							<a href="#" id="btn_nonmember" class="btn_nonmember btn_tab">비회원후원</a>
							<a href="#" id="btn_noname" class="btn_tab btn_nonmember ml5">무기명후원</a>
							</asp:PlaceHolder>

							<asp:PlaceHolder runat="server" ID="ph_frequency_regular" Visible="false">
							<a href="<%:this.ViewState["joinUrl"].ToString() %>" class="btn_nonmember btn_tab" onclick="javascript:NTrackObj.callTrackTag('32037', callbackFn, 12490);">가입 후 바로후원</a>
							</asp:PlaceHolder>
							
						</div>
						
						<div class="pn_login pn_add_form" >
							<div class="input_area clear2">
								<div class="input">
									<input type="text" id="user_id" class="input_type4" placeholder="아이디 입력" style="margin-bottom:3px">
									<input type="password"  id="user_pwd" class="input_type4" placeholder="비밀번호 입력">
								</div>
								<div class="btn"><a href="#" id="btn_login" class="login" onclick="javascript:NTrackObj.callTrackTag('32038', callbackFn, 12490);">로그인</a></div>
							</div>
							<div class="btnGroup2">
								<a href="<%:this.ViewState["joinUrl"].ToString() %>" class="btn_join" onclick="javascript:NTrackObj.callTrackTag('32037', callbackFn, 12490);">회원가입</a>
								<span class="bar"></span>
								<a href="<%:this.ViewState["findIdUrl"].ToString() %>" class="btn_id">아이디찾기</a>
								<span class="bar"></span>
								<a href="<%:this.ViewState["findPwUrl"].ToString() %>" class="btn_pw">비밀번호찾기</a>
							</div>
						</div>

					</div>
					<!--// 로그인박스 -->

					<!-- 비회원일 경우 보여지는 문구 -->
					<div class="memberType_info tac pn_add_form pn_nonmember"  style="display:none">
						<p>회원가입을 하시면 결제정보나 국세청 영수증 조회 등을 이용하실 수 있고,<br />후원에 관한 다양한 정보도 받아보실 수 있습니다. 지금 바로 간편가입 해 보세요.</p>

						<a href="/sponsor/pay/temporary/" class="btn_type1 mr10"  id="btn_sponsor_nonmember" runat="server">비회원으로 후원 계속</a>
						<a href="<%:this.ViewState["joinUrl"].ToString() %>" class="btn_type10" >회원가입 후 후원</a>
					</div>
					<!--// -->

					<!-- 무기명일 경우 보여지는 문구 -->
					<div class="memberType_info tac pn_add_form pn_noname" style="display:none">
						<p>무기명 후원 시 후원자 정보가 없어<br /><em class="fc_blue">추후 소득공제 및 후원관련 확인 및 증빙서류</em>를 받으실 수 없습니다.</p>

						<a href="/sponsor/pay/temporary/?noname=Y" class="btn_type1" id="btn_sponsor_noname" runat="server" >다음</a>
					</div>
					<!--// -->

				</div>
				
			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>

</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<form id="exfrm" method="post" action="<%:this.ViewState["LoginPage"].ToString() %>">
		<input type="hidden" id="userId" name="userId" />
		<input type="hidden" id="userPwd" name="userPwd" />
		<input type="hidden" id="r" name="r" value="<%:Request.UrlEx() %>"/>
        <%-- [이종진] 추가 --%>
        <input type="hidden" id="sessionId" name="sessionId" value="<%:this.ViewState["sessionId"] %>"/>
	</form>
</asp:Content>
