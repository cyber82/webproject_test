﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;

public partial class sponsortest : FrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}
	
	protected override void OnBeforePostBack() {
		// 20160212164406722
		
		// 양육되지 않은 아이

		WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
		Object[] objSql = new object[1] { "select top 1 * from tChildMaster where childmasterid not in (select childmasterid from tCommitmentMaster)" };
		DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);


		if(ds.Tables[0].Rows.Count > 0) {
			var childMasterId = ds.Tables[0].Rows[0]["childMasterId"].ToString().Trim();
			cdsp_child.Text = childMasterId;
		}

		using(FrontDataContext dao = new FrontDataContext()) {
			foreach(var item in dao.sp_tSpecialFunding_list_f("" , "" , "","", "")) {
				civ_campaignId.Items.Add(new ListItem(item.CampaignName , item.CampaignID));
				civ_campaignId2.Items.Add(new ListItem(item.CampaignName, item.CampaignID));
				
			}
		}
	}

	// CIV 일시
	protected void btn_submit_Click( object sender, EventArgs e ) {

		var codeName = "";
		using(FrontDataContext dao = new FrontDataContext()) {

			var list = dao.sp_tSpecialFunding_get_f(civ_campaignId.SelectedValue).ToList();

			if(list.Count < 1) {
				return;
			}

			var entity = list[0];
			
			codeName = entity.CampaignName;
			
			PayItemSession.SetCookie(this.Context, new PayItemSession.Entity() { type = PayItemSession.Entity.enumType.SPECIAL_FUNDING, relation_key = entity.sf_id , campaignId = civ_campaignId.SelectedValue, frequency = "일시", group = entity.AccountClassGroup, codeId = entity.AccountClass, codeName = codeName, amount = 10000 });
			Response.Redirect("/sponsor/pay/temporary/");
		}


	}

	// CDSP
	protected void btn_cdsp_Click( object sender, EventArgs e ) {

		//PayItemSession.SetCookie(this.Context, new PayItemSession.Entity() { campaignId = "90000000000000000", frequency = "정기", group = "CDSP" , codeId = "DS", amount = 45000 , childMasterId= cdsp_child.Text });
		PayItemSession.SetCookie(this.Context, new PayItemSession.Entity() {  frequency = "정기", group = "CDSP", codeId = "DS" , amount = 45000, childMasterId = cdsp_child.Text });
		Response.Redirect("/sponsor/pay/regular/");

	}

	// CIV 정기
	protected void LinkButton1_Click( object sender, EventArgs e ) {

		var codeName = "";
		using(FrontDataContext dao = new FrontDataContext()) {
			var list = dao.sp_tSpecialFunding_get_f(civ_campaignId2.SelectedValue).ToList();

			if(list.Count < 1) {
				return;
			}

			var entity = list[0];

			codeName = entity.CampaignName;

			PayItemSession.SetCookie(this.Context, new PayItemSession.Entity() { type = PayItemSession.Entity.enumType.SPECIAL_FUNDING, relation_key = entity.sf_id , campaignId = civ_campaignId2.SelectedValue, frequency = "정기", group = "CIV", codeId = "PBCIV", codeName = codeName, amount = Convert.ToInt32(civ_amount.Text) });
			Response.Redirect("/sponsor/pay/regular/");

		}

		
	}
}