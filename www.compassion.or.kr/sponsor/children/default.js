﻿$(function () {
    //브라우저 체크 
    function browerCheck(type) {

        // 재활용을 위해 "hackerc_Agent" 에 정보를 저장.
        var hackerc_Agent = navigator.userAgent;
        var result = hackerc_Agent.match('LG | SAMSUNG | Samsung | iPhone | iPod | Android | Windows CE | BlackBerry | Symbian | Windows Phone | webOS | Opera Mini | Opera Mobi | POLARIS | IEMobile | lgtelecom | nokia | SonyEricsson');

        var pc = 'http://www.';
        var mobile = 'http://m.';

        if (type == 'pc') {
            if (result == null) {
                link = location.href.replace(mobile, pc)
                location.href = link;
                return false;
            }
        } else if (type == 'mobile') {
            if (result != null) {
                //link = location.href.replace(pc, mobile)
                //location.href = link;


                (function () {

                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'http://m.compassion.or.kr/';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'http://m.compassionkr.com/';
                    else if (loc.indexOf('localhost') != -1) host = 'http://localhost:35563/';


                    var http_us = host;// "http://m.compassion.or.kr/"; //모바일이동URL
                    var http_param = [];
                    var str_param = "";
                    if (document.location.search != "") {
                        http_param.push(document.location.search.replace(/^\?/, ""));
                    }
                    if (document.referrer != "" && !/OV_REFFER/.test(document.location.search)) {
                        http_param.push("OV_REFFER=" + document.referrer);
                    }
                    if (http_param.length > 0) {
                        str_param = (/\?/.test(http_us) ? "&" : "?") + http_param.join("&");
                    } else {
                        str_param = "";
                    }
                    location.href = http_us + str_param;
                })();

                return false;
            }
        }

    }


    //모바일로 접속시 모바일 홈페이지로 이동 
    if (getParameterByName("pc") != "ok") {
        if (cookie.get("locationMobile") != 1) {
            browerCheck('mobile');
        }
    } else {
        cookie.set("locationMobile", 1, 1);
    }

    $("#btn_goCDSP").click(function () {

        var childmasterid = $(this).data("childmasterid");
        var childkey = $(this).data("childkey");

        $.get("/sponsor/pay-gateway.ashx?t=go-cdsp-new", { childMasterId: childmasterid, childkey: childkey }).success(function (r) {

            if (r.success) {
                location.href = r.data;
            } else {

                if (r.action == "nonpayment") {		// 미납금

                    if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
                        location.href = "/my/sponsor/pay-delay/";
                    }

                } else {
                    alert(r.message);
                }
            }
        });

    })



});


(function () {
    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService) {

        $scope.list = [];
        //$scope.detail = [];
        $scope.nomore = false;
        $scope.nodata = false;
        //[이종진] 조회방법
        $scope.dbgp_kind = $("#dbgp_kind").val();
        $scope.list30 = []; //GP조회의 경우, 30개를 미리 넣어둠

        $scope.params = {
            //orderby: paramService.getParameter("orderby") ? paramService.getParameter("orderby") : "new",
            orderby: paramService.getParameter("orderby") ? paramService.getParameter("orderby") : "waiting",
            page: 1,
            rowsPerPage: 6,
            country: paramService.getParameter("country") ? paramService.getParameter("country") : "",
            //minAge: "3",
            minAge: paramService.getParameter("minAge") ? paramService.getParameter("minAge") : "1",
            //maxAge: "20",
            maxAge: paramService.getParameter("maxAge") ? paramService.getParameter("maxAge") : "20",
            //gender: "",		// 성별 Y,N
            gender: paramService.getParameter("gender") ? paramService.getParameter("gender") : "",
            //today: "",		// 오늘이 생일인경우 Y,N
            today: paramService.getParameter("today") ? paramService.getParameter("today") : "",
            //mm: "",			// 특별한 기념일 월 mm
            mm: paramService.getParameter("mm") ? paramService.getParameter("mm") : "",
            //dd: "",			// 특별한 기념일 일 dd
            dd: paramService.getParameter("dd") ? paramService.getParameter("dd") : "",
            orphan: paramService.getParameter("orphan") ? paramService.getParameter("orphan") : "",		// 고아 Y,N
            specialNeed: paramService.getParameter("specialNeed") ? paramService.getParameter("specialNeed") : ""	// 장애 Y,N
        };

        if (paramService.getParameter("specialNeed") == "Y")
            $("#s_specialNeed").prop("checked", true);

        if (paramService.getParameter("orphan") == "Y")
            $("#s_orphan").prop("checked", true);

        if (paramService.getParameter("orderby") == "waiting")
            $("#s_waiting").prop("checked", true);

        if (paramService.getParameter("today") == "Y")
            $("#s_today").prop("checked", true);

        if (paramService.getParameter("country") != "") {

            country = {
                "GH": "가나",
                "BF": "부르키나파소",
                "TG": "토고",
                "UG": "우간다",
                "RW": "르완다",
                "LK": "스리랑카",
                "ET": "에티오피아",
                "KE": "케냐",
                "TZ": "탄자니아",
                "IN": "인도",
                "BD": "방글라데시",
                "TH": "태국",
                "PH": "필리핀",
                "IO": "인도네시아",
                "ME": "멕시코",
                "GU": "과테말라",
                "ES": "엘살바도르",
                "NI": "니카리과",
                "CO": "콜롬비아",
                "EC": "에콰도르",
                "PE": "페루",
                "BO": "볼리비아",
                "HA": "아이티",
                "DR": "도미니카공화국",
                "HO": "온두라스",
                "BR": "브라질"
            }

            if (typeof paramService.getParameter("country") != "undefined") {
                param = paramService.getParameter("country").toUpperCase();
                ko = country[param];
                $("#s_country").selectbox("change", param, ko);
            }


        }

        if (paramService.getParameter("minAge")) {
            $("#s_minAge").selectbox("change", paramService.getParameter("minAge"), paramService.getParameter("minAge"));
        }

        if (paramService.getParameter("maxAge")) {
            $("#s_maxAge").selectbox("change", paramService.getParameter("maxAge"), paramService.getParameter("maxAge"));
        }

        if (paramService.getParameter("mm")) {
            $("#s_mm").selectbox("change", paramService.getParameter("mm"), paramService.getParameter("mm"));
        }

        if (paramService.getParameter("dd")) {
            $("#s_dd").selectbox("change", paramService.getParameter("dd"), paramService.getParameter("dd"));
        }

        // list
        $scope.getList = function (params) {

            loading.show();
            switch ($scope.dbgp_kind) {
                case "1":
                    $scope.getListDB();
                    break;
                case "2":
                    $scope.getListGP();
                    break;
                default:
                    $scope.getListDB();
                    break;
            }
            loading.hide();
        }

        $scope.getListDB = function (params) {
            $scope.params = $.extend($scope.params, params);
            console.log($scope.params);

            $http.get("/api/tcpt.ashx?t=list", { params: $scope.params }).success(function (r) {
                console.log(r);
                if (r.success) {

                    var data = r.data;

                    $scope.nomore = data.length / $scope.params.rowsPerPage < 1

                    var row = [];
                    $.each(data, function (i) {
                        this.birthdate = new Date(this.birthdate);
                        this.row = $scope.list.length;
                        if (i % 3 == 0) {
                            row = [];
                        }
                        row.push(this);

                        if (i % 3 == 2 || i + 1 == data.length) {
                            $scope.list.push($.extend([], row));
                            //$scope.detail.push("");
                        }


                    });
                    console.log($scope.list);

                    // SNS 애니메이션
                    setTimeout(function () {
                        common.bindSNSAction();
                    }, 500);


                    if ($scope.params.page == 1) {
                        $scope.nodata = r.data.length < 1;
                    }

                    if (params)
                        scrollTo($("#l"), 30);

                    loading.hide();

                } else {

                    //alert(r.message);
                }
            });

        }

        //[이종진] 2018-01-31 GlobalPool 조회의 경우, 조회속도가 최소 3초이상이므로 한번에 무조건 30개를 가져옴. 
        // '더보기'(아래화살표. showmore) 클릭 시, 6개씩 5번을 더 뿌려주고, 더보기 추가 클릭 시, 더 조회되지는 않음. (고객(장성권과장)과 협의한 사항임)
        $scope.getListGP = function (params) {
            $scope.params = $.extend($scope.params, params);
            console.log($scope.params);

            //파라미터중, 페이지관련 재설정.
            $scope.params.rowsPerPage = 30; //한번에 30개를 가져옴.

            $http.get("/api/tcpt.ashx?t=list", { params: $scope.params }).success(function (r) {
                console.log(r);
                if (r.success) {

                    $scope.list30 = r.data; //30개를 미리 넣어둠
                    var data = r.data;

                    $scope.nomore = data.length / 6 <= 1; //6개보다 같거나 적으면, 더 보여줄 데이터가 없으므로, showmore 버튼 숨김
                    
                    var row = [];
                    $.each(data, function (i) {
                        if (i == 6) { //6개만 바인딩함
                            return false; //each 문은 return false 가 break 역할을 함
                        }
                        this.birthdate = new Date(this.birthdate);
                        this.row = $scope.list.length;
                        if (i % 3 == 0) {
                            row = [];
                        }
                        row.push(this);

                        if (i % 3 == 2 || i + 1 == data.length) {
                            $scope.list.push($.extend([], row));
                            //$scope.detail.push("");
                        }


                    });
                    console.log($scope.list);

                    // SNS 애니메이션
                    setTimeout(function () {
                        common.bindSNSAction();
                    }, 500);


                    if ($scope.params.page == 1) {
                        $scope.nodata = r.data.length < 1;
                    }

                    if (params)
                        scrollTo($("#l"), 30);

                    loading.hide();

                } else {

                    //alert(r.message);
                }
            });

        }

        $scope.search = function ($event) {
            $event.preventDefault();

            if (parseInt($("#s_minAge").val()) > parseInt($("#s_maxAge").val())) {
                alert("나이 설정을 다시 해주세요.");
                return false;
            }

            loading.show();
            $scope.params.page = 1;
            $scope.list = [];
            $scope.getList();
        }

        $scope.showMore = function ($event) {

            //DB조회
            if ($scope.dbgp_kind == "1") {
                $event.preventDefault();
                $scope.params.page++;
                $scope.getList();
            }
            else {
                $event.preventDefault();
                $scope.params.page = $scope.params.page + 1;
                //$scope.getList();
                //페이지카운트만큼의 데이터를 data에 넣어준다.
                var start =  ($scope.params.page - 1) * 6;
                var end =  ($scope.params.page * 6) - 1;
                var data = $scope.list30;
               
                $.each(data, function (i) {
                    if (i < start) { //start보다 적으면 continue해서 다음 실행. * each문은 return true가 continue
                        return true;
                    }
                    if (i > end) {
                        return false; //end보다 크면 break. * each 문은 return false 가 break
                    }
                    this.birthdate = new Date(this.birthdate);
                    this.row = $scope.list.length;
                    if (i % 3 == 0) {
                        row = [];
                    }
                    row.push(this);

                    if (i % 3 == 2 || i + 1 == data.length) {
                        $scope.list.push($.extend([], row));
                        //$scope.detail.push("");
                    }
                });
                //개수에 따라서, showmore 숨김. end는 5, 11, 17, ..이기 때문에 , +1해줌
                $scope.nomore = data.length <= end+1;

            }
            
        }

        $scope.checkOption = function ($event, id) {

            $scope.params.page = 1;

            var checked = $("#" + id).prop("checked");

            if (id == "s_orphan") {
                $scope.params.orphan = checked ? "Y" : "";
            } else if (id == "s_specialNeed") {
                $scope.params.specialNeed = checked ? "Y" : "";
            } else if (id == "s_waiting") {
                $scope.params.orderby = checked ? "waiting" : "new";
            } else if (id == "s_today") {
                $scope.params.today = checked ? "Y" : "";

                if (checked) {

                    $scope.params.mm = "";
                    $scope.params.dd = "";

                    var mm_sb = $("#s_mm").attr("sb");
                    $("#sbSelector_" + mm_sb).text($($("#s_mm option")[0]).text());

                    var dd_sb = $("#s_dd").attr("sb");
                    $("#sbSelector_" + dd_sb).text($($("#s_dd option")[0]).text());

                }

            }


        }

        $scope.changeDate = function () {
            $scope.params.today = "";
            $("#s_today").prop("checked", false);
            if ($("#s_mm").val() == 2) {
                $('a[href="#30"]').hide();
                $('a[href="#31"]').hide();
            } else {
                $('a[href="#30"]').show();
                $('a[href="#31"]').show();
            }

        }

        $scope.showChildPop = function ($event, item) {
            loading.show();
            if ($event) $event.preventDefault();
            popup.init($scope, "/common/child/pop/" + item.countrycode + "/" + item.childmasterid + "/" + item.childkey, function (modal) {

                modal.show();
                //console.log('modalChildItem', item)
                initChildPop($http, $scope, modal, item);

            }, { top: 0, iscroll: true, removeWhenClose: true });
        }

        $scope.getList();

        var childMasterId = paramService.getParameter("c");
        var ChildKey = paramService.getParameter("ck");

        if (childMasterId) {

            $http.get("/api/tcpt.ashx?t=get", { params: { childMasterId: childMasterId } }).success(function (r) {
                console.log(r);
                if (r.success) {

                    var data = r.data;
                    data.birthdate = new Date(data.birthdate);

                    $scope.showChildPop(null, data);


                } else {

                    alert(r.message);
                }
            });

        }

    });


})();
