﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class sponsor_children_default : FrontBasePage {

	public override bool RequireSSL {
		get {
			return false;
		}
	}
	
	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		this.ViewState["pic"] = "";
		this.ViewState["childmasterid"] = "";

		for(int i = 3; i < 21; i++) {
			s_minAge.Items.Add(new ListItem(i.ToString(), i.ToString()));
			s_maxAge.Items.Add(new ListItem(i.ToString(), i.ToString()));
		}

		s_minAge.SelectedValue = "3";
		s_maxAge.SelectedValue = "20";

		for(int i = 1; i < 13; i++) {
			s_mm.Items.Add(new ListItem(i.ToString(), i.ToString()));
		}

		for(int i = 1; i < 32; i++) {
			s_dd.Items.Add(new ListItem(i.ToString(), i.ToString()));
		}

		// 국가정보
		var actionResult = new CountryAction().GetList();
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message , "goBack()");
			return;
		}

		var countries = (List<sp_country_list_fResult>)actionResult.data;
		foreach(var item in countries) {
			s_country.Items.Add(new ListItem(item.c_name, item.c_id));
		}

		// 최근 본 회원
		var childAction = new ChildAction();

        actionResult = childAction.GetEnsures();

        if (actionResult.success) {

			var dt = (DataTable)actionResult.data;
			
			if(dt.Rows.Count > 0) {

                dt.DefaultView.Sort = "RegisterDate desc";

                DataRowView dr = dt.DefaultView[0];
				var childKey = dr["ChildId"].ToString();
                //this.ViewState["pic"] = ChildAction.GetPicByChildKey(childKey);
                this.ViewState["pic"] = new ChildAction().GetChildImage(dr["childMasterId"].ToString(), dr["ChildId"].ToString());
                this.ViewState["childmasterid"] = dr["childmasterid"].ToString();
				ph_child.Visible = true;
			}

		}
	}

	protected override void loadComplete( object sender, EventArgs e ) {

		var childMasterId = Request["c"].EmptyIfNull();

        // 기존 방식 - DB 조회
        if (string.IsNullOrEmpty(childMasterId))
        {

        }
        else
        {
            var actionResult = new ChildAction().GetChild(childMasterId);
            if (actionResult.success)
            {
                var item = (ChildAction.ChildItem)actionResult.data;
                // SNS
                this.ViewState["meta_url"] = Request.UrlEx();
                this.ViewState["meta_title"] = "1:1어린이 양육-" + item.NameKr;

                // optional(설정 안할 경우 주석)
                //this.ViewState["meta_description"] = "";
                this.ViewState["meta_keyword"] = this.ViewState["meta_keyword"].ToString() + ",1:1어린이양육," + item.NameKr + "," + item.NameEn;
                // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,FrontBasePage)
                this.ViewState["meta_image"] = item.Pic;
            }
        }
	}


}