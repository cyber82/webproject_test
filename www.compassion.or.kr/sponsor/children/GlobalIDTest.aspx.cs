﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TCPTModel;
using TCPTModel.Response.Supporter;

public partial class sponsor_children_GlobalIDTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string sponsorid = Request.QueryString.Get("sponsorid");
        string childMasterID = Request.QueryString.Get("childmasterid");

        TCPTService.Service svc = new TCPTService.Service();
        DataSet ds = svc.GetCommitmentInfoForTCPT(sponsorid, childMasterID);

        if (ds == null || ds.Tables.Count == 0)
        {
            // 조회 오류
        }
        else
        {
            DataRow dr = ds.Tables[0].Rows[0];
            bool regSuccess = false;
            string result = string.Empty;
            string userid = "";
            string userName = "";
            string GlobalSponsorID = string.Empty;
            string ChildGlobalID = dr["ChildGlobalID"].ToString();
            string CommitmentID = dr["CommitmentID"].ToString();
            string holdID = dr["HoldID"].ToString();
            string holdUID = dr["HoldUID"].ToString();
            string holdType = dr["HoldType"].ToString();
            string conid = "54-" + dr["ConID"].ToString();
            string firstName = dr["FirstName"].ToString();
            string lastName = dr["LastName"].ToString();
            string genderCode = dr["GenderCode"].ToString();
            if (dr["SponsorGlobalID"].ToString() != "")
            {
                GlobalSponsorID = dr["SponsorGlobalID"].ToString();
                regSuccess = true;
            }

            if (genderCode == "M")
                genderCode = "Male";
            else if (genderCode == "F")
                genderCode = "Female";

            if (string.IsNullOrEmpty(GlobalSponsorID))
            {
                result = svc.RegisterSponsorProfile(conid, firstName, lastName, genderCode, userid);

                TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(result);
                if (msg.IsSuccessStatusCode)
                {
                    SupporterProfileResponse_Kit response = JsonConvert.DeserializeObject<SupporterProfileResponse_Kit>(msg.RequestMessage.ToString());
                    GlobalSponsorID = response.SupporterProfileResponse[0].GlobalID;

                    // 스폰서  Global id update
                    svc.UpdateSponsorGlobalID(sponsorid, GlobalSponsorID);
                    regSuccess = true;
                }
            }

            if (regSuccess)
            {
                // 결연 킷 발송을 위해 템프 디비에 저장
                svc.InsertTCPT_CommitmentTemp(CommitmentID, sponsorid, childMasterID, holdUID.ToString(), holdID, userid, userName);
            }

        }
    }
}