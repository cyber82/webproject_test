﻿$(function () {

	$("#btn_goCDSP").click(function () {

		var childmasterid = $(this).data("childmasterid");

		$.get("/sponsor/pay-gateway.ashx?t=go-cdsp", { childMasterId: childmasterid }).success(function (r) {

			if (r.success) {
				location.href = r.data;
			} else {
				
				if (r.action == "nonpayment") {		// 미납금

					if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
						location.href = "/my/sponsor/pay-delay/";
					}

				} else {
					alert(r.message);
				}
			}
		});

	})



});


(function () {
	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup , paramService ) {
		
		$scope.list = [];
		//$scope.detail = [];
		$scope.nomore = false;
		$scope.nodata = false;

		$scope.params = {
			orderby: paramService.getParameter("orderby") ? paramService.getParameter("orderby") : "new",
			page: 1,
			rowsPerPage: 6,
			country: paramService.getParameter("country") ? paramService.getParameter("country") : "EC,HA",
			minAge: "3",
			maxAge: "20",
			gender: "",		// 성별 Y,N
			today: "",		// 오늘이 생일인경우 Y,N
			mm: "",			// 특별한 기념일 월 mm
			dd: "",			// 특별한 기념일 일 dd
			orphan: paramService.getParameter("orphan") ? paramService.getParameter("orphan") : "",		// 고아 Y,N
			specialNeed: paramService.getParameter("specialNeed") ? paramService.getParameter("specialNeed") : ""	// 장애 Y,N
		};
		
		if (paramService.getParameter("specialNeed") == "Y")
			$("#s_specialNeed").prop("checked", true);

		if (paramService.getParameter("orphan") == "Y")
			$("#s_orphan").prop("checked", true);

		if (paramService.getParameter("orderby") == "waiting")
		    $("#s_waiting").prop("checked", true);

		if (paramService.getParameter("country") != "") {

			country = {
				"GH": "가나",
				"BF": "부르키나파소",
				"TG": "토고",
				"UG": "우간다",
				"RW": "르완다",
				"LK": "스리랑카",
				"ET": "에티오피아",
				"KE": "케냐",
				"TZ": "탄자니아",
				"IN": "인도",
				"BD": "방글라데시",
				"TH": "태국",
				"PH": "필리핀",
				"IO": "인도네시아",
				"ME": "멕시코",
				"GU": "과테말라",
				"ES": "엘살바도르",
				"NI": "니카리과",
				"CO": "콜롬비아",
				"EC": "에콰도르",
				"PE": "페루",
				"BO": "볼리비아",
				"HA": "아이티",
				"DR": "도미니카공화국",
				"HO": "온두라스",
				"BR": "브라질"
			}
			
			if (typeof paramService.getParameter("country") != "undefined") {
			    param = paramService.getParameter("country").toUpperCase();
			    ko = country[param];
			    $("#s_country").selectbox("change", param, ko);
			}

			
		}



		// list
		$scope.getList = function (params) {

			$scope.params = $.extend($scope.params, params);
			console.log($scope.params);

			$http.get("/api/tcpt.ashx?t=list", { params: $scope.params }).success(function (r) {
				console.log(r);
				if (r.success) {

					var data = r.data;
					
					$scope.nomore = data.length / $scope.params.rowsPerPage < 1
					
					var row = [];
					$.each(data, function (i) {
						this.birthdate = new Date(this.birthdate);
						this.row = $scope.list.length;
						if (i % 3 == 0) {
							row = [];
						}
						row.push(this);

						if (i % 3 == 2 || i + 1 == data.length) {
							$scope.list.push($.extend([], row));
							//$scope.detail.push("");
						}


					});
					console.log($scope.list);

					// SNS 애니메이션
					setTimeout(function () {
						common.bindSNSAction();
					}, 500);
					

					if ($scope.params.page == 1) {
						$scope.nodata = r.data.length < 1;
					}

					if (params)
						scrollTo($("#l"), 30);

					loading.hide();
					
				} else {

					//alert(r.message);
				}
			});


		}

		$scope.search = function ($event) {
		    $event.preventDefault();
		    
		    if (parseInt($("#s_minAge").val()) > parseInt($("#s_maxAge").val())) {
		        alert("나이 설정을 다시 해주세요.");
		        return false;
		    }

		    loading.show();
		    $scope.params.page = 1;
		    $scope.list = [];
		    $scope.getList();
		}

		$scope.showMore = function ($event) {
			$event.preventDefault();
			$scope.params.page++;
			$scope.getList();
		}

		$scope.checkOption = function ($event, id) {
			
			$scope.params.page = 1;

			var checked = $("#" + id).prop("checked");

			if (id == "s_orphan") {
				$scope.params.orphan = checked ? "Y" : "";
			} else if (id == "s_specialNeed") {
				$scope.params.specialNeed = checked ? "Y" : "";
			} else if (id == "s_waiting") {
				$scope.params.orderby = checked ? "waiting" : "new";
			} else if (id == "s_today") {
				$scope.params.today = checked ? "Y" : "";

				if (checked) {

					$scope.params.mm = "";
					$scope.params.dd = "";

					var mm_sb = $("#s_mm").attr("sb");
					$("#sbSelector_" + mm_sb).text($($("#s_mm option")[0]).text());

					var dd_sb = $("#s_dd").attr("sb");
					$("#sbSelector_" + dd_sb).text($($("#s_dd option")[0]).text());

				}

			}

		
		}
		
		$scope.changeDate = function () {
			$scope.params.today = "";
			$("#s_today").prop("checked", false);
			if ($("#s_mm").val() == 2) {
			    $('a[href="#30"]').hide();
			    $('a[href="#31"]').hide();
			} else {
			    $('a[href="#30"]').show();
			    $('a[href="#31"]').show();
			}
			
		}

		$scope.showChildPop = function ($event, item) {
			loading.show();
			if ($event) $event.preventDefault();
			popup.init($scope, "/common/child/pop/" + item.countrycode + "/" + item.childmasterid + "/" + item.childkey , function (modal) {
			    
				modal.show();
				
				initChildPop($http, $scope, modal, item);

			}, { top: 0, iscroll: true, removeWhenClose: true });
		}
		
		$scope.getList();

		var childMasterId = paramService.getParameter("c");
		if (childMasterId) {
			
			$http.get("/api/tcpt.ashx?t=get", { params: { childMasterId: childMasterId } }).success(function (r) {
				console.log(r);
				if (r.success) {

					var data = r.data;
					data.birthdate = new Date(data.birthdate);
					
					$scope.showChildPop(null , data);
					

				} else {

					alert(r.message);
				}
			});

		}

	});


})();
