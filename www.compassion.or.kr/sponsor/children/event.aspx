﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="event.aspx.cs" Inherits="sponsor_children_default" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/common/js/site/moveToMobile.js"></script>
	<script type="text/javascript" src="/sponsor/children/event.js"></script>
</asp:Content>

<asp:Content runat="server" ID="head_middle" ContentPlaceHolderID="head_middle">
	<!-- 최근 확인한 어린이 -->
	<asp:PlaceHolder runat="server" ID="ph_child" Visible="false" >
	<div class="recent_child_wrap" style="display:block">
		<div class="recent_child">
			<a href="#" id="btn_goCDSP" data-childmasterid="<%:this.ViewState["childmasterid"].ToString()%>"><span class="img" style="background:url('<%:this.ViewState["pic"].ToString()%>') no-repeat center;background-size:cover;background-size:104px;background-position-y:0"></span></a>

			<span class="tooltip" style="display:block;z-index:10">
				<span class="tit">최근 확인하신 어린이 정보가 있습니다.</span>
				<span class="con">
					해당 어린이 확인을 원하시면 이미지를 클릭해 주세요.<br />
					클릭 시 후원 결제 페이지로 이동합니다.
				</span>
				<button class="close" onclick="$('.tooltip').hide();return false;">닫기</button>
				<span class="arr"></span>
			</span>
		</div>
	</div>
</asp:PlaceHolder>
	<!--// 최근 확인한 어린이 -->
</asp:Content>
<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>1:1<em>어린이양육</em></h1>
				<span class="desc">1:1어린이양육은 한 어린이의 인생에 희망을 심어주는 감동적인 만남입니다</span>

				 <uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents sponsor">

			<div class="mantoman">
			
				<!-- 비주얼이미지 -->
				<div class="bgContent visual">
					
					<div class="w980">
						<p class="tit">한 어린이의 삶이 바뀝니다.</p>
						<span class="bar"></span>
						<p class="con">
							한 어린이가 꿈을 찾는 양육비는 매월 4만 5천 원입니다.<br />
							후원금과 기도, 사랑의 편지로 어린이의 삶에 기적을 선물해주세요.
						</p>
						<a href="/sponsor/custom/" class="btn_b_type2 mt20">어떤 어린이를 후원해야 할지 모르겠어요</a>

					</div>
				</div>
				<!--// 비주얼이미지 -->

				<!-- 검색영역 -->
				<div class="search_child">
					<div class="w980">

						<!-- 국가, 나이, 성별 -->
						<div class="search1 clear2">
							<div class="nation">
								<p class="field">국가</p>
								<span class="sel_type2" style="width:100%;">
									<label for="s_country" class="hidden">국가선택</label>
									<asp:DropDownList runat="server" ID="s_country" class="custom_sel" ng-model="params.country" >
										<asp:ListItem Text="전체" Value=""></asp:ListItem>
									</asp:DropDownList>
								</span>
							</div>
							<div class="age">
								<p class="field pr10">나이</p>
								<span class="sel_type2" style="width:115px;">
									<label for="s_minAge" class="hidden">최저나이선택</label>
									<asp:DropDownList runat="server" ID="s_minAge" ng-model="params.minAge" class="custom_sel" >
									</asp:DropDownList>
								</span>&nbsp;세
								&nbsp;&nbsp;&nbsp;&nbsp;~&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="sel_type2" style="width:115px;">
									<label for="s_maxAge" class="hidden">최고나이선택</label>
									<asp:DropDownList runat="server" ID="s_maxAge"  ng-model="params.maxAge" class="custom_sel" >
									</asp:DropDownList>
									
								</span>&nbsp;세
							</div>

							<div class="sex">
								<p class="field">성별</p>
								<span class="radio_ui">
									<span class="block">
										<input type="radio" id="sex_all" name="s_gender" value="" class="css_radio"  ng-model="params.gender" checked />
										<label for="sex_all" class="css_label">전체</label>
									</span>
									<span class="block">
										<input type="radio" id="sex_1" name="s_gender" value="M" class="css_radio" ng-model="params.gender" />
										<label for="sex_1" class="css_label">남자</label>
									</span>
									<span class="block">
										<input type="radio" id="sex_2" name="s_gender" value="F" class="css_radio"  ng-model="params.gender" />
										<label for="sex_2" class="css_label">여자</label>
									</span>
								</span>
							</div>
						</div>
						<!--// -->

						<!-- 어린이선택 -->
						<div class="search2 clear2">
							<div class="check_area">
								<span class="checkbox_ui">
									<input type="checkbox" class="css_checkbox" id="s_waiting" ng-click="checkOption($event,'s_waiting')"/>
									<label for="s_waiting" class="css_label font2 mr40">오래 기다린 어린이부터 보기</label>

									<input type="checkbox" class="css_checkbox" id="s_today" ng-click="checkOption($event,'s_today')" />
									<label for="s_today" class="css_label font2">오늘 생일인 어린이 보기</label>
								</span>
							</div>
							<button class="detail"><span>상세검색 열기</span></button>
						</div>
						<!--// -->

						<script type="text/javascript">
							$(function () {
								$("button.detail").click(function () {
									$(".detailSearch").slideToggle("fast");
									if ($(this).hasClass("on")) {
										$(this).removeClass("on");
										$(this).find("span").text("상세검색 열기");
									}
									else {
										$(this).addClass("on");
										$(this).find("span").text("상세검색 닫기");
									}
								})
							})
						</script>

						<!-- 특별한 기념일 -->
						<div class="search3 clear2 detailSearch">
							<div class="special">
								<p class="field pr10">특별한 기념일</p>
								<span class="sel_type2" style="width:115px;">
									<label for="s_mm" class="hidden">월 선택</label>
									<asp:DropDownList runat="server" ID="s_mm" class="custom_sel" ng-change="changeDate()" ng-model="params.mm">
										<asp:ListItem Text="월" Value=""></asp:ListItem>
									</asp:DropDownList>
								</span>&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="sel_type2" style="width:115px;">
									<label for="s_dd" class="hidden">일 선택</label>
									<asp:DropDownList runat="server" ID="s_dd" class="custom_sel" ng-change="changeDate()" ng-model="params.dd">
										<asp:ListItem Text="일" Value=""></asp:ListItem>
									</asp:DropDownList>
								</span>&nbsp;
							</div>

							<div class="check_area">
								<p class="field">관심과 보호가 필요해요</p>
								<span class="checkbox_ui">
									<input type="checkbox" class="css_checkbox" id="s_specialNeed" ng-click="checkOption($event,'s_specialNeed')" />
									<label for="s_specialNeed" class="css_label font2 mr40">몸이 불편한 어린이</label>

									<input type="checkbox" class="css_checkbox" id="s_orphan" ng-click="checkOption($event,'s_orphan')" />
									<label for="s_orphan" class="css_label font2">부모님이 안 계신 어린이</label>
								</span>
							</div>
						</div>
						<!--// -->

						<div class="tac pt40">
							<a href="#" class="btn_type5" ng-click="search($event)">검색하기</a>
						</div>

					</div>
				</div>
				<!--// 검색영역 -->

				<!-- 어린이리스트 -->
				<div class="child_list" id="l">

					<!-- set -->
					<div ng-repeat="row in list">
						<div class="w980 childRow">
							<div class="wrap clear2" >

								<div class="card" ng-repeat="item in row">
									<div class="childBox">
										<div class="snsWrap">
											<span class="day">{{item.waitingdays}}일</span>
											<span class="sns_ani">
												<span class="common_sns_group">
													<span class="wrap">
														<a href="#" title="페이스북" data-role="sns" data-provider="fb" data-url="" class="sns_facebook" child-item="{{item}}">페이스북</a>
														<a href="#" title="카카오스토리" data-role="sns" data-provider="ks" data-url="" class="sns_story" child-item="{{item}}">카카오스토리</a>
														<a href="#" title="트위터" data-role="sns" data-provider="tw" data-url="" class="sns_twitter" child-item="{{item}}">트위터</a>
														<a href="#" title="url 공유" data-role="sns" data-provider="copy" data-url="" class="sns-copy sns_url" child-item="{{item}}">url 공유</a>
													</span>
												</span>
												<button class="common_sns_share">공유하기</button>
											</span>
										</div>

										<div class="child_info">
											<span class="pic" background-img="{{item.pic}}" data-default-image="/common/img/page/my/no_pic.png" style="background-repeat: no-repeat;background-position:center top; background-size: 150px;">양육어린이사진</span>
											<span class="name">{{item.name}}</span>
											<p class="info">
												<span>국가 : {{item.countryname}}</span><br />
												<span class="bar">생일 : {{item.birthdate | date:'yyyy.MM.dd'}} ({{item.age}}세)</span><span>성별 : {{item.gender}}</span>
											</p>
										</div>

										<div class="more"><a href="#" ng-click="showChildPop($event , item)">더 알아보기</a></div>
									</div>
								</div>

							</div>

							<!-- 어린이 세부정보 -->
							<div class="childDetail" ng-bind-html="detail[$index] | trusted"></div>
							<!--// 어린이 세부정보 -->

						</div>
						<!--// set -->


					</div>

					
						<div class="no_content" ng-show="nodata">선택하신 조건에 맞는 어린이가 없습니다.</div>
					

					<div class="w980" ng-show="!nomore">
						<button class="btn_com_more mb60" ng-click="showMore($event)"><span>더 보기</span></button>
					</div>
				</div>
				<!--// 어린이리스트 -->

				<!-- 함께하는 양육 -->
				<div class="bgContent together">

					<div class="w980 relative">
						<p class="tit">한 어린이와 끝까지 함께하는 양육</p>
						<span class="bar"></span>						
						<p class="con">엄마 뱃속에서부터 자립 가능한 성인이 될 때까지 각 단계에 맞는 커리큘럼으로 어린이들을 양육합니다.</p>

						<div class="desc desc1">
							<p class="d_tit">태아&middot;영아생존프로그램</p>
							태아·영아 사망률이 높은 지역의<br />
							산모와 0~3세의 어린이가 생존할 수<br />
							있도록 합니다.
						</div>
						<div class="desc desc2">
							<p class="d_tit">1:1어린이양육프로그램</p>
							컴패션 사역의 핵심 프로그램으로서, 전인적인 양육을 통해<br />
							모든 어린이들에게	‘가난에서 벗어날 수 있는 힘’을 제공합니다.
						</div>

						<span class="step txt1"><em>태아&middot;영아</em><br />0-3세</span>
						<span class="step txt2"><em>유아기</em><br />3-5세</span>
						<span class="step txt3"><em>아동기</em><br />5-12세</span>
						<span class="step txt4"><em>청소년기</em><br />12세-성인</span>

						<div class="desc desc3">
							<p class="d_tit">양육보완프로그램</p>
							1:1어린이양육을 기초로, 어린이들에게 발생하는 단기적이고<br />
							시급한 문제들을 빠르게 대응할 수 있도록 돕습니다.
						</div>
					</div>

				</div>
				<!--// 함께하는 양육 -->

				<!-- 삶의변화 -->
				<div class="bgContent black life">

					<div class="w980">
						<p class="tit">어린이들의 삶은 어떻게 변화될까요?</p>
						<span class="bar"></span>
						<p class="con mb40">
							컴패션 졸업생 삼손이 1:1어린이양육프로그램에 대해 설명합니다.<br />
							가난 속에서 태어나 늘 수업을 낙제했던 그가 어떻게 꿈을 찾고 자립할 수 있었는지 들어볼까요?
						</p>

						<script type="text/javascript">
							$(function () {
								$(".how_list .how").mouseenter(function () {
									$(".howCon").fadeOut("fast");
									$(this).find(".howCon").fadeIn("fast");
								});
								$(".howCon").mouseleave(function () {
									$(".howCon").fadeOut("fast");
								});
							})
						</script>

						<div class="how_list clear2">
							<div class="how how1">
								<span class="tit">건강하게 자랐어요</span>
								<span class="desc">(신체적 영역)</span>

								<!-- over 화면 -->
								<div class="howCon">
									어린이센터에서 정기적인 건강검진과 균형 잡힌 식사,<br />
									위생교육을 받으며 자랐어요.<br />
									그들은 제 건강과 관련된 모든 것들을<br />
									가르쳐주고 지원해줬어요.
								</div>
							</div>
							<div class="how how2">
								<span class="tit">충분한 교육을 받았어요</span>
								<span class="desc">(지적 영역)</span>

								<!-- over 화면 -->
								<div class="howCon">
									어린이센터에 등록됐다는 소식을 듣고,<br />
									저도 교복을 입고 학교에 갈 수 있다는 생각에 정말 기뻤어요.<br />
									체험 활동과 방과후학습 등의 다양한 교육들이<br />
									저의 삶을 바꿔놨어요.
								</div>
							</div>
							<div class="how how3">
								<span class="tit">내 편이 생겼어요</span>
								<span class="desc">(사회&middot;정서적 영역)</span>

								<!-- over 화면 -->
								<div class="howCon">
									컴패션을 통해 제가 사랑 받고 있다는 사실을 알았어요.<br />
									저를 위해 항상 기도하고 격려해주시던<br />
									후원자님의 편지는 아직도 간직하고 있는 보물이랍니다.
								</div>
							</div>
							<div class="how how4">
								<span class="tit">예수님의 사랑을 배웠어요</span>
								<span class="desc">(영적 영역)</span>

								<!-- over 화면 -->
								<div class="howCon">
									어린이센터에서 처음 예수님에 대해서 배웠던 시간은<br />
									절대 잊을 수 없는 사건이었어요.<br />
									선생님들로부터 성경 이야기를 듣고<br />
									함께 찬양하던 시간은 정말 행복했던 경험이었어요.
								</div>
							</div>
						</div>
						
					</div>

				</div>
				<!--// 삶의변화 -->

				<!-- 자주묻는질문 -->
				<div class="faq">
					<div class="w980">

						<p class="faq_tit">자주 묻는 질문</p>

						<ul class="tab_type1 mb60">
							<li style="width:33%" class="on"><a href="#">후원금은 어떻게 쓰이나요?</a></li>
							<li style="width:34%"><a href="#">비기독교인 어린이도 후원을 받을 수 있나요?</a></li>
							<li style="width:33%"><a href="#">컴패션 1:1 양육의 효과는 어떤가요?</a></li>
						</ul>

						<script type="text/javascript">
							$(function () {
								var a = $(".faq .tab_type1 li");

								a.click(function () {
									a.removeClass("on");
									$(this).addClass("on");

									var num = $(this).index() + 1;

									$(".faqLayer").hide();
									$(".faqLayer" + num).show();

									return false;
								});
							})
						</script>

						<!-- layer1 -->
						<div class="faqLayer faqLayer1">
							<div class="box_type2">
								<p class="tit">후원금은 어떻게 쓰이나요?</p>
								<p class="con">
									컴패션은 후원금의 80% 이상을 반드시 어린이 양육을 위해 사용합니다.<br />
									모든 양육은 현지 인력과 시설을 중심으로 진행되며, 이는 비용 절감 및 현지 경제에도 큰 도움을 줍니다.
								</p>
							</div>
							<ol class="faq_list">
								<li>
									<p class="tit">1. 어린이 양육비, 눈으로 확인할 수 있어요</p>
									<p class="con">7,000여 개 어린이센터에서는 매달 사용된 후원금을 정기적으로 보고하며, 모든 세금계산서와 영수증을 정리하여 언제든 열람할 수 있게 합니다.</p>
								</li>
								<li>
									<p class="tit">2. 어린이 양육에 직접 쓰여집니다</p>
									<p class="con">
										후원금은 지역개발금이 아닌 어린이 양육비입니다.<br />
										학교 교육, 균형 잡힌 식사, 건강관리 및 치료,  교재 및 필수품 구입, 1:1 상담 등 어린이에게 직접 사용됩니다.<br />
										※ 어린이 양육 환경 개선에 사용되는 비용은 별도 기금으로 운영됩니다.
									</p>
								</li>
								<li>
									<p class="tit">3. 매달보고, 매년 감사, 투명하게 운영돼요</p>
									<p class="con">
										매달 정기 보고 시, 미흡한 점이 발견되면 불시에 감사를 받고 3일~7일 사이에 모든 보고를 점검 받습니다.<br />
										각 수혜국의 어린이센터는 B등급 이상의 평가를 받아야만 지속적인 운영이 가능합니다.
									</p>
								</li>
								<li>
									<p class="tit">4. 통일된 시스템으로 운영돼요</p>
									<p class="con">12개 후원국과 26개 수혜국이 국제컴패션 사무국을 통해 동일한 시스템으로 소통함으로써 비용절감 효과와 높은 효율성을 갖습니다.</p>
								</li>
								<li>
									<p class="tit">5. 어린이 센터를 관리하는 양육사업 지원 담당자인 PF(Program Facilitator)가 있어요</p>
									<p class="con">
										전인적인 어린이 양육과 협업에 대한 현지 전문가인 직원(PF)이 어린이센터를 정기적으로 직접 방문해<br />
										어린이들이 제대로 양육 받고 있는지 관리하고 감독합니다.
									</p>
								</li>
							</ol>
							<div class="btn">
								<a href="/about-us/report/" class="btn_s_type6">지난 사업성과보고서 보러 가기</a>
							</div>
						</div>
						<!--// -->

						<!-- layer2 -->
						<div class="faqLayer faqLayer2" style="display:none">
							<p class="tit">비기독교인 어린이도 후원을 받을 수 있나요?</p>
							<p class="con">
								<span>네, 그렇습니다.</span><br />
								컴패션은 어린이와 가족들의 종교와 관계없이 어린이가 가진 존귀함을 최고의 가치로 여기며, 예수님의 사랑을 바탕으로 어린이들을 돕습니다.<br />
								따라서 가장 연약하고 도움이 필요한 어린이들을 돕습니다.<br /><br />

								만성 질병이나 영양실조에 걸린 어린이들이 어린이센터에 최우선으로 등록되고, 그 다음은 저소득 가정의 어린이들이 등록됩니다.<br /><br />

								그 외에도 어린이센터가 있는 지역에 오래 남아있을 수 있는지, 어린이들이 가까이 사는지 등을 고려하여 등록시키게 됩니다.
							</p>
						</div>
						<!--// -->

						<!-- layer3 -->
						<div class="faqLayer faqLayer3" style="display:none">
							<div class="box_type2">
								<p class="tit">컴패션 1:1 양육의 효과는 어떤가요?</p>
								<p class="con">
									미국 경제학자인 브루스 위딕 교수가 세계 최초로 1:1어린이결연의 경제학적 효과를 조사했습니다.<br />
									경제학적 분석으로 진행된 이 연구는 컴패션의 1:1어린이양육프로그램이 어린이의 삶에 큰 변화를 가져왔음을 보여주고 있습니다.
								</p>
							</div>
							<div class="effect">
								<ul class="clear2">
									<li class="edu">
										<span class="tit"><em>교육</em><br />더 커진 미래</span><br />
										<span class="rate">중등교육 졸업 비율<br /><em>27 ~ 40</em>%</span><br />
										<span class="rate">대학교육을 마친 비율<br /><em>50 ~ 80</em>%</span>
									</li>
									<li class="job">
										<span class="tit"><em>직업</em><br />스스로의 가능성을 찾아 나가는 힘</span><br />
										<span class="rate">직장인이 되는 비율<br /><em>14 ~ 18</em>%</span><br />
										<span class="rate">선생님이 되는 비율<br /><em>63</em>%</span>
									</li>
									<li class="leader">
										<span class="tit"><em>리더십</em><br />베푸는 사람으로의 성장</span><br />
										<span class="rate">지역사회 지도자가 될 가능성<br /><em>30 ~ 75</em>%</span><br />
										<span class="rate">교회 지도자가 될 가능성<br /><em>40 ~ 70</em>%</span>
									</li>
								</ul>
							</div>
							<div class="btn">
								<a href="/common/download/CTK_Wydick.pdf" class="btn_s_type6" target="_blank">크리스채너티 투데이 한국판 기사 보기</a>
							</div>
						</div>
						<!--// -->


					</div>
				</div>
				<!--// 자주묻는질문 -->

				<!-- 양육을 하시면? -->
				<div class="benefit">
					<div class="w980">

						<p class="tit">1:1 어린이 양육을 하시면?</p>
						<p class="desc">결연하신 어린이의 사진이 담긴 소개서와 가이드파일을 받으실 수 있습니다.</p>

						<ul class="clear2">
							<li>
								<span class="img img1"></span>
								<span class="txt">첫 결연 후원자 가이드</span>
							</li>
							<li>
								<span class="img img2"></span>
								<span class="txt">어린이의 편지(온라인)</span>
							</li>
							<li>
								<span class="img img3"></span>
								<span class="txt">어린이 성장 보고서</span>
							</li>
							<li>
								<span class="img img4"></span>
								<span class="txt">E-mail뉴스레터<br />(신청자 대상)</span>
							</li>
						</ul>

					</div>
				</div>
				<!--// 양육을 하시면? -->

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>

</asp:Content>
