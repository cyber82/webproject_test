﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using WinHttp;
using Scripting;
using Newtonsoft.Json.Linq;

public class payco_util {
	public string payco_reserve( string mData ) {
		clsHTTP_Object Result;
		string resultValue;
		JObject tmpJSON;

		string URL_reserve = ConfigurationManager.AppSettings[ConfigurationManager.AppSettings["appMode"].ToLower() + "_URL_reserve"];
		//string URL_reserve = ConfigurationManager.AppSettings["pc_URL_reserve"].ToLower();
		Result = Call_API(URL_reserve, "json", mData);

		switch(Result.Status) {
			case "200":
				resultValue = Result.ResponseText;
				break;
			default:
				tmpJSON = new JObject();
				tmpJSON.Add("result", JToken.FromObject("주문 예약 API 호출 도중 오류가 발생하였습니다."));
				tmpJSON.Add("message", JToken.FromObject(Result.ResponseText));
				tmpJSON.Add("code", JToken.FromObject(Result.Status));
				resultValue = tmpJSON.ToString();
				break;
		}
		return Result.ResponseText;
	}

	public string payco_approval( string mData ) {
		clsHTTP_Object Result;
		string resultValue;
		JObject tmpJSON;

		string URL_reserve = ConfigurationManager.AppSettings[ConfigurationManager.AppSettings["appMode"].ToLower() + "_URL_approval"];
		Result = Call_API(URL_reserve, "json", mData);

		switch(Result.Status) {
			case "200":
				resultValue = Result.ResponseText;
				break;
			default:
				tmpJSON = new JObject();
				tmpJSON.Add("result", JToken.FromObject("결제 승인 API 호출 도중 오류가 발생하였습니다."));
				tmpJSON.Add("message", JToken.FromObject(Result.ResponseText));
				tmpJSON.Add("code", JToken.FromObject(Result.Status));
				resultValue = tmpJSON.ToString();
				break;
		}
		return Result.ResponseText;
	}

	public string payco_cancel_check( string mData ) {
		clsHTTP_Object Result;
		string resultValue;
		JObject tmpJSON;

		string URL_reserve = ConfigurationManager.AppSettings[ConfigurationManager.AppSettings["appMode"].ToLower() + "_URL_cancel_check"];
		Result = Call_API(URL_reserve, "json", mData);

		switch(Result.Status) {
			case "200":
				resultValue = Result.ResponseText;
				break;
			default:
				tmpJSON = new JObject();
				tmpJSON.Add("result", JToken.FromObject("주문 결제 취소 가능 여부 조회 도중 오류가 발생하였습니다."));
				tmpJSON.Add("message", JToken.FromObject(Result.ResponseText));
				tmpJSON.Add("code", JToken.FromObject(Result.Status));
				resultValue = tmpJSON.ToString();
				break;
		}
		return Result.ResponseText;
	}

	public string payco_cancel( string mData ) {
		clsHTTP_Object Result;
		string resultValue;
		JObject tmpJSON;

		string URL_reserve = ConfigurationManager.AppSettings[ConfigurationManager.AppSettings["appMode"].ToLower() + "_URL_cancel"];
		Result = Call_API(URL_reserve, "json", mData);

		switch(Result.Status) {
			case "200":
				resultValue = Result.ResponseText;
				break;
			default:
				tmpJSON = new JObject();
				tmpJSON.Add("result", JToken.FromObject("주문 결제 취소 도중 오류가 발생하였습니다."));
				tmpJSON.Add("message", JToken.FromObject(Result.ResponseText));
				tmpJSON.Add("code", JToken.FromObject(Result.Status));
				resultValue = tmpJSON.ToString();
				break;
		}
		return Result.ResponseText;
	}

	public string payco_upstatus( string mData ) {
		clsHTTP_Object Result;
		string resultValue;
		JObject tmpJSON;

		string URL_reserve = ConfigurationManager.AppSettings[ConfigurationManager.AppSettings["appMode"].ToLower() + "_URL_upstatus"];
		Result = Call_API(URL_reserve, "json", mData);

		switch(Result.Status) {
			case "200":
				resultValue = Result.ResponseText;
				break;
			default:
				tmpJSON = new JObject();
				tmpJSON.Add("result", JToken.FromObject("주문 상태 변경 도중 오류가 발생하였습니다."));
				tmpJSON.Add("message", JToken.FromObject(Result.ResponseText));
				tmpJSON.Add("code", JToken.FromObject(Result.Status));
				resultValue = tmpJSON.ToString();
				break;
		}
		return Result.ResponseText;
	}

	public string payco_cancelmileage( string mData ) {
		clsHTTP_Object Result;
		string resultValue;
		JObject tmpJSON;

		string URL_reserve = ConfigurationManager.AppSettings[ConfigurationManager.AppSettings["appMode"].ToLower() + "_URL_cancelMileage"];
		Result = Call_API(URL_reserve, "json", mData);

		switch(Result.Status) {
			case "200":
				resultValue = Result.ResponseText;
				break;
			default:
				tmpJSON = new JObject();
				tmpJSON.Add("result", JToken.FromObject("마일리지 적립 취소 도중 오류가 발생하였습니다."));
				tmpJSON.Add("message", JToken.FromObject(Result.ResponseText));
				tmpJSON.Add("code", JToken.FromObject(Result.Status));
				resultValue = tmpJSON.ToString();
				break;
		}
		return Result.ResponseText;
	}

	public string payco_keycheck( string mData ) {
		clsHTTP_Object Result;
		string resultValue;
		JObject tmpJSON;

		string URL_reserve = ConfigurationManager.AppSettings[ConfigurationManager.AppSettings["appMode"].ToLower() + "_URL_checkUsability"];
		Result = Call_API(URL_reserve, "json", mData);

		switch(Result.Status) {
			case "200":
				resultValue = Result.ResponseText;
				break;
			default:
				tmpJSON = new JObject();
				tmpJSON.Add("result", JToken.FromObject("가맹점별 연동키 유효성 체크 도중 오류가 발생하였습니다."));
				tmpJSON.Add("message", JToken.FromObject(Result.ResponseText));
				tmpJSON.Add("code", JToken.FromObject(Result.Status));
				resultValue = tmpJSON.ToString();
				break;
		}
		return Result.ResponseText;
	}

	public string payco_verifypayment( string mData ) {
		clsHTTP_Object Result;
		string resultValue;
		JObject tmpJSON;

		string URL_reserve = ConfigurationManager.AppSettings[ConfigurationManager.AppSettings["appMode"].ToLower() + "_URL_verifyPayment"];
		Result = Call_API(URL_reserve, "json", mData);

		switch(Result.Status) {
			case "200":
				resultValue = Result.ResponseText;
				break;
			default:
				tmpJSON = new JObject();
				tmpJSON.Add("result", JToken.FromObject("결제 상세 내역 호출 도중 오류가 발생하였습니다."));
				tmpJSON.Add("message", JToken.FromObject(Result.ResponseText));
				tmpJSON.Add("code", JToken.FromObject(Result.Status));
				resultValue = tmpJSON.ToString();
				break;
		}
		return Result.ResponseText;
	}

	public string URLDecode( string sStr ) {
		sStr.Replace("+", " ");
		return System.Web.HttpUtility.UrlDecode(sStr);
	}

	public clsHTTP_Object Call_API( string SiteURL, string App_Mode, string Param ) {

		WinHttpRequest HTTP_Object = new WinHttpRequest();

		HTTP_Object.SetTimeouts(30000, 30000, 30000, 30000);
		HTTP_Object.Open("POST", SiteURL, false);
		HTTP_Object.SetRequestHeader("Content-Type", "application/" + App_Mode + "; charset=UTF-8");

		Write_Log("CALL API   " + SiteURL + " Mode : " + App_Mode);
		Write_Log("CALL API   " + SiteURL + " Data : " + Param);

		HTTP_Object.Send(Param);
		HTTP_Object.WaitForResponse(30000);

		clsHTTP_Object Result = new clsHTTP_Object();

		Result.Status = HTTP_Object.Status.ToString();
		Result.ResponseText = HTTP_Object.ResponseText;

		Write_Log("API Result " + SiteURL + " Status : " + HTTP_Object.Status.ToString());
		Write_Log("API Result " + SiteURL + " ResponseText : " + HTTP_Object.ResponseText);

		return Result;
	}

	public void Write_Log( string Log_String ) {
		if(ConfigurationManager.AppSettings["LogUse"] != "TRUE") {
			return;
		}

		string Write_LogFile = ConfigurationManager.AppSettings["Write_LogFile"];

		try {
			FileSystemObject oFSO = new FileSystemObject();
			
			TextStream oTextStream = oFSO.OpenTextFile(Write_LogFile, IOMode.ForAppending, true, 0);

			oTextStream.WriteLine(String.Format("{0:yyyy-MM-dd hh:mm:ss}", DateTime.Now) + " " + Log_String);
			oTextStream.Close();

			oTextStream = null;
			oFSO = null;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, Write_LogFile + " : " +  ex.ToString());
		}
	}
}

public class clsHTTP_Object {
	private string m_Status;
	private string m_ResponseText;

	public string Status {
		get { return m_Status; }
		set { m_Status = value; }
	}

	public string ResponseText {
		get { return m_ResponseText; }
		set { m_ResponseText = value; }
	}
}

