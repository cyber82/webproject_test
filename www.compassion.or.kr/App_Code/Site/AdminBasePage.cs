﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using NLog;
using CommonLib;

public class AdminBasePage : Page {

	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	protected HtmlInputControl sortColumn;
	protected HtmlInputControl sortDirection;

	/// <summary>
	/// 권한 인증이 필요한지 여부 체크
	/// </summary>
	protected virtual bool CheckAuthentication {
		get { return true; }
	}

	protected void Page_Load(object sender, EventArgs e) {

		if (!IsPostBack)
			this.OnBeforePostBack();
		else
			this.OnAfterPostBack();
		
	}

	protected virtual void OnBeforePostBack() {
		
		if (this.CheckAuthentication && !AdminLoginSession.HasCookie(this.Context)) {
			if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["admin_auth_key"]) 
				|| Request.RawUrl != "/@mgt"
				|| (!string.IsNullOrEmpty(Request.QueryString["auth"]) && Request.QueryString["auth"] == ConfigurationManager.AppSettings["admin_auth_key"])) {
					Response.Redirect("/@mgt/?r=" + Request.RawUrl);
					return;
			}

			Response.Redirect("/");
			
		}

		if (!AdminLoginSession.HasCookie(this.Context)) {
			return;
		}

        // 권한체크
        if (this.GetPageAuth() == null) {
            Response.Redirect("/@mgt/deny.aspx");
        }
 
		this.ViewState.Add("q", Request.QueryString.ToString());

	}

	protected virtual void OnAfterPostBack() {
		if (Request.Path.IndexOf("/@mgt/default.aspx") < 0) {
			if (this.CheckAuthentication && !AdminLoginSession.HasCookie(this.Context)) {
				Response.Redirect("/@mgt/");
			}
		}
	}

    protected v_admin_auth GetPageAuth()
    {
        return this.GetPageAuth(this.Context);
    }

    public v_admin_auth GetPageAuth(HttpContext context)
    {
        return this.GetPageAuth(context.Request.Path);
    }

    public v_admin_auth GetPageAuth(string path)
    {
        string[] excepts = new string[] { "/@mgt/", "/@mgt/dashboard/" };
        path = path.Substring(0, path.LastIndexOf('/') + 1);
        if (excepts.Contains(path))
            return new v_admin_auth();

        while (true)
        {
            //          Response.Write(path + "<br>");
            AdminEntity admin = AdminLoginSession.GetCookie(this.Context);
            if (admin.auth_menus.Any(p => p.am_path == path))
                return admin.auth_menus.First(p => p.am_path == path);

            if (admin.menus.Any(p => path.IndexOf(p.am_path) > -1))
                return null;

            path = path.Substring(0, path.Length - 1);
            if (path.LastIndexOf('/') < 0) return null;
            path = path.Substring(0, path.LastIndexOf('/') + 1);
        }
    }

    protected Uploader.FileGroup FileGroup {
		set {
			this.ViewState.Add("file_group", value);
		}
		get {
			var result = this.ViewState["file_group"];
			if (result == null)
				throw new Exception("undefined FileGroup");
			return (Uploader.FileGroup)this.ViewState["file_group"];
		}
	}

	protected string FileRoot {
		set {
			this.ViewState.Add("file_root", value);
		}
		get {
			return this.ViewState["file_root"].ToString();
		}
	}

	protected string PrimaryKey {
		set {
			if (!string.IsNullOrEmpty(value)) {
				this.ViewState.Add("idx", value);
				/*
				foreach (var file in TemporaryFiles) {
					file.f_ref_id = value;
				}
				SaveTemporaryFiles(TemporaryFiles);
				*/
			}
		}
		get {
			if (this.ViewState["idx"] == null)
				return "-1";
			return this.ViewState["idx"].ToString();
		}
	}

	public string Action {
		set {
			if (string.IsNullOrEmpty(value))
				this.ViewState.Add("action", "insert");
			else
				this.ViewState.Add("action", value);
		}
		get {
			return this.ViewState["action"].ToString();
		}
	}

	protected string Thumb {
		set {
			if (!string.IsNullOrEmpty(value))
				this.ViewState["thumb"] = value;
		}
		get {
			return (this.ViewState["thumb"] != null) ? this.ViewState["thumb"].ToString() : string.Empty;
		}
	}


	protected List<file> GetTemporaryFiles() {
		return this.GetTemporaryFiles("files");
	}

	protected List<file> GetTemporaryFiles(string key) {
		List<file> file;
		if (this.ViewState[key] == null) {
			file = new List<file>();
		} else {
			file = this.ViewState[key].ToString().ToObject<List<file>>();
		}
		return file;

	}

	protected void SaveTemporaryFiles(List<file> list) {
		this.SaveTemporaryFiles(list, "files");
	}

	protected void SaveTemporaryFiles(List<file> list, string key) {
		this.ViewState[key] = list.ToJson();
	}

	protected void FileRemove(object sender) {
		this.FileRemove(sender, "files");
	}

	protected void FileRemove(object sender, string storeKey) {
		var list = this.GetTemporaryFiles(storeKey);
		LinkButton b = sender as LinkButton;
		string filename = b.CommandArgument;

		if (list.Any(p => p.f_name == filename)) {
			list.Remove(list.First(p => p.f_name == filename));
		}

		this.SaveTemporaryFiles(list, storeKey);

	}

	protected void FileClear() {
		FileClear("files");
	}

	protected void FileClear(string storeKey) {
		this.ViewState[storeKey] = null;
	}

	protected void FileAdd(int fileSize, string fileName) {
		this.FileAdd(fileSize, fileName, "files");
	}

	protected void FileAdd(int fileSize, string fileName, string storeKey) {
		FileAdd(fileSize, fileName, storeKey, "");
	}

	protected void FileAdd(int fileSize, string fileName, string storeKey, string group2) {
		var list = this.GetTemporaryFiles(storeKey);

		file f = new file() {
			f_size = fileSize,
			f_name = fileName,
			f_order = list.Count,
			f_regdate = DateTime.Now,
			f_group = FileGroup.ToString(),
			f_group2 = group2
		};
		if (Action == "update") {
			f.f_ref_id = this.PrimaryKey;
		}

		list.Add(f);
		this.SaveTemporaryFiles(list, storeKey);
	}

	protected void FileLoad() {
		this.FileLoad("files");
	}

	protected void FileLoad(string storeKey) {
        using (AdminDataContext dao = new AdminDataContext())
        {
            var list = this.GetTemporaryFiles(storeKey);


            //var fileList = (from p in dao.file
            //                orderby p.f_order ascending
            //                where p.f_ref_id == PrimaryKey && p.f_group == FileGroup.ToString()
            //                select p).ToList();
            var fileList = www6.selectQ<file>("f_ref_id", PrimaryKey, "f_group", FileGroup.ToString(), "f_order asc");
            list.AddRange(fileList);
            this.SaveTemporaryFiles(list, storeKey);
        }
	}

	protected virtual void FileListBound(object sender, RepeaterItemEventArgs e) {
		file entity = e.Item.DataItem as file;

		((Literal)e.Item.FindControl("fileName")).Text = entity.f_name.GetFileName();
		((Literal)e.Item.FindControl("fileSize")).Text = (entity.f_size).ToString() + "KB (" + (entity.f_size / 1024.0).ToString("0.00") + "MB)";
		((Literal)e.Item.FindControl("regdate")).Text = entity.f_regdate.ToString("yyyy-MM-dd");
		((HyperLink)e.Item.FindControl("lnFileName")).NavigateUrl = (entity.f_name).WithFileServerHost();
	}

	protected virtual void list_LoadComplete(object sender, EventArgs e) {
		var page = Request["p"].ValueIfNull("1");
		this.GetList(Convert.ToInt32(page));
	}


	protected void AlertWithJavascript( string msg, string script ) {

		ScriptManager.RegisterStartupScript(this, typeof(Page), "", string.Format("alert('{0}');{1};", msg, script), true);
		//Page.ClientScript.RegisterStartupScript(this.GetType(), "", string.Format("alert('{0}');{1};", msg , (back ? "history.back()" : "")), true);
	}

	protected void AlertWithJavascript( string msg ) {
		this.AlertWithJavascript(msg, "");
	}

	protected void ConfirmWithJavascript( string msg, string script_fail ) {

		this.ConfirmWithJavascript(msg, script_fail, "");
	}

	protected void ConfirmWithJavascript( string msg, string script_fail, string script_success ) {

		ScriptManager.RegisterStartupScript(this, typeof(Page), "", string.Format("if (confirm('{0}')){{{1};}}else{{{2};}}", msg, script_success, script_fail), true);
		//Page.ClientScript.RegisterStartupScript(this.GetType(), "", string.Format("alert('{0}');{1};", msg , (back ? "history.back()" : "")), true);
	}


	protected virtual void GetList(int page) {
	}

	protected void paging_Navigate(int page , string hash) {
		this.GetList(page);
	}

	protected virtual void search(object sender, EventArgs e) {
		this.GetList(1);
	}

	protected virtual void sort(object sender, EventArgs e) {
		string col = ((LinkButton)sender).CommandArgument;

		if (col == sortColumn.Value) {
			sortDirection.Value = sortDirection.Value == "desc" ? "asc" : "desc";
		} else {
			sortColumn.Value = ((LinkButton)sender).CommandArgument;
			sortDirection.Value = "desc";
		}

		this.GetList(1);
	}


	protected void WriteLog(AdminLog.Type type) {
		this.WriteLog(type , "");
	}

	protected void WriteLog(AdminLog.Type type , string msg) {
		AdminLog.Write(this.Context, type, this.GetPageAuth().am_code , msg);
	}


	private bool _refreshState;
	private bool _isRefresh;

	public bool IsRefresh {
		get {
			return _isRefresh;
		}
	}


	protected override void LoadViewState( object savedState ) {
		object[] AllStates = (object[])savedState;
		base.LoadViewState(AllStates[0]);
		_refreshState = bool.Parse(AllStates[1].ToString());
		_isRefresh = _refreshState == (Session["__ISREFRESH"] != null && bool.Parse(Session["__ISREFRESH"].ToString()));
	}

	protected override object SaveViewState() {
		Session["__ISREFRESH"] = _refreshState;
		object[] AllStates = new object[2];
		AllStates[0] = base.SaveViewState();
		AllStates[1] = !(_refreshState);
		return AllStates;
	}

}
