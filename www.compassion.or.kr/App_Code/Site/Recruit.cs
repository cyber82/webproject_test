﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;
using NLog;

public class Recruit
{
	public Recruit()
	{
	}

	public enum FileGroup2 {
		edu , lang , work , aptitude, recommend, job
	}

	public static List<KeyValuePair<string, string>> RegistStatus() {
		
		return new List<KeyValuePair<string, string>>(){
			new KeyValuePair<string,string>( "step1" , "인적사항" ) , 
			new KeyValuePair<string,string>( "step2" , "학력/자격/어학사항" ) , 
			new KeyValuePair<string,string>( "step3" , "자기소개" ) , 
			new KeyValuePair<string,string>( "step4" , "경험사항/경력사항" ) , 
			new KeyValuePair<string,string>( "done" , "지원서 확인/제출" ) , 
		};
		
	}


	public static List<KeyValuePair<string, string>> MilitaryStatus() {
		
		return new List<KeyValuePair<string, string>>(){
			new KeyValuePair<string,string>( "" , "선택하세요" ) , 
			new KeyValuePair<string,string>( "만기제대" , "만기제대" ) , 
			new KeyValuePair<string,string>( "제대 기타" , "제대 기타" ) , 
			new KeyValuePair<string,string>( "복무중" , "복무중" ) , 
			new KeyValuePair<string,string>( "병역특례 복무만료" , "병역특례 복무만료" ) , 
			new KeyValuePair<string,string>( "병역특례 복무중(전문)" , "병역특례 복무중(전문)" ) , 
			new KeyValuePair<string,string>( "병역특례 복무중(산업)" , "병역특례 복무중(산업)" ) , 
			new KeyValuePair<string,string>( "군미필" , "군미필" ) , 
			new KeyValuePair<string,string>( "면제" , "면제" ) ,
			new KeyValuePair<string,string>( "해당없음" , "해당없음(여성)" ) 
		};
		
	}

	public static List<KeyValuePair<string, string>> MilitaryType() {
		
		return new List<KeyValuePair<string, string>>(){
			new KeyValuePair<string,string>( "" , "선택하세요" ) , 
			new KeyValuePair<string,string>( "육군" , "육군" ) , 
			new KeyValuePair<string,string>( "해군" , "해군" ) , 
			new KeyValuePair<string,string>( "공군" , "공군" ) , 
			new KeyValuePair<string,string>( "해병" , "해병" ) , 
			new KeyValuePair<string,string>( "공익요원" , "공익요원" ) , 
			new KeyValuePair<string,string>( "전경" , "전경" ) , 
			new KeyValuePair<string,string>( "의경" , "의경" ) , 
		};
		
	}

	public static List<KeyValuePair<string, string>> DisabilityStatus() {

		
			return new List<KeyValuePair<string, string>>(){
				new KeyValuePair<string,string>( "대상" , "대상" ) , 
				new KeyValuePair<string,string>( "비대상" , "비대상" ) 
			};
		
	}

	public static List<KeyValuePair<string, string>> DisabilityType() {

	
			return new List<KeyValuePair<string, string>>(){
				new KeyValuePair<string,string>( "" , "해당없음" ) , 
				new KeyValuePair<string,string>( "지체장애" , "지체장애" ) , 
				new KeyValuePair<string,string>( "뇌병변장애" , "뇌병변장애" ) , 
				new KeyValuePair<string,string>( "시각장애" , "시각장애" ) , 
				new KeyValuePair<string,string>( "청각장애" , "청각장애" ) , 
				new KeyValuePair<string,string>( "언어장애" , "언어장애" ) , 
				new KeyValuePair<string,string>( "지적장애" , "지적장애" ) , 
				new KeyValuePair<string,string>( "자페성장애" , "자페성장애" ) , 
				new KeyValuePair<string,string>( "정신장애" , "정신장애" ) , 
				new KeyValuePair<string,string>( "신장장애" , "신장장애" ) , 
				new KeyValuePair<string,string>( "심장장애" , "심장장애" ) , 
				new KeyValuePair<string,string>( "호흡기장애" , "호흡기장애" ) , 
				new KeyValuePair<string,string>( "간장애" , "간장애" ) , 
				new KeyValuePair<string,string>( "안변변형장애" , "안변변형장애" ) , 
				new KeyValuePair<string,string>( "장루-요루장애" , "장루-요루장애" ) , 
				new KeyValuePair<string,string>( "간질장애" , "간질장애" ) , 
			};
		
	}

	public static List<KeyValuePair<string, string>> DisabilityLevel() {
		
			return new List<KeyValuePair<string, string>>(){
				new KeyValuePair<string,string>( "" , "해당없음" ) , 
				new KeyValuePair<string,string>( "1급" , "1급" ) , 
				new KeyValuePair<string,string>( "2급" , "2급" ) , 
				new KeyValuePair<string,string>( "3급" , "3급" ) , 
				new KeyValuePair<string,string>( "4급" , "4급" ) , 
				new KeyValuePair<string,string>( "5급" , "5급" ) , 
				new KeyValuePair<string,string>( "6급" , "6급" )
				
			};
		
	}

	public static List<KeyValuePair<string, string>> Blood() {

			return new List<KeyValuePair<string, string>>(){
				new KeyValuePair<string,string>( "" , "선택하세요" ) , 
				new KeyValuePair<string,string>( "A" , "A" ) , 
				new KeyValuePair<string,string>( "B" , "B" ) , 
				new KeyValuePair<string,string>( "O" , "O" ) , 
				new KeyValuePair<string,string>( "AB" , "AB" ) , 
				new KeyValuePair<string,string>( "RH-A" , "RH-A" ) , 
				new KeyValuePair<string,string>( "RH-B" , "RH-B" ) , 
				new KeyValuePair<string,string>( "RH-O" , "RH-O" ) , 
				new KeyValuePair<string,string>( "RH-AB" , "RH-AB" ) , 
				new KeyValuePair<string,string>( "기타" , "기타" ) , 
				
			};
		
	}

	public static List<KeyValuePair<string, string>> Marriage() {

			return new List<KeyValuePair<string, string>>(){
				new KeyValuePair<string,string>( "" , "선택하세요" ) , 
				new KeyValuePair<string,string>( "0" , "미혼" ) , 
				new KeyValuePair<string,string>( "1" , "기혼" ) , 
				
			};
		
	}

	
	public static List<KeyValuePair<string, string>> Region() {

		return new List<KeyValuePair<string, string>>(){
			new KeyValuePair<string,string>( "서울" , "A01" ) , 
			new KeyValuePair<string,string>( "경기" , "A02" ) , 
			new KeyValuePair<string,string>( "경북" , "A03" ) , 
			new KeyValuePair<string,string>( "경남" , "A04" ) , 
			new KeyValuePair<string,string>( "충북" , "A05" ) , 
			new KeyValuePair<string,string>( "충남" , "A06" ) , 
			new KeyValuePair<string,string>( "전남" , "A07" ) , 
			new KeyValuePair<string,string>( "전북" , "A08" ) , 
			new KeyValuePair<string,string>( "강원" , "A09" ) , 
			new KeyValuePair<string,string>( "제주" , "A10" ) , 
			new KeyValuePair<string,string>( "부산" , "A11" ) , 
			new KeyValuePair<string,string>( "광주" , "A12" ) , 
			new KeyValuePair<string,string>( "대전" , "A13" ) , 
			new KeyValuePair<string,string>( "대구" , "A14" ) , 
			new KeyValuePair<string,string>( "인천" , "A15" ) , 
			new KeyValuePair<string,string>( "울산" , "A16" ) , 
			new KeyValuePair<string,string>( "해외" , "B00" ) , 

			new KeyValuePair<string,string>( "미국" , "B01" ) , 
			new KeyValuePair<string,string>( "중국" , "B02" ) , 
			new KeyValuePair<string,string>( "일본" , "B03" ) , 
			new KeyValuePair<string,string>( "영국" , "B04" ) , 
			new KeyValuePair<string,string>( "호주" , "B05" ) , 
			new KeyValuePair<string,string>( "독일" , "B06" ) , 
			new KeyValuePair<string,string>( "네덜란드" , "B07" ) , 
			new KeyValuePair<string,string>( "캐나다" , "B08" ) , 
			new KeyValuePair<string,string>( "프랑스" , "B09" ) , 
			new KeyValuePair<string,string>( "벨기에" , "B10" ) , 
			new KeyValuePair<string,string>( "스위스" , "B11" ) , 
			new KeyValuePair<string,string>( "스웨덴" , "B12" ) , 
			new KeyValuePair<string,string>( "아일랜드" , "B13" ) , 
			new KeyValuePair<string,string>( "뉴질랜드" , "B14" ) , 
			new KeyValuePair<string,string>( "홍콩" , "B15" ) , 
			new KeyValuePair<string,string>( "핀란드" , "B16" ) , 
			new KeyValuePair<string,string>( "대만" , "B17" ) , 
			new KeyValuePair<string,string>( "덴마크" , "B18" ) , 
			new KeyValuePair<string,string>( "스페인" , "B19" ) , 
			new KeyValuePair<string,string>( "싱가포르" , "B20" ) , 
			new KeyValuePair<string,string>( "브라질" , "B21" ) , 
			new KeyValuePair<string,string>( "모로코" , "B22" ) , 
			new KeyValuePair<string,string>( "오스트리아" , "B23" ) , 
			new KeyValuePair<string,string>( "이탈리아" , "B24" ) , 
			new KeyValuePair<string,string>( "체코" , "B25" ) , 
			new KeyValuePair<string,string>( "필리핀" , "B26" ) , 
			new KeyValuePair<string,string>( "헝가리" , "B27" ) , 
			new KeyValuePair<string,string>( "크로아티아" , "B28" ) , 
			new KeyValuePair<string,string>( "파키스탄" , "B29" ) , 
			new KeyValuePair<string,string>( "남아프리카 공화국" , "B30" ) , 
			new KeyValuePair<string,string>( "노르웨이" , "B31" ) , 
			new KeyValuePair<string,string>( "러시아" , "B32" ) , 
			new KeyValuePair<string,string>( "멕시코" , "B33" ) , 
				
		};
		
	}
	

	public static string GetRegistStatusText( string val) {
		if (RegistStatus().Any(p => p.Key == val))
			return RegistStatus().First(p => p.Key == val).Value;
		
		return "";
	}

}