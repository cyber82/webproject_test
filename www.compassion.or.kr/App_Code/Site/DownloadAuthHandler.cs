﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using NLog;

public class DownloadAuthHandler : IHttpHandler{
	
	public void ProcessRequest(HttpContext context) {
		
		if (context.Request.Path.StartsWith("/file/")) {
			Download(context, context.Request.Path);
			return;
		}

		Logger logger = LogManager.GetCurrentClassLogger();		
		Status status = this.CheckAuth(context, context.Request.Path);
		
		switch (status) {
			case Status.login_required :
				HttpContext.Current.Response.Write("<script>alert(\"로그인이 필요합니다.\");history.back();</script>");
				return;
			case Status.auth_required:
				HttpContext.Current.Response.Write("<script>alert(\"권한이 부족합니다.\");history.back();</script>");
				return;
			case Status.invaild_argument:
				HttpContext.Current.Response.Write("<script>alert(\"필수정보가 누락되었습니다.\");history.back();</script>");
				return;
		}

		// 다운로드
		bool success = Download(context , context.Request.Path);

		// todo 다운로드 기록 저장

	}

	public enum Status {
		success , 
		login_required , 
		auth_required , 
		invaild_argument , 
	}

	public Status CheckAuth(HttpContext context , string path) {

		
		if (AdminLoginSession.HasCookie(context))
			return Status.success;
		
		if (!FrontLoginSession.HasCookie(context))
			return Status.login_required;
		
		// todo

		return Status.success;
	}

	bool Download(HttpContext context , string path) {

		var download = context.Request["download"].ValueIfNull("0") == "1";		// 강제로 다운로드 받을것인지, 0 = 이미지의 경우 보여준다.
		path = context.Server.MapPath(path);
		if (!File.Exists(path)) {
			HttpContext.Current.Response.Write("<script>alert(\"File Not Found\");history.back();</script>");
			return false;
		}

		if(!new List<string> { ".pdf", ".zip", ".jpg", ".jpeg", ".gif", ".png", ".exe", ".gsd", ".dwg", ".igs", ".dxf", ".docx", ".doc", ".xlsx", ".xls", ".pptx", ".ppt" }.Contains(Path.GetExtension(path).ToLower())) {
			HttpContext.Current.Response.Write("<script>alert(\"Not Allowed.(501)\");history.back();</script>");
			return false;
		}

		var contentType = "application/octet-stream";
		
		FileStream inStr = null;
		byte[] buffer = new byte[1024];
		long byteCount;
		try {
			HttpContext.Current.Response.Clear();
			HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
			HttpContext.Current.Response.Buffer = false;
			HttpContext.Current.Response.ContentType = contentType;
			if (contentType == "application/octet-stream") {
				HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(Path.GetFileName(path)));
			}

		//	throw new Exception();
			inStr = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 1024, false);
			while ((byteCount = inStr.Read(buffer, 0, buffer.Length)) > 0) {
				
				if (HttpContext.Current.Response.IsClientConnected) {
					HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
					HttpContext.Current.Response.Flush();
				} else {
					break;
				}
			}

			return true;

		} catch {
			try {
				HttpContext.Current.Response.Clear();
				HttpContext.Current.Response.ContentType = "text/html";
				HttpContext.Current.Response.ClearHeaders();
				HttpContext.Current.Response.Write("<script>alert(\"Download Error\");history.back();</script>");
			} catch {
			}
			return false;
			
		} finally {
			if (inStr != null)
				inStr.Close();
		}
		
	}

	public bool IsReusable {
		get {
			return false;
		}
	}

}