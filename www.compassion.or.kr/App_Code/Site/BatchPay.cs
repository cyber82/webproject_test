﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CommonLib;
using System.Data;

// 결제하고 컴파스에 등록
public class BatchPay {
	
	public BatchPay()
	{
	}

	public JsonWriter Pay( string commitmentId, string orderId, int amount, string payInfoGroup ) {

		var sess = new UserInfo();
		JsonWriter result = new JsonWriter() { success = false };

		WWWService.Service _wwwService = new WWWService.Service();
		DataSet dsPaymentType = null;
		try {
			dsPaymentType = _wwwService.selectUsingPaymentAccount(sess.SponsorID, "");
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			result.message = "납부방법을 가져오는 중 오류가 발생했습니다.";
			return result;
		}

		var paymentName = dsPaymentType.Tables[0].Rows[0]["PaymentName"].ToString().Trim();
		var batchKey = dsPaymentType.Tables[0].Rows[0]["Account"].ToString().Trim();
		var paymentAccountID = dsPaymentType.Tables[0].Rows[0]["paymentAccountID"].ToString().Trim();
		var bankCode = dsPaymentType.Tables[0].Rows[0]["bankCode"].ToString().Trim();
		var juminSaup = dsPaymentType.Tables[0].Rows[0]["juminSaup"].ToString().Trim();
		var owner = dsPaymentType.Tables[0].Rows[0]["Requester"].ToString().Trim();
		var group_idxx = dsPaymentType.Tables[0].Rows[0]["DEPOSITOR"].ToString().Trim();
		

		return this.Pay(group_idxx , commitmentId, orderId , "한국컴패션후원금" , amount , paymentName,  batchKey,  paymentAccountID,  bankCode,  juminSaup,  owner, payInfoGroup);
	}

	public JsonWriter Pay(string group_idxx , string commitmentId , string orderId , string goodName , int amount , 
		string paymentName , string batchKey , string paymentAccountID , string bankCode , string juminSaup , string owner, string payInfoGroup) {
		JsonWriter result = new JsonWriter() { success = false };

		var sess = new UserInfo();
        var actionCommitment = new CommitmentAction();

        WWWService.Service _wwwService = new WWWService.Service();
	

		if(paymentName == "신용카드자동결제") {
			#region 신용카드 
            //실제 결제
			var payResult = new KCPBatchPay().Pay(group_idxx , batchKey, orderId, goodName, amount.ToString(),
				sess.UserName, sess.Email, sess.Phone, sess.Mobile);

            ErrorLog.Write(HttpContext.Current, 0, string.Format("신용카드 즉시결제 : {0},{1},{2},{3},{4},{5},{6},{7},{8}", paymentAccountID, commitmentId, amount.ToString(),
                payResult.card_cd, payResult.card_name, payResult.app_no,
                payResult.app_time, payResult.tno, orderId
                ));
            
            if(!payResult.success || string.IsNullOrEmpty(payResult.tno)) {
				//result.message = "결제에 실패했습니다.";
				result.message = payResult.res_msg;
                
                return result;
			}
            
			var actionResult = new CommitmentAction().BatchPay(paymentAccountID, commitmentId, amount.ToString(), payResult.card_cd, payResult.card_name, payResult.app_no, payResult.app_time, payResult.tno, orderId);
			result.success = actionResult.success;
            if (actionResult.success)
            {
                //[고진혁] GlobalSponsorID 생성 및 GlobalCommitment 생성 tCommitmentMaster Update
                if (payInfoGroup == "CDSP")
                {
                    JsonWriter successResult = actionCommitment.ProcSuccessPay(commitmentId);
                    if (!successResult.success)
                    {
                        result.message = successResult.message;
                        result.success = successResult.success;
                    }
                }
                return result;
            }
            else
            {
                result.message = actionResult.message;
                return result;
            }

            #endregion
        } else if(paymentName.ToUpper() == "CMS") {

			#region CMS
			var actionResult = new HSCMSAction().Pay(bankCode, batchKey, juminSaup, owner, amount , paymentAccountID , commitmentId);
			result.success = actionResult.success;
			if(actionResult.success) {
				var payResult = (HSCMSAction.PaymentResult)actionResult.data;
				// 전문 송수신은 성공이지만 결제성공여부를 체크
				if(payResult.success) {
                    if (payResult != null && payResult.msg != null && payResult.msg.Contains("즉시결제 대상은행이 아님"))
                    {
                        result.message = payResult.msg;
                    }
                    else
                    {
                        //[고진혁] GlobalSponsorID 생성 및 GlobalCommitment 생성 tCommitmentMaster Update
                        if (payInfoGroup == "CDSP")
                        {
                            JsonWriter successResult = actionCommitment.ProcSuccessPay(commitmentId);
                            if (!successResult.success)
                            {
                                result.message = successResult.message;
                                result.success = successResult.success;
                            }
                        }
                        
                    }
                    return result;
                }
				else {
                    result.success = false;
                    result.message = "에러코드(" + payResult.code + ")";
                    return result;
				}
			} else {
                result.message = actionResult.message;
                return result;
			}
			#endregion
		}

		result.success = true;
		return result;
	}



}