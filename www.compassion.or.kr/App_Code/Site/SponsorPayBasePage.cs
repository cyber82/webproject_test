﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using NLog;
using System.Text;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;
using System.Data;

public abstract class SponsorPayBasePage : FrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	public WWWService.Service _wwwService = new WWWService.Service();
	public WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
	
	HtmlInputHidden hdSponsorId, hdBirthDate, hd_age, user_class, hdState, hdManageType, jumin, exist_account, motiveCode, motiveName, hdLocation, hd_auth_domain;
	HtmlInputHidden gender, hdCI, ci, di, cert_gb, parent_name, parent_juminId, parent_mobile, parent_email, hfAddressType, zipcode, addr1, addr2, hdOrganizationID, parent_cert , dmYN , translationYN;
    HtmlInputHidden dspAddrDoro, dspAddrJibun;
    HtmlInputControl user_name;
    //HtmlInputText first_name, last_name, church_name;
    HtmlInputControl first_name, last_name, church_name, religion;
    PlaceHolder ph_no_jumin, ph_cert, ph_parent_cert, ph_new_pay, ph_exist_pay , ph_exist_pay2, ph_yes_jumin , ph_payment_method_oversea , ph_payment_method_cms , ph_payment_method_card;
	HtmlInputRadioButton religion1, religion2, religion3, religion4;
	HtmlInputCheckBox noname , addr_overseas , p_receipt_pub_ok;
	HtmlInputRadioButton payment_method_card, payment_method_cms, payment_method_oversea , payment_method_phone , payment_method_giro , payment_method_virtualaccount;
	Literal lb_exist_pay_msg1, lb_exist_pay_msg2 , lt_exist_pay2;
	DropDownList ddlHouseCountry , cms_bank_etc;
	HtmlTableRow pn_addr_domestic, pn_addr_overseas;
	HtmlGenericControl ph_cert_me;
    
    private delegate void DoStuff(); //delegate for the action
    private string CommitmentID;
    private string SponsorID;
    private string ChildMasterID;
	

	// 정기후원
	protected void OnBeforePostBackRegular() {

		if(!PayItemSession.HasCookie(this.Context)) {
			Response.Redirect("/");
			return;
		}

		var payInfo = PayItemSession.GetCookie(this.Context);
		if(payInfo.frequency == "일시") {
			Response.Redirect("/");
			return;

		}
		this.OnBeforePostBackRegular(payInfo);
	}

	protected void OnBeforePostBackRegular( PayItemSession.Entity payInfo ) {

		this.ViewState["good_name"] = "한국컴패션후원금";
		this.ViewState["codeName"] = "";
		this.ViewState["motiveCode"] = "";
		this.ViewState["motiveName"] = "";
		this.ViewState["user_name"] = "";
		this.ViewState["childMasterId"] = "";
		this.ViewState["kcpgroup_id"] = ConfigurationManager.AppSettings["kcpgroup_id"];
		this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];
		this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_batch_site_cd"];

		this.InitRegularControl();

		hd_auth_domain.Value = this.ViewState["auth_domain"].ToString();
		
		this.ViewState["childMasterId"] = payInfo.childMasterId;
		this.ViewState["campaignId"] = payInfo.campaignId;
		this.ViewState["codeId"] = payInfo.codeId;
		this.ViewState["codeName"] = payInfo.codeName;
	

		if(!UserInfo.IsLogin) {
			base.AlertWithJavascript("로그인한 사용자만 정기결제가 가능합니다.", "goLogin()");
			return;
		}

		// CMS 은행정보
		if(cms_bank_etc != null) {
			var actionResult = new CodeAction().CMSBanks();
			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message, "goBack()");
			} else {

				var exceptCodes = new string[] { "004",  "011", "012", "088", "020", "081", "003" };        // radio 로 제공하는 은행
				DataTable dt = (DataTable)actionResult.data;
				foreach(DataRow dr in dt.Rows) {
					if(exceptCodes.Contains(dr["codeId"].ToString()))
						continue;
					cms_bank_etc.Items.Add(new ListItem(dr["name"].ToString(), dr["codeId"].ToString()));
				}
			}
		}

		this.GetUserInfo();

		this.GetUserDatInfo(true);

		this.RenderControl();
		
	}


    protected void OnBeforePostBackPayAgain()
    {
        if (!PayItemSession.HasCookie(this.Context))
        {
            Response.Redirect("/");
            return;
        }
        var payInfo = PayItemSession.GetCookie(this.Context);

        this.ViewState["good_name"] = "한국컴패션후원금";
        this.ViewState["codeName"] = "";
        this.ViewState["motiveCode"] = "";
        this.ViewState["motiveName"] = "";
        this.ViewState["user_name"] = "";
        this.ViewState["childMasterId"] = "";
        this.ViewState["kcpgroup_id"] = ConfigurationManager.AppSettings["kcpgroup_id"];
        this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];
        this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_batch_site_cd"];
        this.ViewState["good_mny"] = "";

        this.InitPayAgainControl();

        hd_auth_domain.Value = this.ViewState["auth_domain"].ToString();

        this.ViewState["childMasterId"] = payInfo.childMasterId;
        this.ViewState["campaignId"] = payInfo.campaignId;
        this.ViewState["codeId"] = payInfo.codeId;
        this.ViewState["codeName"] = payInfo.codeName;


        if (!UserInfo.IsLogin)
        {
            base.AlertWithJavascript("로그인한 사용자만 정기결제가 가능합니다.", "goLogin()");
            return;
        }

        // CMS 은행정보
        if (cms_bank_etc != null)
        {
            var actionResult = new CodeAction().CMSBanks();
            if (!actionResult.success)
            {
                base.AlertWithJavascript(actionResult.message, "goBack()");
            }
            else
            {

                var exceptCodes = new string[] { "004", "011", "012", "088", "020", "081", "003" };        // radio 로 제공하는 은행
                DataTable dt = (DataTable)actionResult.data;
                foreach (DataRow dr in dt.Rows)
                {
                    if (exceptCodes.Contains(dr["codeId"].ToString()))
                        continue;
                    cms_bank_etc.Items.Add(new ListItem(dr["name"].ToString(), dr["codeId"].ToString()));
                }
            }
        }

        this.GetUserInfo();

        this.GetUserDatInfoPayAgain();

        //this.RenderControl();

    }

    // 정기결제 폼 컨트롤 초기화
    void InitRegularControl() {

		dmYN = (HtmlInputHidden)Page.FindAnyControl("dmYN");
		translationYN = (HtmlInputHidden)Page.FindAnyControl("translationYN");
		
		hd_auth_domain = (HtmlInputHidden)Page.FindAnyControl("hd_auth_domain");
		hdSponsorId = (HtmlInputHidden)Page.FindAnyControl("hdSponsorId");
		user_name = (HtmlInputHidden)Page.FindAnyControl("user_name");
		hdBirthDate = (HtmlInputHidden)Page.FindAnyControl("hdBirthDate");
		hd_age = (HtmlInputHidden)Page.FindAnyControl("hd_age");
		user_class = (HtmlInputHidden)Page.FindAnyControl("user_class");
		ph_no_jumin = (PlaceHolder)Page.FindAnyControl("ph_no_jumin");
		ph_cert = (PlaceHolder)Page.FindAnyControl("ph_cert");				// 휴대폰,이메일인증
		ph_cert_me = (HtmlGenericControl)Page.FindAnyControl("ph_cert_me");		// 본인인증

		hdState = (HtmlInputHidden)Page.FindAnyControl("hdState");
		hdManageType = (HtmlInputHidden)Page.FindAnyControl("hdManageType");
		jumin = (HtmlInputHidden)Page.FindAnyControl("jumin");
		ph_parent_cert = (PlaceHolder)Page.FindAnyControl("ph_parent_cert");

		exist_account = (HtmlInputHidden)Page.FindAnyControl("exist_account");
		ph_new_pay = (PlaceHolder)Page.FindAnyControl("ph_new_pay");
		ph_exist_pay = (PlaceHolder)Page.FindAnyControl("ph_exist_pay");
		ph_exist_pay2 = (PlaceHolder)Page.FindAnyControl("ph_exist_pay2");
		ph_payment_method_oversea = (PlaceHolder)Page.FindAnyControl("ph_payment_method_oversea");
		ph_payment_method_cms = (PlaceHolder)Page.FindAnyControl("ph_payment_method_cms");
		ph_payment_method_card = (PlaceHolder)Page.FindAnyControl("ph_payment_method_card");
		
		lb_exist_pay_msg1 = (Literal)Page.FindAnyControl("lb_exist_pay_msg1");
		lb_exist_pay_msg2 = (Literal)Page.FindAnyControl("lb_exist_pay_msg2");
		lt_exist_pay2 = (Literal)Page.FindAnyControl("lt_exist_pay2");

		motiveCode = (HtmlInputHidden)Page.FindAnyControl("motiveCode");
		motiveName = (HtmlInputHidden)Page.FindAnyControl("motiveName");

		first_name = (HtmlInputText)Page.FindAnyControl("first_name");
		last_name = (HtmlInputText)Page.FindAnyControl("last_name");
		gender = (HtmlInputHidden)Page.FindAnyControl("gender");
		hdCI = (HtmlInputHidden)Page.FindAnyControl("hdCI");
		ci = (HtmlInputHidden)Page.FindAnyControl("ci");
		di = (HtmlInputHidden)Page.FindAnyControl("di");
		cert_gb = (HtmlInputHidden)Page.FindAnyControl("cert_gb");
		parent_name = (HtmlInputHidden)Page.FindAnyControl("parent_name");
		parent_juminId = (HtmlInputHidden)Page.FindAnyControl("parent_juminId");
		parent_mobile = (HtmlInputHidden)Page.FindAnyControl("parent_mobile");
		parent_email = (HtmlInputHidden)Page.FindAnyControl("parent_email");
		parent_cert = (HtmlInputHidden)Page.FindAnyControl("parent_cert");

        dspAddrDoro = (HtmlInputHidden)Page.FindAnyControl("dspAddrDoro");
        dspAddrJibun = (HtmlInputHidden)Page.FindAnyControl("dspAddrJibun");

        ph_yes_jumin = (PlaceHolder)Page.FindAnyControl("ph_yes_jumin");
		ph_no_jumin = (PlaceHolder)Page.FindAnyControl("ph_no_jumin");
		religion1 = (HtmlInputRadioButton)Page.FindAnyControl("religion1");
		religion2 = (HtmlInputRadioButton)Page.FindAnyControl("religion2");
		religion3 = (HtmlInputRadioButton)Page.FindAnyControl("religion3");
		religion4 = (HtmlInputRadioButton)Page.FindAnyControl("religion4");
		church_name = (HtmlInputText)Page.FindAnyControl("church_name");

		hdLocation = (HtmlInputHidden)Page.FindAnyControl("hdLocation");
		
		addr_overseas = (HtmlInputCheckBox)Page.FindAnyControl("addr_overseas");

		cms_bank_etc = (DropDownList)Page.FindAnyControl("cms_bank_etc");
		ddlHouseCountry = (DropDownList)Page.FindAnyControl("ddlHouseCountry");
		hfAddressType = (HtmlInputHidden)Page.FindAnyControl("hfAddressType");
		pn_addr_domestic = (HtmlTableRow)Page.FindAnyControl("pn_addr_domestic");
		pn_addr_overseas = (HtmlTableRow)Page.FindAnyControl("pn_addr_overseas");
		zipcode = (HtmlInputHidden)Page.FindAnyControl("zipcode");
		addr1 = (HtmlInputHidden)Page.FindAnyControl("addr1");
		addr2 = (HtmlInputHidden)Page.FindAnyControl("addr2");
		hdOrganizationID = (HtmlInputHidden)Page.FindAnyControl("hdOrganizationID");
		
		p_receipt_pub_ok = (HtmlInputCheckBox)Page.FindAnyControl("p_receipt_pub_ok");

		payment_method_card = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_card");
		payment_method_cms = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_cms");
		payment_method_oversea = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_oversea");
		payment_method_giro = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_giro");
		payment_method_virtualaccount = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_virtualaccount");
	}

    void InitPayAgainControl()
    {

        dmYN = (HtmlInputHidden)Page.FindAnyControl("dmYN");
        translationYN = (HtmlInputHidden)Page.FindAnyControl("translationYN");

        hd_auth_domain = (HtmlInputHidden)Page.FindAnyControl("hd_auth_domain");
        hdSponsorId = (HtmlInputHidden)Page.FindAnyControl("hdSponsorId");
        user_name = (HtmlInputHidden)Page.FindAnyControl("user_name");
        hdBirthDate = (HtmlInputHidden)Page.FindAnyControl("hdBirthDate");
        hd_age = (HtmlInputHidden)Page.FindAnyControl("hd_age");
        user_class = (HtmlInputHidden)Page.FindAnyControl("user_class");
        ph_no_jumin = (PlaceHolder)Page.FindAnyControl("ph_no_jumin");
        ph_cert = (PlaceHolder)Page.FindAnyControl("ph_cert");              // 휴대폰,이메일인증
        ph_cert_me = (HtmlGenericControl)Page.FindAnyControl("ph_cert_me");     // 본인인증

        hdState = (HtmlInputHidden)Page.FindAnyControl("hdState");
        hdManageType = (HtmlInputHidden)Page.FindAnyControl("hdManageType");
        jumin = (HtmlInputHidden)Page.FindAnyControl("jumin");
        ph_parent_cert = (PlaceHolder)Page.FindAnyControl("ph_parent_cert");

        exist_account = (HtmlInputHidden)Page.FindAnyControl("exist_account");
        ph_new_pay = (PlaceHolder)Page.FindAnyControl("ph_new_pay");
        ph_exist_pay = (PlaceHolder)Page.FindAnyControl("ph_exist_pay");
        ph_exist_pay2 = (PlaceHolder)Page.FindAnyControl("ph_exist_pay2");
        ph_payment_method_oversea = (PlaceHolder)Page.FindAnyControl("ph_payment_method_oversea");
        ph_payment_method_cms = (PlaceHolder)Page.FindAnyControl("ph_payment_method_cms");
        ph_payment_method_card = (PlaceHolder)Page.FindAnyControl("ph_payment_method_card");


        lb_exist_pay_msg1 = (Literal)Page.FindAnyControl("lb_exist_pay_msg1");
        lb_exist_pay_msg2 = (Literal)Page.FindAnyControl("lb_exist_pay_msg2");
        lt_exist_pay2 = (Literal)Page.FindAnyControl("lt_exist_pay2");

        motiveCode = (HtmlInputHidden)Page.FindAnyControl("motiveCode");
        motiveName = (HtmlInputHidden)Page.FindAnyControl("motiveName");

        first_name = (HtmlInputHidden)Page.FindAnyControl("first_name");
        last_name = (HtmlInputHidden)Page.FindAnyControl("last_name");
        gender = (HtmlInputHidden)Page.FindAnyControl("gender");
        hdCI = (HtmlInputHidden)Page.FindAnyControl("hdCI");
        ci = (HtmlInputHidden)Page.FindAnyControl("ci");
        di = (HtmlInputHidden)Page.FindAnyControl("di");
        cert_gb = (HtmlInputHidden)Page.FindAnyControl("cert_gb");
        parent_name = (HtmlInputHidden)Page.FindAnyControl("parent_name");
        parent_juminId = (HtmlInputHidden)Page.FindAnyControl("parent_juminId");
        parent_mobile = (HtmlInputHidden)Page.FindAnyControl("parent_mobile");
        parent_email = (HtmlInputHidden)Page.FindAnyControl("parent_email");
        parent_cert = (HtmlInputHidden)Page.FindAnyControl("parent_cert");

        dspAddrDoro = (HtmlInputHidden)Page.FindAnyControl("dspAddrDoro");
        dspAddrJibun = (HtmlInputHidden)Page.FindAnyControl("dspAddrJibun");
        /*
        ph_yes_jumin = (PlaceHolder)Page.FindAnyControl("ph_yes_jumin");
        ph_no_jumin = (PlaceHolder)Page.FindAnyControl("ph_no_jumin");
        */
        religion = (HtmlInputHidden)Page.FindAnyControl("religion");
        religion1 = (HtmlInputRadioButton)Page.FindAnyControl("religion1");
        religion2 = (HtmlInputRadioButton)Page.FindAnyControl("religion2");
        religion3 = (HtmlInputRadioButton)Page.FindAnyControl("religion3");
        religion4 = (HtmlInputRadioButton)Page.FindAnyControl("religion4");
        church_name = (HtmlInputHidden)Page.FindAnyControl("church_name");

        hdLocation = (HtmlInputHidden)Page.FindAnyControl("hdLocation");

        addr_overseas = (HtmlInputCheckBox)Page.FindAnyControl("addr_overseas");

        cms_bank_etc = (DropDownList)Page.FindAnyControl("cms_bank_etc");
        ddlHouseCountry = (DropDownList)Page.FindAnyControl("ddlHouseCountry");
        hfAddressType = (HtmlInputHidden)Page.FindAnyControl("hfAddressType");
        pn_addr_domestic = (HtmlTableRow)Page.FindAnyControl("pn_addr_domestic");
        pn_addr_overseas = (HtmlTableRow)Page.FindAnyControl("pn_addr_overseas");
        zipcode = (HtmlInputHidden)Page.FindAnyControl("zipcode");
        addr1 = (HtmlInputHidden)Page.FindAnyControl("addr1");
        addr2 = (HtmlInputHidden)Page.FindAnyControl("addr2");
        hdOrganizationID = (HtmlInputHidden)Page.FindAnyControl("hdOrganizationID");

        p_receipt_pub_ok = (HtmlInputCheckBox)Page.FindAnyControl("p_receipt_pub_ok");

        payment_method_card = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_card");
        payment_method_cms = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_cms");
        payment_method_oversea = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_oversea");
        payment_method_giro = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_giro");
        payment_method_virtualaccount = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_virtualaccount");
    }
    // 사용자정보
    protected bool GetUserInfo() {

		if(!UserInfo.IsLogin) {
			return false;
		}

		var sess = new UserInfo();
		user_name.Value = sess.UserName;
		this.ViewState["user_name"] = user_name.Value;

		if (hdSponsorId != null)
			hdSponsorId.Value = sess.SponsorID;
		hdLocation.Value = sess.LocationType;
		
		var birth = sess.Birth.EmptyIfNull().Length > 9 ? sess.Birth.Substring(0, 10) : "1900-01-01";
		if (hdBirthDate != null) hdBirthDate.Value = birth.Replace("-", "");
		var age = Convert.ToDateTime(birth).GetAge();
		hd_age.Value = age.ToString();

		// 만 19세 미만
		if(age < 19) {
			user_class.Value = "19세미만";
		}

		if(age < 14) {
			user_class.Value = "14세미만";
			if(ph_no_jumin != null)
				ph_no_jumin.Visible = false;
			if(ph_cert != null)
				ph_cert.Visible = false;
		}

		if(sess.GenderCode == "C") {
			user_class.Value = "기업";
		}

		hdState.Value = user_class.Value;
		
		if(!(sess.UserClass == "기업" || sess.UserClass == "기타")) {
			if(ph_parent_cert != null) {
				if(age < 19) {
					ph_parent_cert.Visible = true;
				}
			}
		}

		// 본인인증 제외인경우 

		//if (sess.GenderCode == "C" || sess.UserClass == "기타" || sess.LocationType == "미주" || sess.LocationType == "국외") {
		if(sess.GenderCode == "C" || sess.UserClass == "기타" ) {
			if (ph_cert_me != null) {
				ph_cert_me.Style["display"] = "none";
			}
		}

		if(sess.LocationType != "국내" ) {
			if(ph_cert != null)
				ph_cert.Visible = false;
		}

        /*
		if(motiveCode != null)
			motiveCode.Value = "M0048";
		if(motiveName != null)
			motiveName.Value = "컴패션 홈페이지, 검색";
            */

        //if (motiveCode != null)
        //{
        //    motiveCode.Value = Request.QueryString["motiveCode"];
        //}
        //if (motiveName != null)
        //{
        //    motiveName.Value = Request.QueryString["motiveName"];
        //}

        return true;
	}


    protected bool GetUserDatInfoPayAgain()
    {

        if (!UserInfo.IsLogin)
        {
            return false;
        }

        DataSet dsSponsor = new DataSet();
        SponsorAction sponsorAction = new SponsorAction();
        JsonWriter actionResult;

         

        var sess = new UserInfo();

        actionResult = sponsorAction.GetSponsor();
        if (!actionResult.success)
        {
            if (actionResult.action != "not_registered")
            {
                base.AlertWithJavascript(actionResult.message, "goBack()");
            }
            return false;
        }

        SponsorAction.SponsorItem sponsorItem = (SponsorAction.SponsorItem)actionResult.data;


        #region 기본정보

        if (string.IsNullOrEmpty(sponsorItem.UserClass))
        {
            base.AlertWithJavascript("회원님의 가입유형이 존재하지 않습니다. \\r\\n관리자에게 문의해주세요.", "goBack()");
            return false;
        }

        //후원자구분
        hdState.Value = sponsorItem.UserClass;

        hdManageType.Value = sponsorItem.ManageType;
        if (hdManageType.Value == "")
            hdManageType.Value = "0";
        var manageType = hdManageType.Value;

        //주민번호
        if (sponsorItem.UserClass == "기업" || sponsorItem.UserClass == "기타")
        {
        }
        else
        {
            var ssn = sponsorItem.JuminID;
            jumin.Value = ssn;

        }


        gender.Value = sponsorItem.GenderCode;
        hdCI.Value = ci.Value = sponsorItem.CI;
        di.Value = sponsorItem.DI;
        cert_gb.Value = sponsorItem.CertifyOrgan;
        /*
		if(motiveCode != null)
			motiveCode.Value = sponsorItem.MotiveCode.ValueIfNull("M0048");
		if(motiveName != null)
			motiveName.Value = sponsorItem.MotiveCodeName.ValueIfNull("컴패션 홈페이지, 검색");
            */

        hdLocation.Value = sponsorItem.LocationType;

         

            first_name.Value = sponsorItem.FirstName;
            last_name.Value = sponsorItem.LastName;
            if (parent_name != null)
            {
                /*
				parent_name.Value = sponsorItem.ParentName;
				parent_juminId.Value = sponsorItem.ParentJuminID;
				parent_mobile.Value = sponsorItem.ParentMobile;
				parent_email.Value = sponsorItem.ParentEmail;
				*/
            }
            var religionType = sponsorItem.ReligionType;
            religion.Value = religionType;

            if (religionType == "기독교" || religionType == "")
            {
                church_name.Value = sponsorItem.ChurchName;
            }

            translationYN.Value = (sponsorItem.TranslationFlag == "Y") ? "Y" : "N";

            // 후원정보 수신
            actionResult = sponsorAction.GetReceiveSponsorInfo();
            if (!actionResult.success)
            {
                base.AlertWithJavascript(actionResult.message);
                return false;
            }

            dmYN.Value = (bool)actionResult.data ? "Y" : "N";

        

        #endregion

        #region 연말정산 거부정보

        // 연말정산 영수증
        actionResult = sponsorAction.IsPayReceipt();
        if (!actionResult.success)
        {
            base.AlertWithJavascript(actionResult.message);
            return false;
        }

        p_receipt_pub_ok.Checked = (bool)actionResult.data;

        #endregion

         

            #region 주소정보
            actionResult = sponsorAction.GetAddress();
            if (!actionResult.success)
            {
                base.AlertWithJavascript(actionResult.message, "goBack()");
                return false;
            }

            SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)actionResult.data;
            zipcode.Value = addr_data.Zipcode;
            addr1.Value = addr_data.Addr1;
            addr2.Value = addr_data.Addr2;
            hfAddressType.Value = addr_data.AddressType;
            //ddlHouseCountry.SelectedValue = addr_data.Country;

            if (dspAddrDoro != null)
                dspAddrDoro.Value = addr_data.DspAddrDoro;
            if (dspAddrJibun != null)
                dspAddrJibun.Value = addr_data.DspAddrJibun;

            if (!addr_data.ExistAddress)
            {

                if (pn_addr_domestic != null)
                    pn_addr_domestic.Style["display"] = "table-row";
            }
            #endregion

            if (ph_new_pay != null)
            {
                #region 기존 납부방법 조회
                exist_account.Value = "N";

                DataSet dsPaymentType = null;
                try
                {
                    dsPaymentType = _wwwService.selectUsingPaymentAccount(sess.SponsorID, "");
                }
                catch (Exception ex)
                {
                    ErrorLog.Write(this.Context, 0, ex.Message);
                    base.AlertWithJavascript("납부방법을 가져오는 중 오류가 발생했습니다.", "goBack()");
                    return false;
                }

                if (dsPaymentType.Tables[0].Rows.Count > 0)
                {

                    // 자동결제 정보 
                    this.ViewState["paymentAccount"] = dsPaymentType;

                    var paymentName = dsPaymentType.Tables[0].Rows[0]["PaymentName"].ToString().Trim();
                    var bankName = dsPaymentType.Tables[0].Rows[0]["BankName"].ToString().Trim();

                    //if(paymentName == "지로" || paymentName == "해외카드" ) {
                    if (paymentName == "해외카드")
                    {     // CMS 첫신청인 경우 지로로 등록되는 로직이 있다.
                        if (ph_payment_method_cms != null)
                            ph_payment_method_cms.Visible = ph_payment_method_card.Visible = false;
                        return true;
                    }
                    ph_new_pay.Visible = false;
                    ph_exist_pay.Visible = ph_exist_pay2.Visible = true;
                    exist_account.Value = "Y";

                    if (paymentName == "신용카드자동결제")
                    {
                        if (payment_method_card != null)
                            payment_method_card.Checked = true;
                        lb_exist_pay_msg1.Text = "기존에 <em class='fc_blue'>등록하신 신용카드 자동이체 결제정보</em>가 있습니다."; //결제방법                    
                        lb_exist_pay_msg2.Text = "기존 정보에 추가로 결제금액을 등록합니다.";

                    }
                    else if (paymentName.ToUpper() == "CMS")
                    {
                        if (payment_method_cms != null)
                            payment_method_cms.Checked = true;
                        lb_exist_pay_msg1.Text = "기존에 <em class='fc_blue'>등록하신 CMS자동이체 결제정보</em>가 있습니다.";
                        lb_exist_pay_msg2.Text = "기존 정보에 추가로 결제금액을 등록합니다.";

                    }
                    else if (paymentName == "가상계좌")
                    {
                        if (payment_method_virtualaccount != null)
                            payment_method_virtualaccount.Checked = true;
                        lb_exist_pay_msg1.Text = "후원자님은 현재 <em class='fc_blue'>가상계좌</em>로 후원금을 납부하고 계시며,";
                        lb_exist_pay_msg2.Text = "기존 정보에 추가로 결제금액을 등록합니다.";
                        if (lt_exist_pay2 != null)
                            lt_exist_pay2.Text = "아래 결제 버튼을 누르시면 다음달부터 동일한 가상계좌로 납부하실 수 있습니다.";
                    }
                    else if (paymentName == "미주")
                    {
                        lb_exist_pay_msg1.Text = "후원자님은 현재 매월 <em class='fc_blue'>미주</em>로 후원금을 납부하고 계십니다.";
                        lb_exist_pay_msg2.Text = "미주 사무실로 연락 부탁 드립니다.";
                    }
                    else if (paymentName == "지로")
                    {
                        if (payment_method_giro != null)
                            payment_method_giro.Checked = true;
                        lb_exist_pay_msg1.Text = "후원자님은 현재 <em class='fc_blue'>지로</em>로 후원금을 납부하고 계시며,";
                        if (lt_exist_pay2 != null)
                            lt_exist_pay2.Text = "아래 결제 버튼을 누르시면 다음달부터 지로로 납부하실 수 있습니다.";
                        /*
						lb_exist_pay_msg2.Text = "추가로 결연하신 어린이의 후원금도 다음 월부터 지로로 납부하시게 됩니다."
									+ "<br />후원금을 보다 편리하게 납부하실 수 있는 자동이체를 신청하시려면 [마이페이지 - 후원관리 - 납부방법 변경] 메뉴를 이용하시거나<Br/> 한국컴패션으로 전화주세요! (전화 : 02-740-1000)";
									*/
                        lb_exist_pay_msg2.Text = "기존 정보에 추가로 결제금액을 등록합니다.";
                    }
                }
                #endregion
            }

         
        return true;

    }

    // DAT 사용자정보
    protected bool GetUserDatInfo( bool isRegular ) {

		if(!UserInfo.IsLogin) {
			return false;
		}
		
		DataSet dsSponsor = new DataSet();
		SponsorAction sponsorAction = new SponsorAction();
		JsonWriter actionResult;

		#region //국가
		if(isRegular) {

			actionResult = new CodeAction().Countries();
			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message, "goBack()");
				return false;
			}

			ddlHouseCountry.DataSource = actionResult.data;
			ddlHouseCountry.DataTextField = "CodeName";
			ddlHouseCountry.DataValueField = "CodeName";
			ddlHouseCountry.DataBind();


		}
		#endregion

		var sess = new UserInfo();
		
		actionResult = sponsorAction.GetSponsor();
		if(!actionResult.success) {
			if (actionResult.action != "not_registered") {
				base.AlertWithJavascript(actionResult.message, "goBack()");
			}
			return false;
		}

		SponsorAction.SponsorItem sponsorItem = (SponsorAction.SponsorItem)actionResult.data;

		
		#region 기본정보

		if(string.IsNullOrEmpty(sponsorItem.UserClass)){
			base.AlertWithJavascript("회원님의 가입유형이 존재하지 않습니다. \\r\\n관리자에게 문의해주세요.", "goBack()");
			return false;
		}

		//후원자구분
		hdState.Value = sponsorItem.UserClass;
		
		hdManageType.Value = sponsorItem.ManageType;
		if(hdManageType.Value == "")
			hdManageType.Value = "0";
		var manageType = hdManageType.Value;

		//주민번호
		if(sponsorItem.UserClass == "기업" || sponsorItem.UserClass == "기타") {
		} else {
			var ssn = sponsorItem.JuminID;
			jumin.Value = ssn;

		}


		gender.Value = sponsorItem.GenderCode;
		hdCI.Value = ci.Value = sponsorItem.CI;
		di.Value = sponsorItem.DI;
		cert_gb.Value = sponsorItem.CertifyOrgan;
        /*
		if(motiveCode != null)
			motiveCode.Value = sponsorItem.MotiveCode.ValueIfNull("M0048");
		if(motiveName != null)
			motiveName.Value = sponsorItem.MotiveCodeName.ValueIfNull("컴패션 홈페이지, 검색");
            */

		hdLocation.Value = sponsorItem.LocationType;
		
		if(isRegular) {
			
			first_name.Value = sponsorItem.FirstName;
			last_name.Value = sponsorItem.LastName;
			if(parent_name != null) {
				/*
				parent_name.Value = sponsorItem.ParentName;
				parent_juminId.Value = sponsorItem.ParentJuminID;
				parent_mobile.Value = sponsorItem.ParentMobile;
				parent_email.Value = sponsorItem.ParentEmail;
				*/
			}
			var religionType = sponsorItem.ReligionType;
			if(religionType == "기독교" || religionType == "") {
				religion1.Checked = true;
				church_name.Value = sponsorItem.ChurchName;

				if(church_name.Value != null) {
					actionResult = sponsorAction.GetChurch();
					if(!actionResult.success) {
						base.AlertWithJavascript(actionResult.message, "goBack()");
						return false;
					}

					SponsorAction.ChurchResult church_data = (SponsorAction.ChurchResult)actionResult.data;
					hdOrganizationID.Value = church_data.organizationId;
				}

			} else if(religionType == "천주교") {
				religion2.Checked = true;
			} else if(religionType == "불교") {
				religion3.Checked = true;
			} else {
				religion4.Checked = true;
			}
	
			translationYN.Value = (sponsorItem.TranslationFlag == "Y" ) ? "Y" : "N";

			// 후원정보 수신
			actionResult = sponsorAction.GetReceiveSponsorInfo();
			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message);
				return false;
			}

			dmYN.Value = (bool)actionResult.data ? "Y" : "N";
			
		}

		#endregion
		
		#region 연말정산 거부정보
		
		// 연말정산 영수증
		actionResult = sponsorAction.IsPayReceipt();
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message);
			return false;
		}

		p_receipt_pub_ok.Checked = (bool)actionResult.data;
		
		#endregion

		if(isRegular) {

			#region 주소정보
			actionResult = sponsorAction.GetAddress();
			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message, "goBack()");
				return false;
			}

			SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)actionResult.data;
			zipcode.Value = addr_data.Zipcode;
			addr1.Value = addr_data.Addr1;
			addr2.Value = addr_data.Addr2;
			hfAddressType.Value = addr_data.AddressType;
			ddlHouseCountry.SelectedValue = addr_data.Country;

            if (dspAddrDoro != null)
            dspAddrDoro.Value = addr_data.DspAddrDoro;
            if (dspAddrJibun != null)
                dspAddrJibun.Value = addr_data.DspAddrJibun;

            if (!addr_data.ExistAddress) {

				if(pn_addr_domestic != null)
					pn_addr_domestic.Style["display"] = "table-row";
			}
			#endregion

			if(ph_new_pay != null) {
				#region 기존 납부방법 조회
				exist_account.Value = "N";

				DataSet dsPaymentType = null;
				try {
					dsPaymentType = _wwwService.selectUsingPaymentAccount(sess.SponsorID, "");
				} catch(Exception ex) {
					ErrorLog.Write(this.Context, 0, ex.Message);
					base.AlertWithJavascript("납부방법을 가져오는 중 오류가 발생했습니다.", "goBack()");
					return false;
				}

				if(dsPaymentType.Tables[0].Rows.Count > 0) {
				
					// 자동결제 정보 
					this.ViewState["paymentAccount"] = dsPaymentType;

					var paymentName = dsPaymentType.Tables[0].Rows[0]["PaymentName"].ToString().Trim();
					var bankName = dsPaymentType.Tables[0].Rows[0]["BankName"].ToString().Trim();

					//if(paymentName == "지로" || paymentName == "해외카드" ) {
					if(paymentName == "해외카드") {     // CMS 첫신청인 경우 지로로 등록되는 로직이 있다.
						if (ph_payment_method_cms != null)
							ph_payment_method_cms.Visible = ph_payment_method_card.Visible = false;
						return true;
					}
					ph_new_pay.Visible = false;
					ph_exist_pay.Visible = ph_exist_pay2.Visible = true;
					exist_account.Value = "Y";

					if(paymentName == "신용카드자동결제") {
						if(payment_method_card != null)
							payment_method_card.Checked = true;
						lb_exist_pay_msg1.Text = "기존에 <em class='fc_blue'>등록하신 신용카드 자동이체 결제정보</em>가 있습니다."; //결제방법                    
						lb_exist_pay_msg2.Text = "기존 정보에 추가로 결제금액을 등록합니다.";

					} else if(paymentName.ToUpper() == "CMS") {
						if(payment_method_cms != null)
							payment_method_cms.Checked = true;
						lb_exist_pay_msg1.Text = "기존에 <em class='fc_blue'>등록하신 CMS자동이체 결제정보</em>가 있습니다.";
						lb_exist_pay_msg2.Text = "기존 정보에 추가로 결제금액을 등록합니다.";

					} else if(paymentName == "가상계좌") {
						if(payment_method_virtualaccount != null)
							payment_method_virtualaccount.Checked = true;
						lb_exist_pay_msg1.Text = "후원자님은 현재 <em class='fc_blue'>가상계좌</em>로 후원금을 납부하고 계시며,";
						lb_exist_pay_msg2.Text = "기존 정보에 추가로 결제금액을 등록합니다.";
						if (lt_exist_pay2 != null)
                            lt_exist_pay2.Text = "아래 결제 버튼을 누르시면 다음달부터 가상계좌로 납부하실 수 있습니다.";
					} else if(paymentName == "미주") {
						lb_exist_pay_msg1.Text = "후원자님은 현재 매월 <em class='fc_blue'>미주</em>로 후원금을 납부하고 계십니다.";
						lb_exist_pay_msg2.Text = "미주 사무실로 연락 부탁 드립니다.";
					} else if(paymentName == "지로") {
						if(payment_method_giro != null)
							payment_method_giro.Checked = true;
						lb_exist_pay_msg1.Text = "후원자님은 현재 <em class='fc_blue'>지로</em>로 후원금을 납부하고 계시며,";
						if(lt_exist_pay2 != null)
                            lt_exist_pay2.Text = "아래 결제 버튼을 누르시면 다음달부터 지로로 납부하실 수 있습니다.";
						/*
						lb_exist_pay_msg2.Text = "추가로 결연하신 어린이의 후원금도 다음 월부터 지로로 납부하시게 됩니다."
									+ "<br />후원금을 보다 편리하게 납부하실 수 있는 자동이체를 신청하시려면 [마이페이지 - 후원관리 - 납부방법 변경] 메뉴를 이용하시거나<Br/> 한국컴패션으로 전화주세요! (전화 : 02-740-1000)";
									*/
						lb_exist_pay_msg2.Text = "기존 정보에 추가로 결제금액을 등록합니다.";
					}
				}
				#endregion
			}

		}
		return true;

	}

	void RenderControl() {

		if(jumin != null && ph_no_jumin != null && ph_yes_jumin != null) {
			if(string.IsNullOrEmpty(jumin.Value)) {
				ph_no_jumin.Visible = true;
			} else {
				ph_yes_jumin.Visible = true;
			}
		}

		if(ph_cert != null ) {
			if(string.IsNullOrEmpty(ci.Value)) {
				ph_cert.Visible = true;
			} else {
				ph_cert.Visible = false;

			}
		}

		// 2016.09.12 비회원은 본인인증 안함
		if(!UserInfo.IsLogin) {
			if(ph_cert != null) {
				ph_cert.Visible = false;
			}
		}

		if(hdLocation.Value == "국내" || !UserInfo.IsLogin) {
			addr_overseas.Checked = false;
			if(pn_addr_domestic != null)
				pn_addr_domestic.Style["display"] = "table-row";
		} else {
			addr_overseas.Checked = true;
			if(pn_addr_overseas != null)
				pn_addr_overseas.Style["display"] = "table-row";

		}
	}
	
	// 정기결제시 폼 유효성 검사
	protected bool UpdateRegularUserInfo() {

		this.InitRegularControl();

		this.ViewState["motiveCode"] = motiveCode.Value;
		this.ViewState["motiveName"] = motiveName.Value;

		if(!UserInfo.IsLogin) {
			base.AlertWithJavascript("정기후원은 로그인 후 이용할 수 있습니다.");
			return false;
		}

		var sess = new UserInfo();
		SponsorAction sponsorAction = new SponsorAction();
		JsonWriter actionResult;

		var sSponsorID = sess.SponsorID;
		var skip_receipt = hdState.Value == "기업" || hdState.Value == "기타";	// 현금영수증 발행 무시하는 경우

		#region 0. 유효성검증
		// 영문이름
		if(string.IsNullOrEmpty(first_name.Value) || string.IsNullOrEmpty(last_name.Value)) {
			base.AlertWithJavascript("영문이름을 입력해 주세요");
			return false;
		}

		// 주소
		if(string.IsNullOrEmpty(zipcode.Value) || string.IsNullOrEmpty(addr1.Value) || string.IsNullOrEmpty(addr2.Value)) {
			base.AlertWithJavascript("주소를 입력해 주세요");
			return false;
		}

		#region 국내거주인경우
		if(!addr_overseas.Checked) {
			
			// 본인인증체크
			if(hdState.Value != "기업" && hdState.Value != "기타") {
				if(string.IsNullOrEmpty(cert_gb.Value)) {
					base.AlertWithJavascript("결제를 위해서 본인인증이 필요합니다.");
					return false;
				}

				// 연말정산 체크
				if(p_receipt_pub_ok.Checked && (string.IsNullOrEmpty(ci.Value) || string.IsNullOrEmpty(di.Value) || string.IsNullOrEmpty(jumin.Value))) {
					base.AlertWithJavascript("연말정산영수증 신청을 위해서는 주민번호 인증이 필요합니다.");
					return false;
				}

			}

			// 보호자 동의체크
			if(hdState.Value != "기업" && hdState.Value != "기타" && (user_class.Value == "14세미만" || user_class.Value == "19세미만")) {
				if(string.IsNullOrEmpty(parent_juminId.Value)) {
					base.AlertWithJavascript("결제를 위해서 보호자 동의가 필요합니다.");
					return false;
				}
			}

			if(!string.IsNullOrEmpty(ci.Value)) {
				DataSet dsSponsor = _www6Service.NTx_ExecuteQuery("SqlCompass4", new object[1] { "SELECT * FROM tSponsorMaster WHERE CurrentUse = 'Y' and sponsorId <> '" + sess.SponsorID + "' and ci = '" + ci.Value + "'" }, "Text", null, null);
				if(dsSponsor.Tables[0].Rows.Count > 0) {

                    // CI가 동일한 회원중에 웹회원가입된 이력이 있으면,
                    using (AuthDataContext dao = new AuthDataContext())
                    {
                        //if(dao.tSponsorMaster.Any(p => p.SponsorID == sess.SponsorID)) {
                        Object[] op1 = new Object[] { "UserID", "UserPW", "CurrentUse", "SponsorID", "SponsorName", "Email", "Mobile" };
                        Object[] op2 = new Object[] { "", "", "Y", sess.SponsorID, "", "", "" };
                        var list = www6.selectSPAuth("sp_tSponsorMaster_get_f", op1, op2).DataTableToList<CommonLib.sp_tSponsorMaster_get_fResult>();

                        //if (dao.sp_tSponsorMaster_get_f("", "", "Y", sess.SponsorID, "", "", "").Count() > 0)
                        if(list.Count > 0)
                        {
                            var userId = dsSponsor.Tables[0].Rows[0]["userid"].ToString().Mask("*", 3);
                            base.AlertWithJavascript(string.Format("이미 인증된 회원정보가 있습니다. (아이디 : {0}). \\n해당 회원으로 로그인하시어 결제 진행해 주십시요.", userId), "changeLogin()");
                            return false;
                        }
                        else
                        {
                            base.AlertWithJavascript(string.Format("이미 후원회원으로 등록된 인증정보가 있습니다. 관리자에게 문의해 주세요. 스폰서아이디 : {0}", sess.SponsorID));
                            return false;
                        }
                    }

				}

			}
		}
		#endregion

		#endregion

		#region 1. 폼값 세팅
		var juminID = jumin.Value;
		var translateYN = translationYN.Value;
		var religion = "";
		if(religion1.Checked)
			religion = religion1.Value;
		else if(religion2.Checked)
			religion = religion2.Value;
		else if(religion3.Checked)
			religion = religion3.Value;
		else if(religion4.Checked)
			religion = religion4.Value;
		//string sResult;
		string sChurchName = "";
		string sOrganizationID = "";

		if(!religion1.Checked) {
			church_name.Value = "";
			sChurchName = "";
		} else {
			if(church_name.Value.Trim().Length > 0 && hdOrganizationID.Value.ToString().Trim().Length > 0) {
				sChurchName = church_name.Value.Trim();
				sOrganizationID = hdOrganizationID.Value.ToString().Trim();
			} else if(church_name.Value.Trim().Length > 0)
				sChurchName = church_name.Value.Trim();

		}

		#endregion

		bool? is_receipt = null;
		if (!skip_receipt)
			is_receipt = p_receipt_pub_ok.Checked;
		
		if(string.IsNullOrEmpty(sSponsorID)) {
			// 실명인증,본인인증은 ajax 로 처리되기때문에 ci ,di , jumin 값은 넘기지 않는다.
			actionResult = sponsorAction.AddSponsor("", "", "" , sess.GroupNo, translateYN, religion, sChurchName, sOrganizationID, motiveCode.Value, motiveName.Value, "", first_name.Value, last_name.Value, addr_overseas.Checked ? "국외" : "국내" ,
				ddlHouseCountry.SelectedValue, zipcode.Value, addr1.Value, addr2.Value, parent_name.Value, parent_juminId.Value, parent_mobile.Value, parent_email.Value,
				null, is_receipt, dmYN.Value == "Y"
				);

			if(actionResult.success) {
				sSponsorID = (string)actionResult.data;
				this.ViewState["sponsorId"] = sSponsorID;
			}

		} else {
			
			actionResult = sponsorAction.UpdateSponsor(sSponsorID, first_name.Value, last_name.Value, null, translateYN, null, null, null, addr_overseas.Checked ? "국외" : "국내" , ddlHouseCountry.SelectedValue,
				hfAddressType.Value, zipcode.Value, addr1.Value, addr2.Value, religion, sChurchName, sOrganizationID , motiveCode.Value , motiveName.Value, null, is_receipt , dmYN.Value == "Y");
		}

		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message, "");
			return false;
		}
		
		return true;
	}

	// 일시후원
	// 일시결제 폼 컨트롤 초기화
	void InitTemporaryControl() {

		noname = (HtmlInputCheckBox)Page.FindAnyControl("noname");
		hd_auth_domain = (HtmlInputHidden)Page.FindAnyControl("hd_auth_domain");
		hdSponsorId = (HtmlInputHidden)Page.FindAnyControl("hdSponsorId");
        user_name = (HtmlInputText)Page.FindAnyControl("user_name");
		hd_age = (HtmlInputHidden)Page.FindAnyControl("hd_age");
		user_class = (HtmlInputHidden)Page.FindAnyControl("user_class");
		ph_no_jumin = (PlaceHolder)Page.FindAnyControl("ph_no_jumin");
		ph_cert = (PlaceHolder)Page.FindAnyControl("ph_cert");
		ph_cert_me = (HtmlGenericControl)Page.FindAnyControl("ph_cert_me");


		hdBirthDate = (HtmlInputHidden)Page.FindAnyControl("hdBirthDate");
		hdState = (HtmlInputHidden)Page.FindAnyControl("hdState");
		hdManageType = (HtmlInputHidden)Page.FindAnyControl("hdManageType");
		jumin = (HtmlInputHidden)Page.FindAnyControl("jumin");
		motiveCode = (HtmlInputHidden)Page.FindAnyControl("motiveCode");
		motiveName = (HtmlInputHidden)Page.FindAnyControl("motiveName");
		hfAddressType = (HtmlInputHidden)Page.FindAnyControl("hfAddressType");

		gender = (HtmlInputHidden)Page.FindAnyControl("gender");
		hdCI = (HtmlInputHidden)Page.FindAnyControl("hdCI");
		ci = (HtmlInputHidden)Page.FindAnyControl("ci");
		di = (HtmlInputHidden)Page.FindAnyControl("di");
		cert_gb = (HtmlInputHidden)Page.FindAnyControl("cert_gb");


		ph_yes_jumin = (PlaceHolder)Page.FindAnyControl("ph_yes_jumin");
		ph_no_jumin = (PlaceHolder)Page.FindAnyControl("ph_no_jumin");

		hdLocation = (HtmlInputHidden)Page.FindAnyControl("hdLocation");
		
		addr_overseas = (HtmlInputCheckBox)Page.FindAnyControl("addr_overseas");
		
		p_receipt_pub_ok = (HtmlInputCheckBox)Page.FindAnyControl("p_receipt_pub_ok");

		payment_method_card = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_card");
		payment_method_cms = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_cms");
		payment_method_oversea = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_oversea");
		payment_method_phone = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_phone");
    }


    void InitAgainControl()
    {

        noname = (HtmlInputCheckBox)Page.FindAnyControl("noname");
        hd_auth_domain = (HtmlInputHidden)Page.FindAnyControl("hd_auth_domain");
        hdSponsorId = (HtmlInputHidden)Page.FindAnyControl("hdSponsorId");
        user_name = (HtmlInputHidden)Page.FindAnyControl("user_name");
        hd_age = (HtmlInputHidden)Page.FindAnyControl("hd_age");
        user_class = (HtmlInputHidden)Page.FindAnyControl("user_class");
        ph_no_jumin = (PlaceHolder)Page.FindAnyControl("ph_no_jumin");
        ph_cert = (PlaceHolder)Page.FindAnyControl("ph_cert");
        ph_cert_me = (HtmlGenericControl)Page.FindAnyControl("ph_cert_me");


        hdBirthDate = (HtmlInputHidden)Page.FindAnyControl("hdBirthDate");
        hdState = (HtmlInputHidden)Page.FindAnyControl("hdState");
        hdManageType = (HtmlInputHidden)Page.FindAnyControl("hdManageType");
        jumin = (HtmlInputHidden)Page.FindAnyControl("jumin");
        motiveCode = (HtmlInputHidden)Page.FindAnyControl("motiveCode");
        motiveName = (HtmlInputHidden)Page.FindAnyControl("motiveName");
        hfAddressType = (HtmlInputHidden)Page.FindAnyControl("hfAddressType");

        gender = (HtmlInputHidden)Page.FindAnyControl("gender");
        hdCI = (HtmlInputHidden)Page.FindAnyControl("hdCI");
        ci = (HtmlInputHidden)Page.FindAnyControl("ci");
        di = (HtmlInputHidden)Page.FindAnyControl("di");
        cert_gb = (HtmlInputHidden)Page.FindAnyControl("cert_gb");


        ph_yes_jumin = (PlaceHolder)Page.FindAnyControl("ph_yes_jumin");
        ph_no_jumin = (PlaceHolder)Page.FindAnyControl("ph_no_jumin");

        hdLocation = (HtmlInputHidden)Page.FindAnyControl("hdLocation");

        addr_overseas = (HtmlInputCheckBox)Page.FindAnyControl("addr_overseas");

        p_receipt_pub_ok = (HtmlInputCheckBox)Page.FindAnyControl("p_receipt_pub_ok");

        payment_method_card = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_card");
        payment_method_cms = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_cms");
        payment_method_oversea = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_oversea");
        payment_method_phone = (HtmlInputRadioButton)Page.FindAnyControl("payment_method_phone");
    }

    protected void OnBeforePostBackTemporary() {
		if(!PayItemSession.HasCookie(this.Context)) {
			Response.Redirect("/");
			return;
		}

		var payInfo = PayItemSession.GetCookie(this.Context);
		if(payInfo.frequency == "정기") {
			//Response.Redirect("/sponsor/pay/regular/");
			Response.Redirect("/");
			return;
		}

		this.OnBeforePostBackTemporary(payInfo);
	}

	protected void OnBeforePostBackTemporary( PayItemSession.Entity payInfo ) {

		this.ViewState["returnUrl"] = "";
		this.ViewState["site_cd"] = "";
		this.ViewState["used_card_YN"] = "N";
		this.ViewState["used_card"] = "";
		this.ViewState["overseas_card"] = "N";
		this.ViewState["pay_method"] = "";
		this.ViewState["jumin"] = "";
		this.ViewState["ci"] = "";
		this.ViewState["di"] = "";
		this.ViewState["good_name"] = "한국컴패션후원금";
		this.ViewState["good_mny"] = "";

		this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];


		this.InitTemporaryControl();

		hd_auth_domain.Value = this.ViewState["auth_domain"].ToString();
		

	

		this.ViewState["campaignId"] = payInfo.campaignId;
		this.ViewState["codeId"] = payInfo.codeId;
		this.ViewState["codeName"] = payInfo.codeName;

		this.GetUserInfo();

		this.GetUserDatInfo(false);

		this.RenderControl();

	}

    protected bool UpdateTemporaryUserInfo()
    {

        this.InitTemporaryControl();

        JsonWriter result = new JsonWriter();
        SponsorAction sponsorAction = new SponsorAction();
        JsonWriter actionResult;

        if (UserInfo.IsLogin)
        {

            var sess = new UserInfo();
            var sSponsorID = sess.SponsorID;
            var skip_receipt = hdState.Value == "기업" || hdState.Value == "기타";  // 현금영수증 발행 무시하는 경우

            #region 유효성검증

            if (!addr_overseas.Checked)
            {

                // 본인인증체크
                if (hdState.Value != "기업" && hdState.Value != "기타")
                {
                    if (string.IsNullOrEmpty(cert_gb.Value))
                    {
                        base.AlertWithJavascript("결제를 위해서 본인인증이 필요합니다.");
                        return false;
                    }

                    // 연말정산 체크
                    if (p_receipt_pub_ok.Checked && (string.IsNullOrEmpty(ci.Value) || string.IsNullOrEmpty(di.Value) || string.IsNullOrEmpty(jumin.Value)))
                    {
                        base.AlertWithJavascript("연말정산영수증 신청을 위해서는 주민번호 인증이 필요합니다.");
                        return false;
                    }
                }

                // 무통장/비회원은 인증 안받음
                if (!string.IsNullOrEmpty(ci.Value))
                {
                    actionResult = new SponsorAction().CheckSponsor(ci.Value);

                    if (!actionResult.success)
                    {
                        var script = "";
                        if (actionResult.action == "login")
                            script = "changeLogin()";

                        base.AlertWithJavascript(actionResult.message, script);
                    }
                }

                /*
				
			Int64 iTimeStamp = Convert.ToInt64(_wwwService.GetDate().ToString("yyyyMMddHHmmssff2"));

				if(!string.IsNullOrEmpty(ci.Value)) {
					DataSet dsSponsor = _www6Service.NTx_ExecuteQuery("SqlCompass4", new object[1] { "SELECT * FROM tSponsorMaster WHERE CurrentUse = 'Y' and UserClass <> '비회원' and sponsorId <> '" + sess.SponsorID + "' and ci = '" + ci.Value + "'" }, "Text", null, null);
					if(dsSponsor.Tables[0].Rows.Count > 0) {

						// CI가 동일한 회원중에 웹회원가입된 이력이 있으면,
						using(AuthDataContext dao = new AuthDataContext()) {
							if(dao.tSponsorMaster.Any(p => p.SponsorID == sess.SponsorID)) {
								var userId = dsSponsor.Tables[0].Rows[0]["userid"].ToString().Mask("*", 3);
								base.AlertWithJavascript(string.Format("이미 인증된 회원정보가 있습니다. (아이디 : {0}). \\n해당 회원으로 로그인하시어 결제 진행해 주십시요.", userId), "changeLogin()");
								return false;
							} else {
								base.AlertWithJavascript(string.Format("이미 후원회원으로 등록된 인증정보가 있습니다. 관리자에게 문의해 주세요. 스폰서아이디 : {0}", sess.SponsorID));
								return false;
							}
						}

					}
				}
				*/

            }
            #endregion

            actionResult = sponsorAction.UpdateLocation(addr_overseas.Checked ? "국외" : "국내");
            if (!actionResult.success)
            {
                base.AlertWithJavascript(actionResult.message, "");
                return false;
            }

            bool? is_receipt = null;
            if (!skip_receipt)
                is_receipt = p_receipt_pub_ok.Checked;

            if (string.IsNullOrEmpty(sSponsorID))
            {
                // 실명인증,본인인증은 ajax 로 처리되기때문에 ci ,di , jumin 값은 넘기지 않는다.
                actionResult = sponsorAction.AddSponsor("", "", "", sess.GroupNo, "", "", "", "", null, null, "", "", "", addr_overseas.Checked ? "국외" : "국내",
                    "", "", "", "", parent_name != null ? parent_name.Value : "", parent_juminId != null ? parent_juminId.Value : "", parent_mobile != null ? parent_mobile.Value : "", parent_email != null ? parent_email.Value : "",
                    null, is_receipt, null
                    );

            }
            else
            {

                actionResult = sponsorAction.UpdateSponsor(sSponsorID, null, null, null, null, null, null, null, addr_overseas.Checked ? "국외" : "국내", null,
                    null, null, null, null, null, null, null, null, null, null, is_receipt, null);
            }

            if (!actionResult.success)
            {
                base.AlertWithJavascript(actionResult.message, "");
                return false;
            }



        }

        return true;
    }

    // 일시결제시 폼 유효성 검사
    protected bool UpdateAgainUserInfo() {

		this.InitAgainControl();

		JsonWriter result = new JsonWriter();
		SponsorAction sponsorAction = new SponsorAction();
		JsonWriter actionResult;

		if(UserInfo.IsLogin) {

			var sess = new UserInfo();
			var sSponsorID = sess.SponsorID;
			var skip_receipt = hdState.Value == "기업" || hdState.Value == "기타";  // 현금영수증 발행 무시하는 경우

			#region 유효성검증

			if(!addr_overseas.Checked) {
			
				// 본인인증체크
				if(hdState.Value != "기업" && hdState.Value != "기타") {
					if(string.IsNullOrEmpty(cert_gb.Value)) {
						base.AlertWithJavascript("결제를 위해서 본인인증이 필요합니다.");
						return false;
					}

					// 연말정산 체크
					if(p_receipt_pub_ok.Checked && (string.IsNullOrEmpty(ci.Value) || string.IsNullOrEmpty(di.Value) || string.IsNullOrEmpty(jumin.Value))) {
						base.AlertWithJavascript("연말정산영수증 신청을 위해서는 주민번호 인증이 필요합니다.");
						return false;
					}
				}

				// 무통장/비회원은 인증 안받음
				if(!string.IsNullOrEmpty(ci.Value)) {
					actionResult = new SponsorAction().CheckSponsor(ci.Value);

					if(!actionResult.success) {
						var script = "";
						if(actionResult.action == "login")
							script = "changeLogin()";

						base.AlertWithJavascript(actionResult.message, script);
					}
				}

				/*
				
			Int64 iTimeStamp = Convert.ToInt64(_wwwService.GetDate().ToString("yyyyMMddHHmmssff2"));

				if(!string.IsNullOrEmpty(ci.Value)) {
					DataSet dsSponsor = _www6Service.NTx_ExecuteQuery("SqlCompass4", new object[1] { "SELECT * FROM tSponsorMaster WHERE CurrentUse = 'Y' and UserClass <> '비회원' and sponsorId <> '" + sess.SponsorID + "' and ci = '" + ci.Value + "'" }, "Text", null, null);
					if(dsSponsor.Tables[0].Rows.Count > 0) {

						// CI가 동일한 회원중에 웹회원가입된 이력이 있으면,
						using(AuthDataContext dao = new AuthDataContext()) {
							if(dao.tSponsorMaster.Any(p => p.SponsorID == sess.SponsorID)) {
								var userId = dsSponsor.Tables[0].Rows[0]["userid"].ToString().Mask("*", 3);
								base.AlertWithJavascript(string.Format("이미 인증된 회원정보가 있습니다. (아이디 : {0}). \\n해당 회원으로 로그인하시어 결제 진행해 주십시요.", userId), "changeLogin()");
								return false;
							} else {
								base.AlertWithJavascript(string.Format("이미 후원회원으로 등록된 인증정보가 있습니다. 관리자에게 문의해 주세요. 스폰서아이디 : {0}", sess.SponsorID));
								return false;
							}
						}

					}
				}
				*/

			}
			#endregion

			actionResult = sponsorAction.UpdateLocation(addr_overseas.Checked ? "국외" : "국내");
			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message, "");
				return false;
			}

			bool? is_receipt = null;
			if(!skip_receipt)
				is_receipt = p_receipt_pub_ok.Checked;

			if(string.IsNullOrEmpty(sSponsorID)) {
				// 실명인증,본인인증은 ajax 로 처리되기때문에 ci ,di , jumin 값은 넘기지 않는다.
				actionResult = sponsorAction.AddSponsor("", "", "" ,sess.GroupNo, "", "" , "" , "", null , null, "", "", "" , addr_overseas.Checked ? "국외" : "국내",
					"" , "" , "" , "" , parent_name != null ? parent_name.Value : "", parent_juminId != null ? parent_juminId.Value : "", parent_mobile != null ? parent_mobile.Value : "", parent_email != null ? parent_email.Value : "",
					null, is_receipt, null
					);

			} else {
				
				actionResult = sponsorAction.UpdateSponsor(sSponsorID, null , null, null, null, null, null, null, addr_overseas.Checked ? "국외" : "국내", null ,
					null ,null , null , null , null , null , null, null, null, null, is_receipt, null);
			}

			if(!actionResult.success) {
				base.AlertWithJavascript(actionResult.message, "");
				return false;
			}



		}

		return true;
	}

	// 일시결제 
	protected void SetTemporaryPaymentInfo() {

		this.InitTemporaryControl();


		if(noname != null && noname.Checked) {
			jumin.Value = "";
			ci.Value = "";
			di.Value = "";
			user_name.Value = "";
		}
		
		this.ViewState["jumin"] = jumin.Value;
		this.ViewState["ci"] = ci.Value;
		this.ViewState["di"] = di.Value;

		this.ViewState["used_card_YN"] = "Y";
		this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_domestic"];

		if(payment_method_card.Checked) {
			this.ViewState["pay_method"] = "100000000000";
			this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card"];

		} else if(payment_method_cms.Checked) {
			this.ViewState["pay_method"] = "010000000000";
			this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_account"];

		} else if(payment_method_phone != null && payment_method_phone.Checked) {
			this.ViewState["pay_method"] = "000010000000";
			this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_phone"];

		} else if(payment_method_oversea.Checked) {
			this.ViewState["pay_method"] = "100000000000";
			this.ViewState["overseas_card"] = "Y";
			this.ViewState["used_card_YN"] = "Y";
			this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_oversea"];
			this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card_Foreign"];
		}
	}

    protected void RegistCommitmentTemp(string sponsorId, string sponsorName, string childMasterId)
    {
        CommitmentID = string.Empty;
        SponsorID = sponsorId;
        ChildMasterID = childMasterId;

        ErrorLog.Write(HttpContext.Current, 0, "RegistCommitmentTemp start " + sponsorId + " " + sponsorName + " " + childMasterId);

        // CommitmentID 조회
        ChildAction child = new ChildAction();
        var childInfo = child.GetChild(ChildMasterID);

        if (childInfo.success)
        {
            ChildAction.ChildItem item = (ChildAction.ChildItem)childInfo.data;
            string childKey = item.ChildKey;

            var childDetail = child.MyChildren(childKey, 1, 1);
            if (childDetail.success)
            {
                List<ChildAction.MyChildItem> detail = (List<ChildAction.MyChildItem>)childDetail.data;
                if (detail == null || detail.Count == 0)
                {
                    ErrorLog.Write(HttpContext.Current, 0, "어린이 결연 정보 조회에 실패했습니다. " + sponsorId + " " + sponsorName + " " + childMasterId);
                }
                else
                    CommitmentID = detail[0].commitmentId;
            }
            else
            {
                ErrorLog.Write(HttpContext.Current, 0, "어린이 결연 정보 조회에 실패했습니다. " + sponsorId + " " + sponsorName + " " + childMasterId);
            }
        }
        else
        {
            ErrorLog.Write(HttpContext.Current, 0, "어린이 정보 조회에 실패했습니다. " + sponsorId + " " + sponsorName + " " + childMasterId);
        }

        if (!string.IsNullOrEmpty(CommitmentID))
        {
            DoStuff myAction = new DoStuff(DoTCPT);
            myAction.BeginInvoke(null, null);
        }
    }

    private void DoTCPT()
    {
        bool regGlobalID = false;
        bool updateHold = false;
        //string sponsorid = context.Request["sponsorid"].EmptyIfNull().EscapeSqlInjection();
        //string childMasterId = context.Request["childmasterid"].EmptyIfNull().EscapeSqlInjection();
        // 문희원 테스트용 추가
        string test = "";
        try
        {
            TCPTService.Service svc = new TCPTService.Service();
            svc.Timeout = 180000;

            test += "결연정보 조회 / ";
            //결연정보 조회
            DataSet ds = svc.GetCommitmentInfoForTCPT(CommitmentID, CommitmentID);

            if (ds == null || ds.Tables.Count == 0)
            {
                test += "결연정보 조회 실패 / ";
            }
            else
            {
                //결연정보 조회되지 않음
                if (ds.Tables[0].Rows.Count == 0)
                {
                    test += "결연정보 조회 되지 않음 / ";
                }
                else
                {
                    test += "결연정보 조회됨 / ";
                    // GlobalSponsorID  생성
                    DataRow dr = ds.Tables[0].Rows[0];

                    bool regSuccess = false;
                    string result = string.Empty;
                    string sponsorID = SponsorID;
                    string childMasterId = dr["ChildMasterID"].ToString();
                    string userid = "";
                    string userName = "";
                    string GlobalSponsorID = string.Empty;
                    string ChildGlobalID = dr["ChildGlobalID"].ToString();
                    //string CommitmentID = dr["CommitmentID"].ToString();
                    string holdID = dr["HoldID"].ToString();
                    string holdUID = dr["HoldUID"].ToString();
                    string holdType = dr["HoldType"].ToString();
                    string conid = "54-" + dr["ConID"].ToString();
                    string firstName = dr["FirstName"].ToString();
                    string lastName = dr["LastName"].ToString();
                    string genderCode = dr["GenderCode"].ToString();

                    if (dr["SponsorGlobalID"].ToString() != "")
                    {
                        GlobalSponsorID = dr["SponsorGlobalID"].ToString();
                        regSuccess = true;
                    }

                    if (genderCode == "M")
                        genderCode = "Male";
                    else if (genderCode == "F")
                        genderCode = "Female";

                    if (string.IsNullOrEmpty(GlobalSponsorID))
                    {
                        test += "Sponsor GlobalID  생성 시작 / ";

                        try
                        {
                            TCPTModel.Request.Supporter.SupporterCreateInSFCI_POST kit = new TCPTModel.Request.Supporter.SupporterCreateInSFCI_POST();
                            TCPTModel.Request.Supporter.SupporterProfile profile = new TCPTModel.Request.Supporter.SupporterProfile();

                            profile.GlobalPartner = "Compassion Korea";
                            profile.CorrespondenceDeliveryPreference = "Digital";
                            profile.FirstName = firstName;
                            profile.LastName = lastName;
                            if (!string.IsNullOrEmpty(genderCode))
                                profile.Gender = genderCode;
                            profile.GPID = "54-" + conid;
                            profile.PreferredName = "";
                            profile.Status = "Active";
                            profile.StatusReason = "New";
                            profile.MandatoryReview = false;

                            kit.SupporterProfile.Add(profile);

                            string json = Newtonsoft.Json.JsonConvert.SerializeObject(kit);
                            result = svc.SupporterCreateInSFCI_POST(json);
                            TCPTModel.TCPTResponseMessage msg = Newtonsoft.Json.JsonConvert.DeserializeObject<TCPTModel.TCPTResponseMessage>(result);
                            if (msg.IsSuccessStatusCode)
                            {
                                test += "Sponsor GlobalID  생성 성공 / ";
                                TCPTModel.Response.Supporter.SupporterProfileResponse_Kit response = Newtonsoft.Json.JsonConvert.DeserializeObject<TCPTModel.Response.Supporter.SupporterProfileResponse_Kit>(msg.RequestMessage.ToString());
                                GlobalSponsorID = response.SupporterProfileResponse[0].GlobalID;

                                test += "Sponsor GlobalID  생성 성공 " + GlobalSponsorID + " / ";
                                test += "Sponsor GlobalID  업데이트 시작 / ";
                                bool b = svc.UpdateSponsorGlobalID(sponsorID, GlobalSponsorID);

                                if (b)
                                {
                                    regGlobalID = true;
                                    test += "Sponsor GlobalID  업데이트 성공 / ";
                                }
                                else
                                {
                                    test += "Sponsor GlobalID  업데이트 실패 / ";
                                }
                            }
                            else
                            {
                                test += "Sponsor GlobalID  생성 실패 " + msg.RequestMessage.ToString() + " / ";
                                //오류
                            }
                        }
                        catch (Exception ex)
                        {
                            test += "Sponsor GlobalID  생성 오류 " + ex.Message + "/ ";
                        }
                    }


                    test += "no money hold 로 업데이트 시작 / ";
                    //  no money hold 로 업데이트
                    //if (!string.IsNullOrEmpty(GlobalSponsorID))
                    //{

                    string endDT = DateTime.Now.AddMonths(3).ToString("yyyy-MM-dd");

                    try
                    {
                        TCPTModel.Request.Hold.Beneficiary.BeneficiaryHoldRequestList hKit = new TCPTModel.Request.Hold.Beneficiary.BeneficiaryHoldRequestList();
                        hKit.Beneficiary_GlobalID = ChildGlobalID;
                        hKit.BeneficiaryState = "No Money Hold";
                        hKit.HoldEndDate = endDT + "T23:59:59Z";
                        hKit.IsSpecialHandling = false;
                        hKit.PrimaryHoldOwner = conid;
                        hKit.GlobalPartner_ID = "KR";
                        hKit.HoldID = holdID;

                        string hJson = Newtonsoft.Json.JsonConvert.SerializeObject(hKit);

                        result = svc.BeneficiaryHoldSingle_PUT(ChildGlobalID, hKit.HoldID, hJson);
                        TCPTModel.TCPTResponseMessage updateResult = Newtonsoft.Json.JsonConvert.DeserializeObject<TCPTModel.TCPTResponseMessage>(result);
                        if (updateResult.IsSuccessStatusCode)
                        {
                            test += "no money hold 로 업데이트 성공 / ";
                            test += "기존 hold  expired  처리 시작 / ";
                            // 기존 hold  expired  처리
                            bool b = svc.UpdateHoldStatus(new Guid(holdUID), holdID, hKit.BeneficiaryState, endDT, "Expired", "2000", "", sponsorID, sponsorID);

                            if (b)
                            {
                                test += "기존 hold  expired  처리 성공 / ";
                            }
                            else
                            {
                                test += "기존 hold  expired  처리 실패 / ";
                            }
                            // 신규  hold insert
                            Guid newHoldUID = Guid.NewGuid();
                            test += "신규 hold insert / ";
                            b = svc.InsertHoldHistory(newHoldUID, holdID, ChildGlobalID, hKit.BeneficiaryState, endDT, sponsorID, "", "2000", "web 어린이 결연");
                            if (b)
                            {
                                test += "신규 hold insert 성공 / ";
                            }
                            else
                            {
                                if (b)
                                {
                                    test += "신규 hold insert 성공 / ";
                                    updateHold = true;
                                }
                                else
                                {
                                    test += "신규 hold insert 실패 / ";
                                }
                            }

                            // TCPT_CommitmentTemp 테이블에 insert
                            test += "TCPT_CommitmentTemp 테이블에 insert / ";
                            string s = svc.InsertTCPT_CommitmentTemp(CommitmentID, sponsorID, ChildMasterID, newHoldUID.ToString(), holdID, sponsorID, "");
                            test += "TCPT_CommitmentTemp 테이블에 insert 결과 : " + s + " / ";

                        }
                        else
                        {
                            // hold update error
                            test += "no money hold 로 업데이트 실패 " + updateResult.RequestMessage.ToString() + " / ";
                        }
                    }
                    catch (Exception ex)
                    {
                        test += "no money hold 로 업데이트 오류 " + ex.Message + " / ";
                    }
                }

            }
            // }
        }
        catch (Exception ex)
        {
            test += "오류 오류 " + ex.Message + " / ";
        }

        ErrorLog.Write(HttpContext.Current, 0, test);
    }
}
