﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Data;

using KCP.PP_CLI_COM.LIB;

// ARS 전화녹취 기능
public partial class KCPBatchPay {

	#region var
	/* - -------------------------------------------------------------------------------- - */
	private string m_strCFG_paygw_url;    // 결제 GW URL
	private string m_strCFG_paygw_port;   // 결제 GW PORT
	private string m_strCFG_key_path;     // KCP 모듈 KEY 경로
	private string m_strCFG_log_path;     // KCP 모듈 LOG 경로
	private string m_strCFG_site_cd;      // 상점 코드
	private string m_strCFG_site_key;     // 상점 키
										  /* - -------------------------------------------------------------------------------- - */
	private string m_strCustIP;
	private string m_strTxCD;
	private string m_strTxMony;
	/* - -------------------------------------------------------------------------------- - */
	protected string req_tx;              // 요청 종류
	protected string pay_method;          // 결제 방법
	protected string ordr_idxx;           // 주문 번호
	protected string good_name;           // 상품 정보
	protected string good_mny;            // 결제 금액
	protected string buyr_name;           // 주문자 이름
	protected string buyr_mail;           // 주문자 E-Mail
	protected string buyr_tel1;           // 주문자 전화번호
	protected string buyr_tel2;           // 주문자 휴대폰번호
	protected string currency;            // 화폐단위 (WON/USD)
										  /* - -------------------------------------------------------------------------------- - */
	protected string tran_cd;             // 트랜잭션 코드
	protected string bSucc;               // DB 작업 성공 여부
										  /* - -------------------------------------------------------------------------------- - */
	protected string mod_type;            // 변경TYPE(승인취소시 필요)
	protected string mod_desc;            // 변경사유
	protected string amount;              // 총 금액
	protected string panc_mod_mny;        // 부분취소 요청금액
	protected string panc_rem_mny;        // 부분취소 가능금액
	protected string mod_mny;             // 부분취소 요청금액
	protected string rem_mny;             // 부분취소 가능금액	
										  /* = -------------------------------------------------------------------------------- = */
	protected string res_cd;              // 결과코드
	protected string res_msg;             // 결과메시지
	protected string tno;                 // 거래번호
										  /* - -------------------------------------------------------------------------------- - */
	protected string card_pay_method;     // 카드 결제 방법 
	protected string card_cd;             // 카드 코드
	protected string card_no;             // 카드 번호
	protected string card_name;           // 카드명
	protected string app_time;            // 승인시간
	protected string app_no;              // 승인번호
	protected string noinf;               // 무이자여부
	protected string quota;               // 할부개월
										  /* - -------------------------------------------------------------------------------- - */
	protected string bt_group_id;         // 배치 그룹 아이디
	protected string bt_batch_key;        // 배치키
										  /* - -------------------------------------------------------------------------------- - */
	protected string m_strResCD;
	protected string m_strResMsg;
	#endregion

	public KCPBatchPay() {

		//	m_strCustIP = HttpContext.Current.Request.ServerVariables.Get("REMOTE_ADDR");

		//	this.m_f__load_env();

	}

	public Result Pay(string group_idxx , string bt_batch_key, string ordr_idxx, string good_name, string good_mny, string buyr_name, string buyr_mail, string buyr_tel1, string buyr_tel2) {

		Result result = new Result() { success = false };
		WWWService.Service _wwwService = new WWWService.Service();

		DataSet dsResult = new DataSet();

		string sReqDate = DateTime.Now.ToString("yyyyMMdd");
		string sReqTime = DateTime.Now.ToString("HHmmss");

		try { 
			ErrorLog.Write(HttpContext.Current, 0, string.Format("KCPBatchPay send : {0},{1},{2},{3},{4},{5},{6},{7},{8}" , group_idxx , ordr_idxx, group_idxx
									, good_name, good_mny, buyr_name
									, sReqDate, sReqTime, bt_batch_key));
		}catch{}

		/*
        ErrorLog.Write(HttpContext.Current, 0, string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", ordr_idxx , ConfigurationManager.AppSettings["kcpgroup_id"]
                                    , good_name, good_mny, buyr_name
                                    , sReqDate, sReqTime, bt_batch_key));

        CommonLib.PAY4Service.ServiceSoapClient pay4 = new CommonLib.PAY4Service.ServiceSoapClient();
        DataSet dsBatchKey = MakePaymentCardBatchDataSet("pay", ordr_idxx, ConfigurationManager.AppSettings["kcpgroup_id"]
                                    , good_name, good_mny, buyr_name
                                    , sReqDate, sReqTime, bt_batch_key
                                    , "", "");


        dsResult = pay4.PaymentCardBatch(dsBatchKey);
        ErrorLog.Write(HttpContext.Current , 0 , "테스트 " + dsResult.ToJson());
        */

		//dsResult = _wwwService.PaymentCardBatch( CodeAction.ChannelType, ConfigurationManager.AppSettings["kcpgroup_id"], buyr_name, ordr_idxx , good_name, good_mny, bt_batch_key);
		dsResult = _wwwService.PaymentCardBatch(CodeAction.ChannelType, group_idxx, buyr_name, ordr_idxx, good_name, good_mny, bt_batch_key);


		//getResult
		result.res_cd = dsResult.Tables[0].Rows[0]["ResCd"].ToString();
		result.res_msg = dsResult.Tables[0].Rows[0]["ResMsg"].ToString();

		try {
			ErrorLog.Write(HttpContext.Current, 0, "KCPBatchPay receive : " + result.ToJson());
		} catch { }

		//===== 결제성공시
		if(result.res_cd == "0000") {
			result.success = true;
			result.app_no = dsResult.Tables[0].Rows[0]["AppNo"].ToString();
			result.app_time = dsResult.Tables[0].Rows[0]["AppDate"].ToString() + dsResult.Tables[0].Rows[0]["AppTime"].ToString();
			result.card_cd = dsResult.Tables[0].Rows[0]["AppCardCd"].ToString();
			result.card_name = dsResult.Tables[0].Rows[0]["AppCardName"].ToString();
			result.noinf = "N";
			result.quota = "00";
			result.tno = dsResult.Tables[0].Rows[0]["OrderTNo"].ToString();
			
		} else {
			
		}

		return result;

		//this.bt_batch_key = bt_batch_key;
		//this.ordr_idxx = ordr_idxx;
		//this.req_tx = "pay";
		//this.pay_method = "CARD";
		//this.good_name = good_name;
		//m_strTxMony = this.good_mny = good_mny;
		//this.buyr_name = buyr_name;
		//buyr_mail = this.buyr_mail;
		//buyr_tel1 = this.buyr_tel1;
		//buyr_tel2 = this.buyr_tel2;
		//currency = "410";

		//card_pay_method = "Batch";
		//quota = "00";

		//C_PP_CLI_COM c_PP_CLI = new C_PP_CLI_COM();
		//int nDataSetInx_req;

		///* -------------------------------------------------------------------------------- */
		///* +    초기화                                                                    + */
		///* - ---------------------------------------------------------------------------- - */
		//m_strTxCD = "";
		//nDataSetInx_req = 0;
		///* - ---------------------------------------------------------------------------- - */
		//c_PP_CLI.m_f__set_env(m_strCFG_paygw_url, m_strCFG_paygw_port,
		//					   m_strCFG_log_path, m_strCFG_key_path);

		///* - ---------------------------------------------------------------------------- - */
		//c_PP_CLI.m_f__init();
		///* -------------------------------------------------------------------------------- */

		///* -------------------------------------------------------------------------------- */
		///* +    요청 처리                                                                 + */
		///* - ---------------------------------------------------------------------------- - */
		//nDataSetInx_req = m_f__set_dataset_pay(ref c_PP_CLI);

		///* - ---------------------------------------------------------------------------- - */
		//if(!m_strTxCD.Equals("")) {
		//	c_PP_CLI.m_f__do_tx(req_tx, m_strTxCD, nDataSetInx_req, m_strCustIP,
		//						 m_strCFG_site_cd, m_strCFG_site_key, ordr_idxx);

		//	m_strResCD = c_PP_CLI.m_strResCD;
		//	m_strResMsg = c_PP_CLI.m_strResMsg;
		//} else {
		//	m_strResCD = "9562";
		//	m_strResMsg = "지불모듈 연동 오류 (TX_CD) 가 정의되지 않았습니다.";
		//}
		///* -------------------------------------------------------------------------------- */

		///* -------------------------------------------------------------------------------- */
		///* +    결과 처리                                                                 + */
		///* - ---------------------------------------------------------------------------- - */
		//if(m_strResCD.Equals("0000")) {

		//	return m_f__disp_rt_pay_succ(ref c_PP_CLI);

		//} else {

		//	return m_f__disp_rt_fail();
		//}

	}
    /* ==================================================================================== */

    public DataSet MakePaymentCardBatchDataSet( string sReqTx, string sOrderIDxx, string sGroupID
                                , string sGoodName, string sGoodMny, string sBuyerName
                                , string sReqDate, string sReqTime, string sBatchKey
                                , string sTno, string sCustIP ) {
        DataTable dtCommonT = new DataTable();
        DataTable dtSslT = new DataTable();
        DataTable dtModT = new DataTable();
        DataSet dsData = new DataSet();


        // DataTable Create
        dtCommonT.TableName = "CommonT";

        dtCommonT.Columns.Add("req_tx", typeof(System.String));
        dtCommonT.Columns.Add("ordr_idxx", typeof(System.String));
        dtCommonT.Columns.Add("good_name", typeof(System.String));
        dtCommonT.Columns.Add("amount", typeof(System.String));

        dtCommonT.Columns.Add("buyr_name", typeof(System.String));
        dtCommonT.Columns.Add("currency", typeof(System.String));
        dtCommonT.Columns.Add("quotaopt", typeof(System.String));
        dtCommonT.Columns.Add("group_idxx", typeof(System.String));

        dtCommonT.Columns.Add("req_date", typeof(System.String));
        dtCommonT.Columns.Add("req_time", typeof(System.String));
        dtCommonT.Columns.Add("batch_key", typeof(System.String));

        dsData.Tables.Add(dtCommonT);


        // DataSet Add
        dsData.Tables["CommonT"].Rows.Add();

        dsData.Tables["CommonT"].Rows[0]["req_tx"] = sReqTx;
        dsData.Tables["CommonT"].Rows[0]["ordr_idxx"] = sOrderIDxx;
        dsData.Tables["CommonT"].Rows[0]["good_name"] = sGoodName;
        dsData.Tables["CommonT"].Rows[0]["amount"] = sGoodMny;

        dsData.Tables["CommonT"].Rows[0]["buyr_name"] = sBuyerName;
        dsData.Tables["CommonT"].Rows[0]["currency"] = "410";
        dsData.Tables["CommonT"].Rows[0]["quotaopt"] = "00";
        dsData.Tables["CommonT"].Rows[0]["group_idxx"] = sGroupID;

        dsData.Tables["CommonT"].Rows[0]["req_date"] = sReqDate;
        dsData.Tables["CommonT"].Rows[0]["req_time"] = sReqTime;
        dsData.Tables["CommonT"].Rows[0]["batch_key"] = sBatchKey;

        // DataTable Create
        dtModT.TableName = "ModT";
        dtModT.Columns.Add("tno", typeof(System.String));
        dtModT.Columns.Add("m_strCustIP", typeof(System.String));
        dsData.Tables.Add(dtModT);


        // DataSet Add
        dsData.Tables["ModT"].Rows.Add();
        dsData.Tables["ModT"].Rows[0]["tno"] = sTno;
        dsData.Tables["ModT"].Rows[0]["m_strCustIP"] = sCustIP;


        return dsData;
    }



    private void m_f__load_env() {

		m_strCFG_paygw_url = ConfigurationManager.AppSettings["g_conf_gw_url"];
		m_strCFG_paygw_port = ConfigurationManager.AppSettings["g_conf_gw_port"];
		m_strCFG_key_path = ConfigurationManager.AppSettings["g_kcp_key_path"];
		m_strCFG_log_path = ConfigurationManager.AppSettings["g_kcp_log_path"];
		m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_batch_pay_site_cd"];
		m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_batch_pay_site_key"];
		bt_group_id = ConfigurationManager.AppSettings["kcpgroup_id"];

	}


	/* ==================================================================================== */
	/* +    METHOD : 망상 취소 처리                                                       + */
	/* - -------------------------------------------------------------------------------- - */
	private Result m_f__do_net_can( ref C_PP_CLI_COM parm_c_PP_CLI ) {
		int nDataSetInx_req;
		bool bDoNetCan = false;

		/* -------------------------------------------------------------------------------- */
		/* +    망상 취소 DATA 설정                                                       + */
		/* - ---------------------------------------------------------------------------- - */
		mod_type = "STSC";
		tno = parm_c_PP_CLI.m_f__get_res("tno");
		/* - ---------------------------------------------------------------------------- - */
		parm_c_PP_CLI.m_f__init();
		/* - ---------------------------------------------------------------------------- - */
		if(req_tx.Equals("pay")) {

			bDoNetCan = true;
			mod_desc = "처리 오류로 인한 거래 자동 취소";
		}
		/* -------------------------------------------------------------------------------- */

		/* -------------------------------------------------------------------------------- */
		/* +    자동 취소 처리                                                            + */
		/* - ---------------------------------------------------------------------------- - */
		if(bDoNetCan == true) {
			nDataSetInx_req = m_f__set_dataset_mod(ref parm_c_PP_CLI);
			parm_c_PP_CLI.m_f__do_tx(req_tx, m_strTxCD, nDataSetInx_req, m_strCustIP,
									  m_strCFG_site_cd, m_strCFG_site_key, ordr_idxx);
			m_strResCD = parm_c_PP_CLI.m_strResCD;
			m_strResMsg = parm_c_PP_CLI.m_strResMsg;

			m_f__disp_rt_can(ref parm_c_PP_CLI);
		}
		/* -------------------------------------------------------------------------------- */

		return new Result() { success = false , res_msg = mod_desc  , res_cd = "" , tno = tno};
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 요청 DATA 생성                                                       + */
	/* - -------------------------------------------------------------------------------- - */
	private int m_f__set_dataset_pay( ref C_PP_CLI_COM parm_c_PP_CLI ) {
		int nDataSetInx_req;
		int nDataSetInx_payx;
		int nDataSetInx_payx_common;
		int nDataSetInx_payx_card;
		int nDataSetInx_ordr;

		/* -------------------------------------------------------------------------------- */
		/* +    적립/조회/사용 요청 DATA 구성                                             + */
		/* - ---------------------------------------------------------------------------- - */
		m_strTxCD = "00100000";
		/* - ---------------------------------------------------------------------------- - */
		nDataSetInx_req = parm_c_PP_CLI.m_f__get_dataset("plan_data");
		nDataSetInx_payx = parm_c_PP_CLI.m_f__get_dataset("payx_data");
		nDataSetInx_payx_common = parm_c_PP_CLI.m_f__get_dataset("common");
		nDataSetInx_payx_card = parm_c_PP_CLI.m_f__get_dataset("card");
		nDataSetInx_ordr = parm_c_PP_CLI.m_f__get_dataset("ordr_data");
		/* - ---------------------------------------------------------------------------- - */
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_payx_common, "amount", m_strTxMony);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_payx_common, "currency", currency);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_payx_common, "cust_ip", m_strCustIP);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_payx_common, "escw_mod", "N");
		/* - ---------------------------------------------------------------------------- - */
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_payx_card, "card_mny", good_mny);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_payx_card, "card_tx_type", "11511000");
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_payx_card, "quota", quota);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_payx_card, "bt_group_id", bt_group_id);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_payx_card, "bt_batch_key", bt_batch_key);
		/* - ---------------------------------------------------------------------------- - */
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_ordr, "ordr_idxx", ordr_idxx);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_ordr, "good_name", good_name);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_ordr, "good_mny", good_mny);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_ordr, "buyr_name", buyr_name);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_ordr, "buyr_tel1", buyr_tel1);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_ordr, "buyr_tel2", buyr_tel2);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_ordr, "buyr_mail", buyr_mail);
		/* - ---------------------------------------------------------------------------- - */
		parm_c_PP_CLI.m_f__add_data(nDataSetInx_payx, nDataSetInx_payx_common, "\x1e");
		parm_c_PP_CLI.m_f__add_data(nDataSetInx_payx, nDataSetInx_payx_card, "\x1e");
		/* - ---------------------------------------------------------------------------- - */
		parm_c_PP_CLI.m_f__add_data(nDataSetInx_req, nDataSetInx_payx, "\x1c");
		parm_c_PP_CLI.m_f__add_data(nDataSetInx_req, nDataSetInx_ordr, "\x1c");
		/* -------------------------------------------------------------------------------- */

		return nDataSetInx_req;
	}

	/* ==================================================================================== */
	/* +    METHOD : 요청 DATA 생성                                                       + */
	/* - -------------------------------------------------------------------------------- - */
	private int m_f__set_dataset_mod( ref C_PP_CLI_COM parm_c_PP_CLI ) {
		int nDataSetInx_req;
		int nDataSetInx_mod;


		/* -------------------------------------------------------------------------------- */
		/* +    변경 요청 DATA 구성                                                       + */
		/* - ---------------------------------------------------------------------------- - */
		m_strTxCD = "00200000";
		/* - ---------------------------------------------------------------------------- - */
		nDataSetInx_req = parm_c_PP_CLI.m_f__get_dataset("plan_data");
		nDataSetInx_mod = parm_c_PP_CLI.m_f__get_dataset("mod_data");
		/* - ---------------------------------------------------------------------------- - */
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_mod, "mod_type", mod_type);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_mod, "tno", tno);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_mod, "mod_ip", m_strCustIP);
		parm_c_PP_CLI.m_f__set_data(nDataSetInx_mod, "mod_desc", mod_desc);

		if(mod_type.Equals("STPC")) {
			parm_c_PP_CLI.m_f__set_data(nDataSetInx_mod, "mod_mny", mod_mny);
			parm_c_PP_CLI.m_f__set_data(nDataSetInx_mod, "rem_mny", rem_mny);
		}
		/* - ---------------------------------------------------------------------------- - */
		parm_c_PP_CLI.m_f__add_data(nDataSetInx_req, nDataSetInx_mod, "\x1c");
		/* -------------------------------------------------------------------------------- */

		return nDataSetInx_req;
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 정상 결제 결과 처리                                                  + */
	/* - -------------------------------------------------------------------------------- - */
	private bool m_f__to_do_shop_pay() {
		bool bRT = true;

		/* -------------------------------------------------------------------------------- */
		/* +    결과 처리                                                                 + */
		/* - ---------------------------------------------------------------------------- - */

		/* - ------------------------------------------------------------------------ - */
		/* +    TODO : 정상 사용 결과 처리 부분                                       + */
		/* - ------------------------------------------------------------------------ - */

		/* -------------------------------------------------------------------------------- */

		/* -------------------------------------------------------------------------------- */
		/* +    TODO (필수) : 처리 결과가 정상인 경우에는 반드시 bRT 값을 true로,         + */
		/* +                  오류가 발생한 경우에는 false 로 설정하여 주시기 바랍니다.   + */
		/* - ---------------------------------------------------------------------------- - */
		bRT = true;                  /* 정상 처리인 경우 : true, 오류가 발생한 경우 : false */
									 /* -------------------------------------------------------------------------------- */

		return bRT;
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 정상 취소 결과 처리                                                  + */
	/* - -------------------------------------------------------------------------------- - */
	private bool m_f__to_do_shop_mod() {
		bool bRT = true;

		/* -------------------------------------------------------------------------------- */
		/* +    TODO : 취소 결과 처리                                                     + */
		/* - ---------------------------------------------------------------------------- - */
		/* +    실패 시 m_strResCD (오류 사유 코드), m_strResMsg (오류 메시지) 설정       + */
		/* - ---------------------------------------------------------------------------- - */
		bRT = true;                  /* 정상 처리인 경우 : true, 오류가 발생한 경우 : false */
									 /* -------------------------------------------------------------------------------- */

		return bRT;
	}
	/* ==================================================================================== */
	

	/* ==================================================================================== */
	/* +    METHOD : 결과 출력 (적립/조회/사용 정상)                                      + */
	/* - -------------------------------------------------------------------------------- - */
	private Result m_f__disp_rt_pay_succ( ref C_PP_CLI_COM parm_c_PP_CLI ) {
		/* -------------------------------------------------------------------------------- */
		/* +    정상 결과 출력                                                            + */
		/* - ---------------------------------------------------------------------------- - */
		res_cd = m_strResCD;
		res_msg = m_strResMsg;
		/* - ---------------------------------------------------------------------------- - */
		tno = parm_c_PP_CLI.m_f__get_res("tno");
		good_mny = m_strTxMony;
		/* - ---------------------------------------------------------------------------- - */
		app_time = parm_c_PP_CLI.m_f__get_res("app_time");
		app_no = parm_c_PP_CLI.m_f__get_res("app_no");
		card_cd = parm_c_PP_CLI.m_f__get_res("card_cd");
		card_name = parm_c_PP_CLI.m_f__get_res("card_name");
		noinf = parm_c_PP_CLI.m_f__get_res("noinf");
		quota = parm_c_PP_CLI.m_f__get_res("quota");
		/* -------------------------------------------------------------------------------- */

		return new Result() {
			success = true , 
			app_no = app_no, app_time = app_time, card_cd = card_cd, card_name = card_name, noinf = noinf, quota = quota, res_cd = res_cd, res_msg = res_msg, tno = tno
		};

	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 결과 출력 (오류)                                                     + */
	/* - -------------------------------------------------------------------------------- - */
	private Result m_f__disp_rt_fail() {
		/* -------------------------------------------------------------------------------- */
		/* +    오류 결과 출력                                                            + */
		/* - ---------------------------------------------------------------------------- - */
		res_cd = m_strResCD;
		res_msg = m_strResMsg;
		/* -------------------------------------------------------------------------------- */

		return new Result() {
		success = false, res_cd = res_cd, res_msg = res_msg
		};

	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 결과 출력 (취소)                                                     + */
	/* - -------------------------------------------------------------------------------- - */
	private void m_f__disp_rt_can( ref C_PP_CLI_COM parm_c_PP_CLI ) {
		/* -------------------------------------------------------------------------------- */
		/* +    오류 결과 출력                                                            + */
		/* - ---------------------------------------------------------------------------- - */
		mod_desc = mod_desc;
		res_cd = m_strResCD;
		res_msg = m_strResMsg;
		/* -------------------------------------------------------------------------------- */
		// 부분취소 결과 처리
		if(req_tx.Equals("mod")) {
			if(res_cd.Equals("0000")) {
				if(mod_type.Equals("STPC")) {
					amount = parm_c_PP_CLI.m_f__get_res("amount"); // 총 금액
					panc_mod_mny = parm_c_PP_CLI.m_f__get_res("panc_mod_mny"); // 부분취소 요청금액
					panc_rem_mny = parm_c_PP_CLI.m_f__get_res("panc_rem_mny"); // 부분취소 가능금액
				}
			}
		}
	}
	/* ==================================================================================== */

	public class Result {
		public bool success;
		public string res_cd;
		public string res_msg;
		public string tno;
		public string app_time;
		public string app_no;
		public string card_cd;
		public string card_name;
		public string noinf;
		public string quota;
	}

}
