﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using NLog;
using CommonLib;

public class UploadFileAuthHandler : IHttpHandler {

	public static readonly List<string> ImageExtensions = new List<string> { ".jpg", ".jpeg", ".bmp", ".gif", ".png" };

	public void ProcessRequest(HttpContext context) {

		if (AdminLoginSession.HasCookie(context)) {
			Download(context, context.Request.Path, "application/octet-stream");
			return;
		}

		if (context.Request.Path.StartsWith("/upload/recruit/")) {

            using (MainDataContext dao = new MainDataContext())
            {
                var r_id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));

                var exist = www6.selectQ<resume>("r_id", r_id);
                //if (r_id == -1 || !dao.resume.Any(p => p.r_id == r_id))
                if (r_id == -1 || !exist.Any())
                {
                    NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
                    HttpContext.Current.Response.Write("<script>alert(\"File Not Found\");history.back();</script>");
                    return;
                }

                var resume = exist[0]; //dao.resume.First(p => p.r_id == r_id);

                if (!RecruitSession.HasCookie(context) || RecruitSession.GetCookie(context).id != resume.r_ap_id)
                {
                    NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
                    logger.Warn("권한없는 사용자가 채용데이타조회를 시도했습니다. r_id=" + context.Request["id"].EmptyIfNull());

                    HttpContext.Current.Response.Write("<script>alert(\"File Not Found(auth)\");history.back();</script>");
                    return;
                }

                var id = RecruitSession.GetCookie(context).id;
                var fileName = Path.GetFileName(context.Request.Path);

                if (exist.Any(p => p.r_ap_id == id && (p.r_file == fileName || p.r_txt_aboutme_4 == fileName)))
                {
                    Download(context, context.Request.Path, "application/octet-stream");
                }
                else
                {
                    HttpContext.Current.Response.Write("<script>alert(\"File Not Found\");history.back();</script>");
                    return;
                }
            }


		} else {
			Download(context, context.Request.Path, "application/octet-stream");
			return;
		}

	}

	bool Download(HttpContext context, string path, string contentType) {

		path = context.Server.MapPath(path);
		if (!File.Exists(path)) {
			HttpContext.Current.Response.Write("<script>alert(\"File Not Found\");history.back();</script>");
			return false;
		}

		foreach (var ext in new List<string> { ".js", ".aspx", ".config", ".cs", ".html", ".css" }) {
			if (path.IndexOf(ext) > -1) {
				HttpContext.Current.Response.Write("<script>alert(\"File Not Found.(500)\");history.back();</script>");
				return false;
			}
		}

		FileStream inStr = null;
		byte[] buffer = new byte[1024];
		long byteCount;
		try {
			HttpContext.Current.Response.Clear();
			HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
			HttpContext.Current.Response.Buffer = false;
			HttpContext.Current.Response.ContentType = contentType;
			if (contentType == "application/octet-stream") {
				HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(Path.GetFileName(path)));
			}

			//	throw new Exception();
			inStr = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 1024, false);
			while ((byteCount = inStr.Read(buffer, 0, buffer.Length)) > 0) {

				if (HttpContext.Current.Response.IsClientConnected) {
					HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
					HttpContext.Current.Response.Flush();
				} else {
					break;
				}
			}

			return true;

		} catch {
			try {
				HttpContext.Current.Response.Clear();
				HttpContext.Current.Response.ContentType = "text/html";
				HttpContext.Current.Response.ClearHeaders();
				HttpContext.Current.Response.Write("<script>alert(\"Download Error\");history.back();</script>");
			} catch {
			}
			return false;

		} finally {
			if (inStr != null)
				inStr.Close();
		}

	}

	public bool IsReusable
	{
		get
		{
			return false;
		}
	}

}