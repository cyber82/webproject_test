﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class AdminLoginSession
{

	public AdminLoginSession()
	{
	}

	public static bool HasCookie(HttpContext context) {
	//	return (context.Session[Constants.ADMIN_USER_COOKIE_NAME] != null);
        return context.Request.Cookies[Constants.ADMIN_USER_COOKIE_NAME] != null;
	}


    public static AdminEntity GetCookie(HttpContext context)
    {
		
		if (!HasCookie(context))
			return new AdminEntity();
	/*
		var entity = JsonConvert.DeserializeObject<AdminEntity>(context.Session[Constants.ADMIN_USER_COOKIE_NAME].ToString());
		entity.context = context;
		return entity;
	
	*/	
        HttpCookie cookie = context.Request.Cookies[Constants.ADMIN_USER_COOKIE_NAME];

		var jsonData = cookie["data"].ToString().Decrypt();
        var entity = JsonConvert.DeserializeObject<AdminEntity>(jsonData);
        entity.context = context;
        return entity;
		
	}
	

	public static void ClearCookie(HttpContext context) {
		/*
		context.Session[Constants.ADMIN_USER_COOKIE_NAME] = null;
		context.Session.Abandon();
		 */

		if(!HasCookie(context))
			return;
		
        HttpCookie cookie = context.Response.Cookies[Constants.ADMIN_USER_COOKIE_NAME];
        cookie.Domain = context.Request.Url.Host;
		cookie.Expires = DateTime.Now.AddYears(-1);
		context.Response.Cookies.Set(cookie);
		
	}


	public static void SetCookie(HttpContext context, AdminEntity entity){
		/*
				context.Session[Constants.ADMIN_USER_COOKIE_NAME] = entity.ToJson();
*/
		HttpCookie cookie = new HttpCookie(Constants.ADMIN_USER_COOKIE_NAME);

		var encData = JsonWriter.toLowerCaseJson(entity).Encrypt();

		context.Response.Write(encData);

		cookie.Values["data"] = encData;
		cookie.Path = "/";
		cookie.Domain = context.Request.Url.Host;
        
		context.Response.Cookies.Add(cookie);
				
	}


}