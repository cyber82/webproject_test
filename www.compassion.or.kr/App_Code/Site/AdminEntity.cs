﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using NLog;
using CommonLib;

public class AdminEntity {

    public int identifier{get;set;}
    public string name{get;set;}
    public string dept{get;set;}
    public string email{get;set; }
	public string type{ get; set; }
	public admin rawData{get;set;}
    internal HttpContext context { get; set; }
	
    public List<admin_menu> menus {
        get {
            if (this.context == null) return null;
			if (context.Session[Constants.ADMIN_MENU_SESSION_NAME] == null)
            {
                using (AdminDataContext dao = new AdminDataContext())
                {
                    //var list = from p in dao.admin_menu where p.am_display == true orderby p.am_order ascending select p;
                    var list = www6.selectQ<admin_menu>("am_display", 1, "am_order asc");
                    context.Session[Constants.ADMIN_MENU_SESSION_NAME] = list.ToList();
                }
            }

			return (List<admin_menu>)context.Session[Constants.ADMIN_MENU_SESSION_NAME];
        }
    }
	
    public List<v_admin_auth> auth_menus
    {
        get {
            if (this.context == null) return null;
			if (context.Session[Constants.ADMIN_AUTH_MENU_SESSION_NAME] == null)
            {
                using (AdminDataContext dao = new AdminDataContext())
                {
                    //var a_type = dao.admin.First(p => p.a_id == this.identifier && !p.a_deleted).a_type;
                    var a_type = www6.selectQF<admin>("a_id", this.identifier, "a_deleted", 0).a_type;
                    List<v_admin_auth> list = null;

                    if (a_type == "super")
                    {
                        var adminDT = www6.selectQ<admin_menu>();
                        
                        list = (from p in adminDT//dao.admin_menu
                                where p.am_display == true && p.am_path.Length > 0
                                orderby p.am_order ascending
                                select new v_admin_auth()
                                {
                                    am_code = p.am_code,
                                    am_name = p.am_name,
                                    am_depth = p.am_depth,
                                    am_display = p.am_display,
                                    am_group = p.am_group,
                                    am_group_order = p.am_group_order,
                                    am_order = p.am_order,
                                    am_path = p.am_path,
                                    aa_a_id = this.identifier,
                                    aa_auth_create = true,
                                    aa_auth_delete = true,
                                    aa_auth_read = true,
                                    aa_auth_update = true
                                }).ToList();
                    }
                    else
                    {
                        var vAdminAuth = www6.selectQ<v_admin_auth>();
                        list = (from p in vAdminAuth//dao.v_admin_auth
                                where p.aa_a_id == this.identifier && p.am_display == true
                                orderby p.am_order ascending
                                select p).ToList();
                    }
                    context.Session[Constants.ADMIN_AUTH_MENU_SESSION_NAME] = list.ToList();
                }
            }

			return (List<v_admin_auth>)context.Session[Constants.ADMIN_AUTH_MENU_SESSION_NAME];
        }
    }
}