﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using NLog;
using System.Configuration;

public class AdminLog
{
	public AdminLog()
	{
	}

	public enum Type{
		insert , 
		update , 
		delete , 
		select
	}

	public static string GetTypeName(object type) {

		switch (type.ToString().ToLower()) {
			default:
			return "조회";
			case "update" :
			return "수정";
			case "delete":
			return "삭제";
			case "insert":
			return "등록";
		}

	}


	public static void Write(HttpContext context, Type type , string am_code , string comment) {

		LogEventInfo evt = new LogEventInfo() {
			Level = LogLevel.Info, TimeStamp = DateTime.Now
		};

        evt.Properties["la_am_code"] = am_code;
        evt.Properties["la_type"] = type.ToString();
        evt.Properties["la_url"] = context.Request.RawUrl;
        evt.Properties["la_a_id"] = AdminLoginSession.GetCookie(context).identifier;
        evt.Properties["la_comment"] = comment.Replace(@"'", "");
        //Logger logger = LogManager.GetLogger("admin.action");
        //logger.Log(evt);
        
        string sqlStr = string.Format(@"INSERT INTO log_admin_action( la_am_code , la_type , la_url , la_a_id , la_comment) 
                                                             VALUES ('{0}' , '{1}' , '{2}' , {3} , '{4}' )"
                                     , am_code, type.ToString(), context.Request.RawUrl, AdminLoginSession.GetCookie(context).identifier, comment.Replace(@"'", ""));
        www6.cud(sqlStr);
	}

}