﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Dynamic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Configuration;
using System.IO;
using NLog;
using System.Web.UI.HtmlControls;
using CommonLib;

public abstract class AdminBoardPage : AdminBasePage {


	protected string BoardType {
		set {
			this.ViewState.Add("type", value);
		}
		get {
			var result = this.ViewState["type"];
			if (result == null)
				throw new Exception("undefined BoardType");
			return this.ViewState["type"].ToString();
		}
	}

	protected void Remove()
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));
            //daoDel.tVisionTripIndividualDetail.DeleteOnSubmit(entity_individual);
            //daoDel.SubmitChanges();
            www6.delete(entity);
        }
	}

	protected bool Update(board arg) {
		var fileList = base.GetTemporaryFiles();

		using (AdminDataContext dao = new AdminDataContext()) {
			if (Action == "update") {
                //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));

                entity.b_title = arg.b_title;
				entity.b_content = arg.b_content;
				entity.b_a_id = arg.b_a_id;
				entity.b_display = arg.b_display;
				entity.b_hot = arg.b_hot;
				entity.b_main = arg.b_main;
				entity.b_regdate = arg.b_regdate;
				entity.b_meta_keyword = arg.b_meta_keyword;
				entity.b_summary = arg.b_summary;
                //dao.SubmitChanges();
                www6.update(entity);

            } else {
				arg.b_hits = 0;
				arg.b_type = this.BoardType;
                //dao.board.InsertOnSubmit(arg);
                www6.insert(arg);
                //dao.SubmitChanges();

                PrimaryKey = arg.b_id.ToString();

				foreach (var file in fileList) {
					file.f_ref_id = PrimaryKey;
				}
				SaveTemporaryFiles(fileList);
			}

            //dao.file.DeleteAllOnSubmit(dao.file.Where(p => p.f_ref_id == PrimaryKey && p.f_group == FileGroup.ToString()));
            var fiList = www6.selectQ<file>("f_ref_id", PrimaryKey, "f_group", FileGroup.ToString());
            www6.delete(fiList);

            //if (fileList.Count > 0)
            //{
            //	dao.file.InsertAllOnSubmit(fileList);
            //}
            foreach (var fi in fileList)
            {
                www6.insert(fi);
            }
			//dao.SubmitChanges();
		}

		return true;

	}




}
