﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CommonLib;
using Newtonsoft.Json.Linq;
using System.Configuration;

public partial class _default : FrontBasePage {

	protected override void OnBeforePostBack() {
        //string str = "아르넬asdf";//"아르넬asdf"; //ㅇㅏㄹㅡㄴㅔㄹ  
        //string str1 = str.Normalize(NormalizationForm.FormD);
        //string str2 = str1.Normalize(NormalizationForm.FormC);
        //test1.Value = str1;
        //test2.Value = str2;

        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;
        //Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        //Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));

        //김태형 2017.06.01
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else
        {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }


        ph_sec4.Visible = true;

        if (ph_sec4.Visible)
        {
            this.GetChildren();
        }
	}



	// 비로그인 나오는 어린이 추천 3종 , 일별 캐싱
	

	// 1:1 어린이 양육 목록 , 일별 캐싱
	void GetChildren() {
		 
		var action = new ChildAction();

        //var actionResult = action.GetChildren(null, null, null, null, null, 6, null, null, 1, 16, "waiting", -1, -1);
        /*20160207 후원>1:1어린이양육 기준으로 검색조건 수정*/

        //?country = GH--국가선택(exec sp_country_list_f 의 컬럼 c_id)
        //& minAge = 3--최저나이선택
        //& maxAge = 20--최고나이선택
        //& gender = M--성별(M: 남자, F: 여자)
        //& orderby = waiting--오래 기다린 어린이부터 보기 (waiting:체크시, new:기본값) 
        //&today = Y--오늘 생일인 어린이 보기 (Y:체크시) 
        //&mm = 6--특별한 기념일 -월
        //& dd = 1--특별한 기념일 -일
        //& specialNeed = Y--몸이 불편한 어린이(Y: 체크시)
        //& orphan = Y--부모님이 안 계신 어린이 (Y:체크시)

        string country = Request.QueryString["country"];
        string gender = Request.QueryString["gender"];//.EmptyIfNull();
        int? mm = Request.QueryString["mm"].EmptyIfNull().Equals("") ? null : (int?)Convert.ToInt32(Request.QueryString["mm"]);
        int? dd = Request.QueryString["dd"].EmptyIfNull().Equals("") ? null : (int?)Convert.ToInt32(Request.QueryString["dd"]);
        int? minAge = Convert.ToInt32(Request.QueryString["minAge"].ValueIfNull("3"));
        int? maxAge = Convert.ToInt32(Request.QueryString["maxAge"].ValueIfNull("20"));
        bool? orphan = Request["orphan"] == null ? null : (bool?) Convert.ToBoolean(Request["orphan"]);
        bool? specialNeed = Request["specialNeed"] == null ? null : (bool?)Convert.ToBoolean(Request["specialNeed"]);
        string orderby = Request.QueryString["orderby"].ValueIfNull("new");

        //var actionResult = action.GetChildren(null, null, null, null, null, 20, null, null, 1, 16, "new", -1, -1);
        var actionResult = action.GetChildren(country, gender, mm, dd, minAge, maxAge, orphan, specialNeed, 1, 16, orderby, -1, -1);
        if (actionResult.success) {

			var items = (List<ChildAction.ChildItem>)actionResult.data;

			if(items.Count > 0) {
				Random rnd = new Random();
				var start_index = rnd.Next(items.Count);
                var end_index = 3;
                List<ChildAction.ChildItem> selected_list = new List<ChildAction.ChildItem>();

                if (items.Count < 3)
                {
                    end_index = items.Count;
                }

				for(int i = 0; i < end_index; i++) {

					var index = start_index + i;
					if(index >= items.Count) {
						index = index - items.Count;
					}

					selected_list.Add(items[index]);
				}

				repeater_children.DataSource = selected_list;
				repeater_children.DataBind();
			}
		}
		
	}
}