﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _test_setcookie : System.Web.UI.Page {
	protected void Page_Load( object sender, EventArgs e ) {
	
		{
			HttpCookie cookie = new HttpCookie("token");
			cookie.Values["data"] = "tokenval";
			cookie.Path = "/";
			cookie.Domain = "wisekit.co";
			cookie.Expires = DateTime.Now.AddYears(1);
			Response.Cookies.Add(cookie);
			
		}

		{
			HttpCookie cookie = new HttpCookie("token");
			cookie.Value = "tokenvalue3";
			cookie.Path = "/";
			//cookie.Domain = "*";
			//cookie.Expires = DateTime.Now.AddYears(1);
			Response.Cookies.Add(cookie);
		}

	}
}