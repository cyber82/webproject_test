﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;

public partial class _test_cardbatch : System.Web.UI.Page {
    protected void Page_Load( object sender, EventArgs e ) {

        reqdate.Value = DateTime.Now.ToString("yyyyMMdd");
        reqtime.Value = DateTime.Now.ToString("HHmmss");
        ordr_idxx.Value = DateTime.Now.ToString("yyyyMMddHHmmssff");
    }

    protected void btn_submit_Click( object sender, EventArgs e ) {
        CommonLib.PAY4Service.ServiceSoapClient pay4 = new CommonLib.PAY4Service.ServiceSoapClient();
        DataSet dsBatchKey = MakePaymentCardBatchDataSet("pay", ordr_idxx.Value, groupid.Value
                                    , good_name.Value, good_mny.Value, buyr_name.Value
                                    , reqdate.Value, reqtime.Value, bt_batch_key.Value
                                    , "", "");


        DataSet dsResult = pay4.PaymentCardBatch(dsBatchKey);
        msg.Value = dsResult.ToJson();

    }

    public DataSet MakePaymentCardBatchDataSet( string sReqTx, string sOrderIDxx, string sGroupID
                                , string sGoodName, string sGoodMny, string sBuyerName
                                , string sReqDate, string sReqTime, string sBatchKey
                                , string sTno, string sCustIP ) {
        DataTable dtCommonT = new DataTable();
        DataTable dtSslT = new DataTable();
        DataTable dtModT = new DataTable();
        DataSet dsData = new DataSet();


        // DataTable Create
        dtCommonT.TableName = "CommonT";

        dtCommonT.Columns.Add("req_tx", typeof(System.String));
        dtCommonT.Columns.Add("ordr_idxx", typeof(System.String));
        dtCommonT.Columns.Add("good_name", typeof(System.String));
        dtCommonT.Columns.Add("amount", typeof(System.String));

        dtCommonT.Columns.Add("buyr_name", typeof(System.String));
        dtCommonT.Columns.Add("currency", typeof(System.String));
        dtCommonT.Columns.Add("quotaopt", typeof(System.String));
        dtCommonT.Columns.Add("group_idxx", typeof(System.String));

        dtCommonT.Columns.Add("req_date", typeof(System.String));
        dtCommonT.Columns.Add("req_time", typeof(System.String));
        dtCommonT.Columns.Add("batch_key", typeof(System.String));

        dsData.Tables.Add(dtCommonT);


        // DataSet Add
        dsData.Tables["CommonT"].Rows.Add();

        dsData.Tables["CommonT"].Rows[0]["req_tx"] = sReqTx;
        dsData.Tables["CommonT"].Rows[0]["ordr_idxx"] = sOrderIDxx;
        dsData.Tables["CommonT"].Rows[0]["good_name"] = sGoodName;
        dsData.Tables["CommonT"].Rows[0]["amount"] = sGoodMny;

        dsData.Tables["CommonT"].Rows[0]["buyr_name"] = sBuyerName;
        dsData.Tables["CommonT"].Rows[0]["currency"] = "410";
        dsData.Tables["CommonT"].Rows[0]["quotaopt"] = "00";
        dsData.Tables["CommonT"].Rows[0]["group_idxx"] = sGroupID;

        dsData.Tables["CommonT"].Rows[0]["req_date"] = sReqDate;
        dsData.Tables["CommonT"].Rows[0]["req_time"] = sReqTime;
        dsData.Tables["CommonT"].Rows[0]["batch_key"] = sBatchKey;

        // DataTable Create
        dtModT.TableName = "ModT";
        dtModT.Columns.Add("tno", typeof(System.String));
        dtModT.Columns.Add("m_strCustIP", typeof(System.String));
        dsData.Tables.Add(dtModT);


        // DataSet Add
        dsData.Tables["ModT"].Rows.Add();
        dsData.Tables["ModT"].Rows[0]["tno"] = sTno;
        dsData.Tables["ModT"].Rows[0]["m_strCustIP"] = sCustIP;


        return dsData;
    }

}