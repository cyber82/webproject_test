﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class recruit_view : FrontBasePage {

	const string listPath = "/_test/angular/";

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		
		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}

		base.PrimaryKey = requests[0];
		id.Value = PrimaryKey.ToString();

		
		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();


	}


	protected override void loadComplete(object sender, EventArgs e) {

		using (FrontDataContext dao = new FrontDataContext()) {

			var entity = dao.ssb_article.First(p => p.idx == Convert.ToInt32(PrimaryKey));
			title.Text = entity.title;
			sub_title.Text = entity.sub_title;
			reg_date.Text = entity.reg_date.ToString("yyyy.MM.dd");
			view_num.Text = ((int)entity.view_num).ToString("N0");
			article.Text = entity.body;


			bindPrevNext(dao);
		}
	}


	//이전 다음
	void bindPrevNext(FrontDataContext dao) {

		string s_type = Request["s_type"].EmptyIfNull().EscapeSqlInjection();
		string k_type = Request["k_type"].ValueIfNull("both").EscapeSqlInjection();
		string k_word = Request["k_word"].EmptyIfNull().EscapeSqlInjection();
		string s_column = Request["s_column"].EmptyIfNull().EscapeSqlInjection();


		sp_sympathy_prevNext_fResult result = dao.sp_sympathy_prevNext_f(Convert.ToInt32(PrimaryKey), s_type, k_type, k_word, s_column, "", "").First();

		if (result.prev_idx.HasValue) {
			var href = string.Format("view/{0}?{1}#c", result.prev_idx.ToString(), this.ViewState["q"].ToString());
			btnPrev.HRef = href;
			prevTitle.Text = result.prev_title;
		} else {
			btnPrev.Visible = false;
		}

		if (result.next_idx.HasValue) {
			var href = string.Format("view/{0}?{1}#c", result.next_idx.ToString(), this.ViewState["q"].ToString());
			btnNext.HRef = href;
			nextTitle.Text = result.next_title;
		} else {
			btnNext.Visible = false;
		}

	}


}