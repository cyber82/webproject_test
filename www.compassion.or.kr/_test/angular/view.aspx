﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="recruit_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        
        angular.module("defaultApp", ['bw.paging'], function ($httpProvider) {
            // http://stackoverflow.com/questions/19254029/angularjs-http-post-does-not-send-data
            // Use x-www-form-urlencoded Content-Type
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

            /**
             * The workhorse; converts an object to x-www-form-urlencoded serialization.
             * @param {Object} obj
             * @return {String}
             */
            var param = function (obj) {
                var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

                for (name in obj) {
                    value = obj[name];

                    if (value instanceof Array) {
                        for (i = 0; i < value.length; ++i) {
                            subValue = value[i];
                            fullSubName = name + '[' + i + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if (value instanceof Object) {
                        for (subName in value) {
                            subValue = value[subName];
                            fullSubName = name + '[' + subName + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if (value !== undefined && value !== null)
                        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                }

                return query.length ? query.substr(0, query.length - 1) : query;
            };

            // Override $http service's default transformRequest
            $httpProvider.defaults.transformRequest = [function (data) {
                return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
            }];

        })
        .filter("maskUserId", function () { 
            return function (value) {
                var len = value.length;
                return value.substr(0, len-3) + "***";
            }
        })
        .controller("defaultCtrl", function ($scope, $http) {

            $scope.isLogin = common.isLogin();
            $scope.userId = common.getUserId();

            $scope.total = 0;
            $scope.page = 1;
            $scope.rowsPerPage = 10;


            $scope.list = [];
            $scope.params = {
                id : $("#id").val(),
                page: $scope.page,
                rowsPerPage: $scope.rowsPerPage,
                type : "story"
            };

            $scope.getList = function (params) {
                $scope.params = $.extend($scope.params, params);
                $http.get("/api/sympathy_reply.ashx?t=list", { params: $scope.params }).success(function (result) {
                    if (result.success) {
                        $scope.list = result.data;
                        $scope.total = result.data.length > 0 ? result.data[0].total : 0;
                    }
                });
            }


            $scope.update = function () {
                //console.log($scope.content);
                if ($scope.content == "") {
                    alert("댓들을 입력해주세요.");
                    $("#reply").focus();
                    return false;
                }

                // 신규 등록
                if ($scope.updateId == -1) {
                    $http.post("/api/sympathy_reply.ashx", {t: 'add',id: $("#id").val(),content: $scope.content}).success(function (result) {
                        if (result.success) {
                            $scope.getList({page : 1});
                        } else {
                            alert(result.message);
                        }
                    })
                } else {
                    $http.post('/api/sympathy_reply.ashx', {t: 'update',id: $scope.updateId,content: $scope.content}).success(function (result) {
                        if (result.success) {
                            $scope.getList();
                        } else {
                            alert(result.message);
                        }

                        $scope.updateId == -1;
                    })
                }

                $scope.content = "";
            }


            $scope.remove = function (id) {
                $http.post("/api/sympathy_reply.ashx?t=remove&id=" + id).success(function (result) {
                    if (result.success) {
                        alert("삭제되었습니다.");
                        $scope.getList();
                    } else {
                        alert(result.message);
                    }
                });
            }

            $scope.updateId = -1;
            $scope.modify = function (entity) {
                $scope.updateId = entity.idx;
                $scope.content = entity.body
            }

            $scope.getList();
        })
        ;

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <div ng-app="defaultApp" ng-controller="defaultCtrl">

        <input type="hidden" id="id" runat="server" />
        <a href="#" title="목록" runat="server" id="btnList">목록</a>
        <table>
            <tr>
                <th>제목</th>
                <td><asp:Literal runat="server" ID="title" /></td>
                <th>작성</th>
                <td><asp:Literal runat="server" ID="sub_title" /></td>
                <th>등록일</th>
                <td><asp:Literal runat="server" ID="reg_date" /></td>
                <th>조회수</th>
                <td><asp:Literal runat="server" ID="view_num" /></td>
            </tr>
        </table>



        <!-- reply -->
        댓글 {{total}} <br />

        <textarea id="reply" name="content" ng-model="content" ng-bind="content"></textarea><a ng-click="update()">등록</a>

        
        <ul>
            <li ng-repeat="item in list" ng-click="goView(item.idx)">
                {{item.user_id | maskUserId}}<br />
                {{item.body}}<br />
                <a href="#" ng-click="modify(item)" ng-show="userId == item.user_id">수정</a> | 
                <a href="#" ng-click="remove(item.idx)" ng-show="userId == item.user_id">삭제</a>
                {{item.reg_date | date : 'yyyy-MM-dd HH:mm:ss' }}<br />
            </li>

            <li ng-hide="list.length">데이터가 없습니다.</li>
        </ul>

        
        <paging
            class="small"
            page="page" 
            page-size="rowsPerPage" 
            total="total"
            show-prev-next="true"
            show-first-last="true"
            paging-action="getList({page : page})">
        </paging>   





        <a href="#" runat="server" id="btnPrev">
            이전글
            <asp:Literal runat="server" ID="prevTitle"></asp:Literal>
        </a>
        <br />
        
        <a href="#" runat="server" id="btnNext">
            다음글
            <asp:Literal runat="server" ID="nextTitle"></asp:Literal>
        </a>

        <asp:Literal runat="server" ID="article" />

    </div>
    

        

    
</asp:Content>