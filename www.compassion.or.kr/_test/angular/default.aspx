﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="recruit_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        angular.module("defaultApp", ['bw.paging'])
        .controller("defaultCtrl", function ($scope, $http) {
            
            $scope.total = 0;
            $scope.page = 1;
            $scope.rowsPerPage = 10;

            $scope.list = null;
            $scope.params = {
                page : $scope.page,
                rowsPerPage: $scope.rowsPerPage,
                s_type: "child"
            };


            // 검색
            $scope.search = function (params) {
                $scope.params = $.extend($scope.params, params);
                $scope.params.k_type = $("#k_type").val();
                $scope.params.k_word = $("#k_word").val();
                $scope.getList();
            }


            // list
            $scope.getList = function (params) {
                $scope.params = $.extend($scope.params, params);
                $http.get("/api/sympathy.ashx?t=list", { params: $scope.params }).success(function (result) {
                    $scope.list = result.data;
                    $scope.total = result.data.length > 0 ? result.data[0].total : 0;
                });
            }


            // 상세페이지
            $scope.goView = function (idx) {
                location.href = "/_test/angular/view/" + idx + "?"+$.param($scope.params);
            }

            // 정렬
            $scope.sort = function (s_column) {
                $scope.params.s_column = s_column;
                $scope.params.page = 1;
                $scope.params.k_type = "";
                $scope.params.k_word = "";
                $scope.getList();
            }

            $scope.getList();

        })
        ;

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div ng-app="defaultApp" ng-controller="defaultCtrl">
            
        <a href="#" ng-click="sort('reg_date')">최신순</a> | <a href="#" ng-click="sort('view_count')">인기순</a>

        <ul>
            <li ng-repeat="item in list" ng-click="goView(item.idx)">
                <img ng-src="/ssBoard/thumbnail/{{item.idx}}" style="width:100px;height:100px;"><br />
                {{item.title}}<br />
                {{item.sub_title}}<br />
                {{item.reg_date}}<br />
            </li>

             <li ng-if="list != null && !list.length">데이터가 없습니다.</li>
        </ul>

        <select id="k_type" name="k_type">
            <option value="">전체</option>
            <option value="title">제목</option>
            <option value="content">내용</option>
        </select>
        <input type="text" name="k_word" id="k_word" />
        <a href="#" ng-click="search()">검색</a>

        <div>
            <paging
                class="small"
                page="page" 
                page-size="rowsPerPage" 
                total="total"
                show-prev-next="true"
                show-first-last="true"
                paging-action="getList({page : page})">
            </paging>   
        </div>
    </div>
    
</asp:Content>