﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cardbatch.aspx.cs" Inherits="_test_cardbatch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        kcp group id : <input type="text" runat="server" id="groupid" style="width:300px" value="BA0011000348" />
        <br />
        batch-key : <input type="text" runat="server" id="bt_batch_key" style="width:300px" value="" />
        <br />
        ordr_idxx : <input type="text" runat="server" id="ordr_idxx" style="width:300px" value="" />
        <br />
        buyr_name : <input type="text" runat="server" id="buyr_name" style="width:300px" value="홍길동" />
        <br />
        good_name : <input type="text" runat="server" id="good_name" style="width:300px" value="한국컴패션후원금" />
        <br />
        good_mny : <input type="text" runat="server" id="good_mny" style="width:300px" value="10000" />
        <br />
        req date : <input type="text" runat="server" id="reqdate" style="width:300px" value="" />
        <br />
        req time : <input type="text" runat="server" id="reqtime" style="width:300px" value="" />
        <br />
       
        <asp:LinkButton runat="server" ID="btn_submit" OnClick="btn_submit_Click">전송</asp:LinkButton>

        <h3 >
            <textarea runat="server" id="msg" style="width:800px;height:500px"></textarea>
        
        </h3>

        <h3>소스코드</h3>

        <textarea style="width:1000px;height:1000px">
    protected void Page_Load( object sender, EventArgs e ) {

        reqdate.Value = DateTime.Now.ToString("yyyyMMdd");
        reqtime.Value = DateTime.Now.ToString("HHmmss");
        ordr_idxx.Value = DateTime.Now.ToString("yyyyMMddHHmmssff");
    }

    protected void btn_submit_Click( object sender, EventArgs e ) {
        CommonLib.PAY4Service.ServiceSoapClient pay4 = new CommonLib.PAY4Service.ServiceSoapClient();
        DataSet dsBatchKey = MakePaymentCardBatchDataSet("pay", ordr_idxx.Value, groupid.Value
                                    , good_name.Value, good_mny.Value, buyr_name.Value
                                    , reqdate.Value, reqtime.Value, bt_batch_key.Value
                                    , "", "");


        DataSet dsResult = pay4.PaymentCardBatch(dsBatchKey);
        msg.Value = dsResult.ToJson();

    }

    public DataSet MakePaymentCardBatchDataSet( string sReqTx, string sOrderIDxx, string sGroupID
                                , string sGoodName, string sGoodMny, string sBuyerName
                                , string sReqDate, string sReqTime, string sBatchKey
                                , string sTno, string sCustIP ) {
        DataTable dtCommonT = new DataTable();
        DataTable dtSslT = new DataTable();
        DataTable dtModT = new DataTable();
        DataSet dsData = new DataSet();


        // DataTable Create
        dtCommonT.TableName = "CommonT";

        dtCommonT.Columns.Add("req_tx", typeof(System.String));
        dtCommonT.Columns.Add("ordr_idxx", typeof(System.String));
        dtCommonT.Columns.Add("good_name", typeof(System.String));
        dtCommonT.Columns.Add("amount", typeof(System.String));

        dtCommonT.Columns.Add("buyr_name", typeof(System.String));
        dtCommonT.Columns.Add("currency", typeof(System.String));
        dtCommonT.Columns.Add("quotaopt", typeof(System.String));
        dtCommonT.Columns.Add("group_idxx", typeof(System.String));

        dtCommonT.Columns.Add("req_date", typeof(System.String));
        dtCommonT.Columns.Add("req_time", typeof(System.String));
        dtCommonT.Columns.Add("batch_key", typeof(System.String));

        dsData.Tables.Add(dtCommonT);


        // DataSet Add
        dsData.Tables["CommonT"].Rows.Add();

        dsData.Tables["CommonT"].Rows[0]["req_tx"] = sReqTx;
        dsData.Tables["CommonT"].Rows[0]["ordr_idxx"] = sOrderIDxx;
        dsData.Tables["CommonT"].Rows[0]["good_name"] = sGoodName;
        dsData.Tables["CommonT"].Rows[0]["amount"] = sGoodMny;

        dsData.Tables["CommonT"].Rows[0]["buyr_name"] = sBuyerName;
        dsData.Tables["CommonT"].Rows[0]["currency"] = "410";
        dsData.Tables["CommonT"].Rows[0]["quotaopt"] = "00";
        dsData.Tables["CommonT"].Rows[0]["group_idxx"] = sGroupID;

        dsData.Tables["CommonT"].Rows[0]["req_date"] = sReqDate;
        dsData.Tables["CommonT"].Rows[0]["req_time"] = sReqTime;
        dsData.Tables["CommonT"].Rows[0]["batch_key"] = sBatchKey;

        // DataTable Create
        dtModT.TableName = "ModT";
        dtModT.Columns.Add("tno", typeof(System.String));
        dtModT.Columns.Add("m_strCustIP", typeof(System.String));
        dsData.Tables.Add(dtModT);


        // DataSet Add
        dsData.Tables["ModT"].Rows.Add();
        dsData.Tables["ModT"].Rows[0]["tno"] = sTno;
        dsData.Tables["ModT"].Rows[0]["m_strCustIP"] = sCustIP;


        return dsData;
    }
        </textarea>

    </div>
    </form>
</body>
</html>
