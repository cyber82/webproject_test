﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using LGCNS.KMpay.Merchant.Certification;

using LGCNS.CNSPay.Service;


public partial class _test_kakaopay_KakaopayLiteRequest : Page{
        protected String RequestDealApproveUrl = String.Empty;
        protected String RequestDealPage = String.Empty;
        protected String Amt = String.Empty;
        protected String MID = String.Empty;
        protected String EncodeKey = String.Empty;
        protected String MerchantEncKey = String.Empty;
        protected String MerchantHashKey = String.Empty;
        protected String TargetUrl = String.Empty;
        protected String EdiDate = String.Empty;
        protected String Hash_String = String.Empty;
        protected String RequestDealPaymentUrl = String.Empty;
        protected String merchantTxnNum = String.Empty;
        protected String channelType = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
			this.ViewState["payInfo"] = "{ \"type\":3,\"group\":\"CIV\",\"codeId\":\"PBCIV\",\"codeName\":\"상상e上 컴퓨터교실\",\"amount\":10000,\"month\":1,\"frequency\":\"일시\",\"campaignId\":\"20160115135621832\",\"childMasterId\":\"\",\"relation_key\":28,\"sponsorId\":null,\"sponsorName\":null,\"userId\":null,\"commitmentId\":null,\"payMethod\":0,\"TypeName\":\"특별한 나눔\",\"PayMethodName\":\"신용카드\"}";
			//아래 값은 가맹점에서 DB 또는 Config 파일로 관리한다.
			MerchantEncKey = ConfigurationManager.AppSettings["MerchantEncKey"];
            MerchantHashKey = ConfigurationManager.AppSettings["MerchantHashKey"];
            RequestDealApproveUrl = ConfigurationManager.AppSettings["RequestDealApproveUrl"];
            RequestDealPage = ConfigurationManager.AppSettings["RequestDealPage"];
            EncodeKey = ConfigurationManager.AppSettings["EncodeKey"];
            MID = ConfigurationManager.AppSettings["MID"];
            RequestDealPaymentUrl = ConfigurationManager.AppSettings["RequestDealPaymentUrl"];
            channelType = ConfigurationManager.AppSettings["channelType"];

            Amt = "1003";   //상품 가격
            EdiDate = DateTime.Now.ToString("yyyyMMddHHmmss");  //전문생성일시
            String md_src = EdiDate + MID + Amt;  //위변조 처리

            CnsPayCipher chiper = new CnsPayCipher();
            Hash_String = chiper.SHA256Salt(md_src, EncodeKey);

            merchantTxnNum = DateTime.Now.ToString("yyyyMMddHH24mmss");
			
		}
    }
