﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using LGCNS.CNSPay.Service;

    public partial class _test_kakaopay_KakaopayLiteResult : System.Web.UI.Page
    {
        protected String buyerName = String.Empty;       // 구매자명
        protected String goodsName = String.Empty;      // 상품명

        protected String resultCode = String.Empty;         // 결과코드 (정상 :3001 , 그 외 에러)
        protected String resultMsg = String.Empty;          // 결과메시지
        protected String authDate = String.Empty;           // 승인일시 YYMMDDHH24mmss
        protected String authCode = String.Empty;           // 승인번호
        protected String payMethod = String.Empty;       // 결제수단
        protected String mid = String.Empty;                // 가맹점ID
        protected String tid = String.Empty;                  // 거래ID
        protected String moid = String.Empty;               // 주문번호
        protected String amt = String.Empty;                 // 금액

        protected String cardCode = String.Empty;         // 카드사 코드
        protected String cardName = String.Empty;        // 결제카드사명
        protected String cardQuota = String.Empty;        // 00:일시불,02:2개월
        protected String cardInterest = String.Empty;      // 무이자 여부  (0:일반, 1:무이자)
        protected String cardCl = String.Empty;             // 체크카드여부 (0:일반, 1:체크카드)
        protected String cardBin = String.Empty;           // 카드BIN번호
        protected String cardPoint = String.Empty;         // 카드사포인트사용여부 (0:미사용, 1:포인트사용, 2:세이브포인트사용)
        protected String cardNo = String.Empty;

        protected String nonRepToken = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            KakaoPayResult();
        }

        public void KakaoPayResult()
        {
            CnsPayWebConnector connector = new CnsPayWebConnector();
            //결제 처리 경로(DB 또는 Config 파일로 관리한다.)
            connector.RequestUrl = ConfigurationManager.AppSettings["RequestDealPaymentUrl"];

            // 1. 로그 디렉토리 생성 : cnsPayHome/log 로 생성
            connector.SetCnsPayHome();

            // 2. 요청 페이지 파라메터 셋팅
            connector.SetRequestData();

            // 3. 추가 파라메터 셋팅
            connector.AddRequestData("actionType", "PY0");  // actionType : CL0 취소, PY0 승인, CI0 조회
            connector.AddRequestData("MallIP", Request.ServerVariables["LOCAL_ADDR"]);  // 가맹점 고유 ip
            connector.AddRequestData("CancelPwd", "123456");

            // 필요 시 전문 항목 추가
            connector.AddRequestData("GoodsVat", "0");
            connector.AddRequestData("ServiceAmt", "0");

            //가맹점키 셋팅 (MID 별로 틀림) - 가맹점키 값은 가맹점에서 DB 또는 Config 파일로 관리한다.
            String EncodeKey = ConfigurationManager.AppSettings["EncodeKey"];
            connector.AddRequestData("EncodeKey", EncodeKey);

            // 4. CNSPAY Lite 서버 접속하여 처리
            connector.RequestAction();

            // 5. 결과 처리
            resultCode = connector.GetResultData("ResultCode");     // 결과코드 (정상 :3001 , 그 외 에러)
            resultMsg = connector.GetResultData("ResultMsg");       // 결과메시지
            authDate = connector.GetResultData("AuthDate");         // 승인일시 YYMMDDHH24mmss
            authCode = connector.GetResultData("AuthCode");         // 승인번호
            buyerName = connector.GetResultData("BuyerName");       // 구매자명
            goodsName = connector.GetResultData("GoodsName");       // 상품명
            payMethod = connector.GetResultData("PayMethod");       // 결제수단
            mid = connector.GetResultData("MID");                   // 가맹점ID
            tid = connector.GetResultData("TID");                   // 거래ID
            moid = connector.GetResultData("Moid");                 // 주문번호
            amt = connector.GetResultData("Amt");                   // 금액
            cardCode = connector.GetResultData("CardCode");		      // 카드사 코드
            cardName = connector.GetResultData("CardName");         // 결제카드사명
            cardQuota = connector.GetResultData("CardQuota");       // 할부개월수 ex) 00:일시불,02:2개월
            cardInterest = connector.GetResultData("CardInterest"); // 무이자 여부 (0:일반, 1:무이자)
            cardCl = connector.GetResultData("CardCl");             // 체크카드여부 (0:일반, 1:체크카드)
            cardBin = connector.GetResultData("CardBin");           // 카드BIN번호
            cardPoint = connector.GetResultData("CardPoint");       // 카드사포인트사용여부 (0:미사용, 1:포인트사용, 2:세이브포인트사용)


		string payInfo = Request.Form["payInfo"];       // json custom
		Response.Write("payInfo>" + payInfo);

																//부인방지토큰값
		nonRepToken = Request.Form["NON_REP_TOKEN"];

            Boolean paySuccess = false;		// 결제 성공 여부

            /** 위의 응답 데이터 외에도 전문 Header와 개별부 데이터 Get 가능 */
            //신용카드
            if (payMethod.Equals("CARD"))
            {
                // 결과코드 (정상 :3001 , 그 외 에러)
                if (resultCode.Equals("3001"))
                {
                    paySuccess = true;
                }
            }

            if (paySuccess)
            {
                // 결제 성공시 DB처리 하세요.
            }
            else
            {
                // 결제 실패시 DB처리 하세요.
            }
        }
    }
