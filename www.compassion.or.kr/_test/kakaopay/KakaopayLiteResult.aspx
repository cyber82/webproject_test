﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="KakaopayLiteResult.aspx.cs" Inherits="_test_kakaopay_KakaopayLiteResult" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CNSPay 결제 요청 결과 샘플 페이지</title>
</head>
<body>

    <b>결제 내역입니다.</b><br />
    <ul>
        <li>결과 내용 : [<%=(resultCode)%>] <%=resultMsg%></li>
        <li>결제 수단 : <%=(payMethod) %></li>
        <li>상품명 : <%=goodsName %></li>
        <li>금액 : <%=amt %> 원</li>
        <li>TID : <%=tid%></li>
        <li>MID : <%= mid %></li>
        <li>가맹점거래번호 : <%= moid %></li> <!-- moid = merchant_txn_num -->
        <li>카드사명 : <%= cardName %></li>
        <li>할부개월 : <%= cardQuota %></li>
        <li>카드사 코드 : <%= cardCode %></li>
        <li>무이자여부 : <%= cardInterest %></li>
        <li>체크카드여부 : <%= cardCl %></li>
        <li>카드BIN번호 : <%= cardBin %></li>
        <li>카드사포인트사용여부 : <%= cardPoint %></li>
        <li>승인일시 : <%= authDate %></li>
        <li>승인번호 : <%= authCode %></li>
        <li>부인방지토큰 : <textarea rows="5" cols="45" readonly="readonly" style="resize:none;" name="contents"><%=nonRepToken %>&nbsp;</textarea></li>
    </ul>

</body>
</html>
