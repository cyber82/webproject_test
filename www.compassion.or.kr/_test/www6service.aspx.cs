﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class _test_www6service : FrontBasePage {
    protected override void OnBeforePostBack() {

		WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();


		{

			var objSql = new object[1] { " SELECT top 1 * from tcommitmentmaster"};
			DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

			Response.Write(ds2.ToJson());
			return;
		}

		{
			Object[] objParam = new object[] { "page", "rowsPerPage", "countryCode", "gender", "birthMM", "birthDD", "minAge", "maxAge", "isOrphan", "isHandicap", "orderby" };
			Object[] objValue = new object[] { 1, 6, "", "", "","", 4 , 16,
			"N", "N" , "new"};
			Object[] objSql = new object[] { "sp_web_tChildMaster_list_f" };
			DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

			var data = new List<ChildAction.ChildItem>();
			foreach(DataRow dr in dt.Rows) {
				data.Add(new ChildAction.ChildItem() {
					IsOrphan = dr["Orphan"].ToString() == "Y",
					IsHandicapped = dr["handicapped"].ToString() == "Y",
					WaitingDays = Convert.ToInt32(dr["WaitingDays"]),
					Age = Convert.ToInt32(dr["childAge"]),
					BirthDate = Convert.ToDateTime(dr["BirthDate"]),
					CountryCode = dr["countryCode"].ToString(),
					CountryName = dr["countryName"].ToString(),
					ChildMasterId = dr["childMasterId"].ToString(),
					ChildKey = dr["ChildKey"].ToString(),
					NameKr = dr["NameKr"].ToString(),
					NameEn = dr["NameEng"].ToString(),
					Name = dr["Name"].ToString(),
					Gender = dr["childGender"].ToString(), // 남자,여자
					Pic = ChildAction.GetPicByChildKey(dr["ChildKey"].ToString()),
				});
			}

			Response.Write(data.ToJson());
		}
	}


}