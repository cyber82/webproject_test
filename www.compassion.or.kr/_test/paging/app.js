
angular.module('myApp', ["bw.paging"]);

angular.module('myApp').controller('sampleCtrl', ['$scope', '$log', function($scope, $log) {

	$scope.DoCtrlPagingAct = function (text, page, pageSize, total) {
        $log.info({
            text: text,
            page: page,
            pageSize: pageSize,
            total: total
        });
    };

}]);
