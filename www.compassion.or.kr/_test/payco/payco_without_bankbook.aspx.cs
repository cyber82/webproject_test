﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Newtonsoft.Json.Linq;

	//-----------------------------------------------------------------------------
	// PAYCO 무통장입금 처리 통보 API 페이지 샘플 ( ASP.NET )
	// payco_without_bankbook.aspx
	// 2015-03-25	PAYCO기술지원 <dl_payco_ts@nhnent.com>
	//-----------------------------------------------------------------------------


    public partial class _test_payco_without_bankbook : System.Web.UI.Page
    {
        protected string Result;

        private payco_util pu = new payco_util();

        protected void Page_Load(object sender, EventArgs e)
        {
	        //-----------------------------------------------------------------------------
	        // 이 문서는 x-www-form-urlencoded 형태의 데이터를 반환합니다. ( OK 또는 ERROR 만 반환 )
	        //-----------------------------------------------------------------------------
	        Response.ContentType = "text/html";

	        //-----------------------------------------------------------------------------
	        // (로그) 호출 시점과 호출값을 파일에 기록합니다.
	        //-----------------------------------------------------------------------------
	        string receive_str;
            receive_str = "payco_without_bankbook.aspx is Called - ";
            foreach (string key in Request.Form.Keys)
            {
                receive_str += key + " : " + Request.Form[key] + ", ";
            }
            pu.Write_Log(receive_str);

	        //-----------------------------------------------------------------------------
	        // 오류가 발생했는지 기억할 변수와 결과를 담을 변수를 선언합니다.
	        //-----------------------------------------------------------------------------
            
	        bool ErrBoolean;
            string readValue, resultValue;
	        ErrBoolean = true;		//미리 오류라고 가정

	        readValue = Request.Form["response"];

	        //-----------------------------------------------------------------------------
	        // POST 값 중 response 값이 없으면 에러를 표시하고 API를 종료합니다.
	        //-----------------------------------------------------------------------------
	        if( readValue.Equals("") ) {
		        resultValue = "Parameter is nothing.";
		        pu.Write_Log("payco_without_bankbook.asp send Result : ERROR (" + resultValue + ")");
		        Response.Write("ERROR");
		        Response.End();
	        }

            try {
	            //-----------------------------------------------------------------------------
	            // Payco 에서 송신하는 값(response)을 JSON 형태로 변경
	            // 데이터 확인에 필요한 값을 변수에 담아 처리합니다.
	            //-----------------------------------------------------------------------------
	            JObject Read_Data;
	            Read_Data = new JObject(readValue);
	            pu.Write_Log("without_bankbook_api.aspx receive json data : " + Read_Data.ToString());

	            //-----------------------------------------------------------------------------
	            // 이곳에 가맹점에서 필요한 데이터 처리를 합니다.
	            //-----------------------------------------------------------------------------

		            //-----------------------------------------------------------------------------
		            // 수신 데이터 사용 예제( 주문서 찾기 )
		            //-----------------------------------------------------------------------------
		            string sellerOrderReferenceKey, reserveOrderNo, orderNo, memberName;
		            sellerOrderReferenceKey = Read_Data["sellerOrderReferenceKey"].ToString();			//가맹점에서 발급하는 주문 연동 Key
		            reserveOrderNo = Read_Data["reserveOrderNo"].ToString();							//주문예약번호
		            orderNo = Read_Data["orderNo"].ToString();											//주문번호
		            memberName = Read_Data["memberName"].ToString();									//주문자명
		            //-----------------------------------------------------------------------------
		            // ...
		            // 기타 주문서 생성에 필요한 정보를 가지고 주문서를 조회합니다.
		            // 예) 무통장 입금 확인 필드 업데이트
		            //-----------------------------------------------------------------------------
		            string paymentCompletionYn;
		            paymentCompletionYn = Read_Data["paymentCompletionYn"].ToString();					//지급완료 값 ( Y/N )
		            if( paymentCompletionYn.Equals("Y") ) {
			            //-----------------------------------------------------------------------------
			            //지급이 완료 되었다고 받았으면 지급 완료 처리
			            //-----------------------------------------------------------------------------
			            ErrBoolean = false;		//정상처리를 위해 오류 표시를 해제
		            }

	            //-----------------------------------------------------------------------------
	            // 결과값을 생성
	            //-----------------------------------------------------------------------------
	            if( ErrBoolean ) {
		            resultValue = "ERROR";	//오류가 있으면 ERROR를 설정
	            } else {
		            resultValue = "OK";		//오류가 없으면 OK 설정
	            }

	            //-----------------------------------------------------------------------------
	            //오류일 경우 상세내역을 기록
	            //-----------------------------------------------------------------------------
                if (resultValue.Equals("ERROR")) {
		            pu.Write_Log("payco_without_bankbook.aspx has item error : Couldn't find order.");		//오류 상세 내역을 이곳에 표시합니다. ( DB 및 주문서 찾기등 오류)
	            }
	            //-----------------------------------------------------------------------------

            } catch(Exception ex) {
	            //-----------------------------------------------------------------------------
	            // 상단 On Error Resume Next 가 설정되어 있다면 아래 If문 주석을 해제하여 오류값을 리턴합니다.
	            //-----------------------------------------------------------------------------
	            resultValue = "ERROR";
	            pu.Write_Log("payco_without_bankbook.aspx has logical error : Number - 9999, Description - " + ex.Message);
            }

	        //-----------------------------------------------------------------------------
	        // 결과를 PAYCO 쪽에 리턴
	        //-----------------------------------------------------------------------------
	        Response.Write(resultValue);
        }
    }
