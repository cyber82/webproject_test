﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Newtonsoft.Json.Linq;
using CommonLib;
using payco_easypay_pay1_csharp;

public partial class _test_payco_index : System.Web.UI.Page
    {
        protected string AppWebPath;
        protected string WebMode;                  // USER-AGENT를 분석하여 호출 모드를 설정
        protected string CustomerOrderNumber;

        protected string serverPath;

        protected void Page_Load(object sender, EventArgs e)
        {

		
            Response.Charset = "UTF-8";

            m_f__load_env(); // 환경설정
            m_f__set_ordr_no();
        }

        private void m_f__load_env()
        {
		
			ConfigurationManager.AppSettings.Set("Write_LogFile", string.Format(ConfigurationManager.AppSettings["Write_LogFile"], String.Format("{0:yyyyMMdd}", DateTime.Now)));


			payco_util pu = new payco_util();
		pu.Write_Log("test");
	}

        private void m_f__set_ordr_no()
        {
            CustomerOrderNumber = "TEST" + String.Format("{0:yyyyMMddhhmmss}", DateTime.Now);
        }
    }
