﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Newtonsoft.Json.Linq;

    public partial class _test_payco_callback : System.Web.UI.Page
    {

        protected string Result;
        protected string WebMode;                     //USER-AGENT를 분석하여 호출 모드를 설정

        private payco_util pu = new payco_util();

        protected void Page_Load(object sender, EventArgs e)
        {

		Response.Write("OK");
		return;
		ErrorLog.Write(this.Context , 0 , "_test_payco_callback!!!" );
            //-----------------------------------------------------------------------------
            // 이 문서는 text/html 형태의 데이터를 반환합니다. ( OK 또는 ERROR 만 반환 )
            //-----------------------------------------------------------------------------
            Response.ContentType = "text/html";

            //-----------------------------------------------------------------------------
            // (로그) 호출 시점과 호출값을 파일에 기록합니다.
            //-----------------------------------------------------------------------------
            string receive_str = "payco_callback.aspx is Called - ";

            foreach (string key in Request.Form.Keys)
            {
                receive_str += key + " : " + Request.Form[key] + ", ";
            }
            pu.Write_Log(receive_str);


		Response.Charset = "UTF-8";

		//-----------------------------------------------------------------------------
		// USER-AGENT 구분
		//-----------------------------------------------------------------------------
		WebMode = Request.UserAgent.ToLower();

		if(!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0)) {
			WebMode = "MOBILE";
		} else {
			WebMode = "PC";
		}
		EXEC_API();
	}

        protected void EXEC_API()
        {
	        //-----------------------------------------------------------------------------
	        // 오류가 발생했는지 기억할 변수와 결과를 담을 변수를 선언합니다.
	        //-----------------------------------------------------------------------------
	        Boolean ErrBoolean;
            string readValue, resultValue;
	        ErrBoolean = false;											//기본적으로 오류가 아닌것으로 설정

	        readValue = Request.Form["response"];
	        //-----------------------------------------------------------------------------
	        // response 값이 없으면 에러(ERROR)를 돌려주고 로그를 기록한 뒤 API를 종료합니다.
	        //-----------------------------------------------------------------------------
	        if( readValue.Equals("") ) {
		        resultValue = "Parameter is nothing.";
                pu.Write_Log("payco_callback.aspx send Result : ERROR (" + resultValue + ")");
		        Response.Write("ERROR");
		        Response.End();
	        }

            try {
	            //-----------------------------------------------------------------------------
	            // Payco 에서 송신하는 값(response)을 JSON 형태로 변경
	            // 데이터 확인에 필요한 값을 변수에 담아 처리합니다.
	            //-----------------------------------------------------------------------------
                JObject Read_Data = JObject.Parse(readValue);
                pu.Write_Log("payco_callback.aspx receive json data : " + Read_Data.ToString());			// 디버그용

	            //-----------------------------------------------------------------------------
	            // 이곳에 가맹점에서 필요한 데이터 처리를 합니다.
	            // 예) 재고 체크, 매출금액 확인, 주문서 생성 등등
	            //-----------------------------------------------------------------------------
		        //-----------------------------------------------------------------------------
		        // 수신 데이터 사용 예제( 재고 체크 )
		        //-----------------------------------------------------------------------------
		        string ItemCode = "", ItemName = "";
                int ItemStock;

                foreach (JToken orderProducts in Read_Data["orderProducts"])
                {
			        ItemCode = orderProducts["orderProductNo"].ToString();	//상품 코드
			        //-----------------------------------------------------------------------------
			        // ItemCode 로 DB 에서 재고 수량 체크
			        // ( DB 에서 상품명과 재고를 읽어와 ItemName과 ItemStock 에 넣었다고 가정 )
			        //-----------------------------------------------------------------------------
			        ItemName = orderProducts["sellerOrderProductReferenceKey"].ToString();
			        ItemStock = 10;					//연동 실패를 테스트 하시려면 값을 0 으로 설정하시고 정상으로 테스트 하시려면 1보다 큰 값을 넣으세요.
			        if( ItemStock < 1 ) {			//재고가 1보다 작다면 오류로 설정
				        ErrBoolean = true;
				        break;
			        }
                }

                string serviceUrlParam1, serviceUrlParam2, serviceUrlParam3;
                //주문예약시 전달한 serviceUrlParam 의 처리
                //---------------------------------------------------------------------------------
                //주문예약시 MY_DATA, MY_DATA2, MY_DATA3 처럼 값을 여러개로 보냈을때 읽는 방법
                serviceUrlParam1 = Request["SAMPLE_SERVICE_PARAM1"];                                   // 주문예약시 전달한 serviceUrlParam ( "SAMPLE_SERVICE_PARAM1",  CStr("001001") 을 전송했었음. )
                serviceUrlParam2 = Request["SAMPLE_SERVICE_PARAM2"];                                   // 주문예약시 전달한 serviceUrlParam ( "SAMPLE_SERVICE_PARAM2",  CStr("001002") 을 전송했었음. )
                serviceUrlParam3 = Request["SAMPLE_SERVICE_PARAM3"];                                   // 주문예약시 전달한 serviceUrlParam ( "SAMPLE_SERVICE_PARAM3",  CStr("001003") 을 전송했었음. )
                //---------------------------------------------------------------------------------

                //-----------------------------------------------------------------------------
		        // 수신 데이터 사용 호출 예제( 주문서 데이터 )
		        //-----------------------------------------------------------------------------
		        string sellerOrderReferenceKey, reserveOrderNo, orderNo, orderCertifyKey, memberName;
		        int totalOrderAmt, totalDeliveryFeeAmt, totalRemoteAreaDeliveryFeeAmt, totalPaymentAmt;

		        sellerOrderReferenceKey = Read_Data["sellerOrderReferenceKey"].ToString();					// 가맹점에서 발급했던 주문 연동 Key
		        reserveOrderNo = Read_Data["reserveOrderNo"].ToString();									// PAYCO에서 발급한 주문예약번호
		        orderNo = Read_Data["orderNo"].ToString();													// PAYCO에서 발급한 주문번
		        orderCertifyKey = Read_Data["orderCertifyKey"].ToString();									// PAYCO에서 발급한 인증값
		        memberName = Read_Data["memberName"].ToString();											// 주문자명
		        totalOrderAmt = (int)Read_Data["totalOrderAmt"];									    	// 총 주문 금액
		        totalDeliveryFeeAmt = (int)Read_Data["totalDeliveryFeeAmt"];						    	// 총 배송비 금액
		        totalRemoteAreaDeliveryFeeAmt = (int)Read_Data["totalRemoteAreaDeliveryFeeAmt"];	    	// 총 추가배송비 금액
		        totalPaymentAmt = (int)Read_Data["totalPaymentAmt"];								    	// 총 결제 금액

                string orderProductNo, sellerOrderProductReferenceKey, orderProductStatusCode;
                string orderProductStatusName = "", productKindCode, productPaymentAmt, originalProductPaymentAmt;

                foreach (JObject orderProduct in Read_Data["orderProducts"])
                {
                    orderProductNo = orderProduct["orderProductNo"].ToString();                                    //주문상품번호
                    sellerOrderProductReferenceKey = orderProduct["sellerOrderProductReferenceKey"].ToString();    //가맹점에서 보낸 상품키값
                    orderProductStatusCode = orderProduct["orderProductStatusCode"].ToString();                    //주문상품상태코드
                    orderProductStatusName = orderProduct["orderProductStatusName"].ToString();                    //주문상품상태명
                    productKindCode = orderProduct["productKindCode"].ToString();                                  //상품종류코드
                    productPaymentAmt = orderProduct["productPaymentAmt"].ToString();                              //상품금액
                    originalProductPaymentAmt = orderProduct["originalProductPaymentAmt"].ToString();              //상품원금액
                }
                pu.Write_Log("orderProductStatusName : " + orderProductStatusName);               //변수 읽기 샘플

                string paymentTradeNo, paymentMethodCode, paymentAmt, paymentMethodName;
                JToken nonBankbookSettleInfo;
                string bankName, bankCode, accountNo, paymentExpirationYmd;
                JToken cardSettleInfo;
                string cardCompanyName, cardCompanyCode, cardNo = "", cardInstallmentMonthNumber;
                JToken couponSettleInfo;
                JToken realtimeAccountTransferSettleInfo;
                string discountAmt, discountConditionAmt;

                foreach (JObject paymentDetail in Read_Data["paymentDetails"])
                {
                    paymentTradeNo = paymentDetail["paymentTradeNo"].ToString();                                                //결제수단별거래번호
                    paymentMethodCode = paymentDetail["paymentMethodCode"].ToString();                                          //결제수단코드
                    paymentAmt = paymentDetail["paymentAmt"].ToString();                                                        //결제수단 사용금액
                    paymentMethodName = paymentDetail["paymentMethodName"].ToString();                                          //결제수단명
                    switch (paymentMethodCode)
                    {
                        case "02":                                                                                              //무통장입금
                            nonBankbookSettleInfo = paymentDetail["nonBankbookSettleInfo"];                                     //무통장입금 결제정보
                            bankName = nonBankbookSettleInfo["bankName"].ToString();                                            //은행명
                            bankCode = nonBankbookSettleInfo["bankCode"].ToString();                                            //은행코드 
                            accountNo = nonBankbookSettleInfo["accountNo"].ToString();                                          //계좌번호
                            paymentExpirationYmd = nonBankbookSettleInfo["paymentExpirationYmd"].ToString();                    //입금만료일 
                            break;
                        case "31":                                                                          //신용카드(일반) '신용카드
                            cardSettleInfo = paymentDetail["cardSettleInfo"];
                            cardCompanyName = cardSettleInfo["cardCompanyName"].ToString();                                  //카드사명
                            cardCompanyCode = cardSettleInfo["cardCompanyCode"].ToString();                                  //카드사코드 
                            cardNo = cardSettleInfo["cardNo"].ToString();                                                    //카드번호	
                            cardInstallmentMonthNumber = cardSettleInfo["cardInstallmentMonthNumber"].ToString();            //할부개월(MM)
                            break;
                        case "35":                                                                          //계좌이체 '바로이체
                            realtimeAccountTransferSettleInfo = paymentDetail["realtimeAccountTransferSettleInfo"];  //실시간계좌이체 결제정보
                            bankName = realtimeAccountTransferSettleInfo["bankName"].ToString();                   //은행명
                            bankCode = realtimeAccountTransferSettleInfo["bankCode"].ToString();                   //은행코드 
                            break;
                        case "76":                                                                          //쿠폰사용정보
                            couponSettleInfo = paymentDetail["couponSettleInfo"];
                            discountAmt = couponSettleInfo["discountAmt"].ToString();                                          //쿠폰사용금액
                            discountConditionAmt = couponSettleInfo["discountConditionAmt"].ToString();                        //쿠폰사용조건금액
                            break;
                        case "98":                                                                          //포인트 사용정보
                            break;
                    }
                }
                pu.Write_Log("paymentDetails's CardNumber : " + cardNo);                                       //변수 읽기 샘플


		        //-----------------------------------------------------------------------------
		        // 기타 주문서 생성에 필요한 정보를 가지고 주문서를 작성합니다.
		        // SERVICE API 가 처음 호출 되었을 때 PAYCO 주문번호로 주문서가 이미 만들어져 있다면 오류(ERROR) 입니다.
		        // SERVICE API 가 가결제건으로 인해 재 호출 되었을 때 PAYCO 주문번호로 주문서가 이미 만들어져 있다면 정상처리(OK)를 합니다.
		        //-----------------------------------------------------------------------------
                ErrBoolean = false;
	            //-----------------------------------------------------------------------------
	            // 결과값을 생성
	            //-----------------------------------------------------------------------------
	            if( ErrBoolean ) {
		            resultValue = "ERROR";	//오류가 있으면 ERROR를 설정
	            } else {
		            resultValue = "OK";		//오류가 없으면 OK 설정
	            }

	            //-----------------------------------------------------------------------------
	            //오류일 경우 상세내역을 기록하고 전체 취소 API( payco_cancel.asp )를 호출 합니다.
	            //-----------------------------------------------------------------------------
	            if( resultValue.Equals("ERROR") ) {
                    pu.Write_Log("payco_callback.aspx is Item Error : Item - " + ItemName + " 상품의 재고 부족으로 연동 오류 발생");		//오류 내용을 기록 합니다.
		            //---------------------------------------------------------------------------------
		            // 결제 취소 API 호출 ( PAYCO 에서 받은 결제정보를 이용해 전체 취소를 합니다. )
		            // 취소 내역을 담을 JSON OBJECT를 선언합니다.
		            //-----------------------------------------------------------------------------
		            JObject cancelOrder;

		            cancelOrder = new JObject();
		            cancelOrder.Add("sellerKey", JToken.FromObject(ConfigurationManager.AppSettings["sellerKey"]));								//가맹점 코드. payco_config.asp 에 설정 (필수)
			        cancelOrder.Add("sellerOrderReferenceKey", JToken.FromObject(sellerOrderReferenceKey));	//취소주문연동키. ( 파라메터로 넘겨 받은 값 ) (필수)
			        cancelOrder.Add("cancelTotalAmt", JToken.FromObject(totalPaymentAmt));					//주문서의 총 금액을 입력합니다. (전체취소, 부분취소 전부다) (필수)
			        cancelOrder.Add("orderCertifyKey", JToken.FromObject(orderCertifyKey));					//PAYCO에서 발급한 인증값 (필수)

                    pu.Write_Log("payco_callback.aspx is Item Error : try cancel : " + cancelOrder.ToString());

		            Result = pu.payco_cancel(cancelOrder.ToString());
                    pu.Write_Log("payco_callback.aspx cancel API Result : " + Result);
				//-----------------------------------------------------------------------------
				// 취소 결과가 오류인 경우 가결제건이 생성되니 PAYCO로 문의 부탁드립니다.
				//-----------------------------------------------------------------------------
					if(Result.Equals("200")) {
						//취소 결과가 정상
					} else {
							//취소 실패
		            }
	            }

            } catch(Exception ex) {
                resultValue = "ERROR";
                pu.Write_Log("payco_callback.aspx Logical Error : Number - 9999, Description - " + ex.Message);
	            Response.Write(resultValue);
	            Response.End();
            }

	        //-----------------------------------------------------------------------------
	        // 결과값을 파일에 기록한다.( 디버그용 )
	        //-----------------------------------------------------------------------------
	        pu.Write_Log("payco_service.aspx send result : " + resultValue);
	        //-----------------------------------------------------------------------------

	        //-----------------------------------------------------------------------------
	        // 결과를 PAYCO 쪽에 리턴 ( OK 또는 ERROR )
	        //-----------------------------------------------------------------------------
	        Response.Write(resultValue);
        }
    }
