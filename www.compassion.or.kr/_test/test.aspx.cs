﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Helpers;

public partial class _test_test : FrontBasePage {

    public _test_test()
    {
        

    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        GetPosts(45, "");


    }




    protected override void OnBeforePostBack() {
		
		Response.Write("19790515".Encrypt());
		return;
        var phone = "";
        var mobile = "";
        var dash_phone = "";
        var phone_type = "";

        
    }

    string GetAccessToken()
    {

        string result = "";

        var facebookAppId = ConfigurationManager.AppSettings["facebookAppId"];
        var facebookAppSecret = ConfigurationManager.AppSettings["facebookAppSecret"];

        string url = string.Format("https://graph.facebook.com/oauth/access_token?client_id={0}&client_secret={1}&grant_type=client_credentials&redirect_uri=", facebookAppId, facebookAppSecret);

        using (WebClient wc = new WebClient())
        {
            wc.Encoding = System.Text.Encoding.UTF8;
            result = wc.DownloadString(url).Trim();
            return result;
        }
    }

    public void GetPosts(int size, string pageVal)
    {
        string result = "";
        try
        {

            string url = pageVal;

            if (string.IsNullOrEmpty(pageVal))
            {
                string key = "";
                string token = GetAccessToken();
                dynamic data = Json.Decode(token);
                foreach ( KeyValuePair<string, object> item in data)
                {
                    if (string.IsNullOrEmpty(key))
                        key = item.Value.ToString();
                }
                key = "access_token=" + key;
                url = string.Format(ConfigurationManager.AppSettings["facebookPostPath"], size, key);
                //url = "https://graph.facebook.com/compassion.Korea/posts?fields=message,full_picture,created_time,link,likes,name,comments,object_id&limit=10&access_token=896920290412806|4mMlCTqWABxuhbSiFkiGVa8EWkE";
            }

            div_test.InnerText += url;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = System.Text.Encoding.UTF8;

                string str = wc.DownloadString(url);
                div_test.InnerText += str;
                //context.Response.Write(str);
                dynamic data = Json.Decode(str);

                var items = new List<SocialData.Post>();

                foreach (var item in data.data)
                {

                    var id = item.id.IndexOf('_') > -1 ? item.id.Split('_')[1] : item.id;

                    items.Add(new SocialData.Post()
                    {
                        provider = "facebook",
                        content = item.message,
                        link = item.link,
                        //picture = item.picture,
                        picture = item.full_picture,
                        //author = item.from.name,
                        regdate = DateTime.Parse(item.created_time),
                        likeCount = item.likes != null ? item.likes.data.Length : 0,
                        commentCount = item.comments != null ? item.comments.data.Length : 0,
                        //id = item.id
                        //id = item.object_id
                        id = id
                        //rawdata = Json.Encode(item)	// 성능을 위해 일단 막음
                    });
                }

                //return new SocialData()
                //{
                //    next = data.paging.next,
                //    prev = data.paging.previous,
                //    items = items
                //};

            }
        }
        catch (Exception ex)
        {
            div_test.InnerText += ex.Message;
        }



    }

}