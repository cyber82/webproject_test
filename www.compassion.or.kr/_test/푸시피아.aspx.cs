﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Configuration;
using System.Text;

public partial class _test_푸시피아 : FrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack(){
		
	}
	
	protected void btn_submit_Click( object sender, EventArgs e ) {

		var res = Pushpia.Send(bizId.Value, title.Value, message.Value, "", custId.Value);

		Response.Write(res.ToJson());
	}

}