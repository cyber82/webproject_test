﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_test_moment_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
     <script src="/@mgt/template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="../../@mgt/common/js/moment.min.js"></script>
    <script src="../../@mgt/common/js/moment-timezone.min.js"></script>
    <script>
        $(function () {

            moment.tz.add('America/Los_Angeles|PST PDT|80 70|0101|1Lzm0 1zb0 Op0');
            moment.tz.add('Asia/Seoul|LMT KST JCST JST KST KDT KDT|-8r.Q -8u -90 -90 -90 -9u -a0|01234151515151515146464|-2um8r.Q 97XV.Q 12FXu jjA0 kKo0 2I0u OL0 1FB0 Rb0 1qN0 TX0 1tB0 TX0 1tB0 TX0 1tB0 TX0 2ap0 12FBu 11A0 1o00 11A0|23e6');



            var date = moment().tz("Asia/Seoul");
            console.log(moment().toDate());
            console.log(date.toDate());

            /*
            var a = moment.tz("2013-11-18 11:55", "America/Los_Angeles");
            var b = moment.tz("May 12th 2014 8PM", "MMM Do YYYY hA", "America/Los_Angeles");
            var c = moment.tz(1403454068850, "America/Los_Angeles");
            console.log(a.format()); // 2013-11-18T11:55:00-05:00
            console.log(b.format()); // 2014-05-12T20:00:00-04:00
            console.log(c.format()); // 2014-06-22T12:21:08-04:00
            */
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
