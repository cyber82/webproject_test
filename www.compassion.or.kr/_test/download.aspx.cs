﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _test_download : System.Web.UI.Page {
	protected void Page_Load( object sender, EventArgs e ) {

		if (Request["a"] == "a") {
			btn_download_Click(null , null);
		}

	}


	protected void btn_download_Click( object sender, EventArgs e ) {

		Response.ContentType = "application/x-zip-compressed";
		Response.AppendHeader("Content-Disposition", "attachment; filename=test.avi");

		Response.TransmitFile(@"D:\temp\sample.avi");

		/*
		for(int i = 0; i < 10; i++) {
			Response.TransmitFile(@"D:\temp\sample.avi", 2048 * i , 2048 );
		}
		*/

		Response.End();

	}
}