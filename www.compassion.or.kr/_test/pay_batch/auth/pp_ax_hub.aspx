﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pp_ax_hub.aspx.cs" Inherits="KCP.PP_CLI_COM.SRC.pp_cli_com" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>*** KCP Online Payment System [ASP.NET Version] ***</title>

    <script type="text/javascript">
            function goResult()
            {
                document.pay_info.submit();
            }

            // 결제 중 새로고침 방지 샘플 스크립트
            function noRefresh()
            {
                /* CTRL + N키 막음. */
                if ((event.keyCode == 78) && (event.ctrlKey == true))
                {
                    event.keyCode = 0;
                    return false;
                }
                /* F5 번키 막음. */
                if(event.keyCode == 116)
                {
                    event.keyCode = 0;
                    return false;
                }
            }
            document.onkeydown = noRefresh ;

    </script>
</head>
<body onload="goResult();">
    <form name="pay_info" method="post" action="./result">
        <input  type="hidden"  name="res_cd"      value="<%= res_cd    %>" />  <!-- 결과코드 -->
        <input  type="hidden"  name="res_msg"     value="<%= res_msg   %>" />  <!-- 결과메시지 -->
        <input  type="hidden"  name="ordr_idxx"	  value="<%= ordr_idxx %>" />  <!-- 주문번호 -->
        <input  type="hidden"  name="buyr_name"   value="<%= buyr_name %>" />  <!-- 주문자명 -->
        <input  type="hidden"  name="card_cd"     value="<%= card_cd   %>" />  <!-- 카드코드  -->
        <input  type="hidden"  name="batch_key"   value="<%= batch_key %>" />  <!-- 배치 인증키-->

    </form>
</body>
</html>

