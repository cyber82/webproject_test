﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

using KCP.PP_CLI_COM.LIB;

public partial class _test_pay_batch_test_pp_ax_hub : System.Web.UI.Page {


	WWWService.Service _wwwService = new WWWService.Service();
	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
	
	protected void Page_Load( object sender, EventArgs e ) {
		
		var sess = new UserInfo();
		DataSet dsResult;
		string site_cd = ConfigurationManager.AppSettings["g_conf_batch_site_cd"];
		string site_key = ConfigurationManager.AppSettings["g_conf_batch_site_key"];
		string bt_group_id = ConfigurationManager.AppSettings["kcpgroup_id"];
		string sPaymentType = m_f__get_post_data("pay_method");
		string tran_cd = m_f__get_post_data("tran_cd");
		string payday = m_f__get_post_data("payday");
		string trad_numb = m_f__get_post_data("trace_no");
		string enct_info = m_f__get_post_data("enc_info");
		string enct_data = m_f__get_post_data("enc_data");
		string req_tx = m_f__get_post_data("req_tx");
		string ordr_idxx = m_f__get_post_data("ordr_idxx");
		string buyr_name = m_f__get_post_data("buyr_name");
		string m_strCustIP = Request.ServerVariables.Get("REMOTE_ADDR");

		dsResult = _wwwService.IssueCardCMS(req_tx, sPaymentType, ordr_idxx, buyr_name, bt_group_id
													, site_cd, site_key, trad_numb, tran_cd, enct_info
													, enct_data, m_strCustIP, "05", "", "0003"
													, sess.SponsorID, "", sess.UserId, sess.UserName, "Web");

		string[] sResult = new string[3];

		//getResult
		sResult[0] = dsResult.Tables[0].Rows[0]["ResCd"].ToString();
		sResult[1] = dsResult.Tables[0].Rows[0]["ResMsg"].ToString();
		sResult[2] = ordr_idxx;

		string bt_batch_key = dsResult.Tables[0].Rows[0]["BatchKey"].ToString();
		//===== 결제성공시
		if(sResult[0] == "0000") {

			Response.Write("bt_batch_key > " + bt_batch_key + "<br>");
			Response.Write(dsResult.Tables[0].Rows[0].ToJson() + "<<<");

			var payResult = new KCPBatchPay().Pay("" , bt_batch_key , ordr_idxx, "한국컴패션후원금", "1000", "위재일", "", "", "");
			Response.Write(payResult.ToJson());

			if(payResult.success) {

				

			}

		} else {

			Response.Write("error");
			
		}
		
	}


	/* ==================================================================================== */
	/* +    METHOD : GET POST DATA                                                        + */
	/* - -------------------------------------------------------------------------------- - */
	private string m_f__get_post_data( string parm_strName ) {
		string strRT;

		strRT = Request.Form[parm_strName];

		if(strRT == null)
			strRT = "";

		return strRT;
	}
	/* ==================================================================================== */



}
