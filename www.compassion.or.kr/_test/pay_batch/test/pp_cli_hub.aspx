﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pp_cli_hub.aspx.cs" Inherits="_test_pay_batch_test_pp_cli_com" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>*** KCP Online Payment System [ASP.NET Version] ***</title>

    <script type="text/javascript">
            function goResult()
            {
                document.pay_info.submit();
            }

            // 결제 중 새로고침 방지 샘플 스크립트
            function noRefresh()
            {
                /* CTRL + N키 막음. */
                if ((event.keyCode == 78) && (event.ctrlKey == true))
                {
                    event.keyCode = 0;
                    return false;
                }
                /* F5 번키 막음. */
                if(event.keyCode == 116)
                {
                    event.keyCode = 0;
                    return false;
                }
            }
            document.onkeydown = noRefresh ;

    </script>
</head>
<body onload="goResult();">
    <form name="pay_info" method="post" action="./result">
            <input type="hidden" name="req_tx"     value="<%= req_tx     %>">  <!-- 요청 구분 -->
            <input type="hidden" name="pay_method" value="<%= pay_method %>">  <!-- 사용한 결제 수단 -->
            <input type="hidden" name="bSucc"      value="<%= bSucc      %>">  <!-- 쇼핑몰 DB 처리 성공 여부 -->
            <input type="hidden" name="mod_type"   value="<%= mod_type   %>">
            <input type="hidden" name="amount"     value="<%= amount     %>">  <!-- 총 금액 -->
            <input type="hidden" name="panc_mod_mny"   value="<%=panc_mod_mny%>">  <!-- 부분취소 요청금액 -->
            <input type="hidden" name="panc_rem_mny"   value="<%=panc_rem_mny%>">  <!-- 부분취소 가능금액 -->

            <input type="hidden" name="res_cd"     value="<%= res_cd     %>">  <!-- 결과 코드 -->
            <input type="hidden" name="res_msg"    value="<%= res_msg    %>">  <!-- 결과 메세지 -->
            <input type="hidden" name="ordr_idxx"  value="<%= ordr_idxx  %>">  <!-- 주문번호 -->
            <input type="hidden" name="tno"        value="<%= tno        %>">  <!-- KCP 거래번호 -->
            <input type="hidden" name="good_mny"   value="<%= good_mny   %>">  <!-- 결제금액 -->
            <input type="hidden" name="good_name"  value="<%= good_name  %>">  <!-- 상품명 -->
            <input type="hidden" name="buyr_name"  value="<%= buyr_name  %>">  <!-- 주문자명 -->
            <input type="hidden" name="buyr_tel1"  value="<%= buyr_tel1  %>">  <!-- 주문자 전화번호 -->
            <input type="hidden" name="buyr_tel2"  value="<%= buyr_tel2  %>">  <!-- 주문자 휴대폰번호 -->
            <input type="hidden" name="buyr_mail"  value="<%= buyr_mail  %>">  <!-- 주문자 E-mail -->

            <input type="hidden" name="card_cd"    value="<%= card_cd    %>">  <!-- 카드코드 -->
            <input type="hidden" name="card_no"    value="<%= card_no    %>">  <!-- 카드번호 -->
            <input type="hidden" name="card_name"  value="<%= card_name  %>">  <!-- 카드명 -->
            <input type="hidden" name="app_time"   value="<%= app_time   %>">  <!-- 승인시간 -->
            <input type="hidden" name="app_no"     value="<%= app_no     %>">  <!-- 승인번호 -->
            <input type="hidden" name="quota"      value="<%= quota      %>">  <!-- 할부개월 -->
            <input type="hidden" name="noinf"      value="<%= noinf      %>">  <!-- 무이자여부 -->
    </form>
</body>
</html>