﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _test_handicap : FrontBasePage {

	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

	protected override void OnBeforePostBack() {



		var a = this.GetHandicapTranslate("0700375684");

		Response.Write(a);

	}
	
	string GetHandicapTranslate( string childMasterId ) {

		#region src
		try {

			string sTranslationString = "";
			Object[] objSql = new object[1] { "sp_web_child_handicap_get_f" };
			Object[] objParam = new object[] { "childMasterId" };
			Object[] objValue = new object[] { childMasterId };


			var ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
			var dtHealth = ds.Tables[0];
			var sHealth = ds.Tables[1].Rows[0]["studyContent"].ToString();

			var saHealth = sHealth != "" ? sHealth.Split(',') : null;
			
			//- saHH의 배열값이 없을 경우
			if(saHealth == null) {
				return "";
			}

			string[] saHealth0; //for
			string[] saHealth5;
			string[] saHealth6;
			string[] saHealth7;
			string[] saHealth8;
			string[] saHealth9;
			string[] saHealth10;
			string[] saHealth11;
			string[] saHealth12;
			string[] saHealth13;
			string[] saHealth14;
			string[] saHealth15;
			string[] saHealth16;
			string[] saHealth17;
			string[] saHealth18;
			string[] saHealth19;
			string[] saHealth20;


			//- saHealth : Epilepsy, Asthma, Polio, developmentally disabled

			for(int i = 0; i < 4; i++) //saHH의 1~4번만
			{
				saHealth0 = saHealth[i].Split(':');
				if(saHealth0[1].ToString().Trim() == "T") //2016-07-11 문지예
				{
					dtHealth.DefaultView.RowFilter = "CodeID = '" + saHealth0[0].Trim() + "'";
					sTranslationString += " " + dtHealth.DefaultView[0]["CodeName"].ToString();
				}

			}

			
			//- saHealth : Spine due to
			saHealth5 = saHealth[5].Split(':');
			if(saHealth5[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 척추골 장애가 있습니다.";
			}

			//- saHealth : Left Foot due to, Right Foot due to
			saHealth6 = saHealth[6].Split(':');
			saHealth7 = saHealth[7].Split(':');
			if(saHealth6[1].ToLower().Contains("normal") == false && saHealth7[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 양쪽 발에 장애가 있습니다.";
			} else if(saHealth6[1].ToLower().Contains("normal") == false && saHealth7[1].ToLower().Contains("normal") == true) {
				sTranslationString += " 왼발에 장애가 있습니다.";
			} else if(saHealth6[1].ToLower().Contains("normal") == true && saHealth7[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 오른발에 장애가 있습니다.";
			}

			//- saHealth : Left Hand due to, Right Hand due to
			saHealth8 = saHealth[8].Split(':');
			saHealth9 = saHealth[9].Split(':');
			if(saHealth8[1].ToLower().Contains("normal") == false && saHealth9[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 양쪽 손에 장애가 있습니다.";
			} else if(saHealth8[1].ToLower().Contains("normal") == false && saHealth9[1].ToLower().Contains("normal") == true) {
				sTranslationString += " 왼손에 장애가 있습니다.";
			} else if(saHealth8[1].ToLower().Contains("normal") == true && saHealth9[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 오른손에 장애가 있습니다.";
			}

			//- saHealth : Left Leg due to, Right Leg due to
			saHealth10 = saHealth[10].Split(':');
			saHealth11 = saHealth[11].Split(':');
			if(saHealth10[1].ToLower().Contains("normal") == false && saHealth11[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 양쪽 다리에 장애가 있습니다.";
			} else if(saHealth10[1].ToLower().Contains("normal") == false && saHealth11[1].ToLower().Contains("normal") == true) {
				sTranslationString += " 왼다리에 장애가 있습니다.";
			} else if(saHealth10[1].ToLower().Contains("normal") == true && saHealth11[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 오른다리에 장애가 있습니다.";
			}

			//- saHealth : Normal, Left Arm due to, Right Arm due to
			saHealth12 = saHealth[12].Split(':');
			saHealth13 = saHealth[13].Split(':');
			if(saHealth12[1].ToLower().Contains("normal") == false && saHealth13[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 양쪽 팔에 장애가 있습니다.";
			} else if(saHealth12[1].ToLower().Contains("normal") == false && saHealth13[1].ToLower().Contains("normal") == true) {
				sTranslationString += " 왼팔에 장애가 있습니다.";
			} else if(saHealth12[1].ToLower().Contains("normal") == true && saHealth13[1].ToLower().Contains("normal") == false) {
				sTranslationString += " 오른팔에 장애가 있습니다.";
			}

			//- saHealth : Speech
			saHealth14 = saHealth[14].Split(':');
			if(saHealth14[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 언어 장애가 있습니다.";
			} else if(saHealth14[1].ToLower().Contains("mute") == true) {
				sTranslationString += " 말을 하지 못합니다.";
			}

			//- saHealth : Hearing Left Ear, Hearing Right Ear
			saHealth15 = saHealth[15].Split(':');
			saHealth16 = saHealth[16].Split(':');
			if(saHealth15[1].ToLower().Contains("deaf") == true && saHealth16[1].ToLower().Contains("deaf") == true) {
				sTranslationString += " 소리를 듣지 못합니다.";
			} else if(saHealth15[1].ToLower().Contains("defective") == true && saHealth16[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 청각 장애가 있습니다.";
			} else if(saHealth15[1].ToLower().Contains("deaf") == true && saHealth16[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 왼쪽 귀가 들리지 않고 오른쪽 청각에도 장애가 있습니다.";
			} else if(saHealth15[1].ToLower().Contains("defective") == true && saHealth16[1].ToLower().Contains("deaf") == true) {
				sTranslationString += " 오른쪽 귀가 들리지 않고 왼쪽 청각에도 장애가 있습니다.";
			} else if(saHealth15[1].ToLower().Contains("deaf") == true) {
				sTranslationString += " 왼쪽 귀가 들리지 않습니다.";
			} else if(saHealth15[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 왼쪽 청각에 장애가 있습니다.";
			} else if(saHealth16[1].ToLower().Contains("deaf") == true) {
				sTranslationString += " 오른쪽 귀가 들리지 않습니다.";
			} else if(saHealth16[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 오른쪽 청각에 장애가 있습니다.";
			}

			//- saHealth : Sight Left Eye, Sight Right Eye
			saHealth17 = saHealth[17].Split(':');
			saHealth18 = saHealth[18].Split(':');
			if(saHealth17[1].ToLower().Contains("blind") == true && saHealth18[1].ToLower().Contains("blind") == true) {
				sTranslationString += " 앞을 보지 못합니다.";
			} else if(saHealth17[1].ToLower().Contains("defective") == true && saHealth18[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 시각 장애가 있습니다.";
			} else if(saHealth17[1].ToLower().Contains("blind") == true && saHealth18[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 왼쪽 눈을 볼 수 없고 오른쪽 눈에도 장애가 있습니다.";
			} else if(saHealth17[1].ToLower().Contains("defective") == true && saHealth18[1].ToLower().Contains("blind") == true) {
				sTranslationString += " 오른쪽 눈을 볼 수 없고 왼쪽 눈에도 장애가 있습니다.";
			} else if(saHealth17[1].ToLower().Contains("blind") == true) {
				sTranslationString += " 왼쪽 눈을 볼 수 없습니다.";
			} else if(saHealth17[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 왼쪽 눈에 장애가 있습니다.";
			} else if(saHealth18[1].ToLower().Contains("blind") == true) {
				sTranslationString += " 오른쪽 눈을 볼 수 없습니다.";
			} else if(saHealth18[1].ToLower().Contains("defective") == true) {
				sTranslationString += " 오른쪽 눈에 장애가 있습니다.";
			}

			//- saHealth : Regular medical treatmnt?, Regular medication?
			saHealth19 = saHealth[19].Split(':');
			saHealth20 = saHealth[20].Split(':');
			if(saHealth19[1].Trim() != "F" && saHealth20[1].Trim() != "F") {
				sTranslationString += " 정기적인 치료를 받고 약을 복용하고 있습니다.";
			} else {

				if(saHealth19[1].Trim() != "F") {
					dtHealth.DefaultView.RowFilter = "CodeID = '" + saHealth19[0].Trim() + "'";
					sTranslationString += " " + dtHealth.DefaultView[0]["CodeName"].ToString();
				} else if(saHealth20[1].Trim() != "F") {
					dtHealth.DefaultView.RowFilter = "CodeID = '" + saHealth20[0].Trim() + "'";
					sTranslationString += " " + dtHealth.DefaultView[0]["CodeName"].ToString();
				}
			}

			return sTranslationString;

		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current , 0 , ex.ToString());
			return "";
		}
		#endregion

	}
}