﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pcc.aspx.cs" Inherits="_test_pcc" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:auth_domain %>/cert/cert.js"></script>
	<script type="text/javascript">
		$(function () {

			// 본인인증테스트

			// 휴대폰 인증
			$("[data-id=btn_cert_by_phone]").click(function () {
				cert_openPopup("phone" , "<%:auth_domain %>");

				return false;
			})

			// 아이핀 인증
			$("[data-id=btn_cert_by_ipin]").click(function () {
				cert_openPopup("ipin" , "<%:auth_domain %>");

				return false;
			})

		});

		// 본인인증 결과 응답
		// result = Y or N , birth = yyyyMMdd
		// sex = M or F
		// method = ipin or phone
		var cert_setCertResult = function (method, result, ci, di, name, birth, sex) {
			alert(result)
		}

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<div>
		<a href="/join/">개인회원</a>
		<a href="/join/offline/">기존 후원자</a>
		<a href="/join/company/">기업/단체 회원</a>
    </div>
	
	<hr />
	실명인증

	<input type="text" id="ci" placeholder="CI" />
	<a href="#" data-id="btn_cert_by_phone">휴대폰</a>
	<a href="#" data-id="btn_cert_by_ipin">아이핀</a>

</asp:Content>
