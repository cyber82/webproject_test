﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CommonLib;

public partial class _test_pbciv : FrontBasePage {
	protected override void OnBeforePostBack(){

		var result = new SpecialFundingAction().GetCandidateList();
		if(result.success) {

			var data = (List<tSpecialFundingEx>)result.data;
			//	Response.Write(data.ToJson());

			foreach(var entity in data) {
				pbcivList.Items.Add(new ListItem(entity.CampaignName, string.Format("{0}_{1}_{2}" , entity.AccountClassGroup , entity.AccountClass , entity.CampaignID)));
			}
			pbcivList.Items.Insert(0, new ListItem("선택하세요", ""));

		} else {

			base.AlertWithJavascript(result.message);
		}

	
	}
	
}