﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class _test_sitemap : FrontBasePage {
	protected override void OnBeforePostBack(){

		Response.ContentType = "application/json";
		Response.Clear();
		var root = SiteMap.Providers["web"].RootNode;
		
		Response.Write(root.ToMenu().ToJson());
	}
	
	
}