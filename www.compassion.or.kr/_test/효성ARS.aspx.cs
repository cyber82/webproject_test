﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Configuration;
using System.Text;

public partial class _test_효성ARS : FrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack(){
		
	}
	
	protected void btn_submit_Click( object sender, EventArgs e ) {

		//string postData = "{\"agreementAgentId\": "AGREEMENT - AGENT - 01", "memberId": "MEMBER - 01", "phone": "01012345678", "callMode": "Outbound", "paymentKind": "CMS", "paymentCompany": { "code": null, "name": "신한은행" }, "paymentNumber": "1234567890", "payerName": "홍길동", "payerNumber": "900101" } ";

		var res = new CMSARS().Request("test", "01025757728", "88", "신한은행", "24711002185", "위재일", "790515");
		Response.Write(res.ToJson());
		
		agreementAgentId.Value = res.agreementAgentId;

		/*
		Dictionary<string, object> data = new Dictionary<string, object>();
		data.Add("agreementAgentId", DateTime.Now.ToString("yyyyMMddhhmmss"));
		data.Add("memberId", "test");
		data.Add("phone", "01025757728");
		data.Add("callMode", "Outbound");
		data.Add("paymentKind", "CMS");
		data.Add("paymentCompany", new Dictionary<string, string>() { { "code" , "88" } , { "name" , "신한은행" } });
		data.Add("paymentNumber", "24711002185");
		data.Add("payerName", "위재일");
		data.Add("payerNumber", "790515");
		
		string postData = data.ToJson();
		
	//	postData = "agreementAgentId=" + DateTime.Now.ToString("yyyyMMddhhmmss") + "&memberId=test&phone=01025757728&callMode=Outbound&paymentKind=CMS&paymentCompany.code=88&paymentCompany.name=신한은행&paymentNumber=24711002185&payerName=위재일&payerNumber=790515";
		
		//ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
		using(WebClient wc = new WebClient()) {
		//	wc.Encoding = System.Text.Encoding.UTF8;
			//wc.Headers.Add("Host", "add.efnc.co.kr");
			//wc.Headers.Add("Authorization", string.Format("VAN {0}:{0} ", ConfigurationManager.AppSettings["tbProgramPw"], ConfigurationManager.AppSettings["tbComPw"]));
			wc.Headers.Add("Authorization", string.Format("VAN {0}:{1}", "4LjFflzr6z4YSknp", "BT2z4D5DUm7cE5tl"));
		//	wc.Headers.Add("Accept", "application/json");
			wc.Headers.Add("Content-Type", "application/json; charset=UTF-8");
			
			//byte[] response = wc.UploadData(string.Format("https://api.efnc.co.kr/v1/custs/{0}/agreement-agents", ConfigurationManager.AppSettings["tbComId"]), "POST", Encoding.UTF8.GetBytes(postData));
			byte[] response = wc.UploadData(string.Format("https://add.efnc.co.kr:1443/v1/custs/{0}/agreement-agents", "sdsitest"), "POST", Encoding.UTF8.GetBytes(postData));
			



			var res = Encoding.UTF8.GetString(response);
			Response.Write(res);

		}
		*/

	}

	protected void btn_verify_Click( object sender, EventArgs e ) {

		var res = new CMSARS().Verify(agreementAgentId.Value);
		Response.Write("verify > " + res.ToJson());

		switch(res.result.flag) {
			case "Y":
				Response.Write(res.result.message);
				break;
			case "N":
				Response.Write(res.result.message);
				break;
			default:	// null (대기)
				// 일정시간 후 다시 요청 , Y or N이 나올때까지 요청(timeout 필요)
				break;
		}

		/*
		using(WebClient wc = new WebClient()) {
			wc.Headers.Add("Authorization", string.Format("VAN {0}:{1}", "4LjFflzr6z4YSknp", "BT2z4D5DUm7cE5tl"));
			wc.Headers.Add("Content-Type", "application/json; charset=UTF-8");

			//byte[] response = wc.UploadData(string.Format("https://api.efnc.co.kr/v1/custs/{0}/agreement-agents", ConfigurationManager.AppSettings["tbComId"]), "POST", Encoding.UTF8.GetBytes(postData));
			string res = wc.DownloadString(string.Format("https://add.efnc.co.kr:1443/v1/custs/{0}/agreement-agents/{1}", "sdsitest" , agreementAgentId.Value));
			Response.Write("verify > " + res);


		}
		*/

	}
}