﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="childRandom.aspx.cs" Inherits="childRandom" MasterPageFile="~/top_without_header.master" %>
<%@ MasterType VirtualPath="~/top_without_header.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	<meta name="keywords" content="한국컴패션,어린이후원"  />
	<meta name="description" content="꿈을 잃은 어린이들에게 그리스도의 사랑을" />
	<meta property="og:url" content="http://www.compassionkr.com" />
	<meta property="og:title" content="한국컴패션" />
	<meta property="og:description" content="꿈을 잃은 어린이들에게 그리스도의 사랑을" />
	<meta property="og:image" content="http://www.compassion.or.kr/common/img/sns_default_image.jpg" />
</asp:Content>
<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <section class="main">
        <div class="main_recmd" id="main_recmd3" runat="server" visible="true">
                <div class="wrap">
                    <div class="w980 relative">
                        <div class="picWrap">
                            <!-- 사진사이즈 : 242 * 333 -->
                            <div class="pic"><span>
                                <img id="main_recmd3_pic" runat="server" width="242" alt="추천어린이" /></span></div>
                            <div class="info">
                                <span class="nation">
                                    <asp:Literal runat="server" ID="main_recmd3_country" /></span>
                                <span class="age">
                                    <asp:Literal runat="server" ID="main_recmd3_age" />세</span>
                                <span class="sex">
                                    <asp:Literal runat="server" ID="main_recmd3_gender" /></span>
                            </div>
                        </div>
                        <div class="textWrap">
                            <span class="title">예수님의 사랑을 생각하며<br />결연을 결심할 때입니다.</span><br />
                            <p class="con">
                                십자가의 놀라운 사랑으로 우리도 희망을 찾았습니다.<br />
                                아직 사랑을 경험해보지 못한 연약한 어린이들.<br />
                                우리가 받은 큰 사랑을,<br />
                                <asp:Literal runat="server" ID="main_recmd3_name" />에게 보내주지 않으시겠어요?
							<br />
                            </p>
                            <div>
                                <%--<a href="/sponsor/children/?c={childMasterId}" target="_blank" runat="server" id="main_recmd3_link" class="btn_more1 mr8" disabled="true">--%>
                                <a href="/sponsor/children/?c={childMasterId}" target="_blank" runat="server" id="main_recmd3_link" class="btn_more1 mr8">
                                    한 어린이 결연 결심하기</a>
                                <a href="/sponsor/children/" class="btn_more2" target="_blank">어린이 더보기</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="main_recmd" id="main_recmd3_empty" runat="server" visible="false">
            어린이가 존재하지 않습니다.
        </div>
    </section>
</asp:Content>
