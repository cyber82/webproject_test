﻿$(function () {

    $(".header .wrap").removeClass("fixed");

    // 메인 프로그램후원 탭메뉴
    var mpl = $(".mainPrgramList > li");
    mpl.click(function () {
        mpl.removeClass("on");
        $(this).addClass("on");

        var prm_num = $(this).attr("data-prm-id");
        $(".prm_con").hide();
        $(".prm_con" + prm_num).show();
        return false;
    })

    /*
	// 전체 레이어메뉴 show/hide
	gnb.js에 기능 겹침
	$(".header_bottom .menu").click(function () {
		if ($(this).hasClass("on")) {
			$(this).removeClass("on");
		}
		else { $(this).addClass("on"); }
		return false;
	})
	*/



    //브라우저 체크 
    function browerCheck(type) {

        // 재활용을 위해 "hackerc_Agent" 에 정보를 저장.
        var hackerc_Agent = navigator.userAgent;
        var result = hackerc_Agent.match('LG | SAMSUNG | Samsung | iPhone | iPod | Android | Windows CE | BlackBerry | Symbian | Windows Phone | webOS | Opera Mini | Opera Mobi | POLARIS | IEMobile | lgtelecom | nokia | SonyEricsson');

        var pc = 'http://www.';
        var mobile = 'http://m.';

        if (type == 'pc') {
            if (result == null) {
                link = location.href.replace(mobile, pc)
                location.href = link;
                return false;
            }
        } else if (type == 'mobile') {
            if (result != null) {
                //link = location.href.replace(pc, mobile)
                //location.href = link;


                (function () {

                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'http://m.compassion.or.kr/';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'http://m.compassionkr.com/';
                    else if (loc.indexOf('localhost') != -1) host = 'http://localhost:35563/';


                    var http_us = host;// "http://m.compassion.or.kr/"; //모바일이동URL
                    var http_param = [];
                    var str_param = "";
                    if (document.location.search != "") {
                        http_param.push(document.location.search.replace(/^\?/, ""));
                    }
                    if (document.referrer != "" && !/OV_REFFER/.test(document.location.search)) {
                        http_param.push("OV_REFFER=" + document.referrer);
                    }
                    if (http_param.length > 0) {
                        str_param = (/\?/.test(http_us) ? "&" : "?") + http_param.join("&");
                    } else {
                        str_param = "";
                    }
                    location.href = http_us + str_param;
                })();

                return false;
            }
        }

    }


    //모바일로 접속시 모바일 홈페이지로 이동 
    if (getParameterByName("pc") != "ok") {
        if (cookie.get("locationMobile") != 1) {
            browerCheck('mobile');
        }
    } else {
        cookie.set("locationMobile", 1, 1);
    }




    mainVisual.init();

    specialFundingVisual.init();

    //sympathyVisual.init();

    $.ajax({
        type: "get",
        url: "/api/my/child.ashx?t=list",
        data: { page: 1, rowsPerPage: 1000 },
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        dataType: "json",
        success: function (data) {
            if (data.success) {
                var bCanLetter = false;
                var d = data.data;
                $.each(d, function () {
                    if (this.canletter) { bCanLetter = true; }
                });
                $('#btn_write_letter').css('display', (bCanLetter ? 'inline-block' : 'none'));
            }
        },
        error: function (request, status, error) { }
    });

});



//쿠키제어
var cookie = {

    htmlEscape: function (str) {
        return String(str)
			.replace(/&/g, '&amp;')
			.replace(/"/g, '&quot;')
			.replace(/'/g, '&#39;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;');
    },

    set: function (name, value, expiredays) {
        var todayDate = new Date();
        todayDate.setDate(todayDate.getDate() + expiredays);
        document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
    },

    get: function (name) {
        var nameOfCookie = name + "=";
        var x = 0;
        while (x <= document.cookie.length) {
            var y = (x + nameOfCookie.length);
            if (document.cookie.substring(x, y) == nameOfCookie) {
                if ((endOfCookie = document.cookie.indexOf(";", y)) == -1)
                    endOfCookie = document.cookie.length;
                return unescape(document.cookie.substring(y, endOfCookie));
            }
            x = document.cookie.indexOf(" ", x) + 1;
            if (x == 0)
                break;
        }
        return null;
    },

    has: function (name) {
        var val = cookie.get(name);
        return val != null && val != "";

    }


}

var mainVisual = {
    animating: false,
    //wheelDownCount : 3 , 

    init: function () {

        $("body,html").bind('mousewheel', function (e) {

            if (mainVisual.animating) return false;

            if (e.deltaY < 0) {
                //	mainVisual.wheelDownCount--;
                //	if (mainVisual.wheelDownCount < 0) {
                mainVisual.moveToMain();
                //	}
            }

            return false;

        });

        if (cookie.get("mainVisual")) {
            //$("body,html").unbind('mousewheel');
        } else {
            gnb.enable = false;
            $(".intro_visual").show();
        }


        $(".intro_visual .scroll").click(function () {
            mainVisual.moveToMain();
            return false;
        }).css("cursor", "pointer");


        if (cookie.get("mainVisual")) {
            popupManager.openAll(true, "layer");

        }

    },

    moveToMain: function () {

        popupManager.openAll(true, "layer"); //팝업실행
        cookie.set("mainVisual", 1);

        mainVisual.animating = true;
        var top = $(".header").offset().top;
        $("body,html").animate({ scrollTop: top }, 1000, "easeInOutQuint", function () {
            gnb.enable = true;
            mainVisual.animating = false;
            $("body,html").unbind('mousewheel');

            $(".intro_visual").remove();
            $("body,html").scrollTop(0);

            $(".header .wrap").addClass("fixed");

        });
    }

}

var specialFundingVisual = {
    init: function () {
        var root = $("#event_container");

        // 메인비주얼
        if (root.length < 1) {
            root.find(".btn_group").hide();
            return;
        }

        if ($("#event_visual").find(".item").length < 2) {
            root.find(".btn_group").hide();
            return;
        }

        root.find("#event_visual").slidesjs({
            width: $(window).width(),
            height: $(window).height(),

            play: {

                active: true,
                effect: "slide",
                interval: 5000,
                auto: true,
                swap: true,
                pauseOnHover: false,
                restartDelay: 2500
            },

            effect: {
                slide: {
                    speed: 1000
                },
                fade: {
                    speed: 300,
                    crossfade: true
                }
            },

            callback: {
                loaded: function () {
                },
                start: function (number) {

                },
                complete: function (number) {
                    root.find(".indi_wrap button").removeClass("on");
                    $(root.find(".indi_wrap button")[number - 1]).addClass("on")
                }
            }

        });


        $.each($("#event_visual").find(".item"), function (i) {
            var item = $('<button' + (i == 0 ? ' class="on"' : '') + ' type="button">O</button>');
            item.click(function () {
                root.find('a[data-slidesjs-item=' + i + ']').trigger("click");		// 이동
            })
            root.find(".indi_wrap").append(item);
        })

        var btn_pause = $('<button type="button" class="pause">pause</button>');
        var btn_play = $('<button type="button" class="play">play</button>');
        root.find(".indi_wrap").append(btn_pause);
        root.find(".indi_wrap").append(btn_play);

        btn_pause.click(function () {
            root.find(".slidesjs-stop").trigger("click");
            return false;
        });

        btn_play.click(function () {
            root.find(".slidesjs-play").trigger("click");
            return false;
        });

        root.find(".prev").click(function () {
            root.find(".slidesjs-previous").trigger("click");
            return false;
        });

        root.find(".next").click(function () {
            root.find(".slidesjs-next").trigger("click");
            return false;
        });
    }
}

/*
var sympathyVisual = {
	init: function () {
		var root = $("#sympathy_container");
	
		// 메인비주얼
		if (root.length < 1)
			return;

		root.find("#sympathy_visual").slidesjs({
			width: 980,
			height: 446,

			play: {

				active: true,
				effect: "slide",
				auto: false,
				swap: true,
				pauseOnHover: false,
				restartDelay: 2500
			},

			effect: {
				slide: {
					speed: 1000
				},
				fade: {
					speed: 300,
					crossfade: true
				}
			},

			callback: {
				loaded: function () {
					root.find(".slidesjs-play").remove();
				},
				start: function (number) {

				},
				complete: function (number) {
				
				}
			}

		});

		root.find(".btn_com_prev1").click(function () {
			root.find(".slidesjs-previous").trigger("click");
			return false;
		});

		root.find(".btn_com_next1").click(function () {
			root.find(".slidesjs-next").trigger("click");
			return false;
		});
	}
}
*/