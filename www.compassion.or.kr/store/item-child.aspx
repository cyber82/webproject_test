﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="item-child.aspx.cs" Inherits="store_item_child"  %>

<div style="background:transparent;" class="fn_pop_container" id="childWrapper">
	
	<div class="pop_type1 w800 fn_pop_content" style="width:800px;height:1150px;padding-top:50px">

		<input type="hidden" data-id="total_ea" value="0" />
		
		<!-- 일반팝업 width : 800 -->
		
		<div class="pop_title">
			<span>나의 어린이에게 선물하고 싶어요.</span>
			<button class="pop_close" ng-click="modalChild.hide($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content myGift">
			
			<div class="box_type2 mb60 relative">
				<div class="prdt" style="background:url('{{modalChild.image}}') no-repeat center;background-size: 127px;"></div>
				<p class="tit">{{modalChild.title}}</p>
				<span class="sel_type2 white" style="width:230px;">
					<select  name=""  data-id="option">
					</select>
				</span>
			</div>

			<p class="sub_tit">나의 어린이 선택</p>
			
			<!-- 나의 어린이 리스트 -->
			<div class="tableWrap3 mb30">
				<table class="tbl_type4">
					<caption>나의 어린이 리스트</caption>
					<colgroup>
						<col style="width:20%">
						<col style="width:35%">
						<col style="width:14%">
						<col style="width:17%">
						<col style="width:14%">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">어린이ID</th>
							<th scope="col">어린이 이름</th>
							<th scope="col">성별</th>
							<th scope="col">생일</th>
							<th scope="col">수량</th>
						</tr>
					</thead>
					<tbody >
						<tr class="child_row" ng-repeat="item in modalChild.list">
							<td class="tit" data-id="childid">{{item.childkey}}</td>
							<td data-id="namekr">{{item.namekr}}</td>
							<td data-id="gender">{{item.gender}}</td>
							<td data-id="birthdate">{{item.birthdate | date:'yyyy.MM.dd'}}</td>
							<td>
								<div class="btn_quantity">
									<input type="text" id="quantity" class="input_type2" value="0" data-id="ea" ng-value="{{item.ea}}" data-group="{{item.childkey}}" readonly />
									<button class="pluse" data-id="btn_plus_ea" data-group="{{item.childkey}}" ng-click="modalChild.plusEA($event ,item)">수량추가</button>
									<button class="minus" data-id="btn_minus_ea" data-group="{{item.childkey}}"  ng-click="modalChild.minusEA($event ,item)">수량삭제</button>
								</div>
							</td>
						</tr>

					</tbody>
				</table>
			</div>
			<!--// 나의 어린이 리스트 -->

			<!-- page navigation -->
			<div class="tac mb50">
				<paging class="small" page="modalChild.page" page-size="modalChild.rowsPerPage" total="modalChild.total" show-prev-next="true" show-first-last="true" paging-action="modalChild.getList({page : page});"></paging>

			</div>
			<!--// page navigation -->

			<p class="sub_tit">선택 결과</p>

			<!-- 어린이 선택결과 -->
			<div class="tableWrap3 mb30">
				<table class="tbl_type4">
					<caption>어린이 선택결과 리스트</caption>
					<colgroup>
						<col style="width:33%">
						<col style="width:33%">
						<col style="width:34%">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">선택된 어린이</th>
							<th scope="col">총 상품 개수</th>
							<th scope="col">총 금액</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="tit" data-id="child_count">{{modalChild.child_count | number:0}}</td>
							<td class="tit" data-id="item_count">{{modalChild.item_count | number:0}}</td>
							<td class="fc_blue fs_m" data-id="total_amount">{{modalChild.total_amount | number:0}}원</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--// 어린이 선택결과 -->

			<div class="btn_ac">
				<div>
					<a href="#" class="btn_type10 fl mr10" ng-click="modalChild.buy('cart')">장바구니</a>
					<a href="#" class="btn_type1 fl" ng-click="modalChild.buy('buy')">선택 된 어린이에게 즉시 선물</a>
				</div>
			</div>
		</div>
	</div>
</div>