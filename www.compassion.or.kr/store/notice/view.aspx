﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="store_notice_view" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/paging.ascx" TagPrefix="uc" TagName="paging" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/store/notice/default.js"></script>
	<script type="text/javascript">
		$(function () {

		});

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	 <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>컴패션 <em>스토어</em></h1>
				<span class="desc">마음이 담긴 따뜻한 손길로 희망을 선물해주세요</span>

				<div class="loc_wrap">
					<span>홈</span>
					<span>스토어</span>
					<span class="current">공지사항</span>
				</div>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents store">

			<div class="w980">

				<div class="sub_tit">
					<h2>공지사항</h2>
					<div class="pos1">
						<!--
						<label for="cat" class="hidden">검색분류 선택</label>
						<span class="sel_type3 mr15" style="width:80px;">
							<select class="custom_sel" name="" id="cat">
								<option value="전체">전체</option>
								<option value="제목">제목</option>
								<option value="내용">내용</option>
							</select>
						</span>
						<span class="relative">
							<label for="search" class="hidden">검색어 입력</label>
							<input type="text" id="search" class="input_search1 vam" style="width:245px" placeholder="검색어를 입력해 주세요" />
							<a href="#" class="search_area1">검색</a>
						</span>
						-->
					</div>
				</div>

				<!-- 게시판 상세 -->
				<div class="boardView_1">
					<div class="tit_box">
						<span class="tit"><asp:Label runat="server" ID="title"/></span>
						<span class="txt">
							<span><asp:Label runat="server" ID="regDate"/></span>
							<span class="bar"></span>
							<span class="hit"><asp:Label runat="server" ID="viewCount"></asp:Label></span>
						</span>
					</div>
					<div class="view_contents line2">
						<asp:Label runat="server" ID="content"></asp:Label>

					</div>

				</div>
				<!--// 게시판 상세 -->

				<div class="tar"><a title="목록" runat="server" id="btnList" class="btn_type4">목록</a></div>

				
			</div>
		</div>
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

	<!--
	첨부파일 : <asp:HyperLink runat=server ID=lnFileName><asp:Literal runat="server" ID="lb_filename"/></asp:HyperLink><br />
	-->
    
</asp:Content>
