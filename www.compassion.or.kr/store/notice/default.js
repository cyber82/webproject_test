﻿/// <reference path="default.js" />
$(function () {

	$page.init();

});

var $page = {

	processing: false,

	init: function () {
	
	},

}


var app = angular.module('cps.page', []);

app.controller("noticeCtrl", function ($scope, $http, $filter, paramService) {

	$scope.total = -1;
	$scope.list = null;
	$scope.isSearch = false;

	$scope.params = {
		page: 1,
		rowsPerPage: 10,
		type: '',
		keyword : ""
	};

	// 파라미터 초기화
	$scope.params = $.extend($scope.params, paramService.getParameterValues());
	if ($scope.params.keyword) $("#keyword").val($scope.params.keyword);

	// getReviwList
	$scope.getList = function (params) {

		$scope.params = $.extend($scope.params, params);

		$http.get("/api/store.ashx?t=notice_list", { params: $scope.params }).success(function (r) {
			
			if (r.success) {
				$scope.list = r.data;
				$scope.total = r.data.length > 0 ? r.data[0].total : 0;

				$scope.isSearch = false;
				if ($scope.params.k_word != "") {
					$scope.isSearch = true;
				}


				$.each($scope.list, function () {
					this.dtreg = new Date(this.dtreg);
					if (!this.viewcount) {
						this.viewcount = 0;
					}
					//this.is_new = (new Date() - this.dtreg) < (1000 * 60 * 60 * 24 * 7) ? true : false;
					//console.log(this.is_new);
				})
				
				if (params)
				    scrollTo($("#l"));
                  
			} else {
				alert(r.message);
			}
		});
	}

	$scope.search = function () {
		if ($("#keyword").val() == "") { alert("검색어를 입력해 주세요"); return false;}

	    $scope.params.page = 1;
	    $scope.params.type = $("#cat option:selected").val();
	    $scope.params.keyword = $("#keyword").val();
	    
		$scope.getList();
	}

	$scope.goView = function (idx) {

	    $http.get("/api/store.ashx?t=increase_notice_hit", { params: { idx: idx } }).success(function (r) {
	        console.log(r);
	        if (r.success) {
	            location.href = "/store/notice/view/" + idx + "?" + $.param($scope.params);
	        } else {
	            alert(r.message);
	        }
	    });

	}

	$scope.getList();
});
