﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class store_notice_view : FrontBasePage {
	const string listPath = "/store/notice/";

	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1){
			Response.Redirect( listPath, true );
		}


		var isValid = new RequestValidator()
			.Add( "p", RequestValidator.Type.Numeric )
			.Validate( this.Context, listPath );

		base.PrimaryKey = requests[0];


		//Response.Write( PrimaryKey );
	}

	protected override void loadComplete(object sender, EventArgs e){
		ShowData();
	}


	void ShowData(){

        using (StoreDataContext dao = new StoreDataContext())
        {
            var exist = www6.selectQStore<notice>("idx", Convert.ToInt32(PrimaryKey));
            //if (!dao.notice.Any(p => p.idx == Convert.ToInt32(PrimaryKey)))
            if(!exist.Any())
            {
                Response.Redirect("/store/notice/");
                return;
            }

            //var entity = dao.notice.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQFStore<notice>("idx", Convert.ToInt32(PrimaryKey));

            title.Text = entity.title;
            regDate.Text = entity.dtReg.ToString() != "" ? ((DateTime)entity.dtReg).ToString("yyyy.MM.dd") : "";
            viewCount.Text = entity.viewCount.ToString() == "" ? "0" : entity.viewCount.ToString();
            content.Text = entity.body;

            //lb_filename.Text = entity.fileName;
            //lnFileName.NavigateUrl = entity.saveFileName.WithFileServerHost();


        }

	}
	
	

}