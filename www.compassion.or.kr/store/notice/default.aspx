﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="store_notice" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/store/notice/default.js"></script>
	<script type="text/javascript">
		$(function () {

		});

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	 <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="noticeCtrl">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>컴패션 <em>스토어</em></h1>
				<span class="desc">마음이 담긴 따뜻한 손길로 희망을 선물해주세요</span>
				
				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents store" id="l">

			<div class="w980">

				<div class="sub_tit">
					<h2>공지사항</h2>
					<div class="pos1">
						<label for="cat" class="hidden">검색분류 선택</label>
						<span id="type" class="sel_type3 mr15" style="width:80px;">
							<select class="custom_sel" name="" id="cat">
								<option value="">전체</option>
								<option value="title">제목</option>
								<option value="body">내용</option>
							</select>
						</span>
						<span class="fr relative">
							<label for="keyword" class="hidden">검색어 입력</label>
							<input type="text"  id="keyword" ng-enter="search()"  class="input_search1 vam" style="width:245px" placeholder="검색어를 입력해 주세요" />
							<a href="#" class="search_area1" ng-click="search()">검색</a>
						</span>
					</div>
				</div>

				<div class="tableWrap1 mb40">
					<table class="tbl_type5">
						<caption>공지사항 리스트 테이블</caption>
						<colgroup>
							<col style="width:70%">
							<col style="width:15%">
							<col style="width:15%">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">제목</th>
								<th scope="col">작성일</th>
								<th scope="col">조회수</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="item in list">
								<td><a class="subject" ng-click="goView(item.idx)">{{item.title}}<span class="new" ng-if="item.is_new">new</span></a></td>
								<td>{{item.dtreg | date:'yyyy.MM.dd'}}</td>
								<td>{{item.viewcount}}</td>
							</tr>

							<!-- 검색결과 없을때 -->
							<tr ng-if="total == 0 && isSearch">
								<td colspan="3" class="no_result">검색결과가 없습니다.</td>
							</tr>
							<!--//  -->
							
							<!-- 게시글 없을때 -->
							<tr ng-if="total == 0 && !isSearch">
								<td colspan="3" class="no_content">등록된 글이 없습니다.</td>
							</tr>
							<!--//  -->

						</tbody>
					</table>
				</div>
				
				<!-- page navigation -->
				<div class="tac">
					<paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
				</div>
				<!--// page navigation -->

				
			</div>
		</div>
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->


</asp:Content>
