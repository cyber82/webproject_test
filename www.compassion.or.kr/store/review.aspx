﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="review.aspx.cs" Inherits="store_review" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/store/review.js"></script>
    <script type="text/javascript">
        $(function () {

        });

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <!-- sub body -->
    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>후기/문의</h1>
                <span class="desc">후원자님들의 후기와 문의사항을 모아 놓은 공간입니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents store"  id="l">

            <div class="w980">

                <!-- 탭메뉴 -->
                <ul class="tab_type1 mb40">
                    <li style="width: 50%" class="on" ng-click="review.getList({page : 1})"><a href="#">후기</a></li>
                    <li style="width: 50%"><a href="#" ng-click="qna.getList({page : 1})">문의</a></li>
                </ul>
                <!--// -->



                <!-- 후기 리스트 -->
                <div class="reviewLayer">
                    <div class="table_tit"><span class="tit">전체후기</span> &nbsp; <span class="subtit">글쓰기 작성은 해당 상품 페이지에서 가능합니다.</span></div>
                    <div class="tableWrap3 mb40">
                        <table class="tbl_type4 order">
                            <caption>후기 리스트 테이블</caption>
                            <colgroup>
                                <col style="width: 15%">
                                <col style="width: 45%">
                                <col style="width: 20%">
                                <col style="width: 20%">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col" colspan="2">제목</th>
                                    <th scope="col">작성자</th>
                                    <th scope="col">작성일</th>
                                </tr>
                            </thead>
                            <tbody>

                                <!-- set -->
                                <tr class="reList" ng-repeat-start="item in review.list">
                                    <td>
                                        <!-- 제품이미지 사이즈 : 74 * 74 -->
                                        <span class="prdt_pic">
                                            <img ng-src="{{item.name_img}}" alt="상품 썸네일 이미지" style="width: 74px;" /></span>
                                    </td>
                                    <td>
                                        <button type="button" class="tal" ng-click="review.toggle(item.idx, $event)">
                                            <span class="review_tit">{{item.name}}</span>
                                            <span class="review_sum">{{item.body}}</span>
                                        </button>
                                    </td>
                                    <td>{{item.user_id}}</td>
                                    <td>{{item.reg_date | date:'yyyy.MM.dd'}}</td>
                                </tr>
                                <tr class="reView" data-review-idx="{{item.idx}}" ng-repeat-end>
                                    <td colspan="4" class="tal" ng-bind-html="item.body"></td>
                                </tr>
                                <!--// -->

								<!-- 게시글 없을때 -->
								<tr ng-if="review.total == 0">
									<td colspan="4" class="no_content">등록된 글이 없습니다.</td>
								</tr>
								<!--// -->

                            </tbody>
                        </table>
                    </div>

                    <!-- page navigation -->
                    <div class="tac">
                        <paging class="small" page="review.params.page" page-size="review.params.rowsPerPage" total="review.total" show-prev-next="true" show-first-last="true" paging-action="review.getList({page : page} , true)"></paging>
                    </div>
                    <!--// page navigation -->

                </div>
                <!--// 후기 리스트 -->

                <!-- 문의 리스트 -->
                <div class="reviewLayer" style="display: none">
                    <div class="table_tit"><span class="tit">전체문의</span> &nbsp; <span class="subtit">글쓰기 작성은 해당 상품 페이지에서 가능합니다.</span></div>
                    <div class="tableWrap3 mb40">
                        <table class="tbl_type4 order">
                            <caption>문의 리스트 테이블</caption>
                            <colgroup>
                                <col style="width: 13%">
                                <col style="width: 47%">
                                <col style="width: 10%">
                                <col style="width: 17%">
                                <col style="width: 13%">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col" colspan="2">제목</th>
                                    <th scope="col">진행상황</th>
                                    <th scope="col">작성자</th>
                                    <th scope="col">작성일</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- set -->


                                <tr class="reList" ng-repeat-start="item in qna.list">
                                    <td>
                                        <!-- 제품이미지 사이즈 : 74 * 74 -->
                                        <span class="prdt_pic">
                                            <img ng-src="{{item.name_img}}" alt="제품 썸네일 이미지" style="width: 74px;" /></span>
                                    </td>
                                    <td>
                                        <button class="tal" ng-click="qna.toggle(item.idx, $event)">
                                            <span class="review_tit">{{item.name}}</span>
                                            <span class="review_sum" ng-bind-html="item.question" ></span>
                                        </button>
                                    </td>
                                    <td><span class="status {{item.answerClass}}" ng-bind-html="item.answerYN"></span></td>
                                    <td>{{item.user_id}}</td>
                                    <td>{{item.reg_date | date:'yyyy.MM.dd'}}</td>

                                </tr>
                            
                                <tr class="reView" data-qna-idx="{{item.idx}}" ng-repeat-end>
                                    <td colspan="5" class="tal" >
										<p class="q" ng-if="!item.isopen || item.is_owner" ng-bind-html="item.question"></p>
										<p class="a" ng-if="(!item.isopen || item.is_owner) && item.answer != null">{{item.answer}}</p>
	
										<!-- 비공개글 -->
										<p class="scrt_ment" ng-if="item.isopen && !item.is_owner">비공개 글 입니다.<br />글쓴이와 관리자만 열람 가능합니다.</p>
										<!--// -->
                                    </td>
                                </tr>

								<!-- 게시글 없을때 -->
								<tr ng-if="qna.qna_total == 0">
									<td colspan="5" class="no_content">등록된 글이 없습니다.</td>
								</tr>
								<!--// -->

                            </tbody>
                        </table>
                    </div>

                    <!-- page navigation -->
                    <div class="tac">
                        <paging class="small" page="qna.params.page" page-size="qna.params.rowsPerPage" total="qna.total" show-prev-next="true" show-first-last="true" paging-action="qna.getList({page : page} , true)"></paging>
                    </div>
                    <!--// page navigation -->

                </div>
                <!--// 문의 리스트 -->


            </div>
        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>
    <!--// sub body -->


</asp:Content>
