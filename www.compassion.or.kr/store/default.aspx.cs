﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class store_default : FrontBasePage {
	
	protected override void OnBeforePostBack() {
		
		this.ViewState["type"] = "-1";
		base.LoadComplete += new EventHandler(list_LoadComplete);

		GetMainPage();
	}



	void GetMainPage()
    {

		using (FrontDataContext dao = new FrontDataContext()) {

            //var list = dao.sp_mainpage_list_f(4, "store_top").ToList();
            Object[] op1 = new Object[] { "count", "position" };
            Object[] op2 = new Object[] { 4, "store_top" };
            var list = www6.selectSP("sp_mainpage_list_f", op1, op2).DataTableToList<CommonLib.sp_mainpage_list_fResult>();

            foreach (var entity in list) {
				entity.mp_image = entity.mp_image.WithFileServerHost();
			}

			repeater_visual.DataSource = list;
			repeater_visual.DataBind();

			repeater_visual_title.DataSource = list;
			repeater_visual_title.DataBind();

            //Response.Write(list.ToJson() );
		}

        using (StoreDataContext dao = new StoreDataContext())
        {
            //var randomProduct = dao.sp_randomProduct_list_f(3);
            Object[] op1 = new Object[] { "num" };
            Object[] op2 = new Object[] { 3 };
            var randomProduct = www6.selectSPStore("sp_randomProduct_list_f", op1, op2).DataTableToList<CommonLib.sp_randomProduct_list_fResult>();

            // SELECT TOP 3 * FROM product P with(nolock) WHERE p_type = 1 and display_flag = 1 ORDER BY newid()
            repeater_randomProduct.DataSource = randomProduct;
            repeater_randomProduct.DataBind();
        }
    }







}