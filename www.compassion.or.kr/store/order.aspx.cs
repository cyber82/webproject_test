﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class store_order : FrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {

		this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];

		UserInfo sess = new UserInfo();
		if (sess.LocationType != "국외") {

			var addr_result = new SponsorAction().GetAddress();
			if(!addr_result.success) {
				base.AlertWithJavascript(addr_result.message, "goBack()");
				return;
			}

			SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;

			// 보여줄값 저장 
			addr_domestic_zipcode.Value = addr_data.Zipcode;
			addr_domestic_addr1.Value = addr_data.Addr1;
			addr_domestic_addr2.Value = addr_data.Addr2;
			hfAddressType.Value = addr_data.AddressType;
			dspAddrDoro.Value = addr_data.DspAddrDoro;
			dspAddrJibun.Value = addr_data.DspAddrJibun;
			// 본래값 저장 
			temp_zipcode.Value = addr_data.Zipcode;
			temp_addr1.Value = addr_data.Addr1;
			temp_addr2.Value = addr_data.Addr2;

			var comm_result = new SponsorAction().GetCommunications();
			if(!comm_result.success) {
				base.AlertWithJavascript(comm_result.message, "goBack()");
				return;
			}

			SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
			mobile.Value = comm_data.Mobile;
			name.Value = sess.UserName;

            temp_mobile.Value = comm_data.Mobile;
            temp_name.Value = sess.UserName;


		}else {
			base.AlertWithJavascript("국내거주 회원만 구매가 가능합니다.", "goBack()");
			return;
		}

		string imagePath = Uploader.GetRoot(Uploader.FileGroup.image_shop);
		var list = (List<sp_temp_order_cart_list_fResult>)(new StoreAction().GetOrderTemp(imagePath).data);
		if(list.Count < 1) {
			Response.Redirect("/store/");
			return;
		}

		StoreAction.orderItem order_item = new StoreAction.orderItem();
		order_item.cartList = list;
		order_item.optionPrice = list.Sum(p => p.option1_price).Value;
		order_item.orderPrice = list.Sum(p => p.orderPrice).Value;
		
		this.ViewState["order_item"] = order_item.ToJson();

        if (order_item.orderPrice == 0)
        {
            tr_paymethod.Attributes.Add("style", "display:none;");
        }
    }



	// 결제
	protected void btn_submit_Click( object sender, EventArgs e ) {

		if (base.IsRefresh) {
			return;
		}

		UserInfo sess = new UserInfo();
		if(sess.LocationType == "국내") {
			var sponsorAction = new SponsorAction();

			sponsorAction.UpdateAddress(false, hfAddressType.Value , sess.LocationType, "한국", addr_domestic_zipcode.Value, addr_domestic_addr1.Value, addr_domestic_addr2.Value);
			
		}
		
        if (name.Value == "") {
			base.AlertWithJavascript("수령인을 입력해 주세요");
			name.Focus();
            return;
        }
        if(mobile.Value == "") {
			base.AlertWithJavascript("휴대폰번호를 입력해 주세요");
			mobile.Focus();
            return;
        }
        if(addr_domestic_zipcode.Value == "") {
			base.AlertWithJavascript("배송지정보를 선택해 주세요");
			addr_domestic_zipcode.Focus();
			return;
        }
        if(addr_domestic_addr1.Value == "") {
			base.AlertWithJavascript("배송지 상세주소를 입력해주세요");
			addr_domestic_addr1.Focus();
			return;
        }
        if (!chk_agree.Checked) {
			base.AlertWithJavascript("구매에 동의하여 주세요");
			chk_agree.Focus();
			return;
        }

            
        StoreAction.orderItem order_item = this.ViewState["order_item"].ToString().ToObject<StoreAction.orderItem>();
		order_item.receiver = name.Value;
		order_item.zipcode = addr_domestic_zipcode.Value;
		order_item.addr1 = addr_domestic_addr1.Value;
		order_item.addr2 = addr_domestic_addr2.Value;
		order_item.email = "";
		order_item.memo = memo.Value;
		order_item.phone = phone.Value;
		order_item.mobile = mobile.Value;
		order_item.deliveryFee = Convert.ToInt32(hd_delivery_fee.Value);
		order_item.device = "W";
		order_item.orderNo = StringExtension.GetRandomString(3) + DateTime.Now.ToString("yyyyMMddHHmmss");

		this.ViewState["order_item"] = order_item.ToJson();
		this.ViewState["good_mny"] = hd_good_mny.Value;
		this.ViewState["good_name"] = hd_good_name.Value;
		this.ViewState["buyr_name"] = name.Value;
		this.ViewState["ordr_idxx"] = order_item.orderNo;
		this.ViewState["used_card_YN"] = "Y";
		this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_domestic"];

        new PayItemSession.Store().Create(order_item.orderNo, order_item.ToJson());

        kcp_no_form.Hide();
        kakaopay_form.Hide();
		kcp_form.Hide();
		payco_form.Hide();

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = "Store";  // 결제 페이지 종류 

        if (hd_good_mny.Value == "0")
        {
            //this.ViewState["pay_method"] = "100000000000";
            //this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_shop_card"];

            kcp_no_form.Show(this.ViewState);
        }
        else if (payment_method_card.Checked) {

			this.ViewState["pay_method"] = "100000000000";
			this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_shop_card"];
			kcp_form.Show(this.ViewState);
		

		} else if(payment_method_cms.Checked) {
			this.ViewState["pay_method"] = "010000000000";
			this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_shop_account"];
			kcp_form.Show(this.ViewState);

		} else if(payment_method_phone.Checked) {
			this.ViewState["pay_method"] = "000010000000";
			this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_shop_phone"];
			kcp_form.Show(this.ViewState);

		}
        else if(payment_method_oversea.Checked) {
			this.ViewState["pay_method"] = "100000000000";
			this.ViewState["overseas_card"] = "Y";
			this.ViewState["used_card_YN"] = "Y";
            this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_oversea"];
            this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_web_card_Foreign_Store"];
            //this.ViewState["used_card"] = ConfigurationManager.AppSettings["g_conf_card_oversea"];
            //this.ViewState["site_cd"] = ConfigurationManager.AppSettings["g_conf_site_cd_shop_card"];
            kcp_form.Show(this.ViewState);

		}
        else if(payment_method_payco.Checked) {
			payco_form.Show(this.ViewState);
		} else if(payment_method_kakao.Checked) {
			kakaopay_form.Show(this.ViewState);
		}
		

		

	}
}