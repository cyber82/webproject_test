﻿$(function () {

    $page.init();

});

var $page = {

    processing: false,

    init: function () {

        // 후기,문의 레이어 show/hide
        $(".tab_type1 li").click(function () {
            $(".tab_type1 li").removeClass("on");
            $(this).addClass("on");
            var a = $(this).index();
            $(".reviewLayer").hide();
            $(".reviewLayer").eq(a).show();
            return false;
        })

    },


}


var app = angular.module('cps.page', []);

app.controller("defaultCtrl", function ($scope, $http, $filter) {

    /* 문의 */
    $scope.qna = {
        qna_total: -1,
        list: null,

        params: {
            page: 1,
            rowsPerPage: 10,
            product_idx: -1,
            user_id: '',
            type: "all"
        },

        // getList
        getList: function (params , bool) {

            $scope.qna.params = $.extend($scope.qna.params, params);
            //console.log($scope.params);

            $http.get("/api/store.ashx?t=qna_list", { params: $scope.qna.params }).success(function (r) {
                
                if (r.success) {

                    $scope.qna.list = r.data;
                    $.each($scope.qna.list, function () {
                        if ($("#_userid").val() == this.user_id) {
                            this.isopen = false;
                        }
                        this.reg_date = new Date(this.reg_date);
                        this.show_detail = false;

                        if (this.answer == null || this.answer == "") {
                        	this.answerClass = "ing"
                        	this.answerYN = "진행중";
                        } else {
                        	this.answerClass = "end"
                        	this.answerYN = "답변<br/>완료";
                        }

						/*
                        if (this.answer_date == null) {
                            this.answerClass = "ing"
                            this.answerYN = "처리중";
                        } else {
                            this.answerClass = "end"
                            this.answerYN = "답변<br/>완료";
                        }
						*/
                    })
                    

                    $scope.qna.total = r.data.length > 0 ? r.data[0].total : 0;
                    if (params && bool)
                        scrollTo($("#l"));
                } else {
                    alert(r.message);
                }
            });
        },

        // 클릭이벤트
        toggle: function (idx, $event) {
            $event.preventDefault();
            var target = $(".reView[data-qna-idx='" + idx + "']");

            if (target.hasClass("open")) {
                $(".reView").hide();
                target.removeClass("open");
            }else {
                $(".reView").hide();
                $(".reView").removeClass("open");
                target.show();
                target.addClass("open");
            }
        }


    }

    /* 후기 */
    $scope.review = {
        total: -1,
        list: null,

        params: {
            page: 1,
            rowsPerPage: 10,
            product_idx: -1,
            user_id: '',
            type: "all"
        },

        // getReviwList
        getList: function (params , bool) {

            $scope.review.params = $.extend($scope.review.params, params);
            //console.log($scope.params);

            $http.get("/api/store.ashx?t=review_list", { params: $scope.review.params }).success(function (r) {
                
                if (r.success) {

                    $scope.review.list = r.data;

                    $.each($scope.review.list, function () {
                        this.reg_date = new Date(this.reg_date);
                    })

                    $scope.review.total = r.data.length > 0 ? r.data[0].total : 0;
                    if (params && bool)
                        scrollTo($("#l"));
                } else {
                    alert(r.message);
                }
            });
        },

        // 클릭이벤트
        toggle: function (idx, $event) {
            $event.preventDefault();
            var target = $(".reView[data-review-idx='" + idx + "']");

            if (target.hasClass("open")) {
                $(".reView").hide();
                target.removeClass("open");
            }
            else {
                $(".reView").hide();
                $(".reView").removeClass("open");
                target.show();
                target.addClass("open");
            }
        }
    }

    $scope.review.getList();
    $scope.qna.getList();
});
