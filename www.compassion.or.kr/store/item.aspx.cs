﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;


public partial class store_item : FrontBasePage {

    const string listPath = "/store/";

    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();
        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if(requests.Count < 1) {
            Response.Redirect(listPath, true);
        }
		
        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);

        base.PrimaryKey = requests[0];
        key.Value = requests[0];

        using (StoreDataContext dao = new StoreDataContext())
        {
            //var randomProduct = dao.sp_randomProduct_list_f(3);
            Object[] op1 = new Object[] { "num" };
            Object[] op2 = new Object[] { 3 };
            var randomProduct = www6.selectSPStore("sp_randomProduct_list_f", op1, op2).DataTableToList<CommonLib.sp_randomProduct_list_fResult>();


            // SELECT TOP 3 * FROM product P with(nolock) WHERE p_type = 1 and display_flag = 1 ORDER BY newid()
            repeater_randomProduct.DataSource = randomProduct;
            repeater_randomProduct.DataBind();
        }


        using (StoreDataContext dao = new StoreDataContext())
        {
            HttpContext context = HttpContext.Current;
            var product_idx = Convert.ToInt32(base.PrimaryKey);
            var userInfo = new UserInfo();

            // 상품 구매 여부 체크
            //var checkPass = dao.sp_review_check_f(product_idx, userInfo.UserId).ToList();
            Object[] op1 = new Object[] { "product_idx", "user_id" };
            Object[] op2 = new Object[] { product_idx, userInfo.UserId };
            var checkPass = www6.selectSPStore("sp_review_check_f", op1, op2).DataTableToList<CommonLib.sp_review_check_fResult>();

            buy_check.Value = checkPass[0].result;

        }

    }

    protected override void loadComplete( object sender, EventArgs e ) {

        ShowData();
    }

    void ShowData() {

        using (StoreDataContext dao = new StoreDataContext())
        {
            var exist = www6.selectQStore<product>("idx", Convert.ToInt32(PrimaryKey), "display_flag", 1);
            //if (!dao.product.Any(p => p.idx == Convert.ToInt32(PrimaryKey) && p.display_flag == true))
            if(!exist.Any())
            {
                Response.Redirect("/store/");
                return;
            }

            //var entity = dao.product.First(p => p.idx == Convert.ToInt32(PrimaryKey) && p.display_flag == true);
            var entity = www6.selectQFStore<product>("idx", Convert.ToInt32(PrimaryKey), "display_flag", 1);

            //var opts = dao.product_option.Where(p => p.product_idx == Convert.ToInt32(PrimaryKey) && p.option_level == 0 && p.del_flag == false && p.display == true).ToList();
            var opts = www6.selectQStore<product_option>("product_idx", Convert.ToInt32(PrimaryKey), "option_level", 0, "del_flag", 0, "display", 1);

            //var imgs = dao.product_image.Where(p => p.product_idx == Convert.ToInt32(PrimaryKey)).ToList();
            var imgs = www6.selectQStore<product_image>("product_idx", Convert.ToInt32(PrimaryKey));

            btn_show_child.Visible = entity.gift_flag.Value;
            hd_title.Value = title.Text = entity.name;
            hd_price.Value = entity.selling_price.ToString();
            this.ViewState["hd_title"] = entity.name;
            price.Text = entity.selling_price.ToString("N0");
            status.Text = entity.inventory > 0 ? "판매중" : "품절";
            hd_inventory.Value = entity.inventory.ToString();
            product_detail.Text = entity.product_detail;
            hd_delivery_charge.Value = entity.delivery_charge.ToString().ValueIfNull("0");
            hd_gift_flag_name.Value = entity.gift_flag.HasValue ? entity.gift_flag.Value.ToString().ToLower() : "false";
            this.ViewState["hd_gift_flag_name"] = entity.gift_flag.HasValue ? entity.gift_flag.Value.ToString().ToLower() : "false";

            if (opts.Count > 0)
            {

                option.Items.Add(new ListItem("옵션을 선택하세요", ""));

                foreach (var opt in opts)
                {
                    var text = opt.option_name + (opt.option_price > 0 ? "(+" + opt.option_price.Value.ToString("N0") + "원)" : "");
                    var item = new ListItem(text, opt.idx.ToString());
                    item.Attributes["data-price"] = opt.option_price.Value.ToString();
                    option.Items.Add(item);
                }
            }
            else
            {
                option.Items.Add(new ListItem("옵션 없음", "0"));
            }
            this.ViewState["image"] = "";

            if (imgs.Count != 0)
            {
                this.ViewState["image"] = (Uploader.GetRoot(Uploader.FileGroup.image_shop) + imgs[0].saved_file_name).WithFileServerHost();
            }

            repeater_image.DataSource = imgs;
            repeater_image.DataBind();

            if (UserInfo.IsLogin)
            {
                hd_user_id.Value = new UserInfo().UserId;
            }

            hd_item_id.Value = this.PrimaryKey.ToString();
            this.ViewState["hd_item_id"] = this.PrimaryKey.ToString();


            /*  문의   */
            //var qna_list = dao.qna.Where(p => p.idx == Convert.ToInt32(PrimaryKey)).ToList();
            var list = www6.selectQStore<qna>("idx", Convert.ToInt32(PrimaryKey));

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-스토어] {0}", entity.name);

            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = "";
            //this.ViewState["meta_keyword"] = "";
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,FrontBasePage)
            if (imgs.Count != 0)
            {
                this.ViewState["meta_image"] = (Uploader.GetRoot(Uploader.FileGroup.image_shop) + imgs[0].saved_file_name).WithFileServerHost();
            }
        }
    }
	
}