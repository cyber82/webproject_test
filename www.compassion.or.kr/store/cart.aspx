﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cart.aspx.cs" Inherits="store_cart" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/store/cart.js?v=1.1"></script>
	<script type="text/javascript">
		$(function () {

		});

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	
    <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>장바구니</h1>
				<span class="desc">마음이 담긴 따뜻한 손길로 희망을 선물해주세요</span>
				
				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 store">

			<div class="w980" >

				<!-- 주문프로세스 -->
				<div class="order_process">
					<ol>
						<li>
							<span class="wrap">
								<span class="step step1 on">STEP 01</span>
								<span class="txt">장바구니</span>
							</span>
						</li>
						<li>
							<span class="wrap">
								<span class="step step2">STEP 02</span>
								<span class="txt">주문/결제</span>
							</span>
						</li>
						<li>
							<span class="wrap">
								<span class="step step3">STEP 03</span>
								<span class="txt">주문완료</span>
							</span>
						</li>
					</ol>
				</div>
				<!--// 주문프로세스 -->

				<!-- 장바구니 리스트 -->
				<p class="tbl_head">장바구니 상품 <em class="fc_blue" id="total"></em>건</p>
				<div class="tableWrap3 mb40">
					<table class="tbl_type4 order"  id="item_container">
						<caption>장바구니 리스트 테이블</caption>
						<colgroup>
							<col style="width:6%">
							<col style="width:12%">
							<col style="width:28%">
							<col style="width:14%">
							<col style="width:10%">
							<col style="width:15%">
							<col style="width:15%">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">
									<span class="checkbox_ui blue default">
										<input type="checkbox" class="css_checkbox" id="chk_all" checked/>
										<label for="chk_all" class="css_label">상품 모두선택</label>
									</span>
								</th>
								<th scope="col" colspan="2">상품정보</th>
								<th scope="col">판매가</th>
								<th scope="col">수량</th>
								<th scope="col">상품구분</th>
								<th scope="col">주문금액</th>
							</tr>
						</thead>
						<tbody>
						
							
						</tbody>
					</table>
				</div>
				<!--// 장바구니 리스트 -->

				<!-- 주문금액 -->
				<div class="total_sum mb30 nodata">
					<span>주문금액 합계<em id="sub_total"></em></span>
					<span class="pluse"></span>
					<span>배송비<em id="delivery_fee"></em></span>
					<span class="sum"></span>
					<span>총 주문 합계<em class="fc_blue" id="total_amount"></em></span>
				</div>
				<!--// 주문금액 -->

				<div class="clear2 mb60">
					<a href="#"  id="btn_remove" class="btn_type3 fl nodata">선택상품삭제</a>

					<a href="#" id="btn_order_all" class="btn_type1 fr ml10 nodata">전체주문</a>
					<a href="#" id="btn_order" class="btn_type1 fr ml10 nodata">선택주문</a>
					<a href="/store/" class="btn_type10 fr"><span class="ic_cart"></span>계속 쇼핑하기</a>
				</div>

				<div class="box_type3">
					<span class="info_delivery"><em>3만원 이상 구매 및 어린이 선물 구매 시 무료배송 / 3만원 미만 구매 시 배송비 2,500원</em> (어린이 선물을 포함한 총 상품 금액 3만 원 이상인 경우 무료배송)</span>
				</div>



			</div>


		</div>	
		<!--// e: sub contents -->



		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

	<textarea class="no_template" style="display:none">
		<tr>
			<td class="empty_cart" colspan="7">
				현재 장바구니에 보관된 상품이 없습니다.
			</td>
		</tr>
	</textarea>

	<textarea class="template" style="display:none">
		<tr class="item_row">
			<td>
				<span class="checkbox_ui default">
					<input type="checkbox" class="css_checkbox item_obj" id="chk1" data-id="chk" checked/>
					<label for="chk1" class="css_label">상품선택</label>
				</span>
			</td>
			<td>
				<!-- 제품이미지 사이즈 : 98 * 98 -->
				<div class="prdt_pic"><a href="#" data-id="title2"><img data-id="img" alt="상품 썸네일 이미지" class="item_obj" style="width:98px;"/></a></div>
			</td>
			<td class="tal">
				<a href="#" class="prdt_name item_obj" data-id="title"></a>
				<p class="prdt_option item_obj" data-id="option"></p>
				<p class="prdt_option item_obj" data-id="child"></p>
				<!--<button type="button" class="btn_type8 btn_one_remove"><span>삭제</span></button>-->
			</td>
			<td class="color1 item_obj" data-id="price"></td>
			<td>
				<div class="btn_quantity">
					<input type="text" id="quantity" data-id="ea" class="input_type2 item_obj" value="0" readonly />
					<div data-id="ea_control">
						<button class="pluse item_obj" data-id="btn_plus_ea">수량추가</button>
						<button class="minus item_obj" data-id="btn_minus_ea">수량삭제</button>
					</div>
				</div>
			</td>
			<td class="item_obj" data-id="type" >어린이에게 보내는 선물</td>
			<td class="tit item_obj" data-id="total" data-group=""></td>
		</tr>
	</textarea>

</asp:Content>
