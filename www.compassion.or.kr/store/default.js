﻿$(function () {

    $page.init();

});


var $page = {

    init: function () {

        // store 안내 레이어 show/hide
        $(".store_info .si_btn").click(function () {
            $(this).parent().find(".layer").fadeIn("fast");
            return false;
        })

        $(".btn_back").click(function () {
            $(".store_info .layer").fadeOut("fast");
            return false;
        })

        $page.setMainVisual();

    },

    setMainVisual: function () {
        $(".visual_tab li").eq(0).addClass("on");

        var root = $(".visual");

        // 메인비주얼
        root.find(".visual_tab > li").each(function (i) {

            var self = $(this);
            $(this).click(function () {

                root.find(".visual_tab > li").removeClass("on");
                self.addClass("on");
                root.find('a[data-slidesjs-item=' + i + ']').trigger("click");		// 이동

                //root.find(".btn_control").addClass("btn_play").removeClass("btn_pause");

                return false;
            });

        });

        root.find(".vi_con_wrap").slidesjs({
            width: 980,
            height: 380,

            pagination: { effect: "slide" },

            play: {
                active: true,
                interval: 5000,
                auto: true,
                swap: true,
                pauseOnHover: false,
                restartDelay: 2500
            },

            callback: {
                loaded: function () { },
                start: function (number) {

                },
                complete: function (number) {

                    root.find(".visual_tab > li").removeClass("on");
                    $(root.find(".visual_tab > li")[number - 1]).addClass("on");

                }
            }

        });

    }

};

var app = angular.module('cps.page', []);

app.controller("defaultCtrl", function ($scope, $http, popup, $filter, paramService) {

    $scope.total = -1;
    $scope.rowsPerPage = 6;
    $scope.list = null;

    $scope.params = {
        page: 1,
        rowsPerPage: $scope.rowsPerPage,
        gift_flag: -1,
        keyword: ""
    };

    // 파라미터 초기화
    $scope.params = $.extend($scope.params, paramService.getParameterValues());

    $scope.$watch('total', function (newValue, oldValue) {
        if (newValue !== oldValue) {

            if (oldValue < 0) return;

            setTimeout(function () {
                if (newValue == 0) {
                    scrollTo($("#search_noresult"));
                } else {
                    scrollTo($("#search_hasresult"), 20);


                }
            })

        }
    });

    $scope.getList = function (params) {
        $scope.params = $.extend($scope.params, params);

        $http.get("/api/store.ashx?t=get_store_list", { params: $scope.params }).success(function (r) {
            console.log(r);
            if (r.success) {

                $scope.list = r.data;
                $scope.total = r.data.length > 0 ? r.data[0].total : 0;

                if ($scope.total == 0) {
                    $scope.params.keyword = "";

                }

                if (params)
                    scrollTo($("#search_hasresult"), 20);

            } else {
                alert(r.message);
            }

        });

    }
    //검색
    $scope.search = function ($event) {
        if ($event) { $event.preventDefault(); }

        var keyword = $("#keyword").val();

        if (keyword == "") { alert("검색어를 입력해주세요."); return false; }

        $scope.params.keyword = keyword;
        $scope.params.page = 1;
        $scope.getList();


    }
    // 검색결과 없을때 검색
    $scope.searchResultNone = function ($event) {
        if ($event) { $event.preventDefault(); }

        var keyword = $("#keyword2").val();
        if (keyword == "") { alert("검색어를 입력해주세요."); return false; }

        $scope.params.keyword = keyword;
        $scope.params.page = 1;
        $scope.getList();
    }


    $scope.notice_list = null;
    $scope.notice_index = 0;
    $scope.notice_total = 0;
    $scope.getNoticeList = function () {

        $scope.notice_list = null;
        $http.get("/api/store.ashx?t=notice_list", { page: 1, rowsPerPage: 10, }).success(function (r) {
            //console.log(r);
            if (r.success) {

                $scope.notice_list = r.data;

                if (r.data.length > 0) {
                    $scope.notice_total = r.data[0].total
                    $(".elps").text(r.data[0].title).attr("href", "/store/notice/view/" + r.data[0].idx);


                    date = new Date(r.data[0].dtreg);
                    date = moment(date).format('YYYY.MM.DD')
                    $(".date").text(date)
                }

            } else {
                alert(r.message);
            }
        });
    }

    $scope.prevNextNotice = function (str, $event) {

        if (str == "prev" && $scope.notice_index != 0) {
            $scope.notice_index--;
        } else if (str == "next" && $scope.notice_index != ($scope.notice_total - 1)) {
            $scope.notice_index++;
        }

        if (str == "prev" && ($scope.notice_index == 0)) {
            $(".prev").hide();
            $(".next").show();
        } else if (str == "next" && ($scope.notice_index == $scope.notice_total - 1)) {
            $(".next").hide();
            $(".prev").show();
        } else {
            $(".next, .prev").show();
        }

        date = new Date($scope.notice_list[$scope.notice_index].dtreg);
        date = moment(date).format('YYYY.MM.DD')

        $(".elps").text($scope.notice_list[$scope.notice_index].title).attr("href", "/store/notice/view/" + $scope.notice_list[$scope.notice_index].idx);
        $(".date").text(date)


        $event.preventDefault();

    }

    // 탭 이벤트 처리
    $scope.clickTabEvent = function (idx, $event) {
        //		$(".tab_menu").removeClass("on");
        //$(".tab_menu[data-index=" + idx + "]").addClass("on");
        $scope.params.page = 1;
        $scope.getList({ gift_flag: idx });

        $event.preventDefault();

    }



    $scope.goView = function (idx, $event, item) {
        //GA Product Start
        dataLayer.push({
            'event': 'Click',
            'ecommerce': {
                'currencyCode': 'KRW',
                'click': {
                    'actionField': { 'list': '스토어' }, // 상품 전시 영역          
                    'products': [{
                        'id': item.idx,  // 상품 코드           
                        'name': item.name, // 상품 이름           
                        'brand': '한국컴패션', // 브랜드           
                        'category': item.gift_flag ? '어린이선물' : '일반상품' // 상품 카테고리                
                    }]
                }
            },
        });
        //console.log(dataLayer);
        //GA Product End

        location.href = "/store/item/" + idx + "?" + $.param($scope.params);
        $event.preventDefault();

    }

    $scope.getList();

    $scope.getNoticeList();
    //공지사항 가기
    $scope.goNotice = function ($event) {
        location.href = "/store/notice/";
        $event.preventDefault();
    }

    //제품리스트 가기
    $scope.goProductList = function () {
        location.href = "/store/";

    }




});
