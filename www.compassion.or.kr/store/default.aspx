﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="store_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script src="/common/js/jquery/jquery.slides.wisekit.js"></script>
    <script type="text/javascript" src="/store/default.js?v=1.0"></script>
	<script type="text/javascript" src="/common/js/site/moveToMobile.js"></script>
    <script type="text/javascript">
        $(function () {

        });

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <!-- sub body -->
    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>컴패션 <em>스토어</em></h1>
                <span class="desc">마음이 담긴 따뜻한 손길로 희망을 선물해주세요</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents store" ng-class="{padding0 : total == 0}">

            <!-- 검색결과가 없을때 -->
            <div ng-show="total == 0" id="search_noresult">
                <div class="search_noresult">
                    <span class="txt"><em>검색결과가 없습니다.</em><br />
                        다른 상품을 검색해주세요.</span><br />

                    <div class="inblock mb30 relative">
                        <label for="search" class="hidden">검색어 입력</label>
                        <input type="text" id="keyword2" class="input_search2" ng-model="params.keyword" style="width: 410px" ng-enter="searchResultNone()" placeholder="상품을 검색해보세요." />
                        <a href="#" class="search_area2" ng-click="searchResultNone($event)">검색</a>
                    </div>
                    <br />
                    <a ng-click="goProductList()" class="btn_s_type1">전체상품 보기</a>
                </div>
            </div>

            <div class="w980">

                <!-- 검색결과가 없을때 -->
                <div ng-show="total == 0">
                    <h2 class="result_tit">스토어 다른 상품</h2>

                    <!-- 제품리스트 -->
                    <div class="prdtList_wrap mb20">
                        <ul class="prdtList">
                            <asp:Repeater runat="server" ID="repeater_randomProduct">
                                <ItemTemplate>
                                    <li>
                                        <!-- 이미지 사이즈 : 310 * 310 -->
                                        <div class="img" style="background:url('<%# Eval("name_img") == null ? "" : (Uploader.GetRoot(Uploader.FileGroup.image_shop) + Eval("name_img").ToString()).WithFileServerHost() %>') no-repeat center;"></div>
                                        <p class="name"><%#Eval("name") %></p>
                                        <p class="price"><em>&#8361;</em>{{<%#Eval("selling_price") %> | number:N0}}</p>

                                        <!-- 마우스 오버 시 -->
                                        <span class="over">
                                            <p class="name2"><%#Eval("name") %></p>
                                            <a class="btn_view1" ng-click="goView(<%#Eval("idx") %> , $event)">바로 구매하기</a>
                                        </span>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>

                        </ul>
                    </div>
                    <!--// 제품리스트 -->
                </div>

                <!-- 검색결과가 있을때 -->
                <div ng-show="total > 0">
                    <!-- 비주얼영역 -->
                    <div class="visual">
                        <h2 class="tit">행복한 선물! 컴패션 스토어의 또 다른 후원의 방법</h2>
                        <div class="vi_tab">

                            <div class="vi_con_wrap">
                                <!-- 백그라운드 이미지 사이즈 : 980 * 380 -->
                                <asp:Repeater runat="server" ID="repeater_visual">
                                    <ItemTemplate>
                                        <a href="<%#Eval("mp_image_link") %>" target="<%#Eval("mp_is_new_window").ToString() == "True" ? "_blank" : "_self" %>">
                                            <div class="vi_con item" style="background: url('<%#Eval("mp_image")%>') no-repeat; background-size: 980px;">
                                            </div>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </div>
                            <ul class="visual_tab">
                                <asp:Repeater runat="server" ID="repeater_visual_title">
                                    <ItemTemplate>
                                        <li>
                                            <button><span><%#Eval("mp_title") %></span></button></li>

                                    </ItemTemplate>
                                </asp:Repeater>

                            </ul>

                        </div>
                    </div>
                    <!--// 비주얼영역 -->

                    <!-- 검색 -->
                    <div class="sortWrap tar mb20" id="search_hasresult">
                        <div class="relative">
                            <label for="keyword" class="hidden">검색어 입력</label>
                            <input type="text" id="keyword" ng-model="params.keyword" class="input_search1 " style="width: 245px" ng-enter="search()" placeholder="상품을 검색해보세요." />

                            <a href="#" class="search_area1" ng-click="search($event)">검색</a>
                        </div>
                    </div>
                    <!--// -->


                    <!-- tab menu -->
                    <ul class="tab_type1 main" id="l">
                        <li style="width: 33.33%" class="tab_menu" ng-class="{'on' : params.gift_flag == -1}" data-index="-1" ng-click="clickTabEvent( -1 , $event)"><a href="#"><span class="all"></span>전체상품</a></li>
                        <li style="width: 33.33%" class="tab_menu" ng-class="{'on' : params.gift_flag == 0}" data-index="0" ng-click="clickTabEvent( 0 , $event)"><a href="#"><span class="com"></span>일반상품</a></li>
                        <li style="width: 33.33%" class="tab_menu" ng-class="{'on' : params.gift_flag == 1}" data-index="1" ng-click="clickTabEvent( 1 , $event)"><a href="#"><span class="child"></span>어린이 선물</a></li>
                    </ul>
                    <!--// -->

                    <!-- 제품리스트 -->
                    <div class="prdtList_wrap">

                        <ul class="prdtList">
                            <li ng-repeat="item in list">
                                <!-- 이미지 사이즈 : 310 * 310 -->
                                <div class="img" style="background:url('{{item.name_img}}') no-repeat center;"></div>
                                <p class="name">{{item.name}}</p>
                                <p class="price"><em>&#8361;</em>{{item.selling_price | number:N0}}</p>

                                <!-- 마우스 오버 시 -->
                                <span class="over">
                                    <p class="name2">{{item.name}}</p>
                                    <a class="btn_view1" ng-click="goView(item.idx , $event, item)">바로 구매하기</a>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <!--// 제품리스트 -->


                    <!-- page navigation -->
                    <div class="tac mb60">
                        <paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>
                    </div>
                    <!--// page navigation -->

                    <!-- 스토어안내 -->
                    <div class="store_info">
                        <div class="logo"></div>
                        <ul>
                            <li>
                                <button class="si_btn btn1">
                                    <span>컴패션 스토어는<br />
                                        어떻게 운영할까요?</span></button>
                                <div class="layer layer1">
                                    <p class="q"><span></span>컴패션 스토어는 어떻게 운영 할까요?</p>
                                    <div class="con">
                                        <p class="txt1">
                                            컴패션 스토어는<br />
                                            <em>한국컴패션</em>에서 운영하는 온라인 쇼핑몰입니다.
                                        </p>
                                        <p class="txt2">
                                            컴패션 스토어는 컴패션과 뜻을 함께하는 기업체, 디자이너, 후원자분의<br />
                                            협력으로 제작됩니다.
                                        </p>
                                    </div>
                                    <button class="btn_back"><span>BACK</span></button>
                                </div>
                            </li>
                            <li>
                                <button class="si_btn btn2">
                                    <span>컴패션 스토어의 수익금은<br />
                                        어떻게 쓰이나요?</span></button>
                                <div class="layer layer2">
                                    <p class="q"><span></span>컴패션 스토어의 수익금은 어떻게 쓰이나요?</p>
                                    <div class="con">
                                        <p class="txt1">
                                            컴패션 스토어의 <em>수익금은 한국컴패션에 기부</em>되어<br />
                                            전세계 가난한 어린이들을 위해 쓰여집니다.
                                        </p>
                                        <p class="txt2">
                                            기부된 수익금은 컴패션의 후원금과 같이 국제컴패션을 통해 어린이가 등록되어 있는 수혜국 현지<br />
                                            컴패션 어린이센터로 전달되며, 후원어린이의 전인적인 양육을 위해서 사용됩니다.
                                        </p>
                                    </div>
                                    <button class="btn_back"><span>BACK</span></button>
                                </div>
                            </li>
                            <li>
                                <button class="si_btn btn3">
                                    <span>후원어린이에게 선물을<br />
                                        어떻게 하나요?</span></button>
                                <div class="layer layer3">
                                    <p class="q"><span></span>후원어린이에게 선물을 어떻게 하나요?</p>
                                    <div class="con">
                                        <p class="txt1">
                                            컴패션 스토어에서 <em>‘후원어린이에게 선물로 보낼 수 있는 상품’</em>을<br />
                                            구매하면, 후원어린이에게 직접 전달됩니다.
                                        </p>
                                        <p class="txt2">
                                            자세한 내용은 공지사항의 안내를 참고바랍니다.
											<a href="/store/notice/view/4" class="btn_s_type2 ml10">공지사항 바로가기</a><br />
                                            <span class="tel">문의 : 02-3668-3434</span>
                                        </p>
                                    </div>
                                    <button class="btn_back"><span>BACK</span></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--// 스토어안내 -->

                    <!-- 공지사항 -->
                    <div class="notice_ticker">
                        <div class="conWrap">
                            <span class="tit" ng-click="goNotice($event)" style="cursor: pointer">공지사항</span>
                            <span class="bar"></span>
                            <span class="con"><a href="#" class="elps"></a></span>
                            <span class="date"></span>
                        </div>

                        <button class="prev" style="display: none;" ng-click="prevNextNotice('prev', $event)"><span>PREV</span></button>
                        <button class="next" ng-click="prevNextNotice('next', $event)"><span>NEXT</span></button>
                    </div>
                    <!--// 공지사항 -->
                </div>

            </div>
        </div>
        <!--// e: sub contents -->


        <div class="h100"></div>


    </section>
    <!--// sub body -->

    <!--
	총 : <asp:Literal runat="server" ID="lbTotal" /> <br />
	-->

</asp:Content>
