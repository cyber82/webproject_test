﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="order.aspx.cs" Inherits="store_order" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
<%@ Register Src="/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/pay/kcp_no_form.ascx" TagPrefix="uc" TagName="kcp_no_form" %>
<%@ Register Src="/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>
<%--<%@ Register Src="/store/pay/kcp_form.ascx" TagPrefix="uc" TagName="kcp_form" %>
<%@ Register Src="/store/pay/kcp_no_form.ascx" TagPrefix="uc" TagName="kcp_no_form" %>
<%@ Register Src="/store/pay/payco_form.ascx" TagPrefix="uc" TagName="payco_form" %>
<%@ Register Src="/store/pay/kakaopay_form.ascx" TagPrefix="uc" TagName="kakaopay_form" %>--%>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
    
	<script type="text/javascript" src="/store/order.js?v=2.2"></script>
	<script type="text/javascript">
	
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="hfAddressType" value="" />
	<input type="hidden" runat="server" id="hd_good_mny" value="" />
	<input type="hidden" runat="server" id="hd_good_name" value="" />
	<input type="hidden" runat="server" id="hd_delivery_fee" value="" />

	<input type="hidden" id="addr_domestic_zipcode" runat="server" /> 
	<input type="hidden" id="addr_domestic_addr1" runat="server" /> 
	<input type="hidden" id="addr_domestic_addr2" runat="server" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
    <input type="hidden" id="temp_name" runat="server" /> 
	<input type="hidden" id="temp_mobile" runat="server" />
    <input type="hidden" id="temp_zipcode" runat="server" /> 
    <input type="hidden" id="temp_addr1" runat="server" /> 
    <input type="hidden" id="temp_addr2" runat="server" /> 

	  <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>주문 / 결제</h1>
				<span class="desc">마음이 담긴 따뜻한 손길로 희망을 선물해주세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents store">

			<div class="w980">

				<!-- 주문프로세스 -->
				<div class="order_process">
					<ol>
						<li>
							<span class="wrap">
								<span class="step step1">STEP 01</span>
								<span class="txt">장바구니</span>
							</span>
						</li>
						<li>
							<span class="wrap">
								<span class="step step2 on">STEP 02</span>
								<span class="txt">주문/결제</span>
							</span>
						</li>
						<li>
							<span class="wrap">
								<span class="step step3">STEP 03</span>
								<span class="txt">주문완료</span>
							</span>
						</li>
					</ol>
				</div>
				<!--// 주문프로세스 -->

				<!-- 장바구니 리스트 -->
				<p class="tbl_head tar"><a href="/store/cart/" class="btn_s_type3">장바구니 이동</a></p>
				<div class="tableWrap3 mb40">
					<table class="tbl_type4 order">
						<caption>장바구니 리스트 테이블</caption>
						<colgroup>
							<col style="width:4%">
							<col style="width:13%">
							<col style="width:30%">
							<col style="width:14%">
							<col style="width:9%">
							<col style="width:15%">
							<col style="width:15%">
						</colgroup>
						<thead>
							<tr>
								<th scope="col"></th>
								<th scope="col" colspan="2">상품정보</th>
								<th scope="col">판매가</th>
								<th scope="col">수량</th>
								<th scope="col">상품구분</th>
								<th scope="col">주문금액</th>
							</tr>
						</thead>
						<tbody id="item_container">
					
							
						</tbody>
					</table>
				</div>
				<!--// 장바구니 리스트 -->

				<!-- 주문금액 -->
				<div class="total_sum mb30">
					<span>주문금액 합계<em id="sub_total"></em></span>
					<span class="pluse"></span>
					<span>배송비<em id="delivery_fee"></em></span>
					<span class="sum"></span>
					<span>총 주문 합계<em class="fc_blue" id="total_amount"></em></span>
				</div>
				<!--// 주문금액 -->

				<div class="box_type3 mb60">
					<span class="info_delivery"><em>3만원 이상 구매 및 어린이 선물 구매 시 무료배송 / 3만원 미만 구매 시 배송비 2,500원</em> (어린이 선물을 포함한 총 상품 금액 3만 원 이상인 경우 무료배송)</span>
				</div>

				<!-- 배송정보 -->
				<p class="table_tit" id="deliveryTitle">
					<span class="tit">배송정보</span>
					<span class="nec_info">표시는 필수입력 사항입니다.</span>
				</p>
				<div class="tableWrap2 mb50" id="deliveryContent">
					<table class="tbl_type2">
						<caption>배송정보 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><span>배송지 선택</span></th>
								<td>
									<span class="radio_ui">
										<input type="radio" id="rdoAddrOld" name="rdoAddr" class="css_radio rdoAddr" checked runat="server"/>
										<label for="rdoAddrOld" class="css_label">주문고객과 동일정보</label>
									</span>
									<span class="radio_ui ml30">
										<input type="radio" id="rdoAddrNew" name="rdoAddr" class="css_radio rdoAddr" runat="server"/>
										<label for="rdoAddrNew" class="css_label">새로운 배송지</label>
									</span>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="name"><span class="nec">수령인</span></label></th>
								<td><input type="text" id="name" runat="server" class="input_type2" style="width:300px" maxlength="20" /></td>
							</tr>
							<tr>
								<th scope="row"><label for="mobile"><span class="nec">휴대폰</span></label></th>
								<td><input type="text" id="mobile" class="input_type2 number_only" maxlength="11" style="width:300px" runat="server" placeholder="-없이 숫자만 입력" /></td>
							</tr>
							<tr>

								<th scope="row"><span class="nec">배송주소</span></th>
								<td>
									
									
								
									<label for="addr_domestic_zipcode1" class="hidden">우편번호</label>
									<input type="text" id="addr_domestic_zipcode1" class="input_type2" placeholder="주소" style="width:300px; background:#fdfdfd;border:1px solid #d8d8d8;" disabled/>
									<a href="#" class="btn_s_type1 ml5" id="btn_find_addr">주소찾기</a>
								
									<p id="addr_road" class="fs14 mt15"></p>
									<p id="addr_jibun" class="fs14 mt10"></p>

									<p class="fs13 mt10">주문 후에는 배송지 변경이 불가능합니다.</p>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="phone"><span>비상연락처</span></label></th>
								<td><input type="text" id="phone" class="input_type2 number_only"  maxlength="11" runat="server" style="width:300px" placeholder="-없이 숫자만 입력" /></td>
							</tr>
							<tr>
								<th scope="row" class="line1"><label for="emergency"><span>배송메모</span></label></th>
								<td class="line1">
									<span class="sel_type2 memo_1" style="width:300px;">
										<select id="memo_template" class="" name="" disabled>
											<option value="선택해 주세요">선택해 주세요</option>
											<option value="부재 시 경비실에 맡겨주세요">부재 시 경비실에 맡겨주세요</option>
											<option value="도착 전 연락주세요">도착 전 연락주세요</option>
											<option value="기타">기타</option>
										</select>
									</span>&nbsp;
									<label for="memo" class="hidden">기타사항 입력</label>
									<input type="text" id="memo" runat="server" maxlength="100" class="input_type2" disabled style="width:295px" />
									<span>(</span><span id="count"></span><span>/100자)</span>
									<!-- 추가 필요 -->
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 배송정보 -->

				<!-- 결제정보 -->
				<p class="table_tit">
					<span class="tit">결제정보</span>
					<span class="nec_info">표시는 필수입력 사항입니다.</span>
				</p>
				<div class="tableWrap2 mb30">
					<table class="tbl_type2">
						<caption>배송정보 테이블</caption>
						<colgroup>
							<col style="width:20%" />
							<col style="width:80%" />
						</colgroup>
						<tbody>
							<tr id="tr_paymethod" runat="server">
							
							<%--간편결제와 휴대폰결제 추가 필요 
							<input type="radio" runat="server" id="payment_method_card_" name="payment_method"  checked="true"/> 신용카드 자동이체
							<input type="radio" runat="server" id="payment_method_cms_" name="payment_method"  /> 실시간 계좌이체
							<input type="radio" runat="server" id="payment_method_oversea" name="payment_method"  /> 해외발급카드 즉시결제
							--%>

								<th scope="row"><span class="nec">결제수단</span></th>
								
								<td>
									<span class="radio_ui">
										<input type="radio" id="payment_method_card" name="payment_method" runat="server" class="css_radio" checked />
										<label for="payment_method_card" class="css_label" style="margin-right: 30px;">신용카드</label>

										<input type="radio" id="payment_method_cms" name="payment_method" runat="server" class="css_radio" />
										<label for="payment_method_cms" class="css_label" style="margin-right: 30px;">실시간 계좌이체</label>

										
										<input type="radio" id="payment_method_phone" name="payment_method" runat="server" class="css_radio" />
										<label for="payment_method_phone" class="css_label" style="margin-right: 30px;">휴대폰 결제</label>

                                        <input type="radio" id="payment_method_oversea" name="payment_method" runat="server" class="css_radio" />
										<label for="payment_method_oversea" class="css_label">해외발급 카드</label>
										
									</span>
									<!--
									<span class="radio_ui">
										<input type="radio" id="payment_method_kakao" name="payment_method" runat="server" class="css_radio" />
										<label for="payment_method_kakao" class="css_label relative" style="width:170px"><img src="/common/img/icon/kakaopay.jpg" class="kakao" style="pointer-events:none" alt="kakaoPay" onclick="$('#payment_method_kakao').trigger('click')"/>kakaopay</label>

										<input type="radio" id="payment_method_payco" name="payment_method" runat="server" class="css_radio" />
										<label for="payment_method_payco" class="css_label relative" style="width:170px"><img src="/common/img/icon/payco.jpg" class="payco" style="pointer-events:none" alt="payco" onclick="$('#payment_method_payco').trigger('click')"/>payco</label>
									</span>
									-->
								</td>

							</tr>
							<tr>
								<th scope="row" rowspan="3" class="line1"><span>결제금액</span></th>
								<td class="info_payment">주문금액 합계<span id="sub_total2"></span></td>
							</tr>
							<tr>
								<td class="info_payment">배송비<span id="delivery_fee2"></span></td>
							</tr>
							<tr>
								<td class="info_payment sum line1">결제금액<span id="total_amount2"></span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--// 결제정보 -->

				<div class="box_type2 padding1 mb30">
					<span class="checkbox_ui fs16" style="line-height:18px;">
						<input type="checkbox" class="css_checkbox" id="chk_agree" runat="server"/>
						<label for="chk_agree" class="css_label">&nbsp;&nbsp;&nbsp;<em class="fc_black">위 주문의 상품명, 가격, 배송 정보를 확인하였으며 구매에 동의하시겠습니까?</em> (전자상거래법 제8조 2항)</label>
					</span>
				</div>

				<div class="tac"><asp:LinkButton runat="server" ID="btn_submit" type="button" OnClick="btn_submit_Click" OnClientClick="return;" class="btn_type1">결제하기</asp:LinkButton></div>

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->


	<textarea class="template" style="display:none">
		<tr class="item_row">
			<td></td>
			<td>
				<!-- 제품이미지 사이즈 : 98 * 98 -->
				<div class="prdt_pic"><img src="/" alt="제품 썸네일 이미지" data-id="img" style="width:98px;"/></div>
			</td>
			<td class="tal">
				<p class="prdt_name" data-id="title"></p>
				<p class="prdt_option" data-id="option"></p>
				<p class="prdt_option" data-id="child"></p>
				<!--<button class="btn_type8 btn_one_remove" ><span>삭제</span></button>-->
			</td>
			<td class="color1" data-id="price"></td>
			<td data-id="ea">3</td>
			<td data-id="type">어린이에게 보내는 선물</td>
			<td class="tit" data-id="total">44,000원</td>
		</tr>
	</textarea>
	<!-- 
	디자인 부분에서 사라진듯 
	<div  data-id="child" ></div>


	<a href="#" id="btn_cancel">이전/취소</a>
	-->


</asp:Content>

<asp:Content runat="server" ID="ex_frm" ContentPlaceHolderID="ex_frm">
	<uc:kcp_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_form" />
	<uc:kcp_no_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kcp_no_form" />
    <uc:payco_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="payco_form" />
	<uc:kakaopay_form runat="server" EnableViewState="true" ViewStateMode="Enabled" id="kakaopay_form" />
</asp:Content>
