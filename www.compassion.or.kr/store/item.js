﻿


$(function () {

    $page.init();

});
var $page = {

    processing: false,

    init: function () {


        $("#option").selectbox({

            onOpen: function (inst) {
            },

            onChange: function (val, inst) {
                $page.calculate();
            }
        });


        $("#btn_minus_ea").click(function () {

            var ea = parseInt($("#ea").val()) - 1;
            if (ea < 1) {
                return false;
            }

            $("#ea").val(ea);
            $page.calculate();
            return false;
        })

        $("#btn_plus_ea").click(function () {
            var inventory = parseInt($("#hd_inventory").val());
            var ea = parseInt($("#ea").val()) + 1;
            if (ea > inventory) {
                alert('재고량이 부족 합니다.');
                return false;
            }

            $("#ea").val(ea);
            $page.calculate();
            return false;
        })

        $("#btn_buy , #btn_cart").click(function () {


            if ($page.processing) return false;
            if (!common.checkLogin()) {
                return false;
            }

            var index = $('#option option').index($('#option option:selected'));
            if ($('#option option').size() > 1 && index < 1) {
                alert('옵션을 선택해주세요');
                $("#option").focus();
                return false;
            }

            $page.processing = true;
            var action = $(this).attr("id") == "btn_cart" ? "cart" : "buy";
            $page.setBasket(action);


            return false;
        });

        //이미지 확대
        $(".select_image").click(function () {
            var url = $(this).children($(".src_value")).attr("style");
            $("#main_image").attr("style", url);

            return false;
        })

        // 어린이에게 선물하기
        /*
		$("#btn_show_child").click(function () {

			if (!common.checkLogin()) {
				return false;
			}

			modalShow($("#child_form"), function (obj) {

				$page.setGiftLayerEvent(obj);
				
			}, true, false);

			return false;

		});
		*/

        $("#userid").val(common.getUserId());

        // sns 공유버튼 열림 : down direction
        var timeout = null;
        $(".sns_ani").mouseenter(function () {
            if (timeout != null)
                clearTimeout(timeout);
            $(this).find(".common_sns_group").stop().animate({ "height": "185px", "opacity": "1" }, 300);
        }).mouseleave(function () {
            if (timeout != null)
                clearTimeout(timeout);
            timeout = setTimeout(function () {
                $(".common_sns_group").stop().animate({ "height": "0", "opacity": "0" }, 300);
            }, 100);

        });


        // 탭메뉴 클릭 시, 페이지 스크롤
        $(".tab_type1 li").click(function () {

            var a = $(this).index();
            var content = $($(".detailViewTab")[a]);
            var top = content.offset().top - 100;
            $('body,html').animate({ scrollTop: top }, 400);

            return false;

        })

        $(".cancel_review").click(function () {
            $("#review_content").val("");
            $(".detail_write.review").hide();
        })

        // 제품상세 : 후기작성하기 show/hide
        $(".btn_mainQna").click(function () {
            if (!common.checkLogin()) {
                $event.preventDefault();
                return false;
            }

            if ($(".detail_write.qna").css("display") == "none") {
                $("#qna_content").val("30자 이상")
                $("#qna_cotent_count").text("0")
                $("#qna_isOpen").prop("checked", false);
                $(".detail_write.qna").slideDown(300);
            } else {
                $(".detail_write.qna").slideUp(300);
            }
        })

        $(".cancel_qna").click(function () {
            $("#qna_content").val("")
            $(".detail_write.qna").hide();
        })

        // store 안내 레이어 show/hide
        $(".store_info .si_btn").click(function () {
            $(this).parent().find(".layer").fadeIn("fast");
            return false;
        })

        $(".btn_back").click(function () {
            $(".store_info .layer").fadeOut("fast");
            return false;
        })

        //장바구니담기 확인 레이어
        $(".layer_type1 .btn_close , #go_shopping").click(function () {
            $(".layer_type1.cart").hide();
            return false;
        })

        $("#go_cart").click(function () {
            location.href = '/store/cart';
        })

        $("#qna_content").keyup(function () {
            $("#qna_cotent_count").text($(this).val().length)
        })

        $("#review_content").keyup(function () {
            $("#review_cotent_count").text($(this).val().length)
        })




        setPlaceholder($("#review_content"), "30자 이상")

        setPlaceholder($("#qna_content"), "30자 이상")

        this.calculate();

    },

    setBasket: function (action) {
        //console.log(action);
        var json = {};
        json.user_id = $("#hd_user_id").val();
        json.item_id = $("#hd_item_id").val();
        json.option_name = $("#option option:selected").text();
        json.option = $("#option").val();
        json.option_price = $("#option option:selected").data("price") || 0;
        json.quantity = $("#ea").val();

        var jsonStr = $.toJSON(json);

        //GA Product Start
        dataLayer.push({
            'event': 'Add',
            'ecommerce': {
                'currencyCode': 'KRW',
                'add': {
                    'products': [{
                        'id': $("#hd_item_id").val(),  // 상품 코드           
                        'name': $("#hd_title").val(), // 상품 이름           
                        'brand': '한국컴패션', // 브랜드           
                        'category': $("#hd_gift_flag_name").val() == 'true' ? '어린이선물' : '일반상품', // 상품 카테고리           
                        'price': $("#hd_price").val(), // 가격(\)           
                        'quantity': $("#ea").val(), // 제품 수량           
                        'variant': $("#option option:selected").text() // 상품 옵션        
                    }]
                }
            }
        });
        //GA Product End

        $.post("/api/store.ashx", { t: "set_basket", data: jsonStr }, function (r) {

            $page.processing = false;
            if (r.success) {




                if (action == "cart") {
                    $(".layer_type1.cart").show();
                } else if (action == "buy") {
                    location.href = '/store/cart';
                }

            } else {
                alert(r.message);
            }

        });

        return;


    },

    calculate: function () {

        var price = parseInt($("#hd_price").val());
        var ea = parseInt($("#ea").val());
        var opt_price = $("#option option:selected").data("price") || 0;
        var sub_total = (price + opt_price) * ea;
        //var delivery_fee = 2500;
        var delivery_fee = parseInt($("#hd_delivery_charge").val());
        if (sub_total >= 30000) {
            delivery_fee = 0;
        }

        var total = sub_total + delivery_fee;
        $("#delivery_fee").html(delivery_fee.format());


        $("#total_amount").html(total.format());
        $("#hd_total_amount").val(total);
        $("#hd_delivery_fee").val(delivery_fee);
    },

    validReview: function () {
        if (common.checkLogin()) {
            if ($("#review_content").val().length < 30) {
                alert("30자 이상 입력해야 등록가능합니다.")
                return false;
            }
            return true;
        }
        return false;
    },

    validQna: function () {
        if (common.checkLogin()) {
            if ($("#qna_content").val().length < 30) {
                alert("30자 이상 입력해야 등록가능합니다.")
                return false;
            }
            return true;
        }
        return false;
    },

}

var app = angular.module('cps.page', []);
app.controller("itemCtrl", function ($scope, $http, $filter, popup) {

    // 해당 기능들 review , qna , notice 하위로 넣기 , 메소드명 변경 , 대상 #region 부터 #endregion 까지

    /* Review */
    $scope.review = {
        total: -1,
        list: [],

        params: {
            page: 1,
            rowsPerPage: 3,
            product_idx: $("#key").val(),
            type: 'all'
        },

        // getReviwList
        getList: function (params) {

            params: $.extend($scope.review.params, params);

            $http.get("/api/store.ashx?t=review_list", { params: $scope.review.params }).success(function (r) {
                //console.log(r)
                if (r.success) {

                    $scope.review.list = r.data;

                    $.each($scope.review.list, function () {
                        this.reg_date = new Date(this.reg_date);
                        this.show_detail = false;
                    })

                    $scope.review.total = r.data.length > 0 ? r.data[0].total : 0;

                } else {
                    alert(r.message);
                }
            });
        },

        // 클릭이벤트
        toggle: function (idx) {
            ////console.log(idx);

            var self = $(".tit[data-idx='" + idx + "']")

            if (self.hasClass("on") == true) {
                self.parent().next(".con").slideUp(300);
                self.removeClass("on");
            } else {
                self.parent().parent().parent().find(".con").slideUp(300);
                self.parent().next(".con").slideDown(300);
                self.parent().parent().parent().find(".tit").removeClass("on");
                self.addClass("on");
            }

        },

        // 리뷰 등록 
        add: function () {
            if ($page.validReview()) {
                $http.post("/api/store.ashx", { t: 'add_review', id: $("#key").val(), content: $("#review_content").val() }).success(function (result) {
                    if (result.success) {
                        $("#review_content").val("");
                        $(".detail_write.review").slideUp(300);
                        $scope.review.getList({ page: 1 });
                    } else {
                        alert(result.message);

                    }
                })
            }
        },

        checkBuy: function () {
            if (!common.checkLogin()) {
                $event.preventDefault();
                return false;
            }

            if ($("#buy_check").val() == "N") {
                alert("구매하신 상품만 후기를 작성할 수 있습니다.");
                return false;
            }

            $http.post("/api/store.ashx", { t: 'check_review', id: $("#key").val() }).success(function (result) {

                //console.log(result);
                if (result.success) {

                    if ($(".detail_write.review").css("display") == "none") {
                        $("#review_content").val("30자 이상")
                        $("#review_cotent_count").text("0")
                        $(".detail_write.review").slideDown(300);
                    } else {
                        $(".detail_write.review").slideUp(300);
                    }

                } else {
                    alert(result.message);

                }

            })
        }
    },

    /* QnA */
	$scope.qna = {
	    total: -1,
	    list: [],
	    tempdata: "",


	    params: {
	        page: 1,
	        rowsPerPage: 3,
	        product_idx: $("#key").val(),
	        type: 'all'
	    },

	    // getList
	    getList: function (params) {
	        $scope.qna.params = $.extend($scope.qna.params, params);
	        $http.get("/api/store.ashx?t=qna_list", { params: $scope.qna.params }).success(function (r) {

	            if (r.success) {
	                //console.log(r.data);
	                $scope.qna.list = r.data;
	                $.each($scope.qna.list, function () {
	                    if ($("#_userid").val() == this.user_id) {
	                        this.isopen = false;
	                    }
	                    this.reg_date = new Date(this.reg_date);
	                    this.show_detail = false;



	                    if (this.answer == null || this.answer == "") {
	                        this.answerClass = "ing"
	                        this.answerYN = "진행중";
	                    } else {
	                        this.answerClass = "end"
	                        this.answerYN = "답변<br/>완료";
	                    }

	                    /*
						if (this.answer_date == null) {
							this.answerClass = "ing"
							this.answerYN = '처리중';
						} else {
							this.answerClass = "end"
							this.answerYN = '답변<br /> 완료';
						}
						*/
	                })
	                $scope.qna.total = r.data.length > 0 ? r.data[0].total : 0;
	            } else {
	                alert(r.message);
	            }
	        });
	    },

	    // 클릭이벤트
	    toggle: function (idx) {
	        $(".detail_write.modify.on").slideUp(300, function () { $(this).find("#modify").val(tempdata); $(this).removeClass("on"); })
	        var self = $(".tit[data-qna-idx='" + idx + "']")

	        if (self.hasClass("on") == true) {
	            self.parent().next(".con").slideUp(300);
	            self.removeClass("on");
	        } else {
	            self.parent().parent().parent().find(".con").slideUp(300);
	            self.parent().next(".con").slideDown(300);
	            self.parent().parent().parent().find(".tit").removeClass("on");
	            self.addClass("on");
	        }

	    },

	    toggleUpdate: function (idx) {

	        $(".detail_write.modify.on").slideUp(300, function () { $(this).find("#modify").val(tempdata); $(this).removeClass("on"); })

	        var target = $(".detail_write.modify[data-qna-idx='" + idx + "']");

	        if (target.css("display") == "none") {
	            target.addClass("on").slideDown(300);
	            tempdata = target.find(".modify_text").val();
	        } else {
	            target.slideUp(300, function () {
	                $(this).find("#modify").val(tempdata);
	                $(this).removeClass("on");
	            });
	        }
	    },

	    // 문의 등록 수정 삭제 
	    add: function () {
	        if ($page.validQna()) {
	            $http.post("/api/store.ashx", { t: 'add_qna', id: $("#key").val(), content: $("#qna_content").val(), isOpen: $scope.qna_isOpen }).success(function (result) {
	                if (result.success) {
	                    $("#qna_content").val("");
	                    $(".detail_write.qna").slideUp(300);
	                    $scope.qna.getList({ page: 1 });
	                } else {
	                    alert(result.message);
	                }
	            })
	        }
	    },

	    update: function (idx) {
	        if (common.checkLogin()) {

	            var content = $(".modify_text[data-qna-idx='" + idx + "']").val();

	            if (content.length < 30) {
	                alert("30자 이상 입력해야 등록가능합니다.")
	                return false;
	            }

	            $http.post("/api/store.ashx", { t: 'update_qna', id: idx, content: content }).success(function (result) {
	                if (result.success) {
	                    $scope.qna.getList({ page: 1 });
	                } else {
	                    alert(result.message);
	                }
	            })
	        }

	    },

	    delete: function (idx) {
	        if (common.checkLogin()) {
	            if (confirm("삭제하시겠습니까?")) {
	                $http.post("/api/store.ashx", { t: 'delete_qna', id: idx }).success(function (result) {
	                    if (result.success) {
	                        alert("삭제 되었습니다.")
	                        $scope.qna.getList({ page: 1 });
	                    } else {
	                        alert(result.message);
	                    }
	                })
	            }
	        }
	    }
	},

    // 공지사항 관련
	$scope.notice = {
	    list: null,
	    index: 0,
	    total: 0,
	    getList: function () {

	        list = null;
	        $http.get("/api/store.ashx?t=notice_list", { page: 1, rowsPerPage: 10, }).success(function (r) {
	            //console.log(r);
	            if (r.success) {

	                $scope.notice.list = r.data;

	                if (r.data.length > 0) {
	                    $scope.notice.total = r.data[0].total
	                    $(".elps").text(r.data[0].title).attr("href", "/store/notice/view/" + r.data[0].idx);


	                    date = new Date(r.data[0].dtreg);
	                    date = moment(date).format('YYYY.MM.DD')
	                    $(".date").text(date)
	                }

	            } else {
	                alert(r.message);
	            }
	        });
	    },

	    prevNextNotice: function (str, $event) {

	        if (str == "prev" && $scope.notice.index != 0) {
	            $scope.notice.index--;
	        } else if (str == "next" && $scope.notice.index != ($scope.notice.total - 1)) {
	            $scope.notice.index++;
	        }

	        if (str == "prev" && ($scope.notice.index == 0)) {
	            $(".prev").hide();
	            $(".next").show();
	        } else if (str == "next" && ($scope.notice.index == $scope.notice.total - 1)) {
	            $(".next").hide();
	            $(".prev").show();
	        } else {
	            $(".next, .prev").show();
	        }

	        date = new Date($scope.notice.list[$scope.notice.index].dtreg);
	        date = moment(date).format('YYYY.MM.DD')

	        $(".elps").text($scope.notice.list[$scope.notice.index].title).attr("href", "/store/notice/view/" + $scope.notice.list[$scope.notice.index].idx);
	        $(".date").text(date)


	        $event.preventDefault();

	    }
	},

    // 상품이미지 이동
	$scope.image = {

	    page: 1,
	    rowsPerPage: 4,
	    isShow: function (index) {

	        if ((parseInt(index / $scope.image.rowsPerPage + 1)) == $scope.image.page) {

	            return true;
	        }
	        $scope.image.pageCount = (parseInt(index / $scope.image.rowsPerPage));
	        return false;
	    },

	    prev: function ($event) {
	        $event.preventDefault();
	        $scope.image.page--;
	        if ($scope.image.page < 1) {
	            $scope.image.page = 1;
	        }
	    },

	    next: function ($event) {
	        $event.preventDefault();
	        if ($scope.image.pageCount == $scope.image.page) {
	            $scope.image.page++;
	        }

	    }

	},


    // 실행 
    $scope.review.getList();
    $scope.qna.getList();
    $scope.notice.getList();

    // 어린이선물
    $scope.modalChild = {

        total: 0,
        page: 1,
        rowsPerPage: 2,
        data: [],
        list: null,
        container: null,
        processing: false,
        total_ea: 0,
        total_amount: 0,
        child_count: 0,
        item_count: 0,
        title: "",
        image: "",
        instance: null,

        init: function () {
            // 팝업

            popup.init($scope, "/store/item-child", function (modal) {
                $scope.modalChild.instance = modal;
                $scope.modalChild.container = $("#childWrapper");

                var obj = $scope.modalChild.container;
                $scope.modalChild.title = $("#hd_title").val();
                $scope.modalChild.image = $("#image").attr("src");

                var option = obj.find("[data-id=option]");
                option.empty();
                option.append($("#option > option").clone());
                option.change(function () {
                    $scope.modalChild.calculate();
                })

                option.selectbox({

                    onOpen: function (inst) {
                    },

                    onChange: function (val, inst) {
                        $scope.modalChild.calculate();
                    }
                });

            }, { top: 0, iscroll: true });

        },

        show: function ($event) {
            $event.preventDefault();
            if (!common.checkLogin()) {
                return false;
            }


            if (!$scope.modalChild.instance)
                return;


            $scope.modalChild.getChildren();


        },

        hide: function ($event) {
            if ($event) $event.preventDefault();
            if (!$scope.modalChild.instance)
                return;
            $scope.modalChild.instance.hide();

        },

        getChildren: function () {

            $http.get("/api/store.ashx?t=get_children", { params: {} }).success(function (r) {

                if (r.success) {

                    $scope.modalChild.data = $.extend($scope.modalChild.data, r.data);

                    if (r.data.length < 1) {
                        alert("한국컴패션 후원자만 어린이에게 선물하실 수 있습니다.");
                        $scope.modalChild.hide();
                        return;
                    }

                    $scope.modalChild.instance.show();

                    $.each($scope.modalChild.data, function () {
                        this.birthdate = new Date(this.birthdate);
                        this.ea = 0;
                    })

                    $scope.modalChild.total = r.data.length;

                    $scope.modalChild.getList({ page: 1 });

                } else {

                    if (r.action == "not_sponsor") {
                        alert("한국컴패션 후원자만 어린이에게 선물하실 수 있습니다.");
                    } else if (!r.data) {
                        alert("후원가능한 어린이가 없습니다.");
                        return;
                    } else {
                        alert(r.message);
                    }
                }
            });

        },

        getList: function (param) {
            $scope.modalChild.page = param.page;
            var begin = ($scope.modalChild.page - 1) * $scope.modalChild.rowsPerPage;
            var end = ($scope.modalChild.page) * $scope.modalChild.rowsPerPage;

            $scope.modalChild.list = $scope.modalChild.data.slice(begin, end);
        },

        calculate: function () {

            // 선택된 상품수
            $scope.modalChild.item_count = $scope.modalChild.total_ea.format();

            var obj = $scope.modalChild.container;
            var price = parseInt($("#hd_price").val());
            var ea = $scope.modalChild.total_ea;
            var opt_price = obj.find("[data-id=option] option:selected").data("price") || 0;


            $scope.modalChild.total_amount = ((opt_price + price) * ea);
            //console.log($scope.modalChild.total_amount);

            // 선택된 어린이수
            /*
            var count = 0;
            $.each(obj.find("[data-id=ea]"), function () {
                if ($(this).val() != "0") {
                    count++;
                }
            })
            */
            var count = 0;
            $.each($scope.modalChild.data, function () {
                if (this.ea > 0) {
                    count++;
                }
            });

            $scope.modalChild.child_count = count;

        },

        setBasket: function (action) {

            var obj = $scope.modalChild.container;

            var json = {};
            json.user_id = $("#hd_user_id").val();
            json.item_id = $("#hd_item_id").val();
            json.option_name = obj.find("[data-id=option] option:selected").text();
            json.option = obj.find("[data-id=option]").val();
            json.option_price = obj.find("[data-id=option] option:selected").data("price") || 0;
            json.quantity = $scope.modalChild.total_ea;

            var children = [];

            $.each($scope.modalChild.data, function () {
                if (this.ea > 0) {
                    var child = {};
                    child.childId = this.childkey;
                    child.childName = this.namekr;
                    child.ea = this.ea;
                    children.push(child);
                }
            })

            json.children = children;

            var jsonStr = $.toJSON(json);

            //	console.log(json);
            //	return;
            $http.post("/api/store.ashx", { t: "set_basket", data: jsonStr }).success(function (r) {

                $scope.modalChild.processing = false;
                //	console.log(r);
                if (r.success) {
                    if (action == "cart") {
                        if (!confirm('장바구니로 이동하시겠습니까?')) {
                            $scope.modalChild.hide();
                            return;
                        }
                    }

                    location.href = '/store/cart';

                } else {
                    alert(r.message);
                }

            });

            return;
        },

        buy: function (action) {

            if ($scope.modalChild.processing) return false;

            var index = $('[data-id=option] option').index($('[data-id=option] option:selected'));
            if ($('[data-id=option] option').size() > 1 && index < 1) {
                alert('옵션을 선택해주세요');
                $("[data-id=option]").focus();
                return false;
            }

            if ($scope.modalChild.total_ea < 1) {
                alert("상품갯수를 선택해주세요");
                return false;
            }

            if ($scope.modalChild.child_count > 50) {
                alert("한번에 50명 이하로만 가능합니다.");
                return false;
            }

            //	$scope.modalChild.processing = true;
            $scope.modalChild.setBasket(action);

        },

        plusEA: function ($event, item) {
            $event.preventDefault();

            var obj = $scope.modalChild.container;

            var group = item.childkey;
            var inventory = parseInt($("#hd_inventory").val());
            var total = $scope.modalChild.total_ea + 1;
            var ea = parseInt(obj.find("[data-id=ea][data-group='" + group + "']").val()) + 1;
            item.ea = ea;
            if (total > inventory) {
                alert('재고량이 부족 합니다.');
                return false;
            }

            $scope.modalChild.total_ea = total;
            obj.find("[data-id=ea][data-group='" + group + "']").val(ea);
            $scope.modalChild.calculate();
        },

        minusEA: function ($event, item) {
            $event.preventDefault();

            var obj = $scope.modalChild.container;
            var group = item.childkey;
            var total = $scope.modalChild.total_ea - 1;
            var ea = parseInt(obj.find("[data-id=ea][data-group='" + group + "']").val()) - 1;

            if (ea < 0) {
                return;
            }

            item.ea = ea;
            $scope.modalChild.total_ea = total;
            obj.find("[data-id=ea][data-group='" + group + "']").val(ea);
            $scope.modalChild.calculate();

        }


    }
    $scope.modalChild.init();

    //공지사항 가기
    $scope.goNotice = function ($event) {
        $event.preventDefault();
        location.href = "/store/notice/";
    }

    // 스토어 다른상품 상세 페이지
    $scope.goView = function (idx, $event) {
        //현 페이지에 $scope.params가 없음.
        location.href = "/store/item/" + idx //+ "?" + $.param($scope.params);
        $event.preventDefault();
    }

});


