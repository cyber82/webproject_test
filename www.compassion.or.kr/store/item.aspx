﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="item.aspx.cs" Inherits="store_item" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ Register Src="/common/paging.ascx" TagPrefix="uc" TagName="paging" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/store/item.js?v=1.3"></script>
	<script type="text/javascript" src="/common/js/site/moveToMobile.js"></script>
    
</asp:Content>


<asp:Content ID="head_script_GA" runat="server" ContentPlaceHolderID="head_script_GA">
    <script>
    //GA Product Start
    dataLayer.push({
        'ecommerce': {
            'currencyCode': 'KRW',
            'detail': {
                'products': [{
                    'id': '<%=this.ViewState["hd_item_id"].ToString()%>',  // 상품 코드           
                    'name': '<%=this.ViewState["hd_title"].ToString()%>', // 상품 이름           
                    'brand': '한국컴패션', // 브랜드           
                    'category': '<%=this.ViewState["hd_gift_flag_name"].ToString()%>' == 'true' ? '어린이선물' : '일반상품' // 상품 카테고리             
                }]
            }
        }
    });
    console.log('dataLayer', dataLayer);
    //GA Product End
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
	<input type="hidden" id="key" runat="server" />
	<input type="hidden" id="hd_user_id" runat="server" />
	<input type="hidden" id="hd_item_id" runat="server" />
    <input type="hidden" id="buy_check" runat="server" />
	<input type="hidden" id="hd_title" runat="server" />
	<input type="hidden" id="hd_price" runat="server" />
	<input type="hidden" id="hd_total_amount" runat="server" />
	<input type="hidden" id="hd_delivery_fee" runat="server" />
	<input type="hidden" id="hd_inventory" runat="server" />
	<input type="hidden" runat="server" id="hd_delivery_charge" value="" />
	<input type="hidden" id="hd_gift_flag_name" runat="server" />
    

    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="itemCtrl">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>컴패션 <em>스토어</em></h1>
				<span class="desc">마음이 담긴 따뜻한 손길로 희망을 선물해주세요</span>

				
				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents store padding0">

			<div class="view_order">
				<div class="w980">
					<div class="img">
						<!-- 확대 이미지 사이즈 : 397 * 397 -->
						<img src="<%: this.ViewState["image"] %>" id="image" style="display:none;" /> 
						<div class="zoom" id="main_image" style="background:url('<%: this.ViewState["image"] %>') no-repeat center;"></div>

						<!-- 썸네일 이미지 사이즈 : 67 * 67 -->
						<!-- 썸네일 전체 div width : 348px -->
						<!-- 썸네일 개별 button width : 87px -->
						<div class="thumb relative">
							<div class="slide_thumb_wrap">
								<div class="slide_thumb_list">
									
								<asp:Repeater ID="repeater_image" runat="server">
									<ItemTemplate>
										<button class="select_image" type="button" ng-show="image.isShow(<%# Container.ItemIndex%>)">
											<span class="src_value" style="background:url('<%# (Uploader.GetRoot(Uploader.FileGroup.image_shop) + Eval("saved_file_name").ToString()).WithFileServerHost()%>') no-repeat center;">
                                  
											</span>
											<span class="over"></span>

										</button>
									</ItemTemplate>
								</asp:Repeater>
								
								</div>
							</div>
							<button class="prev" ng-click="image.prev($event)"><span></span></button>
							<button class="next" ng-click="image.next($event)"><span></span></button>
						</div>
					</div>

					<!-- 주문 옵션 -->
					<div class="order_detail">
						<div class="tit">
							<p><asp:Literal runat="server" ID="title" /></p>
							<span class="sns_ani down">
								<button class="common_sns_share">공유하기</button>
								<span class="common_sns_group">
									<span class="wrap">
                                        <a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
                                        <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
                                        <a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
                                        <a href="#" title="url 공유" data-role="sns" data-provider="copy"  class="sns_url">url 공유</a>
										
									</span>
								</span>
							</span>
						</div>

						<table class="tbl_order">
							<caption>주문 테이블</caption>
							<colgroup>
								<col style="width:17%" />
								<col style="width:83%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">판매가</th>
									<td><span class="price"><em><asp:Literal runat="server" ID="price" /></em> 원</span><span class="nowSale"><asp:Literal runat="server" ID="status" /></span></td>
								</tr>
								<tr>
									<th scope="row">배송비</th>
									<td>
										<span class="price"><span id="delivery_fee"></span> 원</span>
										<p class="pt5">3만원이상 무료 - 도서산간지역일 경우 추가 배송료가 발생할 수 있습니다.</p>
									</td>
								</tr>
								<tr>
									<td colspan="2"><div class="line"></div></td>
								</tr>
								<tr>
									<th scope="row"><label for="option">옵션선택</label></th>
									<td>
										<span class="sel_type2" style="width:96%;">
											<asp:DropDownList runat="server" class="custom_sel1" ID="option">
											</asp:DropDownList>
										</span>
									</td>
								</tr>
								<tr>
									<th scope="row"><label for="ea">주문수량</label></th>
									<td>
										<div class="btn_quantity">
											<input type="text" id="ea" value="1" class="input_type2" readonly/>
											<button type="button" id="btn_plus_ea" class="pluse">수량추가</button>
											<button type="button" id="btn_minus_ea" class="minus">수량삭제</button>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="2"><div class="line2"></div></td>
								</tr>
								<tr class="sum">
									<th scope="row"><label for="quantity">최종금액</label></th>
									<td class="tar"><p class="price"><em class="em2"><span id="total_amount"></span></em> 원</p></td>
								</tr>
							</tbody>
						</table>

						<div>
							<a href="#" class="btn_type5 fr ml10" runat="server" ID="btn_show_child" ng-click="modalChild.show($event)">후원어린이에게 선물하기<span class="gift"></span></a>
							<a href="#" class="btn_type10 btnCart fr ml10" runat="server" ID="btn_cart">장바구니</a>
							<a href="#" class="btn_type1 fr"  runat="server" ID="btn_buy">구매하기</a>
							
						</div>


						<!-- 장바구니담기 레이어 -->
						<div class="layer_type1 cart">
							<div class="con">상품이 장바구니에 담겼습니다.</div>
							<div class="btn_area">
								<a class="btn_s_type2 mr5" id="go_cart">장바구니 바로가기</a>
								<a class="btn_s_type4" id="go_shopping">쇼핑 계속하기</a>
							</div>
							<button class="btn_close"><span><img src="/common/img/btn/close_2.png" alt="레이어 닫기" /></span></button>
						</div>
						<!--// -->

					</div>
					<!-- 주문 옵션 -->
				</div>
			</div>

			<div class="w980">

				<!-- 상품상세정보 -->
				<ul class="tab_type1 detailViewTab">
					<li style="width:25%" class="on"><a href="#">상품정보</a></li>
					<li style="width:25%"><a href="#">배송안내</a></li>
					<li style="width:25%"><a href="#">상품후기</a></li>
					<li style="width:25%"><a href="#">상품문의</a></li>
				</ul>
				
				<div class="detail_view">
					<asp:Literal runat="server" ID="product_detail"></asp:Literal>
				</div>

				<!--// 상품상세정보 -->

				<!-- 배송안내 -->
				<ul class="tab_type1 detailViewTab">
					<li style="width:25%"><a href="#">상품정보</a></li>
					<li style="width:25%" class="on"><a href="#">배송안내</a></li>
					<li style="width:25%"><a href="#">상품후기</a></li>
					<li style="width:25%"><a href="#">상품문의</a></li>
				</ul>
				
				<div class="delivery_info">
					<div class="wrap">
						<span class="tit ic1">배송안내</span>
						<div class="con">
							<ul>
								<li>
									<em class="em1">3만원 이상 구매 시 무료배송 / 3만원 미만 구매 시 배송비 2,500원</em><br />
									도서산간 지역(제주도 포함), 오지 일부 지역은 운임이 추가될 수 있습니다. (해당되신 분들께는 별도 연락을 드립니다.)
								</li>
								<li>
									컴패션 커피는 오후 1시 30분 마감입니다. 미리 볶아두지 않고 주문 받은 후 로스팅합니다. 따라서 평일에만 주문/출고되며,<br />
									공휴일 전날 주문하시면 공휴일이 지난 다음 날 로스팅하여 출고됩니다.
								</li>
								<li>오후 3시 30분 이전 결제가 완료 된 주문은 당일 출고됩니다.</li>
								<li>
									토요일/공휴일 제외한 평균 배송기간은 출고일로부터 1~2일 소요됩니다.<br />
									단, 지역별/업체별 상황에 따라 배송 예정일이 변경될 수 있습니다.
								</li>
								<li>배송조회는 상품이 출고된 다음날부터 가능합니다.</li>
								<li>택배관련 문의는 담당자(02-3668-3434)에게 연락 주시기 바랍니다.</li>
							</ul>
						</div>
					</div>
					<div class="wrap noline" >
						<span class="tit ic2">기타안내</span>
						<div class="con">
							<ul>
								<li>
									<em>품절 안내</em><br />
									주문하신 상품이 품절인 경우 회원님께 문자로 알려 드리고, 경우에 따라 안내 전화를 드린 후 품절 취소 처리가 진행됩니다.
								</li>
								<li id="product_review">
									<em>주문 취소 안내</em><br />
									상품 취소를 원하시는 경우는 한국컴패션(연락처:02-3668-3434)로 연락 주시기 바랍니다.<br />
									상품이 출고 된 이후는 주문 취소가 불가능 합니다.
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!--// 배송안내 -->


				<!-- 상품후기 -->
				<div >
					<ul class="tab_type1 detailViewTab mb60">
						<li style="width:25%"><a href="#">상품정보</a></li>
						<li style="width:25%"><a href="#">배송안내</a></li>
						<li style="width:25%" class="on"><a href="#">상품후기</a></li>
						<li style="width:25%"><a href="#">상품문의</a></li>
					</ul>

					<div class="storeBoard">

						<!-- set -->
						<div class="set" ng-repeat="item in review.list" >
							<div class="subject" ng-show="review.total > 0">
								<button type="button" class="tit" data-idx="{{item.idx}}" ng-click="review.toggle(item.idx, $event)"><span ng-bind-html="item.body"></span></button>
								<span class="name">
									<span>{{item.user_id}}</span>
									<span class="date">{{item.reg_date | date:'yyyy.MM.dd'}}</span>
								</span>
							</div>
							<div class="con" data-idx="{{item.idx}}" ng-bind-html="item.body"></div>
						</div>
						
						<div class="set" ng-if="review.total == 0">
							<div class="subject">
								<span class="tit nodata" >등록된 리뷰가 없습니다.</span>
							</div>
						</div>
						<!--// set -->
						
					</div>
					
					<!-- page navigation -->
					<div class="tac relative mb50">
						<paging class="small" page="review.params.page" page-size="review.params.rowsPerPage" total="review.total" show-prev-next="true" show-first-last="true" paging-action="review.getList({page : page})"></paging> 
						<button type="button" class="btn_type4 btn_mainReview pos1" ng-click="review.checkBuy()"><span>후기작성하기</span></button>
					</div>
					<!--// page navigation -->

					<!-- 후기작성 -->
					<div class="detail_write review">
						<label for="review_content" class="tit" >후기작성하기</label>
                        <div class="tar mb10"><span id="review_cotent_count">0</span>/1000자</div>
                        
						<textarea id="review_content" name="review_content" maxlength="1000" class="textarea_type1 mb20" style="width:100%" rows="4"></textarea>

						<div class="btn_ac">
							<div>
								<button type="button" class="btn_type2 fl mr10 cancel_review"><span>취소</span></button>
								<button type="button" class="btn_type1 fl" ng-click="review.add()"><span>완료</span></button>
							</div >
						</div >
					</div >
					<!--// -->
				</div>
				<!--// 상품후기 -->


				<!-- 상품문의 -->
				<div id="product_qna">
					<ul class="tab_type1 detailViewTab mb60 mt50" >
						<li style="width:25%"><a href="#">상품정보</a></li>
						<li style="width:25%"><a href="#">배송안내</a></li>
						<li style="width:25%"><a href="#">상품후기</a></li>
						<li style="width:25%" class="on"><a href="#">상품문의</a></li>
					</ul>

					<div class="storeBoard">
						<!-- set -->
						<div class="set" ng-repeat="item in qna.list">
							<div class="subject qna" ng-show="qna.total > 0">
								<button type="button" class="tit" data-qna-idx="{{item.idx}}" ng-click="qna.toggle(item.idx, $event)"><span ng-bind-html="item.question"></span><span class="secret" ng-if="item.isopen"></span></button>
								<span class="name">
									<span>{{item.user_id}}</span>
									<span class="date">{{item.reg_date | date:'yyyy.MM.dd'}}</span>
									<span class="status {{item.answerClass}}" ng-bind-html="item.answerYN"></span>
								</span>
							</div>
							<div class="con"  data-qna-idx="{{item.idx}}">

								
								<p class="q" ng-if="!item.isopen || item.is_owner" ng-bind-html="item.question"></p>
								<p class="a" ng-if="(!item.isopen || item.is_owner) && item.answer != null">{{item.answer}}</p>

								<!-- 비공개글 -->
								<p class="scrt_ment" ng-if="item.isopen && !item.is_owner">비공개 글 입니다.<br />글쓴이와 관리자만 열람 가능합니다.</p>
								<!--// -->
								
								<div class="btn_group" ng-if="item.is_owner">
									<button type="button" class="btn_modify modify"  ng-click="qna.toggleUpdate(item.idx)" ><span>수정</span></button>
									<span class="bar"></span>
									<button type="button" class="btn_del"  ng-click="qna.delete(item.idx)"><span>삭제</span></button>
								</div> 
							</div>

							<!-- 수정하기 -->
							<div class="detail_write modify" style="display:none;" data-qna-idx="{{item.idx}}" ng-if="item.is_owner">
								<label for="modify" class="hidden">상품문의 수정하기</label>
								<textarea id="modify" class="textarea_type1 mb20 modify_text"  data-qna-idx="{{item.idx}}" style="width:100%" rows="4" maxlength="1000">{{item.question}}</textarea>

								<div class="tac">
									<button type="button" class="btn_type1 mr5"  ng-click="qna.update(item.idx)"><span>수정하기</span></button>
									<button class="btn_type2" ng-click="qna.toggleUpdate(item.idx)" ><span>취소</span></button>
								</div>
							</div>
							<!--// -->
							
						</div>
						<div class="set" ng-if="qna.total == 0">
							<div class="subject">
								<span class="tit nodata" >등록된 문의가 없습니다.</span>
							</div>
						</div>
						<!--// set -->
						


					</div>
					
					<!-- page navigation -->
					<div class="tac relative mb50">
                        <paging class="small" page="qna.params.page" page-size="qna.params.rowsPerPage" total="qna.total" show-prev-next="true" show-first-last="true" paging-action="qna.getList({page : page});"></paging>  
						<button type="button" class="btn_type4 btn_mainQna pos1"><span>상품문의하기</span></button>
					</div>
					<!--// page navigation -->

				

					<!-- 문의작성 -->
					<div class="detail_write qna">
						<label for="qna_content" class="tit">상품문의하기</label>
                        <div class="tar mb10"><span id="qna_cotent_count">0</span>/1000자</div>
						<textarea id="qna_content" name="qna_content" class="textarea_type1 mb10" maxlength="1000" style="width:100%" rows="4"></textarea>

						<span class="checkbox_ui">
							<input type="checkbox" class="css_checkbox" id="qna_isOpen" name="qna_isOpen" ng-model="qna_isOpen" ng-bind="qna_isOpen"/>
							<label for="qna_isOpen" class="css_label font1">비밀글</label>
						</span>

						<div class="btn_ac">
							<div>
								<button type="button" class="btn_type2 fl mr10 cancel_qna"><span>취소</span></button>
								<button type="button" class="btn_type1 fl" ng-click="qna.add()"><span>완료</span></button>
							</div>
						</div>
					</div>
					<!--// -->
				</div>
				<!--// 상품문의 -->

				<!-- 스토어 다른상품 -->
				<div class="pt50 mb40">
					<h2 class="result_tit">스토어 다른 상품</h2>

					<!-- 제품리스트 -->
					<div class="prdtList_wrap">
						<ul class="prdtList">
                              <asp:Repeater runat="server" ID="repeater_randomProduct">
                                <ItemTemplate>
                                    <li>
                                        <!-- 이미지 사이즈 : 310 * 310 -->
                                        <div class="img" style="background:url('<%# (Uploader.GetRoot(Uploader.FileGroup.image_shop) + Eval("name_img").ToString()).WithFileServerHost() %>') no-repeat center;"></div>
                                    
                                        <p class="name"><%#Eval("name") %></p>
                                        <p class="price"><em>&#8361;</em>{{<%#Eval("selling_price") %> | number:N0}}</p>

                                        <!-- 마우스 오버 시 -->
                                        <span class="over">
                                            <p class="name2"><%#Eval("name") %></p>
                                            <a class="btn_view1" ng-click="goView(<%#Eval("idx") %> , $event)">바로 구매하기</a>
                                        </span>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
							
						</ul>
					</div>
					<!--// 제품리스트 -->


				</div>
				<!--// 스토어 다른상품 -->

				<!-- 스토어안내 -->
				<div class="store_info">
					<div class="logo"></div>
					<ul>
						<li>
							<button class="si_btn btn1"><span>컴패션 스토어는<br />어떻게 운영할까요?</span></button>
							<div class="layer layer1">
								<p class="q"><span></span>컴패션 스토어는 어떻게 운영 할까요?</p>
								<div class="con">
									<p class="txt1">
										컴패션 스토어는<br />
										<em>한국컴패션</em>에서 운영하는 온라인 쇼핑몰입니다.
									</p>
									<p class="txt2">
										컴패션 스토어는 컴패션과 뜻을 함께하는 기업체, 디자이너, 후원자분의<br />
										협력으로 제작됩니다.
									</p>
								</div>
								<button class="btn_back"><span>BACK</span></button>
							</div>
						</li>
						<li>
							<button class="si_btn btn2"><span>컴패션 스토어의 수익금은<br />어떻게 쓰이나요?</span></button>
							<div class="layer layer2">
								<p class="q"><span></span>컴패션 스토어의 수익금은 어떻게 쓰이나요?</p>
								<div class="con">
									<p class="txt1">
										컴패션 스토어의 <em>수익금은 한국컴패션에 기부</em>되어<br />
										전세계 가난한 어린이들을 위해 쓰여집니다.
									</p>
									<p class="txt2">
										기부된 수익금은 컴패션의 후원금과 같이 국제컴패션을 통해 어린이가 등록되어 있는 수혜국 현지<br />
										컴패션 어린이센터로 전달되며, 후원어린이의 전인적인 양육을 위해서 사용됩니다.
									</p>
								</div>
								<button class="btn_back"><span>BACK</span></button>
							</div>
						</li>
						<li>
							<button class="si_btn btn3"><span>후원어린이에게 선물을<br />어떻게 하나요?</span></button>
							<div class="layer layer3">
								<p class="q"><span></span>후원어린이에게 선물을 어떻게 하나요?</p>
								<div class="con">
									<p class="txt1">
										컴패션 스토어에서 <em>‘후원어린이에게 선물로 보낼 수 있는 상품’</em>을<br />
										구매하면, 후원어린이에게 직접 전달됩니다.
									</p>
									<p class="txt2">
										자세한 내용은 공지사항의 안내를 참고바랍니다.
										<a href="/store/notice/view/4" class="btn_s_type2 ml10">공지사항 바로가기</a><br />
										<span class="tel">문의 : 02-3668-3434</span>
									</p>
								</div>
								<button class="btn_back"><span>BACK</span></button>
							</div>
						</li>
					</ul>
				</div>
				<!--// 스토어안내 -->

			<!-- 공지사항 -->
			<div class="notice_ticker">
				<div class="conWrap">
					<span class="tit" ng-click="goNotice($event)" style="cursor: pointer">공지사항</span>
					<span class="bar"></span>
					<span class="con"><a href="#" class="elps"></a></span>
					<span class="date"></span>
				</div>

				<button class="prev" style="display:none;" ng-click="notice.prevNextNotice('prev', $event)"><span>PREV</span></button>
				<button class="next" ng-click="notice.prevNextNotice('next', $event)"><span>NEXT</span></button>
			</div>
			<!--// 공지사항 -->

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

</asp:Content>
