﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default_test2.aspx.cs" Inherits="_default" MasterPageFile="~/top_without_header.Master" %>

<%@ MasterType VirtualPath="~/top_without_header.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
    <script type="text/javascript" src="/default.js"></script>
    <script type="text/javascript" src="/default.sympathy.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/cookie.js"></script>
    <script type="text/javascript" src="/assets/jquery/jquery.mousewheel.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '267588443715394'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=267588443715394&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


<!--NSmart Track Tag Script-->
<script type='text/javascript'>
callbackFn = function() {};
var _nsmart = _nsmart || [];
_nsmart.host = _nsmart.host || (('https:' == document.location.protocol) ? 'https://' : 'http://');
_nsmart.push([12490, 31583]); // 캠페인 번호와 페이지 번호를 배열 객체로 전달
document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
</script>
<!--NSmart Track Tag Script End..-->


</asp:Content>


<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <!-- main -->
    
    <section class="main">
        <%--<textarea id="test" runat ="server" style="width:350px; height:250px;"></textarea>--%>
        <%--<input type="text" id="test1" runat="server" style="border:1px solid #ff0000" />
        <input type="text" id="test2" runat="server" style="border:1px solid #ff0000" />--%>
        <!-- 후원 탭 -->
        <asp:PlaceHolder runat="server" ID="ph_sec4" Visible="true">
            <div class="main_program">
                <div class="w980">
                    <!-- 일대일어린이양육 -->
                    <div class="prm_con prm_con1">
                        <!-- 박스 -->
                        <div class="boxWrap n3">

                            <asp:Repeater runat="server" ID="repeater_children">
                                <ItemTemplate>
                                    <div class="box">
                                        <div class="snsWrap" style="background-color:white;">
                                            <span class="day"><%#Eval("waitingdays")%>일</span>
                                            <span class="sns_ani">
                                                <span class="common_sns_group">
                                                    <span class="wrap">
                                                        <a href="#" title="페이스북" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/children/?c=" + Eval("childmasterId") %>" data-provider="fb" class="sns_facebook">페이스북</a>
                                                        <a href="#" title="카카오스토리" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/children/?c=" + Eval("childmasterId") %>" data-provider="ks" class="sns_story">카카오스토리</a>
                                                        <a href="#" title="트위터" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/children/?c=" + Eval("childmasterId") %>" data-title="<%#"1:1어린이양육-" + Eval("namekr") %>" data-provider="tw" class="sns_twitter">트위터</a>
                                                        <a href="#" title="url 공유" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/children/?c=" + Eval("childmasterId") %>" data-provider="copy" class="sns_url">url 공유</a>
                                                    </span>
                                                </span>
                                                <button class="common_sns_share">공유하기</button>
                                            </span>
                                        </div>

                                        <div class="child_info">
                                            <span class="pic" style="background: url('<%#Eval("pic")%>') no-repeat center top;">양육어린이사진</span>
                                            <span class="name"><%#Eval("name")%></span>
                                            <p class="info">
                                                <span>국가 : <%#Eval("countryName")%></span><br />
                                                <span class="bar">생일 : <%#Eval("birthDate" , "{0:yyyy.MM.dd}")%> (<%#Eval("age")%>세)</span><span>성별 : <%#Eval("gender")%></span>
                                            </p>
                                        </div>

                                        <div class="more" style="background-color:white;"><a href="/sponsor/children/?c=<%#Eval("childmasterid") %>" target="_blank" onclick="javascript:NTrackObj.callTrackTag('31585', callbackFn, 12490);">더 알아보기</a></div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>

                        </div>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>        

    </section>
    <!--// main -->

</asp:Content>

