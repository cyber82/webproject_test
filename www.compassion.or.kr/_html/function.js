﻿
function setDatepickerEvent() {

	$(".btn_calendar").click(function () {
		$(this).parent().find(".date").trigger("focus");
		return false;
	})

	$(".calendar_date").keydown(function () {
		return false;
	});


	$(".calendar_date").datepicker(
        {
        	dateFormat: 'yy-mm-dd',
        	yearRange: "-100:+0",
        	timeFormat: '',
        	numberOfMonths: 1,
        	monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        	dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        	showTime: false,
        	showHour: false,
        	showMinute: false,
        	closeText: '닫기',
        	currentText: '오늘',
        	changeYear: true,
        	onClose: function () {
        		end = $(this).parent().parent().find(".end:first-child");
        		begin = $(this).parent().parent().find(".begin:first-child");

        		if ($(this).hasClass("begin")) {
        			var date = $(this).val();
        			end.datepicker("option", "minDate", date);
        		}

        		if ($(this).hasClass("end")) {
        			var date = $(this).val();
        			begin.datepicker("option", "maxDate", date);
        		}

        	},
        	onSelect: function (text, e) {
        		$(this).datepicker("hide");
        	}
        }
    );

}

//textarea 용 Placeholder
