﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class participation_event_view : FrontBasePage
{

    const string listPath = "/participation/event/";
    public class DatePick
    {
        public string date { get; set; }
        public int limit { get; set; }
        public int apply { get; set; }
    }

    protected override void OnBeforePostBack()
    {

        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;
        //Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        //Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));

        //김태형 2017.06.01
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else
        {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }

        base.OnBeforePostBack();

        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1)
        {
            Response.Redirect(listPath, true);
        }
        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);

        if (!requests[0].CheckNumeric())
        {
            Response.Redirect(listPath, true);
        }

        base.PrimaryKey = requests[0];

        id.Value = PrimaryKey.ToString();

        btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();
        //btnList2.HRef = listPath + "?" + this.ViewState["q"].ToString();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.@event.First(p => p.e_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<@event>("e_id", Convert.ToInt32(PrimaryKey));
            titleHidden.Value = entity.e_title;
        }
    }

    void ShowChild()
    {

    }

    void ShowSpecialFunding()
    {

    }

    protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.@event.First(p => p.e_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<@event>("e_id", Convert.ToInt32(PrimaryKey));

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-캠페인/이벤트] {0}", entity.e_title);
            this.ViewState["meta_image"] = entity.e_thumb.WithFileServerHost();

            title.Text = entity.e_title;
            titleHidden.Value = entity.e_title;
            article.Text = entity.e_content;
            e_show_child.Value = entity.e_show_child ? "Y" : "N";
            e_show_program.Value = entity.e_show_program ? "Y" : "N";
            campaignId.Value = entity.CampaignID;
            e_sf_prices.Value = entity.e_sf_prices;
            location.Value = entity.e_location;
            e_confirm_display.Value = (entity.e_confirm_display == null ? false : (bool)entity.e_confirm_display) ? "Y" : "N";

            // 기간 노출
            if (entity.e_type != "general")
            {
                phJoinDate.Visible = true;
                if (entity.e_type == "experience")
                {
                    joinDate.Text = "체험전 기간 : " + Convert.ToDateTime(entity.e_open).ToString("yyyy년 MM월 dd일") + " ~ " + Convert.ToDateTime(entity.e_close).ToString("yyyy년 MM월 dd일");
                }
                else
                {
                    joinDate.Text = "신청 기간 : " + Convert.ToDateTime(entity.e_begin).ToString("yyyy년 MM월 dd일") + " ~ " + Convert.ToDateTime(entity.e_end).ToString("yyyy년 MM월 dd일");
                }
            }

            // 당첨자 정보
            if (entity.e_announce == null || entity.e_announce.ToString() == "")
            {
                is_announce.Visible = false;
            }
            else
            {
                if (entity.e_type != "general") bar.Visible = true;

                announce.Text = Convert.ToDateTime(entity.e_announce).ToString("yyyy년 MM월 dd일");
                ShowWinner(Convert.ToDateTime(entity.e_announce), Convert.ToInt32(PrimaryKey), entity.e_winner);
            }

            // 체험전인 경우 일정 검색
            if (entity.e_type == "experience")
            {

                //var timetableList = dao.event_timetable.Where(p => p.et_e_id == Convert.ToInt32(PrimaryKey)).Select(p => new { p.et_closed, p.et_date, p.et_ymd }).OrderBy(p => p.et_ymd).ToList();
                var list = www6.selectQ<event_timetable>("et_e_id", Convert.ToInt32(PrimaryKey));
                var timetableList = 
                (
                    from p in list//dao.event_timetable.Where(p => p.et_e_id == Convert.ToInt32(PrimaryKey))
                    orderby p.et_ymd
                    select new
                    {
                        et_closed = p.et_closed,
                        et_date = p.et_date,
                        et_ymd = p.et_ymd
                    }
                ).ToList();

                scheduleData.Value = timetableList.ToJson();
                //var applyList = dao.sp_event_apply_count_list_f(Convert.ToInt32(PrimaryKey));
                Object[] op1 = new Object[] { "e_id" };
                Object[] op2 = new Object[] { Convert.ToInt32(PrimaryKey) };
                var applyList = www6.selectSP("sp_event_apply_count_list_f", op1, op2).DataTableToList<sp_event_apply_count_list_fResult>();

                applyData.Value = applyList.ToJson();
            }

            // 시간선택 이벤트
            if (entity.e_type == "date" || entity.e_type == "complex")
            {
                //var list = dao.sp_event_apply_date_list_f(Convert.ToInt32(PrimaryKey));
                Object[] op1 = new Object[] { "e_id" };
                Object[] op2 = new Object[] { Convert.ToInt32(PrimaryKey) };
                var list = www6.selectSP("sp_event_apply_date_list_f", op1, op2).DataTableToList<sp_event_apply_date_list_fResult>();

                var pickList = new List<DatePick>();
                foreach (var a in list)
                {
                    var data = new DatePick()
                    {
                        date = a.et_date.ToString("yyyy-MM-dd HH:mm"),
                        limit = a.et_limit,
                        apply = Convert.ToInt32(a.apply_count)
                    };
                    pickList.Add(data);
                }
                picked_date.Value = pickList.ToJson();
            }



            string request_contents = "";
            if (entity.e_type != "general")
            {
                if (UserInfo.IsLogin)
                {
                    // 신청내역 있는경우 저장된 값
                    var exist = www6.selectQ<event_apply>("er_user_id", new UserInfo().UserId, "er_e_id", Convert.ToInt32(PrimaryKey));
                    //if (dao.event_apply.Any(p => p.er_user_id == new UserInfo().UserId && p.er_e_id == Convert.ToInt32(PrimaryKey)))
                    if(exist.Any())
                    {
                        //var data = dao.event_apply.First(p => p.er_user_id == new UserInfo().UserId && p.er_e_id == Convert.ToInt32(PrimaryKey));
                        var data = www6.selectQF<event_apply>("er_user_id", new UserInfo().UserId, "er_e_id", Convert.ToInt32(PrimaryKey));

                        name.Value = data.er_name;
                        phone.Value = data.er_phone;
                        route.Value = data.er_route;
                        count.Value = data.er_count.ToString();
                        er_request_contents.Value = string.IsNullOrEmpty(data.er_request_contents) ? "" : data.er_request_contents;
                        request_contents = er_request_contents.Value;
                        if (data.er_date != null) date.Value = ((DateTime)data.er_date).ToString("yyyy-MM-dd HH:mm");
                        action.Value = "edit";
                    }
                    else
                    {
                        GetUserInfo();
                        action.Value = "add";
                    }
                }
                else
                {
                    action.Value = "add";
                }

            }

            eventEntity.Value = new Dictionary<string, Object> {
                {"e_id" , entity.e_id},
                { "e_type" , entity.e_type},
                { "e_maximum" , entity.e_maximum},
                { "e_closed" , IsClosed(entity)},
                { "e_limit" , entity.e_limit},
                { "e_open" , entity.e_open == null ? "" : Convert.ToDateTime(entity.e_open).ToString("yyyy-MM-dd")},
                { "e_close" , entity.e_close == null ? "" : Convert.ToDateTime(entity.e_close).ToString("yyyy-MM-dd")},
                { "e_maximum_person" , entity.e_maximum_person == null ? -1 : entity.e_maximum_person},
                { "e_time_interval" , entity.e_time_interval == null ? 0 : entity.e_time_interval},
                { "e_request_contents" , entity.e_request_contents == null ? false : entity.e_request_contents}
            }.ToJson();

            UserInfo sess = new UserInfo();

            // 후원자 전용
            if (entity.e_target == "sponsor")
            {
                if (sess != null)
                {
                    var commitInfo = new SponsorAction().GetCommitInfo();
                    if (!commitInfo.success)
                    {
                        isSponsor.Value = "false";
                    }
                    if (sess.ConId.Length != 5 && sess.ConId.Length != 6)
                    {
                        isSponsor.Value = "false";
                    }
                }
            }

            // 당첨여부
            //var winner = dao.event_winner.Where(_ => _.ew_e_id == entity.e_id && _.ew_user_id == sess.UserId).FirstOrDefault();
            var winner = www6.selectQF<event_winner>("ew_e_id", entity.e_id, "ew_user_id", sess.UserId);
            if (winner != null)
            {
                isWin.Value = "1";
            }
        }

    }

    protected bool IsClosed(@event entity)
    {
        var result = entity.e_closed;
        if (!entity.e_closed)
        {
            if (entity.e_type == "count")
            {
                var apply_count = 0;
                var exist = www6.selectQ2<event_apply>("er_e_id = ", entity.e_id, "er_count !=", -1);
                //if (dao.event_apply.Any(p => p.er_e_id == entity.e_id && p.er_count != -1))
                if(exist.Any())
                {
                    //apply_count = dao.event_apply.Where(p => p.er_e_id == entity.e_id && p.er_count != -1).Sum(p => p.er_count);
                    apply_count = www6.selectQ2<event_apply>("er_e_id = ", entity.e_id, "er_count !=", -1).Sum(p => p.er_count);
                }
                if (entity.e_limit != -1 && entity.e_limit <= apply_count)
                {
                    result = true;
                }
            }

            if (entity.e_type != "general")
            {
                if (Convert.ToDateTime(entity.e_end).CompareTo(DateTime.Now) < 0)
                {
                    result = true;
                }
            }
        }
        return result;
    }

    /*
	e_winner가 null인경우는 당첨자 선정 방식이 excel
	excel인경우 event_winner에서 데이터를 가져온다
	*/
    protected void ShowWinner(DateTime e_announce, int e_id, string e_winner)
    {
        if (DateTime.Now > e_announce)
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                winner_div.Visible = true;
                // 엑셀업로드인 경우
                if (e_winner == null)
                {
                    //var winner_list = dao.event_winner.Where(p => p.ew_e_id == e_id).ToList();
                    var winner_list = www6.selectQ<event_winner>("ew_e_id", e_id);

                    for (var i = 0; i < winner_list.Count; i++)
                    {
                        winner_list[i].ew_user_id = winner_list[i].ew_user_id.Mask("*", 3);
                    }

                    repeater_winner.DataSource = winner_list;
                    repeater_winner.DataBind();

                    winner_excel.Visible = true;
                    // 에디터 등록인 경우
                }
                else
                {
                    lbWinner.Text = e_winner;
                    winner_editor.Visible = true;
                }
            }
        }
    }

    protected void GetUserInfo()
    {
        UserInfo sess = new UserInfo();
        // 이름
        if (sess.UserName != "")
        {
            name.Value = sess.UserName;
        }
        // 국내거주인경우 연락처를 컴파스에서 가져온다
        if (sess.LocationType == "국내")
        {
            var comm_result = new SponsorAction().GetCommunications();
            if (comm_result.success)
            {
                SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
                phone.Value = comm_data.Mobile.TranslatePhoneNumber();
            }
        }
    }

}