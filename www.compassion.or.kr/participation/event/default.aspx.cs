﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class participation_event_default : FrontBasePage {


	protected override void OnBeforePostBack() {


        

        base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);

		this.GetMainVisual();
	}


	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
	}


	void GetMainVisual()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_mainpage_list_f(10, "participation_event");
            Object[] op1 = new Object[] { "count", "position" };
            Object[] op2 = new Object[] { 10, "participation_event" };
            var list = www6.selectSP("sp_mainpage_list_f", op1, op2).DataTableToList<sp_mainpage_list_fResult>();

            repeater_main_visual.DataSource = list;
            repeater_main_visual.DataBind();
        }
	}

}