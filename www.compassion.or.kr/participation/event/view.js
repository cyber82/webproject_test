﻿
$(function () {

    //브라우저 체크 
    function browerCheck(type) {

        // 재활용을 위해 "hackerc_Agent" 에 정보를 저장.
        var hackerc_Agent = navigator.userAgent;
        var result = hackerc_Agent.match('LG | SAMSUNG | Samsung | iPhone | iPod | Android | Windows CE | BlackBerry | Symbian | Windows Phone | webOS | Opera Mini | Opera Mobi | POLARIS | IEMobile | lgtelecom | nokia | SonyEricsson');

        var pc = 'http://www.';
        var mobile = 'http://m.';

        if (type == 'pc') {
            if (result == null) {
                link = location.href.replace(mobile, pc)
                location.href = link;
                return false;
            }
        } else if (type == 'mobile') {
            if (result != null) {
                //link = location.href.replace(pc, mobile)
                //location.href = link;


                (function () {

                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'http://m.compassion.or.kr/';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'http://m.compassionkr.com/';
                    else if (loc.indexOf('localhost') != -1) host = 'http://localhost:35563/';


                    var http_us = host;// "http://m.compassion.or.kr/"; //모바일이동URL
                    var http_param = [];
                    var str_param = "";
                    if (document.location.search != "") {
                        http_param.push(document.location.search.replace(/^\?/, ""));
                    }
                    if (document.referrer != "" && !/OV_REFFER/.test(document.location.search)) {
                        http_param.push("OV_REFFER=" + document.referrer);
                    }
                    if (http_param.length > 0) {
                        str_param = (/\?/.test(http_us) ? "&" : "?") + http_param.join("&");
                    } else {
                        str_param = "";
                    }
                    location.href = http_us + str_param;
                })();

                return false;
            }
        }

    }


    //모바일로 접속시 모바일 홈페이지로 이동 
    if (getParameterByName("pc") != "ok") {
        if (cookie.get("locationMobile") != 1) {
            browerCheck('mobile');
        }
    } else {
        cookie.set("locationMobile", 1, 1);
    }

});

(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, popup, paramService) {

        $eventEntity = $.parseJSON($("#eventEntity").val());

        // 일정 팝업
        $scope.modalSchedule = {
            instance: null,

            init: function () {
                // 체험전
                if ($eventEntity.e_type == "experience") {
                    popup.init($scope, "/participation/event/experience.html", function (modalSchedule) {
                        $scope.modalSchedule.instance = modalSchedule;

                        $exper.init();

                    }, { top: 0, iscroll: true, backgroundClick: 'n' });
                } else if ($eventEntity.e_type != "general") {
                    popup.init($scope, "/participation/event/event.html", function (modalSchedule) {
                        $scope.modalSchedule.instance = modalSchedule;
                        $event.init();
                    }, { backgroundClick: 'n' });
                }
            },

            show: function () {

                if (!$scope.modalSchedule.instance)
                    return;

                if ($eventEntity.close == "True") {
                    alert("지원이 마감되었습니다.");
                    return false;
                }

                if (common.checkLogin()) {

                    if ($("#isSponsor").val() == "false") {
                        alert("후원자만 신청 가능한 이벤트입니다.");
                        return false;
                    }

                    $scope.modalSchedule.instance.show();
                }
            },

            close: function ($event) {

                $event.preventDefault();
                if (!$scope.modalSchedule.instance)
                    return;
                $scope.modalSchedule.instance.hide();
                location.reload();
            }
        }
        $scope.modalSchedule.init();

        $scope.showChildPop = function ($event) {
            loading.show();
            var item = $page.childInfo;
            if ($event) $event.preventDefault();
            popup.init($scope, "/common/child/pop/" + item.countrycode + "/" + item.childmasterid + "/" + item.childkey, function (modal) {
                modal.show();

                initChildPop($http, $scope, modal, item);

            }, { top: 0, iscroll: true, removeWhenClose: true });
        }

        // 참석 확정 클릭 이벤트 추가 문희원 2017-06-28
        $scope.confirmClick = function () {
            if (confirm("신청을 확정하시겠습니까? ")) {

                var param = {
                    id: $("#id").val(),
                };

                $.post("/api/event.ashx?t=confirm", param, function (r) {
                    if (r.success) {

                        $("#btnCancel").hide();
                        $("#btnCancel2").hide();
                        $(".btnEdit").hide();
                        $("#btnUpdate").hide();
                        $("#btnUpdate2").hide();
                        $("#btnconfirm").hide();
                        $("#btnconfirm2").hide();

                        alert(r.message + "홈페이지로 이동합니다.");

                        var host = '';
                        var loc = String(location.href);
                        if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
                        else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
                        else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
                        if (host != '') location.href = 'http://' + host;

                    } else {
                        alert(r.message);
                    }
                });

            }
        }

    });

})();

var $eventEntity = {};

// 시사회
var $exper = {
    scheduleData: {},
    scheduleArray: [],
    applyData: [],
    page: 1,
    rows: 7,
    hasNext: false,
    template: null,
    info: {
        date: null,
        name: null,
        phone: null,
        count: null,
        route: null,
        applycount: null
    },
    action: null,
    init: function () {

        moment.locale('ko');
        $exper.info.date = $("#date").val();


        if ($("#applyData").val() != "") {
            $exper.applyData = $.parseJSON($("#applyData").val());
        }

        // 템플릿
        $exper.template = $(".fn_pop_content").find(".schedule").clone();
        $(".fn_pop_content").find(".schedule").remove();

        $exper.setSchduleData();
        $exper.setScheduleArray();

        $exper.setMode();
        $exper.initPopup();

    },

    setMode: function () {
        var action = $("#action").val();

        // 신청 여부에 따라 버튼 노출
        if (action == "add") {
            if (!$eventEntity.e_closed) {
                $("#btnApply").show();
                $(".btnAdd").show();
            }
            $exper.step1();
        } else if (action == "edit") {
            // 신청기간 종료후에는 수정 버튼 미노출
            if (!$eventEntity.e_closed) {
                $("#btnUpdate").show();
            }
            $("#btnCancel").show();
            $(".btnEdit").show();

            $exper.step2();
        }

        $("#btnApply").text("예약");
        $("#btnUpdate").text("예약 정보 변경");
        $("#btnCancel").text("예약 취소");
        $("#winner_btn_cancle").text("예약 취소");

    },

    // 노출 데이터
    setScheduleArray: function () {
        var today = moment();
        var open = moment($eventEntity.e_open);
        var close = moment($eventEntity.e_close).add(1, 'days');

        // 오늘 이전 데이터는 사용할 수 없음
        if (open < today) {
            open = today;
        }

        var index = 0;
        while (open < close) {
            var key = open.format("YYYYMMDD");
            if ($exper.scheduleData[key]) {
                $exper.scheduleArray[index++] = $exper.scheduleData[key];
                //console.log($exper.scheduleData[key]);
            } else {
                // 휴관 일도 날짜는 표시해야함
                $exper.scheduleArray[index++] = open.format("YYYY/MM/DD");
            }

            open = open.add(1, 'days');
        }

    },

    // db데이터 
    setSchduleData: function () {
        var loadedData = $.parseJSON($("#scheduleData").val());

        var list = [];
        var date = "";
        $.each(loadedData, function (i, v) {

            if (i == 0) date = moment(this.et_date).format("YYYYMMDD");

            if (date != moment(this.et_date).format("YYYYMMDD")) {
                $exper.scheduleData[date] = list;
                list = [];

                date = moment(this.et_date).format("YYYYMMDD");
            }


            var entity = {
                date: moment(this.et_date).format("YYYY/MM/DD HH:mm:ss"),
                closed: this.et_closed
            }
            list.push(entity);
        });
        $exper.scheduleData[date] = list;
    },


    initPopup: function () {
        var popup = $(".fn_pop_content");

        // 전화번호 형식
        popup.find(".phone").mobileFormat();

        popup.find(".name").val($("#name").val());
        popup.find(".phone").val($("#phone").val());

        popup.find(".location").text("장소 : " + $("#location").val());

        popup.find(".open").text(moment($eventEntity.e_open).format("YYYY년 MM월 DD일"));
        popup.find(".close").text(moment($eventEntity.e_close).format("YYYY년 MM월 DD일"));


        if ($("#action").val() == "edit") {
            $("#number").val($("#count").val());
            $(".route").val($("#route").val());
        }


        // +, -버튼
        popup.find("#number").buttonCounter(popup.find(".minus"), popup.find(".pluse"), { max: 100, min: 0 });
        $("#number").focusin(function () {
            $("#number").val("");
        });
        $("#number").focusout(function () {
            if ($(this).val() == "") $("#number").val("0");
        });


        $exper.paging();
        $exper.setButtonEvent();

        $(".custom_sel").selectbox({});
    },


    setButtonEvent: function () {

        var popup = $(".fn_pop_content");

        // 방향키
        popup.find(".btnLeft").click(function () {
            $exper.page--;
            $exper.paging();
            return false;
        });
        popup.find(".btnRight").click(function () {
            $exper.page++;
            $exper.paging();
            return false;
        });

        // 닫기
        popup.find(".pop_close").click(function (e) {
            angular.element(document.getElementById('defaultCtrl')).scope().modalSchedule.close(e);
            location.reload();
        });
    },


    step1: function () {
        var popup = $(".fn_pop_content");

        popup.find(".content").hide();
        popup.find(".content-1").show();
        popup.find(".content-2").hide();
        popup.find(".content-3").hide();
        popup.css("height", "1569px");

        popup.find(".period").text('선택 가능 기간 : ' + moment($eventEntity.e_open).format("YYYY년 MM월 DD일") + '~' + moment($eventEntity.e_close).format("YYYY년 MM월 DD일"));

        $(".ex_info").removeClass("mb30");
        popup.find(".stepWrap li").removeClass("on").eq(0).addClass("on");
    },

    step2: function () {
        var popup = $(".fn_pop_content");

        if (!$exper.info.date) {
            alert("일정을 선택해주세요.");
            return false;
        }

        popup.find(".content").hide();
        popup.find(".content-1").hide();
        popup.find(".content-2").show();
        popup.find(".content-3").hide();

        $(".ex_info").addClass("mb30");

        popup.find(".period").text('선택 일시 : ' + moment($exper.info.date).format("YYYY년 MM월 DD일 a HH:mm"));

        popup.css("height", "909px");

        popup.find(".stepWrap li").removeClass("on").eq(1).addClass("on");
    },

    step3: function () {
        var popup = $(".fn_pop_content");

        popup.find(".finish_name").text(popup.find(".name").val());
        popup.find(".finish_count").text(popup.find("#number").val());

        popup.find(".content").hide();
        popup.find(".content-1").hide();
        popup.find(".content-2").hide();
        popup.find(".content-3").show();

        $(".ex_info").addClass("mb30");

        popup.find(".period").text(moment($exper.info.date).format("YYYY년 MM월 DD일 a HH:mm"));

        popup.find(".stepWrap li").removeClass("on").eq(2).addClass("on");
    },

    getPagedData: function () {
        var startIndex = ($exper.page - 1) * $exper.rows;
        var endIndex = $exper.page * $exper.rows;
        $exper.hasNext = endIndex < $exper.scheduleArray.length;

        return $exper.scheduleArray.slice(startIndex, endIndex);
    },

    makeTimetable: function () {

        var data = $exper.getPagedData()

        $(".fn_pop_content").find(".schedule").remove();

        //alert($eventEntity.e_maximum_type + '/' + $eventEntity.e_time_interval);

        $.each(data, function () {

            var scheduleEl = $exper.template.clone();

            var date = null;

            if ($.isArray(this)) {

                date = moment(this[0].date);

                // 10 ~ 20:30까지 
                var printTime = new Date(date.format("YYYY/MM/DD") + " 10:00");
                var endTime = new Date(date.format("YYYY/MM/DD") + " 21:00");

                while (printTime < endTime) {

                    var $scuedule = $.grep(this, function (v, i) {
                        return v.date == moment(printTime).format("YYYY/MM/DD HH:mm:ss");
                    })[0]

                    // 해당 시간에 일정이 있으면
                    if ($scuedule) {
                        $date = moment(printTime);

                        // 마감이면 disabled
                        var applyCount = $.grep($exper.applyData, function (v, i) {
                            return moment(v.er_date).format("YYYY/MM/DD HH:mm:ss") == $scuedule.date;
                        })[0];

                        var applyCnt = 0;
                        var applyPersonCnt = 0;
                        if (applyCount) {
                            applyCnt = applyCount.apply_count;
                            applyPersonCnt = applyCount.apply_person_count;
                        }

                        var $el = $("<li class='item' data-date='" + $date.format("YYYY/MM/DD HH:mm") + "' data-applycount='" + applyCnt + "' data-applypersoncount='" + applyPersonCnt + "'>" + $date.format("HH:mm") + "</li>");

                        if ($scuedule.closed || (applyCount
                            && (
                                applyCount.apply_count >= $eventEntity.e_maximum
                                ||
                                applyCount.apply_person_count >= $eventEntity.e_maximum_person
                            ))) {

                            $el.addClass("full");
                            if ($exper.info.date == $date.format("YYYY/MM/DD HH:mm")) {
                                $exper.info.date = null;
                            }

                            // 선책일이면 on
                        } else {
                            if ($exper.info.date == $date.format("YYYY-MM-DD HH:mm")) {
                                $el.addClass("on");
                            }
                        }

                        scheduleEl.find(".timetable").append($el);
                    } else {
                        var $el = $("<li class='item disabled'></li>");
                        scheduleEl.find(".timetable").append($el);
                    }

                    //printTime = new Date(printTime.getTime() + (30 * 60000));
                    printTime = new Date(printTime.getTime() + (parseInt($eventEntity.e_time_interval, 10) * 60000));



                }

                // 날짜에 시간이 비어있으면 휴관일
            } else {
                date = moment(String(this));
                scheduleEl.find(".timetable").append('<li class="holiday">휴관</li><li class="holiday">휴관</li>');

            }
            scheduleEl.find(".date").html('<span class="day">' + date.format("ddd") + '</span><br />' + date.format("YYYY/MM/DD"));

            $(".fn_pop_content").find(".wrapper").append(scheduleEl);

        });

        // 참여기간 비어있으면 채워
        var emptyDate = moment($eventEntity.e_close);
        if (data.length < 7) {
            for (var i = 0; i < 7 - data.length; i++) {
                emptyDate = emptyDate.add(1, 'days');
                var scheduleEl = $exper.template.clone();
                scheduleEl.find(".date").html('<span class="day">' + emptyDate.format("ddd") + '</span><br />' + emptyDate.format("YYYY/MM/DD"));
                $(".fn_pop_content").find(".wrapper").append(scheduleEl);

                // 빈칸 채우기
                $(".timetable").eq(6 - i).append('<li class="holiday"></li><li class="holiday"></li>');
            }
        }

    },

    paging: function () {

        $exper.makeTimetable();

        // 페이징 버튼
        if ($exper.hasNext) {
            $(".fn_pop_content").find(".btnRight").show();
        } else {
            $(".fn_pop_content").find(".btnRight").hide();
        }
        if ($exper.page > 1) {
            $(".fn_pop_content").find(".btnLeft").show();
        } else {
            $(".fn_pop_content").find(".btnLeft").hide();
        }

        $exper.setClickEvent();
    },

    // 시간 선택
    setClickEvent: function () {
        $(".fn_pop_content").find(".item").click(function () {
            if (!$(this).hasClass("disabled") && !$(this).hasClass("full")) {
                $(".fn_pop_content").find(".item").removeClass("on");
                $(this).addClass("on");

                $exper.info.date = $(this).attr("data-date");
                $exper.info.applycount = $(this).attr("data-applycount");
                $exper.info.applypersoncount = $(this).attr("data-applypersoncount");
            }
            return false;
        });
    },

    apply: function () {
        var obj = $(".fn_pop_content");

        if (obj.find(".name").val() == "") {
            alert("이름을 입력해주세요.");
            obj.find(".name").focus();
            return false;
        }

        if (obj.find(".phone").val() == "") {
            alert("연락처를 입력해주세요.");
            obj.find(".phone").focus();
            return false;
        }

        if (!$.isNumeric(obj.find("#number").val())) {
            obj.find("#number").val(0);
        }

        if (obj.find("#number").val() == 0) {
            alert("신청인원을 입력해주세요.");
            //obj.find("#number").focus();
            return false;
        }

        //step2

        if ($("#action").val() == "edit") {
            var originCount = parseInt($("#count").val(), 10);
            if (parseInt($eventEntity.e_maximum, 10) - parseInt($exper.applyData[0].apply_count, 10) < 0) {
                alert("선택하신 일시에 지원할 수 있는 팀 수를 초과하였습니다.");
                return false;
            }

            var applyCount = $.grep($exper.applyData, function (v, i) {
                return moment(v.er_date).format("YYYY/MM/DD HH:mm:ss") == moment($exper.info.date).format("YYYY/MM/DD HH:mm:ss");
            })[0];

            var applyPersonCnt = 0;
            if (applyCount) {
                applyPersonCnt = applyCount.apply_person_count;
            }

            var isDateChange = (moment($exper.info.date).format("YYYY/MM/DD HH:mm:ss") != moment($('#date').val()).format("YYYY/MM/DD HH:mm:ss"));

            var remainPersonCount = parseInt($eventEntity.e_maximum_person, 10) - (parseInt(applyPersonCnt, 10) - (originCount));
            if (isDateChange) {
                remainPersonCount = parseInt($eventEntity.e_maximum_person, 10) - (parseInt(applyPersonCnt, 10));
            }

            if (obj.find("#number").val() > remainPersonCount) {
                alert("선택하신 일시에 지원할 수 있는 인원 수를 초과하였습니다.\n남은 수는 " + remainPersonCount + "명입니다.");
                return false;
            }
        }
        else {
            //$exper.applyData.apply_count
            if (parseInt($eventEntity.e_maximum, 10) - parseInt($exper.info.applycount, 10) < 1) {
                alert("선택하신 일시에 지원할 수 있는 팀 수를 초과하였습니다.");
                return false;
            }
            var remainPersonCount = parseInt($eventEntity.e_maximum_person, 10) - parseInt($exper.info.applypersoncount, 10);
            if (obj.find("#number").val() > remainPersonCount) {
                alert("선택하신 일시에 지원할 수 있는 인원 수를 초과하였습니다.\n남은 수는 " + remainPersonCount + "명입니다.");
                return false;
            }
        }

        //if (obj.find("#number").val() > 5 || obj.find("#number").val() < 0) {
        //	alert("5명이하로 선택해주세요");
        //	obj.find("#number").focus();
        //	return false;
        //}

        //if (obj.find(".route").val() == "") {
        //	alert("컴패션체험전을 알게 된 경로를 선택해주세요.")
        //	obj.find(".route").focus();
        //	return false;
        //}

        $("#loading_bg").css('z-index', '111990');
        loading.show("처리중입니다.");
 
        var param = {
            date: $exper.info.date,
            name: obj.find(".name").val(),
            phone: obj.find(".phone").val(),
            count: obj.find("#number").val(),
            id: $("#id").val(),
            route: obj.find(".route").val(),
            title: $("#titleHidden").val()
        }

        $.post("/api/event.ashx?t=apply", param, function (r) {
            $("#loading_bg").css('z-index', '1990');
            
            if (r.success) {

                $exper.step3();

            } else {
                alert(r.message);
            }
        });
        return true;
    },

    cancel: function () {

        if (common.checkLogin()) {
            if (confirm("취소하시겠습니까?")) {
                $.post("/api/event.ashx?t=cancel&id=" + $("#id").val(), function (r) {
                    if (r.success) {
                        alert("예약이 취소되었습니다.");
                        location.reload();
                    } else {
                        alert(r.message);
                    }
                });
            }
        }
    }

}

// 캠페인 / 이벤트
var $event = {
    type: "",
    $modal: null,
    template: null,
    action: null,
    init: function () {

        moment.locale('ko');
        $event.action = $("#action").val();

        $event.setMode();
        $event.initPopup();

    },

    setMode: function () {
        var action = $("#action").val();

        // 신청 여부에 따라 버튼 노출
        if (action == "add") {
            if (!$eventEntity.e_closed) {
                $("#btnApply").show();
                $("#btnApply2").show();
                $(".btnAdd").show();
            }
        } else if (action == "edit") {
            // 신청기간 종료후에는 수정 버튼 미노출
            if (!$eventEntity.e_closed) {
                $("#btnUpdate").show();
                $("#btnUpdate2").show();
            }
            $("#btnCancel").show();
            $("#btnCancel2").show();
            $(".btnEdit").show();

            $("#btnUpdate").show();
            $("#btnUpdate2").show();

            // 확정버튼 추가 문희원 2017-06-29
            if ($('#e_confirm_display').val() == 'Y') {
                $("#btnconfirm").show();
                $("#btnconfirm2").show(); //코딩에 없음, 캠페인 등록시 html로 등록(이석호)			

                $("#btnUpdate").show();
                $("#btnUpdate2").show();
                $("#btnCancel").show();
                $("#btnCancel2").show();
            }
        }




        if ($("#isWin").val() == "1") {
            $("#btnCancel").hide();
            $("#btnCancel2").hide();
            $(".btnEdit").hide();
            $("#btnUpdate").hide();
            $("#btnUpdate2").hide();
            $("#btnconfirm").hide();
            $("#btnconfirm2").hide();
        }

        if ($("#id").val() == "43") {
            $("#btnCancel").hide();
            $("#btnCancel2").hide();
            $(".btnEdit").hide();
            $("#btnUpdate").hide();
            $("#btnUpdate2").hide();
            $("#btnconfirm").hide();
            $("#btnconfirm2").hide();
        }

        $("#btnApply").text("신청 하기");
        $("#btnUpdate").text("신청 정보 변경");
        $("#btnCancel").text("신청 취소");
        $("#winner_btn_cancle").text("신청 취소")
    },


    initPopup: function () {
        var popup = $(".fn_pop_content");

        // 전화번호 형식
        popup.find(".phone").mobileFormat();

        popup.find(".name").val($("#name").val());
        popup.find(".phone").val($("#phone").val());


        if ($("#action").val() == "edit") {
            $("#number").val($("#count").val());
            $(".route").val($("#route").val());
            $("#request_contents").val($("#er_request_contents").val());
        }


        // +, -버튼
        popup.find("#number").buttonCounter(popup.find(".minus"), popup.find(".pluse"), { max: $eventEntity.e_maximum, min: 1 });
        $("#number").focusin(function () {
            $("#number").val("");
        });
        $("#number").focusout(function () {
            if ($(this).val() == "") $("#number").val("0");
        });

        $("#maximun_request").text($eventEntity.e_maximum);

        // 신청날짜
        if ($eventEntity.e_type == "date" || $eventEntity.e_type == "complex") {
            popup.find(".date_section").show();

            if ($("#picked_date").val() != "") {
                var list = $.parseJSON($("#picked_date").val());
                popup.find(".date").append('<option value="">선택하세요</option>');
                $.each(list, function () {
                    var pickDate = moment(this.date.toString());
                    if (moment() < pickDate) {
                        var selected = this.date == $("#date").val() ? "selected" : "";
                        var closed = this.limit <= this.apply;
                        var optionTag = '<option value="' + pickDate.format("YYYY/MM/DD HH:mm") + '" ' + (selected ? 'selected data-selected="1" ' : '') + (closed ? 'data-closed="1" ' : '') + '>' + pickDate.format("M월D일(ddd) HH시mm분") + (closed ? " - 마감" : "") + '</option>';
                        popup.find(".date").append(optionTag);
                    }
                });
            }

        }

        //신청 내용
        if (($eventEntity.e_type == "count" || $eventEntity.e_type == "complex" || $eventEntity.e_type == "date")
            && $eventEntity.e_request_contents == true) {
            popup.find(".request-contents").show();
        }

        if ($eventEntity.e_type == "count" || $eventEntity.e_type == "complex") {
            popup.find(".count_div").show();
        }

        if ($event.action == "edit") {
            popup.find("#number").val($("#count").val());
            popup.find(".route option[value='" + $("#route").val() + "']").attr("selected", "selected");
            popup.find(".date option[value='" + moment($("#date").val()).format("YYYY/MM/DD") + "']").attr("selected", "selected");
        }


        $(".custom_sel").selectbox({});


        // 닫기
        popup.find(".pop_close").click(function (e) {
            angular.element(document.getElementById('defaultCtrl')).scope().modalSchedule.close(e);
            location.reload();
        });

    },

    apply: function () {
        var obj = $(".fn_pop_content");
        if (obj.find(".name").val() == "") {
            alert("이름을 입력해주세요.");
            obj.find(".name").focus();
            return false;
        }

        if (obj.find(".name").val().length > 20) {
            alert("이름을 정확히 입력해주세요.");
            obj.find(".name").focus();
            return false;
        }

        if (obj.find(".phone").val() == "") {
            alert("연락처를 입력해주세요.");
            obj.find(".phone").focus();
            return false;
        }

        //if (obj.find("#number:visible").length > 0 && obj.find("#number").val() == 0) {
        //    alert("신청인원을 입력해주세요.");
        //    obj.find("#number").focus();
        //    return false;
        //}

        if (obj.find("#number:visible").length > 0 && (obj.find("#number").val() == 0 || obj.find("#number").val() > $eventEntity.e_maximum)) {
            alert("신청 인원을 1~" + $eventEntity.e_maximum + "명 사이로 입력해주세요.");
            obj.find("#number").focus();
            return false;
        }
        

        if (obj.find(".date_section:visible").length > 0) {
            if (obj.find(".date").val() == "") {
                alert("신청날짜를 선택하세요.");
                obj.find(".date").focus();
                return false;
            }

            var selectedOption = obj.find(".date option:selected");
            if (selectedOption.attr("data-selected") != "1" && selectedOption.attr("data-closed") == "1") {
                alert("선택한 시간은 마감되었습니다.");
                obj.find(".date").focus();
                return false;
            }
        }

        if (obj.find("#request_contents:visible").length > 0 && ($.trim(obj.find("#request_contents").val()) == "" || obj.find("#request_contents").val().length > 200)) {
            alert("신청 내용을 200자 이내로 정확히 입력해주세요");
            obj.find("#request_contents").focus();
            return false;
        }
        

        //if (obj.find(".route").val() == "") {
        //	alert("컴패션체험전을 알게 된 경로를 선택해주세요.")
        //	obj.find(".route").focus();
        //	return false;
        //}

        var param = {
            date: obj.find(".date").val(),
            name: obj.find(".name").val(),
            phone: obj.find(".phone").val(),
            count: obj.find("#number").val(),
            id: $("#id").val(),
            route: obj.find(".route").val(),
            title: $("#titleHidden").val(),
            request_contents: $("#request_contents").val()
        }

        $.post("/api/event.ashx?t=apply", param, function (r) {
            if (r.success) {

                obj.find(".finish_name").text(obj.find(".name").val());
                obj.find(".finish_count").text(obj.find("#number").val() == "-1" ? "1" : obj.find("#number").val());

                obj.find(".content-1").hide();
                obj.find(".content-2").show();

                obj.find(".stepWrap li").removeClass("on").eq(1).addClass("on");

            } else {
                alert(r.message);
            }
        });
        return true;
    },

    cancel: function () {

        if (common.checkLogin()) {
            if (confirm("취소하시겠습니까?")) {
                $.post("/api/event.ashx?t=cancel&id=" + $("#id").val(), function (r) {
                    if (r.success) {
                        alert("취소가 완료되었습니다.");
                        location.reload();

                    } else {
                        alert(r.message);
                    }
                });
            }
        }
    }
}

var $page = {
    childInfo: {},
    init: function () {

        $("#special_funding_price_opt").change(function () {
            $(".guide_comment2").hide();
        });

        if ($("#e_show_child").val() == "Y") {
            $page.showChild();
        } else if ($("#e_show_program").val() == "Y") {
            $page.showSpecialFunding();
            $(".btn_sponsor_checkout").click(function () {

                if ($("#pn_special_funding").css("display") != "none" && $("#special_funding_price").val() == "") {
                    //alert("금액을 입력해주세요.");
                    $(".guide_comment2").show();
                    return false;
                }

                var amount = parseInt($("#special_funding_price").val());
                if (amount % 1000 > 0) {
                    alert("천원단위로 입력가능합니다.");
                    return false;
                }

                var type = $(this).data("type");
                var frequency = $(this).data("frequency");
                var group = $(this).data("group");
                var codeId = $(this).data("codeid");
                var codeName = $(this).data("codename");
                var childMasterId = $(this).data("childmasterid");
                var campaignId = $(this).data("campaignid");
                var relation_key = $(this).data("relation_key");
                var amount = $("#special_funding_price").val();

                // cdsp : childmasterId , special : frequency + amount + campaignId
                var t = (type == "CDSP") ? "go-cdsp" : "go-special";

                $.post("/sponsor/pay-gateway.ashx", {
                    t: t,
                    frequency: frequency,
                    amount: amount,
                    childMasterId: childMasterId,
                    campaignid: campaignId
                }).success(function (r) {
                    //console.log(r);
                    if (r.success) {
                        location.href = r.data;

                    } else {
                        alert(r.message);
                    }
                });

                /*
                $.post("/api/sponsor.ashx?t=checkout", {
                    type: type,
                    frequency: frequency,
                    group: group,
                    codeId: codeId,
                    codeName: codeName,
                    childMasterId: childMasterId,
                    campaignid: campaignId,
                    relation_key: relation_key,
                    amount: amount
                }).success(function (r) {
                    //console.log(r);
                    if (r.success) {
                        location.href = r.data;
                        
                    } else {
                        alert(r.message);
                    }
                });
                */

                return false;
            })
        }



    },

    // 1:1 후원인경우 
    showChild: function () {

        $.get("/api/tcpt.ashx?t=event-child", {}).success(function (r) {
            if (r.success) {

                var list = r.data;
                if (list.length > 0) {
                    $.each(list, function () {
                        this.birthdate = new Date(this.birthdate);
                    });

                    var entity = list[0];
                    $("#pn_child").show();

                    $("#c_name").text(entity.name);
                    $("#c_img").css("background-image", 'url("' + entity.pic + '")');
                    $("#c_gender").text(entity.gender);
                    $("#c_age").text(entity.age);
                    $("#c_birthdate").text(moment(new Date(entity.birthdate)).format('YYYY.MM.DD'))
                    $("#c_country").text(entity.countryname);
                    $("#c_waitingdays").text(entity.waitingdays + "일");

                    $page.childInfo = entity;
                    /*
					var btn = $(".btn_sponsor_checkout");
					btn.attr("data-type", "CDSP");
					btn.attr("data-frequency", "정기");
					btn.attr("data-childMasterId", entity.childmasterid);
					*/
                }
            } else {
                alert(r.message);
            }
        });
    },

    showSpecialFunding: function () {

        var prices = $("#e_sf_prices").val();
        $("#special_funding_price_opt").append($("<option value=''>직접입력</option>"));
        $.each(prices.split(','), function () {
            $("#special_funding_price_opt").append($("<option value='" + this + "'>" + this.format() + "원</option>"));
        })
        $("#special_funding_price_opt").change(function () {
            $("#special_funding_price").val($(this).val());
        })

        $(".custom_sel2").selectbox({

            onOpen: function (inst) {
            }
        });

        $.get("/api/special-funding.ashx?t=get", { campaignId: $("#campaignId").val() }).success(function (r) {
            //console.log(r);
            if (r.success) {

                var entity = r.data;
                if (entity) {

                    $("#pn_special_funding").show();
                    $("#sf_title").text(entity.campaignname);


                    var btn = $(".btn_sponsor_checkout");
                    btn.attr("data-type", "special");
                    btn.attr("data-frequency", "일시");
                    btn.attr("data-group", entity.accountclassgroup);
                    btn.attr("data-codeId", entity.accountclass);
                    btn.attr("data-codeName", entity.campaignname);
                    btn.attr("data-childMasterId", "");
                    btn.attr("data-campaignId", entity.campaignid);
                    btn.attr("data-relation_key", entity.sf_id);

                }
            } else {
                alert(r.message);
            }
        });
    }
}

