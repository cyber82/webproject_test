﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="participation_event_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
	<script type="text/javascript" src="/participation/event/default.js"></script>

    <!--NSmart Track Tag Script-->
		 <script type='text/javascript'>
            callbackFn = function() {};
            var _nsmart = _nsmart || [];
            _nsmart.host = _nsmart.host || (('https:' == document.location.protocol) ? 'https://' : 'http://');
            _nsmart.push([12490, 32025]); // 캠페인 번호와 페이지 번호를 배열 객체로 전달
            //document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            // document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            /* v 1.4 ---------------------------------*
            *  Nasmedia Track Object, version 1.4
            *  (c) 2009 nasmedia,mizcom (2009-12-24)
            *---------------------------------------*/
            var nasmedia = {};
            var NTrackObj = null;
            nasmedia.track = function(){
                this.camp, this.img = null;
            };
            nasmedia.track.prototype = {
                callTrackTag : function(p, f, c){
                    if(Object.prototype.toString.call(_nsmart) != '[object Array]' || _nsmart.host == undefined || _nsmart.host == '') return false;
                    if(f != undefined && typeof(f) == 'function') {
                        this.img.onload = this.img.onerror = this.img.onabort = f;
                        var no = ((parseInt(c, 10) % 100) < 10 ? '0' + (parseInt(c, 10) % 100): (parseInt(c, 10) % 100));
                        this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + c + '&page=' + p + '&r=' + Math.random();
                    }
                    else this.callNext(0);
                    return false;
                },
                callNext : function(i) {
                    if(i == this.camp.length) return false;
                    this.img.onload = this.img.onerror = this.img.onabort = function(o, i){return function(){o.callNext(i);};}(this, i+1);
                    var no = ((parseInt(this.camp[i][0], 10) % 100) < 10 ? '0' + (parseInt(this.camp[i][0], 10) % 100): (parseInt(this.camp[i][0], 10) % 100));
                    this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + this.camp[i][0] + '&page=' + this.camp[i][1] + '&r=' + Math.random();
                },
                init : function(c) {
                    this.camp = c, this.img = new Image;
                }
            };
            (function(){
                NTrackObj = new nasmedia.track();
            })();
             NTrackObj.init(_nsmart);
        NTrackObj.callTr
		</script>
<!--NSmart Track Tag Script End..-->

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	 <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>캠페인/이벤트</em></h1>
				<span class="desc">참여의 즐거움과 후원의 기쁨을 누려보세요</span>

				<uc:breadcrumb runat="server" />

			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 part">

			<div class="visual_event" id="main_visual_container">
				<!-- sliding set / size : 100% * 481px -->
				<div class="slide_wrap" id="main_visual">
					
						<asp:Repeater runat="server" ID="repeater_main_visual">
							<ItemTemplate>
						
								<a href="<%#Eval("mp_image_link") %>" target="<%#Eval("mp_is_new_window").ToString() == "True" ? "_blank" : "_self" %>" onclick="javascript:NTrackObj.callTrackTag('32026', callbackFn, 12490);">
                                    <div class="img item" style="background:url('<%#Eval("mp_image").ToString().WithFileServerHost()%>') no-repeat center top"></div>
								</a>

							</ItemTemplate>
						</asp:Repeater>

				</div>
				<!--// -->

				<div class="btn_group" style="z-index:10">
					<div class="indi_wrap">
					
					</div>
					<button class="prev"><img src="/common/img/btn/prev_1.png" alt="previous" /></button>
					<button class="next"><img src="/common/img/btn/next_1.png" alt="next" /></button>
				</div>
			</div>
			
			<div class="w980 event">
				<!-- 검색 -->
				<div class="sortWrap clear2">
					<div class="fr relative">
						<label for="k_word" class="hidden">검색어 입력</label>
						<input type="text" name="k_word" id="k_word" ng-enter="search()" class="input_search1" style="width:245px" placeholder="검색어를 입력해 주세요" />
						<a href="#" class="search_area1" ng-click="search()">검색</a>
					</div>
				</div>
				<!--// -->

				<!-- 페이지 당 노출갯수 : 5개 -->
				<ul class="eventList" id="l">
					<li  ng-repeat="item in list">
						<!-- 이미지사이즈 : 410 * 170 -->
						<span class="img"><a href="#" ng-click="NTrackObj.callTrackTag('32027', callbackFn, 12490); goView(item.e_id)"><img ng-src="{{item.e_thumb}}" alt="이벤트 이미지" /></a></span>
						<span class="con">
							<p class="label" ng-if="item.e_target == 'sponsor'">후원자 전용</p>
							<a href="#" ng-click="goView(item.e_id)" class="tit" onclick="javascript:NTrackObj.callTrackTag('32027', callbackFn, 12490);">{{item.e_title}}</a>
							<p class="txt">
                                <div ng-if="item.e_type == 'general'">
                                </div>
                                <div ng-if="item.e_type == 'date' || item.e_type == 'count' || item.e_type == 'complex'">
                                    신청 기간 : {{parseDate(item.e_begin) | date:'yyyy년 MM월 dd일'}} ~ {{parseDate(item.e_end) | date:'yyyy년 MM월 dd일'}}
                                </div>
                                <div ng-if="item.e_type == 'experience'">
                                    체험전 기간 : {{parseDate(item.e_open) | date:'yyyy년 MM월 dd일'}} ~ {{parseDate(item.e_close) | date:'yyyy년 MM월 dd일'}}
                                </div>
								<span ng-if="item.e_announce">
								당첨자 발표 : {{parseDate(item.e_announce) | date:'yyyy년 MM월 dd일'}} 
								</span>
							</p>
						</span>
						<span class="status">
							<span class="ing" ng-if="!item.event_closed">진행 중</span>
							<span class="end" ng-if="item.event_closed">마감</span>
						</span>
					</li>
					<li class="no_content" ng-if="total == 0">
						검색 결과가 없습니다.
					</li>
				</ul>

				<!-- page navigation -->
				<div class="tac">
					<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
				</div>
				<!--// page navigation -->

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
  
    
</asp:Content>