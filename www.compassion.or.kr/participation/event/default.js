﻿(function () {


	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

        $scope.total = -1;
        $scope.rowsPerPage = 5;

        $scope.list = [];
        $scope.params = {
        	page: 1,
        	rowsPerPage: $scope.rowsPerPage
        };

		// 파라미터 초기화
        $scope.params = $.extend($scope.params, paramService.getParameterValues());
        if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);

        // 검색
        $scope.search = function (params) {
        	$scope.params.page = 1;
        	$scope.params = $.extend($scope.params, params);
        	$scope.params.k_word = $("#k_word").val();
        	$scope.getList();
        }

        // list
        $scope.getList = function (params) {
        	$scope.params = $.extend($scope.params, params);
        	$http.get("/api/event.ashx?t=list", { params: $scope.params }).success(function (result) {
        		$scope.list = result.data;
        		$scope.total = result.data.length > 0 ? result.data[0].total : 0;

        		if (params)
        			scrollTo($("#k_word"), 30);
        	});


        	if (params)
        		scrollTo($("#l"), 10);
        }

        // 상세페이지
        $scope.goView = function (id) {
            $http.post("/api/participation.ashx?t=hits_event&id=" + id).then().finally(function () {
                location.href = "/participation/event/view/" + id + "?" + $.param($scope.params);
        	});
        	
        }

        $scope.parseDate = function (datetime) {
        	return new Date(datetime);
        }

        $scope.mainVisual = function () {
        	var root = $("#main_visual_container");

        	// 메인비주얼
        	if (root.length < 1) {
        		root.find(".btn_group").hide();
        		return;
        	}

        	if ($("#main_visual").find(".item").length < 2) {
        		root.find(".btn_group").hide();
        		return;
        	}

        	root.find("#main_visual").slidesjs({
        		width: $(window).width(),
        		height: $(window).height(),

        		play: {

        			active: true,
        			effect: "slide",
        			interval: 5000,
        			auto: true,
        			swap: true,
        			pauseOnHover: false,
        			restartDelay: 2500
        		},

        		effect: {
        			slide: {
        				speed: 1000
        			},
        			fade: {
        				speed: 300,
        				crossfade: true
        			}
        		},

        		callback: {
        			loaded: function () {
        			},
        			start: function (number) {

        			},
        			complete: function (number) {
        				root.find(".indi_wrap button").removeClass("on");
        				$(root.find(".indi_wrap button")[number - 1]).addClass("on")
        			}
        		}

        	});


        	$.each($("#main_visual").find(".item"), function (i) {
        		var item = $('<button' + (i == 0 ? ' class="on"' : '') + ' type="button">O</button>');
        		item.click(function () {
        			root.find('a[data-slidesjs-item=' + i + ']').trigger("click");		// 이동
        		})
        		root.find(".indi_wrap").append(item);
        	})

        	var btn_pause = $('<button type="button" class="pause">pause</button>');
        	var btn_play = $('<button type="button" class="play">play</button>');
        	root.find(".indi_wrap").append(btn_pause);
        	root.find(".indi_wrap").append(btn_play);

        	btn_pause.click(function () {
        		root.find(".slidesjs-stop").trigger("click");
        		return false;
        	});

        	btn_play.click(function () {
        		root.find(".slidesjs-play").trigger("click");
        		return false;
        	});

        	root.find(".prev").click(function () {
        		root.find(".slidesjs-previous").trigger("click");
        		return false;
        	});

        	root.find(".next").click(function () {
        		root.find(".slidesjs-next").trigger("click");
        		return false;
        	});

        }

        $scope.getList();

        $scope.mainVisual();
    });

	
})();

