﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="participation_event_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<%--<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />--%>
    <meta property="og:image" content="http://www.compassion.or.kr/common/img/sns_default_image.jpg" />
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/common/js/site/moveToMobile.js"></script>
    <script type="text/javascript" src="/assets/moment/moment.min.js"></script>
    <script type="text/javascript" src="/assets/moment/locales.min.js"></script>
    
    <script type="text/javascript" src="/participation/event/view.js?v=3.0"></script>
    <script type="text/javascript">
        $(function () {
            /*
            if ($("#e_type").val() == "general") {
                // 없음
            } else if ($("#e_type").val() == "experience") {
                $exper.init();
            } else {
                $event.init();
            }
            */
            $page.init();
        });

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<input type="hidden" id="e_show_child" runat="server" value="N" />	<!-- 1:1어린이후원 사용여부 -->
	<input type="hidden" id="e_show_program" runat="server" value="N"/>	<!-- 특별한 후원 사용여부 -->
	<input type="hidden" id="campaignId" runat="server" />		<!-- 특별한 후원 아이디 -->
	<input type="hidden" id="e_sf_prices" runat="server" /> <!-- 특별한 후원 금액 -->

    <input type="hidden" id="id" runat="server" />
    <input type="hidden" id="eventEntity" runat="server" />
    <input type="hidden" id="scheduleData" runat="server" />
    <input type="hidden" id="applyData" runat="server" />

    <input type="hidden" id="name" runat="server" />
    <input type="hidden" id="phone" runat="server" />
    <input type="hidden" id="count" runat="server" />
    <input type="hidden" id="route" runat="server" />
    <input type="hidden" id="date" runat="server" />
    <input type="hidden" id="location" runat="server" />
    <input type="hidden" id="action" runat="server" />

    <input type="hidden" id="isWin" runat="server" />
    <input type="hidden" id="er_request_contents" runat="server" />

    
    <input type="hidden" id="isSponsor" runat="server" value="true"/>
    <input type="hidden" runat="server" id="picked_date" />
    <input type="hidden" id="titleHidden" runat="server" />
    <input type="hidden" id="e_confirm_display" runat="server" />

	  <!-- sub body -->
    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl" id="defaultCtrl">
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>캠페인/이벤트</em></h1>
				<span class="desc">참여의 즐거움과 후원의 기쁨을 누려보세요</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents part">

			<div class="w980 event">
				
				<!-- 게시판 상세 -->
				<div class="boardView_1 line1">
					<div class="tit_box noline">
						<span class="tit"><asp:Literal runat="server" ID="title" /></span>
						<span class="txt">
                            <asp:PlaceHolder runat="server" ID="phJoinDate" Visible="false">
							    <span><asp:Literal runat="server" ID="joinDate" /></span>
                            </asp:PlaceHolder>
							<span class="bar" runat="server" id="bar" visible="false"></span>
							<asp:PlaceHolder runat="server" ID="is_announce">
							    <span>당첨자 발표 : <asp:Literal runat="server" ID="announce"></asp:Literal></span>
							</asp:PlaceHolder>
						</span>
					</div>

					<!-- 당첨자 있는 경우 엑셀로 등록시-->
                    <asp:PlaceHolder runat="server" id="winner_div" Visible="false">
                        <asp:PlaceHolder runat="server" ID="winner_excel" Visible="false">
					        <div class="winner">
						        <p>당첨자 발표</p>
						        <ul class="list clear2">
							        <asp:Repeater runat="server" ID="repeater_winner">
								        <ItemTemplate>
									        <li><%#Eval("ew_user_id") %></li>
								        </ItemTemplate>
							        </asp:Repeater>
						        </ul>
					        </div>
                        </asp:PlaceHolder>
                    
                        <asp:PlaceHolder runat="server" ID="winner_editor" Visible="false">
					        <%--<div class="winner">
						        <p>당첨자 발표</p>
                            </div>--%>
					        <div class="view_contents padding0" style="background:#fdfdfd;padding:25px 20px 10px 25px">
                                 <asp:Literal runat="server" ID="lbWinner"></asp:Literal>
                            </div>
                        </asp:PlaceHolder>
                    </asp:PlaceHolder>

					<!--// 당첨자 있는 경우 -->

					<div class="view_contents padding0">
						<asp:Literal runat="server" ID="article" />
					</div>

					<!-- 어린이후원 기능 설정 시 -->
					<div class="tac mb20" id="pn_child" style="display:none">
						<p class="s_tit1 mb40">후원자님을 기다리는<br />어린이의 손을 잡아 주세요</p>

						<div class="child_on">
							<div class="childBox">
								<div class="snsWrap">
									<span class="day" id="c_waitingdays"></span>
								</div>

								<div class="child_info">
									<span class="pic" id="c_img" style="background:no-repeat center;background-position-y:0;background-size:150px">양육어린이사진</span>
									<span class="name" id="c_name"></span>
									<p class="info">
                                        <span>국가: <span id="c_country"></span></span><br />
										

										<span class="bar">생일: <span id="c_birthdate"></span> (<span><span id="c_age"></span>세</span>)</span><span class="">성별: <span id="c_gender"></span></span>
									</p>
								</div>

								<div class="more">
                                    <a href="#" class="" data-type="CDSP" data-frequency="정기" data-group="" data-codeId="" data-codeName="" data-childMasterId="" data-campaignId="" data-relation_key="" ng-click="showChildPop($event)">더 알아보기</a>
								</div>
							</div>
						</div>
						
					</div>
					<!--// 어린이후원 기능 설정 시 -->

					<!-- 캠페인후원 기능 설정 시 -->
					<div class="box_type5 campaign_on"  id="pn_special_funding" style="display:none">
						<p class="s_tit1 mb40" id="sf_title"></p>

						<div class="tableWrap2">
							<table class="tbl_type2">
								<caption>후원금 입력 테이블</caption>
								<colgroup>
									<col style="width:24%" />
									<col style="width:76%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><span>후원 유형</span></th>
										<td>일시후원</td>
									</tr>
									<tr>
										<th scope="row"><label for="emergency"><span>후원금 입력</span></label></th>
										<td>
											<span class="sel_type2 memo_1" style="width:200px;">
												<select class="custom_sel2" name="" id="special_funding_price_opt" disabled></select>
											</span>&nbsp;
											<label for="special_funding_price_opt" class="hidden">금액 직접 입력</label>
											<input type="text" id="special_funding_price" class="input_type2 number_only" style="width:315px" maxlength="8" placeholder="금액을 입력해 주세요."/>
											<p class="pt10"><span class="guide_comment2" style="display:none;">금액을 입력해 주세요.</span></p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

						<div class="tac">
							<a href="#" class="btn_s_type1 btn_sponsor_checkout" data-type="special" data-frequency="일시" data-group="" data-codeId="" data-codeName="" data-childMasterId="" data-campaignId="" data-relation_key="">후원하기</a>
						</div>

					</div>
					<!--// 캠페인후원 기능 설정 시 -->

					<!-- sns 공유하기 -->
					<div class="share_box">
						<span class="sns_ani">
							<span class="common_sns_group">
								<span class="wrap">
                                    <a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook facebook btn_sns btn_fb">페이스북</a>
                                    <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story story btn_sns btn_ks">카카오스토리</a>
                                    <a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter twitter btn_sns btn_tw">트위터</a>
                                    <a href="#" title="url 공유" data-role="sns" data-provider="copy" class="sns_url clipboard btn_sns btn_clipboard">url 공유</a>

								</span>
							</span>
							<button class="common_sns_share">공유하기</button>
						</span>
					</div>
					<!--// -->
					
				</div>

				<div class="btn_ac relative">
					<a id="btnApply" runat="server" class="btn_type1 ml10 event_btn" style="display:none;" ng-click="modalSchedule.show()">예약</a>
					<a id="btnUpdate" runat="server" class="btn_type1 ml10 event_btn" style="display:none;" ng-click="modalSchedule.show()">예약 정보 변경</a>
					<a id="btnCancel" runat="server" class="btn_type2 event_btn" style="display:none;" onclick="$exper.cancel()">예약 취소</a>
                    <a id="btnconfirm" runat="server" class="btn_type1 event_btn" style="display:none; background-color:#C20000;" ng-click="confirmClick()">신청 확정</a>
					<a id="btnList" runat="server" class="btn_type4 posR" title="목록" >목록</a>
				</div>

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->




 
        

    
</asp:Content>