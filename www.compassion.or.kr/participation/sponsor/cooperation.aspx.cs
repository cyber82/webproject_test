﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;
using System.Configuration;

public partial class participation_sponsor_cooperation : FrontBasePage {

	public override bool RequireSSL	{
		get{
			return true;
		}
	}

	protected string queryString {
		set {
			if (!string.IsNullOrEmpty(value))
				this.ViewState["queryString"] = value;
		}
		get {
			return (this.ViewState["queryString"] != null) ? this.ViewState["queryString"].ToString() : string.Empty;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
	}


	protected override void loadComplete(object sender, EventArgs e) {
		
		
	}

	protected void btnAdd_Click(object sender, EventArgs e) {
		try {
			var v_company = company.Text.RemoveXss();
			var v_name = name.Text.RemoveXss();
			var v_email = email.Text.RemoveXss();
			var v_phone = phone.Text.RemoveXss();
			var v_fax= fax.Text.RemoveXss();
			var v_address = address.Text.RemoveXss();
			var v_content= contents.Text.RemoveXss();


			if (v_company == "" || v_name == "" || v_email == "" || v_phone == "" || v_content == "") return;


			var args = new Dictionary<string, string> {
				{"{company}" , v_company} ,
				{ "{name}" , v_name  },
				{ "{email}" , v_email  },
				{ "{phone}" , v_phone  },
				{ "{fax}" , v_fax  },
				{ "{address}" , v_address  },
				{ "{content}" , v_content +"<br/>"+ "담당자 e-mail : "+v_email }
            };

			// to user


			var to = ConfigurationManager.AppSettings["emailCooperation"];
            var to2 = ConfigurationManager.AppSettings["emailCooperation2"];

            var from = ConfigurationManager.AppSettings["emailSender"];             /*20160206 : sender를 info 메일로 고정 (이석호k요청)*/

            //Email.Send(this.Context, v_email, new List<string>() { to },"[컴패션]기업사회공헌-제휴 문의", "/mail/cooperation.html",	args , null);
            Email.Send(this.Context, from, new List<string>() { to, to2 }, "[컴패션]기업사회공헌-제휴 문의", "/mail/cooperation.html", args, null);

        } catch (Exception err) {
			throw err;
		}

        base.AlertWithJavascript("등록되었습니다.");
		


		company.Text = "";
		name.Text = "";
		email.Text = "";
		phone.Text = "";
		fax.Text = "";
		address.Text = "";
		contents.Text = "";

	}

}