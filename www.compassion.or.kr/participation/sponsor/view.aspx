﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="sympathy_view" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">


    <!-- sub body -->
    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>함께하는 기업</em></h1>
                <span class="desc">다양한 분야의 기업들이 컴패션과 함께합니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents part">

            <div class="enterprise">

                <div class="w980">

                    <!-- 게시판 상세 -->
                    <div class="boardView_1 line1">
                        <div class="tit_box noline">
                            <span class="tit">
                                <asp:Literal runat="server" ID="b_title" /></span>
                            <span class="txt">
                                <span>
                                    <asp:Literal runat="server" ID="b_regdate" /></span>
                                <span class="bar"></span>
                                <span class="hit">
                                    <asp:Literal runat="server" ID="b_hits" /></span>
                            </span>
                        </div>

                        <div class="view_contents">
                            <asp:Literal runat="server" ID="b_content" />
                        </div>

                        <!-- sns 공유하기 -->
                        <div class="share_box">
                            <span class="sns_ani">
                                <span class="common_sns_group">
                                    <span class="wrap">

                                        <a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
                                        <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="ssns_story">카카오스토리</a>
                                        <a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
                                        <a href="#" title="url 공유" data-role="sns" data-provider="copy" class="sns_url">url 공유</a>

                                    </span>
                                </span>
                                <button class="common_sns_share">공유하기</button>
                            </span>
                        </div>
                        <!--// -->

                    </div>
                    <!--// 게시판 상세 -->

                    <div class="tar">
                        <a title="목록" runat="server" id="btnList" class="btn_type4 posR">목록</a>
                    </div>

                </div>
            </div>

            <div class="h100"></div>

        </div>
        <!--// e: sub contents -->



    </section>
    <!--// sub body -->





</asp:Content>
