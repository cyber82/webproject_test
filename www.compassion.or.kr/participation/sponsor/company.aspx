﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="company.aspx.cs" Inherits="participation_sponsor_company " Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
    <script type="text/javascript" src="/participation/sponsor/company.js"></script>
    <script>
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <!--<input type="hidden" runat="server" id="test" value=""/> -->

    <!-- sub body -->
    <section class="sub_body" ng-app="cps" ng-cloak >

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>함께하는 기업</em></h1>
                <span class="desc">다양한 분야의 기업들이 컴패션과 함께합니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents part padding0">

            <div class="enterprise">

                <div class="tit_con">
                    <p class="txt1">다양한 분야의 기업과 단체들이 컴패션과 함께합니다.</p>
                    <p class="txt2">가난 가운데에 있는 어린이들에게 희망을 주고자 하는 기업&middot;단체와 함께 더욱 정직하고 투명하게 섬기겠습니다.</p>
                </div>

                <div class="w980">
                    <!-- 기업로고 -->
                    <div class="slide_logo_wrap" ng-controller="sponsorCtrl">
                        <div class="slide_logo_list" id="slides">
                            <div class="logo_list" ng-repeat="list in lists track by $index" start-slides="lists">
                                <div class="img" ng-repeat="item in list" class="inner">
                                    <a href="{{item.c_url}}" target="_blank"><img ng-src="{{item.c_thumb}}" alt="기업 로고이미지" width="170px;" height="80px;"/></a>
                                </div>
                            </div>
                        </div>

                        <button class="btn_com_prev1">이전</button>
                        <button class="btn_com_next1">다음</button>

                        <span class="logo_indi">
							<!--
                            <button class="on">O</button>
                            <button>O</button>
                            <button>O</button>
                            <button>O</button>
							-->
                        </span>

                    </div>
                    <!--// 기업로고 -->

                     <div ng-controller="newsCtrl" id="l">

                        <div class="sub_tit" >
                            <h2>함께하는 기업 소식</h2>
                            <div class="pos1">
                                <span class="fr relative">
                                    <label for="k_word" class="hidden">검색어 입력</label>
                                    <input type="text" class="input_search1 vam" style="width: 245px" name="k_word" id="k_word" ng-enter="search()" placeholder="검색어를 입력해 주세요" />
                                    <a class="search_area1" ng-click="search()">검색</a>
                                </span>
                            </div>
                        </div>

                        <div class="tableWrap1 mb40">
                            <table class="tbl_type5">
                                <caption>공지사항 리스트 테이블</caption>
                                <colgroup>
                                    <col style="width: 70%">
                                    <col style="width: 15%">
                                    <col style="width: 15%">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">제목</th>
                                        <th scope="col">작성일</th>
                                        <th scope="col">조회수</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in list">
                                        <td><a class="subject elps" ng-click="goView(item.b_id)">{{item.b_title}}</a></td>
                                        <td>{{parseDate(item.b_regdate) | date:'yyyy.MM.dd'}}</td>
                                        <td>{{item.b_hits}}</td>
                                    </tr>

                                    <tr ng-if="total == 0">
                                        <td colspan="3" class="no_result">등록된 글이 없습니다.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- page navigation -->
                        <div class="tac">
                            <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>
                        </div>
                        <!--// page navigation -->
                    </div>
                    <!--// 기업소식 -->

                </div>
            </div>

            <div class="h100"></div>

        </div>
        <!--// e: sub contents -->



    </section>
    <!--// sub body -->



</asp:Content>
