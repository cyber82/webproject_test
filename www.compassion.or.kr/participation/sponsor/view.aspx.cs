﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class sympathy_view : FrontBasePage {

	const string listPath = "/participation/sponsor/company";

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		
		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}

		base.PrimaryKey = requests[0];
		
		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString() + "#l";



    }


	protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));

            b_title.Text = entity.b_title;
            b_regdate.Text = entity.b_regdate.ToString("yyyy.MM.dd");
            b_hits.Text = entity.b_hits.ToString("N0");
            b_content.Text = entity.b_content;

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-함께하는 기업] {0}", entity.b_title);

            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = "";
            //this.ViewState["meta_keyword"] = "";
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,FrontBasePage)
            //this.ViewState["meta_image"] = entity.e_thumb.WithFileServerHost();

        }
	}
}