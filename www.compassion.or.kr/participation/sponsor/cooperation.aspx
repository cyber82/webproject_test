﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cooperation.aspx.cs" Inherits="participation_sponsor_cooperation" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    
    <script type="text/javascript">

        $(function () {
            $("#contents").textCount($("#limit"), { limit: 3000 });
        });

        function onSubmit() {

            if (!validateForm([
				{ id: "#company", msg: "기업/단체명을 입력해주세요" },
                { id: "#name", msg: "담당자명을 입력해주세요" },
                { id: "#email", msg: "E-MAIL을 입력해주세요", type: "email" },
                { id: "#phone", msg: "전화번호를 입력해주세요", type: "phone" },
                { id: "#contents", msg: "제휴/후원 내용을 입력해주세요" }
            ])) {
                return false;
            }

            return true;
        }

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <!-- sub body -->
    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>제휴 문의</em></h1>
                <span class="desc">어린이들에게 밝은 미래를 선물하세요</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents part">

            <div class="enterprise">

                <div class="w980">

                    <div class="alliance_con">
                        <p class="tit">기업 가치 상승을 위한 사회공헌의 핵심 키워드</p>

                        <ul class="icon clear2">
                            <li class="icon1">- 사회공헌활동은 단순한 비용이 아니라<br />
                                새로운 가치를 창출하는 투자입니다.</li>
                            <li class="icon2">- 글로벌 사회와의 소통을 바탕으로 한<br />
                                지속가능하고 진정성 있는 사회공헌활동이<br />
                                필요합니다.</li>
                            <li class="icon3">- 내부 고객인 직원들의 참여와 로열티를<br />
                                제고할 수 있는 기업맞춤형 사회공헌이<br />
                                중요합니다.</li>
                        </ul>
                    </div>

                    <div class="alliance_contact">
                        <p class="txt">한국컴패션과의 제휴를 원하시는 경우 아래에 정보를 기입해 주시면 내용 확인 후 연락 드리겠습니다.<br />
                            기타 궁금하신 사항은 아래 연락처로 연락바랍니다.</p>

                        <div class="contact_qna">
                            <p class="tit">문의</p>
                            <div class="clear2">
                                <div class="tel"><span class="icon"><span class="txt">전화 : </span>02-3668-3422, 3512</span></div>
                                <div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:csv@compassion.or.kr">csv@compassion.or.kr</a></span></div>
                            </div>
                        </div>

                        <div class="tar">
                            <a href="/common/download/컴패션기업사회공헌제안서_2016.pdf" class="btn_s_type4">사회공헌제안서 다운로드</a>
                        </div>

                    </div>

                    <!-- 제휴정보 입력 -->
                    <div class="tar mb20">
                        <span class="nec_info">표시는 필수입력 사항입니다.</span>
                    </div>
                    <div class="tableWrap2 mb40">
                        <table class="tbl_type2">
                            <caption>제휴정보 입력 테이블</caption>
                            <colgroup>
                                <col style="width: 20%" />
                                <col style="width: 30%" />
                                <col style="width: 20%" />
                                <col style="width: 30%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <th scope="row">
                                        <label for="company"><span class="nec">기업/단체 명</span></label></th>
                                    <td colspan="3">
                                        <asp:TextBox runat="server" ID="company" class="input_type2" Style="width: 730px" />
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <label for="name"><span class="nec">담당자 명</span></label></th>
                                    <td>
                                        <asp:TextBox runat="server" ID="name" class="input_type2" Style="width: 240px" />
                                    </td>
                                    <th scope="row">
                                        <label for="fax"><span>FAX</span></label></th>
                                    <td>
                                        <asp:TextBox runat="server" ID="fax" class="input_type2 number_only" maxlength="11" Style="width: 240px" />
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <label for="phone"><span class="nec">전화</span></label></th>
                                    <td>
                                        <asp:TextBox runat="server" ID="phone" class="input_type2 number_only" maxlength="11" Style="width: 240px" />
                                    </td>
                                    <th scope="row">
                                        <label for="email"><span class="nec">이메일</span></label></th>
                                    <td>
                                        <asp:TextBox runat="server" ID="email" class="input_type2" Style="width: 240px" />
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <label for="address"><span>주소</span></label></th>
                                    <td colspan="3">
                                        <asp:TextBox runat="server" ID="address" class="input_type2" Style="width: 730px" />
                                    </td>
                                </tr>
                                <tr>
                                    <!--<span id="limit">0</span>/3000 -->
                                    <th scope="row">
                                        <label for="contents"><span>제휴/후원<br />
                                            내용</span></label></th>
                                    <td colspan="3">
                                        <asp:TextBox runat="server" ID="contents" TextMode="MultiLine" class="textarea_type2" Style="width: 730px; height: 380px"></asp:TextBox>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--// 제휴정보 입력 -->

                    <div class="tac">
                        <asp:LinkButton runat="server" ID="btnAdd" class="btn_type1" OnClick="btnAdd_Click" OnClientClick="return onSubmit()">제휴 제안 보내기</asp:LinkButton>
                    </div>






                </div>
            </div>

            <div class="h100"></div>

        </div>
        <!--// e: sub contents -->



    </section>
    <!--// sub body -->








</asp:Content>
