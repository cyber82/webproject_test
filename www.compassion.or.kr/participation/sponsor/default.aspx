﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="participation_sponsor_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">


    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>기업사회공헌</em></h1>
                <span class="desc">컴패션과 함께할 수 있는 특별한 사회공헌 활동을 소개합니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents part padding0">

            <div class="enterprise">

                <div class="bgContent intro">
                    <p class="tit">Great Power Great Responsibility</p>
                    <p class="con2">좋은 기업에서 위대한 기업으로 – 기업사회공헌</p>
                </div>

                <div class="w980 intro">

                    <!-- 참여방법 -->
                    <div class="method">
                        <h2 class="s_tit8">컴패션과 함께하는 사회공헌 프로젝트</h2>
                        <h3 class="s_tit6">기업 사회공헌 참여 방법</h3>

                        <ul class="content clear2">
                            <li>
                                <p class="tit">기업 명의 1:1 어린이 양육</p>
                                <p class="con">기업의 이름으로 어린이를<br />
                                    양육&middot;후원하는 프로그램입니다.</p>
                            </li>
                            <li>
                                <p class="tit">임직원 참여 후원</p>
                                <p class="con">
                                    <span class="fc_black2">1:1 어린이 양육/편지 후원/<br />
                                        끝전 모으기</span><br />
                                    기업 강연 및 사내 캠페인을 통해<br />
                                    임직원의 자발적인 참여로 진행되는<br />
                                    후원 프로그램입니다. 급여의 일부를<br />
                                    후원하거나, 편지 후원으로 함께할 수<br />
                                    있습니다.
                                </p>
                            </li>
                            <li>
                                <p class="tit">매칭 기프트</p>
                                <p class="con">임직원들이 후원하는만큼 기업도<br />
                                    함께 후원하는 프로그램입니다.</p>
                            </li>
                            <li>
                                <p class="tit">기업 시그니처 프로젝트</p>
                                <p class="con">기업 맞춤형 단독 후원으로<br />
                                    기업 사업 분야 또는 사회공헌 방향에<br />
                                    맞춰 단독으로 프로젝트를 후원하고<br />
                                    현지에 기업 명판을 설치할 수 있습니다.</p>
                            </li>
                        </ul>
                    </div>
                    <!--// 참여방법 -->

                    <!-- 협력프로그램 -->
                    <div class="co_program">
                        <h3 class="s_tit6">기업 사회공헌 협력 프로그램</h3>

                        <ul class="content">
                            <li>
                                <p class="tit">Compassion Weekday(기업 강연)</p>
                                <p class="con">
                                    임직원을 대상으로 나눔에 대한 의미를 전달하고 참여를 독려합니다.<br />
                                    * 강사는 일정에 따라 변동 가능
                                </p>
                            </li>
                            <li>
                                <p class="tit">컬래버레이션</p>
                                <p class="con">
                                    브랜드 제품과의 컬래버레이션을 통해 가치를 담은<br />
                                    제품 출시로 후원에 동참합니다.
                                </p>
                            </li>
                        </ul>

                    </div>
                    <!--// 협력프로그램 -->

                </div>

                <!-- 사회공헌 캠페인 -->
                <div class="intro campaign">
                    <div class="w980">
                        <h2 class="s_tit8">기업의 가치 상승과 지속 가능한 사회공헌 캠페인</h2>
                    </div>

                    <div class="bg">
                        <div class="w980">
                            <h3 class="s_tit6">25개국 어린이들의 자립을 위한 <em>기업 사회공헌 캠페인</em></h3>
                            <p class="con">
                                - 수료(졸업)를 3~5년 남겨둔 학생들을 위한 Finishing well 프로그램<br />
                                <br />
                                - 졸업을 앞두고 후원이 중단된 10대 학생들을 기업의 이름으로 지정 후원하여 그들이 학교를 마치고 자립하여<br />
                                경제활동을 하고, 나아가 지역사회의 리더로 성장할 수 있도록 돕는 것이 본 캠페인의 목표입니다.
                            </p>
                        </div>
                    </div>
                </div>
                <!--// 사회공헌 캠페인 -->

                <div class="w980">
                    <!-- 참여방법 -->
                    <div class="intro method_2">

                        <h3 class="s_tit6">참여방법</h3>

                        <div class="box_type3">
                            <span>- 후원 국가/지역을 선발</span>
                            <span>- 후원 기간 지정</span>
                            <span>- 모니터링 트립</span>
                        </div>
                    </div>
                    <!--// 참여방법 -->

                    <div class="tac mb60">
                        <a href="/participation/sponsor/cooperation/" class="btn_type1 mr10">기업 후원/제휴 문의</a>
                        <a href="/participation/sponsor/company/" class="btn_type10">함께하는 기업 현황</a>
                    </div>

                    <div class="contact_qna">
                        <p class="tit">문의</p>
                        <div class="clear2">
                            <div class="tel"><span class="icon"><span class="txt">전화 : </span>02-3668-3457/3442</span></div>
                            <div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:mkim@compassion.or.kr">mkim@compassion.or.kr</a></span></div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="h100"></div>

        </div>
        <!--// e: sub contents -->



    </section>

</asp:Content>
