﻿(function () {

	var app = angular.module('cps.page', []);

	// 함께하는 기업
	app.controller("sponsorCtrl", function ($scope, $http) {
		var rowsPerPage = 30;

		$scope.lists = [];

		// slide하기 위해 2중 array에 담는다
        $scope.getList = function () {

        	$http.get("/api/participation.ashx?t=sponsor_list").success(function (result) {

        		var doubleArray = [];
        		var list = result.data;
        		var total = result.data.length > 0 ? result.data[0].total : 0;

        		var pages = Math.ceil(total / rowsPerPage);
        		var index = 0;
        		$.each(list, function () {
        		    this.c_url = this.c_url.indexOf("http") != -1 ? this.c_url : "http://" + this.c_url;
        		})

        		for (var i = 0; i < pages; i++) {
        			doubleArray[i] = list.slice(index, index + rowsPerPage);
        			index += rowsPerPage;
        		
        		}

        		$scope.lists = doubleArray;
        		
                
        	});
        }

        $scope.getList();
	});

	app.directive('startSlides', function () {
		var directive = {
			restrict: 'A',
			link: function (scope, attrs) {

				// start-slide에 값이 들어오는 시점에 $watch가 동작한다
				scope.$watch(attrs.startSlides, function (value) {

					if ($("#slides").attr("data-is-set") == "Y") return;

					var root = $("#slides");
					root.attr("data-is-set", "Y");
					if (scope.lists.length > 1) {
						root.slidesjs({
							play: {

								active: true,
								effect: "slide",
								interval: 6000,
								auto: true,
								swap: false,
								pauseOnHover: false,
								restartDelay: 2500
							},

							callback: {
								loaded: function () {
									$(".btn_com_prev1").click(function () {
										root.find(".slidesjs-previous").trigger("click");
										return false;
									});


									$(".btn_com_next1").click(function () {
										root.find(".slidesjs-next").trigger("click");
										return false;
									});

								},
								start: function (number) {

								},
								complete: function (number) {
									$(".logo_indi button").removeClass("on");
									$($(".logo_indi button")[number - 1]).addClass("on")
								}
							}

						});

						$(".slidesjs-container").css("height", "480px");
						$(".slidesjs-control").css("height", "480px");

						$.each($("#slides").find(".logo_list"), function (i) {
							var item = $('<button' + (i == 0 ? ' class="on"' : '') + ' type="button">O</button>');
							item.click(function () {
								root.find('a[data-slidesjs-item=' + i + ']').trigger("click");		// 이동
							})
							$(".logo_indi").append(item);
						})
					} else {
						$(".btn_com_prev1").hide();
						$(".btn_com_next1").hide();
					}

				});
			}
		};
		return directive;
	});


	// 함께하는 기업 소식
	app.controller("newsCtrl", function ($scope, $http, paramService) {

		$scope.list = [];

		$scope.total = -1;

		$scope.params = {
			t: "list",
			b_type: "sponsor_news",
			page: 1,
			rowsPerPage: 10
		};
		


		// 파라미터 초기화
		$scope.params = $.extend($scope.params, paramService.getParameterValues());
		if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);

		// 검색
		$scope.search = function (params) {
		    
			$scope.params.k_word = $("#k_word").val();
			$scope.params.page = 1;
			$scope.params = $.extend($scope.params, params);
			$scope.getList();
		}

		$scope.params = $.extend($scope.params, paramService.getParameterValues());
		if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);
		    $scope.getList = function (params) {
		      /*  if ($("#test").val() != "") {
		            $scope.params.k_word = $("#test").val();
		            
		        }*/
			$scope.params = $.extend($scope.params, params);
			$http.get("/api/board.ashx", { params: $scope.params }).success(function (result) {
				$scope.list = result.data;
				$scope.total = result.data.length > 0 ? result.data[0].total : 0;
			    //console.log(typeof(result.data[0].b_regdate))
			
			});
			if (params) {
			    scrollTo($("#l"), 10);
			}
			console.log("getList", $scope.params);
	
		}

		$scope.parseDate = function (datetime) {
		    return new Date(datetime);
		}

		// 상세페이지
		$scope.goView = function (idx) {
			$http.post("/api/board.ashx?t=hits&id=" + idx).then().finally(function () {
				location.href = "/participation/sponsor/view/" + idx + "?" + $.param($scope.params);
			});
		}

		$scope.getList();
	});


})();