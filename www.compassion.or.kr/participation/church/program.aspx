﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="program.aspx.cs" Inherits="participation_church_program" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">


    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>교회학교 컴패션나눔별</em></h1>
                <span class="desc">교회학교 친구들과 힘을 모아 한 어린이에게 꿈을 선물해 주세요</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 part">

            <div class="bgContent bg1">
                <p class="tit">나눔별프로그램이란</p>
                <span class="bar2"></span>
                <p class="con">
                    교회학교 부서 (초등부, 중고등부, 청년부)에서 한 어린이를 결연하여 함께 후원하는 프로그램입니다.<br />
                    교회학교에서 나눔별프로그램을 요청하시면, 컴패션과 교회학교가 연합하여 나눔별예배를 드립니다.
                </p>
            </div>

            <div class="w980 church">

                <div class="program tac">
                    <p class="tit mb20">프로그램 진행 과정</p>
                    <p class="s_con3">
                        <em>컴패션 사역자가 직접 교회로 찾아갑니다</em><br />
                        예수님이 말씀하신 컴패션(함께 아파하는 마음)의 성경적인 의미를 확인하고 우리의 삶 속에서 나눔을 실천하는 첫 걸음이 됩니다.
                    </p>

                    <ul class="clear2">
                        <li>
                            <span class="con_tit">나눔에 대한 설교</span><br />
                            <span class="content">컴패션(함께 아파하는 마음)을 실천하라는<br />
                                하나님의 말씀을 전합니다.
                            </span>
                        </li>
                        <li>
                            <span class="con_tit">후원어린이 소개</span><br />
                            <span class="content">교회학교 부서에서 후원할 어린이를<br />
                                자세하게 소개합니다.
                            </span>
                        </li>
                        <li>
                            <span class="con_tit">기도액자 만들기 (2부순서)</span><br />
                            <span class="content">후원어린이를 기억하며 기도하기 위한<br />
                                기도액자를 만듭니다.
                            </span>
                        </li>
                    </ul>
                </div>

            </div>

            <div class="program_bg">
                <div class="w980 tac">
                    <p class="s_tit6 mb20">지구 반대편 한 어린이와의 만남으로 사랑을 나누고 <em class="fc_blue">함께 성장하는 나눔별프로그램</em>을 지금 시작하세요!</p>
                    <div><span class="bottom_tel"><span class="txt">교회학교 나눔별프로그램 문의 : </span>02) 740-1000</span></div>
                </div>
            </div>

        </div>
        <!--// e: sub contents -->



    </section>


</asp:Content>
