﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="experience.aspx.cs" Inherits="participation_church_experience" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">


    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>교회로 찾아가는 컴패션체험전</em></h1>
                <span class="desc">컴패션의 특별한 이동식 체험 전시 프로그램을 소개합니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 part">

            <div class="church_exp_movie">
                <div class="w980">
                    <iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/QvTsPKs3n7E?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>

            <div class="church_exp">

                <div class="w980">

                    <p class="s_tit8 mb60">교회로 찾아가는 체험전은</p>

                    <ul class="conList clear2">
                        <li>
                            <span class="con_tit">교회로 찾아가는 <em>이동식 체험 전시</em>입니다.</span><br />
                            <span class="content">컴패션체험전은 현지에 가지 않아도 많은 분들이 지구 반대편에<br />
                                사는 어린이의 삶을 느낄 수 있도록 교회로 찾아갑니다.<br />
                                현지와 같이 재현된 어린이의 집과 환경을 보고, 듣고, 만지며<br />
                                가난의 진정한 의미를 깨닫고 꿈을 읽고 살아가던 어린이가<br />
                                주님의 사랑으로 변화되는 과정을 만나볼 수 있습니다.
                            </span>
                        </li>
                        <li>
                            <span class="con_tit"><em>교회와 협력</em>하여 진행됩니다.</span><br />
                            <span class="content">교회 내 설치 공간, 운영 기간, 인원, 상황 등을 교회와 사전에<br />
                                협의한 후 진행합니다. 교회 로비나 예배당 입구 등 성도님들과<br />
                                지역 주민 분들이 쉽게 오셔서 체험할 수 있는 공간에 체험전이<br />
                                설치되며 교회를 섬기고 교회 사역에 생명력을 불어 넣을 수<br />
                                있도록 함께 협력합니다.
                            </span>
                        </li>
                        <li>
                            <span class="con_tit"><em>지역 주민을 전도</em>할 수 있는 기회입니다.</span><br />
                            <span class="content">컴패션체험전을 통해 평소 교회에 방문하기 어려웠던<br />
                                지역 주민들이 교회에 방문하고 나아가 교회가 지역 주민을 전도하는<br />
                                일에 컴패션체험전이 사용되기를 원합니다.
                            </span>
                        </li>
                        <li>
                            <span class="con_tit">하나님이 기뻐하시는 <em>나눔의 삶으로 안내</em>합니다.</span><br />
                            <span class="content">누구나 먼 곳까지 직접 가난한 어린이를 도우러 가거나 복음을<br />
                                전할 기회를 가질 수 있는 것은 아닙니다. 컴패션체험전을 통해 한 영혼을<br />
                                향한 하나님의 사랑을 느끼고 나아가 가난으로 인해 꿈을 잃고<br />
                                살아가는 한 어린이를 품고 나누는 삶을 실천할 수 있습니다.
                            </span>
                        </li>
                    </ul>

                </div>

            </div>


        </div>
        <!--// e: sub contents -->



    </section>


</asp:Content>
