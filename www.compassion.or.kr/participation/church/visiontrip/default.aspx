﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="participation_nk_collaboration_visiontrip_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

	<script type="text/javascript" src="/participation/church/visiontrip/default.js"></script>
    <script type="text/javascript">
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>목회자 비전트립</em></h1>
				<span class="desc">목회자분들을 위한 특별한 비전트립을 소개합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 part">

			<div class="bgContent bg1">
				<p class="tit">목회자분들을 위한 여행</p>
				<span class="bar2"></span>
				<p class="con">
					목회자 비전트립은 ‘가난’이라는 이름으로 어려움을 겪고 있는 전 세계 어린이들을 돕기 위한<br />
					현지 컴패션 어린이센터의 사역 현장을 방문해 컴패션의 비전을 눈으로 확인하고 함께 나누기 위한 목회자분들을 위한 여행입니다.
				</p>
			</div>

			<div class="pastor_bg">

				<div class="w980 church">

					<p class="s_tit6 mb30">목회자 비전트립 프로그램 예시 - <span class="fc_blue">필리핀 4박 6일</span></p>

					<div class="tableWrap1 mb30">
						<table class="tbl_type9 line">
							<caption>목회자 비전트립 프로그램 예시 테이블</caption>
							<colgroup>
								<col style="width:10%" />
								<col style="width:15%" />
								<col style="width:17%" />
								<col style="width:17%" />
								<col style="width:17%" />
								<col style="width:12%" />
								<col style="width:12%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">일정</th>
									<th scope="col">Day1</th>
									<th scope="col">Day2</th>
									<th scope="col">Day3</th>
									<th scope="col">Day4</th>
									<th scope="col">Day5</th>
									<th scope="col">Day6</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row">오전</th>
									<td>
										-인천출발<br />
										-필리핀도착
									</td>
									<td>컴패션어린이센터<br />방문</td>
									<td>컴패션어린이센터<br />방문</td>
									<td>컴패션어린이센터<br />방문</td>
									<td rowspan="3">Fun Day<em class="cf">3)</em></td>
									<td rowspan="4" class="nobottom">인천도착</td>
								</tr>
								<tr>
									<th scope="row">점심</th>
									<td>점심식사</td>
									<td>점심식사</td>
									<td>점심식사</td>
									<td class="ie_border">점심식사</td>
								</tr>
								<tr>
									<th scope="row">오후</th>
									<td>국가사무실 방문</td>
									<td>가정방문</td>
									<td>가정방문</td>
									<td class="ie_border">가정방문</td>
								</tr>
								<tr>
									<th scope="row">저녁</th>
									<td>
										저녁식사<br />
										디브리핑<em class="cf">1)</em>
									</td>
									<td>
										저녁식사<br />
										디브리핑
									</td>
									<td>
										저녁식사<br />
										디브리핑
									</td>
									<td>
										LDP디너<em class="cf">2)</em><br />
										디브리핑
									</td>
									<td class="ie_border">필리핀 출발</td>
								</tr>
							</tbody>
						</table>

					</div>

					<div class="box_type3 pastor_cf">
						<span>1) 디브리핑 - 나눔과 교제의 시간</span>
						<span>2) LDP디너 - 1:1리더십결연 학생 및 졸업생들과의 저녁식사</span>
						<span>3) Fun Day - 후원어린이와의 만남의 시간</span>
					</div>

					<!-- 비전트립 일정 테이블 -->
					<div class="pastor_schedule">
						<p class="tit">목회자 비전트립 일정</p>

						<div class="tableWrap3 mb20">
							<table class="tbl_type4">
								<caption>편지 수정 테이블</caption>
								<colgroup>
									<col style="width:20%" />
									<col style="width:20%" />
									<col style="width:20%" />
									<col style="width:20%" />
									<col style="width:20%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col">트립 일정</th>
										<th scope="col">방문 국가</th>
										<th scope="col">예상 트립비용<br /><span class="color1">신청비 20만원 포함</span></th>
										<th scope="col">상태</th>
										<th scope="col">신청 마감일</th>
									</tr>
                                </thead>
                                <tbody>

                                    <tr class="end" ng-repeat="item in list">
                                        <td>{{item.start_date | date:'yyyy/MM/dd'}} - {{item.end_date| date:'MM/dd'}}</td>
                                        <td>{{item.visit_nation}}</td>
                                        <td>{{item.trip_cost}}</td>
                                        <td><span class="{{item.status_type}}" ng-bind="item.result" id="status_type"></span></td>
                                        <td>{{item.close_date.substr(0, 10) | date:'yyyy/MM/dd'}}</td>
                                    </tr>
                                    <tr ng-hide="list.length">
                                        <td>데이터가 없습니다.</td>
                                    </tr>

                                </tbody>
                            </table>
						</div>

						<div><span class="pastor_tel"><span class="txt">목회자 비전트립 문의 : </span>02) 740-1000</span></div>
					</div>
					<!-- 비전트립 일정 테이블 -->

				</div>

			</div>


		</div>	
		<!--// e: sub contents -->

		

    </section>
    <!--// sub body -->

</asp:Content>