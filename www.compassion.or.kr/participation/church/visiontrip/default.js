﻿

(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http) {

    	$scope.requesting = false;
        $scope.rowsPerPage = 10;

        $scope.list = [];
        $scope.params = {
        	page: 1,
        	rowsPerPage: $scope.rowsPerPage,
        };

        // list
        $scope.getList = function (params) {

            $http.get("/api/visiontrip.ashx?t=list", { params: { type: "pastor" } }).success(function (result) {
                $scope.list = result.data;
                var now = moment();
                $.each($scope.list, function (i) {
                    if (moment(this.open_date) > now) {
                        this.result = "예정";
                        this.status_type = "status expected";
                    } else if (moment(this.close_date) < now) {
                        this.result = "마감";
                        this.status_type = "status end";
                    } else {
                        this.result = "신청중"
                        this.status_type = "status ing";
                    }
                });
            })
        }

        $scope.getList();


        /*
        $scope.getStatus = function (open_date, close_date) {
        	var result = "신청중";
        	var open = new Date(open_date);
        	var now = new Date();
        	var close = new Date(close_date);
      
        	if (open > now) {
        	    result = "예정";
        	    
        	} else if (close < now) {
        	    result = "마감";
        	}
        	return result;
        }
    

	});

	app.filter('status', function () {
		return function (open, close) {
			var result = "신청중";

			var openDate = new Date(open);
			var closeDate = new Date(close);
			var now = new Date();
			if (openDate < now) {
				result = "예정";
			}else if(endDate < now){
				result = "마감";
			}

			return result;
		};
	});


	// 신청마감일(올해가 아닌경우 년도 표시)
	app.filter('lastYear', function ($filter) {
		return function (close) {
			var result = $filter('date')(close, "M/d", "ko")
			if (new Date(close).getFullYear() < new Date().getFullYear()) {
				result = $filter('date')(close, "M/d(yyyy)", "ko")
			}
			return result;
		};
    */
	});
})();

