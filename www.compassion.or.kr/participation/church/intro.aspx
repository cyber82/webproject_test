﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="intro.aspx.cs" Inherits="participation_church_intro" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>교회협력 소개</em></h1>
                <span class="desc">교회와 컴패션이 함께할 수 있는 어린이 사랑 방법을 소개합니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 part">

            <div class="visual_church">
                <span>CHURCH + COMPASSION</span>
            </div>

            <div class="w980 church">

                <div class="intro">
                    <p class="tit"><em class="fc_blue">교회</em>와 <em class="fc_blue">컴패션</em>이 만나 하나님 나라를 확장해 갑니다</p>
                    <p class="con">
                        교회는 하나님의 소망이며 지속적으로 확장되는 하나님 나라입니다.<br />
                        컴패션은 교회와 함께, 교회를 통하여 주님의 지상명령 성취 사역에 열정적으로 동참합니다.
                    </p>

                    <div class="bg"></div>

                    <p class="tit">교회와 교회가 <em class="fc_blue">연합하는 통로</em>가 됩니다</p>
                    <p class="con">
                        컴패션은 하나님의 사랑을 전하는 교회의 사명을 완수하기 위해 전략적으로 어린이들을 돌보는 전 세계 현지 교회와 협력합니다.<br />
                        현재 25개국의 약 6,900개 이상의 교회와 협력하고 있고, 이 곳에서 양육된 어린이들은 희망을 찾고, 삶의 현장에서 기쁨으로<br />
                        예수님을 전하는 작은 선교사로 살아가게 될 것입니다.
                    </p>

                    <p class="tit">어린이가 자라 가정이 변하고, <em class="fc_blue">지역사회가 변화</em>됩니다</p>
                    <p class="con">
                        지역사회 개발이 아닌, 부모의 마음과 전문가의 손길로 한 어린이를 직접적으로 돌봅니다.<br />
                        이를 통해 어린이의 가정이 변화되고, 지역이 변화됩니다.
                    </p>
                </div>

            </div>

            <div class="church_program_detail">
                <p class="tit">교회협력 프로그램 자세히 보기</p>
                <div class="btn">
                    <a href="/participation/church/sunday/">컴패션선데이</a>
                    <a href="/participation/church/sunday/#l">컴패션선데이<br />
                        플러스</a>
                    <a href="/participation/church/program/">교회학교<br />
                        나눔별프로그램</a><br />
                    <a href="/participation/church/experience/">교회로 찾아가는<br />
                        체험전</a>
                    <a href="/participation/church/visiontrip/">목회자<br />
                        비전트립</a>
                </div>
            </div>


        </div>
        <!--// e: sub contents -->



    </section>


</asp:Content>
