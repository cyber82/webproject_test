﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sunday.aspx.cs" Inherits="participation_church_sunday" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">


    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>컴패션선데이</em></h1>
                <span class="desc">교회와 함께 드리는 주일 예배, 컴패션선데이를 소개합니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 part">

            <div class="visual_sunday">
                <div class="w980">
                    <iframe width="980" height="550" title="영상" src="https://www.youtube.com/embed/Pbbu6Cx-q5s?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
                    <p class="con">
                        <em>어느 특별한 주일로의 초대!</em><br />
                        컴패션선데이(CS)란 주일 예배를 교회와 컴패션이 협력하여 드리는 것을 말합니다
                    </p>
                </div>
            </div>

            <div class="church" >

                <div class="sunday tac w980" >

                    <ul class="clear2"  >
                        <li>
                            <span class="con_tit"><em>선교의 기회</em>를<br />
                                제공</span><br />
                            <span class="content">누구나 먼 곳까지 직접 가난한 어린이를<br />
                                도우러 가거나, 복음을 전할 기회를 가질 수 있는<br />
                                것은 아닙니다. 컴패션선데이를 통한 어린이와의<br />
                                1:1 결연은 선교의 통로가 될 수 있습니다.
                            </span>
                        </li>
                        <li>
                            <span class="con_tit">하나님 나라가 확장되는<br />
                                <em>축제의 자리</em></span><br />
                            <span class="content">어린아이를 한 명 영접하는 것은 단순히<br />
                                가난한 어린이를 돕는 의미를 넘어섭니다.<br />
                                어린 영혼을 주님께 향하게 함으로써 하나님<br />
                                나라가 확장됩니다. 교회 안에서 하나님께서<br />
                                가장 기뻐하실, 사랑이 맺어지는 자리,<br />
                                축제의 시간을 가지게 됩니다.
                            </span>
                        </li>
                        <li >
                            <span class="con_tit">교회 사역에<br />
                                <em>활기와 생명력</em>을</span><br />
                            <span class="content">성도님들은 섬김과 나눔을 통해 하나님의 말씀이<br />
                                성취되는 것을 직접 체험하게 됩니다. 정기적인<br />
                                후원과 더불어 어린이를 영적으로 보살피는<br />
                                동안 하나님 사역에 동참하는 은혜를 누리면서<br />
                                교회 안팎에 선한 영향력을 끼칩니다.<br />
                            </span>
                        </li>
                    </ul>
                <div id="l" style="height:50px">&nbsp</div>
                </div>
                
                <div class="sunday_bg">
                    <div class="w980 tac">
                        <p class="tit">컴패션선데이 이후, <em>컴패션선데이플러스</em>가 진행됩니다</p>
                        <span class="bar"></span>
                        <p class="con">
                            컴패션선데이플러스(CS+)란 컴패션선데이를 진행한 후에 교회의 후원자 분들을 대상으로 교회 내에서 진행되는 후원자 후속 모임입니다.<br />
                            후원자 분들이 가장 궁금해 하시는 컴패션의 주요 사역, 후원금 사용 방법 그리고 편지쓰기 방법 및 프로세스 등에 대해 자세하게 안내해 드립니다.<br />
                            그리고 이후에는 자체적으로 모임을 진행할 수 있도록 도와 드립니다.
                        </p>
                        <div><span class="bottom_tel"><span class="txt">컴패션선데이 문의 : </span>02) 740-1000</span></div>
                    </div>
                </div>

            </div>

        </div>
        <!--// e: sub contents -->



    </section>


</asp:Content>
