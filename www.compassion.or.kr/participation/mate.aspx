﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="mate.aspx.cs" Inherits="participation_mate" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">




    </script>
</asp:Content>



<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    

    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>컴패션메이트</em></h1>
                <span class="desc">헌신된 자원봉사자, 메이트를 소개합니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 part">


            <div class="bgContent mate_movie">
                <div class="w980">
                    <p class="s_con3">
                        컴패션 메이트는 컴패션의 비전에 공감하여 자발적으로 자신의 재능과 시간,<br />
                        열정을 전 세계의 가난한 어린 아이들을 위해 나누는 헌신된 자원봉사자들을 말합니다.
                    </p>
                    <iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/PICer6_HJIs?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>


            <div class="w980 mateIntro">

                <div class="mate_value">
                    <h2>Compassion Mate가 추구하는 가치</h2>
                    <ul class="clear2">
                        <li>
                            <p class="s_tit6">Child-centered</p>
                            <p class="s_con3">
                                컴패션 메이트의 모든 활동의 중심에는 어린이가<br />
                                있습니다. 전 세계의 가난한 어린이들이 내일의<br />
                                소망을 품고 살아갈 수 있도록 그들에게 사랑과<br />
                                희망을 나누어 줍니다.
                            </p>
                        </li>
                        <li>
                            <p class="s_tit6">Family</p>
                            <p class="s_con3">
                                컴패션은 하나님 안에서 새로운 가족입니다.<br />
                                컴패션 메이트는 컴패션의 가족으로서 서로<br />
                                교제하며 지속적인 관계를 가지고 사랑을 나누는<br />
                                법을 배워갑니다.
                            </p>
                        </li>
                        <li>
                            <p class="s_tit6">Excellence</p>
                            <p class="s_con3">
                                컴패션은 정직성, 투명성, 효율성을 바탕으로<br />
                                일합니다. 패션을 이끌어가는 또 하나의 축으로<br />
                                컴패션 메이트는 어느 분야에서나 최상의 결실을<br />
                                낼 수 있도록 노력합니다.
                            </p>
                        </li>
                    </ul>
                </div>

                <div class="mate_sector">
                    <h2>Compassion Mate에서 활동할 수 있는 분야</h2>
                    <ul class="clear2">
                        <li>
                            <p class="s_tit6">번역 Mate</p>
                            <p class="s_con3">
                                매 주 약 11,000통 이상의 편지가 한국컴패션<br />
                                사무실로 도착하고 있으며, 이 중 번역이 필요한<br />
                                편지는 4,500통 정도입니다. (어린이편지<br />
                                약 3,600통, 후원자편지 약 900통. 2015년 기준)<br />
                                한국컴패션은 보다 신속하고 정확한 편지 발송을<br />
                                위하여 번역 메이트를 선발하고 있습니다.
                            </p>
                        </li>
                        <li>
                            <p class="s_tit6">사무 Mate</p>
                            <p class="s_con3">
                                빠르게 늘어나고 있는 후원자의 규모에 맞게<br />
                                신속하고 원활한 서비스를 제공하기 위해<br />
                                한국컴패션 전 부서에 걸쳐 다양한 사무 업무가<br />
                                이루어지고 있습니다. 사무 메이트는 각 부서의<br />
                                사무 활동을 지원합니다.
                            </p>
                        </li>
                        <li>
                            <p class="s_tit6">행사 Mate</p>
                            <p class="s_con3">
                                전세계 어린이들을 대신하여 세상에  목소리를<br />
                                높이기 위해 한국컴패션은 연중 다양한 결연<br />
                                행사들을 진행합니다. 이 때 메이트는 행사<br />
                                성격에 따라 다양한 역할을 감당하시는 귀중한<br />
                                손길이 됩니다.
                            </p>
                        </li>
                    </ul>
                </div>

                <div class="btn_ac mt40">
                    <a href="http://mate.compassion.or.kr" target="_blank" class="btn_type1">MATE사이트 바로가기</a>
                </div>
            </div>


        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>

</asp:Content>
