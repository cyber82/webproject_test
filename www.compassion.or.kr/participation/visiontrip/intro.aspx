﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="intro.aspx.cs" Inherits="participation_visiontrip_intro" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>컴패션비전트립 소개</em></h1>
                <span class="desc">비전트립은 컴패션의 양육 현장을 방문해 컴패션의 비전을 눈으로 확인하고 함께 나누는 여행입니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 part">

            <div class="trip_movie">
                <div class="w980">

                    <p class="txt1">컴패션을 통해 양육받고 있는 어린이를 직접 만나고 사랑을 나눌 수 있는<br />컴패션비전트립을 소개합니다.</p>

                    <iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/PR3qzJIYweQ?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>

                    <p class="txt2">
                       비전트립을 통해 후원자님들은 어린이가 살고 있는 나라를 방문하셔서 어린이가 매일 먹는 음식, 어린이의 가정 환경,<br />
						컴패션 양육현장인 컴패션어린이센터 수업 광경을 직접 경험할 뿐 아니라, 사랑스러운 어린이를 직접 안아주실 수 있고 손을 잡아주실 수도<br />
						있습니다. 후원자 여러분의 방문은 어린이들의 삶에 있어 그 무엇보다 귀중한 경험이 될 것이며,<br />
						후원자 여러분에게도 잊지 못할 시간이 될 것입니다.
                    </p>
                </div>
            </div>

            <div class="trip_intro">

                <div class="w980">

                    <div class="sub_tit mb40">
                        <h2 class="tit">EXPERIENCE COMPASSION</h2>
                        <p class="s_con3">비전트립을 통해서 후원자님들은 어린이들이 매일의 삶에서 겪고 있는 ‘가난’이 구체적으로 어떤 모습인지 경험하게 될 것입니다.</p>
                    </div>

                    <div class="content">
                        또한 후원어린이들의 가정을 방문하여 부모님과 가족을 만나고, 어린이들에게 컴패션 양육을 제공하고 있는 교회의 리더십, 그리고 가장 가까이에서<br />
                        어린이들을 양육하고 있는 선생님들 및 컴패션 직원들과 직접 대화하면서 가난에 대해 더 깊이 이해하고 어린이들을 돕는다는 것의 의미를 더 깊이<br />
                        체험하게 될 것입니다.  무엇보다 컴패션의 양육을 통해 변화된 어린이들과 가족, 지역사회에서의 선한 열매를 직접 보면서 열악한 환경을 뛰어넘어<br />
                        일하시는 하나님의 역사를 경험하실 귀한 기회가 될 것입니다.
                    </div>

                    <div class="contact_qna div3">
                        <p class="tit">문의</p>
                        <div class="clear2">
                            <div class="tel"><span class="icon"><span class="txt">전화 : </span>02)3668-3420, 3453</span></div>
                            <div class="fax"><span class="icon"><span class="txt">팩스 : </span>02)3668-3501</span></div>
                            <div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:visiontrip@compassion.or.kr">visiontrip@compassion.or.kr</a></span></div>
                        </div>
                    </div>

                    <div>
                        <span class="fs15 vam">자주 묻는 질문을 이용하시면 더 많은 궁금증을 해결하실 수 있습니다.</span>
                        <a href="/customer/faq/?s_type=J" class="btn_s_type2 ml10">FAQ 바로가기</a>
                    </div>

                </div>

            </div>
            <div class="h100"></div>

        </div>
        <!--// e: sub contents -->



    </section>

</asp:Content>
