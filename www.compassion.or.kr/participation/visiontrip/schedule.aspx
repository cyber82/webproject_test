﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="schedule.aspx.cs" Inherits="participation_visiontrip_schedule" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        var selectedYear = <%= year%>;
    </script>
    <script type="text/javascript" src="/participation/visiontrip/schedule.js?v=1.8"></script>
    <script type="text/javascript">
    </script>
    <style type="text/css">
        .a-disabled,
        .a-disabled[disabled] {
          cursor: default !important;
          pointer-events: none;
        }
        /*신청완료 : #26C281 */
        .tbl_type4 tr td .status.apply {background:#66afe0;}  
        .divLoginCheck {
            margin: 0 auto; text-align: center;
        }
    </style>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <asp:HiddenField runat="server" ID="hdnBirthCheck"/>
        <asp:HiddenField runat="server" ID="hdnApplyType"/>
        <asp:HiddenField runat="server" ID="hdnRequestKey"/>
        <asp:HiddenField runat="server" ID="hdnSponsorID"/>
        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>컴패션비전트립 일정/신청</em></h1>
                <span class="desc">컴패션어린이들과 함께할 설레는 시간을 계획해 보세요</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents part">

            <div class="w980 trip_sch">
                
                <div class="sel_year">
                    <a href="#" class="btn_prev" id="last_year" ng-click="lastYear()">{{params.year - 1}}년</a>
                    <span>
                        <em>{{params.year}}</em> 년 
                    </span>
                    <a href="#" class="btn_next" id="next_year" ng-click="nextYear()">{{params.year + 1}}년</a>
                </div>

                <!-- 비전트립 일정 테이블 -->
                <div class="church">  
                    <div class="pastor_schedule">
                        <p class="tar fs14 mb15">* 일정은 항공 및 현지 상황에 따라 변경될 수 있습니다.<br />*최소 15명 이상이 되어야 트립이 진행됩니다.</p> 
                        <div class="tableWrap3 mb20">
                            <style>
                                .status.new { border-radius: 5px !important; width: 80px !important; height: 36px !important; padding-top: 8px !important; color: white !important; }
                                .status.new.orange { background-color: rgb(244, 180, 56) !important; }
                                .status.new.gray { background-color: rgb(118, 118, 118) !important; }
                                .status.new.lightorange { background-color: rgba(244, 180, 56, .5) !important; }
                                .status.new.lightgray { background-color: rgba(118, 118, 118, .5) !important; }
                            </style>
                            <table class="tbl_type4">
                                <caption>비전트립일정 테이블</caption>
                                <colgroup>
                                    <col style="width: 13%" />
                                    <col style="width: 11%" />
                                    <col style="width: 14%" />
                                    <col style="width: 8%"  ng-if="requestview == true"/>
                                    <%--<col style="width: 7%" />--%>
                                    <col style="width: 8%" /> 
                                    <col style="width: 15%" />
                                    <col style="width: 11%" />
                                    <col style="width: 10%" /> 
                                    <col style="width: 10%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">트립 일정</th>
                                        <th scope="col">방문 국가</th>
                                        <th scope="col">예상 트립비용<br />
                                            <span class="color1" ng-if="requestview == false">신청비 20만원 포함</span></th>
                                        <th scope="col" ng-if="requestview == true">트립명</th>
                                        <%--<th scope="col">신청 현황</th>--%>
                                        <th scope="col">신청 <br/>마감일</th> 
                                        <th scope="col">신청</th>
                                        <th scope="col">신청자수<br /><span class="color1">(신청수/최대인원)</span></th>
                                        <th scope="col">트립안내</th> 
                                        <th scope="col">트립후기</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in list" ng-class="item.visiontriptype == 'Request' ? 'end' : ''">
                                        <td><span class="font-small">{{item.startdate | date:'M/d'}}~{{item.enddate| date:'M/d'}}</span></td>
                                        <td><span class="font-small">{{item.visitcountry}}</span></td>
                                        <td><span class="font-small">{{item.tripcost == '' ? '-' : item.tripcost}}</span></td>
                                        <td ng-if="requestview == true">{{item.tripname}}</td>
                                        <%--<td><span class="status {{item.status}}" ng-bind-html="item.result"></span></td>--%>
                                        <td><span class="font-small">{{item.closedate | lastYear}}</span></td> 
                                        <td>
                                            <%--<a href="javascript:void(0);" ng-click="applyCheck(item)"  class="status new lightorange {{item.applystatus}} {{item.applydisabled}}">
                                                {{item.status == 'end' ? item.result : item.applyresult}}
                                            </a>--%>
                                            <a href="javascript:void(0);" ng-click="applyCheck(item)"  class="status new {{item.statusnew}}">
                                                {{item.statustextnew}}
                                            </a>
                                        </td>
                                        <td>{{item.applycount_view}}</td>
                                        <td><a ng-click="tripnoticeModal.show($event ,item.tripnotice, item)" ng-href="{{item.tripreview}}" class="font-small btn_type8 {{item.tripnoticestatus}}" ng-show="item.tripnotice_view ">보기</a></td>
                                        <td><a ng-click="setYear(this)" ng-href="{{item.tripreview}}" class="btn_type8 font-small" ng-show="item.tripreview" title="트립안내 상세보기 팝업">보기</a></td>
                                    </tr>
                                    <tr ng-hide="list.length">
                                        <td colspan="9">데이터가 없습니다.</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div> 
                        <div ng-class="isLogin == false ? '' : 'divLoginCheck'"  class="pt20" style="text-align:center;"> 
                            <%--<span class="fs15 vam" ng-if="isLogin == false">로그인/회원가입 후 비전트립 신청이 가능합니다.</span>  
                            <a href="javascript:void(0);" ng-if="isLogin == false" ng-click="loginCheck()" class="btn_s_type1 ml10">로그인/회원가입 하기</a>--%>
                            <a ng-click="myVisionTripClick()" class="btn_s_type1 ml10" style="margin-left:0;">나의 비전트립 보러가기</a>
                        </div>
                    </div>
                </div>
                <!-- 비전트립 일정 테이블 -->

            </div>
            <div class="h100"></div>

        </div>
        <!--// e: sub contents --> 
    </section>



</asp:Content>
