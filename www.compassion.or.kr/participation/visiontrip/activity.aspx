﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="activity.aspx.cs" Inherits="participation_visiontrip_activity" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

                
<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션비전트립 활동</em></h1>
				<span class="desc">비전트립을 통해 체험할 수 있는 다양한 활동을 소개합니다</span>

                <uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents part">

			<div class="trip_activity">

				<div class="w980">

					<!-- 탭메뉴 -->
					<ul class="tab_type1 activity font15 mb60">
						<li style="width:17%" class="on"><a href="#">국가사무실 방문</a></li>
						<li style="width:17%"><a href="#">컴패션 어린이센터 방문</a></li>
						<li style="width:17%"><a href="#">가정 방문</a></li>
						<li style="width:17%"><a href="#">컴패션 졸업생과의 만남</a></li>
                        <li style="width:16%"><a href="#">나눔과 교제</a></li>
                        <li style="width:16%"><a href="#">후원 어린이 만남</a></li>
					</ul>
					<!--//-->

					<script type="text/javascript">
						$(function () {
							$(".tab_type1.activity > li").click(function () {
								var idx = $(this).index() + 1;
								$(".tab_type1.activity > li").removeClass("on");
								$(this).addClass("on");
								$(".tc").hide();
								$(".tc" + idx).show();

								if (idx == 6) {
									$(".cq_ta").show();
								}
								else {
									$(".cq_ta").hide();
								}

								return false;
							});
						})
					</script>

					<!-- 탭메뉴 컨텐츠 -->
                    <style>
                        .tab_contents .p1 { color: #333; font-size:22px; margin-bottom:20px; }
                        .tab_contents .p1 .blue { color: #005dab; font-size:22px; }
                        .tab_contents .p2 { color: #767676; font-size:15px; margin-bottom:20px; }
                        .tab_contents .p3 { color: #005dab; font-size:22px; margin-bottom:10px; margin-top:40px; }
                        .subContents.etc { padding-top:0; }
                        .subContents.etc .faq .faqSet .q_wrap .q_icon { width:0; }
                    </style>
					<div class="tab_contents">
						<div class="tc tc1 clear2">
							<div class="con3">
                                <div>
                                    <p class="p1">컴패션 수혜국 <span class="blue">국가사무실</span>을 방문합니다.</p>
                                    <p class="p2">컴패션 양육프로그램을 운영하고 있는 전 세계 25개 수혜국 중 방문 국가의 국가사무실에서 현지 직원들을 만나고, 후원자님들이 보낸 편지가 어린이에게 전달되는 과정 및 후원금이 사용되는 과정 등을 살펴보며, 컴패션 운영의 투명성을 확인하는 시간입니다.</p>
                                    <p class="p3">FAQ</p>
                                    <div class="subContents etc">
                                        <div class="faq">
					                        <div class="faqSet" onClick="toggle(11)">
						                        <button class="q_wrap question11">
							                        <span class="q_icon">&nbsp;</span>
							                        <span class="q_tit">국가사무실을 방문하는 이유는 무엇인가요?</span>
						                        </button>
						                        <div class="a_wrap answer11">국가사무실 방문을 통해 현지 컴패션 사무실에서 진행되는 업무, 절차를 파악하고 컴패션 운영의 투명성을 직접 확인할 수 있기 때문입니다.</div>
					                        </div>
                                            <div class="faqSet" onClick="toggle(12)">
						                        <button class="q_wrap question12">
							                        <span class="q_icon">&nbsp;</span>
							                        <span class="q_tit">국가사무실은 비전트립마다 반드시 방문하게 되나요?</span>
						                        </button>
						                        <div class="a_wrap answer12">가능하면 국가사무실 방문을 포함하도록 일정을 조율하지만 현지 상황이나 일정 진행 상 어려움이 있는 경우 별도의 공간에서 간단한 프레젠테이션으로 대체하기도 합니다.</div>
					                        </div>
					                        <div class="faqSet" onClick="toggle(13)">
						                        <button class="q_wrap question13">
							                        <span class="q_icon">&nbsp;</span>
							                        <span class="q_tit">국가사무실에서는 얼마 정도의 시간을 보내나요?</span>
						                        </button>
						                        <div class="a_wrap answer13">프레젠테이션, 편지 번역 및 전달 과정, 현지 직원들의 업무 환경 견학 등을 포함하여 짧게는 1시간 30분에서 길게는 3시간 정도 소요됩니다.</div>
					                        </div>
                                            <div class="faqSet" onClick="toggle(14)">
						                        <button class="q_wrap question14">
							                        <span class="q_icon">&nbsp;</span>
							                        <span class="q_tit">국가사무실에 가면 현지 컴패션 직원들을 만날 수 있나요?</span>
						                        </button>
						                        <div class="a_wrap answer14">만날 수 있습니다. 하지만 한국을 포함한 12개 후원국 비전트립팀들이 계속해서 국가사무실을 방문하기 때문에 별도의 교제 시간을 갖기는 어렵습니다. 대신 현지 컴패션 직원들의 평소 일하는 모습, 근무 환경 등을 보실 수 있습니다.</div>
					                        </div>
				                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="tc tc2 clear2" style="display:none">
							<div class="con3">
                                <p class="p1"><span class="blue">컴패션 어린이센터</span>를 방문합니다.</p>
                                <p class="p2">컴패션이 현지 어린이들에게 그리스도의 사랑을 어떻게 전하고 있는지 살펴보며 수업, 성경공부, 예배 등 어린이센터 활동에 직접 참여하는 시간을 가집니다. 자신들을 만나기 위해 멀고 먼 후원국에서 달려와 준 후원자들을 바라보는 어린이들의 반짝이는 눈빛과 함박웃음, 그리고 정성스럽게 준비한 환영 인사에 참가자 여러분이 오히려 감동하는 시간이 될 것입니다. 더불어 사진에서만 보았던 후원 어린이를 직접 안아주고, 손을 잡아주고, 후원자님의 이야기를 들려주기도 하며 함께 시간을 보낼 수 있습니다.</p>
                                <p class="p3">FAQ</p>
                                <div class="subContents etc">
                                    <div class="faq">
					                    <div class="faqSet" onClick="toggle(21)">
						                    <button class="q_wrap question21">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">비전트립 일정 중 몇 개의 컴패션 어린이센터를 방문하나요?</span>
						                    </button>
						                    <div class="a_wrap answer21">일반적으로 하루에 1개 센터, 총 3~4개 정도의 컴패션 어린이센터를 방문하게 되며 일정에 따라 달라질 수 있습니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(22)">
						                    <button class="q_wrap question22">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">컴패션 어린이센터 방문 시 몇 명의 어린이들을 만날 수 있나요?</span>
						                    </button>
						                    <div class="a_wrap answer22">방문하게 될 컴패션 어린이센터에 등록된 어린이를 중심으로 때로 지역 어린이들을 같이 만나기도 합니다. 만나게 될 어린이 수는 방문 일정과 센터의 상황에 따라 다릅니다. 적게는 20명에서 많게는 200명 이상의 어린이들을 만나게 되실 수도 있습니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(23)">
						                    <button class="q_wrap question23">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">컴패션 어린이센터에서 평소에 진행되는 수업과 활동을 실제로 볼 수 있나요?</span>
						                    </button>
						                    <div class="a_wrap answer23">컴패션 어린이센터에서 평소에 진행되는 수업과 활동을 관찰하실 수 있으며 상황에 따라 평소 프로그램 대신 특별이벤트 등이 진행될 수도 있습니다. 평일 방문의 경우 어린이들이 학교에 가기 때문에 상대적으로 적은 수의 어린이들을 만나게 되며 수업 및 활동 관찰은 어려울 수 있습니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(24)">
						                    <button class="q_wrap question24">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">컴패션 어린이센터에서 환영프로그램을 준비해주나요?</span>
						                    </button>
						                    <div class="a_wrap answer24">방문자를 위한 환영프로그램을 따로 준비하는 것은 컴패션 어린이센터의 선생님들과 어린이들에게 부담을 줄 수 있어 따로 요청하지는 않습니다. 다만, 이미 준비된 공연, 특송 등이 있는 경우 환영프로그램이 진행되기도 합니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(25)">
						                    <button class="q_wrap question25">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">컴패션 어린이센터에서 만나는 어린이들은 모두 컴패션에 등록되어 후원을 받는 어린이들인가요?</span>
						                    </button>
						                    <div class="a_wrap answer25">대부분 컴패션에 등록된 어린이들인 경우가 많지만 등록되지 않은 어린이들도 포함되어 있습니다. 컴패션 어린이센터는 지역사회에 속한 교회이기도 하기에 컴패션에 등록되지 않은 어린이도 이 교회에 다닐 수 있습니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(26)">
						                    <button class="q_wrap question26">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">컴패션 어린이센터 방문 시 참가자로서 할 수 있는 일은 무엇이 있나요?</span>
						                    </button>
						                    <div class="a_wrap answer26">만나는 어린이, 가족, 선생님 등과 함께 웃으며 재미있는 시간을 보내주시면 됩니다. 어린이들과 더 즐거운 시간을 보내실 수 있도록 풍선, 페이스페인팅 도구, 비누방울 도구 등을 컴패션에서 준비해 갑니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(27)">
						                    <button class="q_wrap question27">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">컴패션 어린이센터 방문 시 선물을 따로 준비해야 하나요?</span>
						                    </button>
						                    <div class="a_wrap answer27">참가자분들의 방문과 그곳에서 함께 보내는 즐거운 시간이 어린이들과 가족, 선생님들에게 가장 큰 선물이 됩니다. 따로 선물을 준비하지는 않으셔도 되지만 개인적으로 준비하기 원하실 경우 많은 어린이들이 함께 나눠서 사용할 수 있는 선물로 스티커를 준비하시는 것도 좋습니다.<br /><br />
                                                ※ 모든 경우의 선물과 관련하여, 한국컴패션 비전트립팀과 사전 협의 없이 출발일 당일, 허용된 기준의 무게와 부피를 초과하는 수하물을 가져오시지 않도록 유념해주시기를 부탁 드립니다.<br />
                                                만일 허용 기준을 초과할 경우, 준비해오신 수하물은 현지로 가져가실 수 없으며, 이 경우 공항에 보관하는 모든 비용은 참가자 본인이 부담하셔야 함을 명심해주시기 바랍니다.
                                            </div>
					                    </div>
				                    </div>
                                </div>
                            </div>
						</div>
						<div class="tc tc3 clear2" style="display:none">
							<div class="con3">
                                <p class="p1"><span class="blue">컴패션 어린이의 가정</span>을 방문합니다.</p>
                                <p class="p2">컴패션 어린이와 가족들이 살고 있는 집을 방문하여 컴패션 사역이 어린이와 가족에게 어떠한 변화를 가져왔는지, 가족이 직면하고 있는 어려움은 무엇인지 등 궁금한 것들을 질문하며 더 깊은 이야기를 나누고 함께 기도할 수 있는 시간입니다. 수혜국 사무실에서 미리 준비한 선물을 전달하고, 사진 촬영도 하면서 어린이와 가족에게 잊을 수 없는 격려와 응원을 보내는 시간이 될 것입니다.</p>
                                <p class="p3">FAQ</p>
                                <div class="subContents etc">
                                    <div class="faq">
					                    <div class="faqSet" onClick="toggle(31)">
						                    <button class="q_wrap question31">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">가정 방문 선물을 따로 준비해야 하나요?</span>
						                    </button>
						                    <div class="a_wrap answer31">
                                                참가자께서 납부하신 비전트립 비용에는 현지에서 방문하는 가정에 전달할 선물 비용이 포함되어 있습니다. 방문하실 가정 상황을 잘 알고 있는 현지 직원이 필요한 식품 및 생필품 등을 구입하여 가정방문 시 참가자 여러분께 전달하여 직접 선물하실 수 있도록 준비합니다. 방문하는 가정과 어린이를 위한 선물을 개인적으로 준비하기 원하실 경우 방문 가정당 1 만원 이내의 범위에서 칫솔, 치약, 비누, 수건, 손톱깎이 등 생필품을 추천해 드립니다.<br /><br />
                                                ※ 모든 경우의 선물과 관련하여, 한국컴패션 비전트립팀과 사전 협의 없이 출발일 당일, 허용된 기준의 무게와 부피를 초과하는 수하물을 가져오시지 않도록 유념해주시기를 부탁 드립니다.<br />
                                                만일 허용 기준을 초과할 경우, 준비해오신 수하물은 현지로 가져가실 수 없으며, 이 경우 공항에 보관하는 모든 비용은 참가자 본인이 부담하셔야 함을 명심해주시기 바랍니다.
						                    </div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(32)">
						                    <button class="q_wrap question32">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">가정 방문 시 한 가정당 얼마 정도의 시간을 보내나요?</span>
						                    </button>
						                    <div class="a_wrap answer32">짧게는 40분에서 길게는 3시간 정도의 시간을 보내게 되며 현지의 상황과 일정에 따라 융통적으로 진행됩니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(33)">
						                    <button class="q_wrap question33">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">가정 방문은 몇 명이 함께 가나요? 영어 통역을 해주시는 분이 같이 가나요?</span>
						                    </button>
						                    <div class="a_wrap answer33">상황에 따라 적게는 3명에서 많게는 8명 정도가 한 조가 되어 함께 방문하게 되며, 조 편성 시 참가자 중 영어가 가능하신 분이 최소 한 명 배정되도록 하고 있습니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(34)">
						                    <button class="q_wrap question34">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">가정 방문을 갈 때 걸어서 가나요?</span>
						                    </button>
						                    <div class="a_wrap answer34">상황에 따라 걸어가기도 하고 벤, 지프니, 소형 트럭, 인력거, 오토바이 등을 타고 가게 되는 경우도 있습니다.</div>
					                    </div>
				                    </div>
                                </div>
                            </div>
						</div>
						<div class="tc tc4 clear2" style="display:none">
							<div class="con3">
								<p class="p1"><span class="blue">컴패션 졸업생</span>들을 만납니다.</p>
                                <p class="p2">컴패션 졸업생들은 1:1어린이양육을 통해 아름답게 성장한 컴패션 사역의 열매입니다. 어린 시절 가난 때문에 꿈을 잃고 살던 어린이가 컴패션 양육으로 변화하여 리더를 꿈꾸게 된 감동적인 스토리를 듣고, 함께 이야기를 나누며 격려해주는 시간이 될 것입니다.</p>
                                <p class="p3">FAQ</p>
                                <div class="subContents etc">
                                    <div class="faq">
					                    <div class="faqSet" onClick="toggle(41)">
						                    <button class="q_wrap question41">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">몇 명의 컴패션 졸업생을 만나게 되나요?</span>
						                    </button>
						                    <div class="a_wrap answer41">적게는 2명에서 많게는 10명 이상의 졸업생을 만나게 될 수도 있습니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(42)">
						                    <button class="q_wrap question42">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">컴패션 졸업생을 만나 무엇을 하나요?</span>
						                    </button>
						                    <div class="a_wrap answer42">함께 식사하고 교제를 나누며 간증을 듣는 시간을 갖습니다. 현지 사정에 따라 졸업생들이 미리 준비하여 게임, 공연, Q&A 시간 등이 이루어질 수도 있지만 필수로 진행되는 사항은 아닙니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(43)">
						                    <button class="q_wrap question43">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">컴패션 졸업생과 얼마 정도의 시간을 보내나요?</span>
						                    </button>
						                    <div class="a_wrap answer43">식사와 모든 프로그램을 포함하여 2시간에서 3시간 정도의 시간을 함께 합니다.</div>
					                    </div>
				                    </div>
                                </div>
							</div>
						</div>
                        <div class="tc tc5 clear2" style="display:none">
                            <div class="con3">
                                <p class="p1">그날의 일정에 대한 <span class="blue">의견과 느낌을 나누며 교제하는 시간</span>을 갖습니다.</p>
                                <p class="p2">하루의 일정을 마치면서 컴패션 어린이센터 방문, 후원 어린이 만남, 지역사회 방문, 현지 직원들과의 만남 등을 통해 보고 느낀 점을 참가자들과 함께 나누는 시간입니다. 하루의 일정을 돌아보며, 60여 년 전, 지금의 컴패션을 시작한 에버렛 스완슨 목사님을 결단하게 한 중요한 질문, ‘가난 가운데 살고 있는 어린이들을 위해 나는 무엇을 할 수 있을까’를 생각해 보고, 비전을 함께 나누는 것으로 하루를 마무리 합니다.</p>
                                <p class="p3">FAQ</p>
                                <div class="subContents etc">
                                    <div class="faq">
					                    <div class="faqSet" onClick="toggle(51)">
						                    <button class="q_wrap question51">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">나눔과 교제의 시간은 어떻게 진행되나요?</span>
						                    </button>
						                    <div class="a_wrap answer51">트립리더의 진행으로 영상 보기, 설명 듣기, 개인의 느낀 점과 생각 발표, 소그룹 나눔 등 다양한 방법으로 진행됩니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(52)">
						                    <button class="q_wrap question52">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">나눔과 교제의 시간에 꼭 참여해야 하나요?</span>
						                    </button>
						                    <div class="a_wrap answer52">자신의 생각과 느낌을 정리하고 서로 나누는 중요한 비전트립 프로그램이므로 모든 나눔과 교제의 시간에 반드시 참석하셔야 합니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(53)">
						                    <button class="q_wrap question53">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">나눔과 교제의 시간은 언제, 어디서 진행되며 얼마 정도의 시간이 걸리나요?</span>
						                    </button>
						                    <div class="a_wrap answer53">가능하면 하루 일정을 마친 후 호텔의 미팅룸, 식당이나 카페의 룸 등 별도로 분리된 공간에서 진행되며 짧게는 1시간에서 길게는 3시간 정도의 시간이 소요됩니다.</div>
					                    </div>
				                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tc tc6 clear2" style="display:none">
                            <div class="con3">
                                <p class="p1"><span class="blue">후원하고 있는 어린이</span>를 만납니다.</p>
                                <p class="p2">편지와 기도로 마음을 나누었던 어린이를 직접 만날 수 있는 감동의 시간입니다. 먼 곳에서부터 어린이를 만나러 와주신 후원자님의 사랑의 마음은 어린이와 가족에게 아름다운 선물이 되며, 후원자님의 사랑으로 더 밝게 자라가는 어린이와의 만남은 후원자님에게도 평생 잊지 못할 소중한 시간이 될 것입니다.</p>
                                <p class="p3">FAQ</p>
                                <div class="subContents etc">
                                    <div class="faq">
					                    <div class="faqSet" onClick="toggle(61)">
						                    <button class="q_wrap question61">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">후원 어린이를 만나려면 어떻게 해야 하나요?</span>
						                    </button>
						                    <div class="a_wrap answer61">비전트립 신청 시 후원 어린이 만남 항목에서 신청해 주시면 됩니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(62)">
						                    <button class="q_wrap question62">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">가족이나 지인의 후원 어린이를 대신하여 만날 수 있나요?</span>
						                    </button>
						                    <div class="a_wrap answer62">비전트립 신청 시 가족관계를 증명할 수 있는 가족관계증명서 및 후원 어린이 만남 동의서(한국컴패션 양식)를 첨부하여 제출해 주시면, 가족이 후원하고 있는 어린이를 만나실 수 있습니다. 단, 가족 외 지인의 경우에는 만남이 승인되지 않습니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(63)">
						                    <button class="q_wrap question63">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">후원 어린이와는 얼마 정도의 시간을 보낼 수 있나요?</span>
						                    </button>
						                    <div class="a_wrap answer63">짧게는 3시간에서 길게는 6시간 정도의 시간을 함께 하실 수 있으며 최대한 많은 시간을 함께 보내실 수 있도록 노력하고 있습니다. 하지만 교통, 날씨, 장소 등 상황에 따라 예상보다 짧아질 수도 있습니다.</div>
					                    </div>
                                        <div class="faqSet" onClick="toggle(64)">
						                    <button class="q_wrap question64">
							                    <span class="q_icon">&nbsp;</span>
							                    <span class="q_tit">후원 어린이를 위한 선물을 준비해도 되나요?</span>
						                    </button>
						                    <div class="a_wrap answer64">마음을 담은 편지와 후원자님이 살고 계신 나라를 상징하는 기념품 등은 후원 어린이에게 소중한 선물이 될 것입니다. 이외에도 생필품, 학용품 혹은 가족을 위한 선물 등을 준비해 오실 수 있습니다. 다만 너무 많은 양의 선물은 비행기 탑승 시 어려움을 초래할 수 있으며 많은 짐을 들고 다니느라 어린이와 함께 하는 시간에 집중하기 어려울 수도 있기에 적당한 양을 어린이가 멜 수 있는 백팩에 넣어 가방 채로 선물하는 것을 추천 드립니다. 그리고 어린이와 만나는 장소 근처에 쇼핑이 가능한 곳이 있다면 어린이에게 맞는 옷이나 신발, 생필품 등을 사주실 수도 있습니다. 다만 현금을 어린이나 가족에게 주는 것은 금지되어 있습니다.</div>
					                    </div>
				                    </div>
                                </div>
                            </div>
                        </div>

                        <script>
                            function toggle (item) {
                                var question = $(".question" + item);
                                var answer = $(".answer" + item);
                                if (answer.is(":visible")) {
                                    question.removeClass("on");
                                    answer.slideUp(300);
                                } else {
                                    $(".answer").hide();
                                    $(".a_wrap").slideUp(300);
                                    answer.slideDown(300);
                                    if ($(".q_wrap").hasClass("on")) {
                                        $(".q_wrap").removeClass("on");
                                    }
                                    question.addClass("on");
                                }
                            }
                        </script>
					</div>
				</div>

				<!-- 비전트립 프로그램 -->
				<div class="bgContent tripProgram">
					<div class="w980">
						<p class="tit">비전트립 프로그램</p>
						<span class="bar"></span>
						<p class="con">
							비전트립은 방문 국가, 항공 일정 등에 따라 4박 6일~7박 9일 정도의 기간 동안 진행됩니다.<br />
							<span>컴패션 수혜국 국가사무실 방문, 컴패션어린이센터 방문, 1:1리더십결연 학생 및 졸업생들과의 저녁식사, 나눔과 교제(디브리핑)의 시간</span>이<br />
							일정에 포함 되며, 수혜국 상황에 따라 태아·영아생존프로그램 및 다양한 양육보완프로그램을 살펴봅니다. 또한, 후원하고 있는 어린이를<br />
							직접 만나 근처 놀이공원, 동물원, 쇼핑몰 등에서 함께 시간을 보내는 <span>후원어린이와의 만남(Fun Day)이 진행되기도 합니다.</span>
						</p>
					</div>
				</div>
				<!--//-->

				<div class="w980">

					<!-- 프로그램 예시 -->
					<div class="church mb60">
						<p class="s_tit6 mb30">비전트립 프로그램 예시 - <span class="fc_blue">필리핀 4박 6일</span></p>

						<div class="tableWrap1 mb30">
							<table class="tbl_type9 line">
								<caption>비전트립 프로그램 예시 테이블</caption>
								<colgroup>
									<col style="width:10%" />
									<col style="width:15%" />
									<col style="width:17%" />
									<col style="width:17%" />
									<col style="width:17%" />
									<col style="width:12%" />
									<col style="width:12%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col">일정</th>
										<th scope="col">Day1</th>
										<th scope="col">Day2</th>
										<th scope="col">Day3</th>
										<th scope="col">Day4</th>
										<th scope="col">Day5</th>
										<th scope="col">Day6</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">오전</th>
										<td>
											-인천출발<br />
											-필리핀도착
										</td>
										<td>컴패션어린이센터<br />방문</td>
										<td>컴패션어린이센터<br />방문</td>
										<td>컴패션어린이센터<br />방문</td>
										<td rowspan="3">Fun Day<em class="cf">3)</em></td>
										<td rowspan="4" class="nobottom">인천도착</td>
									</tr>
									<tr>
										<th scope="row">점심</th>
										<td>점심식사</td>
										<td>점심식사</td>
										<td>점심식사</td>
										<td class="ie_border">점심식사</td>
									</tr>
									<tr>
										<th scope="row">오후</th>
										<td>국가사무실 방문</td>
										<td>가정방문</td>
										<td>가정방문</td>
										<td class="ie_border">가정방문</td>
									</tr>
									<tr>
										<th scope="row">저녁</th>
										<td>
											저녁식사<br />
											디브리핑<em class="cf">1)</em>
										</td>
										<td>
											저녁식사<br />
											디브리핑
										</td>
										<td>
											저녁식사<br />
											디브리핑
										</td>
										<td>
											LDP디너<em class="cf">2)</em><br />
											디브리핑
										</td>
										<td class="ie_border">필리핀 출발</td>
									</tr>
								</tbody>
							</table>

						</div>

						<div class="box_type3 pastor_cf">
							<span>1) 디브리핑 - 나눔과 교제의 시간</span>
							<span>2) LDP디너 - 1:1리더십결연 학생 및 졸업생들과의 저녁식사</span>
							<span>3) Fun Day - 후원어린이와의 만남의 시간</span>
						</div>
					</div>
					<!--// 프로그램 예시 -->

					<div class="tac mb60">
						<a href="/participation/visiontrip/schedule/" class="btn_type1">비전트립 신청하기</a>
					</div>


					<!-- 문의 : 마지막탭에만 활성화 -->
					<div class="cq_ta">
						<div class="contact_qna div3">
							<p class="tit">문의</p>
							<div class="clear2">
								<div class="tel"><span class="icon"><span class="txt">전화 : </span>02)3668-3453, 3447</span></div>
								<div class="fax"><span class="icon"><span class="txt">팩스 : </span>02)3668-3501</span></div>
								<div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:visiontrip@compassion.or.kr">visiontrip@compassion.or.kr</a></span></div>
							</div>
						</div>

						<div class="mb60">
							<span class="fs15 vam">자주 묻는 질문을 이용하시면 더 많은 궁금증을 해결하실 수 있습니다.</span>
							<a href="/customer/faq/" class="btn_s_type2 ml10">FAQ 바로가기</a>
						</div>
					</div>

					<div class="banner">
						<p class="s_tit1 mb20">비전트립의 자세한 후기가 궁금하신가요?</p>
						<a href="/sympathy/vision/vision/"><img src="/common/img/page/participation/btn_banner1.jpg" alt="비전트립 후기 보러가기" /></a>
					</div>

				</div>

			</div>
			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->

		

    </section>

</asp:Content>
