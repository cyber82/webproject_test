﻿using CommonLib;
using Microsoft.AspNet.FriendlyUrls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class participation_visiontrip_form_apply_plan : FrontBasePage
{
    const string listPath = "/participation/visiontrip/schedule";

    private string strUserID = string.Empty;
    private string strSponserID = string.Empty;
    private string strApplyID = string.Empty;

    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }
    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();
        //this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"]; 

        if (!UserInfo.IsLogin)
        {
            Response.ClearContent();
            return;
        }

        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1)
        {
            Response.Redirect(listPath, true);
        }
        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);

        if (!requests[0].CheckNumeric())
        {
            Response.Redirect(listPath, true);
        }

        //parameter
        base.PrimaryKey = requests[0];
        id.Value = PrimaryKey.ToString();
        strApplyID = id.Value;


        UserInfo sess = new UserInfo();
        strUserID = sess.UserId;
        strSponserID = sess.SponsorID;

        //파일경로
        hdnFileRoot.Value = Uploader.GetRoot(Uploader.FileGroup.file_visiontrip) + "plan/" + id.Value + "/" + strUserID + "/";
        //비전트립 일정 정보
        GetVisionTripSchedule();
         
         
        // 이름
        txtName.Value = sess.UserName;
        // 생년월일
        txtBirth.Value = sess.Birth != "" ? sess.Birth.Substring(0, 10) : "";

        #region tSponsorMaster 정보
        string strAddr1_overseas = string.Empty;
        // WEB tSponsorMaster
        using (AuthLibDataContext daoAuth = new AuthLibDataContext())
        {
            //var entity = daoAuth.tSponsorMaster.First(p => p.UserID == sess.UserId);

            CommonLib.WWW6Service.SoaHelperSoap _www6Service;
            _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

            string dbType = "Text";
            string dbName = "SqlCompassionAuth";
            Object[] objSql = new object[1] { "select * from tSponsorMaster where UserID='" + sess.UserId + "'"};
            DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, null, null);
            tSponsorMaster entity = ds.Tables[0].Rows[0].DataTableToFirst<tSponsorMaster>();

            locationType.Value = entity.LocationType;
            strAddr1_overseas = entity.Addr1.Decrypt(); 

            txtEmail.Value = entity.Email.Decrypt();
            txtPhone.Value = entity.Phone.Decrypt().TranslatePhoneNumber().Replace("-", ""); ;

        }
        #endregion

        addr_domestic.Checked = locationType.Value == "국내";
        addr_oversea.Checked = locationType.Value != "국내"; 

        var addr_result = new SponsorAction().GetAddress();
        if (!addr_result.success)
        {
            base.AlertWithJavascript(addr_result.message, "goBack()");
            return;
        }

        SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;

        string doroAddr = addr_data.DspAddrDoro != "" ? addr_data.DspAddrDoro.Replace(addr_data.Addr2, "") : "";
        string jibunAddr = addr_data.DspAddrJibun != "" ? addr_data.DspAddrJibun.Replace(addr_data.Addr2, "") : "";

        hfAddressType.Value = addr_data.AddressType;
        zipcode.Value = addr_data.Zipcode;
        addr1.Value = locationType.Value == "국내" ? doroAddr + "//" + jibunAddr : addr_data.Addr1;//Addr1;
        addr2.Value = addr_data.Addr2;
        dspAddrDoro.Value = addr_data.DspAddrDoro;
        dspAddrJibun.Value = addr_data.DspAddrJibun;
         
        var comm_result = new SponsorAction().GetCommunications();
        if (!comm_result.success)
        {
            base.AlertWithJavascript(comm_result.message, "goBack()");
            return;
        }

        SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
        txtPhone.Value = comm_data.Mobile.Replace("-", ""); ;//.TranslatePhoneNumber();

        #region 국외인경우 국가목록 
        var actionResult = new CodeAction().Countries();
        if (!actionResult.success)
        {
            base.AlertWithJavascript(actionResult.message);
        }

        ddlHouseCountry.DataSource = actionResult.data;
        ddlHouseCountry.DataTextField = "CodeName";
        ddlHouseCountry.DataValueField = "CodeName";
        ddlHouseCountry.DataBind(); 
        #endregion

        if (locationType.Value != "국내")
        {
            var str_location = strAddr1_overseas.EmptyIfNull().Split('$').Length > 1 ? strAddr1_overseas.EmptyIfNull().Split('$')[1] : "한국";
            ddlHouseCountry.SelectedValue = str_location;
        }
        // end 주소


        //현금영수증 정보
        txtCashReceiptName.Value = sess.UserName;
        txtCashReceiptTel.Value = comm_data.Mobile; 
    }

    /// <summary>
    /// VisionTrip Schedule
    /// </summary>
    protected void GetVisionTripSchedule()
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.tVisionTripSchedule.Where(p => p.ScheduleID == Convert.ToInt32(id.Value)).ToList();

            string dbType = "Text";
            string dbName = "SqlCompassionWeb";
            Object[] objSql = new object[1] { "select * from tVisionTripSchedule where ScheduleID='" + Convert.ToInt32(id.Value) + "'" };
            DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, null, null);
            List<tVisionTripSchedule> list = ds.Tables[0].DataTableToList<tVisionTripSchedule>();
             
            if (list != null)
            {
                txtTripSchedule.Value = (list.ToArray())[0].StartDate.ToString() + " ~ " + (list.ToArray())[0].EndDate.ToString();
                txtVisitCountry.Value = (list.ToArray())[0].VisitCountry.ToString();
            }
        }
    }
     
}