﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="apply_plan.aspx.cs" Inherits="participation_visiontrip_form_apply_plan" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>


<asp:Content ID="head_meta" ContentPlaceHolderID="head_meta" runat="Server">
</asp:Content>
<asp:Content ID="head_script" ContentPlaceHolderID="head_script" runat="Server">
    <script type="text/javascript" src="/participation/visiontrip/form/apply_plan.js?v=1.2"></script>
    <script type="text/javascript" src="/participation/visiontrip/form/multiselect.js"></script>
    
	<script type="text/javascript" src="/common/js/site/motive.js"></script>
	<%--<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js"></script>--%>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/email.js"></script>
    
    <link href="/participation/visiontrip/form/template/visiontrip_form.css" rel="stylesheet" />   

    
    <script type="text/javascript">

        $(function () {
            $page.init();

            //주소
            $("#addr_overseas_addr1").setHangulBan();
            $("#addr_overseas_addr2").setHangulBan();
            $(".rd_addr").change(function () {
                if ($("#addr_domestic").prop("checked")) {
                    $(".hide_overseas").show();
                    $("#pn_addr_domestic").show();
                    $("#pn_addr_overseas").hide();
                } else {
                    $(".hide_overseas").hide();
                    $("#pn_addr_domestic").hide();
                    $("#pn_addr_overseas").show();
                }
            });

            $("[data-id=check_addr]").hide();
            if ($("#addr_domestic").prop("checked")) {
                $(".hide_overseas").show();
                $("#pn_addr_domestic").show();
                $("#pn_addr_overseas").hide();
                $(".zipcode_area").show();
            } else {
                $(".hide_overseas").hide();
                $("#pn_addr_domestic").hide();
                $("#pn_addr_overseas").show();
                $(".zipcode_area").hide();
            }

             
            if ($("#addr_domestic").is(":checked")) {

                $("#pn_addr_domestic").show();
                $("#pn_addr_overseas").hide();  

                $("#addr_domestic_zipcode").val($("#zipcode").val());
                $("#addr_domestic_addr1").val($("#addr1").val());
                $("#addr_domestic_addr2").val($("#addr2").val());

                // 컴파스의 데이타를 불러오는경우 
                if ($("#dspAddrDoro").val() != "") {
                    $("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
                    if ($("#dspAddrJibun").val() != "") {
                        $("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
                    }

                } else if ($("#addr1").val() != "") {

                    addr_array = $("#addr1").val().split("//");
                    $("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + addr_array[0] + " " + $("#addr2").val());
                    if (addr_array[1]) {
                        $("#addr_jibun").text("[지번주소] (" + $("#zipcode").val() + ") " + addr_array[1] + " " + $("#addr2").val());
                    }
                }

            } else {
                $("#pn_addr_domestic").hide();
                $("#pn_addr_overseas").show(); 

                $("#addr_overseas_zipcode").val($("#zipcode").val());
                $("#addr_overseas_addr1").val($("#addr1").val());
                $("#addr_overseas_addr2").val($("#addr2").val()); 
            }
            //end - 주소





            //// 컴파스의 데이타를 불러오는경우 
            //if ($("#dspAddrDoro").val() != "") {
            //    $("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
            //    if ($("#dspAddrJibun").val() != "") {
            //        $("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
            //    }

            //} else {
            //    if ($("#addr1").val() != "") {
            //        var addr_array = $("#addr1").val().split("//");
            //        $("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + addr_array[0] + " " + $("#addr2").val());
            //        if (addr_array[1]) {
            //            $("#addr_jibun").text("[지번주소] (" + $("#zipcode").val() + ") " + addr_array[1] + " " + $("#addr2").val());
            //        }
            //    }
            //} 

            //첨부파일 - 여권
            var uploader_passport = $page.attachUploader("btn_passportfile");
            uploader_passport._settings.data.fileDir = $("#hdnFileRoot").val();
            //ploader_passport._settings.data.rename = "y";
            //uploader_passport._settings.data.fileType = "image";
            uploader_passport._settings.data.limit = 2048;

            //if ($(".etcattach").length() > 0) {

            //}

            $("#btnPassportDel").click(function () {
                $("#path_btn_passportfile").val("");
                $("[data-id=path_btn_passportfile]").val("");
            });

            //기독교일 경우에만 교회 입력
            $("#txtChurch").hide();
            $("#ddlReligion").change(function () {
                if ($("#ddlReligion option:selected").val() == "Christian") {
                    $("#txtChurch").show();
                } else {
                    $("#txtChurch").hide();
                }
            });

            $("#ddlCashReceiptType").change(function () {
                if ($("#ddlCashReceiptType option:selected").val() == "M") {
                    $("#txtCashReceiptRelation").hide();
                    $("#txtCashReceiptName").val($("#txtName").val());
                    $("#txtCashReceiptTel").val($("#txtPhone").val());
                    $("#txtCashReceiptRelation").val("본인");
                } else {
                    $("#txtCashReceiptRelation").show();
                    $("#txtCashReceiptName").val("");
                    $("#txtCashReceiptTel").val("");
                    $("#txtCashReceiptRelation").val("");
                }
            });

            $("#ddlChildMeetYn").change(function () {
                if ($("#ddlChildMeetYn option:selected").val() == "N") {
                    $("#spMultiSelect").hide();
                    $("#plChildtext").hide();
                } else {
                    $("#spMultiSelect").show();
                    $("#plChildtext").show();
                }
            });

            $("#ddlRoomType").change(function () {
                if ($("#ddlRoomType option:selected").val() == "S") {
                    $("#txtRoomDetail").hide(); 
                } else {
                    $("#txtRoomDetail").show(); 
                }
            });
            

        }); 
    </script>
    
<script type="text/javascript">
    //<![<%--CDATA[
    var theForm = document.forms['form'];
    if (!theForm) {
        theForm = document.form;
    }
    function __doPostBack(eventTarget, eventArgument) {
        if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
            theForm.__EVENTTARGET.value = eventTarget;
            theForm.__EVENTARGUMENT.value = eventArgument;
            theForm.submit();
        }
    }

    //]]>
    function btnSaveClick() {
        __doPostBack("<%= btnSave %>", "");
    }--%>
</script>

</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="body" runat="Server">
    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        
        <asp:HiddenField runat="server" ID="id" />
        <asp:HiddenField runat="server" ID="hdnApplyType" Value="Plan" />
        <asp:HiddenField runat="server" ID="childlist" />
        <asp:HiddenField runat="server" ID="hdnFileRoot" />  

        <div class="page_tit">
            <div class="titArea">
                <h1><em>컴패션비전트립 참가신청&#183;동의서</em></h1>
                <span class="desc">컴패션어린이들과 함께할 설레는 시간을 계획해 보세요</span>
                <uc:breadcrumb runat="server" />
            </div>
        </div>

        <div class="subContents member">
            <div class="w980 trip_sch">
                <div class="tab_info">  
                    <span class="nec_info">표시는 필수입력 사항입니다.</span>
                </div>

                <div class="input_div">
                    <div class="login_field">
                        <span>참가트립</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력</caption>
                            <colgroup>
                                <%--<col style="width: 400px" />--%>
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="text" id="txtTripSchedule" runat="server" maxlength="10" class="input_type2" value="" style="width: 200px;" readonly="readonly" disabled="disabled" />
                                        <input type="text" id="txtVisitCountry" runat="server" maxlength="10" class="input_type2" value="" style="width: 200px;" readonly="readonly" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            <span class="s_con5">트립 일정과 방문 국가를 확인했습니다.</span>
                                            <span class="sel_type2" style="width: 100px;">
                                                <label for="s_country" class="hidden">일정확인</label>
                                                <asp:DropDownList runat="server" ID="ddlScheduleCheck" class="custom_sel" Width="100">
                                                    <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                        </div>
                                        <span class="guide_comment2" data-id="check_tripschedule" style="display: none"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="input_div">
                    <div class="login_field">
                        <span>참가자정보</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력</caption>
                            <colgroup>
                                <%--<col style="width: 400px" />--%>
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <div>
                                            <input type="text" id="txtName" runat="server" class="input_type2" value="" style="width: 150px;" readonly="readonly" disabled="disabled" />
                                            <input type="text" id="txtNameEng" runat="server" maxlength="50" class="input_type2" value="" style="width: 150px;ime-mode:disabled" placeholder="영문명(여권과 동일)" />
                                            <input type="text" id="txtBirth" runat="server" maxlength="10" class="input_type2" value="" style="width: 150px;" readonly="readonly" disabled="disabled" />
                                            <p class="s_con2">영문명 기재 시 여권과 동일, 알파벳 대문자로 기재해 주세요(성+이름)</p>
                                            <p class="s_con2">
                                                미성년자(출발일 기준 만 19세 미만)의 경우 부모동의서, 위임장, 영문주민등록등본(원본)을 첨부/제출하셔야 합니다(추후 첨부/제출 가능). 
                                            </p>
                                            <p> 
                                                <a class="btn_s_type4" href="/files/visiontrip/doc/미성년자_비전트립 참가 부모동의서.pdf" target="_blank"><span class="ic_down"></span>부모동의서</a>
                                                <a class="btn_s_type4" href="/files/visiontrip/doc/미성년자_비전트립 위임장.pdf" target="_blank"><span class="ic_down"></span>위임장</a>
                                            </p>
                                        </div>
                                        <span class="guide_comment2" data-id="check_engname" style="display: none"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="input_div divUserInfo">
                    <div class="login_field">
                        <span>주소 및 연락처</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 주소 및 연락처</caption>
                            <colgroup>
                                <%--<col style="width: 400px" />--%>
                                <col style="width: /" />
                            </colgroup>
                            <tbody>

                                <tr>
                                    <td>
                                        <p class="s_con9">해당 정보는  비전트립 관련 안내에 한하여 사용되며 웹 등록 정보와 불일치 하여도 무관합니다. </p>
                                        <input type="text" id="txtPhone" runat="server" maxlength="13" class="input_type2 number_only" value="" style="width: 400px;" placeholder="휴대번호(-없이 입력)"/>
                                        <span class="guide_comment2" data-id="check_phone" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="txtEmail" runat="server" maxlength="50" class="input_type2" value="" style="width: 400px;" placeholder="이메일"/>
                                        <span class="guide_comment2" data-id="check_email" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            <input type="hidden" runat="server" id="locationType" />
                                            <input type="hidden" runat="server" id="hfAddressType" value="" />

                                            <input type="hidden" runat="server" id="zipcode" />
                                            <input type="hidden" id="addr1" runat="server" />
                                            <input type="hidden" id="addr2" runat="server" />
                                            <input type="hidden" runat="server" id="dspAddrJibun" value="" />
                                            <input type="hidden" runat="server" id="dspAddrDoro" value="" /> 


                                            <span class="radio_ui">
                                                <input type="radio" runat="server" class="rd_addr css_radio" id="addr_domestic" name="addr_location" />
                                                <label for="addr_domestic" class="css_label">국내</label>

                                                <input type="radio" runat="server" class="rd_addr css_radio" id="addr_oversea" name="addr_location" />
                                                <label for="addr_oversea" class="css_label ml20">해외</label>

                                            </span>
                                            <%--<span id="pn_addr_domestic" style="display: none; padding-left: 31px;">
                                                <label for="zipcode" class="hidden">주소찾기</label>
                                                 <a href="javascript:void(0)" class="btn_s_type2 ml5" runat="server" id="popup" ng-click="addrpopup($event)">주소찾기</a>
                                                <p id="addr_road" class="fs14 mt15"></p>
                                                <p id="addr_jibun" class="fs14 mt10"></p>
                                            </span>--%>

                                            <span id="pn_addr_domestic" runat="server" style="display: none">
                                                <label for="zipcode" class="hidden">주소찾기</label>
                                                <a href="javascript:void(0)" class="btn_s_type2 ml5" runat="server" id="popup" ng-click="addrpopup($event)">주소찾기</a>
                                                <input type="hidden" id="addr_domestic_zipcode" />
                                                <input type="hidden" id="addr_domestic_addr1" />
                                                <input type="hidden" id="addr_domestic_addr2" />

                                                <p id="addr_road" class="mt15"></p>
                                                <p id="addr_jibun" class="mt10"></p>
                                            </span>

                                            <!-- 해외주소 체크 시 -->
                                            <div id="pn_addr_overseas" runat="server" style="display: none; width: 400px;" class="mt15">
                                                <span class="sel_type2 fl" style="width: 195px;">
                                                    <label for="ddlHouseCountry" class="hidden">국가 선택</label>
                                                    <asp:DropDownList runat="server" ID="ddlHouseCountry" class="custom_sel"></asp:DropDownList>

                                                </span>
                                                <label for="addr_overseas_zipcode" class="hidden">우편번호</label>

                                                <input type="text" id="addr_overseas_zipcode" class="input_type2 fr" value="" style="width: 195px" placeholder="우편번호"  maxlength="50" />
                                                <input type="text" id="addr_overseas_addr1" class="input_type2 mt10" placeholder="주소" style="width: 400px;"  maxlength="1000" />
                                                <input type="text" id="addr_overseas_addr2" class="input_type2 mt10" placeholder="상세주소" style="width: 400px;"  maxlength="1000" />
                                            </div>
                                            <!--// -->
                                        </div>

                                        <span class="guide_comment2" data-id="check_addr" style="display: none"></span>

                                        <div class="mt10">
                                            <span class="s_con5">후원과 관련된 모든 안내/정보를 위 정보로 변경하기 원하십니까?</span>
                                            <span class="sel_type2" style="width: 100px;">
                                                <label for="s_country" class="hidden">기존정보변경</label>
                                                <asp:DropDownList runat="server" ID="ddlInfoChange" class="custom_sel" Width="100">
                                                    <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                        </div>
                                        <span class="guide_comment2" data-id="check_infochange" style="display: none"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="input_div divGender">
                    <div class="login_field">
                        <span>성별</span>
                    </div>
                    <div class="login_input ">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 성별</caption>
                            <colgroup>
                                <%--<col style="width: 400px" />--%>
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <div>
                                            <span class="sel_type2" style="width: 120px;">
                                                <label for="s_country" class="hidden">성별 선택</label>
                                                <asp:DropDownList runat="server" ID="ddlGender" class="custom_sel" Width="120">
                                                    <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="남자" Value="M"></asp:ListItem>
                                                    <asp:ListItem Text="여자" Value="F"></asp:ListItem>
                                                </asp:DropDownList>
                                            </span>

                                            <span class="guide_comment2" data-id="check_gender" style="display: none"></span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="input_div divNation">
                    <div class="login_field">
                        <span>국적</span>
                    </div>
                    <div class="login_input ">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 국적</caption>
                            <colgroup>
                                <%--<col style="width: 400px" />--%>
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <div>
                                            <span class="sel_type2" style="width: 120px;">
                                                <label for="s_country" class="hidden">국적 선택</label>
                                                <asp:DropDownList runat="server" ID="ddlNation" class="custom_sel" Width="120">
                                                    <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="대한민국" Value="K"></asp:ListItem>
                                                    <asp:ListItem Text="기타" Value="E"></asp:ListItem>
                                                </asp:DropDownList>
                                            </span>

                                            <span class="guide_comment2" data-id="check_nation" style="display: none"></span>
                                        </div>
                                        <p class="s_con2">외국인 후원자의 경우, 외국인거소증 사본(앞, 뒷면)을 첨부/제출해 주셔야 합니다.</p> 
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="input_div divReligion">
                    <div class="login_field">
                        <span>종교</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 종교</caption>
                            <colgroup>
                                <col style="width: 120px" />
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <div>
                                            <span class="sel_type2" style="width: 120px;">
                                                <label for="s_country" class="hidden">종교선택</label>
                                                <asp:DropDownList runat="server" ID="ddlReligion" class="custom_sel" Width="120" onchange="">
                                                    <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="기독교" Value="Christian"></asp:ListItem>
                                                    <asp:ListItem Text="불교" Value="Buddhist"></asp:ListItem>
                                                    <asp:ListItem Text="천주교" Value="Catholic"></asp:ListItem>
                                                    <asp:ListItem Text="무교" Value="Atheism"></asp:ListItem>
                                                    <asp:ListItem Text="기타" Value="Etc"></asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" id="txtChurch" runat="server" maxlength="25" class="input_type2" value="" style="width: 400px;" placeholder="교회명" />

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <span class="guide_comment2" data-id="check_religion" style="display: none"></span>
                        <span class="guide_comment2" data-id="check_church" style="display: none"></span>
                    </div>
                </div>

                <div class="input_div divGroupField">
                    <table style="width: 100%">
                        <colgroup>
                            <col style="width: 33%;" />
                            <col style="width: 33%;" />
                            <col style="width: 33%;" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="login_field">
                                        <span>영어회화 능력</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="login_field">
                                        <span>비전트립 참가이력</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="login_field">
                                        <span>직업</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div>
                                        <span class="sel_type2" style="width: 120px;">
                                            <label for="s_country" class="hidden">영어회화 능력 선택</label>
                                            <asp:DropDownList runat="server" ID="ddlEnglishLevel" class="custom_sel" Width="120">
                                                <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                <asp:ListItem Text="상" Value="T"></asp:ListItem>
                                                <asp:ListItem Text="중" Value="M"></asp:ListItem>
                                                <asp:ListItem Text="하" Value="B"></asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                    </div>
                                    <span class="guide_comment2 mt5" data-id="check_englishlevel" style="display: none"></span>
                                </td>

                                <td>
                                    <div>
                                        <span class="sel_type2" style="width: 120px;">
                                            <label for="s_country" class="hidden">비전트립 참가이력 선택</label>
                                            <asp:DropDownList runat="server" ID="ddlVTHistory" class="custom_sel" Width="120">
                                                <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                <asp:ListItem Text="처음" Value="Z"></asp:ListItem>
                                                <asp:ListItem Text="1회" Value="F"></asp:ListItem>
                                                <asp:ListItem Text="2회" Value="S"></asp:ListItem>
                                                <asp:ListItem Text="3회 이상" Value="T"></asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                    </div>
                                    <span class="guide_comment2 mt5" data-id="check_visiontriphistory" style="display: none"></span>
                                </td>
                                <td>
                                    <div>
                                        <input type="text" id="txtJob" runat="server" maxlength="100" class="input_type2 mb5" value="" style="width: 200px;" placeholder="직업" />
                                    </div>
                                    <span class="guide_comment2 mt5" data-id="check_job" style="display: none"></span>
                                </td>
                            </tr>
                    </table>
                </div>
                <div class="input_div divMilitary">
                    <div class="login_field">
                        <span>병역</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 병역</caption>
                            <colgroup>
                                <%--<col style="width: 400px" />--%>
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>

                                    <td>
                                        <div>
                                            <span class="sel_type2" style="width: 120px;">
                                                <label for="s_country" class="hidden">병역 선택</label>
                                                <asp:DropDownList runat="server" ID="ddlMilitary" class="custom_sel" Width="120">
                                                    <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="필" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="미필" Value="U"></asp:ListItem>
                                                    <asp:ListItem Text="해당사항없음" Value="N"></asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <span class="guide_comment2" data-id="check_military" style="display: none"></span>
                                        </div>
                                        <p class="s_con2">
                                            병역미필자의 경우 병무청에서 허가를 받으셔야 하며, 개인마다 경우가 다르므로 미리 확인하시기 바랍니다. 
                                            <br />
                                            (<a href="http://www.mma.go.kr/index.do">병무청 사이트 바로가기</a>)
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="input_div divEmergencyContact">
                    <div class="login_field">
                        <span>비상연락처</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 비상연락처</caption>
                            <colgroup>
                                <%--<col style="width: 400px" />--%>
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="text" id="txtEmergencyContactName" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="비상연락처 - 성함" />
                                        <span class="guide_comment2" data-id="check_emergencycontactname" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="txtEmergencyContactTel" runat="server" maxlength="11" class="input_type2 number_only" value="" style="width: 400px;" placeholder="비상연락처 - 휴대번호(-없이 입력)" />

                                        <span class="guide_comment2" data-id="check_emergencycontacttel" style="display: none"></span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="txtEmergencyContactRelation" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="비상연락처 - 참가자와의 관계" />

                                        <span class="guide_comment2" data-id="check_emergencycontactrelation" style="display: none"></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="input_div divRoom">
                    <div class="login_field">
                        <span>방배정</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 방배정</caption>
                            <colgroup>
                                <col style="width: 120px" />
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <span class="sel_type2" style="width: 120px;">
                                            <label for="s_country" class="hidden">방배정 선택</label>
                                            <asp:DropDownList runat="server" ID="ddlRoomType" class="custom_sel" Width="120">
                                                <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                <asp:ListItem Text="싱글" Value="S"></asp:ListItem>
                                                <asp:ListItem Text="트윈" Value="T"></asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtRoomDetail" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="원하시는 룸메이트가 있는 경우 룸메이트의 성함을 기재해 주세요" />
                                        <span class="guide_comment2" data-id="check_roomdetail" style="display: none"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div><span class="guide_comment2" data-id="check_roomtype" style="display: none"></span></div>
                        <p class="s_con2">
                            신청 순서대로 숙소가 배정되어 트윈룸을 신청하셨어도 싱글룸으로 배정될 수 있으며, 싱글룸 사용시 20~30만원 정도 추가됩니다
                        </p>
                    </div>
                </div>
                <div class="input_div divChildMeet">
                    <div class="login_field">
                        <span>후원어린이
                            <br />
                            만남</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 후원어린이만남</caption>
                            <colgroup>
                                <col style="width: 120px" />
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <span class="sel_type2" style="width: 120px;">
                                            <label for="s_country" class="hidden">후원어린이 만남 여부 선택</label>
                                            <asp:DropDownList runat="server" ID="ddlChildMeetYn" class="custom_sel" Width="120">
                                                <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                <asp:ListItem Text="예" Value="Y"></asp:ListItem>
                                                <asp:ListItem Text="아니오" Value="N"></asp:ListItem>
                                            </asp:DropDownList>
                                        </span>

                                    </td>
                                    <td>
                                        <span class="sel_type2" style="width: 300px;" id="spMultiSelect">
                                            <multiselect class="input-xlarge" multiple="true"
                                                ng-model="selectedChild"
                                                options="c.name for c in mychild"
                                                change="selected()"></multiselect>
                                            <div class="well well-small" style="display:none;">
                                                <asp:HiddenField runat="server" ID="hdnSelectedChild" value="{{selectedChild}}"/>
                                            </div>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"> 
                                        <asp:Panel runat="server" ID="plChildtext">
                                            <%-- [<span ng-repeat="input in child_inputs">"{{input.id}}"</span>]--%>
                                            <a ng-click="addInput()" ng-href="" class="btn_s_type2">+직접입력 추가</a>
                                            <div ng-repeat="input in child_inputs" runat="server" on-finish-render="ngRepeatFinished_Child">
                                                <div class="mt5">
                                                    <input type="text" ng-model="input.id" class="input_type2 childinput" maxlength="15" style="width: 340px;" placeholder="후원어린이 번호 입력" runat="server" />
                                                    <a ng-click="removeInput($index)" class="btn_s_type2">삭제</a>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div><span class="guide_comment2" data-id="check_childmeetyn" style="display: none"></span></div>
                        <p class="s_con2">
                            <span class="fc_red">방문국가에 해당하는 후원어린이만</span> 만남 신청이 가능합니다.
                        </p>
                        <p class="s_con2">
                            후원어린이만남을 위한 추가비용(어린이/보호자/어린이센터 선생님의 <span class="fc_red">교통, 숙박,식사 비용 등)은 추후 개별 안내 드리며,</span> 후원자님께서 부담하시게 됩니다.
                        </p>
                        <p class="s_con2">
                            컴패션 규정에 따라 본인 또는 가족이 후원하는 어린이 만남만 가능합니다. 가족 후원자의 어린이를 만나실 경우 후원자 동의서와 가족관계증명서류를 첨부/제출하셔야 합니다(추후 첨부/제출 가능).
                            <a class="btn_s_type4" href="/files/visiontrip/doc/후원어린이만남 동의서_비전트립.pdf" target="_blank"><span class="ic_down"></span>후원자 동의서</a>
                        </p>
                    </div>
                </div>
                <div class="input_div divCashReceipt">
                    <div class="login_field">
                        <span>현금영수증<br />
                            발급</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 현금영수증 발급</caption>
                            <colgroup>
                                <%--<col style="width: 400px" />--%>
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <span class="sel_type2" style="width: 120px;">
                                            <label for="s_country" class="hidden">현금영수증 발급 선택</label>
                                            <asp:DropDownList runat="server" ID="ddlCashReceiptType" class="custom_sel" Width="120">
                                                <asp:ListItem Text="본인" Value="M" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="타인" Value="O"></asp:ListItem>
                                            </asp:DropDownList>
                                        </span><span class="guide_comment2" data-id="check_cashreceipttype" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="txtCashReceiptName" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="현금영수증 발급자 성함" />
                                        <span class="guide_comment2" data-id="check_cashreceiptname" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="txtCashReceiptTel" runat="server" maxlength="11" class="input_type2 number_only" value="" style="width: 400px;" placeholder="현금영수증 발급자 휴대번호(-없이 입력)" />
                                        <span class="guide_comment2" data-id="check_cashreceipttel" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="txtCashReceiptRelation" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;display:none;" placeholder="참자가와의 관계" />
                                        <span class="guide_comment2" data-id="check_cashreceiptrelation" style="display: none"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p class="s_con2">
                            왕복항공권에 한하여 현금영수증 발급이 가능하며, 현지화로 환전하여 사용하는 금액은 현금영수증 발급이 불가합니다.
                        </p>
                        <p class="s_con2">
                            현금영수증 발급자는 참가자 본인이 아니어도 가능하며 서류 제출 후 변경은 불가합니다.
                        </p>
                    </div>
                </div>
                <!-- 약관동의 -->
                <div class="input_div divAgreement">
                    <div class="login_field"><span>약관동의</span></div>
                    <div class="login_input">
                        <span class="guide_comment2 mb5" data-id="check_agree" style="display: none"></span>
                        <div class="box mb5" style="padding-bottom:20px;">
                            <table class="tbl_agreement">
                                <caption>신청서 정보입력 - 약관동의</caption>
                                <colgroup>
                                    <col style="width: 270px" />
                                    <col style="width: 120px" />
                                    <col style="width: /" />
                                </colgroup>
                                <tbody>
                                    <%--<tr>
                                        <td><span class="sp_agree1">비전트립 가이드자료</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(1)" ng-click="agreementModal($event, 1)">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree1">
                                                <asp:HiddenField ID="hdnAgree1" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree1" type="checkbox" runat="server" onclick="agree_checkbox(1)">
                                                <label class="css_label font1" for="chkAgree1"></label>
                                            </span>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td><span class="sp_agree1">어린이보호서약서</span>
                                        </td>
                                        <td>
                                            <a id="btnTerm1" class="btn_s_type4" onclick="checkAcceptterms(1)" ng-click="agreementModal.show($event, 1, '어린이보호서약서')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree1">
                                                <asp:HiddenField ID="hdnAgree1" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree1" type="checkbox" runat="server" onclick="agree_checkbox(1)">
                                                <label class="css_label font1" for="chkAgree1"></label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree2">개인정보의 수집 및 활용</span>
                                        </td>
                                        <td>
                                            <a id="btnTerm2" class="btn_s_type4" onclick="checkAcceptterms(2)" ng-click="agreementModal.show($event, 2, '개인정보의 수집 및 활용')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree2">
                                                <asp:HiddenField ID="hdnAgree2" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree2" type="checkbox" runat="server" onclick="agree_checkbox(2)">
                                                <label class="css_label font1" for="chkAgree2"></label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree8">고유식별정보 수집 및 이용</span>
                                        </td>
                                        <td>
                                            <a id="btnTerm8" class="btn_s_type4" onclick="checkAcceptterms(8)" ng-click="agreementModal.show($event, 8, '고유식별정보 수집 및 이용')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree8">
                                                <asp:HiddenField ID="hdnAgree8" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree8" type="checkbox" runat="server" onclick="agree_checkbox(8)">
                                                <label class="css_label font1" for="chkAgree8"></label>
                                            </span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><span class="sp_agree3">사진 및 영상자료 공유 </span>
                                        </td>
                                        <td><a id="btnTerm3" class="btn_s_type4" onclick="checkAcceptterms(3)" ng-click="agreementModal.show($event, 3, '사진 및 영상자료 공유')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree3">
                                                <asp:HiddenField ID="hdnAgree3" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree3" type="checkbox" runat="server" onclick="agree_checkbox(3)">
                                                <label class="css_label font1" for="chkAgree3"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree4">일정변경 또는 취소</span>
                                        </td>
                                        <td><a id="btnTerm4" class="btn_s_type4" onclick="checkAcceptterms(4)" ng-click="agreementModal.show($event, 4, '일정변경 또는 취소')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree4">
                                                <asp:HiddenField ID="hdnAgree4" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree4" type="checkbox" runat="server" onclick="agree_checkbox(4)">
                                                <label class="css_label font1" for="chkAgree4"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree5">환불규정</span>
                                        </td>
                                        <td><a id="btnTerm5" class="btn_s_type4" onclick="checkAcceptterms(5)" ng-click="agreementModal.show($event, 5, '환불규정')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree5">
                                                <asp:HiddenField ID="hdnAgree5" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree5" type="checkbox" runat="server" onclick="agree_checkbox(5)">
                                                <label class="css_label font1" for="chkAgree5"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree6">예방접종</span>
                                        </td>
                                        <td><a id="btnTerm6" class="btn_s_type4" onclick="checkAcceptterms(6)" ng-click="agreementModal.show($event, 6, '예방접종')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree6">
                                                <asp:HiddenField ID="hdnAgree6" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree6" type="checkbox" runat="server" onclick="agree_checkbox(6)">
                                                <label class="css_label font1" for="chkAgree6"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree7">기타사항</span>
                                        </td>
                                        <td><a id="btnTerm7" class="btn_s_type4" onclick="checkAcceptterms(7)" ng-click="agreementModal.show($event, 7, '기타사항')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree7">
                                                <asp:HiddenField ID="hdnAgree7" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree7" type="checkbox" runat="server" onclick="agree_checkbox(7)">
                                                <label class="css_label font1" for="chkAgree7"></label>
                                            </span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="box divAgreementConfirm">
                            <table class="tbl_agreement">
                                <caption>신청서 정보입력 - 약관동의</caption>
                                <colgroup>
                                    <col style="width: 380px" />
                                    <col style="width: /" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="confirm">
                                            <p>
                                                상기 본인은 컴패션 비전트립 모든 안내 및 동의사항을<br />
                                                숙지하고 이를 준수하고 따를 것을 동의하며
                                                <br />
                                                해당 비전트립 참가를 신청합니다.
                                            </p>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgreeFInal">
                                                <input class="css_checkbox agreecheck_change" id="chkAgreeFinal" type="checkbox" runat="server" ><label class="css_label font1" for="chkAgreeFinal"></label>
                                            </span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--// -->

                <div class="input_div">
                    <p class="s_con3 ml30 mb20">
                        여권 첨부가 어려우신 경우 아래 ‘제출하기’ 버튼을 클릭하여 작성된 신청서를 제출하여 주시고, 추후 여권사본을 첨부 또는 제출하여 주시기를 부탁 드립니다.
                    </p>
                    <div>
                        <div class="login_field no">
                            <label for="attach">여권사본첨부</label>
                        </div>
                        <div class="login_input">

                            <table class="tbl_join">
                                <caption>첨부파일 등록 테이블</caption>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="btn_attach clear2 relative">
                                                <input type="text" runat="server" data-id="path_btn_passportfile" value="" class="input_type2 fl mr10" style="width: 400px;" readonly="readonly" />
                                                <label for="lb_passport_path" class="hidden"></label>
                                                <input type="hidden" runat="server" id="path_btn_passportfile" value="" />
                                               <%-- <input type="hidden" runat="server" id="upload_root" value="" />
                                                <input type="hidden" id="hd_upload_root" runat="server" />
                                                <input type="hidden" id="hd_userpic_upload_root" runat="server" />--%>
                                                <a href="javascript:void(0);" class="btn_s_type2 fl" id="btn_passportfile"><span>파일선택</span></a>
                                                <a href="javascript:void(0);" class="btn_s_type2 fl ml5" id="btnPassportDel"><span>삭제</span></a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div>
                        <div class="login_field no">
                            <label for="attach">기타파일첨부</label>
                        </div>
                        <div class="login_input">
                            <table class="tbl_join">
                                <caption>첨부파일 등록 테이블</caption>
                                <tbody>
                                    <tr>
                                        <td>

                                            <%--<input type="text" runat="server" ng-model="input.files" data-id="etcfile_path" value="" class="input_type2 fl mr10 etcattach" style="width: 400px;" readonly="readonly" />
                                            
                                                <input type="hidden" runat="server" id="etcfile_path" value="" /> 
                                            <a href="javascript:void(0);" class="btn_type8 fl" id="btn_etcfile"><span>파일선택</span></a>--%>
                                            <a ng-click="addInputFiles()" ng-href="" class="btn_s_type2 mb5">+첨부파일추가</a>

                                            <%--[<span ng-repeat="input in input_fils">"{{input}}"</span>]--%>

                                        <div ng-repeat="input in input_fils" class="mb5" on-finish-render="ngRepeatFinished">
                                            <input type="text" runat="server" ng-model="input.filename" data-id="path_btn_etcfile{{input.key}}" value="" class="input_type2 fl mr10" style="width: 400px;" readonly="readonly" />
                                            <input type="hidden" id="path_btn_etcfile{{input.key}}" ng-model="input.filepath" value="" />

                                            <a href="javascript:void(0);" class="btn_s_type2 fl" id="btn_etcfile{{input.key}}"><span>파일선택</span></a>
                                            <a ng-click="removeInputFiles($index)" class="btn_s_type2 ml5">삭제</a>
                                        </div>





                                            <%-- <div class="btn_attach clear2 relative">
                                                <input type="text" runat="server" data-id="file_path" value="" class="input_type2 fl mr10 mb5" style="width: 400px;" readonly="readonly" />
                                                <a href="#" class="btn_type8 fl" id="btn_file_path" ><span>파일선택</span></a>
                                            <button ng-click='add()' class="btn_s_type2">Add</button>
                                            </div> 
                                            <div ng-repeat='item in items'>
                                                <input type="text" runat="server" ng-model='item.value' data-id="file_path" value="" class="input_type2 fl mb5" style="width: 400px;" readonly="readonly" />
                                                <a href="#" class="btn_type8 fl"  ng-model='item.files'  id="btn_file_path"><span>파일선택</span></a>
                                                 
                                                <button ng-click='del($index)' class="btn_s_type2">DEL {{$index}}</button>
                                            </div>
                                            {{items}}--%>




                                            <%--<div class="btn_attach clear2 relative">
                                                <input type="text" runat="server" data-id="file_path" value="" class="input_type2 fl mr10" style="width: 400px;" readonly="readonly" />
                                                <a href="#" class="btn_type8 fl" id="btn_file_path"><span>파일선택</span></a>
                                            </div>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p class="s_con2">여권만료일은 비전트립 후 한국 입국일 기준 6개월 이상, 여권 잔여 페이지는 최소 3페이지 이상 남아있어야 합니다. </p>
                            <p class="s_con2">외국인국적자, 만 15세 미만 미성년자, 가족 후원어린이만남의 경우 추가 서류를 첨부하여 주세요. </p>
                            <p class="s_con2">첨부가 어려우신 경우, 이메일(visiontrip@compassion.or.kr) 또는 팩스(02-3668-3501)로 제출해 주세요.</p>
                        </div>
                    </div>
                </div>


                <div class="input_div agreement ">
                    <p class="confirm">비전트립 가이드자료 및 약관을 잘 숙지하고 이해하였으며, 위 내용에 동의합니다.</p>

                    <div class="tac">
                        <%--<a class="btn_type1" id="btn_submit" runat="server" onserverclick="btn_submit_Click" href="javascript:__doPostBack('ctl00$ctl00$body$body$btn_submit','')">제출하기</a>--%>
                        <a class="btn_type1" id="btn_submit" ng-click="applysubmit()">제출하기</a>
                    </div>
                </div>

                <div class="contact mb10"><span>회원가입이 어려우시거나 본인확인/인증이 안되시는 경우 한국컴패션으로 연락 주시기 바랍니다. <em>후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</em></span></div>

            </div>


        </div> 
       
    </section>
</asp:Content>
