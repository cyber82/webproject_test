﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="apply_individual.aspx.cs" Inherits="participation_visiontrip_form_apply_individual" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>


<asp:Content ID="head_meta" ContentPlaceHolderID="head_meta" runat="Server">
</asp:Content>
<asp:Content ID="head_script" ContentPlaceHolderID="head_script" runat="Server">
    <%--<link data-require="bootstrap-css@2.3.2" data-semver="2.3.2" rel="stylesheet" href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />--%>

    <script type="text/javascript" src="/participation/visiontrip/form/apply_plan.js"></script>
    <script type="text/javascript" src="/participation/visiontrip/form/multiselect.js"></script>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script> 
    <script type="text/javascript" src="/assets/jquery/wisekit/validation/email.js"></script>
    <link href="/participation/visiontrip/form/template/visiontrip_form.css" rel="stylesheet" />  
    
    <script type="text/javascript">

        $(function () {
            $page.init_individual();
            setDatePicker($(".date")); 

            //방문일자는 8주 이내 선택 못함
            $('.visitdate').datepicker("option", "minDate", new Date(+new Date + 1000 * 60 * 60 * 24 * 56));
            $('.visitdate').datepicker("option", 'beforeShowDay', function (date) { return [date.getDay() != 6 && date.getDay() != 0, ""] });


            $('.fdate').datepicker("option", "minDate", new Date()); 

            //방문일자 datepicker 상단 타이틀 추가
            $('.visitdate').datepicker().bind('click keyup', function () {
                if ($('#ui-datepicker-div :last-child').is('table')) {
                    if ($('#ui-datepicker-div').find('.spDateComment').length == 0)
                        $('#ui-datepicker-div').prepend('<span class="spDateComment mb10 s_con5" style="font-size:12px;">후원어린이만남은 최소 방문 10주 전 신청이 가능합니다.</span>');
                }
            }); 

            // 국내 , 국외선택
            $("#addr_overseas_addr1").setHangulBan();
            $("#addr_overseas_addr2").setHangulBan();
            $(".rd_addr").change(function () {
                if ($("#addr_domestic").prop("checked")) {
                    $(".hide_overseas").show();
                    $("#pn_addr_domestic").show();
                    $("#pn_addr_overseas").hide();
                } else {
                    $(".hide_overseas").hide();
                    $("#pn_addr_domestic").hide();
                    $("#pn_addr_overseas").show();
                }
            });

            $("[data-id=check_addr]").hide();
            if ($("#addr_domestic").prop("checked")) {
                $(".hide_overseas").show();
                $("#pn_addr_domestic").show();
                $("#pn_addr_overseas").hide();
                $(".zipcode_area").show();
            } else {
                $(".hide_overseas").hide();
                $("#pn_addr_domestic").hide();
                $("#pn_addr_overseas").show();
                $(".zipcode_area").hide();
            }


            if ($("#addr_domestic").prop("checked")) {

                $("#pn_addr_domestic").show();
                $("#pn_addr_overseas").hide();

                $("#addr_domestic_zipcode").val($("#zipcode").val());
                $("#addr_domestic_addr1").val($("#addr1").val());
                $("#addr_domestic_addr2").val($("#addr2").val());

                // 컴파스의 데이타를 불러오는경우 
                if ($("#dspAddrDoro").val() != "") {
                    $("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
                    if ($("#dspAddrJibun").val() != "") {
                        $("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
                    }

                } else if ($("#addr1").val() != "") {

                    addr_array = $("#addr1").val().split("//");
                    $("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + addr_array[0] + " " + $("#addr2").val());
                    if (addr_array[1]) {
                        $("#addr_jibun").text("[지번주소] (" + $("#zipcode").val() + ") " + addr_array[1] + " " + $("#addr2").val());
                    }
                }

            } else {
                $("#pn_addr_domestic").hide();
                $("#pn_addr_overseas").show();

                $("#addr_overseas_zipcode").val($("#zipcode").val());
                $("#addr_overseas_addr1").val($("#addr1").val());
                $("#addr_overseas_addr2").val($("#addr2").val());
            }

            //

            //기독교일 경우에만 교회 입력
            $("#txtChurch").hide();
            $("#ddlReligion").change(function () {
                if ($("#ddlReligion option:selected").val() == "Christian") {
                    $("#txtChurch").show();
                } else {
                    $("#txtChurch").hide();
                }
            }); 
        }); 
    </script> 

</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="body" runat="Server">
    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
         
        <asp:HiddenField runat="server" ID="id" />
        <asp:HiddenField runat="server" ID="hdnApplyType" Value="Individual" />
        <asp:HiddenField runat="server" ID="childlist" /> 
        <asp:HiddenField runat="server" ID="hdnFileRoot" />  

        <div class="page_tit">
            <div class="titArea">
                <h1><em>개인방문 참가신청&#183;동의서</em></h1>
                <span class="desc">컴패션어린이들과 함께할 설레는 시간을 계획해 보세요</span>
                <uc:breadcrumb runat="server" />
            </div>
        </div>

        <div class="subContents member">
            <div class="w980 trip_sch">
                <div class="tab_info">
                    <span class="nec_info">표시는 필수입력 사항입니다.</span>
                </div>

                <div class="input_div">
                    <div class="login_field">
                        <span>방문유형</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>방문유형 선택</caption>
                            <colgroup>
                                <%--<col style="width: 400px" />--%>
                                <col style="width: /" />
                            </colgroup>
                            <tbody> 
                                <tr>
                                    <td>
                                        <div> <span class="sel_type2" style="width: 200px;">
                                                <label for="s_country" class="hidden">방문유형</label>
                                                <asp:DropDownList runat="server" ID="ddlVisitType" class="custom_sel" Width="200">
                                                    <asp:ListItem Text="--방문유형 선택--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="후원어린이 만남" Value="M"></asp:ListItem>
                                                    <asp:ListItem Text="어린이센터 방문" Value="V"></asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                        </div>
                                        <span class="guide_comment2" data-id="check_visittype" style="display: none"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p class="s_con2">
                           후원어린이가 없는 국가를 방문하기 원하실 경우, '어린이센터방문'을 선택해주세요.
                        </p>
                    </div>
                </div>

                <div class="input_div">
                    <div class="login_field">
                        <span>방문일정</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>방문유형 선택</caption>
                            <colgroup> 
                                <col style="width: /" />
                            </colgroup>
                            <tbody> 
                                <tr>
                                    <td> 
                                        <label for="txtVisitDate1" class="hidden">방문날짜(1지망)</label>
                                        <input type="text" id="txtVisitDate1" runat="server" class="input_type2 date visitdate" placeholder="방문날짜(1지망)" style="width: 173px" />
                                        <label for="txtVisitDate2" class="hidden">방문날짜(2지망)</label>
                                        <input type="text" id="txtVisitDate2" runat="server" class="input_type2 date visitdate" placeholder="방문날짜(2지망)" style="width: 173px" />
                                        <div>
                                            <span class="guide_comment2" data-id="check_visitdate1" style="display: none"></span>
                                        </div>
                                        <div>
                                            <span class="guide_comment2" data-id="check_visitdate2" style="display: none"></span>
                                        </div>
                                        <div>
                                            <p class="s_con2 mt5">
                                                위 일정 중 하루로 방문일이 최종 결정됩니다. 다만, 현지 사정에 따라 요청하신 일정에 방문이 불가 할 수 있습니다. 
                                            </p>
                                        </div>
                                    </td>
                                </tr> 
                                <tr>
                                    <td>
                                        <div>

                                            <span class="sel_type2" style="width: 173px;"> 
                                                <label for="ddlVisitCountry" class="hidden">방문국가</label>

                                                <asp:DropDownList runat="server" ID="ddlVisitCountry" class="custom_sel" Width="173" ng-model="country" ng-change="changeCountry(country)" 
                                                    ng-options="item.c_name for item in countries track by item.c_id">
                                                    <asp:ListItem Text="--방문국가 선택--" Value="" Selected="True"></asp:ListItem>
                                                    <%--item.c_id as item.c_name for item in countries
                                                        "item.c_name for item in countries track by item.c_id"
                                                        <asp:ListItem Text="{{item.c_name}}" Value="{{item.c_id}}" ng-repeat="item in countries"></asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </span>
                                        </div>
                                        <span class="guide_comment2" data-id="check_visitcountry" style="display: none"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="input_div divUserInfo">
                    <div class="login_field">
                        <span>참가자정보</span>
                    </div>
                    <div class="login_input ">
                        <table class="tbl_join">
                            <caption>신청서 정보입력</caption>
                            <colgroup>
                                <%--<col style="width: 400px" />--%>
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <div>
                                            <input type="text" id="txtName" runat="server" class="input_type2" value="" style="width: 150px;" readonly="readonly" disabled="disabled"/>
                                            <input type="text" id="txtNameEng" runat="server" maxlength="50" class="input_type2 english_only" value="" style="width: 150px;ime-mode:disabled" placeholder="영문명(여권과 동일)" />
                                            <input type="text" id="txtBirth" runat="server" maxlength="10" class="input_type2" value="" style="width: 150px;" readonly="readonly"  disabled="disabled"/>                                            
                                        </div>
                                        <span class="guide_comment2" data-id="check_engname" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="s_con9">해당 정보는 개인방문 관련 안내에 한하여 사용되며 웹 등록 정보와 불일치 하여도 무관합니다. </p>
                                        <input type="text" id="txtPhone" runat="server" maxlength="11" class="input_type2 number_only" value="" style="width: 400px;" placeholder="휴대번호(-없이 입력)" />
                                        <span class="guide_comment2" data-id="check_phone" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="txtEmail" runat="server" maxlength="50" class="input_type2" value="" style="width: 400px;" placeholder="이메일"/>
                                        <span class="guide_comment2" data-id="check_email" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            <input type="hidden" runat="server" id="locationType" />
                                            <input type="hidden" runat="server" id="hfAddressType" value="" />

                                            <input type="hidden" runat="server" id="zipcode" />
                                            <input type="hidden" id="addr1" runat="server" />
                                            <input type="hidden" id="addr2" runat="server" />
                                            <input type="hidden" runat="server" id="dspAddrJibun" value="" />
                                            <input type="hidden" runat="server" id="dspAddrDoro" value="" /> 


                                            <span class="radio_ui">
                                                <input type="radio" runat="server" class="rd_addr css_radio" id="addr_domestic" name="addr_location" />
                                                <label for="addr_domestic" class="css_label">국내</label>

                                                <input type="radio" runat="server" class="rd_addr css_radio" id="addr_oversea" name="addr_location" />
                                                <label for="addr_oversea" class="css_label ml20">해외</label>

                                            </span> 

                                            <span id="pn_addr_domestic" runat="server" style="display: none">
                                                <label for="zipcode" class="hidden">주소찾기</label>
                                                <a href="javascript:void(0)" class="btn_s_type2 ml5" runat="server" id="popup" ng-click="addrpopup($event)">주소찾기</a>
                                                <input type="hidden" id="addr_domestic_zipcode" />
                                                <input type="hidden" id="addr_domestic_addr1" />
                                                <input type="hidden" id="addr_domestic_addr2" />

                                                <p id="addr_road" class="mt15"></p>
                                                <p id="addr_jibun" class="mt10"></p>
                                            </span>

                                            <!-- 해외주소 체크 시 -->
                                            <div id="pn_addr_overseas" runat="server" style="display: none; width: 400px;" class="mt15">
                                                <span class="sel_type2 fl" style="width: 195px;">
                                                    <label for="ddlHouseCountry" class="hidden">국가 선택</label>
                                                    <asp:DropDownList runat="server" ID="ddlHouseCountry" class="custom_sel"></asp:DropDownList>

                                                </span>
                                                <label for="addr_overseas_zipcode" class="hidden">우편번호</label>

                                                <input type="text" id="addr_overseas_zipcode" class="input_type2 fr" value="" style="width: 195px" placeholder="우편번호" maxlength="150"/>
                                                <input type="text" id="addr_overseas_addr1" class="input_type2 mt10" placeholder="주소" style="width: 400px;"  maxlength="1000"/>
                                                <input type="text" id="addr_overseas_addr2" class="input_type2 mt10" placeholder="상세주소" style="width: 400px;"  maxlength="1000"/>
                                            </div>
                                            <!--// -->
                                        </div>
                                         

                                        <span class="guide_comment2" data-id="check_addr" style="display: none"></span>

                                        <div>
                                            <span class="s_con5">후원과 관련된 모든 안내/정보를 위 정보로 변경하기 원하십니까?</span>
                                            <span class="sel_type2" style="width: 100px;">
                                                <label for="s_country" class="hidden">기존정보변경</label>
                                                <asp:DropDownList runat="server" ID="ddlInfoChange" class="custom_sel" Width="100">
                                                    <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                        </div>
                                        <span class="guide_comment2" data-id="check_infochange" style="display: none"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                 
                <div class="input_div divReligion">
                    <div class="login_field">
                        <span>종교</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 종교</caption>
                            <colgroup>
                                <col style="width: 120px" />
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <div>
                                            <span class="sel_type2" style="width: 120px;">
                                                <label for="s_country" class="hidden">종교선택</label>
                                                <asp:DropDownList runat="server" ID="ddlReligion" class="custom_sel" Width="120" onchange="">
                                                    <asp:ListItem Text="--종교 선택--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="기독교" Value="Christian"></asp:ListItem>
                                                    <asp:ListItem Text="불교" Value="Buddhist"></asp:ListItem>
                                                    <asp:ListItem Text="천주교" Value="Catholic"></asp:ListItem>
                                                    <asp:ListItem Text="무교" Value="Atheism"></asp:ListItem>
                                                    <asp:ListItem Text="기타" Value="Etc"></asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" id="txtChurch" runat="server" maxlength="25" class="input_type2" value="" style="width: 400px;" placeholder="교회명" />

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <span class="guide_comment2" data-id="check_religion" style="display: none"></span>
                        <span class="guide_comment2" data-id="check_church" style="display: none"></span>
                    </div>
                </div>
                
                <div class="input_div divCompanion">
                    <div class="login_field">
                        <span>동반인
                            <br />
                            정보</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>개인방문 신청서 - 동반인정보</caption>
                            <colgroup> 
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td> 
                                        <a ng-click="addCompanionInput()" class="btn_s_type2">+동반인 추가</a> 
                                        <div ng-repeat="input in companion_inputs" runat="server" class="mt5" on-finish-render="ngRepeatFinished_Companion">
                                            <input type="text" ng-model="input.name_kor" class="input_type2 companion_name_kor" maxlength="25" style="width: 150px;" placeholder="한글성함" runat="server" />
                                            <input type="text" ng-model="input.name_eng" class="input_type2 companion_name_eng" maxlength="50"  style="width: 150px;" placeholder="영문성함" runat="server" />
                                            <input type="text" ng-model="input.birth" class="input_type2 companion_birth number_only" maxlength="8" style="width: 100px;" placeholder="생년월일" runat="server" />
                                            <span class="sel_type2" style="width: 100px;">
                                                <select class="custom_sel gender companion_gender"  ng-model="input.gender">
                                                    <option value="">성별</option>
                                                    <option value="M">남</option>
                                                    <option value="F">여</option> 
                                                </select>
                                            </span>
                                            <a ng-click="removeCompanionInput($index)" class="btn_s_type2 delCompanion">삭제</a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <span class="guide_comment2" data-id="check_companion" style="display: none"></span>
                        </div> 
                        <p class="s_con2">
                            후원자님을 포함하여 최대 8명까지 동행이 가능합니다.
                        </p>
                        <p class="s_con2">
                            방문에 동행하는 동반인의 경우 첨부된 서류를 작성하여 제출해 주셔야 합니다(추후 첨부/제출 가능).
                          <a href="/files/visiontrip/doc/개인방문(동반인)신청서.pdf" target="_blank" class="btn_s_type4"><span class="ic_down"></span>동반인 신청서</a>
                        </p>

                    </div>
                </div>
                
                <div class="input_div divChildMeet">
                    <div class="login_field">
                        <span>어린이정보</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 어린이정보</caption>
                            <colgroup>
                                <col style="width: 120px" />
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr> 
                                    <td>
                                        <span class="sel_type2" style="width: 300px;">
                                            <multiselect class="input-xlarge" multiple="true"
                                                ng-model="selectedChild"
                                                options="c.name for c in mychild"
                                                change="selected()"></multiselect>
                                            <div class="well well-small" style="display: none;">
                                                <asp:HiddenField runat="server" ID="hdnSelectedChild" Value="{{selectedChild}}" />
                                            </div>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Panel runat="server" ID="plChildtext">
                                            <%--  [<span ng-repeat="input in child_inputs">"{{input.id}}"</span>]--%>
                                            <a ng-click="addInput()" ng-href="" class="btn_s_type2">+직접입력 추가</a>
                                            <div ng-repeat="input in child_inputs" runat="server" on-finish-render="ngRepeatFinished_Child">
                                                <div class="mt5">
                                                    <input type="text" ng-model="input.id" class="input_type2 childinput" maxlength="15" style="width: 340px;" placeholder="후원어린이 번호 입력" runat="server" />
                                                    <a ng-click="removeInput($index)" class="btn_s_type2">삭제</a>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div><span class="guide_comment2" data-id="check_childlist" style="display: none"></span></div>
                        <p class="s_con2">
                            컴패션 규정에 따라 본인 또는 가족이 후원하는 어린이 만남만 가능합니다(가족 후원자의 어린이를 만나실 경우 후원자 동의서와 가족관계증명서류를 첨부/제출하셔야 합니다.).                            
                            <a href="/files/visiontrip/doc/후원어린이만남 동의서_개인방문.pdf" target="_blank" class="btn_s_type4"><span class="ic_down"></span>후원자 동의서</a>
                        </p>
                    </div>
                </div>
                <div class="input_div divLocal">
                    <div class="login_field">
                        <span>현지정보</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>개인방문 신청서 - 현지정보</caption>
                            <colgroup> 
                                <col style="width: /" />
                            </colgroup>
                            <tbody>  

                                <tr>
                                    <td>
                                        <input type="text" id="txtLocalAccommodation" runat="server" maxlength="100" class="input_type2" value="" style="width: 200px;" placeholder="현지 숙소명" />
                                        <input type="text" id="txtLocalTel" runat="server" maxlength="100" class="input_type2" value="" style="width: 195px;" placeholder="현지숙소 연락처" />
                                        <div>
                                            <span class="guide_comment2" data-id="check_localaccommodation" style="display: none"></span>
                                            <span class="guide_comment2" data-id="check_localtel" style="display: none"></span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="txtLocalAddress" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="현지숙소 주소" />

                                        <div>
                                            <span class="guide_comment2" data-id="check_localaddress" style="display: none"></span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>  
                        <p class="s_con2">
                            현지 숙소가 미정인 경우 '미정'으로 기재해 주세요.
                        </p>
                    </div>
                </div>
                
                 <div class="input_div divDate">
                    <div class="login_field">
                        <span>출입국 정보</span>
                    </div>
                    <div class="login_input">
                       <table class="tbl_join">
                            <caption>개인방문 신청서 - 출입국정보</caption>
                            <colgroup>  
                                <col style="width: /" />
                            </colgroup>
                           <tbody>
                               <tr>
                                   <td>
                                       <label for="txtDepartureDate" class="hidden">출국일</label>
                                       <input type="text" id="txtDepartureDate" runat="server" class="input_type2 date fdate" placeholder="출국일" style="width: 173px" />
                                       
                                       <label for="txtReturnDate" class="hidden">귀국일</label>
                                       <input type="text" id="txtReturnDate" runat="server" class="input_type2 date fdate" placeholder="귀국일" style="width: 173px" />
                                       
                                   </td>
                               </tr>
                           </tbody>
                        </table>
                        <span class="guide_comment2" data-id="check_departuredate" style="display: none"></span>
                        <span class="guide_comment2" data-id="check_returndate" style="display: none"></span>
                    </div>
                </div> 

                <div class="input_div divEmergencyContact">
                    <div class="login_field">
                        <span>비상연락처</span>
                    </div>
                    <div class="login_input">
                        <table class="tbl_join">
                            <caption>신청서 정보입력 - 비상연락처</caption>
                            <colgroup> 
                                <col style="width: /" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="text" id="txtEmergencyContactName" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="비상연락처 - 성함" />
                                        <span class="guide_comment2" data-id="check_emergencycontactname" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="txtEmergencyContactTel" runat="server" maxlength="11" class="input_type2 number_only" value="" style="width: 400px;" placeholder="비상연락처 - 휴대번호 (-없이 입력)" />
                                        <span class="guide_comment2" data-id="check_emergencycontacttel" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="txtEmergencyContactRelation" runat="server" maxlength="100" class="input_type2" value="" style="width: 400px;" placeholder="비상연락처 - 참가자와의 관계" />
                                        <span class="guide_comment2" data-id="check_emergencycontactrelation" style="display: none"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                 
                <!-- 약관동의 -->
                <div class="input_div divAgreement">
                    <div class="login_field"><span>약관동의</span></div>
                    <div class="login_input">
                        <span class="guide_comment2 mb5" data-id="check_agree" style="display: none"></span>
                        <div class="box mb5" style="height:318px;">
                            <table class="tbl_agreement">
                                <caption>신청서 정보입력 - 약관동의</caption>
                                <colgroup>
                                    <col style="width: 270px" />
                                    <col style="width: 120px" />
                                    <col style="width: /" />
                                </colgroup>
                                <tbody>
                                    <%--<tr>
                                        <td><span class="sp_agree1">개인방문 가이드자료</span>
                                        </td>
                                        <td><a href="/etc/terms" target="_blank" class="btn_s_type4" onclick="checkAcceptterms(1)">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree1">
                                                <asp:HiddenField ID="hdnAgree1" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree1" type="checkbox" runat="server" onclick="agree_checkbox(1)">
                                                <label class="css_label font1" for="chkAgree1"></label>
                                            </span>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td><span class="sp_agree1">어린이보호서약서</span>
                                        </td>
                                        <td><a id="btnTerm1" class="btn_s_type4" onclick="checkAcceptterms(1)" ng-click="agreementModal.show($event, 11, '어린이보호서약서')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree1">
                                                <asp:HiddenField ID="hdnAgree1" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree1" type="checkbox" runat="server" onclick="agree_checkbox(1)">
                                                <label class="css_label font1" for="chkAgree1"></label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree2">개인정보의 수집 및 활용</span>
                                        </td>
                                        <td><a id="btnTerm2" class="btn_s_type4" onclick="checkAcceptterms(2)" ng-click="agreementModal.show($event, 12, '개인정보의 수집 및 활용')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree2">
                                                <asp:HiddenField ID="hdnAgree2" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree2" type="checkbox" runat="server" onclick="agree_checkbox(2)">
                                                <label class="css_label font1" for="chkAgree2"></label>
                                            </span>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td><span class="sp_agree3">신청 및 방문 절차</span>
                                        </td>
                                        <td><a id="btnTerm3" class="btn_s_type4" onclick="checkAcceptterms(3)" ng-click="agreementModal.show($event, 13, '신청 및 방문 절차')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree3">
                                                <asp:HiddenField ID="hdnAgree3" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree3" type="checkbox" runat="server" onclick="agree_checkbox(3)">
                                                <label class="css_label font1" for="chkAgree3"></label>
                                            </span>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td><span class="sp_agree4">일정변경 또는 취소</span>
                                        </td>
                                        <td><a id="btnTerm4" class="btn_s_type4" onclick="checkAcceptterms(4)" ng-click="agreementModal.show($event, 14, '일정변경 또는 취소')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree4">
                                                <asp:HiddenField ID="hdnAgree4" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree4" type="checkbox" runat="server" onclick="agree_checkbox(4)">
                                                <label class="css_label font1" for="chkAgree4"></label>
                                            </span>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td><span class="sp_agree5">예방접종</span>
                                        </td>
                                        <td><a id="btnTerm5" class="btn_s_type4" onclick="checkAcceptterms(5)" ng-click="agreementModal.show($event, 15, '예방접종')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree5">
                                                <asp:HiddenField ID="hdnAgree5" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree5" type="checkbox" runat="server" onclick="agree_checkbox(5)">
                                                <label class="css_label font1" for="chkAgree5"></label>
                                            </span>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td><span class="sp_agree6">기타사항</span>
                                        </td>
                                        <td><a id="btnTerm6" class="btn_s_type4" onclick="checkAcceptterms(6)" ng-click="agreementModal.show($event, 16, '기타사항')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree6">
                                                <asp:HiddenField ID="hdnAgree6" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree6" type="checkbox" runat="server" onclick="agree_checkbox(6)">
                                                <label class="css_label font1" for="chkAgree6"></label>
                                            </span>
                                        </td>
                                    </tr> 
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>
                <!--// -->

                <div class="input_div">
                    <p class="s_con3 ml30 mb20">
                        첨부가 어려우신 경우 아래 ‘제출하기’ 버튼을 클릭하여 작성된 신청서를 제출하여 주시고, 추후 여권사본을 첨부 또는 제출하여 주시기를 부탁 드립니다. 
                    </p> 
                    <div>
                        <div class="login_field no">
                            <label for="attach">기타파일첨부</label>
                        </div>
                        <div class="login_input">
                            <table class="tbl_join">
                                <caption>기타 첨부파일 등록 테이블</caption>
                                <tbody>
                                    <tr>
                                        <td>

                                            <a ng-click="addInputFiles()" ng-href="" class="btn_s_type2 mb5">+첨부파일추가</a>

                                            <%--[<span ng-repeat="input in input_fils">"{{input}}"</span>]--%>

                                            <div ng-repeat="input in input_fils" class="mb5" on-finish-render="ngRepeatFinished">
                                                <input type="text" runat="server" ng-model="input.filename" data-id="path_btn_etcfile{{input.key}}" value="" class="input_type1 fl mr10" style="width: 400px;" readonly="readonly" />
                                                <input type="hidden" id="path_btn_etcfile{{input.key}}" ng-model="input.filepath" value="" />

                                                <a href="javascript:void(0);" class="btn_s_type2 fl" id="btn_etcfile{{input.key}}"><span>파일선택</span></a>
                                                <a ng-click="removeInputFiles($index)" class="btn_s_type2 ml5">삭제</a>
                                            </div>



                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p class="s_con2">가족의 후원어린이를 만나기 원하는 경우 관련서류를 첨부/제출 하셔야 합니다.</p>
                            <p class="s_con2">첨부가 어려우신 경우, 이메일(visiontrip@compassion.or.kr) 또는 팩스(02-3668-3501)로 제출해 주세요.</p>
                        </div>
                    </div>
                </div>
                 
                <div class="input_div agreement">
                    <p class="confirm">개인방문 가이드자료 및 약관을 잘 숙지하고 이해하였으며, 위 내용에 동의합니다.</p>

                    <div class="tac">
                        <a class="btn_type1" id="btn_submit" ng-click="applysubmit_individual()">제출하기</a>
                    </div>
                </div>

                <div class="contact mb10"><span>회원가입이 어려우시거나 본인확인/인증이 안되시는 경우 한국컴패션으로 연락 주시기 바랍니다. <em>후원지원팀 (02-740-1000 평일 9시~18시/공휴일제외) / info@compassion.or.kr</em></span></div>

            </div>


        </div> 
       
    </section>
</asp:Content>
