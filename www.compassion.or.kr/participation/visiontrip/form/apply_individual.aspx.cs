﻿using Microsoft.AspNet.FriendlyUrls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class participation_visiontrip_form_apply_individual : FrontBasePage
{
    const string listPath = "/participation/visiontrip/individual";

    private string strUserID = string.Empty;
    private string strSponserID = string.Empty;
    private string strApplyID = string.Empty;

    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }
    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        if (!UserInfo.IsLogin)
        {
            Response.ClearContent();
            return;
        }

        UserInfo sess = new UserInfo();
        strUserID = sess.UserId;
        strSponserID = sess.SponsorID;

        //// 이름
        //txtName.Value = sess.UserName; 
        //txtBirth.Value = sess.Birth != "" ? sess.Birth.Substring(0,10): "";
        //txtEmail.Value = sess.Email;
        ////txtPhone.Value = sess.Phone;

        hdnFileRoot.Value = Uploader.GetRoot(Uploader.FileGroup.file_visiontrip) + "individual/" + strUserID + "/";

        // check param
        //var requests = Request.GetFriendlyUrlSegments();
        //if (requests.Count < 1)
        //{
        //    Response.Redirect(listPath, true);
        //}
        //var isValid = new RequestValidator()
        //    .Add("p", RequestValidator.Type.Numeric)
        //    .Validate(this.Context, listPath);

        //if (!requests[0].CheckNumeric())
        //{
        //    Response.Redirect(listPath, true);
        //}

        ////parameter
        //base.PrimaryKey = requests[0];
        //id.Value = PrimaryKey.ToString();
        //strApplyID = id.Value;

        //GetVisionTripSchedule();
        /*
        //주소
        locationType.Value = sess.LocationType;
        if (sess.LocationType == "국내")
        {
            var addr_result = new SponsorAction().GetAddress();
            if (!addr_result.success)
            {
                base.AlertWithJavascript(addr_result.message, "goBack()");
                return;
            }

            SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;
            hfAddressType.Value = addr_data.AddressType;
            zipcode.Value = addr_data.Zipcode;
            addr1.Value = addr_data.Addr1;
            addr2.Value = addr_data.Addr2;
            dspAddrDoro.Value = addr_data.DspAddrDoro;
            dspAddrJibun.Value = addr_data.DspAddrJibun;

            var comm_result = new SponsorAction().GetCommunications();
            if (!comm_result.success)
            {
                base.AlertWithJavascript(comm_result.message, "goBack()");
                return;
            }

            SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
            txtPhone.Value = comm_data.Mobile.TranslatePhoneNumber();
        } // end 주소
        */

        //UserInfo sess = new UserInfo();
        //strUserID = sess.UserId;
        //strSponserID = sess.SponsorID;

        // 이름
        txtName.Value = sess.UserName;
        // 생년월일
        txtBirth.Value = sess.Birth != "" ? sess.Birth.Substring(0, 10) : "";

        #region tSponsorMaster 정보
        string strAddr1_overseas = string.Empty;
        // WEB tSponsorMaster
        using (AuthLibDataContext daoAuth = new AuthLibDataContext())
        {
            //var entity = daoAuth.tSponsorMaster.First(p => p.UserID == sess.UserId);
            var entity = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);

            locationType.Value = entity.LocationType;
            strAddr1_overseas = entity.Addr1.Decrypt();

            txtEmail.Value = entity.Email.Decrypt();
            txtPhone.Value = entity.Phone.Decrypt().TranslatePhoneNumber().Replace("-","");
        }
        #endregion

        addr_domestic.Checked = locationType.Value == "국내";
        addr_oversea.Checked = locationType.Value != "국내";



        //var str_addr1 = user.Addr1.EmptyIfNull().Split('$')[0];
        //var str_location = user.Addr1.EmptyIfNull().Split('$').Length > 1 ? user.Addr1.EmptyIfNull().Split('$')[1] : "한국";


        var addr_result = new SponsorAction().GetAddress();
        if (!addr_result.success)
        {
            base.AlertWithJavascript(addr_result.message, "goBack()");
            return;
        }

        SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;

        string doroAddr = addr_data.DspAddrDoro != "" ? addr_data.DspAddrDoro.Replace(addr_data.Addr2, "") : "";
        string jibunAddr = addr_data.DspAddrJibun != "" ? addr_data.DspAddrJibun.Replace(addr_data.Addr2, "") : "";

        hfAddressType.Value = addr_data.AddressType;
        zipcode.Value = addr_data.Zipcode;
        addr1.Value = locationType.Value == "국내" ? doroAddr + "//" + jibunAddr : addr_data.Addr1;//Addr1;
        addr2.Value = addr_data.Addr2;
        dspAddrDoro.Value = addr_data.DspAddrDoro;
        dspAddrJibun.Value = addr_data.DspAddrJibun;

        var comm_result = new SponsorAction().GetCommunications();
        if (!comm_result.success)
        {
            base.AlertWithJavascript(comm_result.message, "goBack()");
            return;
        }

        SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
        txtPhone.Value = comm_data.Mobile.Replace("-", "");//.TranslatePhoneNumber();

        #region 국외인경우 국가목록 
        var actionResult = new CodeAction().Countries();
        if (!actionResult.success)
        {
            base.AlertWithJavascript(actionResult.message);
        }

        ddlHouseCountry.DataSource = actionResult.data;
        ddlHouseCountry.DataTextField = "CodeName";
        ddlHouseCountry.DataValueField = "CodeName";
        ddlHouseCountry.DataBind();
        #endregion

        if (locationType.Value != "국내")
        {
            var str_location = strAddr1_overseas.EmptyIfNull().Split('$').Length > 1 ? strAddr1_overseas.EmptyIfNull().Split('$')[1] : "한국";
            ddlHouseCountry.SelectedValue = str_location;
        }
        // end 주소

    }

}