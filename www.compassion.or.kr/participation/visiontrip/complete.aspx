﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete.aspx.cs" Inherits="participation_visiontrip_complete" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script"> 
    <style>
        em {font-family:noto_m;color:#005dab;}
    </style>
    <%--<script type="text/javascript" src="/participation/visiontrip/complete.js?v=1.2"></script>--%>
    <script type="text/javascript">
        function fnMyvisiontrip() {
            if (!common.checkLogin()) {
                return;
            }
            else {
                location.href = "/my/activity/visiontrip/";
            }
        }
    </script> 
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body"> 
    <section class="sub_body" >
        <asp:HiddenField runat="server" ID="hdnApplyID"/> 
        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em><asp:Label runat="server" ID="lbTitle"></asp:Label> 신청완료</em></h1>
                <span class="desc">컴패션어린이들과 함께할 설레는 시간을 계획해 보세요</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents part advocate">

            <div class="w980 ">
                <div class="aday tac">
                    <div class="aday_conb w980" style="padding: 80px; border: 1px solid lightgray; border-image: none;">
                        <div id="divPlanTrip"  runat="server" visible="false">
                            <p class="sub_tit mb10">
                                <asp:Label runat="server" ID="lbSchedule" CssClass="subject sub_tit"></asp:Label>
                                <asp:Label runat="server" ID="lbTripCountry" CssClass="sub_tit"></asp:Label> 비전트립을
                            </p>
                            <p class="sub_tit">신청해 주셔서 감사합니다.</p>

                            <div class="h40"></div>
                            <div id="divAttachCheck" runat="server" visible="false">
                                <p class="s_con4"><em>&#183;여권사본 제출, 신청비를 납부하셔야 신청이 완료됩니다.</em></p>
                                <p class="s_con4"><em>&#39;나의 현지방문 보러가기&#39;로 이동 후 진행이 가능합니다. </em></p>
                            </div>
                            <div id="divPayCheck" runat="server" visible="false">
                                <p class="s_con4"><em>&#183;신청비를 납부하셔야 신청이 완료됩니다. </em></p>
                                <p class="s_con4"><em>&#39;나의 현지방문 보러가기&#39;로 이동 후 납부가 가능합니다. </em></p>
                            </div>
                        </div>

                        <div id="divIndividualTrip" runat="server" visible="false">
                            <p class="sub_tit mb10">후원어린이 개인방문을 신청해 주셔서 감사합니다. </p>
                            <p class="sub_tit mb10">요청하신 방문일정과 관련하여 현지 확인 후 회신 드리겠습니다.</p> 
                            
                            <div class="h40"></div>
                            <div id="divMoveCheck" runat="server" visible="false"> 
                                <p class="s_con4"><em>&#39;나의 현지방문 보러가기&#39;로 이동 후 신청현황을 확인하실 수 있습니다.</em></p>
                            </div>
                            <div id="divCompanionCheck" runat="server" visible="false">
                                <p class="s_con4"><em>&#183;동반인 신청서류를 제출해 주셔야 신청 완료됩니다. </em></p>
                                <p class="s_con4"><em>&#39;나의 현지방문 보러가기&#39;로 이동 후 진행이 가능합니다.</em></p>
                            </div>
                        </div>

                        <%--<asp:label runat="server" CssClass="s_con3" id="lbMyVisionTrip">‘나의 현지방문 보러가기로 이동 후 진행이 가능합니다.</asp:label>--%>

                        <%--<p class="s_con3">CAD 추천 서비스는 컴패션 모바일 홈페이지를 통해 이용하실 수 있습니다.</p>
                        <div class="shortcut">
                            <span class="s_con3">추천 후에는 내가 추천한 어린이의 결연 현황을 <em>마이컴패션</em>에서 확인하실 수 있습니다.</span>
                            <a class="btn_s_type2 ml10" href="/my/activity/cad">CAD 추천 현황 보기</a>
                        </div>--%>
                    </div>
                </div>
                 
                        <div class="h40"></div>
                        <div style="width:180px;margin:0px auto;padding-top:10px;">
                            <a href="javascript:void(0);" onclick="fnMyvisiontrip();" class="btn_s_type1 ml10">나의 현지방문 보러가기</a>
                        </div> 
                <%--<div class="contact_qna div3"> 
					<div class="clear2 con1" style="font-size:20px;font-weight:bold;text-align:center;padding:20px;">
						  감사합니다.<br />
                        <asp:Label runat="server" ID="lbSchedule" CssClass="subject"></asp:Label>  
                        <asp:Label runat="server" ID="lbTripCountry"></asp:Label> 신청서 제출이 완료되었습니다.
                     </div>
				</div>--%> 
                 
                 
            </div>
            <div class="h100"></div>

        </div>
        <!--// e: sub contents --> 
    </section>



</asp:Content>
