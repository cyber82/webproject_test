﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using Microsoft.AspNet.FriendlyUrls;

public partial class participation_visiontrip_schedule : FrontBasePage {

    public string year;
    const string listPath = "/participation/visiontrip/schedule/"; 
    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();


        // 초기 날자 선택
        var selectedYear = StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "trip" && p.cd_key == "year").OrderBy(p => p.cd_order).ToList();
        if (selectedYear != null && selectedYear.Count > 0)
            year = selectedYear[0].cd_value.ToString();
        else
            year = DateTime.Now.Year.ToString();


        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            hdnBirthCheck.Value = sess.Birth;
            hdnSponsorID.Value = sess.SponsorID;
        }

        // check param
        var requests = Request.GetFriendlyUrlSegments();
        //if (requests.Count < 1)
        //{
        //    Response.Redirect(listPath, true);
        //}
        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);
        if (requests.Count > 1)
        {
            //if (!requests[0].CheckNumeric())
            //{
            //    Response.Redirect(listPath, true);
            //}

            //parameter
            base.PrimaryKey = requests[0];
            hdnRequestKey.Value = PrimaryKey.ToString();
            hdnApplyType.Value = requests[1];
        }



        /*
               base.OnBeforePostBack();
               base.LoadComplete += new EventHandler(list_LoadComplete);

               // 올해
               var year = Request["year"].ValueIfNull(DateTime.Now.ToString("yyyy"));
               current_year.Text = year;
               search_year.Value = year;


               // 작년
               var last = (Convert.ToInt32(year) - 1).ToString();
               if (CheckLastYear(last)) {
                   //last_year.Visible = true;
                   last_year.InnerText = last + "년";
                   //last_year.HRef = "/participation/visiontrip/?year=" + last;
               } else {
                   //last_year.Visible = false;
               }



               // 내년
               var next = (Convert.ToInt32(year) + 1).ToString();
               if (CheckNextYear(next)) {
                   //next_year.Visible = true;
                   next_year.InnerText = next + "년";
                   //next_year.HRef = "/participation/visiontrip/?year=" + next;
               } else {
                   //next_year.Visible = false;
               }


           }


           protected override void OnAfterPostBack() {
               base.OnAfterPostBack();
           }

           protected bool CheckNextYear(string next) {
               bool result = false;
               using (FrontDataContext dao = new FrontDataContext()) {
                   if(dao.visiontrip.Count(p => p.v_type == "vision" && p.open_date.Substring(0, 4) == next) > 0) {
                       result = true;
                   }
               }
               return result;
           }


           protected bool CheckLastYear(string last) {
               bool result = false;
               using (FrontDataContext dao = new FrontDataContext()) {
                   if (dao.visiontrip.Count(p => p.v_type == "vision" && p.open_date.Substring(0, 4) == last) > 0) {
                       result = true;
                   }
               }
               return result;
           */
    }

}