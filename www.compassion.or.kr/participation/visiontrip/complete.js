﻿(function () {

    var app = angular.module('cps.page', []);

    //URL Parameter 사용하기 위해서 
    //app.config(['$locationProvider', function ($locationProvider) {
    //    $locationProvider.html5Mode(true);
    //}]);

    app.controller("defaultCtrl", function ($scope, $http, popup) {
        //요청 트립 URL 관련
        //var searchObject = $location.search(); 
        $scope.visiontripType = $("#hdnApplyType").val() != "" && $("#hdnRequestKey").val() != ""
                                && $("#hdnApplyType").val() == "Request" ? "Request" : "Plan";

        $scope.requestKey = $("#hdnApplyType").val() != "" && $("#hdnRequestKey").val() != ""
                                && $("#hdnApplyType").val() == "Request" ? $("#hdnRequestKey").val() : "";

        $scope.requestview = $("#hdnApplyType").val() != "" && $("#hdnRequestKey").val() != ""
                                && $("#hdnApplyType").val() == "Request" ? true : false;
         
        $scope.userId = common.getUserId();

        $scope.requesting = false;
        $scope.page = 1;
        $scope.rowsPerPage = 100;
        //$scope.curYear = new Date().getFullYear();
        if (selectedYear == null || selectedYear == undefined || selectedYear == "")
            $scope.curYear = new Date().getFullYear();
        else
            $scope.curYear = parseInt(selectedYear);

        if (cookie.get("visiontripYear") != null) {
            $scope.curYear = parseInt(cookie.get("visiontripYear"));
            cookie.set("visiontripYear", $scope.curYear, -1);
        }

        $scope.isLogin = common.isLogin();

        $scope.params = {
            page: $scope.page,
            rowsPerPage: $scope.rowsPerPage,
            year: $scope.curYear
        };

        // list
        $scope.getList = function (params) {
            $http.get("/api/visiontrip.ashx?t=schedulelist", { params: { year: $scope.params.year, type: $scope.visiontripType, requestkey: $scope.requestKey, userid: $scope.userId } }).success(function (result) {
                $scope.list = [];
                $.each(result.data, function () {
                    // if (this.VisionTripType == "Plan") {
                    this.opendate = new Date(this.opendate.replace(/-/g, '/'));
                    this.closedate = new Date(this.closedate.replace(/-/g, '/'));

                    if (this.tripreview != null) {
                        if (this.tripreview.length == 7 && this.tripreview.indexOf("http://") == 0) {
                            this.tripreview = "";
                        }
                        //console.log(this.trip_review.length, this.trip_review.indexOf("http://"))
                    }

                    // 2017.06.07 김태형 : 상태(Y:신청중, S:예정, N:마감, E:종료)
                    this.tripnotice_view = true;
                    this.tripnoticestatus = "";
                    switch (this.tripstate) {
                        case 'Apply': this.status = 'ing'; this.result = '신청중'; break;
                        case 'Expect': this.status = 'expected'; this.result = '예정'; this.tripnotice_view = false; break;
                        case 'Deadline': this.status = 'end'; this.result = '마감'; break;
                        case 'Close': this.status = 'end'; this.result = '종료'; this.tripnoticestatus = "a-disabled"; break;
                        default: this.status = ''; this.result = ''; break;
                    }


                    //신청하기 버튼 apply_count maxcount
                    this.applyresult = '신청하기';
                    this.applyCountCheck = '';
                    if (this.apply_count == this.maxcount) {
                        this.applyCountCheck = 'a-disabled';
                        this.applyresult = '신청마감';
                    }

                    this.applystatus = 'end a-disabled';
                    //if (common.isLogin()) {
                        switch (this.tripstate) {
                           
                            case 'Apply': //신청중
                                this.applystatus = (this.apply_state == "Apply" && common.isLogin() ? 'apply a-disabled ' : 'ing ' ) + this.applyCountCheck;
                                this.applyresult = (this.apply_state == "Apply" && common.isLogin() ?  '신청완료' : this.applyresult);
                                break;
                               
                            case 'Expect': //예정
                                this.applystatus = 'expected a-disabled'; this.applyresult = '신청하기'; break;
                              
                            case 'Deadline':  //마감
                                this.applystatus = 'end a-disabled'; this.applyresult = '신청하기'; break;
                               
                            case 'Close': //종료
                                this.applystatus = 'end a-disabled'; this.applyresult = '신청하기'; break;
                            default: this.applystatus = ''; this.applyresult = ''; break;
                        }
                   // }

                    this.answerClass = "expected"
                    $scope.list.push(this)
                    //  }
                })
                //console.log($scope.list)
            });

        }

        $scope.lastYear = function () {
            $scope.params.year = parseInt($scope.params.year) - 1;
            $scope.params.page = 1;

            $("#next_year").show()
            $scope.hideLastYear();
            $scope.getList();

        }

        $scope.nextYear = function () {
            $scope.params.year = parseInt($scope.params.year) + 1;
            $scope.params.page = 1;

            $("#last_year").show()
            $scope.hideNextYear();
            $scope.getList();

        }

        $scope.setYear = function (obj) {
            //cookie.set("visiontripYear", $scope.params.year, 1000000);
            obj.item.trip_review += "?year=" + $scope.params.year;
        }


        $scope.hideLastYear = function () {
            $("#last_year").hide();
            $http.get("/api/visiontrip.ashx?t=schedulelist", { params: { year: $scope.params.year - 1, type: "" } }).success(function (result) {
                var length = result.data.length;
                if (length != 0) {
                    $("#last_year").show();

                }
            })
        }

        $scope.hideNextYear = function () {
            $("#next_year").hide();
            $http.get("/api/visiontrip.ashx?t=schedulelist", { params: { year: $scope.params.year + 1, type: "" } }).success(function (result) {
                var length = result.data.length;
                if (length != 0) {
                    $("#next_year").show();
                }
            })
        }

        $scope.loginCheck = function () {
            if (!common.checkLogin()) {
                return true;
            }
            else {
                return false;
            }
        }
        $scope.myVisionTripClick = function () {
            if (!common.checkLogin()) {
                return;
            }
            else {
                location.href = "/my/activity/visiontrip/";
            }
        }

        $scope.applyCheck = function (item) {
            if (!common.checkLogin()) {
                return;
            }
            else {//세션 존재하면

                var hdnBirth = $("#hdnBirthCheck").val();
                if (hdnBirth != "") {
                    var birthday = new Date(hdnBirth.substring(0, 10));
                    var today = new Date(item.startdate);

                    var years = today.getFullYear() - birthday.getFullYear();

                    console.log('계산 된 나이 : ' + years);
                    birthday.setFullYear(today.getFullYear());

                    if (today < birthday) years--;
                    console.log('계산 된 만 나이 : ' + years);

                    if (years < 12) {
                        alert("만 12세 미만은 신청할 수 없습니다.");
                        return;
                    }
                    else if (years < 19 && years >= 12) {
                        alert("만 12세 미만은 신청할 수 없습니다.");
                        if (confirm()) {
                            location.href = "/participation/visiontrip/form/apply_plan/" + item.scheduleid + "?";
                        }
                    }
                    else if (years >= 19) {
                        location.href = "/participation/visiontrip/form/apply_plan/" + item.scheduleid + "?";
                    }
                    else {
                        return;
                    }
                }
            }

        }


        //트립안내 popup
        $scope.tripnoticeModal = {
            instance: null,
            banks: [],
            detail_list: [],

            init: function () {
                popup.init($scope, "/participation/visiontrip/tripnotice", function (tripnoticeModal) {
                    $scope.tripnoticeModal.instance = tripnoticeModal;
                    //$scope.tripnoticeModal.show();
                }, { top: 0, iscroll: true });
            },

            show: function ($event, tripnotice) {
                //console.log(orderNo)
                $event.preventDefault();
                if (!$scope.tripnoticeModal.instance)
                    return;

                $scope.tripnoticeModal.instance.show();

                // 상세 주문 내역 
                $scope.tripnoticeModal.tripnotice = tripnotice;
            },
            close: function ($event) {
                $event.preventDefault();
                if (!$scope.tripnoticeModal.instance)
                    return;
                $scope.tripnoticeModal.instance.hide();
            }
        };

        $scope.tripnoticeModal.init();





        $scope.hideNextYear();
        $scope.hideLastYear();
        $scope.getList();

    });

    // 신청마감일(올해가 아닌경우 년도 표시)
    app.filter('lastYear', function ($filter) {
        return function (close) {
            if (close == 'Invalid Date') return '-';
            var result = $filter('date')(close, "M/d", "ko")
            if (new Date(close).getFullYear() < new Date().getFullYear()) {
                result = $filter('date')(close, "M/d(yyyy)", "ko")
            }
            return result;
        };
    });

})();
