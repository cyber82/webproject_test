﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="individual.aspx.cs" Inherits="participation_visiontrip_individual" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
        
//(function () {
     
//    var app = angular.module('cps.page', []);

//    app.controller('defaultCtrl', function ($scope, $http, popup, $location, paramService) {

//        $scope.applyIndividual = function () {
//            if (!common.checkLogin()) {
//                return;
//            }
//            else {
//                location.href = "/participation/visiontrip/form/apply_individual"
//            }
//        }

//    });
//});
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body"> 
    
    <section class="sub_body">
        
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션개인방문</em></h1>
				<span class="desc">개인적으로 어린이를 만날 수 있는 방법을 소개합니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 part">

			<div class="visual_visitor mb60"></div>

				<div class="w980 visitor">

					<!-- 탭메뉴 -->
					<ul class="tab_type1 trip_visitor font15">
						<li style="width:33%" class="on"><a href="#">개인방문 소개</a></li>
						<li style="width:33%"><a href="#">신청 절차</a></li>
						<li style="width:34%"><a href="javascript:void(0);">신청하기</a></li>
					</ul>
					<!--//-->
                     
					<script type="text/javascript">
						$(function () {
							$(".tab_type1.trip_visitor > li").click(function () {
								var idx = $(this).index() + 1;
								$(".tab_type1.trip_visitor > li").removeClass("on");
								$(this).addClass("on");
								$(".tc").hide();
								$(".tc" + idx).show();

								if (idx == 2) {
									$(".banner").show();
								}
								else if (idx == 3) {
								    if (!common.isLogin()) {
								        if (confirm("로그인이 필요합니다. \n로그인 페이지로 이동하시겠습니까?")) {
								            location.href = "/login";
								        }
								        return false;
								    } else { location.href = "/participation/visiontrip/form/apply_individual" }
                                     
								}
								else {
									$(".banner").hide();
								}

								return false;
							});
						})
					</script>

					<!-- 탭메뉴 컨텐츠 -->
					<div class="tab_contents">

						<!-- 개인방문 소개 -->
						<div class="tc tc1">
							<div class="conWrap1">
								<p class="s_con3">후원어린이 혹은 컴패션어린이센터를 개별적으로 방문하고 싶으신가요? 그렇다면 개인 방문을 고려해 보세요.<br />
								방문을 원하시는 시기에 맞춰 10주 전에 신청해주시면, 한국컴패션에서 수혜국 사무실을 통하여 일정을 조율해 드리며,<br />
								수혜국 도착 후에는 현지 직원이 방문을 도와 드립니다.</p>
								<div class="greybox">
									<span class="s_con1">개인방문은 국제 항공권 및 현지 숙소를 방문자께서 직접 예약하시고, 현지에서 발생한 식사비, 교통비, 통역비 등의 비용은 현지에서 납부하셔야  합니다.<br />
									방문 시 현지 직원은 영어로 방문을 도와 드리며, 한국어 통역은 불가합니다. 후원어린이 만남에는 후원어린이와  성인 2명(후원어린이의 보호자 중 한 명과 컴패션어린이센터<br />
									선생님)이 동행하며, 방문 시간은 최대 6시간입니다. 방문 시 동행인은 신청자를 포함하여 최대 8명까지 가능합니다.</span>
								</div>
							</div>
							<div class="conWrap2 clear2">
								<p class="conTit">개인방문 종류</p>
								<div class="con1">
									<p class="subject">후원어린이 방문</p>
									<div class="content">
										<div class="tit">1) 후원어린이가 사는 지역을 방문합니다</div>
										후원하고 있는 어린이의 가정과  컴패션어린이센터를 방문합니다.
										<div class="tit mt20">2) 후원어린이를 후원자님이 계신 곳으로 초청합니다</div>
										후원하고 있는 어린이의 국가를 방문했지만 후원어린이가 살고 있는 지역으로의<br />
										방문이 어려운 경우, 후원자님이 체류하고 계신 지역으로 어린이를 초청합니다.
									</div>
								</div>
								<div class="con2">
									<p class="subject">컴패션어린이센터 방문</p>
									<div class="content">
										<div class="tit">컴패션어린이센터 방문</div>
										컴패션 사역 현장을 경험하고자 하는 후원자님 및 비후원자님 모두에게 열려<br />
										있으며, 컴패션어린이센터 활동을 경험합니다.
									</div>
								</div>
							</div>
							<div class="conWrap3">
								<p class="conTit">개인방문 프로그램 예시</p>
								<ul class="ex_box clear2">
									<li>
										<div class="exTit bg1">
											<span class="txt">STEP 01<br /><span class="txt2">오전</span></span>
										</div>
										<div class="exCon">
											후원자님 숙소에서 현지 직원이 픽업<br />
											컴패션어린이센터 방문<br />
											후원어린이 가정 방문
										</div>
									</li>
									<li>
										<div class="exTit bg2">
											<span class="txt">STEP 02<br /><span class="txt2">점심 식사</span></span>
										</div>
										<div class="exCon">
											근처 쇼핑몰 또는<br />
											레스토랑에서 식사
										</div>
									</li>
									<li>
										<div class="exTit bg3">
											<span class="txt">STEP 03<br /><span class="txt2">오후</span></span>
										</div>
										<div class="exCon">
											후원어린이와 교제<br />
											(근처 놀이공원, 동물원, 쇼핑 등)<br />
											방문 종료
										</div>
									</li>
								</ul>
							</div>
						</div>
						<!--// 개인방문 소개 -->

						<!-- 신청 절차 -->
						<div class="tc tc2 clear2" style="display:none">
							<p class="s_tit6 notopline">1. 신청서류 접수 (방문 10주 전)</p>
							<p class="s_con3">
								현지의 비전트립 / 개인방문 일정 확인 및 방문일 조정을 위하여 방문 10주 전 신청해 주시기 바랍니다.<br />
								신청서 작성 시 방문일과 현지에서 머무실 숙소명을 기재해 주셔야 정확한 일정 및 예상비용 확인이 가능합니다.<br /><br />
								<%--<span class="tit">신청방법</span>
								아래 서류(개인방문 신청서 및 동의서/어린이보호서약서)를 원하시는 방문 10주 전까지 메일이나 팩스로 제출해 주세요.<br />
								<em>이메일 </em><a href="mailto:visiontrip@compassion.or.kr">visiontrip@compassion.or.kr</a>&nbsp;&nbsp;<em>팩스 </em>02-3668-3501--%>
							</p>
							<%--<div class="greybox">
								<p class="tit">후원어린이 개인방문 및 컴패션어린이센터 방문 신청서 및 동의서 / 어린이 보호 서약서</p>
								<ul>
									<li><span class="s_con1">모든 서류는 반드시 후원자님의 <em>자필 서명</em>이 있어야 접수가 가능합니다.</span></li>
									<li><span class="s_con1">어린이 방문 시 동행하시는 분이 계시다면, <em>동행하시는 분 모두 위 서류를 동일하게 제출</em>해 주셔야 합니다. (방문은 최대 8명까지 동행이 가능합니다.)</span></li>
								</ul>
							</div>
							
							<div class="clear2">
								<a href="/common/download/personalVisit_151006.zip" class="btn_s_type4 fr">신청서 및 가이드라인 다운로드</a>
							</div>--%>

							<p class="s_tit6">2. 항공편&middot;숙소 예약, 비자, 예방접종, 여행자보험 등 개별 진행</p>
							<p class="s_con3">국제 항공권 및 현지 숙소를 직접 예약해 주시기 바라며,	숙소 예약 시 후원어린이가 등록된 컴패션어린이센터와 가장 가까운 곳의 숙소를 정하시는 것을 권해 드립니다.<br />
							방문국가의 비자 발급 여부를 확인하시고, 예방접종이 필요한 국가의 경우 의사와 상의하시고 접종을 완료하시기 바랍니다.<br />
							안전한 여행을 위해 출발 전 여행자 보험에 가입하시는 것을 권장 드립니다.</p>

							<p class="s_tit6">3. 일정 및 비용 확인</p>
							<p class="s_con3">신청서 접수 후, 현지를 통하여 방문 일정 및 예상비용을 확인하여 후원자님께 안내 드립니다.</p>

							<p class="s_tit6">4. 오리엔테이션</p>
							<p class="s_con3">일정 및 기타 주의사항 안내를 위하여 전화 오리엔테이션을 진행합니다.</p>

							<p class="s_tit6">5. 현지 담당자와 일정 확인</p>
							<p class="s_con3">방문국 도착 후 현지 담당자와 전화로 방문일, 시간, 장소에 대하여 최종 확인을 합니다.</p>

							<p class="s_tit6">6. 후원어린이 만남</p>
							<p class="s_con3" style="display:none;">방문 2~3일 전 현지 담당자와 전화 또는 문자로 방문일, 시간, 장소에 대한 최종 확인 필요합니다.</p>

							<p class="s_tit6">7. 비용 지불</p>
							<p class="s_con3">현지 직원의 안내에 따라 현지에서 현지 화폐로 직접 지불합니다.<br />
							한국에서 달러로 환전, 방문국가 도착 후 현지화로 환전하는 것을 권해 드립니다.</p>
						</div>
					</div>
					<!--// 탭메뉴 컨텐츠 -->

					<!-- 문의 -->
					<div class="cq_ta">
						<div class="contact_qna div3">
							<p class="tit">문의</p>
							<div class="clear2">
								<div class="tel"><span class="icon"><span class="txt">전화 : </span>02)3668-3578</span></div>
								<div class="fax"><span class="icon"><span class="txt">팩스 : </span>02)3668-3501</span></div>
								<div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:visiontrip@compassion.or.kr">visiontrip@compassion.or.kr</a></span></div>
							</div>
						</div>

						<div class="mb60">
							<span class="fs15 vam">자주 묻는 질문을 이용하시면 더 많은 궁금증을 해결하실 수 있습니다.</span>
							<a href="/customer/faq/?s_type=K" class="btn_s_type2 ml10">FAQ 바로가기</a>
						</div>
					</div>


					<!-- 배너 : 마지막탭에만 활성화 -->
					<div class="banner">
						<p class="s_tit1 mb20">개인방문의 자세한 후기가 궁금하신가요?</p>
						<a href="/sympathy/vision/individual/"><img src="/common/img/page/participation/btn_banner2.jpg" alt="개인방문 후기 보러가기" /></a>
					</div>

				</div>


			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->

		

    </section>

</asp:Content>
