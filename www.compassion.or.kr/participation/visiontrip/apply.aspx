﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="apply.aspx.cs" Inherits="participation_visiontrip_apply" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>
                

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션비전트립 안내</em></h1>
				<span class="desc">자세한 비전트립 신청 방법을 알려드립니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 part">

			<div class="visual_tripApply mb60"></div>

			<div class="w980 tripApply">

				<!-- 탭메뉴 -->
				<ul class="tab_type1 apply font15">
					<li style="width:33%" class="on"><a href="#">신청 안내</a></li>
					<li style="width:33%"><a href="#">참가 자격</a></li>
					<li style="width:34%"><a href="#">취소 및 환불 규정</a></li>
				</ul>
				<!--//-->

				<script type="text/javascript">
					$(function () {
						$(".tab_type1.apply > li").click(function () {
							var idx = $(this).index() + 1;
							$(".tab_type1.apply > li").removeClass("on");
							$(this).addClass("on");
							$(".tc").hide();
							$(".tc" + idx).show();

							return false;
						});
					})
				</script>

				<!-- 탭메뉴 컨텐츠 -->
				<div class="tab_contents">
					<!-- 신청안내 -->
					<div class="tc tc1">
						<div class="conTit">신청 절차
                            <div class="btn" style="display: inline-block;position: absolute;float: right;margin-left: 280px;">
                                <a href="/common/download/비전트립 가이드라인.pdf" class="btn_s_type6" target="_blank" style="float:right;">비전트립 가이드라인</a>
                            </div>
						</div>
						<ul class="step clear2">
							<li>온라인 신청 완료</li>
							<li>트립비용 잔액 입금</li>
							<li>오리엔테이션 참석</li>
							<li>비전트립 참가</li>
						</ul>
						<p class="s_tit6 notopline">1. 온라인신청서 작성, 여권사본 첨부 및 신청비 입금</p>
						<ul class="paper">
							<li>1) 홈페이지 회원가입  또는 로그인 후 비전트립 온라인신청서 작성</li>
							<li>2) 여권사본(기타서류) 첨부 
                                <p class="s_con3">귀국일 기준으로 잔여 유효기간이 6개월 이상이어야 하며, 최소 3 페이지 이상의 잔여 페이지가 있어야 합니다.</p>
                                <p class="s_con3">여권사본(기타서류)은 온라인신청서 작성 시 첨부하시거나 메일 또는 팩스로 제출해 주세요.</p>
							</li> 						
							<li>
								3) 신청비 입금
								<p class="s_con3">온라인신청서 작성 시 안내에 따라 실시간이체 또는 전용계좌로 입금해 주세요.</p>
                                <p class="s_con3">신청비는 총 트립비용에 포함되며, 온라인신청 및 신청비가 입금되어야 신청이 완료됩니다.</p>
							</li>							
						</ul>
						<div class="greybox">
						<%--	<p class="tit">트립비용 입금 계좌 <em>국민은행 490701-01-081707 사회복지법인 한국컴패션</em></p>
							<span class="s_con1">입금자명 기재 시 참가자 이름과 트립 출발일을 함께 기재해 주세요. (예 : 홍길동0624)</span><br />
					--%>		<p class="tit">※반드시 본인 이름으로 입금해 주시되, 입금하는 분의 이름이 다른 경우 전화 연락 부탁 드립니다.</p>
						</div>

						<p class="s_tit6">2. 트립비용 잔액 입금</p>
						<p class="s_con3">선 입금하신 신청비 20만원을 제외한 트립비용 잔액은 트립별로 상이하며, 신청이 완료된 참가자에 한해 안내 드립니다.</p>

						<p class="s_tit6">3. 오리엔테이션 참석</p>
						<p class="s_con3"><em>트립 출발 약 2주 전 한국컴패션 사옥에서 진행되며, 신청이 완료된 참가자에 한해 안내 드립니다.</em></p>

						<p class="s_tit6">4. 비전트립 출발</p>

                        
                        <div style="margin: 0px auto; text-align: center;">  
                            <a href="/participation/visiontrip/schedule" class="btn_s_type1 ml10">비전트립 신청하기</a> 
                        </div>
					</div>
					<!--// 신청안내 -->

					<!-- 참가자격 -->
					<div class="tc tc2" style="display:none">
						<p class="s_tit6 notopline">비전트립 참가 자격</p>
						<p class="s_con3">컴패션비전트립은 컴패션의 양육을 받고 있는 어린이들을 직접 만나고 컴패션 사역현장을 체험하기 원하는 분이라면 후원 여부에 관계 없이 참가하실 수 있습니다.<br />
						단, 해외 여행에 결격 사유가 없어야 합니다.</p>

						<p class="s_tit6">비전트립 참가 가능 연령</p>
						<p class="s_con3">비전트립 참가는 <em>만 12세 이상(출발일 기준)</em>부터 가능하며,  <em>만 19세 미만(출발일 기준)</em>의 경우 반드시 보호자가 동행하셔야 합니다.</p>

						<p class="s_tit6">만 15세 미만 어린이 필리핀 입국 필요 서류</p>
						<p class="s_con3">만 15세 미만(출발일 기준) 어린이의 필리핀 입국 시, 반드시 만 20세 이상(출발일 기준) 보호자 동행을 원칙으로 하며, 아래와 같은 서류를 필요로 합니다.</p>
						<div class="greybox">
							<p class="tit">부모 동반일 경우</p>
							<div class="con pb20">
								1) 어린이 영문 주민등록등본 1부 (여권사본과 동일한 영문 명으로 발급)<br />
								<span class="s_con1">부모 모두 동반 시 권면 사항이나, 부모 중 어머니만 동반 시 필수 사항</span>
							</div>
							<p class="tit">부모 미동반일 경우</p>
							<div class="con">
								1) 필리핀 대사관 부모동의서 1부 <em>(공증 필수)</em><br />
								<span class="s_con1 pb10">필리핀 대사관 홈페이지(<a href="http://www.philembassy-seoul.com/forms.asp" target="_blank" style="color:#2b35fb;text-decoration:underline;">http://www.philembassy-seoul.com/forms.asp</a>) 다운로드 후, 공증 사무소에서 공증</span><br />
								2) 어린이 영문 주민등록등본 1부 (여권사본과 동일한 영문명으로 발급)<br />
								3) 어린이 여권사본 1부<br />
								4) 어린이 부/모의 여권사본 각 1부<br />
								5) 입국 시, 약 $90 수속 비용
							</div>
						</div>						
					</div>
					<!--// 참가자격 -->

					<!-- 취소 및 환불 규정 -->
					<div class="tc tc3" style="display:none">
						<p class="s_con3">한국컴패션의 비전트립 참가 취소 규정은 선 지급된 항공료, 숙박료, 기타 현지 사용비 등에 취소 수수료가 발생하게 됨에 따라 만들어진 규정입니다.<br />
						또한 다른 참가자 분들에 대한 배려와 약속을 지켜드리기 위함입니다.<br />
						이에 따라 한국컴패션은 2012년 1월부터 참가자의 개인적인 사유로 비전트립 취소 시, 아래와 같은 규정을 적용하고 있습니다.</p>
						
						<p class="s_tit6">비전트립 신청 취소 시 환불 규정</p>
						<ul class="mb10">
							<li><span class="s_con8">출발 30일 전까지 취소 요청 시: 신청비를 제외한 참가비 납부액의 전액 환불</span></li>
							<li><span class="s_con8">출발 29~20일 전까지 취소 요청 시: 신청비를 제외한 참가비 납부액의 90% 환불</span></li>
							<li><span class="s_con8">출발 19~10일 전까지 취소 요청 시: 신청비를 제외한 참가비 납부액의 80% 환불</span></li>
							<li><span class="s_con8">출발 9~5일 전까지 취소 요청 시: 신청비를 제외한 납부액의 60% 환불</span></li>
							<li><span class="s_con8">출발 4~1일 전까지 취소 요청 시:  신청비를 제외한 참가비 납부액의 40% 환불</span></li>
							<li><span class="s_con8">출발 당일 취소 요청 시: 전체 트립 비용 환불 불가</span></li>
						</ul>
						<ul class="greybox">
							<li><span class="s_con1">항공 예약 취소 시 계약금을 포함한 취소 수수료가 발생할 경우, 이는 위 금액과 별도로 환불 금액에서 공제되오니 이 점 참고 부탁 드립니다.</span></li>
							<li><span class="s_con1">비전트립을 취소하실 경우, 반드시 전화로 연락 주시기 바랍니다.</span></li>
						</ul>
					</div>
					<!--// 취소 및 환불 규정 -->
				</div>
				<!--// 탭메뉴 컨텐츠 -->

				<div class="contact_qna div3">
					<p class="tit">문의</p>
					<div class="clear2">
						<div class="tel"><span class="icon"><span class="txt">전화 : </span>02)3668-3420. 3453</span></div>
						<div class="fax"><span class="icon"><span class="txt">팩스 : </span>02)3668-3501</span></div>
						<div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:visiontrip@compassion.or.kr">visiontrip@compassion.or.kr</a></span></div>
					</div>
				</div>

				<div>
					<span class="fs15 vam">자주 묻는 질문을 이용하시면 더 많은 궁금증을 해결하실 수 있습니다.</span>
					<a href="/customer/faq/?s_type=J" class="btn_s_type2 ml10">FAQ 바로가기</a>
				</div>

			</div>

			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->

		

    </section>

</asp:Content>
