﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using Microsoft.AspNet.FriendlyUrls;

public partial class participation_visiontrip_complete : FrontBasePage {

    public string year;
    const string listPath = "/participation/visiontrip/schedule/";


    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }
    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();

        if (!UserInfo.IsLogin)
        {
            Response.ClearContent();
            return;
        }

        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1)
        {
            Response.Redirect(listPath, true);
        }
        //else if(requests.Count > 1)
        //{
        //    hdnApplyID.Value = requests[1];
        //}
        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);


        //parameter
        base.PrimaryKey = requests[0];
        hdnApplyID.Value = base.PrimaryKey.ToString();

        GetVisionTripSchedule();

        //if (PrimaryKey.ToString() == "plan" && hdnApplyID.Value != "")
        //{
        //    lbTitle.Text = "비전트립";
        //    GetVisionTripSchedule();
        //}
        //else
        //{
        //    lbTitle.Text = "개인방문";

        //    lbSchedule.Text = DateTime.Now.ToString("yyyy-MM-dd");
        //    lbTripCountry.Text = "개인방문 ";
        //    lbMyVisionTrip.Visible = true;
        //}


        //if (!requests[0].CheckNumeric())
        //{
        //    Response.Redirect(listPath, true);
        //} 
    } 

    protected void GetVisionTripSchedule()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            UserInfo sess = new UserInfo();

            //var apply = dao.tVisionTripApply.Where(p => p.ApplyID == Convert.ToInt32(hdnApplyID.Value) && p.UserID == sess.UserId).FirstOrDefault();
            var apply = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(PrimaryKey), "UserID", sess.UserId);
            if (apply != null)
            {
                if (apply.ApplyType == "Plan")
                {
                    //var list = dao.tVisionTripSchedule.Where(p => p.ScheduleID == Convert.ToInt32(apply.ScheduleID)).FirstOrDefault();
                    var list = www6.selectQF<tVisionTripSchedule>("ScheduleID", Convert.ToInt32(PrimaryKey));
                    if (list != null)
                    {
                        divPlanTrip.Visible = true;

                        lbTitle.Text = "비전트립";
                        lbSchedule.Text = list.StartDate.ToString() + " ~ " + list.EndDate.ToString();
                        lbTripCountry.Text = list.VisitCountry.ToString();

                        //var attach = dao.tVisionTripAttach.Where(p => p.ApplyID == apply.ApplyID && p.AttachType == "passport").ToList();
                        var attach = www6.selectQ<tVisionTripAttach>("ApplyID", apply.ApplyID, "AttachType", "passport");
                        if (attach.Count > 0)
                        {
                            divAttachCheck.Visible = false;
                            divPayCheck.Visible = true;
                        }
                        else
                        {
                            divAttachCheck.Visible = true;
                            divPayCheck.Visible = false;
                        }
                    }
                    else
                    {
                        Response.Redirect(listPath, true);
                    }
                }
                else
                {
                    //var individual = dao.tVisionTripIndividualDetail.Where(p => p.ApplyID == apply.ApplyID).FirstOrDefault();
                    var individual = www6.selectQF<tVisionTripIndividualDetail>("ApplyID", apply.ApplyID);

                    if (individual != null)
                    {
                        divIndividualTrip.Visible = true;

                        lbTitle.Text = "개인방문";
                        //동반인 체크
                        //var companion = dao.tVisionTripCompanion.Where(p => p.IndividualDetailID == individual.IndividualDetailID).ToList(); 
                        var companion = www6.selectQ<tVisionTripCompanion>("IndividualDetailID", individual.IndividualDetailID);

                        if (companion.Count > 0)
                        {
                            //동반인 Y : 첨부파일 체크
                            //var attach = dao.tVisionTripAttach.Where(p => p.ApplyID == apply.ApplyID).ToList();
                            var attach = www6.selectQ<tVisionTripAttach>("ApplyID", apply.ApplyID);
                            if (attach.Count > 0)
                                divMoveCheck.Visible = true;
                            else
                                divCompanionCheck.Visible = true;
                        }
                        else
                        {
                            divMoveCheck.Visible = true;
                        }
                    }
                    else
                    {
                        Response.Redirect(listPath, true);
                    }

                }
            }
        }
    }

}