﻿(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, popup) {
        //요청 트립 URL 관련
        $scope.visiontripType = $("#hdnApplyType").val() != "" && $("#hdnRequestKey").val() != ""
                                && $("#hdnApplyType").val() == "Request" ? "Request" : "";

        $scope.requestKey = $("#hdnApplyType").val() != "" && $("#hdnRequestKey").val() != ""
                                && $("#hdnApplyType").val() == "Request" ? $("#hdnRequestKey").val() : "";

        $scope.requestview = $("#hdnApplyType").val() != "" && $("#hdnRequestKey").val() != ""
                                && $("#hdnApplyType").val() == "Request" ? true : false;

        $scope.userId = common.getUserId();

        $scope.requesting = false;
        $scope.page = 1;
        $scope.rowsPerPage = 100;
        //$scope.curYear = new Date().getFullYear();
        if (selectedYear == null || selectedYear == undefined || selectedYear == "")
            $scope.curYear = new Date().getFullYear();
        else
            $scope.curYear = parseInt(selectedYear);

        if (cookie.get("visiontripYear") != null) {
            $scope.curYear = parseInt(cookie.get("visiontripYear"));
            cookie.set("visiontripYear", $scope.curYear, -1);
        }

        $scope.isLogin = common.isLogin();

        $scope.params = {
            page: $scope.page,
            rowsPerPage: $scope.rowsPerPage,
            year: $scope.curYear
        };

        $scope.loginCheck = function () {
            if (!common.checkLogin()) {
                return true;
            }
            else {
                return false;
            }
        }


        // 트립일정목록
        $scope.getList = function (params) {
            var visionTripType = '';
            if ($scope.visiontripType != '') {
                // 요청트립의 경우, 관리자에서 생성된 url로 접근시 년도에 상관없이 검색되도록
                visionTripType = $scope.visiontripType;
            }
            else {
                //#12773 : 올해 기준 이전년도의 조회시 요청트립 포함
                visionTripType = ($scope.curYear > $scope.params.year ? 'Request,Plan' : '');
            }

            //$http.get("/api/visiontrip.ashx?t=schedulelist", { params: { year: $scope.params.year, type: $scope.visiontripType, requestkey: $scope.requestKey, userid: $scope.userId } }).success(function (result) {
            $http.get("/api/visiontrip.ashx?t=schedulelist", { params: { year: $scope.params.year, type: visionTripType, requestkey: $scope.requestKey, userid: $scope.userId } }).success(function (result) {

                //console.log(result.data);
                $scope.list = [];
                $.each(result.data, function () {
                    // if (this.VisionTripType == "Plan") {
                    this.opendate = new Date(this.opendate.replace(/-/g, '/'));
                    this.closedate = new Date(this.closedate.replace(/-/g, '/'));

                    if (this.tripreview != null) {
                        if (this.tripreview.length == 7 && this.tripreview.indexOf("http://") == 0) {
                            this.tripreview = "";
                        }
                        //console.log(this.trip_review.length, this.trip_review.indexOf("http://"))
                    }

                    this.tripnotice_view = true;//this.tripnotice.length > 0 ? true : false;
                    this.tripnoticestatus = "";
                    switch (this.tripstate) {
                        case 'Apply': this.status = 'ing'; this.result = '신청중'; break;
                        case 'Expect': this.status = 'expected'; this.result = '예정'; this.tripnotice_view = false; break;
                        case 'Deadline': this.status = 'deadline'; this.result = '마감'; break;
                        case 'EarlyDeadline': this.status = 'earlydeadline'; this.result = '조기마감'; break;
                        case 'Close': this.status = 'end'; this.result = '종료'; this.tripnoticestatus = "a-disabled"; break;
                        default: this.status = ''; this.result = ''; break;
                    }



                    //신청하기 버튼 apply_count maxcount
                    this.applyresult = '신청하기';
                    this.applyCountCheck = '';
                    if (this.apply_count == this.maxcount) {
                        this.applyCountCheck = 'a-disabled';
                        this.applyresult = '신청마감';
                    }

                    this.applystatus = 'end a-disabled';
                    //if (common.isLogin()) {
                    switch (this.tripstate) {

                        case 'Apply': //신청중 : 신청일 시작된 경우
                            this.applystatus = (this.apply_state == "Apply" && common.isLogin() ? 'apply a-disabled ' : (this.applyresult == '신청하기' ? 'ing blue ' : 'ing ')) + this.applyCountCheck;
                            this.applyresult = (this.apply_state == "Apply" && common.isLogin() ? '신청완료' : this.applyresult);
                            break;

                        case 'Expect': //예정
                            this.applystatus = 'expected a-disabled'; this.applyresult = '신청하기'; break;

                        case 'Deadline':  //마감 : 신청 마감일 지난 경우 
                            this.applystatus = 'end a-disabled'; this.applyresult = '신청하기'; break;

                        case 'EarlyDeadline':  //조기마감 : 신청인원 다 참
                            this.applystatus = 'end a-disabled'; this.applyresult = '신청하기'; break;

                        case 'Close': //종료 : 트립 종료일 지난 경우
                            this.applystatus = 'end a-disabled'; this.applyresult = '신청하기'; break;
                        default: this.applystatus = ''; this.applyresult = ''; break;
                    }
                    // }

                    this.answerClass = "expected"
                    //{{item.apply_count}}/{{item.maxcount}}

                    this.applycount_view = this.apply_count == 0 && this.maxcount == 0 ? "" : this.apply_count + "/" + this.maxcount;
                    
                    //#13542 start
                    this.statusnew = ''; //orange, gray, lightorange, lightgray
                    this.statustextnew = ''; //마감, 조기마감, 종료, 예정, 신청완료, 신청하기

                    if (this.tripstate == 'Deadline') this.statustextnew = '마감';
                    else if (this.tripstate == 'EarlyDeadline') this.statustextnew = '조기마감';
                    else if (this.tripstate == 'Close') this.statustextnew = '종료';
                    else if (this.tripstate == 'Expect') this.statustextnew = '예정';
                    else if (this.tripstate == 'Apply') {
                        if (this.apply_state == "Apply" && common.isLogin()) this.statustextnew = '신청완료';
                        else this.statustextnew = '신청하기';
                    }

                    if (this.statustextnew == '마감' || this.statustextnew == '조기마감' || this.statustextnew == '종료') {
                        this.statusnew = 'lightgray  a-disabled';
                    }
                    else if (this.statustextnew == '예정') {
                        this.statusnew = 'gray  a-disabled';
                    }
                    else if (this.statustextnew == '신청완료') {
                        this.statusnew = 'lightorange  a-disabled';
                    }
                    else if (this.statustextnew == '신청하기') {
                        this.statusnew = 'orange';
                    }
                    //#13542 end

                    $scope.list.push(this);
                    //  }
                })
                //console.log($scope.list)
            });

        }

        $scope.lastYear = function () {
            $scope.params.year = parseInt($scope.params.year) - 1;
            $scope.params.page = 1;

            $("#next_year").show()
            $scope.hideLastYear();
            $scope.getList();

        }

        $scope.nextYear = function () {
            $scope.params.year = parseInt($scope.params.year) + 1;
            $scope.params.page = 1;

            $("#last_year").show()
            $scope.hideNextYear();
            $scope.getList();

        }

        $scope.setYear = function (obj) {
            //cookie.set("visiontripYear", $scope.params.year, 1000000);
            obj.item.trip_review += "?year=" + $scope.params.year;
        }

        $scope.hideLastYear = function () {
            $("#last_year").hide();
            $http.get("/api/visiontrip.ashx?t=schedulelist", { params: { year: $scope.params.year - 1, type: "" } }).success(function (result) {
                var length = result.data.length;
                if (length != 0) {
                    $("#last_year").show();
                }
            })
        }

        $scope.hideNextYear = function () {
            $("#next_year").hide();
            $http.get("/api/visiontrip.ashx?t=schedulelist", { params: { year: $scope.params.year + 1, type: "" } }).success(function (result) {
                var length = result.data.length;
                if (length != 0) {
                    $("#next_year").show();
                }
            })
        }

        //나의 현지방문 바로가기
        $scope.myVisionTripClick = function () {
            if (!common.checkLogin()) {
                return;
            }
            else {
                location.href = "/my/activity/visiontrip/";
            }
        }

        $scope.applyCheck = function (item) {
            if (!common.checkLogin()) {
                return;
            }
            else {//세션 존재하면

                //var hdnSponsorID = $("#hdnSponsorID").val();
                //if (hdnSponsorID == "") {
                //    alert("후원자님만 신청이 가능합니다.");
                //    return;
                //}

                $scope.getList();

                //선택 일정 신청 현황 다시 체크 
                $http.get("/api/visiontrip.ashx?t=schedule_check", { params: { scheduleid: item.scheduleid } }).success(function (r) {
                    console.log(r);
                    if (r.success) {
                        var list = r.data;
                        $scope.tripstateCheck = list.tripstate;

                        //신청현황이 신청중인 경우에만 신청서 화면으로 이동
                        if ($scope.tripstateCheck == "Apply") {
                            var hdnBirth = $("#hdnBirthCheck").val();
                            if (hdnBirth != "") {
                                var birthday = new Date(hdnBirth.substring(0, 10));
                                var today = new Date(item.startdate);

                                var years = today.getFullYear() - birthday.getFullYear();

                                console.log('계산 된 나이 : ' + years);
                                birthday.setFullYear(today.getFullYear());

                                if (today < birthday) years--;
                                console.log('계산 된 만 나이 : ' + years);

                                if (years < 12) {
                                    alert("만 12세 미만은 신청할 수 없습니다.");
                                    return;
                                }
                                else if (years < 19 && years >= 12) {
                                    //alert("만 12세 미만은 신청할 수 없습니다.");
                                    //if (confirm()) {
                                    location.href = "/participation/visiontrip/form/apply_plan/" + item.scheduleid + "?";
                                    //}
                                }
                                else if (years >= 19) {
                                    location.href = "/participation/visiontrip/form/apply_plan/" + item.scheduleid + "?";
                                }
                                else {
                                    return;
                                }
                            }
                        } else {
                            alert("신청 현황을 다시 확인 해주세요.");
                        }
                        //
                    } else {
                        console.log(r);
                        alert("다시 선택해주세요.");
                    }
                });
            }
        }


        //트립안내 popup
        $scope.tripnoticeModal = {
            instance: null,
            banks: [],
            detail_list: [],

            init: function () {
                popup.init($scope, "/participation/visiontrip/tripnotice", function (tripnoticeModal) {
                    $scope.tripnoticeModal.instance = tripnoticeModal;
                    //$scope.tripnoticeModal.show();
                }, { top: 0, iscroll: true });
            },
            show: function ($event, tripnotice, item) {
                //console.log(orderNo)
                $event.preventDefault();
                if (!$scope.tripnoticeModal.instance)
                    return;

                $scope.tripnoticeModal.instance.show();

                $scope.tripnoticeModal.tripnotice = tripnotice;
                $("#divTripNotice").html(tripnotice);

                //트립안내 상세보기는 트립 신청한 경우에만 표시.
                var noticeDetail = item.tripnoticedetail.replace("http://", "").trim() != "" ? item.tripnoticedetail : "";
                $scope.tripnoticeModal.detailShow = (noticeDetail != "" && item.apply_state == "Apply" && common.isLogin());
                $scope.tripnoticeModal.tripnoticeDetail = item.tripnoticedetail;
            },
            close: function ($event) {
                $event.preventDefault();
                if (!$scope.tripnoticeModal.instance)
                    return;
                $scope.tripnoticeModal.instance.hide();
            }
        };
        $scope.tripnoticeModal.init();

        $scope.download = function (url) {
            var openNewWindow = window.open("about:blank");
            openNewWindow.location.href = url;
        };


        $scope.hideNextYear();
        $scope.hideLastYear();
        $scope.getList();

    });

    // 신청마감일(올해가 아닌경우 년도 표시)
    app.filter('lastYear', function ($filter) {
        return function (close) {
            if (close == 'Invalid Date') return '-';
            var result = $filter('date')(close, "M/d", "ko")
            if (new Date(close).getFullYear() < new Date().getFullYear()) {
                result = $filter('date')(close, "M/d(yyyy)", "ko")
            }
            return result;
        };
    });



})();
