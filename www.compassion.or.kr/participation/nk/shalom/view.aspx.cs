﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;
using System.Web.Configuration;

public partial class participation_nk_news_view : FrontBasePage {

	const string listPath = "/participation/nk/shalom/list/";

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		
		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}

		base.PrimaryKey = requests[0];


		pd_seq.Value = PrimaryKey.ToString();
		
		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();


	}


	protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.TB_PRAYER_DOC.First(p => p.PD_SEQ == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<TB_PRAYER_DOC>("PD_SEQ", Convert.ToInt32(PrimaryKey));

            title.Text = entity.PD_SUBJECT;
            reg_date.Text = Convert.ToDateTime(entity.PD_DATE).ToString("yyyy.MM.dd");
            view_count.Text = entity.PD_VIEW_CNT == null ? "0" : ((int)entity.PD_VIEW_CNT).ToString("N0");
            article.Text = entity.PD_CONTENT;

            pray_cnt.InnerText = entity.PD_PRAYER_CNT == null ? "0" : ((int)entity.PD_PRAYER_CNT).ToString("N0");

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-샬롬기도운동 기도제목] {0}", entity.PD_SUBJECT);

            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = "";
            //this.ViewState["meta_keyword"] = "";
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,FrontBasePage)
            this.ViewState["meta_image"] = entity.PD_MAIN_TN.WithFileServerHost();


            if (entity.PD_MB_ATTACH != "" || entity.PD_MB_ATTACH2 != "" || entity.PD_MB_ATTACH3 != "" || entity.PD_MB_ATTACH4 != "" || entity.PD_MB_ATTACH5 != "")
            {
                phDownload.Visible = true;

                var path = Uploader.GetRoot(Uploader.FileGroup.file_nk);

                if (entity.PD_MB_ATTACH != "")
                {
                    btnFile1.Visible = true;
                    lbFile1.Text = entity.PD_MB_ATTACH.Replace(Uploader.GetRoot(Uploader.FileGroup.file_nk), "");
                    lbFile1.Visible = true;
                    btnFile1.HRef = entity.PD_MB_ATTACH.WithFileServerHost();
                }

                if (entity.PD_MB_ATTACH2 != "")
                {
                    btnFile2.Visible = true;
                    lbFile2.Text = entity.PD_MB_ATTACH2.Replace(Uploader.GetRoot(Uploader.FileGroup.file_nk), "");
                    lbFile2.Visible = true;
                    btnFile2.HRef = entity.PD_MB_ATTACH2.WithFileServerHost();
                }
                if (entity.PD_MB_ATTACH3 != "")
                {
                    btnFile3.Visible = true;
                    lbFile3.Text = entity.PD_MB_ATTACH3.Replace(Uploader.GetRoot(Uploader.FileGroup.file_nk), "");
                    lbFile3.Visible = true;
                    btnFile3.HRef = entity.PD_MB_ATTACH3.WithFileServerHost();
                }
                if (entity.PD_MB_ATTACH4 != "")
                {
                    btnFile4.Visible = true;
                    lbFile4.Text = entity.PD_MB_ATTACH4.Replace(Uploader.GetRoot(Uploader.FileGroup.file_nk), "");
                    lbFile4.Visible = true;
                    btnFile4.HRef = entity.PD_MB_ATTACH4.WithFileServerHost();
                }
                if (entity.PD_MB_ATTACH5 != "")
                {
                    btnFile5.Visible = true;
                    lbFile5.Text = entity.PD_MB_ATTACH5;
                    lbFile5.Visible = true;
                    btnFile5.HRef = entity.PD_MB_ATTACH5.WithFileServerHost();
                }
            }
        }
	}



}