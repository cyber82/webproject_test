﻿(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {


		// 레이어 팝업
		$scope.modal = {
			instance: null,

			init: function () {
				// 팝업
				popup.init($scope, "/participation/nk/shalom/unsubscription", function (modal) {
					$scope.modal.instance = modal;
				});
			},

			show: function () {

				if (!$scope.modal.instance)
					return;

				if (common.checkLogin()) {

					$http.get("/api/participation.ashx?t=subscription").success(function (r) {
						if (r.success) {
							if (r.data.length == "0") {
								alert("구독 중이지 않습니다.");
								return false;
							} else {

								var subscription_type = "";
								if (r.data.sms && r.data.email) {
									subscription_type = "이메일과 SMS";
									$("#unsubscript_all").show();
									$("#unsubscript_sms").show();
									$("#unsubscript_email").show();
								}else if(r.data.sms){
									subscription_type = "SMS";
									$("#unsubscript_sms").show();
									$("#unsubscript_all").hide();
									$("#unsubscript_email").hide();
								} else {
									subscription_type = "이메일"
									$("#unsubscript_email").show();
									$("#unsubscript_all").hide();
									$("#unsubscript_sms").hide();
								}

								$("#subscription_type ").text(subscription_type);

								$scope.modal.instance.show();
							}
						} else {
							alert(r.message);
						}

					});

				}

			},

			request: function ($event, type) {

				if ($scope.requesting) return;
				$scope.requesting = true;


				$.post("/api/participation.ashx?t=unsubscript&type="+type, function (r) {
					if (r.success) {
						alert("구독 해지가 완료되었습니다.");
					} else {
						alert(r.message);
					}
					$scope.modal.close($event);
				});

			},

			close: function ($event) {

				if (!$scope.modal.instance)
					return;

				$scope.modal.instance.hide();
				$event.preventDefault();
			}

		}
		$scope.modal.init();



    });

})();