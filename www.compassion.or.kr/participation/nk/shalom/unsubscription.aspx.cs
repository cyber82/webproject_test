﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;
using System.Web.Configuration;

public partial class participation_nk_shalom_unsubscription : FrontBasePage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		if (!UserInfo.IsLogin) {
			Response.ClearContent();
			return;
		}

		UserInfo sess = new UserInfo();

		u_name.Text = sess.UserName;
	}
}