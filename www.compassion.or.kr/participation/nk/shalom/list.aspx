﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="list.aspx.cs" Inherits="participation_nk_shalom_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/participation/nk/shalom/list.js?v=2"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>샬롬기도운동 자료실</em></h1>
				<span class="desc">하나님의 마음, 그 소망의 마음으로 함께 기도해 주세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
        
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 part">

			<div class="bgContent shalom">
				<%--<p class="tit">이 주의 기도제목</p>
				<span class="bar"></span>
				<p class="con2">
					<!-- <span>[4월 첫번째]</span><br /> -->
                    <asp:Literal runat="server" ID="pray_title"></asp:Literal>
				</p>
                <a runat="server" id="mainView" class="btn_b_type2">더보기</a>--%>
                <p class="con2" style="padding-top:120px;">
					통일을 넘어 하나님께서 이 땅에 이루고자 하시는 샬롬이 이루어지도록,<br />
                    그리고 남과 북의 다음세대가 샬롬의 주인공으로 성장하도록 기도합니다.
				</p>
			</div>

			<div class="north" style="width:980px; margin:0 auto; display:flex;">

				<div class="w980" style="width:480px; display:inline-block; float:left; padding-right:20px;">

					<div class="sortWrap clear2 mb20" id="l">
                        <span style="font-size:20pt; font-weight:bold;">기도제목</span>
						<div class="fr relative">
							<label for="k_word" class="hidden">검색어 입력</label>
							<input type="text"  name="k_word" id="k_word" class="input_search1" style="width:245px" ng-enter="search()" placeholder="검색어를 입력해 주세요" />
							<a href="#" class="search_area1" ng-click="search()">검색</a>
						</div>
					</div>

					<div class="boardList_2">
						<ul class="list" >
							<li ng-repeat="item in list">
								<span class="lst_box">
									<a href="#"><span class="lst_tit2" ng-click="goView(item.pd_seq)">{{item.pd_subject}}</span></a>
									<span class="lst_txt" style="display:none;">
										<span>{{parseDate(item.pd_date) | date:'yyyy.MM.dd'}}</span>
										<span class="lst_bar"></span>
										<span class="lst_hit">{{item.pd_view_cnt}}</span>
									</span>
									<a href="#" class="list_link" style="padding-top:0;" ng-click="goView(item.pd_seq)" ng-bind-html="item.pd_content | limitHtml: 70"></a>
								</span>
							</li>
                            <li>
                                <span ng-if="total == 0" class="no_content">데이터가 없습니다.</span>
                            </li>

							<!-- 썸네일 있는 경우 li에 클래스 "thumb" 추가 -->
								<!-- 썸네일 이미지 사이즈 : 190 * 120 -->
								<!--
							<li class="thumb">
								<span class="lst_img">
									<span class="img_wh"><img src="/common/img/temp/img_23.jpg" alt="썸네일 이미지" /></span>
								</span>
								<span class="lst_box">
									<a href="#"><span class="lst_tit2">온라인 애드보킷이라면 지켜야 할 규칙 10가지!</span></a>
									<span class="lst_txt">
										<span>2016. 09. 01</span>
										<span class="lst_bar"></span>
										<span class="lst_hit">124</span>
									</span>
									<a href="#" class="list_link">
										2016 YVOC 신규지원 1차 합격을 축하드립니다.  합격자 명단과 면접일정을 아래까페에서 확인 부탁드립니다.<br />
										http://cafe.naver.com/iamcompassion 감사합니다.
									</a>
								</span>
							</li>
                                     -->
						
						</ul>
					</div>

					<!-- page navigation -->
					<div class="tac mb30">
						<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
					</div>
					<!--// page navigation -->

					<%--<div class="tar">
						<a href="/participation/nk/shalom/subscription" class="btn_type1">기도제목 구독하기</a>
					</div>--%>

				</div>
                <div class="w980" style="width:480px; display:inline-block; float:right; padding-left:20px;">

					<div class="sortWrap clear2 mb20" id="l2">
                        <span style="font-size:20pt; font-weight:bold;">기도영상</span>
						<div class="fr relative">
							<label for="k_word" class="hidden">검색어 입력</label>
							<input type="text"  name="k_word" id="k_word2" class="input_search1" style="width:245px" ng-enter="search2()" placeholder="검색어를 입력해 주세요" />
							<a href="#" class="search_area1" ng-click="search2()">검색</a>
						</div>
					</div>

					<div class="boardList_2">
						<ul class="list" >
							<li ng-repeat="item in list2">
								<span class="lst_box">
									<a href="#"><span class="lst_tit2" style="padding-bottom:0;" ng-click="goView2(item.pp_seq)">{{item.pp_subject}}</span></a>
									<span class="lst_txt" style="display:none;">
										<span>{{parseDate(item.pp_date) | date:'yyyy.MM.dd'}}</span>
										<span class="lst_bar"></span>
										<span class="lst_hit">{{item.pp_view_cnt}}</span>
									</span>
								</span>
							</li>
                            <li>
                                <span ng-if="total2 == 0" class="no_content">데이터가 없습니다.</span>
                            </li>
						</ul>
					</div>

					<!-- page navigation -->
					<div class="tac mb30">
						<paging class="small" page="params2.page" page-size="rowsPerPage2" total="total2" show-prev-next="true" show-first-last="true" paging-action="getList2({page : page})"></paging> 
					</div>
					<!--// page navigation -->

					<%--<div class="tar">
						<a href="/participation/nk/shalom/subscription" class="btn_type1">기도제목 구독하기</a>
					</div>--%>

				</div>
			</div>
			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->

		

    </section>
    <!--// sub body -->


    <div >

        
        <!--<img src="" runat="server" id="mainImg"/>-->

</asp:Content>