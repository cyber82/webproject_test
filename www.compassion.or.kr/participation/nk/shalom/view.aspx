﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="participation_nk_news_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    
    <script type="text/javascript">
        $(function () {
            // 조회수 증가
            $.post("/api/participation.ashx?t=hits_prayer&id=" + $("#pd_seq").val());

            $("#pray").click(function () {
                $.post("/api/participation.ashx?t=pray&id="+$("#pd_seq").val(), function (r) { 
                    if (r.success) {
                        $("#pray_cnt").text(r.data);
                    } else {
                        alert(r.message);
                    }
                });

            });
        });

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="pd_seq" />

    
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>샬롬기도운동 기도제목</em></h1>
				<span class="desc">하나님의 마음, 그 소망의 마음으로 함께 기도해 주세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents part">

			<div class="north">
				<div class="w980">

					<!-- 게시판 상세 -->
					<div class="boardView_1 line1">
						<div class="tit_box noline">
							<span class="tit" style="padding-bottom: 30px;"><asp:Literal runat="server" ID="title" /></span>
							<span class="txt" style="display:none;">
								<span><asp:Literal runat="server" ID="reg_date" /></span>
								<span class="bar"></span>
								<span class="hit"><asp:Literal runat="server" ID="view_count" /></span>
							</span>
						</div>

						<div class="view_contents">
				<asp:Literal runat="server" ID="article" />
						</div>

        
						<div class="tac">
							<button id="pray"><span class="btn_type1 pray" >기도했어요<br /><span class="hit" runat="server" id="pray_cnt">12</span></span></button>
						</div>

						<!-- sns 공유하기 -->
						<div class="share_box">
							<span class="sns_ani">
								<span class="common_sns_group">
									<span class="wrap">
										<a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
                                        <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
                                        <a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
                                        <a href="#" title="url 공유" data-role="sns" data-provider="copy"  class="sns_url">url 공유</a>
									</span>
								</span>
								<button class="common_sns_share">공유하기</button>
							</span>
						</div>
						<!--// -->

					</div>
					<!--// 게시판 상세 -->

                    <asp:PlaceHolder runat="server" ID="phDownload" Visible="false">
                        <div class="attach_file">
                            <span>첨부파일 다운로드</span>
                            <div class="file_button">
							<a href="#" class="btn_s_type4 mr10" runat="server" ID="btnFile1" target="_blank" visible="false"><asp:Literal runat="server" ID="lbFile1" ></asp:Literal></a>
                            <a href="#" class="btn_s_type4 mr10" runat="server" ID="btnFile2" target="_blank" visible="false"><asp:Literal runat="server" ID="lbFile2" ></asp:Literal></a>
                            <a href="#" class="btn_s_type4 mr10" runat="server" ID="btnFile3" target="_blank" visible="false"><asp:Literal runat="server" ID="lbFile3" ></asp:Literal></a>
                            <a href="#" class="btn_s_type4 mr10" runat="server" ID="btnFile4" target="_blank" visible="false"><asp:Literal runat="server" ID="lbFile4" ></asp:Literal></a>
                            <a href="#" class="btn_s_type4 mr10" runat="server" ID="btnFile5" target="_blank" visible="false"><asp:Literal runat="server" ID="lbFile5" ></asp:Literal></a>
                            </div>
                        </div>

                    </asp:PlaceHolder>
					<!-- 기본 버튼 -->
					<div class="tar">
						<a href="#" class="btn_type4 posR" runat="server" id="btnList">목록</a>
					</div>

				</div>
			</div>

			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->

		

    </section>
    <!--// sub body -->


    
</asp:Content>