﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="unsubscription.aspx.cs" Inherits="participation_nk_shalom_unsubscription"  %>


<div style="background-color:transparent;width:500px;height:400px">
    <!-- 안내팝업 width : 500 -->
	<div class="pop_type1 w500">
		<div class="pop_title">
			<span>구독 해지</span>
			<button class="pop_close"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" ng-click="modal.close()"/></span></button>
		</div>

		<div class="pop_content common_info subscribe">
			<div class="desc">
				현재 <em class="em1"><asp:Literal runat="server" ID="u_name" /></em> 후원자님은 <em class="em1"><span id="subscription_type"></span></em>로 기도 제목을 받아보고 계십니다.<br />
				정말 구독을 해지하실 건가요?
			</div>
			
			<div class="btn_group">
				
				<a href="#" class="btn_type10" id="unsubscript_email" style="display:none;" ng-click="modal.request($event, 'email')"><span class="email"></span>이메일 구독 해지</a>
				<a href="#" class="btn_type10" id="unsubscript_sms" style="display:none;" ng-click="modal.request($event, 'sms')"><span class="sms"></span>SMS 구독 해지</a>
				<a href="#" class="btn_type10" id="unsubscript_all" style="display:none;" ng-click="modal.request($event, 'all')">전체 구독 해지</a>

			</div>
			
		</div>
	</div>
	<!--// popup -->


</div>
    
