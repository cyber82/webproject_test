﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="participation_nk_shalom_intro" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>샬롬기도운동</em></h1>
                <span class="desc">이 땅에 샬롬이 이뤄지도록 샬롬기도운동에 함께해 주세요</span>

                <uc:breadcrumb runat="server" />

            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 part">


            <div class="shalom_movie">
                <div class="w980">
                    <iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/8DuE5jBBf2o?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>

            <div class="north tac">

                <div class="shalom_intro w980">
                    <span class="sh_logo"></span>
                    <p class="s_con3">
                        <%--통일을 넘어 하나님께서 이 땅에 이루고자 하시는 샬롬이 이루어지도록,<br />
                        그리고 남과 북의 다음세대가 샬롬의 주인공으로 성장하도록 기도합니다.<br />
                        <br />--%>
                        이 땅을 향한 하나님의 소망은 진정한 샬롬을 이루는 것입니다.<br />
                        한국교회가 빛과 소금으로 세워져 복음과 사랑으로 북한을 섬길 때 하나님의 소망이 이루어집니다.<br />
                        이를 위해 하나님의 사랑이 북한과 우리의 마음에 부어지도록 기도합니다.<br />
                        <br />
                        남과 북의 어린이가 단순한 통일세대가 아닌 성경적 정체성을 가지고 서로 다름 속에서 조화를 이루는<br />
                        샬롬세대가 될 수 있도록 지금부터 기도로 준비합니다.
                    </p>
                </div>

                <div class="shalom_how" style="background-color:#E6E6E6;">
                    <div class="w980">
                        <p class="sub_tit" style="margin-bottom: 40px;">참여 방법</p>
                        <%--<ul class="clear2">
                            <li class="con1">
                                <div class="con_tit">구독하기</div>
                                <p class="s_con3">
                                    - 기도제목은 매월 북한사역 뉴스레터와 함께 제작됩니다.<br />
                                    - 구독을 신청하시면 문자로 받아보실 수 있습니다.
                                </p>
                                <a href="/participation/nk/shalom/subscription/" class="btn_s_type4">구독 신청하기</a>
                            </li>
                            <li class="con2">
                                <div class="con_tit">기도하기</div>
                                <p class="s_con3">
                                    - 기도 제목을 주변에 소개하고 북한에 대한 소망을 나눌 수 있습니다.<br />
                                    - 공동체의 기도모임에 자유롭게 나누고 함께 기도할 수 있습니다.<br />
                                    - 좋은 기도제목을 SNS에 공유하고 [기도했어요] 버튼을 눌러주세요.
                                </p>
                                <a href="/participation/nk/shalom/list/" class="btn_s_type4">기도제목 보기</a>
                            </li>
                        </ul>--%>
                        <p class="s_con3" style="margin-bottom: 40px;">
                            아래의 제목을 클릭하시면<br />
                            기도내용을 확인하실 수 있는 페이지로 이동합니다.
                        </p>
                        <style>
                            .shalomTitle { font-size:18pt; display:block; font-weight: bold; text-decoration: underline; }
                            .shalomLink { width:24% !important; display:inline-block; vertical-align:top; }
                        </style>
                        <ul class="clear2" style="margin-top:20px;">
                            <li class="shalomLink">
                                <img src="/common/img/page/participation/shalom_prayerlogo.png" style="margin:20px auto 21px auto; height:40px;" />
                                <a href="/participation/nk/shalom/list/" class="btn_type1">샬롬기도운동 자료실</a>
                                <span style="display:block;">북한어린이들을 위한 기도 및 영상<br />[한국컴패션 제공]</span>
                            </li>
                            <li class="shalomLink">
                                <img src="/common/img/page/participation/shalom_days.png" style="margin:auto; height:80px;" />
                                <a href="http://www.pn4n.org/" class="btn_type1">매일 기도제목</a>
                                <span style="display:block;">[PN4N 제공]</span>
                            </li>
                            <li class="shalomLink">
                                <img src="/common/img/page/participation/shalom_weeks.png" style="margin:auto; height:80px;" />
                                <a href="http://jubileeuni.com/menu82" class="btn_type1">매주 기도제목</a>
                                <span style="display:block;">[쥬빌리 통일구국기도회 제공]</span>
                            </li>
                            <li class="shalomLink">
                                <img src="/common/img/page/participation/shalom_months.png" style="margin:auto; height:80px;" />
                                <a href="http://www.missionkorea.org/?page_id=12543" class="btn_type1">매월 기도제목</a>
                                <span style="display:block;">[선교한국 제공]</span>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>


        </div>
        <!--// e: sub contents -->



    </section>
</asp:Content>
