﻿(function () {

    var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

	    $scope.total = -1; $scope.total2 = -1;
	    $scope.rowsPerPage = 10; $scope.rowsPerPage2 = 10;

	    $scope.list = []; $scope.list2 = [];
	    $scope.params = {
	        page: 1,
	        rowsPerPage: $scope.rowsPerPage
	    };
	    $scope.params2 = {
	        page: 1,
	        rowsPerPage: $scope.rowsPerPage2
	    };

		// 파라미터 초기화
	    $scope.params = $.extend($scope.params, paramService.getParameterValues());
	    $scope.params2 = $.extend($scope.params2, paramService.getParameterValues());

	    if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);
	    if ($scope.params2.k_word) $("#k_word2").val($scope.params2.k_word);

        // 검색
        $scope.search = function (params) {
        	$scope.params.page = 1;
        	$scope.params = $.extend($scope.params, params);
        	$scope.params.k_word = $("#k_word").val();
        	$scope.getList();
        }
        $scope.search2 = function (params) {
            $scope.params2.page = 1;
            $scope.params2 = $.extend($scope.params2, params);
            $scope.params2.k_word = $("#k_word2").val();
            $scope.getList2();
        }

        // list
        $scope.getList = function (params) {
        	$scope.params = $.extend($scope.params, params);
        	$http.get("/api/participation.ashx?t=prayer_list", { params: $scope.params }).success(function (result) {
        		$scope.list = result.data;
        		$scope.total = result.data.length > 0 ? result.data[0].total : 0;

        		if (params)
        			scrollTo($("#l"), 10);
        	});
        }
        $scope.getList2 = function (params) {
            $scope.params2 = $.extend($scope.params2, params);
            $http.get("/api/participation.ashx?t=prayer_picture_list", { params: $scope.params2 }).success(function (result) {
                $scope.list2 = result.data;
                $scope.total2 = result.data.length > 0 ? result.data[0].total : 0;

                if (params)
                    scrollTo($("#l2"), 10);
            });
        }

        // 상세페이지
        $scope.goView = function (id) {
        	//$http.post("/api/participation.ashx?t=hits_prayer&id=" + id).then().finally(function () {
        	//	location.href = "/participation/nk/shalom/view/" + id + "?" + $.param($scope.params);
            //});

            location.href = "/participation/nk/shalom/view/" + id + "?" + $.param($scope.params);
        }
        $scope.goView2 = function (id) {
            //$http.post("/api/participation.ashx?t=hits_prayer&id=" + id).then().finally(function () {
            //	location.href = "/participation/nk/shalom/view/" + id + "?" + $.param($scope.params);
            //});

            location.href = "/participation/nk/shalom/viewPicture/" + id + "?" + $.param($scope.params2);
        }

        $scope.getParams = function () {
        	return $.param($scope.params);
        }

        $scope.parseDate = function (datetime) {
        	return new Date(datetime);
        }


        $scope.getList();
        $scope.getList2();


    });

})();