﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;
using System.Web.Configuration;

public partial class participation_nk_shalom_subscription : FrontBasePage {

	public override bool RequireLogin {
		get {
			return true;
		}
	}


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		GetData();
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();

		GetData();
	}

	protected void GetData() {


		UserInfo sess = new UserInfo();
		/*
		if (sess.Email.EmptyIfNull() != "") {
			email.Text = sess.Email;
			//email.Attributes["disabled"] = "disabled";
		}
		phone.Text = sess.Mobile;
		phoneCheck.Value = sess.Mobile;
		*/

		var comm_result = new SponsorAction().GetCommunications();
		if (!comm_result.success) {
			base.AlertWithJavascript(comm_result.message, "goBack()");
			return;
		}

		SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
		phone.Text = comm_data.Mobile.TranslatePhoneNumber();
		phoneCheck.Value = comm_data.Mobile.TranslatePhoneNumber();

		email.Text = comm_data.Email;


	}
/*
	protected void btnSns_Click(object sender, EventArgs e) {
		tabSms.Visible = true;
		tabMail.Visible = false;
	}

	protected void btnMail_Click(object sender, EventArgs e) {
		tabSms.Visible = false;
		tabMail.Visible = true;

	}
    */
	protected void btnSubscription_Click(object sender, EventArgs e)
    {
		UserInfo sess = new UserInfo();
		UpdateWebMobile(sess);
		// SMS 구독
		if (tabValue.Value == "Y")
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                var exist = www6.selectQ<nk_subscription>("ns_type", "sms", "ns_user_id", sess.UserId);
                //if (dao.nk_subscription.Any(p => p.ns_type == "sms" && p.ns_user_id == sess.UserId))
                if(exist.Any())
                {
                    AlertWithJavascript("이미 SMS 구독을 신청하셨습니다.");
                }
                else
                {
                    var entity = new nk_subscription()
                    {
                        ns_user_id = sess.UserId,
                        ns_type = "sms",
                        ns_regdate = DateTime.Now
                    };
                    //dao.nk_subscription.InsertOnSubmit(entity);
                    www6.insert(entity);
                    //dao.SubmitChanges();

                    AlertWithJavascript("구독신청이 완료되었습니다.", "location.href='/participation/nk/shalom/subscription';");
                }
            }

		// 이메일 구독
		}
        else
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                var exist = www6.selectQ<nk_subscription>("ns_type", "email", "ns_user_id", sess.UserId);
                //if (dao.nk_subscription.Any(p => p.ns_type == "email" && p.ns_user_id == sess.UserId))
                if(exist.Any())
                {
                    AlertWithJavascript("이미 이메일 구독을 신청하셨습니다.");
                }
                else
                {
                    var entity = new nk_subscription()
                    {
                        ns_user_id = sess.UserId,
                        ns_type = "email",
                        ns_regdate = DateTime.Now
                    };
                    //dao.nk_subscription.InsertOnSubmit(entity);
                    www6.insert(entity);
                    //dao.SubmitChanges();

                    AlertWithJavascript("구독신청이 완료되었습니다.", "location.href='/participation/nk/shalom/subscription';");
                }
            }
		}
	}

	// 컴파스에서 휴대폰 번호 가져온 경우 웹DB업데이트
	protected void UpdateWebMobile(UserInfo sess)
    {
        using (AuthDataContext dao = new AuthDataContext())
        {
            //var entity = dao.tSponsorMaster.FirstOrDefault(p => p.UserID == sess.UserId);
            var entity = www6.selectQFAuth<tSponsorMaster>("UserID", sess.UserId);
            if (entity != null && entity.Mobile.EmptyIfNull() == "")
            {
                entity.Mobile = phone.Text.Replace("-", "").Encrypt();

                //dao.SubmitChanges();
                www6.updateAuth(entity);
            }
        }
	}
}