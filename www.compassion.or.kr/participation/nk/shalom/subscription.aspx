﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="subscription.aspx.cs" Inherits="participation_nk_shalom_subscription" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    
    <script type="text/javascript" src="/participation/nk/shalom/unsubscription.js"></script>
    <script type="text/javascript">
        $(function () {
            // 메일
            $("#email").formatValidate({ match: false, message: "올바른 이메일 형식이 아닙니다.", pattern: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/ });

            // 구독해지
            $("#unsubscription").click(function () {

                if ($(".btnUnsubscript").length < 1) {
                    alert("구독하고 있지 않습니다.");
                    return false;
                }

                modalShow($("#template"), function (obj) {
                    obj.find(".unsubscript_email").click(function () {
                        $.post("/api/participation.ashx?t=unsubscript&type=email", function (r) {
                            if (r.success) {
                                alert("구독 해지가 완료되었습니다.");
                                location.href = "/participation/nk/shalom/subscription";
                            } else {
                                alert(r.message);
                            }
                        });

                        return false;
                    });

                    obj.find(".unsubscript_sms").click(function () {
                        $.post("/api/participation.ashx?t=unsubscript&type=sms", function (r) {
                            if (r.success) {
                                alert("구독 해지가 완료되었습니다.");
                                location.href = "/participation/nk/shalom/subscription";
                            } else {
                                alert(r.message);
                            }
                        });

                        return false;
                    });

                    obj.find(".unsubscript_all").click(function () {
                        $.post("/api/participation.ashx?t=unsubscript&type=all", function (r) {
                            if (r.success) {
                                alert("구독 해지가 완료되었습니다.");
                                location.href = "/participation/nk/shalom/subscription";
                            } else {
                                alert(r.message);
                            }
                        });

                        return false;
                    });

                }, true, false);
            });
        });


        function onSubmit() {
            
            if ($("#phoneCheck").val() == "") {
                alert("회원 정보에 휴대폰 번호를 등록해 주세요.");
                return false;
            }

            if (!$("#agree").is(":checked")) {
                $("#msg_check").html("신청 약관에 동의하셔야 합니다.").show();
                $("#agree").focus();
                return false;
            }
        }

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" runat="server" id="tabValue" value="Y"/>
    <input type="hidden" runat="server" id="phoneCheck"/>
    
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>샬롬기도운동 구독하기</em></h1>
				<span class="desc">원하시는 채널을 통해 이 주의 기도제목을 받아 보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents part">

			<div class="north">
				<div class="w980 subscribe">

					<!-- 탭메뉴 -->
                    <!--
					<ul class="tab_type1 mb60">
						<li style="width:50%" class="on" id="btn_sns" ><a href="# ID="btnSns" >SMS 구독</a></li>
						<li style="width:50%" id="btn_mail"><a href="# ID="btnMail" >이메일 구독</a></li>
					</ul>
                        -->
					<!--// 탭메뉴 -->
     
					<div class="box_type3">
						<p class="mobile" id="tabSms" runat="server" ><span>SMS 구독</span><asp:Literal runat="server" ID="phone" /></p>
                        <p class="mobile" id="tabMail" runat="server" style="display:none"><span>이메일 주소</span><asp:Literal runat="server" ID="email"/></p>
						<span class="s_con1 mb50">입력된 개인정보가 일치하지 않는 경우, <a href="/my/account/" class="fc_blue">마이컴패션 > 개인정보 수정</a>으로 이동하여 변경해주시기 바랍니다.</span>
      
						<div class="agree">
							<span class="checkbox_ui">
								<input type="checkbox" class="css_checkbox" id="agree"  />
								<label for="agree" class="css_label font2">기도문 업데이트와 관련된 메시지 발송을 통한 기관의 서비스 이용 안내 및 홍보 등의 마케팅 활동에 동의합니다.</label>
							</span>
							<p class="pt10"><span class="guide_comment2" id="msg_check" style="display:none">신청 약관에 동의하셔야 합니다.</span></p>
						</div>
                        <asp:LinkButton class="btn_type1" runat="server" ID="btnSubscription" OnClick="btnSubscription_Click" OnClientClick="return onSubmit();">구독 신청</asp:LinkButton>
					</div>
					

					<div class="tar">
						<a href="#" class="btn_subscribe" ng-click="modal.show()">구독을 해지하고 싶으신가요?</a>
					</div>

				</div>
			</div>

			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->

		

    </section>
    <!--// sub body -->

    
</asp:Content>