﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;
using System.Web.Configuration;

public partial class participation_nk_shalom_viewPicture : FrontBasePage
{
    const string listPath = "/participation/nk/shalom/list/";

    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();
        
        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1)
        {
            Response.Redirect(listPath, true);
        }
        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);

        if (!requests[0].CheckNumeric())
        {
            Response.Redirect(listPath, true);
        }

        base.PrimaryKey = requests[0];


        pp_seq.Value = PrimaryKey.ToString();

        btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();


    }


    protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.TB_PRAYER_DOC.First(p => p.PD_SEQ == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<TB_PRAYER_PICTURE>("PP_SEQ", Convert.ToInt32(PrimaryKey));

            title.Text = entity.PP_SUBJECT;
            reg_date.Text = Convert.ToDateTime(entity.PP_DATE).ToString("yyyy.MM.dd");
            view_count.Text = entity.PP_VIEW_CNT == null ? "0" : ((int)entity.PP_VIEW_CNT).ToString("N0");
            //article.Text = entity.PP_CONTENT;
            this.ViewState["pictureSrc"] = entity.PP_CONTENT.Replace("https://youtu.be/", "https://www.youtube.com/embed/");

            pray_cnt.InnerText = entity.PP_PRAYER_CNT == null ? "0" : ((int)entity.PP_PRAYER_CNT).ToString("N0");

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-샬롬기도운동 기도영상] {0}", entity.PP_SUBJECT);
            
        }
    }
}