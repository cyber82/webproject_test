﻿
(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http) {

	    $scope.list = [];
	    $scope.churchList = [];

        // list
        $scope.getList = function (params) {

        	$http.get("/api/participation.ashx?t=partnership_list&location="+$("#location").val()).success(function (result) {
        	    $scope.list = result.data;

        	    var doubleArray = [];
        	    var total = result.data.length > 0 ? result.data[0].total : 0;

        	    var table = Math.ceil(total / 5);
        	    var index = 0;

        	    for (var i = 0; i < table; i++) {
        	    	doubleArray[i] = $scope.list.slice(index, index += 5);
        	    }

        	    $scope.churchList = doubleArray;
				
        	    //console.log("토탈",total)
        	    //console.log("테이블", table)
        	    //console.log("리스트",$scope.list)
        	    //console.log("철치",$scope.churchList)
                        	          	    
        	});
      
        }
        
        $scope.getList();
        
	});

})();