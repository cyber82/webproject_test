﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class participation_nk_church_partnership_default : FrontBasePage {


	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);


		foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "location_type").OrderBy(p => p.cd_order)) {
			location.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}

	}


	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
	}



}