﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="participation_nk_church_partnership_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

	<script type="text/javascript" src="/participation/nk/church/partnership/default.js"></script>
    <script type="text/javascript">
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션북한사역 파트너십 소개</em></h1>
				<span class="desc">북한에 어린이센터를 운영할 파트너교회가 되어주세요</span>


                <uc:breadcrumb runat="server"/>

			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 part">
			<div class="partner_movie">
				<div class="w980">
					<iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/TXpSaA5vCH4?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>

			<div class="north">

				<div class="w980">

					<!-- 교회와 컴패션의 파트너십 -->
					<div class="partnership">
						<div class="intro">
							<p class="tit">교회와 컴패션의 파트너십</p>
							<p class="con1">
								하나님의 부르심을 받은 교회와 컴패션이 파트너십을 통하여 북한어린이의 전인적 양육이라는<br />
								공동의 목표를 위해 각자의 역할에 최선을 다하고 서로 돕습니다.
							</p>
							<p class="con2 clear2">
								<span class="div">
									<span class="slogan"><em>사명</em>MISSION</span>
									<span class="desc">
										예수님의 이름으로 북한어린이를 전인적으로 양육하여<br />
										이 땅에 샬롬을 이루고 하나님의 이름을 영화롭게 합니다.
									</span>
								</span>
								<span class="div">
									<span class="slogan"><em>기본 가치</em>CORE VALUES</span>
									<span class="desc">
										상호 존중과 상호 이익<br />
										(Mutual Benefit & Mutual Respect)
									</span>
								</span>
							</p>
						</div>

					</div>
					<!--// 교회와 컴패션의 파트너십 -->

				</div>

				<!-- 파트터십 모델 -->
				<div class="ps_model">
					<div class="w980">

						<p class="s_tit7 tac mb30">파트너십 모델_MODEL</p>

						<div class="box_type1 tab clear2">
							<span>교회</span>
							<span>컴패션</span>
						</div>

						<div class="list_con clear2">
							<div class="div2">
								<p class="desc">
									교회는 하나님께서 허락하시는 때에 북한에 1개 이상의<br />
									어린이센터를 세우고 운영할 것을 약속하며 이 사역에<br />
									주인의식을 가지고 인적, 재정적 준비에 임합니다.
								</p>

								<div class="list">
									<p class="tit">인적 준비</p>
									<ul class="mb40">
										<li>- 교회 내 북한사역 준비위원회 출범</li>
										<li>
											- 준비위원장 및 부위원장 임명<br />
											<span class="pl25">ㄴ교역자 또는 평신도 리더</span>
										</li>
										<li>
											- 최소 5명의 준비팀 양성<br />
											<span class="pl25">ㄴ어린이센터 운영 인력</span><br />
											<span class="pl25">ㄴ전인적어린이양육 전문가 및 리더십 자격증 소지자</span>
										</li>
										<li>- 헌신된 목회자/성도의 사역훈련 참여 독려</li>
										<li>- 샬롬기도회를 통한 기도 운동 참여</li>
										<li>- 남한어린이 전인적양육 프로그램 도입 (선택)</li>
									</ul>

									<p class="tit">재정적 준비</p>
									<ul class="margin0">
										<li>- 북한준비기금 모금액 약정</li>
										<li>- 약정기금 마련에 대한 재정 계획 수립 및 연간 계획에 반영</li>
										<li>
											<span class="s_con1">
												북한준비기금이란 1개의 어린이센터 설립과 3개월 어린이양육<br />
												등에 필요한 사역준비금입니다.
											</span>
										</li>
									</ul>
								</div>
							</div>
							<div class="div2">
								<p class="desc">
									컴패션은 이 사역의 주인은 교회임을 기억하며<br />
									교회가 온전히 준비될 수 있도록 필요한 제반<br />
									서비스를 만들고 제공합니다.
								</p>

								<div class="list">
									<p class="tit">인적 준비</p>
									<ul class="mb40">
										<li>- 준비위원회 컨설팅 및 교육</li>
										<li>- 교회 캠페인 지원</li>
										<li>- 사역훈련 제공</li>
										<li>- 샬롬기도운동 지원</li>
										<li>- 남한어린이 전인적양육 프로그램 인재 양성</li>
									</ul>

									<p class="tit">재정적 준비</p>
									<ul class="mb40">
										<li>- 교회의 준비를 위한 서비스 개발 및 실행 비용 부담</li>
										<li>- 교회에서 위탁한 북한사역준비 기금 대리 관리</li>
									</ul>

									<p class="tit">프로그램적 준비</p>
									<ul class="margin0">
										<li>- 어린이센터 수립 전략 개발</li>
										<li>- 북한어린이를 위한 프로그램 및 교재 개발</li>
										<li>- 남한어린이 전인적양육 프로그램 개발</li>
									</ul>
								</div>
							</div>
						</div>

					</div>
				</div>
				<!--// 파트터십 모델 -->

				<!-- 파트터십 사이클 -->
				<div class="ps_cycle">
					<div class="w980">
						<p class="s_tit7 tac mb20">파트너십 사이클_CYCLE</p>
						<p class="s_con3 tac mb40">파트너십은 지속적으로 발전합니다. 공동의 목표를 위해 전진하고 그 과정 가운데 서로의 성장을 돕습니다.</p>
						<div class="bg">
							<ul>
								<li><span>협약식</span></li>
								<li><span>비전 예배</span></li>
								<li><span>준비위원회</span></li>
								<li><span>사역 훈련</span></li>
								<li><span>북한준비기금</span></li>
								<li><span class="line3">연간 리더십<br />및 준비위원회<br />모임</span></li>
								<li><span>비전 캠페인</span></li>
								<li><span>상호 점검</span></li>
								<li><span>협약 갱신</span></li>
							</ul>
							<span class="txt">3년 주기</span>
						</div>
					</div>
				</div>
				<!--// 파트터십 사이클 -->

				<!-- 파트터십 현황 -->
				<div class="ps_present">

					<div class="map">
						<p class="tit">컴패션북한사역 파트너십 현황</p>
					</div>

					<div class="w980 tar">
						<span class="sel_type3 mb20" style="width:140px">
                            <label for="location" class="hidden">지역 선택</label>

                            <select id="location" runat="server" ng-model="model" ng-change="getList()" class="custom_sel">
                                <option value="">지역전체</option>
                            </select>

                        </span>
					</div>

					<div class="w980">
						<div class="tableWrap1 mb60">
							<table class="tbl_type3">
								<caption>컴패션북한사역 파트너십 현황 테이블</caption>
								<colgroup>
									<col style="width:20%" />
									<col style="width:20%" />
									<col style="width:20%" />
									<col style="width:20%" />
									<col style="width:20%" />
								</colgroup>
								<tbody>

									<tr ng-repeat="list in churchList">
										<td  ng-repeat="item in list">
                                        
											<a id="church_list" href="{{item.p_url}}" target="_blank" ng-show="item.p_url">{{item.p_name}}</a>
											<span ng-hide="item.p_url">{{item.p_name}}</span>
                                       
										</td>
                                    </tr>
                                    <tr ng-hide="list.length">
                                        <td colspan="5" >데이터가 없습니다.</td>
                                    </tr>

                                    <!--
                                    <tr>
										<td><a href="#" target="_blank">갈보리교회</a></td>
										<td><a href="#" target="_blank">강북대학연합교회</a></td>
										<td><a href="#" target="_blank">과천교회</a></td>
										<td><a href="#" target="_blank">꿈의교회</a></td>
										<td><a href="#" target="_blank">다움교회</a></td>
									</tr>
									<tr>
										<td><a href="#" target="_blank">갈보리교회</a></td>
										<td><a href="#" target="_blank">과천교회</a></td>
										<td><a href="#" target="_blank">강북대학연합교회</a></td>
										<td><a href="#" target="_blank">동상제일교회</a></td>
										<td><a href="#" target="_blank">다움교회</a></td>
									</tr>
									<tr>
										<td><a href="#" target="_blank">강북대학연합교회</a></td>
										<td><a href="#" target="_blank">갈보리교회</a></td>
										<td><a href="#" target="_blank">과천교회</a></td>
										<td><a href="#" target="_blank">꿈의교회</a></td>
										<td><a href="#" target="_blank">다움교회</a></td>
									</tr>
									<tr>
										<td><a href="#" target="_blank">갈보리교회</a></td>
										<td><a href="#" target="_blank">과천교회</a></td>
										<td><a href="#" target="_blank">강북대학연합교회</a></td>
										<td><a href="#" target="_blank">동상제일교회</a></td>
										<td><a href="#" target="_blank">다움교회</a></td>
									</tr>
									<tr>
										<td><a href="#" target="_blank">갈보리교회</a></td>
										<td><a href="#" target="_blank">강북대학연합교회</a></td>
										<td><a href="#" target="_blank">과천교회</a></td>
										<td><a href="#" target="_blank">꿈의교회</a></td>
										<td><a href="#" target="_blank">다움교회</a></td>
									</tr>
									<tr>
										<td><a href="#" target="_blank">갈보리교회</a></td>
										<td><a href="#" target="_blank">과천교회</a></td>
										<td><a href="#" target="_blank">강북대학연합교회</a></td>
										<td><a href="#" target="_blank">다움교회</a></td>
										<td><a href="#" target="_blank">동상제일교회</a></td>
									</tr>
									<tr>
										<td><a href="#" target="_blank">갈보리교회</a></td>
										<td><a href="#" target="_blank">강북대학연합교회</a></td>
										<td><a href="#" target="_blank">과천교회</a></td>
										<td><a href="#" target="_blank">꿈의교회</a></td>
										<td><a href="#" target="_blank">다움교회</a></td>
									</tr>
									<tr>
										<td><a href="#" target="_blank">갈보리교회</a></td>
										<td><a href="#" target="_blank">과천교회</a></td>
										<td><a href="#" target="_blank">강북대학연합교회</a></td>
										<td><a href="#" target="_blank">동상제일교회</a></td>
										<td><a href="#" target="_blank">다움교회</a></td>
									</tr>
				                        -->
								</tbody>
							</table>
						</div>

						<div class="contact_qna">
							<p class="tit">문의</p>
							<div class="clear2">
								<div class="tel"><span class="icon"><span class="txt">전화 : </span>02) 3668-3517</span></div>
								<div class="email"><span class="icon"><span class="txt">이메일 : </span><a href="mailto:nkhope@compassion.or.kr">nkhope@compassion.or.kr</a></span></div>
							</div>
						</div>

						<div class="tar"><a href="/common/download/2016 북한사역브로슈어_201609_web.pdf" target="_blank" class="btn_s_type4">컴패션북한사역 파트너십 브로셔 다운로드</a></div>
					</div>
				</div>
				<!--// 파트터십 현황 -->

			</div>
			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->

       






    </section>
    <!--// sub body -->
        <div>

    </div>
</asp:Content>