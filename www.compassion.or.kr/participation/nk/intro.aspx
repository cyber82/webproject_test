﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="intro.aspx.cs" Inherits="participation_nk_intro" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script>
        $(function () {

        })
    </script>
    <script type="text/javascript">
        

        (function () {

            var app = angular.module('cps.page', []);

            app.controller("defaultCtrl", function ($scope, popup) {

                // 팝업
                $scope.modal = {
                    instance: null,

                    show: function ($event) {
                        popup.init($scope, "/participation/nk/summit", function (modal) {
                            $scope.modal.instance = modal;
                            $scope.modal.instance.show();
                        }, { top: 0, iscroll: true, removeWhenClose: true });

                        $event.preventDefault();

                    },

                    close: function ($event) {
                        $event.preventDefault();
                        $scope.modal.instance.hide();

                    },
                }

             
            });

        })();


    </script>
</asp:Content>



<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">



    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>컴패션북한사역 소개</em></h1>
                <span class="desc">북한어린이를 향한 소망의 여정으로 초대합니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 part">

            <div class="bgContent visual_northIntro">
                <div class="w980">
                    <span class="bar"></span>
                    <p class="con">
                        <span>컴패션은 지난 60여 년 동안 도움이 필요한 어린이들에게 하나님께서 품으시는 함께 아파하는 마음으로<br />
                            전 세계 교회와 협력하여 1:1 어린이 양육에 주력해 왔습니다. 그리고 이제 북한 어린이를 품기 원하시는 하나님의 마음에 응답하고자 합니다.
                        </span>
                    </p>
                </div>
            </div>

            <div class="north">

                <div class="w980 intro">
                    <div class="con1">
                        <h2>컴패션북한사역</h2>
                        <iframe width="980" height="550" title="영상" class="mb50" src="https://www.youtube.com/embed/VbYXXtAUm6Q?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
                        <p class="desc">
                            <span>
                                <em>컴패션의 북한사역</em>은, 교회와 함께 가난한 어린이에게 전인적인 양육을 제공하는 것이 허용되는 때를 위하여 미리 준비하는 사역으로,<br />
                                비전을 함께하는 교회와 합력하여 북한어린이를 위한 전인적양육 프로그램과 운영 시스템, 전문역량을 개발하고 기도로 마음을 모읍니다.
                            </span>
                            <br />
                            <span>
                                <em>북한과 북한어린이</em>를 위한 한국교회의 헌신과 눈물, 기도와 컴패션의 축적된 어린이 양육 경험과 지식이 합해질 때,<br />
                                하나님의 북한을 향한 소망이 이루어질 것을 믿습니다.
                            </span>
                            <br />
                            <span>
                                <em>하나님</em>이 허락하실 때, 파트너교회와 컴패션이 북한에 어린이센터를 세우고, 25개국에서 현지교회와 협력하여<br />
                                어린이를 양육하듯이 북한어린이를 전인적으로 양육할 것입니다. 그리하여 북한어린이가 남북한이 하나되는 그 시대에<br />
                                성경적 가치관을 가진 책임감 있고 영향력 있는 샬롬세대로 성장할 수 있도록 섬길 것입니다.
                            </span>
                            <br />
                        </p>
                        <a href="/common/download/2016 북한사역브로슈어_201609_web.pdf" class="btn_s_type2 mt5">컴패션북한사역 브로셔 다운로드</a>
                    </div>
                    <div class="con2">
                        <p class="s_tit6 mt50">주제 말씀</p>
                        <div class="txt_box">
                            <p class="txt">
                                너희는 세상의 빛이라... 이같이 너희 빛이 사람 앞에 비치게 하여 그들로 너희 착한 행실을 보고 하늘에 계신 너희 아버지께 영광을 돌리게 하라<br />
                                <span class="fc_gray">(마태복음 5:14, 16)</span>
                            </p>
                        </div>
                        <p class="desc">
                            가난한 어린이를 돕는 일은 크리스천으로서 마땅히 실천해야 할 '착한 행실'이며 말씀에는 이로써 하나님 아버지께 영광을 돌리게 하라고 명하고 있습니다.<br />
                            컴패션북한사역은 북한을 섬기기 위해 우리가 먼저 세상의 빛으로 서서 하나님께 영광을 돌릴 수 있기를 기대합니다. 따라서 북한어린이 양육을 미리 준비하는<br />
                            지금의 과정에서부터 교회와 함께 출발하였고, 앞으로도 교회와 긴밀하게 협력하여 하나님의 사랑 안에 먼저 하나되기를 원합니다. 또한 북한에서 전인적어린이<br />
                            양육이 가능해졌을 때, 세상에 빛으로 하나된 그 모습으로 어린이를 돌볼 수 있기를 소망합니다.  
                        </p>
                    </div>
                    <p class="s_tit6 mt50">표어</p>
                </div>

                <div class="slogan_bg">
                    <div class="w980">
                        <span class="slogan"></span>
                        <p class="txt">
                            컴패션은 전 세계 6,900개 이상의 교회를 통해 어린이센터를 운영하고 가난한 어린이들에게 전인적인 양육을 제공합니다. 그리스도는 그의 사명을 완수하기
							위해 교회를 세웠으며(마태복음 16:18), 성경은 이 목적을 위한 다른 어떤 기관도 언급하지 않았고, 교회만큼 사랑으로 어린이를 양육할 수 있는 곳이 없다는
							컴패션의 믿음은 북한에서의 전인적어린이양육 사역을 준비함에 있어서도 변하지 않습니다. 북한을 향한 하나님의 소망이 교회를 통해 이루어질 것을 기대하고
							준비합니다.
                        </p>
                    </div>
                </div>

                <div class="w980 prepare tac">
                    <p class="s_tit6 mt50">준비 영역</p>
                    <p class="s_con3 mt30">
                        북한이 기독교와 교회에 문을 열고 어린이들에게 기독교적 양육을 허용하는 때에, 한국교회와 한인 디아스포라 교회와 성경적인 파트너십 안에서<br />
                        북한 어린이를 섬길 수 있도록 체계적이고 투명하며 전문적인 양육 방법과 비전을 제시하고 실질적인 준비를 합니다.
                    </p>
                    <ul class="bg clear2">
                        <li>
                            <div class="alph">
                                <img src="/common/img/page/participation/prepare_img1.png" alt="H" />
                            </div>
                            <div class="tit_area">
                                <span class="s_tit5">전인적 어린이양육프로그램</span><br />
                                <span class="txt">HOLISTIC CHILD DEVELOPMENT PROGRAM</span>
                            </div>
                            <p class="s_con3">
                                - 북한어린이 니즈 연구<br />
                                - 북한어린이 양육프로그램 및 교재<br />
                                - 남한어린이 양육프로그램 및 교재
                            </p>
                        </li>
                        <li>
                            <div class="alph">
                                <img src="/common/img/page/participation/prepare_img2.png" alt="O" />
                            </div>
                            <div class="tit_area">
                                <span class="s_tit5">어린이센터 수립전략 및 운영 시스템</span><br />
                                <span class="txt">OPERATION SYSTEM</span>
                            </div>
                            <p class="s_con3">
                                - 개방시나리오 연구<br />
                                - 어린이센터수립전략<br />
                                - 조직 및 운영 계획
                            </p>
                        </li>
                        <li>
                            <div class="alph">
                                <img src="/common/img/page/participation/prepare_img3.png" alt="P" />
                            </div>
                            <div class="tit_area">
                                <span class="s_tit5">기도운동</span><br />
                                <span class="txt">PRAYER MOVEMENT</span>
                            </div>
                            <p class="s_con3">
                                - 샬롬기도운동
                            </p>
                        </li>
                        <li>
                            <div class="alph">
                                <img src="/common/img/page/participation/prepare_img4.png" alt="E" />
                            </div>
                            <div class="tit_area">
                                <span class="s_tit5">비전캐스팅 및 사역훈련</span><br />
                                <span class="txt">EQUIPPING PARTNER CHURCHES</span>
                            </div>
                            <p class="s_con3">
                                - 비전캐스팅<br />
                                - 비전나눔 패키지<br />
                                - 2015 북한사역서밋<br />
								- 교회-컴패션 북한사역 파트너십<br />
								- 컴패션 사역훈련
                            </p>
                        </li>
                    </ul>
                </div>

                <div class="bgContent promise mt50">
                    <div class="w980">
                        <p class="tit"><span>북한어린이를 섬기기 위한</span> 컴패션의 약속</p>
                        <p class="con">
                            <span class="desc">컴패션은  교회와 함께 북한 어린이를 전인적으로 양육할 수 있도록 주님이 허락해주실 그 때를 위해 미리 기도하며 준비할 것을 서약합니다.</span>
                            <span>하나. 하나님의 사랑으로 북한을 섬길 것입니다.<br />
                                하나. 가장 도움이 필요한 어린이를 최우선으로 돕겠습니다.<br />
                                하나. 교회와 지속적으로 협력하겠습니다. 이를 통해 교회가 각 가정과 지역을 섬기도록 돕겠습니다.<br />
                                하나. 체계적이고 전인적인 양육 프로그램으로 어린이를 양육합니다.<br />
                                하나. 투명하고 효율적인 운영으로 최선의 결과를 기대합니다.
                            </span>
                        </p>
                    </div>
                </div>

                <div class="w980 roadmap">
                    <h2>로드맵</h2>
                    <div class="bg">
                        <div class="group1">
                            <p class="s_tit5">01. READY 준비</p>
                            <p class="curcle">개방전</p>
                            <ol>
                                <li>
                                    <span class="subject">론칭</span><br />
                                    <span class="content ta_le">
                                        <span class="fc_blue">2013. 8</span><br />
                                        컴패션 북한사역 론칭
                                    </span>
                                </li>
                                <li>
                                    <span class="subject">비전</span><br />
                                    <span class="content ta_ri">
                                        <span class="fc_blue">2014. 9</span><br />
                                        “소망의 땅, 북한:교회가<br />
                                        희망입니다”는<br />
                                        메시지로 한국교회와<br />
                                        한인 디아스포라<br />
                                        교회에 비전을 나눕니다.
                                    </span>
                                </li>
                                <li>
                                    <span class="subject">기도</span><br />
                                    <span class="content ta_le">
                                        <span class="fc_blue">2015. 3</span><br />
                                        북한어린이를 위한<br />
                                        샬롬기도운동 시작
                                    </span>
                                </li>
                                <li>
                                    <span class="subject">서밋</span><br />
                                    <span class="content ta_ri">
                                        <span class="fc_blue">2015 6.8~9</span><br />
                                        2015 북한사역서밋<br />
                                        <span class="fs13">
											국내외 163개 교회<br />
											1,283명 참석<br />
											98개 교회 헌신
										</span>
                                    </span>
                                    <a ng-click="modal.show($event)">북한사역서밋에<br />
                                        대한 자세한 내용이<br />
                                        궁금하신가요?
                                    </a>
                                </li>
                                <li>
									<span class="subject">파트너십</span><br />
									<span class="content ta_le">
										<span class="fc_blue">2016. 5~</span><br />
										교회-컴패션 북한사역<br />
										파트너십 협약 및<br />
										준비위원회 발족<br />
									</span>
								</li>
								<li>
									<span class="subject">훈련</span><br />
									<span class="content ta_ri">
										<span class="fc_blue">2016. 12</span><br />
										컴패션 사역훈련<br />
										<span class="fs13">
											북한어린이를 전인적으로<br />
											양육할 전인적 양육 전문가와<br />
											리더십 양성
										</span>
									</span>
								</li>
								<li>
									<span class="subject">교재 및<br /> 프로그램</span><br />
									<span class="content ta_le">
										<span class="fc_blue">2016. 12</span><br />
										북한어린이<br />
										전인적 양육 교재 및<br />
										프로그램	개발 완료
									</span>
								</li>
                                <li>
                                    <span class="subject">양육</span><br />
                                    <span class="content ta_ri">
                                        <span class="fc_blue">2017. 3~</span><br />
                                        남한어린이를 위한<br />
                                        전인적 양육 프로그램 론칭
                                    </span>
                                </li>
                                <li>
                                    <span class="subject">준비완료</span><br />
                                    <span class="content ta_le">
                                        <span class="fc_blue">2018. 12</span><br />
                                        북한어린이 사역을<br />
                                        위해 헌신되고 훈련된<br />
                                        교회 200개
                                    </span>
                                </li>
                            </ol>
                        </div>
                        <div class="group2">
                            <p class="s_tit5">02. START 시작</p>
                            <p class="curcle">개방후</p>
                            <ol>
                                <li>
                                    <span class="subject">Step 1<br />
                                        협약</span><br />
                                    <span class="content ta_le">
                                        <span class="fc_blue">협약</span><br />
                                        어린이센터에 대한<br />
                                        운영수칙 및 재정 관리에<br />
                                        대한 협약을 진행
                                    </span>
                                </li>
                                <li>
                                    <span class="subject">Step 2<br />
                                        점검</span><br />
                                    <span class="content ta_ri">
                                        <span class="fc_blue">점검</span><br />
                                        파송팀을 선정하고<br />
                                        재교육 실시<br />
                                        북한 현장 조사 및<br />
                                        현지 조율
                                    </span>
                                </li>
                                <li>
                                    <span class="subject">Step 3<br />
                                        파송</span><br />
                                    <span class="content ta_le">
                                        <span class="fc_blue">파송</span><br />
                                        재교육을 완료한 팀의<br />
                                        북한 현지 파송
                                    </span>
                                </li>
                                <li>
                                    <span class="subject">Step 4<br />
                                        설립</span><br />
                                    <span class="content ta_le">
                                        <span class="fc_blue">설립</span><br />
                                        어린이센터를 세우고,<br />
                                        기자재 구입 및 설치
                                    </span>
                                </li>
                                <li>
                                    <span class="subject">Step 5<br />
                                        캠페인</span><br />
                                    <span class="content ta_ri">
                                        <span class="fc_blue">캠페인</span><br />
                                        지역사회를 대상으로<br />
                                        어린이센터와 전인적<br />
                                        양육 프로그램 홍보
                                    </span>
                                </li>
                                <li>
                                    <span class="subject">Step 6<br />
                                        등록</span><br />
                                    <span class="content ta_le">
                                        <span class="fc_blue">등록</span><br />
                                        현장 및 가정 방문을 통하여<br />
                                        그 지역의 어린이 필요를<br />
                                        파악하고 도움이 필요한<br />
                                        어린이를 우선적으로<br />
                                        선별하여 등록
                                    </span>
                                </li>
                                <li>
                                    <span class="subject">Step 7<br />
                                        시작</span><br />
                                    <span class="content ta_ri">
                                        <span class="fc_blue">시작</span><br />
                                        정기적인 양육<br />
                                        프로그램 운영
                                    </span>
                                </li>
                            </ol>
                        </div>
                        <div class="box group3">
                            <p class="s_tit5 mb30">03. EXPAND 도약</p>
                            <p class="desc mb30">
                                <span class="fc_blue">안정화 (개방 후 약 1년 후)</span><br />
                                각 지역의 필요에 맞게 사역을 확장합니다.<br />
                            </p>
                            <span class="s_con8">주변 지역의 더 많은 어린이를 양육</span><br />
                            <span class="s_con8">청소년, 대학생, 장애인 등 특화된 사역으로 확대</span><br />
                            <span class="s_con8">사역자 역량 강화 및 현지 사역자 양성</span>
                        </div>
                        <div class="box group4">
                            <p class="s_tit5 mb30">04. GOAL 자립</p>
                            <p class="desc mb30">
                                <span class="fc_blue">장기화 (5년 이상 후)</span><br />
                                교회가 현지 리소스를 가지고 자립적으로<br />
                                전인적 어린이 양육을 할 수 있을 때를 소망합니다.
                            </p>
                            <span class="s_con8">어린이사역에 대한 교회의 전문성 확립</span><br />
                            <span class="s_con8">축하 및 감사예배</span><br />
                            <span class="s_con8">파트너십 종료 프로세스 진행</span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="h100"></div>

        </div>
        <!--// e: sub contents -->



    </section>
</asp:Content>
