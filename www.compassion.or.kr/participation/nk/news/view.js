﻿(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http) {

		$scope.isLogin = common.isLogin();
		$scope.userId = common.getUserId();

		$scope.total = 0;
		$scope.page = 1;
		$scope.rowsPerPage = 10;


		$scope.list = [];
		$scope.params = {
			id: $("#id").val(),
			page: $scope.page,
			rowsPerPage: $scope.rowsPerPage,
			type: "nk_news"
		};

		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);
			$http.get("/api/comment.ashx?t=list", { params: $scope.params }).success(function (result) {
				if (result.success) {
					$scope.list = result.data;
					$scope.total = result.data.length > 0 ? result.data[0].total : 0;
				}
			});
		}


		$scope.update = function () {
			//console.log($scope.content);
			if ($scope.content == "") {
				alert("댓들을 입력해주세요.");
				$("#reply").focus();
				return false;
			}

			// 신규 등록
			if ($scope.updateId == -1) {
				$http.post("/api/comment.ashx", { t: 'add', id: $("#id").val(), content: $scope.content, type: $scope.params.type }).success(function (result) {
					if (result.success) {
						$scope.getList({ page: 1 });
						$("#count").text(0);
					} else {
						alert(result.message);
					}
				})
			} else {
				$http.post('/api/comment.ashx', { t: 'update', id: $scope.updateId, content: $scope.content }).success(function (result) {
					if (result.success) {
						$scope.getList();
					} else {
						alert(result.message);
					}

					$scope.updateId == -1;
				})
			}

			$scope.content = "";
		}


		$scope.remove = function (id) {
			$http.post("/api/comment.ashx?t=remove&id=" + id).success(function (result) {
				if (result.success) {
					alert("삭제되었습니다.");
					$scope.getList();
				} else {
					alert(result.message);
				}
			});
		}

		$scope.updateId = -1;
		$scope.modify = function (entity) {
			$scope.updateId = entity.c_id;
			$scope.content = entity.c_content
		}

		$scope.getList();
	})
	;

})();