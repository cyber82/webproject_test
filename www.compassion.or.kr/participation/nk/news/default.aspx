﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="participation_nk_news_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/participation/nk/news/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">


    <!-- sub body -->
    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>컴패션북한사역 소식</em></h1>
                <span class="desc">북한사역 소식을 알려드립니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents part" id="l">

            <div class="north">

                <div class="w980 ">
                    <!-- 탭메뉴 -->
                    <ul class="tab_type1 mb40">
                        <li style="width: 50%" class="tap_menu on"><a href="#" ng-click="sorting('news')">새 소식</a></li>
                        <li style="width: 50%" class="tap_menu" id="tap_down"><a href="#" ng-click="sorting('download')">자료실</a></li>
                    </ul>
                    <!--// 탭메뉴 -->

                    <div class="sortWrap clear2 mb20">
                        <div class="fr relative">
                            <label for="k_word" class="hidden">검색어 입력</label>
                            <input type="text" class="input_search1" name="k_word" id="k_word" style="width: 245px" ng-enter="search()" placeholder="검색어를 입력해 주세요" />
                            <a href="#" class="search_area1" ng-click="search()">검색</a>
                        </div>
                    </div>




                    <div class="boardList_2">
                        <!--리스트 -->
                        <ul class="list">
                            <li ng-repeat="item in list">
                                <span class="lst_box">
                                    <span class="whole_box" ng-show="item.b_type == 'nk_hot'">
                                        <span class="whole_notice">[전체공지]</span>
                                        <a class="lst_tit" ng-click="goView(item.b_id)">{{item.b_title}}</a>
                                    </span>
                                    <a ng-show="item.b_type != 'nk_hot'"><span class="lst_tit2" ng-click="goView(item.b_id)">{{item.b_title}}</span></a>
                                    <span class="lst_txt">
                                        <span>{{parseDate(item.b_regdate) | date:'yyyy.MM.dd'}}</span>
                                        <span class="lst_bar"></span>
                                        <span class="lst_hit">{{item.b_hits}}</span>
                                    </span>
                                    <a class="list_link" ng-bind-html="item.b_content | limitHtml: 70" ng-click="goView(item.b_id)"></a>
                                </span>
                            </li>

                            <!-- 썸네일 있는 경우 li에 클래스 "thumb" 추가 -->
                            <!-- 썸네일 이미지 사이즈 : 190 * 120 -->
                            <!--// 
							<li class="thumb">
								<span class="lst_img">
									<span class="img_wh"><img src="/common/img/temp/img_23.jpg" alt="썸네일 이미지" /></span>
								</span>
								<span class="lst_box">
									<a href="#"><span class="lst_tit2">{{item.b_title}}</span></a>
									<span class="lst_txt">
										<span>{{parseDate(item.b_regdate) | date:'yyyy.MM.dd'}}</span>
										<span class="lst_bar"></span>
										<span class="lst_hit">{{item.b_hits}}</span>
									</span>
									<a href="#" class="list_link" ng-bind-html="item.b_content">
										
									</a>
								</span>
							</li>
                                    -->
                            <li ng-if="total == 0" class="no_content">등록된 글이 없습니다.</li>
                        </ul>
                    </div>

                    <!-- page navigation -->
                    <div class="tac mb30">
                        <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>
                    </div>
                    <!--// page navigation -->


                </div>

            </div>
            <div class="h100"></div>

        </div>
        <!--// e: sub contents -->



    </section>
    <!--// sub body -->


</asp:Content>
