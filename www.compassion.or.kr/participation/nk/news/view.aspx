﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="participation_nk_news_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    
    <script type="text/javascript" src="/participation/nk/news/view.js"></script>
    <script type="text/javascript">
    /*    $(function () {
            $("#reply").textCount($("#count"), { limit: 300 });
        });
        */
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" id="id" runat="server" />
        <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
					<h1><em>컴패션북한사역 소식</em></h1>
				<span class="desc">북한사역 소식을 알려드립니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->
        
		<!-- s: sub contents -->
		<div class="subContents part">

			<div class="north">
				<div class="w980">

					<!-- 게시판 상세 -->
					<div class="boardView_1 line1">
						<div class="tit_box noline">
							<span class="tit"><asp:Literal runat="server" ID="title" /></span>
							<span class="txt">
								<span><asp:Literal runat="server" ID="reg_date" /></span>
								<span class="bar"></span>
								<span class="hit"><asp:Literal runat="server" ID="view_count" /></span>
							</span>
						</div>

						<div class="view_contents">
							<asp:Literal runat="server" ID="article" />
						</div>

						<!-- sns 공유하기 -->
						<div class="share_box">
							<span class="sns_ani">
								<span class="common_sns_group">
									<span class="wrap">
										<a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
                                        <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
                                        <a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
                                        <a href="#" title="url 공유" data-role="sns" data-provider="copy"  class="sns_url">url 공유</a>
									</span>
								</span>
								<button class="common_sns_share">공유하기</button>
							</span>
						</div>
						<!--// -->

					</div>
					<!--// 게시판 상세 -->

					<div class="attach_file" Visible="false" runat="server" id="file_down">
						<span>첨부파일 다운로드</span>
                        <div class="file_button">
                            <asp:Repeater runat="server" ID="repeater">
                                <ItemTemplate>
                                    <a href="<%#Eval("f_name") %>" class="btn_s_type4 mr10" target="_blank"><%#Eval("f_display_name")%></a>
                                    
                                </ItemTemplate>
                            </asp:Repeater>
                            <!--
							<a href="#" class="btn_s_type4 mr10">2016년_4월_1일_샬롬기도제목_pdf.pdf</a>
                            <a href="#" class="btn_s_type4 mr10">2016년 4월_1_pdf.pdf</a>
                            <a href="#" class="btn_s_type4 mr10">2016년 4월_1_pdf.pdf</a>
                            <a href="#" class="btn_s_type4 mr10">2016년 4월_1_pptx.pptx</a>
                            <a href="#" class="btn_s_type4 mr10">2016년 4월_1_word.docx</a>
                            -->
                        </div>
					</div>

					<!-- 기본 버튼 -->
					<div class="tar">
                        <a href="#" class="btn_type4 posR" runat="server" id="btnList">목록</a>
					</div>

				</div>
			</div>

			<div class="h100"></div>

		</div>	
		<!--// e: sub contents -->



    <asp:PlaceHolder runat="server" Visible="false">
        <asp:Literal runat="server" ID="sub_title" />

         <!-- 댓글 -->
        댓글 {{total}} <br />
        <span id="count">0</span> / 300자
        <textarea id="reply" name="content" ng-model="content" ng-bind="content"></textarea><a ng-click="update()">등록</a>

        
        <ul>
            <li ng-repeat="item in list" ng-click="goView(item.idx)">
                {{item.c_user_id}}<br />
                {{item.c_content}}<br />
                <a href="#" ng-click="modify(item)" ng-show="item.is_owner">수정</a> | 
                <a href="#" ng-click="remove(item.c_id)" ng-show="item.is_owner">삭제</a>
                {{item.c_regdate | date : 'yyyy-MM-dd HH:mm:ss' }}<br />
            </li>

            <li ng-if="total == 0">데이터가 없습니다.</li>
        </ul>
        <!-- 페이징 -->
		<div class="tac mb60">
			<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  
        </div> 

        <a href="#" runat="server" id="btnPrev">
            이전글
            <asp:Literal runat="server" ID="prevTitle"></asp:Literal>
        </a>
        <br />
        
        <a href="#" runat="server" id="btnNext">
            다음글
            <asp:Literal runat="server" ID="nextTitle"></asp:Literal>
        </a>


        </asp:PlaceHolder>

    
</asp:Content>