﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class participation_nk_news_view : FrontBasePage {

	const string listPath = "/participation/nk/news/";

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		
		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}

		base.PrimaryKey = requests[0];
		id.Value = PrimaryKey.ToString();
		
		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();
		
	}


	protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));

            title.Text = entity.b_title;
            reg_date.Text = entity.b_regdate.ToString("yyyy.MM.dd");
            view_count.Text = entity.b_hits.ToString("N0");
            article.Text = entity.b_content;

            //var fileList = dao.file.Where(p => Convert.ToInt32(p.f_ref_id) == Convert.ToInt32(PrimaryKey)).ToList();
            var fileList = www6.selectQ<file>("f_ref_id", PrimaryKey.ToString());

            foreach (var item in fileList)
            {

                if (string.IsNullOrEmpty(item.f_display_name))
                {
                    item.f_display_name = item.f_name.Replace(Uploader.GetRoot(Uploader.FileGroup.file_nk), "");
                }
                item.f_name = item.f_name.WithFileServerHost();
                //Response.Write(item.f_display_name);
            }

            if (fileList.Count > 0)
            {
                file_down.Visible = true;
            }

            repeater.DataSource = fileList;
            repeater.DataBind();

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-컴패션북한사역 소식] {0}", entity.b_title);


            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = "";
            //this.ViewState["meta_keyword"] = "";
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,FrontBasePage)
            //this.ViewState["meta_image"] = entity.thumb.WithFileServerHost();


        }

            
    }
	
}