﻿$(function () {


    $(".tap_menu").click(function () {

        $(".tap_menu").removeClass("on");
        $(this).addClass("on");

        return false;
    })
    

});

(function () {

    var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {


        $scope.total = -1;

        $scope.list = [];
        $scope.params = {
        	page: 1,
        	rowsPerPage: 10,
        	b_type: 'nk_hot,nk_news'
        };

		// 파라미터 초기화
        $scope.params = $.extend($scope.params, paramService.getParameterValues());
        if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);


        // 검색
        $scope.search = function (params) {
        	$scope.params.page = 1;
        	$scope.params = $.extend($scope.params, params);
        	$scope.params.k_word = $("#k_word").val();
        	$scope.getList();
        }


        // list
        $scope.getList = function (params) {
        	$scope.params = $.extend($scope.params, params);
        	$http.get("/api/board.ashx?t=list", { params: $scope.params }).success(function (result) {
        		$scope.list = result.data;
        		$scope.total = result.data.length > 0 ? result.data[0].total : 0;
                
        
        		//console.log("리스트", $scope.list)
        		//console.log("x", $scope.total)
        	});

        	if ($scope.params.b_type == "nk_hot,nk_download") {
        	    $(".tap_menu").removeClass("on");
        	    $("#tap_down").addClass("on");
        	}

        	if (params) {
        	    scrollTo($("#l"));
        	}
        }

        $scope.sorting = function (type) {
        	if (type == "news") {
        		$scope.params.b_type = "nk_hot,nk_news";
        	} else {
        	    $scope.params.b_type = "nk_hot,nk_download";
        	    $(".tap_menu").removeClass("on");
        	    $("#tap_down").addClass("on");
        	}

        	$scope.params.page = 1;
        	$("#k_word").val("");

        	$scope.search();
        }


        // 상세페이지
        $scope.goView = function (id) {
        	$http.post("/api/board.ashx?t=hits&id=" + id).then().finally(function () {
        		location.href = "/participation/nk/news/view/" + id + "?" + $.param($scope.params);
        	});
        }

        $scope.parseDate = function (datetime) {
        	return new Date(datetime);
        }


        $scope.getList();



    });

})();