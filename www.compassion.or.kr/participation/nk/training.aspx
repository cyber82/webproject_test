﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="training.aspx.cs" Inherits="participation_nk_training" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">




    </script>
</asp:Content>



<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">


    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>컴패션 사역훈련</em></h1>
                <span class="desc">북한어린이를 전인적으로 양육할 전문가로 준비되어주세요</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 part">

            <div class="bgContent ministry"></div>
            <div class="w980" style="margin-bottom:30px;">
                <iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/3t143-2A9PA" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="north tac" style="margin-top: 60px;">

                <div class="mt_intro w980">
					<p class="s_con3">
						교회와 컴패션이 북한의 가난한 어린이들에게 전인적인 양육을 제공하는 것이 허용되는 때를<br />
						대비하여 북한 어린이를 양육할 전인적양육 전문가와 리더십을 양성합니다.
					</p>
                    <div class="kind clear2">
                        <div class="conL">
                            <h2 class="tit">전인적양육전문가</h2>
                            <span class="eng">CHILD DEVELOPMENT WORKER</span>
                            <p class="s_con3">
                                어린이센터에서 어린이를 전인적으로 양육하는 일에<br />
                                전문적인 역량을 가진 어린이센터장, 교사, 사례관리자,<br />
                                회계/경리 등 어린이를 섬기는 모든 사역자를 지칭합니다.
                            </p>
                        </div>
                        <div class="conR">
                            <h2 class="tit">전인적양육리더십</h2>
                            <span class="eng">CHILD DEVELOPMENT LEADERSHIP</span>
                            <p class="s_con3">
                                전인적인 영역에서 어린이를 양육하는 비전을<br />
                                가지고 어린이센터의 주요 의사결정에 참여하는<br />
                                센터위원회의 의장 혹은 위원을 지칭합니다.
                            </p>
                        </div>
                       
                        <a href="http://training.nkcompassion.or.kr/" target="_blank" class="btn_type1" style="margin-top:50px;">사역훈련 홈페이지 바로가기</a>
                        <p class="s_con3" style="margin-top: 10px;">자세한 내용은 사역훈련 홈페이지를 참고 바랍니다.</p>
                    </div>
                    <!-- 프로그램 -->
                    <div class="program">
                        <h2 class="tit">프로그램</h2>
                        <p class="s_con3" style="margin-top: 20px;">
						    사역훈련은 참가자분들이 북한어린이를 섬기기 위한 마음(Heart), 지식(Head),<br />
                            그리고 기술(Hands)이  균형있게 성장하고 준비될 수 있도록 구성되어 있습니다.
					    </p>
                        <img src="../../common/img/page/participation/training1.png" style="display:inline-block; margin-top:30px;" />
                        <img src="../../common/img/page/participation/training2.jpg" style="display:inline-block; margin-top:30px;" />

                        <%--<div class="conBg1">
                            
                            <span class="cert">자격증 수여</span>
                            <ul>
                                <li>
									<span class="c_bl">기초과정</span>
									<span class="desc">- 성경적 배경 및 개념 이해</span>
								</li>
								<li>
									<span class="c_bl">심화과정</span>
									<span class="desc">- 실천적 지식 및 기술</span>
								</li>
								<li>
									<span class="c_bl">응용과정</span>
									<span class="desc">
										- 선택특강<br />
										- 필리핀 러닝트립
									</span>
								</li>
								<li>
									<span class="c_ye">개방 후<br />스타트업 훈련</span>
								</li>
                            </ul>
                        </div>

                        <div class="conBg2">
							<span>연 2회 운영( 상반기 : 3~5월  /  하반기 : 10~12월)</span>
							<span>연 1회 운영 (매년 6~8월)</span>
							<span>개방 후</span>
                        </div>--%>
                    </div>
					
                    <!--// 프로그램 -->
                </div>

            </div>
			<div class="h100"></div>

        </div>
        <!--// e: sub contents -->



    </section>

</asp:Content>
