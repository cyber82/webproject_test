﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="summit.aspx.cs" Inherits="participation_nk_summit" %>

<div style="background:transparent;" class="fn_pop_container" id="childModalview">
<div class="pop_type1 w800 fn_pop_content">
    <div class="pop_title">
        <span>2015 북한사역서밋</span>
        <button class="pop_close" ng-click="modal.close($event)"><span>
            <img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
    </div>

    <div class="pop_content summit">

        <!-- 탭메뉴 -->
        <ul class="tab_type1 summitTab mb40">
            <li style="width: 50%" class="on"><a href="#">스케치 영상</a></li>
            <li style="width: 50%"><a href="#">행사 소개</a></li>
        </ul>
        <!--//-->

        <script type="text/javascript">
            $(function () {
                $(".tab_type1.summitTab > li").click(function () {
                    var idx = $(this).index() + 1;
                    $(".tab_type1.summitTab > li").removeClass("on");
                    $(this).addClass("on");
                    $(".tc").hide();
                    $(".tc" + idx).show();

                    return false;
                });
            })
        </script>
        <script type="text/javascript">
            $(function () {
                $(".tab_type4.tab_in_tab > li").click(function () {
                    var idx = $(this).index() + 1;
                    $(".tab_type4.tab_in_tab > li").removeClass("on");
                    $(this).addClass("on");
                    $(".tintc").hide();
                    $(".tintc" + idx).show();

                    return false;
                });
            })
        </script>

        <!-- 탭메뉴 컨텐츠 -->
        <div class="tab_contents">

            <!-- 스케치영상 -->
            <div class="tc tc1">
                <iframe width="741" height="416" title="영상" src="https://www.youtube.com/embed/_qche4a56EU?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
                <p class="s_con3 mt30">
                    <span class="con_tit fc_blue">Thank You</span><br />
                    <br />
                    하나님의 멈추지 않는 소망의 여정, 2015 북한사역서밋(North Korea Ministry Summit)에<br />
                    함께해주셔서 감사 드립니다. 이번 서밋은 176개 교회 및 기관의 1,283명이 참석하여 하나님께서 열어주실 소망의 땅,<br />
                    북한을 어떻게 바라보고 준비해야 하는지, 하나님의 마음을 나눌 수 있는 뜻 깊은 시간이었습니다.<br />
                    <br />
                    다시 한번 함께해주신 모든 교회의 목사님들과 성도님들께 싶은 감사를 드립니다.<br />
                    <br />
                    한국컴패션 대표 서정인 드림
                </p>
            </div>
            <!--// 스케치영상 -->

            <!-- 행사소개 -->
            <div class="tc tc2" style="display: none">
                <div class="con_top">
                    <span class="img"></span>
                    <p class="con_tit">일시 및 장소</p>
                    <p class="s_con3">
                        2015년 6월 8일(월)-9일(화)<br />
                        더케이호텔서울 그랜드볼룸
                    </p>
                    <p class="con_tit">주제: 3-Firsts</p>
                    <p class="s_con3">
                        First Frontier_소망의 땅을 바라봅니다<br />
                        First Generation_소망의 세대를 품습니다<br />
                        First Action_소망을 향해 일어납니다
                    </p>
                </div>

                <!-- 탭인탭메뉴 -->
                <ul class="tab_type4 tab_in_tab">
                    <li style="width: 50%" class="on"><a href="#">프로그램</a></li>
                    <li style="width: 50%"><a href="#">강사 소개</a></li>
                </ul>
                <!--//-->



                <!-- 탭메뉴 컨텐츠 -->
                <div class="tint_contents">
                    <div class="tintc tintc1">

                        <span class="tbl_tit">Day1</span>
                        <table class="tbl_type10 line_b mb15">
                            <caption>2015북한사역서밋 프로그램</caption>
                            <colgroup>
                                <col width="16%" />
                                <col width="14%" />
                                <col width="39%" />
                                <col width="31%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">시간</th>
                                    <th scope="col" colspan="2" class="deep">프로그램</th>
                                    <th scope="col">강사</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="bg">
                                    <th scope="row">13:30 ~ 15:30</th>
                                    <td>메인 세션 1</td>
                                    <td>First Frontier : 소망의 땅을 바라봅니다</td>
                                    <td>전우택 교수</td>
                                </tr>
                                <tr>
                                    <th scope="row">15:30 ~ 16:00</th>
                                    <td colspan="3" class="tac">휴식</td>
                                </tr>
                                <tr>
                                    <th scope="row" rowspan="4" class="vat">16:00 ~ 17:30</th>
                                    <td rowspan="4" class="vat">분반 세션 1</td>
                                    <td class="vat">트랙 1 : 북한의 미래와 교회의 역할</td>
                                    <td>좌장 : 이규현 목사<br />
                                        발제 : 임성빈 교수<br />
                                        패널토의 : 김병로 교수, 윤영관 교수
                                    </td>
                                </tr>
                                <tr>
                                    <td class="vat">트랙 2 : 독일 통일의 교훈과 적용</td>
                                    <td>좌장 : 조건회 목사<br />
                                        발제 : 주도홍 교수<br />
                                        패널토의 : 노창수 목사
                                    </td>
                                </tr>
                                <tr>
                                    <td class="vat">트랙 3 : 북한사역, 교회 그리고 어린이</td>
                                    <td>좌장 : 김승욱 목사<br />
                                        발제 : Wess Stafford 박사<br />
                                        패널토의 : 양승헌 목사
                                    </td>
                                </tr>
                                <tr>
                                    <td class="vat">트랙 4 : 성경적인 섬김 : 상처 주지 않고 돕는 방법</td>
                                    <td>좌장 : 진재혁 목사<br />
                                        발제 : Brian Fikkert 교수<br />
                                        패널토의 : 장현식 교수
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">17:30 ~ 19:00</th>
                                    <td colspan="3" class="tac">저녁 식사</td>
                                </tr>
                                <tr class="bg">
                                    <th scope="row">19:00 ~ 21:00</th>
                                    <td>메인 세션 2</td>
                                    <td>First Generation : 소망의 세대를 품습니다</td>
                                    <td>Scott Todd 박사</td>
                                </tr>
                            </tbody>
                        </table>

                        <span class="tbl_tit">Day2</span>
                        <table class="tbl_type10 line_b">
                            <caption>2015북한사역서밋 프로그램</caption>
                            <colgroup>
                                <col width="16%" />
                                <col width="14%" />
                                <col width="39%" />
                                <col width="31%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">시간</th>
                                    <th scope="col" colspan="2" class="deep">프로그램</th>
                                    <th scope="col">강사</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="bg">
                                    <th scope="row">10:00 ~ 12:00</th>
                                    <td>메인 세션 3</td>
                                    <td>First Action : 소망을 향해 일어납니다</td>
                                    <td>유기성 목사</td>
                                </tr>
                                <tr>
                                    <th scope="row">12:00 ~ 13:30</th>
                                    <td colspan="3" class="tac">점심 식사</td>
                                </tr>
                                <tr>
                                    <th scope="row" rowspan="4" class="vat">13:30 ~ 14:20</th>
                                    <td rowspan="4" class="vat">분반 세션 2</td>
                                    <td class="vat">액션 1 : [커리큘럼] 북한어린이를 위한 전인적 양육</td>
                                    <td>발제 : 홍민기 목사, 고종률 목사
                                    </td>
                                </tr>
                                <tr>
                                    <td class="vat">액션 2 : [교육] 전인적 양육을 위한 교사양성</td>
                                    <td>발제 : 조세핀김 교수, 조명숙 교감
                                    </td>
                                </tr>
                                <tr>
                                    <td class="vat">액션 3 : [선교] 북한어린이 양육을 위한 사역자훈련</td>
                                    <td>발제 : 조봉희 목사, 허남일 목사
                                    </td>
                                </tr>
                                <tr>
                                    <td class="vat">액션 4 : [동역] 북한사역을 위한 교회의 역할</td>
                                    <td>발제 : 이재훈 목사,<br />
                                        Ricardo Jr. Cosico 디렉터
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">14:20 ~ 14:40</th>
                                    <td colspan="3" class="tac">휴식</td>
                                </tr>
                                <tr>
                                    <th scope="row" rowspan="4" class="vat">14:40 ~ 15:30</th>
                                    <td rowspan="4" class="vat">분반 세션 3</td>
                                    <td class="vat">액션 1 : [커리큘럼] 북한어린이를 위한 전인적 양육</td>
                                    <td>발제 : 홍민기 목사, 고종률 목사
                                    </td>
                                </tr>
                                <tr>
                                    <td class="vat">액션 2 : [교육] 전인적 양육을 위한 교사양성</td>
                                    <td>발제 : 조세핀김 교수, 조명숙 교감
                                    </td>
                                </tr>
                                <tr>
                                    <td class="vat">액션 3 : [선교] 북한어린이 양육을 위한 사역자훈련</td>
                                    <td>발제 : 조봉희 목사, 허남일 목사
                                    </td>
                                </tr>
                                <tr>
                                    <td class="vat">액션 4 : [동역] 북한사역을 위한 교회의 역할</td>
                                    <td>발제 : 이재훈 목사,<br />
                                        Ricardo Jr. Cosico 디렉터
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">15:30 ~ 16:00</th>
                                    <td colspan="3" class="tac">휴식</td>
                                </tr>
                                <tr class="bg">
                                    <th scope="row">16:00 ~ 18:00</th>
                                    <td>피날레 집회</td>
                                    <td>소망의 땅, 북한 : 교회가 희망입니다</td>
                                    <td>홍정길 목사<br />
                                        Santiago "Jimmy" Mellado 총재
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="tintc tintc2" style="display: none;">
						<div class="tableWrap5 mt20">
							<table class="tbl_type10 line_b">
								<caption>2015북한사역서밋 강사소개</caption>
								<colgroup>
									<col width="16%" />
									<col width="40%" />
									<col width="44%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">메인세션 1</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_전우택.png" class="face" alt="전우택" />
											<span>전우택</span>
										</td>
										<td>연세대학교 의학교육학과 교수<br />
											연세의료원 통일보건의료센터 소장<br />
											한반도평화연구원 원장
										</td>
									</tr>
									<tr>
										<th scope="row">메인세션 2</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_ScottTodd.png" class="face" alt="Scott Todd" />
											<span class="p2">Scott Todd<br />
												(스캇 토드)</span>
										</td>
										<td>국제컴패션 글로벌애드보커시 부총재<br />
											전 스탠포드 대학 의학센터 면역학 교수<br />
											Hope Rising (2014) 저자
										</td>
									</tr>
									<tr>
										<th scope="row">메인세션 3</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_유기성.png" class="face" alt="유기성" />
											<span>유기성</span>
										</td>
										<td>선한목자교회 담임목사
										</td>
									</tr>
									<tr>
										<th scope="row" rowspan="2" class="vat">메인세션 4</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_홍정길.png" class="face" alt="홍정길" />
											<span>홍정길</span>
										</td>
										<td>남서울은혜교회 원로목사<br />
											현 신동아학원 이사장<br />
											남북나눔운동 회장
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_JimmyMellado.png" class="face" alt="Santiago Jimmy Mellado" />
											<span class="p2">Santiago "Jimmy" Mellado<br />
												(산티아고 지미 메야도)</span>
										</td>
										<td>국제컴패션 총재<br />
											전 윌로크릭어소시에이션 총재
										</td>
									</tr>
									<tr>
										<th scope="row" rowspan="4" class="vat">분반세션 트랙 1</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_이규현.png" class="face" alt="이규현" />
											<span>이규현</span>
										</td>
										<td>수영로교회 담임목사
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_임성빈.png" class="face" alt="임성빈" />
											<span>임성빈</span>
										</td>
										<td>장로회신학대학교 교수
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_윤영관.png" class="face" alt="윤영관" />
											<span>윤영관</span>
										</td>
										<td>서울대학교 정치외교학부 교수
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_김병로.png" class="face" alt="김병로" />
											<span>김병로</span>
										</td>
										<td>서울대학교 통일평화연구원 교수
										</td>
									</tr>
									<tr>
										<th scope="row" rowspan="3" class="vat">분반세션 트랙 2</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_조건회.png" class="face" alt="조건회" />
											<span>조건회</span>
										</td>
										<td>예능교회 담임목사
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_주도홍.png" class="face" alt="주도홍" />
											<span>주도홍</span>
										</td>
										<td>백석대학교 교수
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_노창수.png" class="face" alt="노창수" />
											<span>노창수</span>
										</td>
										<td>남가주사랑의교회 담임목사
										</td>
									</tr>
									<tr>
										<th scope="row" rowspan="3" class="vat">분반세션 트랙 3</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_김승욱.png" class="face" alt="김승욱" />
											<span>김승욱</span>
										</td>
										<td>할렐루야교회 담임목사
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_WessStafford.png" class="face" alt="Wess Stafford" />
											<span class="p2">Wess Stafford<br />
												(웨스 스태포드)</span>
										</td>
										<td>전 국제컴패션 총재
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_양승헌.png" class="face" alt="양승헌" />
											<span>양승헌</span>
										</td>
										<td>세대로교회 담임목사
										</td>
									</tr>
									<tr>
										<th scope="row" rowspan="3" class="vat">분반세션 트랙 4</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_진재혁.png" class="face" alt="진재혁" />
											<span>진재혁</span>
										</td>
										<td>지구촌교회 담임목사
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_BrianFikkert.png" class="face" alt="Brian Fikkert" />
											<span class="p2">Brian Fikkert<br />
												(브라이언 피커트)</span>
										</td>
										<td>커버넌트 대학교 경제학 교수<br />
											챌머스 경제개발연구소 설립자겸 전무이사<br />
											예일대학교 국제경제 및 경제개발 박사
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_장현식.png" class="face" alt="장현식" />
											<span>장현식</span>
										</td>
										<td>서울대학교 행정대학원 초빙교수
										</td>
									</tr>
									<tr>
										<th scope="row" rowspan="2" class="vat">분반세션 액션 1</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_홍민기.png" class="face" alt="홍민기" />
											<span>홍민기</span>
										</td>
										<td>호산나교회 담임목사
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_고종률.png" class="face" alt="고종률" />
											<span>고종률</span>
										</td>
										<td>파이디온선교회 대표
										</td>
									</tr>
									<tr>
										<th scope="row" rowspan="2" class="vat">분반세션 액션 2</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_조명숙.png" class="face" alt="조명숙" />
											<span>조명숙</span>
										</td>
										<td>여명학교 교감
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_조세핀킴.png" class="face" alt="조세핀킴" />
											<span>조세핀킴</span>
										</td>
										<td>하버드대학교 교육대학원 교수
										</td>
									</tr>
									<tr>
										<th scope="row" rowspan="2" class="vat">분반세션 액션 3</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_허남일.png" class="face" alt="허남일" />
											<span>허남일</span>
										</td>
										<td>한사랑교회 담임목사
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_조봉희.png" class="face" alt="조봉희" />
											<span>조봉희</span>
										</td>
										<td>지구촌교회 담임목사
										</td>
									</tr>
									<tr>
										<th scope="row" rowspan="2" class="vat noline">분반세션 액션 4</th>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_RicardoCosico.png" class="face" alt="Ricardo Cosico" />
											<span class="p2">Ricardo Cosico<br />
												(리카르도 주니어 코시코)</span>
										</td>
										<td>국제컴패션 글로벌애드보커시 교회협력 디렉터
										</td>
									</tr>
									<tr>
										<td>
											<img src="/common/img/page/participation/summit_spker/speakers_이재훈.png" class="face" alt="이재훈" />
											<span>이재훈</span>
										</td>
										<td>온누리교회 담임목사
										</td>
									</tr>
								</tbody>
							</table>
						</div>
                    </div>
                </div>
            </div>
            <!--// 행사소개 -->

        </div>
        <!--// 탭메뉴 컨텐츠 -->

    </div>
</div>

    </div>
