﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="mate.aspx.cs" Inherits="participation_experience_mate" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

</asp:Content>



<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    

    <section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>체험전 메이트 신청</em></h1>
				<span class="desc">한 어린이가 후원자를 만나고 변화되는 기적 같은 이야기를 소개해 주실 체험전 메이트(체험전 운영 자원봉사자)를 모집합니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 part">

			<div class="visual_mate"></div>

			<div class="w980 mate">

				<div class="whatis">
					<p class="tit">컴패션체험전 MATE란?</p>
					<p class="con">
						컴패션체험전 MATE는 컴패션의 비전에 공감하여 자신의 재능과 시간, 열정을 전 세계의<br />
						가난한 어린아이들을 위해 나누는 헌신된 자원봉사자들을 말하며 특히 컴패션체험전의 스텝으로<br />
						체험전 방문자들의 원활한 체험을 돕는 역할을 담당하게 됩니다.
					</p>
				</div>

				<div class="desc">
					<div class="listWrap clear2">
						<div class="list">
							<p class="tit">활동 기간</p>
							<p class="period">2016년 9월 ~ 2017년 2월 (6개월)<br /><span class="fs13">&lowast; 추후 모집 예정</span></p>
							<ul>
								<li>
									<span class="s_con1">
										지역별 세부 봉사 활동 일자 및 시간은 담당자와 협의하여 조정 가능 (일반적으로 오전 /<br />
										오후 / 저녁으로 구분되어 선택 가능하며 종일 스태프로도 참여 가능)
									</span>
								</li>
								<li><span class="s_con1">활동 기간은 이후 연장 가능</span></li>
							</ul>
						</div>
						<div class="list">
							<p class="tit">신청 자격</p>
							<ul class="con2">
								<li>고등학교 재학생 이상 (2016년 1학기 기준)</li>
								<li>컴패션 사역을 이해하고, 자원봉사 활동에 관심 있는 분</li>
								<li>컴패션 후원자 및 컴패션체험전 경험자 우대</li>
							</ul>
						</div>
					</div>
					<div class="listWrap clear2">
						<div class="list">
							<p class="tit">주요 활동 내용</p>
							<ul class="con2">
								<li>체험전 방문자 응대 및 안내 (입장 안내, 결연 안내, 이벤트 안내 등)</li>
								<li>체험전메이트 온라인 커뮤니티를 통한 활발한 활동</li>
								<li>MATE DAY 등 오프라인 행사 참석 (선택 사항)</li>
							</ul>
						</div>
						<div class="list">
							<p class="tit">혜택</p>
							<ul class="con2">
								<li>봉사시간 제공 (* 단, 1365 및 VMS 인증은 불가하오니 유의 바랍니다.)</li>
								<li>체험전 MATE용 티셔츠 제공</li>
								<li>컴패션 각종 행사 시 우선 초대</li>
								<li>누적봉사시간 50시간 달성 시 특별한 선물 증정</li>
							</ul>
						</div>
					</div>

					<div class="tac">
						<a href="http://bit.ly/1SxZaYw" target="_blank" class="btn_type1">체험전 MATE 신청</a>
					</div>
				</div>

				<div class="share">
					<span class="txt">친구들과 체험전 MATE 함께하기</span>
					<!-- sns 공유하기 -->
					<div class="share_box">
						<span class="sns_ani">
							<span class="common_sns_group">
								<span class="wrap">
                                    <a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
                                    <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
                                    <a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
                                    <a href="#" title="url 공유" data-role="sns" data-provider="copy" class="sns_url">url 공유</a>
				
								</span>
							</span>
							<button class="common_sns_share">공유하기</button>
						</span>
					</div>
					<!--// -->
				</div>
				

			</div>


		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>

 </asp:Content>
