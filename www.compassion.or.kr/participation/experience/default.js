﻿(function () {

	var app = angular.module('cps.page', []);

	app.filter('getImage', function () {
		return function (thumb, url) {
			if (thumb) return thumb;
			var id = url.substring(url.lastIndexOf("/") + 1, url.length);
			return "https://img.youtube.com/vi/" + id + "/0.jpg";
		};
	})

	app.controller("defaultCtrl", function ($scope, $http, popup) {

        $scope.list = [];

        // list
        $scope.getList = function (params) {
        	$http.get("/api/participation.ashx?t=promotional_video").success(function (result) {
        	    $scope.list = result.data;
        	    console.log($scope.list)
        	});
        }

        $scope.getList();

        $scope.play = function (url) {
        	console.log(url);
        }

        $scope.modal = {
        	instance: null,
        	show: function (url, $event) {
        		popup.init($scope, "/participation/experience/player.aspx?url=" + url, function (modal) {
        			$scope.modal.instance = modal;
        			modal.show();
        		},{ removeWhenClose: true });
        		$event.preventDefault();
        	},

        	close: function ($event) {
        		if (!$scope.modal.instance)
        			return;
        	    // 영상재상 초기화
        		$("#youtube_frame").attr("src", $("#youtube_frame").attr("src"));
        		$scope.modal.instance.hide();
        		$event.preventDefault();
        		$(".pop_type1").remove();
        	}
        }
        //$scope.modal.init();
	});

})();