﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="participation_visiontrip_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/participation/experience/default.js"></script>
    <script type="text/javascript">
        $(function () {
            
        });
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>컴패션체험전 소개</em></h1>
				<span class="desc">가난에서 희망까지, 기적 같은 이야기를 만나 보세요</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents part">

			<div class="w980 expIntro">
				
				<div class="con1">
					<p class="con_tit">필리핀 쓰레기 마을에 사는 <em>한 어린이의 이야기</em>를 담았습니다.</p>
					<p class="content">
						쓰레기가 산처럼 쌓인 필리핀 세부의 한 쓰레기 마을. 컴패션 어린이 알조(12, 남)가 사는 집은 이 쓰레기 마을 한가운데 있습니다.<br />
						마을 사람들은 매일 언덕에 올라 돈이 될만한 쓰레기를 찾고 아이들은 먹을 만한 음식을 줍습니다. 어느 순간, 알조의 귓가에 이런 목소리가 울리기 시작합니다.<br />
						‘넌 쓰레기 같은 아이야’, ‘네 인생도 이렇게 쓰레기나 줍다가 끝나겠지.’ <em>알조는 이 가난 속에서 어떻게 희망을 찾을 수 있을까요?</em>
					</p>
					<p class="con_tit">한 어린이의 삶에 나타나는 <em>기적같은 이야기</em>를 만나보세요.</p>
					<p class="content">
						컴패션비전트립을 통해 어린이가 살고 있는 수혜국 현지에 다녀오신 분들은 그곳에서 가난을 보았고, 그 가난을 이기는 <br />
						컴패션의 양육을 체험했다고 이야기합니다. 가난으로 인해 꿈을 잃고 살아가던 어린이는 어떻게 절망에서 희망으로 나올 수 있을까요?<br />
						<em>한 어린이가 후원자를 만나고 변화되는 기적같은 이야기! 컴패션 체험전을 통해 가난한 어린이의 삶과 컴패션 양육의 힘을 직접 체험해 보세요.</em>
					</p>
				</div>

			</div>

			<div class="exp_movie">
				<div class="w980"><iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/QvTsPKs3n7E?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe></div>
			</div>

			<div class="bgContent exp_view">
				<div class="w980">
					<p class="tit">가난에서 희망까지 15분</p>
					<span class="bar"></span>
					<p class="con">
						컴패션 체험전에서는 태블릿 PC를 이용해 총 6개의 방으로 구성된 공간에 직접 들어가서 보고, 듣고, 만지고, 느끼며 체험할 수 있습니다.<br />
						가난한 어린이의 삶이 희망으로 변화된 기적 같은 이야기를 15분 내에 경험해 보세요.
					</p>
					<a href="/participation/event?k_word=체험전" class="btn_b_type2">진행 중인 체험전 보기</a>
				</div>
			</div>

			<div class="w980 expIntro">
                
       
				<div class="pr">
                    <p class="tit box-body">체험전 홍보 영상</p>
                    <ul class="clear2 mailbox-attachments clearfix" id="preview">
                        <li ng-repeat="item in list">
                            <!-- 썸네일 이미지 사이즈 : 310 * 175 -->
                            <button class="thumb" style="background:url('{{item.pv_thumb | getImage : item.pv_url}}'); background-size:310px; background-position:0px;" ng-click="modal.show(item.pv_url, $event)">
                                <span>
                                    <img src="/common/img/btn/play_3.png" class="inline" alt="영상보기"/>
                                </span>
                            </button>
                            <span class="thumb_tit elps">{{item.pv_title}}</span>
                        </li>
                        <li ng-if="list != null && !list.length">
                            <span class="no_content">데이터가 없습니다.</span>
                        </li>
                    </ul>
                </div>

				<!-- 체험전 FAQ -->
				<div class="box_type3 faq">
					<p class="tit">체험전 FAQ</p>
					<ul>
						<li>
							<p class="q">체험전은 언제 열리나요?</p>
							<p class="a">
								체험전은 컴패션 본사 및 컴패션과 함께하는 다양한 장소에서 열립니다.<br />
								전시가 진행되는 기간과 시간은 장소에 따라 다르므로 ‘캠페인/이벤트’ 페이지내의 모집글을 통해 자세한 정보를 만나 보세요!
							</p>
						</li>
						<li>
							<p class="q">관람 비용이 있나요?</p>
							<p class="a">체험전은 무료체험입니다. 따로 신청 비용이 없습니다.</p>
						</li>
						<li>
							<p class="q">어떻게 체험하는 건가요?</p>
							<p class="a">
								필리핀의 쓰레기 마을에 사는 컴패션어린이 알조의 삶을 보고, 듣고, 만지며 체험할 수 있습니다.<br />
								태블릿PC와 헤드폰을 통해 영상을 보고, 스토리를 들으며 실제 수혜국 현지의 상황과 유사하게 구성된 공간을 현지에 온 것처럼 생생하게 경험합니다.
							</p>
						</li>
						<li>
							<p class="q">연령에 제한이 있나요?</p>
							<p class="a">
								별도의 연령 제한은 없습니다. 하지만, 5-6세 미만일 경우 체험은 가능하지만 이해도가 낮을 수 있습니다.<br />
								보통 7세 미만일 경우 부모님과 함께 체험할 수 있고 7세 이상이면 각자 태블릿 PC를 가지고 체험할 수 있습니다.
							</p>
						</li>
					</ul>
				</div>
				<!--// 체험전 FAQ -->

			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->



</asp:Content>