﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class participation_experience_player : FrontBasePage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var url = Request["url"].EmptyIfNull();
		if(url != "") {

			// url에서 id추출
			var id = url.Substring(url.LastIndexOf("/"));

			var youtube_url = "https://www.youtube.com/embed/"+ id;
			youtube_frame.Attributes["src"] = youtube_url;
		}
	}
	
}