﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_participation_visiontrip_individual_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false" %>

<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
    <script type="text/javascript">

        $(function () {
            
            // 에디터
            image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
            initEditor(oEditors, "txtTripNotice");

	        if ($("#ddlTripType").val() == "Request") {
	            $(".request_trip_form").show();
	        } else {
	            $(".request_trip_form").hide();
	            //$(".request_trip_form").find("input").val("");
	        }

	        if ("<%:base.Action%>" == "update"){
	            $(".update_form").show();
	        }
	        else{ 
	            $(".insert_form").show();
                
	            $('#txtVisitDate1').datepicker('setStartDate', new Date());
	            $('#txtVisitDate2').datepicker('setStartDate', new Date());
	        }  

             
            $("#btnAdd").click(function () {
                addTextbox();
            });  
                         
            $(".rd_addr").on("ifChanged", function (sender) { 

                var val = $(this).attr("id");
                var checked = $(this).prop("checked");

                if(val == "addr_domestic" && checked){
                    $(".hide_overseas").show();
                    $("#pn_addr_domestic").show();
                    $("#pn_addr_overseas").hide();
                }  else {
                    $(".hide_overseas").hide();
                    $("#pn_addr_domestic").hide();
                    $("#pn_addr_overseas").show();
                }
            });


            if ($("#addr_domestic").prop("checked")) {
                $(".hide_overseas").show();
                $("#pn_addr_domestic").show();
                $("#pn_addr_overseas").hide();
            } else {
                $(".hide_overseas").hide();
                $("#pn_addr_domestic").hide();
                $("#pn_addr_overseas").show();
            }


        //end
	    });

        var onSubmit = function () {
            oEditors.getById["txtTripNotice"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.
            
            var addChild = "";
            $(".childtextbox").each(function(){ 
                var text = $(this).val();
                //alert(text);
                addChild += text+ "^";
            });
            $("#hdnAddText").val(addChild);

            if("<%:base.Action%>" == "update"){
                if (!validateForm([
                    { id: "#ddlTripType", msg: "타입을 선택하세요" },
                    { id: "#ddlVisitCountry", msg: "방문국가를 선택하세요" } 
                ])) {
                    return false;
                } 
            }
            else{
                if (!validateForm([
                    { id: "#hdnSponsorID", msg: "사용자 조회해주세요." },
                    { id: "#txtUserName", msg: "성함을 입력하세요." } ,
                    { id: "#txtTel", msg: "휴대번호를 입력하세요." } ,
                    { id: "#addr1", msg: "주소를 입력하세요." } ,
                    { id: "#addr2", msg: "상세 주소를 입력하세요." } ,
                    { id: "#zipcode", msg: "우편번호를 입력하세요." } ,
                    { id: "#ddlTripType", msg: "방문유형을 선택하세요" },
                    { id: "#ddlVisitCountry", msg: "방문국가를 선택하세요" } 
                ])) {
                    return false;
                } 
            }
            return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
	    } 

        var onRemove = function () {
            return confirm("삭제하시겠습니까?");
        } 

        var txtCount = 0;
        var addTextbox = function() {
            var s = "";
             
            txtCount++;
            s = '<div style="width: 100%; float: left; vertical-align: middle;" class="btnDelete' + txtCount + ' divAddChild"><input type="text" style="width:150px;" class="childtextbox form-control pull-left" id="txtAdd' + txtCount + '">';
            s += '<input type="button" class="btnChilddelete btn btn-default" id="btnDelete' + txtCount + '" value="삭제" onclick="delClick(this)"></div>';
            var div = $("#divChild").html(); 
            $("#divChild").append(s);
            console.log(txtCount);
        }

        var delClick = function (val) {
            var btnID = val.id;

            $("."+btnID).remove(); 
        };
        var delRepeaterClick = function (val) { 
            $("#" + val).hide();
            $("#" + val).find(".hdnDelYN").val("Y");
        };
    
        
        var addrPopup = function () {
            cert_setDomain();
            var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");
        }

        var selectAddressRow;
        function jusoCallback(zipNo, addr1, addr2, jibun) {
            // 실제 저장 데이타
            $("#addr1").val(addr1 + "//" + jibun);
            $("#addr2").val(addr2);
            // 화면에 표시
            $("#zipcode").val(zipNo); 

            $("#addr_road").text("[도로명주소] (" + zipNo + ") " +addr1 + " " +addr2);
            $("#addr_jibun").text("[지번주소] (" + zipNo + ") " + jibun + " " + addr2);

            $("#dspAddrJibun").val(jibun);
            $("#dspAddrDoro").val(addr1);

            $("#addr_domestic_zipcode").val($("#zipcode").val());
            $("#addr_domestic_addr1").val($("#addr1" ).val());
            $("#addr_domestic_addr2").val($("#addr2").val());
        };
        function cert_setDomain() {
            //return;
            //if (location.hostname.startsWith("auth")) return;
            if (location.hostname.indexOf("compassionko.org") > -1)
                document.domain = "compassionko.org";
            else if (location.hostname.indexOf("compassionkr.com") > -1)
                document.domain = "compassionkr.com";
            else
                document.domain = "compassion.or.kr";
        }
    </script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">

            <li runat="server" id="tabm1" class="active">
                <asp:LinkButton runat="server" ID="LinkButton1" CommandArgument="1">기본정보</asp:LinkButton></li>

        </ul>
        <div class="tab-content">

            <div class="active tab-pane" id="tab1" runat="server">
                <div class="form-horizontal">

                    <style type="text/css">
                        #b_display label {
                            margin-left: 5px;
                            width: 50px;
                        }
                    </style>
                    <div class="form-group insert_form"  style="display: none;">
                        <label class="col-sm-2 control-label">사용자정보 조회</label>
                        <div class="col-sm-10">

                            <asp:TextBox runat="server" ID="txtUserID" CssClass="form-control pull-left" Style="width: 150px;" placeholder="ConID를 입력하세요."></asp:TextBox>
                            <asp:Button runat="server" ID="btnUserSearch" Text="검색" CssClass="btn btn-default pull-left" OnClick="btnUserSearch_Click" />
                            <asp:HiddenField runat="server" ID="hdnUserID" />
                            <asp:HiddenField runat="server" ID="hdnSponsorID" />
                            <asp:HiddenField runat="server" ID="hdnConID" />
                            <asp:HiddenField runat="server" ID="hdnGenderCode" />
                            <asp:HiddenField runat="server" ID="hdnBirthDate" />

                        </div>
                    </div>

                    <div runat="server" id="divUserInfo">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">성함</label>
                            <div class="col-sm-10">
                                <asp:TextBox runat="server" ID="txtUserName" CssClass="form-control" Width="400"></asp:TextBox>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">영문성함</label>
                            <div class="col-sm-10">
                                <asp:TextBox runat="server" ID="txtUserNameEng" CssClass="form-control" Width="400"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">휴대번호</label>
                            <div class="col-sm-10">
                                <asp:TextBox runat="server" ID="txtTel" CssClass="form-control" Width="400"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">이메일</label>
                            <div class="col-sm-10">
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" Width="400"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">주소</label>
                            <div class="col-sm-10">
                                <div>
                                    <input type="hidden" runat="server" id="locationType" />
                                    <input type="hidden" runat="server" id="hfAddressType" value="" />

                                    <input type="hidden" runat="server" id="zipcode" />
                                    <input type="hidden" id="addr1" runat="server" />
                                    <input type="hidden" id="addr2" runat="server" />
                                    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
                                    <input type="hidden" runat="server" id="dspAddrDoro" value="" />

                                    <span class="radio_ui">
                                        <input type="radio" runat="server" class="rd_addr css_radio" id="addr_domestic" name="addr_location" />
                                        <label for="addr_domestic" class="css_label">국내</label>

                                        <input type="radio" runat="server" class="rd_addr css_radio" id="addr_oversea" name="addr_location" />
                                        <label for="addr_oversea" class="css_label ml20">해외</label>
                                    </span>

                                    <span id="pn_addr_domestic" runat="server" style="display: none">
                                        <label for="zipcode" class="hidden">주소찾기</label>
                                        <a href="javascript:void(0)" class="btn btn-default" runat="server" id="popup" onclick="addrPopup()">주소찾기</a>
                                        <input type="hidden" id="addr_domestic_zipcode" runat="server" />
                                        <input type="hidden" id="addr_domestic_addr1" runat="server" />
                                        <input type="hidden" id="addr_domestic_addr2" runat="server" />

                                        <p id="addr_road" class="mt15" runat="server"></p>
                                        <p id="addr_jibun" class="mt10" runat="server"></p>
                                    </span>

                                    <!-- 해외주소 체크 시 -->
                                    <div id="pn_addr_overseas" runat="server" style="display: none; width: 400px;">
                                        <span class="sel_type2 fl" style="width: 195px;">
                                            <label for="ddlHouseCountry" class="hidden">국가 선택</label>
                                            <asp:DropDownList runat="server" ID="ddlHouseCountry" class="form-control" Style="width: 195px;"></asp:DropDownList>

                                        </span>
                                        <label for="addr_overseas_zipcode" class="hidden">우편번호</label>

                                        <input type="text" runat="server" id="addr_overseas_zipcode" class="form-control" value="" style="width: 195px" placeholder="우편번호" />
                                        <input type="text" runat="server" id="addr_overseas_addr1" class="form-control" placeholder="주소" style="width: 400px;" />
                                        <input type="text" runat="server" id="addr_overseas_addr2" class="form-control" placeholder="상세주소" style="width: 400px;" />
                                    </div>
                                    <!--// -->
                                </div>

                            </div>
                        </div>

                    </div> 
                    
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">방문유형</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlVisitType" CssClass="form-control" Style="width: 150px;">
                                <asp:ListItem Text="선택하세요" Value=""></asp:ListItem>
                                <asp:ListItem Value="V" Text="어린이센터 방문"></asp:ListItem>
                                <asp:ListItem Value="M" Text="후원어린이 만남"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">방문국가</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlVisitCountry" CssClass="form-control" Width="400" OnSelectedIndexChanged="ddlVisitCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">방문국가 영문명</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtVisitCountryEng" CssClass="form-control" Width="400"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">방문일정1</label>
                        <div class="col-sm-10"> 
							<asp:TextBox runat="server" ID="txtVisitDate1" Width="100px" CssClass="form-control inline date" Style="margin-right: 5px;"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">방문일정2</label>
                        <div class="col-sm-10"> 
							<asp:TextBox runat="server" ID="txtVisitDate2" Width="100px" CssClass="form-control inline date" Style="margin-right: 5px;"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                                <label class="col-sm-2 control-label control-label">후원어린이 만남</label>
                                <div class="col-sm-10">
                                    <asp:CheckBoxList runat="server" ID="chkChildList">
                                    </asp:CheckBoxList> 
                                   <%-- <asp:Button runat="server" ID="btnAdd" OnClientClick="create()" Text="직접입력 추가" /> OnItemCommand="Repeater1_ItemCommand"--%>
                                    <input type="button" id="btnAdd" class="btn btn-default" value="직접 입력 추가" />
                                    <asp:Repeater ID="rpChildList" runat="server" OnItemDataBound="ListBound">
                                        <ItemTemplate>
                                            <div style="width: 100%; float: left; vertical-align: middle;" id='<%# Eval("ChildMeetID") %>'>
                                                <asp:TextBox runat="server" ID="txtChildKey" CssClass="form-control pull-left" Width="150"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hdnChildMeetID" Value='<%# Eval("ChildMeetID") %>' />
                                                <input type="hidden" runat="server" ID="hdnDelYN" Value="N" class="hdnDelYN"/>
                                                <input type="button" value="삭제" class="btn btn-default" onclick="delRepeaterClick('<%# Eval("ChildMeetID") %>')" />
                                             <%--   <asp:Button ID="btnDelChild"
                                                    CommandName="Delete" CommandArgument='<%# Eval("ChildMeetID") %>'
                                                    AlternateText="Delete" CssClass="oDeleteIcon btn btn-default"
                                                    runat="server" Text="삭제" />--%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:HiddenField runat="server" ID="hdnAddText" Value='' />
                                    <asp:PlaceHolder runat="server" ID="phChildList">

                                    </asp:PlaceHolder>
                                    <div id="divChild">

                                    </div>
                        </div>
                    </div> 
                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">후기 보러가기 링크</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtTripReview" CssClass="form-control" Width="400" placeholder="http://" value="http://"></asp:TextBox>
                        </div>
                    </div> 

                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">안내보기</label>
                        <div class="col-sm-10">
                            <textarea name="txtTripNotice" id="txtTripNotice" runat="server" style="width:600px; height:212px;display: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">안내보기 URL</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtTripNoticeDetail" CssClass="form-control" Width="400" placeholder="http://" value="http://"></asp:TextBox>
                        </div>
                    </div> 

                    <asp:PlaceHolder runat="server" ID="ph_regdate" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
                            <div class="col-sm-10" style="margin-top: 7px;">
                                <asp:Label runat="server" ID="reg_date"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>


            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->


    <div class="box-footer clearfix text-center">

        <asp:LinkButton runat="server" ID="btn_remove" OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
        <asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" Style="margin-right: 5px">등록</asp:LinkButton>
        <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
    </div> 
</asp:Content>
