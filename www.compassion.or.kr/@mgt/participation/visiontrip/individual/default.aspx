﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_participation_visiontrip_individual_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        .float_right { float:right; margin-right:10px; }
    </style>
	<script type="text/javascript">
		$(function () {
		});

		function updateTripYear() {

		    var year = $("#a_type").val();

		    alert(year);
            
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    
	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left" style="width:100%;">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
                <div style="float:right; width:360px;">
                    <h3 class="box-title" style="padding-top:5px; float:left; margin-right:10px;">일정페이지 초기 날짜 선택</h3> 
                    <asp:LinkButton runat="server" ID="btnTripYearSet" OnClick="btnTripYearSet_Click"  CssClass="btn btn-bitbucket pull-right float_right">저장</asp:LinkButton>
                    <asp:DropDownList runat="server" ID="a_type" EnableViewState="true" Width="80px" CssClass="form-control float_right"></asp:DropDownList>
                </div>
			</div>

		</div>
		<div class="form-horizontal box-body" >
			<div class="box-body">
                <div class="form-group">
					<label for="s_keyword" class="col-sm-2 control-label">유형별 검색</label>
					<div class="col-sm-10">
						<asp:RadioButtonList runat="server" ID="s_tripType" RepeatDirection="Horizontal" RepeatLayout="Flow" >
						    <asp:ListItem Selected="True"  Value="" Text="전체&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						    <asp:ListItem Value="V" Text="어린이센터 방문&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						    <asp:ListItem Value="M" Text="후원어린이 만남&nbsp;&nbsp;&nbsp;"></asp:ListItem>
					    </asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="s_keyword" class="col-sm-2 control-label">방문국가 검색</label>
					<div class="col-sm-10">
						<asp:TextBox runat="server" ID="s_keyword" Width="415" CssClass="form-control inline"></asp:TextBox>
						<%--* 방문국가--%>
					</div>
				</div>
				<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">기간 검색(일정)</label>
					<div class="col-sm-10">
                        <div>
                            <asp:RadioButtonList runat="server" ID="s_dateType" RepeatDirection="Horizontal" RepeatLayout="Flow" >
						        <asp:ListItem Selected="True"  Value="year" Text="년도&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						        <asp:ListItem Value="direct" Text="날짜선택&nbsp;&nbsp;&nbsp;"></asp:ListItem>
					        </asp:RadioButtonList>
                        </div>
                        <div id="divContainerYear" style="display:block;">
                            <asp:DropDownList runat="server" ID="s_b_year" CssClass="form-control" Width="80px"></asp:DropDownList>
                        </div>
                        <div id="divContainerDirect" style="display:none;">
                            <div class="inline">
							<asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							    ~
						    <asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						    </div>

						    <div class="inline">
							    <button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							    <div class="btn-group">
								    <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								    <a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								    <a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								    <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								    <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								    <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							    </div>
						    </div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                $('#s_dateType').find('ins, label').each(function () {
                                    $(this).bind('click', function () {
                                        var tagName = $(this).prop('tagName');
                                        var o = tagName == 'INS' ? $(this).closest('.iradio_flat-blue') : $(this).prev();
                                        var v = o.find('input').val();
                                        if (v == "year") {
                                            $("#divContainerYear").show();
                                            $("#divContainerDirect").hide();
                                        }
                                        else {
                                            $("#divContainerYear").hide();
                                            $("#divContainerDirect").show();
                                        }
                                    });
                                });

                                $('#s_dateType').find('.iradio_flat-blue').each(function (i) {
                                    var o = (i == 0 ? $("#divContainerYear") : $("#divContainerDirect"));
                                    if ($(this).hasClass('checked')) o.show();
                                    else o.hide();
                                });
                            });
                        </script>
					</div>

				</div>

				<div class="box-footer clearfix text-center">
                    <asp:LinkButton runat="server" ID="btn_search" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
				</div>
				
			</div>
			
		</div>
	</div><!-- /.box -->



    <div class="box box-primary">
		
		 <div class="box-header">
            <h3 class="box-title">총 <asp:Literal runat="server" ID="lbTotal"></asp:Literal> 건</h3>
        </div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding"> 
			<table class="table table-hover table-bordered ">
				<colgroup>
				</colgroup>
				<thead>
					<tr>
					    <th>번호</th>
					    <th>유형</th>
                        <th>방문국가</th>
                        <th>방문국가<br />(영문명)</th>
					    <th>방문일정1</th>
                        <th>방문일정2</th>
					    <th>신청일</th> 
                        <th>안내보기</th> 
					</tr>
				</thead>
				<tbody>
					<asp:Repeater runat="server" ID="repeater" OnItemDataBound="ListBound">
					    <ItemTemplate>
						    <tr onclick="goPage('update.aspx' , { c: '<%#Eval("ApplyID")%>' , p: '<%#paging.CurrentPage %>' , t: 'update' })" class="tr_link">
							    <td><asp:Literal runat=server ID=lbIdx></asp:Literal></td>
							    <td><%#Eval("VisitTypeName")%></td>
                                <td><%#Eval("VisitCountryName")%></td>
                                <td><%#Eval("VisitCountryEng")%></td>
							    <td><%#Eval("VisitDate1")%></td>
							    <td><%#Eval("VisitDate2")%></td>
							    <td><%#Eval("ApplyDate")%></td>  
                                <td><asp:Literal runat="server" ID="ltTripNotice"/></td> 
						    </tr>
					    </ItemTemplate>
					    <FooterTemplate>
						    <tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
							    <td colspan="8">데이터가 없습니다.</td>
						    </tr>
					    </FooterTemplate>
				    </asp:Repeater>
				
				</tbody>
			</table>
		</div>
        
		<div class="box-footer clearfix text-center">
	        <uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
        </div>
        

        <div class="box-footer clearfix text-center">
            <asp:LinkButton runat="server" href="#" ID="btn_add" OnClientClick="goPage('update.aspx' , {t:'insert'})" CssClass="btn btn-bitbucket pull-right">신규등록</asp:LinkButton>
        </div>
	</div>
</asp:Content>