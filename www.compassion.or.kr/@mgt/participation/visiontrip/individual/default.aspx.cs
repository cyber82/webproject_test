﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
using System.Data;

public partial class mgt_participation_visiontrip_individual_default : AdminBoardPage
{

    CommonLib.WWW6Service.SoaHelperSoap _www6Service;

    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();
        base.LoadComplete += new EventHandler(list_LoadComplete);

        v_admin_auth auth = base.GetPageAuth();
        btn_add.Visible = auth.aa_auth_create;
        btn_add.Attributes.Add("href", "update.aspx");

        var selectedYear = StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "trip" && p.cd_key == "individual_year").OrderBy(p => p.cd_order).ToList();
        string year = string.Empty;
        if (selectedYear != null && selectedYear.Count > 0)
            year = selectedYear[0].cd_value.ToString();
        else
            year = DateTime.Now.Year.ToString();

        for (int i = 2005; i <= DateTime.Now.AddYears(1).Year; i++)
        {
            a_type.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        a_type.SelectedValue = year;

        for (int i = 2005; i <= DateTime.Now.AddYears(1).Year; i++)
        {
            s_b_year.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        a_type.SelectedValue = DateTime.Now.Year.ToString();

        //일정페이지 초기 날짜 선택 값 조회바인딩
        s_b_year.SelectedValue = year;
    }

    protected override void OnAfterPostBack()
    {
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();

        btn_add.Attributes.Add("href", "update.aspx");
    }

    protected override void GetList(int page)
    {

        Master.IsSearch = false;
        if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "")
        {
            Master.IsSearch = true;
        }

        string bDate = string.Empty;
        string eDate = string.Empty;

        if (s_dateType.SelectedItem.Value == "year")
        {
            bDate = s_b_year.SelectedValue + "-01-01";
            eDate = s_b_year.SelectedValue + "-12-31";
        }
        else
        {
            bDate = s_b_date.Text;
            eDate = s_e_date.Text;
        }

        //using (AdminDataContext dao = new AdminDataContext()) {

        //var list = dao.sp_visiontrip_list(page, paging.RowsPerPage, "vision,request" , "", s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
        //var list = dao.sp_tVisionTripIndividual_list(page, paging.RowsPerPage, s_tripType.SelectedValue, s_keyword.Text, bDate, eDate).ToList();
        var total = 0;

        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
        string dbName = "SqlCompassionWeb";
        object[] objSql = new object[1] { "sp_tVisionTripIndividual_list" };
        string dbType = "SP";
        object[] objParam = new object[6] { "page", "rowsPerPage", "type", "keyword", "startdate", "enddate" };
        object[] objValue = new object[6] { page, paging.RowsPerPage, s_tripType.SelectedValue, s_keyword.Text, bDate, eDate };
        DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, objParam, objValue);
        List<sp_tVisionTripIndividual_listResult> list = ds.Tables[0].DataTableToList<sp_tVisionTripIndividual_listResult>();

        if (list.Count > 0)
            total = list[0].total.Value;

        lbTotal.Text = total.ToString();

        paging.CurrentPage = page;
        paging.Calculate(total);
        repeater.DataSource = list;
        repeater.DataBind();

        //}
    }

    protected void ListBound(object sender, RepeaterItemEventArgs e)
    {

        if (repeater != null)
        {
            if (e.Item.ItemType != ListItemType.Footer)
            {
                sp_tVisionTripIndividual_listResult entity = e.Item.DataItem as sp_tVisionTripIndividual_listResult;
                ((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();

                //트립안내 등록여부
                ((Literal)e.Item.FindControl("ltTripNotice")).Text = !string.IsNullOrEmpty(entity.TripNotice) ? "O" : "X";

            }
        }

    }

    protected void btnTripYearSet_Click(object sender, EventArgs e)
    {
        //using (MainLibDataContext dao = new MainLibDataContext())
        //{
            _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
            string dbName = "SqlCompassionWeb";
            string dbType = "Text";
            object[] objSql = new object[1] { "UPDATE code set cd_value = '" + a_type.SelectedValue +"' where cd_group = 'trip' and cd_key = 'individual_year'" };
            DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, null, null);
            //List<code> list = ds.Tables[0].DataTableToList<code>();


            //var s = dao.code.First(p => p.cd_group == "trip" && p.cd_key == "individual_year");
            //s.cd_value = a_type.SelectedValue;
            Master.ValueMessage.Value = "저장되었습니다.";
            //dao.SubmitChanges(); 
        //}
    }


}
