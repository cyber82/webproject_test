﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class mgt_participation_visiontrip_individual_update: AdminBoardPage
{

    protected override void OnBeforePostBack()
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        base.OnBeforePostBack();
        v_admin_auth auth = base.GetPageAuth();
        base.BoardType = "notice";
        base.FileGroup = Uploader.FileGroup.file_board;
        base.FileRoot = Uploader.GetRoot(base.FileGroup);
        base.PrimaryKey = Request["c"];
        base.Action = Request["t"];

        btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

        // querystring 유효성 검사 후 문제시 리다이렉트
        var isValid = new RequestValidator()
            .Add("c", RequestValidator.Type.Numeric)
            .Add("t", RequestValidator.Type.Alphabet)
            .Add("s_main", RequestValidator.Type.AlphaNumeric)
            .Validate(this.Context, "default.aspx");

        var domain = ConfigurationManager.AppSettings["domain"];


        #region 방문국가
        //using (AdminDataContext dao = new AdminDataContext())
        //{
        //    var list = dao.country.OrderBy(p => p.c_name).ToList();
        //    ddlVisitCountry.Items.Add(new ListItem("전체", ""));

        //    foreach (country item in list)
        //    {
        //        ddlVisitCountry.Items.Add(new ListItem(item.c_name, item.c_id.ToString()));
        //    }
        //}

        var list = www6.selectQ<country>("c_name");

        foreach (country item in list)
        {
            ddlVisitCountry.Items.Add(new ListItem(item.c_name, item.c_id.ToString()));
        }

        #endregion

        divUserInfo.Visible = false;

        if (base.Action == "update")
        {
            new ChildAction().MyChildren("", 1, 10000, false);
            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(PrimaryKey));

                //var detail = dao.tVisionTripIndividualDetail.First(p => p.ApplyID == entity.ApplyID);
                var detail = www6.selectQF<tVisionTripIndividualDetail>("ApplyID", entity.ApplyID);

                ddlVisitType.SelectedValue = detail.VisitType.ToString();
                ddlVisitCountry.SelectedValue = detail.VisitCountry;
                txtVisitCountryEng.Text = detail.VisitCountryEng;
                txtVisitDate1.Text = detail.VisitDate1;
                txtVisitDate2.Text = detail.VisitDate2;
                txtTripReview.Text = detail.TripReview;
                txtTripNotice.InnerText = detail.TripNotice;
                txtTripNoticeDetail.Text = detail.TripNoticeDetail;

                reg_date.Text = entity.RegisterDate.ToString();

                //후원어린이 정보
                getSponChildList(entity.SponsorID, entity.ApplyID, "");
                #region MyRegion
                ////후원어린이 정보
                //Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId", "childKey", "orderby", "checkLetter" };
                //Object[] objValue = new object[] { 1, 10000, entity.SponsorID, "", "", 0 };
                //Object[] objSql = new object[] { "sp_web_mychild_list_f" };
                //DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

                ////chkChildList.DataSource = dt;
                ////chkChildList.DataValueField = "ChildKey";
                ////chkChildList.DataTextField = "Name";
                ////chkChildList.DataBind();

                //foreach (DataRow dr in dt.Rows)
                //{
                //    chkChildList.Items.Add(new ListItem(dr["ChildKey"].ToString() + "" + dr["Name"].ToString(), dr["ChildKey"].ToString()));
                //}

                //var list = dao.tVisionTripChildMeet.Where(p => p.ApplyID == entity.ApplyID && p.CurrentUse == 'Y').ToList();

                //if (list.Count > 0)
                //{
                //    foreach (ListItem item in chkChildList.Items)
                //    {
                //        foreach (tVisionTripChildMeet cm in list)
                //        {
                //            if (item.Value == cm.ChildKey)
                //            {
                //                item.Selected = true;
                //            }
                //        }
                //    }
                //}
                //rpChildList.DataSource = list.Where(p => p.ChildMeetType == 'I');
                //rpChildList.DataBind(); 
                #endregion

            }
            ph_regdate.Visible = true;
            base.FileLoad();
        }
        else
        {
            pn_addr_domestic.Style["display"] = "none";
            pn_addr_overseas.Style["display"] = "none";
            btn_update.Visible = auth.aa_auth_create;
            btn_remove.Visible = false;
        }
    }

    protected void ListBound(object sender, RepeaterItemEventArgs e)
    {
        if (rpChildList != null)
        {
            if (e.Item.ItemType != ListItemType.Footer)
            {
                tVisionTripChildMeet item = e.Item.DataItem as tVisionTripChildMeet;
                if (item.ChildMeetType == 'I')
                {
                    ((TextBox)e.Item.FindControl("txtChildKey")).Text = item.ChildKey;
                }
            }
        }
    }

    protected void Repeater1_ItemCommand(Object Sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            string id = e.CommandArgument.ToString();
            ((TextBox)e.Item.FindControl("txtChildKey")).Visible = false;
            ((Button)e.Item.FindControl("btnDelChild")).Visible = false;
            ((HtmlInputHidden)e.Item.FindControl("hdnDelYN")).Value = "Y";
            // the rest of your code
        }
    }
    TextBox tb;
    Button btn;
    static int i = 0;
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        //string reqstr = Request.Form.ToString();
        //int ID = ((reqstr.Length - reqstr.Replace("txtDynamic", "").Length) / "txtDynamic".Length);

        //int cont = (Convert.ToInt16(hdnAddText.Value) + 1);
        //int i = rpChildList.Items.Count + cont; 
        //for (int j = 0; j <= i; j++)
        //{


        //    tb = new TextBox();
        //    tb.ID = "txtChildAdd"+j.ToString();
        //    tb.CssClass = "form-control pull-left";
        //    tb.Style.Add("width", "150px"); 
        //    phChildList.Controls.Add(tb);
        //    btn = new Button();
        //    btn.Text = "삭제";
        //    btn.ID = "btnDelChild" + j.ToString();
        //    btn.CssClass = "btn btn-default pull-left";
        //    phChildList.Controls.Add(btn);
        //}
        //i++; 

        //HtmlGenericControl divContent = new HtmlGenericControl("divChild");

        //HtmlGenericControl divContent =
        //(HtmlGenericControl)FindControl("divChild");

        //TextBox tb = new TextBox();
        //tb.ID = "testt";
        //divContent.Controls.Add(tb);
        //HtmlGenericControl divcontrol = new HtmlGenericControl();
        //divcontrol.Attributes["class"] = "sxro sx1co";
        //divcontrol.TagName = "div";

        //pnlUserSearch.Controls.Add(divcontrol);
        //Label question = new Label();
        //questionDescription.Text = "text";
        //divcontrol.Controls.Add(question); // add to the new div, not to the panel

        //TextBox tb = new TextBox();
        //tb.ID = "testt"; 
        //divChild.Controls.Add(tb);

    }

    protected override void OnAfterPostBack()
    {
        base.OnAfterPostBack();
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
    }

    protected void btn_update_click(object sender, EventArgs e)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        UserInfo sess = new UserInfo();


        var apply_arg = new tVisionTripApply()
        {
            SponsorID = hdnSponsorID.Value,
            SponsorName = txtUserName.Text,
            SponsorNameEng = txtUserNameEng.Text,
            UserID = hdnUserID.Value,
            GenderCode = hdnGenderCode.Value,
            BirthDate = hdnBirthDate.Value,
            Tel = txtTel.Text,
            Email = txtEmail.Text,
            Address1 = (addr1.Value + "$" + ddlHouseCountry.SelectedValue),
            Address2 = addr2.Value,
            ZipCode = zipcode.Value,
            LocationType = addr_domestic.Checked ? "국내" : "해외",

            RegisterDate = DateTime.Now,
            RegisterID = AdminLoginSession.GetCookie(this.Context).email.ToString(),
            RegisterName = AdminLoginSession.GetCookie(Context).name,

            ModifyDate = DateTime.Now,
            ModifyID = AdminLoginSession.GetCookie(Context).identifier.ToString(),
            ModifyName = AdminLoginSession.GetCookie(Context).name
        };
        var detail_arg = new tVisionTripIndividualDetail()
        {
            VisitType = ddlVisitType.SelectedValue == "V" ? 'V' : 'M',
            VisitCountry = ddlVisitCountry.SelectedValue,
            VisitCountryEng = txtVisitCountryEng.Text,
            VisitDate1 = txtVisitDate1.Text,
            VisitDate2 = txtVisitDate2.Text,
            TripNoticeDetail = txtTripNoticeDetail.Text,
            TripReview = txtTripReview.Text,
            TripNotice = txtTripNotice.InnerHtml.ToHtml(false)
        };

        //sing (FrontDataContext dao = new FrontDataContext())
        //{

        if (base.Action == "update")
        {
            #region update

            //var apply_entity = dao.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(PrimaryKey));
            var apply_entity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(PrimaryKey));


            //var detail_entity = dao.tVisionTripIndividualDetail.First(p => p.ApplyID == Convert.ToInt32(PrimaryKey));
            var detail_entity = www6.selectQF<tVisionTripIndividualDetail>("ApplyID", Convert.ToInt32(PrimaryKey));

            apply_entity.ModifyID = apply_arg.ModifyID;
            apply_entity.ModifyDate = apply_arg.ModifyDate;
            apply_entity.ModifyName = apply_arg.ModifyName;
            www6.update(apply_entity);


            detail_entity.VisitType = detail_arg.VisitType;
            detail_entity.VisitCountry = detail_arg.VisitCountry;
            detail_entity.VisitCountryEng = detail_arg.VisitCountryEng;
            detail_entity.VisitDate1 = detail_arg.VisitDate1;
            detail_entity.TripNoticeDetail = detail_arg.TripNoticeDetail;
            detail_entity.TripReview = detail_arg.TripReview;
            detail_entity.TripNotice = detail_arg.TripNotice;
            www6.update(detail_entity);

            #region 어린이 저장
            var cmList = new List<tVisionTripChildMeet>();
            string addChildText = hdnAddText.Value;
            foreach (string chkey in addChildText.Split('^'))
            {
                if (chkey.Trim() != "")
                {
                    cmList.Add(new tVisionTripChildMeet()
                    {
                        ApplyID = Convert.ToInt32(PrimaryKey),
                        ChildKey = chkey,
                        ChildMeetType = 'I',
                        ChildName = "",
                        CurrentUse = 'Y',
                        RegisterDate = DateTime.Now,
                        RegisterID = AdminLoginSession.GetCookie(Context).identifier.ToString(),
                        RegisterName = AdminLoginSession.GetCookie(Context).name
                    });
                }
            }

            foreach (ListItem item in chkChildList.Items)
            {
                var ChildKey = item.Value;
                //var child = dao.tVisionTripChildMeet.Where(p => p.ChildKey == ChildKey && p.ApplyID == Convert.ToInt32(PrimaryKey) && p.CurrentUse == 'Y').FirstOrDefault();
                var child = www6.selectQF<tVisionTripChildMeet>("ChildKey", ChildKey, "ApplyID", Convert.ToInt32(PrimaryKey), "CurrentUse", "Y");


                if (child != null)
                {
                    child.CurrentUse = item.Selected ? 'Y' : 'N';
                    child.ModifyDate = DateTime.Now;
                    child.ModifyID = AdminLoginSession.GetCookie(Context).identifier.ToString();
                    child.ModifyName = AdminLoginSession.GetCookie(Context).name;
                    
                    www6.update(child);
                }
                else if (child == null && item.Selected)
                {
                    var ChildName = item.Text.Replace(item.Value, "");
                    cmList.Add(new tVisionTripChildMeet()
                    {
                        ApplyID = Convert.ToInt32(PrimaryKey),
                        ChildKey = item.Value,
                        ChildMeetType = 'S',
                        ChildName = ChildName,
                        CurrentUse = 'Y',
                        RegisterDate = DateTime.Now,
                        RegisterID = AdminLoginSession.GetCookie(Context).identifier.ToString(),
                        RegisterName = AdminLoginSession.GetCookie(Context).name
                    });
                }
            }
            #endregion


            foreach (RepeaterItem i in rpChildList.Items)
            {
                TextBox txtChildKey = (TextBox)i.FindControl("txtChildKey");
                HtmlInputHidden hdnDelYN = (HtmlInputHidden)i.FindControl("hdnDelYN");
                HiddenField hdnChildMeetID = (HiddenField)i.FindControl("hdnChildMeetID");

                if (txtChildKey != null)
                {
                    //var lst = dao.tVisionTripChildMeet.Where(p => p.ChildMeetID == Convert.ToInt16(hdnChildMeetID.Value) && p.ApplyID == Convert.ToInt32(PrimaryKey) && p.CurrentUse == 'Y').FirstOrDefault();
                    var lst = www6.selectQF<tVisionTripChildMeet>("ChildMeetID", Convert.ToInt16(hdnChildMeetID.Value), "ApplyID", Convert.ToInt32(PrimaryKey), "CurrentUse", "Y");

                    if (lst != null)
                    {
                        lst.ChildKey = txtChildKey.Text;
                        lst.CurrentUse = hdnDelYN.Value == "Y" ? 'N' : 'Y';
                        lst.ModifyDate = DateTime.Now;
                        lst.ModifyID = AdminLoginSession.GetCookie(Context).identifier.ToString();
                        lst.ModifyName = AdminLoginSession.GetCookie(Context).name;

                        www6.update(lst);
                    }
                }
            }
            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "수정되었습니다.";
            
            //dao.tVisionTripChildMeet.InsertAllOnSubmit(cmList);
            foreach (tVisionTripChildMeet insert in cmList)
            {
                www6.insert(insert);
            }
            //dao.SubmitChanges();

            base.WriteLog(AdminLog.Type.update, string.Format("{0}", detail_entity.ToJson()));
            #endregion
        }
        else
        {
            var vtApplyList = new List<tVisionTripApply>();
            vtApplyList.Add(new tVisionTripApply()
            {
                ApplyType = "Individual",
                SponsorID = apply_arg.SponsorID,
                SponsorName = apply_arg.SponsorName,
                SponsorNameEng = apply_arg.SponsorNameEng,
                UserID = apply_arg.UserID,
                GenderCode = apply_arg.GenderCode,
                BirthDate = apply_arg.BirthDate != "" ? apply_arg.BirthDate.Encrypt() : "",
                Tel = apply_arg.Tel.Encrypt(),
                Email = apply_arg.Email.Encrypt(),
                LocationType = apply_arg.LocationType,
                Address1 = apply_arg.Address1.Encrypt(),
                Address2 = apply_arg.Address2.Encrypt(),
                ZipCode = apply_arg.ZipCode.Encrypt(),
                ChangeYN = 'N',
                ApplyDate = DateTime.Now,
                CompassYN = 'N',
                GroupType = getGroupType(apply_arg.SponsorID),
                CommitmentCount = getCommitmentCount(apply_arg.SponsorID),
                CurrentUse = 'Y',
                RegisterID = apply_arg.RegisterID,
                RegisterName = apply_arg.RegisterName,
                RegisterDate = apply_arg.RegisterDate
            });


            //dao.tVisionTripApply.InsertAllOnSubmit(vtApplyList);
            www6.insert(vtApplyList[0]);
            //dao.SubmitChanges();

            //List<tVisionTripApply> lst = ds_tVisionTripApply.Tables[0].DataTableToList<tVisionTripApply>();
            var exist = www6.selectQ<tVisionTripApply>("ds_tVisionTripApply");

            int iApplyID = 0;
            if (exist.Any())
            {
                iApplyID = exist.First().ApplyID;
            }

            try
            {
                if (iApplyID > 0)
                {

                    var vtIndividualDetail = new List<tVisionTripIndividualDetail>();
                    vtIndividualDetail.Add(new tVisionTripIndividualDetail()
                    {
                        ApplyID = iApplyID,
                        VisitType = detail_arg.VisitType,
                        VisitDate1 = detail_arg.VisitDate1,
                        VisitDate2 = detail_arg.VisitDate2,
                        VisitCountry = detail_arg.VisitCountry,
                        LocalAccommodation = detail_arg.VisitCountryEng,
                        AcceptTerms = "111111"
                    });

                    //dao.tVisionTripIndividualDetail.InsertAllOnSubmit(vtIndividualDetail);
                    www6.insert(vtIndividualDetail[0]);


                    #region 어린이 정보
                    var cmList = new List<tVisionTripChildMeet>();
                    string addChildText = hdnAddText.Value;
                    foreach (string chkey in addChildText.Split('^'))
                    {
                        if (chkey.Trim() != "")
                        {
                            cmList.Add(new tVisionTripChildMeet()
                            {
                                ApplyID = iApplyID,
                                ChildKey = chkey,
                                ChildMeetType = 'I',
                                ChildName = "",
                                CurrentUse = 'Y',
                                RegisterDate = DateTime.Now,
                                RegisterID = AdminLoginSession.GetCookie(Context).identifier.ToString(),
                                RegisterName = AdminLoginSession.GetCookie(Context).name
                            });
                        }
                    }

                    foreach (ListItem item in chkChildList.Items)
                    {
                        var ChildKey = item.Value;
                        //var child = dao.tVisionTripChildMeet.Where(p => p.ChildKey == ChildKey && p.ApplyID == iApplyID && p.CurrentUse == 'Y').FirstOrDefault();
                        var child = www6.selectQF<tVisionTripChildMeet>("ChildKey", ChildKey, "ApplyID", iApplyID, "CurrentUse", "Y");

                        if (child == null && item.Selected)
                        {
                            var ChildName = item.Text.Replace(item.Value, "");
                            cmList.Add(new tVisionTripChildMeet()
                            {
                                ApplyID = iApplyID,
                                ChildKey = item.Value,
                                ChildMeetType = 'S',
                                ChildName = ChildName,
                                CurrentUse = 'Y',
                                RegisterDate = DateTime.Now,
                                RegisterID = AdminLoginSession.GetCookie(Context).identifier.ToString(),
                                RegisterName = AdminLoginSession.GetCookie(Context).name
                            });
                        }
                    }
                    #endregion

                    //dao.tVisionTripChildMeet.InsertAllOnSubmit(cmList);
                    foreach (tVisionTripChildMeet insert in cmList)
                    {
                        www6.insert(insert);
                    }
                    //dao.SubmitChanges();

                    Master.ValueAction.Value = "list";
                    Master.ValueMessage.Value = "등록되었습니다.";
                }

            }
            catch (Exception ex)
            {
                //using (FrontDataContext daoDel = new FrontDataContext())
                //{ 
                //    var entity = daoDel.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                //    daoDel.tVisionTripApply.DeleteOnSubmit(entity);
                //    daoDel.SubmitChanges();  
                //}

                //www6.cud(MakeSQL.delQ( 0,"tVisionTripApply", "ApplyID", Convert.ToInt32(iApplyID)));
                var entity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(iApplyID));
                www6.delete(entity);

                ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                Master.ValueMessage.Value = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
            }
        }
        //}

    }

    protected int getCommitmentCount(string sponserid)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        int iCommitmentCount = 0;

        var objSql = new object[1] { "  SELECT COUNT(*) AS CommitmentCount " +
         "  FROM tCommitmentMaster ComM WITH (NOLOCK) " +
         "  LEFT OUTER JOIN tSponsorMaster SM WITH (NOLOCK) ON ComM.SponsorID = SM.SponsorID " +
         "  LEFT OUTER JOIN tChildMaster CM WITH (NOLOCK) ON ComM.ChildMasterID = CM.ChildMasterID " +
         "  WHERE ComM.SponsorID = '" + sponserid + "' " +
         "  AND ComM.SponsorItemEng IN ('DS', 'LS') " +
         "  AND (ComM.StopDate IS NULL OR ComM.StopDate > GETDATE()) " };

        DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

        if (ds2 == null || ds2.Tables.Count == 0 || ds2.Tables[0].Rows.Count == 0)
            iCommitmentCount = 0;
        else
            iCommitmentCount = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());

        return iCommitmentCount;
    }

    protected string getGroupType(string sponserid)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        string grouptype = "";

        //using (FrontDataContext dao = new FrontDataContext())
        //{
        var userInfo = new UserInfo();

        #region Compass - tSponsorGroup : GroupType 가져오기 
        Object[] objSql = new object[1] { "VT_WebInfoInquiry" };
        Object[] objParam = new object[] { "DIVIS", "ConID" };
        Object[] objValue = new object[] { "GroupInfo", sponserid };

        var list = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
        if (list.Rows.Count > 0)
        {
            grouptype = list.Rows[0]["GroupType"].ToString();
        }
        #endregion
        //}
        return grouptype;
    }

    protected void getSponChildList(string sponserid, int applyid, string countrycode)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        //using (AdminDataContext dao = new AdminDataContext())
        //{
        //후원어린이 정보
        Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId", "childKey", "orderby", "checkLetter" };
        Object[] objValue = new object[] { 1, 10000, sponserid, "", "", 0 };
        Object[] objSql = new object[] { "sp_web_mychild_list_f" };
        DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

        chkChildList.Items.Clear();
        foreach (DataRow dr in dt.Rows)
        {
            if (countrycode != "")
            {
                if (countrycode == dr["CountryCode"].ToString())
                {
                    chkChildList.Items.Add(new ListItem(dr["ChildKey"].ToString() + "" + dr["Name"].ToString(), dr["ChildKey"].ToString()));
                }
            }
            else
            {
                chkChildList.Items.Add(new ListItem(dr["ChildKey"].ToString() + "" + dr["Name"].ToString(), dr["ChildKey"].ToString()));
            }
        }

        if (applyid > 0)
        {
            //var list = dao.tVisionTripChildMeet.Where(p => p.ApplyID == applyid && p.CurrentUse == 'Y').ToList();
            var list = www6.selectQ<tVisionTripChildMeet>("ApplyID", applyid, "CurrentUse", "Y");

            if (list.Count > 0)
            {
                foreach (ListItem item in chkChildList.Items)
                {
                    foreach (tVisionTripChildMeet cm in list)
                    {
                        if (item.Value == cm.ChildKey)
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
            rpChildList.DataSource = list.Where(p => p.ChildMeetType == 'I');
            rpChildList.DataBind();
        }
        //}
    }

    protected void btn_remove_click(object sender, EventArgs e)
    {
        //using (AdminDataContext dao = new AdminDataContext())
        //{
        //    var entity = dao.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(PrimaryKey));
        //    entity.CurrentUse = 'N';
        //    dao.SubmitChanges();

        //    base.WriteLog(AdminLog.Type.delete, string.Format("{0}", entity.ToJson()));
        //}

        var entity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(PrimaryKey));
        entity.CurrentUse = 'N';

        www6.update(entity);
        base.WriteLog(AdminLog.Type.delete, string.Format("{0}", entity.ToJson()));

        Master.ValueAction.Value = "list";
        Master.ValueMessage.Value = "삭제되었습니다.";
    }

    protected void btnUserSearch_Click(object sender, EventArgs e)
    {

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();


        #region 국외인경우 국가목록 
        var actionResult = new CodeAction().Countries();
        if (!actionResult.success)
        {
            base.AlertWithJavascript(actionResult.message);
        }

        ddlHouseCountry.DataSource = actionResult.data;
        ddlHouseCountry.DataTextField = "CodeName";
        ddlHouseCountry.DataValueField = "CodeName";
        ddlHouseCountry.DataBind();
        #endregion



        #region Compass - tSponsorGroup : 사용자 정보
        Object[] objSql = new object[1] { "VT_WebInfoInquiry" };
        Object[] objParam = new object[] { "DIVIS", "ConID" };
        Object[] objValue = new object[] { "BaseInfo", txtUserID.Text };


        string addr_zipcode, addr1_doro, addr1_jibun, addr1_overseas, addr2_overseas,
            addr2_doro, addr2_jibun;

        var userinfo = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
        if (userinfo.Rows.Count > 0)
        {
            divUserInfo.Visible = true;
            addr_zipcode = userinfo.Rows[0]["ZipCode"].ToString();
            addr1_doro = userinfo.Rows[0]["Doro_Addr1"].ToString();
            addr2_doro = userinfo.Rows[0]["Doro_Addr2"].ToString();
            addr1_jibun = userinfo.Rows[0]["Jibun_Addr1"].ToString();
            addr2_jibun = userinfo.Rows[0]["Jibun_Addr2"].ToString();

            addr1_overseas = userinfo.Rows[0]["Address1"].ToString();
            addr2_overseas = userinfo.Rows[0]["Address2"].ToString();


            txtUserName.Text = userinfo.Rows[0]["SponsorName"].ToString();
            txtUserNameEng.Text = userinfo.Rows[0]["EngName"].ToString();
            txtTel.Text = userinfo.Rows[0]["Phone"].ToString();
            txtEmail.Text = userinfo.Rows[0]["Email"].ToString();
            hdnSponsorID.Value = userinfo.Rows[0]["SponsorID"].ToString();
            hdnGenderCode.Value = userinfo.Rows[0]["GenderCode"].ToString();
            hdnUserID.Value = userinfo.Rows[0]["UserID"].ToString();
            hdnBirthDate.Value = userinfo.Rows[0]["BirthDate"].ToString();

            dspAddrJibun.Value = addr1_jibun;
            dspAddrDoro.Value = addr1_doro;


            locationType.Value = userinfo.Rows[0]["LocationType"].ToString();

            if (userinfo.Rows[0]["LocationType"].ToString() == "국내")
            {
                addr_domestic.Checked = true;
                pn_addr_domestic.Style["display"] = "block";

                addr_domestic_zipcode.Value = addr_zipcode;
                addr_domestic_addr1.Value = addr1_doro + "//" + addr1_jibun;
                addr_domestic_addr2.Value = addr2_doro;

                addr_road.InnerText = "[도로명] (" + addr_zipcode + ") " + addr1_doro + " " + addr2_doro; addr_road.InnerText = "[도로명주소] (" + addr_zipcode + ") " + addr1_doro + " " + addr2_doro;
                addr_jibun.InnerText = "[지번] (" + addr_zipcode + ") " + addr1_jibun + " " + addr2_jibun;

                addr1.Value = addr1_doro + "//" + addr1_jibun;
                addr2.Value = addr2_doro;
                zipcode.Value = addr_zipcode;
            }
            else
            {
                addr_oversea.Checked = true;
                pn_addr_overseas.Style["display"] = "block";

                addr_overseas_zipcode.Value = addr_zipcode;
                addr_overseas_addr1.Value = addr1_overseas;
                addr_overseas_addr2.Value = addr2_overseas;


                addr1.Value = addr1_overseas;
                addr2.Value = addr2_overseas;
                zipcode.Value = addr_zipcode;

                ddlHouseCountry.SelectedValue = userinfo.Rows[0]["CountryCode"].ToString();
            }

            //후원어린이 정보
            getSponChildList(hdnSponsorID.Value, 0, "");
        }
        else
        {
            hdnSponsorID.Value = "";


            Master.ValueMessage.Value = "사용자 정보가 조회되지 않습니다.";
        }
        #endregion

    }

    protected void ddlVisitCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        getSponChildList(hdnSponsorID.Value, Convert.ToInt32(PrimaryKey), ddlVisitCountry.SelectedValue);
    }
}