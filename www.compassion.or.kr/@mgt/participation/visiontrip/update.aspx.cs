﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_participation_visiontrip_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.BoardType = "notice";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");



		foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "visiontrip_type").OrderBy(p => p.cd_order)) {
			v_type.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}

        // 2017.06.07 김태형 : 상태(Y:신청중, S:예정, N:마감, E:종료)
        trip_state.Items.Add(new ListItem("신청중", "Y"));
        trip_state.Items.Add(new ListItem("예정", "S"));
        trip_state.Items.Add(new ListItem("마감", "N"));
        trip_state.Items.Add(new ListItem("종료", "E"));


        if (base.Action == "update")
        {

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.visiontrip.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<visiontrip>("idx", Convert.ToInt32(PrimaryKey));

                trip_view.Checked = entity.trip_view == 'Y';
                v_type.SelectedValue = entity.v_type;
                visit_nation.Text = entity.visit_nation;
                start_date.Text = entity.start_date;
                end_date.Text = entity.end_date;
                expect_num.Text = entity.expect_num;
                request_cost.Text = entity.request_cost;
                trip_cost.Text = entity.trip_cost;
                open_date.Text = entity.open_date.ToString() != "" ? entity.open_date.Substring(0, 10) : "";
                close_date.Text = entity.close_date.ToString() != "" ? entity.close_date.Substring(0, 10) : "";
                trip_review.Text = entity.trip_review;
                reg_date.Text = entity.reg_date.ToString();
                trip_state.SelectedValue = entity.trip_state.ToString();

            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			ph_regdate.Visible = true;
            base.FileLoad();

			
		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

        var arg = new visiontrip() {
            trip_view = trip_view.Checked ? 'Y' : 'N',
            v_type = v_type.SelectedValue,
            visit_nation = visit_nation.Text,
            start_date = start_date.Text,
            end_date = end_date.Text,
            expect_num = expect_num.Text,
            request_cost = request_cost.Text,
            trip_cost = trip_cost.Text,
            open_date = open_date.Text == "" ? "" : open_date.Text + " 00:00:00",
            close_date = close_date.Text == "" ? "" : close_date.Text + " 23:59:59",
            del_flag = 'N',
            v_a_id = AdminLoginSession.GetCookie(this.Context).identifier,
            reg_date = DateTime.Now,
            trip_review = trip_review.Text,
            trip_state = Convert.ToChar(trip_state.SelectedValue)
        };


		using (AdminDataContext dao = new AdminDataContext()) {


			if (base.Action == "update") {

                //var entity = dao.visiontrip.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<visiontrip>("idx", Convert.ToInt32(PrimaryKey));

                entity.trip_view = arg.trip_view;
				entity.v_type = arg.v_type;
				entity.visit_nation = arg.visit_nation;
				entity.start_date = arg.start_date;
				entity.end_date = arg.end_date;
				entity.expect_num = arg.expect_num;
				entity.request_cost = arg.request_cost;
				entity.trip_cost = arg.trip_cost;
				entity.open_date = arg.open_date;
				entity.close_date = arg.close_date;
                entity.trip_review = arg.trip_review;
                entity.trip_state = arg.trip_state;

                Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "수정되었습니다.";
                //dao.SubmitChanges();
                //string wClause = string.Format("idx = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
			} else {
                //dao.visiontrip.InsertOnSubmit(arg);
                www6.insert(arg);

                Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "등록되었습니다.";
				//dao.SubmitChanges();

				base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));
			}

		}

	}

	protected void btn_remove_click(object sender, EventArgs e) {

		using (AdminDataContext dao = new AdminDataContext()) {
            //var entity = dao.visiontrip.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            //entity.del_flag = 'Y';
            //dao.SubmitChanges();
            string delStr = string.Format("delete from visiontrip where idx = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
		}

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}
}