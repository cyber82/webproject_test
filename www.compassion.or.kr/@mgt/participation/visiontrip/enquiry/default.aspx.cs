﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_participation_visiontrip_enquiry_default : AdminBoardPage
{


    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();
        base.LoadComplete += new EventHandler(list_LoadComplete);

        //트립유형
        foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "trip_type").OrderBy(p => p.cd_order))
        {
            ddlTripType.Items.Add(new ListItem(a.cd_value, a.cd_key));
        }
        ddlTripType.SelectedValue = "Request";

        //년도
        for (int i = 2005; i <= DateTime.Now.AddYears(1).Year; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlYear.SelectedValue = DateTime.Now.Year.ToString();
         
        //using (AdminDataContext dao = new AdminDataContext())
        //{
        //    var tripSchedule = dao.tVisionTripSchedule.Where(p => p.VisionTripType == "Plan" && p.CurrentUse == 'Y'
        //            && p.StartDate.Substring(0, 4) == ddlYear.SelectedValue).OrderBy(p => p.VisitCountry).ToList();

        //    foreach (tVisionTripSchedule list in tripSchedule)
        //    {
        //        ddlTripName.Items.Add(new ListItem(list.VisitCountry, list.ScheduleID.ToString()));
        //    }
        //}
    }

    protected override void OnAfterPostBack()
    {
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();
    }

    protected override void GetList(int page)
    {

        Master.IsSearch = false;
    }

    protected void ddlTripType_ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlTripName.Items.Clear();

        string strYear = ddlYear.SelectedValue;
        string strTripType = ddlTripType.SelectedValue;
        using (AdminDataContext dao = new AdminDataContext())
        {
            var list1 = www6.selectQ<tVisionTripSchedule>();
            var tripSchedule = list1.Where(p => p.CurrentUse == 'Y'
                    && p.StartDate.Substring(0, 4) == strYear && (strTripType == "" || p.VisionTripType == strTripType)).OrderBy(p => p.VisitCountry).ToList();

            ddlTripName.Items.Add(new ListItem("전체", "0"));

            foreach (tVisionTripSchedule list in tripSchedule)
            {
                ddlTripName.Items.Add(new ListItem(list.VisitCountry, list.ScheduleID.ToString()));
            }
        }
    }
}
