﻿
(function () {

    //var app = angular.module("defaultApp", []);

    var app = angular.module('cps.page', []);

    app.directive('onFinishRender', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                //if (scope.$first === true) {
                //    window.alert('First thing about to render');
                //}
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit(attr.onFinishRender);
                    }, 1);
                }
            }
        };
    });

    app.controller("defaultCtrl", function ($scope, $http) {

   // $scope.list = [];
    $scope.filepath = "/files/visiontrip/plan/";


    //엑셀다운로드
    $scope.exportData = function () {
        var exportID = $("#hdnTabInfo").val();
        //var table = document.getElementById(exportID);
        //var html = table.outerHTML;
        //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));

        var title = "";
        switch (exportID) {
            case "1": title = "신청현황"; break;
                case "2": title = "신청정보"; break;
            case "3": title = "참가자정보"; break;
            case "4" : title = "참가자주소"; break; 
            case "5" : title = "통계"; break;
            }
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() +1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = year + "" +month + "" +day +""+hour + "" +mins;
        ////creating a temporary HTML link element (they support setting file names)

        var table_div = document.getElementById("tableExcel" + exportID);
        var table_html = table_div.outerHTML.replace(/ /g, '%20');

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        //setting the file name 
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(table_div.outerHTML);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, '비전트립_' + title + "_" + postfix + '.xls');
            return (sa);
        }
        else {
            var a = document.createElement('a');
            var data_type = 'data:application/vnd.ms-excel;base64,';
            a.href = data_type + ', ' + table_html;
            a.download = '비전트립_' + title + "_" + postfix + '.xls';
            a.click();
        }

        //var a = document.createElement('a');
        //    //getting data from our div that contains the HTML table
        //    var data_type = 'data:application/vnd.ms-excel';
        //    var table_div = document.getElementById("tableExcel"+exportID);
        //var table_html = table_div.outerHTML.replace(/ /g, '%20');
        //    a.href = data_type + ', ' +table_html;
        //        //setting the file name
        //        a.download = '비전트립_' +title + "_" +postfix + '.xls';
        //    //triggering the function
        //a.click();
    };

    //검색조건 
    $scope.searchSelectChange = function () {
        $scope.select_tripname = []; 
        console.log('tripname');
        $http.get("/@mgt/api/visiontrip.ashx?t=tripname", { params: { tripYear: $("#ddlYear option:selected").val(), tripType: $("#ddlTripType option:selected").val() } }).success(function (r) {
            console.log(r);
            if (r.success) {
                var list = r.data; 
                $scope.select_tripname = list; 
            } else {
                alert(r.message);
            }
        });
    };
    $scope.searchSelectChange();
    $("#ddlYear, #ddlTripType").change(function () { $scope.searchSelectChange(); });


    $scope.tripYear = $("#ddlYear option:selected").val();
    $scope.tripType = $("#ddlTripType option:selected").val();
    $scope.tripID = $("#ddlTripName option:selected").val();
    //end - 검색조건 

    $scope.searchClick = function () {
        $scope.tripYear = $("#ddlYear option:selected").val();
        $scope.tripType = $("#ddlTripType option:selected").val();
        $scope.tripID = $("#ddlTripName option:selected").val();

        var param = {
            tripYear: $scope.tripYear,
            tripType: $scope.tripType,
            tripID: $scope.tripID
        };

        var exportID = $("#hdnTabInfo").val();
        switch (exportID) {
            case "1": $scope.getApplyStateList(param); break;
            case "2": $scope.getApplyInfoList(param); break;
            case "3": $scope.getParticipantList(param); break;
            case "4": $scope.getAddrList(param); break; 
            case "5": $scope.getStatisticsList(param); break;
        }
    }
    //신청현황
    $scope.select_applystate = [{ codevalue: 'A', codename: '신청중' }, { codevalue: 'C', codename: '신청완료' }];
    $scope.select_paymentYN = [{ codevalue: 'Y', codename: '납부' }, { codevalue: 'N', codename: '미납' }];
       
    $scope.a_total = 0;
    $scope.a_page = 1;
    $scope.a_rowsPerPage = 10;
     
    $scope.a_params = {
        page: $scope.a_page,
        rowsPerPage: $scope.a_rowsPerPage,
        tripYear: $scope.tripYear,
        tripType: $scope.tripType,
        tripID: $scope.tripID
    };

    $scope.getApplyStateList = function (params) {
        $scope.a_params = $.extend($scope.a_params, params);
        console.log('applystate_list_p');
        $http.get("/@mgt/api/visiontrip.ashx?t=applyplay_list&list=state", { params: $scope.a_params }).success(function (r) {
            console.log(r);
            if (r.success) { 
                var list = r.data;

                if (list.length > 0) {
                    $.each(list, function () {
                        this.grid_type = 'apply';

                        this.cancel_check = this.cancel_yn == "Y" ? true : false;
                        this.rowselect = false;
                        this.cancel_class = this.cancel_check ? "tr_cancel" : "";
                    });

                }
                $scope.applystate_list = list;
                $scope.a_total = r.data.length > 0 ? r.data[0].total : 0;
            } else {
                alert(r.message);
            }
        });

    } 
    //첫 로딩
    $scope.getApplyStateList();

    $scope.attachPopup = function (item) {
        var ra_id = item.applyid;
        window.open("/@mgt/participation/visiontrip/apply_plan/attachlist?id=" + ra_id, "printFrm", "width=810px,height=500px,scrollbars=yes");
        return false;
    }
    // end - 신청현황

    //신청정보
    $scope.i_total = 0;
    $scope.i_page = 1;
    $scope.i_rowsPerPage = 10;

    $scope.i_params = {
        page: $scope.i_page,
        rowsPerPage: $scope.i_rowsPerPage,
        tripYear: $scope.tripYear,
        tripType: $scope.tripType,
        tripID: $scope.tripID
    };

    $scope.getApplyInfoList = function (params) {
        $scope.i_params = $.extend($scope.i_params, params);
        console.log('applystate_list_p');
        $http.get("/@mgt/api/visiontrip.ashx?t=applyplay_list&list=info", { params: $scope.i_params }).success(function (r) {
            console.log(r);
            if (r.success) {
                var list = r.data;

                if (list.length > 0) {
                    $.each(list, function () {
                        this.grid_type = 'info'; 
                        this.cancel_check = this.cancel_yn == "Y" ? true : false;
                        this.rowselect = false;
                        this.cancel_class = this.cancel_check ? "tr_cancel" : "";
                    });

                }
                $scope.applyinfo_list = list;
                $scope.i_total = r.data.length > 0 ? r.data[0].total : 0;
            } else {
                alert(r.message);
            }
        });

    }  
    // end - 신청정보

    //참가자정보
    $scope.p_total = 0;
    $scope.p_page = 1;
    $scope.p_rowsPerPage = 10;

    $scope.p_params = {
        page: $scope.p_page,
        rowsPerPage: $scope.p_rowsPerPage,
        tripYear: $scope.tripYear,
        tripType: $scope.tripType,
        tripID: $scope.tripID
    };

    $scope.getParticipantList = function (params) {
        $scope.p_params = $.extend($scope.p_params, params);
        console.log('participant_list_p');
        $http.get("/@mgt/api/visiontrip.ashx?t=applyplay_list&list=participant", { params: $scope.p_params }).success(function (r) {
            console.log(r);
            if (r.success) {
                var list = r.data;

                $.each(list, function () {
                    this.grid_type = 'participant';
                    this.cancel_check = this.cancel_yn == "Y" ? true : false;
                    this.rowselect = false;
                    this.cancel_class = this.cancel_check ? "tr_cancel" : "";
                });

                $scope.participant_list = list;
                $scope.p_total = r.data.length > 0 ? r.data[0].total : 0;
            } else {
                alert(r.message);
            }
        });

    }
    // end - 참가자정보

    //참가자주소
    $scope.d_total = 0;
    $scope.d_page = 1;
    $scope.d_rowsPerPage = 10;

    $scope.d_params = {
        page: $scope.d_page,
        rowsPerPage: $scope.d_rowsPerPage,
        tripYear: $scope.tripYear,
        tripType: $scope.tripType,
        tripID: $scope.tripID
    };

    $scope.getAddrList = function (params) {
        $scope.d_params = $.extend($scope.d_params, params);
        console.log('addr_list_p');
        $http.get("/@mgt/api/visiontrip.ashx?t=applyplay_list&list=addr", { params: $scope.d_params }).success(function (r) {
            console.log(r);
            if (r.success) {
                var list = r.data;

                if (list.length > 0) {
                    $.each(list, function () {
                        this.grid_type = 'addr';
                        this.cancel_check = this.cancel_yn == "Y" ? true : false;
                        this.rowselect = false;
                        this.cancel_class = this.cancel_check ? "tr_cancel" : "";

                        this.addr_colspan = this.location == "국내" ? 1 : 2;

                        this.domesticYN = this.location == "국내" ? true : false;
                        this.full_doro = this.location == "국내" ? this.address_doro + " " + this.address2 : "";
                        this.full_jibun = this.location == "국내" ? this.address_jibun + " " + this.address2 : "";
                        this.addr2_overseas = this.location != "국내" ? this.address2 : "";
                        this.zipcode_overseas = this.location != "국내" ? this.zipcode : "";
                    }); 
                }
                $scope.addr_list = list;
                $scope.d_total = r.data.length > 0 ? r.data[0].total : 0;
            } else {
                alert(r.message);
            }
        });

    }
    
    // end - 참가자주소
       
     
    //통계
    $scope.s_total = 0;
    $scope.s_page = 1;
    $scope.s_rowsPerPage = 10;
     
    $scope.s_params = {
        page: $scope.s_page,
        rowsPerPage: $scope.s_rowsPerPage,
        tripYear: $scope.tripYear,
        tripType: $scope.tripType,
        tripID: $scope.tripID
    };

    $scope.getStatisticsList = function (params) {
        $scope.s_params = $.extend($scope.s_params, params);
        console.log('statisticslist_p');
        $http.get("/@mgt/api/visiontrip.ashx?t=applyplay_list&list=statistics", { params: $scope.s_params }).success(function (r) {
            console.log(r);
            if (r.success) {
                //var list = $.parseJSON(r.data);
                var list = r.data;

                //if (list.length > 0) {
                //    $.each(list, function () {
                //        this.visit_date = new Date(this.visit_date);
                //        this.tripnotice_view = this.tripnotice != "" ? true : false;
                //        this.child_name = this.child_name;
                //        this.child_key = this.child_key != null ? this.child_key.replace(/, /g, ', <br/>') : '';
                //    }); 
                //}
                $scope.statisticts_list = list;
                $scope.s_total = r.data.length > 0 ? r.data[0].total : 0;
            } else {
                alert(r.message);
            }
        });

    } 
    // end - 통계 

});


    app.directive('icheck', ['$timeout', function ($timeout) {
        return {
            require: 'ngModel',
            link: function ($scope, element, $attrs, ngModel) {
                return $timeout(function () {
                    var value = $attrs['value'];

                    $scope.$watch($attrs['ngModel'], function (newValue) {
                        $(element).iCheck('update');
                    })

                    return $(element).iCheck({
                        checkboxClass: 'icheckbox_flat-blue checked',
                        radioClass: 'iradio_flat-blue checked'
                    }).on('ifChanged', function (event) {
                        if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                            $scope.$apply(function () {
                                return ngModel.$setViewValue(event.target.checked);
                            });
                        }
                        if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                            return $scope.$apply(function () {
                                return ngModel.$setViewValue(value);
                            });
                        }
                    });
                });
            }
        };
    }]);



})();

var $page = {

    timer: null,

    attachUploader: function (button) {
        return new AjaxUpload(button, {
            action: '/common/handler/upload',
            responseType: 'json',
            onChange: function (file) {
                var fileName = file.toLowerCase();
            },
            onSubmit: function (file, ext) {
                this.disable();
            },
            onComplete: function (file, response) {

                this.enable();

                console.log(file, response);
                if (response.success) { 
                    $("#path_" + button).val(response.name);
                    $("[data-id=path_" + button + "]").val(response.name.replace(/^.*[\\\/]/, ''));

                    console.log($("#path_" + button).val()); //filepullpath
                    console.log($("[data-id=path_" + button + "]").val());  //filename  
                } else
                    alert(response.msg);
            }
        });
    }

};



















//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////












/*!
 * iCheck v1.0.1, http://git.io/arlzeA
 * ===================================
 * Powerful jQuery and Zepto plugin for checkboxes and radio buttons customization
 *
 * (c) 2013 Damir Sultanov, http://fronteed.com
 * MIT Licensed
 */

(function ($) {

    // Cached vars
    var _iCheck = 'iCheck',
      _iCheckHelper = _iCheck + '-helper',
      _checkbox = 'checkbox',
      _radio = 'radio',
      _checked = 'checked',
      _unchecked = 'un' + _checked,
      _disabled = 'disabled',
      _determinate = 'determinate',
      _indeterminate = 'in' + _determinate,
      _update = 'update',
      _type = 'type',
      _click = 'click',
      _touch = 'touchbegin.i touchend.i',
      _add = 'addClass',
      _remove = 'removeClass',
      _callback = 'trigger',
      _label = 'label',
      _cursor = 'cursor',
      _mobile = /ipad|iphone|ipod|android|blackberry|windows phone|opera mini|silk/i.test(navigator.userAgent);

    // Plugin init
    $.fn[_iCheck] = function (options, fire) {

        // Walker
        var handle = 'input[type="' + _checkbox + '"], input[type="' + _radio + '"]',
          stack = $(),
          walker = function (object) {
              object.each(function () {
                  var self = $(this);

                  if (self.is(handle)) {
                      stack = stack.add(self);
                  } else {
                      stack = stack.add(self.find(handle));
                  };
              });
          };

        // Check if we should operate with some method
        if (/^(check|uncheck|toggle|indeterminate|determinate|disable|enable|update|destroy)$/i.test(options)) {

            // Normalize method's name
            options = options.toLowerCase();

            // Find checkboxes and radio buttons
            walker(this);

            return stack.each(function () {
                var self = $(this);

                if (options == 'destroy') {
                    tidy(self, 'ifDestroyed');
                } else {
                    operate(self, true, options);
                };

                // Fire method's callback
                if ($.isFunction(fire)) {
                    fire();
                };
            });

            // Customization
        } else if (typeof options == 'object' || !options) {

            // Check if any options were passed
            var settings = $.extend({
                checkedClass: _checked,
                disabledClass: _disabled,
                indeterminateClass: _indeterminate,
                labelHover: true,
                aria: false
            }, options),

              selector = settings.handle,
              hoverClass = settings.hoverClass || 'hover',
              focusClass = settings.focusClass || 'focus',
              activeClass = settings.activeClass || 'active',
              labelHover = !!settings.labelHover,
              labelHoverClass = settings.labelHoverClass || 'hover',

              // Setup clickable area
              area = ('' + settings.increaseArea).replace('%', '') | 0;

            // Selector limit
            if (selector == _checkbox || selector == _radio) {
                handle = 'input[type="' + selector + '"]';
            };

            // Clickable area limit
            if (area < -50) {
                area = -50;
            };

            // Walk around the selector
            walker(this);

            return stack.each(function () {
                var self = $(this);

                // If already customized
                tidy(self);

                var node = this,
                  id = node.id,

                  // Layer styles
                  offset = -area + '%',
                  size = 100 + (area * 2) + '%',
                  layer = {
                      position: 'absolute',
                      top: offset,
                      left: offset,
                      display: 'block',
                      width: size,
                      height: size,
                      margin: 0,
                      padding: 0,
                      background: '#fff',
                      border: 0,
                      opacity: 0
                  },

                  // Choose how to hide input
                  hide = _mobile ? {
                      position: 'absolute',
                      visibility: 'hidden'
                  } : area ? layer : {
                      position: 'absolute',
                      opacity: 0
                  },

                  // Get proper class
                  className = node[_type] == _checkbox ? settings.checkboxClass || 'i' + _checkbox : settings.radioClass || 'i' + _radio,

                  // Find assigned labels
                  label = $(_label + '[for="' + id + '"]').add(self.closest(_label)),

                  // Check ARIA option
                  aria = !!settings.aria,

                  // Set ARIA placeholder
                  ariaID = _iCheck + '-' + Math.random().toString(36).substr(2, 6),

                  // Parent & helper
                  parent = '<div class="' + className + '" ' + (aria ? 'role="' + node[_type] + '" ' : ''),
                  helper;

                // Set ARIA "labelledby"
                if (aria) {
                    label.each(function () {
                        parent += 'aria-labelledby="';

                        if (this.id) {
                            parent += this.id;
                        } else {
                            this.id = ariaID;
                            parent += ariaID;
                        }

                        parent += '"';
                    });
                };

                // Wrap input
                parent = self.wrap(parent + '/>')[_callback]('ifCreated').parent().append(settings.insert);

                // Layer addition
                helper = $('<ins class="' + _iCheckHelper + '"/>').css(layer).appendTo(parent);

                // Finalize customization
                self.data(_iCheck, { o: settings, s: self.attr('style') }).css(hide);
                !!settings.inheritClass && parent[_add](node.className || '');
                !!settings.inheritID && id && parent.attr('id', _iCheck + '-' + id);
                parent.css('position') == 'static' && parent.css('position', 'relative');
                operate(self, true, _update);

                // Label events
                if (label.length) {
                    label.on(_click + '.i mouseover.i mouseout.i ' + _touch, function (event) {
                        var type = event[_type],
                          item = $(this);

                        // Do nothing if input is disabled
                        if (!node[_disabled]) {

                            // Click
                            if (type == _click) {
                                if ($(event.target).is('a')) {
                                    return;
                                }
                                operate(self, false, true);

                                // Hover state
                            } else if (labelHover) {

                                // mouseout|touchend
                                if (/ut|nd/.test(type)) {
                                    parent[_remove](hoverClass);
                                    item[_remove](labelHoverClass);
                                } else {
                                    parent[_add](hoverClass);
                                    item[_add](labelHoverClass);
                                };
                            };

                            if (_mobile) {
                                event.stopPropagation();
                            } else {
                                return false;
                            };
                        };
                    });
                };

                // Input events
                self.on(_click + '.i focus.i blur.i keyup.i keydown.i keypress.i', function (event) {
                    var type = event[_type],
                      key = event.keyCode;

                    // Click
                    if (type == _click) {
                        return false;

                        // Keydown
                    } else if (type == 'keydown' && key == 32) {
                        if (!(node[_type] == _radio && node[_checked])) {
                            if (node[_checked]) {
                                off(self, _checked);
                            } else {
                                on(self, _checked);
                            };
                        };

                        return false;

                        // Keyup
                    } else if (type == 'keyup' && node[_type] == _radio) {
                        !node[_checked] && on(self, _checked);

                        // Focus/blur
                    } else if (/us|ur/.test(type)) {
                        parent[type == 'blur' ? _remove : _add](focusClass);
                    };
                });

                // Helper events
                helper.on(_click + ' mousedown mouseup mouseover mouseout ' + _touch, function (event) {
                    var type = event[_type],

                      // mousedown|mouseup
                      toggle = /wn|up/.test(type) ? activeClass : hoverClass;

                    // Do nothing if input is disabled
                    if (!node[_disabled]) {

                        // Click
                        if (type == _click) {
                            operate(self, false, true);

                            // Active and hover states
                        } else {

                            // State is on
                            if (/wn|er|in/.test(type)) {

                                // mousedown|mouseover|touchbegin
                                parent[_add](toggle);

                                // State is off
                            } else {
                                parent[_remove](toggle + ' ' + activeClass);
                            };

                            // Label hover
                            if (label.length && labelHover && toggle == hoverClass) {

                                // mouseout|touchend
                                label[/ut|nd/.test(type) ? _remove : _add](labelHoverClass);
                            };
                        };

                        if (_mobile) {
                            event.stopPropagation();
                        } else {
                            return false;
                        };
                    };
                });
            });
        } else {
            return this;
        };
    };

    // Do something with inputs
    function operate(input, direct, method) {
        var node = input[0],
          state = /er/.test(method) ? _indeterminate : /bl/.test(method) ? _disabled : _checked,
          active = method == _update ? {
              checked: node[_checked],
              disabled: node[_disabled],
              indeterminate: input.attr(_indeterminate) == 'true' || input.attr(_determinate) == 'false'
          } : node[state];

        // Check, disable or indeterminate
        if (/^(ch|di|in)/.test(method) && !active) {
            on(input, state);

            // Uncheck, enable or determinate
        } else if (/^(un|en|de)/.test(method) && active) {
            off(input, state);

            // Update
        } else if (method == _update) {

            // Handle states
            for (var state in active) {
                if (active[state]) {
                    on(input, state, true);
                } else {
                    off(input, state, true);
                };
            };

        } else if (!direct || method == 'toggle') {

            // Helper or label was clicked
            if (!direct) {
                input[_callback]('ifClicked');
            };

            // Toggle checked state
            if (active) {
                if (node[_type] !== _radio) {
                    off(input, state);
                };
            } else {
                on(input, state);
            };
        };
    };

    // Add checked, disabled or indeterminate state
    function on(input, state, keep) {
        var node = input[0],
          parent = input.parent(),
          checked = state == _checked,
          indeterminate = state == _indeterminate,
          disabled = state == _disabled,
          callback = indeterminate ? _determinate : checked ? _unchecked : 'enabled',
          regular = option(input, callback + capitalize(node[_type])),
          specific = option(input, state + capitalize(node[_type]));

        // Prevent unnecessary actions
        if (node[state] !== true) {

            // Toggle assigned radio buttons
            if (!keep && state == _checked && node[_type] == _radio && node.name) {
                var form = input.closest('form'),
                  inputs = 'input[name="' + node.name + '"]';

                inputs = form.length ? form.find(inputs) : $(inputs);

                inputs.each(function () {
                    if (this !== node && $(this).data(_iCheck)) {
                        off($(this), state);
                    };
                });
            };

            // Indeterminate state
            if (indeterminate) {

                // Add indeterminate state
                node[state] = true;

                // Remove checked state
                if (node[_checked]) {
                    off(input, _checked, 'force');
                };

                // Checked or disabled state
            } else {

                // Add checked or disabled state
                if (!keep) {
                    node[state] = true;
                };

                // Remove indeterminate state
                if (checked && node[_indeterminate]) {
                    off(input, _indeterminate, false);
                };
            };

            // Trigger callbacks
            callbacks(input, checked, state, keep);
        };

        // Add proper cursor
        if (node[_disabled] && !!option(input, _cursor, true)) {
            parent.find('.' + _iCheckHelper).css(_cursor, 'default');
        };

        // Add state class
        parent[_add](specific || option(input, state) || '');

        // Set ARIA attribute
        disabled ? parent.attr('aria-disabled', 'true') : parent.attr('aria-checked', indeterminate ? 'mixed' : 'true');

        // Remove regular state class
        parent[_remove](regular || option(input, callback) || '');
    };

    // Remove checked, disabled or indeterminate state
    function off(input, state, keep) {
        var node = input[0],
          parent = input.parent(),
          checked = state == _checked,
          indeterminate = state == _indeterminate,
          disabled = state == _disabled,
          callback = indeterminate ? _determinate : checked ? _unchecked : 'enabled',
          regular = option(input, callback + capitalize(node[_type])),
          specific = option(input, state + capitalize(node[_type]));

        // Prevent unnecessary actions
        if (node[state] !== false) {

            // Toggle state
            if (indeterminate || !keep || keep == 'force') {
                node[state] = false;
            };

            // Trigger callbacks
            callbacks(input, checked, callback, keep);
        };

        // Add proper cursor
        if (!node[_disabled] && !!option(input, _cursor, true)) {
            parent.find('.' + _iCheckHelper).css(_cursor, 'pointer');
        };

        // Remove state class
        parent[_remove](specific || option(input, state) || '');

        // Set ARIA attribute
        disabled ? parent.attr('aria-disabled', 'false') : parent.attr('aria-checked', 'false');

        // Add regular state class
        parent[_add](regular || option(input, callback) || '');
    };

    // Remove all traces
    function tidy(input, callback) {
        if (input.data(_iCheck)) {

            // Remove everything except input
            input.parent().html(input.attr('style', input.data(_iCheck).s || ''));

            // Callback
            if (callback) {
                input[_callback](callback);
            };

            // Unbind events
            input.off('.i').unwrap();
            $(_label + '[for="' + input[0].id + '"]').add(input.closest(_label)).off('.i');
        };
    };

    // Get some option
    function option(input, state, regular) {
        if (input.data(_iCheck)) {
            return input.data(_iCheck).o[state + (regular ? '' : 'Class')];
        };
    };

    // Capitalize some string
    function capitalize(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };

    // Executable handlers
    function callbacks(input, checked, callback, keep) {
        if (!keep) {
            if (checked) {
                input[_callback]('ifToggled');
            };

            input[_callback]('ifChanged')[_callback]('if' + capitalize(callback));
        };
    };
})(window.jQuery || window.Zepto);

$(function () {
    $('input').iCheck();
    $('input.all').on('ifChecked ifUnchecked', function (event) {
        if (event.type == 'ifChecked') {
            $('input.check').iCheck('check');
        } else {
            $('input.check').iCheck('uncheck');
        }
    });
    $('input.check').on('ifUnchecked', function (event) {
        $('input.all').iCheck('uncheck');
    });
});