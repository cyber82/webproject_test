﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_participation_visiontrip_enquiry_default" MasterPageFile="~/@mgt/mgt_afterlogin.master" %>

<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        .tr_cancel{
            background-color:steelblue;
        }

        .float_right {
            float: right;
            margin-right: 10px;
        }

        .icheckbox {
            display: inline-block;
            *display: inline;
            vertical-align: middle;
            margin: 0;
            padding: 0;
            width: 20px;
            height: 20px;
            background: url(/@mgt/template/plugins/iCheck/flat/blue.png) no-repeat;
            border: none;
            cursor: pointer;
        }

            .icheckbox.checked {
                background-position: -22px 0;
            }
        .table tr td {
            vertical-align: middle !important;
        }
        .btnModify{
            min-width:140px;
        }  
.iradio {
    display: inline-block;
    *display: inline;
    vertical-align: middle;
    margin: 0;
    padding: 0;
    width: 20px;
    height: 20px;
    background: url(/@mgt/template/plugins/iCheck/flat/blue.png) no-repeat;
    border: none;
    cursor: pointer;
} 
.iradio {
    background-position: -88px 0;
}
    .iradio.checked {
        background-position: -110px 0;
    }
    .iradio.disabled {
        background-position: -132px 0;
        cursor: default;
    }
    .iradio.checked.disabled {
        background-position: -154px 0;
    } 
    </style>
    <script type="text/javascript" src="/assets/angular/1.4.8/angular.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0-beta.2/angular-sanitize.js"></script>
    
    <script type="text/javascript" src="/assets/moment/moment-with-locales.min.js"></script> 
    <script type="text/javascript" src="/assets/angular/angular-moment.min.js"></script>

    <script type="text/javascript" src="/@mgt/participation/visiontrip/enquiry/default.js"></script>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script src="/@mgt/template/plugins/iCheck/icheck.min.js"></script>
    
    <script type="text/javascript" src="/@mgt/common/js/paging.js"></script>
    <script type="text/javascript" src="/common/js/site/angular-app.js"></script>
    
    <script type="text/javascript">
        $(function () {

            //탭이동
            $(".nav-tabs > li").click(function () {
                var idx = $(this).index() + 1;
                $(".nav-tabs > li").removeClass("active");
                $(this).addClass("active");
                $(".tab-pane").hide();
                $("#tab" + idx).show();

                $("#hdnTabInfo").val(idx);

                $(".allcheck").iCheck("uncheck");

                return false;
            });

            $(".allcheck").on("ifChanged", function (sender) {
                var val = $(this).attr("data-val");
                var checked = $(this).prop("checked");

                $.each($(".item_check"), function () {
                    $(this).iCheck(checked ? "check" : "uncheck");
                });
            });


            //$(".rd_addr").on("ifChanged", function (sender) {
            //    var val = $(this).attr("id");
            //    var checked = $(this).prop("checked");
                 
            //    alert(val);
            //});


        });

        function applyAllCheck() {
            var val = $(this).attr("data-val");
            var checked = $(this).prop("checked");

            $.each($(".item_check"), function () {
                $($(this).find("input")).iCheck(checked ? "check" : "uncheck");
            });
        }

        
    </script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <iframe id="txtArea1" style="display: none"></iframe>
        <input type="hidden" id="hdnTabInfo" value="1" />
        <div class="box box-default collapsed-box search_container">
            <div class="box-header with-border section-search">
                <div class="pull-left" style="width: 100%;">
                    <button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;"><i class="fa fa-plus search_toggle"></i></button>
                    <h3 class="box-title" style="padding-top: 5px">검색</h3>
                      <div style="float: right;"> 
                        <input type="button" id="btnExcel" class="btn btn-bitbucket " ng-click="exportData()" value="엑셀불러오기" /> 
                    </div>
                </div>

            </div>
            <div class="form-horizontal box-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">트립유형</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlTripType" CssClass="form-control" Style="width: 150px;">
                                <asp:ListItem Text="전체" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">년 도</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control" Width="80px" ></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">트립명</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlTripName" CssClass="form-control" Width="150px" >
                                <asp:ListItem Text="전체" Value=""></asp:ListItem>
                                <%--<asp:ListItem ng-repeat="lst in select_tripname" Value="{{lst.scheduleid}}" ng-model="select_tripname">{{lst.visitcountry}}</asp:ListItem>--%>
                                <%--#12855 요청으로 costcenter로 변경--%>
                                <asp:ListItem ng-repeat="lst in select_tripname" Value="{{lst.scheduleid}}" ng-model="select_tripname">{{lst.costcenter}}</asp:ListItem>
                            </asp:DropDownList> 
                        </div>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <a ng-click="searchClick()" id="btn_search" class="btn btn-primary" style="width: 100px">검색 <i class="fa fa-arrow-circle-right"></i></a>
                    </div>

                </div>

            </div>
        </div>
        <!-- /.box -->

        <%--<div class="box box-primary">--%>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
            <li runat="server" id="tabm1" class="active" ng-click="getApplyStateList({ page:1, tripYear: tripYear, tripType: tripType, tripID: tripID })"><a href="javascript:void(0);">신청현황</a></li>
                <li runat="server" id="tabm2" ng-click="getApplyInfoList({ page:1, tripYear: tripYear, tripType: tripType, tripID: tripID })"><a href="javascript:void(0);">신청정보</a></li>
                <li runat="server" id="tabm3" ng-click="getParticipantList({ page:1, tripYear: tripYear, tripType: tripType, tripID: tripID })"><a href="javascript:void(0);">참가자정보</a></li>
                <li runat="server" id="tabm4" ng-click="getAddrList({ page:1, tripYear: tripYear, tripType: tripType, tripID: tripID })"><a href="javascript:void(0);">참가자주소</a></li>
                  <li runat="server" id="tabm5" ng-click="getStatisticsList({ page:1, tripYear: tripYear, tripType: tripType, tripID: tripID })"><a href="javascript:void(0);">통계</a></li>
            </ul>


            <div class="tab-content">

                <%--신청현황--%>
                <div class="active tab-pane" id="tab1" runat="server">
                    <div class="box-header">
                        <h3 class="box-title">총 {{ a_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered " id="tableApply">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="chkApplyAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th>#</th>
                                    <th>트립유형</th>
                                    <th>년도</th>
                                    <th>트립명</th>
                                    <th>참가자명</th>
                                    <th>후원자번호</th>
                                    <th>신청현황</th>
                                    <th>신청일</th>
                                    <th>신청비</th>
                                    <th>참가비</th>
                                    <th>후원어린이만남비</th>
                                    <th>여권</th>
                                    <th>추가첨부</th>
                                    <th>취소</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applystate_list" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>
                                        <label>
                                            <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="check{{item.applyid }}" icheck /></label></td>
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.visiontrip_type }}</td>
                                    <td>{{item.trip_year }}</td>
                                    <td>{{item.trip_name }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>{{item.conid }}</td>
                                    <td>
                                        <span>{{item.apply_state}}</span>
                                    </td>
                                    <td>{{item.apply_date }}</td>
                                    <td>
                                        <span>{{item.request_cost | number }}</span>
                                    </td>
                                    <td>
                                        <span>{{item.trip_cost | number }}</span>
                                    </td>
                                    <td>
                                        <span>{{item.childmeet_cost | number }}</span>
                                    </td>
                                    <td><span ng-if="item.passport_path != ''"><a ng-href="{{item.passport_path}}" target="_blank"><i class="fa fa-files-o "></i></a></span>
                                    </td>
                                    <td><span ng-if="item.etc_attach > 0"><a href="javascript:void(0);" ng-click="attachPopup(item)"><i class="fa fa-files-o "></i></a></span>
                                    </td>
                                    <td> <span>{{item.cancel_yn}}</span> 
                                    </td>
                                </tr>
                                <tr ng-hide="a_total > 0">
                                    <td colspan="15">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="a_page" page-size="a_rowsPerPage" total="a_total" show-prev-next="true" show-first-last="true" paging-action="getApplyStateList({page : page})"></paging>
                    </div>

                    <div class="excel-area" style="display:none;"> 
                        <table class="table table-hover table-bordered " id="tableExcel1" style="border:1px solid #f4f4f4">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr> 
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">#</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">트립유형</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">년도</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">트립명</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">참가자명</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">후원자번호</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">신청현황</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">신청일</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">신청비</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">참가비</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">후원어린이만남비</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">여권</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">추가첨부</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">취소</th> 
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applystate_list|filter:{rowselect: true}" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td style="border:1px solid #f4f4f4;">{{item.rownum }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.visiontrip_type }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.trip_year }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.trip_name }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.sponsor_name }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.conid }}</td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.apply_state}}</span></td>
                                    <td style="border:1px solid #f4f4f4;">{{item.apply_date }}</td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.request_cost | number }}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.trip_cost | number }}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.childmeet_cost | number }}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span ng-if="item.passport_path != ''">{{item.passport_name}}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span ng-if="item.etc_attach > 0 ">{{item.etc_attach}}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.cancel_yn}}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 신청현황--%>

                <%--신청정보--%>
                <div class="tab-pane" id="tab2" runat="server">

                    <div class="box-header">
                        <h3 class="box-title">총 {{ i_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered ">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="chkInfoAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th>#</th>
                                    <th>참가자명</th>
                                    <th>후원여부</th>
                                    <th>성별</th>
                                    <th>생년월일</th>
                                    <th>국적</th>
                                    <th>영어능력</th>
                                    <th>요청룸</th>
                                    <th>룸메이트</th>
                                    <th style="width: 100px;">후원어린이 만남</th>
                                    <th>종교</th>
                                    <th>VT참가수</th>
                                    <th>그룹</th>
                                    <th>참가비<br />
                                        (관리자)</th>
                                    <th>후원어린이
                                        <br />
                                        만남비
                                        <br />
                                        (관리자)</th> 
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applyinfo_list" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>
                                        <label>
                                            <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="info_{{item.applyid }}" icheck /></label></td>
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>{{item.sponsor_yn }}</td>
                                    <td>{{item.gender_code }}</td>
                                    <td>{{item.birth_date }}</td>
                                    <td>{{item.nation }}</td>
                                    <td>{{item.english_level }}</td>
                                    <td>{{item.room_type }}</td>
                                    <td>{{item.room_detail }}</td> 
                                    <td>
                                       {{item.child_key}}
                                    </td>
                                    <td>{{item.religion }}</td>
                                    <td>{{item.visiontrip_history }}</td>
                                    <td>{{item.group_type }}</td>
                                    <td> 
                                        <span>{{item.trip_cost | number }}</span> 
                                    </td>
                                    <td>
                                        <span>{{item.childmeet_cost | number }}</span>  
                                    </td> 
                                </tr>
                                <tr ng-hide="i_total > 0">
                                    <td colspan="16">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="i_page" page-size="i_rowsPerPage" total="i_total" show-prev-next="true" show-first-last="true" paging-action="getApplyInfList({page : page})"></paging>
                    </div>

                    
                    <div class="excel-area" id="divInfo" style="display:none;"> 
                        <table class="table table-bordered" style="border:1px solid #f4f4f4" id="tableExcel2" >
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr> 
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">#</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">참가자명</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">후원여부</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">성별</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">생년월일</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">국적</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">영어능력</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">요청룸</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">룸메이트</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">후원어린이 만남</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">종교</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">VT참가수</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">그룹</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">참가비<br />(관리자)</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">후원어린이<br />만남비<br />(관리자)</th> 
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applyinfo_list|filter:{rowselect: true}" ng-class="item.cancel_class" ng-model="item.cancel_class"> 
                                    <td style="border:1px solid #f4f4f4;">{{item.rownum }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.sponsor_name }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.sponsor_yn }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.gender_code }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.birth_date }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.nation }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.english_level }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.room_type }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.room_detail }}</td> 
                                    <td style="border:1px solid #f4f4f4;">{{item.child_key}}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.religion }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.visiontrip_history }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.group_type }}</td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.trip_cost | number }}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.childmeet_cost | number }}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 신청정보--%>
                <%--참가자정보--%>
                <div class="tab-pane" id="tab3" runat="server">

                    <div class="box-header">
                        <h3 class="box-title">총 {{ p_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered ">
                            <thead>
                                <tr>
                                    <th rowspan="2">
                                        <input type="checkbox" id="chkParticipantAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th rowspan="2">#</th>
                                    <th rowspan="2">성명</th>
                                    <th rowspan="2">영문명</th>
                                    <th rowspan="2">휴대번호</th>
                                    <th rowspan="2">Email</th>
                                    <th rowspan="2">직업</th>
                                    <th rowspan="2">교회명</th>
                                    <th rowspan="2">비고<br />(관리자only)</th>
                                    <th colspan="3">현금영수증</th>
                                    <th colspan="3">비상연락처</th> 
                                </tr>
                                <tr>
                                    <th>발급자</th>
                                    <th>휴대번호</th>
                                    <th>참가자와<br />관계</th>
                                    <th>성함</th>
                                    <th>연락처</th>
                                    <th>참가자와<br />관계</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in participant_list" ng-class="item.cancel_class" ng-model="item.cancel_class" > 
                                    <td>     
                                        <label>
                                        <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="participant_{{item.applyid }}" icheck /></label></td>

                                    <td>{{item.rownum }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>{{item.sponsor_name_eng }}</td>
                                    <td>
                                        <span>{{item.tel}}</span> 
                                    </td>
                                    <td>
                                        <span>{{item.email}}</span> 
                                    </td>
                                    <td>{{item.job }}</td>
                                    <td>{{item.church }}</td>
                                    <td>
                                        <span>{{item.remark}}</span> 
                                    </td>
                                    <td>
                                        <span>{{item.cashreceipt_name}}</span> 
                                    </td>
                                    <td>
                                        <span>{{item.cashreceipt_tel}}</span> 
                                    </td>
                                    <td>
                                        <span>{{item.cashreceipt_relation}}</span> 
                                    </td>
                                    <td>
                                        <span>{{item.emergencycontact_name}}</span> 
                                    </td>
                                    <td>
                                        <span>{{item.emergencycontact_tel}}</span> 
                                    </td>
                                    <td>
                                        <span>{{item.emergencycontact_relation}}</span> 
                                    </td> 
                                </tr>
                                <tr ng-hide="p_total > 0">
                                    <td colspan="15">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="p_page" page-size="p_rowsPerPage" total="p_total" show-prev-next="true" show-first-last="true" paging-action="getParticipantList({page : page})"></paging>
                    </div>
                    <div class="excel-area" id="divParticipant" style="display:none;"> 
                        <table class="table table-hover table-bordered " id="tableExcel3" style="border:1px solid #f4f4f4">
                            <thead>
                                <tr> 
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">#</th>
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">성명</th>
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">영문명</th>
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">휴대번호</th>
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">Email</th>
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">직업</th>
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">교회명</th>
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">비고<br />(관리자only)</th>
                                    <th colspan="3" style="border:1px solid #f4f4f4;font-weight:bold;">현금영수증</th>
                                    <th colspan="3" style="border:1px solid #f4f4f4;font-weight:bold;">비상연락처</th> 
                                </tr>
                                <tr>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">발급자</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">휴대번호</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">참가자와<br />관계</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">성함</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">연락처</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">참가자와<br />관계</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in participant_list|filter:{rowselect: true}" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td style="border:1px solid #f4f4f4;">{{item.rownum }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.sponsor_name }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.sponsor_name_eng }}</td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.tel}}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.email}}</span></td>
                                    <td style="border:1px solid #f4f4f4;">{{item.job }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.church }}</td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.remark}}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.cashreceipt_name}}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.cashreceipt_tel}}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.cashreceipt_relation}}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.emergencycontact_name}}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.emergencycontact_tel}}</span></td>
                                    <td style="border:1px solid #f4f4f4;"><span>{{item.emergencycontact_relation}}</span></td>
                                </tr>
                            </tbody>
                        </table>
               
                    </div>

                </div>
                <%--end 참가자정보--%>
                <%--참가자주소--%>
                <div class="tab-pane" id="tab4" runat="server">

                    <div class="box-header">
                        <h3 class="box-title">총 {{ d_total}} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered ">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr ng-class="">
                                    <th>
                                        <input type="checkbox" id="chkAddrAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th>#</th>
                                    <th>성명</th>
                                    <th>주소1</th>
                                    <th>주소2</th>
                                    <th>우편번호</th> 
                                </tr> 
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in addr_list"  ng-class="item.cancel_class" ng-model="item.cancel_class" > 
                                    <td>
                                        <label>
                                            <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="{{'p_check_'+item.applyid }}" icheck /></label></td>
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td> 
                                        <span ng-if="item.domesticYN == true">{{item.address_doro }} &nbsp;//&nbsp; {{item.address_jibun }}</span>
                                        <span ng-if="item.domesticYN != true">{{item.address_overseas }}</span> 
                                    </td> 
                                    <td><span>{{item.address2 }}</span></td>
                                    <td> <span>{{item.zipcode }}</span></td> 
                                </tr> 
                                <tr ng-hide="d_total > 0">
                                    <td colspan="6">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="d_page" page-size="d_rowsPerPage" total="d_total" show-prev-next="true" show-first-last="true" paging-action="getAddrList({page : page})"></paging>
                    </div>

                    <div class="box-body table-responsive no-padding" style="display:none;"> 
                        <table class="table table-hover table-bordered"  id="tableExcel4" style="border:1px solid #f4f4f4">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr> 
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">#</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">성명</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">주소1</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">주소2</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">우편번호</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in addr_list | filter:{rowselect: true}">
                                    <td style="border: 1px solid #f4f4f4;">{{item.rownum }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.sponsor_name }}</td>
                                    <td style="border: 1px solid #f4f4f4;">
                                        <span ng-if="item.domesticYN == true">{{item.address_doro }} &nbsp;//&nbsp; {{item.address_jibun }}</span>
                                        <span ng-if="item.domesticYN != true">{{item.address_overseas }}</span>
                                    </td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.address2 }}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.zipcode }}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 참가자주소--%> 
                <%--통계--%>
                <div class="tab-pane" id="tab5" runat="server">
                    <div class="box-header">
                        <h3 class="box-title">총 {{ s_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered ">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th rowspan="2">#</th>
                                    <th rowspan="2">트립명</th>
                                    <th rowspan="2">참가자수</th>
                                    <th colspan="2">성별</th>
                                    <th rowspan="2">CDSP후원자수</th>
                                    <th colspan="5">연령</th>
                                    <th colspan="4">비전트립 참가 횟수</th>
                                </tr>
                                <tr>
                                    <th>남</th>
                                    <th>여</th>
                                    <th>10대</th>
                                    <th>20대</th>
                                    <th>30대</th>
                                    <th>40대</th>
                                    <th>50대이상</th>
                                    <th>처음</th>
                                    <th>1회</th>
                                    <th>2회</th>
                                    <th>3회이상</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in statisticts_list">
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.visit_country }}</td>
                                    <td>{{item.apply_cnt }}</td>
                                    <td>{{item.male_cnt }}</td>
                                    <td>{{item.female_cnt }}</td>
                                    <td>{{item.commitment_count }}</td>
                                    <td>{{item.ten }}</td>
                                    <td>{{item.twenty }}</td>
                                    <td>{{item.thirty }}</td>
                                    <td>{{item.forty }}</td>
                                    <td>{{item.fifty }}</td>
                                    <td>{{item.vh_zero }}</td>
                                    <td>{{item.vh_first }}</td>
                                    <td>{{item.vh_second }}</td>
                                    <td>{{item.vh_third }}</td>
                                </tr>
                                <tr ng-hide="s_total > 0">
                                    <td colspan="15">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="s_page" page-size="s_rowsPerPage" total="s_total" show-prev-next="true" show-first-last="true" paging-action="getStatisticsList({page : page})"></paging>
                    </div>
                    
                    <div class="box-body table-responsive no-padding" style="display:none;"> 
                        <table id="tableExcel6" style="border:1px solid #f4f4f4">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">#</th>
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">트립명</th>
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">참가자수</th>
                                    <th colspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">성별</th>
                                    <th rowspan="2" style="border:1px solid #f4f4f4;font-weight:bold;">CDSP후원자수</th>
                                    <th colspan="5" style="border:1px solid #f4f4f4;font-weight:bold;">연령</th>
                                    <th colspan="4" style="border:1px solid #f4f4f4;font-weight:bold;">비전트립 참가 횟수</th>
                                </tr>
                                <tr>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">남</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">여</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">10대</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">20대</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">30대</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">40대</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">50대이상</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">처음</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">1회</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">2회</th>
                                    <th style="border:1px solid #f4f4f4;font-weight:bold;">3회이상</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in statisticts_list">
                                    <td style="border:1px solid #f4f4f4;">{{item.rownum }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.visit_country }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.apply_cnt }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.male_cnt }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.female_cnt }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.commitment_count }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.ten }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.twenty }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.thirty }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.forty }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.fifty }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.vh_zero }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.vh_first }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.vh_second }}</td>
                                    <td style="border:1px solid #f4f4f4;">{{item.vh_third }}</td>
                                </tr> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 통계--%>
            </div> 
            <!-- /.box-header -->  
        </div>

    </section>
</asp:Content>
