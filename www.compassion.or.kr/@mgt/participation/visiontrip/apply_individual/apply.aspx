﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="apply.aspx.cs" Inherits="mgt_participation_visiontrip_apply_plan_apply" %>
 
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">

<head id="Head1" runat="server">
    <title>compassion Admin</title> 

        <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/@mgt/template/bootstrap/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="/@mgt/template/dist/css/AdminLTE.min.css" />
    <!-- iCheck -->
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/square/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/minimal/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/flat/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/dist/css/skins/skin-green.min.css" />
    <!-- Date Picker -->
    <link rel="stylesheet" href="/@mgt/template/plugins/datepicker/datepicker3.css" />
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/@mgt/template/plugins/daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" href="/@mgt/common/css/admin.css" />
    <link rel="stylesheet" href="/@mgt/common/js/bootstrap-switch/bootstrap-switch.min.css" />

    
    <!-- jQuery 2.1.4 -->
    <script src="/@mgt/template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!-- Bootstrap 3.3.5 -->
    <script src="/@mgt/template/bootstrap/js/bootstrap.min.js"></script>
    <script src="/@mgt/common/js/bootstrap-switch/bootstrap-switch.min.js"></script>

    <!-- iCheck -->
    <script src="/@mgt/template/plugins/iCheck/icheck.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="/@mgt/template/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->

    <script src="/@mgt/template/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="/@mgt/template/plugins/datepicker/locales/bootstrap-datepicker.kr.js"></script>

    <script type="text/javascript" src="/@mgt/common/js/common.js" defer="defer"></script>
    <script type="text/javascript" src="/@mgt/common/js/message.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/jquery.json-2.4.min.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/jquery.json-3.3.2.min.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/form.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/cookie.js" defer="defer"></script>

    <script type="text/javascript" src="/assets/jquery/wisekit/function.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.search.js" defer="defer"></script>
    <script type="text/javascript" src="/@mgt/common/js/ajaxupload.3.6.wisekit.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.bootstrap.js"></script>


    <script type="text/javascript" src="/assets/angular/1.4.8/angular.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0-beta.2/angular-sanitize.js"></script>

    <script type="text/javascript" src="/assets/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="/assets/angular/angular-moment.min.js"></script>
     
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script src="/@mgt/template/plugins/iCheck/icheck.min.js"></script>

    <script type="text/javascript" src="/@mgt/common/js/paging.js"></script>
    <script type="text/javascript" src="/common/js/site/angular-app.js"></script>



    <script type="text/javascript" src="/@mgt/participation/visiontrip/apply_individual/apply.js"></script>
    
	<script type="text/javascript" src="/common/js/site/motive.js"></script> 
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    
    <script type="text/javascript" src="/@mgt/participation/visiontrip/common/multiselect.js"></script>
    <link href="/@mgt/participation/visiontrip/common/template/visiontrip_form.css" rel="stylesheet" />  

    
    <script type="text/javascript">

        $(function () {
            $page.init(); 

            $("#btn_close").click(function () {
                window.close();
            });

            if ($("#message").val() != "") {
                alert($("#message").val());
                if ($("#action").val() == "close") {
                    window.opener.location.reload();
                    self.close();
                }
            }

            //주소
            //$("#addr_overseas_addr1").setHangulBan();
            //$("#addr_overseas_addr2").setHangulBan();
            $(".rd_addr").change(function () {
                if ($("#addr_domestic").prop("checked")) {
                    $(".hide_overseas").show();
                    $("#pn_addr_domestic").show();
                    $("#pn_addr_overseas").hide();
                } else {
                    $(".hide_overseas").hide();
                    $("#pn_addr_domestic").hide();
                    $("#pn_addr_overseas").show();
                }
            });

            $("[data-id=check_addr]").hide();
            if ($("#addr_domestic").prop("checked")) {
                $(".hide_overseas").show();
                $("#pn_addr_domestic").show();
                $("#pn_addr_overseas").hide();
                $(".zipcode_area").show();
            } else {
                $(".hide_overseas").hide();
                $("#pn_addr_domestic").hide();
                $("#pn_addr_overseas").show();
                $(".zipcode_area").hide();
            }

             
            if ($("#addr_domestic").is(":checked")) {

                $("#pn_addr_domestic").show();
                $("#pn_addr_overseas").hide();  

                $("#addr_domestic_zipcode").val($("#zipcode").val());
                $("#addr_domestic_addr1").val($("#addr1").val());
                $("#addr_domestic_addr2").val($("#addr2").val());

                // 컴파스의 데이타를 불러오는경우 
                if ($("#dspAddrDoro").val() != "") {
                    $("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
                    if ($("#dspAddrJibun").val() != "") {
                        $("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
                    }

                } else if ($("#addr1").val() != "") {

                    addr_array = $("#addr1").val().split("//");
                    $("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + addr_array[0] + " " + $("#addr2").val());
                    if (addr_array[1]) {
                        $("#addr_jibun").text("[지번주소] (" + $("#zipcode").val() + ") " + addr_array[1] + " " + $("#addr2").val());
                    }
                }

            } else {
                $("#pn_addr_domestic").hide();
                $("#pn_addr_overseas").show(); 

                $("#addr_overseas_zipcode").val($("#zipcode").val());
                $("#addr_overseas_addr1").val($("#addr1").val());
                $("#addr_overseas_addr2").val($("#addr2").val()); 
            }
            //end - 주소
             

            //기독교일 경우에만 교회 입력
            $("#txtChurch").hide();
            $("#ddlReligion").change(function () {
                if ($("#ddlReligion option:selected").val() == "Christian") {
                    $("#txtChurch").show();
                } else {
                    $("#txtChurch").hide();
                }
            });

            $("#ddlCashReceiptType").change(function () {
                if ($("#ddlCashReceiptType option:selected").val() == "M") {
                    $("#txtCashReceiptRelation").hide();
                    $("#txtCashReceiptName").val($("#txtUserName").val());
                    $("#txtCashReceiptTel").val($("#txtTel").val());
                    $("#txtCashReceiptRelation").val("본인");
                } else {
                    $("#txtCashReceiptRelation").show();
                    $("#txtCashReceiptName").val("");
                    $("#txtCashReceiptTel").val("");
                    $("#txtCashReceiptRelation").val("");
                }
            });
             
            $("#ddlChildMeetYn").change(function () {
                if ($("#ddlChildMeetYn option:selected").val() == "N") {
                    $("#spMultiSelect").hide();
                    $("#plChildtext").hide();
                } else {
                    $("#spMultiSelect").show();
                    $("#plChildtext").show();
                }
            });

            $('.date').datepicker({
                orientation: 'bottom',
                language: 'kr',
                format: 'yyyy-mm-dd',
                todayHighlight: true,
            }).on("changeDate", function (e) {

                if ($(".btn_date_search").length > 0) {
                    eval($(".btn_date_search").attr("href").replace("javascript:", ""));
                }
                $(this).datepicker('hide');

            });

            $('#txtVisitDate1').datepicker('setStartDate', new Date());
            $('#txtVisitDate2').datepicker('setStartDate', new Date());

            $('#txtVisitDate1').datepicker('todayHighlight', true);
            $('#txtVisitDate2').datepicker('todayHighlight', true);
        }); 

        var btnUserSearchClick = function () {
            if ($("#hdnSponsorID").val() != "" && $("#ddlSchedule").val() != "") {
                if (confirm("선택된 정보가 초기화 됩니다. 사용자 정보 조회하시겠습니까?")) {
                    return true;
                }
                else
                    return false;
            }
            else {
                return true;
            }
        };

    </script>
    
<script type="text/javascript">
    //<![<%--CDATA[
    var theForm = document.forms['form'];
    if (!theForm) {
        theForm = document.form;
    }
    function __doPostBack(eventTarget, eventArgument) {
        if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
            theForm.__EVENTTARGET.value = eventTarget;
            theForm.__EVENTARGUMENT.value = eventArgument;
            theForm.submit();
        }
    }

    //]]>
    function btnSaveClick() {
        __doPostBack("<%= btnSave %>", "");
    }--%>
</script>
    
</head>

<body>
    <form runat="server" id="form">
    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <input type="hidden" id="action" runat="server" value="" />
        <input type="hidden" id="message" runat="server" value="" />
        
        <asp:HiddenField runat="server" ID="id" />
        <asp:HiddenField runat="server" ID="hdnApplyType" Value="Individual" />
        <asp:HiddenField runat="server" ID="childlist" />
        <asp:HiddenField runat="server" ID="hdnFileRoot" />



        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>개인방문 등록</strong></h3>
                    </div>
                    <!-- /.box-header --> 
                        <div class="box-body">
                            <div class="form-horizontal">
                               <!-- 내용 -->
                                
                                <div class="box-header ">
                                    <h3 class="box-title">사용자 정보</h3>
                                </div>

                                <div class="box-body">
                                    <div class="form-group insert_form">
                                        <label class="col-sm-2 control-label">사용자정보 조회</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox runat="server" ID="txtUserID" CssClass="form-control pull-left" Style="width: 150px;" placeholder="ConID를 입력하세요"></asp:TextBox>
                                            <asp:Button runat="server" ID="btnUserSearch" Text="검색" CssClass="btn btn-default pull-left" OnClick="btnUserSearch_Click" />
                                            <asp:HiddenField runat="server" ID="hdnUserID" />
                                            <asp:HiddenField runat="server" ID="hdnSponsorID" />
                                            <asp:HiddenField runat="server" ID="hdnConID" />
                                            <asp:HiddenField runat="server" ID="hdnGenderCode" />
                                            <asp:HiddenField runat="server" ID="hdnBirthDate" />
                                        </div>
                                    </div>
                                    <div runat="server" id="divUserInfo">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">성함</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox runat="server" ID="txtUserName" CssClass="form-control" Width="400" MaxLength="30" ReadOnly="true"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">영문성함</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox runat="server" ID="txtUserNameEng" CssClass="form-control" Width="400" MaxLength="40"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">휴대번호</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox runat="server" ID="txtTel" CssClass="form-control" Width="400" MaxLength="13" ></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">이메일</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" Width="400" MaxLength="50" ></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">주소</label>
                                            <div class="col-sm-10">
                                                <div>
                                                    <input type="hidden" runat="server" id="locationType" />
                                                    <input type="hidden" runat="server" id="hfAddressType" value="" />

                                                    <input type="hidden" runat="server" id="zipcode" />
                                                    <input type="hidden" id="addr1" runat="server" />
                                                    <input type="hidden" id="addr2" runat="server" />
                                                    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
                                                    <input type="hidden" runat="server" id="dspAddrDoro" value="" />

                                                    <span class="radio_ui">
                                                        <input type="radio" runat="server" class="rd_addr css_radio" id="addr_domestic" name="addr_location" />
                                                        <label for="addr_domestic" class="css_label">국내</label>

                                                        <input type="radio" runat="server" class="rd_addr css_radio" id="addr_oversea" name="addr_location" />
                                                        <label for="addr_oversea" class="css_label ml20">해외</label>
                                                    </span>

                                                    <span id="pn_addr_domestic" runat="server" style="display: none">
                                                        <label for="zipcode" class="hidden">주소찾기</label>
                                                        <a href="javascript:void(0)" class="btn btn-default" runat="server" id="popup" ng-click="addrpopup()">주소찾기</a>
                                                        <input type="hidden" id="addr_domestic_zipcode" runat="server" />
                                                        <input type="hidden" id="addr_domestic_addr1" runat="server" />
                                                        <input type="hidden" id="addr_domestic_addr2" runat="server" />

                                                        <p id="addr_road" class="mt15" runat="server"></p>
                                                        <p id="addr_jibun" class="mt10" runat="server"></p>
                                                    </span>

                                                    <!-- 해외주소 체크 시 -->
                                                    <div id="pn_addr_overseas" runat="server" style="display: none; width: 400px;">
                                                        <span class="sel_type2 fl" style="width: 195px;">
                                                            <label for="ddlHouseCountry" class="hidden">국가 선택</label>
                                                            <asp:DropDownList runat="server" ID="ddlHouseCountry" class="form-control" Style="width: 195px;"></asp:DropDownList>

                                                        </span>
                                                        <label for="addr_overseas_zipcode" class="hidden">우편번호</label>

                                                        <input type="text" runat="server" id="addr_overseas_zipcode" class="form-control" value="" style="width: 195px" placeholder="우편번호"  maxlength="50" />
                                                        <input type="text" runat="server" id="addr_overseas_addr1" class="form-control" placeholder="주소" style="width: 400px;"  maxlength="1000" />
                                                        <input type="text" runat="server" id="addr_overseas_addr2" class="form-control" placeholder="상세주소" style="width: 400px;"  maxlength="1000" />
                                                    </div>
                                                    <!--// -->
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                
                               <!-- 신청서 -->
                                <div runat="server" id="divApplyInfo">
                                    <div class="box-header">
                                        <h3 class="box-title">신청서 정보</h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">방문유형</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <asp:DropDownList runat="server" ID="ddlVisitType" CssClass="form-control" Style="width: 310px;">
                                                            <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="후원어린이 만남" Value="M"></asp:ListItem>
                                                    <asp:ListItem Text="어린이센터 방문" Value="V"></asp:ListItem>
                                        
                                                </asp:DropDownList> 
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">방문날짜</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <input type="text" id="txtVisitDate1" runat="server" class="form-control date visitdate pull-left" placeholder="방문날짜(1지망)" style="width: 200px" />
                                                <input type="text" id="txtVisitDate2" runat="server" class="form-control date visitdate" placeholder="방문날짜(2지망)" style="width: 200px" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">방문국가</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <asp:DropDownList runat="server" ID="ddlVisitCountry" class="form-control" Width="200" >
                                                    <asp:ListItem Text="--선택--" Value="" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label control-label">방문국가 영문명</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox runat="server" ID="txtVisitCountryEng" CssClass="form-control" Width="400"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">종교</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <asp:DropDownList runat="server" ID="ddlReligion" CssClass="form-control pull-left" Style="width: 150px;">
                                                    <asp:ListItem Text="선택하세요" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="기독교" Value="Christian"></asp:ListItem>
                                                    <asp:ListItem Text="불교" Value="Buddhist"></asp:ListItem>
                                                    <asp:ListItem Text="천주교" Value="Catholic"></asp:ListItem>
                                                    <asp:ListItem Text="무교" Value="Atheism"></asp:ListItem>
                                                    <asp:ListItem Text="기타" Value="Etc"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox runat="server" ID="txtChurch" CssClass="form-control" MaxLength="25" Width="160" placeholder="교회명"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">동반인 정보</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">

                                                
                                        <a ng-click="addCompanionInput()" class="btn btn-default">+동반인 추가</a> 
                                        <div ng-repeat="input in companion_inputs" runat="server" class="mt5" style="width:550px;">
                                            <input type="text" ng-model="input.name_kor" class="form-control companion_name_kor pull-left" maxlength="25" style="width: 150px;" placeholder="한글성함" runat="server" />
                                            <input type="text" ng-model="input.name_eng" class="form-control companion_name_eng pull-left" maxlength="50" style="width: 150px;" placeholder="영문성함" runat="server" />
                                            <input type="text" ng-model="input.birth" class="form-control companion_birth number_only pull-left" maxlength="8" style="width: 100px;" placeholder="생년월일" runat="server" />
                                            <%--  <input type="text" ng-model="input.gender" class="form-control companion_gender pull-left"  maxlength="1" style="width: 80px;" placeholder="성별" runat="server" />--%>

                                            <select class="form-control gender companion_gender pull-left" ng-model="input.gender" style="width: 80px">
                                                <option value="">성별</option>
                                                <option value="M">남</option>
                                                <option value="F">여</option>
                                            </select>

                                            <a ng-click="removeCompanionInput($index)" class="btn btn-default  pull-left">삭제</a>
                                        </div>


                                            </div>
                                        </div>









                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">어린이 정보</label>
                                            <div class="col-sm-10" style="margin-top: 5px;"> 
                                                <span class="pull-left" style="width: 300px;" id="spMultiSelect" >
                                                    <multiselect class="input-xlarge" multiple="true" style="width: 300px;"
                                                        ng-model="selectedChild"
                                                        options="c.name for c in mychild"
                                                        change="selected()"></multiselect>
                                                    <div class="well well-small" style="display: none;">
                                                        <asp:HiddenField runat="server" ID="hdnSelectedChild" Value="{{selectedChild}}" />
                                                    </div>
                                                </span>
                                                <div>
                                                    <asp:Panel runat="server" ID="plChildtext">
                                                        <a ng-click="addInput()" ng-href="" class="btn btn-default">+직접입력 추가</a>
                                                        <div ng-repeat="input in child_inputs" runat="server" on-finish-render="ngRepeatFinished_Child">
                                                            <div class="mt5" style="width: 450px;">
                                                                <input type="text" ng-model="input.id" class="form-control childinput pull-left" maxlength="15" style="width: 300px;" placeholder="후원어린이 번호 입력" runat="server" />
                                                                <a ng-click="removeInput($index)" class="btn btn-default">삭제</a>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>

                                        </div>
                                                <div class="form-group">
                                            <label class="col-sm-2 control-label">현지정보</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <input type="text" id="txtLocalAccommodation" runat="server" maxlength="100" class="form-control pull-left" value="" style="width: 200px;" placeholder="현지 숙소명" />
                                                <input type="text" id="txtLocalTel" runat="server" maxlength="100" class="form-control" value="" style="width: 200px;" placeholder="현지숙소 연락처" />
                                                <input type="text" id="txtLocalAddress" runat="server" maxlength="100" class="form-control" value="" style="width: 400px;" placeholder="현지숙소 주소" />
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">출국일/귀국일</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <input type="text" id="txtDepartureDate" runat="server" class="form-control date fdate pull-left" placeholder="출국일" style="width: 200px" />
                                                <input type="text" id="txtReturnDate" runat="server" class="form-control date " placeholder="귀국일" style="width: 200px" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">비상연락처</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <asp:TextBox runat="server" ID="txtEmergencyContactName" CssClass="form-control" Width="400" MaxLength="100" placeholder="비상연락처 - 성함"></asp:TextBox>

                                                <asp:TextBox runat="server" ID="txtEmergencyContactTel" CssClass="form-control" Width="400" MaxLength="100" placeholder="비상연락처 - 휴대번호"></asp:TextBox>

                                                <asp:TextBox runat="server" ID="txtEmergencyContactRelation" CssClass="form-control" Width="400" MaxLength="100" placeholder="비상연락처 - 참가자와의 관계"></asp:TextBox>

                                            </div>
                                        </div>
                                         
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">기타첨부</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">

                                                <a ng-click="addInputFiles()" ng-href="" class="btn btn-default mb5">+첨부파일추가</a>
                                                <div ng-repeat="input in input_fils" class="mb5" on-finish-render="ngRepeatFinished">
                                                    <input type="text" runat="server" ng-model="input.filename" data-id="path_btn_etcfile{{input.key}}" value="" class="form-control pull-left mr10" style="width: 400px;" readonly="readonly" />
                                                    <input type="hidden" id="path_btn_etcfile{{input.key}}" ng-model="input.filepath" value="" />

                                                    <a href="javascript:void(0);" class="btn btn-default pull-left" id="btn_etcfile{{input.key}}"><span>파일선택</span></a>
                                                    <a ng-click="removeInputFiles($index)" class="btn btn-default ml5">삭제</a>
                                                </div>
                                            </div>
                                        </div> 
                                        <!-- -->
                                    </div>
                                </div>
                               <!-- /.신청서 --> 

                               <!-- /.내용 -->
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-right">
                          <%--  <asp:Button runat="server" ID="btn_save" Text="등록하기" CssClass="btn btn-danger" OnClick="btn_save_ServerClick" OnClientClick="return onSubmit()" />--%>
                            <a class="btn btn-danger" id="btn_submit" ng-click="applysubmit_individual()">제출하기</a>
                            <button class="btn btn-default" runat="server" id="btn_close"><i class="fa fa-times"></i>닫기</button>
                        </div>
                        <!-- /.box-footer --> 
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
         
                <!-- 약관동의 -->
               <%-- <div class="input_div divAgreement">
                    <div class="login_field"><span>약관동의</span></div>
                    <div class="login_input">
                        <span class="guide_comment2 mb5" data-id="check_agree" style="display: none"></span>
                        <div class="box mb5" style="height: 368px;">
                            <table class="tbl_agreement">
                                <caption>신청서 정보입력 - 약관동의</caption>
                                <colgroup>
                                    <col style="width: 270px" />
                                    <col style="width: 120px" />
                                    <col style="width: /" />
                                </colgroup>
                                <tbody> 
                                    <tr>
                                        <td><span class="sp_agree1">어린이보호서약서</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(1)" ng-click="agreementModal.show($event, 1, '어린이보호서약서')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree1">
                                                <asp:HiddenField ID="hdnAgree1" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree1" type="checkbox" runat="server" onclick="agree_checkbox(1)">
                                                <label class="css_label font1" for="chkAgree1"></label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree2">개인정보의 수집 및 활용</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(2)" ng-click="agreementModal.show($event, 2, '개인정보의 수집 및 활용')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree2">
                                                <asp:HiddenField ID="hdnAgree2" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree2" type="checkbox" runat="server" onclick="agree_checkbox(2)">
                                                <label class="css_label font1" for="chkAgree2"></label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree3">사진 및 영상자료 공유 </span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(3)" ng-click="agreementModal.show($event, 3, '사진 및 영상자료 공유')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree3">
                                                <asp:HiddenField ID="hdnAgree3" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree3" type="checkbox" runat="server" onclick="agree_checkbox(3)">
                                                <label class="css_label font1" for="chkAgree3"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree4">일정변경 또는 취소</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(4)" ng-click="agreementModal.show($event, 4, '일정변경 또는 취소')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree4">
                                                <asp:HiddenField ID="hdnAgree4" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree4" type="checkbox" runat="server" onclick="agree_checkbox(4)">
                                                <label class="css_label font1" for="chkAgree4"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree5">환불규정</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(5)" ng-click="agreementModal.show($event, 5, '환불규정')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree5">
                                                <asp:HiddenField ID="hdnAgree5" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree5" type="checkbox" runat="server" onclick="agree_checkbox(5)">
                                                <label class="css_label font1" for="chkAgree5"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree6">예방접종</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(6)" ng-click="agreementModal.show($event, 6, '예방접종')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree6">
                                                <asp:HiddenField ID="hdnAgree6" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree6" type="checkbox" runat="server" onclick="agree_checkbox(6)">
                                                <label class="css_label font1" for="chkAgree6"></label>
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="sp_agree7">기타사항</span>
                                        </td>
                                        <td><a class="btn_s_type4" onclick="checkAcceptterms(7)" ng-click="agreementModal.show($event, 7, '기타사항')">약관 전문 보기</a>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgree7">
                                                <asp:HiddenField ID="hdnAgree7" runat="server" />
                                                <input class="css_checkbox agreecheck_change" id="chkAgree7" type="checkbox" runat="server" onclick="agree_checkbox(7)">
                                                <label class="css_label font1" for="chkAgree7"></label>
                                            </span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="box divAgreementConfirm">
                            <table class="tbl_agreement">
                                <caption>신청서 정보입력 - 약관동의</caption>
                                <colgroup>
                                    <col style="width: 380px" />
                                    <col style="width: /" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="confirm">
                                            <p>
                                                상기 본인은 컴패션 비전트립 모든 안내 및 동의사항을<br />
                                                숙지하고 이를 준수하고 따를 것을 동의하며
                                                <br />
                                                해당 비전트립 참가를 신청합니다.
                                            </p>
                                        </td>
                                        <td>
                                            <span class="checkbox_ui" id="spAgreeFInal">
                                                <input class="css_checkbox agreecheck_change" id="chkAgreeFinal" type="checkbox" runat="server" ><label class="css_label font1" for="chkAgreeFinal"></label>
                                            </span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>--%>
                <!--// -->
         
       
    </section>

    </form>

</body>
</html>
