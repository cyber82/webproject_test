﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using CommonLib;

public partial class mgt_participation_visiontrip_apply_individual_attachlist : AdminBasePage {

	
	protected string ids{
		set {
			this.ViewState.Add("ids", value);
		}
		get {
			if (this.ViewState["ids"] == null) return "";
			return this.ViewState["ids"].ToString();
		}
	}
     

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		v_admin_auth auth = base.GetPageAuth(); 

		ids = Request["id"];

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var attachList = dao.tVisionTripAttach.Where(p => p.ApplyID == Convert.ToInt32(ids) && p.CurrentUse == 'Y' && p.AttachType == "etc").ToList();
            var attachList = www6.selectQ<tVisionTripAttach>("ApplyID", Convert.ToInt32(ids), "CurrentUse", "Y", "AttachType", "etc");

            var total = 0;
            if (attachList.Count > 0)
                total = attachList.Count;

            lbTotal.Text = total.ToString();
             
            repeater.DataSource = attachList;
            repeater.DataBind(); 
        }

    }

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack(); 
	}


    protected void ListBound(object sender, RepeaterItemEventArgs e)
    { 
        if (repeater != null)
        {
            if (e.Item.ItemType != ListItemType.Footer)
            {
                tVisionTripAttach entity = e.Item.DataItem as tVisionTripAttach;
                ((Literal)e.Item.FindControl("lbIdx")).Text = (e.Item.ItemIndex +1 ).ToString(); 
            }
        }

    } 

}