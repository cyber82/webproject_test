﻿using CommonLib;
using Microsoft.AspNet.FriendlyUrls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mgt_participation_visiontrip_apply_plan_apply : AdminBoardPage
{ 
    protected string ids
    {
        set
        {
            this.ViewState.Add("ids", value);
        }
        get
        {
            if (this.ViewState["ids"] == null) return "";
            return this.ViewState["ids"].ToString();
        }
    }
    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();
        v_admin_auth auth = base.GetPageAuth();

        ids = Request["id"];

        divUserInfo.Visible = false;
        divApplyInfo.Visible = false;     
        //파일경로
        hdnFileRoot.Value = Uploader.GetRoot(Uploader.FileGroup.file_visiontrip) + "individual/";


        #region 방문국가
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.country.OrderBy(p => p.c_name).ToList();
            var list = www6.selectQ<country>("c_name");

            ddlVisitCountry.Items.Add(new ListItem("전체", ""));

            foreach (country item in list)
            {
                ddlVisitCountry.Items.Add(new ListItem(item.c_name, item.c_id.ToString()));
            }

            if (!string.IsNullOrEmpty(ids))
            {
                ddlVisitCountry.SelectedValue = ids;
            }
        }
        #endregion
    }
    protected override void OnAfterPostBack()
    {
        base.OnAfterPostBack();
        message.Value = "";
    }

    protected void btnUserSearch_Click(object sender, EventArgs e)
    {

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();


        #region 국외인경우 국가목록 
        var actionResult = new CodeAction().Countries();
        if (!actionResult.success)
        {
            base.AlertWithJavascript(actionResult.message);
        }

        ddlHouseCountry.DataSource = actionResult.data;
        ddlHouseCountry.DataTextField = "CodeName";
        ddlHouseCountry.DataValueField = "CodeName";
        ddlHouseCountry.DataBind();
        #endregion
         

        #region Compass - tSponsorGroup : 사용자 정보
        Object[] objSql = new object[1] { "VT_WebInfoInquiry" };
        Object[] objParam = new object[] { "DIVIS", "ConID" };
        Object[] objValue = new object[] { "BaseInfo", txtUserID.Text };


        string addr_zipcode, addr1_doro, addr1_jibun, addr1_overseas, addr2_overseas,
            addr2_doro, addr2_jibun;

        var userinfo = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
        if (userinfo.Rows.Count > 0)
        {
            divUserInfo.Visible = true;
            divApplyInfo.Visible = true;
            addr_zipcode = userinfo.Rows[0]["ZipCode"].ToString();
            addr1_doro = userinfo.Rows[0]["Doro_Addr1"].ToString();
            addr2_doro = userinfo.Rows[0]["Doro_Addr2"].ToString();
            addr1_jibun = userinfo.Rows[0]["Jibun_Addr1"].ToString();
            addr2_jibun = userinfo.Rows[0]["Jibun_Addr2"].ToString();

            addr1_overseas = userinfo.Rows[0]["Address1"].ToString();
            addr2_overseas = userinfo.Rows[0]["Address2"].ToString();


            txtUserName.Text = userinfo.Rows[0]["SponsorName"].ToString();
            txtUserNameEng.Text = userinfo.Rows[0]["EngName"].ToString();
            txtTel.Text = userinfo.Rows[0]["Phone"].ToString();
            txtEmail.Text = userinfo.Rows[0]["Email"].ToString();
            hdnSponsorID.Value = userinfo.Rows[0]["SponsorID"].ToString();
            hdnGenderCode.Value = userinfo.Rows[0]["GenderCode"].ToString();
            hdnUserID.Value = userinfo.Rows[0]["UserID"].ToString();
            hdnBirthDate.Value = userinfo.Rows[0]["BirthDate"].ToString();



            dspAddrJibun.Value = addr1_jibun;
            dspAddrDoro.Value = addr1_doro;

            locationType.Value = userinfo.Rows[0]["LocationType"].ToString();

            if (userinfo.Rows[0]["LocationType"].ToString() == "국내")
            {
                addr_domestic.Checked = true;
                pn_addr_domestic.Style["display"] = "block";

                addr_domestic_zipcode.Value = addr_zipcode;
                addr_domestic_addr1.Value = addr1_doro + "//" + addr1_jibun;
                addr_domestic_addr2.Value = addr2_doro;

                addr_road.InnerText = "[도로명] (" + addr_zipcode + ") " + addr1_doro + " " + addr2_doro; addr_road.InnerText = "[도로명주소] (" + addr_zipcode + ") " + addr1_doro + " " + addr2_doro;
                addr_jibun.InnerText = "[지번] (" + addr_zipcode + ") " + addr1_jibun + " " + addr2_jibun;

                addr1.Value = addr1_doro + "//" + addr1_jibun;
                addr2.Value = addr2_doro;
                zipcode.Value = addr_zipcode;
            }
            else
            {
                addr_oversea.Checked = true;
                pn_addr_overseas.Style["display"] = "block";

                addr_overseas_zipcode.Value = addr_zipcode;
                addr_overseas_addr1.Value = addr1_overseas;
                addr_overseas_addr2.Value = addr2_overseas;


                addr1.Value = addr1_overseas;
                addr2.Value = addr2_overseas;
                zipcode.Value = addr_zipcode;

                ddlHouseCountry.SelectedValue = userinfo.Rows[0]["CountryCode"].ToString();
            }
              
            action.Value = "UserSuccess";
        }
        else
        {
            hdnSponsorID.Value = "";

            message.Value = "사용자 정보가 조회되지 않습니다.";
        }
        #endregion

    }  



}