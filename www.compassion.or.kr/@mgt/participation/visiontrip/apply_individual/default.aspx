﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_participation_visiontrip_apply_individual_default" MasterPageFile="~/@mgt/mgt_afterlogin.master" %>

<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>   
        .loading_box {min-width:282px;height:134px;padding:90px 35px 0 35px;font-size:15px;color:#929292;text-align:center;letter-spacing:-0.5px;border:1px solid #ededed;background:#fff url('/common/img/common/loading.gif') no-repeat center 30px;}
    
        .tr_cancel {
            background-color: steelblue;
        }

        .float_right {
            float: right;
            margin-right: 10px;
        }

        .icheckbox, .iradio {
            display: inline-block;
            *display: inline;
            vertical-align: middle;
            margin: 0;
            padding: 0;
            width: 20px;
            height: 20px;
            background: url(/@mgt/template/plugins/iCheck/flat/blue.png) no-repeat;
            border: none;
            cursor: pointer;
        }

            .icheckbox.checked {
                background-position: -22px 0;
            }

        .table tr td {
            vertical-align: middle !important;
        }

        .btnModify {
            min-width: 140px;
        }

        .iradio {
            display: inline-block;
            *display: inline;
            vertical-align: middle;
            margin: 0;
            padding: 0;
            width: 20px;
            height: 20px;
            background: url(/@mgt/template/plugins/iCheck/flat/blue.png) no-repeat;
            border: none;
            cursor: pointer;
        }

        .iradio {
            background-position: -88px 0;
        }

            .iradio.checked {
                background-position: -110px 0;
            }

            .iradio.disabled {
                background-position: -132px 0;
                cursor: default;
            }

            .iradio.checked.disabled {
                background-position: -154px 0;
            }
    </style>

    <script type="text/javascript" src="/assets/angular/1.4.8/angular.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0-beta.2/angular-sanitize.js"></script>

    <script type="text/javascript" src="/assets/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="/assets/angular/angular-moment.min.js"></script>

    <script type="text/javascript" src="/@mgt/participation/visiontrip/apply_individual/default.js"></script>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script src="/@mgt/template/plugins/iCheck/icheck.min.js"></script>


    <script type="text/javascript" src="/@mgt/common/js/paging.js"></script>

    <%--    <script type="text/javascript" src="/common/js/angular/paging.js"></script>--%>
    <script type="text/javascript" src="/common/js/site/angular-app.js"></script>
    
    
    <script type="text/javascript" src="/@mgt/participation/visiontrip/common/multiselect.js"></script> 
    <link href="/@mgt/participation/visiontrip/common/template/visiontrip_form.css" rel="stylesheet" />  
    <script type="text/javascript">
        $(function () {

            //탭이동
            $(".nav-tabs > li").click(function () {
                var idx = $(this).index() + 1;
                $(".nav-tabs > li").removeClass("active");
                $(this).addClass("active");
                $(".tab-pane").hide();
                $("#tab" + idx).show();


                $("#hdnTabInfo").val(idx);

                $(".allcheck").iCheck("uncheck");

                //컴파스 전송은 신청현황탭에서만
                if (idx == 1)
                    $("#btnCompassSend").show();
                else
                    $("#btnCompassSend").hide();

                return false;
            });

            $(".allcheck").on("ifChanged", function (sender) {
                var val = $(this).attr("data-val");
                var checked = $(this).prop("checked");

                $.each($(".item_check"), function () {
                    $(this).iCheck(checked ? "check" : "uncheck");
                });
            });


            //$(".rd_addr").on("ifChanged", function (sender) {
            //    var val = $(this).attr("id");
            //    var checked = $(this).prop("checked");

            //    alert(val);
            //});

        });

        function applyAllCheck() {
            var val = $(this).attr("data-val");
            var checked = $(this).prop("checked");

            $.each($(".item_check"), function () {
                $($(this).find("input")).iCheck(checked ? "check" : "uncheck");
            });
        }


    </script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
         <iframe id="txtArea1" style="display:none"></iframe>
    <!-- 로딩 -->
    <div style="width: 100%; height: 100%; position: fixed; left: 0; top: 0; z-index: 1990; opacity: 0.0; background: #000; display: none" id="loading_bg"></div>
    <div class="loading_box" style="position: fixed; left: 35%; width: 30%; top: 40%; z-index: 1991; display: none;" id="loading_container">
        <span class="msg"></span>
    </div>
    <!--//-->

        <%--<div ng-app="defaultApp" ng-controller="defaultCtrl" id="defaultCtrl">--%>
        <input type="hidden" id="hdnTabInfo" value="1" />
        <div class="box box-default collapsed-box search_container">
            <div class="box-header with-border section-search">
                <div class="pull-left" style="width: 100%;">
                    <button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;"><i class="fa fa-plus search_toggle"></i></button>
                    <h3 class="box-title" style="padding-top: 5px">검색</h3>
                    <div style="float: right;">
                        <input type="button" id="btnCompassSend" class="btn btn-bitbucket " ng-click="compassSend()" value="참가자관리 업로드" />
                        <input type="button" id="btnExcel" class="btn btn-bitbucket " ng-click="exportData()" value="엑셀불러오기" />
                        <input type="button" id="btnAdd" class="btn btn-bitbucket " ng-click="createPopup()" value="신규" />
                    </div>
                </div>
            </div>
            <div class="form-horizontal box-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">방문유형</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlVisitType" CssClass="form-control" Style="width: 150px;">
                                <asp:ListItem Text="전체" Value=""></asp:ListItem>
                                <asp:ListItem Text="어린이센터 방문" Value="V"></asp:ListItem>
                                <asp:ListItem Text="후원어린이 만남" Value="M"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">년 도</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control" Width="80px"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">국가명</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlVisitCountry" CssClass="form-control" Width="150px">
                                <asp:ListItem Text="전체" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <a ng-click="searchClick()" id="btn_search" class="btn btn-primary" style="width: 100px">검색 <i class="fa fa-arrow-circle-right"></i></a>
                    </div>

                </div>

            </div>
        </div>
        <!-- /.box -->

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li runat="server" id="tabm1" class="active" ng-click="getApplyStateList()"><a href="javascript:void(0);">신청현황</a></li>
                <li runat="server" id="tabm2" ng-click="getApplyInfoList()"><a href="javascript:void(0);">신청정보</a></li>
                <li runat="server" id="tabm3" ng-click="getParticipantList()"><a href="javascript:void(0);">참가자정보</a></li>
                <li runat="server" id="tabm4" ng-click="getAddrList()"><a href="javascript:void(0);">참가자주소</a></li>
                <li runat="server" id="tabm5" ng-click="getSMSList()"><a href="javascript:void(0);">SMS/MMS</a></li>
                <li runat="server" id="tabm6" ng-click="getStatisticsList()"><a href="javascript:void(0);">통계</a></li>
            </ul>


            <div class="tab-content">
                <%--신청현황--%>
                <div class="active tab-pane" id="tab1" runat="server">
                    <div class="box-header">
                        <h3 class="box-title">총 {{ a_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered " id="tableApply">
                            <colgroup>
                                <col style="width: 50px;" />
                                <col style="width: 50px;" />
                                <col style="width: 80px;" />
                                <col style="width: 80px;" />
                                <col style="" />
                                <col style="width: 80px;" />
                                <col style="width: 80px;" />
                                <col style="width: 100px" />
                                <col style="width: 100px" />
                                <col style="width: 100px" />
                                <col />
                                <col />
                                <col />
                                <col style="width: 80px;" />
                                <col />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="chkApplyAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th>#</th>
                                    <th>방문유형</th>
                                    <th>방문년도</th>
                                    <th>국가명</th>
                                    <th>성명</th>
                                    <th>후원자<br />
                                        번호</th>
                                    <th>신청서<br />
                                        제출일</th>
                                    <th>방문일1</th>
                                    <th>방문일2</th>
                                    <th>동반인수</th>
                                    <th>후원어린이</th>
                                    <th>첨부파일</th>
                                    <th>취소</th>
                                    <th>수정</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applystate_list" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>
                                        <label>
                                            <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="check{{item.applyid }}" icheck /></label></td>
                                    <td>{{item.rownum }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.visit_type_name}}</span>
                                        <div class="form-group-sm">
                                            <select id="apply_{{item.applyid}}_visit_type" ng-if="item.editmode == true" style="width: 80px;" class="form-control">
                                                <option ng-repeat="visit in select_visittype" ng-selected="item.visit_type == visit.codevalue" value="{{visit.codevalue}}">{{visit.codename}}</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>{{item.visit_year }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.visit_country_name}}</span>

                                        <div class="form-group-sm">
                                            <span class="sel_type2" style="width: 195px;" ng-if="item.editmode == true">
                                                <label for="apply_{{item.applyid }}_visit_country" class="hidden">국가선택</label>
                                                <select id="apply_{{item.applyid }}_visit_country" class="form-control" style="width: 80px;">
                                                    <option ng-repeat="lst in select_visitcountry" ng-selected="item.visit_country == lst.c_id" value="{{lst.c_id}}">{{lst.c_name}}</option>
                                                </select>
                                            </span>
                                        </div>
                                    </td>
                                    <td style="width: 80px;">{{item.sponsor_name }}</td>
                                    <td>{{item.conid }}</td>
                                    <td>{{item.apply_date }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.visit_date1}}</span>
                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" class="date inline form-control" id="{{'apply_'+item.applyid + '_visit_date1'}}" value="{{item.visit_date1}}" maxlength="10" style="width: 95px;" />
                                        </div>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.visit_date2}}</span>
                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" class="date inline form-control" id="{{'apply_'+item.applyid + '_visit_date2'}}" value="{{item.visit_date2}}" maxlength="10" style="width: 95px;" />
                                        </div>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true"><a ng-click="companionPopup(item)">{{item.companion_cnt}}</a></span>
                                        <div ng-if="item.editmode == true" style="width: 320px; text-align: left;">
                                            <a ng-click="addCompanionInput(item)" class="btn btn-default">+동반인 추가</a>
                                            <div ng-repeat="input in item.companion_inputs | filter:{delyn: 'N'}" runat="server" class="form-group-sm">
                                                <input type="text" ng-model="input.name_kor" class="form-control pull-left companion_name_kor" maxlength="25" style="width: 80px;" placeholder="한글성함" runat="server" />
                                                <input type="text" ng-model="input.name_eng" class="form-control pull-left companion_name_eng" maxlength="50" style="width: 80px;" placeholder="영문성함" runat="server" />
                                                <input type="text" ng-model="input.birth" class="form-control pull-left companion_birth number_only" maxlength="8" style="width: 50px;" placeholder="생년월일" runat="server" />
                                             <%--   <input type="text" ng-model="input.gender" class="form-control pull-left companion_gender" maxlength="1" style="width: 40px;" placeholder="성별" runat="server" />--%>
                                                 <select class="form-control gender companion_gender pull-left" ng-model="input.gender" style="width:60px">
                                                    <option value="">성별</option>
                                                    <option value="M">남</option>
                                                    <option value="F">여</option> 
                                                </select>
                                                <a ng-click="removeCompanionInput(item, $index, input.id)" class="btn btn-default"><i class="fa fa-close"></i></a>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width: 220px;">
                                        <span ng-if="item.editmode != true">{{item.child_key}}</span>
                                        <div ng-if="item.editmode == true" style="width: 230px; text-align: left;">
                                            <span class="pull-left" style="width: 230px;" id="spMultiSelect">
                                                <multiselect class="input-xlarge" multiple="true" style="width: 230px;"
                                                    ng-model="item.selectedChild"
                                                    options="c.name for c in item.mychild"
                                                    change="selected()"></multiselect>
                                            </span>
                                            <div class="form-group text-left">
                                                <a ng-click="addInput(item)" ng-href="" class="btn btn-default">+직접입력추가</a>
                                                <div ng-repeat="input in item.child_inputs| filter:{delyn: 'N'}" runat="server" on-finish-render="ngRepeatFinished_Child">
                                                    <div>
                                                        <input type="text" ng-model="input.id" class="form-control pull-left  childinput" maxlength="15" style="width: 170px;" placeholder="번호 입력" runat="server" />
                                                        <a ng-click="removeInput(item, $index, input.key)" class="btn btn-default  pull-left"><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <%--<a ng-click="addInput(item)" ng-href="" class="btn btn-default">+추가</a>
                                            <div ng-repeat="input in item.child_inputs| filter:{delyn: 'N'}" runat="server" on-finish-render="ngRepeatFinished_Child">
                                                <div class="form-group-sm">
                                                    <input type="text" ng-model="input.id" class="form-control pull-left  childinput" maxlength="15" style="width: 110px;" placeholder="번호 입력" runat="server" />
                                                    <input type="text" ng-model="input.name" class="form-control pull-left  childinput" maxlength="15" style="width: 90px;" placeholder="이름 입력" runat="server" />
                                                    <a ng-click="removeInput(item, $index, input.key)" class="btn btn-default"><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>--%>
                                        </div>
                                    </td>
                                    <td><span ng-if="item.etc_attach > 0 && item.editmode != true"><a ng-click="attachPopup(item)"><i class="fa fa-files-o "></i></a></span>
                                        <div ng-if="item.editmode == true" style="width: 230px; text-align: left;">

                                            <div ng-repeat="input in item.input_fils| filter:{delyn: 'N', newyn:'N'}" class="form-group-sm">
                                                <a>{{input.filename}}</a><a ng-click="modifyInputFiles(item, $index, input.key)" class="btn btn-default"><i class="fa fa-close"></i></a>
                                            </div>


                                            <a ng-click="addInputFiles(item, $index)" ng-href="" class="btn btn-default"><%--<i class="fa fa-plus attachment"></i>--%>첨부파일추가</a>

                                            <div ng-repeat="input in item.input_fils | filter:{delyn:'N', newyn: 'Y'}" class="form-group-sm" on-finish-render="ngRepeatFinished">

                                                <input type="text" runat="server" ng-model="input.filename" data-id="path_btn_etcfile_{{input.applyid +'_'+ input.key}}" value="" class="form-control pull-left" style="width: 100px;" />
                                                <input type="hidden" id="path_btn_etcfile_{{input.applyid}}_{{input.key}}" ng-model="input.filepath" value="" />

                                                <a href="javascript:void(0);" class="btn btn-default fl" id="btn_etcfile_{{input.applyid}}_{{input.key}}"><span>파일선택</span></a>
                                                <a ng-click="removeInputFiles(item, $index, input.key)" class="btn btn-default"><i class="fa fa-close"></i></a>

                                            </div>
                                        </div>

                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.cancel_yn}}</span>
                                        <label ng-if="item.editmode == true">
                                            <input ng-model="item.cancel_check" class="icheck" id="{{item.applyid }} + '_cancel'"
                                                type="checkbox" icheck />
                                        </label>
                                    </td>
                                    <td class="center">
                                        <input type="button" ng-if="item.editmode != true" class="btn btn-default" ng-click="modifyClick(item)" value="수정" />
                                        <input type="button" ng-if="item.editmode == true" class="btn btn-default" ng-click="saveClick(item)" value="저장" />
                                        <input type="button" ng-if="item.editmode == true" class="btn btn-default" ng-click="cancelClick(item)" value="취소" />
                                    </td>
                                </tr>
                                <tr ng-hide="a_total > 0">
                                    <td colspan="14">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="a_page" page-size="a_rowsPerPage" total="a_total" show-prev-next="true" show-first-last="true" paging-action="getApplyStateList({ page : page})"></paging>
                    </div>

                    <div class="excel-area" style="display: none;">
                        <table class="table table-hover table-bordered " id="tableExcel1">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>방문유형</th>
                                    <th>방문년도</th>
                                    <th>국가명</th>
                                    <th>성명</th>
                                    <th>후원자번호</th>
                                    <th>신청서제출일</th>
                                    <th>방문일1</th>
                                    <th>방문일2</th>
                                    <th>동반인수</th>
                                    <th>후원어린이</th>
                                    <th>취소</th>
                                    <th>수정</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applystate_list|filter:{rowselect: true}" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.visit_type_name}} 
                                    </td>
                                    <td>{{item.visit_year }}</td>
                                    <td>{{item.visit_country }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>{{item.conid }}</td>
                                    <td>{{item.apply_date }}</td>
                                    <td>{{item.visit_date1}} </td>
                                    <td>{{item.visit_date2}}</td>
                                    <td>{{item.companion_cnt }}</td>
                                    <td>{{item.child_key }}</td>
                                    <td>{{item.cancel_yn}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 신청현황--%>

                <%--신청정보--%>
                <div class="tab-pane" id="tab2" runat="server">

                    <div class="box-header">
                        <h3 class="box-title">총 {{ i_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered ">
                            <colgroup>
                                <col style="width: 50px" />
                                <col style="width: 50px" />
                                <col style="width: 80px" />
                                <col />
                                <col />
                                <col />
                                <col />
                                <col />
                                <col />
                                <col />
                                <col />
                                <col style="max-width: 150px" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="chkInfoAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th>#</th>
                                    <th>성명</th>
                                    <th>현지숙소명</th>
                                    <th>현지숙소주소</th>
                                    <th>현지숙소 연락처</th>
                                    <th>출국일</th>
                                    <th>귀국일</th>
                                    <th>비상연락처 성함</th>
                                    <th>비상연락처</th>
                                    <th>참가자와의 관계</th>
                                    <th>수정</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applyinfo_list" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>
                                        <label>
                                            <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="info_{{item.applyid }}" icheck /></label></td>
                                    <td>{{item.rownum }}</td>
                                    <td style="min-width: 70px;">{{item.sponsor_name }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.local_accommodation}}</span>
                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" id="{{'info_'+item.applyid + '_local_accommodation'}}" value="{{item.local_accommodation}}" class="form-control" style="width: 100px;" maxlength="100" />
                                        </div>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.local_address}}</span>
                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" id="{{'info_'+item.applyid + '_local_address'}}" value="{{item.local_address}}" class="form-control" style="width: 100px;" maxlength="100" />
                                        </div>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.local_tel}}</span>
                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" id="{{'info_'+item.applyid + '_local_tel'}}" value="{{item.local_tel}}" class="form-control" style="width: 100px;" maxlength="100" />
                                        </div>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.departure_date}}</span>
                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" id="{{'info_'+item.applyid + '_departure_date'}}" value="{{item.departure_date}}" class="date inline form-control" style="width: 95px;" />
                                        </div>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.return_date}}</span>
                                        <div class="inline form-group-sm"></div>
                                        <input type="text" ng-if="item.editmode == true" id="{{'info_'+item.applyid + '_return_date'}}" value="{{item.return_date}}" class="date inline form-control" style="width: 95px;" />
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.emergencycontact_name}}</span>
                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" id="{{'info_'+item.applyid + '_emergencycontact_name'}}" value="{{item.emergencycontact_name}}" maxlength="100" class="form-control" style="width: 100px;" />
                                        </div>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.emergencycontact_tel}}</span>
                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" id="{{'info_'+item.applyid + '_emergencycontact_tel'}}" value="{{item.emergencycontact_tel}}" maxlength="100" class="form-control" style="width: 100px;" />
                                        </div>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.emergencycontact_relation}}</span>
                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" id="{{'info_'+item.applyid + '_emergencycontact_relation'}}" value="{{item.emergencycontact_relation}}" maxlength="100" class="form-control" style="width: 100px;" />
                                        </div>
                                    </td>
                                    <td class="center" ng-class="item.editmode == true ? btnModify : ''">
                                        <input type="button" ng-if="item.editmode != true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="modifyClick(item)" value="수정" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="saveClick(item)" value="저장" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="cancelClick(item)" value="취소" />
                                    </td>
                                </tr>
                                <tr ng-hide="i_total > 0">
                                    <td colspan="12">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="i_page" page-size="i_rowsPerPage" total="i_total" show-prev-next="true" show-first-last="true" paging-action="getApplyInfList({page : page})"></paging>
                    </div>

                    <div class="excel-area" id="divInfo" style="display: none;">
                        <table class="table table-hover table-bordered" id="tableExcel2">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>성명</th>
                                    <th>현지숙소명</th>
                                    <th>현지숙소주소</th>
                                    <th>현지숙소 연락처</th>
                                    <th>출국일</th>
                                    <th>귀국일</th>
                                    <th>비상연락처 성함</th>
                                    <th>비상연락처</th>
                                    <th>참가자와의 관계</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applyinfo_list|filter:{rowselect: true}" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>{{item.local_accommodation}} </td>
                                    <td>{{item.local_address}}  </td>
                                    <td>{{item.local_tel}}</td>
                                    <td>{{item.departure_date}}</td>
                                    <td>{{item.return_date}}</td>
                                    <td>{{item.emergencycontact_name}}</td>
                                    <td>{{item.emergencycontact_tel}}</td>
                                    <td>{{item.emergencycontact_relation}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 신청정보--%>
                <%--참가자정보--%>
                <div class="tab-pane" id="tab3" runat="server">
                    <div class="box-header">
                        <h3 class="box-title">총 {{ p_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover table-bordered ">
                            <colgroup>
                                <col style="width: 50px" />
                                <col style="width: 50px" />
                                <col style="width: 80px" />
                                <col style="width: 50px" />
                                <col style="width: 100px" />
                                <col style="width: 110px" />
                                <col style="width: 170px" />
                                <col style="width: 100px" />
                                <col style="width: auto" />
                                <col style="width: 140px" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="chkParticipantAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th>#</th>
                                    <th>성명</th>
                                    <th>성별</th>
                                    <th>생년월일</th>
                                    <th>휴대번호</th>
                                    <th>이메일</th>
                                    <th>그룹</th>
                                    <th>비고</th>
                                    <th>수정</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in participant_list" ng-class="item.cancel_class" ng-model="item.cancel_class" style="vertical-align: middle;">
                                    <td>
                                        <label>
                                            <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="participant_{{item.applyid }}" icheck /></label></td>

                                    <td>{{item.rownum }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>{{item.gender_code }}</td>
                                    <td>{{item.birth_date }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.tel}}</span>
                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" class="form-control" id="{{'participant_'+item.applyid + '_tel'}}" value="{{item.tel}}" maxlength="13" style="width: 100px;" />
                                        </div>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.email}}</span>
                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" class="form-control" id="{{'participant_'+item.applyid + '_email'}}" value="{{item.email}}" maxlength="50" style="width: 160px;" />
                                        </div>
                                    </td>
                                    <td>{{item.group_type }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.remark}}</span>

                                        <div class="inline form-group-sm">
                                            <input type="text" ng-if="item.editmode == true" class="form-control" id="{{'participant_'+item.applyid + '_remark'}}" value="{{item.remark}}" maxlength="2000" style="width: 100%;" />
                                        </div>
                                    </td>
                                    <td class="center">
                                        <input type="button" ng-if="item.editmode != true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="modifyClick(item)" value="수정" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="saveClick(item)" value="저장" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="cancelClick(item)" value="취소" />
                                    </td>
                                </tr>
                                <tr ng-hide="p_total > 0">
                                    <td colspan="9">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="p_page" page-size="p_rowsPerPage" total="p_total" show-prev-next="true" show-first-last="true" paging-action="getParticipantList({page : page})"></paging>
                    </div>
                    <div class="excel-area" id="divParticipant" style="display: none;">
                        <table class="table table-hover table-bordered " id="tableExcel3">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>성명</th>
                                    <th>성별</th>
                                    <th>생년월일</th>
                                    <th>휴대번호</th>
                                    <th>이메일</th>
                                    <th>그룹</th>
                                    <th>비고</th>
                                    <th>수정</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in participant_list|filter:{rowselect: true}" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>{{item.gender_code }}</td>
                                    <td>{{item.birth_date }}</td>
                                    <td>{{item.tel}}</td>
                                    <td>{{item.email}}</td>
                                    <td>{{item.group_type }}</td>
                                    <td>{{item.remark}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
                <%--end 참가자정보--%>
                <%--참가자주소--%>
                <div class="tab-pane" id="tab4" runat="server">

                    <div class="box-header">
                        <h3 class="box-title">총 {{ d_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered ">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr ng-class="">
                                    <th>
                                        <input type="checkbox" id="chkAddrAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th>#</th>
                                    <th>성명</th>
                                    <th>주소1</th>
                                    <th>주소2</th>
                                    <th>우편번호</th>
                                    <th>수정</th>
                                </tr>
                                <%-- <tr>
                                    <th>지번</th>
                                    <th>도로명</th>
                                </tr>--%>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in addr_list" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>
                                        <label>
                                            <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="{{'p_check_'+item.applyid }}" icheck /></label></td>
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true && item.domesticYN == true">{{item.address_doro }} &nbsp;//&nbsp; {{item.address_jibun }}</span>
                                        <span ng-if="item.editmode != true && item.domesticYN != true">{{item.address_overseas }}</span>

                                        <div ng-if="item.editmode == true" class="form-group-sm text-left">

                                            <input type="hidden" id="locationType_{{item.applyid }}" value="" />
                                            <input type="hidden" id="hfAddressType_{{item.applyid }}" value="" />

                                            <input type="hidden" id="zipcode_{{item.applyid }}" />
                                            <input type="hidden" id="addr1_{{item.applyid }}" />
                                            <input type="hidden" id="addr2_{{item.applyid }}" />
                                            <input type="hidden" id="dspAddrJibun_{{item.applyid }}" value="" />
                                            <input type="hidden" id="dspAddrDoro_{{item.applyid }}" value="" />


                                            <div class="location form-group-sm">
                                                <div class="pull-left" style="width: 30%;">
                                                    <label for="selectlocation_{{item.applyid }}" class="hidden">국내/해외 </label>
                                                    <select id="selectlocation_{{item.applyid }}" onchange="locationChange(this)" class="form-control" style="width: 100%;">
                                                        <option ng-repeat="lst in select_location" ng-selected="item.location == lst.codevalue" value="{{lst.codevalue}}">{{lst.codename}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="pn_addr_domestic_{{item.applyid }}" style="display: none">
                                                <div class="pull-left">
                                                    <label for="zipcode" class="hidden">주소찾기</label>
                                                    <a href="javascript:void(0)" class="btn btn-default" runat="server" id="popup" ng-click="addrpopup(item, $event)">주소찾기</a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="hidden" id="addr_domestic_zipcode_{{item.applyid }}" />
                                                    <input type="hidden" id="addr_domestic_addr1_{{item.applyid }}" />
                                                    <input type="hidden" id="addr_domestic_addr2_{{item.applyid }}" />
                                                    <div class="pull-left" style="padding-top: 5px;">
                                                        <p id="addr_road_{{item.applyid }}" class="margin-bottom">{{item.full_doro }} </p>
                                                        <p id="addr_jibun_{{item.applyid }}" class="">{{item.full_jibun }} </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- 해외주소 체크 시 -->
                                            <div id="pn_addr_overseas_{{item.applyid }}" style="display: none">
                                                <div class="form-group-sm">
                                                    <div class="pull-left" style="width: 30%;">
                                                        <label for="selectHouseCountry_{{item.applyid }}" class="hidden">국가선택</label>
                                                        <select id="selectHouseCountry_{{item.applyid }}" class="form-control" style="width: 100%;">
                                                            <option ng-repeat="lst in countries" ng-selected="item.location == lst.codename" value="{{lst.codename}}">{{lst.codename}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="pull-left" style="width: 40%;">
                                                        <label for="addr_overseas_zipcode_{{item.applyid }}" class="hidden">우편번호</label>
                                                        <input type="text" id="addr_overseas_zipcode_{{item.applyid }}" class="form-control" value="{{item.zipcode_overseas}}" style="width: 100%" placeholder="우편번호" maxlength="150" />
                                                    </div>
                                                </div>
                                                <div class="form-group-sm">
                                                    <div style="width: 100%;">
                                                        <input type="text" id="addr_overseas_addr1_{{item.applyid }}" class="form-control" value="{{item.address_overseas}}" placeholder="주소" style="width: 100%;" maxlength="1000" />
                                                    </div>
                                                </div>
                                                <div class="form-group-sm">
                                                    <div style="width: 100%;">
                                                        <input type="text" id="addr_overseas_addr2_{{item.applyid }}" class="form-control" value="{{item.addr2_overseas}}" placeholder="상세주소" style="width: 100%;" maxlength="1000" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!--// -->
                                        </div>

                                    </td>
                                    <%-- <td ng-if="item.addr_colspan == 1"><span ng-if="item.editmode != true">{{item.address_doro }}</span></td>--%>
                                    <td><span ng-if="item.editmode != true">{{item.address2 }}</span></td>
                                    <td><span ng-if="item.editmode != true">{{item.zipcode }}</span></td>
                                    <td class="center">
                                        <input type="button" ng-if="item.editmode != true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="modifyClick(item)" value="수정" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="saveClick(item)" value="저장" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="cancelClick(item)" value="취소" />
                                    </td>
                                </tr>
                                <tr ng-hide="d_total > 0">
                                    <td colspan="7">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="d_page" page-size="d_rowsPerPage" total="d_total" show-prev-next="true" show-first-last="true" paging-action="getAddrList({page : page})"></paging>
                    </div>

                    <div class="box-body table-responsive no-padding" style="display: none;">
                        <table class="table table-hover table-bordered" id="tableExcel4">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>성명</th>
                                    <th>주소1</th>
                                    <th>주소2</th>
                                    <th>우편번호</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in addr_list | filter:{rowselect: true}">
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>
                                        <span ng-if="item.domesticYN == true">{{item.address_doro }} &nbsp;//&nbsp; {{item.address_jibun }}</span>
                                        <span ng-if="item.domesticYN != true">{{item.address_overseas }}</span>
                                    </td>
                                    <td><span>{{item.address2 }}</span></td>
                                    <td><span>{{item.zipcode }}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 참가자주소--%>
                <%--SMS/MMS--%>
                <div class="tab-pane" id="tab5" runat="server">
                    <div class="box-body">
                        <div style="float: left; width: 30%">
                            <div class="form-horizontal box-body">


                                <div class="box-body table-responsive no-padding">

                                    <table class="table table-hover table-bordered" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" id="chkSmsAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                                <th>#</th>
                                                <th>성명</th>
                                                <th>휴대번호</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in sms_list" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                                <td>
                                                    <label>
                                                        <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="check{{item.userid }}" icheck /></label></td>
                                                <td>{{item.rownum}}</td>
                                                <td>{{item.sponsor_name}}</td>
                                                <td>{{item.tel}}</td>
                                            </tr>
                                            <tr ng-hide="m_total > 0">
                                                <td colspan="4">데이터가 없습니다.</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                                <div class="box-footer clearfix text-center">
                                    <paging class="small" page="m_page" page-size="m_rowsPerPage" total="m_total" show-prev-next="true" show-first-last="true" paging-action="getSMSList({page : page})"></paging>
                                </div>
                            </div>
                        </div>
                        <div style="float: left; width: 30%">

                            <div class="form-horizontal box-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <asp:TextBox runat="server" ID="txtTitle" CssClass="form-control" Width="100%" PlaceHolder="제목" MaxLength="400"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-12">
                                        <asp:TextBox runat="server" ID="txtMessage" CssClass="form-control" Width="100%" TextMode="MultiLine" Rows="12" PlaceHolder="내용" MaxLength="1000"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="s_result" class="col-sm-3 control-label">발신번호</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox runat="server" ID="txtSendTel" CssClass="form-control" Width="100%" MaxLength="11"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="s_result" class="col-sm-3 control-label">발송구분</label>
                                    <div class="col-sm-9 form-radio" style="margin-top: 5px;">
                                        <asp:RadioButtonList runat="server" ID="rdoSendType" RepeatLayout="Flow" RepeatDirection="Horizontal" Width="100%">
                                            <asp:ListItem Text="즉시발송" Value="D"></asp:ListItem>
                                            <asp:ListItem Text="예약발송" Value="R"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="s_result" class="col-sm-3 control-label">예약날짜</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="reservedatetime" runat="server" id="txtReserveDate" readonly="readonly" class="date datetime form-control inline" style="width: 100%; margin-right: 5px" />

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="float: left; width: 40%">
                            <div class="form-horizontal box-body">
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <asp:TextBox runat="server" ID="txtTelAdd" CssClass="form-control fc_numeric" Width="100%" PlaceHolder="휴대번호 직접입력" MaxLength="11"></asp:TextBox>
                                    </div>
                                    <label for="s_result" class="col-sm-3"><a class="btn btn-default" ng-click="addTel()">추가</a></label>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <label>
                                            받는사람
                                        </label>
                                        <label style="float: right; color: darkred; font-size: 10px;">
                                            *중복번호 1회 발송
                                        </label>
                                        <select multiple="multiple" id="ddlSMS" style="border: 1px solid; border-radius: 0; box-shadow: none; border-color: #d2d6de; width: 100%; min-height: 180px; height: auto;">
                                            <option ng-repeat="lst in sms_inputs" value="{{lst.id}}">{{lst.tel}}</option>
                                        </select>
                                    </div>
                                    <label for="s_result" class="col-sm-3">
                                        <a class="btn btn-default" ng-click="selectSMSList()">선택 불러오기</a>
                                        <a class="btn btn-default" ng-click="messageResultPopup()">결과보기</a>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-11">
                                        <a class="btn btn-default" ng-click="deleteSMSList()">선택삭제</a>
                                        <a class="btn btn-default" ng-click="clearSMSList()">전체삭제</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-11">
                                        <a class="btn btn-danger" ng-click="sendSMS()">전송하기</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--end SMS/MMS--%>
                <%--통계--%>
                <div class="tab-pane" id="tab6" runat="server">
                    <div class="box-header">
                        <h3 class="box-title">총 {{ s_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th rowspan="2">#</th>
                                    <th rowspan="2">방문국가명</th>
                                    <th rowspan="2">방문일</th>
                                    <th rowspan="2">참가자수<br />
                                        (신청자+동반인)</th>
                                    <th colspan="2">성별</th>
                                    <th colspan="6">연령</th>
                                </tr>
                                <tr>
                                    <th>남</th>
                                    <th>여</th>
                                    <th>10대 이하</th>
                                    <th>10대</th>
                                    <th>20대</th>
                                    <th>30대</th>
                                    <th>40대</th>
                                    <th>50대이상</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in statisticts_list">
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.visit_country_name }}</td>
                                    <td>{{item.visit_date }}</td>
                                    <td>{{item.companion_cnt }}</td>
                                    <td>{{item.male_cnt }}</td>
                                    <td>{{item.female_cnt }}</td>
                                    <td>{{item.child }}</td>
                                    <td>{{item.ten }}</td>
                                    <td>{{item.twenty }}</td>
                                    <td>{{item.thirty }}</td>
                                    <td>{{item.forty }}</td>
                                    <td>{{item.fifty }}</td>
                                </tr>
                                <tr ng-hide="s_total > 0">
                                    <td colspan="12">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="s_page" page-size="s_rowsPerPage" total="s_total" show-prev-next="true" show-first-last="true" paging-action="getStatisticsList({page : page})"></paging>
                    </div>

                    <div class="box-body table-responsive no-padding" style="display: none;">
                        <table class="table table-hover table-bordered" id="tableExcel6">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th rowspan="2">#</th>
                                    <th rowspan="2">방문국가명</th>
                                    <th rowspan="2">방문일</th>
                                    <th rowspan="2">참가자수<br />
                                        (신청자+동반인)</th>
                                    <th colspan="2">성별</th>
                                    <th colspan="6">연령</th>
                                </tr>
                                <tr>
                                    <th>남</th>
                                    <th>여</th>
                                    <th>10대 이하</th>
                                    <th>10대</th>
                                    <th>20대</th>
                                    <th>30대</th>
                                    <th>40대</th>
                                    <th>50대이상</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in statisticts_list">
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.visit_country_name }}</td>
                                    <td>{{item.visit_date }}</td>
                                    <td>{{item.companion_cnt }}</td>
                                    <td>{{item.male_cnt }}</td>
                                    <td>{{item.female_cnt }}</td>
                                    <td>{{item.child }}</td>
                                    <td>{{item.ten }}</td>
                                    <td>{{item.twenty }}</td>
                                    <td>{{item.thirty }}</td>
                                    <td>{{item.forty }}</td>
                                    <td>{{item.fifty }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 통계--%>
            </div> 


            <!-- /.box-header -->
            <%--   <div class="box-footer clearfix text-center">
                <asp:LinkButton runat="server" href="#" ID="btn_add" OnClientClick="goPage('update.aspx' , {t:'insert'})" CssClass="btn btn-bitbucket pull-right">결과보기</asp:LinkButton>
            </div>--%>
        </div>

    </section>
</asp:Content>
