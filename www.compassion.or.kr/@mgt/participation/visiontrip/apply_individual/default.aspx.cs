﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
using System.Data;

public partial class mgt_participation_visiontrip_apply_individual_default : AdminBoardPage {

    CommonLib.WWW6Service.SoaHelperSoap _www6Service;


    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        //if (AdminLoginSession.HasCookie(this.Context))
        //{
        //    Response.Redirect("/@mgt/dashboard/");
        //}

        //v_admin_auth auth = base.GetPageAuth();
        //btn_add.Visible = auth.aa_auth_create;
        //btn_add.Attributes.Add("href", "update.aspx");

        //년도
        for (int i = 2005; i <= DateTime.Now.AddYears(1).Year; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlYear.SelectedValue = DateTime.Now.Year.ToString();



        //using (FrontDataContext dao = new FrontDataContext())
        //{
        //    var countryList = dao.country.OrderBy(p => p.c_name).ToList();

        //    foreach (country list in countryList)
        //    {
        //        ddlVisitCountry.Items.Add(new ListItem(list.c_name, list.c_id.ToString()));
        //    }
        //}

        var countrylist = www6.selectQ<country>("c_name");

        foreach ( country list in countrylist)
        {
            ddlVisitCountry.Items.Add(new ListItem(list.c_name, list.c_id.ToString()));
        }

    }

    protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();

		//btn_add.Attributes.Add("href", "update.aspx");
	}

	protected override void GetList(int page) {

		Master.IsSearch = false;
		//if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
		//	Master.IsSearch = true;
		//}


  //      string bDate = string.Empty;
  //      string eDate = string.Empty;

  //      if (s_dateType.SelectedItem.Value == "year") {
  //          bDate = s_b_year.SelectedValue + "-01-01";
  //          eDate = s_b_year.SelectedValue + "-12-31";
  //      }
  //      else {
  //          bDate = s_b_date.Text;
  //          eDate = s_e_date.Text;
  //      }

  //      using (AdminDataContext dao = new AdminDataContext()) {

  //          //var list = dao.sp_visiontrip_list(page, paging.RowsPerPage, "vision,request" , "", s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
  //          var list = dao.sp_visiontrip_list(page, paging.RowsPerPage, s_tripType.SelectedValue, "", s_keyword.Text, bDate, eDate).ToList();
  //          var total = 0;

		//	if (list.Count > 0)
		//		total = list[0].total.Value;

		//	lbTotal.Text = total.ToString();

		//	paging.CurrentPage = page;
		//	paging.Calculate(total);
		//	repeater.DataSource = list;
		//	repeater.DataBind();

		//}
	}
     
    protected void btnTripYearSet_Click(object sender, EventArgs e)
    {
        //using (MainLibDataContext dao = new MainLibDataContext())
        //{
        //    var s = dao.code.First(p => p.cd_group == "trip" && p.cd_key == "year");
        //    s.cd_value = a_type.SelectedValue;
        //    Master.ValueMessage.Value = "저장되었습니다.";
        //    dao.SubmitChanges();
        //}
    }
     

    protected void btnStatistics_Click(object sender, EventArgs e)
    { 
        //string strYear = ddlYear.SelectedValue;
        //string strTripType = ddlTripType.SelectedValue;
        //string strTripID = ddlTripName.SelectedValue;

        //using (AdminDataContext dao = new AdminDataContext())
        //{ 
        //    var list = dao.sp_tVisionTripStatistics_list_p(1, paging.RowsPerPage, strTripType, strYear, Convert.ToInt32(strTripID)).ToList();
        //    var total = 0;

        //    if (list.Count > 0)
        //        total = list[0].total.Value;

        //    lbTotal.Text = total.ToString();

        //    paging.CurrentPage = 1;
        //    paging.Calculate(total);
        //    repeater.DataSource = list;
        //    repeater.DataBind(); 
        //} 
    }

    protected void ListBound(object sender, RepeaterItemEventArgs e)
    {

        //if (repeater != null)
        //{
        //    if (e.Item.ItemType != ListItemType.Footer)
        //    {
        //        sp_tVisionTripSchedule_listResult entity = e.Item.DataItem as sp_tVisionTripSchedule_listResult;
        //        ((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
        //        //노출여부
        //        ((Literal)e.Item.FindControl("ltTripViewYN")).Text = entity.TripViewYN == 'Y' ? "O" : "X";
        //        //트립안내 등록여부
        //        ((Literal)e.Item.FindControl("ltTripNotice")).Text = !string.IsNullOrEmpty(entity.TripNotice) ? "O" : "X";

        //        // 신청일
        //        string date = "";
        //        if (entity.OpenDate.EmptyIfNull() != "")
        //        {
        //            date = entity.OpenDate.ToString().Substring(0, 10);
        //        }
        //        if (entity.CloseDate.EmptyIfNull() != "")
        //        {
        //            date += " ~ " + entity.CloseDate.ToString().Substring(0, 10);
        //        }
        //        ((Literal)e.Item.FindControl("ltDate")).Text = date;

        //        string status = "-";
        //        switch (entity.TripState.ToString())
        //        {
        //            case "Apply": status = "신청중"; break;
        //            case "Expect": status = "예정"; break;
        //            case "Deadline": status = "마감"; break;
        //            case "Close": status = "종료"; break;
        //            default: status = "-"; break;
        //        }
        //        ((Literal)e.Item.FindControl("ltTripState")).Text = status;
        //        //통계여부
        //        ((Literal)e.Item.FindControl("ltStatisticsYN")).Text = entity.StatisticsYN == 'Y' ? "O" : "X";
        //    }
        //}

    }
}
