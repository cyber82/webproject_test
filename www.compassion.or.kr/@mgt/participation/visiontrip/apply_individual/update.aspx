﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_participation_visiontrip_apply_Individual_update" ValidateRequest="false" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">

<head id="Head1" runat="server">
    <title>compassion Admin</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/@mgt/template/bootstrap/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="/@mgt/template/dist/css/AdminLTE.min.css" />
    <!-- iCheck -->
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/square/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/minimal/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/flat/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/dist/css/skins/skin-green.min.css" />
    <!-- Date Picker -->
    <link rel="stylesheet" href="/@mgt/template/plugins/datepicker/datepicker3.css" />
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/@mgt/template/plugins/daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" href="/@mgt/common/css/admin.css" />
    <link rel="stylesheet" href="/@mgt/common/js/bootstrap-switch/bootstrap-switch.min.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 2.1.4 -->
    <script src="/@mgt/template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!-- Bootstrap 3.3.5 -->
    <script src="/@mgt/template/bootstrap/js/bootstrap.min.js"></script>
    <script src="/@mgt/common/js/bootstrap-switch/bootstrap-switch.min.js"></script>

    <!-- iCheck -->
    <script src="/@mgt/template/plugins/iCheck/icheck.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="/@mgt/template/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->

    <script src="/@mgt/template/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="/@mgt/template/plugins/datepicker/locales/bootstrap-datepicker.kr.js"></script>

    <script type="text/javascript" src="/@mgt/common/js/common.js" defer="defer"></script>
    <script type="text/javascript" src="/@mgt/common/js/message.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/jquery.json-2.4.min.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/jquery.json-3.3.2.min.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/form.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/cookie.js" defer="defer"></script>

    <script type="text/javascript" src="/assets/jquery/wisekit/function.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.search.js" defer="defer"></script>
    <script type="text/javascript" src="/@mgt/common/js/ajaxupload.3.6.wisekit.js" defer="defer"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.bootstrap.js"></script>



    <%--    <script type="text/javascript" src="/assets/angular/1.4.8/angular.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0-beta.2/angular-sanitize.js"></script>

    <script type="text/javascript" src="/assets/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="/assets/angular/angular-moment.min.js"></script>

    <script type="text/javascript" src="/@mgt/participation/visiontrip/apply_plan/update.js"></script>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>  
    <script type="text/javascript" src="/common/js/site/angular-app.js"></script>--%>

    <script type="text/javascript">
        $(function () {

            $("#btn_close").click(function () {
                window.close();
            });

            if ($("#message").val() != "") {
                alert($("#message").val());
                if ($("#action").val() == "close") {
                    window.opener.location.reload();
                    self.close();
                }
            } 

            $("#btnAdd").click(function () {
                addTextbox();
            });

            //주소 - location 변경
            $(".rd_addr").change(function (sender) {
                var val = $(this).attr("id");
                var checked = $(this).prop("checked");

                if (val == "addr_domestic" && checked) {
                    $(".hide_overseas").show();
                    $("#pn_addr_domestic").show();
                    $("#pn_addr_overseas").hide();
                } else {
                    $(".hide_overseas").hide();
                    $("#pn_addr_domestic").hide();
                    $("#pn_addr_overseas").show();
                }
            });

            if ($("#addr_domestic").prop("checked")) {
                $(".hide_overseas").show();
                $("#pn_addr_domestic").show();
                $("#pn_addr_overseas").hide();
            } else {
                $(".hide_overseas").hide();
                $("#pn_addr_domestic").hide();
                $("#pn_addr_overseas").show();
            }


            //selectbox change event 
            //기독교일 경우에만 교회 입력
            $("#txtChurch").hide();
            $("#ddlReligion").change(function () {
                if ($("#ddlReligion option:selected").val() == "Christian") {
                    $("#txtChurch").show();
                } else {
                    $("#txtChurch").hide();
                }
            });

            $("#ddlCashReceiptType").change(function () {
                if ($("#ddlCashReceiptType option:selected").val() == "M") {
                    $("#txtCashReceiptRelation").hide();
                    $("#txtCashReceiptName").val($("#txtUserName").val());
                    $("#txtCashReceiptTel").val($("#txtTel").val());
                    $("#txtCashReceiptRelation").val("본인");
                } else {
                    $("#txtCashReceiptRelation").show();
                    $("#txtCashReceiptName").val("");
                    $("#txtCashReceiptTel").val("");
                    $("#txtCashReceiptRelation").val("");
                }
            });

            $("#ddlChildMeetYn").change(function () {
                if ($("#ddlChildMeetYn option:selected").val() == "N") {
                    $("#spMultiSelect").hide();
                    $("#plChildtext").hide();
                } else {
                    $("#spMultiSelect").show();
                    $("#plChildtext").show();
                }
            });

            $("#ddlRoomType").change(function () {
                if ($("#ddlRoomType option:selected").val() == "S") {
                    $("#txtRoomDetail").hide();
                } else {
                    $("#txtRoomDetail").show();
                }
            });
            // end
        });

        var btnUserSearchClick = function () {
            if ($("#hdnSponsorID").val() != "" && $("#ddlSchedule").val() != "") {
                if (confirm("선택된 정보가 초기화 됩니다. 사용자 정보 조회하시겠습니까?")) {
                    return true;
                }
                else
                    return false;
            }
            else {
                return true;
            }
        }


        var txtCount = 0;
        var addTextbox = function () {
            var s = "";

            txtCount++;
            s = '<div style="width: 100%; float: left; vertical-align: middle;" class="btnDelete' + txtCount + ' divAddChild"><input type="text" style="width:150px;" class="childtextbox form-control pull-left" maxlength="100" id="txtAdd' + txtCount + '">';
            s += '<input type="button" class="btnChilddelete btn btn-default" id="btnDelete' + txtCount + '" value="삭제" onclick="delClick(this)"></div>';
            var div = $("#divChild").html();
            $("#divChild").append(s);
            console.log(txtCount);
        }

        var delClick = function (val) {
            var btnID = val.id;

            $("." + btnID).remove();
        };
        var delRepeaterClick = function (val) {
            $("#" + val).hide();
            $("#" + val).find(".hdnDelYN").val("Y");
        };

        var addrPopup = function () {
            cert_setDomain();
            var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");
        }
        var selectAddressRow;
        function jusoCallback(zipNo, addr1, addr2, jibun) {
            // 실제 저장 데이타
            $("#addr1").val(addr1 + "//" + jibun);
            $("#addr2").val(addr2);
            // 화면에 표시
            $("#zipcode").val(zipNo);

            $("#addr_road").text("[도로명주소] (" + zipNo + ") " + addr1 + " " + addr2);
            $("#addr_jibun").text("[지번주소] (" + zipNo + ") " + jibun + " " + addr2);

            $("#dspAddrJibun").val(jibun);
            $("#dspAddrDoro").val(addr1);

            $("#addr_domestic_zipcode").val($("#zipcode").val());
            $("#addr_domestic_addr1").val($("#addr1").val());
            $("#addr_domestic_addr2").val($("#addr2").val());
        };
        function cert_setDomain() {
            //return;
            //if (location.hostname.startsWith("auth")) return;
            if (location.hostname.indexOf("compassionko.org") > -1)
                document.domain = "compassionko.org";
            else if (location.hostname.indexOf("compassionkr.com") > -1)
                document.domain = "compassionkr.com";
            else
                document.domain = "compassion.or.kr";
        }


        var onSubmit = function () {
            var addChild = "";
            $(".childtextbox").each(function () {
                var text = $(this).val();
                //alert(text);
                addChild += text + "^";
            });
            $("#hdnAddText").val(addChild);

            if (!validateForm([
                  { id: "#hdnSponsorID", msg: "사용자 조회해주세요." },
                  { id: "#txtUserName", msg: "성함을 입력하세요." },
                  { id: "#txtTel", msg: "휴대번호를 입력하세요." },
                  { id: "#addr1", msg: "주소를 입력하세요." },
                //{ id: "#addr2", msg: "상세 주소를 입력하세요." },
                  { id: "#ddlVisitType", msg: "방문유형을 선택하세요." },
                  { id: "#txtVisitDate1", msg: "방문날짜(1지망)을 선택하세요." },
                  { id: "#txtVisitDate2", msg: "방문날짜(2지망)을 선택하세요." },
                  { id: "#ddlVisitCountry", msg: "방문국가를 선택하세요." }
            ])) {
                return false;
            }
            else {
                return true;
            }
        }




    </script>
</head>

<body>
    <form runat="server" id="form">
        <input type="hidden" id="action" runat="server" value="" />
        <input type="hidden" id="message" runat="server" value="" />

        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><strong>개인방문 신청서</strong></h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="form-horizontal">


                                <div class="box-header ">
                                    <h3 class="box-title">사용자 정보</h3>
                                </div>

                                <div class="box-body">
                                    <div class="form-group insert_form">
                                        <label class="col-sm-2 control-label">사용자정보 조회</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox runat="server" ID="txtUserID" CssClass="form-control pull-left" Style="width: 150px;" placeholder="ConID를 입력하세요"></asp:TextBox>
                                            <asp:Button runat="server" ID="btnUserSearch" Text="검색" CssClass="btn btn-default pull-left" OnClick="btnUserSearch_Click" />
                                            <asp:HiddenField runat="server" ID="hdnUserID" />
                                            <asp:HiddenField runat="server" ID="hdnSponsorID" />
                                            <asp:HiddenField runat="server" ID="hdnConID" />
                                        </div>
                                    </div>
                                    <div runat="server" id="divUserInfo">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">성함</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox runat="server" ID="txtUserName" CssClass="form-control" Width="400" MaxLength="30" ReadOnly="true"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">영문성함</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox runat="server" ID="txtUserNameEng" CssClass="form-control" Width="400" MaxLength="40"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">휴대번호</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox runat="server" ID="txtTel" CssClass="form-control" Width="400" MaxLength="13"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">이메일</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" Width="400" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">주소</label>
                                            <div class="col-sm-10">
                                                <div>
                                                    <input type="hidden" runat="server" id="locationType" />
                                                    <input type="hidden" runat="server" id="hfAddressType" value="" />

                                                    <input type="hidden" runat="server" id="zipcode" />
                                                    <input type="hidden" id="addr1" runat="server" />
                                                    <input type="hidden" id="addr2" runat="server" />
                                                    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
                                                    <input type="hidden" runat="server" id="dspAddrDoro" value="" />

                                                    <span class="radio_ui">
                                                        <input type="radio" runat="server" class="rd_addr css_radio" id="addr_domestic" name="addr_location" />
                                                        <label for="addr_domestic" class="css_label">국내</label>

                                                        <input type="radio" runat="server" class="rd_addr css_radio" id="addr_oversea" name="addr_location" />
                                                        <label for="addr_oversea" class="css_label ml20">해외</label>
                                                    </span>

                                                    <span id="pn_addr_domestic" runat="server" style="display: none">
                                                        <label for="zipcode" class="hidden">주소찾기</label>
                                                        <a href="javascript:void(0)" class="btn btn-default" runat="server" id="popup" onclick="addrPopup()">주소찾기</a>
                                                        <input type="hidden" id="addr_domestic_zipcode" runat="server" />
                                                        <input type="hidden" id="addr_domestic_addr1" runat="server" />
                                                        <input type="hidden" id="addr_domestic_addr2" runat="server" />

                                                        <p id="addr_road" class="mt15" runat="server"></p>
                                                        <p id="addr_jibun" class="mt10" runat="server"></p>
                                                    </span>

                                                    <!-- 해외주소 체크 시 -->
                                                    <div id="pn_addr_overseas" runat="server" style="display: none; width: 400px;">
                                                        <span class="sel_type2 fl" style="width: 195px;">
                                                            <label for="ddlHouseCountry" class="hidden">국가 선택</label>
                                                            <asp:DropDownList runat="server" ID="ddlHouseCountry" class="form-control" Style="width: 195px;"></asp:DropDownList>

                                                        </span>
                                                        <label for="addr_overseas_zipcode" class="hidden">우편번호</label>

                                                        <input type="text" runat="server" id="addr_overseas_zipcode" class="form-control" value="" style="width: 195px" placeholder="우편번호" maxlength="50" />
                                                        <input type="text" runat="server" id="addr_overseas_addr1" class="form-control" placeholder="주소" style="width: 400px;" maxlength="1000" />
                                                        <input type="text" runat="server" id="addr_overseas_addr2" class="form-control" placeholder="상세주소" style="width: 400px;" maxlength="1000" />
                                                    </div>
                                                    <!--// -->
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>



                                <div runat="server" id="divApplyInfo">
                                    <div class="box-header">
                                        <h3 class="box-title">신청서 정보</h3>
                                    </div>
                                    <div class="box-body">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">방문유형 선택</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <asp:DropDownList runat="server" ID="ddlVisitType" class="form-control" Width="200">
                                                    <asp:ListItem Text="--선택--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="후원어린이 만남" Value="M"></asp:ListItem>
                                                    <asp:ListItem Text="어린이센터 방문" Value="V"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">방문날짜</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <input type="text" id="txtVisitDate1" runat="server" class="form-control date visitdate pull-left" placeholder="방문날짜(1지망)" style="width: 200px" />
                                                <input type="text" id="txtVisitDate2" runat="server" class="form-control date visitdate" placeholder="방문날짜(2지망)" style="width: 200px" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">방문국가</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <asp:DropDownList runat="server" ID="ddlVisitCountry" class="form-control" Width="200">
                                                    <asp:ListItem Text="--선택--" Value="" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label control-label">방문국가 영문명</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox runat="server" ID="txtVisitCountryEng" CssClass="form-control" Width="400"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">종교</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <asp:DropDownList runat="server" ID="ddlReligion" CssClass="form-control" Style="width: 200px;">
                                                    <asp:ListItem Text="선택하세요" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="기독교" Value="Christian"></asp:ListItem>
                                                    <asp:ListItem Text="불교" Value="Buddhist"></asp:ListItem>
                                                    <asp:ListItem Text="천주교" Value="Catholic"></asp:ListItem>
                                                    <asp:ListItem Text="무교" Value="Atheism"></asp:ListItem>
                                                    <asp:ListItem Text="기타" Value="Etc"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox runat="server" ID="txtChurch" CssClass="form-control" MaxLength="25" Width="200" placeholder="교회명"></asp:TextBox>
                                            </div>
                                        </div>


                                        <%--                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">동반인 정보</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <asp:Button ID="AddNewRow" runat="server" Text="Button" CommandName="Add" />
                                                <asp:Repeater ID="RepeaterDetailsRow" runat="server" OnItemCommand="RepeaterDetailsRow_ItemCommand">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtMyTextBox" runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:Repeater>


                                            </div>
                                        </div>--%>



                                        <div class="form-group" >
                                            <label class="col-sm-2 control-label control-label">후원어린이 선택</label>
                                            <div class="col-sm-10">
                                                <asp:CheckBoxList runat="server" ID="chkChildList">
                                                </asp:CheckBoxList>
                                                <input type="button" id="btnAdd" class="btn btn-default" value="직접 입력 추가" />
                                                <asp:HiddenField runat="server" ID="hdnAddText" Value="" />
                                                <asp:PlaceHolder runat="server" ID="phChildList"></asp:PlaceHolder>
                                                <div id="divChild">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">현지정보</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <input type="text" id="txtLocalAccommodation" runat="server" maxlength="100" class="form-control pull-left" value="" style="width: 200px;" placeholder="현지 숙소명" />
                                                <input type="text" id="txtLocalTel" runat="server" maxlength="100" class="form-control" value="" style="width: 200px;" placeholder="현지숙소 연락처" />
                                                <input type="text" id="txtLocalAddress" runat="server" maxlength="100" class="form-control" value="" style="width: 400px;" placeholder="현지숙소 주소" />
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">출국일/귀국일</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <input type="text" id="txtDepartureDate" runat="server" class="form-control date fdate pull-left" placeholder="출국일" style="width: 200px" />
                                                <input type="text" id="txtReturnDate" runat="server" class="form-control date " placeholder="귀국일" style="width: 200px" />
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">비상연락처</label>
                                            <div class="col-sm-10" style="margin-top: 5px;">
                                                <asp:TextBox runat="server" ID="txtEmergencyContactName" CssClass="form-control" Width="400" MaxLength="100" placeholder="비상연락처 - 성함"></asp:TextBox>

                                                <asp:TextBox runat="server" ID="txtEmergencyContactTel" CssClass="form-control" Width="400" MaxLength="100" placeholder="비상연락처 - 휴대번호"></asp:TextBox>

                                                <asp:TextBox runat="server" ID="txtEmergencyContactRelation" CssClass="form-control" Width="400" MaxLength="100" placeholder="비상연락처 - 참가자와의 관계"></asp:TextBox>

                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-right">
                            <asp:Button runat="server" ID="btn_save" Text="등록하기" CssClass="btn btn-danger" OnClick="btn_save_ServerClick" OnClientClick="return onSubmit()" />

                            <button class="btn btn-default" runat="server" id="btn_close"><i class="fa fa-times"></i>닫기</button>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->

    </form>

</body>
</html>
