﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using CommonLib;

public partial class mgt_participation_visiontrip_apply_individual_companion : AdminBasePage {

	
	protected string ids{
		set {
			this.ViewState.Add("ids", value);
		}
		get {
			if (this.ViewState["ids"] == null) return "";
			return this.ViewState["ids"].ToString();
		}
	}


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		v_admin_auth auth = base.GetPageAuth(); 

		ids = Request["r_id"];

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var companionList = dao.tVisionTripCompanion.Where(p => p.IndividualDetailID == Convert.ToInt32(ids) && p.CurrentUse == 'Y').ToList();
            var companionList = www6.selectQ<tVisionTripCompanion>("IndividualDetailID", Convert.ToInt32(ids), "CurrentUse", "Y");


            var total = 0;
            if (companionList.Count > 0)
                total = companionList.Count;

            lbTotal.Text = total.ToString();
             
            repeater.DataSource = companionList;
            repeater.DataBind(); 
        }

    }

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack(); 
	}


    protected void ListBound(object sender, RepeaterItemEventArgs e)
    { 
        if (repeater != null)
        {
            if (e.Item.ItemType != ListItemType.Footer)
            {
                tVisionTripCompanion entity = e.Item.DataItem as tVisionTripCompanion;
                ((Literal)e.Item.FindControl("lbIdx")).Text = (e.Item.ItemIndex +1 ).ToString(); 
            }
        }

    } 

}