﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
using System.Data;
using System.Web.UI.HtmlControls;


public partial class mgt_participation_visiontrip_apply_Individual_update : AdminBoardPage
{


    CommonLib.WWW6Service.SoaHelperSoap _www6Service;
    protected string ids
    {
        set
        {
            this.ViewState.Add("ids", value);
        }
        get
        {
            if (this.ViewState["ids"] == null) return "";
            return this.ViewState["ids"].ToString();
        }
    }


    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        v_admin_auth auth = base.GetPageAuth();
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        ids = Request["r_id"];

        divUserInfo.Visible = false;
        divApplyInfo.Visible = false;

        #region 방문국가
        //using (AdminDataContext dao = new AdminDataContext())
        //{
        //    var list = dao.country.OrderBy(p => p.c_name).ToList();
        //    ddlVisitCountry.Items.Add(new ListItem("전체", ""));

        //    foreach (country item in list)
        //    {
        //        ddlVisitCountry.Items.Add(new ListItem(item.c_name, item.c_id.ToString()));
        //    }
        //}

        string dbName = "SqlCompassionWeb";
        object[] objSql = new object[1] { "SELECT * from country order by c_name" };
        string dbType = "Text";
        DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, null, null);
        List<country> countrylist = ds.Tables[0].DataTableToList<country>();

        foreach (country list in countrylist)
        {
            ddlVisitCountry.Items.Add(new ListItem(list.c_name, list.c_id.ToString()));
        }
        #endregion
    }

    protected override void OnAfterPostBack()
    {
        base.OnAfterPostBack();
        message.Value = "";
    }




    protected void btnUserSearch_Click(object sender, EventArgs e)
    {

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();


        #region 국외인경우 국가목록 
        var actionResult = new CodeAction().Countries();
        if (!actionResult.success)
        {
            base.AlertWithJavascript(actionResult.message);
        }

        ddlHouseCountry.DataSource = actionResult.data;
        ddlHouseCountry.DataTextField = "CodeName";
        ddlHouseCountry.DataValueField = "CodeName";
        ddlHouseCountry.DataBind();
        #endregion 

        #region Compass - tSponsorGroup : 사용자 정보
        Object[] objSql = new object[1] { "VT_WebInfoInquiry" };
        Object[] objParam = new object[] { "DIVIS", "ConID" };
        Object[] objValue = new object[] { "BaseInfo", txtUserID.Text };


        string addr_zipcode, addr1_doro, addr1_jibun, addr1_overseas, addr2_overseas,
            addr2_doro, addr2_jibun;

        var userinfo = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
        if (userinfo.Rows.Count > 0)
        {
            divUserInfo.Visible = true;
            divApplyInfo.Visible = true;
            addr_zipcode = userinfo.Rows[0]["ZipCode"].ToString();
            addr1_doro = userinfo.Rows[0]["Doro_Addr1"].ToString();
            addr2_doro = userinfo.Rows[0]["Doro_Addr2"].ToString();
            addr1_jibun = userinfo.Rows[0]["Jibun_Addr1"].ToString();
            addr2_jibun = userinfo.Rows[0]["Jibun_Addr2"].ToString();

            addr1_overseas = userinfo.Rows[0]["Address1"].ToString();
            addr2_overseas = userinfo.Rows[0]["Address2"].ToString();


            txtUserName.Text = userinfo.Rows[0]["SponsorName"].ToString();
            txtUserNameEng.Text = userinfo.Rows[0]["EngName"].ToString();
            txtTel.Text = userinfo.Rows[0]["Phone"].ToString();
            txtEmail.Text = userinfo.Rows[0]["Email"].ToString();
            hdnSponsorID.Value = userinfo.Rows[0]["SponsorID"].ToString();

            dspAddrJibun.Value = addr1_jibun;
            dspAddrDoro.Value = addr1_doro;

            locationType.Value = userinfo.Rows[0]["LocationType"].ToString();

            if (userinfo.Rows[0]["LocationType"].ToString() == "국내")
            {
                addr_domestic.Checked = true;
                pn_addr_domestic.Style["display"] = "block";

                addr_domestic_zipcode.Value = addr_zipcode;
                addr_domestic_addr1.Value = addr1_doro + "//" + addr1_jibun;
                addr_domestic_addr2.Value = addr2_doro;

                addr_road.InnerText = "[도로명] (" + addr_zipcode + ") " + addr1_doro + " " + addr2_doro; addr_road.InnerText = "[도로명주소] (" + addr_zipcode + ") " + addr1_doro + " " + addr2_doro;
                addr_jibun.InnerText = "[지번] (" + addr_zipcode + ") " + addr1_jibun + " " + addr2_jibun;

                addr1.Value = addr1_doro + "//" + addr1_jibun;
                addr2.Value = addr2_doro;
                zipcode.Value = addr_zipcode;
            }
            else
            {
                addr_oversea.Checked = true;
                pn_addr_overseas.Style["display"] = "block";

                addr_overseas_zipcode.Value = addr_zipcode;
                addr_overseas_addr1.Value = addr1_overseas;
                addr_overseas_addr2.Value = addr2_overseas;


                addr1.Value = addr1_overseas;
                addr2.Value = addr2_overseas;
                zipcode.Value = addr_zipcode;

                ddlHouseCountry.SelectedValue = userinfo.Rows[0]["CountryCode"].ToString();
            }

            //후원어린이 정보
            getSponChildList(hdnSponsorID.Value, 0);
        }
        else
        {
            hdnSponsorID.Value = "";

            message.Value = "사용자 정보가 조회되지 않습니다.";
        }
        #endregion

    }
    protected int getCommitmentCount(string sponserid)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        int iCommitmentCount = 0;

        var objSql = new object[1] { "  SELECT COUNT(*) AS CommitmentCount " +
         "  FROM tCommitmentMaster ComM WITH (NOLOCK) " +
         "  LEFT OUTER JOIN tSponsorMaster SM WITH (NOLOCK) ON ComM.SponsorID = SM.SponsorID " +
         "  LEFT OUTER JOIN tChildMaster CM WITH (NOLOCK) ON ComM.ChildMasterID = CM.ChildMasterID " +
         "  WHERE ComM.SponsorID = '" + sponserid + "' " +
         "  AND ComM.SponsorItemEng IN ('DS', 'LS') " +
         "  AND (ComM.StopDate IS NULL OR ComM.StopDate > GETDATE()) " };

        DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

        if (ds2 == null || ds2.Tables.Count == 0 || ds2.Tables[0].Rows.Count == 0)
            iCommitmentCount = 0;
        else
            iCommitmentCount = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());

        return iCommitmentCount;
    }

    protected string getGroupType(string sponserid)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        string grouptype = "";

        //using (FrontDataContext dao = new FrontDataContext())
        //{
        var userInfo = new UserInfo();

        #region Compass - tSponsorGroup : GroupType 가져오기 
        Object[] objSql = new object[1] { "VT_WebInfoInquiry" };
        Object[] objParam = new object[] { "DIVIS", "ConID" };
        Object[] objValue = new object[] { "GroupInfo", sponserid };

        var list = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
        if (list.Rows.Count > 0)
        {
            grouptype = list.Rows[0]["GroupType"].ToString();
        }
        #endregion
        //}

        return grouptype;
    }

    protected void getSponChildList(string sponserid, int applyid)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        chkChildList.Items.Clear();
        //using (AdminDataContext dao = new AdminDataContext())
        //{
        //후원어린이 정보
        Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId", "childKey", "orderby", "checkLetter" };
        Object[] objValue = new object[] { 1, 10000, sponserid, "", "", 0 };
        Object[] objSql = new object[] { "sp_web_mychild_list_f" };
        DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

        foreach (DataRow dr in dt.Rows)
        {
            chkChildList.Items.Add(new ListItem(dr["ChildKey"].ToString() + "" + dr["Name"].ToString(), dr["ChildKey"].ToString()));
        }
        //}
    }

    protected void btn_save_ServerClick(object sender, EventArgs e)
    {
        var apply_arg = new tVisionTripApply()
        {
            ApplyType = "Individual",
            SponsorID = hdnSponsorID.Value,
            SponsorName = txtUserName.Text,
            SponsorNameEng = txtUserNameEng.Text,
            Tel = txtTel.Text,
            Email = txtEmail.Text,
            Address1 = (addr1.Value + "$" + ddlHouseCountry.SelectedValue),
            Address2 = addr2.Value,
            ZipCode = zipcode.Value,
            LocationType = addr_domestic.Checked ? "국내" : "해외",

            ReligionType = ddlReligion.SelectedValue,
            ChurchName = txtChurch.Text,
            EmergencyContactName = txtEmergencyContactName.Text,
            EmergencyContactTel = txtEmergencyContactTel.Text,
            EmergencyContactRelation = txtEmergencyContactRelation.Text,
            ApplyDate = DateTime.Now,
            GroupType = getGroupType(hdnSponsorID.Value),
            CommitmentCount = getCommitmentCount(hdnSponsorID.Value),

            RegisterDate = DateTime.Now,
            RegisterID = AdminLoginSession.GetCookie(Context).identifier.ToString(),
            RegisterName = AdminLoginSession.GetCookie(Context).name
        };

        var detail_arg = new tVisionTripIndividualDetail()
        {
            VisitType = ddlVisitType.SelectedValue == "V" ? 'V' : 'M',
            VisitCountry = ddlVisitCountry.SelectedValue,
            VisitCountryEng = txtVisitCountryEng.Text,
            VisitDate1 = txtVisitDate1.Value,
            VisitDate2 = txtVisitDate2.Value,
            LocalAccommodation = txtLocalAccommodation.Value,
            LocalTel = txtLocalTel.Value,
            LocalAddress = txtLocalAddress.Value,

            DepartureDate = txtDepartureDate.Value,
            ReturnDate = txtReturnDate.Value
        };

        //using (FrontDataContext dao = new FrontDataContext())
        //{  
            var vtApplyList = new List<tVisionTripApply>();
            vtApplyList.Add(new tVisionTripApply()
            {
                ApplyType = apply_arg.ApplyType, 
                SponsorID = apply_arg.SponsorID,
                SponsorName = apply_arg.SponsorName,
                SponsorNameEng = apply_arg.SponsorNameEng,
                Tel = apply_arg.Tel.Encrypt(),
                Email = apply_arg.Email.Encrypt(),
                LocationType = apply_arg.LocationType,
                Address1 = apply_arg.Address1.Encrypt(),
                Address2 = apply_arg.Address2.Encrypt(),
                ZipCode = apply_arg.ZipCode.Encrypt(),
                ChangeYN = 'N',
                ReligionType = apply_arg.ReligionType,
                ChurchName = apply_arg.ChurchName,
                EmergencyContactName = apply_arg.EmergencyContactName,
                EmergencyContactTel = apply_arg.EmergencyContactTel,
                EmergencyContactRelation = apply_arg.EmergencyContactRelation, 
                ApplyDate = apply_arg.ApplyDate,
                CompassYN = 'N',
                GroupType = apply_arg.GroupType,
                CommitmentCount = apply_arg.CommitmentCount,
                CurrentUse = 'Y',
                RegisterID = apply_arg.RegisterID,
                RegisterName = apply_arg.RegisterName,
                RegisterDate = apply_arg.RegisterDate
            });

        //    dao.tVisionTripApply.InsertAllOnSubmit(vtApplyList);
        www6.insert(vtApplyList[0]);
        //    dao.SubmitChanges();
        
        int iApplyID = 0;
        iApplyID = vtApplyList[0].ApplyID;

        try
        {
            if (iApplyID > 0)
            {
                var vtIndividialDetail = new tVisionTripIndividualDetail();
                vtIndividialDetail.ApplyID = iApplyID;
                vtIndividialDetail.VisitType = detail_arg.VisitType;
                vtIndividialDetail.VisitCountry = detail_arg.VisitCountry;
                vtIndividialDetail.VisitCountryEng = detail_arg.VisitCountryEng;
                vtIndividialDetail.VisitDate1 = detail_arg.VisitDate1;
                vtIndividialDetail.VisitDate2 = detail_arg.VisitDate2;
                vtIndividialDetail.LocalAccommodation = detail_arg.LocalAccommodation;
                vtIndividialDetail.LocalTel = detail_arg.LocalTel;
                vtIndividialDetail.LocalAddress = detail_arg.LocalAddress;
                vtIndividialDetail.DepartureDate = detail_arg.DepartureDate;
                vtIndividialDetail.ReturnDate = detail_arg.ReturnDate;

                //dao.tVisionTripIndividualDetail.InsertOnSubmit(vtIndividialDetail);
                www6.insert(vtIndividialDetail);

                #region 후원어린이 선택
                var cmList = new List<tVisionTripChildMeet>();

                foreach (ListItem item in chkChildList.Items)
                {
                    var ChildKey = item.Value;
                    if (item.Selected)
                    {
                        var ChildName = item.Text.Replace(item.Value, "");
                        cmList.Add(new tVisionTripChildMeet()
                        {
                            ApplyID = iApplyID,
                            ChildKey = item.Value,
                            ChildMeetType = 'S',
                            ChildName = ChildName,
                            CurrentUse = 'Y',
                            RegisterDate = DateTime.Now,
                            RegisterID = AdminLoginSession.GetCookie(Context).identifier.ToString(),
                            RegisterName = AdminLoginSession.GetCookie(Context).name
                        });
                    }
                }

                string addChildText = hdnAddText.Value;
                foreach (string chkey in addChildText.Split('^'))
                {
                    if (chkey.Trim() != "")
                    {
                        cmList.Add(new tVisionTripChildMeet()
                        {
                            ApplyID = iApplyID,
                            ChildKey = chkey,
                            ChildMeetType = 'I',
                            ChildName = "",
                            CurrentUse = 'Y',
                            RegisterDate = DateTime.Now,
                            RegisterID = AdminLoginSession.GetCookie(Context).identifier.ToString(),
                            RegisterName = AdminLoginSession.GetCookie(Context).name
                        });
                    }
                }
                #endregion

                //dao.tVisionTripChildMeet.InsertAllOnSubmit(cmList);
                foreach (var item in cmList)
                {
                    www6.insert(item);
                }
                //dao.SubmitChanges();
                

                action.Value = "close";
                message.Value = "등록되었습니다.";
                //Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.opener.location.reload();self.close();");
            }
        }
        catch (Exception ex)
        {
            //using (FrontDataContext daoDel = new FrontDataContext())
            //{ 
            //    var entity = daoDel.tVisionTripApply.First(p => p.tVisionTripApply == Convert.ToInt32(iApplyID));
            //    daoDel.tVisionTripApply.DeleteOnSubmit(entity);
            //    daoDel.SubmitChanges();
            //}

            //www6.cud(MakeSQL.delQ(0, "tVisionTripApply", "tVisionTripApply", Convert.ToInt32(iApplyID)));
            var entity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(iApplyID));
            www6.delete(entity);

            throw new Exception(ex.Message);

            //ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            //message.Value = "예외오류가 발생했습니다.asdfasd 담당자에게 문의 바랍니다." + ex.Message);
        }
    }
    //}
}
//int txtNumber2 = 0;
//protected void RepeaterDetailsRow_ItemCommand(object source, RepeaterCommandEventArgs e)
//{
//    if (e.CommandName == "Add")
//    {
//        // declare a textbox
//        //TextBox t = new TextBox();
//        //// set the textbox's id to whatever you want
//        //t.ID = e.Item.ItemIndex.ToString();
//        //// add the textbox to the repeater
//        //e.Item.Controls.Add(t);

//        foreach (RepeaterItem ri in RepeaterDetailsRow.Items)
//        {

//            //Label lbl = ((Label)ri.FindControl("lblName"));

//            string txtBxText = ((TextBox)ri.ItemIndex.FindControl("txtMyTextBox" + txtNumber2.ToString())).Text;
//            ;//the problem is here, it read just first textbox

//            txtNumber2++;

//        }

//    }
//}
