﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_participation_visiontrip_schedule_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false" %>

<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
    <script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
    <script type="text/javascript">

        $(function () {

            // 에디터
            image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
            initEditor(oEditors, "txtTripNotice");


            $("#ddlTripType").change(function () {
                if (($(this).val() == "Request") && "<%:base.Action%>" == "update") {
                    $(".request_trip_form").show();
                } else {
                    $(".request_trip_form").hide();
                    //$(".request_trip_form").find("input").val("");
                }
            })

            if ($("#ddlTripType").val() == "Request") {
                $(".request_trip_form").show();
            } else {
                $(".request_trip_form").hide();
                //$(".request_trip_form").find("input").val("");
            }
            if ("<%:base.Action%>" == "update")
	            $(".update_form").show();

        });

        var onSubmit = function () {
            oEditors.getById["txtTripNotice"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

            if (!validateForm([
				{ id: "#ddlTripType", msg: "타입을 선택하세요" },
				{ id: "#txtVisitCountry", msg: "방문국가를 입력하세요" },
				{ id: "#txtStartDate", msg: "일정 시작일을 입력하세요" },
				{ id: "#txtEndDate", msg: "일정 종료일을 입력하세요" },
            ])) {
                return false;
            }
            return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
        }


        var onRemove = function () {
            return confirm("삭제하시겠습니까?");
        }

        //var initEditor = function (ref, holder) {
        //    nhn.husky.EZCreator.createInIFrame({
        //        oAppRef: ref,
        //        elPlaceHolder: holder,
        //        sSkinURI: "/common/smartEditor/SmartEditor2Skin.html",
        //        htParams: {
        //            bUseToolbar: true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
        //            bUseVerticalResizer: true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
        //            bUseModeChanger: true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
        //            //aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
        //            fOnBeforeUnload: function () {
        //                //alert("완료!");
        //            }
        //        }, //boolean
        //        fOnAppLoad: function () {
        //            //oEditors.getById["content"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
        //        },
        //        fCreator: "createSEditor2"
        //    });
        //}

    </script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">

            <li runat="server" id="tabm1" class="active">
                <asp:LinkButton runat="server" ID="LinkButton1" CommandArgument="1">기본정보</asp:LinkButton></li>

        </ul>
        <div class="tab-content">

            <div class="active tab-pane" id="tab1" runat="server">
                <div class="form-horizontal">

                    <style type="text/css">
                        #b_display label {
                            margin-left: 5px;
                            width: 50px;
                        }
                    </style>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">노출여부</label>
                        <div class="col-sm-10" style="margin-top: 5px;">
                            <asp:CheckBox runat="server" ID="trip_view" Checked="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">타입</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlTripType" CssClass="form-control" Style="width: 200px; display:inline-block;" AutoPostBack="true" OnSelectedIndexChanged="ddlTripType_SelectedIndexChanged">
                                <asp:ListItem Text="선택하세요" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList runat="server" ID="ddlRequestSubType" CssClass="form-control" Style="width: 200px; display:inline-block;" Visible="false">
                                <asp:ListItem Text="선택하세요" Value=""></asp:ListItem>
                                <asp:ListItem Text="CR" Value="CR"></asp:ListItem>
                                <asp:ListItem Text="MKT" Value="MKT"></asp:ListItem>
                                <asp:ListItem Text="ADV" Value="ADV"></asp:ListItem>
                                <asp:ListItem Text="PR" Value="PR"></asp:ListItem>
                                <asp:ListItem Text="NK" Value="NK"></asp:ListItem>
                                <asp:ListItem Text="기타" Value="기타"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">방문국가</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtVisitCountry" CssClass="form-control" Width="400" MaxLength="30"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">방문국가 영문명</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtVisitCountryEng" CssClass="form-control" Width="400" MaxLength="30"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">트립일정</label>
                        <div class="col-sm-10">

                            <button class="btn btn-primary btn-sm dayrange" data-from="txtStartDate" data-end="txtEndDate"><i class="fa fa-calendar"></i></button>
                            <asp:TextBox runat="server" ID="txtStartDate" Width="100px" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                            ~
							<asp:TextBox runat="server" ID="txtEndDate" Width="100px" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group request_trip_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">트립명(요청트립)</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtTripName" CssClass="form-control" Width="200" MaxLength="30"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">인원(최대인원)</label>
                        <div class="col-sm-10">
                            <input type="number" runat="server" id="txtMaxCount" cssclass="form-control" style="padding: 7px 12px; border: 1px solid #ccc; color: #555; width: 200px;">
                        </div>
                    </div>

                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">신청비</label>
                        <div class="col-sm-10">
                            <input type="number" runat="server" id="txtRequestCost" cssclass="form-control" style="padding: 7px 12px; border: 1px solid #ccc; color: #555; width: 200px;">
                        </div>
                    </div>

                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">
                            예상 트립비용<br />
                            (신청비 20만원 포함)</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtTripCost" CssClass="form-control" Width="400" MaxLength="30"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">신청일</label>
                        <div class="col-sm-10">
                            <button class="btn btn-primary btn-sm dayrange" data-from="txtOpenDate" data-end="txtCloseDate"><i class="fa fa-calendar"></i></button>
                            <asp:TextBox runat="server" ID="txtOpenDate" Width="100px" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                            ~
							<asp:TextBox runat="server" ID="txtCloseDate" Width="100px" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">신청현황</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlTripState" CssClass="form-control" Style="width: 200px;">
                                <asp:ListItem Text="선택하세요" Value=" "></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">코스트센터</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtCostCenter" CssClass="form-control" Width="200" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">트립리더</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtTripLeader" CssClass="form-control" Width="200" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">행정담당자</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtAdministrations" CssClass="form-control" Width="200" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>



                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">후기 보러가기 링크</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtTripReview" CssClass="form-control" Width="400" MaxLength="150" placeholder="http://" value="http://"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group request_trip_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">요청트립 신청 URL</label>
                        <div class="col-sm-10" style="margin-top: 7px;">
                            <asp:Label runat="server" ID="lbRequestTripURL"></asp:Label>
                        </div>
                    </div>

                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">안내보기</label>
                        <div class="col-sm-10">
                            <textarea name="txtTripNotice" id="txtTripNotice" runat="server" style="width: 600px; height: 212px; display: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">안내보기 URL</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtTripNoticeDetail" MaxLength="2000" CssClass="form-control" Width="400" placeholder="http://" value="http://"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group update_form" style="display: none;">
                        <label class="col-sm-2 control-label control-label">통계</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlStatisticsYN" CssClass="form-control" Style="width: 150px;">
                                <asp:ListItem Text="선택하세요" Value=" "></asp:ListItem>
                                <asp:ListItem Text="예" Value="Y"></asp:ListItem>
                                <asp:ListItem Text="아니오" Value="N"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>


                    <asp:PlaceHolder runat="server" ID="ph_regdate" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
                            <div class="col-sm-10" style="margin-top: 7px;">
                                <asp:Label runat="server" ID="reg_date"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>


            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->


    <div class="box-footer clearfix text-center">

        <asp:LinkButton runat="server" ID="btn_remove" OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
        <asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" Style="margin-right: 5px">등록</asp:LinkButton>
        <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
    </div>
</asp:Content>
