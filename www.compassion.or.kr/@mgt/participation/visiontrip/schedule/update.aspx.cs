﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
using System.Data;

public partial class mgt_participation_visiontrip_schedule_update : AdminBoardPage
{

    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();
        v_admin_auth auth = base.GetPageAuth();
        base.BoardType = "notice";
        base.FileGroup = Uploader.FileGroup.file_board;
        base.FileRoot = Uploader.GetRoot(base.FileGroup);
        base.PrimaryKey = Request["c"];
        base.Action = Request["t"];

        btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

        // querystring 유효성 검사 후 문제시 리다이렉트
        var isValid = new RequestValidator()
            .Add("c", RequestValidator.Type.Numeric)
            .Add("t", RequestValidator.Type.Alphabet)
            .Add("s_main", RequestValidator.Type.AlphaNumeric)
            .Validate(this.Context, "default.aspx");


        foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "trip_type").OrderBy(p => p.cd_order))
        {
            ddlTripType.Items.Add(new ListItem(a.cd_value, a.cd_key));
        }

        // 2017.06.07 김태형 : 상태(Y:신청중, S:예정, N:마감, E:종료)
        ddlTripState.Items.Add(new ListItem("신청중", "Apply"));
        ddlTripState.Items.Add(new ListItem("예정", "Expect"));
        ddlTripState.Items.Add(new ListItem("조기마감", "EarlyDeadline"));
        ddlTripState.Items.Add(new ListItem("마감", "Deadline"));
        ddlTripState.Items.Add(new ListItem("종료", "Close"));

        var domain = ConfigurationManager.AppSettings["domain"];

        if (base.Action == "update")
        {

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.tVisionTripSchedule.First(p => p.ScheduleID == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<tVisionTripSchedule>("ScheduleID", Convert.ToInt32(PrimaryKey));

                trip_view.Checked = entity.TripViewYN == 'Y';
                ddlTripType.SelectedValue = entity.VisionTripType;

                ddlRequestSubType.SelectedValue = entity.RequestSubType.EmptyIfNull();
                ddlRequestSubType.Visible = ddlTripType.SelectedValue.Equals("Request");

                txtVisitCountry.Text = entity.VisitCountry;
                txtVisitCountryEng.Text = entity.VisitCountryEng;
                txtStartDate.Text = entity.StartDate;
                txtEndDate.Text = entity.EndDate;
                txtTripName.Text = entity.TripName;
                txtMaxCount.Value = entity.MaxCount.ToString();
                txtRequestCost.Value = entity.RequestCost;
                txtTripCost.Text = entity.TripCost;
                txtOpenDate.Text = entity.OpenDate.ToString() != "" ? entity.OpenDate : "";
                txtCloseDate.Text = entity.CloseDate.ToString() != "" ? entity.CloseDate : "";
                txtTripReview.Text = entity.TripReview;
                txtTripNotice.InnerText = entity.TripNotice;
                txtTripNoticeDetail.Text = entity.TripNoticeDetail;
                ddlStatisticsYN.SelectedValue = entity.StatisticsYN != null ? entity.StatisticsYN.ToString() : "";
                txtCostCenter.Text = entity.CostCenter;
                txtTripLeader.Text = entity.TripLeader;
                txtAdministrations.Text = entity.Administrations;

                reg_date.Text = entity.RegisterDate.ToString();
                ddlTripState.SelectedValue = entity.TripState.ToString();
                lbRequestTripURL.Text = domain + entity.RequestURL; 
            }
            //통계처리 완료되었으면 수정 못함.
            if (ddlStatisticsYN.SelectedValue == "Y")
            {
                ddlStatisticsYN.Enabled = false;
            }
            else
            {
                btn_update.Visible = auth.aa_auth_update;
                btn_remove.Visible = auth.aa_auth_delete;
            }
            ph_regdate.Visible = true;
            base.FileLoad();
        }
        else
        {

            btn_update.Visible = auth.aa_auth_create;
            btn_remove.Visible = false;
        }

    }

    protected override void OnAfterPostBack()
    {
        base.OnAfterPostBack();
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
    }

    protected void btn_update_click(object sender, EventArgs e)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        UserInfo sess = new UserInfo();

        string strRequestTripKey = DateTime.Now.ToString("yyyyMMddhhmmssfff");

        var arg = new tVisionTripSchedule()
        {
            TripViewYN = trip_view.Checked ? 'Y' : 'N',
            VisionTripType = ddlTripType.SelectedValue,
            VisitCountry = txtVisitCountry.Text,
            VisitCountryEng = txtVisitCountryEng.Text,
            StartDate = txtStartDate.Text,
            EndDate = txtEndDate.Text,
            RequestCost = txtRequestCost.Value,
            TripCost = txtTripCost.Text,
            TripState = ddlTripState.SelectedValue,
            OpenDate = txtOpenDate.Text,
            CloseDate = txtCloseDate.Text,
            MaxCount = txtMaxCount.Value != "" ? Convert.ToInt32(txtMaxCount.Value) : 0,
            TripNotice = txtTripNotice.InnerHtml.ToHtml(false),
            TripNoticeDetail = txtTripNoticeDetail.Text,
            TripReview = txtTripReview.Text,
            TripName = txtTripName.Text,
            RequestURL = "participation/visiontrip/schedule/" + strRequestTripKey + "/Request",
            RequestTripKey = strRequestTripKey,
            StatisticsYN = Convert.ToChar(ddlStatisticsYN.SelectedValue),

            CostCenter = txtCostCenter.Text,
            TripLeader = txtTripLeader.Text,
            Administrations = txtAdministrations.Text,

            CurrentUse = 'Y',
            RegisterID = AdminLoginSession.GetCookie(this.Context).email.ToString(),
            RegisterName = AdminLoginSession.GetCookie(this.Context).name,
            RegisterDate = DateTime.Now,
            RequestSubType = ddlRequestSubType.SelectedValue
        };


        using (AdminDataContext dao = new AdminDataContext())
        {

            if (base.Action == "update")
            {
                //var entity = dao.tVisionTripSchedule.First(p => p.ScheduleID == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<tVisionTripSchedule>("ScheduleID", Convert.ToInt32(PrimaryKey));

                entity.TripViewYN = arg.TripViewYN;
                entity.VisionTripType = arg.VisionTripType;
                entity.VisitCountry = arg.VisitCountry;
                entity.VisitCountryEng = arg.VisitCountryEng;
                entity.StartDate = arg.StartDate;
                entity.EndDate = arg.EndDate;
                entity.RequestCost = arg.RequestCost;
                entity.TripCost = arg.TripCost;
                entity.TripState = arg.TripState;
                entity.OpenDate = arg.OpenDate;
                entity.CloseDate = arg.CloseDate;
                entity.MaxCount = arg.MaxCount;
                entity.TripNotice = arg.TripNotice;
                entity.TripNoticeDetail = arg.TripNoticeDetail;
                entity.TripReview = arg.TripReview;
                entity.TripName = arg.TripName;
                entity.StatisticsYN = arg.StatisticsYN;
                entity.CostCenter = arg.CostCenter;
                entity.TripLeader = arg.TripLeader;
                entity.Administrations = arg.Administrations;

                if ((string.IsNullOrEmpty(entity.RequestTripKey) || string.IsNullOrEmpty(entity.RequestURL)) && arg.VisionTripType == "Request")
                {
                    entity.RequestTripKey = arg.RequestTripKey;
                    entity.RequestURL = arg.RequestURL;
                }

                entity.ModifyDate = DateTime.Now;
                entity.ModifyID = AdminLoginSession.GetCookie(this.Context).email.ToString();
                entity.ModifyName = AdminLoginSession.GetCookie(this.Context).name;

                entity.RequestSubType = arg.RequestSubType;

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";

                //dao.SubmitChanges();
                //string wClause = string.Format("ScheduleID = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);

                #region Compass - VT_Master 데이터 등록 : VisionTrip 일정 정보 등록
                string sVisitTripType = arg.VisionTripType == "Request" ? "요청" : "기획";
                string sYear = string.IsNullOrEmpty(arg.StartDate) ? "" : arg.StartDate.Substring(0, 4);
                string sMonth = string.IsNullOrEmpty(arg.StartDate) ? "" : arg.StartDate.Substring(5, 2);

                Object[] objSql = new object[1] { "VT_ScheduleInfoSave" };
                Object[] objParam = new object[] { "DIVIS", "Year", "Month", "StartDate", "EndDate", "VisitCountry", "VisitCountryEng", "VisionTripType", "TripName", "UserID", "UserName", "ScheduleID", "TripLeader", "Administrations" };
                Object[] objValue = new object[] { "Schedule_Reg", sYear, sMonth, arg.StartDate, arg.EndDate, arg.VisitCountry, arg.VisitCountryEng, sVisitTripType, arg.CostCenter, AdminLoginSession.GetCookie(this.Context).email.ToString(), AdminLoginSession.GetCookie(this.Context).name.ToString(), entity.ScheduleID, arg.TripLeader, arg.Administrations };

                var scheduleInsert = _www6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue); 
                //if(scheduleInsert < 0) 
                #endregion

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
            }
            else
            {
                //dao.tVisionTripSchedule.InsertOnSubmit(arg);
                www6.insert(arg);

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";
                //dao.SubmitChanges();

                #region Compass - VT_Master 데이터 등록 : VisionTrip 일정 정보 등록
                string sVisitTripType = arg.VisionTripType == "Request" ? "요청" : "기획";
                string sYear = string.IsNullOrEmpty(arg.StartDate) ? "" : arg.StartDate.Substring(0, 4);
                string sMonth = string.IsNullOrEmpty(arg.StartDate) ? "" : arg.StartDate.Substring(5, 2);
                Object[] objSql = new object[1] { "VT_ScheduleInfoSave" };
                Object[] objParam = new object[] { "DIVIS", "Year", "Month", "StartDate", "EndDate", "VisitCountry", "VisitCountryEng", "VisionTripType", "TripName", "UserID", "UserName", "ScheduleID", "TripLeader", "Administrations" };
                Object[] objValue = new object[] { "Schedule_Reg", sYear, sMonth, arg.StartDate, arg.EndDate, arg.VisitCountry, arg.VisitCountryEng, sVisitTripType, arg.CostCenter, AdminLoginSession.GetCookie(this.Context).email.ToString(), AdminLoginSession.GetCookie(this.Context).name.ToString(), arg.ScheduleID, arg.TripLeader, arg.Administrations };
                var scheduleInsert = _www6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);
                //if(scheduleInsert < 0) 
                #endregion

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));
            }
        }

    }

    protected void btn_remove_click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tVisionTripSchedule.First(p => p.ScheduleID == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<tVisionTripSchedule>("ScheduleID", Convert.ToInt32(PrimaryKey));

            entity.CurrentUse = 'N';
            //dao.SubmitChanges();
            //string wClause = string.Format("ScheduleID = {0}", Convert.ToInt32(PrimaryKey));
            www6.update(entity);


            #region Compass - VT_Master 데이터 등록 : VisionTrip 일정 정보 등록
            CommonLib.WWW6Service.SoaHelperSoap _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

            string sVisitTripType = ddlTripType.SelectedValue == "Request" ? "요청" : "기획";
            string sYear = string.IsNullOrEmpty(txtStartDate.Text) ? "" : txtStartDate.Text.Substring(0, 4);
            string sMonth = string.IsNullOrEmpty(txtEndDate.Text) ? "" : txtEndDate.Text.Substring(5, 2);
            Object[] objSql = new object[1] { "VT_ScheduleInfoSave" };
            Object[] objParam = new object[] { "DIVIS", "UserID", "UserName", "ScheduleID" };
            Object[] objValue = new object[] { "Schedule_Del", AdminLoginSession.GetCookie(this.Context).email.ToString(), AdminLoginSession.GetCookie(this.Context).name.ToString(), PrimaryKey };
            var scheduleInsert = _www6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);
            
            //if(scheduleInsert < 0) 
            #endregion


            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", entity.ToJson()));
        }

        Master.ValueAction.Value = "list";
        Master.ValueMessage.Value = "삭제되었습니다.";
    }

    /// <summary>
    /// 통계 여부 Y 이면 결연 어린이 수 가져오기
    /// </summary>
    /// <param name="sponserid"></param>
    /// <returns></returns>
    protected int getCommitmentCount(string sponserid)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        int iCommitmentCount = 0;
        
        var objSql = new object[1] { "  SELECT COUNT(*) AS CommitmentCount " +
         "  FROM tCommitmentMaster ComM WITH (NOLOCK) " +
         "  LEFT OUTER JOIN tSponsorMaster SM WITH (NOLOCK) ON ComM.SponsorID = SM.SponsorID " +
         "  LEFT OUTER JOIN tChildMaster CM WITH (NOLOCK) ON ComM.ChildMasterID = CM.ChildMasterID " +
         "  WHERE ComM.SponsorID = '" + sponserid + "' " +
         "  AND ComM.SponsorItemEng IN ('DS', 'LS') " +
         "  AND (ComM.StopDate IS NULL OR ComM.StopDate > GETDATE()) " };
         
        DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

        if (ds2 == null || ds2.Tables.Count == 0 || ds2.Tables[0].Rows.Count == 0)
            iCommitmentCount = 0;
        else
            iCommitmentCount = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());

        return iCommitmentCount;
    }


    protected void ddlTripType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlRequestSubType.Visible = ddlTripType.SelectedValue.Equals("Request");
    }
}