﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_participation_visiontrip_schedule_default : AdminBoardPage {


	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;
		btn_add.Attributes.Add("href", "update.aspx");

        ViewState["authRead"] = auth.aa_auth_read;
        ViewState["authUpdate"] = auth.aa_auth_update;

        var selectedYear = StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "trip" && p.cd_key == "year").OrderBy(p => p.cd_order).ToList();
        string year = string.Empty;
        if (selectedYear != null && selectedYear.Count > 0)
            year = selectedYear[0].cd_value.ToString();
        else
            year = DateTime.Now.Year.ToString();

        for (int i = 2005; i <= DateTime.Now.AddYears(1).Year; i++)
        {
            a_type.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        a_type.SelectedValue = year;

        for (int i = 2005; i <= DateTime.Now.AddYears(1).Year; i++)
        {
            s_b_year.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        a_type.SelectedValue = DateTime.Now.Year.ToString();

        //일정페이지 초기 날짜 선택 값 조회바인딩
        s_b_year.SelectedValue = year; 
    }

	protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();

		btn_add.Attributes.Add("href", "update.aspx");
	}

	protected override void GetList(int page) {

		Master.IsSearch = false;
		if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
			Master.IsSearch = true;
		}


        string bDate = string.Empty;
        string eDate = string.Empty;

        if (s_dateType.SelectedItem.Value == "year") {
            bDate = s_b_year.SelectedValue + "-01-01";
            eDate = s_b_year.SelectedValue + "-12-31";
        }
        else {
            bDate = s_b_date.Text;
            eDate = s_e_date.Text;
        }

        // #13115 : 관리자로그인아이디가 vision1~vision6인 경우, 요청트립에서 설정한 하위분류에 해당하는 리스트만 조회
        string requestSubType = "";
        string adminLoginID = AdminLoginSession.GetCookie(this.Context).email;
        if (adminLoginID.Equals("vision1") || adminLoginID.Equals("vision2") || adminLoginID.Equals("vision3") || adminLoginID.Equals("vision4") || adminLoginID.Equals("vision5") || adminLoginID.Equals("vision6"))
        {
            switch (adminLoginID)
            {
                case "vision1": requestSubType = "CR"; break;
                case "vision2": requestSubType = "MKT"; break;
                case "vision3": requestSubType = "ADV"; break;
                case "vision4": requestSubType = "PR"; break;
                case "vision5": requestSubType = "NK"; break;
                case "vision6": requestSubType = "기타"; break;
                default: requestSubType = ""; break;
            }
        }

        using (AdminDataContext dao = new AdminDataContext()) {

            //var list = dao.sp_visiontrip_list(page, paging.RowsPerPage, "vision,request" , "", s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            //var list = dao.sp_tVisionTripSchedule_list(page, paging.RowsPerPage, s_tripType.SelectedValue, s_keyword.Text, bDate, eDate).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "keyword", "startdate", "enddate", "requestSubType" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_tripType.SelectedValue, s_keyword.Text, bDate, eDate, requestSubType };
            var list = www6.selectSP("sp_tVisionTripSchedule_list", op1, op2).DataTableToList<sp_tVisionTripSchedule_listResult>().ToList();

            var total = 0;

			if (list.Count > 0)
				total = list[0].total.Value;

			lbTotal.Text = total.ToString();

			paging.CurrentPage = page;
			paging.Calculate(total);
			repeater.DataSource = list;
			repeater.DataBind();

		}
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
                sp_tVisionTripSchedule_listResult entity = e.Item.DataItem as sp_tVisionTripSchedule_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
                //노출여부
				((Literal)e.Item.FindControl("ltTripViewYN")).Text = entity.TripViewYN == 'Y' ? "O" : "X";
                //트립안내 등록여부
                ((Literal)e.Item.FindControl("ltTripNotice")).Text = !string.IsNullOrEmpty(entity.TripNotice) ? "O" : "X";
                 
                // 신청일
                string date = ""; 
				if(entity.OpenDate.EmptyIfNull() != "") {
					date = entity.OpenDate.ToString().Substring(0, 10);
				}
				if (entity.CloseDate.EmptyIfNull() != "") {
					date += " ~ " + entity.CloseDate.ToString().Substring(0, 10);
				}
                ((Literal)e.Item.FindControl("ltDate")).Text = date;
                 
                string status = "-";  
                switch (entity.TripState.ToString())
                {
                    case "Apply": status = "신청중"; break;
                    case "Expect": status = "예정"; break;
                    case "Deadline": status = "마감"; break;
                    case "Close": status = "종료"; break;
                    default: status = "-"; break;
                } 
				((Literal)e.Item.FindControl("ltTripState")).Text = status;
                ((Literal)e.Item.FindControl("ltCostCenter")).Text = entity.CostCenter;
                //통계여부
                //((Literal)e.Item.FindControl("ltStatisticsYN")).Text = entity.StatisticsYN == 'Y' ? "O" : "X"; 
            }
        }

	}

    protected void btnTripYearSet_Click(object sender, EventArgs e)
    {
        using (MainLibDataContext dao = new MainLibDataContext())
        {
            //var s = dao.code.First(p => p.cd_group == "trip" && p.cd_key == "year");
            var entity = www6.selectQF<code>("cd_group", "trip", "cd_key", "year");

            entity.cd_value = a_type.SelectedValue;
            Master.ValueMessage.Value = "저장되었습니다.";
            //dao.SubmitChanges();
            www6.update(entity);
        }
    }
    

}
