﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_participation_visiontrip_default : AdminBoardPage {


	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;
		btn_add.Attributes.Add("href", "update.aspx");

        var selectedYear = StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "trip" && p.cd_key == "year").OrderBy(p => p.cd_order).ToList();
        string year = string.Empty;
        if (selectedYear != null && selectedYear.Count > 0)
            year = selectedYear[0].cd_value.ToString();
        else
            year = DateTime.Now.Year.ToString();

        for (int i = 2005; i <= DateTime.Now.AddYears(1).Year; i++)
        {
            a_type.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        a_type.SelectedValue = year;

        for (int i = 2005; i <= DateTime.Now.AddYears(1).Year; i++)
        {
            s_b_year.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        s_b_year.SelectedValue = DateTime.Now.Year.ToString();

    }

	protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();

		btn_add.Attributes.Add("href", "update.aspx");
	}

	protected override void GetList(int page) {

		Master.IsSearch = false;
		if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
			Master.IsSearch = true;
		}


        string bDate = string.Empty;
        string eDate = string.Empty;

        if (s_dateType.SelectedItem.Value == "year") {
            bDate = s_b_year.SelectedValue + "-01-01";
            eDate = s_b_year.SelectedValue + "-12-31";
        }
        else {
            bDate = s_b_date.Text;
            eDate = s_e_date.Text;
        }

        using (AdminDataContext dao = new AdminDataContext()) {

            //var list = dao.sp_visiontrip_list(page, paging.RowsPerPage, "vision,request" , "", s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            //var list = dao.sp_visiontrip_list(page, paging.RowsPerPage, s_tripType.SelectedValue, "", s_keyword.Text, bDate, eDate).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "trip_view", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_tripType.SelectedValue, "", s_keyword.Text, bDate, eDate };
            var list = www6.selectSP("sp_visiontrip_list", op1, op2).DataTableToList<sp_visiontrip_listResult>().ToList();

            var total = 0;

			if (list.Count > 0)
				total = list[0].total.Value;

			lbTotal.Text = total.ToString();

			paging.CurrentPage = page;
			paging.Calculate(total);
			repeater.DataSource = list;
			repeater.DataBind();

		}
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				sp_visiontrip_listResult entity = e.Item.DataItem as sp_visiontrip_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
				((Literal)e.Item.FindControl("lbDisplay")).Text = entity.trip_view == 'Y' ? "O" : "X";



                //((Literal)e.Item.FindControl("lbTypeName")).Text = entity.v_type_name == 'Y' ? "O" : "X";

                // 신청일
                string date = "";

				if(entity.open_date.EmptyIfNull() != "") {
					date = entity.open_date.ToString().Substring(0, 10);
				}
				if (entity.close_date.EmptyIfNull() != "") {
					date += " ~ " + entity.close_date.ToString().Substring(0, 10);
				}
				((Literal)e.Item.FindControl("lbDate")).Text = date;


				string status = "-";
				if(entity.v_type != "request") {

                    //var open = "";

                    //if (entity.open_date != "") {
                    //	open = Convert.ToDateTime( entity.open_date ).ToString(); ;
                    //} else {
                    //	open = "-";
                    //}


                    //var close = "";

                    //if (entity.close_date != "") {
                    //	close = Convert.ToDateTime( entity.close_date ).ToString(); ;
                    //} else {
                    //	close = "-";
                    //}


                    //if(open != "-" && close != "-") {
                    //                   if (open == "" || close == "")
                    //                       status = string.Format("예정");
                    //	else if (Convert.ToDateTime(open).CompareTo(DateTime.Now) > 0) {
                    //		status = string.Format("예정");
                    //	} else if (Convert.ToDateTime( close).CompareTo(DateTime.Now) < 0) {
                    //		status = "마감";
                    //	} else {
                    //		status = "신청중";
                    //	}
                    //} else {
                    //	status = "-";
                    //}

                    // 2017.06.07 김태형 : 상태(Y:신청중, S:예정, N:마감, E:종료)
                    switch (entity.trip_state.ToString()) {
                        case "Y": status = "신청중"; break;
                        case "S": status = "예정"; break;
                        case "N": status = "마감"; break;
                        case "E": status = "종료"; break;
                        default: status = "-"; break;
                    }
				}
				((Literal)e.Item.FindControl("display_status")).Text = status;
			}
		}

	}

    protected void btnTripYearSet_Click(object sender, EventArgs e)
    {
        using (MainLibDataContext dao = new MainLibDataContext())
        {
            //var s = dao.code.First(p => p.cd_group == "trip" && p.cd_key == "year");
            var entity = www6.selectQF<code>("cd_group", "trip", "cd_key", "year");

            entity.cd_value = a_type.SelectedValue;
            Master.ValueMessage.Value = "저장되었습니다.";
            //dao.SubmitChanges();
            www6.update(entity);
        }
    }
    

}
