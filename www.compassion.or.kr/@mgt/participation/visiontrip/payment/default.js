﻿
(function () {

    //var app = angular.module("defaultApp", []);

    var app = angular.module('cps.page', []);

    app.directive('onFinishRender', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                //if (scope.$first === true) {
                //    window.alert('First thing about to render');
                //}
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit(attr.onFinishRender);
                    }, 1);
                }
            }
        };
    });

    app.controller("defaultCtrl", function ($scope, $http) {
         
    //엑셀다운로드
    $scope.exportData = function () { 
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() +1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = year + "" + month + "" + day + "" + hour + "" + mins;

        var table_div = document.getElementById("tablePayment");
        var table_html = table_div.outerHTML.replace(/ /g, '%20');

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        //setting the file name 
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(table_div.outerHTML);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, '입금목록_' + postfix + '.xls');
            return (sa);
        }
        else {
            var a = document.createElement('a');
            var data_type = 'data:application/vnd.ms-excel;';
            a.href = data_type + ', ' + table_html;
            a.download = '입금목록_' + postfix + '.xls';
            a.click();
        }
        //var a = document.createElement('a'); 
        //    var data_type = 'data:application/vnd.ms-excel';
        //    var table_div = document.getElementById("tablePayment");
        //var table_html = table_div.outerHTML.replace(/ /g, '%20');
        //    a.href = data_type + ', ' +table_html; 
        //        a.download = '입금목록_'+postfix + '.xls'; 
        //a.click();
    }; 

    //검색조건 
    $scope.scheduleID = $("#ddlTripName option:selected").val();
    $scope.paymentType = $("#ddlPaymentType option:selected").val();
    $scope.amount = $("#ddlAmount option:selected").val();
    $scope.depositor = $("#txtDepositor").val();
    $scope.startdate = $("#txtStartDate").val();
    $scope.enddate = $("#txtEndDate").val();

    $("#txtStartDate").change(function () { $scope.search_trip();});

    $scope.search_trip = function () {
        $scope.select_params = {
            scheduleID: $("#ddlTripName option:selected").val(),
            paymentType: $("#ddlPaymentType option:selected").val(),
            amount: $("#ddlAmount option:selected").val(),
            depositor: $("#txtDepositor").val(),
            startdate: $("#txtStartDate").val(),
            enddate: $("#txtEndDate").val(),
            tripYear: $("#txtStartDate").val() != "" ? $("#txtStartDate").val().slice(0, 4) : '',
            tripType: ''
        }; 
        $scope.select_tripname = []; 
        console.log('tripname');
        $http.get("/@mgt/api/visiontrip.ashx?t=tripname", { params: $scope.select_params }).success(function (r) {
            console.log(r);
            if (r.success) {
                var list = r.data; 
                $scope.select_tripname = list; 
            } else {
                alert(r.message);
            }
        });

    };

    $scope.search_amount = function () {
        $scope.select_params = {
            scheduleID: $("#ddlTripName option:selected").val(),
            paymentType: $("#ddlPaymentType option:selected").val(),
            amount: $("#ddlAmount option:selected").val(),
            depositor: $("#txtDepositor").val(),
            startdate: $("#txtStartDate").val(),
            enddate: $("#txtEndDate").val()
        }; 
        $scope.select_amount = [];
        console.log('select_amount');
        $http.get("/@mgt/api/visiontrip.ashx?t=payment_amount", { params: $scope.select_params }).success(function (r) {
            console.log(r);
            if (r.success) {
                var list = r.data;
                $scope.select_amount = list;
            } else {
                alert(r.message);
            }
        });
    };
         
    $scope.search_trip(); $scope.search_amount();
    $("#ddlPaymentType").change(function () { $scope.search_trip(); $scope.search_amount(); });
    $("#ddlTripName").change(function () { $scope.search_amount(); });

    $scope.searchClick = function () {
        $scope.scheduleID = $("#ddlTripName option:selected").val();
        $scope.paymentType = $("#ddlPaymentType option:selected").val();
        $scope.amount = $("#ddlAmount option:selected").val();
        $scope.depositor = $("#txtDepositor").val();
        $scope.startdate = $("#txtStartDate").val();
        $scope.enddate = $("#txtEndDate").val();
        $scope.params = {
            page: $scope.page, 
            scheduleID: $scope.scheduleID,
            paymentType: $scope.paymentType,
            amount: $scope.amount,
            depositor: $scope.depositor,
            startdate: $scope.startdate,
            enddate: $scope.enddate
        };
        $scope.getPaymentList($scope.params);
    } 
    //end - 검색조건 
 
    //입금목록
    $scope.select_applystate = [{ codevalue: 'A', codename: '신청중' }, { codevalue: 'C', codename: '신청완료' }];
    $scope.select_paymentYN = [{ codevalue: 'Y', codename: '납부' }, { codevalue: 'N', codename: '미납' }];
       
    $scope.total = 0;
    $scope.page = 1;
    $scope.rowsPerPage = 10;

    $scope.params = {
        page: $scope.page,
        rowsPerPage: $scope.rowsPerPage,
        scheduleID: $scope.scheduleID,
        paymentType: $scope.paymentType,
        amount: $scope.amount,
        depositor: $scope.depositor,
        startdate: $scope.startdate,
        enddate: $scope.enddate
    };

    $scope.getPaymentList = function (params) {
        $scope.params = $.extend($scope.params, params);
        console.log('paymentlist');
        $http.get("/@mgt/api/visiontrip.ashx?t=paymentlist", { params: $scope.params }).success(function (r) {
            console.log(r);
            if (r.success) { 
                var list = r.data; 
                //if (list.length > 0) {
                //    $.each(list, function () { 
                //    }); 
                //}
                $scope.paymentlist = list;
                $scope.total = r.data.length > 0 ? r.data[0].total : 0;
            } else {
                alert(r.message);
            }
        });

    }  
    $scope.getPaymentList();
    // end - 입금목록
         

});

     


})();
 
 