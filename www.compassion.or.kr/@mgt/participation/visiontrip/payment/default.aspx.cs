﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_participation_visiontrip_payment_default : AdminBoardPage {


	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
         

        txtStartDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("yyyy-MM-dd");

        txtEndDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

        //var selectedYear = StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "trip" && p.cd_key == "individual_year").OrderBy(p => p.cd_order).ToList();
        //string year = string.Empty;
        //if (selectedYear != null && selectedYear.Count > 0)
        //    year = selectedYear[0].cd_value.ToString();
        //else
        //    year = DateTime.Now.Year.ToString();
         
    }

	protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
         
	}  

}
