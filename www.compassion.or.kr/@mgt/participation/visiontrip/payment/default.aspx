﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_participation_visiontrip_payment_default" MasterPageFile="~/@mgt/mgt_afterlogin.master" %>

<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        .float_right {
            float: right;
            margin-right: 10px;
        }
    </style>
    <script type="text/javascript" src="/assets/angular/1.4.8/angular.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0-beta.2/angular-sanitize.js"></script>

    <script type="text/javascript" src="/assets/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="/assets/angular/angular-moment.min.js"></script>

    <script type="text/javascript" src="/@mgt/participation/visiontrip/payment/default.js"></script>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript" src="/@mgt/common/js/paging.js"></script>
    <script type="text/javascript" src="/common/js/site/angular-app.js"></script>
    <script type="text/javascript">
        $(function () {
        });
    </script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
         <iframe id="txtArea1" style="display:none"></iframe>

        <div class="box box-default collapsed-box search_container">
            <div class="box-header with-border section-search">
                <div class="pull-left" style="width: 100%;">
                    <button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;"><i class="fa fa-plus search_toggle"></i></button>
                    <h3 class="box-title" style="padding-top: 5px">검색</h3>
                    <a class="btn btn-bitbucket pull-right float_right" ng-click="exportData()">엑셀불러오기</a>
                </div>

            </div>
            <div class="form-horizontal box-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="s_b_date" class="col-sm-2 control-label">기간 검색(일정)</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtStartDate" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                            ~
						    <asp:TextBox runat="server" ID="txtEndDate" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">납부방법</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlPaymentType" CssClass="form-control" Width="150px">
                                <asp:ListItem Value="" Text="전체"></asp:ListItem>
                                <asp:ListItem Value="실시간 계좌이체" Text="실시간 이체"></asp:ListItem>
                                <asp:ListItem Value="가상계좌" Text="1회성 가상계좌"></asp:ListItem>
                                <asp:ListItem Value="고정가상계좌" Text="고정 가상계좌"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">트립명</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlTripName" CssClass="form-control" Width="150px">
                                <asp:ListItem Text="전체" Value="0"></asp:ListItem>
                                <asp:ListItem ng-repeat="lst in select_tripname" Value="{{lst.scheduleid}}" ng-model="select_tripname">{{lst.visitcountry}}</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">입금액</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlAmount" CssClass="form-control" Width="150px">
                                <asp:ListItem Text="전체" Value="0"></asp:ListItem>
                                <asp:ListItem ng-repeat="lst in select_amount" Value="{{lst.amount}}" ng-model="select_amount">{{lst.amount | number}}원</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">의뢰인/수취인</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="txtDepositor" Width="415" CssClass="form-control inline"></asp:TextBox> 
                        </div>
                    </div>  
                    <div class="box-footer clearfix text-center">
                        <a class="btn btn-primary" style="width: 100px" ng-click="searchClick()">검색 <i class="fa fa-arrow-circle-right"></i></a>
                    </div>

                </div>

            </div>
        </div>
        <!-- /.box -->



        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">총
                    <asp:Literal runat="server" ID="lbTotal">{{total}}</asp:Literal>
                    건</h3>
            </div>
            <!-- /.box-header -->

            <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-bordered ">
                    <colgroup>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>번호</th>
                            <th>거래일시</th>
                            <th>납부방법</th>
                            <th>의뢰인/수취인</th>
                            <th>입금액(원)</th>
                            <th>입금계좌번호</th>
                            <th>처리점</th>
                            <th>트립명</th>
                            <th>비용구분</th>
                            <th>코스트센터</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="item in paymentlist">
                            <td>{{item.rownum }}</td>
                            <td>{{item.payment_date}} </td>
                            <td>{{item.payment_type }}</td>
                            <td>{{item.depositor }}</td>
                            <td>{{item.amount | number }}</td>
                            <td>{{item.account }}</td>
                            <td>{{item.bank_name}} </td>
                            <td>{{item.visit_country}}</td>
                            <td>{{item.amount_type }}</td>
                            <td>{{item.cost_center }}</td>
                        </tr>
                        <tr ng-hide="total > 0" id="trNoData">
                            <td colspan="10">데이터가 없습니다.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="box-body table-responsive no-padding" style="display:none;">
                <table class="table table-hover table-bordered " id="tablePayment">
                    <colgroup>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>번호</th>
                            <th>거래일시</th>
                            <th>납부방법</th>
                            <th>의뢰인/수취인</th>
                            <th>입금액(원)</th>
                            <th>입금계좌번호</th>
                            <th>처리점</th>
                            <th>트립명</th>
                            <th>비용구분</th>
                            <th>코스트센터</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="item in paymentlist">
                            <td>{{item.rownum }}</td>
                            <td>{{item.payment_date}} </td>
                            <td>{{item.payment_type }}</td>
                            <td>{{item.depositor }}</td>
                            <td>{{item.amount | number}}</td>
                            <td>{{item.account }}</td>
                            <td>{{item.bank_name}} </td>
                            <td>{{item.visit_country}}</td>
                            <td>{{item.amount_type }}</td>
                            <td>{{item.cost_center }}</td>
                        </tr> 
                    </tbody>
                </table>
            </div>

            <div class="box-footer clearfix text-center">
                <paging class="small" page="page" page-size="rowsPerPage" total="a_total" show-prev-next="true" show-first-last="true" paging-action="getApplyStateList({ page : page})"></paging>
            </div>



        </div>

    </section>
</asp:Content>
