﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_participation_visiontrip_apply_plan_default" MasterPageFile="~/@mgt/mgt_afterlogin.master" %>

<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        .loading_box {min-width:282px;height:134px;padding:90px 35px 0 35px;font-size:15px;color:#929292;text-align:center;letter-spacing:-0.5px;border:1px solid #ededed;background:#fff url('/common/img/common/loading.gif') no-repeat center 30px;}
        .tr_cancel {
            background-color: steelblue;
        }

        .float_right {
            float: right;
            margin-right: 10px;
        }

        .icheckbox {
            display: inline-block;
            *display: inline;
            vertical-align: middle;
            margin: 0;
            padding: 0;
            width: 20px;
            height: 20px;
            background: url(/@mgt/template/plugins/iCheck/flat/blue.png) no-repeat;
            border: none;
            cursor: pointer;
        }

            .icheckbox.checked {
                background-position: -22px 0;
            }

        .table tr td {
            vertical-align: middle !important;
        }

        .btnModify {
            min-width: 140px;
        }

        .iradio {
            display: inline-block;
            *display: inline;
            vertical-align: middle;
            margin: 0;
            padding: 0;
            width: 20px;
            height: 20px;
            background: url(/@mgt/template/plugins/iCheck/flat/blue.png) no-repeat;
            border: none;
            cursor: pointer;
        }

        .iradio {
            background-position: -88px 0;
        }

            .iradio.checked {
                background-position: -110px 0;
            }

            .iradio.disabled {
                background-position: -132px 0;
                cursor: default;
            }

            .iradio.checked.disabled {
                background-position: -154px 0;
            }
    </style>
    <script type="text/javascript" src="/assets/angular/1.4.8/angular.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0-beta.2/angular-sanitize.js"></script>

    <script type="text/javascript" src="/assets/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="/assets/angular/angular-moment.min.js"></script>

    <script type="text/javascript" src="/@mgt/participation/visiontrip/apply_plan/default.js?v=1.2"></script>
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script src="/@mgt/template/plugins/iCheck/icheck.min.js"></script>

    <script type="text/javascript" src="/@mgt/common/js/paging.js"></script>
    <script type="text/javascript" src="/common/js/site/angular-app.js"></script>

    <script type="text/javascript" src="/@mgt/participation/visiontrip/common/multiselect.js"></script>
    <link href="/@mgt/participation/visiontrip/common/template/visiontrip_form.css" rel="stylesheet" />

    <script type="text/javascript">
        $(function () {

            //탭이동
            $(".nav-tabs > li").click(function () {
                var idx = $(this).index() + 1;
                $(".nav-tabs > li").removeClass("active");
                $(this).addClass("active");
                $(".tab-pane").hide();
                $("#tab" + idx).show();

                $("#hdnTabInfo").val(idx);

                $(".allcheck").iCheck("uncheck");

                //컴파스 전송은 신청현황탭에서만
                if (idx == 1)
                    $("#btnCompassSend").show();
                else
                    $("#btnCompassSend").hide();

                return false;
            });

            $(".allcheck").on("ifChanged", function (sender) {
                var val = $(this).attr("data-val");
                var checked = $(this).prop("checked");

                $.each($(".item_check"), function () {
                    $(this).iCheck(checked ? "check" : "uncheck");
                });
            });


            //$(".rd_addr").on("ifChanged", function (sender) {
            //    var val = $(this).attr("id");
            //    var checked = $(this).prop("checked");

            //    alert(val);
            //});


        });

        function applyAllCheck() {
            var val = $(this).attr("data-val");
            var checked = $(this).prop("checked");

            $.each($(".item_check"), function () {
                $($(this).find("input")).iCheck(checked ? "check" : "uncheck");
            });
        }


    </script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    <input type="hidden" runat="server" id="hdnRequestSubType" />
    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <%--<div ng-app="defaultApp" ng-controller="defaultCtrl" id="defaultCtrl">--%>
        <iframe id="txtArea1" style="display: none"></iframe>

        <!-- 로딩 -->
        <div style="width: 100%; height: 100%; position: fixed; left: 0; top: 0; z-index: 1990; opacity: 0.0; background: #000; display: none" id="loading_bg"></div>
        <div class="loading_box" style="position: fixed; left: 35%; width: 30%; top: 40%; z-index: 1991; display: none;" id="loading_container">
            <span class="msg"></span>
        </div>
        <!--//-->

        <input type="hidden" id="hdnTabInfo" value="1" />
        <div class="box box-default collapsed-box search_container">
            <div class="box-header with-border section-search">
                <div class="pull-left" style="width: 100%;">
                    <button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;"><i class="fa fa-plus search_toggle"></i></button>
                    <h3 class="box-title" style="padding-top: 5px">검색</h3>
                    <div style="float: right;">
                        <input type="button" id="btnCompassSend" class="btn btn-bitbucket " ng-click="compassSend()" value="참가자관리 업로드" />
                        <input type="button" id="btnExcel" class="btn btn-bitbucket " ng-click="exportData()" value="엑셀불러오기" />
                        <input type="button" id="btnCreate" class="btn btn-bitbucket " ng-click="createPopup()" value="신규" />
                    </div>
                </div>

            </div>
            <div class="form-horizontal box-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">트립유형</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlTripType" CssClass="form-control" Style="width: 150px;">
                                <asp:ListItem Text="전체" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">년 도</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control" Width="80px"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="s_keyword" class="col-sm-2 control-label">트립명</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="ddlTripName" CssClass="form-control" Width="150px">
                                <asp:ListItem Text="전체" Value=""></asp:ListItem>
                                <%--<asp:ListItem ng-repeat="lst in select_tripname" Value="{{lst.scheduleid}}" ng-model="select_tripname">{{lst.visitcountry}}</asp:ListItem>--%>
                                <%--#12725 요청으로 costcenter로 변경--%>
                                <asp:ListItem ng-repeat="lst in select_tripname" Value="{{lst.scheduleid}}" ng-model="select_tripname">{{lst.costcenter}}</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <a ng-click="searchClick()" id="btn_search" class="btn btn-primary" style="width: 100px">검색 <i class="fa fa-arrow-circle-right"></i></a>
                    </div>

                </div>

            </div>
        </div>
        <!-- /.box -->

        <%--<div class="box box-primary">--%>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li runat="server" id="tabm1" class="active" ng-click="getApplyStateList({ page:1, tripYear: tripYear, tripType: tripType, tripID: tripID })"><a href="javascript:void(0);">신청현황</a></li>
                <li runat="server" id="tabm2" ng-click="getApplyInfoList({ page:1, tripYear: tripYear, tripType: tripType, tripID: tripID })"><a href="javascript:void(0);">신청정보</a></li>
                <li runat="server" id="tabm3" ng-click="getParticipantList({ page:1, tripYear: tripYear, tripType: tripType, tripID: tripID })"><a href="javascript:void(0);">참가자정보</a></li>
                <li runat="server" id="tabm4" ng-click="getAddrList({ page:1, tripYear: tripYear, tripType: tripType, tripID: tripID })"><a href="javascript:void(0);">참가자주소</a></li>
                <li runat="server" id="tabm5" ng-click="getSMSList({ page:1, tripYear: tripYear, tripType: tripType, tripID: tripID })"><a href="javascript:void(0);">SMS/MMS</a></li>
                <li runat="server" id="tabm6" ng-click="getStatisticsList({ page:1, tripYear: tripYear, tripType: tripType, tripID: tripID })"><a href="javascript:void(0);">통계</a></li>
            </ul>


            <div class="tab-content">

                <%--신청현황--%>
                <div class="active tab-pane" id="tab1" runat="server">
                    <div class="box-header">
                        <h3 class="box-title">총 {{ a_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered " id="tableApply">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="chkApplyAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th>#</th>
                                    <th>트립유형</th>
                                    <th>년도</th>
                                    <th>트립명</th>
                                    <th>참가자명</th>
                                    <th>후원자번호</th>
                                    <th>신청현황</th>
                                    <th>신청일</th>
                                    <th>신청비</th>
                                    <th>참가비</th>
                                    <th>후원어린이만남비</th>
                                    <th>여권</th>
                                    <th>추가첨부</th>
                                    <th>취소</th>
                                    <th>수정</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applystate_list" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>
                                        <label>
                                            <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="check{{item.applyid }}" icheck /></label></td>
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.visiontrip_type }}</td>
                                    <td>{{item.trip_year }}</td>
                                    <td>{{item.trip_name }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>{{item.conid }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.apply_state}}</span>
                                        <div class="form-group-sm" ng-if="item.editmode == true" style="width: 80px;">
                                            <select id="{{item.applyid}}_apply_state" class="form-control">
                                                <option ng-repeat="state in select_applystate" ng-selected="item.apply_state_cd == state.codevalue" value="{{state.codevalue}}">{{state.codename}}</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>{{item.apply_date }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true && item.request_cost_yn == 'Y' ">{{item.request_cost | number }}</span>
                                        <div class="form-group-sm" ng-if="item.editmode == true">
                                            <select id="{{item.applyid}}_request_cost" class="form-control" style="width: 80px;" ng-disabled="item.request_cost_yn == 'Y' ? true : false">
                                                <option ng-repeat="pay in select_paymentYN" ng-selected="item.request_cost_yn == pay.codevalue" value="{{pay.codevalue}}">{{pay.codename}}</option>
                                            </select>
                                        </div>
                                        <%--<input type="number" ng-if="item.editmode == true" id="{{item.applyid + '_request_cost'}}" value="{{item.request_cost}}" />--%>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true && item.trip_cost_yn == 'Y'">{{item.trip_cost | number }}</span>
                                        <div class="form-group-sm" ng-if="item.editmode == true">
                                            <select id="{{item.applyid}}_trip_cost" class="form-control" style="width: 80px;" ng-disabled="item.trip_cost_yn == 'Y' ? true : false">
                                                <option ng-repeat="pay in select_paymentYN" ng-selected="item.trip_cost_yn == pay.codevalue" value="{{pay.codevalue}}">{{pay.codename}}</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true && item.childmeet_cost_yn == 'Y'">{{item.childmeet_cost | number }}</span>
                                        <div class="form-group-sm" ng-if="item.editmode == true">
                                            <select id="{{item.applyid}}_childmeet_cost" class="form-control" style="width: 80px;" ng-disabled="item.childmeet_cost_yn == 'Y' ? true : false">
                                                <option ng-repeat="pay in select_paymentYN" ng-selected="item.childmeet_cost_yn == pay.codevalue" value="{{pay.codevalue}}">{{pay.codename}}</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td><span ng-if="item.passport_path != '' && item.editmode != true"><a ng-href="{{item.passport_path}}" target="_blank"><i class="fa fa-files-o "></i></a></span>
                                        <div ng-if="item.editmode == true" style="width: 230px;">

                                            <div ng-show="item.passportChange == false && item.passport_path != '' ">
                                                <a ng-href="{{item.passport_path }}" target="_blank" style="width: 100px;">{{item.passport_name}}</a>
                                                <a href="javascript:void(0);" class="btn btn-default" id="btnPassportDel_{{item.applyid }}" ng-click="passportDelete(item, $event)"><span><i class="fa fa-close"></i></span></a>
                                                <input type="hidden" id="hdnPassportID_{{item.applyid }}" value="" />
                                                <input type="hidden" id="hdnDelyn_{{item.applyid }}" value="{{item.passport }}" />
                                            </div>
                                            <div ng-show="item.passportChange == true  || item.passport_path == ''">
                                                <input type="text" runat="server" data-id="path_btn_passportfile_{{item.applyid }}" value="{{item.passport_name }}" class="form-control pull-left" style="width: 100px;" readonly="readonly" />
                                                <label for="lb_passport_path_{{item.applyid }}" class="hidden"></label>
                                                <input type="hidden" id="path_btn_passportfile_{{item.applyid }}" value="" />
                                                <a href="javascript:void(0);" class="btn btn-default" id="btn_passportfile_{{item.applyid }}"><span>파일선택</span></a>
                                                <a href="javascript:void(0);" class="btn btn-default" id="btnPassportChange_Del_{{item.applyid }}" ng-click="passportChangeDelete(item, $event)"><span><i class="fa fa-close"></i></span></a>
                                            </div> 
                                        </div> 
                                    </td>
                                    <td><span ng-if="item.etc_attach > 0 && item.editmode != true"><a ng-click="attachPopup(item)"><i class="fa fa-files-o "></i></a></span>
                                        <div ng-if="item.editmode == true" style="width: 230px; text-align: left;">

                                            <div ng-repeat="input in item.input_fils| filter:{delyn: 'N', newyn:'N'}" class="form-group-sm">
                                                <a>{{input.filename}}</a><a ng-click="modifyInputFiles(item, $index, input.key)" class="btn btn-default btn-xs"><i class="fa fa-close"></i></a>
                                            </div>


                                            <a ng-click="addInputFiles(item, $index)" ng-href="" class="btn btn-default btn-xs"><%--<i class="fa fa-plus attachment"></i>--%>첨부파일추가</a>

                                            <div ng-repeat="input in item.input_fils | filter:{delyn:'N', newyn: 'Y'}" class="form-group-sm" on-finish-render="ngRepeatFinished">

                                                <input type="text" runat="server" ng-model="input.filename" data-id="path_btn_etcfile_{{input.applyid +'_'+ input.key}}" value="" class="form-control pull-left" style="width: 100px;" />
                                                <input type="hidden" id="path_btn_etcfile_{{input.applyid}}_{{input.key}}" ng-model="input.filepath" value="" />

                                                <a href="javascript:void(0);" class="btn btn-default fl" id="btn_etcfile_{{input.applyid}}_{{input.key}}"><span>파일선택</span></a>
                                                <a ng-click="removeInputFiles(item, $index, input.key)" class="btn btn-default"><i class="fa fa-close"></i></a>

                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.cancel_yn}}</span>
                                        <label ng-if="item.editmode == true">
                                            <input ng-model="item.cancel_check" class="icheck" id="{{item.applyid }} + '_cancel'"
                                                type="checkbox" icheck />
                                        </label>
                                    </td>
                                    <td class="center">
                                        <input type="button" ng-if="item.editmode != true" class="btn btn-default" ng-click="modifyClick(item)" value="수정" />
                                        <input type="button" ng-if="item.editmode == true" class="btn btn-default" ng-click="saveClick(item)" value="저장" />
                                        <input type="button" ng-if="item.editmode == true" class="btn btn-default" ng-click="cancelClick(item)" value="취소" />
                                    </td>
                                </tr>
                                <tr ng-hide="a_total > 0">
                                    <td colspan="16">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="a_page" page-size="a_rowsPerPage" total="a_total" show-prev-next="true" show-first-last="true" paging-action="getApplyStateList({page : page})"></paging>
                    </div>

                    <div class="excel-area" style="display: none;">
                        <table class="table table-hover table-bordered " id="tableExcel1" style="border: 1px solid #f4f4f4">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">#</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">트립유형</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">년도</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">트립명</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">참가자명</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">후원자번호</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">신청현황</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">신청일</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">신청비</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">참가비</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">후원어린이만남비</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">여권</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">추가첨부</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">취소</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applystate_list|filter:{rowselect: true}" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td style="border: 1px solid #f4f4f4;">{{item.rownum }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.visiontrip_type }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.trip_year }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.trip_name }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.sponsor_name }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.conid }}</td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.apply_state}}</span></td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.apply_date }}</td>
                                    <td style="border: 1px solid #f4f4f4;"><span ng-if="item.request_cost_yn == 'Y'">{{item.request_cost | number }}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span ng-if="item.trip_cost_yn == 'Y'">{{item.trip_cost | number }}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.childmeet_cost | number }}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span ng-if="item.passport_path != ''">{{item.passport_name}}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span ng-if="item.etc_attach > 0 ">{{item.etc_attach}}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.cancel_yn}}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 신청현황--%>

                <%--신청정보--%>
                <div class="tab-pane" id="tab2" runat="server">

                    <div class="box-header">
                        <h3 class="box-title">총 {{ i_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered ">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="chkInfoAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th>#</th>
                                    <th>참가자명</th>
                                    <th>후원여부</th>
                                    <%--<th>성별</th>--%>
                                    <th>생년월일</th>
                                    <th>국적</th>
                                    <th>영어능력</th>
                                    <th>요청룸</th>
                                    <th>룸메이트</th>
                                    <th style="width: 100px;">후원어린이 만남</th>
                                    <th>종교</th>
                                    <th>VT참가수</th>
                                    <th>그룹</th>
                                    <th>참가비<br />
                                        (관리자)</th>
                                    <th>후원어린이
                                        <br />
                                        만남비
                                        <br />
                                        (관리자)</th>
                                    <th>수정</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applyinfo_list" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>
                                        <label>
                                            <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="info_{{item.applyid }}" icheck /></label></td>
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>{{item.sponsor_yn }}</td>
                                    <%--<td>{{item.gender_code }}</td>--%>
                                    <td>{{item.birth_date }}</td>
                                    <td>{{item.nation }}</td>
                                    <td>{{item.english_level }}</td>
                                    <td>{{item.room_type }}</td>
                                    <td>{{item.room_detail }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.child_key }}</span>
                                        <div ng-if="item.editmode == true">
                                            <span class="pull-left" style="width: 230px;" id="spMultiSelect">
                                                <multiselect class="input-xlarge" multiple="true" style="width: 230px;"
                                                    ng-model="item.selectedChild"
                                                    options="c.name for c in item.mychild"
                                                    change="selected()"></multiselect>
                                            </span>
                                            <div class="form-group text-left">
                                                <a ng-click="addInput(item)" ng-href="" class="btn btn-default">+직접입력추가</a>
                                                <div ng-repeat="input in item.child_inputs| filter:{delyn: 'N'}" runat="server" on-finish-render="ngRepeatFinished_Child">
                                                    <div>
                                                        <input type="text" ng-model="input.id" class="form-control pull-left  childinput" maxlength="15" style="width: 170px;" placeholder="번호 입력" runat="server" />
                                                        <a ng-click="removeInput(item, $index, input.key)" class="btn btn-default  pull-left"><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{item.religion }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.visiontrip_history}}</span>
                                        <div class="form-group-sm">
                                            <select id="info_{{item.applyid}}_visiontrip_history" ng-if="item.editmode == true" style="width: 70px;" class="form-control">
                                                <option ng-repeat="vth in select_vthistory" ng-selected="item.visiontrip_history_code == vth.codename" value="{{vth.codevalue}}">{{vth.codename}}</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>{{item.group_type }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.trip_cost | number }}</span>
                                        <input type="number" ng-if="item.editmode == true" id="{{'info_'+item.applyid + '_trip_cost'}}" value="{{item.trip_cost}}" />
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.childmeet_cost | number }}</span>
                                        <input type="number" ng-if="item.editmode == true" id="{{'info_'+item.applyid + '_childmeet_cost'}}" value="{{item.childmeet_cost}}" />
                                    </td>
                                    <td class="center">
                                        <input type="button" ng-if="item.editmode != true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="modifyClick(item)" value="수정" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="saveClick(item)" value="저장" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="cancelClick(item)" value="취소" />
                                    </td>
                                </tr>
                                <tr ng-hide="i_total > 0">
                                    <td colspan="17">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="i_page" page-size="i_rowsPerPage" total="i_total" show-prev-next="true" show-first-last="true" paging-action="getApplyInfoList({page : page})"></paging>
                    </div>


                    <div class="excel-area" id="divInfo" style="display: none;">
                        <table class="table table-bordered" style="border: 1px solid #f4f4f4" id="tableExcel2">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">#</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">참가자명</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">후원여부</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">생년월일</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">국적</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">영어능력</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">요청룸</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">룸메이트</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">후원어린이 만남</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">종교</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">VT참가수</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">그룹</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">참가비<br />
                                        (관리자)</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">후원어린이<br />
                                        만남비<br />
                                        (관리자)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in applyinfo_list|filter:{rowselect: true}" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td style="border: 1px solid #f4f4f4;">{{item.rownum }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.sponsor_name }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.sponsor_yn }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.birth_date }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.nation }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.english_level }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.room_type }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.room_detail }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.child_key}}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.religion }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.visiontrip_history }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.group_type }}</td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.trip_cost | number }}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.childmeet_cost | number }}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 신청정보--%>
                <%--참가자정보--%>
                <div class="tab-pane" id="tab3" runat="server">

                    <div class="box-header">
                        <h3 class="box-title">총 {{ p_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered ">
                            <thead>
                                <tr>
                                    <th rowspan="2">
                                        <input type="checkbox" id="chkParticipantAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th rowspan="2">#</th>
                                    <th rowspan="2">성명</th>
                                    <th rowspan="2">영문명</th>
                                    <th rowspan="2">성별</th>
                                    <th rowspan="2">휴대번호</th>
                                    <th rowspan="2">Email</th>
                                    <th rowspan="2">직업</th>
                                    <th rowspan="2">교회명</th>
                                    <th rowspan="2">비고<br />
                                        (관리자only)</th>
                                    <th colspan="3">현금영수증</th>
                                    <th colspan="3">비상연락처</th>
                                    <th rowspan="2">수정</th>
                                </tr>
                                <tr>
                                    <th>발급자</th>
                                    <th>휴대번호</th>
                                    <th>참가자와<br />
                                        관계</th>
                                    <th>성함</th>
                                    <th>연락처</th>
                                    <th>참가자와<br />
                                        관계</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in participant_list" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>
                                        <label>
                                            <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="participant_{{item.applyid }}" icheck /></label></td>

                                    <td>{{item.rownum }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td> 
                                        <span ng-if="item.editmode != true">{{item.sponsor_name_eng}}</span>
                                        <input type="text" ng-if="item.editmode == true" id="{{'participant_'+item.applyid + '_sponsor_name_eng'}}" class="form-control" value="{{item.sponsor_name_eng}}" maxlength="50" /></td>
                                    <td> 
                                        <span ng-if="item.editmode != true">{{item.gendercode}}</span>
                                        <input type="text" ng-if="item.editmode == true" id="{{'participant_'+item.applyid + '_gendercode'}}" class="form-control" value="{{item.gendercode}}" maxlength="1" /></td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.tel}}</span>
                                        <input type="text" ng-if="item.editmode == true" id="{{'participant_'+item.applyid + '_tel'}}" class="form-control" value="{{item.tel}}" maxlength="13" />
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.email}}</span>
                                        <input type="text" ng-if="item.editmode == true" id="{{'participant_'+item.applyid + '_email'}}" class="form-control" value="{{item.email}}" maxlength="50" />
                                    </td>
                                    <td>{{item.job }}</td>
                                    <td>{{item.church }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.remark}}</span>
                                        <input type="text" ng-if="item.editmode == true" id="{{'participant_'+item.applyid + '_remark'}}" class="form-control" value="{{item.remark}}" maxlength="2000" />
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.cashreceipt_name}}</span>
                                        <input type="text" ng-if="item.editmode == true" id="{{'participant_'+item.applyid + '_cashreceipt_name'}}" class="form-control" value="{{item.cashreceipt_name}}" maxlength="100" />
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.cashreceipt_tel}}</span>
                                        <input type="text" ng-if="item.editmode == true" id="{{'participant_'+item.applyid + '_cashreceipt_tel'}}" class="form-control" value="{{item.cashreceipt_tel}}" maxlength="100" />
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.cashreceipt_relation}}</span>
                                        <input type="text" ng-if="item.editmode == true" id="{{'participant_'+item.applyid + '_cashreceipt_relation'}}" class="form-control" value="{{item.cashreceipt_relation}}" maxlength="100" />
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.emergencycontact_name}}</span>
                                        <input type="text" ng-if="item.editmode == true" id="{{'participant_'+item.applyid + '_emergencycontact_name'}}" class="form-control" value="{{item.emergencycontact_name}}" maxlength="100" />
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.emergencycontact_tel}}</span>
                                        <input type="text" ng-if="item.editmode == true" id="{{'participant_'+item.applyid + '_emergencycontact_tel'}}" class="form-control" value="{{item.emergencycontact_tel}}" maxlength="100" />
                                    </td>
                                    <td>
                                        <span ng-if="item.editmode != true">{{item.emergencycontact_relation}}</span>
                                        <input type="text" ng-if="item.editmode == true" id="{{'participant_'+item.applyid + '_emergencycontact_relation'}}" class="form-control" value="{{item.emergencycontact_relation}}" maxlength="100" />
                                    </td>
                                    <td class="center">
                                        <input type="button" ng-if="item.editmode != true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="modifyClick(item)" value="수정" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="saveClick(item)" value="저장" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="cancelClick(item)" value="취소" />
                                    </td>
                                </tr>
                                <tr ng-hide="p_total > 0">
                                    <td colspan="16">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="p_page" page-size="p_rowsPerPage" total="p_total" show-prev-next="true" show-first-last="true" paging-action="getParticipantList({page : page})"></paging>
                    </div>
                    <div class="excel-area" id="divParticipant" style="display: none;">
                        <table class="table table-hover table-bordered " id="tableExcel3" style="border: 1px solid #f4f4f4">
                            <thead>
                                <tr>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">#</th>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">성명</th>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">영문명</th>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">성별</th>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">휴대번호</th>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">Email</th>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">직업</th>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">교회명</th>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">비고<br />
                                        (관리자only)</th>
                                    <th colspan="3" style="border: 1px solid #f4f4f4; font-weight: bold;">현금영수증</th>
                                    <th colspan="3" style="border: 1px solid #f4f4f4; font-weight: bold;">비상연락처</th>
                                </tr>
                                <tr>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">발급자</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">휴대번호</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">참가자와<br />
                                        관계</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">성함</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">연락처</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">참가자와<br />
                                        관계</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in participant_list|filter:{rowselect: true}" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td style="border: 1px solid #f4f4f4;">{{item.rownum }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.sponsor_name }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.sponsor_name_eng }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.gendercode }}</td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.tel}}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.email}}</span></td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.job }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.church }}</td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.remark}}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.cashreceipt_name}}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.cashreceipt_tel}}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.cashreceipt_relation}}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.emergencycontact_name}}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.emergencycontact_tel}}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.emergencycontact_relation}}</span></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>

                </div>
                <%--end 참가자정보--%>
                <%--참가자주소--%>
                <div class="tab-pane" id="tab4" runat="server">

                    <div class="box-header">
                        <h3 class="box-title">총 {{ d_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered ">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr ng-class="">
                                    <th>
                                        <input type="checkbox" id="chkAddrAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                    <th>#</th>
                                    <th>성명</th>
                                    <th>주소1</th>
                                    <th>주소2</th>
                                    <th>우편번호</th>
                                    <th>수정</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in addr_list" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                    <td>
                                        <label>
                                            <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="{{'p_check_'+item.applyid }}" icheck /></label></td>
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.sponsor_name }}</td>
                                    <td>
                                        <span ng-if="item.editmode != true && item.domesticYN == true">{{item.address_doro }} &nbsp;//&nbsp; {{item.address_jibun }}</span>
                                        <span ng-if="item.editmode != true && item.domesticYN != true">{{item.address_overseas }}</span>

                                        <div ng-if="item.editmode == true" class="form-group-sm text-left">

                                            <input type="hidden" id="locationType_{{item.applyid }}" value="" />
                                            <input type="hidden" id="hfAddressType_{{item.applyid }}" value="" />

                                            <input type="hidden" id="zipcode_{{item.applyid }}" />
                                            <input type="hidden" id="addr1_{{item.applyid }}" />
                                            <input type="hidden" id="addr2_{{item.applyid }}" />
                                            <input type="hidden" id="dspAddrJibun_{{item.applyid }}" value="" />
                                            <input type="hidden" id="dspAddrDoro_{{item.applyid }}" value="" />


                                            <div class="location form-group-sm">
                                                <div class="pull-left" style="width: 30%;">
                                                    <label for="selectlocation_{{item.applyid }}" class="hidden">국내/해외 </label>
                                                    <select id="selectlocation_{{item.applyid }}" onchange="locationChange(this)" class="form-control" style="width: 100%;">
                                                        <option ng-repeat="lst in select_location" ng-selected="item.location == lst.codevalue" value="{{lst.codevalue}}">{{lst.codename}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="pn_addr_domestic_{{item.applyid }}" style="display: none">
                                                <div class="pull-left">
                                                    <label for="zipcode" class="hidden">주소찾기</label>
                                                    <a href="javascript:void(0)" class="btn btn-default" runat="server" id="popup" ng-click="addrpopup(item, $event)">주소찾기</a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="hidden" id="addr_domestic_zipcode_{{item.applyid }}" />
                                                    <input type="hidden" id="addr_domestic_addr1_{{item.applyid }}" />
                                                    <input type="hidden" id="addr_domestic_addr2_{{item.applyid }}" />
                                                    <div class="pull-left" style="padding-top: 5px;">
                                                        <p id="addr_road_{{item.applyid }}" class="margin-bottom">{{item.full_doro }} </p>
                                                        <p id="addr_jibun_{{item.applyid }}" class="">{{item.full_jibun }} </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- 해외주소 체크 시 -->
                                            <div id="pn_addr_overseas_{{item.applyid }}" style="display: none">
                                                <div class="form-group-sm">
                                                    <div class="pull-left" style="width: 30%;">
                                                        <label for="selectHouseCountry_{{item.applyid }}" class="hidden">국가선택</label>
                                                        <select id="selectHouseCountry_{{item.applyid }}" class="form-control" style="width: 100%;">
                                                            <option ng-repeat="lst in countries" ng-selected="item.location == lst.codename" value="{{lst.codename}}">{{lst.codename}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="pull-left" style="width: 40%;">
                                                        <label for="addr_overseas_zipcode_{{item.applyid }}" class="hidden">우편번호</label>
                                                        <input type="text" id="addr_overseas_zipcode_{{item.applyid }}" class="form-control" value="{{item.zipcode_overseas}}" style="width: 100%" placeholder="우편번호" maxlength="150" />
                                                    </div>
                                                </div>
                                                <div class="form-group-sm">
                                                    <div style="width: 100%;">
                                                        <input type="text" id="addr_overseas_addr1_{{item.applyid }}" class="form-control" value="{{item.address_overseas}}" placeholder="주소" style="width: 100%;" maxlength="1000" />
                                                    </div>
                                                </div>
                                                <div class="form-group-sm">
                                                    <div style="width: 100%;">
                                                        <input type="text" id="addr_overseas_addr2_{{item.applyid }}" class="form-control" value="{{item.addr2_overseas}}" placeholder="상세주소" style="width: 100%;" maxlength="1000" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!--// -->
                                        </div>

                                    </td>
                                    <%-- <td ng-if="item.addr_colspan == 1"><span ng-if="item.editmode != true">{{item.address_doro }}</span></td>--%>
                                    <td><span ng-if="item.editmode != true">{{item.address2 }}</span></td>
                                    <td><span ng-if="item.editmode != true">{{item.zipcode }}</span></td>
                                    <td class="center">
                                        <input type="button" ng-if="item.editmode != true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="modifyClick(item)" value="수정" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="saveClick(item)" value="저장" />
                                        <input type="button" ng-if="item.editmode == true && item.cancel_yn == 'N'" class="btn btn-default" ng-click="cancelClick(item)" value="취소" />
                                    </td>
                                </tr>
                                <tr ng-hide="d_total > 0">
                                    <td colspan="7">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="d_page" page-size="d_rowsPerPage" total="d_total" show-prev-next="true" show-first-last="true" paging-action="getAddrList({page : page})"></paging>
                    </div>

                    <div class="box-body table-responsive no-padding" style="display: none;">
                        <table class="table table-hover table-bordered" id="tableExcel4" style="border: 1px solid #f4f4f4">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">#</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">성명</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">주소1</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">주소2</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">우편번호</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in addr_list | filter:{rowselect: true}">
                                    <td style="border: 1px solid #f4f4f4;">{{item.rownum }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.sponsor_name }}</td>
                                    <td style="border: 1px solid #f4f4f4;">
                                        <span ng-if="item.domesticYN == true">{{item.address_doro }} &nbsp;//&nbsp; {{item.address_jibun }}</span>
                                        <span ng-if="item.domesticYN != true">{{item.address_overseas }}</span>
                                    </td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.address2 }}</span></td>
                                    <td style="border: 1px solid #f4f4f4;"><span>{{item.zipcode }}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 참가자주소--%>
                <%--SMS/MMS--%>
                <div class="tab-pane" id="tab5" runat="server">
                    <div class="box-body">
                        <div style="float: left; width: 30%">
                            <div class="form-horizontal box-body">


                                <div class="box-body table-responsive no-padding">

                                    <table class="table table-hover table-bordered" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" id="chkSmsAll" class="icheck icheckbox_flat-blue allcheck" /></th>
                                                <th>#</th>
                                                <th>성명</th>
                                                <th>휴대번호</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in sms_list" ng-class="item.cancel_class" ng-model="item.cancel_class">
                                                <td>
                                                    <label>
                                                        <input type="checkbox" ng-model="item.rowselect" class="icheck item_check" id="check{{item.userid }}" icheck /></label></td>
                                                <td>{{item.rownum}}</td>
                                                <td>{{item.sponsor_name}}</td>
                                                <td>{{item.tel}}</td>
                                            </tr>
                                            <tr ng-hide="m_total > 0">
                                                <td colspan="4">데이터가 없습니다.</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                                <div class="box-footer clearfix text-center">
                                    <paging class="small" page="m_page" page-size="m_rowsPerPage" total="m_total" show-prev-next="true" show-first-last="true" paging-action="getSMSList({page : page})"></paging>
                                </div>
                            </div>
                        </div>
                        <div style="float: left; width: 30%">

                            <div class="form-horizontal box-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <asp:TextBox runat="server" ID="txtTitle" CssClass="form-control" Width="100%" PlaceHolder="제목" MaxLength="400"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-12">
                                        <asp:TextBox runat="server" ID="txtMessage" CssClass="form-control" Width="100%" TextMode="MultiLine" Rows="12" PlaceHolder="내용" MaxLength="1000"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="s_result" class="col-sm-3 control-label">발신번호</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox runat="server" ID="txtSendTel" CssClass="form-control" Width="100%" MaxLength="11"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="s_result" class="col-sm-3 control-label">발송구분</label>
                                    <div class="col-sm-9 form-radio" style="margin-top: 5px;">
                                        <asp:RadioButtonList runat="server" ID="rdoSendType" RepeatLayout="Flow" RepeatDirection="Horizontal" Width="100%">
                                            <asp:ListItem Text="즉시발송" Value="D"></asp:ListItem>
                                            <asp:ListItem Text="예약발송" Value="R"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="s_result" class="col-sm-3 control-label">예약날짜</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="reservedatetime" runat="server" id="txtReserveDate" readonly="readonly" class="date datetime form-control inline" style="width: 100%; margin-right: 5px" />

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="float: left; width: 40%">
                            <div class="form-horizontal box-body">
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <asp:TextBox runat="server" ID="txtTelAdd" CssClass="form-control fc_numeric" Width="100%" PlaceHolder="휴대번호 직접입력" MaxLength="11"></asp:TextBox>
                                    </div>
                                    <label for="s_result" class="col-sm-3"><a class="btn btn-default" ng-click="addTel()">추가</a></label>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <label>
                                            받는사람
                                        </label>
                                        <label style="float: right; color: darkred; font-size: 10px;">
                                            *중복번호 1회 발송
                                        </label>
                                        <select multiple="multiple" id="ddlSMS" style="border: 1px solid; border-radius: 0; box-shadow: none; border-color: #d2d6de; width: 100%; min-height: 180px; height: auto;">
                                            <option ng-repeat="lst in sms_inputs" value="{{lst.id}}">{{lst.tel}}</option>
                                        </select>
                                    </div>
                                    <label for="s_result" class="col-sm-3">
                                        <a class="btn btn-default" ng-click="selectSMSList()">선택 불러오기</a>
                                        <a class="btn btn-default" ng-click="messageResultPopup()">결과보기</a>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-11">
                                        <a class="btn btn-default" ng-click="deleteSMSList()">선택삭제</a>
                                        <a class="btn btn-default" ng-click="clearSMSList()">전체삭제</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-11">
                                        <a class="btn btn-danger" ng-click="sendSMS()">전송하기</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--end SMS/MMS--%>
                <%--통계--%>
                <div class="tab-pane" id="tab6" runat="server">
                    <div class="box-header">
                        <h3 class="box-title">총 {{ s_total }} 건</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered ">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th rowspan="2">#</th>
                                    <th rowspan="2">트립명</th>
                                    <th rowspan="2">참가자수</th>
                                    <th colspan="2">성별</th>
                                    <th rowspan="2">CDSP후원자수</th>
                                    <th colspan="5">연령</th>
                                    <th colspan="4">비전트립 참가 횟수</th>
                                </tr>
                                <tr>
                                    <th>남</th>
                                    <th>여</th>
                                    <th>10대</th>
                                    <th>20대</th>
                                    <th>30대</th>
                                    <th>40대</th>
                                    <th>50대이상</th>
                                    <th>처음</th>
                                    <th>1회</th>
                                    <th>2회</th>
                                    <th>3회이상</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in statisticts_list">
                                    <td>{{item.rownum }}</td>
                                    <td>{{item.visit_country }}</td>
                                    <td>{{item.apply_cnt }}</td>
                                    <td>{{item.male_cnt }}</td>
                                    <td>{{item.female_cnt }}</td>
                                    <td>{{item.commitment_count }}</td>
                                    <td>{{item.ten }}</td>
                                    <td>{{item.twenty }}</td>
                                    <td>{{item.thirty }}</td>
                                    <td>{{item.forty }}</td>
                                    <td>{{item.fifty }}</td>
                                    <td>{{item.vh_zero }}</td>
                                    <td>{{item.vh_first }}</td>
                                    <td>{{item.vh_second }}</td>
                                    <td>{{item.vh_third }}</td>
                                </tr>
                                <tr ng-hide="s_total > 0 ">
                                    <td colspan="15">데이터가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <paging class="small" page="s_page" page-size="s_rowsPerPage" total="s_total" show-prev-next="true" show-first-last="true" paging-action="getStatisticsList({page : page})"></paging>
                    </div>

                    <div class="box-body table-responsive no-padding" style="display: none;">
                        <table id="tableExcel6" style="border: 1px solid #f4f4f4">
                            <colgroup>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">#</th>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">트립명</th>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">참가자수</th>
                                    <th colspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">성별</th>
                                    <th rowspan="2" style="border: 1px solid #f4f4f4; font-weight: bold;">CDSP후원자수</th>
                                    <th colspan="5" style="border: 1px solid #f4f4f4; font-weight: bold;">연령</th>
                                    <th colspan="4" style="border: 1px solid #f4f4f4; font-weight: bold;">비전트립 참가 횟수</th>
                                </tr>
                                <tr>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">남</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">여</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">10대</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">20대</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">30대</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">40대</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">50대이상</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">처음</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">1회</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">2회</th>
                                    <th style="border: 1px solid #f4f4f4; font-weight: bold;">3회이상</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in statisticts_list">
                                    <td style="border: 1px solid #f4f4f4;">{{item.rownum }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.visit_country }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.apply_cnt }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.male_cnt }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.female_cnt }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.commitment_count }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.ten }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.twenty }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.thirty }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.forty }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.fifty }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.vh_zero }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.vh_first }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.vh_second }}</td>
                                    <td style="border: 1px solid #f4f4f4;">{{item.vh_third }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--end 통계--%>
            </div>
            <!-- /.box-header -->
        </div>

    </section>
</asp:Content>
