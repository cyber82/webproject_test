﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="message_result.aspx.cs" Inherits="mgt_participation_visiontrip_apply_individual_message_result" ValidateRequest="false"%>

    <%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head id="Head1" runat="server">
    <title>compassion Admin</title>
    
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/@mgt/template/bootstrap/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="/@mgt/template/dist/css/AdminLTE.min.css" />
    <!-- iCheck -->
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/square/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/minimal/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/flat/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/dist/css/skins/skin-green.min.css" />
	<!-- Date Picker -->
    <link rel="stylesheet" href="/@mgt/template/plugins/datepicker/datepicker3.css" />
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/@mgt/template/plugins/daterangepicker/daterangepicker-bs3.css" /><link rel="stylesheet" href="/@mgt/common/css/admin.css" /><link rel="stylesheet" href="/@mgt/common/js/bootstrap-switch/bootstrap-switch.min.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- jQuery 2.1.4 -->
    <script src="/@mgt/template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!-- Bootstrap 3.3.5 -->
    <script src="/@mgt/template/bootstrap/js/bootstrap.min.js"></script>
	<script src="/@mgt/common/js/bootstrap-switch/bootstrap-switch.min.js"></script>

    <!-- iCheck -->
    <script src="/@mgt/template/plugins/iCheck/icheck.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="/@mgt/template/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->

    <script src="/@mgt/template/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="/@mgt/template/plugins/datepicker/locales/bootstrap-datepicker.kr.js"></script>

	<script type="text/javascript" src="/@mgt/common/js/common.js" defer="defer"></script>
	<script type="text/javascript" src="/@mgt/common/js/message.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/jquery.json-2.4.min.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/jquery.json-3.3.2.min.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/form.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/cookie.js" defer="defer"></script>
	
	<script type="text/javascript" src="/assets/jquery/wisekit/function.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.search.js" defer="defer"></script>
	<script type="text/javascript" src="/@mgt/common/js/ajaxupload.3.6.wisekit.js" defer="defer"></script> 
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.bootstrap.js"></script>
	<script type="text/javascript">

	    $(function () {

	        $("#btn_close").click(function () {
	            window.close();
	        });

	        if ($("#message").val() != "") {
	            alert($("#message").val());
	            $("#message").val("");
	            $("#action").val("");
	        }

            //전체 체크
	        $("#chkAll").change(function () {
	            if ($("#chkAll").is(":checked")) {
	                $(".messgeCheck").find("input[type=checkbox]").prop("checked", "checked");
	            } else {
	                $(".messgeCheck").find("input[type=checkbox]").prop("checked", false);
	            }
	        });

	    });

        //제목 더블클릭 시 본문 내용 보임
	    var showBody = function (id) {
	        $("#" + id).show(); 
	    };
	    var hideBody = function (id) {
	        $("#" + id).hide();
	    };

	    var DeleteClick = function () {
	        if ($(".messgeCheck").find("input[type=checkbox]:checked").length > 0) {
	            return true;
	        }
	        else 
	        {
	            alert("삭제할 항목을 선택 해주세요.");
	            return false;
	        }

	    };
    </script> 
</head>
           
<body>
<form runat="server" id="form">
        <input type="hidden" id="action" runat="server" value="" />
        <input type="hidden" id="message" runat="server" value="" />

    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">SMS/MMS 결과보기</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-header">
                        <h3 class="box-title">총
                            <asp:Literal runat="server" ID="lbTotal"></asp:Literal>
                            건</h3>
                        
                        <a class="btn btn-default" style="float:right;" runat="server" id="btnDelete" onserverclick="btnDelete_ServerClick" onclick="return DeleteClick()">삭제</a>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-bordered ">
                            <colgroup>
                                <col style="width:50px"/>
                                <col style="width:50px"/>
                                <col style="width:100px"/>
                                <col style="width:auto"/>
                                <col style="width:100px"/>
                                <col style="width:80px"/>
                                <col style="width:80px"/>
                                <col style="width:80px"/>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th><asp:CheckBox runat="server" ID="chkAll" /></th>
                                    <th>번호</th>
                                    <th>발송일</th>
                                    <th>제목</th>
                                    <th>총건수</th>
                                    <th>전달</th>
                                    <th>실패</th>
                                    <th>진행상황</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="repeater" OnItemDataBound="ListBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td><asp:CheckBox id="chkItem" runat="server" CssClass="messgeCheck"/>
                                                <asp:HiddenField id="hdnGroupNo" runat="server"/>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
                                            <td><%#Eval("SendDate")%></td>
                                            <td style="text-align: center;"><a ondblclick="showBody('<%#Eval("GroupNo")%>')"><%#Eval("Title")%></a>
                                                <div class="form-horizontal box-body" style="display: none; margin-top: 10px; width: 100%; height: auto; min-height: 50px; border: 1px solid #eee; text-align: center;" id="<%#Eval("GroupNo")%>">
                                                    <div class="form-group">
                                                        <div class="col-sm-8">
                                                            <label>메세지 내용</label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <a id="A2" class="btn btn-default right btn-xs" onclick="hideBody('<%#Eval("GroupNo")%>')"><i class="fa fa-close" aria-hidden="true"></i>숨김</a>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:TextBox runat="server" TextMode="MultiLine" ID="txtBody" Rows="6" Width="100%"
                                                                Text='<%#Eval("MessageBody")%>' CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                            </td>
                                            <td><%#Eval("TotalCount")%></td>
                                            <td><%#Eval("SussessCount")%></td>
                                            <td><%#Eval("FailCount")%></td>
                                            <td><%#Eval("MessageState")%></td> 
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr runat="server" visible="<%#repeater.Items.Count == 0 %>">
                                            <td colspan="7">데이터가 없습니다.</td>
                                        </tr>
                                    </FooterTemplate>
                                </asp:Repeater>

                            </tbody>
                        </table>
                    </div>

                    <div class="box-footer clearfix text-center">
                        <uc:paging runat="server" id="paging" onnavigate="paging_Navigate" enableviewstate="true" rowsperpage="10" pagespergroup="10" />
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right"> 
                        <button class="btn btn-default" runat="server" id="btn_close"><i class="fa fa-times"></i> 닫기</button>
                    </div><!-- /.box-footer -->
                </div><!-- /. box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    
    </section><!-- /.content -->

</form>

</body>    
</html>
