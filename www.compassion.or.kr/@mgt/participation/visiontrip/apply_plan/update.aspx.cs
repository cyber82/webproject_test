﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
using System.Data;
using System.Web.UI.HtmlControls;


public partial class mgt_participation_visiontrip_apply_plan_update : AdminBoardPage {


    CommonLib.WWW6Service.SoaHelperSoap _www6Service;
    protected string ids{
		set {
			this.ViewState.Add("ids", value);
		}
		get {
			if (this.ViewState["ids"] == null) return "";
			return this.ViewState["ids"].ToString();
		}
	}


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		v_admin_auth auth = base.GetPageAuth();
         
        ids = Request["r_id"]; 

        divUserInfo.Visible = false;
        divApplyInfo.Visible = false;
    }

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
        message.Value = ""; 
    }




    protected void btnUserSearch_Click(object sender, EventArgs e)
    {

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();


        #region 국외인경우 국가목록 
        var actionResult = new CodeAction().Countries();
        if (!actionResult.success)
        {
            base.AlertWithJavascript(actionResult.message);
        }

        ddlHouseCountry.DataSource = actionResult.data;
        ddlHouseCountry.DataTextField = "CodeName";
        ddlHouseCountry.DataValueField = "CodeName";
        ddlHouseCountry.DataBind();
        #endregion



        #region Compass - tSponsorGroup : 사용자 정보
        Object[] objSql = new object[1] { "VT_WebInfoInquiry" };
        Object[] objParam = new object[] { "DIVIS", "ConID" };
        Object[] objValue = new object[] { "BaseInfo", txtUserID.Text };


        string addr_zipcode, addr1_doro, addr1_jibun, addr1_overseas, addr2_overseas,
            addr2_doro, addr2_jibun;

        var userinfo = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
        if (userinfo.Rows.Count > 0)
        {
            divUserInfo.Visible = true;
            divApplyInfo.Visible = true;
            addr_zipcode = userinfo.Rows[0]["ZipCode"].ToString();
            addr1_doro = userinfo.Rows[0]["Doro_Addr1"].ToString();
            addr2_doro = userinfo.Rows[0]["Doro_Addr2"].ToString();
            addr1_jibun = userinfo.Rows[0]["Jibun_Addr1"].ToString();
            addr2_jibun = userinfo.Rows[0]["Jibun_Addr2"].ToString();

            addr1_overseas = userinfo.Rows[0]["Address1"].ToString();
            addr2_overseas = userinfo.Rows[0]["Address2"].ToString();


            txtUserName.Text = userinfo.Rows[0]["SponsorName"].ToString();
            txtUserNameEng.Text = userinfo.Rows[0]["EngName"].ToString();
            txtTel.Text = userinfo.Rows[0]["Phone"].ToString();
            txtEmail.Text = userinfo.Rows[0]["Email"].ToString();
            hdnSponsorID.Value = userinfo.Rows[0]["SponsorID"].ToString();

            dspAddrJibun.Value = addr1_jibun;
            dspAddrDoro.Value = addr1_doro;
            
            locationType.Value = userinfo.Rows[0]["LocationType"].ToString();

            if (userinfo.Rows[0]["LocationType"].ToString() == "국내")
            {
                addr_domestic.Checked = true;
                pn_addr_domestic.Style["display"] = "block";

                addr_domestic_zipcode.Value = addr_zipcode;
                addr_domestic_addr1.Value = addr1_doro + "//" + addr1_jibun;
                addr_domestic_addr2.Value = addr2_doro;

                addr_road.InnerText = "[도로명] (" + addr_zipcode + ") " + addr1_doro + " " + addr2_doro; addr_road.InnerText = "[도로명주소] (" + addr_zipcode + ") " + addr1_doro + " " + addr2_doro;
                addr_jibun.InnerText = "[지번] (" + addr_zipcode + ") " + addr1_jibun + " " + addr2_jibun;

                addr1.Value = addr1_doro + "//" + addr1_jibun;
                addr2.Value = addr2_doro;
                zipcode.Value = addr_zipcode;
            }
            else
            {
                addr_oversea.Checked = true;
                pn_addr_overseas.Style["display"] = "block";

                addr_overseas_zipcode.Value = addr_zipcode;
                addr_overseas_addr1.Value = addr1_overseas;
                addr_overseas_addr2.Value = addr2_overseas;


                addr1.Value = addr1_overseas;
                addr2.Value = addr2_overseas;
                zipcode.Value = addr_zipcode;

                ddlHouseCountry.SelectedValue = userinfo.Rows[0]["CountryCode"].ToString();
            }

            //트립 일정
            getTripSchedule();
            //후원어린이 정보
            getSponChildList(hdnSponsorID.Value, 0);
        }
        else
        {
            hdnSponsorID.Value = "";

            message.Value = "사용자 정보가 조회되지 않습니다.";
        }
        #endregion

    }


    protected void getTripSchedule()
    {
        //using (AdminDataContext dao = new AdminDataContext())
        //{
        //    var scheduleList = dao.tVisionTripSchedule.Where(p => p.CurrentUse == 'Y' && p.TripState != "Close" && Convert.ToDateTime(p.StartDate) > DateTime.Now).ToList();

        //    foreach (tVisionTripSchedule item in scheduleList)
        //    {
        //        ddlSchedule.Items.Add(new ListItem(item.VisitCountry + "(" + item.StartDate + "~" + item.EndDate + ")", item.ScheduleID.ToString()));
        //    }
        //}

        string dbName = "SqlCompassionWeb";
        object[] objSql = new object[1] { "SELECT * from tVisionTripSchedule where CurrentUse= 'Y' and TripState = 'Close' and StartDate > GETDATE()" };
        string dbType = "Text";
        DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, null, null);
        List<tVisionTripSchedule> scheduleList = ds.Tables[0].DataTableToList<tVisionTripSchedule>();

        foreach (tVisionTripSchedule item in scheduleList)
        {
            ddlSchedule.Items.Add(new ListItem(item.VisitCountry + "(" + item.StartDate + "~" + item.EndDate + ")", item.ScheduleID.ToString()));
        }


    }
    protected int getCommitmentCount(string sponserid)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        int iCommitmentCount = 0;

        var objSql = new object[1] { "  SELECT COUNT(*) AS CommitmentCount " +
         "  FROM tCommitmentMaster ComM WITH (NOLOCK) " +
         "  LEFT OUTER JOIN tSponsorMaster SM WITH (NOLOCK) ON ComM.SponsorID = SM.SponsorID " +
         "  LEFT OUTER JOIN tChildMaster CM WITH (NOLOCK) ON ComM.ChildMasterID = CM.ChildMasterID " +
         "  WHERE ComM.SponsorID = '" + sponserid + "' " +
         "  AND ComM.SponsorItemEng IN ('DS', 'LS') " +
         "  AND (ComM.StopDate IS NULL OR ComM.StopDate > GETDATE()) " };

        DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

        if (ds2 == null || ds2.Tables.Count == 0 || ds2.Tables[0].Rows.Count == 0)
            iCommitmentCount = 0;
        else
            iCommitmentCount = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());

        return iCommitmentCount;
    }

    protected string getGroupType(string sponserid)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        string grouptype = "";
        
        //using (FrontDataContext dao = new FrontDataContext())
        //{
            var userInfo = new UserInfo();

            #region Compass - tSponsorGroup : GroupType 가져오기 
            Object[] objSql = new object[1] { "VT_WebInfoInquiry" };
            Object[] objParam = new object[] { "DIVIS", "ConID" };
            Object[] objValue = new object[] { "GroupInfo", sponserid };

            var list = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
            if (list.Rows.Count > 0)
            {
                grouptype = list.Rows[0]["GroupType"].ToString();
            }
            #endregion
        //}
        return grouptype;
    }

    protected void getSponChildList(string sponserid, int applyid)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        chkChildList.Items.Clear();
        //using (AdminDataContext dao = new AdminDataContext())
        //{
            //후원어린이 정보
            Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId", "childKey", "orderby", "checkLetter" };
            Object[] objValue = new object[] { 1, 10000, sponserid, "", "", 0 };
            Object[] objSql = new object[] { "sp_web_mychild_list_f" };
            DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];
             
            foreach (DataRow dr in dt.Rows)
            {
                chkChildList.Items.Add(new ListItem(dr["ChildKey"].ToString() + "" + dr["Name"].ToString(), dr["ChildKey"].ToString()));
            } 
        //}
    } 

    protected void btn_save_ServerClick(object sender, EventArgs e)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        string dbName = "SqlCompassionWeb";
        string dbType_T = "Text";

        var apply_arg = new tVisionTripApply()
        {
            ApplyType = "Plan",
            ScheduleID = ddlSchedule.SelectedValue == "" ? 0 : Convert.ToInt16(ddlSchedule.SelectedValue),
            SponsorID = hdnSponsorID.Value,
            SponsorName = txtUserName.Text,
            SponsorNameEng = txtUserNameEng.Text,
            Tel = txtTel.Text,
            Email = txtEmail.Text,
            Address1 = (addr1.Value + "$" + ddlHouseCountry.SelectedValue),
            Address2 = addr2.Value,
            ZipCode = zipcode.Value,
            LocationType = addr_domestic.Checked ? "국내" : "해외",
             
            ReligionType = ddlReligion.SelectedValue,
            ChurchName = txtChurch.Text,
            EmergencyContactName = txtEmergencyContactName.Text,
            EmergencyContactTel = txtEmergencyContactTel.Text,
            EmergencyContactRelation = txtEmergencyContactRelation.Text,
            ChildMeetYN = ddlChildMeetYN.SelectedValue == "Y" ? 'Y' : 'N',
            ApplyDate = DateTime.Now,
            GroupType = getGroupType(hdnSponsorID.Value),
            CommitmentCount = getCommitmentCount(hdnSponsorID.Value), 

            RegisterDate = DateTime.Now,
            RegisterID = AdminLoginSession.GetCookie(Context).identifier.ToString(),
            RegisterName = AdminLoginSession.GetCookie(Context).name
        };
        var detail_arg = new tVisionTripPlanDetail() 
        {
            ScheduleAgreeYN = ddlScheduleCheck.SelectedValue == "Y" ? 'Y' : 'N',
            //Nation = ddlNation.SelectedValue != "" ? Convert.ToChar(ddlNation.SelectedValue) : Convert.ToChar(null),
            //EnglishLevel = Convert.ToChar(ddlEnglishLevel.SelectedValue),
            //VisionTripHistory = Convert.ToChar(ddlVTHistory.SelectedValue),
            Job = txtJob.Text,
            //MilitaryYN = Convert.ToChar(ddlMilitary.SelectedValue),
            //RoomType = Convert.ToChar(ddlRoomType.SelectedValue),
            RoomDetail = txtRoomDetail.Text,
            //CashReceiptType = Convert.ToChar(ddlCashReceiptType.SelectedValue),
            CashReceiptName = txtCashReceiptName.Text,
            CashReceiptTel = txtCashReceiptTel.Text,
            CashReceiptRelation = txtCashReceiptRelation.Text,
            ApplyState = 'A'
        };
        if (ddlNation.SelectedValue != "") detail_arg.Nation = Convert.ToChar(ddlNation.SelectedValue);
        if (ddlEnglishLevel.SelectedValue != "") detail_arg.EnglishLevel = Convert.ToChar(ddlEnglishLevel.SelectedValue);
        if (ddlVTHistory.SelectedValue != "") detail_arg.VisionTripHistory = Convert.ToChar(ddlVTHistory.SelectedValue);
        if (ddlMilitary.SelectedValue != "") detail_arg.MilitaryYN = Convert.ToChar(ddlMilitary.SelectedValue);
        if (ddlRoomType.SelectedValue != "") detail_arg.RoomType = Convert.ToChar(ddlRoomType.SelectedValue); 
        if (ddlCashReceiptType.SelectedValue != "") detail_arg.CashReceiptType = Convert.ToChar(ddlCashReceiptType.SelectedValue);
          
        //LINE:294
        using (FrontDataContext dao = new FrontDataContext())
        { 
            //var schedule = dao.tVisionTripSchedule.Where(p => p.ScheduleID == apply_arg.ScheduleID).FirstOrDefault();

            object[] objSql_schedule = new object[1] { "SELECT * from tVisionTripSchedule where ScheduleID = '"+ apply_arg.ScheduleID + "'" };
            string dbType = "Text";
            DataSet dsSchedule = _www6Service.NTx_ExecuteQuery(dbName, objSql_schedule, dbType, null, null);
            List<tVisionTripSchedule> schedule = dsSchedule.Tables[0].DataTableToList<tVisionTripSchedule>();

            
            var vtApplyList = new List<tVisionTripApply>();
            vtApplyList.Add(new tVisionTripApply()
            {
                ApplyType = apply_arg.ApplyType,
                ScheduleID = apply_arg.ScheduleID,
                SponsorID = apply_arg.SponsorID,
                SponsorName = apply_arg.SponsorName,
                SponsorNameEng = apply_arg.SponsorNameEng,
                Tel = apply_arg.Tel.Encrypt(),
                Email = apply_arg.Email.Encrypt(),
                LocationType = apply_arg.LocationType,
                Address1 = apply_arg.Address1.Encrypt(),
                Address2 = apply_arg.Address2.Encrypt(),
                ZipCode = apply_arg.ZipCode.Encrypt(),
                ChangeYN = 'N',
                ReligionType = apply_arg.ReligionType,
                ChurchName = apply_arg.ChurchName,
                EmergencyContactName = apply_arg.EmergencyContactName,
                EmergencyContactTel = apply_arg.EmergencyContactTel,
                EmergencyContactRelation = apply_arg.EmergencyContactRelation,
                ChildMeetYN = apply_arg.ChildMeetYN,
                ApplyDate = apply_arg.ApplyDate,
                CompassYN = 'N',
                GroupType = apply_arg.GroupType,
                CommitmentCount = apply_arg.CommitmentCount,
                CurrentUse = 'Y',
                RegisterID = apply_arg.RegisterID,
                RegisterName = apply_arg.RegisterName,
                RegisterDate = apply_arg.RegisterDate
            });

            //dao.tVisionTripApply.InsertAllOnSubmit(vtApplyList);
            //dao.SubmitChanges();

            object[] objSql_insertTripApply = new object[1] { "INSERT into tVisionTripApply (ApplyType, ScheduleID, SponsorID, SponsorName, SponsorNameEng, Tel, Email, LocationType, Address1, Address2, ZipCode, ChangeYN, ReligionType, ChurchName, EmergencyContactName, EmergencyContactTel, EmergencyContactRelation, ChildMeetYN, ApplyDate, CompassYN, GroupType, CommitmentCount, CurrentUse, RegisterID, RegisterName, RegisterDate) " 
                                                     + "values ('"+ vtApplyList[0].ApplyType + "', '" + vtApplyList[0].ScheduleID + "', '" + vtApplyList[0].SponsorID + "', '" + vtApplyList[0].SponsorName + "', '" + vtApplyList[0].SponsorNameEng + "', '" + vtApplyList[0].Tel + "', '" + vtApplyList[0].Email + "', '" + vtApplyList[0].LocationType + "', '" + vtApplyList[0].Address1 + "', '" + vtApplyList[0].Address2 + "', '" + vtApplyList[0].ZipCode + "', '" + vtApplyList[0].ChangeYN + "', '" + vtApplyList[0].ReligionType + "', '" + vtApplyList[0].ChurchName + "', '" + vtApplyList[0].EmergencyContactName + "', '" + vtApplyList[0].EmergencyContactTel + "', '" + vtApplyList[0].EmergencyContactRelation + "', '" + vtApplyList[0].ChildMeetYN + "', '" + vtApplyList[0].ApplyDate + "', '" + vtApplyList[0].CompassYN + "', '" + vtApplyList[0].GroupType + "', '" + vtApplyList[0].CommitmentCount + "', '" + vtApplyList[0].CurrentUse + "', '" + vtApplyList[0].RegisterID + "', '" + vtApplyList[0].RegisterName + "', '" + vtApplyList[0].RegisterDate +"')" };
            DataSet dsTripApply = _www6Service.NTx_ExecuteQuery(dbName, objSql_insertTripApply, dbType, null, null);
            List<tVisionTripApply> TripApplyList = dsTripApply.Tables[0].DataTableToList<tVisionTripApply>();
            
            int iApplyID = 0;
            iApplyID = vtApplyList[0].ApplyID;

            try
            {
                if (iApplyID > 0)
                {
                    var vtPlanDetail = new tVisionTripPlanDetail();
                    vtPlanDetail.ApplyID = iApplyID;
                    vtPlanDetail.ScheduleAgreeYN = detail_arg.ScheduleAgreeYN;
                    if (detail_arg.Nation != null)
                        vtPlanDetail.Nation = detail_arg.Nation;
                    if (detail_arg.EnglishLevel != null)
                        vtPlanDetail.EnglishLevel = detail_arg.EnglishLevel;
                    if (detail_arg.VisionTripHistory != null)
                        vtPlanDetail.VisionTripHistory = detail_arg.VisionTripHistory;
                    vtPlanDetail.Job = detail_arg.Job;
                    if (detail_arg.MilitaryYN != null)
                        vtPlanDetail.MilitaryYN = detail_arg.MilitaryYN;
                    if (detail_arg.RoomType != null)
                        vtPlanDetail.RoomType = detail_arg.RoomType;
                    vtPlanDetail.RoomDetail = detail_arg.RoomDetail;
                    if (detail_arg.CashReceiptType != null)
                        vtPlanDetail.CashReceiptType = detail_arg.CashReceiptType;
                    vtPlanDetail.CashReceiptName = detail_arg.CashReceiptName;
                    vtPlanDetail.CashReceiptTel = detail_arg.CashReceiptTel;
                    vtPlanDetail.CashReceiptRelation = detail_arg.CashReceiptRelation;
                    vtPlanDetail.ApplyState = 'A';
                    vtPlanDetail.RequestCost = schedule[0].RequestCost != "" ? Convert.ToDecimal(schedule[0].RequestCost) : 0;


                    //dao.tVisionTripPlanDetail.InsertOnSubmit(vtPlanDetail);

                    object[] objSql_insertTripPlan = new object[1] { "INSERT into tVisionTripPlanDetail (ApplyID, ScheduleAgreeYN, Nation, EnglishLevel, VisionTripHistory, Job, MilitaryYN, RoomType, RoomDetail, CashReceiptType, CashReceiptName, CashReceiptTel, CashReceiptRelation, ApplyState, RequestCost) "
                                                                   + "values ('"+ vtPlanDetail.ApplyID + "', '" + vtPlanDetail.ScheduleAgreeYN + "', '" + vtPlanDetail.Nation + "', '" + vtPlanDetail.EnglishLevel + "', '" + vtPlanDetail.VisionTripHistory + "', '" + vtPlanDetail.Job + "', '" + vtPlanDetail.MilitaryYN + "', '" + vtPlanDetail.RoomType + "', '" + vtPlanDetail.RoomDetail + "', '" + vtPlanDetail.CashReceiptType + "', '" + vtPlanDetail.CashReceiptName + "', '" + vtPlanDetail.CashReceiptTel + "', '" + vtPlanDetail.CashReceiptRelation + "', '" + vtPlanDetail.ApplyState + "', '" +vtPlanDetail.RequestCost + "')" };
                    DataSet dsTripPlan = _www6Service.NTx_ExecuteQuery(dbName, objSql_insertTripPlan, dbType, null, null);
                    List<tVisionTripPlanDetail> TripPlanList = dsTripPlan.Tables[0].DataTableToList<tVisionTripPlanDetail>();


                    #region 후원어린이 선택
                    var cmList = new List<tVisionTripChildMeet>();

                    foreach (ListItem item in chkChildList.Items)
                    {
                        var ChildKey = item.Value;
                        if (item.Selected)
                        {
                            var ChildName = item.Text.Replace(item.Value, "");
                            cmList.Add(new tVisionTripChildMeet()
                            {
                                ApplyID = iApplyID,
                                ChildKey = item.Value,
                                ChildMeetType = 'S',
                                ChildName = ChildName,
                                CurrentUse = 'Y',
                                RegisterDate = DateTime.Now,
                                RegisterID = AdminLoginSession.GetCookie(Context).identifier.ToString(),
                                RegisterName = AdminLoginSession.GetCookie(Context).name
                            });
                        }
                    }

                    string addChildText = hdnAddText.Value;
                    foreach (string chkey in addChildText.Split('^'))
                    {
                        if (chkey.Trim() != "")
                        {
                            cmList.Add(new tVisionTripChildMeet()
                            {
                                ApplyID = iApplyID,
                                ChildKey = chkey,
                                ChildMeetType = 'I',
                                ChildName = "",
                                CurrentUse = 'Y',
                                RegisterDate = DateTime.Now,
                                RegisterID = AdminLoginSession.GetCookie(Context).identifier.ToString(),
                                RegisterName = AdminLoginSession.GetCookie(Context).name
                            });
                        }
                    }
                    #endregion


                    //dao.tVisionTripChildMeet.InsertAllOnSubmit(cmList);
                    //dao.SubmitChanges();


                    foreach (tVisionTripChildMeet insert in cmList)
                    {
                        object[] objSql_cmList = new object[1] { "INSERT into tVisionTripChildMeet(ApplyID, ChildKey, ChildMeetType, ChildName, CurrentUse, RegisterDate, RegisterID, RegisterName) "
                                                        +"values('" + iApplyID + "', '"+ insert.ChildKey + "', '" + insert.ChildMeetType + "', '" + insert.ChildName + "', '" + insert.CurrentUse +"', GETDATE(), '"+ insert.RegisterID + "', '"+ insert.RegisterName + "' )" };
                        DataSet cmListDS = _www6Service.NTx_ExecuteQuery(dbName, objSql_cmList, dbType_T, null, null);
                        List<tVisionTripChildMeet> tVisionTripChildMeetList = cmListDS.Tables[0].DataTableToList<tVisionTripChildMeet>();
                    }

                    action.Value = "close";
                    message.Value = "등록되었습니다.";
                    //Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.opener.location.reload();self.close();");
                }
            }
            catch (Exception ex)
            {
                //using (FrontDataContext daoDel = new FrontDataContext())
                //{
                //    //   iApplyID
                //    var entity = daoDel.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                //    daoDel.tVisionTripApply.DeleteOnSubmit(entity);
                //    daoDel.SubmitChanges();
                //}

                object[] objSql_Del = new object[1] { "DELETE from tVisionTripApply where ApplyID =" + Convert.ToInt32(iApplyID) + " )" };
                DataSet DelDS = _www6Service.NTx_ExecuteQuery(dbName, objSql_Del, dbType_T, null, null);
                List<tVisionTripApply> DeltVisionTripApplyList = DelDS.Tables[0].DataTableToList<tVisionTripApply>();

                ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                message.Value = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다."; 
            } 
        }
    }
}