﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using CommonLib;
using System.Data;

public partial class mgt_participation_visiontrip_apply_individual_message_result : AdminBasePage {

    CommonLib.WWW6Service.SoaHelperSoap _www6Service;
    protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
        base.LoadComplete += new EventHandler(list_LoadComplete);


        v_admin_auth auth = base.GetPageAuth(); 

		//ids = Request["r_id"];  
    }

    protected override void GetList(int page)
    {
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        //using (AdminDataContext dao = new AdminDataContext())
        //{
        //    var list = dao.sp_tVisionTripAdminSMSResult(page, paging.RowsPerPage, "PlanAdmin").ToList();
        //    var total = 0;

        //    if (list.Count > 0)
        //        total = list[0].total.Value;

        //    lbTotal.Text = total.ToString();

        //    paging.CurrentPage = page;
        //    paging.Calculate(total);
        //    repeater.DataSource = list;
        //    repeater.DataBind();

        //}
        try
        {
            string dbName = "SqlCompassionWeb";
            object[] objSql = new object[1] { "sp_tVisionTripAdminSMSResult" };
            string dbType = "SP";
            object[] objParam = new object[3] { "page", "rowsPerPage", "ApplyType" };
            object[] objValue = new object[3] { page, paging.RowsPerPage, "PlanAdmin" };
            DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, objParam, objValue);
            List<sp_tVisionTripAdminSMSResultResult> list = ds.Tables[0].DataTableToList<sp_tVisionTripAdminSMSResultResult>();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;
            
            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }
        catch (Exception ex)
        {
            action.Value = "fail";
            message.Value = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다./n"+ ex.Message; ;

        }


    }
    protected override void OnAfterPostBack() {
		base.OnAfterPostBack(); 
	}


    protected void ListBound(object sender, RepeaterItemEventArgs e)
    { 
        if (repeater != null)
        {
            if (e.Item.ItemType != ListItemType.Footer)
            {
                sp_tVisionTripAdminSMSResultResult entity = e.Item.DataItem as sp_tVisionTripAdminSMSResultResult;
                ((Literal)e.Item.FindControl("lbIdx")).Text = (e.Item.ItemIndex +1 ).ToString();
                ((HiddenField)e.Item.FindControl("hdnGroupNo")).Value = entity.GroupNo;
            }
        } 
    }

    protected void btnDelete_ServerClick(object sender, EventArgs e)
    {
        foreach (RepeaterItem ri in repeater.Items)
        { 
            CheckBox item_check = (CheckBox)ri.FindControl("chkItem");
            HiddenField hdnGroupNo = (HiddenField)ri.FindControl("hdnGroupNo");
            if (item_check.Checked)
            {

                try
                {
                    //using (FrontDataContext dao = new FrontDataContext())
                    //{
                    //    var msg = dao.tVisionTripMessage.Where(p => p.CurrentUse == 'Y' && p.GroupNo == hdnGroupNo.Value).ToList();
                    //    if (msg.Count > 0)
                    //    {
                    //        foreach (tVisionTripMessage lst in msg)
                    //        {
                    //            lst.CurrentUse = 'N';
                    //            lst.ModifyDate = DateTime.Now;
                    //            lst.ModifyID = AdminLoginSession.GetCookie(Context).identifier.ToString();
                    //            lst.ModifyName = AdminLoginSession.GetCookie(Context).name;
                    //        }
                    //    }
                    //    dao.SubmitChanges();

                    string dbName = "SqlCompassionWeb";
                    object[] objSql = new object[1] { "UPDATE tVisionTripMessage set CurrentUse = 'N', ModifyDate=GETDATE()"
                                                             + ", ModifyID = '" + AdminLoginSession.GetCookie(Context).identifier.ToString()
                                                             + "', ModifyName = '" + AdminLoginSession.GetCookie(Context).name + "' where CurrentUse = 'Y' and GroupNo='" + hdnGroupNo.Value + "'" };
                    string dbType = "Text";
                    DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, null, null);

                   
                    action.Value = "success";
                    message.Value = "삭제되었습니다.";
                }
                catch (Exception)
                {
                    action.Value = "fail";
                    message.Value = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
                }
            }
        }
        if (action.Value == "success")
        {
            GetList(1);
        }
    }
}