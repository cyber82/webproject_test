﻿

function jusoCallback(zipNo, addr1, addr2, jibun) {
    // 실제 저장 데이타
    $("#addr1").val(addr1 + "//" + jibun);
    $("#addr2").val(addr2); 
    // 화면에 표시
    $("#zipcode").val(zipNo);
     

    $("#addr_road").text("[도로명주소] (" + zipNo + ") " + addr1 + " " + addr2);
    $("#addr_jibun").text("[지번주소] (" + zipNo + ") " + jibun + " " + addr2);
     
    $("#addr_domestic_zipcode").val($("#zipcode").val());
    $("#addr_domestic_addr1").val($("#addr1").val());
    $("#addr_domestic_addr2").val($("#addr2").val());
};
function cert_setDomain() {
    //return;
    //if (location.hostname.startsWith("auth")) return;
    if (location.hostname.indexOf("compassionko.org") > -1)
        document.domain = "compassionko.org";
    else if (location.hostname.indexOf("compassionkr.com") > -1)
        document.domain = "compassionkr.com";
    else
        document.domain = "compassion.or.kr";
}

function checkAcceptterms(chkID) { 
    $("#hdnAgree" + chkID).val("1");
    $("[data-id=check_agree]").hide();
    $(".sp_agree" + chkID).removeClass("agree_check");
}
function agree_checkbox(chkID) {
    if ($("#hdnAgree" + chkID).val() == "") {
        scrollTo("#hdnAgree" + chkID);
        $("[data-id=check_agree]").html("약관전문보기를 하셔야 동의 체크 가능합니다.").addClass("guide_comment2").show();
        $("#chkAgree" + chkID).prop({ "checked": false });
        $(".sp_agree" + chkID).addClass("agree_check");
        return false;
    }
    else
        $(".sp_agree" + chkID).removeClass("agree_check");
}

(function () {

    var app = angular.module('cps.page', ['ui.multiselect']); 

    app.directive('onFinishRender', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                //if (scope.$first === true) {
                //    window.alert('First thing about to render');
                //}
                if (scope.$last === true) {
                    $timeout(function () { 
                        scope.$emit(attr.onFinishRender);
                    },1);
                }
            }
        };
    });

    app.controller('defaultCtrl', function ($scope, $http, popup, $location, paramService) {

        if($("#action").val() == "UserSuccess"){ 

        $scope.name = 'Child';
        $scope.mychild = [{ id: 1, name: '' }, { id: 2, name: '' }, { id: 1, name: '' }];
        $scope.selectedChild = [];

        $http.get("/@mgt/api/visiontrip_apply.ashx?t=childlist", { params: { sponserid: $("#hdnSponsorID").val() } }).success(function (result) {
            $scope.list = []; 

            $.each(result.data, function () {
                $scope.list.push({ id: this.childkey, name: this.name });
            });
            $scope.name = 'World'; 
            $scope.mychild = $scope.list;

        });
        }
        //후원어린이선택
        $scope.child_inputs = [];
        $scope.addInput = function () {
            if ($scope.child_inputs.length < 3) { //최대 3개 까지만 가능
                $scope.child_inputs.push({ id: '', name: '' });
            }
        } 
        $scope.removeInput = function (index) {
            $scope.child_inputs.splice(index, 1);
        }
        $scope.$on('ngRepeatFinished_Child', function (ngRepeatFinishedEvent) { 
            //$(".childinput").setHangulBan();
        });
        //end 후원어린이선택

        //동반인 
        $scope.companion_inputs = [{ id: '', name_kor: '', name_eng: '', birth: '', gender: '' }];
        $scope.addCompanionInput = function () {
            if ($scope.companion_inputs.length < 7) { //최대 7개 까지만 가능
                $scope.companion_inputs.push({ id: '', name_kor: '', name_eng: '', birth: '', gender: '' });
            }
        }
        $scope.removeCompanionInput = function (index) {
            $scope.companion_inputs.splice(index, 1);
        }
        //end 동반인 

        //기타 파일
        $scope.input_fils = [{ key: 0, filename: '', filepath:'' }];
        var newItemNo = 0;
        $scope.addInputFiles = function ($index) {
            newItemNo++;
            //newItemNo = $scope.input_fils.length + 1; 
            $scope.input_fils.push({ key: newItemNo, filename: '', filepath: '' });
        }

        $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
            if ($scope.input_fils.length > 0) {
                var idx = $scope.input_fils[$scope.input_fils.length - 1].key;
                $scope.setFileUploader(idx);
            }
        });
        $scope.setFileUploader = function (idx) {
            var uploader_passport = $page.attachUploader("btn_etcfile" + idx);
            uploader_passport._settings.data.fileDir = $("#hdnFileRoot").val() + "/MGT/" + $("#hdnSponsorID").val() + "/";
            uploader_passport._settings.data.rename = "y"; 
            uploader_passport._settings.data.limit = 2048; 
        };

        $scope.removeInputFiles = function (index) {
            $scope.input_fils.splice(index, 1);
        }
        //end 기타파일

        //주소 검색 팝업
        $scope.addrpopup = function ($event) {
            $event.preventDefault();
            cert_setDomain();
            var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");
        } 

        //약관 popup
        $scope.agreementModal = {
            instance: null,
            banks: [],
            detail_list: [],

            init: function () { 
                popup.init($scope, "/participation/visiontrip/form/term", function (agreementModal) {
                    $scope.agreementModal.instance = agreementModal;
                    //$scope.tripnoticeModal.show();
                }, { top: 0, iscroll: true });
            },

            show: function ($event, type, title) {
                //console.log(orderNo)
                $event.preventDefault();
                if (!$scope.agreementModal.instance)
                    return;

                $scope.agreementModal.instance.show();

                $scope.agreementModal.title = title;

                $scope.agreementModal.type1 = type ==  1;
                $scope.agreementModal.type2 = type == 2;
                $scope.agreementModal.type3 = type == 3;
                $scope.agreementModal.type4 = type == 4;
                $scope.agreementModal.type5 = type == 5;
                $scope.agreementModal.type6 = type == 6;
                $scope.agreementModal.type7 = type == 7;
                //$scope.agreementModal.type8 = type == 8;

                $scope.agreementModal.typei1 = type == 11;
                $scope.agreementModal.typei2 = type == 12;
                $scope.agreementModal.typei3 = type == 13;
                $scope.agreementModal.typei4 = type == 14;
                $scope.agreementModal.typei5 = type == 15;
                $scope.agreementModal.typei6 = type == 16; 

            },
            close: function ($event) {
                $event.preventDefault();
                if (!$scope.agreementModal.instance)
                    return;
                $scope.agreementModal.instance.hide();
            }
        };

        $scope.agreementModal.init();

          

        //일반트립 신청서 저장
        $scope.applysubmit = function () { 
            if (!validateForm([
              { id: "#hdnSponsorID", msg: "사용자 조회해주세요." },
              { id: "#txtUserName", msg: "성함을 입력하세요." },
              { id: "#txtTel", msg: "휴대번호를 입력하세요." },
              { id: "#addr1", msg: "주소를 입력하세요." },
                //{ id: "#addr2", msg: "상세 주소를 입력하세요." },
              { id: "#zipcode", msg: "우편번호를 입력하세요." },
              { id: "#ddlSchedule", msg: "비전트립 일정을 선택하세요." },
              { id: "#ddlScheduleCheck", msg: "트립 일정과 방문 국가를 확인 여부를 선택하세요." },
              { id: "#ddlNation", msg: "국적을 선택하세요." },
              { id: "#ddlReligion", msg: "종교를 선택하세요." },
              { id: "#ddlEnglishLevel", msg: "영어회화능력을 선택하세요." },
              { id: "#ddlVTHistory", msg: "비전트립 참가이력을 선택하세요." },
              { id: "#ddlMilitary", msg: "병역여부를 선택하세요." },
              { id: "#ddlRoomType", msg: "방배정 구분을 선택하세요." },
              { id: "#ddlChildMeetYn", msg: "후원어린이 만남 여부를 선택하세요." },
              { id: "#ddlCashReceiptType", msg: "현금영수증 발급 구분을 선택하세요." }
            ])) {
                return false;
            }  

            //if (!$page.onSubmit())
            //    return false;

            var ScheduleID = $("#ddlSchedule").val();
            var ScheduleAgreeYN = $("#ddlScheduleCheck").val(); //스케줄확인여부
            var Nation = $("#ddlNation").val();// 국적
            var EnglishLevel = $("#ddlEnglishLevel").val(); //영어회화능력
            var VisionTripHistory = $("#ddlVTHistory").val();//비전트립 참가이력
            var Job = $("#txtJob").val();//직업
            var Military = $("#ddlMilitary").val(); //병역
            var RoomType = $("#ddlRoomType").val(); //방배정
            var RoomDetail = $("#txtRoomDetail").val(); //방배정상세
            var CashReceiptType = $("#ddlCashReceiptType").val(); //현금영수증 구분
            var CashReceiptName = $("#txtCashReceiptName").val(); //현금영수증 성함
            var CashReceiptTel = $("#txtCashReceiptTel").val(); //현금영수정 번호
            var CashReceiptRelation = $("#txtCashReceiptRelation").val();//현금영수증 참가자와의 관계
            //var AcceptTerms = ($("#chkAgree1").is(":checked") ? "1" : "0") + ($("#chkAgree2").is(":checked") ? "1" : "0") +
            //            ($("#chkAgree3").is(":checked") ? "1" : "0") + ($("#chkAgree4").is(":checked") ? "1" : "0") +
            //            ($("#chkAgree5").is(":checked") ? "1" : "0") + ($("#chkAgree6").is(":checked") ? "1" : "0") +
            //            ($("#chkAgree7").is(":checked") ? "1" : "0") ; //약관동의 

            //var ApplyAgree = $("#chkAgreeFinal").is(":checked") ? "Y" : "N";  //약관동의 최종확인

            var ApplyType = "Plan"; //비전트립 구분 - 기획/개인방문
            var SponsorNameEng = $("#txtUserNameEng").val();  //신청자 영문명
            var Tel = $("#txtTel").val(); //신청자 전화번호
            var Email = $("#txtEmail").val(); //신청자 이메일
            var SponsorName = $("#txtUserName").val(); //신청자 명
            var SponsorID = $("#hdnSponsorID").val();
            var UserID = $("#hdnUserID").val();
            var GenderCode = $("#hdnGenderCode").val();
            var BirthDate = $("#hdnBirthDate").val();
            
            var Location = $("#addr_domestic").is(":checked") ? "국내" : "국외";
            var Country = $("#addr_domestic").is(":checked") ? "한국" : $("#ddlHouseCountry").val();

            var Address1 = $("#addr1").val(); //신청자 주소
            var Address2 = $("#addr2").val();
            var ZipCode = $("#zipcode").val();
            var ChangeYN = $("#ddlInfoChange").val(); //기존정보 변경 여부
            var Religion = $("#ddlReligion").val(); //종교
            var Church = $("#ddlReligion").val() == "Christian" ? $("#txtChurch").val() : ""; //교회명
            var EmergencyContactName = $("#txtEmergencyContactName").val(); //비상연락처
            var EmergencyContactTel = $("#txtEmergencyContactTel").val();
            var EmergencyContactRelation = $("#txtEmergencyContactRelation").val();
            var ChildMeetYN = $("#ddlChildMeetYn").val(); //후원어린이 만남여부

            var ChildList = [];

            for (var k in $scope.selectedChild) {
                ChildList.push($scope.selectedChild[k]);
            }
            for (var k in $scope.child_inputs) {
                ChildList.push($scope.child_inputs[k]);
            }

            var AttachFile = [];
            if ($("#path_btn_passportfile").val() != "") {
                AttachFile.push({ type: "passport", name: $("[data-id=path_btn_passportfile]").val(), path: $("#path_btn_passportfile").val() });
            }
            for (var f in $scope.input_fils) {
                if ($("#path_btn_etcfile" + $scope.input_fils[f].key).val() != "") {
                    AttachFile.push({ type: "etc", name: $("[data-id=path_btn_etcfile" + $scope.input_fils[f].key + "]").val(), path: $("#path_btn_etcfile" + $scope.input_fils[f].key).val() });
                }
            }

            var param = {
                t: "add_planapply",
                applyType: "Plan",
                sponsorID: SponsorID,
                userID: UserID,
                sponsorName: SponsorName,
                genderCode: GenderCode,
                birthDate: BirthDate,
                scheduleID: ScheduleID,
                sponsorNameEng: SponsorNameEng,
                tel: Tel,
                email: Email,
                address1: Address1,
                address2: Address2,
                zipcode: ZipCode,
                location: Location,
                country: Country,
                changeYn: ChangeYN,
                religion: Religion,
                church: Church,
                emergencycontactName: EmergencyContactName,
                emergencycontactTel: EmergencyContactTel,
                emergencycontactRelation: EmergencyContactRelation,
                childmeetYn: ChildMeetYN,
                scheduleagreeYn: ScheduleAgreeYN,
                nation: Nation,
                englishLevel: EnglishLevel,
                visiontripHistory: VisionTripHistory,
                job: Job,
                military: Military,
                roomType: RoomType,
                roomDetail: RoomDetail,
                cashreceiptType: CashReceiptType,
                cashreceiptName: CashReceiptName,
                cashreceiptTel: CashReceiptTel,
                cashreceiptRelation: CashReceiptRelation,
                //acceptTerms: AcceptTerms,
                //applyAgree: ApplyAgree,
                childList: JSON.stringify(ChildList),
                attachFile: JSON.stringify(AttachFile)
            }

            $http.post("/@mgt/api/visiontrip_apply.ashx", param).success(function (result) {
                console.log(result);
                if (result.success) {
                    console.log(result.data);
                    alert("등록되었습니다.");
                    window.opener.location.reload();
                    self.close();
                } else {
                    if (result.message == "") {
                        alert("신청 실패하였습니다. 관리자에게 문의하시기 바랍니다.");
                    } else {
                        alert(result.message);
                    }
                }
            });
        };
         
    });  
})();

var $page = {

    timer: null,

    init: function () { 



    },

    init_individual: function () { 
        
        $(".childinput").keyup(function (e) {
            $(this).val($(this).val().replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, ''));
        }); 
    },
     
    attachUploader: function (button) {
        return new AjaxUpload(button, {
            action: '/common/handler/upload',
            responseType: 'json',
            onChange: function (file) {
                var fileName = file.toLowerCase();
            },
            onSubmit: function (file, ext) {
                this.disable();
            },
            onComplete: function (file, response) {

                this.enable();

                console.log(file, response);
                if (response.success) { 
                    $("#path_" + button).val(response.name);
                    $("[data-id=path_" + button + "]").val(response.name.replace(/^.*[\\\/]/, ''));

                    console.log($("#path_" + button).val()); //filepullpath
                    console.log($("[data-id=path_" + button + "]").val());  //filename 

                } else
                    alert(response.msg);
            }
        });
    }

};