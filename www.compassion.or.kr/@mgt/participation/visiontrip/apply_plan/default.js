﻿
(function () {

    //var app = angular.module("defaultApp", []);

    var app = angular.module('cps.page', ['ui.multiselect']);

    app.directive('onFinishRender', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                //if (scope.$first === true) {
                //    window.alert('First thing about to render');
                //}
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit(attr.onFinishRender);
                    }, 1);
                }
            }
        };
    });

    app.controller("defaultCtrl", function ($scope, $http) {

        $scope.filepath = "/files/visiontrip/plan/";

        //검색조건 
        $scope.searchSelectChange = function () {
            $scope.select_tripname = [];
            console.log('tripname');
            $http.get("/@mgt/api/visiontrip.ashx?t=tripname", { params: { tripYear: $("#ddlYear option:selected").val(), tripType: $("#ddlTripType option:selected").val() } }).success(function (r) {
                console.log(r);
                if (r.success) {
                    var list = r.data;
                    $scope.select_tripname = list;
                } else {
                    alert(r.message);
                }
            });
        };
        $scope.searchSelectChange();
        $("#ddlYear, #ddlTripType").change(function () { $scope.searchSelectChange(); });

        $scope.tripYear = $("#ddlYear option:selected").val();
        $scope.tripType = $("#ddlTripType option:selected").val();
        $scope.tripID = $("#ddlTripName option:selected").val();
        //end - 검색조건 

        $scope.searchClick = function () {
            $scope.tripYear = $("#ddlYear option:selected").val();
            $scope.tripType = $("#ddlTripType option:selected").val();
            $scope.tripID = $("#ddlTripName option:selected").val();

            var param = {
                tripYear: $scope.tripYear,
                tripType: $scope.tripType,
                tripID: $scope.tripID
            };

            var exportID = $("#hdnTabInfo").val();
            switch (exportID) {
                case "1": $scope.getApplyStateList(param); break;
                case "2": $scope.getApplyInfoList(param); break;
                case "3": $scope.getParticipantList(param); break;
                case "4": $scope.getAddrList(param); break;
                case "5": $scope.getSMSList(param); break;
                case "6": $scope.getStatisticsList(param); break;
            }
        }

        $("#btnPassportDel").click(function () {
            $("#path_btn_passportfile").val("");
            $("[data-id=path_btn_passportfile]").val("");
        });

        $scope.showLoading = function () {
            $("#loading_bg, #loading_container").show();
            $(".msg").html('잠시만 기다려 주세요.');
        };
        $scope.hideLoading = function () {
            $("#loading_bg, #loading_container").hide();
            $(".msg").html('');
        };

        //grid edit 
        $scope.modifyClick = function (item) {
            item.editmode = true;
            $scope.showLoading();
            if (item.grid_type == 'apply') {
                setTimeout(function () {
                    //여권
                    var uploader_passport = $page.attachUploader("btn_passportfile_" + item.applyid);
                    uploader_passport._settings.data.fileDir = $scope.filepath + item.scheduleid + "/" + item.userid + "/";
                    uploader_passport._settings.data.limit = 2048; 
                }, 500);

                item.passportChange = false;
                $scope.passportDelete = function (item, $event) {
                    $("#path_btn_passportfile_" + item.applyid).val("");
                    $("[data-id=path_btn_passportfile_" + item.applyid + "]").val("");
                    $("#hdnDelyn_" + item.applyid).val("Y");
                    $("#hdnPassportID_" + item.applyid).val("");
                    item.passportChange = true;
                }
                $scope.passportChangeDelete = function (item, $event) {
                    $("#path_btn_passportfile_" + item.applyid).val("");
                    $("[data-id=path_btn_passportfile_" + item.applyid + "]").val("");
                }


                //기타 파일
                item.input_fils = [];//[{ key: 0, filename: '', filepath: '' }];
                item.newItemNo = 0;
                $http.get("/@mgt/api/visiontrip.ashx?t=attachlist", { params: { apply_id: item.applyid, attach_type: 'etc' } }).success(function (r) {
                    console.log(r);
                    if (r.success) {
                        var list = r.data;
                        $.each(list, function () {
                            item.input_fils.push({
                                key: item.newItemNo++, filename: this.attachname, filepath: this.attachpath, applyid: item.applyid, delyn: 'N', attachid: this.attachid, newyn: 'N'
                            });
                        });
                        //$scope.child_inputs = list;
                    } else {
                        console.log(r);
                    }
                });
                $scope.addInputFiles = function (item, $index) {
                    item.newItemNo++;
                    //newItemNo = $scope.input_fils.length + 1; 
                    item.input_fils.push({ key: item.newItemNo, filename: '', filepath: '', applyid: item.applyid, delyn: 'N', attachid: '', newyn: 'Y' });
                }

                $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
                    if (item.input_fils.length > 0) {
                        var idx = item.input_fils[item.input_fils.length - 1].key;
                        $scope.setFileUploader(idx);
                    }
                });
                $scope.setFileUploader = function (idx) {
                    console.log("btn_etcfile" + "_" + item.applyid + "_" + idx);
                    var btnid = "btn_etcfile" + "_" + item.applyid + "_" + idx;
                    var uploader_passport = $page.attachUploader(btnid);
                    uploader_passport._settings.data.fileDir = "/files/visiontrip/plan/" + item.applyid + "/" + item.userid + "/";
                    uploader_passport._settings.data.rename = "y";
                    uploader_passport._settings.data.limit = 2048;
                };

                $scope.removeInputFiles = function (item, index, num) {
                    //item.input_fils.splice(index, 1);
                    if (item.input_fils.filter(function (itm) { return itm.key === num }).length > 0)
                        item.input_fils.filter(function (itm) { return itm.key === num })[0].delyn = 'Y';
                }
                $scope.modifyInputFiles = function (item, index, num) {
                    //item.input_fils[index].delyn = 'Y';
                    if (item.input_fils.filter(function (itm) { return itm.key === num }).length > 0)
                        item.input_fils.filter(function (itm) { return itm.key === num })[0].delyn = 'Y';
                }
                //

                //end 기타파일
            }
            else if (item.grid_type == 'info') {

                //후원어린이 만남  
                item.child_inputs = [];
                $scope.addInput = function (item) {
                    if (item.child_inputs.filter(function (cmitem) { return cmitem.delyn === "N"; }).length < 3) { //최대 3개 까지만 가능
                        item.child_inputs.push({ key: item.iChildmeet, id: '', name: '', delyn: 'N', childmeetid: '', childmeettype: 'I' });
                        item.iChildmeet++;
                    }
                }
                $scope.removeInput = function (item, index, num) {
                    if (item.child_inputs.filter(function (itm) { return itm.key === num }).length > 0)
                        item.child_inputs.filter(function (itm) { return itm.key === num })[0].delyn = 'Y';
                }


                $http.get("/@mgt/api/visiontrip_apply.ashx?t=childlist", { params: { sponserid: item.sponsorid } }).success(function (result) {
                    $scope.list = [];

                    $.each(result.data, function () {
                        $scope.list.push({ id: this.childkey, name: this.name });
                    });
                    item.name = 'World';
                    item.mychild = $scope.list;
                });
                item.selectedChild = [];
                item.searchSelectChild = [];
                item.iChildmeet = 0;
                $http.get("/@mgt/api/visiontrip.ashx?t=childmeetlist", { params: { applyID: item.applyid } }).success(function (r) {
                    console.log(r);
                    if (r.success) {
                        var list = r.data;
                        $.each(list, function () {
                            if (this.childmeettype == "S") {
                                item.selectedChild.push({ id: this.childkey, name: this.childname });
                                item.searchSelectChild.push({ id: this.childkey, name: this.childname });
                            }
                            else {
                                item.child_inputs.push({ key: item.iChildmeet, id: this.childkey, name: '', delyn: 'N', childmeetid: this.childmeetid, childmeettype: 'I' });
                                item.iChildmeet++;
                            }
                        });
                    } else {
                        console.log(r);
                    }
                });
                //end 후원어린이 만남     
            }
            else if (item.grid_type == 'addr') {
                //$scope.countries = null;
                $scope.country = [];
                $http.get("/@mgt/api/visiontrip.ashx?t=country", { params: {} }).success(function (r) {
                    $scope.countries = r.data;
                });

                setTimeout(function () {
                    if (item.location == "국내") {
                        $("#pn_addr_domestic_" + item.applyid).show();
                        $("#pn_addr_overseas_" + item.applyid).hide();
                    }
                    else {
                        $("#pn_addr_domestic_" + item.applyid).hide();
                        $("#pn_addr_overseas_" + item.applyid).show();
                    }
                }, 500)

            }
              
            $scope.hideLoading();
        };

        $scope.saveClick = function (item) {
            if (item.grid_type == 'apply') {
                var applystate = $("#" + item.applyid + "_apply_state option:selected").val();
                var request_cost = $("#" + item.applyid + "_request_cost option:selected").val();
                var trip_cost = $("#" + item.applyid + "_trip_cost option:selected").val();
                var childmeet_cost = $("#" + item.applyid + "_childmeet_cost option:selected").val();
                var cancel = item.cancel_check;

                item.apply_state = $("#" + item.applyid + "_apply_state option:selected").text();
                item.apply_state_cd = applystate;
                item.request_cost_yn = request_cost;
                item.trip_cost_yn = trip_cost;
                item.childmeet_cost_yn = childmeet_cost;
                item.cancel_yn = cancel ? "Y" : "N";
                item.cancel_class = cancel ? "tr_cancel" : "";

                var attachfile = [];

                attachfile.push({
                    type: "passport"
                    , name: $("[data-id=path_btn_passportfile_" + item.applyid + "]").val()
                    , path: $("#path_btn_passportfile_" + item.applyid).val()
                    , attachid: $("#hdnPassportID_" + item.applyid).val()
                    , delyn: $("#hdnDelyn_" + item.applyid).val()
                });
                //기타파일
                for (var f in item.input_fils) {
                    if (item.input_fils[f].newyn == 'Y') {
                        if ($("#path_btn_etcfile_" + item.applyid + "_" + item.input_fils[f].key).val() != "" && item.input_fils[f].delyn == 'N') {
                            attachfile.push({
                                type: "etc"
                                , name: $("[data-id=path_btn_etcfile" + "_" + item.applyid + "_" + item.input_fils[f].key + "]").val()
                                , path: $("#path_btn_etcfile" + "_" + item.applyid + "_" + item.input_fils[f].key).val()
                                , attachid: item.input_fils[f].attachid
                                , delyn: item.input_fils[f].delyn
                            });
                        }
                    }
                    else {
                        attachfile.push({
                            type: "etc", name: item.input_fils[f].filename
                            , path: item.input_fils[f].filepath
                            , attachid: item.input_fils[f].attachid
                            , delyn: item.input_fils[f].delyn
                        });
                    }
                }
                item.attachfile = attachfile;
                item.etc_attach = attachfile.filter(function (item) {
                    return item.delyn === "N";
                }).length;
            }
            else if (item.grid_type == 'info') {
                var trip_cost = $("#info_" + item.applyid + "_trip_cost").val();
                var childmeet_cost = $("#info_" + item.applyid + "_childmeet_cost").val();

                var visiontrip_history_text = $("#info_" + item.applyid + "_visiontrip_history option:selected").text();
                var visiontrip_history_code = $("#info_" + item.applyid + "_visiontrip_history option:selected").val();

                item.trip_cost = trip_cost;
                item.childmeet_cost = childmeet_cost;
                item.visiontrip_history = visiontrip_history_text;
                item.visiontrip_history_code = visiontrip_history_code;

            }
            else if (item.grid_type == 'participant') {
                var sponsor_name_eng = $("#participant_" + item.applyid + "_sponsor_name_eng").val();
                var gendercode = $("#participant_" + item.applyid + "_gendercode").val();
                var tel = $("#participant_" + item.applyid + "_tel").val();
                var email = $("#participant_" + item.applyid + "_email").val();
                var remark = $("#participant_" + item.applyid + "_remark").val();
                var cashreceipt_name = $("#participant_" + item.applyid + "_cashreceipt_name").val();
                var cashreceipt_tel = $("#participant_" + item.applyid + "_cashreceipt_tel").val();
                var cashreceipt_relation = $("#participant_" + item.applyid + "_cashreceipt_relation").val();
                var emergencycontact_name = $("#participant_" + item.applyid + "_emergencycontact_name").val();
                var emergencycontact_tel = $("#participant_" + item.applyid + "_emergencycontact_tel").val();
                var emergencycontact_relation = $("#participant_" + item.applyid + "_emergencycontact_relation").val();
                item.sponsor_name_eng = sponsor_name_eng;
                item.gendercode = gendercode;
                item.tel = tel;
                item.email = email;
                item.remark = remark;
                item.cashreceipt_name = cashreceipt_name;
                item.cashreceipt_tel = cashreceipt_tel;
                item.cashreceipt_relation = cashreceipt_relation;
                item.emergencycontact_name = emergencycontact_name;
                item.emergencycontact_relation = emergencycontact_relation;
                item.emergencycontact_tel = emergencycontact_tel;

                if (item.gendercode == null || item.gendercode == "" ||
                    (item.gendercode != "F" && item.gendercode != "f" && item.gendercode != "M" && item.gendercode != "m")) {
                    alert("성별은 M(남자), F(여자)만 입력 할 수 있습니다.");
                    return;
                }
            }
            else if (item.grid_type == 'addr') {

                var locationtype = $("#selectlocation_" + item.applyid + " option:selected").val();
                var zipcode_domestic = $("#zipcode_" + item.applyid).val();
                var addr1_domestic = $("#addr1_" + item.applyid).val();
                var addr2_domestic = $("#addr2_" + item.applyid).val();

                var addrJibun = $("#dspAddrJibun_" + item.applyid).val();
                var addrDoro = $("#dspAddrDoro_" + item.applyid).val();

                var country_overseas = $("#selectHouseCountry_" + item.applyid + " option:selected").val();
                var zipcode_overseas = $("#addr_overseas_zipcode_" + item.applyid).val();
                var addr1_overseas = $("#addr_overseas_addr1_" + item.applyid).val();
                var addr2_overseas = $("#addr_overseas_addr2_" + item.applyid).val();

                if (locationtype == "국내") {
                    item.location = "국내";
                    item.zipcode = zipcode_domestic;
                    item.address1 = addr1_domestic;
                    item.address2 = addr2_domestic;
                    item.country = "한국";

                    item.address_doro = addrDoro;
                    item.address_jibun = addrJibun;
                    item.zipcode_domestic = zipcode_domestic;
                }
                else {
                    item.location = "국외";
                    item.zipcode = zipcode_overseas;
                    item.address1 = addr1_overseas;
                    item.address2 = addr2_overseas;
                    item.country = country_overseas;

                    item.address_overseas = addr1_overseas;
                    item.zipcode_domestic = zipcode_domestic;
                    item.addr2_overseas = addr2_overseas;
                }

            }

            $scope.update(item);
            item.editmode = false;
        };

        $scope.cancelClick = function (item) {
            item.editmode = false;
        };

        //비교
        var compareJSON = function (obj1, obj2) {
            var ret = {};
            for (var i in obj2) {
                if (!obj1.hasOwnProperty(i) || obj2[i] !== obj1[i]) {
                    ret[i] = obj2[i];
                }
            }
            return ret;
        };
        // 저장
        $scope.update = function (item) {
            $scope.showLoading();
            var ChildList = [];
            if (item.grid_type == 'info') {
                for (var k in item.child_inputs) {
                    ChildList.push(item.child_inputs[k]);
                }

                var newchild = compareJSON(item.searchSelectChild, item.selectedChild);
                var delchild = compareJSON(item.selectedChild, item.searchSelectChild);

                for (var k in newchild) {
                    ChildList.push({ key: 0, id: newchild[k].id, name: newchild[k].name, delyn: 'N', childmeetid: '', childmeettype: 'S' });
                }
                for (var k in delchild) {
                    ChildList.push({ key: 0, id: delchild[k].id, name: delchild[k].name, delyn: 'Y', childmeetid: '', childmeettype: 'S' });
                }
                //console.log(ChildList); 
            }
            $scope.updateparam = {
                t: 'update_apply',
                grid_type: item.grid_type,
                scheduleid: item.scheduleid,
                applyid: item.applyid,
                apply_state_cd: item.apply_state_cd,
                request_cost_yn: item.request_cost_yn,
                trip_cost_yn: item.trip_cost_yn,
                childmeet_cost_yn: item.childmeet_cost_yn,
                cancel_yn: item.cancel_yn,
                attachlist: JSON.stringify(item.attachfile),
                //info
                trip_cost: item.trip_cost,
                childmeet_cost: item.childmeet_cost,
                childmeet: JSON.stringify(ChildList),
                visiontrip_history_code: item.visiontrip_history_code,
                //participant
                sponsor_name_eng: item.sponsor_name_eng,
                gendercode: item.gendercode,
                tel: item.tel,
                email: item.email,
                remark: item.remark,
                cashreceipt_name: item.cashreceipt_name,
                cashreceipt_tel: item.cashreceipt_tel,
                cashreceipt_relation: item.cashreceipt_relation,
                emergencycontact_name: item.emergencycontact_name,
                emergencycontact_relation: item.emergencycontact_relation,
                emergencycontact_tel: item.emergencycontact_tel,

                //addr
                location: item.location,
                zipcode: item.zipcode,
                address1: item.address1,
                address2: item.address2,
                country: item.country
            };

            $http.post("/@mgt/api/visiontrip.ashx", $scope.updateparam).success(function (r) {
                //hideWaiting();
                if (r.success) {
                    if (item.grid_type == "apply") {
                        $http.get("/@mgt/api/visiontrip.ashx?t=attachlist", { params: { apply_id: item.applyid, attach_type: 'passport' } }).success(function (r) {
                            console.log(r);
                            if (r.success) {
                                var list = r.data;
                                if (list.length > 0) {
                                    item.passport = this.attachid;
                                    item.passport_path = this.attachpath;
                                    item.passport_name = this.attachname;
                                }
                                //$scope.child_inputs = list;
                            } else {
                                console.log(r);
                            }
                        });
                    } else if (item.grid_type == "info") {
                        $http.get("/@mgt/api/visiontrip.ashx?t=childmeetlist", { params: { applyID: item.applyid } }).success(function (r) {
                            if (r.success) {
                                var list = r.data;
                                item.child_key = "";
                                $.each(list, function () {
                                    item.child_key += this.childkey + ",";
                                });
                            } else {
                                console.log(r);
                            }
                        });
                    }
                    $scope.hideLoading();
                    alert("저장되었습니다.");
                }
                else {
                    $scope.hideLoading();
                    alert(r.message);
                }
            });
            $scope.hideLoading();
        }


        //엑셀다운로드
        $scope.exportData = function () {
            var exportID = $("#hdnTabInfo").val(); 

            var title = "";
            switch (exportID) {
                case "1": title = "신청현황"; break;
                case "2": title = "신청정보"; break;
                case "3": title = "참가자정보"; break;
                case "4": title = "참가자주소"; break;
                case "5": title = ""; break;
                case "6": title = "통계"; break;
            }
            var dt = new Date();
            var day = dt.getDate();
            var month = dt.getMonth() + 1;
            var year = dt.getFullYear();
            var hour = dt.getHours();
            var mins = dt.getMinutes();
            var postfix = year + "" + month + "" + day + "" + hour + "" + mins;
            ////creating a temporary HTML link element (they support setting file names)

            //var a = document.createElement('a');
            //    //getting data from our div that contains the HTML table
            //var data_type = 'data:application/vnd.ms-excel;base64,';
            //    var table_div = document.getElementById("tableExcel"+exportID);
            //var table_html = table_div.outerHTML.replace(/ /g, '%20');
            //    a.href = data_type + ', ' +table_html;
            //        //setting the file name
            //        a.download = '비전트립_' +title + "_" +postfix + '.xls';
            //    //triggering the function
            //a.click();

            var table_div = document.getElementById("tableExcel" + exportID);
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            //setting the file name 
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(table_div.outerHTML);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, '비전트립_' + title + "_" + postfix + '.xls');
                return (sa);
            }
            else {
                var a = document.createElement('a');
                var data_type = 'data:application/vnd.ms-excel;base64,';
                a.href = data_type + ', ' + table_html;
                a.download = '비전트립_' + title + "_" + postfix + '.xls';
                a.click();
            }
        };

        //컴파스 전송
        $scope.compassSend = function () {
            $scope.compassList = $scope.applystate_list.filter(function (item) {
                return item.rowselect === true;
            });
            if ($scope.compassList.length > 0) {
                $scope.showLoading();
                var selectID = "";
                for (var k in $scope.compassList) {
                    selectID += $scope.compassList[k].applyid + ",";
                }

                $http.get("/@mgt/api/visiontrip.ashx?t=compass_plan", { params: { applyid_list: selectID } }).success(function (r) {
                    if (r.success) {
                        $scope.hideLoading();
                        alert("업로드되었습니다.");
                        $scope.getApplyStateList();
                    }
                    else {
                        $scope.hideLoading();
                        alert(r.message);
                    }
                });
            }
            else {
                alert("업로드할 항목을 선택해주세요.");
            }
        };

        //비전트립 신청 추가
        $scope.createPopup = function () {
            var tripID = $("#ddlTripName option:selected").val();
            var popup = window.open("/@mgt/participation/visiontrip/apply_plan/apply?id=" + tripID, "planNew", "width=" + screen.width + ",height=" + screen.height + ",scrollbars=yes,fullscreen=yes");

            popup.moveTo(0, 0);
            popup.resizeTo(screen.width, screen.height);
            return false;
        }


        //신청현황
        $scope.select_applystate = [{ codevalue: 'A', codename: '신청중' }, { codevalue: 'C', codename: '신청완료' }];
        $scope.select_paymentYN = [{ codevalue: 'Y', codename: '납부' }, { codevalue: 'N', codename: '미납' }];

        $scope.a_total = 0;
        $scope.a_page = 1;
        $scope.a_rowsPerPage = 50;

        $scope.a_params = {
            page: $scope.a_page,
            rowsPerPage: $scope.a_rowsPerPage,
            tripYear: $scope.tripYear,
            tripType: $scope.tripType,
            tripID: $scope.tripID
        };

        $scope.getApplyStateList = function (params) {
            $scope.showLoading();
            $scope.a_params = $.extend($scope.a_params, params);
            console.log('applystate_list_p');

            var requestSubType = $('#hdnRequestSubType').val();

            $http.get("/@mgt/api/visiontrip.ashx?t=applyplay_list&list=state&requestSubType=" + requestSubType, { params: $scope.a_params }).success(function (r) {
                console.log(r);
                if (r.success) {
                    var list = r.data;

                    var k = 0;
                    if (list.length > 0) {
                        $.each(list, function () {
                            this.grid_type = 'apply';

                            this.cancel_check = this.cancel_yn == "Y" ? true : false;
                            this.rowselect = false;
                            this.cancel_class = this.cancel_check ? "tr_cancel" : "";
                            this.passportChange = false;
                        });

                    }
                    $scope.applystate_list = list;
                    $scope.a_total = r.data.length > 0 ? r.data[0].total : 0;
                    $scope.hideLoading();
                } else {
                    $scope.hideLoading();
                    alert(r.message);
                }
            });

        }
        //첫 로딩
        $scope.getApplyStateList();

        $scope.attachPopup = function (item) {
            var ra_id = item.applyid;
            window.open("/@mgt/participation/visiontrip/apply_plan/attachlist?id=" + ra_id, "attachFrm", "width=810px,height=500px,scrollbars=yes");
            return false;
        }
        // end - 신청현황

        ////신청정보
         
        $scope.select_vthistory = [{ codevalue: 'Z', codename: '처음' }, { codevalue: 'F', codename: '1회' }, { codevalue: 'S', codename: '2회' }, { codevalue: 'T', codename: '3회 이상' }];

        $scope.i_total = 0;
        $scope.i_page = 1;
        $scope.i_rowsPerPage = 50;

        $scope.i_params = {
            page: $scope.i_page,
            rowsPerPage: $scope.i_rowsPerPage,
            tripYear: $scope.tripYear,
            tripType: $scope.tripType,
            tripID: $scope.tripID
        };

        $scope.getApplyInfoList = function (params) {
            $scope.showLoading();
            $scope.i_params = $.extend($scope.i_params, params);
            console.log('applystate_list_p');
            var requestSubType = $('#hdnRequestSubType').val();
            $http.get("/@mgt/api/visiontrip.ashx?t=applyplay_list&list=info&requestSubType=" + requestSubType, { params: $scope.i_params }).success(function (r) {
                console.log(r);
                if (r.success) {
                    var list = r.data;

                    if (list.length > 0) {
                        $.each(list, function () {
                            this.grid_type = 'info';
                            this.cancel_check = this.cancel_yn == "Y" ? true : false;
                            this.rowselect = false;
                            this.cancel_class = this.cancel_check ? "tr_cancel" : "";
                            this.mychild = []; //후원어린이
                            this.selectedChild = []; //선택어린이

                        });

                    }
                    $scope.applyinfo_list = list;
                    $scope.i_total = r.data.length > 0 ? r.data[0].total : 0;
                    $scope.hideLoading();
                } else {
                    $scope.hideLoading();
                    alert(r.message);
                }
            });

        }
        // end - 신청정보

        //참가자정보
        $scope.p_total = 0;
        $scope.p_page = 1;
        $scope.p_rowsPerPage = 50;

        $scope.p_params = {
            page: $scope.p_page,
            rowsPerPage: $scope.p_rowsPerPage,
            tripYear: $scope.tripYear,
            tripType: $scope.tripType,
            tripID: $scope.tripID
        };

        $scope.getParticipantList = function (params) {
            $scope.showLoading();
            $scope.p_params = $.extend($scope.p_params, params);
            console.log('participant_list_p');
            var requestSubType = $('#hdnRequestSubType').val();
            $http.get("/@mgt/api/visiontrip.ashx?t=applyplay_list&list=participant&requestSubType=" + requestSubType, { params: $scope.p_params }).success(function (r) {
                console.log(r);
                if (r.success) {
                    var list = r.data;

                    $.each(list, function () {
                        this.grid_type = 'participant';
                        this.cancel_check = this.cancel_yn == "Y" ? true : false;
                        this.rowselect = false;
                        this.cancel_class = this.cancel_check ? "tr_cancel" : "";
                    });

                    $scope.participant_list = list;
                    $scope.p_total = r.data.length > 0 ? r.data[0].total : 0;
                    $scope.hideLoading();
                } else {
                    $scope.hideLoading();
                    alert(r.message);
                }
            });

        }
        // end - 참가자정보

        //참가자주소
        $scope.d_total = 0;
        $scope.d_page = 1;
        $scope.d_rowsPerPage = 50;

        $scope.d_params = {
            page: $scope.d_page,
            rowsPerPage: $scope.d_rowsPerPage,
            tripYear: $scope.tripYear,
            tripType: $scope.tripType,
            tripID: $scope.tripID
        };

        $scope.getAddrList = function (params) {
            $scope.showLoading();
            $scope.d_params = $.extend($scope.d_params, params);
            console.log('addr_list_p');
            var requestSubType = $('#hdnRequestSubType').val();
            $http.get("/@mgt/api/visiontrip.ashx?t=applyplay_list&list=addr&requestSubType=" + requestSubType, { params: $scope.d_params }).success(function (r) {
                console.log(r);
                if (r.success) {
                    var list = r.data;

                    if (list.length > 0) {
                        $.each(list, function () {
                            this.grid_type = 'addr';
                            this.cancel_check = this.cancel_yn == "Y" ? true : false;
                            this.rowselect = false;
                            this.cancel_class = this.cancel_check ? "tr_cancel" : "";

                            this.addr_colspan = this.location == "국내" ? 1 : 2;

                            this.domesticYN = this.location == "국내" ? true : false;
                            this.full_doro = this.location == "국내" ? this.address_doro + " " + this.address2 : "";
                            this.full_jibun = this.location == "국내" ? this.address_jibun + " " + this.address2 : "";
                            this.addr2_overseas = this.location != "국내" ? this.address2 : "";
                            this.zipcode_overseas = this.location != "국내" ? this.zipcode : "";
                        });
                    }
                    $scope.addr_list = list;
                    $scope.d_total = r.data.length > 0 ? r.data[0].total : 0;
                    $scope.hideLoading();
                } else {
                    $scope.hideLoading();
                    alert(r.message);
                }
            });

        }
        $scope.select_location = [{ codevalue: '국내', codename: '국내' }, { codevalue: '국외', codename: '해외' }];
        locationChange = function (val) {
            var selectID = val.id;
            var id;
            if (selectID != undefined) {
                id = selectID.replace("selectlocation_", '');
            }

            if ($("#" + selectID).val() == "국내") {
                $("#pn_addr_domestic_" + id).show();
                $("#pn_addr_overseas_" + id).hide();
            }
            else {
                $("#pn_addr_domestic_" + id).hide();
                $("#pn_addr_overseas_" + id).show();
            }

        }
        //주소 검색 팝업
        $scope.addrpopup = function (item, $event) {
            $event.preventDefault();
            cert_setDomain();
            selectAddressRow = item.applyid;
            var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");
        }
        // end - 참가자주소


        //sms 
        $scope.m_total = 0;
        $scope.m_page = 1;
        $scope.m_rowsPerPage = 50;

        $scope.m_params = {
            page: $scope.m_page,
            rowsPerPage: $scope.m_rowsPerPage,
            tripYear: $scope.tripYear,
            tripType: $scope.tripType,
            tripID: $scope.tripID
        };

        $scope.getSMSList = function (params) {
            $scope.showLoading();
            $scope.m_params = $.extend($scope.m_params, params);
            console.log('sms_list_p');
            var requestSubType = $('#hdnRequestSubType').val();
            $http.get("/@mgt/api/visiontrip.ashx?t=applyplay_list&list=sms&requestSubType=" + requestSubType, { params: $scope.m_params }).success(function (r) {
                console.log(r);
                if (r.success) {
                    var list = r.data;

                    $.each(list, function () {
                        this.grid_type = 'sms';
                        this.cancel_check = this.cancel_yn == "Y" ? true : false;
                        this.rowselect = false;
                        this.cancel_class = this.cancel_check ? "tr_cancel" : "";
                    });

                    $scope.sms_list = list;
                    $scope.m_total = r.data.length > 0 ? r.data[0].total : 0;
                    $scope.hideLoading();
                } else {
                    $scope.hideLoading();
                    alert(r.message);
                }
            });
            //$scope.sms_inputs = null;
            $scope.sms_inputs = [];
        }
        //발송 번호 선택
        $scope.sms_inputs = [];
        $scope.smsid = 0;
        $scope.selectSMSList = function () {
            var sametel = 0;
            for (var k in $scope.sms_list) {
                if ($scope.sms_list[k].rowselect) {
                    var telCheck = $scope.sms_inputs.filter(function (item) {
                        return item.tel === $scope.sms_list[k].tel;
                    }).length;
                    if (telCheck == 0) {
                        $scope.sms_inputs.push({ id: $scope.smsid, applyid: $scope.sms_list[k].applyid, userid: $scope.sms_list[k].userid, sponsor_name: $scope.sms_list[k].sponsor_name, tel: $scope.sms_list[k].tel });
                        $scope.smsid++;
                    }
                    else {
                        sametel++;
                    }
                }
            }
            if (sametel > 0) {
                alert("중복 휴대번호는 제외하고 선택되었습니다.");
            }
        };

        $scope.clearSMSList = function () {
            $scope.sms_inputs = [];
        };
        $scope.deleteSMSList = function () {
            var sub = $("#ddlSMS").val();

            for (k in sub) {
                //$scope.sms_inputs.id = sub[k]
                for (i in $scope.sms_inputs) {
                    if ($scope.sms_inputs[i].id == sub[k]) {
                        $scope.sms_inputs.splice(i, 1);
                    }
                }
            }
        };
        $scope.addTel = function () {
            var tel = $("#txtTelAdd").val();
            var telCheck = $scope.sms_inputs.filter(function (item) {
                return item.tel === tel;
            }).length;

            if (tel.trim() != "" && telCheck == 0) {
                $scope.sms_inputs.push({ id: $scope.smsid++, applyid: 0, userid: "", sponsor_name: "", tel: tel });
            } else if (telCheck > 0) {
                alert("이미 등록된 휴대번호 입니다.");
            }
        }
        //end 발송 번호 선택

        $scope.sendSMS = function () {
            $scope.hideLoading();
            var title = $("#txtTitle").val();
            var message = $("#txtMessage").val();
            var sendType = $("#rdoSendType input:checked").val();
            var sendDateTime = $("#txtReserveDate").val();
            var sendTel = $("#txtSendTel").val();
            var messageType = "PlanAdmin";

            var time = new Date().getTime();
            var date = new Date(time);

            var unixTimeStamp = sendType == "R" ? $scope.fetch_unix_timestamp(sendDateTime) : 0;

            if (!validateForm([
                  { id: "#txtTitle", msg: "메세지 제목을 입력하세요." },
                  { id: "#txtMessage", msg: "메세지 내용을 입력하세요." },
                  { id: "#txtSendTel", msg: "발신번호를 입력하세요." }
            ])) {
                return false;
            } else if (sendType == "") {
                alert("발송구분을 선택하세요.");
                return false;
            } else if (sendType == "R" && sendDateTime == "") {
                alert("예약날짜를 선택하세요.");
                return false;
            } else if ($scope.sms_inputs.length == 0) {
                alert("받는 사람을 선택하세요.");
            }
            else {
                $scope.showLoading();
                $scope.messageparam = {
                    t: "send_message",
                    title: title,
                    message: message,
                    sendtype: sendType,
                    senddatetime: sendDateTime,
                    unixtimestamp: unixTimeStamp,
                    messagetype: messageType,
                    tellist: JSON.stringify($scope.sms_inputs),
                    //applytype: "개인방문",
                    sendtel: sendTel,
                    groupno: date//Math.floor(Date.now() / 1000)
                };

                //alert(sendDateTime);
                $http.post("/@mgt/api/visiontrip.ashx", $scope.messageparam).success(function (r) {
                    if (r.success) {
                        $scope.hideLoading();
                        alert("전송되었습니다.");
                    }
                    else {
                        $scope.hideLoading();
                        alert(r.message);
                    }
                });
            }
        };

        $scope.fetch_unix_timestamp = function (date) {
            var selectDate = new Date(date);
            return Math.floor(selectDate.getTime() / 1000);
        }

        //timestamp = fetch_unix_timestamp();

        $scope.messageResultPopup = function () {
            //var ra_id = item.individualdetailid;
            window.open("/@mgt/participation/visiontrip/apply_plan/message_result", "smsResult", "width=810px,height=500px,scrollbars=yes");
            return false;
        }

        // end - sms

        //통계
        $scope.s_total = 0;
        $scope.s_page = 1;
        $scope.s_rowsPerPage = 50;

        $scope.s_params = {
            page: $scope.s_page,
            rowsPerPage: $scope.s_rowsPerPage,
            tripYear: $scope.tripYear,
            tripType: $scope.tripType,
            tripID: $scope.tripID
        };

        $scope.getStatisticsList = function (params) {
            $scope.showLoading();
            $scope.s_params = $.extend($scope.s_params, params);
            console.log('statisticslist_p');
            var requestSubType = $('#hdnRequestSubType').val();
            $http.get("/@mgt/api/visiontrip.ashx?t=applyplay_list&list=statistics&requestSubType=" + requestSubType, { params: $scope.s_params }).success(function (r) {
                console.log(r);
                if (r.success) {
                    //var list = $.parseJSON(r.data);
                    var list = r.data;

                    //if (list.length > 0) {
                    //    $.each(list, function () {
                    //        this.visit_date = new Date(this.visit_date);
                    //        this.tripnotice_view = this.tripnotice != "" ? true : false;
                    //        this.child_name = this.child_name;
                    //        this.child_key = this.child_key != null ? this.child_key.replace(/, /g, ', <br/>') : '';
                    //    }); 
                    //}
                    $scope.statisticts_list = list;
                    $scope.s_total = r.data.length > 0 ? r.data[0].total : 0;
                    $scope.hideLoading();
                } else {
                    $scope.hideLoading();
                    alert(r.message);
                }
            });

        }
        // end - 통계


    });


    app.directive('icheck', ['$timeout', function ($timeout) {
        return {
            require: 'ngModel',
            link: function ($scope, element, $attrs, ngModel) {
                return $timeout(function () {
                    var value = $attrs['value'];

                    $scope.$watch($attrs['ngModel'], function (newValue) {
                        $(element).iCheck('update');
                    })

                    return $(element).iCheck({
                        checkboxClass: 'icheckbox_flat-blue checked',
                        radioClass: 'iradio_flat-blue checked'
                    }).on('ifChanged', function (event) {
                        if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                            $scope.$apply(function () {
                                return ngModel.$setViewValue(event.target.checked);
                            });
                        }
                        if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                            return $scope.$apply(function () {
                                return ngModel.$setViewValue(value);
                            });
                        }
                    });
                });
            }
        };
    }]);



})();

var $page = {

    timer: null,

    attachUploader: function (button) {
        return new AjaxUpload(button, {
            action: '/common/handler/upload',
            responseType: 'json',
            onChange: function (file) {
                var fileName = file.toLowerCase();
            },
            onSubmit: function (file, ext) {
                this.disable();
            },
            onComplete: function (file, response) {

                this.enable();

                console.log(file, response);
                if (response.success) {
                    $("#path_" + button).val(response.name);
                    $("[data-id=path_" + button + "]").val(response.name.replace(/^.*[\\\/]/, ''));

                    console.log($("#path_" + button).val()); //filepullpath
                    console.log($("[data-id=path_" + button + "]").val());  //filename  
                } else
                    alert(response.msg);
            }
        });
    }

};




var selectAddressRow;
function jusoCallback(zipNo, addr1, addr2, jibun) {
    // 실제 저장 데이타
    $("#addr1_" + selectAddressRow).val(addr1 + "//" + jibun);
    $("#addr2_" + selectAddressRow).val(addr2);
    // 화면에 표시
    $("#zipcode_" + selectAddressRow).val(zipNo);

    $("#addr_road_" + selectAddressRow).text("[도로명주소] (" + zipNo + ") " + addr1 + " " + addr2);
    $("#addr_jibun_" + selectAddressRow).text("[지번주소] (" + zipNo + ") " + jibun + " " + addr2);

    $("#dspAddrJibun_" + selectAddressRow).val(jibun);
    $("#dspAddrDoro_" + selectAddressRow).val(addr1);

    $("#addr_domestic_zipcode_" + selectAddressRow).val($("#zipcode_" + selectAddressRow).val());
    $("#addr_domestic_addr1_" + selectAddressRow).val($("#addr1_" + selectAddressRow).val());
    $("#addr_domestic_addr2_" + selectAddressRow).val($("#addr2_" + selectAddressRow).val());
};
function cert_setDomain() {
    //return;
    //if (location.hostname.startsWith("auth")) return;
    if (location.hostname.indexOf("compassionko.org") > -1)
        document.domain = "compassionko.org";
    else if (location.hostname.indexOf("compassionkr.com") > -1)
        document.domain = "compassionkr.com";
    else
        document.domain = "compassion.or.kr";
}















//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////












/*!
 * iCheck v1.0.1, http://git.io/arlzeA
 * ===================================
 * Powerful jQuery and Zepto plugin for checkboxes and radio buttons customization
 *
 * (c) 2013 Damir Sultanov, http://fronteed.com
 * MIT Licensed
 */

(function ($) {

    // Cached vars
    var _iCheck = 'iCheck',
      _iCheckHelper = _iCheck + '-helper',
      _checkbox = 'checkbox',
      _radio = 'radio',
      _checked = 'checked',
      _unchecked = 'un' + _checked,
      _disabled = 'disabled',
      _determinate = 'determinate',
      _indeterminate = 'in' + _determinate,
      _update = 'update',
      _type = 'type',
      _click = 'click',
      _touch = 'touchbegin.i touchend.i',
      _add = 'addClass',
      _remove = 'removeClass',
      _callback = 'trigger',
      _label = 'label',
      _cursor = 'cursor',
      _mobile = /ipad|iphone|ipod|android|blackberry|windows phone|opera mini|silk/i.test(navigator.userAgent);

    // Plugin init
    $.fn[_iCheck] = function (options, fire) {

        // Walker
        var handle = 'input[type="' + _checkbox + '"], input[type="' + _radio + '"]',
          stack = $(),
          walker = function (object) {
              object.each(function () {
                  var self = $(this);

                  if (self.is(handle)) {
                      stack = stack.add(self);
                  } else {
                      stack = stack.add(self.find(handle));
                  };
              });
          };

        // Check if we should operate with some method
        if (/^(check|uncheck|toggle|indeterminate|determinate|disable|enable|update|destroy)$/i.test(options)) {

            // Normalize method's name
            options = options.toLowerCase();

            // Find checkboxes and radio buttons
            walker(this);

            return stack.each(function () {
                var self = $(this);

                if (options == 'destroy') {
                    tidy(self, 'ifDestroyed');
                } else {
                    operate(self, true, options);
                };

                // Fire method's callback
                if ($.isFunction(fire)) {
                    fire();
                };
            });

            // Customization
        } else if (typeof options == 'object' || !options) {

            // Check if any options were passed
            var settings = $.extend({
                checkedClass: _checked,
                disabledClass: _disabled,
                indeterminateClass: _indeterminate,
                labelHover: true,
                aria: false
            }, options),

              selector = settings.handle,
              hoverClass = settings.hoverClass || 'hover',
              focusClass = settings.focusClass || 'focus',
              activeClass = settings.activeClass || 'active',
              labelHover = !!settings.labelHover,
              labelHoverClass = settings.labelHoverClass || 'hover',

              // Setup clickable area
              area = ('' + settings.increaseArea).replace('%', '') | 0;

            // Selector limit
            if (selector == _checkbox || selector == _radio) {
                handle = 'input[type="' + selector + '"]';
            };

            // Clickable area limit
            if (area < -50) {
                area = -50;
            };

            // Walk around the selector
            walker(this);

            return stack.each(function () {
                var self = $(this);

                // If already customized
                tidy(self);

                var node = this,
                  id = node.id,

                  // Layer styles
                  offset = -area + '%',
                  size = 100 + (area * 2) + '%',
                  layer = {
                      position: 'absolute',
                      top: offset,
                      left: offset,
                      display: 'block',
                      width: size,
                      height: size,
                      margin: 0,
                      padding: 0,
                      background: '#fff',
                      border: 0,
                      opacity: 0
                  },

                  // Choose how to hide input
                  hide = _mobile ? {
                      position: 'absolute',
                      visibility: 'hidden'
                  } : area ? layer : {
                      position: 'absolute',
                      opacity: 0
                  },

                  // Get proper class
                  className = node[_type] == _checkbox ? settings.checkboxClass || 'i' + _checkbox : settings.radioClass || 'i' + _radio,

                  // Find assigned labels
                  label = $(_label + '[for="' + id + '"]').add(self.closest(_label)),

                  // Check ARIA option
                  aria = !!settings.aria,

                  // Set ARIA placeholder
                  ariaID = _iCheck + '-' + Math.random().toString(36).substr(2, 6),

                  // Parent & helper
                  parent = '<div class="' + className + '" ' + (aria ? 'role="' + node[_type] + '" ' : ''),
                  helper;

                // Set ARIA "labelledby"
                if (aria) {
                    label.each(function () {
                        parent += 'aria-labelledby="';

                        if (this.id) {
                            parent += this.id;
                        } else {
                            this.id = ariaID;
                            parent += ariaID;
                        }

                        parent += '"';
                    });
                };

                // Wrap input
                parent = self.wrap(parent + '/>')[_callback]('ifCreated').parent().append(settings.insert);

                // Layer addition
                helper = $('<ins class="' + _iCheckHelper + '"/>').css(layer).appendTo(parent);

                // Finalize customization
                self.data(_iCheck, { o: settings, s: self.attr('style') }).css(hide);
                !!settings.inheritClass && parent[_add](node.className || '');
                !!settings.inheritID && id && parent.attr('id', _iCheck + '-' + id);
                parent.css('position') == 'static' && parent.css('position', 'relative');
                operate(self, true, _update);

                // Label events
                if (label.length) {
                    label.on(_click + '.i mouseover.i mouseout.i ' + _touch, function (event) {
                        var type = event[_type],
                          item = $(this);

                        // Do nothing if input is disabled
                        if (!node[_disabled]) {

                            // Click
                            if (type == _click) {
                                if ($(event.target).is('a')) {
                                    return;
                                }
                                operate(self, false, true);

                                // Hover state
                            } else if (labelHover) {

                                // mouseout|touchend
                                if (/ut|nd/.test(type)) {
                                    parent[_remove](hoverClass);
                                    item[_remove](labelHoverClass);
                                } else {
                                    parent[_add](hoverClass);
                                    item[_add](labelHoverClass);
                                };
                            };

                            if (_mobile) {
                                event.stopPropagation();
                            } else {
                                return false;
                            };
                        };
                    });
                };

                // Input events
                self.on(_click + '.i focus.i blur.i keyup.i keydown.i keypress.i', function (event) {
                    var type = event[_type],
                      key = event.keyCode;

                    // Click
                    if (type == _click) {
                        return false;

                        // Keydown
                    } else if (type == 'keydown' && key == 32) {
                        if (!(node[_type] == _radio && node[_checked])) {
                            if (node[_checked]) {
                                off(self, _checked);
                            } else {
                                on(self, _checked);
                            };
                        };

                        return false;

                        // Keyup
                    } else if (type == 'keyup' && node[_type] == _radio) {
                        !node[_checked] && on(self, _checked);

                        // Focus/blur
                    } else if (/us|ur/.test(type)) {
                        parent[type == 'blur' ? _remove : _add](focusClass);
                    };
                });

                // Helper events
                helper.on(_click + ' mousedown mouseup mouseover mouseout ' + _touch, function (event) {
                    var type = event[_type],

                      // mousedown|mouseup
                      toggle = /wn|up/.test(type) ? activeClass : hoverClass;

                    // Do nothing if input is disabled
                    if (!node[_disabled]) {

                        // Click
                        if (type == _click) {
                            operate(self, false, true);

                            // Active and hover states
                        } else {

                            // State is on
                            if (/wn|er|in/.test(type)) {

                                // mousedown|mouseover|touchbegin
                                parent[_add](toggle);

                                // State is off
                            } else {
                                parent[_remove](toggle + ' ' + activeClass);
                            };

                            // Label hover
                            if (label.length && labelHover && toggle == hoverClass) {

                                // mouseout|touchend
                                label[/ut|nd/.test(type) ? _remove : _add](labelHoverClass);
                            };
                        };

                        if (_mobile) {
                            event.stopPropagation();
                        } else {
                            return false;
                        };
                    };
                });
            });
        } else {
            return this;
        };
    };

    // Do something with inputs
    function operate(input, direct, method) {
        var node = input[0],
          state = /er/.test(method) ? _indeterminate : /bl/.test(method) ? _disabled : _checked,
          active = method == _update ? {
              checked: node[_checked],
              disabled: node[_disabled],
              indeterminate: input.attr(_indeterminate) == 'true' || input.attr(_determinate) == 'false'
          } : node[state];

        // Check, disable or indeterminate
        if (/^(ch|di|in)/.test(method) && !active) {
            on(input, state);

            // Uncheck, enable or determinate
        } else if (/^(un|en|de)/.test(method) && active) {
            off(input, state);

            // Update
        } else if (method == _update) {

            // Handle states
            for (var state in active) {
                if (active[state]) {
                    on(input, state, true);
                } else {
                    off(input, state, true);
                };
            };

        } else if (!direct || method == 'toggle') {

            // Helper or label was clicked
            if (!direct) {
                input[_callback]('ifClicked');
            };

            // Toggle checked state
            if (active) {
                if (node[_type] !== _radio) {
                    off(input, state);
                };
            } else {
                on(input, state);
            };
        };
    };

    // Add checked, disabled or indeterminate state
    function on(input, state, keep) {
        var node = input[0],
          parent = input.parent(),
          checked = state == _checked,
          indeterminate = state == _indeterminate,
          disabled = state == _disabled,
          callback = indeterminate ? _determinate : checked ? _unchecked : 'enabled',
          regular = option(input, callback + capitalize(node[_type])),
          specific = option(input, state + capitalize(node[_type]));

        // Prevent unnecessary actions
        if (node[state] !== true) {

            // Toggle assigned radio buttons
            if (!keep && state == _checked && node[_type] == _radio && node.name) {
                var form = input.closest('form'),
                  inputs = 'input[name="' + node.name + '"]';

                inputs = form.length ? form.find(inputs) : $(inputs);

                inputs.each(function () {
                    if (this !== node && $(this).data(_iCheck)) {
                        off($(this), state);
                    };
                });
            };

            // Indeterminate state
            if (indeterminate) {

                // Add indeterminate state
                node[state] = true;

                // Remove checked state
                if (node[_checked]) {
                    off(input, _checked, 'force');
                };

                // Checked or disabled state
            } else {

                // Add checked or disabled state
                if (!keep) {
                    node[state] = true;
                };

                // Remove indeterminate state
                if (checked && node[_indeterminate]) {
                    off(input, _indeterminate, false);
                };
            };

            // Trigger callbacks
            callbacks(input, checked, state, keep);
        };

        // Add proper cursor
        if (node[_disabled] && !!option(input, _cursor, true)) {
            parent.find('.' + _iCheckHelper).css(_cursor, 'default');
        };

        // Add state class
        parent[_add](specific || option(input, state) || '');

        // Set ARIA attribute
        disabled ? parent.attr('aria-disabled', 'true') : parent.attr('aria-checked', indeterminate ? 'mixed' : 'true');

        // Remove regular state class
        parent[_remove](regular || option(input, callback) || '');
    };

    // Remove checked, disabled or indeterminate state
    function off(input, state, keep) {
        var node = input[0],
          parent = input.parent(),
          checked = state == _checked,
          indeterminate = state == _indeterminate,
          disabled = state == _disabled,
          callback = indeterminate ? _determinate : checked ? _unchecked : 'enabled',
          regular = option(input, callback + capitalize(node[_type])),
          specific = option(input, state + capitalize(node[_type]));

        // Prevent unnecessary actions
        if (node[state] !== false) {

            // Toggle state
            if (indeterminate || !keep || keep == 'force') {
                node[state] = false;
            };

            // Trigger callbacks
            callbacks(input, checked, callback, keep);
        };

        // Add proper cursor
        if (!node[_disabled] && !!option(input, _cursor, true)) {
            parent.find('.' + _iCheckHelper).css(_cursor, 'pointer');
        };

        // Remove state class
        parent[_remove](specific || option(input, state) || '');

        // Set ARIA attribute
        disabled ? parent.attr('aria-disabled', 'false') : parent.attr('aria-checked', 'false');

        // Add regular state class
        parent[_add](regular || option(input, callback) || '');
    };

    // Remove all traces
    function tidy(input, callback) {
        if (input.data(_iCheck)) {

            // Remove everything except input
            input.parent().html(input.attr('style', input.data(_iCheck).s || ''));

            // Callback
            if (callback) {
                input[_callback](callback);
            };

            // Unbind events
            input.off('.i').unwrap();
            $(_label + '[for="' + input[0].id + '"]').add(input.closest(_label)).off('.i');
        };
    };

    // Get some option
    function option(input, state, regular) {
        if (input.data(_iCheck)) {
            return input.data(_iCheck).o[state + (regular ? '' : 'Class')];
        };
    };

    // Capitalize some string
    function capitalize(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };

    // Executable handlers
    function callbacks(input, checked, callback, keep) {
        if (!keep) {
            if (checked) {
                input[_callback]('ifToggled');
            };

            input[_callback]('ifChanged')[_callback]('if' + capitalize(callback));
        };
    };
})(window.jQuery || window.Zepto);

$(function () {
    $('input').iCheck();
    $('input.all').on('ifChecked ifUnchecked', function (event) {
        if (event.type == 'ifChecked') {
            $('input.check').iCheck('check');
        } else {
            $('input.check').iCheck('uncheck');
        }
    });
    $('input.check').on('ifUnchecked', function (event) {
        $('input.all').iCheck('uncheck');
    });
});