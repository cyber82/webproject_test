﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_participation_visiontrip_apply_plan_default : AdminBoardPage
{


    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();
        base.LoadComplete += new EventHandler(list_LoadComplete);

        //트립유형
        foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "trip_type").OrderBy(p => p.cd_order))
        {
            ddlTripType.Items.Add(new ListItem(a.cd_value, a.cd_key));
        }
        //년도
        for (int i = 2005; i <= DateTime.Now.AddYears(1).Year; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlYear.SelectedValue = DateTime.Now.Year.ToString();



        //using (AdminDataContext dao = new AdminDataContext())
        //{
        //    var tripSchedule = dao.tVisionTripSchedule.Where(p => p.VisionTripType == "Plan" && p.CurrentUse == 'Y' 
        //            && p.StartDate.Substring(0, 4) == ddlYear.SelectedValue).OrderBy(p=> p.VisitCountry).ToList();

        //    foreach(tVisionTripSchedule list in tripSchedule)
        //    { 
        //        ddlTripName.Items.Add(new ListItem(list.VisitCountry, list.ScheduleID.ToString()));
        //    }
        //}

        // #13115 : 관리자로그인아이디가 vision1~vision6인 경우, 요청트립에서 설정한 하위분류에 해당하는 리스트만 조회
        string requestSubType = "";
        string adminLoginID = AdminLoginSession.GetCookie(this.Context).email;
        if (adminLoginID.Equals("vision1") || adminLoginID.Equals("vision2") || adminLoginID.Equals("vision3") || adminLoginID.Equals("vision4") || adminLoginID.Equals("vision5") || adminLoginID.Equals("vision6"))
        {
            switch (adminLoginID)
            {
                case "vision1": requestSubType = "CR"; break;
                case "vision2": requestSubType = "MKT"; break;
                case "vision3": requestSubType = "ADV"; break;
                case "vision4": requestSubType = "PR"; break;
                case "vision5": requestSubType = "NK"; break;
                case "vision6": requestSubType = "기타"; break;
                default: requestSubType = ""; break;
            }
        }
        hdnRequestSubType.Value = requestSubType;

    }

    protected override void OnAfterPostBack()
    {
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();
    }

    protected void btnTripYearSet_Click(object sender, EventArgs e)
    {
        //using (MainLibDataContext dao = new MainLibDataContext())
        //{
        //    var s = dao.code.First(p => p.cd_group == "trip" && p.cd_key == "year");
        //    s.cd_value = a_type.SelectedValue;
        //    Master.ValueMessage.Value = "저장되었습니다.";
        //    dao.SubmitChanges();
        //}
    }



    protected void ddlTripType_ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlTripName.Items.Clear();

        string strYear = ddlYear.SelectedValue;
        string strTripType = ddlTripType.SelectedValue;
        using (AdminDataContext dao = new AdminDataContext())
        {
            var list1 = www6.selectQ<tVisionTripSchedule>();
            var tripSchedule = list1.Where(p => p.CurrentUse == 'Y'
                    && p.StartDate.Substring(0, 4) == strYear && (strTripType == "" || p.VisionTripType == strTripType)).OrderBy(p => p.VisitCountry).ToList();

            ddlTripName.Items.Add(new ListItem("전체", "0"));

            foreach (tVisionTripSchedule list in tripSchedule)
            {
                ddlTripName.Items.Add(new ListItem(list.VisitCountry, list.ScheduleID.ToString()));
            }
        }
    }
}
