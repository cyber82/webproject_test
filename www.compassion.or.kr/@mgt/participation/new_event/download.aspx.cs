﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class mgt_participation_event_download : AdminBasePage {

	protected override void OnBeforePostBack() {

		var id = Convert.ToInt32(Request["c"]);
		var title = Request["title"];
		base.OnBeforePostBack();
		this.GetList(id, title);


		// 익스에서 한글, 띄어쓰기 깨짐
		HttpBrowserCapabilities brObject = Request.Browser;
		var browser = brObject.Type.ToLower();
		var fileName = "이벤트_" + title.Replace(" ", "_").Replace(",", "").Replace("'", "") + "_" + id.ToString() + ".xls";
		if (browser.Contains("ie")) {
			fileName = System.Web.HttpUtility.UrlEncode(fileName);
		}


		Response.Clear();
		Response.Charset = "utf-8";
		Response.ContentType = "application/vnd.ms-excel";
		Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
	}


	protected void GetList(int id, string title) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.sp_tEvent_apply_list(id, "", 1, 999999).OrderByDescending(e => e.regdate).ToList();
            //Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword", "startdate", "enddate" };
            Object[] op1 = new Object[] { "e_id", "regdate", "page", "rowsPerPage" };
            Object[] op2 = new Object[] { id, "", 1, 999999};
            var list = www6.selectSP("sp_tEvent_apply_list", op1, op2).DataTableToList<sp_tEvent_apply_listResult>().OrderByDescending(e => e.regdate).ToList();

            repeater.DataSource = list;
            repeater.DataBind();

            base.WriteLog(AdminLog.Type.select, string.Format("이벤트_{0} 다운로드 ({1})", title, id));
        }
	}



	protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e) {
		if (e.Item.ItemType == ListItemType.Footer)
			return;

		sp_tEvent_apply_listResult entity = e.Item.DataItem as sp_tEvent_apply_listResult;

	}
}