﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="download.aspx.cs" Inherits="mgt_participation_event_download"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head runat="server">
    <title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
		html{background:#fff}
		table { border-collapse:initial; }  
		th, td { border:1px solid #000000;}
		td { mso-number-format:\@; }
	</style>
</head>
<body>
    <form id="form" runat="server">
		<table class="listBoard">
			<tbody>	
				<tr>
					<td style="width:50px">번호</td>
					<td style="width:150px">SponsorID</td>
                    <td style="width:150px">ConID</td>
                    <td style="width:150px">UserID</td>
                    <th style="width:150px">경로</th>
                    <th style="width:150px">후원종류</th>
                    <th style="width:150px">후원금액</th>
                    <th style="width:150px">등록일자</th>
				</tr>		
				<asp:Repeater runat=server ID=repeater  OnItemDataBound="repeater_ItemDataBound">
					<ItemTemplate>
						<tr>
							<td><%#Container.ItemIndex + 1 %></td>
							<td><%#Eval("SponsorID" )%></td>
                            <td><%#Eval("ConID" )%></td>
                            <td><%#Eval("UserID" )%></td>
							<td><%#Eval("evt_Route" )%></td>
							<td><%#Eval("SponsorType" )%></td>
                            <td><%#Eval("SponsorAmount" )%></td>
							<td><%#Eval("regdate" , "{0:yyyy'.'MM'.'dd HH:mm}")%></td>
						</tr>	
					</ItemTemplate>
					<FooterTemplate>
						<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
							<td colspan="11">데이터가 없습니다.</td>
						</tr>
					</FooterTemplate>
				</asp:Repeater>	
				
			</tbody>
		</table>
    </form>
</body>
</html>
