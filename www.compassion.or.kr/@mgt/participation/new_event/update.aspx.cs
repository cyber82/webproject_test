﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Data;

public partial class mgt_participation_tEvent_update : AdminBoardPage {
	public class DatePick {
		public string date { get; set;}
		public string limit { get; set; }
	}
	
	protected override void OnBeforePostBack()
    {
		base.OnBeforePostBack();

		v_admin_auth auth = base.GetPageAuth();

		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];
		formAction.Value = base.Action;

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context , "default.aspx");




        using (AdminDataContext dao = new AdminDataContext())
        {
            if (base.Action == "update")
            {
                //var entity = dao.@tEvent.First(p => p.e_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<@tEvent>("e_id", Convert.ToInt32(PrimaryKey));

                if (entity.e_begin != null) e_begin.Text = ((DateTime)entity.e_begin).ToString("yyyy-MM-dd");
                if (entity.e_end != null) e_end.Text = ((DateTime)entity.e_end).ToString("yyyy-MM-dd");

                e_title.Text = entity.e_title;

                e_content.InnerText = entity.e_content;
                evt_id.InnerText = PrimaryKey;


                e_regdate.Text = entity.e_regdate.ToString("yyyy-MM-dd hh:mm:ss");

                btn_apply_download.Visible = true;

                btn_update.Visible = auth.aa_auth_update;
                btn_remove.Visible = auth.aa_auth_delete;
                ph_regdate.Visible = true;

                //응모자 다운로드 버튼
                btn_apply_download.Attributes["data-id"] = this.PrimaryKey.ToString();
                btn_apply_download.Attributes["data-title"] = e_title.Text;

                btn_apply_download2.Attributes["data-id"] = this.PrimaryKey.ToString();
                btn_apply_download2.Attributes["data-title"] = e_title.Text;


                e_id.Value = PrimaryKey.ToString();


            }
            else
            {
                btn_update.Visible = auth.aa_auth_create;
                btn_remove.Visible = false;
                btn_apply_download.Visible = false;
                EventIdDiv.Visible = false;
            }
        }
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";


		 
	}

	protected void btn_update_click(object sender, EventArgs e) {
		
		using (AdminDataContext dao = new AdminDataContext()) {

            //var entity = base.Action == "update" ? dao.@tEvent.First(p => p.e_id == Convert.ToInt32(PrimaryKey)) : new @tEvent();
            var entity = new @tEvent();
            if (base.Action == "update")
            {
                entity = www6.selectQF<@tEvent>("e_id", Convert.ToInt32(PrimaryKey));
            }


            entity.e_title = e_title.Text;
			entity.e_content = e_content.InnerHtml.ToHtml(false);
			

			// 이벤트가능기간
            if(e_begin.Text.Length > 0 && e_end.Text.Length > 0) { 
			    entity.e_begin = DateTime.Parse(e_begin.Text);
			    entity.e_end = DateTime.Parse(e_end.Text + " 23:59:59");
            }


            if (base.Action == "update")
            {
                //dao.SubmitChanges();
                
                www6.update(entity);

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
				AlertWithJavascript("수정되었습니다.", "goList()");

			}
            else
            {
				entity.e_regdate = DateTime.Now;
                entity.delYn = 'N';
                //entity.e_a_id = AdminLoginSession.GetCookie(this.Context).identifier;
                //dao.@tEvent.InsertOnSubmit(entity);
                //dao.SubmitChanges();
                
                www6.insert(entity);

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", entity.ToJson()));
				AlertWithJavascript("등록되었습니다.", "goList()");

			}
		}
	}
     

	protected void btn_remove_click(object sender, EventArgs e) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.@tEvent.First(p => p.e_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<@tEvent>("e_id", Convert.ToInt32(PrimaryKey));
            entity.delYn = 'Y';

            //www6.cud(MakeSQL.upQ(entity,  "e_id"));
            www6.update(entity);

            //dao.SubmitChanges();

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", entity.ToJson()));
        }

		AlertWithJavascript("삭제되었습니다.", "goList()");
	}

    protected void btn_section_Click(object sender, EventArgs e)
    {
        LinkButton obj = sender as LinkButton;
        var no = obj.CommandArgument;
        int tabCount = 2;
        int current = 0;

        for (int i = 1; i < tabCount + 1; i++)
        {
            if (no == i.ToString())
            {
                current = i;
                ((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "active";
                ((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "active tab-pane";
            }
            else
            {
                ((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "";
                ((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "tab-pane";
            }
        }

        if (current == 2)
        {
            this.GetApplyList(1);
            hpExcelUpload.Visible = true;
        }
        else
        {
            et_date.Value = "";
            hpExcelUpload.Visible = false;
        }
    }

    // 참여자 검색 
    protected void search2(object sender, EventArgs e)
    {
        this.GetApplyList(1);
    }

    protected void GetApplyList(int page)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_event_apply_list(Convert.ToInt32(PrimaryKey), et_date.Value, page, paging_apply.RowsPerPage, s_keyword2.Value).ToList();

            //var list = dao.sp_tEvent_apply_list(Convert.ToInt32(PrimaryKey), "", 1, 999999).OrderByDescending(e => e.regdate).ToList();
            //Object[] op1 = new Object[] { "e_id", "regdate", "page", "rowsPerPage" };
            //Object[] op2 = new Object[] { Convert.ToInt32(PrimaryKey), "", 1, 999999 };
            //var list = www6.selectSP("sp_tEvent_apply_list", op1, op2).DataTableToList<sp_tEvent_apply_listResult>().OrderByDescending(e => e.regdate).ToList();

            Object[] op1 = new Object[] { "e_id", "regdate", "page", "rowsPerPage" };
            Object[] op2 = new Object[] { Convert.ToInt32(PrimaryKey), "", page, paging_apply.RowsPerPage };
            var list = www6.selectSP("sp_tEvent_apply_list", op1, op2).DataTableToList<sp_tEvent_apply_listResult>().OrderByDescending(e => e.EVT_ID).ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging_apply.CurrentPage = page;
            paging_apply.Calculate(total);

            //foreach (sp_event_apply_listResult item in list)
            //{

            //    string sponsorID = item.tempSponsorID;
            //    if (string.IsNullOrEmpty(sponsorID))
            //        item.IsSponsor = "N";
            //    else
            //    {
            //        // 센터 소개 수정 문희원 2017-04-06
            //        var objSql = new object[1] { " SELECT  " +
            //                                " count(CM.CommitmentID) as cnt " +
            //                             " FROM  " +
            //                                " dbo.tCommitmentMaster CM WITH (NOLOCK)  " +
            //                             " WHERE CM.SponsorID = '" + item.tempSponsorID + "' AND stopdate is null and (SponsorItemEng = 'ds' or sponsorItemEng = 'ls') AND (SponsorTypeEng = 'CHISPO' or sponsorItemEng = 'CHIMON') " };

            //        DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

            //        if (ds2 == null || ds2.Tables.Count == 0 || ds2.Tables[0].Rows.Count == 0)
            //            item.IsSponsor = "N";
            //        else
            //        {
            //            int n = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());
            //            if (n > 0)
            //                item.IsSponsor = "Y";
            //            else
            //                item.IsSponsor = "N";
            //        }
            //    }
            //}

            repeater_apply.DataSource = list;
            repeater_apply.DataBind();

        }


    }

    protected void paging_apply_Navigate(int page, string hash)
    {
        this.GetApplyList(page);
    }

    protected void btnRemove_Click(object sender, EventArgs e)
    {

        LinkButton obj = sender as LinkButton;
        var er_id = obj.CommandArgument;

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.event_apply.First(p => p.er_id == Convert.ToInt32(er_id));
            var entity = www6.selectQF<event_apply>("er_id", Convert.ToInt32(er_id));

            //dao.event_apply.DeleteOnSubmit(entity);
            www6.delete(entity);

            // 당첨자 삭제
            //var winner = dao.event_winner.FirstOrDefault(p => p.ew_e_id == Convert.ToInt32(PrimaryKey) && p.ew_user_id == entity.er_user_id);
            var winner = www6.selectQF<event_winner>("ew_e_id", Convert.ToInt32(PrimaryKey), "ew_user_id", entity.er_user_id);

            if (winner != null)
            {
                //dao.event_winner.DeleteOnSubmit(winner);
                www6.delete(winner);
            }

            //dao.SubmitChanges();

            Master.ValueMessage.Value = "삭제되었습니다.";
            GetApplyList(paging_apply.CurrentPage);
        }
    }

    protected void repeater_apply_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Footer)
            return;

        sp_event_apply_listResult entity = e.Item.DataItem as sp_event_apply_listResult;

        
        ((Literal)e.Item.FindControl("lbIdx")).Text = (paging_apply.TotalRecords - ((paging_apply.CurrentPage - 1) * paging_apply.RowsPerPage) - e.Item.ItemIndex).ToString();


        //((Literal)e.Item.FindControl("lbUserId")).Text = entity.er_a_id == -1 ? entity.er_user_id : "관리자 등록";


        //((DropDownList)e.Item.FindControl("attended")).SelectedValue = entity.er_attended ? "1" : "0";
        //((DropDownList)e.Item.FindControl("related")).SelectedValue = entity.er_related ? "1" : "0";



        //// 인원선택 미입력 이벤트도 있음
        //if (entity.er_count == -1)
        //{
        //    ((Literal)e.Item.FindControl("none_count")).Visible = true;
        //}
        //else
        //{
        //    // 관리자가 등록한 것 (관리자 등록 = 단체)
        //    if (entity.er_user_id.EmptyIfNull() == "")
        //    {
        //        ((TextBox)e.Item.FindControl("team_count")).Text = entity.er_count.ToString();
        //        ((TextBox)e.Item.FindControl("team_count")).Visible = true;
        //        ((DropDownList)e.Item.FindControl("member_count")).Visible = false;

        //    }
        //    else
        //    {
        //        ((DropDownList)e.Item.FindControl("member_count")).SelectedValue = entity.er_count.ToString();
        //        ((DropDownList)e.Item.FindControl("member_count")).Visible = true;
        //        ((TextBox)e.Item.FindControl("team_count")).Visible = false;
        //    }
        //}

        //// 체험전은 분까지, 이벤트는 날짜만
        //if (entity.er_date != null)
        //{
        //    ((Literal)e.Item.FindControl("lbDate")).Text = ((DateTime)entity.er_date).ToString("yyyy-MM-dd HH:mm");
        //}
        //else
        //{
        //    ((Literal)e.Item.FindControl("lbDate")).Text = "-";
        //}

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.event_apply;
            var list = www6.selectQ<event_apply>();

            foreach (RepeaterItem item in repeater_apply.Items)
            {

                var er_id = Convert.ToInt32(((LinkButton)item.FindControl("btnRemove")).CommandArgument);

                var er_count = -1;
                if (((DropDownList)item.FindControl("member_count")).Visible)
                {
                    er_count = Convert.ToInt32(((DropDownList)item.FindControl("member_count")).SelectedValue);
                }
                else if (((TextBox)item.FindControl("team_count")).Visible)
                {
                    er_count = Convert.ToInt32(((TextBox)item.FindControl("team_count")).Text);
                }

                var attended = ((DropDownList)item.FindControl("attended")).SelectedValue == "1";
                var related = ((DropDownList)item.FindControl("related")).SelectedValue == "1";

                var entity = list.First(p => p.er_id == er_id);
                entity.er_count = er_count;
                entity.er_attended = attended;
                entity.er_related = related;
                string updateStr = string.Format(@"update event_apply set  er_count     = '{0}'
                                                                         , er_attended  = '{1}'
                                                                         , er_related   = '{2}'
                                                   where where er_id = {4}"
                                                                         , entity.er_count
                                                                         , entity.er_attended
                                                                         , entity.er_related
                                                                         , er_id);
                www6.cud(updateStr);

            }

            //dao.SubmitChanges();
        }

        Master.ValueMessage.Value = "수정되었습니다.";
        //GetList(1);
    }
}