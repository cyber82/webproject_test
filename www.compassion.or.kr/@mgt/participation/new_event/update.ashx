﻿<%@ WebHandler Language="C#" Class="api_board" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_board : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();
        if(t == "close") {
            Close(context);
        }else if(t == "apply_team") {
            ApplyTeam(context);
        }
    }



    void Close(HttpContext context) {

        JsonWriter result = new JsonWriter() {
            success = true
        };

        var et_id = context.Request["id"];
        var close = context.Request["close"];

        using (FrontDataContext dao = new FrontDataContext()) {
            var entity = dao.event_timetable.First(p => p.et_id == Convert.ToInt32(et_id));
            entity.et_closed = close == "y";
            dao.SubmitChanges();

            //result.data = dao.event_timetable.Count(p => p.et_e_id == entity.et_e_id);
            var count = dao.event_apply.Count(p => p.er_e_id == entity.et_e_id && p.er_date == entity.et_date);
            var time = entity.et_date;

            var data = new Dictionary<string, string>();
            data.Add("count", count.ToString());
            data.Add("date", entity.et_date.ToString("yyyy-MM-dd HH:mm:ss"));
            //result.data = dao.event_apply.Count(p => p.er_e_id == entity.et_e_id && p.er_date == entity.et_date);
            result.data = data;
        }

        JsonWriter.Write(result, context);
    }



    void ApplyTeam(HttpContext context) {

        JsonWriter result = new JsonWriter() {
            success = true
        };

        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var name = context.Request["name"].EmptyIfNull();
        var phone = context.Request["phone"].EmptyIfNull();
        var member = Convert.ToInt32(context.Request["member"].ValueIfNull("-1"));

        if(id == -1 || name == "" || phone == "" || member == -1) {
            result.success = false;
            result.message = "필수 정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext()) {

            var event_timetable = dao.event_timetable.First(p => p.et_id == id);

            var entity = new event_apply() {
                er_e_id = event_timetable.et_e_id,
                er_user_id = "",
                er_name = name,
                er_phone = phone,
                er_count = member,
                er_date = event_timetable.et_date,
                er_route = "",
                er_attended = false,
                er_related = false,
                er_regdate = DateTime.Now,
                er_a_id = AdminLoginSession.GetCookie(context).identifier
            };
            dao.event_apply.InsertOnSubmit(entity);

            dao.SubmitChanges();
        }

        JsonWriter.Write(result, context);
    }

}