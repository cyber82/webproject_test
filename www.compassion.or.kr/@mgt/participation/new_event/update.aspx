﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_participation_tEvent_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <link rel="stylesheet" href="/@mgt/common/js/fullcalendar.min.css">

    <script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
    <script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>	
	<script type="text/javascript" src="/@mgt/common/js/fullcalendar.min.js"></script>
	<script type="text/javascript" src="/@mgt/common/js/fullcalendar.ko.js"></script>
    <script type="text/javascript" src="/@mgt/participation/new_event/schedule.js"></script>
    <script type="text/javascript" src="/@mgt/participation/new_event/update.js"></script>
    
    <script src="../../common/js/moment-timezone.min.js"></script>
    
	<style type="text/css">
		#e_type label, #e_target label, #direct_sponsor label, #winner_type label{margin-left:2px;margin-right:5px;}

        /* 시간선택 달력에 next, prev버튼 제거*/
        #schedule .fc-right{display:none;}

        /*fc-content*/
        .fc-content{cursor:pointer;height:25px;text-align:center;padding-top:2px;}

        .fc-time-grid-event{width: 200px;}

        .fc-slats tr{ height: 25px; }

        .fc-time-grid-event.fc-short .fc-title{font-size:1.4em;}
        .fc-time-grid-event .fc-time{font-size:1.4em;}
        
        /* 마감 버튼 */
        .schedule-label{width:60px;}

        .btnView{width:230px;}

	</style>
	<script type="text/javascript">

	    $(function () {
	        $hell.init();

	        

	        // 에디터
	        image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
		    initEditor(oEditors, "e_content");
		    

	 


	        // 날짜 선택
	        $("#e_begin , #e_end ").focus(function () {
	        	var className = $(this).data("range");
	        	$("." + className).trigger("click");
	        	$("." + className).focus();
		        return false;
	        })


	        $("#btn_apply_download").click(function () {
	            console.log("down load btn clock");
	            var id = $(this).attr("data-id");
	            var title = $(this).attr("data-title");

	            location.href = "/@mgt/participation/new_event/download?c=" + id + "&title=" + escape(title);
	            return false;
	        })

             
             

	        // 신청 날자 추가
	        $("#btn_add").click(function () {
	            addPick();
	        });

	        $(".btn_apply_download").click(function () {

	            var id = $(this).attr("data-id");
	            var title = $(this).attr("data-title");
	            var keyword = $("#s_keyword2").val();

	            //alert(id + '/' + title); return;

	            location.href = "/@mgt/participation/new_event/download?c=" + id + "&title=" + escape(title) + "&keyword=" + escape(keyword);
	            return false;
	        })

            // 엑셀업로드
		    var uploader_excel = attachUploader("btn_winner_upload");
		    uploader_excel._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_participation)%>";

        });


        
        

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <asp:PlaceHolder runat="server" ID="hpExcelUpload" Visible="false">
	    <div class="alert alert-danger alert-dismissable">
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert">×</button>
		    <h4><i class="icon fa fa-check"></i>Alert!</h4>
            엑셀을 다운로드 한 뒤 '호환모드'로 변경하신 뒤 업로드 하세요.<br />
            엑셀의 '당첨여부'란에 O(영문)으로 표시된 회원이 당첨회원입니다.<br />
            엑셀 업로드시 기존 당첨내역은 삭제됩니다.<br />
	    </div>
    </asp:PlaceHolder>
 
    <input type="hidden" runat="server" id="e_id" value="" />
    <input type="hidden" runat="server" id="et_date" value="" />
    
    <input type="hidden" runat="server" id="formAction" value="" />

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" CommandArgument="1" OnClick="btn_section_Click">기본정보</asp:LinkButton></li>
            <li runat="server" id="tabm2" class=""><asp:LinkButton runat="server" id="btnApplyTab" CommandArgument="2" OnClick="btn_section_Click">신청/참여</asp:LinkButton></li>
		
            
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">
					  
                    <div class="form-group" id="EventIdDiv" runat="server">
						<label class="col-sm-2 control-label control-label">이벤트ID</label>
						<div class="col-sm-10">
							<label runat=server ID="evt_id" CssClass="form-control" style="padding-top:7px;"></label>
                            
						</div>
					</div>

					<div class="form-group form-type form-date form-count form-complex " id="date_apply" style="display:block;">
						<label class="col-sm-2 control-label control-label">이벤트 기간</label>
						<div class="col-sm-10">
							<button class="btn btn-primary btn-sm dayrange dayrange1 " data-from="e_begin" data-end="e_end"><i class="fa fa-calendar"></i></button>
							<asp:TextBox runat="server" ID="e_begin" Width="100px"  data-range="dayrange1" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
								~
							<asp:TextBox runat="server" ID="e_end" Width="100px"  data-range="dayrange1" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>
					</div>
                    
                        

                    <div class="form-group">
						<label class="col-sm-2 control-label control-label">제목</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=e_title CssClass="form-control" Width=600 MaxLength="50"></asp:TextBox>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10">
							<textarea name="e_content" id="e_content" runat="server" style="width:600px; height:212px; display:none;"></textarea>
                        </div>
					</div>
                        

                    <asp:PlaceHolder runat="server" ID="ph_regdate" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="e_regdate"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>
				</div>
	
	             <div class="box-footer clearfix text-center">
         
		            <asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		            <asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
                    <a class="btn btn-bitbucket pull-right " runat="server" id="btn_apply_download" style="margin-right:5px;">다운로드</a>
		            <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	            </div>
			</div>
		    
			<div class="tab-pane" id="tab2" runat="server" >


                <div class="box-header">
                    <span class="pull-right">
                    <input type="text" runat="server" id="s_keyword2" class="form-control inline" style="width:300px; margin-right:10px;" OnTextChanged="search" AutoPostBack="true" placeholder="아이디, 이름검색" />
						<asp:LinkButton runat="server"  class="btn btn-primary inline" style="width:70px;display:inline-block;vertical-align:top;margin-top:2px" OnClick="search2">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
                        </span>
                </div>
				<div class="box-header">
					<h3 class="box-title">총 <asp:Literal runat="server" ID="lbTotal" /> 건</h3>
					<span class="pull-right">
						<uc:paging runat="server" ID="paging_apply" OnNavigate="paging_apply_Navigate" EnableViewState="true" RowsPerPage="20" PagesPerGroup="10" Hash="p2" />
						<a href="#" class="btn btn-bitbucket pull-right btn_apply_download" runat="server" id="btn_apply_download2" style="margin-left:5px">다운로드</a>
                        <a href="#" class="btn btn-danger pull-right btn_winner_upload" runat="server" id="btn_winner_upload" style="margin-left:5px">당첨자 업로드</a>
					</span>
				</div>
				<!-- /.box-header -->

				<div class="box-body table-responsive no-padding">
					
					<table class="table table-hover table-bordered ">
						<colgroup>
							<col width="5%" />
							<col width="10%" />
							<col width="10%" />
                            <col width="10%" />
							<col width="20%" />
							<col width="20%" />
							<col width="10%" />
							<col width="15%" />
						</colgroup>
						<thead>
							<tr>
								<th>번호</th>
								<th>SponsorID</th>
								<th>ConID</th>
								<th>UserID</th>
                                <th>경로</th>
								<th>후원종류</th>
                                <th>후원금액</th>
                                <th>등록일자</th>
							</tr>
						</thead>
						<tbody>

							<asp:Repeater runat="server" ID="repeater_apply" OnItemDataBound="repeater_apply_ItemDataBound">
								<ItemTemplate>
									<tr class="tr_over">
										<td><%--<%#Eval("rownum" )%>--%><asp:Literal runat=server ID=lbIdx></asp:Literal></td>
                                        <td><%#Eval("SponsorID" )%></td>
                                        <td><%#Eval("ConID" )%></td>
                                        <td><%#Eval("UserID" )%></td>
                                        <td><%#Eval("evt_Route" )%></td>
                                        <td><%#Eval("SponsorType" )%></td>
                                        <td><%#Eval("SponsorAmount" )%></td>
                                        <td><%#Eval("regdate" , "{0:yyyy'.'MM'.'dd HH:mm}")%></td>

										<%--<td><asp:Literal runat="server" ID="lbUserId"></asp:Literal> </td>
										<td><%#Eval("er_name" )%></td>
										<td><%#Eval("er_phone" )%></td>
                                        <td> <asp:Literal ID="lbDate" runat="server"/></td>

										<td>
                                            <asp:DropDownList runat="server" ID="member_count" Visible="false" CssClass="form-control" style="display:inline-block;text-align:center">
                                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:TextBox runat="server" ID="team_count" Visible="false" CssClass="form-control"></asp:TextBox>

                                            <asp:Literal runat="server" ID="none_count" Visible="false">-</asp:Literal>
										</td>

                                        <td><%#Eval("er_route" )%></td>
                                        <td><%#Eval("BirthDate" )%></td>
                                        <td><%#Eval("IsSponsor" )%></td>
                                        <td><%#Eval("er_regdate" , "{0:yyyy'.'MM'.'dd HH:mm}")%></td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="attended" CssClass="form-control" style="width:70px;display:inline-block;text-align:center">
                                                <asp:ListItem Text="참석" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="불참" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="related" CssClass="form-control" style="width:100px;display:inline-block;text-align:center">
                                                <asp:ListItem Text="결연" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="미결연" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
										<td><%#Eval("ew_id").ToString() == "-1" ? "X" : "O" %></td>
                                        <td>
                                            <asp:LinkButton runat="server" ID="btnRemove" OnClick="btnRemove_Click" OnClientClick="return confirm('삭제하시겠습니까?');" CommandArgument=<%#Eval("er_id") %>  CssClass="btn btn-danger">삭제</asp:LinkButton>
                                        </td>--%>

									</tr>
								</ItemTemplate>
								<FooterTemplate>
									<tr runat="server" visible="<%#repeater_apply.Items.Count == 0 %>">
										<td colspan="12">데이터가 없습니다.</td>
									</tr>
								</FooterTemplate>
							</asp:Repeater>

						</tbody>
					</table>
				</div>

                <div class="box-footer clearfix text-right">
		            <asp:LinkButton runat=server ID=btnUpdate OnClick="btnUpdate_Click" OnClientClick="return onUpdate();" class="btn btn-bitbucket">수정</asp:LinkButton>
	            </div>

			</div> 

			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->
    
</asp:Content>