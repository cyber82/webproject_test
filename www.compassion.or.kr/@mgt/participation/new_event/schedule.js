﻿$page = {
	defaultDate: '',
	selectedDate: '',
	scheduleData: {},
	e_begin: '',
	e_end: '',
	type : '',
	init: function () {

		 


		$('#calendar').fullCalendar({
			lang: "ko",
			events: [{
				start: $page.e_begin,
				end: $page.e_end,
				rendering: 'background',
				color: '#ffd5cc'
			}],
			ignoreTimezone: true,
			selectable: true,
			timezone: 'local',
			select: function (start, end, jsEvent, view, resources) {
                /*
				// 체험전
				if ($page.type == "experience") {

					var selectedBegin = moment(start).format('YYYY-MM-DD');
					var selectedEnd = moment(end).format('YYYY-MM-DD');

					// 하루만 선택가능
					if (start.add('days', 1).date() != end.date()) {
						$page.reset();
						return;
					}

					// 참여기간 외
					if (selectedBegin < $page.e_open || selectedEnd > $page.e_close) {
						alert('참여기간에서 선택해주세요.');
						$page.reset();
						return;
					} else {
						$page.schedule.init(selectedBegin);
						$page.selectedDate = selectedBegin;
					}
				}

				// 이벤트
				else {

				}
                */

			}
		});

		// 오늘
		$page.defaultDate = $("#calendar").fullCalendar('getDate').format("YYYY-MM-DD");
		$page.selectedDate = $page.defaultDate;

		
		$page.schedule.init($page.defaultDate);
		
	},

	reset: function () {
		$("#calendar").fullCalendar('unselect');
		$page.selectedDate = $page.defaultDate;
		$page.schedule.init($page.defaultDate);
	},


	schedule: {
		init: function (initDate) {
			
			if ($('#schedule').children().length > 0) {
				$("#schedule").fullCalendar('destroy');
			}

			var options = {
				defaultView: 'agendaDay',
				minTime: '09:00:00',
				maxTime: '22:00:00',
				contentHeight: 720,
				timezone: 'local',
				selectable: true,
				select: function (start, end, jsEvent, view, resources) {
 

					var b_date = start.toDate();
					var e_date = end.toDate();

					var jsonData = [];
					while (b_date < e_date) {
						jsonData.push({
							date: moment(b_date).format('YYYY-MM-DD HH:mm:ss')
						});
						b_date = new Date(b_date.getTime() + 30 * 60000);
					}

					$page.scheduleData[$page.selectedDate] = jsonData;

				},
				eventClick: function (calEvent, jsEvent, view) {


					var title = calEvent.title;

					// data- 속성 검색
					var data_id = $.grep(this.className.split(" "), function (v, i) {
						return v.indexOf('data-') === 0;
					})[0]

					 
				},
				eventRender: function (event, element) {

					// 제목을 테그로
					element.find('.fc-title').html(event.title);

					// 닫기버튼에 시간 지우기
					if (element.hasClass("schedule-label")) {
						element.find(".fc-time").remove();
					}

				},

				eventAfterRender: function (event, element, view) {
					 

					// 지우기 버튼
					if (event.title == "지우기") {
						element.css("font-size", "1.5em");
					}
				}

			}


			if (initDate) {
				options['defaultDate'] = initDate;

				// 선택한 날의 시간표 출력

				if ($page.scheduleData[initDate]) {

					var data = $page.scheduleData[initDate];

					//console.log(data);

					options['events'] = [];
					$.each(data, function () {
						var option = {
							start: moment(this.date),
							end: (moment(this.date).add(30, 'minutes')),
							rendering: 'background'
						};
						 


						options['events'].push(option);

					});


					// 초기화 버튼
					var btnInit = {
						title : "지우기",
						allDay: true,
						start: initDate,
						color: "#f39c12"
					}

					options['events'].push(btnInit);
				}
			}
			$("#schedule").fullCalendar(options);

		}
	}



}