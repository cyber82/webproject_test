﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_participation_pastor_default : AdminBoardPage {


	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;
		btn_add.Attributes.Add("href", "update.aspx");
	}

	protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();

		btn_add.Attributes.Add("href", "update.aspx");
	}

	protected override void GetList(int page) {

		Master.IsSearch = false;
		if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
			Master.IsSearch = true;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.sp_visiontrip_list(page, paging.RowsPerPage, "pastor", "", s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "trip_view", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, "pastor", "", s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_visiontrip_list", op1, op2).DataTableToList<sp_visiontrip_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				sp_visiontrip_listResult entity = e.Item.DataItem as sp_visiontrip_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
				//((Literal)e.Item.FindControl("lbClose")).Text = Convert.ToDateTime(entity.close_date) < DateTime.Now ? "O" : "X";
				((Literal)e.Item.FindControl("lbDisplay")).Text = entity.trip_view == 'Y' ? "O" : "X";

				string status = "";

				var open = Convert.ToDateTime(entity.open_date);
				var close = Convert.ToDateTime(entity.close_date);

				if (open.CompareTo(DateTime.Now) > 0) {
					status = string.Format("예정");
				} else if (close.CompareTo(DateTime.Now) < 0) {
					status = "마감";
				} else {
					status = "신청중";
				}

				((Literal)e.Item.FindControl("display_status")).Text = status;
			}
		}

	} 
	
}
