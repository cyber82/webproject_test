﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_participation_pastor_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">

	    var onSubmit = function () {

			if (!validateForm([
				{ id: "#visit_nation", msg: "방문국가를 입력하세요" },
				{ id: "#start_date", msg: "일정 시작일을 입력하세요" },
				{ id: "#end_date", msg: "일정 종료일을 입력하세요" },
				//{ id: "#expect_num", msg: "예상인원을 입력하세요" },
				//{ id: "#request_cost", msg: "신청비를 입력하세요" },
				{ id: "#trip_cost", msg: "총 여행경비를 입력하세요" },
                { id: "#close_date", msg: "신청마감일를 입력하세요" }
			])) {
				return false;
			}

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<style type="text/css">
						#b_display label{margin-left:5px;width:50px}
					</style>
					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="trip_view" Checked="true" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">방문국가</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=visit_nation CssClass="form-control" Width=400></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">일정</label>
						<div class="col-sm-10">
							
							<button class="btn btn-primary btn-sm dayrange" data-from="start_date" data-end="end_date"><i class="fa fa-calendar"></i></button>
							<asp:TextBox runat="server" ID="start_date" Width="100px" CssClass="form-control inline date" Style="margin-right: 5px;"></asp:TextBox>
								~
							<asp:TextBox runat="server" ID="end_date" Width="100px" CssClass="form-control inline date" Style="margin-right: 5px;"></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group" style="display:none">
						<label class="col-sm-2 control-label control-label">예상인원</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=expect_num CssClass="form-control" Width=200></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group" style="display:none">
						<label class="col-sm-2 control-label control-label">신청비</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=request_cost CssClass="form-control" Width=200></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">예상 트립비용<br />(신청비 20만원 포함)</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=trip_cost CssClass="form-control" Width=400></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">신청일</label>
						<div class="col-sm-10">
							<button class="btn btn-primary btn-sm dayrange" data-from="open_date" data-end="close_date"><i class="fa fa-calendar"></i></button>
							<asp:TextBox runat="server" ID="open_date" Width="100px" CssClass="form-control inline date" Style="margin-right: 5px;"></asp:TextBox>
								~
							<asp:TextBox runat="server" ID="close_date" Width="100px" CssClass="form-control inline date" Style="margin-right: 5px;"></asp:TextBox>
						</div>
					</div>

                    <asp:PlaceHolder runat="server" ID="ph_regdate" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="reg_date"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>