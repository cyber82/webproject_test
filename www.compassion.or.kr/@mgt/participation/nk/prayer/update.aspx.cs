﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_participation_nk_prayer_update : AdminBoardPage {

	protected string File1 {
		set {
				this.ViewState["File1"] = value;
		}
		get {
			return (this.ViewState["File1"] != null) ? this.ViewState["File1"].ToString() : "";
		}
	}
	
	protected string File2 {
		set {
				this.ViewState["File2"] = value;
		}
		get {
			return (this.ViewState["File2"] != null) ? this.ViewState["File2"].ToString() : "";
		}
	}
	
	
	protected string File3 {
		set {
				this.ViewState["File3"] = value;
		}
		get {
			return (this.ViewState["File3"] != null) ? this.ViewState["File3"].ToString() : "";
		}
	}
	
	
	protected string File4 {
		set {
				this.ViewState["File4"] = value;
		}
		get {
			return (this.ViewState["File4"] != null) ? this.ViewState["File4"].ToString() : "";
		}
	}
	
	
	protected string File5 {
		set {
				this.ViewState["File5"] = value;
		}
		get {
			return (this.ViewState["File5"] != null) ? this.ViewState["File5"].ToString() : "";
		}
	}



	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");

		pd_yyyy.Items.Add(new ListItem("선택하세요", ""));
		var year = Convert.ToInt32(DateTime.Now.ToString("yyyy"));

		for (int i = 0; i < 10; i++) {
			pd_yyyy.Items.Add(new ListItem((year- i).ToString() + "년", (year- i).ToString()));
		}

		pd_mm.Items.Add(new ListItem("선택하세요", ""));
		for (int i=1; i<13; i++) {
			pd_mm.Items.Add(new ListItem(i + "월", i.ToString("00")));
		}

		pd_mmww.Items.Add(new ListItem("선택하세요", ""));
		for (int i = 1; i < 6; i++) {
			pd_mmww.Items.Add(new ListItem(i.ToString() + "주", i.ToString()));
		}


		if (base.Action == "update") {

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.TB_PRAYER_DOC.First(p => p.PD_SEQ == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<TB_PRAYER_DOC>("PD_SEQ", Convert.ToInt32(PrimaryKey));

                //pd_yyyy.SelectedValue = entity.PD_YYYYMM.Substring(0, 4);
                //pd_mm.SelectedValue = entity.PD_YYYYMM.Substring(4, 2);
                //pd_mmww.SelectedValue = entity.PD_MMWW.ToString();
                pd_subject.Text = entity.PD_SUBJECT;
                pd_content.InnerHtml = entity.PD_CONTENT;
                pd_view_yn.Checked = entity.PD_VIEW_YN == "Y";
                pd_main_yn.Checked = entity.PD_MAIN_YN;

                pd_date.Text = entity.PD_DATE;


                Thumb = entity.PD_MAIN_TN;
                File1 = entity.PD_MB_ATTACH;
                File2 = entity.PD_MB_ATTACH2;
                File3 = entity.PD_MB_ATTACH3;
                File4 = entity.PD_MB_ATTACH4;
                File5 = entity.PD_MB_ATTACH5;
            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
            
			this.ShowFiles();


		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;

			pd_date.Text = DateTime.Now.ToString("yyyy-MM-dd");
		}

	}


	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var thumb = base.Thumb;

		var fileRoot = Uploader.GetRoot(Uploader.FileGroup.file_nk);

		var arg = new TB_PRAYER_DOC() {
			PD_YYYYMM = "",
			PD_DATE = pd_date.Text,
			PD_SUBJECT = pd_subject.Text,
			PD_CONTENT = pd_content.InnerHtml.ToHtml(),
			PD_MB_ATTACH = File1,
			PD_MB_ATTACH2=File2,
			PD_MB_ATTACH3=File3,
			PD_MB_ATTACH4=File4,
			PD_MB_ATTACH5=File5,
			PD_PRAYER_CNT=0,
			PD_VIEW_CNT = 0,
			PD_VIEW_YN = pd_view_yn.Checked ? "Y" : "N",
			PD_REGDATE = DateTime.Now,
			PD_UPDDATE = DateTime.Now,
			PD_MMWW = -1,
			PD_MAIN_TN = Thumb,
			PD_MAIN_YN= pd_main_yn.Checked,
			PD_A_ID = AdminLoginSession.GetCookie(this.Context).identifier,
		};


		var fileList = base.GetTemporaryFiles();

        using (AdminDataContext dao = new AdminDataContext())
        {
            if (pd_main_yn.Checked)
            {
                //foreach (var mainEntity in dao.TB_PRAYER_DOC.Where(p => p.PD_MAIN_YN).ToList())
                var list = www6.selectQ<TB_PRAYER_DOC>("PD_MAIN_YN", 1);
                foreach (var mainEntity in list)
                {
                    mainEntity.PD_MAIN_YN = false;
                }
            }

            if (base.Action == "update")
            {

                //var entity = dao.TB_PRAYER_DOC.First(p => p.PD_SEQ == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<TB_PRAYER_DOC>("PD_SEQ", Convert.ToInt32(PrimaryKey));

                entity.PD_YYYYMM = arg.PD_YYYYMM;
                entity.PD_DATE = arg.PD_DATE;
                entity.PD_SUBJECT = arg.PD_SUBJECT;
                entity.PD_CONTENT = arg.PD_CONTENT;
                entity.PD_MB_ATTACH = arg.PD_MB_ATTACH;
                entity.PD_MB_ATTACH2 = arg.PD_MB_ATTACH2;
                entity.PD_MB_ATTACH3 = arg.PD_MB_ATTACH3;
                entity.PD_MB_ATTACH4 = arg.PD_MB_ATTACH4;
                entity.PD_MB_ATTACH5 = arg.PD_MB_ATTACH5;
                entity.PD_VIEW_YN = arg.PD_VIEW_YN;
                entity.PD_MMWW = arg.PD_MMWW;
                entity.PD_MAIN_TN = arg.PD_MAIN_TN;
                entity.PD_MAIN_YN = arg.PD_MAIN_YN;
                entity.PD_UPDDATE = DateTime.Now;

                //string wClause = string.Format("PD_SEQ = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);
                //dao.SubmitChanges();


                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
            }
            else
            {
                //dao.TB_PRAYER_DOC.InsertOnSubmit(arg);
                //dao.SubmitChanges();
                www6.insert(arg);
                
                PrimaryKey = arg.PD_SEQ.ToString();

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";
            }

        }

			

	}


	protected void btn_remove_click(object sender, EventArgs e) {

		using (AdminDataContext dao = new AdminDataContext()) 
        {
            //var entity = dao.TB_PRAYER_DOC.First(p => p.PD_SEQ == Convert.ToInt32(PrimaryKey));
            //dao.TB_PRAYER_DOC.DeleteOnSubmit(entity);
            //dao.SubmitChanges();

            //Delete
            string delStr = string.Format("delete from TB_PRAYER_DOC where PD_SEQ = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
		}

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}


	// 파일
	void ShowFiles() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			btn_thumb.Src = base.Thumb.WithFileServerHost();
			btn_thumb.Attributes["data-exist"] = "1";
		}

		if (string.IsNullOrEmpty(this.File1)) {
			lnFileName1.Visible = false;
			fileButton1.Visible = false;
		} else {
			lnFileName1.NavigateUrl = this.File1.WithFileServerHost();
			lnFileName1.Visible = true;
			fileName1.Text = this.File1;
			fileButton1.Visible = true;
		}


		if (string.IsNullOrEmpty(this.File2)) {
			lnFileName2.Visible = false;
			fileButton2.Visible = false;
		} else {
			lnFileName2.NavigateUrl = this.File2.WithFileServerHost();
			lnFileName2.Visible = true;
			fileName2.Text = this.File2;
			fileButton2.Visible = true;
		}

		if (string.IsNullOrEmpty(this.File3)) {
			lnFileName3.Visible = false;
			fileButton3.Visible = false;
		} else {
			lnFileName3.NavigateUrl = this.File3.WithFileServerHost();
			lnFileName3.Visible = true;
			fileName3.Text = this.File3;
			fileButton3.Visible = true;
		}

		if (string.IsNullOrEmpty(this.File4)) {
			
			lnFileName4.Visible = false;
			fileButton4.Visible = false;
		} else {
			lnFileName4.NavigateUrl = this.File4.WithFileServerHost();
			lnFileName4.Visible = true;
			fileName4.Text = this.File4;
			fileButton4.Visible = true;
		}

		if (string.IsNullOrEmpty(this.File5)) {

			lnFileName5.Visible = false;
			fileButton5.Visible = false;
		} else {
			lnFileName5.NavigateUrl = this.File5.WithFileServerHost();
			lnFileName5.Visible = true;
			fileName5.Text = this.File5;
			fileButton5.Visible = true;
		}
	}

	protected void btn_remove_file_click(object sender, EventArgs e) {
		LinkButton obj = sender as LinkButton;

		switch(obj.CommandArgument) {
			case "file1":
				this.File1 = "";
				break;

			case "file2":
				this.File2 = "";
				break;

			case "file3":
				this.File3 = "";
				break;

			case "file4":
				this.File4 = "";
				break;

			case "file5":
				this.File5 = "";
				break;
		}


		this.ShowFiles();
	}



	protected void btn_update_temp_file_click(object sender, EventArgs e) {

		switch (temp_file_type.Value) {
			case "thumb":
				base.Thumb = temp_file_name.Value;
				break;

			case "file1":
				this.File1 = temp_file_name.Value;
				break;

			case "file2":
				this.File2 = temp_file_name.Value;
				break;

			case "file3":
				this.File3 = temp_file_name.Value;
				break;

			case "file4":
				this.File4 = temp_file_name.Value;
				break;

			case "file5":
				this.File5 = temp_file_name.Value;
				break;

		}

		this.ShowFiles();

	}
	
}