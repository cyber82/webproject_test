﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_participation_nk_prayer_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript">

		$(function () {
		    image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.file_nk)%>";
		    initEditor(oEditors, "pd_content");
            
		    var uploader;

			uploader = attachUploader("btn_thumb");
			uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_nk)%>";
		    uploader._settings.data.rename = "y";
            

			uploader = attachUploader("file1");
			uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_nk)%>";
		    uploader._settings.data.rename = "y";
            

		    uploader = attachUploader("file2");
			uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_nk)%>";
		    uploader._settings.data.rename = "y";
            

		    uploader = attachUploader("file3");
			uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_nk)%>";
		    uploader._settings.data.rename = "y";
            

		    uploader = attachUploader("file4");
			uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_nk)%>";
		    uploader._settings.data.rename = "y";
            

		    uploader = attachUploader("file5");
			uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_nk)%>";
		    uploader._settings.data.rename = "y";

		});



	    var attachUploader = function (button) {
	        return new AjaxUpload(button, {
	            action: '/common/handler/upload',
	            responseType: 'json',
	            onChange: function () {
	                // 이미지 업로드 되면 에디터 초기화 되는 현상 방지
	                // oEditors가 없는 경우 에러남
	                try {
	                    if (oEditors) {
	                        $.each(oEditors, function () {
	                            this.exec("UPDATE_CONTENTS_FIELD", []);
	                        });
	                    }
	                } catch (e) { }
	            },
	            onSubmit: function (file, ext) {
	                this.disable();
	            },
	            onComplete: function (file, response) {

	                this.enable();

	                if (response.success) {

	                    var c = $("#" + button).attr("data-id");
	                    $(".temp_file_type").val(c);
	                    $(".temp_file_name").val(response.name);
	                    $(".temp_file_size").val(response.size);

	                    eval($(".btn_update_temp_file").attr("href").replace("javascript:", ""));

	                } else
	                    alert(response.msg);
	            }
	        });
	    }



		var onSubmit = function () {

		    if (!validateForm([
                //{ id: "#pd_yyyy", msg: "관리년월을 선택하세요" },
                //{ id: "#pd_mm", msg: "관리년월을 선택하세요" },
                //{ id: "#pd_mmww", msg: "주차를 선택하세요" },
				{ id: "#pd_subject", msg: "제목을 입력하세요" },
                { id: "#pd_date", msg : "타입을 선택하세요" }
			])) {
				return false;
		    }
            /*
		    if ($("#pd_main_yn").is(":checked")) {
		        if ($("#btn_thumb").attr("data-exist") != "1") {
		            alert("상단노출인 경우 썸네일 이미지가 필요합니다.");
		            $("#btn_thumb").focus();
		            return false;
		        }
		    }
            */
			oEditors.getById["pd_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<style type="text/css">
						#PD_VIEW_YN label{margin-left:5px;width:50px}
					</style>
					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="pd_view_yn" Checked="true" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">상단노출</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="pd_main_yn" Checked="false" />
						</div>
					</div>

					<div class="form-group" style="display:none;">
						<label class="col-sm-2 control-label control-label">관리년월</label>
						<div class="col-sm-10">
							<asp:DropDownList runat="server" ID="pd_yyyy" CssClass="form-control inline" style="width:110px;margin-right:5px;"></asp:DropDownList>
                            <asp:DropDownList runat="server" ID="pd_mm" CssClass="form-control inline" style="width:110px;"></asp:DropDownList>
						</div>
					</div>
                    
					<div class="form-group" style="display:none;">
						<label class="col-sm-2 control-label control-label">주차</label>
						<div class="col-sm-10">
							<asp:DropDownList runat="server" ID="pd_mmww" CssClass="form-control inline" style="width:110px;margin-right:5px;"></asp:DropDownList>
						</div>
					</div>
                   
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">제목</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=pd_subject CssClass="form-control pd_subject" Width=600 MaxLength="50"></asp:TextBox>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10">
							<textarea name="pd_content" id="pd_content" runat="server" style="width:600px; height:212px; display:none;"></textarea>
                        </div>
					</div>

                    
					<div class="form-group" style="display:none;">
						<label class="col-sm-2 control-label control-label">썸네일</label>
						<div class="col-sm-10">
							<img id="btn_thumb" class="thumb" data-id="thumb" runat=server src="/@mgt/common/img/empty_thumb.png" style="max-width:400px;" />
                            <br /><div>※상단 노출인 경우에만 사용됩니다.</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">첨부파일1</label>
						<div class="col-sm-10">
							
                            <button type="button" class="btn btn-default btn-xs" id="file1" data-id="file1" style="margin-top:5px;margin-bottom:2px;">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 파일선택
                            </button><br />
							<asp:LinkButton runat=server ID=fileButton1 CssClass="btn btn-default btn-xs" Visible="false" Style="margin-bottom:2px" CommandArgument=file1 OnClick="btn_remove_file_click"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 삭제</asp:LinkButton>
                            <asp:HyperLink runat=server ID=lnFileName1 Visible="false"><asp:Literal runat=server ID=fileName1></asp:Literal></asp:HyperLink>
                        </div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">첨부파일2</label>
						<div class="col-sm-10">
							
                            <button type="button" class="btn btn-default btn-xs" id="file2" data-id="file2" style="margin-top:5px;margin-bottom:2px;">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 파일선택
                            </button><br />
							<asp:LinkButton runat=server ID=fileButton2 CssClass="btn btn-default btn-xs" Visible="false" Style="margin-bottom:2px" CommandArgument=file2 OnClick="btn_remove_file_click"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 삭제</asp:LinkButton>
                            <asp:HyperLink runat=server ID=lnFileName2 Visible="false"><asp:Literal runat=server ID=fileName2></asp:Literal></asp:HyperLink>
                        </div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">첨부파일3</label>
						<div class="col-sm-10">
							
                            <button type="button" class="btn btn-default btn-xs" id="file3" data-id="file3" style="margin-top:5px;margin-bottom:2px;">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 파일선택
                            </button><br />
							<asp:LinkButton runat=server ID=fileButton3 CssClass="btn btn-default btn-xs" Visible="false" Style="margin-bottom:2px" CommandArgument=file3 OnClick="btn_remove_file_click"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 삭제</asp:LinkButton>
                            <asp:HyperLink runat=server ID=lnFileName3 Visible="false"><asp:Literal runat=server ID=fileName3></asp:Literal></asp:HyperLink>
                        </div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">첨부파일4</label>
						<div class="col-sm-10">
							
                            <button type="button" class="btn btn-default btn-xs" id="file4" data-id="file4" style="margin-top:5px;margin-bottom:2px;">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 파일선택
                            </button><br />
							<asp:LinkButton runat=server ID=fileButton4 CssClass="btn btn-default btn-xs" Visible="false" Style="margin-bottom:2px" CommandArgument=file4 OnClick="btn_remove_file_click"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 삭제</asp:LinkButton>
                            <asp:HyperLink runat=server ID=lnFileName4 Visible="false"><asp:Literal runat=server ID=fileName4></asp:Literal></asp:HyperLink>
                        </div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">첨부파일5</label>
						<div class="col-sm-10">
							
                            <button type="button" class="btn btn-default btn-xs" id="file5" data-id="file5" style="margin-top:5px;margin-bottom:2px;">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 파일선택
                            </button><br />
							<asp:LinkButton runat=server ID=fileButton5 CssClass="btn btn-default btn-xs" Visible="false" Style="margin-bottom:2px" CommandArgument=file5 OnClick="btn_remove_file_click"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 삭제</asp:LinkButton>
                            <asp:HyperLink runat=server ID=lnFileName5 Visible="false"><asp:Literal runat=server ID=fileName5></asp:Literal></asp:HyperLink>
                        </div>
					</div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">등록일</label>
						<div class="col-sm-10">
                            <asp:TextBox runat="server" ID="pd_date" CssClass="form-control date" style="width:100px;"></asp:TextBox>
                        </div>
                    </div>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>