﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_participation_nk_news_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript">

		$(function () {
		    image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
		    initEditor(oEditors, "b_content");
            

			var uploader = attachUploader("btn_upload");
			uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_nk)%>";
		    uploader._settings.data.rename = "n";
		});



	    var attachUploader = function (button) {
	        return new AjaxUpload(button, {
	            action: '/common/handler/upload',
	            responseType: 'json',
	            onChange: function () {
	                // 이미지 업로드 되면 에디터 초기화 되는 현상 방지
	                // oEditors가 없는 경우 에러남
	                try {
	                    if (oEditors) {
	                        $.each(oEditors, function () {
	                            this.exec("UPDATE_CONTENTS_FIELD", []);
	                        });
	                    }
	                } catch (e) { }
	            },
	            onSubmit: function (file, ext) {
	                this.disable();
	            },
	            onComplete: function (file, response) {

	                this.enable();

	                if (response.success) {

	                    var c = $("#" + button).attr("class").replace(" ", "");
	                    $(".temp_file_type").val(c.indexOf("img_1") > -1 ? "thumb" : "file");
	                    $(".temp_file_name").val(response.name);
	                    $(".temp_file_size").val(response.size);

	                    eval($(".btn_update_temp_file").attr("href").replace("javascript:", ""));

	                } else
	                    alert(response.msg);
	            }
	        });
	    }



		var onSubmit = function () {

			if (!validateForm([
				{ id: "#b_title", msg: "제목을 입력하세요" },
                { id: "#b_type", msg : "타입을 선택하세요" }
			])) {
				return false;
			}
			oEditors.getById["b_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<style type="text/css">
						#b_display label{margin-left:5px;width:50px}
					</style>
					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="b_display" cssClass="form-control1" Checked="true" />
						</div>
					</div>
                    <!--
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">메인노출</label>
						<div class="col-sm-3" style="margin-top:5px;">
                            <asp:CheckBox runat=server ID=b_main />
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-2 control-label control-label">상단공지</label>
						<div class="col-sm-3" style="margin-top:5px;">
                            <asp:CheckBox runat=server ID=b_hot />
                        </div>
                    </div>
                    -->

                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">타입</label>
						<div class="col-sm-3" style="margin-top:5px;">
                            <asp:DropDownList runat="server" ID="b_type" CssClass="form-control" Width="200">
                                <asp:ListItem Text="선택하세요" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>


					<div class="form-group">
						<label class="col-sm-2 control-label control-label">제목</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=b_title CssClass="form-control b_title" Width=600 MaxLength="50"></asp:TextBox>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10">
							<textarea name="b_content" id="b_content" runat="server" style="width:600px; height:212px; display:none;"></textarea>
                        </div>
					</div>

                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">파일</label>
						<div class="col-sm-10">
							
                            <button type="button" class="btn btn-default btn-xs" id="btn_upload" style="margin-top:5px;margin-bottom:2px;">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 파일추가
                            </button><br />

					        <asp:Repeater runat=server ID=repeater OnItemDataBound=FileListBound>
						        <ItemTemplate>
							        <asp:LinkButton runat=server ID=btn_remove_file CssClass="btn btn-default btn-xs" Style="margin-bottom:2px" CommandArgument=<%#Eval("f_name")%> OnClick="btn_remove_file_click"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 삭제</asp:LinkButton>
							        <asp:HyperLink runat=server ID=lnFileName><asp:Literal runat=server ID=fileName></asp:Literal></asp:HyperLink>
							        | <asp:Literal runat=server ID=fileSize></asp:Literal>
							        | <asp:Literal runat=server ID=regdate></asp:Literal>
						
							        <br />
						        </ItemTemplate>
					        </asp:Repeater>	
                        </div>
					</div>

                    <asp:PlaceHolder runat="server" ID="ph_regdate" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="b_regdate"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>