﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_participation_nk_news_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");




		// 게시판 종류
		foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "board" && p.cd_key.StartsWith("nk_")).OrderBy(p => p.cd_order)) {
			b_type.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}


		if (base.Action == "update")
        {
            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));

                b_title.Text = entity.b_title;
                b_content.InnerHtml = entity.b_content;
                b_display.Checked = entity.b_display;
                b_main.Checked = entity.b_main;
                b_type.SelectedValue = entity.b_type;

                b_regdate.Text = entity.b_regdate.ToString("yyyy.MM.dd HH:mm");

            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			ph_regdate.Visible = true;
            base.FileLoad();
			this.ShowFiles();


		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}


	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var thumb = base.Thumb;

		var arg = new board() {
			b_title     = b_title.Text, b_content = b_content.InnerHtml.ToHtml(),
			b_a_id      = AdminLoginSession.GetCookie(this.Context).identifier,
			b_display   = b_display.Checked,
			b_hot       = b_type.SelectedValue == "nk_hot",
			b_main      = b_main.Checked,
			b_type      = b_type.SelectedValue,
			b_regdate   = DateTime.Now,
			b_hits      = 0
		};


		var fileList = base.GetTemporaryFiles();

		using (AdminDataContext dao = new AdminDataContext()) {
			if (base.Action == "update") {

                //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));
                entity.b_title = arg.b_title;
                entity.b_content = arg.b_content;
                entity.b_a_id = arg.b_a_id;
                entity.b_display = arg.b_display;
                entity.b_hot = arg.b_hot;
                entity.b_main = arg.b_main;
                entity.b_type = arg.b_type;
                //dao.SubmitChanges();
                www6.update(entity);
                
                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

				Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "수정되었습니다.";
			}
            else
            {
				arg.b_hits = 0;
                //dao.board.InsertOnSubmit(arg);
                www6.insert(arg);
                //dao.SubmitChanges();
                
                PrimaryKey = arg.b_id.ToString();

				foreach (var file in fileList)
                {
					file.f_ref_id = PrimaryKey;
				}

                SaveTemporaryFiles(fileList);

				base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

				Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "등록되었습니다.";
			}


            //dao.file.DeleteAllOnSubmit(dao.file.Where(p => p.f_ref_id == PrimaryKey && p.f_group == FileGroup.ToString()));
            string delStr = string.Format("delete from [file] where f_ref_id = '{0}' and f_group = '{1}'", PrimaryKey, FileGroup.ToString());
            www6.cud(delStr);

            //  if (fileList.Count > 0) {
            //	dao.file.InsertAllOnSubmit(fileList);
            //}
            foreach (file fi in fileList)
            {
                www6.insert(fi);
            }

			//dao.SubmitChanges();
		}

			

	}


	protected void btn_remove_click(object sender, EventArgs e) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
            //dao.board.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from board where b_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}


	// 파일
	void ShowFiles() {

		repeater.DataSource = base.GetTemporaryFiles();
		repeater.DataBind();
	}

	protected void btn_remove_file_click(object sender, EventArgs e) {
		base.FileRemove(sender);
		this.ShowFiles();
	}

	protected void btn_update_temp_file_click(object sender, EventArgs e) {

		base.FileAdd(Convert.ToInt32(temp_file_size.Value), temp_file_name.Value);
		this.ShowFiles();

	}



	protected string GetBoardType() {
		string result = "";

		return result;
	}
}