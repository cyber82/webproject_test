﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_participation_nk_partnership_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.BoardType = "notice";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context , "default.aspx");



		foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "location_type").OrderBy(p => p.cd_order)) {
			p_location.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}



		if (base.Action == "update")
        {
            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.partnership.First(p => p.p_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<partnership>("p_id", Convert.ToInt32(PrimaryKey));

                p_display.Checked = entity.p_display;
                p_location.SelectedValue = entity.p_location;
                p_name.Text = entity.p_name;
                p_url.Text = entity.p_url;
                p_regdate.Text = entity.p_regdate.ToString("yyyy-MM-dd hh:mm:ss");

            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			ph_regdate.Visible = true;
			
		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var arg = new partnership() {
			p_name = p_name.Text,
			p_location = p_location.SelectedValue,
			p_url= p_url.Text,
			p_display= p_display.Checked,
			p_regdate= DateTime.Now,
			p_a_id = AdminLoginSession.GetCookie(this.Context).identifier
		};


		using (AdminDataContext dao = new AdminDataContext()) {


			if (base.Action == "update") {

                //var entity = dao.partnership.First(p => p.p_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<partnership>("p_id", Convert.ToInt32(PrimaryKey));

                entity.p_name = arg.p_name;
				entity.p_location = arg.p_location;
				entity.p_url= arg.p_url;
				entity.p_display = arg.p_display;

                //string wClause = string.Format("p_id = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);

				Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "수정되었습니다.";
				//dao.SubmitChanges();


				base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
			}
            else
            {
				//dao.partnership.InsertOnSubmit(arg);
                www6.insert(arg);

                Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "등록되었습니다.";
				//dao.SubmitChanges();

				base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));
			}

		}

	}

	protected void btn_remove_click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.partnership.First(p => p.p_id == Convert.ToInt32(PrimaryKey));
            //dao.partnership.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from partnership where p_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}
}