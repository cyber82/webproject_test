﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_participation_nk_partnership_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">

	    var onSubmit = function () {

			if (!validateForm([
				{ id: "#p_location", msg: "지역을 선택하세요" },
				{ id: "#p_name", msg: "교회명을 입력하세요" }
			])) {
				return false;
			}

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<style type="text/css">
						#b_display label{margin-left:5px;width:50px}
					</style>
					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="p_display" Checked="true" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">지역</label>
						<div class="col-sm-10">
							<asp:DropDownList runat="server" ID="p_location" CssClass="form-control" style="width:150px;">
                                <asp:ListItem Text="선택하세요" Value=""></asp:ListItem>
							</asp:DropDownList>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">교회명</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=p_name CssClass="form-control" Width=300></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">홈페이지</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=p_url CssClass="form-control" Width=600></asp:TextBox>
						</div>
					</div>
                    

                    <asp:PlaceHolder runat="server" ID="ph_regdate" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="p_regdate"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>