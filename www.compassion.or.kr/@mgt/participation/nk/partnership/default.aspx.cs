﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_participation_nk_partnership_default : AdminBoardPage {


	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;
		btn_add.Attributes.Add("href", "update.aspx");

		foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "location_type").OrderBy(p => p.cd_order)) {
			p_location.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}

	}

	protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();

		btn_add.Attributes.Add("href", "update.aspx");
	}

	protected override void GetList(int page) {

		Master.IsSearch = false;
		if (p_location.SelectedValue != "" || s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
			Master.IsSearch = true;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.sp_partnership_list(page, paging.RowsPerPage, -1, p_location.SelectedValue, s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "display", "location", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, -1, p_location.SelectedValue, s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_partnership_list", op1, op2).DataTableToList<sp_partnership_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				sp_partnership_listResult entity = e.Item.DataItem as sp_partnership_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
				((Literal)e.Item.FindControl("lbDisplay")).Text = entity.p_display ? "O" : "X";

			}
		}

	} 
	
}
