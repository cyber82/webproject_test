﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_participation_nk_subscription_download : AdminBoardPage{

    protected override void OnBeforePostBack(){

		var keyword = Request["k"].EmptyIfNull();
		var b_date = Request["b"].EmptyIfNull();
		var e_date = Request["e"].EmptyIfNull();
		base.OnBeforePostBack();

		this.GetList();


		// 익스에서 한글, 띄어쓰기 깨짐
		HttpBrowserCapabilities brObject = Request.Browser;
		var browser = brObject.Type.ToLower();
		var fileName = "북한사역_구독자_" + DateTime.Now.ToString("yyyyMMdd")+ ".xls";
		if (browser.Contains("ie")) {
			fileName = System.Web.HttpUtility.UrlEncode(fileName);
		}


		Response.Clear();
		Response.Charset = "utf-8";
		Response.ContentType = "application/vnd.ms-excel";
		Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
	}


	protected void GetList() {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.sp_nk_subscription_list(1, 100000, "", "", "", "").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { 1, 100000, "", "", "", "" };
            var list = www6.selectSP("sp_nk_subscription_list", op1, op2).DataTableToList<sp_nk_subscription_listResult>().ToList();

            repeater.DataSource = list;
            repeater.DataBind();

            base.WriteLog(AdminLog.Type.select, string.Format("북한사역 구독자 다운로드 ({0})", DateTime.Now.ToString("yyyyMMdd")));
        }
	}

}
