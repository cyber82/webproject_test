﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="download.aspx.cs" Inherits="mgt_participation_nk_subscription_download"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head runat="server">
    <title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
		html{background:#fff}
		table { border-collapse:initial; }  
		th, td { border:1px solid #000000;}
		td { mso-number-format:\@; }
	</style>
</head>
<body>
    <form id="form" runat="server">
		<table class="listBoard">
			<tbody>	
				<tr>
                    <!--
					<th style="width:50px">번호</th>
                    -->
                    <th style="width:150px">아이디</th>
					<th style="width:150px">이름</th>
					<th style="width:150px">타입</th>
					<th style="width:150px">정보</th>
                    <th style="width:150px">등록일</th>
					
				</tr>		
				<asp:Repeater runat=server ID=repeater >
					<ItemTemplate>
						<tr>
                            <!--
							<td><%#Container.ItemIndex + 1 %></td>
                            -->
                            <td><%#Eval("UserID")%></td>
						    <td class="text-left"><%#Eval("SponsorName")%></td>
                            <td><%#Eval("ns_type")%></td>
                            <td><%#Eval("ns_type").ToString() == "sms" ? Eval("Mobile") : Eval("Email")%></td>
						    <td><%#Eval("ns_regdate" , "{0:yy.MM.dd}")%></td>
						</tr>	
					</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								<td colspan="8">데이터가 없습니다.</td>
							</tr>
					</FooterTemplate>
				</asp:Repeater>	
				
			</tbody>
		</table>
    </form>
</body>
</html>