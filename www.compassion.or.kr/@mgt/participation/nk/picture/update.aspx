﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="_mgt_participation_nk_picture_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" %>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript">

		var onSubmit = function () {

		    if (!validateForm([
                { id: "#pd_subject", msg: "제목을 입력하세요" },
                { id: "#pd_date", msg : "타입을 선택하세요" }
			])) {
				return false;
		    }

			oEditors.getById["pd_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<style type="text/css">
						#PD_VIEW_YN label{margin-left:5px;width:50px}
					</style>
					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="pd_view_yn" Checked="true" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">상단노출</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="pd_main_yn" Checked="false" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">제목</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=pd_subject CssClass="form-control pd_subject" Width=600 MaxLength="50"></asp:TextBox>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">영상주소</label>
						<div class="col-sm-10">
                            <asp:TextBox runat=server ID=pd_content CssClass="form-control pd_subject" Width=600 MaxLength="50"></asp:TextBox>
                        </div>
					</div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">등록일</label>
						<div class="col-sm-10">
                            <asp:TextBox runat="server" ID="pd_date" CssClass="form-control date" style="width:100px;"></asp:TextBox>
                        </div>
                    </div>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>
