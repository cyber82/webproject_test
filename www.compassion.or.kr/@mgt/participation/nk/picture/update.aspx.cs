﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class _mgt_participation_nk_picture_update : AdminBoardPage
{
    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();
        v_admin_auth auth = base.GetPageAuth();
        base.PrimaryKey = Request["c"];
        base.Action = Request["t"];

        btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

        // querystring 유효성 검사 후 문제시 리다이렉트
        var isValid = new RequestValidator()
            .Add("c", RequestValidator.Type.Numeric)
            .Add("t", RequestValidator.Type.Alphabet)
            .Add("s_main", RequestValidator.Type.AlphaNumeric)
            .Validate(this.Context, "default.aspx");
        
        if (base.Action == "update")
        {

            using (AdminDataContext dao = new AdminDataContext())
            {
                var entity = www6.selectQF<TB_PRAYER_PICTURE>("PP_SEQ", Convert.ToInt32(PrimaryKey));

                pd_subject.Text = entity.PP_SUBJECT;
                pd_content.Text = entity.PP_CONTENT;
                pd_view_yn.Checked = entity.PP_VIEW_YN == "Y";
                pd_main_yn.Checked = entity.PP_MAIN_YN;

                pd_date.Text = entity.PP_DATE;
            }
            btn_update.Visible = auth.aa_auth_update;
            btn_remove.Visible = auth.aa_auth_delete;
            
        }
        else
        {

            btn_update.Visible = auth.aa_auth_create;
            btn_remove.Visible = false;

            pd_date.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

    }


    protected override void OnAfterPostBack()
    {
        base.OnAfterPostBack();
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
    }

    protected void btn_update_click(object sender, EventArgs e)
    {
        var arg = new TB_PRAYER_PICTURE()
        {
            PP_DATE = pd_date.Text,
            PP_SUBJECT = pd_subject.Text,
            PP_CONTENT = pd_content.Text,
            PP_PRAYER_CNT = 0,
            PP_VIEW_CNT = 0,
            PP_VIEW_YN = pd_view_yn.Checked ? "Y" : "N",
            PP_REGDATE = DateTime.Now,
            PP_UPDDATE = DateTime.Now,
            PP_MAIN_YN = pd_main_yn.Checked
        };
        
        using (AdminDataContext dao = new AdminDataContext())
        {
            if (pd_main_yn.Checked)
            {
                var list = www6.selectQ<TB_PRAYER_PICTURE>("PP_MAIN_YN", 1);
                foreach (var mainEntity in list)
                {
                    mainEntity.PP_MAIN_YN = false;
                }
            }

            if (base.Action == "update")
            {
                var entity = www6.selectQF<TB_PRAYER_PICTURE>("PP_SEQ", Convert.ToInt32(PrimaryKey));

                entity.PP_DATE = arg.PP_DATE;
                entity.PP_SUBJECT = arg.PP_SUBJECT;
                entity.PP_CONTENT = arg.PP_CONTENT;
                entity.PP_VIEW_YN = arg.PP_VIEW_YN;
                entity.PP_MAIN_YN = arg.PP_MAIN_YN;
                entity.PP_UPDDATE = DateTime.Now;

                //string wClause = string.Format("PD_SEQ = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);
                //dao.SubmitChanges();


                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
            }
            else
            {
                //dao.TB_PRAYER_DOC.InsertOnSubmit(arg);
                //dao.SubmitChanges();
                www6.insert(arg);

                PrimaryKey = arg.PP_SEQ.ToString();

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";
            }

        }



    }


    protected void btn_remove_click(object sender, EventArgs e)
    {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.TB_PRAYER_DOC.First(p => p.PD_SEQ == Convert.ToInt32(PrimaryKey));
            //dao.TB_PRAYER_DOC.DeleteOnSubmit(entity);
            //dao.SubmitChanges();

            //Delete
            string delStr = string.Format("delete from TB_PRAYER_PICTURE where PD_SEQ = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

        Master.ValueAction.Value = "list";
        Master.ValueMessage.Value = "삭제되었습니다.";
    }
    
}