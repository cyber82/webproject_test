﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_participation_sponsor_company_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.BoardType = "notice";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context , "default.aspx");


        using (AdminDataContext dao = new AdminDataContext())
        {
            if (base.Action == "update")
            {
                //var entity = dao.company.First(p => p.c_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<company>("c_id", Convert.ToInt32(PrimaryKey));

                c_name.Text = entity.c_name;
                c_url.Text = entity.c_url;
                c_display.Checked = entity.c_display;
                c_order.Text = entity.c_order.ToString();
                c_regdate.Text = entity.c_regdate.ToString("yyyy-MM-dd hh:mm:ss");

                this.Thumb = entity.c_thumb;

                btn_update.Visible = auth.aa_auth_update;
                btn_remove.Visible = auth.aa_auth_delete;
                ph_regdate.Visible = true;


                ShowFiles();

            }
            else
            {

                var order = 1;
                var list = www6.selectQ<company>();

                if (list.Count() > 0)
                {
                    order = list.Max(p => p.c_order) + 1;
                }
                c_order.Text = order.ToString();

                btn_update.Visible = auth.aa_auth_create;
                btn_remove.Visible = false;
            }
        }

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var arg = new company() {
			c_name = c_name.Text,
			c_thumb = this.Thumb,
			c_url= c_url.Text,
			c_display= c_display.Checked,
			c_order = Convert.ToInt32( c_order.Text),
			c_regdate= DateTime.Now,
			c_a_id = AdminLoginSession.GetCookie(this.Context).identifier
		};


		using (AdminDataContext dao = new AdminDataContext()) {


			if (base.Action == "update") {

                //var entity = dao.company.First(p => p.c_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<company>("c_id", Convert.ToInt32(PrimaryKey));

                entity.c_name = arg.c_name;
				entity.c_thumb = arg.c_thumb;
				entity.c_url= arg.c_url;
				entity.c_order = arg.c_order;
				entity.c_display = arg.c_display;

				Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "수정되었습니다.";

                //dao.SubmitChanges();
                //string wClause = string.Format("c_id = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
			}
            else
            {
                //dao.company.InsertOnSubmit(arg);
                www6.insert(arg);

                Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "등록되었습니다.";
				//dao.SubmitChanges();

				base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));
			}

		}

	}

	protected void btn_remove_click(object sender, EventArgs e) {

		using (AdminDataContext dao = new AdminDataContext()) {
            //var entity = dao.company.First(p => p.c_id == Convert.ToInt32(PrimaryKey));
            //dao.company.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from company where c_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
		}

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}




	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		base.Thumb = temp_file_name.Value;
		this.ShowFiles();

	}

	void ShowFiles() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			c_thumb.Src = base.Thumb.WithFileServerHost();
			c_thumb.Attributes["data-exist"] = "1";
		}
	}
}