﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_participation_sponsor_company_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
	    $(function () {
            // 썸네일
		    var thumbUploader = attachUploader("c_thumb");
		    thumbUploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.image_company)%>";
	        thumbUploader._settings.data.rename = "y";
	    });



	    var attachUploader = function (button) {
	        return new AjaxUpload(button, {
	            action: '/common/handler/upload',
	            responseType: 'json',
	            onChange: function () {
	                // 이미지 업로드 되면 에디터 초기화 되는 현상 방지
	                // oEditors가 없는 경우 에러남
	                try {
	                    if (oEditors) {
	                        $.each(oEditors, function () {
	                            this.exec("UPDATE_CONTENTS_FIELD", []);
	                        });
	                    }
	                } catch (e) { }
	            },
	            onSubmit: function (file, ext) {
	                this.disable();
	            },
	            onComplete: function (file, response) {
	                this.enable();
	                if (response.success) {
	                    var c = $("#" + button).attr("class").replace(" ", "");
	                    $(".temp_file_type").val(c.indexOf("image") > -1 ? "thumb" : "file");
	                    $(".temp_file_name").val(response.name);
	                    $(".temp_file_size").val(response.size);
	                    eval($(".btn_update_temp_file").attr("href").replace("javascript:", ""));
	                } else
	                    alert(response.msg);
	            }
	        });
	    }

	    var onSubmit = function () {

			if (!validateForm([
				{ id: "#c_name", msg: "기업/단체명을 입력하세요." },
                { id: "#c_order", msg: "순서를 입력하세요.", type : "numeric" }
			])) {
				return false;
			}

			if (!$("#c_thumb").is('[data-exist]')) {
			    alert("로고이미지를 등록하세요.");
			    $("#c_thumb").focus();
			    return false;
			}

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>


    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<style type="text/css">
						#b_display label{margin-left:5px;width:50px}
					</style>
					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="c_display" Checked="true" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">기업/단체명</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=c_name CssClass="form-control" Width=300></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">홈페이지</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=c_url CssClass="form-control" Width=600></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">로고<br />(170*80px) </label>
						<div class="col-sm-10">
							<img id="c_thumb" class="c_thumb" runat=server src="/@mgt/common/img/empty_thumb.png" style="max-width:400px;" />
						</div>
					</div>
                    
                    
					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">순서</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=c_order CssClass="form-control number_only" Width=100></asp:TextBox><br />
                            <span>* 높은 숫자가 뒤에 나타납니다.</span>
						</div>
					</div>

                    <asp:PlaceHolder runat="server" ID="ph_regdate" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="c_regdate"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>