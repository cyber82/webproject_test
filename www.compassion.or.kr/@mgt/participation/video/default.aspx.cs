﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_particitipation_video_default : AdminBoardPage{


	const int VISIBLE_COUNT = 3;

    protected override void OnBeforePostBack(){
        base.OnBeforePostBack();

        Master.Content.Controls.BindAllTextBox(this.Context);

        base.LoadComplete += new EventHandler(list_LoadComplete);
        v_admin_auth auth = base.GetPageAuth();
        btn_add.Visible = auth.aa_auth_create;


		// 노출중인 컨텐츠만 가져오기
		using (AdminDataContext dao = new AdminDataContext()) {
            //visible_list.Value = dao.sp_promotional_video_list(1, VISIBLE_COUNT, "pv_order", 1, "", "", "").ToJson();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "sort_column", "display", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { 1, VISIBLE_COUNT, "pv_order", 1, "", "", "" };
            var list = www6.selectSP("sp_promotional_video_list", op1, op2).DataTableToList<sp_promotional_video_listResult>().ToList();
            visible_list.Value = list.ToJson();
        }
	}

    protected override void OnAfterPostBack(){
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();
    }

    protected override void GetList(int page) {

		Master.ValueMessage.Value = "";

		Master.IsSearch = false;
		if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
			Master.IsSearch = true;
		}

		using (AdminDataContext dao = new AdminDataContext()){

            //var list = dao.sp_promotional_video_list(page, paging.RowsPerPage, "pv_id", Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "sort_column", "display", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, "pv_id", Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_promotional_video_list", op1, op2).DataTableToList<sp_promotional_video_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

			

		}

    }


    protected void ListBound(object sender, RepeaterItemEventArgs e){

        if (e.Item.ItemType == ListItemType.Footer)
            return;

		sp_promotional_video_listResult entity = e.Item.DataItem as sp_promotional_video_listResult;
        ((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
        ((Literal)e.Item.FindControl("lbDisplay")).Text = entity.pv_display ? "O" : "X";


    }


	protected void btnSave_Click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var hide_list = dao.promotional_video.Where(p => p.pv_display);
            var hide_list = www6.selectQ<promotional_video>("pv_display", 1);

            foreach (var entity in hide_list)
            {
                entity.pv_display = false;
            }

            var order = 0;
            foreach (var showEntity in visible_list.Value.ToObject<List<promotional_video>>())
            {
                //var entity = dao.promotional_video.First(p => p.pv_id == showEntity.pv_id);
                var entity = www6.selectQF<promotional_video>("pv_id", showEntity.pv_id);

                entity.pv_display = true;
                entity.pv_order = ++order;

                //string wClause = string.Format("pv_id = {0}", showEntity.pv_id);
                www6.update(entity);
            }


            //dao.SubmitChanges();
            Master.ValueMessage.Value = "적용되었습니다.";
        }
			
	}
}
