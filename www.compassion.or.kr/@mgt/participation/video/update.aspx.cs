﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_particitipation_video_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		base.BoardType = "promotional_video";
		base.FileGroup = Uploader.FileGroup.image_participation;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);

		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");

		if (base.Action == "update") {
            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.promotional_video.First(p => p.pv_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<promotional_video>("pv_id", Convert.ToInt32(PrimaryKey));

                pv_title.Text = entity.pv_title;
                pv_url.Text = entity.pv_url;
                this.Thumb = entity.pv_thumb;
            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			
			ShowFiles();

		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}




	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {


		var arg = new promotional_video() {
			pv_title = pv_title.Text,
			pv_thumb = this.Thumb,
			pv_url = pv_url.Text.Replace("https://", "http://"), 
			pv_order = 1000,
			pv_hits = 0,
			pv_display = false,
			pv_a_id = AdminLoginSession.GetCookie(this.Context).identifier,
			pv_regdate = DateTime.Now
		};



        using (AdminDataContext dao = new AdminDataContext())
        {
            if (Action == "update")
            {
                //var entity = dao.promotional_video.First(p => p.pv_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<promotional_video>("pv_id", Convert.ToInt32(PrimaryKey));

                entity.pv_title = arg.pv_title;
                entity.pv_url = arg.pv_url;
                //dao.SubmitChanges();
                //string wClause = string.Format("pv_id = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
            }
            else
            {

                //dao.promotional_video.InsertOnSubmit(arg);
                //dao.SubmitChanges();
                www6.insert(arg);

                PrimaryKey = arg.pv_id.ToString();

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";
            }

        }
	}


	// file
	void ShowFiles() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			b_image.Src = base.Thumb.WithFileServerHost();
			b_image.Attributes["data-exist"] = "1";
		}
	}


	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		base.Thumb = temp_file_name.Value;
		this.ShowFiles();

	}

	protected void btn_remove_click(object sender, EventArgs e) {

		using (AdminDataContext dao = new AdminDataContext()) {
            //var entity = dao.promotional_video.First(p => p.pv_id == Convert.ToInt32(PrimaryKey));
            //dao.promotional_video.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from promotional_video where pv_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
		}

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}
}