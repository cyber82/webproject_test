﻿
angular.module("defaultApp", [])
.filter('getImage', function () {
	return function (thumb, url) {
		if (thumb) return thumb;
		var id = url.substring(url.lastIndexOf("/") + 1, url.length);
		return "https://img.youtube.com/vi/" + id + "/0.jpg";
	};
})

.controller("defaultCtrl", function ($scope, $http) {

   	$scope.list = [];

   	// list
   	$scope.getVisibleList = function () {

   		if ($("#visible_list").val() != "") {
   			$scope.list = $.parseJSON($("#visible_list").val());

   			$.each($(".selected"), function () {
   				var obj = $(this);
   				var id = obj.attr("data-id");
   				if ($.grep($scope.list, function (entity) {
   					return entity.pv_id == id;
   				}).length > 0) {
					obj.iCheck("check");
   				}
			});

   		}
   	}


   	$scope.getVisibleList();

   	$scope.uncheck = function (id) {
   		if ($(".selected[data-id=" + id + "]").length > 0) {
   			$(".selected[data-id=" + id + "]").iCheck("uncheck");
   		} else {
   			$scope.list = $.grep($scope.list, function (entity) {
   				return entity.pv_id != parseInt(id);
   			});
   			$("#visible_list").val(JSON.stringify($scope.list));
   		}
   		
   	}

   	$(".selected").on("ifChanged", function (sender) {

   		var id = $(this).attr('data-id');
   		var title = $(this).attr("data-title");
   		var thumb = $(this).attr("data-thumb");
   		var url = $(this).attr("data-url");

   		if ($(this).prop("checked")) {
   			var entity = {
   				pv_id: id,
   				pv_title: title,
   				pv_thumb: thumb,
   				pv_url: url
   			}
   			$scope.list.push(entity);
   			$("#visible_list").val(JSON.stringify($scope.list));
   			$scope.safeApply();

   		} else {

   			$scope.list = $.grep($scope.list, function (entity) {
   				return entity.pv_id != parseInt(id);
   			});
   			$("#visible_list").val(JSON.stringify($scope.list));
   			$scope.safeApply();
   		}
   	})


	// 체크박스 클릭시에는 apply해야하나 ng-click에서는 apply하면 안됨
	// 2중으로 불리는 것을 방지하기 위한 메서드
   	$scope.safeApply = function (fn) {
   		var phase = this.$root.$$phase;
   		if (phase == '$apply' || phase == '$digest') {
   			if (fn && (typeof (fn) === 'function')) {
   				fn();
   			}
   		} else {
   			this.$apply(fn);
   		}
   	};
	
});


