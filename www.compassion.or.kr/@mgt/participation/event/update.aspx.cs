﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Data;

public partial class mgt_participation_event_update : AdminBoardPage
{
    public class DatePick
    {
        public string date { get; set; }
        public string limit { get; set; }
    }

    protected string ThumbM
    {
        set
        {
            if (!string.IsNullOrEmpty(value))
                this.ViewState["thumb_m"] = value;
        }
        get
        {
            return (this.ViewState["thumb_m"] != null) ? this.ViewState["thumb_m"].ToString() : string.Empty;
        }
    }



    protected override void OnBeforePostBack()
    {


        base.OnBeforePostBack();

        v_admin_auth auth = base.GetPageAuth();

        base.PrimaryKey = Request["c"];
        base.Action = Request["t"];
        formAction.Value = base.Action;

        btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

        // querystring 유효성 검사 후 문제시 리다이렉트
        var isValid = new RequestValidator()
            .Add("c", RequestValidator.Type.Numeric)
            .Add("t", RequestValidator.Type.Alphabet)
            .Validate(this.Context, "default.aspx");


        foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "event_type").OrderBy(p => p.cd_order))
        {
            e_type.Items.Add(new ListItem(a.cd_value, a.cd_key));
        }

        // 최대인원
        e_maximum.Items.Add(new ListItem("선택", "-1"));
        for (int i = 0; i < 20; i++)
        {
            e_maximum.Items.Add(new ListItem((i + 1).ToString(), (i + 1).ToString()));
        }


        // 최대팀
        e_maximum_exper.Items.Add(new ListItem("선택", "-1"));
        for (int i = 0; i < 7; i++)
        {
            e_maximum_exper.Items.Add(new ListItem((i + 1).ToString(), (i + 1).ToString()));
        }

        // 예약분단위
        e_time_interval.Items.Add(new ListItem("선택", "0"));
        e_time_interval.Items.Add(new ListItem("15분", "15"));
        e_time_interval.Items.Add(new ListItem("30분", "30"));

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var specialFundingList = dao.sp_tSpecialFunding_list(1, 1000, "", "", "", "").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword", "startdate", "enddate", "fixedYN" };
            Object[] op2 = new Object[] { 1, 1000, "", "", "", "" };
            var specialFundingList = www6.selectSP("sp_tSpecialFunding_list", op1, op2).DataTableToList<sp_tSpecialFunding_listResult>().ToList();

            campaignId.Items.Add(new ListItem("선택하세요", ""));
            foreach (var item in specialFundingList)
            {
                campaignId.Items.Add(new ListItem(item.CampaignName, item.CampaignID));
            }

            if (base.Action == "update")
            {
                //var entity = dao.@event.First(p => p.e_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<@event>("e_id", Convert.ToInt32(PrimaryKey));

                e_display.Checked = entity.e_display;
                e_confirm_display.Checked = entity.e_confirm_display == null ? false : (bool)entity.e_confirm_display;
                e_closed.Checked = entity.e_closed;
                e_type.SelectedValue = entity.e_type;
                e_target.SelectedValue = entity.e_target;
                campaignId.SelectedValue = entity.CampaignID;
                e_sf_prices.Value = entity.e_sf_prices;
                e_request_contents.Checked = entity.e_request_contents == null ? false : (bool)entity.e_request_contents;

                if (entity.e_begin != null) e_begin.Text = ((DateTime)entity.e_begin).ToString("yyyy-MM-dd");
                if (entity.e_end != null) e_end.Text = ((DateTime)entity.e_end).ToString("yyyy-MM-dd");
                if (entity.e_open != null) e_open.Text = ((DateTime)entity.e_open).ToString("yyyy-MM-dd");
                if (entity.e_close != null) e_close.Text = ((DateTime)entity.e_close).ToString("yyyy-MM-dd");

                if (entity.e_announce != null)
                {
                    e_announce.Text = ((DateTime)entity.e_announce).ToString("yyyy-MM-dd");
                    on_announce.Checked = true;
                }
                if (entity.e_show_child)
                    direct_sponsor.SelectedValue = "child";
                else if (entity.e_show_program)
                    direct_sponsor.SelectedValue = "program";
                else
                    direct_sponsor.SelectedValue = "";

                e_title.Text = entity.e_title;
                e_location.Text = entity.e_location;
                e_content.InnerText = entity.e_content;
                e_content_m.InnerText = entity.e_content_m;
                e_winner.InnerText = entity.e_winner;

                e_regdate.Text = entity.e_regdate.ToString("yyyy-MM-dd hh:mm:ss");


                // 당첨자 선정 방식
                // entity.e_winner가 null인 경우 event_winner테이블에서 당첨자 가져옴
                if (entity.e_winner == null)
                {
                    winner_type.SelectedValue = "excel";
                }
                else
                {
                    winner_type.SelectedValue = "editor";
                }

                this.Thumb = entity.e_thumb;
                ThumbM = entity.e_thumb_m;


                if (entity.e_type == "experience")
                {
                    e_maximum_exper.SelectedValue = entity.e_maximum.ToString();
                    e_maximum.SelectedValue = entity.e_maximum_person.ToString();

                    e_maximum_exper.Enabled = false;
                    e_maximum.Enabled = false;
                    e_time_interval.Enabled = false;

                    e_time_interval.SelectedValue = entity.e_time_interval.ToString();
                    hdnTimeInterval.Value = entity.e_time_interval.ToString();
                }
                else
                {
                    e_maximum.SelectedValue = entity.e_maximum.ToString();
                    e_maximum_exper.SelectedValue = entity.e_maximum.ToString();
                }

                e_limit.Text = entity.e_limit == -1 ? "" : entity.e_limit.ToString();


                btn_update.Visible = auth.aa_auth_update;
                btn_remove.Visible = auth.aa_auth_delete;
                ph_regdate.Visible = true;

                ShowFiles();




                // 체험전인 경우만 달력 노출
                if (entity.e_type == "experience")
                {
                    tabm2.Visible = true;

                    GetTimetableList();
                }

                // 이벤트와 체험전인 경우에 지원 리스트 노출
                if (entity.e_type != "general")
                {
                    tabm3.Visible = true;
                }

                //응모자 다운로드 버튼
                btn_apply_download.Attributes["data-id"] = this.PrimaryKey.ToString();
                btn_apply_download.Attributes["data-title"] = e_title.Text;


                e_id.Value = PrimaryKey.ToString();

                if (entity.e_type == "date" || entity.e_type == "complex")
                {
                    //var list = dao.event_timetable.Where(p => p.et_e_id == Convert.ToInt32(PrimaryKey)).OrderBy(p => p.et_date).Select(p => new { p.et_date, p.et_limit });
                    var list = www6.selectQ<event_timetable>("et_e_id", Convert.ToInt32(PrimaryKey), "et_date");

                    var pickList = new List<DatePick>();
                    foreach (var a in list)
                    {
                        var data = new DatePick()
                        {
                            date = a.et_date.ToString("yyyy-MM-dd HH:mm"),
                            limit = a.et_limit.ToString()
                        };
                        pickList.Add(data);
                    }
                    picked_date.Value = pickList.ToJson();
                }

            }
            else
            {
                btn_update.Visible = auth.aa_auth_create;
                btn_remove.Visible = false;
            }
        }
    }

    protected override void OnAfterPostBack()
    {
        base.OnAfterPostBack();
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";


        // 체험전인 경우만 달력 노출
        if (e_type.SelectedValue == "experience")
        {
            //GetTimetableList();
        }
    }

    protected void btn_update_click(object sender, EventArgs e)
    {

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var entity = base.Action == "update" ? dao.@event.First(p => p.e_id == Convert.ToInt32(PrimaryKey)) : new @event();
            var entity = new @event();
            if (base.Action == "update")
            {
                entity = www6.selectQF<@event>("e_id", Convert.ToInt32(PrimaryKey));
            }

            // 기존 타입과 다르거나 date or complex면 event_timetable삭제
            if (entity.e_type != e_type.SelectedValue || (entity.e_type == "date" || entity.e_type == "complex"))
            {
                //var removeList = dao.event_timetable.Where(p => p.et_e_id == Convert.ToInt32(PrimaryKey));
                var removeList = www6.selectQ<event_timetable>("et_e_id", this.PrimaryKey);

                //dao.event_timetable.DeleteAllOnSubmit(removeList);
                foreach (event_timetable et in removeList)
                {
                    string delStr = string.Format("delete from event_timetable where et_id = {0}", et.et_id);
                    www6.cud(delStr);
                }
            }

            
            entity.e_display = e_display.Checked;
            entity.e_confirm_display = e_confirm_display.Checked;
            entity.e_closed = e_closed.Checked;
            entity.e_type = e_type.SelectedValue;
            entity.e_target = e_target.SelectedValue;
            entity.CampaignID = campaignId.SelectedValue;
            entity.e_sf_prices = e_sf_prices.Value;
            entity.e_show_child = direct_sponsor.SelectedValue == "child";
            entity.e_show_program = direct_sponsor.SelectedValue == "program";
            entity.e_title = e_title.Text;
            entity.e_location = e_location.Text;
            entity.e_content = e_content.InnerHtml.ToHtml(false);
            entity.e_content_m = e_content_m.InnerHtml.ToHtml(false);
            entity.e_thumb = this.Thumb;
            entity.e_thumb_m = ThumbM;
            entity.e_limit = Convert.ToInt32(e_limit.Text == "" ? "-1" : e_limit.Text);

            //2018-05-10 이종진 WO-160
            //신청내용(e_request_contents) 추가. 구분(e_type)이 '날짜 선택 필요', '방문인원 선택 필요', '날짜+방문 인원 선택 필요' 일 경우만 값 넣음
            if (e_type.SelectedValue == "date"
                || e_type.SelectedValue == "count"
                || e_type.SelectedValue == "complex")
            {
                entity.e_request_contents = e_request_contents.Checked;
            }
            else
            {
                entity.e_request_contents = null;
            }

            // 신청가능기간
            if (e_type.SelectedValue != "general")
            {
                entity.e_begin = DateTime.Parse(e_begin.Text);
                entity.e_end = DateTime.Parse(e_end.Text + " 23:59:59");
            }


            // 참여 기간
            if (e_type.SelectedValue == "experience")
            {
                entity.e_open = DateTime.Parse(e_open.Text);
                entity.e_close = DateTime.Parse(e_close.Text + " 23:59:59");

                entity.e_time_interval = Convert.ToInt32(e_time_interval.SelectedValue);
            }


            if (e_type.SelectedValue == "experience")
            {
                entity.e_maximum = Convert.ToInt32(e_maximum_exper.SelectedValue);
                entity.e_maximum_person = Convert.ToInt32(e_maximum.SelectedValue);

            }
            else
            {
                entity.e_maximum = Convert.ToInt32(e_maximum.SelectedValue);
            }

            if (on_announce.Checked)
            {
                entity.e_announce = DateTime.Parse(e_announce.Text);
            }
            else
            {
                entity.e_announce = null;
            }

            // 당첨자 선정 방식
            if (on_announce.Checked)
            {
                if (winner_type.SelectedValue == "excel")
                {
                    entity.e_winner = null;
                }
                else
                {
                    entity.e_winner = e_winner.InnerHtml.ToHtml(false);
                }
            }
            else
            {
                entity.e_winner = null;
            }

            if (base.Action == "update")
            {
                //dao.SubmitChanges();
                
                www6.update( entity);

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
                AlertWithJavascript("수정되었습니다.", "goList()");

            }
            else
            {
                entity.e_regdate = DateTime.Now;
                entity.e_a_id = AdminLoginSession.GetCookie(this.Context).identifier;
                //dao.@event.InsertOnSubmit(entity);
                //dao.SubmitChanges();
                
                www6.insert(entity);

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", entity.ToJson()));
                AlertWithJavascript("등록되었습니다.", "goList()");

            }

            if ((entity.e_type == "date" || entity.e_type == "complex") && picked_date.Value != "")
            {
                AddTimetable(dao, entity);
            }


        }
    }

    protected void AddTimetable(AdminDataContext dao, @event entity)
    {
        var pickedList = picked_date.Value.ToObject<List<DatePick>>();

        foreach (var pickedDate in pickedList)
        {
            var timetable = new event_timetable()
            {
                et_e_id = entity.e_id,
                et_ymd = pickedDate.date.Substring(0, 10).Replace("-", ""),
                et_date = Convert.ToDateTime(pickedDate.date),
                et_limit = Convert.ToInt32(pickedDate.limit),
                et_closed = false,
                et_regdate = DateTime.Now,
                et_a_id = AdminLoginSession.GetCookie(this.Context).identifier
            };
            //dao.event_timetable.InsertOnSubmit(timetable);
            
            www6.insert(timetable);
        }

        //dao.SubmitChanges();
    }

    protected void btn_remove_click(object sender, EventArgs e)
    {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.@event.First(p => p.e_id == Convert.ToInt32(PrimaryKey));
            //dao.@event.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from event where e_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

        AlertWithJavascript("삭제되었습니다.", "goList()");
    }

    protected void btn_update_temp_file_click(object sender, EventArgs e)
    {


        if (temp_file_type.Value == "e_thumb")
        {
            base.Thumb = temp_file_name.Value;
            this.ShowFiles();
        }
        else if (temp_file_type.Value == "e_thumb_m")
        {
            ThumbM = temp_file_name.Value;
            this.ShowFiles();
        }
        else
        {
            readExcel();
        }


    }

    void ShowFiles()
    {
        if (!string.IsNullOrEmpty(base.Thumb))
        {
            e_thumb.Src = base.Thumb.WithFileServerHost();
            e_thumb.Attributes["data-exist"] = "1";
        }


        if (!string.IsNullOrEmpty(ThumbM))
        {
            e_thumb_m.Src = ThumbM.WithFileServerHost();
            e_thumb_m.Attributes["data-exist"] = "1";
        }
    }

    protected void Unnamed_Click(object sender, EventArgs e)
    {

    }

    protected void btn_section_Click(object sender, EventArgs e)
    {
        LinkButton obj = sender as LinkButton;
        var no = obj.CommandArgument;
        int tabCount = 3;
        int current = 0;

        for (int i = 1; i < tabCount + 1; i++)
        {
            if (no == i.ToString())
            {
                current = i;
                ((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "active";
                ((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "active tab-pane";
            }
            else
            {
                ((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "";
                ((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "tab-pane";
            }
        }

        if (current == 2)
        {
            GetTimetableList();
            hpExcelUpload.Visible = false;
        }
        else if (current == 3)
        {
            this.GetApplyList(1);
            hpExcelUpload.Visible = true;
        }
        else
        {
            et_date.Value = "";
            hpExcelUpload.Visible = false;
        }
    }

    class Schedule
    {
        public string date { get; set; }
    }


    // 일정 저장
    protected void btnSchedule_Click(object sender, EventArgs e)
    {

        if (scheduleData.Value != "")
        {
            using (AdminDataContext dao = new AdminDataContext())
            {
                //var list = dao.event_timetable.Where(p => p.et_e_id == Convert.ToInt32(PrimaryKey)).ToList();
                var list = www6.selectQ<event_timetable>("et_e_id", PrimaryKey);

                //dao.event_timetable.DeleteAllOnSubmit(dao.event_timetable.Where(p => p.et_e_id == Convert.ToInt32(PrimaryKey)));
                foreach (event_timetable et in list)
                {
                    string delStr = string.Format("delete from event_timetable where et_e_id = {0}", et.et_id);
                    www6.cud(delStr);
                }

                foreach (var schedule in scheduleData.Value.ToObject<List<Schedule>>())
                {
                    var entity = new event_timetable()
                    {
                        et_e_id = Convert.ToInt32(PrimaryKey),
                        et_ymd = schedule.date.Substring(0, 10).Replace("-", ""),
                        et_date = Convert.ToDateTime(schedule.date),
                        et_regdate = DateTime.Now,
                        et_a_id = AdminLoginSession.GetCookie(this.Context).identifier
                    };

                    var before = list.FirstOrDefault(p => p.et_date == Convert.ToDateTime(schedule.date));
                    if (before != null)
                    {
                        entity.et_closed = before.et_closed;
                    }

                    //dao.event_timetable.InsertOnSubmit(entity);
                    www6.insert(entity);
                }

                //dao.SubmitChanges();
                AlertWithJavascript("저장되었습니다.", "");


                //var timetableList = dao.event_timetable.Where(p => p.et_e_id == Convert.ToInt32(PrimaryKey)).Select(p => new { p.et_id, p.et_closed, p.et_count_member, p.et_count_team, p.et_date, p.et_ymd }).OrderBy(p => p.et_ymd).ToList();
                //var timetableList = dao.sp_event_timetable_list(Convert.ToInt32(PrimaryKey)).ToList();
                Object[] op1 = new Object[] { "e_id" };
                Object[] op2 = new Object[] { Convert.ToInt32(PrimaryKey) };
                var timetableList = www6.selectSP("sp_event_timetable_list", op1, op2).DataTableToList<sp_event_timetable_listResult>().ToList();

                scheduleData.Value = timetableList.ToJson();
            }

        }
    }

    protected void GetTimetableList()
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var timetableList = dao.sp_event_timetable_list(Convert.ToInt32(PrimaryKey)).ToList();
            Object[] op1 = new Object[] { "e_id" };
            Object[] op2 = new Object[] { Convert.ToInt32(PrimaryKey) };
            var timetableList = www6.selectSP("sp_event_timetable_list", op1, op2).DataTableToList<sp_event_timetable_listResult>().ToList();

            scheduleData.Value = timetableList.ToJson();
        }
    }

    // 참여자 검색 
    protected void search2(object sender, EventArgs e)
    {
        et_date.Value = ""; //일정탭에서 스케줄을 선택하여 검색조건에 추가되는 값을 지움
        this.GetApplyList(1);
    }

    protected void GetApplyList(int page)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        using (AdminDataContext dao = new AdminDataContext())
        {

            if (et_date.Value != "")
            {
                s_b_date.Text = "";
                s_e_date.Text = "";
            }

            //var list = dao.sp_event_apply_list(Convert.ToInt32(PrimaryKey), et_date.Value, page, paging_apply.RowsPerPage, s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "e_id", "er_date", "page", "rowsPerPage",  "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { Convert.ToInt32(PrimaryKey), et_date.Value, page, paging_apply.RowsPerPage, s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_event_apply_list", op1, op2).DataTableToList<sp_event_apply_listResult>().ToList();


            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging_apply.CurrentPage = page;
            paging_apply.Calculate(total);

            foreach (sp_event_apply_listResult item in list)
            {

                string sponsorID = item.tempSponsorID;
                if (string.IsNullOrEmpty(sponsorID))
                    item.IsSponsor = "N";
                else
                {
                    // 센터 소개 수정 문희원 2017-04-06
                    var objSql = new object[1] { " SELECT  " +
                                            " count(CM.CommitmentID) as cnt " +
                                         " FROM  " +
                                            " dbo.tCommitmentMaster CM WITH (NOLOCK)  " +
                                         " WHERE CM.SponsorID = '" + item.tempSponsorID + "' AND stopdate is null and (SponsorItemEng = 'ds' or sponsorItemEng = 'ls') AND (SponsorTypeEng = 'CHISPO' or sponsorItemEng = 'CHIMON') " };

                    DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

                    if (ds2 == null || ds2.Tables.Count == 0 || ds2.Tables[0].Rows.Count == 0)
                        item.IsSponsor = "N";
                    else
                    {
                        int n = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());
                        if (n > 0)
                            item.IsSponsor = "Y";
                        else
                            item.IsSponsor = "N";
                    }
                }
            }

            repeater_apply.DataSource = list;
            repeater_apply.DataBind();

        }


    }

    protected void repeater_apply_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Footer)
            return;

        sp_event_apply_listResult entity = e.Item.DataItem as sp_event_apply_listResult;

        ((Literal)e.Item.FindControl("lbUserId")).Text = entity.er_a_id == -1 ? entity.er_user_id : "관리자 등록";


        ((DropDownList)e.Item.FindControl("attended")).SelectedValue = entity.er_attended ? "1" : "0";
        ((DropDownList)e.Item.FindControl("related")).SelectedValue = entity.er_related ? "1" : "0";



        // 인원선택 미입력 이벤트도 있음
        if (entity.er_count == -1)
        {
            ((Literal)e.Item.FindControl("none_count")).Visible = true;
        }
        else
        {
            // 관리자가 등록한 것 (관리자 등록 = 단체)
            if (entity.er_user_id.EmptyIfNull() == "")
            {
                ((TextBox)e.Item.FindControl("team_count")).Text = entity.er_count.ToString();
                ((TextBox)e.Item.FindControl("team_count")).Visible = true;
                ((DropDownList)e.Item.FindControl("member_count")).Visible = false;

            }
            else
            {
                ((DropDownList)e.Item.FindControl("member_count")).SelectedValue = entity.er_count.ToString();
                ((DropDownList)e.Item.FindControl("member_count")).Visible = true;
                ((TextBox)e.Item.FindControl("team_count")).Visible = false;
            }
        }

        // 체험전은 분까지, 이벤트는 날짜만
        if (entity.er_date != null)
        {
            ((Literal)e.Item.FindControl("lbDate")).Text = ((DateTime)entity.er_date).ToString("yyyy-MM-dd");
            ((Literal)e.Item.FindControl("lbTime")).Text = ((DateTime)entity.er_date).ToString("HH:mm");
        }
        else
        {
            ((Literal)e.Item.FindControl("lbDate")).Text = "-";
            ((Literal)e.Item.FindControl("lbTime")).Text = "-";
        }

        //신청내용
        //if (string.IsNullOrEmpty(entity.er_request_contents))
        //{
        //    ((Literal)e.Item.FindControl("lbRequestContents")).Visible = false;
        //}
        //else
        //{
        //    ((Literal)e.Item.FindControl("lbRequestContents")).Visible = true;
        //    //((Literal)e.Item.FindControl("lbRequestContents")).Text = "보기";
        //}

    }

    protected void paging_apply_Navigate(int page, string hash)
    {
        this.GetApplyList(page);
    }


    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.event_apply;
            string sqlStr = string.Format("select * from event_apply");
            var list = www6.selectQ<event_apply>();

            foreach (RepeaterItem item in repeater_apply.Items)
            {

                var er_id = Convert.ToInt32(((LinkButton)item.FindControl("btnRemove")).CommandArgument);

                var er_count = -1;
                if (((DropDownList)item.FindControl("member_count")).Visible)
                {
                    er_count = Convert.ToInt32(((DropDownList)item.FindControl("member_count")).SelectedValue);
                }
                else if (((TextBox)item.FindControl("team_count")).Visible)
                {
                    er_count = Convert.ToInt32(((TextBox)item.FindControl("team_count")).Text);
                }

                var attended = ((DropDownList)item.FindControl("attended")).SelectedValue == "1";
                var related = ((DropDownList)item.FindControl("related")).SelectedValue == "1";

                var entity = list.First(p => p.er_id == er_id);
                entity.er_count = er_count;
                entity.er_attended = attended;
                entity.er_related = related;
                
                www6.update(entity);
            }

            //dao.SubmitChanges();
        }

        Master.ValueMessage.Value = "수정되었습니다.";
        //GetList(1);
    }

    protected void btnRemove_Click(object sender, EventArgs e)
    {

        LinkButton obj = sender as LinkButton;
        var er_id = obj.CommandArgument;

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.event_apply.First(p => p.er_id == Convert.ToInt32(er_id));
            var entity = www6.selectQF<event_apply>("er_id", Convert.ToInt32(er_id));

            //dao.event_apply.DeleteOnSubmit(entity);
            //www6.cud(MakeSQL.delQ(0, "event_apply", "er_id", Convert.ToInt32(er_id)));
            www6.delete(entity);
            // 당첨자 삭제
            //var winner = dao.event_winner.FirstOrDefault(p => p.ew_e_id == Convert.ToInt32(PrimaryKey) && p.ew_user_id == entity.er_user_id);
            
            var winner = www6.selectQF<event_winner>("ew_e_id", Convert.ToInt32(PrimaryKey), "ew_user_id", entity.er_user_id);
            if (winner != null)
            {
                //dao.event_winner.DeleteOnSubmit(winner);
                string delStr2 = string.Format("delete from event_winner where ew_e_id = {0} and ew_user_id = '{1}'", Convert.ToInt32(PrimaryKey), entity.er_user_id);
                www6.cud(delStr2);
            }

            //dao.SubmitChanges();

            Master.ValueMessage.Value = "삭제되었습니다.";
            GetApplyList(paging_apply.CurrentPage);
        }
    }

    protected void btnRegist_Click(object sender, EventArgs e)
    {

    }


    protected void readExcel()
    {

        string connectionString = ConfigurationManager.AppSettings["excel_connection"];
        string fileName = temp_file_name.Value;

        //string rootDirectory = HttpContext.Current.Request.PhysicalApplicationPath;
        //string uploadFile = rootDirectory + fileName;
        string uploadFile = Server.MapPath(fileName);

        string excelConnection = string.Format(connectionString, uploadFile);

        try
        {
            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {

                conn.Open();
                DataTable dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    string sheetName = row["TABLE_NAME"].ToString();

                    try
                    {

                        using (AdminDataContext dao = new AdminDataContext())
                        {

                            using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheetName + "]", conn))
                            {

                                OleDbDataAdapter adpt = new OleDbDataAdapter(cmd);
                                DataSet ds = new DataSet();
                                adpt.Fill(ds);

                                //dao.event_winner.DeleteAllOnSubmit(dao.event_winner.Where(p => p.ew_e_id == Convert.ToInt32(PrimaryKey)));
                                var delList = www6.selectQ<event_winner>("ew_e_id", Convert.ToInt32(PrimaryKey));

                                foreach (event_winner ew in delList)
                                {
                                    string delStr = string.Format("delete from event_winner where ew_id = {0}", ew.ew_id);
                                    www6.cud(delStr);
                                }

                                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                {
                                    DataRow dr1 = ds.Tables[0].Rows[i];

                                    var user_id = dr1[1];
                                    var user_name = dr1[2];
                                    var winner = dr1[12].ToString();

                                    if (winner.ToUpper() == "O" && user_id.ToString() != "" && user_name.ToString() != "")
                                    {
                                        var entity = new event_winner()
                                        {
                                            ew_e_id = Convert.ToInt32(PrimaryKey),
                                            ew_user_id = user_id.ToString(),
                                            ew_user_name = user_name.ToString(),
                                            ew_regdate = DateTime.Now,
                                            ew_a_id = AdminLoginSession.GetCookie(this.Context).identifier
                                        };

                                        //dao.event_winner.InsertOnSubmit(entity);
                                        
                                        www6.insert(entity);
                                    }
                                }

                                //dao.SubmitChanges();
                                GetApplyList(1);
                                Master.ValueMessage.Value = "등록되었습니다.";
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        Master.ValueMessage.Value = ex.Message;
                    }
                }
            }
        }
        catch (Exception ee)
        {
            if (ee.Message == "외부 테이블 형식이 잘못되었습니다." || ee.Message == "External table is not in the expected format.")
            {
                Master.ValueMessage.Value = "엑셀파일을 호환모드로 변경 후 등록해주세요.";
            }
            else
            {
                Master.ValueMessage.Value = ee.Message;
            }

        }
    }
}