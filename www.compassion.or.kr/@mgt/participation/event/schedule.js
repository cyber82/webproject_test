﻿$page = {
    defaultDate: '',
    selectedDate: '',
    scheduleData: {},
    e_open: '',
    e_close: '',
    type: '',
    init: function () {

        $page.e_open = $("#e_open").val().substring(0, 10);
        $page.e_close = moment($("#e_close").val().substring(0, 10)).add('days', 1).format("YYYY-MM-DD");

        $page.type = $("#e_type input[type=radio]:checked").val();
        //$page.maximum = $("#e_maximum_exper").val();
        //$page.maximumType = $("#e_maximum").val() == -1 ? 'team' : 'person';
        $page.maximum = $("#e_maximum").val() == -1 ? parseInt($("#e_maximum_exper").val(), 10) : parseInt($("#e_maximum").val(), 10);
        $page.maximum_person = -1;
        if ($page.type == "experience") {
            $page.maximum = parseInt($("#e_maximum_exper").val(), 10);
            $page.maximum_person = parseInt($("#e_maximum").val(), 10);
        }

        // 스케쥴 조회 정보
        if ($page.type == "experience" && $("#scheduleData").val() != "" && $("#scheduleData").val() != "[]") {
            var loadedData = $.parseJSON($("#scheduleData").val());

            var list = [];
            var date = "";
            $.each(loadedData, function (i, v) {
                if (i == 0) date = moment(this.et_date).format("YYYY-MM-DD");

                if (date != moment(this.et_date).format("YYYY-MM-DD")) {
                    $page.scheduleData[date] = list;
                    list = [];

                    date = moment(this.et_date).format("YYYY-MM-DD");
                }


                var entity = {
                    date: moment(this.et_date).format("YYYY-MM-DD HH:mm:ss"),
                    //member: this.et_count_member,
                    //team: this.et_count_team,
                    totalmember: parseInt(this.et_count_reserve_member, 10) + parseInt(this.et_count_organ_member, 10),
                    reservemember: this.et_count_reserve_member,
                    reserveteam: this.et_count_reserve_team,
                    organmember: this.et_count_organ_member,
                    organteam: this.et_count_organ_team,
                    closed: this.et_closed,
                    id: this.et_id
                }
                list.push(entity);
            });

            $page.scheduleData[date] = list;
        }

        //일정탭에서 스케줄을 선택하여 검색조건에 추가되는 값에 따라 선택되는 날짜 세팅
        var et_date = '';
        if ($('#et_date').val() == '') {
            et_date = moment(new Date()).format("YYYY-MM-DD");
        }
        else {
            et_date = moment($('#et_date').val()).format("YYYY-MM-DD");
        }

        $('#calendar').fullCalendar({
            lang: "ko",
            events: [{
                start: $page.e_open,
                end: $page.e_close,
                rendering: 'background',
                color: '#ffd5cc'
            }],
            ignoreTimezone: true,
            selectable: true,
            timezone: 'local',
            defaultDate: moment(et_date),
            select: function (start, end, jsEvent, view, resources) {

                // 체험전
                if ($page.type == "experience") {

                    var selectedBegin = moment(start).format('YYYY-MM-DD');
                    var selectedEnd = moment(end).format('YYYY-MM-DD');

                    // 하루만 선택가능
                    if (start.add('days', 1).date() != end.date()) {
                        $page.reset();
                        return;
                    }

                    // 참여기간 외
                    if (selectedBegin < $page.e_open || selectedEnd > $page.e_close) {
                        alert('참여기간에서 선택해주세요.');
                        $page.reset();
                        return;
                    } else {
                        $page.schedule.init(selectedBegin);
                        $page.selectedDate = selectedBegin;
                    }
                }

                    // 이벤트
                else {

                }

            }
        });

        // 오늘
        $page.defaultDate = $("#calendar").fullCalendar('getDate').format("YYYY-MM-DD");
        //$page.defaultDate = new Date(2017, 10, 28);
        $page.selectedDate = $page.defaultDate;

        // 시간표 초기화
        if ($page.type == "experience") {
            $page.schedule.init($page.defaultDate);
        }
    },

    reset: function () {
        $("#calendar").fullCalendar('unselect');
        $page.selectedDate = $page.defaultDate;
        $page.schedule.init($page.defaultDate);
    },


    schedule: {
        init: function (initDate) {

            var timeInterval = $('#hdnTimeInterval').val() == '' || $('#hdnTimeInterval').val() == '0' ? '60' : $('#hdnTimeInterval').val(); //예약분단위

            if ($('#schedule').children().length > 0) {
                $("#schedule").fullCalendar('destroy');
            }

            var options = {
                defaultView: 'agendaDay',
                minTime: '09:00:00',
                maxTime: '22:00:00',
                contentHeight: 720,
                timezone: 'local',
                selectable: true,
                slotMinutes: parseInt(timeInterval, 10),
                slotDuration: timeInterval == '60' ? '01:00:00' : '00:' + timeInterval + ':00',
                snapDuration: timeInterval == '60' ? '01:00:00' : '00:' + timeInterval + ':00',
                select: function (start, end, jsEvent, view, resources) {

                    // 기간 외 날짜에서 시간 선택한경우
                    if (start.format("YYYY-MM-DD") < $page.e_open || start.format("YYYY-MM-DD") >= $page.e_close) {
                        alert('참여기간에서 선택해주세요.');
                        $page.reset();
                        return;
                    }

                    var b_date = start.toDate();
                    var e_date = end.toDate();

                    var jsonData = [];
                    while (b_date < e_date) {
                        jsonData.push({
                            date: moment(b_date).format('YYYY-MM-DD HH:mm:ss')
                        });
                        //b_date = new Date(b_date.getTime() + 30 * 60000);
                        b_date = new Date(b_date.getTime() + parseInt(timeInterval, 10) * 60000);
                    }

                    $page.scheduleData[$page.selectedDate] = jsonData;

                },
                eventClick: function (calEvent, jsEvent, view) {


                    var title = calEvent.title;

                    // data- 속성 검색
                    var data_id = $.grep(this.className.split(" "), function (v, i) {
                        return v.indexOf('data-') === 0;
                    })[0]

                    // 마감 / 마감취소 버튼
                    if (title == "마감" || title == "마감취소") {
                        if (confirm(title + "처리 하시겠습니까?")) {
                            $.post("/@mgt/participation/event/update.ashx?t=close&close=" + (title == "마감" ? "y" : "n") + "&id=" + data_id.replace("data-", ""), function (r) {
                                //console.log(r);
                                if (r.success) {
                                    alert(title + "처리 되었습니다.");

                                    //console.log(r.data);
                                    //console.log(r.data.date.substring(0, 10));

                                    var loadData = $page.scheduleData[r.data.date.substring(0, 10)];
                                    var updateData = $.grep(loadData, function (entity) {
                                        return entity.date == r.data.date;
                                    })

                                    updateData[0].closed = title == "마감";


                                    if ($page.type == "experience") {
                                        if (r.data.count >= $page.maximum || r.data.countPerson >= $page.maximum_person) {
                                            calEvent.title = "FULL";
                                            calEvent.color = "gray";
                                        } else if (title == "마감") {
                                            calEvent.title = "마감취소";
                                            calEvent.color = "gray";
                                        } else {
                                            calEvent.title = "마감";
                                            calEvent.color = "green";
                                        }
                                    }
                                    else {
                                        if (r.data.count >= $page.maximum) {
                                            calEvent.title = "FULL";
                                            calEvent.color = "gray";
                                        } else if (title == "마감") {
                                            calEvent.title = "마감취소";
                                            calEvent.color = "gray";
                                        } else {
                                            calEvent.title = "마감";
                                            calEvent.color = "green";
                                        }
                                    }

                                    $('#schedule').fullCalendar('updateEvent', calEvent);

                                    $page.schedule.init($page.selectedDate);
                                }
                            });
                        }

                    } else if (title == "FULL") {
                        return;
                    }

                        // 단체등록
                    else if (title == "단체등록") {
                        if ($(this).next().find('.fc-title').text() == 'FULL') {
                            alert('Full 상태에서는 등록이 불가합니다.');
                            return;
                        }

                        var maxNum = $.grep($(this).prev().attr('class').split(" "), function (v, i) {
                            return v.indexOf('maxnum-') === 0;
                        })[0];

                        var maxNumPerson = $.grep($(this).prev().attr('class').split(" "), function (v, i) {
                            return v.indexOf('maxnumperson-') === 0;
                        })[0];

                        $("#registModal").attr("data-date", calEvent.start.format("YYYY-MM-DD HH:mm"));
                        $("#registModal").attr("data-id", data_id);
                        $("#registModal").attr("maxnum", maxNum.replace('maxnum-', ''));
                        $("#registModal").attr("maxnumperson", maxNumPerson.replace('maxnumperson-', ''));
                        $('#registModal').modal()
                    }

                        // 지우기 버튼
                    else if (title == "지우기") {

                        var date = calEvent.start.format("YYYY-MM-DD");

                        delete $page.scheduleData[date];
                        $page.schedule.init(date);
                    }

                        // 시간 버튼
                    else {
                        $("#et_date").val(calEvent.start.format("YYYY-MM-DD HH:mm"));
                        $('#hdnSchedulScrollTop').val($('.fc-time-grid-container.fc-scroller').scrollTop());

                        eval($("#btnApplyTab").attr("href").replace("javascript:", ""));
                    }
                },
                eventRender: function (event, element) {

                    // 제목을 테그로
                    element.find('.fc-title').html(event.title);

                    // 닫기버튼에 시간 지우기
                    if (element.hasClass("schedule-label")) {
                        element.find(".fc-time").remove();
                    }

                },
                eventAfterRender: function (event, element, view) {
                    // 단체등록 버튼 위치 조정
                    //if (event.title == "단체등록" || (event.title == "FULL" && element.closest('a').hasClass('btnTeam'))) {
                    if (event.title == "단체등록") {
                        //element.css("left", "236px");
                        element.css("float", "right");
                        element.css("left", "auto");
                        element.css("right", "45px");
                        element.css("margin-right", "20px");
                    }

                    // 마감버튼 위치 조정
                    //if (event.title == "마감" || event.title == "마감취소" || (event.title == "FULL" && element.closest('a').hasClass('btnClose'))) {
                    if (event.title == "마감" || event.title == "마감취소" || event.title == "FULL") {
                        //element.css("left", "302px");
                        element.css("left", "auto");
                        element.css("float", "right");
                        element.css("margin-right", "0");
                    }

                    // 지우기 버튼
                    if (event.title == "지우기") {
                        element.css("font-size", "1.5em");
                    }

                    if ($(element).hasClass('btnView')) {
                        $(element).attr('title', event.title);
                        $(element).css('width', $(element).parent().width() - 150);
                    }


                }

            }


            if (initDate) {
                options['defaultDate'] = initDate;

                // 선택한 날의 시간표 출력

                if ($page.scheduleData[initDate]) {

                    var data = $page.scheduleData[initDate];

                    //console.log(data);

                    options['events'] = [];
                    $.each(data, function () {
                        var option = {
                            start: moment(this.date),
                            end: (moment(this.date).add(timeInterval, 'minutes')),
                            rendering: 'background'
                        };
                        // db조회 데이터인 경우
                        if (typeof this.reservemember != 'undefined') {
                            $.extend(option, {
                                //title: '[인원:' + this.member + '][팀:' + this.team + ']',
                                title: '[전체인원:' + this.totalmember + '][예약인원:' + this.reservemember + '][예약팀:' + this.reserveteam + '][단체인원:' + this.organmember + '][단체팀:' + this.organteam + ']',
                                rendering: '',
                                className: [
                                    "btnView",
                                    "data-" + this.id,
                                    "maxnum-" + parseInt(parseInt(this.reserveteam, 10) + parseInt(this.organteam, 10), 10),
                                    "maxnumperson-" + this.totalmember,
                                ]
                            });


                            var isFull = false;
                            if (parseInt(this.reserveteam, 10) + parseInt(this.organteam, 10) >= $page.maximum
                                || parseInt(this.totalmember, 10) >= $page.maximum_person) {
                                isFull = true;
                            }

                            // 등록버튼
                            var regist = {
                                title: "단체등록",
                                color: "#205081",
                                //title: isFull ? "-" : "단체등록",
                                //color: isFull ? "gray" : "#205081",
                                start: moment(this.date),
                                end: (moment(this.date).add(timeInterval, 'minutes')),
                                rendering: '',
                                className: ["schedule-label", "btnTeam", "data-id" + this.id]
                            };
                            options['events'].push(regist);



                            // 마감버튼
                            var btn_close = "";
                            //if (this.team >= $page.maximum) {
                            //	btn_close = "FULL";
                            //} else if (this.closed) {
                            //	btn_close = "마감취소";
                            //} else {
                            //	btn_close = "마감";
                            //}

                            if (parseInt(this.reserveteam, 10) + parseInt(this.organteam, 10) >= $page.maximum
							    || parseInt(this.totalmember, 10) >= $page.maximum_person) {
                                btn_close = "FULL";
                            } else if (this.closed) {
                                btn_close = "마감취소";
                            } else {
                                btn_close = "마감";
                            }

                            var close = {
                                title: btn_close,
                                color: (btn_close == "마감" ? "green" : "gray"),
                                start: moment(this.date),
                                end: (moment(this.date).add(timeInterval, 'minutes')),
                                rendering: '',
                                className: ["btnClose", "data-" + this.id, "schedule-label"]
                            };
                            options['events'].push(close);

                        }


                        options['events'].push(option);

                    });


                    // 초기화 버튼
                    var btnInit = {
                        title: "지우기",
                        allDay: true,
                        start: initDate,
                        color: "#f39c12"
                    }

                    options['events'].push(btnInit);
                }
            }
            $("#schedule").fullCalendar(options);

            //일정탭에서 스케줄을 선택하여 검색조건에 추가되는 값에 따라 스케줄 스크롤바 위치 세팅
            var et_date = '';
            if ($('#et_date').val() != '') {
                setTimeout(function () {
                    $('.fc-time-grid-container.fc-scroller').scrollTop(parseInt($('#hdnSchedulScrollTop').val(), 10));
                }, 1000);
            }
        }
    }



}