﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Data;

public partial class mgt_participation_event_download : AdminBasePage
{

    protected override void OnBeforePostBack()
    {

        var id = Convert.ToInt32(Request["c"]);
        var title = Request["title"];
        var keyword = Request["keyword"];
        base.OnBeforePostBack();
        this.GetList(id, title, keyword);


        // 익스에서 한글, 띄어쓰기 깨짐
        HttpBrowserCapabilities brObject = Request.Browser;
        var browser = brObject.Type.ToLower();
        var fileName = "이벤트_" + title.Replace(" ", "_").Replace(",", "").Replace("'", "") + "_" + id.ToString() + ".xls";
        if (browser.Contains("ie"))
        {
            fileName = System.Web.HttpUtility.UrlEncode(fileName);
        }


        Response.Clear();
        Response.Charset = "utf-8";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
    }


    protected void GetList(int id, string title, string keyword)
    {

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_event_apply_list(id, "", 1, 999999, keyword, "", "").ToList();
            Object[] op1 = new Object[] { "e_id", "er_date", "page", "rowsPerPage", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { id, "", 1, 999999, keyword, "", "" };
            var list = www6.selectSP("sp_event_apply_list", op1, op2).DataTableToList<sp_event_apply_listResult>().ToList();

            foreach (sp_event_apply_listResult item in list)
            {

                string sponsorID = item.tempSponsorID;
                if (string.IsNullOrEmpty(sponsorID))
                    item.IsSponsor = "N";
                else
                {
                    // 센터 소개 수정 문희원 2017-04-06
                    var objSql = new object[1] { " SELECT  " +
                                            " count(CM.CommitmentID) as cnt " +
                                         " FROM  " +
                                            " dbo.tCommitmentMaster CM WITH (NOLOCK)  " +
                                         " WHERE CM.SponsorID = '" + item.tempSponsorID + "' AND stopdate is null and (SponsorItemEng = 'ds' or sponsorItemEng = 'ls') AND (SponsorTypeEng = 'CHISPO' or sponsorItemEng = 'CHIMON') " };

                    DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

                    if (ds2 == null || ds2.Tables.Count == 0 || ds2.Tables[0].Rows.Count == 0)
                        item.IsSponsor = "N";
                    else
                    {
                        int n = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());
                        if (n > 0)
                            item.IsSponsor = "Y";
                        else
                            item.IsSponsor = "N";
                    }
                }
            }

            repeater.DataSource = list;
            repeater.DataBind();

            base.WriteLog(AdminLog.Type.select, string.Format("이벤트_{0} 다운로드 ({1})", title, id));
        }
    }



    protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
            return;

        sp_event_apply_listResult entity = e.Item.DataItem as sp_event_apply_listResult;

        ((Literal)e.Item.FindControl("attended")).Text = entity.er_attended ? "O" : "X";
        ((Literal)e.Item.FindControl("related")).Text = entity.er_related ? "O" : "X";
        ((Literal)e.Item.FindControl("winner")).Text = entity.ew_id == -1 ? "X" : "O";
    }
}