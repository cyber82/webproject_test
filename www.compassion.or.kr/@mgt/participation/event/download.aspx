﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="download.aspx.cs" Inherits="mgt_participation_event_download"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head runat="server">
    <title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
		html{background:#fff}
		table { border-collapse:initial; }  
		th, td { border:1px solid #000000;}
		td { mso-number-format:\@; }
	</style>
</head>
<body>
    <form id="form" runat="server">
		<table class="listBoard">
			<tbody>	
				<tr>
					<td style="width:50px">번호</td>
					<td style="width:150px">아이디</td>
					<td style="width:150px">이름</td>
					<td style="width:150px">연락처</td>
				    <th style="width:150px">신청일자</th>
				    <th style="width:150px">신청인원</th>
                    <th style="width:150px">경로</th>
                    <th style="width:150px">생일</th>
                    <th style="width:150px">후원여부</th>
                    <th style="width:150px">등록일자</th>
                    <th style="width:150px">참석여부</th>
                    <th style="width:150px">결연여부</th>
                    <th style="width:150px">당첨여부</th>
                    <th style="width:250px">신청내용</th>
				</tr>		
				<asp:Repeater runat=server ID=repeater  OnItemDataBound="repeater_ItemDataBound">
					<ItemTemplate>
						<tr>
							<td><%#Container.ItemIndex + 1 %></td>
							<td><%#Eval("er_user_id" )%></td>
							<td><%#Eval("er_name" )%></td>
							<td><%#Eval("er_phone" )%></td>
                            <td><%#Eval("er_date" , "{0:yyyy'.'MM'.'dd HH:mm}")%></td>
							<td><%#Eval("er_count" , "{0:N0}" )%></td>
                            <td><%#Eval("er_route") %></td>
                            <td><%#Eval("BirthDate" )%></td>
                            <td><%#Eval("IsSponsor" )%></td>
							<td><%#Eval("er_regdate" , "{0:yyyy'.'MM'.'dd HH:mm}")%></td>
                            <td><asp:Literal runat="server" ID="attended" /></td>
                            <td><asp:Literal runat="server" ID="related" /></td>
                            <td><asp:Literal runat="server" ID="winner" /></td>
                            <td><%#Eval("er_request_contents")%></td>
						</tr>	
					</ItemTemplate>
					<FooterTemplate>
						<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
							<td colspan="11">데이터가 없습니다.</td>
						</tr>
					</FooterTemplate>
				</asp:Repeater>	
				
			</tbody>
		</table>
    </form>
</body>
</html>
