﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_participation_event_default : AdminBoardPage {


	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;
		btn_add.Attributes.Add("href", "update.aspx");

		e_type.Items.Add(new ListItem("전체", ""));
		foreach (var a in StaticData.Code.GetList(this.Context).Where(p => p.cd_group == "event_type").OrderBy(p => p.cd_order)) {
			var item = new ListItem(a.cd_value, a.cd_key);
			e_type.Items.Add(item);
		}

	}

	protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();

		btn_add.Attributes.Add("href", "update.aspx");
	}

	protected override void GetList(int page) {

		Master.IsSearch = false;
		if (e_type.SelectedValue != "" || s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
			Master.IsSearch = true;
		}

		using (AdminDataContext dao = new AdminDataContext()) {

            //var list = dao.sp_event_list(page, paging.RowsPerPage, e_type.SelectedValue, Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "display", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, e_type.SelectedValue, Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_event_list", op1, op2).DataTableToList<sp_event_listResult>().ToList();

            var total = 0;

			if (list.Count > 0)
				total = list[0].total.Value;

			lbTotal.Text = total.ToString();

			paging.CurrentPage = page;
			paging.Calculate(total);
			repeater.DataSource = list;
			repeater.DataBind();

		}
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				sp_event_listResult entity = e.Item.DataItem as sp_event_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();


				string closed = "진행중";

				if (entity.close_type == "e_closed") {
					closed = "마감 (설정됨)";
				} else if (entity.close_type == "e_end") {
					closed = "마감 (기간종료)";
				}else if (entity.close_type == "count") {
					closed = "마감 (선착순)";
				}else if(entity.close_type == "date" || entity.close_type == "complex") {
					closed = "마감 (인원제한)";
				}

				((Literal)e.Item.FindControl("lbClose")).Text = closed;
				((Literal)e.Item.FindControl("display_status")).Text = entity.e_display ? "노출중" : "미노출"; ;

			}
		}

	}
}
