﻿<%@ WebHandler Language="C#" Class="api_board" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_board : BaseHandler
{
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context)
    {

        var t = context.Request["t"].EmptyIfNull();
        if(t == "close") {
            Close(context);
        }else if(t == "apply_team") {
            ApplyTeam(context);
        }
    }



    void Close(HttpContext context)
    {

        JsonWriter result = new JsonWriter()
        {
            success = true
        };

        var et_id = context.Request["id"];
        var close = context.Request["close"];

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.event_timetable.First(p => p.et_id == Convert.ToInt32(et_id));
            var entity = www6.selectQF<event_timetable>("et_id", Convert.ToInt32(et_id));
            entity.et_closed = close == "y";

            //dao.SubmitChanges();
            www6.update(entity);

            //var count = dao.event_apply.Count(p => p.er_e_id == entity.et_e_id && p.er_date == entity.et_date);
            var list = www6.selectQ<event_apply>("er_e_id", entity.et_e_id, "er_date", entity.et_date);

            var count = list.Count();
            var time = entity.et_date;

            //var countPerson = dao.event_apply.Where(p => p.er_e_id == entity.et_e_id && p.er_date == entity.et_date).Sum(p => p.er_count);
            var countPerson = list.Sum(p => p.er_count);

            var data = new Dictionary<string, string>();
            data.Add("count", count.ToString());
            data.Add("date", entity.et_date.ToString("yyyy-MM-dd HH:mm:ss"));
            data.Add("countPerson", countPerson.ToString());

            result.data = data;
        }

        JsonWriter.Write(result, context);
    }



    void ApplyTeam(HttpContext context)
    {
        JsonWriter result = new JsonWriter()
        {
            success = true
        };

        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var name = context.Request["name"].EmptyIfNull();
        var phone = context.Request["phone"].EmptyIfNull();
        var member = Convert.ToInt32(context.Request["member"].ValueIfNull("-1"));

        if(id == -1 || name == "" || phone == "" || member == -1)
        {
            result.success = false;
            result.message = "필수 정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var event_timetable = dao.event_timetable.First(p => p.et_id == id);
            var event_timetable = www6.selectQF<event_timetable>("et_id", id);
            var e_id = event_timetable.et_e_id;

            //var eventEntity = dao.@event.FirstOrDefault(p => p.e_id == e_id);
            var eventEntity = www6.selectQF<@event>("e_id", e_id);

            if (eventEntity == null)
            {
                result.success = false;
                result.message = "잘못된 요청입니다.";
                JsonWriter.Write(result, context);
                return;
            }

            if (eventEntity.e_type.Equals("experience"))
            {
                if (eventEntity.e_closed || Convert.ToDateTime(eventEntity.e_end).CompareTo(DateTime.Now) < 0)
                {
                    result.success = false;
                    result.message = "지원이 마감되었습니다.";
                    JsonWriter.Write(result, context);
                    return;
                }
            }

            if (eventEntity.e_type.Equals("experience"))
            {
                //var applyList = dao.sp_event_apply_count_list_f(e_id).Where(p => p.er_date == Convert.ToDateTime(event_timetable.et_date)).FirstOrDefault();
                Object[] op1 = new Object[] { "e_id" };
                Object[] op2 = new Object[] { e_id };
                var applyList = www6.selectSP("sp_event_apply_count_list_f", op1, op2).DataTableToList<sp_event_apply_count_list_fResult>().Where(p => p.er_date == Convert.ToDateTime(event_timetable.et_date)).FirstOrDefault();

                if (applyList != null)
                {
                    var remainCount = (eventEntity.e_maximum - applyList.apply_count).ToString();
                    //ruokman
                    //if (eventEntity.e_maximum_type.Equals("team"))
                    //{
                    //    if (applyList.apply_count > eventEntity.e_maximum + 1)
                    //    {
                    //        result.success = false;
                    //        result.message = "선택하신 일시에 지원할 수 있는 팀 수를 초과하였습니다.\n남은 수는 " + remainCount + "팀입니다.";
                    //        JsonWriter.Write(result, context);
                    //        return;
                    //    }
                    //}
                    //else if (eventEntity.e_maximum_type.Equals("person"))
                    //{
                    //    if (applyList.apply_person_count > eventEntity.e_maximum + member)
                    //    {
                    //        result.success = false;
                    //        result.message = "선택하신 일시에 지원할 수 있는 인원 수를 초과하였습니다.\n남은 수는 " + remainCount + "명입니다.";
                    //        JsonWriter.Write(result, context);
                    //        return;
                    //    }
                    //}

                }

            }

            var entity = new event_apply()
            {
                er_e_id = event_timetable.et_e_id,
                er_user_id = "",
                er_name = name,
                er_phone = phone,
                er_count = member,
                er_date = event_timetable.et_date,
                er_route = "",
                er_attended = false,
                er_related = false,
                er_regdate = DateTime.Now,
                er_a_id = AdminLoginSession.GetCookie(context).identifier
            };
            //dao.event_apply.InsertOnSubmit(entity);
            www6.insert(entity);
            //dao.SubmitChanges();
        }

        JsonWriter.Write(result, context);
    }

}