﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_participation_event_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<link rel="stylesheet" href="/@mgt/common/js/fullcalendar.min.css">
	<script type="text/javascript" src="/@mgt/common/js/fullcalendar.min.js"></script>
	<script type="text/javascript" src="/@mgt/common/js/fullcalendar.ko.js"></script>
    <script type="text/javascript" src="/@mgt/participation/event/schedule.js?v=1.2"></script>
    <script type="text/javascript" src="/@mgt/participation/event/update.js?v=1.2"></script>
    
    <script src="../../common/js/moment-timezone.min.js"></script>
    
	<style type="text/css">
		#e_type label, #e_target label, #direct_sponsor label, #winner_type label{margin-left:2px;margin-right:5px;}

        /* 시간선택 달력에 next, prev버튼 제거*/
        #schedule .fc-right{display:none;}

        /*fc-content*/
        .fc-content{cursor:pointer;height:25px;text-align:center;padding-top:2px;}

        .fc-time-grid-event{width: 200px;}

        .fc-slats tr{ height: 25px; }

        .fc-time-grid-event.fc-short .fc-title{font-size:1.4em;}
        .fc-time-grid-event .fc-time{font-size:1.4em;}
        
        /* 마감 버튼 */
        .schedule-label{width:60px;}

        .btnView{width:430px;}

        .request-content-style0{
            display:none;
        }
        .request-content-style1{
            cursor:pointer;
        }

	</style>
	<script type="text/javascript">

	    $(function () {
	        $hell.init();

	        $("#direct_sponsor input[type=radio]").on("ifChecked", function () {
				
				if ($(this).val() == "program") {
					$("#direct_sponsor_program").show();
				} else {
					$("#direct_sponsor_program").hide();
				}
			})
			if ($("#direct_sponsor input:checked").val() == "program") {
				$("#direct_sponsor_program").show();
			}

	        // 에디터
	        image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
		    initEditor(oEditors, "e_content");
		    initEditor(oEditors, "e_content_m");
	        initEditor(oEditors, "e_winner");


	        // 썸네일
	        var thumbUploader = attachUploader("e_thumb");
	        thumbUploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.image_participation)%>";
        	thumbUploader._settings.data.rename = "y";
        	thumbUploader._settings.data.limit = 300;

            
	        thumbUploader = attachUploader("e_thumb_m");
	        thumbUploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.image_participation)%>";
        	thumbUploader._settings.data.rename = "y";
        	thumbUploader._settings.data.limit = 300;


	        // 날짜 선택
	        $("#e_begin , #e_end , #e_open , #e_close").focus(function () {
	        	var className = $(this).data("range");
	        	$("." + className).trigger("click");
	        	$("." + className).focus();
		        return false;
	        })


	        // 당첨자 발표
	        $("#on_announce").bootstrapSwitch({
		        onSwitchChange: function (event, state) {
			        if (state) {
			            $(".announce_form").show();
			            if ($("#winner_type input[type=radio]:checked").val() == "editor") {
			                $(".annouce_editor").show();
			            } else {
			                $(".annouce_excel").show();
			            }

			        } else {
			            $(".announce_form").hide();
			            $("#e_announce").val("");
			            $("#e_winner").val("");
			            $(".annouce_editor").hide();
			            $(".annouce_excel").hide();
			            oEditors.getById["e_winner"].exec("UPDATE_CONTENTS_FIELD", []);
			        }
		        }
	        });

		    // 당첨자 입력방식 선택
	        if ($("#e_announce").val() != "" && $("#on_announce").is(":checked")) {
	            $(".announce_form").show();

	            if ($("#winner_type input[type=radio]:checked").val() == "editor") {
	                $(".annouce_editor").show();
	            } else {
	                $(".annouce_excel").show();
	            }
	        }
	        $("#winner_type input[type=radio]").on("ifChanged", function () {
	            if ($(this).val() == "excel") {
	                $(".annouce_editor").hide();
	                $(".annouce_excel").show();
	            } else {
	                $(".annouce_editor").show();
	                $(".annouce_excel").hide();
	            }
	        })


	        if ($("#tabm2").length > 0 && $("#calendar").is(":visible")) {
	            $page.init();
	        }

	        $(".btn_apply_download").click(function () {

	            var id = $(this).attr("data-id");
	            var title = $(this).attr("data-title");
	            var keyword = $("#s_keyword").val();

	            location.href = "/@mgt/participation/event/download?c=" + id + "&title=" + escape(title) + "&keyword=" + escape(keyword);
	            return false;
	        })


	        $('#registModal').on('show.bs.modal', function (event) {
	            $(this).find('.modal-title').text($(event.target).attr("data-date") + " 단체 등록")
	        })

	        $('#registModal').on('shown.bs.modal', function (e) {
	            var target = $(e.target);

	            target.find(".phone").mobileFormat();
	            target.find(".member").formatValidate({ match: true, message: "숫자만 입력 가능합니다.", pattern: /[0-9\-]+/g });

	            // 저장
	            target.find(".btnSave").click(function () {
	                if (target.find(".name").val() == "") {
	                    alert("이름을 입력해주세요.");
	                    target.find(".name").focus();
	                    return false;
	                }

	                if (target.find(".phone").val() == "") {
	                    alert("전화번호를 입력해주세요.");
	                    target.find(".phone").focus();
	                    return false;
	                }

	                if (target.find(".member").val() == "") {
	                    alert("입원을 입력해주세요.");
	                    target.find(".member").focus();
	                    return false;
	                }

	                var applyCnt = parseInt(target.attr("maxnum"), 10);
	                var applyPersonCnt = parseInt(target.attr("maxnumperson"), 10);
	                
	                if (applyCnt + 1 > $page.maximum) {
	                    alert('등록 가능한 최대팀을 초과하였습니다.');
	                    return false;
	                }
	                if (applyPersonCnt + parseInt(target.find(".member").val(), 10) > $page.maximum_person) {
	                    alert('등록 가능한 최대 인원수를 초과하였습니다. 가능한 인원수는 ' + ($page.maximum_person - applyPersonCnt) + '명 입니다.');
	                    return false;
	                }

	                var id = target.attr("data-id").replace("data-id", "");
	                var param = {
	                    id: id,
	                    name: target.find(".name").val(),
	                    phone: target.find(".phone").val(),
	                    member: target.find(".member").val()
	                }

	                $.post("/@mgt/participation/event/update.ashx?t=apply_team", param, function (r) {
	                    if (r.success) {

	                        // 달력 데이터 업데이트
	                        var date = target.attr("data-date").substring(0, 10);
	                        //$.grep($page.scheduleData[date], function (e) { return e.id == id; })[0].team++;
	                        var schData = $.grep($page.scheduleData[date], function (e) { return e.id == id; })[0];
	                        var memCnt = parseInt(target.find(".member").val(), 10);

	                        schData.organmember += memCnt;
	                        schData.organteam++;
	                        schData.totalmember += memCnt;

	                        console.log($page.scheduleData);

	                        $page.schedule.init(date);

	                        alert("등록되었습니다.");
                            
	                        target.find(".name").val("");
	                        target.find(".phone").val("");
	                        target.find(".member").val("");


	                        $('#registModal').modal('hide');

	                        //$('#calendar').fullCalendar('select','2017-11-18');
	                        //location.reload();

	                    } else {
	                        alert(r.message);

	                        target.find(".name").val("");
	                        target.find(".phone").val("");
	                        target.find(".member").val("");

	                        $('#registModal').modal('hide');
	                    }
	                });

	            });


	            // 취소
	            target.find(".btnCancel").click(function () {
	                target.find(".name").val("");
	                target.find(".phone").val("");
	                target.find(".member").val("");

	                $('#registModal').modal('hide');

	            });
	        })

	        $('#registModal').on('hidden.bs.modal', function (e) {
	            var target = $(e.target);
	            target.find(".btnSave").unbind("click");
	        })

            // 엑셀업로드
		    var uploader_excel = attachUploader("btn_winner_upload");
		    uploader_excel._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_participation)%>";


	        // 신청 날자 추가
	        $("#btn_add").click(function () {
	            addPick();
	        });

	        if ($("#picked_date").val() != "") {
	            var list = $.parseJSON($("#picked_date").val());
	            $.each(list, function () {
	                addPick(this);
	            });
            }

        });

        //신청내용 보기
        $(document).on("click", "a[name=btnViewContents]", function (e) {
        
            var contents = $(this).attr("contents");

            if (contents == "undefined" || contents == null) {
                contents = "";
            }

            var user = $(this).attr("user");

            if (user == "undefined" || user == null) {
                user = "";
            }

            $("#view_request_contents").text(contents);
            $("#contentsModalTitle").text(user);

            $('#contentsModal').modal('show');

        });
        //신청내용 닫기 - 동적으로 추가된 태그에 대한 이벤트
        $(document).on("click", "#btnClose", function () {
            $('#contentsModal').modal('hide');
        });
        
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    
    <asp:PlaceHolder runat="server" ID="hpExcelUpload" Visible="false">
	    <div class="alert alert-danger alert-dismissable">
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert">×</button>
		    <h4><i class="icon fa fa-check"></i>Alert!</h4>
            엑셀을 다운로드 한 뒤 '호환모드'로 변경하신 뒤 업로드 하세요.<br />
            엑셀의 '당첨여부'란에 O(영문)으로 표시된 회원이 당첨회원입니다.<br />
            엑셀 업로드시 기존 당첨내역은 삭제됩니다.<br />
	    </div>
    </asp:PlaceHolder>


    <input type="hidden" runat="server" id="scheduleData" />
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>
    <input type="hidden" runat="server" id="et_date" value="" />
    <input type="hidden" runat="server" id="e_id" value="" />
    <input type="hidden" runat="server" id="picked_date" value=''/>
    <input type="hidden" runat="server" id="formAction" value="" />
    <input type="hidden" runat="server" id="hdnTimeInterval" value="" />
    <input type="hidden" runat="server" id="hdnSchedulScrollTop" value="" />

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" CommandArgument="1" OnClick="btn_section_Click">기본정보</asp:LinkButton></li>
            <li runat="server" id="tabm2" class="" visible="false"><asp:LinkButton runat="server" id="btnScheduleTab" CommandArgument="2" OnClick="btn_section_Click">일정</asp:LinkButton></li>
            <li runat="server" id="tabm3" class="" visible="false"><asp:LinkButton runat="server" id="btnApplyTab" CommandArgument="3" OnClick="btn_section_Click">신청/참여</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="e_display" Checked="true" />
						</div>
					</div>
                    <div class="form-group">
						<label class="col-sm-2 control-label">신청확정노출</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="e_confirm_display" Checked="true" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">마감여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="e_closed" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">신청대상</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:RadioButtonList runat=server ID=e_target RepeatDirection="Horizontal">
                                <asp:ListItem Text="전체" Value="all"></asp:ListItem>
                                <asp:ListItem Text="후원자 전용" Value="sponsor"></asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">구분</label>
						<div class="col-sm-10" style="margin-top:5px;">
                            <asp:RadioButtonList runat="server" ID="e_type" AutoPostBack="true" RepeatLayout="Flow" RepeatDirection="Horizontal"></asp:RadioButtonList>
						</div>
					</div>
                    
                    <div class="form-group form-type form-date form-count form-complex form-request-contents" style="display:none;">
						<label class="col-sm-2 control-label control-label">신청 내용</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="e_request_contents" Checked="false" />
						</div>
					</div>

					<div class="form-group form-type form-date form-count form-complex form-experience" id="date_apply" style="display:none;">
						<label class="col-sm-2 control-label control-label">신청 가능 기간</label>
						<div class="col-sm-10">
							<button class="btn btn-primary btn-sm dayrange dayrange1 " data-from="e_begin" data-end="e_end"><i class="fa fa-calendar"></i></button>
							<asp:TextBox runat="server" ID="e_begin" Width="100px"  data-range="dayrange1" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
								~
							<asp:TextBox runat="server" ID="e_end" Width="100px"  data-range="dayrange1" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>
					</div>
                    
                                        
					<div class="form-group form-type form-experience" id="date_join" style="display:none;">
						<label class="col-sm-2 control-label control-label">체험전 기간</label>
						<div class="col-sm-10">
							<button class="btn btn-primary btn-sm dayrange dayrange2" data-from="e_open" data-end="e_close"><i class="fa fa-calendar"></i></button>
							<asp:TextBox runat="server" ID="e_open" Width="100px" data-range="dayrange2" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
								~
							<asp:TextBox runat="server" ID="e_close" Width="100px"  data-range="dayrange2" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                            <br/>* 일자별 오픈 시간 설정은 글 등록 이후 '일정' 탭에서 가능합니다.
						</div>
					</div>
                    
					<div class="form-group form-type form-count form-complex" style="display:none;" id="e_maximum_form">
						<label class="col-sm-2 control-label control-label">최대 인원</label>
						<div class="col-sm-10" style="margin-top:5px;">
                            <asp:DropDownList runat="server" ID="e_maximum" RepeatLayout="Flow" RepeatDirection="Horizontal" CssClass="form-control" style="width:80px;"></asp:DropDownList>
						</div>
					</div>
                    
					<div class="form-group form-type form-experience" style="display:none;" id="e_maximum_exper_form">
						<label class="col-sm-2 control-label control-label">최대 팀</label>
						<div class="col-sm-10" style="margin-top:5px;">
                            <asp:DropDownList runat="server" ID="e_maximum_exper" RepeatLayout="Flow" RepeatDirection="Horizontal" CssClass="form-control" style="width:80px;"></asp:DropDownList>
                            * 입력하신 최대 팀 수 기준으로 해당 체험전 전체 타임에 마감 제한이 적용됩니다.
						</div>
					</div>
                    
                    <div class="form-group form-type form-experience" style="display:none;">
						<label class="col-sm-2 control-label control-label">예약분단위</label>
						<div class="col-sm-10" style="margin-top:5px;">
                            <asp:DropDownList runat="server" ID="e_time_interval" RepeatLayout="Flow" RepeatDirection="Horizontal" CssClass="form-control" style="width:80px;"></asp:DropDownList>
                            * 선택하지 않으면 한시간 단위로 적용됩니다.
						</div>
					</div>
                    
					<div class="form-group form-type form-date form-complex" id="date_pick_form" style="display:none;">
						<label class="col-sm-2 control-label control-label">참여 시간</label>
						<div class="col-sm-10">
                            <button id="btn_add" type="button" class="btn btn-danger">추가</button><br />
                            <div id="pick_date"></div>
						</div>
					</div>

                    <div class="form-group">
						<label class="col-sm-2 control-label control-label">제목</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=e_title CssClass="form-control" Width=600 MaxLength="50"></asp:TextBox>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10">
							<textarea name="e_content" id="e_content" runat="server" style="width:600px; height:212px; display:none;"></textarea>
                        </div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">모바일 내용</label>
						<div class="col-sm-10">
							<textarea name="e_content_m" id="e_content_m" runat="server" style="width:600px; height:212px; display:none;"></textarea>
                        </div>
					</div>
                    

					<div class="form-group form-type form-experience" style="display:none;">
						<label class="col-sm-2 control-label control-label">위치</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=e_location CssClass="form-control" Width=600></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group form-type form-count" style="display:none;">
						<label class="col-sm-2 control-label control-label">선착순 인원</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=e_limit CssClass="form-control" Width=200></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">썸네일<br />(410 * 170px)</label>
                        
						<div class="col-sm-10">
							<img id="e_thumb" class="e_thumb" runat=server src="/@mgt/common/img/empty_thumb.png" style="max-width:400px;" />
						</div>
					</div>

					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">모바일 썸네일<br />(636 * 263px)</label>
						<div class="col-sm-10">
							<img id="e_thumb_m" class="e_thumb_m" runat=server src="/@mgt/common/img/empty_thumb.png" style="max-width:400px;" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">바로 후원 기능<br />노출 여부(선택)</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:RadioButtonList runat=server ID=direct_sponsor RepeatLayout="Flow" RepeatDirection="Horizontal">
								<asp:ListItem Text="사용안함" Value="" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="어린이 추천" Value="child"></asp:ListItem>
                                <asp:ListItem Text="진행중인 후원 프로그램" Value="program"></asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>

					<div class="form-group" id="direct_sponsor_program" style="display:none">
						<label class="col-sm-2 control-label control-label">진행중인 프로그램정보</label>
						<div class="col-sm-10" style="">
                            <asp:DropDownList runat="server" ID="campaignId" CssClass="form-control inline" style="width:300px"></asp:DropDownList>
							<input type="text" id="e_sf_prices" runat="server" class="form-control inline" style="width:300px" placeholder="만 원 이상, ‘,’쉼표로 금액 구분"/>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">당첨자 발표</label>
						<div class="col-sm-10" style="margin-top:5px;">
                            <input type="checkbox" runat="server" id="on_announce">
						</div>
					</div>
                    
					<div class="form-group announce_form" style="display:none;">
						<label class="col-sm-2 control-label control-label">당첨자 발표일</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=e_announce CssClass="date form-control" style="float:left;" Width=100></asp:TextBox>
                            <br /><br /><div>당첨자 발표일 전까지 노출되지 않습니다.</div>
						</div>
					</div>
                    
					<div class="form-group announce_form" style="display:none;">
						<label class="col-sm-2 control-label control-label">당첨자 선정 타입</label>
						<div class="col-sm-10" style="margin-top:5px;">
                            <asp:RadioButtonList runat=server ID=winner_type RepeatLayout="Flow" RepeatDirection="Horizontal">
                                <asp:ListItem Text="엑셀 업로드" Value="excel" Selected="True"></asp:ListItem>
								<asp:ListItem Text="당첨자 입력" Value="editor"></asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>
                    
					<div class="form-group annouce_editor" style="display:none;">
						<label class="col-sm-2 control-label control-label">당첨자 입력</label>
						<div class="col-sm-10">
                            <textarea name="e_winner" id="e_winner" runat="server" style="width:600px; height:212px; display:none;"></textarea>
						</div>
					</div>
                    
					<div class="form-group annouce_excel" style="display:none;">
						<label class="col-sm-2 control-label control-label">당첨자 입력</label>
						<div class="col-sm-10">
                            <pre style="width:600px;">엑셀 업로드를 통한 당첨자 발표는 글 등록 후 [신청/참여]탭에서 사용 가능합니다.</pre>
						</div>
					</div>

                    <asp:PlaceHolder runat="server" ID="ph_regdate" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="e_regdate"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>
				</div>
	
	             <div class="box-footer clearfix text-center">
         
		            <asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		            <asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		            <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	            </div>
			</div>
		    
			<div class="tab-pane" id="tab2" runat="server" >
                
				<div class="form-horizontal">
                    <div class="form-group">
						<div class="col-sm-4">
                            <div id="calendar"></div>
                        </div>
                        <div class="col-sm-8">
                            <div id="schedule"></div>

                            <!--
                            <div class="pull-right" style="margin-top:20px;">
                                <input type="text" id="beginTime" class="form-control inline" style="width:70px;"/> ~ 
                                <input type="text" id="endTime" class="form-control inline" style="width:70px;"/>
                            </div>
                            -->
                            *일별로 체험 가능 시간을 드래그하신 후 [등록]을 클릭해 주세요.
                        </div>
                    </div>

                </div>

                
                <div class="box-footer clearfix text-right">
		            <asp:LinkButton runat=server ID=btnSchedule OnClick="btnSchedule_Click" OnClientClick="return onSchedule();" class="btn btn-danger">등록</asp:LinkButton>
	            </div>
                
            </div>

            <div class="tab-pane" id="tab3" runat="server" >
                <div class="box box-default collapsed-box search_container">
		            <div class="box-header with-border section-search" >
			            <div class="pull-left ">
				            <button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				            <h3 class="box-title" style="padding-top:5px">검색</h3>
			            </div>
			
		            </div>
		            <div class="form-horizontal box-body" >
			            <div class="box-body">
				            <div class="form-group">
					            <label for="s_keyword" class="col-sm-2 control-label">검색</label>
					            <div class="col-sm-10">
						            <asp:TextBox runat="server" id="s_keyword" Width="415" cssClass="form-control inline"  OnTextChanged="search" AutoPostBack="true"  placeholder="이름, 아이디, 연락처 검색"></asp:TextBox>
						        </div>
				            </div>
				            <div class="form-group">
					            <label for="s_b_date" class="col-sm-2 control-label">기간 검색(신청일)</label>
					            <div class="col-sm-10">
						            <div class="inline">
							            <asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							            ~
						            <asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						            </div>

						            <div class="inline">
							            <button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							            <div class="btn-group">
								            <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								            <a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								            <a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								            <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								            <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								            <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							            </div>
						            </div>

					            </div>

				            </div>

				            <div class="box-footer clearfix text-center">
                                <asp:LinkButton runat="server" ID="btn_search" class="btn btn-primary" style="width:100px" OnClick="search2">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
				            </div>
				
			            </div>
			
		            </div>
	            </div>
                <!-- /.box -->

               <%-- <div class="box-header">
                    <span class="pull-right">
                    <input type="text" runat="server" id="s_keyword2" class="form-control inline" style="width:300px; margin-right:10px;" OnTextChanged="search" AutoPostBack="true" placeholder="아이디, 이름검색" />
						<asp:LinkButton runat="server"  class="btn btn-primary inline" style="width:70px;display:inline-block;vertical-align:top;margin-top:2px" OnClick="search2">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
                        </span>
                </div>--%>
				<div class="box-header">
					<h3 class="box-title">총 <asp:Literal runat="server" ID="lbTotal" /> 건</h3>
					<span class="pull-right">
						<uc:paging runat="server" ID="paging_apply" OnNavigate="paging_apply_Navigate" EnableViewState="true" RowsPerPage="20" PagesPerGroup="10" Hash="p2" />
						<a href="#" class="btn btn-bitbucket pull-right btn_apply_download" runat="server" id="btn_apply_download" style="margin-left:5px">다운로드</a>
                        <a href="#" class="btn btn-danger pull-right btn_winner_upload" runat="server" id="btn_winner_upload" style="margin-left:5px">당첨자 업로드</a>
					</span>
				</div>
				<!-- /.box-header -->

				<div class="box-body table-responsive no-padding">
					
					<table class="table table-hover table-bordered ">
						<colgroup>
							<col width="3%" />
							<col width="7%" />
							<col width="6%" />
                            <col width="10%" />
							<col width="10%" />
                            <col width="5%" />
							<col width="5%" />
							<col width="8%" />
							<col width="8%" />
							<col width="7%" />
							<col width="7%" />
                            <col width="7%" />
                            <col width="7%" />
							<col width="5%" />
						</colgroup>
						<thead>
							<tr>
								<th>번호</th>
								<th>아이디</th>
								<th>이름</th>
								<th>연락처</th>
                                <th>신청일</th>
                                <th>시간</th>
								<th>신청인원</th>
                                <th>경로</th>
                                <th>생일</th>
                                <th>후원여부</th>
                                <th>등록일자</th>
                                <th>참석여부</th>
                                <th>결연여부</th>
                                <th>당첨여부</th>
                                <th>신청내용</th>
                                <th>삭제</th>
							</tr>
						</thead>
						<tbody>

							<asp:Repeater runat="server" ID="repeater_apply" OnItemDataBound="repeater_apply_ItemDataBound">
								<ItemTemplate>
									<tr class="tr_over">
										<td><%#(Convert.ToInt32(Eval("total" )) - Convert.ToInt32(Eval("rownum" )) + 1)%></td>
										<td><asp:Literal runat="server" ID="lbUserId"></asp:Literal> </td>
										<td><%#Eval("er_name" )%></td>
										<td><%#Eval("er_phone" )%></td>
                                        <td> <asp:Literal ID="lbDate" runat="server"/></td>
                                        <td> <asp:Literal ID="lbTime" runat="server"/></td>
										<td>
                                            <asp:DropDownList runat="server" ID="member_count" Visible="false" CssClass="form-control" style="display:inline-block;text-align:center; min-width:65px;">
                                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:TextBox runat="server" ID="team_count" Visible="false" CssClass="form-control"></asp:TextBox>

                                            <asp:Literal runat="server" ID="none_count" Visible="false">-</asp:Literal>
										</td>

                                        <td><%#Eval("er_route" )%></td>
                                        <td><%#Eval("BirthDate" )%></td>
                                        <td><%#Eval("IsSponsor" )%></td>
                                        <td><%#Eval("er_regdate" , "{0:yyyy'.'MM'.'dd HH:mm}")%></td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="attended" CssClass="form-control" style="width:70px;display:inline-block;text-align:center">
                                                <asp:ListItem Text="참석" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="불참" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="related" CssClass="form-control" style="width:100px;display:inline-block;text-align:center">
                                                <asp:ListItem Text="결연" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="미결연" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
										<td><%#Eval("ew_id").ToString() == "-1" ? "X" : "O" %></td>
                                        <td> 
                                            <a name='btnViewContents' contents='<%#Eval("er_request_contents").ToString() %>' user='<%#Eval("er_name") %>' class='request-content-style<%#Eval("is_contents") %>'>보기</a>
                                            <%--<%#Eval("er_request_contents") != null && Eval("er_request_contents").ToString() != "" ? "보기" : ""%>--%>
                                                <%--<a name='btnViewContents' contents='<%#Eval("er_request_contents").ToString() %>'>보기</a>--%>
                                        </td>
                                        <td>
                                            <asp:LinkButton runat="server" ID="btnRemove" OnClick="btnRemove_Click" OnClientClick="return confirm('삭제하시겠습니까?');" CommandArgument=<%#Eval("er_id") %>  CssClass="btn btn-danger">삭제</asp:LinkButton>
                                        </td>

									</tr>
								</ItemTemplate>
								<FooterTemplate>
									<tr runat="server" visible="<%#repeater_apply.Items.Count == 0 %>">
										<td colspan="12">데이터가 없습니다.</td>
									</tr>
								</FooterTemplate>
							</asp:Repeater>

						</tbody>
					</table>
				</div>

                <div class="box-footer clearfix text-right">
		            <asp:LinkButton runat=server ID=btnUpdate OnClick="btnUpdate_Click" OnClientClick="return onUpdate();" class="btn btn-bitbucket">수정</asp:LinkButton>
	            </div>

			</div>

			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

    <!-- modal 캠페인 신청내용 '보기'-->
    <div class="modal fade" id="contentsModal" tabindex="-1" role="dialog" aria-labelledby="contentsModalTitle">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="contentsModalTitle"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
					    <div class="form-group">
						    <label class="col-sm-2 control-label">신청내용</label>
						    <div class="col-sm-10">
                                <span id="view_request_contents"></span>
						    </div>
					    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btnClose" id="btnClose">닫기</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal 캠페인 신청내용 '보기' -->

    <!-- modal -->
    <div class="modal fade" id="registModal" tabindex="-1" role="dialog" aria-labelledby="registModalTitle">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="registModalTitle">단체 등록</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
					    <div class="form-group">
						    <label class="col-sm-2 control-label">이름</label>
						    <div class="col-sm-10">
							    <input type="text" class="form-control name" style="width:200px;" />
						    </div>
					    </div>

                        <div class="form-group">
						    <label class="col-sm-2 control-label">연락처</label>
						    <div class="col-sm-10">
							    <input type="text" class="form-control phone" style="width:200px;" />
						    </div>
					    </div>
                        
                        <div class="form-group">
						    <label class="col-sm-2 control-label">인원</label>
						    <div class="col-sm-10">
							    <input type="text" class="form-control member" style="width:200px;" />
						    </div>
					    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btnCancel" >취소</button>
                    <button type="button" class="btn btn-primary btnSave">저장</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal -->


</asp:Content>