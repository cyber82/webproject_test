﻿var $hell = {
    init: function () {
        $hell.setType();
    },

    setType: function () {
        $("#e_type input[type=radio]").on("ifChecked", function () {
            $hell.typeViewer($(this).val());
        });
        $hell.typeViewer($("#e_type input:checked").val());
    },

    typeViewer: function (type) {
        $(".form-type").hide();
        $(".form-" + type).show();

        if (type == 'experience') {
            $('.form-count.form-complex').show();
            $('.form-request-contents').hide();
        }
    }
}


// 선택날짜
var joinTimeSetter = function () {
    var list = [];
    $.each($("#pick_date .date-item"), function () {
        var date = $(this).find(".date_pick").val();
        var limit = $(this).find(".date_limit").val();
        list.push({ date: date, limit: limit });
    });
    $("#picked_date").val(JSON.stringify(list));
}


var attachUploader = function (button) {
    return new AjaxUpload(button, {
        action: '/common/handler/upload',
        responseType: 'json',
        onChange: function () {
            try {

                // 이미지 업로드 되면 에디터 초기화 되는 현상 방지
                // oEditors가 없는 경우 에러남
                if (oEditors) {
                    $.each(oEditors, function () {
                        this.exec("UPDATE_CONTENTS_FIELD", []);
                    });
                }

                joinTimeSetter();

            } catch (e) { }
        },
        onSubmit: function (file, ext) {
            this.disable();
        },
        onComplete: function (file, response) {
            this.enable();
            if (response.success) {
                // 마우스 오버시 hover클래스 삽입됨
                var c = $("#" + button).attr("class").split(" ").join("").split("hover").join("");
                $(".temp_file_type").val(c.indexOf("thumb") > -1 ? c : "file");
                $(".temp_file_name").val(response.name);
                $(".temp_file_size").val(response.size);
                eval($(".btn_update_temp_file").attr("href").replace("javascript:", ""));
            } else
                alert(response.msg);
        }
    });
}

var onSubmit = function () {


    oEditors.getById["e_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.
    oEditors.getById["e_content_m"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.
    oEditors.getById["e_winner"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

    if ($("#e_target input[type=radio]:checked").length < 1) {
        alert("신청 대상을 선택하세요.");
        $("#e_target input[type=radio]:first").focus();
        return false;
    }

    if ($("#e_type input[type=radio]:checked").length < 1) {
        alert("구분을 선택하세요.");
        $("#e_type input[type=radio]:first").focus();
        return false;
    }

    if (($("#e_type input:checked").val() == "count" || $("#e_type input:checked").val() == "complex") && $("#e_maximum").val() == "-1") {
        alert("최대 인원을 선택하세요.");
        $("#e_maximum").focus();
        return false;
    }

    //if (($("#e_type input:checked").val() == "experience") && $("#e_maximum_exper").val() == "-1") {
    //	alert("최대 팀을 선택하세요.");
    //	$("#e_maximum_exper").focus();
    //	return false;
    //}

    if (($("#e_type input:checked").val() == "experience")) {
        //if ($("#e_maximum").val() == "-1" && $("#e_maximum_exper").val() == "-1") {
        //    alert("최대 인원 또는 최대 팀을 선택하세요.");
        //    return false;
        //}
        //if ($("#e_maximum").val() != "-1" && $("#e_maximum_exper").val() != "-1") {
        //    alert("최대 인원 또는 최대 팀 중 하나만 선택하세요.");
        //    return false;
        //}

        if ($("#e_maximum").val() == "-1") {
            alert("최대 인원을 선택하세요.");
            return false;
        }
        if ($("#e_maximum_exper").val() == "-1") {
            alert("최대 팀을 선택하세요.");
            return false;
        }
    }

    if ($("#e_begin").is(":visible")) {
        if (!validateForm([
			{ id: "#e_begin", msg: "신청 가능 기간을 입력하세요." },
			{ id: "#e_end", msg: "신청 가능 기간을 입력하세요." }
        ])) {
            return false;
        }
    }
    if ($("#e_open").is(":visible")) {
        if (!validateForm([
			{ id: "#e_open", msg: "참여 기간을 입력하세요." },
			{ id: "#e_close", msg: "참여 기간을 입력하세요." }
        ])) {
            return false;
        }
    }

    var ok = true;
    if ($("#date_pick_form").is(":visible")) {
        if ($("#pick_date").find("input").length < 1) {
            alert("참여시간을 추가하세요.");
            $("#btn_add").focus();
            return false;
        }
        $.each($("#pick_date").find("input"), function () {
            if ($(this).val() == "") {
                if ($(this).hasClass("date_pick")) {
                    alert("참여시간을 입력하세요");
                } else {
                    alert("인원제한을 입력하세요");
                }
                $(this).focus();
                ok = false;
                return false;
            }
        })

    }
    if (!ok) return false;

    joinTimeSetter();

    if (!validateForm([
		{ id: "#e_title", msg: "제목을 입력하세요." }
    ])) {
        return false;
    }

    if ($("#e_location").is(":visible")) {
        if (!validateForm([
			{ id: "#e_location", msg: "위치를 입력하세요." }
        ])) {
            return false;
        }
    }

    if (!$("#e_thumb").is('[data-exist]')) {
        alert("PC 썸네일 이미지를 등록해 주세요.");
        $("#e_thumb").focus();
        return false;
    }


    if (!$("#e_thumb_m").is('[data-exist]')) {
        alert("모바일 썸네일 이미지를 등록해 주세요.");
        $("#e_thumb_m").focus();
        return false;
    }

    if ($("#on_announce").bootstrapSwitch('state')) {

        if (!validateForm([
			{ id: "#e_announce", msg: "당첨자 발표일을 선택하세요." }
        ])) {
            return false;
        }
    }

    if ($("#e_limit").val() != "") {
        if (!validateForm([
			{ id: "#e_limit", msg: "", type: "numeric" }
        ])) {
            return false;
        }
    }

    return confirm(($("#formAction").val() == "update" ? "수정" : "추가") + " 하시겠습니까?");
}

var onRemove = function () {
    return confirm("삭제하시겠습니까?");
}

var onSchedule = function () {
    // 날짜가 키로 들어있는 object array에서 key 제거
    var data = [];
    $.each($page.scheduleData, function () {
        $.each(this, function () {
            data.push(this);
        })
    });

    if (data.length < 1) {
        alert("등록된 일정이 없습니다.");
        return false;
    }

    $("#scheduleData").val(JSON.stringify(data));
}

var onUpdate = function () {
    return confirm("데이터를 수정하시겠습니까?");
};

var addPick = function (entity) {
    var element = $('<div class="date-item"><input type="text" class="form-control inline datetime date_pick" readonly Style="margin-top: 5px;width:140px;" value="' + (entity ? entity.date : "") + '"></input>&nbsp;&nbsp;인원제한<input type="text"  class="form-control inline datetime date_limit" Style="margin-top: 5px;width:70px;" maxlength="5" value="' + (entity ? entity.limit : "") + '"/><a href="#" class="remove-pick">삭제</a><br /></div>');

    element.find(".date_pick").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        timePicker: true,
        format: 'YYYY-MM-DD HH:mm',
        locale: {
            applyLabel: '적용',
            cancelLabel: '취소',
            fromLabel: '시작일',
            toLabel: '종료일',
            weekLabel: 'W',
            customRangeLabel: '사용자 지정',
            daysOfWeek: ['일', '월', '화', '수', '목', '금', '토'],
            monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
        },
    });
    element.find(".remove-pick").click(function () {
        element.remove();
        return false;
    });

    $("#pick_date").append(element);
}
