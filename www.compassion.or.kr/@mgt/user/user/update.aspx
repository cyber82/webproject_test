﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_user_user_update" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<style>
		.col-sm-10 {padding-top: 7px;}
	</style>
	<script type="text/javascript" src="update.js"></script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li runat="server" id="tabm1" visible="false" class="active"><a>기본정보</a></li>
		</ul>
		<div class="tab-content">
		
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">
					
					<div class="col-xs-12">
						<h2 class="page-header"><i class="fa fa-plus-square"></i> 기본정보</h2>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">회원아이디</label>
						<div class="col-sm-10" >
							<asp:Literal runat="server" ID="userId"></asp:Literal>
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-2 control-label">후원회원아이디</label>
						<div class="col-sm-10" >
							<asp:Literal runat="server" ID="sponsorId"></asp:Literal>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">CONID</label>
						<div class="col-sm-10" >
							<asp:Literal runat="server" ID="conId"></asp:Literal>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">이름</label>
						<div class="col-sm-10" >
							<asp:Literal runat="server" ID="sponsorName"></asp:Literal>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">회원종류</label>
						<div class="col-sm-10" >
							<asp:Literal runat="server" ID="userkind"></asp:Literal>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">생일</label>
						<div class="col-sm-10" >
							<asp:Literal runat="server" ID="birthdate"></asp:Literal>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">가입일</label>
						<div class="col-sm-10" >
							<asp:Literal runat="server" ID="userdate"></asp:Literal>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">회원구분</label>
						<div class="col-sm-10" >
							<asp:Literal runat="server" ID="userclass"></asp:Literal>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">휴대폰/집전화/이메일</label>
						<div class="col-sm-10" >
							<asp:Literal runat="server" ID="mobile"/> /
							<asp:Literal runat="server" ID="phone"/> / 
							<asp:Literal runat="server" ID="email"/>
						</div>
					</div>

					<asp:PlaceHolder runat="server" ID="ph_addr">
					<div class="form-group">
						<label class="col-sm-2 control-label">주소</label>
						<div class="col-sm-10" >
							<asp:Literal runat="server" ID="zipcode"/> /
							<asp:Literal runat="server" ID="addr"/> 
						</div>
					</div>
					</asp:PlaceHolder>

					<div class="form-group">
						<label class="col-sm-2 control-label">SMS/이메일 수신동의</label>
						<div class="col-sm-10" >
							<asp:Literal runat="server" ID="agree"/>
						</div>
					</div>

                    <div class="form-group" runat="server" ID="attach_file_view" visible="false">
                        <label class="col-sm-2 control-label">첨부파일</label>
                        <div class="col-sm-10">
                            <a runat="server" ID="attach_file" target="_blank"/>
                        </div>
                    </div>
					
					<asp:PlaceHolder runat="server" ID="ph_join" Visible="false">
					<div class="col-xs-20" style="padding-top:30px">
						<h2 class="page-header"><i class="fa fa-plus-square"></i> 오프라인회원 연결</h2>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">회원검색</label>
						<div class="col-sm-2"  style="width:600px">
							<asp:TextBox runat="server" ID="s_keyword" Width="300px" CssClass="form-control" style="float:left;display:inline-block" placeholder="이름/CONID/SPONSORID 입력"></asp:TextBox>
							<div style="float:left;display:inline-block;padding-left:10px;padding-top:2px">
								<asp:LinkButton class="btn btn-sm btn-info btn-flat" runat="server" ID="btn_search" OnClick="btn_search_Click">검색</asp:LinkButton>
							</div>

						</div>
					</div>

					<asp:PlaceHolder runat="server" ID="ph_search_result" Visible="false">
					<div class="form-group">
						<label class="col-sm-2 control-label">회원선택</label>
						<div class="col-sm-2" style="width:600px">
							<asp:DropDownList runat="server" ID="s_user"  Width="300px" CssClass="form-control" style="float:left;display:inline-block">
								<asp:ListItem Text="회원검색에서 검색어 입력 후 Enter"></asp:ListItem>
							</asp:DropDownList>
							<div style="float:left;display:inline-block;padding-left:10px;padding-top:2px">
								<asp:LinkButton class="btn btn-sm btn-danger btn-flat" runat="server" ID="btn_join" OnClick="btn_join_Click" OnClientClick="return confirm('오프라인 회원과 연결 하시겠습니까?')">연결</asp:LinkButton>
							</div>
						</div>
					</div>
					</asp:PlaceHolder>
					</asp:PlaceHolder>

				</div>


			</div>
	
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->


	 <div class="box-footer clearfix text-center">
        
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>

</asp:Content>