﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class _mgt_user_popupUserLogout : FrontBasePage2
{
    protected override void OnBeforePostBack()
    {
        var currentDomain = ConfigurationManager.AppSettings["domain"];
        AuthUser auth = new AuthUser();

        auth.Logout(); 

        Response.Redirect(currentDomain + "@mgt/user/user/");
    }

    
}