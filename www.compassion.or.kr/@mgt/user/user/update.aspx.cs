﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Web.UI.HtmlControls;

public partial class mgt_user_user_update : AdminBasePage {

    protected override void OnBeforePostBack() {

        base.OnBeforePostBack();
        v_admin_auth auth = base.GetPageAuth();
        base.PrimaryKey = Request["c"];

        btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

        this.GetData();

    }

    protected override void OnAfterPostBack() {
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();
    }

    void GetData() {
        using(AuthDataContext dao = new AuthDataContext()) {

            //var row = dao.tSponsorMaster.First(p => p.UserID == PrimaryKey);
            //var row = dao.sp_tSponsorMaster_get(PrimaryKey).First();
            Object[] op1 = new Object[] { "UserID" };
            Object[] op2 = new Object[] { PrimaryKey };
            var row = www6.selectSPAuth("sp_tSponsorMaster_get", op1, op2).DataTableToList<sp_tSponsorMaster_getResult>().First();

            userId.Text = row.UserId;
            sponsorId.Text = row.SponsorId;
            sponsorName.Text = row.SponsorName;
            conId.Text = row.ConId;

            switch(row.GenderCode) {
                default:
                userkind.Text = "개인회원";
                break;
                case "C":
                userkind.Text = string.Format("기업회원 (사업자번호 : {0} , 담당자 : {1})", row.ComRegistration, row.ManagerName);
                attach_file_view.Visible = true;
                if(row.FilePath == "") {
                    attach_file.InnerHtml = "등록된 첨부파일이 없습니다.";
                } else {
                    attach_file.InnerHtml = attach_file.HRef = row.FilePath;
                }

                break;
                case "M":
                userkind.Text = "개인회원";
                sponsorName.Text += " (남)";
                break;
                case "F":
                userkind.Text = "개인회원";
                sponsorName.Text += " (여)";
                break;
            }

            if(!string.IsNullOrEmpty(row.BirthDate) && row.BirthDate.Length >= 8)
                birthdate.Text = row.BirthDate.ToDateFormat("-");
            userdate.Text = row.UserDate.HasValue ? row.UserDate.Value.ToString("yyyy-MM-dd") : "";
            userclass.Text = row.UserClass;

            mobile.Text = string.IsNullOrEmpty(row.Mobile) ? "없음" : row.Mobile;
            phone.Text = string.IsNullOrEmpty(row.Phone) ? "없음" : row.Phone;
            email.Text = string.IsNullOrEmpty(row.Email) ? "없음" : row.Email;


            zipcode.Text = row.ZipCode;
            addr.Text = string.Format("{0} {1}", row.Addr1, row.Addr2);

            ph_addr.Visible = !string.IsNullOrEmpty(row.ZipCode);
            agree.Text = row.AgreeEmail.HasValue && row.AgreeEmail.Value ? "동의" : "거절";

            ph_join.Visible = false;
            if(string.IsNullOrEmpty(row.SponsorId)) {
                ph_join.Visible = true;
            }
        }
    }

    protected void btn_section_Click( object sender, EventArgs e ) {
        LinkButton obj = sender as LinkButton;
        var no = obj.CommandArgument;

        int tabCount = 3;

        for(int i = 1; i < tabCount + 1; i++) {

            if(no == i.ToString()) {
                ((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "active";
                ((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "active tab-pane";
            } else {
                ((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "";
                ((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "tab-pane";

            }


        }

    }

    // 오프라인 회원 연결
    protected void btn_join_Click( object sender, EventArgs e ) {

        if(base.IsRefresh) {
            return;
        }

        var sponsorId = s_user.SelectedValue;
        var items = this.ViewState["users"].ToString().ToObject<List<SponsorAction.SponsorItem>>();
        SponsorAction.SponsorItem item = items.First(p => p.SponsorID == sponsorId);
        item.UserID = base.PrimaryKey.ToString();

        var actionResult = new SponsorAction().JoinOnOffUse(item);
        if(actionResult.success) {

            this.GetData();

        } else {
            base.AlertWithJavascript(actionResult.message);
        }

    }

    // 오프라인 회원 검색
    protected void btn_search_Click( object sender, EventArgs e ) {


        var actionResult = new SponsorAction().SearchOfflineUser(s_keyword.Text);
        if(actionResult.success) {

            this.ViewState["users"] = actionResult.data.ToJson();
            s_user.Items.Clear();
            foreach(SponsorAction.SponsorItem item in (List<SponsorAction.SponsorItem>)actionResult.data) {
                s_user.Items.Add(new ListItem(string.Format("{0}/{1}/{2}", item.SponsorName, item.SponsorID, item.ConID), item.SponsorID));
            }

            ph_search_result.Visible = true;


        } else {
            base.AlertWithJavascript(actionResult.message);
        }

    }
}