﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_user_user_default" MasterPageFile="~/@mgt/mgt_afterlogin.master" %>

<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <script type="text/javascript">
        
	    $(document).ready(function () {
            $('.openPop').click(function () {

                $('#loading_bg').show();
                $('.loading_box').show();
                $(".msg").html('잠시만 기다려 주세요.');
	            $('iframe').attr("src", "popupUserDetail.aspx?i=" + this.id);
	        });

            //close
            $("#closeModal").click(function () {
                //$(location).attr('href','popupUserLogout.aspx');
                var url = "/@mgt/api/popupLogout.ashx";
                var params = "";

                $.ajax({
                    type: "POST",
                    url: url,
                    data: params,
                    success: function (args) {
                        //$("#result").html(args);
                        //hideWindow();
                    },
                    
                    error: function (e) {
                        alert(e.responseText);
                    }
                });
            });
        });
        $(function () {
            $('#userDetailInfo').load(function () {
                $('#loading_bg').hide()
                $('.loading_box').hide();
            });

    
        });
    </script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    
    <!-- 로딩 -->
    <%--<div style="width: 100%; height: 100%; position: fixed; left: 0; top: 0; z-index: 1990; opacity: 0.0; background: #000; display: none" id="loading_bg"></div>--%>
    <div class="loading_box" style="position: fixed; left: 35%; width: 30%; top: 40%; z-index: 1991; display: none;" id="loading_container">
        <span class="msg"></span>
    </div>
    <!--//-->
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert">×</button>
        <h4><i class="icon fa fa-check"></i>Alert!</h4>
        웹회원가입정보만 조회가 가능합니다.
    </div>

    <div class="box box-default collapsed-box search_container">
        <div class="box-header with-border section-search">
            <div class="pull-left ">
                <button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;"><i class="fa fa-plus search_toggle"></i></button>
                <h3 class="box-title" style="padding-top: 5px">검색</h3>
            </div>

        </div>
        <div class="form-horizontal box-body">

            <div class="form-group">
                <label for="s_b_date" class="col-sm-2 control-label">기간 검색(등록일)</label>
                <div class="col-sm-10">
                    <div class="inline">
                        <asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                        ~
						<asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                    </div>

                    <div class="inline">
                        <button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

                        <div class="btn-group">
                            <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
                            <a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
                            <a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
                            <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
                            <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
                            <a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
                        </div>
                    </div>

                </div>

            </div>


            <div class="form-group">
                <label for="s_keyword" class="col-sm-2 control-label">검색</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" ID="s_keyword" Width="415" CssClass="form-control inline" OnTextChanged="search" AutoPostBack="true"></asp:TextBox>
                    * 이름 , 회원아이디
                </div>
            </div>

            <div class="box-footer clearfix text-center">
                <asp:LinkButton runat="server" ID="LinkButton1" class="btn btn-primary" Style="width: 100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
            </div>

        </div>
    </div>

    <br />


    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">총 <strong>
                <asp:Literal runat="server" ID="lbTotal"></asp:Literal></strong> 건</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">

            <table class="table table-hover table-bordered ">
                <colgroup>
                    <col width="5%" />
                    <col width="15%" />
                    <col width="15%" />
                    <col width="*" />
                    <col width="15%" />
                    <col width="15%" />
                    <col width="15%" />
                </colgroup>
                <thead>
                    <tr>
                        <th>번호</th>
                        <th>회원아이디</th>
                        <th>후원회원아이디</th>
                        <th>이름</th>
                        <th>거주구분</th>
                        <th>가입일</th>
                        <th>회원구분</th>
                        <th>상세보기</th>
                    </tr>
                </thead>
                <tbody>

                    <asp:Repeater runat="server" ID="repeater" OnItemDataBound="ListBound">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
                                <td onclick="goPage('update.aspx' , { c: '<%#Eval("userId") %>', p: '<%#paging.CurrentPage %>' })" class="tr_link text-left"><%#Eval("userid") %></td>
                                <td onclick="goPage('update.aspx' , { c: '<%#Eval("userId") %>', p: '<%#paging.CurrentPage %>' })" class="text-left"><%#Eval("sponsorId") %></td>
                                <td onclick="goPage('update.aspx' , { c: '<%#Eval("userId") %>', p: '<%#paging.CurrentPage %>' })" class="text-left"><%#Eval("sponsorName") %></td>
                                <td><%#Eval("locationType") %></td>
                                <td><%#Eval("userdate" , "{0:yy'/'MM'/'dd}") %></td>
                                <td><%#Eval("userclass") %></td>
                                <td>
                                    <button type="button" data-toggle="modal" data-target="#showDetailUserInfo" data-backdrop="static" class="showDetail openPop" id="<%#Eval("userId") %>">조회</button></td>
                                <%-- <td><button onclick="window.open('popupUserDetail.aspx?i=<%#Eval("userid") %>', 'test', width=400, height=300 )">조회</button></td>--%>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr runat="server" visible="<%#repeater.Items.Count == 0 %>">
                                <td colspan="7">데이터가 없습니다.</td>
                            </tr>
                        </FooterTemplate>
                    </asp:Repeater>

                </tbody>
            </table>
        </div>

        <div id="showDetailUserInfo" class="modal showDetail fade" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" style="width: 1020px;">
                <div class="modal-content">
                    <a id="closeModal" class="fancybox-close" data-dismiss="modal" aria-hidden="true"></a>
                    <div style="width: 100%; height: 100%; position: fixed; left: 0; top: 0; z-index: 1990; opacity: 0.3; background: #000; display: none" id="loading_bg"></div>
                    <%--<button id="closeModal" type="button" class="modal_close" data-dismiss="modal" aria-hidden="true">×</button>--%>
                    <iframe name="userDetailInfo" id="userDetailInfo" width="100%" height="730px">
                    </iframe>
                </div>
            </div>
        </div>

        <div class="box-footer clearfix text-center">
            <uc:paging runat="server" ID="paging" OnNavigate="paging_Navigate" EnableViewState="true" RowsPerPage="10" PagesPerGroup="10" />

        </div>

        <!--	
		<div class="box-footer clearfix text-center">
		</div>
		-->

    </div>
</asp:Content>
