﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class _mgt_user_popupUserDetail : FrontBasePage2
{
    private WWWService.Service _wwwService = new WWWService.Service();

    protected override void OnBeforePostBack()
    {
        var r = ConfigurationManager.AppSettings["domain_file"] + "/my/";
                
        //세션 만들기
        var i = Request["i"];
        AuthUser auth = new AuthUser();

        JsonWriter result = auth.Login(i, "ADMINCHECK");

        if (!result.success)
        {
            string data = result.data == null ? "null" : result.data.ToString();
            ErrorLog.Write(HttpContext.Current, 9987, "@MGT.auth.login.result " + result.message + " result.data :" + data);
            
            return;
        }

        //리다이렉트
        Response.Redirect(r);

    }
    
}