﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_user_corp_pending_default : AdminBasePage {

	int total = 0;
	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		
	}

	protected override void OnAfterPostBack(){
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page){
		Master.IsSearch = false;
		if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != ""){
			Master.IsSearch = true;
		}

        using (AuthDataContext dao = new AuthDataContext())
        {

            //var list = dao.sp_tSponsorMaster_corp_pending_list("Y", s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "CurrentUse", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { "Y", s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSPAuth("sp_tSponsorMaster_corp_pending_list", op1, op2).DataTableToList<sp_tSponsorMaster_corp_pending_listResult>().ToList();

            lbTotal.Text = list.Count.ToString();

            total = list.Count;
            repeater.DataSource = list;
            repeater.DataBind();

        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e){
		if (e.Item.ItemType != ListItemType.Footer){
			((Literal)e.Item.FindControl("lbIdx")).Text = (total - e.Item.ItemIndex).ToString();	
		}
	}


	protected void btn_accept_Click( object sender, EventArgs e ) {
		if(base.IsRefresh)
			return;

		var userId = (sender as LinkButton).CommandArgument;

        using (AuthDataContext dao = new AuthDataContext())
        {
            var a_id = AdminLoginSession.GetCookie(this.Context).identifier;

            //dao.sp_tSponsorMaster_corp_accept(userId);
            Object[] op1 = new Object[] { "UserID" };
            Object[] op2 = new Object[] { userId };
            www6.selectSPAuth("sp_tSponsorMaster_corp_accept", op1, op2);

            base.WriteLog(AdminLog.Type.update, string.Format("기업회원승인 : {0}", userId));

            //var sponsor_list = dao.tSponsorMaster.First(p => p.UserID == userId);
            //var sponsor_list = dao.sp_tSponsorMaster_get(userId).First();
            Object[] op3 = new Object[] { "UserID" };
            Object[] op4 = new Object[] { userId };
            var sponsor_list = www6.selectSPAuth("sp_tSponsorMaster_get", op3, op4).DataTableToList<sp_tSponsorMaster_getResult>().ToList().First();
            
            var email = sponsor_list.Email;
            var sponsor_name = sponsor_list.SponsorName;
            this.SendMail(userId, email, sponsor_name);
        }

		this.GetList(1);
	}

    void SendMail( string UserID, string email, string SponsorName ) {

        var args = new Dictionary<string, string> {
                    {"{userId}" , UserID},
                    {"{SponsorName}", SponsorName }
        };

        Email.Send(HttpContext.Current, Email.SystemSender, new List<string>() { email },
            "[한국컴패션] 회원승인이 완료 되었습니다..", " /mail/company-join.html",
            args, null);

     
    }
}
