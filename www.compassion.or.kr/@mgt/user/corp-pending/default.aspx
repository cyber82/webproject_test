﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_user_corp_pending_default" MasterPageFile="~/@mgt/mgt_afterlogin.master" %>

<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
		$(function () {
		});
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">


	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert">×</button>
		<h4><i class="icon fa fa-check"></i>Alert!</h4>
		웹회원가입정보만 조회가 가능합니다.
	</div>

	<div class="box box-default collapsed-box search_container">
        <div class="box-header with-border section-search">
			<div class="pull-left ">
                <button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;"><i class="fa fa-plus search_toggle"></i></button>
                <h3 class="box-title" style="padding-top: 5px">검색</h3>
			</div>
			
		</div>
        <div class="form-horizontal box-body">
			
			<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">기간 검색(등록일)</label>
					<div class="col-sm-10">
						<div class="inline">
							<asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							~
						<asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>

						<div class="inline">
							<button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							<div class="btn-group">
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								<a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								<a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							</div>
						</div>

					</div>

				</div>	
			
				
			<div class="form-group">
				<label for="s_keyword" class="col-sm-2 control-label">검색</label>
				<div class="col-sm-10">
                    <asp:TextBox runat="server" ID="s_keyword" Width="415" CssClass="form-control inline" OnTextChanged="search" AutoPostBack="true"></asp:TextBox>
						* 기업명 , 회원아이디
				</div>
			</div>
			
			<div class="box-footer clearfix text-center">
                <asp:LinkButton runat="server" ID="LinkButton1" class="btn btn-primary" Style="width: 100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
			</div>

		</div>
	</div>

	<br />


    <div class="box box-primary">
		<div class="box-header">
            <h3 class="box-title">총 <strong>
                <asp:Literal runat="server" ID="lbTotal"></asp:Literal></strong> 건</h3>
        </div>
        <!-- /.box-header -->
		<div class="box-body table-responsive no-padding">

			<table class="table table-hover table-bordered ">
				<colgroup>
					<col width="5%" />
					<col width="10%" />
					<col width="*" />
                    <col width="12%" />
					<col width="12%" />
                    <col width="12%" />
					<col width="12%" />
					<col width="15%" />
					<col width="7%" />
					<col width="7%" />
				</colgroup>
				<thead>
					<tr>
						<th>번호</th>
						<th>회원아이디</th>
						<th>기업명</th>
                        <th>사업자등록번호</th>
						<th>담당자</th>
						<th>휴대폰</th>
                        <th>전화번호</th>
                        <th>이메일</th>
						<th>가입일</th>
						<th>승인</th>
					</tr>
				</thead>
				<tbody>
				
                    <asp:Repeater runat="server" ID="repeater" OnItemDataBound="ListBound">
						<ItemTemplate>
							<tr class="tr_over">
                                <td>
                                    <asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
								<td><%#Eval("userid") %></td>
								<td><%#Eval("sponsorName") %></td>
                                <td><%#Eval("comregistration") %></td>
								<td><%#Eval("ManagerName") %></td>
								<td><%#Eval("mobile") == "" ? "-" : Eval("mobile") %></td>
                                <td><%#Eval("phone") == "" ? "-" : Eval("phone") %></td>
                                <td><%#Eval("email") %></td>
								<td><%#Eval("userdate" , "{0:yy'/'MM'/'dd}") %></td>
                                <td>
                                    <asp:LinkButton class="btn btn-sm btn-warning btn-flat" runat="server" ID="btn_accept" OnClick="btn_accept_Click" OnClientClick="return confirm('승인 하시겠습니까?')" CommandArgument='<%#Eval("userid") %>'>승인</asp:LinkButton>
                                </td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
                            <tr runat="server" visible="<%#repeater.Items.Count == 0 %>">
								<td colspan="7">데이터가 없습니다.</td>
							</tr>
						</FooterTemplate>
					</asp:Repeater>

				</tbody>
			</table>
		</div>

		<!--	
		<div class="box-footer clearfix text-center">
		</div>
		-->

	</div>

</asp:Content>
