﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_board_notice_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.BoardType = "notice";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");

		if (base.Action == "update") {

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));

                b_title.Text = entity.b_title;
                b_content.InnerHtml = entity.b_content;
                b_display.Checked = entity.b_display;
                b_main.Checked = entity.b_main;

                b_hot.Checked = entity.b_hot;
                b_regdate.Text = entity.b_regdate.ToString("yyyy.MM.dd");

            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			ph_regdate.Visible = true;
            base.FileLoad();

			
		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var thumb = base.Thumb;

		var entity = new board() {
			b_title = b_title.Text, b_content = b_content.InnerHtml.ToHtml(), 
			b_a_id = AdminLoginSession.GetCookie(this.Context).identifier,
			b_display = b_display.Checked,
			b_hot = b_hot.Checked,
			b_main = b_main.Checked,
			b_regdate = base.Action == "update" ? Convert.ToDateTime(b_regdate.Text) : DateTime.Now
        };

		if (base.Update(entity)) {
			if (base.Action == "update") {

				base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

				Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "수정되었습니다.";
			} else {

				base.WriteLog(AdminLog.Type.insert, string.Format("{0}", entity.ToJson()));

				Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "등록되었습니다.";
			}
		}

	}

	protected void btn_remove_click(object sender, EventArgs e) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
            //dao.board.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from board where b_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}
}