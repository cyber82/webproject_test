﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_board_press_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript">
		$(function () {
		    image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
			initEditor(oEditors, "p_content");
		});
		var onSubmit = function () {

			if (!validateForm([
				{ id: "#p_title", msg: "제목을 입력하세요" },
				{ id: "#p_press", msg: "언론사를 입력하세요" },
				//{ id: "#p_url", msg: "URL을 입력하세요" },
			    { id: "#p_regdate", msg: "등록일을 입력하세요" }
			])) {
				return false;
			}

			oEditors.getById["p_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.
			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<style type="text/css">
						#n_display label{margin-left:5px;width:50px}
					</style>
					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="p_display" cssClass="form-control1" Checked="true" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">제목</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=p_title CssClass="form-control p_title" Width=600 MaxLength="50"></asp:TextBox>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">언론사</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=p_press CssClass="form-control p_title" Width=200></asp:TextBox>
                        </div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">URL</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=p_url CssClass="form-control p_title" Width=600 placeholder="http://"></asp:TextBox>
                        </div>
					</div>

					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10">
							<textarea name="b_content" id="p_content" runat="server" style="width:600px; height:212px; display:none;"></textarea>
                        </div>
					</div>

                    <asp:PlaceHolder runat="server" ID="ph_regdate">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <input type="date" runat="server" ID="p_regdate"></input>
                            </div>
                        </div>
                    </asp:PlaceHolder>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>