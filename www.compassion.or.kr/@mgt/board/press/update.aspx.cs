﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_board_press_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context , "default.aspx");

		if (base.Action == "update") {

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.press.First(p => p.p_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<press>("p_id", Convert.ToInt32(PrimaryKey));
                
                p_title.Text = entity.p_title;
                p_display.Checked = entity.p_display;
                p_url.Text = entity.p_url;
                p_press.Text = entity.p_press;
                p_content.Value = entity.p_content;
                p_regdate.Value = entity.p_regdate.ToString("yyyy-MM-dd");

            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			ph_regdate.Visible = true;
			
		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var thumb = base.Thumb;

        var arg = new press() {
            p_title = p_title.Text,
            p_url = p_url.Text,
            p_press = p_press.Text,
            p_a_id = AdminLoginSession.GetCookie(this.Context).identifier,
            p_display = p_display.Checked,
            p_content = p_content.Value,
            p_hits = 0,
            p_regdate = Convert.ToDateTime(p_regdate.Value)
		};



        using (AdminDataContext dao = new AdminDataContext())
        {
            if (base.Action == "update")
            {

                //var entity = dao.press.First(p => p.p_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<press>("p_id", Convert.ToInt32(PrimaryKey));
                entity.p_a_id = arg.p_a_id;
                entity.p_display = arg.p_display;
                entity.p_title = arg.p_title;
                entity.p_url = arg.p_url;
                entity.p_press = arg.p_press;
                entity.p_content = arg.p_content;
                entity.p_regdate = arg.p_regdate;

                www6.update(entity);

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
                //dao.SubmitChanges();

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
            }
            else
            {
                //dao.press.InsertOnSubmit(arg);
                www6.insert(arg);

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";
                //dao.SubmitChanges();

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));
            }

        }

	}

	protected void btn_remove_click(object sender, EventArgs e) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.press.First(p => p.p_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<press>("p_id", Convert.ToInt32(PrimaryKey));
            //dao.press.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            www6.delete(entity);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", entity.ToJson()));
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}
}