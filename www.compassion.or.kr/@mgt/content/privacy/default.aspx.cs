﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
public partial class mgt_content_privacy_default : AdminBasePage {

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		Master.Content.Controls.BindAllTextBox(this.Context);

		this.PrimaryKey = "privacy";
		base.Action = Request["t"];

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.content.Where(p => p.c_id == this.PrimaryKey);
            var list = www6.selectQ<content>("c_id", this.PrimaryKey);

            if (list == null || list.Count() < 1)
            {
                //info.Visible = true;
            }
            else
            {
                var entity = list.First();
                c_text.InnerHtml = entity.c_text;
                base.Action = "update";
            }
        }


		base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
		btn_update.Visible = auth.aa_auth_update;
	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}


	protected void btn_update_click(object sender, EventArgs e) {
        using (AdminDataContext dao = new AdminDataContext())
        {
            if (base.Action == "update")
            {
                //var entity = dao.content.First(p => p.c_id == this.PrimaryKey);
                //entity.c_text = c_text.InnerHtml.ToHtml();
                //dao.SubmitChanges();
                string updateStr1 = string.Format("update content set c_text = '{0}' where  c_id = '{1}'", c_text.InnerHtml.ToHtml(), this.PrimaryKey);
                www6.cudAuth(updateStr1);

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", updateStr1));
            }
            else
            {
                //var entity = new content();
                //entity.c_a_id = AdminLoginSession.GetCookie(this.Context).identifier;
                //entity.c_id = this.PrimaryKey;
                //entity.c_text = c_text.InnerHtml.ToHtml();
                //entity.c_regdate = DateTime.Now;

                //dao.content.InsertOnSubmit(entity);
                //dao.SubmitChanges();
                string insertStr = string.Format(@"insert into content (  c_a_id   
                                                                        , c_id     
                                                                        , c_text   
                                                                        , c_regdate)
                                                                values (  {0} 
                                                                        , '{1}' 
                                                                        , '{2}' 
                                                                        , {3})"
                                                                        , AdminLoginSession.GetCookie(this.Context).identifier
                                                                        , this.PrimaryKey
                                                                        , c_text.InnerHtml.ToHtml()
                                                                        , DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                www6.cud(insertStr);

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", insertStr));
            }

            Master.ValueMessage.Value = "적용되었습니다.";
        }
	}

}
