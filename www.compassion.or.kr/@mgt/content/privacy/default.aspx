﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_content_privacy_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript">
	    $(function () {
            image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_popup)%>";
	        initEditor(oEditors, "c_text");
	    });

        var onSubmit = function () {
            oEditors.getById["c_text"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			if ($("#c_text").val() == "") {
				alert("내용을 입력하세요");
				return false;
			}

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "등록") + " 하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	

	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
		</ul>
		<div class="tab-content">
            
			<div class="active tab-pane" id="tab1" runat="server">
                <div class="form-horizontal">

					<div class="form-group">
						
						<div class="col-sm-10" style="width:100%; ">
							<textarea name="p_content" id="c_text" runat="server" style="width:100%; height:600px; display:none;"></textarea>
						</div>

					</div>
                </div>
            </div>
        </div>
    </div>

	<div class="box-footer clearfix text-center">
		<asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
	</div>

</asp:Content>