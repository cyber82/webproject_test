﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class mgt_participation_event_download : AdminBasePage {

	protected override void OnBeforePostBack() {

		var id = Convert.ToInt32(Request["c"]);
		var title = Request["title"];
		base.OnBeforePostBack();
		this.GetList(id, title);


		// 익스에서 한글, 띄어쓰기 깨짐
		HttpBrowserCapabilities brObject = Request.Browser;
		var browser = brObject.Type.ToLower();
		var fileName = "모바일출석체크_" + title.Replace(" ", "_").Replace(",", "") + "_" + id.ToString() + ".xls";
		if (browser.Contains("ie")) {
			fileName = System.Web.HttpUtility.UrlEncode(fileName);
		}


		Response.Clear();
		Response.Charset = "utf-8";
		Response.ContentType = "application/vnd.ms-excel";
		Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
	}


	protected void GetList(int id, string title) {

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_mobile_attendance_user_list(1, 999999, id.ToString(), "", "", "", "").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "mu_ma_id", "UserID", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { 1, 999999, id.ToString(), "", "", "", "" };
            var list = www6.selectSP("sp_mobile_attendance_user_list", op1, op2).DataTableToList<sp_mobile_attendance_user_listResult>().ToList();

            repeater.DataSource = list;
            repeater.DataBind();

            base.WriteLog(AdminLog.Type.select, string.Format("모바일출석체크_{0} 다운로드 ({1})", title, id));
        }
	}


}