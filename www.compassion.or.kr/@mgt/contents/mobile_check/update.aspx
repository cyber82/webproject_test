﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_stamp_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" %>

<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
    <script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>

    <script type="text/javascript">

        $(function () {

            // 에디터
            image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_mainpage)%>";
            initEditor(oEditors, "event_content");


            // 엑셀
            $(".btn_download").click(function () {

                var id = $(this).attr("data-id");
                var title = $(this).attr("data-title");

                location.href = "/@mgt/contents/mobile_check/download?c=" + id + "&title=" + escape(title);
                return false;
            })

        })

        var onSubmit = function () {

            oEditors.getById["event_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

            if (!validateForm([
				{ id: "#event_name", msg: "이벤트명을 입력하세요" },
				{ id: "#stamp_list", msg: "스템프 아이디를 입력하세요" },
                { id: "#begin_date", msg: "이벤트 시작일을 입력하세요" },
                { id: "#end_date", msg: "이벤트 종료일을 입력하세요" },
                { id: "#event_content", msg: "이벤트 내용을 입력하세요" },
				{ id: "#password", msg: "비밀번호를 입력하세요" }
            ])) {
                return false;
            }

            return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


        var onRemove = function () {
            return confirm("삭제하시겠습니까?");
        }



    </script>

</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    <input type="hidden" runat="server" id="delDate" />
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li runat="server" id="tabm1" class="active">
                <asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="1">기본정보</asp:LinkButton></li>
            <li runat="server" id="tabm2">
                <asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="2">참여자정보</asp:LinkButton></li>
        </ul>
        <div class="tab-content">

            <div class="active tab-pane" id="tab1" runat="server">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">노출 여부 </label>
                        <div class="col-sm-10" style="margin-top: 5px;">
                            <asp:RadioButtonList runat="server" ID="display" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Value="True" Text="노출&nbsp;&nbsp;&nbsp;" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="False" Text="비노출&nbsp;&nbsp;&nbsp;"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">이벤트명 </label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="event_name" class="form-control" Style="width: 300px;"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">스템프 아이디 </label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="stamp_list" CssClass="selectbox form-control" Style="width: 300px;"></asp:DropDownList>
                        </div>
                    </div>

                    

                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">이벤트 기간</label>
                        <div class="col-sm-10">
                            <button class="btn btn-primary btn-sm dayrange " data-from="begin_date" data-end="end_date"><i class="fa fa-calendar"></i></button>
                            <asp:TextBox runat="server" ID="begin_date" Width="100px" data-range="dayrange1" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                            ~
							<asp:TextBox runat="server" ID="end_date" Width="100px" data-range="dayrange1" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-2 control-label">이벤트 내용</label>
                        <div class="col-sm-10">
                            <textarea runat="server" name="event_content" id="event_content" class="form-control" style="width: 650px;"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">비밀번호</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="password" class="form-control number_only" Style="width: 300px;" MaxLength="4" placeholder="숫자 4자리 입력해 주세요."></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">랭딩 페이지<br />(선택사항)</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="ma_url" class="form-control" Style="width: 600px;" placeholder="http://"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group" style="padding-right: 20px;">
                        <asp:LinkButton runat="server" ID="btn_update_stamp" OnClick="btn_update_stamp_Click" OnClientClick="return onSubmit()" class="btn btn-danger pull-right" Style="margin-right: 5px">등록</asp:LinkButton>
                    </div>
                </div>

            </div>

            <!--탭-->
            <div class="tab-pane" id="tab2" runat="server">
                <div>
                    <div class="box-header">
                        <h4 class="box-title">총 <strong>
                            <asp:Literal runat="server" ID="lbTotal"></asp:Literal></strong> 건</h4>

                        <div class="pull-right">
                            <input type="text" runat="server" id="s_keyword" class="form-control inline" style="width: 300px; margin-right: 10px;" ontextchanged="search" autopostback="true" placeholder="아이디, 이름검색" />
                            <asp:LinkButton runat="server" class="btn btn-primary inline" Style="width: 70px; display: inline-block; vertical-align: top; margin-top: 2px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
                            <a href="#" class="btn btn-bitbucket pull-right btn_download" runat="server" id="btn_download" style="margin-left:5px;margin-top:2px;">다운로드</a>
                        </div>
                    </div>

                    <table class="table table-hover table-bordered">
                        <colgroup>

                            <col width="3%" />
                            <col width="7%" />
                            <col width="10%" />
                            <col width="7%" />
                            <col width="7%" />
                            <col width="10%" />
                            <col width="12%" />
                            <col width="*" />
                            <col width="5%" />
                        </colgroup>
                        <thead>
                            <tr>

                                <th>번호</th>
                                <th>아이디</th>
                                <th>후원유형</th>
                                <th>이름 </th>
                                <th>생년월일</th>
                                <th>휴대폰</th>
                                <th>이메일</th>
                                <th>주소</th>
                                <th>참여일</th>
                            </tr>
                        </thead>
                        <tbody>

                            <asp:Repeater runat="server" ID="repeater" OnItemDataBound="ListBound">
                                <ItemTemplate>
                                    <tr>
                                        <td><asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
                                        <td><%#Eval("UserID") %></td>
                                        <td><%#Eval("SponsorID").ToString() == "" ? "비후원" : "후원" %></td>
                                        <td><%#Eval("mu_name") %></td>
                                        <td><%#Eval("mu_birth") != null && Eval("mu_birth").ToString() != "" ? Eval("mu_birth").ToString().Decrypt().Substring(0, 10) : ""%></td>
                                        <td><%#Eval("mu_phone") != null ? Eval("mu_phone").ToString().Decrypt() : "" %></td>
                                        <td><%#Eval("mu_email") != null ? Eval("mu_email").ToString().Decrypt() : "" %></td>
                                        <td><%#Eval("mu_addr") != null ? Eval("mu_addr").ToString().Decrypt() : "" %></td>
                                        <td><%#Eval("mu_regdate", "{0:yyyy.MM.dd}") %></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr runat="server" visible="<%#repeater.Items.Count == 0 %>">
                                        <td colspan="8">데이터가 없습니다.</td>
                                    </tr>
                                </FooterTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>

                    <div class="box-footer text-center">
                        <uc:paging runat="server" ID="paging" OnNavigate="paging_Navigate" EnableViewState="true" RowsPerPage="10" PagesPerGroup="10" />

                    </div>

                </div>

            </div>

            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->


    <div class="box-footer clearfix text-center">
        <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
        <asp:LinkButton runat="server" ID="btn_remove_stamp" OnClick="btn_remove_stamp_Click" OnClientClick="return onRemove()" class="btn btn-danger pull-right">삭제</asp:LinkButton>
    </div>

</asp:Content>
