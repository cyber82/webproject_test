﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="download.aspx.cs" Inherits="mgt_participation_event_download"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head runat="server">
    <title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
		html{background:#fff}
		table { border-collapse:initial; }  
		th, td { border:1px solid #000000;}
		td { mso-number-format:\@; }
	</style>
</head>
<body>
    <form id="form" runat="server">
		<table class="listBoard">
			<tbody>	
				<tr>
					<td style="width:50px">번호</td>
					<td style="width:150px">아이디</td>
					<td style="width:150px">이름</td>
                    <td style="width:150px">생년월일</td>
					<td style="width:150px">연락처</td>
				    <th style="width:150px">이메일</th>
				    <th style="width:400px">주소</th>
                    <th style="width:150px">참여일</th>
				</tr>		
				<asp:Repeater runat=server ID=repeater >
					<ItemTemplate>
						<tr>
							<td><%#Container.ItemIndex + 1 %></td>
                            <td><%#Eval("UserID") %></td>
                            <td><%#Eval("mu_name") %></td>
                            <td><%#Eval("mu_birth") != null && Eval("mu_birth").ToString() != "" ? Eval("mu_birth").ToString().Decrypt().Substring(0, 10) : ""%></td>
                            <td><%#Eval("mu_phone") != null ? Eval("mu_phone").ToString().Decrypt() : "" %></td>
                            <td><%#Eval("mu_email") != null ? Eval("mu_email").ToString().Decrypt() : "" %></td>
                            <td><%#Eval("mu_addr") != null ? Eval("mu_addr").ToString().Decrypt() : "" %></td>
                            <td><%#Eval("mu_regdate", "{0:yyyy.MM.dd}") %></td>
						</tr>	
					</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								<td colspan="8">데이터가 없습니다.</td>
							</tr>
					</FooterTemplate>
				</asp:Repeater>	
				
			</tbody>
		</table>
    </form>
</body>
</html>
