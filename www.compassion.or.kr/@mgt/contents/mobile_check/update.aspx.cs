﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_stamp_update : AdminBasePage {

    protected override void OnBeforePostBack() {

        base.OnBeforePostBack();
        v_admin_auth auth = base.GetPageAuth();
        base.PrimaryKey = Request["c"];
        base.Action = Request["t"];

        btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var stampList = dao.stamp.Where(p => p.s_online == false).ToList();
            var stampList = www6.selectQ<stamp>("s_online", 0);

            foreach (var a in stampList)
            {
                var stampItem = new ListItem(a.s_name, a.s_id);
                stamp_list.Items.Add(stampItem);
            }
            stamp_list.Items.Insert(0, new ListItem("스탬프 아이디를 선택해 주세요", ""));


            if (base.Action == "update")
            {

                //var row = dao.mobile_attendance.First(p => p.ma_id == Convert.ToInt32(PrimaryKey));
                var row = www6.selectQF<mobile_attendance>("ma_id", Convert.ToInt32(PrimaryKey));

                event_name.Text = row.ma_title;
                event_content.InnerText = row.ma_content;
                password.Text = row.ma_password;
                stamp_list.SelectedValue = row.ma_s_id;

                begin_date.Text = row.ma_begin_date.ToString("yyyy.MM.dd");
                end_date.Text = row.ma_end_date.ToString("yyyy.MM.dd");

                if (DateTime.Compare(row.ma_end_date, DateTime.Now) == 1)
                {
                    display.SelectedValue = row.ma_display.ToString();

                }
                else
                {
                    display.SelectedValue = "False";
                }

                ma_url.Text = row.ma_url;

                tabm2.Visible = true;

                btn_download.Attributes["data-id"] = this.PrimaryKey.ToString();
                btn_download.Attributes["data-title"] = event_name.Text;
            }
            else
            {
                tabm2.Visible = false;

            }
        }

    }

    protected override void OnAfterPostBack() {
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();
    }

    // 유저 리스트
    protected override void GetList( int page ) {
        if(base.Action != "update") { return; }


        Master.IsSearch = false;
        if(s_keyword.Value != "")
        {
            Master.IsSearch = true;
        }
        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_mobile_attendance_user_list(page, paging.RowsPerPage, PrimaryKey, "", s_keyword.Value, "", "").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "mu_ma_id", "UserID", "keyword",  "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, PrimaryKey, "", s_keyword.Value, "", "" };
            var list = www6.selectSP("sp_mobile_attendance_user_list", op1, op2).DataTableToList<sp_mobile_attendance_user_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }
    }

    protected void ListBound( object sender, RepeaterItemEventArgs e ) {
        if(repeater != null) {
            if(e.Item.ItemType != ListItemType.Footer) {
                ((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
            }
        }
    }

    protected void btn_section_Click( object sender, EventArgs e ) {

        LinkButton obj = sender as LinkButton;
        var no = obj.CommandArgument;

        int tabCount = 2;

        for(int i = 1; i < tabCount + 1; i++) {

            if(no == i.ToString()) {
                ((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "active";
                ((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "active tab-pane";
            } else {
                ((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "";
                ((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "tab-pane";
            }


            if(no == "2") {
                this.GetList(1);
            }
        }

    }

    // 스탬프 추가 , 삭제
    protected void btn_update_stamp_Click( object sender, EventArgs e ) {

        AdminEntity adm = AdminLoginSession.GetCookie(this.Context);

        var arg = new mobile_attendance() {
            ma_display = Convert.ToBoolean(display.SelectedValue),
            ma_title = event_name.Text,
            ma_s_id = stamp_list.SelectedValue,
            ma_content = event_content.InnerText,
            ma_password = password.Text,
            ma_a_id = adm.identifier,
            ma_begin_date = DateTime.Parse(begin_date.Text),
            ma_end_date = DateTime.Parse(end_date.Text),
            ma_modate = DateTime.Now,
            ma_regdate = DateTime.Now,
			ma_url = ma_url.Text
        };

        using (AdminDataContext dao = new AdminDataContext())
        {

            if (base.Action == "update")
            {
                // 수정
                //var entity = dao.mobile_attendance.First(p => p.ma_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<mobile_attendance>("ma_id", Convert.ToInt32(PrimaryKey));
                entity.ma_display = arg.ma_display;
                entity.ma_title = arg.ma_title;
                entity.ma_s_id = arg.ma_s_id;
                entity.ma_content = arg.ma_content;
                entity.ma_password = arg.ma_password;
                entity.ma_a_id = arg.ma_a_id;
                entity.ma_begin_date = arg.ma_begin_date;
                entity.ma_end_date = arg.ma_end_date;
                entity.ma_modate = DateTime.Now;
                entity.ma_url = arg.ma_url;
                //dao.SubmitChanges();

                www6.update(entity);

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
            }
            else
            {
                var exist = www6.selectQ<mobile_attendance>("ma_id", arg.ma_id);
                if (exist.Any())
                {
                    Master.ValueMessage.Value = "이미 등록된 아이디 입니다.";
                    return;
                }

                // 등록
                //dao.mobile_attendance.InsertOnSubmit(arg);
                //dao.SubmitChanges();

                www6.insert(arg);
                //PrimaryKey = arg.idx.ToString();

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";

            }
        }


    }

    protected void btn_remove_stamp_Click( object sender, EventArgs e )
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            var exist = www6.selectQ<mobile_attendance_user>("mu_ma_id", Convert.ToInt32(PrimaryKey));

            //if (dao.mobile_attendance_user.Any(p => p.mu_ma_id == PrimaryKey))
            if (exist.Any())
            {
                Master.ValueMessage.Value = "스탬프를 획득한 유저가 있어 삭제가 불가능합니다.";
                return;
            }

            //var entity = dao.mobile_attendance.First(p => p.ma_id == Convert.ToInt32(PrimaryKey));
            //dao.mobile_attendance.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from mobile_attendance where ma_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "삭제되었습니다.";
        };


    }


}