﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="download.aspx.cs" Inherits="mgt_pray_download" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        html {
            background: #fff;
        }

        table {
            border-collapse: initial;
        }

        th, td {
            border: 1px solid #000000;
        }

        td {
            mso-number-format: \@;
        }
    </style>
</head>
<body>
    <form id="form" runat="server">
        <table class="listBoard">
            <tbody>
                <tr>
                    <td style="width: 150px">날짜</td>
                    <td style="width: 150px">내용</td>
                    <td style="width: 150px">편지여부</td>
                </tr>

                <tr>
                    <td>2016-08-25 (yyyy-mm-dd 형식)</td>
                    <td>내용</td>
                    <td>"O 또는 X"</td>
                </tr>

            </tbody>
        </table>
    </form>
</body>
</html>
