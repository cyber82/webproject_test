﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
using System.Configuration;
using System.Data.OleDb;
using System.Data;

public partial class mgt_pray_default : AdminBasePage {

    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();

        Master.Content.Controls.BindAllTextBox(this.Context);

        base.LoadComplete += new EventHandler(list_LoadComplete);

        v_admin_auth auth = base.GetPageAuth();
        btn_add.Visible = auth.aa_auth_create;

    }

    protected override void OnAfterPostBack() {

        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();
    }

    protected override void GetList( int page ) {

        add_idx.Text = "";
        add_date.Value = "";
        add_content.Value = "";
        add_check.Checked = false;

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_prayer_distribute_list(page, paging.RowsPerPage, s_date.Text, s_keyword.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "date", "keyword" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_date.Text, s_keyword.Text };
            var list = www6.selectSP("sp_prayer_distribute_list", op1, op2).DataTableToList<sp_prayer_distribute_listResult>();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }
    }

    protected void ListBound( object sender, RepeaterItemEventArgs e ) {
        if(repeater != null) {
            if(e.Item.ItemType != ListItemType.Footer) {

                sp_tDolVisit_listResult entity = e.Item.DataItem as sp_tDolVisit_listResult;
                ((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();

            }
        }
    }

    protected void btn_add_visit_Click( object sender, EventArgs e ) {

        if(base.IsRefresh) {
            return;
        }

        var p_date = "";
        if(add_date.Value != "") { p_date = add_date.Value; }

        var p_content = "";
        if(add_content.Value != "") { p_content = add_content.Value; }

        var p_check = add_check.Checked;

        using (AdminDataContext dao = new AdminDataContext())
        {
            // 중복 체크 
            var exist = www6.selectQ<prayer>("p_date", Convert.ToDateTime(p_date));
            if (exist.Any())
            {
                Master.ValueMessage.Value = "이미 등록된 날짜 입니다.";
                return;
            }

            var entity = new prayer() {
                                          p_date = Convert.ToDateTime(p_date)
                                        , p_content = p_content
                                        , p_check = p_check
                                        , p_regdate = DateTime.Now
                                        , p_a_id = AdminLoginSession.GetCookie(this.Context).identifier };
            //dao.prayer.InsertOnSubmit(entity);
            www6.insert(entity);

            //dao.SubmitChanges();
            

        }

        this.GetList(1);

    }

    protected void btn_control_modify_Click( object sender, EventArgs e ) {

        if(base.IsRefresh) {
            return;
        }

        var idx = Convert.ToInt32(add_idx.Text);

        var p_date = "";
        if(add_date.Value != "") { p_date = add_date.Value; }

        var p_content = "";
        if(add_content.Value != "") { p_content = add_content.Value; }

        var p_check = add_check.Checked;


        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.prayer.First(p => p.p_id == idx);
            var entity = www6.selectQF<prayer>("p_id", idx);
            
            // 중복 체크 
            if (entity.p_date != Convert.ToDateTime(p_date))
            {
                var exist = www6.selectQ<prayer>("p_date", Convert.ToDateTime(p_date));

                //if (dao.prayer.Any(p => p.p_date == Convert.ToDateTime(p_date)))
                if (exist.Any())
                {
                    Master.ValueMessage.Value = "이미 등록된 날짜 입니다.";
                    return;
                }
            }

            entity.p_date = Convert.ToDateTime(p_date);
            entity.p_content = p_content;
            entity.p_check = p_check;
            string updateStr1 = string.Format("update content set p_date = '{0}', p_content = '{1}', p_check = {2} where  p_id = {0}", Convert.ToDateTime(p_date).ToString("yyyy-MM-dd HH:mm:ss"), p_content, p_check, idx);
            www6.update(entity);
            //dao.SubmitChanges();


            base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
        }

        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "수정되었습니다.";


        this.GetList(paging.CurrentPage);

    }

    protected void btn_delete_Click( object sender, EventArgs e ) {

        if(base.IsRefresh) {
            return;
        }

        LinkButton btn = sender as LinkButton;
        var idx = Convert.ToInt32(btn.CommandName);

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.prayer.First(p => p.p_id == idx);
            //dao.prayer.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from prayer where p_id = {0}", idx);
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "삭제되었습니다.";


        this.GetList(1);
    }

    protected void btn_update_temp_file_click( object sender, EventArgs e ) {
        if(temp_file_type.Value == "i_image") {
            var limit_image_size = Convert.ToInt32(ConfigurationManager.AppSettings["limit_image_size"]);
            if(Convert.ToInt32(temp_file_size.Value) > limit_image_size) {
                Master.ValueMessage.Value = "허용 용량을 초과했습니다.(" + limit_image_size + "kb)";
                return;
            }

        } else {
            readExcel();
        }


    }

    protected void readExcel() {
        //엑셀읽기
        string connectionString = ConfigurationManager.AppSettings["excel_connection"];
        string fileName = temp_file_name.Value;
		//string rootDirectory = HttpContext.Current.Request.PhysicalApplicationPath;
		//string uploadFile = rootDirectory + fileName;
		string uploadFile = Server.MapPath(fileName);
        string excelConnection = string.Format(connectionString, uploadFile);
        string aaa = "";
        try {
            using(OleDbConnection conn = new OleDbConnection(excelConnection)) {

                conn.Open();
                DataTable dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if(dt.Rows.Count > 0) {
                    DataRow row = dt.Rows[0];
                    string sheetName = row["TABLE_NAME"].ToString();

                    try {

                        using (AdminDataContext dao = new AdminDataContext())
                        {

                            using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheetName + "]", conn))
                            {

                                OleDbDataAdapter adpt = new OleDbDataAdapter(cmd);
                                DataSet ds = new DataSet();
                                adpt.Fill(ds);


                                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                {
                                    DataRow dr1 = ds.Tables[0].Rows[i];

                                    //dao.prayer.DeleteAllOnSubmit(dao.prayer.Where(p => p.p_date == Convert.ToDateTime(dr1[0])));
                                    //string delStr = string.Format("delete from prayer where p_date = {0}", Convert.ToDateTime(dr1[0]).ToString("yyyy-MM-dd HH:mm:ss"));
                                    string delStr = string.Format("delete from prayer where p_date = '{0}'", Convert.ToDateTime(dr1[0]).ToString("yyyy-MM-dd HH:mm:ss"));
                                    www6.cud(delStr);

                                    var entity = new prayer()
                                    {
                                        p_date = Convert.ToDateTime(dr1[0]),
                                        p_content = dr1[1].ToString(),
                                        p_check = Convert.ToBoolean(dr1[2].ToString().ToLower() == "o" ? true : false),
                                        p_regdate = DateTime.Now,
                                        p_a_id = AdminLoginSession.GetCookie(this.Context).identifier

                                    };
                                    //dao.prayer.InsertOnSubmit(entity);

                                    string insertStr = string.Format(@"insert into prayer (  p_date   
                                                                                            , p_content
                                                                                            , p_check  
                                                                                            , p_regdate
                                                                                            , p_a_id)
                                                                                    values (  '{0}' 
                                                                                            , '{1}' 
                                                                                            , {2} 
                                                                                            , '{3}'
                                                                                            , {4})"
                                                                                            , Convert.ToDateTime(dr1[0]).ToString("yyyy-MM-dd HH:mm:ss")
                                                                                            , dr1[1].ToString()
                                                                                            , dr1[2].ToString().ToLower() == "o" ? "1" : "0"
                                                                                            , DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                                                                                            , AdminLoginSession.GetCookie(this.Context).identifier);
                                    aaa = insertStr;
                                    www6.cud(insertStr);
                                }

                                //dao.SubmitChanges();
                                GetList(1);
                                Master.ValueAction.Value = "uploaded";


                            }

                        }

                    } catch(Exception ex) {
                        Master.ValueMessage.Value = aaa + ex.Message;
                        //Response.Write(ex);

                    }
                }
            }
        } catch(Exception ee) {
            Master.ValueMessage.Value = ee.Message;

        }
    }
}
