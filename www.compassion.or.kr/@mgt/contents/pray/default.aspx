﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_pray_default" MasterPageFile="~/@mgt/mgt_afterlogin.master" %>

<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <script type="text/javascript">
        $(function () {
            $page.init();

            // 엑셀업로드
            var uploader_excel = attachUploader("btn_execel_upload");
            uploader_excel._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_board)%>";

            //엑셀다운로드
            $(".btn_execel_download").click(function () {
                //location.href = "/@mgt/contents/pray/download";
                location.href = "/@mgt/contents/pray/기도나눔_template.xls";
                return false;
            })
        });

        $page = {

            init: function () {
                $(".btn_modify").click(function () {
                    $page.changeBtn("show", $(this))
                })
                $("#btm_control_cancel").click(function () {
                    $page.changeBtn("hide", '')
                })

                $page.setSearchBox();

            },

            changeBtn: function (flag, obj) {
                if (obj != "") {
                    num = obj.data("id")
                    date = obj.parent().parent().find(".date").text();
                    content = obj.parent().parent().find(".content").text();
                    check = obj.parent().parent().find(".check").text() == "O" ? "check" : "uncheck";
                }

                if (flag == "show") {
                    $("#add_idx").val(num);
                    $("#add_date").val(date);
                    $("#add_content").val(content);
                    $("#add_check").iCheck(check);

                    $("#btn_add_visit").hide();
                    $(".control_btn").show();
                } else {
                    $("#add_idx").val("");
                    $("#add_date").val("");
                    $("#add_content").val("");
                    $("#add_check").iCheck("uncheck");

                    $(".control_btn").hide();
                    $("#btn_add_visit").show();
                }
            },

            validCheck: function () {
                if ($("#add_date").val() == "") {
                    alert("날짜를 입력해주세요")
                    return false;
                } else if ($("#add_content").val() == "") {
                    alert("내용을 입력해주세요")
                    return false;
                }
                return true;
            },

            setSearchBox: function () {

                if ($("#s_keyword").val() != "" || $("#s_date").val() != "") {
                    $("#searchBox").show();
                }
            },
            confirmDelete: function () {
                return confirm("삭제 하시겠습니까?");
            }

        };

        var attachUploader = function (button) {
            return new AjaxUpload(button, {

                action: '/common/handler/upload',
                responseType: 'json',
                onChange: function () {
                    // 이미지 업로드 되면 에디터 초기화 되는 현상 방지
                    // oEditors가 없는 경우 에러남
                    try {
                        if (oEditors) {
                            $.each(oEditors, function () {
                                this.exec("UPDATE_CONTENTS_FIELD", []);
                            });
                        }
                    } catch (e) { }
                },
                onSubmit: function (file, ext) {
                    this.disable();
                },
                onComplete: function (file, response) {

                    this.enable();
                    if (response.success) {
                        var c = $("#" + button).attr("class").replace(" ", "");
                        $(".temp_file_type").val(c.indexOf("image") > -1 ? "thumb" : "file");
                        $(".temp_file_name").val(response.name);
                        $(".temp_file_size").val(response.size);
                        eval($(".btn_update_temp_file").attr("href").replace("javascript:", ""));
                    } else
                        alert(response.msg);
                }
            });
        }

    </script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    <input type="hidden" runat="server" id="temp_file_type" class="temp_file_type" value="" />
    <input type="hidden" runat="server" id="temp_file_name" class="temp_file_name" value="" />
    <input type="hidden" runat="server" id="temp_file_size" class="temp_file_size" value="" />
    <asp:LinkButton runat="server" ID="btn_update_temp_file" CssClass="btn_update_temp_file" OnClick="btn_update_temp_file_click"></asp:LinkButton>

    <input type="hidden" runat="server" id="sortColumn" value="mp_order" />
    <input type="hidden" runat="server" id="sortDirection" value="asc" />
    <div class="box box-default collapsed-box search_container">
        <div class="box-header with-border section-search">
            <div class="pull-left ">
                <button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;"><i class="fa fa-plus search_toggle"></i></button>
                <h3 class="box-title" style="padding-top: 5px">검색</h3>
            </div>

        </div>

        <div class="form-horizontal box-body" id="searchBox">

            <div class="form-group">
                <label for="s_date" class="col-sm-2 control-label">날짜</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" ID="s_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <label for="s_keyword" class="col-sm-2 control-label">검색</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" ID="s_keyword" Width="415px" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                    * 내용
                </div>
            </div>

            <div class="box-footer clearfix text-center">
                <asp:LinkButton runat="server" ID="LinkButton1" class="btn btn-primary" Style="width: 100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
            </div>

        </div>
    </div>

    <br />

    <div class="box box-primary">


        <div class="box-header">
            <h3 class="box-title">총 <strong>
                <asp:Literal runat="server" ID="lbTotal"></asp:Literal></strong> 건</h3>
        </div>
        <!-- /.box-header -->


        <div class="box-body table-responsive no-padding">
            <div class="box-header with-border section-search">
                <div class="form-inline">
                    <asp:TextBox runat="server" ID="add_idx" Width="150px" CssClass="date form-control inline" Style="display: none;"></asp:TextBox>


                    <div class="input-group" style="width: 730px">
                        <input type="text" id="add_date" runat="server" placeholder="날짜 선택" class="form-control date" style="display: inline-block; width: 200px; margin-right: 10px">
                        <textarea type="text" id="add_content" runat="server" placeholder="내용" class="form-control" style="display: inline-block; width: 400px; height: 34px; max-width: 400px;"></textarea>

                        <span style="display: inline-block; margin-left: 10px; margin-top: 5px;">
                            <label for="add_check" class="control-label">편지여부 &nbsp;&nbsp;&nbsp;</label>
                            <asp:CheckBox runat="server" ID="add_check" />
                        </span>
                    </div>

                    <asp:LinkButton runat="server" ID="btn_add_visit" CssClass="btn btn-primary" OnClientClick="return $page.validCheck()" OnClick="btn_add_visit_Click">추가</asp:LinkButton>
                    <asp:LinkButton runat="server" ID="btn_control_modify" CssClass="btn btn-primary control_btn" Style="display: none;" OnClientClick="return $page.validCheck()" OnClick="btn_control_modify_Click">수정</asp:LinkButton>
                    <a id="btm_control_cancel" class="btn btn-primary control_btn" style="display: none;">취소</a>

                    <a href="#" class="btn btn-warning btn_execel_upload" runat="server" id="btn_execel_upload" style="margin-left: 5px">엑셀 업로드</a>
                    <a href="#" class="btn btn-bitbucket btn_execel_download" runat="server" id="btn_execel_download" style="margin-left: 5px">템플릿 다운로드</a>



                </div>
            </div>

            <table class="table table-hover table-bordered ">
                <colgroup>
                    <col width="5%" />
                    <col width="13%" />
                    <col width="*%" />
                    <col width="7%" />
                    <col width="20%" />
                </colgroup>
                <thead>
                    <tr>
                        <th>번호</th>
                        <th>날짜</th>
                        <th>내용</th>
                        <th>편지여부</th>
                        <th>버튼</th>
                    </tr>
                </thead>
                <tbody>

                    <asp:Repeater runat="server" ID="repeater" OnItemDataBound="ListBound">
                        <ItemTemplate>
                            <tr class="tr_link">
                                <td><asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
                                <td class="date"><%#Eval("p_date", "{0:yy/MM/dd}") %></td>
                                <td class="content text-left" style="white-space:pre-line;word-break:break-all;"><%#Eval("p_content") %></td>
                                <td class="check"><%#Eval("p_check").ToString() == "True" ? "O" : "X"%></td>
                                <td>
                                    <a id="btn_modify" class="btn_modify btn btn-primary btn-sm" data-id="<%#Eval("p_id").ToString()%>">수정하기</a>
                                    <asp:LinkButton runat="server" CssClass="btn btn-danger btn-sm" ID="btn_delete" CommandName='<%#Eval("p_id").ToString()%>' OnClientClick="return $page.confirmDelete()" OnClick="btn_delete_Click">삭제하기</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr runat="server" visible="<%#repeater.Items.Count == 0 %>">
                                <td colspan="5">데이터가 없습니다.</td>
                            </tr>
                        </FooterTemplate>
                    </asp:Repeater>

                </tbody>
            </table>
        </div>


        <div class="box-footer clearfix text-center">
            <uc:paging runat="server" ID="paging" OnNavigate="paging_Navigate" EnableViewState="true" RowsPerPage="10" PagesPerGroup="10" />
            <!--
				<asp:LinkButton runat="server" href="#" ID="btn_add" OnClientClick="goPage('update.aspx' , {t:'insert'})" CssClass="btn btn-bitbucket pull-right">신규등록</asp:LinkButton>
			-->
        </div>

        <!--	
		<div class="box-footer clearfix text-center">
		</div>
		-->

    </div>

</asp:Content>
