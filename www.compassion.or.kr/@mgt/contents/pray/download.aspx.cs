﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
using System.Configuration;
using System.Data.OleDb;
using System.Data;

public partial class mgt_pray_download : AdminBasePage {

    protected override void OnBeforePostBack() {

        base.OnBeforePostBack();
        //var currTime = DateTime.Now.ToString("yyyy_MM_dd");
        //this.GetList(currTime);

        // 익스에서 한글, 띄어쓰기 깨짐
        HttpBrowserCapabilities brObject = Request.Browser;
        var browser = brObject.Type.ToLower();
        var fileName = "기도나눔_template.xls";
        if(browser.Contains("ie")) {
            fileName = HttpUtility.UrlEncode(fileName);
        }


        Response.Clear();
        Response.Charset = "utf-8";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
    }

    /*
    protected void GetList(string currTime) {

        using(AdminDataContext dao = new AdminDataContext()) {

            var list = dao.sp_prayer_distribute_list(1, 10, "", "").ToList();
            repeater.DataSource = list;
            repeater.DataBind();

            base.WriteLog(AdminLog.Type.select, string.Format("이벤트_다운로드_{0}", currTime));
        }
    }

    */
}