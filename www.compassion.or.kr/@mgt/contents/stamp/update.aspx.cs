﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_stamp_update : AdminBasePage {

	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
        var primaryYear = Request["y"];
        base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();


		if (base.Action == "update"){
            using (AdminDataContext dao = new AdminDataContext())
            {
                //var row = dao.stamp.First(p => p.s_id == PrimaryKey && p.s_year == primaryYear);
                //string sqlStr = string.Format("select * from stamp where s_id = '{0}' and s_year = '{1}'", PrimaryKey, primaryYear);
                //var row = www6.selectText(sqlStr).Rows[0].DataTableToFirst<stamp>();
                var row = www6.selectQF<stamp>("s_id", PrimaryKey, "s_year", primaryYear);

                id.Text = row.s_id;
                name.Text = row.s_name;
                year.Text = row.s_year;
                order.Text = row.s_order.ToString();
                online.SelectedValue = row.s_online.ToString();
                display.SelectedValue = row.s_display.ToString();
            }

			id.ReadOnly = true;
			tabm2.Visible = true;
		} else{
			tabm2.Visible = false;

		}

	}

	protected override void OnAfterPostBack(){
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	// 유저 리스트
	protected override void GetList(int page) {
        var primaryYear = Request["y"];
        if (base.Action != "update"){ return; }


			Master.IsSearch = false;
		if (s_keyword.Value != "" ){
			Master.IsSearch = true;
		}
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.sp_stamp_user_list(page, paging.RowsPerPage, PrimaryKey, s_keyword.Value, "", "", primaryYear).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "su_s_id", "keyword", "startdate", "enddate", "su_s_year" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, PrimaryKey, s_keyword.Value, "", "", primaryYear };
            var list = www6.selectSP("sp_stamp_user_list", op1, op2).DataTableToList<sp_stamp_user_listResult>();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e){
		if (repeater != null){
			if (e.Item.ItemType != ListItemType.Footer)	{
				((Literal)e.Item.FindControl( "lbIdx" )).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
			}
		}
	}

	protected void btn_section_Click(object sender, EventArgs e){

		LinkButton obj = sender as LinkButton;
		var no = obj.CommandArgument;

		int tabCount = 2;

		for (int i = 1; i < tabCount + 1; i++){

			if (no == i.ToString()){
				((HtmlControl)this.FindAnyControl( "tabm" + i.ToString() )).Attributes["class"] = "active";
				((HtmlControl)this.FindAnyControl( "tab" + i.ToString() )).Attributes["class"] = "active tab-pane";
			}
			else {
				((HtmlControl)this.FindAnyControl( "tabm" + i.ToString() )).Attributes["class"] = "";
				((HtmlControl)this.FindAnyControl( "tab" + i.ToString() )).Attributes["class"] = "tab-pane";
			}


			if (no == "2"){
				this.GetList( 1);
			}
		}

	}

	// 스탬프 추가 , 삭제
	protected void btn_update_stamp_Click(object sender, EventArgs e){

		AdminEntity adm = AdminLoginSession.GetCookie( this.Context );

		var arg = new stamp(){
			s_display = Convert.ToBoolean( display.SelectedValue),
			s_online = Convert.ToBoolean( online.SelectedValue),
			s_id = id.Text,
			s_name = name.Text,
			s_order = Convert.ToInt32( order.Text ),
			s_year = year.Text,
			s_a_id = adm.identifier,
			s_regdate = DateTime.Now
		};

        using (AdminDataContext dao = new AdminDataContext())
        {
            if (base.Action == "update")
            {
                // 수정
                //var entity = dao.stamp.First(p => p.s_id == PrimaryKey);
                var entity = www6.selectQF<stamp>("s_id", PrimaryKey);

                entity.s_display = arg.s_display;
                entity.s_online = arg.s_online;
                entity.s_name = arg.s_name;
                entity.s_order = arg.s_order;
                entity.s_year = arg.s_year;
                entity.s_a_id = arg.s_a_id;
                entity.s_regdate = arg.s_regdate;
                //dao.SubmitChanges();
                www6.update(entity);
                
                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
            }
            else
            {
                var exist = www6.selectQ<stamp>("s_id", arg.s_id, "s_year", arg.s_year);
                //if (dao.stamp.Any(p => p.s_id == arg.s_id && p.s_year == arg.s_year))
                if (exist.Any())
                {
                    Master.ValueMessage.Value = "이미 등록된 아이디 입니다.";
                    return;
                }

                // 등록
                //dao.stamp.InsertOnSubmit(arg);
                //www6.cud(MakeSQL.insertQ2(arg));
                www6.insert(arg);
                //dao.SubmitChanges();
                
                PrimaryKey = arg.s_id.ToString();

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";

            }
        }


	}

	protected void btn_remove_stamp_Click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            var exist = www6.selectQ<stamp_user>("su_s_id", PrimaryKey);
            //if (dao.stamp_user.Any(p => p.su_s_id == PrimaryKey))
            if (exist.Any())
            {
                Master.ValueMessage.Value = "스탬프를 획득한 유저가 있어 삭제가 불가능합니다.";
                return;
            }

            //var entity = dao.stamp.First(p => p.s_id == PrimaryKey);
            //dao.stamp.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            //string delStr = MakeSQL.delQ(0, "stamp", "s_id", PrimaryKey);
            //www6.cud(delStr);
            var entity = www6.selectQF<stamp>("s_id", PrimaryKey);
            www6.delete(entity);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", entity.ToJson()));

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "삭제되었습니다.";
        };


	}

	// 유저 추가 삭제
	protected void btn_update_user_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}

		AdminEntity adm = AdminLoginSession.GetCookie( this.Context );

		var idList = user_id.Value.Split( ',' );



		List<stamp_user> argList = new List<stamp_user>();


		Master.ValueMessage.Value = "";
		for(var i = 0; i < idList.Length; i++){

			var arg = new stamp_user {
				su_s_id = PrimaryKey,
				UserID = "",
				UserName = "",
				su_a_id = adm.identifier,
				su_regdate = DateTime.Now
			};

            // 중복체크
            using (AdminDataContext dao = new AdminDataContext())
            {
                var exist = www6.selectQ<stamp_user>("UserID", idList[i], "su_s_id", arg.su_s_id);
                //if (dao.stamp_user.Any(p => p.UserID == idList[i] && p.su_s_id == arg.su_s_id))
                if (exist.Any())
                {
                    Master.ValueMessage.Value += "[ " + idList[i] + " ]는 이미 등록된 아이디 입니다.\n";
                    continue;
                }
            }

            // 아이디 존재유무 확인
            using (AuthDataContext dao = new AuthDataContext())
            {
                //var list = dao.sp_tSponsorMaster_get(idList[i]).ToList();
                Object[] op1 = new Object[] { "UserID" };
                Object[] op2 = new Object[] { idList[i] };
                var list = www6.selectSPAuth("sp_tSponsorMaster_get", op1, op2).DataTableToList<sp_tSponsorMaster_getResult>().ToList();
                
                if (list.Count == 0)
                {
                    Master.ValueMessage.Value += "[ " + idList[i] + " ]는 없는 아이디 입니다.\n";
                    continue;
                }
                else
                {
                    arg.UserID = idList[i];
                    arg.UserName = list[0].SponsorName;
                }
            }

			argList.Add( arg );
			Master.ValueMessage.Value += "[ " + idList[i] + " ]가 등록되었습니다.";
		}; // for 


        // 등록
        using (AdminDataContext dao = new AdminDataContext())
        {
            if (argList.Count() == 0)
            {
                return;
            }


            //dao.stamp_user.InsertOnSubmit( arg );
            //dao.stamp_user.InsertAllOnSubmit(argList);
            //dao.SubmitChanges();
            foreach (stamp_user su in argList)
            {
                string insertStr = string.Format(@"insert stamp_user (  su_s_id   
                                                                      , UserID    
                                                                      , UserName  
                                                                      , su_a_id   
                                                                      , su_regdate)
                                                              values (  '{0}' 
                                                                      , '{1}' 
                                                                      , '{2}' 
                                                                      , {3}
                                                                      , '{4}')"
                                                                      , su.su_s_id
                                                                      , su.UserID
                                                                      , su.UserName
                                                                      , su.su_a_id
                                                                      , su.su_regdate.ToString("yyyy-MM-dd HH:mm:ss"));
                www6.cud(insertStr);
            }
            base.WriteLog(AdminLog.Type.insert, string.Format("{0}", argList.ToJson()));

            GetList(1);
            Master.ValueMessage.Value += "";
        }

	}


	protected void btn_remove_user_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}

		var del_list = delDate.Value.Split( ',' );

        using (AdminDataContext dao = new AdminDataContext())
        {
            for (var i = 0; i < del_list.Length; i++)
            {
                var entity = www6.selectQF<stamp_user>("su_id", Convert.ToInt32(del_list[i]));
                www6.delete(entity);

                base.WriteLog(AdminLog.Type.delete, string.Format("{0}", entity.ToJson()));
            }
            //dao.stamp_user.DeleteAllOnSubmit( list );
            //dao.SubmitChanges();
            
            GetList(paging.CurrentPage);
            Master.ValueMessage.Value = "삭제되었습니다.";
        };


	}
}