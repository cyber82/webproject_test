﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_stamp_update" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	
	<script type="text/javascript">
	
		var onSubmit = function () {

			if (!validateForm([
				{ id: "#id", msg: "아이디를 입력하세요" },
				{ id: "#name", msg: "스탬프명을 입력하세요" },
				{ id: "#year", msg: "대상년도를 입력하세요" },
				
				{ id: "#order", msg: "순서를 입력하세요" }
			])) {
				return false;
			}

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}
		
		var onSubmit2 = function () {

			if (!validateForm([
				{ id: "#user_id", msg: "아이디를 입력하세요" }
			])) {
				return false;
			}

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}

		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

		var removeUser = function () {
			if (confirm("삭제하시겠습니까?")) {
				var str = "";
				var length = $(".su_id:checked").length
				for (var i = 0 ; i < length ; i++) {
					str += $(".su_id:checked").eq(i).val();
					if (i != length - 1) {
						str += ",";
					}
				}


				if (str == "") {
					alert("삭제할 항목을 선택해 주세요");
				} else {
					$("#delDate").val(str);
					return true;
				}
			}
			return false;
		};

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
		<input type="hidden" runat="server" id="delDate" />
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="1" >기본정보</asp:LinkButton></li>
			<li runat="server" id="tabm2"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="2" >획득유저정보</asp:LinkButton></li>
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server" >

				<div class="alert alert-danger alert-dismissable">
					<h4><i class="icon fa fa-check"></i>Alert!</h4>
					온라인스탬프의 경우 사전 정의된 아이디를 입력해주세요<br />
					<b>cdsp</b> : 1:1어린이 결연 , 
					<b>sf</b> : 특별한 나눔 후원 , 
					<b>ufj</b> : 나눔펀딩 후원 , 
					<b>ufc</b> : 나눔펀딩 개설 , 
					<b>store</b> : 스토어 주문 , 
					<b>mail</b> : 편지쓰기 , 
					<b>cad </b>: CAD결연
				</div>


				<div class="form-horizontal">
				
					
					<div class="form-group">
						<label class="col-sm-2 control-label">온라인 여부 </label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:RadioButtonList runat="server" ID="online" RepeatDirection="Horizontal" RepeatLayout="Flow" >
								<asp:ListItem Value="True" Text="온라인&nbsp;&nbsp;&nbsp;" Selected="True"></asp:ListItem>
								<asp:ListItem Value="False" Text="오프라인&nbsp;&nbsp;&nbsp;"></asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">노출 여부 </label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:RadioButtonList runat="server" ID="display" RepeatDirection="Horizontal" RepeatLayout="Flow" >
								<asp:ListItem Value="True" Text="노출&nbsp;&nbsp;&nbsp;" Selected="True"></asp:ListItem>
								<asp:ListItem Value="False" Text="비노출&nbsp;&nbsp;&nbsp;"></asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">아이디 </label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=id class="form-control" style="width:300px;" ></asp:TextBox>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">대상년도</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=year class="form-control number_only" style="width:300px;" MaxLength="4" placeholder="yyyy"></asp:TextBox>
							
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">스탬프명</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=name class="form-control" style="width:300px;" ></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">노출순서</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=order class="form-control number_only" style="width:300px;" MaxLength="3"></asp:TextBox>
						</div>
					</div>

					<div class="form-group" style="padding-right:20px;">
						<asp:LinkButton runat="server" ID="btn_update_stamp" OnClick="btn_update_stamp_Click" OnClientClick="return onSubmit()" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
					</div>
				</div>

			</div>

			<!--탭:획득유저(페이징,아이디검색,등록O,삭제O,멀티삭제O,수정X) -->
			<div class="tab-pane" id="tab2" runat="server" >

				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert"></button>
					<h4><i class="icon fa fa-check"></i>Alert!</h4>
					사용자 등록시 등록버튼을 클릭해야 최종 반영됩니다. 
					<br /> , 를 사용하면 한번에 여러개 등록이 가능합니다. ex) apple , banana
					<br /> 관리자가 등록한 스탬프만 삭제가 가능합니다. (사용자 획득 스탬프는 checkbox 노출안됨)
				</div>
				<div class="box box-default collapsed-box search_container">
					<div class="box-header with-border section-search">
						<div class="form-inline">

							<div class="input-group" style="width: 410px">
								<input type="text" runat=server ID=user_id class="form-control" style="display:inline-block;width:400px;margin-right:10px" placeholder="사용자 아이디" />
							</div>
							<asp:LinkButton runat="server" ID="LinkButton1" OnClick="btn_update_user_Click" OnClientClick="return onSubmit2()" class="btn btn-danger" style="margin-right:5px">등록</asp:LinkButton>
						</div>
					</div>
				</div>

		
				<div>

					
						<div class="box-header">
							<h4 class="box-title">총 <strong><asp:Literal runat="server" ID="lbTotal"></asp:Literal></strong> 건</h4>
						
							<div class="pull-right">
								<input type="text" runat="server" id="s_keyword" class="form-control inline" style="width:300px; margin-right:10px;" OnTextChanged="search" AutoPostBack="true" placeholder="아이디, 이름검색" />
								<asp:LinkButton runat="server"  class="btn btn-primary inline" style="width:70px;display:inline-block;vertical-align:top;margin-top:2px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
							</div>
						</div>
					
					<table class="table table-hover table-bordered">
						<colgroup>
							<col width="5%" />
							<col width="10%" />
							<col width="25%" />
							<col width="25%" />
							<col width="10%" />
						</colgroup>
						<thead>
							<tr>
								<th style="text-align:center"><asp:LinkButton runat="server" ID="btn_remove_user" OnClick="btn_remove_user_Click"  OnClientClick="return removeUser()" class="btn btn-danger" Style="height:23px;padding-top:2px;margin:0">선택삭제</asp:LinkButton></th>
								<th>번호</th>
								<th>아이디</th>
								<th>이름 </th>
								<th>등록일</th>
							</tr>
						</thead>
						<tbody>

							<asp:Repeater runat="server" ID="repeater" OnItemDataBound=ListBound>
								<ItemTemplate>
									<tr>
										<td><div style="<%#Convert.ToInt32(Eval("su_a_id")) > 0 ? "" : "display:none;"%>"><input type="checkbox" class="su_id" value="<%#Eval("su_id") %>"/></div></td>
										<td><asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
										<td><%#Eval("UserID") %></td>
										<td><%#Eval("UserName") %></td>
										<td><%#Eval("su_regdate", "{0:yy.MM.dd}") %></td>			
									</tr>
								</ItemTemplate>
								<FooterTemplate>
									<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
										<td colspan="5">데이터가 없습니다.</td>
									</tr>
								</FooterTemplate>
							</asp:Repeater>
						</tbody>

					</table>
					
					<div class="box-footer text-center">
						<uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />	
						
					</div>

				</div>

			</div>

			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->


	 <div class="box-footer clearfix text-center">
		 <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
		<asp:LinkButton runat="server" ID="btn_remove_stamp" OnClick="btn_remove_stamp_Click" class="btn btn-danger pull-right">삭제</asp:LinkButton>
	</div>

</asp:Content>