﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class admin_login : AdminBasePage{

	protected override bool CheckAuthentication {
		get { return false; }
	}

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		if (AdminLoginSession.HasCookie(this.Context)) {
			Response.Redirect("/@mgt/dashboard/");
		}

		if (string.IsNullOrEmpty(Request.QueryString["r"])){
			if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["admin_auth_key"])){
				if (string.IsNullOrEmpty(Request.QueryString["auth"])
					|| Request.QueryString["auth"] != ConfigurationManager.AppSettings["admin_auth_key"]) {
						Response.Redirect("/");
				}
			}
		}
        //Response.Write("1111".sha256Hash());
	}

	protected void btn_login_Click(object sender, EventArgs e) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            try
            {
                //var entity = dao.admin.First(p => p.a_email == login_id.Value && p.a_pwd == login_pw.Value.SHA256Hash() && !p.a_deleted);
                var entity = www6.selectQF<admin>("a_email", login_id.Value, "a_pwd", login_pw.Value.SHA256Hash(), "a_deleted", 0);

                AdminLoginSession.SetCookie(this.Context, new AdminEntity()
                {
                      identifier = entity.a_id
                    , name = entity.a_name
                    , dept = entity.a_part
                    , email = entity.a_email
                    , type = entity.a_type
                    , rawData = entity
                });

                if (!string.IsNullOrEmpty(Request["r"]))
                    Response.Redirect(Request["r"]);
                else
                    Response.Redirect("/@mgt/dashboard/");

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                message.Value = "아이디가 없거나 비밀번호가 일치하지 않습니다.";
            }
        }

	}
}