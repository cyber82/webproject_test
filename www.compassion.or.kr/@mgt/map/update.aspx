﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_map_update" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<link href="update.css" rel="stylesheet" />
	<script>

		

		var onSubmit = function () {

			if (!validateForm([
				{ id: "#lat", msg: "위도를 입력하세요" },
				{ id: "#lng", msg: "경도를 입력하세요" },
				{ id: "#country_code", msg: "국가 코드를 입력하세요" },
				{ id: "#country_name", msg: "국가명을 입력하세요" }
			])) {
				return false;
			}

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");

		}

		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>


	 

	<script type="text/javascript" src="update.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?language=ko&region=kr&key=<%:this.ViewState["googleMapApiKey"].ToString() %>&libraries=places&callback=initAutocomplete" async defer></script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	
	<input type="hidden" id="googleMapApiKey" runat="server" />

	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1" data-toggle="tab">기본정보</a></li>
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1">
				<div class="form-horizontal">
				
					<!--지도 부분 -->
					<div style="height:500px; padding:30px;">
						<input id="pac-input" class="controls" type="text" placeholder="Search Box">
						<div id="map"></div>
					</div>


					<!-- 입력 부분 -->
					<div class="form-group" style="display:none">
						<label class="col-sm-2 control-label">참고용 주소</label>
						<div class="col-sm-10">
							<input type="text" runat=server id="addr" class="form-control" style="width:600px;" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">위도</label>
						<div class="col-sm-10">
							<input type="text" runat=server id="lat" class="form-control" style="width:300px;" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">경도</label>
						<div class="col-sm-10">
							<input type="text" runat=server id="lng" class="form-control" style="width:300px;" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">국가코드</label>
						<div class="col-sm-10">
							<input type="text" runat=server id="country_code" class="form-control" style="width:600px;" placeholder="직접입력" maxlength="2" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">국가명</label>
						<div class="col-sm-10">
							<input type="text" runat=server id="country_name" class="form-control" style="width:600px;" placeholder="직접입력" />
							* 컴파스에 등록된 국가명으로 입력해야 날씨,시간정보조회가 가능합니다.
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">국가명(영문)</label>
						<div class="col-sm-10">
							<input type="text" runat=server id="c_name_en" class="form-control" style="width:600px;" placeholder="직접입력" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">대륙</label>
						<div class="col-sm-10">
							<asp:DropDownList runat="server" ID="c_region" CssClass="form-control" Width="600px">
								<asp:ListItem Value="asia">아시아</asp:ListItem>
								<asp:ListItem Value="america">중남미</asp:ListItem>
								<asp:ListItem Value="africa">아프리카</asp:ListItem>
							</asp:DropDownList>
						</div>
					</div>

				</div>
			</div>
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->


	 <div class="box-footer clearfix text-center">
		<asp:LinkButton runat=server ID=btn_remove  OnClientClick="return onRemove();" OnClick="btn_remove_Click" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update  OnClientClick="return onSubmit();" OnClick="btn_update_Click"  class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>

</asp:Content>