﻿
var map;
var markers = [];
var geocoder;
var infowindow;
var key = "";
$(function () {
	key = $("#googleMapApiKey").val();
})

function initAutocomplete() {

	lat = $("#lat").val() == "" ? 37.5665893418942360 : parseFloat($("#lat").val());
	lng = $("#lng").val() == "" ? 126.97792053222656 : parseFloat($("#lng").val());


	map = new google.maps.Map(document.getElementById('map'), {
		center: { lat: lat, lng: lng },
		zoom: 5,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	if ($("#lat").val() != ""){ 
		var marker = new google.maps.Marker({
			position: { lat  , lng},
			map: map,
		});
		markers.push(marker);
	}


	geocoder = new google.maps.Geocoder;
	infowindow = new google.maps.InfoWindow;

	// Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
	});

	// Listen for the event fired when the user selects a prediction and retrieve
	// more details for that place.
	searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}

		// Clear out the old markers.
		markers.forEach(function(marker) {
			marker.setMap(null);
		});
		markers = [];

		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
			var icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
		};

			// Create a marker for each place.
			markers.push(new google.maps.Marker({
				map: map,
				icon: icon,
				title: place.name,
				position: place.geometry.location
			}));

			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
			setData(place.geometry.location)
		});
		deleteMarkers()
		map.fitBounds(bounds);
	});

	map.addListener('click', function (e) {
		addMarker(e.latLng);
		setData(e. latLng);
	});

};

function addMarker(location) {
	var marker = new google.maps.Marker({
		position: location,
		map: map
	});
	deleteMarkers()
	markers.push(marker);
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
	setMapOnAll(null);
}

function deleteMarkers() {
	clearMarkers();
	markers = [];
}

function setData(latLng) {
	var lat = latLng.lat()
	var lng = latLng.lng()

	//	console.log("lat :" + lat, "lng :" + lng);
	$("#lat").val(lat);
	$("#lng").val(lng);

	//formatted_address
	$.get("https://maps.googleapis.com/maps/api/geocode/json", { latlng: lat + "," + lng, key: key }).success(function (r) {
		if (r.status == "OK") {
			$("#addr").val(r.results[0].formatted_address);
		}
	});

	return false;

}


