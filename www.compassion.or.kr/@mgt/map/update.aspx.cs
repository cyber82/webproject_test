﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Configuration;

public partial class mgt_map_update : AdminBasePage {

	
	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();

		googleMapApiKey.Value = ConfigurationManager.AppSettings["googleMapApiKey"];
		this.ViewState["googleMapApiKey"] = googleMapApiKey.Value;
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		if (base.Action == "update")
        {
            using (AdminDataContext dao = new AdminDataContext())
            {
                //var row = dao.country.First(p => p.c_id == PrimaryKey);
                var row = www6.selectQF<country>("c_id", PrimaryKey);

                country_code.Value = row.c_id;
                country_name.Value = row.c_name;
                c_name_en.Value = row.c_name_en;
                c_region.SelectedValue = row.c_region;
                lat.Value = row.c_lat.ToString();
                lng.Value = row.c_lng.ToString();
            }
		};
		
	}

	protected override void OnAfterPostBack(){
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e){

	}


	protected void btn_update_Click(object sender, EventArgs e){

		var arg = new country() {
			c_id = country_code.Value,
			c_name = country_name.Value,
			c_lat = Convert.ToDouble( lat.Value),
			c_lng = Convert.ToDouble( lng.Value),
			c_name_en = c_name_en.Value , 
			c_regdate = DateTime.Now,
			c_region = c_region.SelectedValue
		};


        using (AdminDataContext dao = new AdminDataContext())
        {
            if (base.IsRefresh)
            {
                return;
            }

            if (PrimaryKey != arg.c_id)
            {
                var exist = www6.selectQ<country>("c_id", arg.c_id);
                //if (dao.country.Any(p => p.c_id == arg.c_id))
                if (exist.Any())
                {
                    Master.ValueAction.Value = "";
                    Master.ValueMessage.Value = "이미 등록된 국가 코드 입니다.";
                    return;
                }
            }


            if (base.Action == "update")
            {
                //var entity = dao.country.First(p => p.c_id == PrimaryKey);
                var entity = www6.selectQF<country>("c_id", PrimaryKey);
                entity.c_id = arg.c_id;
                entity.c_name = arg.c_name;
                entity.c_lat = arg.c_lat;
                entity.c_lng = arg.c_lng;
                entity.c_name_en = arg.c_name_en;
                entity.c_region = arg.c_region;
                //dao.SubmitChanges();
                www6.update(entity);

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";

            }
            else
            {

                //dao.country.InsertOnSubmit(arg);
                //www6.cud(MakeSQL.insertQ2(arg));
                www6.insert(arg);
                //dao.SubmitChanges();

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";
            }
        }



	}

	protected void btn_remove_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var entity = dao.country.First(p => p.c_id == PrimaryKey);
            //dao.country.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from country where c_id = '{0}'", PrimaryKey);
            www6.cud(delStr);

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "삭제되었습니다.";
        }
	}



}