﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class mgt_admin_admin_default : AdminBasePage{

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		Master.Content.Controls.BindAllTextBox(this.Context);

		CommonLib.v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;
		
		base.LoadComplete += new EventHandler(list_LoadComplete);
	}

	protected override void GetList(int page) {

		var a = AdminLoginSession.GetCookie(this.Context).rawData;

        using (AdminDataContext dao = new AdminDataContext())
        {

            Master.IsSearch = false;

            string where = "";
            where += " and a_deleted = 0";

            if (s_keyword.Text.Trim() != "")
            {
                Master.IsSearch = true;
                where += string.Format(" and (a_name like '%{0}%' or a_position like '%{0}%' or a_part like '%{0}%'  or a_email like '%{0}%' )", s_keyword.Text.Trim());
            }

            if (a.a_type == "recruit")
            {
                where += string.Format(" and a_type = '{0}'", a.a_type);
            }

            //var aaa = www6.selectQ2<v_admin>("v_ admin", "a_deleted = ", 0, "(", new Ept(), "- a_name like", "abcd".likeAll(), " +or a_position like", "aaaaa".like1(), " +or a_part like ", "xxxxxx".like2(), " +or a_email like ", "werwerwer".likeAll(), "- )", new Ept()
            //                                 , "a_type = ", a.a_type, "regDate BETWEEN", utils.btween(DateTime.Now.AddDays(-1), DateTime.Now)
            //                                 , "+or (", new Ept(), "- userID IN ", utils.include(0, 1, 2, 3, 4), "admin IN ", utils.include("aa", "bb", "cc", "dd", "ee")
            //                                 , "- )", new Ept(), "regdate > ", "GETDATE()", "+or FixedYN ", "IS NOT NULL");

            string query = string.Format("select * from v_admin where 1=1 {0} order by a_name asc ", where);
            //var list = dao.ExecuteQuery<v_admin>(query);
            var list = www6.selectText(query).DataTableToList<v_admin>();

            //int totalCount = dao.ExecuteQuery<v_admin>(query).Count();
            int totalCount = list.Count;

            lbTotal.Text = totalCount.ToString();

            paging.CurrentPage = page;
            paging.Calculate(totalCount);
            repeater.DataSource = list.Skip((page - 1) * paging.RowsPerPage).Take(paging.RowsPerPage);
            repeater.DataBind();

        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (e.Item.ItemType == ListItemType.Footer)
			return;

		v_admin entity = e.Item.DataItem as v_admin;
		((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();


	}
	
}