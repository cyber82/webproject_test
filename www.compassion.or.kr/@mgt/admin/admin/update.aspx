﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_admin_admin_update" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>

<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
		
		var onSubmit = function () {

		    if (!validateForm([
                { id: "#a_type", msg: "구분을 선택하세요" },
				{ id: "#a_name", msg: "이름을 입력하세요" },
				{ id: "#a_email", msg: "아이디을 입력하세요" },
				{ id: "#a_part", msg: "부서를 입력하세요" }
				
			])) {
				return false;
			}

			if ($("#a_branch_code").length > 0 && $("#a_branch_code").val() == "") {
				$("#a_branch_code").focus();
				alert("지사코드를 입력하세요");
				return false;
			}

			if ($("#a_agent_code").length > 0 && $("#a_agent_code").val() == "") {
				$("#a_agent_code").focus();
				alert("대리점코드를 입력하세요");
				return false;
			}

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}
		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

		var onResetPassword = function () {
			return confirm("임시 비밀번호를 발송하시겠습니까?");
		}

		$(function () {
			if ("<%:base.Action%>" == "update") {
				$("#a_email").attr("readonly", "readonly");
			}

			$(".check_all").on("ifChanged" , function (sender) {
			    var val = $(this).attr("data-val");
				var checked = $(this).prop("checked");
		
				$.each($("." + val), function () {
					$($(this).find("input")).iCheck(checked ? "check" : "uncheck");
				});

			})

		})

	</script>

</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="1" >기본정보</asp:LinkButton></li>
			<li runat="server" id="tabm2" visible="false"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="2" >메뉴권한</asp:LinkButton></li>
			<li runat="server" id="tabm3" visible="false"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="3" >직무권한</asp:LinkButton></li>
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">


					<div class="form-group">
						<label class="col-sm-2 control-label ">구분</label>
						<div class="col-sm-2" style="width:600px">
							<div style="width:150px;display:inline-block">
								<asp:DropDownList runat="server" ID="a_type" EnableViewState="true" AutoPostBack="true" OnSelectedIndexChanged="a_type_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
							</div>	
							<div style="display:inline-block">
								<strong class="guide">* 슈퍼관리자는 메뉴권한에 관련없이 모든 메뉴의 접근이 가능합니다.</strong> 
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label ">비밀번호</label>
						<div class="col-sm-2" style="width:600px">
							<div style="width:150px;display:inline-block">
								<asp:TextBox runat="server" ID="a_pwd" TextMode="Password" CssClass="form-control"></asp:TextBox>
							</div>	
							<div style="display:inline-block">
								<strong class="guide">* 변경시만 입력</strong> 
							</div>
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-sm-2 control-label">아이디</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="a_email" runat=server style="width:300px" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">이름</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="a_name" runat=server style="width:300px" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">부서</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="a_part" runat=server style="width:300px" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">직위</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="a_position" runat=server  style="width:300px"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">휴대폰</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="a_phone_mobile" runat=server style="width:300px"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">회사전화</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="a_phone_office" runat=server style="width:300px"/>
						</div>
					</div>
					
				</div>
			</div>

			<div class="tab-pane" id="tab2" runat="server" >
				
				<div class="box-body table-responsive no-padding icheckbox">
		
					<table class="table table-hover table-bordered table-condensed ">
						<colgroup>
							<col width="200px" />
						</colgroup>
						<thead>
							<tr>
								<th>메뉴명</th>
								<th><input type=checkbox class="check_all" data-val="create"> 등록</th>
								<th><input type=checkbox class="check_all"  data-val="update"> 수정</th>
								<th><input type=checkbox class="check_all" data-val="delete"> 삭제</th>
								<th><input type=checkbox class="check_all" data-val="read"> 조회</th>
							</tr>
						</thead>
						<tbody>
								 
							<asp:Repeater runat=server ID=repeater_menu OnItemDataBound="repeater_menu_ItemDataBound">
								<ItemTemplate>
									<tr class="<%#((int)Eval("am_depth") == 0 || string.IsNullOrEmpty(Eval("am_path").ToString())) ?"":"tr_link"%>" style="cursor:default">
										<td class="text-left" <%#((int)Eval("am_depth") == 0 ) ?"colspan=5":""%>>
											<%#((int)Eval("am_depth") == 0 || string.IsNullOrEmpty(Eval("am_path").ToString())) ?"":"<input type=checkbox class='check_all' data-val='"+Eval("am_code")+"'/>"%>
											<span style="font-weight:<%#(int)Eval("am_depth") == 0?"bold":"normal"%>"><%#Eval("am_name")%></span>
										</td>
										<td style="display:<%#((int)Eval("am_depth") == 0 || string.IsNullOrEmpty(Eval("am_path").ToString())) ?"none":""%>"><asp:CheckBox runat="server" ID="cb_auth_create" CssClass=<%#Eval("am_code") + " create" %> /></td>
										<td style="display:<%#((int)Eval("am_depth") == 0 || string.IsNullOrEmpty(Eval("am_path").ToString())) ?"none":""%>"><asp:CheckBox runat="server" ID="cb_auth_update" CssClass=<%#Eval("am_code") + " update" %> /></td>
										<td style="display:<%#((int)Eval("am_depth") == 0 || string.IsNullOrEmpty(Eval("am_path").ToString())) ?"none":""%>"><asp:CheckBox runat="server" ID="cb_auth_delete" CssClass=<%#Eval("am_code") + " delete" %> /></td>
										<td style="display:<%#((int)Eval("am_depth") == 0 || string.IsNullOrEmpty(Eval("am_path").ToString())) ?"none":""%>"><asp:CheckBox runat="server" ID="cb_auth_read" CssClass=<%#Eval("am_code") + " read" %> /></td>
							
									</tr>	
								</ItemTemplate>
							</asp:Repeater>	
				
						</tbody>
					</table>
				</div>



			</div>

            
			<div class="tab-pane" id="tab3" runat="server" >
				
				<div class="box-body table-responsive no-padding icheckbox">
		
					<table class="table table-hover table-bordered table-condensed ">
						<colgroup>
							<col width="200px" />
						</colgroup>
						<thead>
							<tr>
					            <th>1단계</th>
					            <th><input type=checkbox class="check_all" data-val="recruit">2단계</th>
							</tr>
						</thead>
						<tbody>
								 
							<asp:Repeater runat=server ID=repeater_recruit OnItemDataBound="repeater_recruit_ItemDataBound">
					            <ItemTemplate>
						            <tr class="<%#Eval("rj_depth2").ToString() == "" ?"":"tr_link"%>" style="cursor:default">
							            <td class="text-left">
								            <%#Eval("rj_depth2").ToString() == "" ? "<input type='checkbox' class='check_all' data-val='"+Eval("rj_depth1")+"'>" : ""%>
								            <asp:Literal runat="server" ID="rj_depth1"/>
							            </td>

							            <td class="text-left">
								            <asp:Repeater runat=server ID=repeater_recruit_depth2 OnItemDataBound="repeater_recruit_depth2_ItemDataBound">
									            <ItemTemplate>
										            <asp:CheckBox runat="server" ID="rj_id" ToolTip=<%#Eval("rj_id") %> CssClass=<%#Eval("rj_depth1") + " recruit" %> /> <%#Eval("rj_name") %>
									            </ItemTemplate>
								            </asp:Repeater>			
							            </td>
						            </tr>	
					            </ItemTemplate>
				            </asp:Repeater>	
				
						</tbody>
					</table>
				</div>



			</div>

			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">

		<asp:LinkButton runat="server" ID="btn_remove" OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right">삭제</asp:LinkButton>
		 <asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>

		 <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>


</asp:Content>