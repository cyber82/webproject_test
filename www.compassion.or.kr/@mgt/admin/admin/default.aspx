﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_admin_admin_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">

<script type="text/javascript">
    $(function () {
        $("#s_keyword").keydown(function (e) {
            if (e.keyCode == 13) {
                search();
            }
        })
    });
</script>

</asp:Content>


<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>

		</div>
		<div class="form-horizontal box-body" >
			<div class="box-body">
				<div class="form-group">
					<label for="s_keyword" class="col-sm-2 control-label">검색</label>
					<div class="col-sm-10">
						<asp:TextBox runat="server" ID="s_keyword" Width="415" OnTextChanged="search" AutoPostBack="true" cssClass="form-control inline" ></asp:TextBox>
						* 검색대상 : 이름 , 이메일 , 직위 , 부서
					</div>
				</div>
				<div class="box-footer clearfix text-center">
					<asp:LinkButton runat="server" ID="btn_search" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
				</div>
			</div>
			<!--
			<div class="box-footer">
				<button type="submit" class="btn btn-default">Cancel</button>
				<button type="submit" class="btn btn-info pull-right">Sign in</button>
			</div>
			-->
			
		</div>
		
	</div><!-- /.box -->
        
	<div class="box box-primary">
		
		 <div class="box-header">
            <h3 class="box-title">총 <asp:Literal runat=server ID=lbTotal/> 건</h3>
            
        </div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
		
			<table class="table table-hover table-bordered ">
				<colgroup>
					<col width="5%" />
					<col width="15%" />
					<col width="15%" />
					<col width="15%" />
					<col width="10%" />
					<col width="10%" />
					<col width="10%" />
				</colgroup>
				<thead>
					<tr>
						<th>번호</th>
						<th>구분</th>
						<th>아이디</th>
						<th>이름</th>
						<th>부서</th> 
						<th>직위</th>
						<th>등록일</th>

					</tr>
				</thead>
				<tbody>
							
					<asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
						<ItemTemplate>
							<tr onclick="goPage('update.aspx' , { a_id: '<%#Eval("a_id")%>', p: '<%#paging.CurrentPage %>' , t: 'update' })" class="tr_link">
								<td><asp:Literal runat=server ID=lbIdx></asp:Literal></td>
								<td><%#Eval("a_type_name")%></td>
								<td class="text-left"><%#Eval("a_email")%></td>
								<td ><%#Eval("a_name")%></td>
								<td><%#Eval("a_part")%></td>
								<td><%#Eval("a_position")%></td>
								<td><%#Eval("a_regdate" , "{0:yy.MM.dd}")%></td>
							
							</tr>	
						</ItemTemplate>
						 <FooterTemplate>
							 <tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								 <td colspan="7" class="text-center">데이터가 없습니다.</td>
							 </tr>
						</FooterTemplate>
					</asp:Repeater>	
						
				
				</tbody>
			</table>
		</div>

		 <div class="box-footer clearfix text-center">

			 <uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
			 
			 <asp:LinkButton runat="server" ID="btn_add" href="update.aspx" CssClass="btn btn-bitbucket pull-right">신규등록</asp:LinkButton>
			 <!--
			 <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
			 -->
        </div>

	</div>


</asp:Content>