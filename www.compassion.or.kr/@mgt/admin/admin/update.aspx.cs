﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_admin_admin_update : AdminBasePage{


	protected override void OnPreRender(EventArgs e) {
		a_pwd.Attributes.Add("value", a_pwd.Text);
		base.OnPreRender(e);
	}

	List<v_admin_auth> AdminAuth {
		get {
			if (this.ViewState["cur_admin_auth"] == null)
				this.ViewState["cur_admin_auth"] = new List<v_admin_auth>().ToJson();
			return this.ViewState["cur_admin_auth"].ToString().ToObject<List<v_admin_auth>>();
		}
		set {
			this.ViewState["cur_admin_auth"] = value.ToJson();
		}
	}

	List<admin_menu> AdminMenu {
		get {
			if (this.ViewState["admin_menu"] == null)
				this.ViewState["admin_menu"] = new List<admin_menu>().ToJson();
			return this.ViewState["admin_menu"].ToString().ToObject<List<admin_menu>>();
		}
		set {
			this.ViewState["admin_menu"] = value.ToJson();
		}
	}


	protected int AdminId {
		set {
			this.ViewState.Add("a_id", value);
		}
		get {
			if (this.ViewState["a_id"] == null)
				return 0;
			return Convert.ToInt32(this.ViewState["a_id"]);
		}
	}

	
	List<recruit_jobcode> JobCode {
		get {
			if (this.ViewState["recruit_jobcode"] == null)
				this.ViewState["recruit_jobcode"] = new List<recruit_jobcode>().ToJson();
			return this.ViewState["recruit_jobcode"].ToString().ToObject<List<recruit_jobcode>>();
		}
		set {
			this.ViewState["recruit_jobcode"] = value.ToJson();
		}
	}

	

	List<admin_recruit_jobcode> AdminJobCode {
		get {
			if (this.ViewState["admin_recruit_jobcode"] == null)
				this.ViewState["admin_recruit_jobcode"] = new List<admin_recruit_jobcode>().ToJson();
			return this.ViewState["admin_recruit_jobcode"].ToString().ToObject<List<admin_recruit_jobcode>>();
		}
		set {
			this.ViewState["admin_recruit_jobcode"] = value.ToJson();
		}
	}

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		v_admin_auth auth = base.GetPageAuth();

		base.Action = Request["t"];
		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		var isValid = new RequestValidator()
		.Add("a_id", RequestValidator.Type.Numeric)
		.Add("t", RequestValidator.Type.Alphabet)
		.Validate(this.Context, "default.aspx");


		var cookie = AdminLoginSession.GetCookie(this.Context);
		var admin_types = new List<code>();
		if(cookie.type == "recruit") {
			admin_types = StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "admin_type" && p.cd_key == cookie.type).OrderBy(p => p.cd_order).ToList();
		} else {
			admin_types = StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "admin_type").OrderBy(p => p.cd_order).ToList();
		}

		a_type.Items.Add(new ListItem("선택하세요", ""));
		foreach (var a in admin_types) {
			a_type.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}

		if (base.Action == "update") {
			this.AdminId = Convert.ToInt32(Request["a_id"]);

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.admin.First(p => p.a_id == this.AdminId);
                //string sqlStr = string.Format("select * from admin where a_id = '{0}'", this.AdminId);
                //var entity = www6.selectText(sqlStr).Rows[0].DataTableToFirst<admin>();
                var entity = www6.selectQF<admin>("a_id", this.AdminId);

                a_name.Value = entity.a_name;
                a_part.Value = entity.a_part;
                a_position.Value = entity.a_position;
                a_email.Value = entity.a_email;

                a_phone_mobile.Value = entity.a_phone_mobile;
                a_phone_office.Value = entity.a_phone_office;
                a_type.SelectedValue = entity.a_type;

                tabm2.Visible = tab2.Visible = entity.a_type == "normal" || entity.a_type == "recruit";
                tabm3.Visible = tab3.Visible = entity.a_type == "recruit";

                //this.AdminAuth = dao.v_admin_auth.Where(p => p.aa_a_id == this.AdminId).ToList();
                this.AdminAuth = www6.selectQ<v_admin_auth>("aa_a_id", this.AdminId);
            }

			base.LoadComplete += new EventHandler(list_LoadComplete);
			
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;

		} else {
			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

		//var admin = AdminLoginSession.GetCookie(this.Context).rawData;
		
		this.ShowMenus();
		this.ShowJobCode();


	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		//this.ShowMenus();
	}

	protected void btn_update_click(object sender, EventArgs e) {
        using (AdminDataContext dao = new AdminDataContext())
        {

            var entity = new admin();
            if (base.Action == "update")
            {
                //entity = dao.admin.First(p => p.a_id == this.AdminId);
                entity = www6.selectQF<admin>("a_id", this.AdminId);
                if (a_pwd.Text.Trim() != "")
                {
                    entity.a_pwd = a_pwd.Text.SHA256Hash();
                }
                entity.a_name = a_name.Value;
                entity.a_email = a_email.Value;
                entity.a_part = a_part.Value;
                entity.a_position = a_position.Value;
                entity.a_type = a_type.SelectedValue;
                entity.a_phone_mobile = a_phone_mobile.Value;
                entity.a_phone_office = a_phone_office.Value;

                //dao.SubmitChanges();                
                //www6.update(entity);
                www6.update(entity);
                
                base.WriteLog(AdminLog.Type.update, string.Format("{0} , {1} , {2}", a_type.SelectedItem.Text, a_name.Value, a_email.Value));

                Master.ValueAction.Value = "none";

            }
            else
            {
                var existEmail = www6.selectQ<admin>("a_email", a_email.Value);

                //if (dao.admin.Any(p => p.a_email == a_email.Value))
                if(existEmail.Any())
                {
                    Master.ValueAction.Value = "none";
                    Master.ValueMessage.Value = "이미 등록된 아이디가 있습니다.";
                    return;
                }

                entity.a_name = a_name.Value;
                entity.a_email = a_email.Value;
                entity.a_part = a_part.Value;
                entity.a_type = a_type.SelectedValue;
                entity.a_position = a_position.Value;
                entity.a_regdate = DateTime.Now;
                entity.a_pwd = a_pwd.Text.SHA256Hash();
                entity.a_a_id = AdminLoginSession.GetCookie(this.Context).identifier;
                entity.a_phone_mobile = a_phone_mobile.Value;
                entity.a_phone_office = a_phone_office.Value;
                //dao.admin.InsertOnSubmit(entity);
                //www6.insert(entity);
                www6.insert(entity);
                //dao.SubmitChanges();

                base.WriteLog(AdminLog.Type.update, string.Format("{0} , {1} , {2}", a_type.SelectedItem.Text, a_name.Value, a_email.Value));
                Master.ValueAction.Value = "list";            
                this.AdminId = entity.a_id;
            }

            // 일반 관리자와 채용관리자만 메뉴권한 체크
            if (entity.a_type == "normal" || entity.a_type == "recruit")
            {
                UpdateMenu(this.AdminId);
            }

            // 채용 관리자만
            if (entity.a_type == "recruit")
            {
                UpdateJobCode(this.AdminId);
            }

            a_pwd.Text = "";
            Master.ValueMessage.Value = "적용되었습니다.";
        }
	}

	void UpdateMenu(int a_id) {

		List<admin_auth> result = new List<admin_auth>();
		for( int i = 0 ; i < repeater_menu.Items.Count ; i++) {
			RepeaterItem item = repeater_menu.Items[i];
			var entity = this.AdminMenu[i];

			if (entity.am_depth < 1)
				continue;

			CheckBox cb_auth_create = item.FindControl("cb_auth_create") as CheckBox;
			CheckBox cb_auth_update = item.FindControl("cb_auth_update") as CheckBox;
			CheckBox cb_auth_delete = item.FindControl("cb_auth_delete") as CheckBox;
			CheckBox cb_auth_read = item.FindControl("cb_auth_read") as CheckBox;


			//Response.Write("cb_auth_create.Checked = "+ cb_auth_create.Checked+"<br/>");
			//Response.Write("cb_auth_update.Checked = " + cb_auth_update.Checked + "<br/>");
			//Response.Write("cb_auth_delete.Checked = " + cb_auth_delete.Checked + "<br/>");
			//Response.Write("cb_auth_read.Checked = " + cb_auth_read.Checked + "<br/>");

			if (cb_auth_create.Checked || cb_auth_update.Checked || cb_auth_delete.Checked || cb_auth_read.Checked) {
				result.Add(new admin_auth() {
					aa_a_id = a_id, aa_am_code = entity.am_code,
					aa_auth_create = cb_auth_create.Checked, aa_auth_update = cb_auth_update.Checked, aa_auth_delete = cb_auth_delete.Checked, aa_auth_read = cb_auth_read.Checked
				});
			}
		}

        using (AdminDataContext dao = new AdminDataContext())
        {
            //dao.admin_auth.DeleteAllOnSubmit(dao.admin_auth.Where(p => p.aa_a_id == a_id));
            var delList = www6.selectQ<admin_auth>("aa_a_id", a_id);
            www6.delete(delList);
            
            //dao.admin_auth.InsertAllOnSubmit(result);
            foreach (admin_auth rs in result)
            {
                //www6.cud(MakeSQL.insertQ2(rs));
                www6.insert(rs);
            }
        }
		
	}

	protected void btn_remove_click(object sender, EventArgs e) {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.admin.First(p => p.a_id == this.AdminId);
            var entity = www6.selectQF<admin>("a_id", AdminId);
            entity.a_deleted = true;
            
            //dao.SubmitChanges();
            www6.update(entity);
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

	void ShowMenus() {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //this.AdminMenu = dao.admin_menu.Where(p => p.am_display == true && ((p.am_depth == 1 && p.am_path != "") || p.am_depth == 0 || p.am_depth == 2)).OrderBy(p => p.am_group_order).ThenBy(p => p.am_order).ToList();
            string aMenuStr = string.Format(@"select * from admin_menu 
                                              where am_display = 1 and ((am_depth = 1 and am_path != '') or am_depth = 0 or am_depth = 2)
                                              order by am_group_order, am_order");
            this.AdminMenu = www6.selectText(aMenuStr).DataTableToList<admin_menu>().ToList();

            // 채용담당자는 채용만 볼 수 있다.
            if (a_type.SelectedValue == "recruit")
            {
                this.AdminMenu = this.AdminMenu.Where(p => p.am_part == "recruit").ToList();
            }
            repeater_menu.DataSource = this.AdminMenu;
            repeater_menu.DataBind();
        }
	}



	void ShowJobCode() {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //this.AdminJobCode = dao.admin_recruit_jobcode.Where(p => p.aj_a_id == AdminId).ToList();
            this.AdminJobCode = www6.selectQ<admin_recruit_jobcode>("aj_a_id", AdminId);

            //this.JobCode = dao.recruit_jobcode.Where(p => p.rj_display == true).OrderBy(p => p.rj_depth1).ThenBy(p => p.rj_order).ToList();
            this.JobCode = www6.selectQ<recruit_jobcode>("rj_display", 1, "rj_depth1, rj_order");
            
            repeater_recruit.DataSource = this.JobCode.Where(p => p.rj_depth2 == "");
            repeater_recruit.DataBind();
        }
	}

	protected void repeater_menu_ItemDataBound(object sender, RepeaterItemEventArgs e) {
		admin_menu entity = e.Item.DataItem as admin_menu;
		
		if (this.AdminAuth.Any(p => p.am_code == entity.am_code)) {

			var auth = this.AdminAuth.First(p => p.am_code == entity.am_code);
			CheckBox cb_auth_create = e.Item.FindControl("cb_auth_create") as CheckBox;
			CheckBox cb_auth_update = e.Item.FindControl("cb_auth_update") as CheckBox;
			CheckBox cb_auth_delete = e.Item.FindControl("cb_auth_delete") as CheckBox;
			CheckBox cb_auth_read = e.Item.FindControl("cb_auth_read") as CheckBox;

			cb_auth_create.Checked = auth.aa_auth_create;
			cb_auth_update.Checked = auth.aa_auth_update;
			cb_auth_delete.Checked = auth.aa_auth_delete;
			cb_auth_read.Checked = auth.aa_auth_read;

		}
	}

	protected void paging_Navigate(int page) {
		this.GetList(page);
	}

	protected void btn_section_Click(object sender, EventArgs e) {
		LinkButton obj = sender as LinkButton;
		var no = obj.CommandArgument;

		int tabCount = 3;

		for(int i = 1; i < tabCount + 1; i++) {
			
			if (no == i.ToString()) {
				((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "active";
				((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "active tab-pane";
			} else {
				((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "";
				((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "tab-pane";

			}

			if (no == "2") {
				this.ShowMenus();
			}

			if(no == "3") {
				this.ShowJobCode();
			}


		}

	}

	protected void a_type_SelectedIndexChanged(object sender, EventArgs e) {

		tabm2.Visible = tab2.Visible = false;
		tabm3.Visible = tab3.Visible = false;

		if (a_type.SelectedValue == "normal" || a_type.SelectedValue == "recruit") {
			tabm2.Visible = tab2.Visible = true;
		}


		if (a_type.SelectedValue == "recruit") {
			tabm3.Visible = tab3.Visible = true;
		}

	}


	// 채용 직무 권한
	void UpdateJobCode(int a_id) {

		List<admin_recruit_jobcode> result = new List<admin_recruit_jobcode>();

		foreach (RepeaterItem depth1 in repeater_recruit.Items) {
			Repeater depth2_repeater = depth1.FindControl("repeater_recruit_depth2") as Repeater;

			foreach (RepeaterItem depth2 in depth2_repeater.Items) {
				CheckBox chk = depth2.FindControl("rj_id") as CheckBox;
				if (chk.Checked) {
					var rj_id = Convert.ToInt32(chk.ToolTip);
					var entity = this.JobCode.First(p => p.rj_id == rj_id);

					result.Add(new admin_recruit_jobcode() {
						aj_a_id = AdminId,
						aj_rj_depth1 = entity.rj_depth1,
						aj_rj_depth2 = entity.rj_depth2
					});
				}
			}

		}

        using (AdminDataContext dao = new AdminDataContext())
        {
            //dao.admin_recruit_jobcode.DeleteAllOnSubmit(dao.admin_recruit_jobcode.Where(p => p.aj_a_id == a_id));
            var delList = www6.selectQ< admin_recruit_jobcode>("aj_a_id", a_id);
            www6.delete(delList);

            //dao.admin_recruit_jobcode.InsertAllOnSubmit(result);
            foreach (admin_recruit_jobcode rs in result)
            {
                //www6.cud(MakeSQL.insertQ2(rs));
                www6.insert(rs);
            }
            //dao.SubmitChanges();
        }
	}


	protected void repeater_recruit_ItemDataBound(object sender, RepeaterItemEventArgs e) {
		recruit_jobcode entity = e.Item.DataItem as recruit_jobcode;
		Literal rj_depth1 = e.Item.FindControl("rj_depth1") as Literal;
		Repeater repeater_recruit_depth2 = e.Item.FindControl("repeater_recruit_depth2") as Repeater;

		rj_depth1.Text = entity.rj_name;
		repeater_recruit_depth2.DataSource = this.JobCode.Where(p => p.rj_depth1 == entity.rj_depth1 && p.rj_depth2 != "").OrderBy(p => p.rj_order).ThenBy(p => p.rj_name);
		repeater_recruit_depth2.DataBind();
	}


	protected void repeater_recruit_depth2_ItemDataBound(object sender, RepeaterItemEventArgs e) {
		recruit_jobcode entity = e.Item.DataItem as recruit_jobcode;

		CheckBox rj_id = e.Item.FindControl("rj_id") as CheckBox;
		rj_id.Checked = this.AdminJobCode.Any(p => p.aj_rj_depth1 == entity.rj_depth1 && p.aj_rj_depth2 == entity.rj_depth2);
	}

}