﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="admin_login" MasterPageFile="~/@mgt/mgt_top.master"%>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<script type="text/javascript">

	$(function () {
		
		$("body").addClass("hold-transition login-page");

		$(".login_id").focus();
		if ($(".message").val() != "") {
			alert($(".message").val());
		}

		$("#login_pw").keydown(function (e) {
			
			if (e.keyCode == 13) {
				eval($("#btn_login").attr("href").replace("javascript:", ""));
				
			}
		})
	});

	var onSubmit = function () {

		if ($(".login_id").val() == "") {
			alert("아이디를 입력해주세요.");
			$(".login_id").focus();
			return false;
		}
		if ($(".login_pw").val() == "") {
			alert("비밀번호를 입력해주세요.");
			$(".login_pw").focus();
			return false;
		}
		return true;
	}
</script>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
	<input type="hidden" runat="server" id="message" class="message" />

	<div class="login-box" style="width:400px">
      <div class="login-logo">
        <a href="/"><b>compassion</b> Admin</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">관리자 계정을 입력해주세요</p>
        
          <div class="form-group has-feedback">
            <input type="text" class="form-control login_id" id="login_id" name="login_id" runat=server  placeholder="admin ID">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control login_pw" id="login_pw" runat=server  placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              
            </div><!-- /.col -->
            <div class="col-xs-4">
				<asp:LinkButton runat="server" ID="btn_login" OnClientClick="return onSubmit()" CssClass="btn btn-primary btn-block btn-flat" OnClick="btn_login_Click">로그인</asp:LinkButton>

            </div><!-- /.col -->
          </div>
        
		  <!--
        <a href="#">I forgot my password</a><br>
        -->

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->



</asp:Content>