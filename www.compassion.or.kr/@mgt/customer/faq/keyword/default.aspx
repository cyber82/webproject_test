﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_customer_keyword" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
		$(function () {
		
			$page.init();


		});

		var $page = {

			removes : [] , 

			init: function () {

				$("#list").sortable({
					placeholder: "sort-highlight",
					handle: ".handle",
					forcePlaceholderSize: true,
					zIndex: 999999
				}).disableSelection();


				// 업데이트
				$("#btn_update").click(function () {

					if (confirm("업데이트 하시겠습니까?")) {

						$page.update();

					};

					return false;
				});

				// 추가
				$("#btn_add").click(function () {
					var title = $("#keyword").val();

					if (title == "") {
						alert("키워드를 입력해주세요");
						$("#keyword").focus();
						return false;
					}

					if (confirm("추가 하시겠습니까?")) {
						
						$page.add(title);
						$("#keyword").val("");
					};

					return false;
				});


				this.getList();

			},

			add : function(title){
				
				var container = $("ul[data-role='list-container']");
				
				var id = "-1";

				var template = $("ul[data-role='list-template'] li").clone();

				template.attr("data-id", id);
				template.find("[data-role=title]").html(title);
				template.find("[data-role=btn_remove]")
					.click(function () {
						$page.remove(template);
						return false;

					})
					.attr("data-id", id);
				container.append(template);


			} , 

			update : function(){
			
				var data = [];
				$.each($("ul[data-role='list-container'] li"), function (i) {

					var k_id = $(this).attr("data-id");
					var k_word = $(this).find("[data-role=title]").html();
					var k_order = i;

					data.push({ k_id: k_id, k_word: k_word, k_order: k_order, k_type: "faq" });

				});

				var json = $.toJSON(data);
				var removes = $.toJSON($page.removes);
				
				$.post("default.ashx", { t: "update", d: json, removes: removes}, function (r) {

					alert(r.message);

					if (r.success) {

						$page.removes = [];
						$page.getList();

					} 
				});

			} , 

			remove: function (li) {

				var k_id = parseInt(li.attr("data-id"));
				
				if (k_id > 0) {
					$page.removes.push(k_id);
				}
				li.remove();

			} ,

			getList: function () {

			    $.get("default.ashx", { t: "list", k_type: "faq" }, function (r) {

					var container = $("ul[data-role='list-container']");
					container.empty();

					$.each(r.data, function () {

						var title = this.k_word;
						var id = this.k_id;

						var template = $("ul[data-role='list-template'] li").clone();

						template.attr("data-id", id);
						template.find("[data-role=title]").html(title);
						template.find("[data-role=btn_remove]")
							.click(function () {
								$page.remove(template);
								return false;

							})
							.attr("data-id", id);
						container.append(template);

					})
				});

			}

		}
		
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert">×</button>
		<h4><i class="icon fa fa-check"></i>Alert!</h4>
		FAQ의 키워드를 관리하는 페이지입니다.<br />
        
        <div style="display:inline;color:#444;">
            <i class="fa fa-ellipsis-v"></i>
            <i class="fa fa-ellipsis-v"></i>
        </div>
        아이콘을 드래그하여 노출 순서를 변경하세요<br />
		변경사항은 적용버튼을 클릭해야 반영됩니다.
	</div>

    
	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search">
            <div class="form-inline">

                
			    <div class="input-group" style="width: 300px">
				    <input type="text" name="message" id="keyword" placeholder="등록하고자 하는 키워드 입력" class="form-control">
                </div>
                <button type="button" runat="server" id="btn_add" class="btn btn-danger">추가</button>
		    </div>
        </div>

    </div>



	<div class="box box-primary">
		
		<!-- /.box-header -->
		<div class="box-body">
			<ul data-role="list-template" style="display:none">
				<li data-role="item" data-id="">
					<span class="handle">
						<i class="fa fa-ellipsis-v"></i>
						<i class="fa fa-ellipsis-v"></i>
					</span>
					
					<span class="text" data-role="title"></span>
					<div class="tools">
						<i class="fa fa-trash-o" data-role="btn_remove" data-id=""></i>
					</div>
				</li>

			</ul>

			<ul id="list" data-role="list-container" class="todo-list">

			</ul>
		</div>
	
	</div>
	<!-- /.box -->

	<div class="box-footer clearfix text-center">
		<div class="form-inline pull-right">
            <button type="button" runat="server" id="btn_update" class="btn btn-danger">적용</button>
		</div>
	</div>

</asp:Content>