﻿<%@ WebHandler Language="C#" Class="mgt_customer_faq_keyword_update_ashx" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;

public class mgt_customer_faq_keyword_update_ashx : BaseHandler
{
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context)
    {

        var t = context.Request["t"].EmptyIfNull();

        if (t == "list")
        {
            this.GetList(context);
        }
        else if (t == "update")
        {
            this.Update(context);
        }
    }


    void Update(HttpContext context)
    {

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        JsonWriter json = new JsonWriter()
        {
            success = false
        };

        if (!AdminLoginSession.HasCookie(context))
        {
            json.message = "관리자 로그인이 필요합니다.";
            json.Write(context);
            return;
        }

        try
        {

            var removes = context.Request["removes"].ToObject<List<int>>();   // 삭제대상
            var d = context.Request["d"];
            var a_id = AdminLoginSession.GetCookie(context).identifier;

            var data = d.ToObject<List<keyword>>();
            
            //using (FrontDataContext dao = new FrontDataContext())
            //{

            foreach (var entity in removes)
            {

                //var item = dao.keyword.First(p => p.k_id == entity);
                //dao.keyword.DeleteOnSubmit(item);

                string dbName = "SqlCompassionWeb";
                string dbType = "Text";
                object[] objSql_keyword = new object[1] { "DELETE from keyword where k_id ='" + entity + "'" };
                DataSet ds_keyword = _www6Service.NTx_ExecuteQuery(dbName, objSql_keyword, dbType, null, null);
                

            }

            var inserts = new List<keyword>();
            foreach (var entity in data)
            {
                if (entity.k_id < 1)
                {
                    //dao.keyword.InsertOnSubmit(new keyword()
                    //{
                    //    k_a_id = a_id,
                    //    k_order = entity.k_order,
                    //    k_regdate = DateTime.Now,
                    //    k_type = entity.k_type,
                    //    k_word = entity.k_word
                    //});

                    string dbName = "SqlCompassionWeb";
                    string dbType = "Text";
                    object[] objSql_keyword = new object[1] { "INSERT into keyword(k_a_id, k_order, k_regdate, k_type, k_word) " +
                                                                  "values ('"+ a_id +"','" + entity.k_order + "', GETDATE(), '" + entity.k_type + "', '" + entity.k_word + "')" };
                    DataSet ds_keyword = _www6Service.NTx_ExecuteQuery(dbName, objSql_keyword, dbType, null, null);
                    
                }
                else
                {

                    //var item = dao.keyword.First(p => p.k_id == entity.k_id);
                    //item.k_order = entity.k_order;

                    string dbName = "SqlCompassionWeb";
                    string dbType = "Text";
                    object[] objSql_keyword = new object[1] { "UPDATE keyword set k_order = '" + entity.k_order + "' where k_id = '" + entity.k_id + "'" };
                    DataSet ds_keyword = _www6Service.NTx_ExecuteQuery(dbName, objSql_keyword, dbType, null, null);
                    
                }
            }

            //dao.SubmitChanges();
            //}

            json.message = "적용되었습니다.";
            json.success = true;

            AdminLog.Write(context, AdminLog.Type.update, new AdminBasePage().GetPageAuth("/@mgt/customer/faq/keyword/").am_code, string.Format("삭제 : {0} , 추가/변경 : {1}", removes.ToJson(), d));


        }
        catch (Exception ex)
        {
            ErrorLog.Write(context, 500, ex.ToString());
            json.message = ex.Message;
        }

        json.Write(context);

    }


    void GetList(HttpContext context)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        using (FrontDataContext dao = new FrontDataContext())
        {
            var type = context.Request["k_type"];
            //var list = dao.keyword.Where(p => p.k_type == type).OrderBy(p => p.k_order).ToList();

            string dbName = "SqlCompassionWeb";
            string dbType = "Text";
            object[] objSql_list = new object[1] { "SELECT * from keyword where k_type = '" + type + "' order by k_order" };
            DataSet ds_list = _www6Service.NTx_ExecuteQuery(dbName, objSql_list, dbType, null, null);
            List<keyword> list = ds_list.Tables[0].DataTableToList<keyword>();

            JsonWriter json = new JsonWriter()
            {
                data = list,
                success = true
            };

            json.Write(context);
        }

    }
}
