﻿<%@ WebHandler Language="C#" Class="mgt_customer_faq_category_ashx" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;

public class mgt_customer_faq_category_ashx : BaseHandler
{
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context)
    {

        var t = context.Request["t"].EmptyIfNull();

        if (t == "list")
        {
            this.GetList(context);
        }
        else if (t == "update")
        {
            this.Update(context);
        }
    }


    void Update(HttpContext context)
    {

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        JsonWriter json = new JsonWriter()
        {
            success = false
        };

        if (!AdminLoginSession.HasCookie(context))
        {
            json.message = "관리자 로그인이 필요합니다.";
            json.Write(context);
            return;
        }

        try
        {

            var removes = context.Request["removes"].ToObject<List<string>>();   // 삭제대상
            var d = context.Request["d"];
            var a_id = AdminLoginSession.GetCookie(context).identifier;
            var data = d.ToObject<List<code>>();

            //using (FrontDataContext dao = new FrontDataContext())
            //{

            foreach (var entity in removes)
            {

                //faq등록 여부 조회
                //var faq_count = dao.faq.Count(p => p.f_type == entity);

                string dbName = "SqlCompassionWeb";
                string dbType = "Text";
                object[] objSql_faq = new object[1] { "SELECT * from faq where f_type ='" + entity + "'" };
                DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql_faq, dbType, null, null);
                List<faq> faq = ds.Tables[0].DataTableToList<faq>();

                if (faq.Count < 1)
                {
                    //저장하지 않은 상태에서도 태그를 만들었다 삭제한 경우 removes에 담겨저 온다
                    //DB에 있는지 확인 후 삭제해야함
                    //var count = dao.code.Count(p => p.cd_key == entity && p.cd_group == "faq");

                    object[] objSql_codeCount = new object[1] { "SELECT * from code where cd_key ='" + entity + "' and cd_group = 'faq'" };
                    DataSet ds_codeCount = _www6Service.NTx_ExecuteQuery(dbName, objSql_codeCount, dbType, null, null);
                    List<code> codeCount = ds_codeCount.Tables[0].DataTableToList<code>();

                    if (codeCount.Count > 0)
                    {
                        //var item = dao.code.First(p => p.cd_key == entity && p.cd_group == "faq");
                        //dao.code.DeleteOnSubmit(item);

                        object[] objSql_delCode = new object[1] { "DELETE from code where cd_key ='" + entity + "' and cd_group = 'faq'" };
                        DataSet ds_delCode = _www6Service.NTx_ExecuteQuery(dbName, objSql_delCode, dbType, null, null);

                    }

                    //}
                }
            }
            //dao.SubmitChanges();

            var inserts = new List<code>();
            foreach (var insert in data)
            {
                    
                string dbName = "SqlCompassionWeb";
                string dbType = "Text";
                //var count = dao.code.Count(p => p.cd_key == entity.cd_key && p.cd_group == "faq");
                object[] objSql_codeCount = new object[1] { "SELECT * from code where cd_key ='" + insert.cd_key + "' and cd_group = 'faq'" };
                DataSet ds_codeCount = _www6Service.NTx_ExecuteQuery(dbName, objSql_codeCount, dbType, null, null);
                List<code> codeCount = ds_codeCount.Tables[0].DataTableToList<code>();

                //등록된 코드는 수정
                if (codeCount.Count > 0)
                {
                    //var item = dao.code.First(p => p.cd_key == insert.cd_key && p.cd_group == "faq");
                    //item.cd_value = insert.cd_value;
                    //item.cd_order = insert.cd_order;
                    //item.cd_display = insert.cd_display;
                    //dao.SubmitChanges();

                    object[] objSql_selectCode = new object[1] { "SELECT * from code where cd_key ='" + insert.cd_key + "' and cd_group = 'faq'" };
                    DataSet ds_selectCode = _www6Service.NTx_ExecuteQuery(dbName, objSql_selectCode, dbType, null, null);
                    code selectCodeResult = ds_selectCode.Tables[0].Rows[0].DataTableToFirst<code>();

                    object[] objSql_updateCode = new object[1] { "UPDATE code set cd_value ='" + insert.cd_value + "', cd_order = '" + insert.cd_order + "', cd_display = '" + insert.cd_display + "' where cd_key ='" + insert.cd_key + "' and cd_group = 'faq'" };
                    DataSet ds_updateCode = _www6Service.NTx_ExecuteQuery(dbName, objSql_updateCode, dbType, null, null);
                    //tVisionTripChildMeet updateCodeResult = ds_updateCode.Tables[0].Rows[0].DataTableToFirst<tVisionTripChildMeet>();

                    //AdminLog.Write(context, AdminLog.Type.update, new AdminBasePage().GetPageAuth("/@mgt/customer/faq/category/").am_code, string.Format("{0}", item.ToJson()));
                    AdminLog.Write(context, AdminLog.Type.update, new AdminBasePage().GetPageAuth("/@mgt/customer/faq/category/").am_code, string.Format("{0}", selectCodeResult.ToJson()));
                    //신규 코드는 등록
                }
                else
                {

                    var item = new code()
                    {
                        cd_group = "faq",
                        cd_key = insert.cd_key,
                        cd_value = insert.cd_value,
                        cd_order = insert.cd_order,
                        cd_display = insert.cd_display
                    };

                    //dao.code.InsertOnSubmit(item);

                    object[] objSql_insertCode = new object[1] { "INSERT into code(cd_group, cd_key, cd_value, cd_order, cd_display)"
                                                                    +"values('faq', '" + insert.cd_key + "', '"+ insert.cd_value + "', '" + insert.cd_order + "', '" + insert.cd_display + "')" };
                    DataSet ds_insertCode = _www6Service.NTx_ExecuteQuery(dbName, objSql_insertCode, dbType, null, null);


                    //AdminLog.Write(context, AdminLog.Type.insert, new AdminBasePage().GetPageAuth("/@mgt/customer/faq/category/").am_code, string.Format("{0}", entity.ToJson()));
                    AdminLog.Write(context, AdminLog.Type.insert, new AdminBasePage().GetPageAuth("/@mgt/customer/faq/category/").am_code, string.Format("{0}", insert.ToJson()));

                    //dao.SubmitChanges();
                }

            }

        
            json.message = "적용되었습니다.";
        json.success = true;




    }
        catch (Exception ex)
        {
            ErrorLog.Write(context, 500, ex.ToString());
            json.message = ex.Message;
        }

json.Write(context);

    }


    void GetList(HttpContext context)
{
    CommonLib.WWW6Service.SoaHelperSoap _www6Service;
    _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

    //using (FrontDataContext dao = new FrontDataContext())
    //{
    //var list = dao.code.Where(p => p.cd_group == "faq").OrderBy(p => p.cd_order).ToList();
    //var list = dao.sp_faq_category_list(-1, "");

    string dbName = "SqlCompassionWeb";
    object[] objSql = new object[1] { "sp_faq_category_list" };
    string dbType = "SP";
    object[] objParam = new object[2] { "display", "keyword" };
    object[] objValue = new object[2] { -1, ""};
    DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, objParam, objValue);
    List<sp_faq_category_listResult> list = ds.Tables[0].DataTableToList<sp_faq_category_listResult>();

    JsonWriter json = new JsonWriter()
    {
        data = list,
        success = true
    };

    json.Write(context);
}

}
//}
