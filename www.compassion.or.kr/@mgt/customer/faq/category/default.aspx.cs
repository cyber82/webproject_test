﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_customer_faq_category : AdminBasePage {

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();


		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
		btn_update.Visible = auth.aa_auth_update;
		btn_add.Visible = auth.aa_auth_create;
	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}
	
}
