﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_customer_faq_category" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
		$(function () {
		    $page.init();


		    $("#cd_display").bootstrapSwitch();

		    
		});

		var $page = {

			removes : [] , 

			init: function () {

				$("#list").sortable({
					placeholder: "sort-highlight",
					handle: ".handle",
					forcePlaceholderSize: true,
					zIndex: 999999
				}).disableSelection();


				// 업데이트
				$("#btn_update").click(function () {

					if (confirm("적용 하시겠습니까?")) {

						$page.update();

					};

					return false;
				});

				// 추가
				$("#btn_add").click(function () {
				    var cd_key = $("#cd_key").val();
				    var cd_value = $("#cd_value").val();
				    var cd_display = $("#cd_display").is(":checked");

				    if (cd_key == "") {
				        alert("코드를 입력해주세요.");
				        $("#cd_key").focus();
				        return false;
				    }

				    var item = $("#list li").find("[data-role=cd_key][data-key='" + cd_key + "']");
				    if (item.length > 0) {
				        alert("중복된 코드가 존재합니다. [" + item.eq(0).next().text() + "]");
				        return false;
				    }

				    if (cd_value == "") {
						alert("키워드를 입력해주세요");
						$("#cd_value").focus();
						return false;
					}

					if (confirm("추가 하시겠습니까?")) {
						
					    $page.add({ cd_key: cd_key, cd_value: cd_value, cd_display: cd_display });

					    $("#cd_key").val("");
					    $("#cd_value").val("");
					    $('#cd_display').bootstrapSwitch('state', true);
					};

					return false;
				});

				$("#btn_modify").click(function () {
				    $page.modify();
				});

				$("#btn_cancel").click(function () {
				    $page.cancel();
				});


				this.getList();

			},

			add : function(entity){
			    $page.addRow(entity);
			} , 

			update : function(){
			
				var data = [];
				$.each($("ul[data-role='list-container'] li"), function (i) {

				    var cd_key = $(this).find("[data-role=cd_key]").attr("data-key");
				    var cd_value = $(this).find("[data-role=cd_value]").attr("data-value");
				    var cd_display = $(this).find("[data-role=cd_display]").is(":checked");
				    var cd_order = i;

					data.push({ cd_key: cd_key, cd_value: cd_value, cd_display: cd_display, cd_order: cd_order });

				});

				var json = $.toJSON(data);
				var removes = $.toJSON($page.removes);
				$.post("default.ashx", { t: "update", d: json, removes: removes }, function (r) {

					alert(r.message);

					if (r.success) {

						$page.removes = [];
						$page.getList();

					} 
				});

			} , 

			remove: function (li) {

			    $page.cancel();

			    var count = li.find("[data-role=cd_total]").text();
			    if (count != "") {
			        alert(count + "개의 FAQ가 등록되어 있습니다.\nFAQ삭제 후 다시 시도해주세요");
			        return false;
			    }

			    var id = li.attr("data-id");
			    
				
			    if (id.length > 0) {
					$page.removes.push(id);
				}
				li.remove();

			} ,

			getList: function () {
			    $.get("default.ashx", { t: "list" }, function (r) {

					var container = $("ul[data-role='list-container']");
					container.empty();
					$.each(r.data, function () {
					    $page.addRow(this);
					})
				});

			},

			addRow: function (entity) {
			    var container = $("ul[data-role='list-container']");

			    var template = $("ul[data-role='list-template']>li").clone();

			    template.attr("data-id", entity.cd_key);
			    template.find("[data-role=cd_key]").html(entity.cd_key);
			    template.find("[data-role=cd_key]").attr("data-key", entity.cd_key);
			    template.find("[data-role=cd_value]").html(entity.cd_value);
			    template.find("[data-role=cd_value]").attr("data-value", entity.cd_value);
			    template.find("[data-role=cd_display]").bootstrapSwitch("state", entity.cd_display);

			    if (entity.cd_count) {
			        template.find("[data-role=cd_total]").text(entity.cd_count);
			    }
			    
			    template.find("[data-role=btn_remove]")
					.click(function () {
					    $page.remove(template);
					    return false;

					})
					.attr("data-id", entity.cd_key);
			    template.find("[data-role=btn_edit]")
                    .click(function () {
                        $page.edit(template);
                        return false;
                    });
			    container.append(template);
			},

			edit: function (li) {

			    var cd_key = li.find("[data-role=cd_key]").attr("data-key");
			    var cd_value = li.find("[data-role=cd_value]").attr("data-value");
			    var cd_display = li.find("[data-role=cd_display]").is(":checked");

			    $("#cd_key").val(cd_key);
			    $("#cd_key").attr("disabled", true);
			    $("#cd_value").val(cd_value);
			    $("#cd_display").bootstrapSwitch("state", cd_display);
			    $("#btn_modify").show();
			    $("#btn_cancel").show();
			    $("#btn_add").hide();
			},

			modify: function () {
			    var cd_key = $("#cd_key").val();
			    var li = $("#list > li[data-id=" + cd_key + "]");
			    li.find("[data-role=cd_value]").attr("data-value", $("#cd_value").val());
			    li.find("[data-role=cd_value]").text($("#cd_value").val());
			    li.find("[data-role=cd_display]").bootstrapSwitch("state", $("#cd_display").is(":checked"));
			    $page.cancel();
			},

			cancel: function () {
			    $("#cd_key").val("");
			    $("#cd_key").attr("disabled", false);
			    $("#cd_value").val("");
			    $('#cd_display').bootstrapSwitch('state', true);

			    $("#btn_add").show();
			    $("#btn_modify").hide();
			    $("#btn_cancel").hide();
			}

		}
		
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert">×</button>
		<h4><i class="icon fa fa-check"></i>Alert!</h4>
		FAQ의 카테고리를 관리하는 페이지입니다.<br />
        ON, OFF버튼으로 노출여부를 설정할 수 있습니다.<br />

        <div style="display:inline;color:#444;">
            <i class="fa fa-ellipsis-v"></i>
            <i class="fa fa-ellipsis-v"></i>
        </div>
        아이콘을 드래그하여 노출 순서를 변경하세요<br />

        <span class="label label-default" data-role="cd_total">123</span>아이콘은 카테고리에 등록된 FAQ개수입니다.<br />

        우측 <div style="display:inline;">
            <i class="fa fa-edit"></i>
            <i class="fa fa-trash-o"></i>
        </div>아이콘을 이용해 수정, 삭제할 수 있습니다.<br />

		변경사항은 적용버튼을 클릭해야 반영됩니다.
	</div>

    
	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search">
            <div class="form-inline">
			    <div class="input-group" >
                    <input type="text" name="message" id="cd_key" placeholder="코드" class="form-control">
                </div>
                <div class="input-group" style="width:300px;">
				    <input type="text" name="message" id="cd_value" placeholder="카테고리명" class="form-control">
			    </div>
                <div class="input-group" style="width:90px;height:28px;">
                    <input type="checkbox" id="cd_display" data-size="small" checked="checked"/>
                </div>
                <button type="button" runat="server" id="btn_add" class="btn btn-danger">추가</button>
                <button type="button" runat="server" id="btn_modify" class="btn btn-warning" style="display:none;">수정</button>
                <button type="button" runat="server" id="btn_cancel" class="btn btn-success" style="display:none;">취소</button>
		    </div>
        </div>

    </div>


	<div class="box box-primary">
		
		<!-- /.box-header -->
		<div class="box-body">
			<ul data-role="list-template" style="display:none">
				<li data-role="item" data-id="">
					<span class="handle">
						<i class="fa fa-ellipsis-v"></i>
						<i class="fa fa-ellipsis-v"></i>
					</span>
                    <span class="text" data-role="cd_key" style="width:50px;"></span>
                    <span class="text" data-role="cd_value" style="width:200px;"></span>
                    <span class="text" style="width:70px;"><input type="checkbox" data-role="cd_display"/></span>
                    <span class="label label-default" data-role="cd_total"></span>
					<div class="tools">
                        <i class="fa fa-edit" data-role="btn_edit"></i>
						<i class="fa fa-trash-o" data-role="btn_remove"></i>
					</div>
				</li>
			</ul>

			<ul id="list" data-role="list-container" class="todo-list">
			</ul>
		</div>
	
	</div>
	<!-- /.box -->

	<div class="box-footer clearfix text-center">
		<a id="btn_update" runat="server" class="btn btn-danger pull-right">적용</a>
	</div>

</asp:Content>