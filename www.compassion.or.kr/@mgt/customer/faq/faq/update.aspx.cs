﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using CommonLib;
using System.Data;

public partial class mgt_customer_faq_faq_update : AdminBoardPage
{

    CommonLib.WWW6Service.SoaHelperSoap _www6Service;

    protected override void OnBeforePostBack()
    {
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        base.OnBeforePostBack();

        v_admin_auth auth = base.GetPageAuth();
        base.PrimaryKey = Request["f_id"];
        base.Action = Request["t"];


        btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

        // querystring 유효성 검사 후 문제시 리다이렉트
        var isValid = new RequestValidator()
            .Add("f_id", RequestValidator.Type.AlphaNumeric)
            .Add("t", RequestValidator.Type.Alphabet)
            .Validate(this.Context, "default.aspx");


        foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "faq").OrderBy(p => p.cd_order))
        {
            s_type.Items.Add(new ListItem(a.cd_value, a.cd_key)
            {
                //Selected = true
            });
        }


        if (base.Action == "update")
        {
            //using (AdminDataContext dao = new AdminDataContext()) {
            //var entity = dao.faq.First(p => p.f_id == Convert.ToInt32(PrimaryKey));

            string dbName = "SqlCompassionWeb";
            string dbType = "Text";
            object[] objSql_faq = new object[1] { "SELECT * from faq where f_id = '" + Convert.ToInt32(PrimaryKey) + "'" };
            DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql_faq, dbType, null, null);
            faq entity = ds.Tables[0].Rows[0].DataTableToFirst<faq>();

            s_type.SelectedValue = entity.f_type;
            f_display.Checked = entity.f_display;
            f_top.Checked = entity.f_top;
            f_question.Text = entity.f_question;
            f_answer.InnerHtml = entity.f_answer;
            f_order.Text = entity.f_order.ToString();

            //}

            btn_update.Visible = auth.aa_auth_update;
            btn_remove.Visible = auth.aa_auth_delete;


        }
        else
        {
            btn_update.Visible = auth.aa_auth_create;
            btn_remove.Visible = false;

        }

    }

    protected override void OnAfterPostBack()
    {
        base.OnAfterPostBack();
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
    }

    protected void btn_update_click(object sender, EventArgs e)
    {
        var arg = new faq()
        {
            f_regdate = DateTime.Now,
            f_question = f_question.Text,
            f_order = Convert.ToInt32(f_order.Text),
            f_display = f_display.Checked,
            f_top = f_top.Checked,
            f_answer = f_answer.InnerText,
            f_a_id = AdminLoginSession.GetCookie(this.Context).identifier,
            f_type = s_type.SelectedValue
        };

        //using (AdminDataContext dao = new AdminDataContext())
        //{
        if (Action == "update")
        {
            //var entity = dao.faq.First(p => p.f_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<faq>("f_id", Convert.ToInt32(PrimaryKey));

            entity.f_order = arg.f_order;
            entity.f_question = arg.f_question;
            entity.f_display = arg.f_display;
            entity.f_top = arg.f_top;
            entity.f_answer = arg.f_answer;
            entity.f_a_id = arg.f_a_id;
            entity.f_type = arg.f_type;

            //dao.SubmitChanges();
            www6.update(entity);

            base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "수정되었습니다.";
        }
        else
        {
            //dao.faq.InsertOnSubmit(arg);
            www6.insert(arg);
            //dao.SubmitChanges();

            base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

            base.PrimaryKey = arg.f_id.ToString();

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "등록되었습니다.";

        }
        //}
    }

    protected void btn_remove_click(object sender, EventArgs e)
    {
        //using (AdminDataContext dao = new AdminDataContext())
        //{

        //var entity = dao.faq.First(p => p.f_id == Convert.ToInt32(PrimaryKey));
        //dao.faq.DeleteOnSubmit(entity);
        //dao.SubmitChanges();
        //string delSql = MakeSQL.delQ(0, "faq", "f_id", Convert.ToInt32(PrimaryKey));
        //www6.cud(delSql);
        var entity = www6.selectQF<faq>("f_id", Convert.ToInt32(PrimaryKey));
        www6.delete(entity);

        base.WriteLog(AdminLog.Type.delete, string.Format("{0}", entity.ToJson()));
        //}


        Master.ValueAction.Value = "list";
        Master.ValueMessage.Value = "삭제되었습니다.";
    }

}