﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
using System.Data;

public partial class mgt_customer_faq_faq_default : AdminBoardPage {

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		base.sortColumn = sortColumn;
		base.sortDirection = sortDirection;
	

		foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p =>  p.cd_display == true && p.cd_group == "faq").OrderBy(p => p.cd_order)) {
			s_type.Items.Add(new ListItem(a.cd_value, a.cd_key) {
				Selected = true
			});
		}

		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;
		btn_add.Attributes.Add("href", "update.aspx");
	}

	protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();

		btn_add.Attributes.Add("href", "update.aspx");
	}

	protected override void GetList(int page)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        Master.IsSearch = false;
		if (f_top.Checked || s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
			Master.IsSearch = true;
		}

		//using (AdminDataContext dao = new AdminDataContext()) {
			string where_type = "";
			foreach (ListItem a in s_type.Items) {
				if (a.Selected) {
					where_type += string.Format("{0},", a.Value);
				}
			}
			
			//var list = dao.sp_faq_list(page, paging.RowsPerPage, where_type , Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text, f_top.Checked ? 1 : -1).ToList();
			var total = 0;

            string dbName = "SqlCompassionWeb";
            object[] objSql = new object[1] { "sp_faq_list" };
            string dbType = "SP";
            object[] objParam = new object[8] { "page", "rowsPerPage", "type", "display", "keyword", "startdate", "enddate", "top" };
            object[] objValue = new object[8] { page, paging.RowsPerPage, where_type, Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text, f_top.Checked ? 1 : -1 };
            DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, objParam, objValue);
            List<sp_faq_listResult> list = ds.Tables[0].DataTableToList<sp_faq_listResult>();

            if (list.Count > 0)
				total = list[0].total.Value;

			lbTotal.Text = total.ToString();

			paging.CurrentPage = page;
			paging.Calculate(total);
			repeater.DataSource = list;
			repeater.DataBind();

		//}
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				sp_faq_listResult entity = e.Item.DataItem as sp_faq_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
				((Literal)e.Item.FindControl("lbDisplay")).Text = entity.f_display ? "O" : "X";

				((Literal)e.Item.FindControl("lbType")).Text = s_type.Items.FindByValue(entity.f_type).Text;
			}
		}

	} 
	
}
