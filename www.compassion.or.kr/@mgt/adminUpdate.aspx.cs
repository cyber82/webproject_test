﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_adminUpdate : AdminBasePage {

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();
		
		this.ShowData();

		Master.PageTitle.Text = "관리자정보수정";
		Master.SubPageTitle.Text = "";
	}

	void ShowData() {

		var cookie = AdminLoginSession.GetCookie(this.Context);
		a_id.Text = cookie.email;

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.admin.First(p => p.a_id == cookie.identifier);
            var entity = www6.selectQF<admin>("a_id", cookie.identifier);
            a_name.Text = entity.a_name;
            a_part.Text = entity.a_part;
            a_position.Text = entity.a_position;
        }

	}

	protected void btn_update_click(object sender, EventArgs e) {

		var cookie = AdminLoginSession.GetCookie(this.Context);

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.admin.First(p => p.a_id == AdminLoginSession.GetCookie(this.Context).identifier);
            var entity = www6.selectQF<admin>("a_id", AdminLoginSession.GetCookie(this.Context).identifier);
            if (!string.IsNullOrEmpty(pwd.Text))
                entity.a_pwd = pwd.Text.SHA256Hash();

            entity.a_name = a_name.Text;
            entity.a_part = a_part.Text;
            entity.a_position = a_position.Text;

            //dao.SubmitChanges();

            www6.update(entity);

            Master.ValueAction.Value = "none";
            Master.ValueMessage.Value = "수정되었습니다.";
        }

	}


}