﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_sponsor_special_fixed_update" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">

	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript" src="update.js"></script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	
	<input type=hidden runat=server id=upload_root value="" />
	<input type=hidden runat=server id="hd_sponsor_types" value="" />
	<input type=hidden runat=server id="hd_campaigns" value="" />
	<input type=hidden runat=server id="hd_accountClass" value="" />
	<input type="hidden" runat="server" id="sf_thumb" />
	<input type="hidden" runat="server" id="sf_thumb_m" />
	<input type="hidden" runat="server" id="sf_image" />
	<input type="hidden" runat="server" id="sf_image_m" />
	
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="1" >기본정보</asp:LinkButton></li>
			<!--<li runat="server" id="tabm2"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="2" >후원자</asp:LinkButton></li>-->
			
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
			
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert">×</button>
					<h4><i class="icon fa fa-check"></i>Alert!</h4>
					검색키워드 (optional) : 검색엔진에서 페이지를 검색하는데 사용되는 키워드입니다.<br />
					
				</div>
					
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label">나눔펀딩에서 사용</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<input type="checkbox" runat="server" id="sf_is_uf" />
						</div>
					</div>
					<div class="form-group" style="display:none">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<input type="checkbox" runat="server" id="sf_display" />
						</div>
					</div>
					<div class="form-group" style="display:none">
						<label class="col-sm-2 control-label">기간</label>
						<div class="col-sm-10">
							<input type="text" class="date form-control inline" id="startDate" runat=server style="width:100px" /> ~
							<input type="text" class="date form-control inline" id="endDate" runat=server style="width:100px" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label ">분류</label>
						<div class="col-sm-2" style="width:600px">
							<div style="width:600px;display:inline-block">
								<asp:DropDownList runat="server" ID="accountClassGroup" EnableViewState="true" CssClass="form-control" style="display:inline-block;width:145px"></asp:DropDownList>

								<select id="accountClass" class="form-control" style="display:inline-block;width:250px">
									<option value="">선택하세요</option>
								</select>
								
							</div>	
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label ">캠페인선택</label>
						<div class="col-sm-2" style="width:600px">
							<div style="width:400px;display:inline-block">
								<asp:DropDownList runat="server" ID="campaignId" EnableViewState="true" CssClass="form-control"></asp:DropDownList>
							</div>	
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">캠페인명</label>
						<div class="col-sm-2" style="width:600px">
							<input type="text" class="form-control" id="campaignName" runat=server style="width:400px" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">목표금액</label>
						<div class="col-sm-10">
							<input type="text" class="form-control number_only" id="sf_goal_amount" runat=server style="width:200px" />
						</div>
					</div>
					<div class="form-group" >
						<label class="col-sm-2 control-label">현재금액</label>
						<div class="col-sm-10">
							<input type="text" class="form-control number_only" id="sf_current_amount" value="0" readonly="true" runat=server style="width:200px" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">결제사용</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<input type="checkbox" runat="server" id="sf_is_regular" /> 정기결제
							<input type="checkbox" runat="server" id="sf_is_temporary" /> 일시결제
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">썸네일</label>
						<div class="col-sm-10">

							<div style="display:inline-block;float:left;width:300px">

								<div style="min-height:150px">
								<img style="max-width:100px" id="thumb" src="/@mgt/common/img/empty_square.png" runat=server />
								</div>
								<div style="display:inline-block;float:left;vertical-align:top;width:200px">
									<span style="font-weight:bold">PC</span>
									<br />
									권장사이즈 : 470px * 331px
									<br />
									용량 : 200kb이하
									<br />
									파일포맷 : png, jpg(jpeg)
									<br />
									파일명 : <span id="thumb_msg"><asp:Literal runat="server" ID="thumb_msg"></asp:Literal></span>
								</div>

							</div>
							
							<div style="display:inline-block;float:left;width:300px">

								<div style="min-height:150px">
								<img style="max-width:100px" id="thumb_m" src="/@mgt/common/img/empty_square.png" runat=server />
								</div>
								<div style="display:inline-block;float:left;vertical-align:top;width:200px">
									<span style="font-weight:bold">모바일</span>
									<br />
									권장사이즈 : 393px * 156px
									<br />
									용량 : 200kb이하
									<br />
									파일포맷 : png, jpg(jpeg)
									<br />
									파일명 :  <span id="thumb_m_msg"><asp:Literal runat="server" ID="thumb_m_msg"></asp:Literal></span>
								</div>

							</div>

						</div>

					</div>

                    <!-- 사용안함-->
					<div class="form-group" style="display:none;">
						<label class="col-sm-2 control-label">상세이미지</label>
						<div class="col-sm-10">

							<div style="display:inline-block;float:left;width:300px">

								<div style="min-height:150px">
								<img style="max-width:100px" id="image" src="/@mgt/common/img/empty_square.png" runat=server />
								</div>
								<div style="display:inline-block;float:left;vertical-align:top;width:200px">
									<span style="font-weight:bold">PC</span>
									<br />
									권장사이즈 : 250px * 250px
									<br />
									용량 : 500kb이하
									<br />
									파일포맷 : png, jpg(jpeg)
									<br />
									파일명 : <span id="image_msg"><asp:Literal runat="server" ID="image_msg"></asp:Literal></span>
								</div>

							</div>
							
							<div style="display:inline-block;float:left;width:300px">

								<div style="min-height:150px">
								<img style="max-width:100px" id="image_m" src="/@mgt/common/img/empty_square.png" runat=server />
								</div>
								<div style="display:inline-block;float:left;vertical-align:top;width:200px">
									<span style="font-weight:bold">모바일</span>
									<br />
									권장사이즈 : 250px * 250px
									<br />
									용량 : 500kb이하
									<br />
									파일포맷 : png, jpg(jpeg)
									<br />
									파일명 : <span id="image_m_msg"><asp:Literal runat="server" ID="image_m_msg"></asp:Literal></span>
								</div>

							</div>

						</div>

					</div>
					
					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">요약</label>
						<div class="col-sm-10">
							<textarea runat="server" id="sf_summary" name="sf_summary" style="width:300px;height:70px"></textarea>
						</div>
					</div>
					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">내용(웹)<!--<br />(나눔펀딩 본문 노출 내용)--></label>
						<div class="col-sm-10">
							<textarea name="sf_content" id="sf_content" runat="server" style="width:800px; height:500px; display:none;"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용(모바일)</label>
						<div class="col-sm-10">
							<textarea name="sf_content_m" id="sf_content_m" runat="server" style="width:800px; height:500px; display:none;"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">검색키워드 (optional)</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="sf_meta_keyword" runat=server style="width:500px" /> * , 구분자로 입력
						</div>
					</div>
					<div class="form-group"  style="display:none">
						<label class="col-sm-2 control-label">노출순서</label>
						<div class="col-sm-10">
							<input type="text" class="form-control number_only" id="sf_order" runat=server style="width:100px" value="100000" /> 
						</div>
					</div>
				</div>



				<div class="box-footer clearfix text-center">
					<asp:LinkButton runat=server ID="btn_update" OnClick="btn_update_Click" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
				</div>

			</div>

			<div class="tab-pane" id="tab2" runat="server" >

				

			</div>

			

			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->


	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID="btn_remove" OnClick="btn_remove_Click" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>

</asp:Content>