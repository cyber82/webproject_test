﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Web.UI.HtmlControls;

public partial class mgt_sponsor_special_fixed_update : AdminBasePage {

	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];


		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();
		upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_shop);

		// 후원종류 , CIV , CSP , PBCIV(2depth) 등
		var action = new CodeAction().SponsorTypes();
		if(!action.success) {
			base.AlertWithJavascript(action.message , "goBack()");
			return;
		}
		hd_sponsor_types.Value = action.data.ToJson();
		accountClassGroup.Items.Add(new ListItem("선택하세요", ""));
		foreach(var item in ((List<CodeAction.SponsorTypeEntity>)action.data).Select(p=>p.groupType).Distinct()) {
			accountClassGroup.Items.Add(new ListItem(string.Format("{0}", item), item));
		}
		
		// 캠페인목록
		action = new SpecialFundingAction().GetCandidateList();
		if(!action.success) {
			base.AlertWithJavascript(action.message, "goBack()");
			return;
		}

		campaignId.Items.Add(new ListItem("선택하세요", ""));
		foreach(var item in (List<tSpecialFundingEx>)action.data) {
			campaignId.Items.Add(new ListItem(string.Format("{0}" , item.CampaignName ), item.CampaignID));
		}
		
		hd_campaigns.Value = action.data.ToJson();
		
		if(base.Action == "update") {
            using (AdminDataContext dao = new AdminDataContext())
            {
                campaignId.Enabled = false;

                //var entity = dao.tSpecialFunding.First(p => p.sf_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<tSpecialFunding>("sf_id", Convert.ToInt32(PrimaryKey));

                hd_accountClass.Value = entity.AccountClass;
                accountClassGroup.SelectedValue = entity.AccountClassGroup;
                campaignId.SelectedValue = entity.CampaignID;
                campaignName.Value = entity.CampaignName;
                if (entity.StartDate.HasValue)
                    startDate.Value = entity.StartDate.Value.ToString("yyyy-MM-dd");
                if (entity.EndDate.HasValue)
                    endDate.Value = entity.EndDate.Value.ToString("yyyy-MM-dd");
                sf_summary.Value = entity.sf_summary;
                sf_content.Value = entity.sf_content;
                sf_content_m.Value = entity.sf_content_m;
                sf_goal_amount.Value = entity.sf_goal_amount.ToString();
                sf_current_amount.Value = entity.sf_current_amount.ToString();
                sf_is_regular.Checked = entity.sf_is_regular;
                sf_is_temporary.Checked = entity.sf_is_temporary;
                sf_display.Checked = entity.sf_display;
                sf_meta_keyword.Value = entity.sf_meta_keyword;
                sf_is_uf.Checked = entity.sf_is_uf;

                sf_thumb.Value = entity.sf_thumb;
                sf_thumb_m.Value = entity.sf_thumb_m;
                sf_image.Value = entity.sf_image;
                sf_image_m.Value = entity.sf_image_m;
                sf_order.Value = entity.sf_order.ToString();

                if (!string.IsNullOrEmpty(entity.sf_thumb))
                {
                    thumb.Src = entity.sf_thumb.WithFileServerHost();
                    thumb_m.Src = entity.sf_thumb_m.WithFileServerHost();
                    image.Src = entity.sf_image.WithFileServerHost();
                    image_m.Src = entity.sf_image_m.WithFileServerHost();

                    thumb_msg.Text = entity.sf_thumb.EmptyIfNull().Replace(upload_root.Value, "");
                    thumb_m_msg.Text = entity.sf_thumb_m.EmptyIfNull().Replace(upload_root.Value, "");
                    image_msg.Text = entity.sf_image.EmptyIfNull().Replace(upload_root.Value, "");
                    image_m_msg.Text = entity.sf_image_m.EmptyIfNull().Replace(upload_root.Value, "");
                }
            }
		} else {

			tabm2.Visible = false;

		}

	}

	protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}
	
	protected void btn_update_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {
            if (base.Action == "update")
            {
                //var entity = dao.tSpecialFunding.First(p => p.sf_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<tSpecialFunding>("sf_id", Convert.ToInt32(PrimaryKey));

                entity.sf_a_id = AdminLoginSession.GetCookie(this.Context).identifier;
                entity.sf_content = sf_content.Value;
                entity.sf_content_m = sf_content_m.Value;
                //		entity.sf_display = sf_display.Checked;
                entity.sf_goal_amount = Convert.ToInt64(sf_goal_amount.Value);
                entity.sf_image = sf_image.Value;
                entity.sf_image_m = sf_image_m.Value;
                entity.sf_is_regular = sf_is_regular.Checked;
                entity.sf_is_temporary = sf_is_temporary.Checked;
                entity.sf_meta_keyword = sf_meta_keyword.Value;
                //		entity.sf_order = Convert.ToInt32(sf_order.Value);
                entity.sf_summary = sf_summary.Value;
                entity.sf_thumb = sf_thumb.Value;
                entity.sf_thumb_m = sf_thumb_m.Value;
                entity.AccountClass = hd_accountClass.Value;
                entity.AccountClassGroup = accountClassGroup.SelectedValue;
                //	entity.CampaignID = campaignId.SelectedValue;
                entity.CampaignName = campaignName.Value;
                entity.sf_is_fixed = true;
                entity.sf_is_uf = sf_is_uf.Checked;
                if (string.IsNullOrEmpty(startDate.Value))
                    entity.StartDate = null;
                else
                    entity.StartDate = Convert.ToDateTime(startDate.Value);
                if (string.IsNullOrEmpty(startDate.Value))
                    entity.EndDate = null;
                else
                    entity.EndDate = Convert.ToDateTime(endDate.Value + " 23:59:59");

                //dao.SubmitChanges();
                //string wClause = string.Format("sf_id = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);

                base.AlertWithJavascript("수정되었습니다.", "goList()");

            }
            else
            {
                var exist = www6.selectQ<tSpecialFunding>("CampaignID", campaignId.SelectedValue);

                //if (dao.tSpecialFunding.Any(p => p.CampaignID == campaignId.SelectedValue))
                if(exist.Any())
                {
                    base.AlertWithJavascript("이미 사용중인 캠페인입니다.");
                    return;
                }

                var entity = new tSpecialFunding()
                {
                    sf_a_id = AdminLoginSession.GetCookie(this.Context).identifier,
                    sf_display = true, //sf_goal_amount = 0 ,
                    sf_order = 0,
                    sf_current_amount = 0,
                    sf_content = sf_content.Value,
                    sf_content_m = sf_content_m.Value,
                    //		sf_display = sf_display.Checked,
                    sf_goal_amount = Convert.ToInt64(sf_goal_amount.Value),
                    sf_image = sf_image.Value,
                    sf_image_m = sf_image_m.Value,
                    sf_is_regular = sf_is_regular.Checked,
                    sf_is_temporary = sf_is_temporary.Checked,
                    sf_meta_keyword = sf_meta_keyword.Value,
                    //		sf_order = Convert.ToInt32(sf_order.Value),
                    sf_summary = sf_summary.Value,
                    sf_thumb = sf_thumb.Value,
                    sf_thumb_m = sf_thumb_m.Value,
                    sf_regdate = DateTime.Now,
                    AccountClass = hd_accountClass.Value,
                    AccountClassGroup = accountClassGroup.SelectedValue,
                    CampaignID = campaignId.SelectedValue,
                    CampaignName = campaignName.Value,
                    //		sf_current_amount = Convert.ToInt64(sf_current_amount.Value),
                    sf_deleted = false,
                    sf_is_fixed = true,
                    sf_is_uf = sf_is_uf.Checked
                };

                /*
				if(string.IsNullOrEmpty(startDate.Value))
					entity.StartDate = null;
				else
					entity.StartDate = Convert.ToDateTime(startDate.Value);
				if(string.IsNullOrEmpty(startDate.Value))
					entity.EndDate = null;
				else
					entity.EndDate = Convert.ToDateTime(endDate.Value + " 23:59:59");
					*/

                //dao.tSpecialFunding.InsertOnSubmit(entity);
                www6.insert(entity);

                //dao.SubmitChanges();

                base.AlertWithJavascript("등록되었습니다.", "goList()");

                this.PrimaryKey = entity.sf_id.ToString();
                tabm2.Visible = true;
            }
        }
		
	}

	protected void btn_remove_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

        using (FrontDataContext dao = new FrontDataContext())
        {
            if (base.Action == "update")
            {
                //var entity = dao.tSpecialFunding.First(p => p.sf_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<tSpecialFunding>("sf_id", Convert.ToInt32(PrimaryKey));
                entity.sf_deleted = true;

                //dao.SubmitChanges();
                //string wClause = string.Format("sf_id = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);

                base.AlertWithJavascript("삭제되었습니다.", "goList()");
            }

        }


	}
	
	protected void btn_section_Click( object sender, EventArgs e ) {
		LinkButton obj = sender as LinkButton;
		var no = obj.CommandArgument;

		int tabCount = 2;

		for(int i = 1; i < tabCount + 1; i++) {

			if(no == i.ToString()) {
				((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "active";
				((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "active tab-pane";
			} else {
				((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "";
				((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "tab-pane";

			}

			if(no == "2") {
				
			}


		}

	}
	
}