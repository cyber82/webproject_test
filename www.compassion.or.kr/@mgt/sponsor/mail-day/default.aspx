﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_mainpage_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
		$(function () {
			$page.init();

		});

		$page = {

			init: function () {
				$(".btn_modify").click(function () {
					$page.changeBtn("show", $(this))
				})
				$("#btm_control_cancel").click(function () {
					$page.changeBtn("hide", '')
				})

			},

			changeBtn: function (flag, obj) {
				if (obj != "") {
					num = obj.data("id")
					date = obj.parent().parent().find(".visitDate").text();
					order = obj.parent().parent().find(".visitOrder").text();
				}

				if (flag == "show") {
					$("#add_idx").val(num);
					$("#add_date").val(date);
					$("#add_visitOrder").val(order);

					$("#btn_add").hide();
					$(".control_btn").show();
				} else {
					$("#add_idx").val("");
					$("#add_date").val("");
					$("#add_visitOrder").val("");

					$(".control_btn").hide();
					$("#btn_add").show();
				}
			}

		};

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	<input type="hidden" runat="server" id="sortColumn" value="mp_order" />
	<input type="hidden" runat="server" id="sortDirection" value="asc" />
	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>

		<div class="form-horizontal box-body" >

			<div class="form-group">
				<label for="s_date" class="col-sm-2 control-label">날짜 선택</label>
				<div class="col-sm-10">
					<asp:TextBox runat="server" ID="s_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
				</div>
			</div>
		
			<div class="box-footer clearfix text-center">
                <asp:LinkButton runat="server" ID="LinkButton1" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
			</div>

		</div>
	</div>

	<br />

    <div class="box box-primary">
		<div class="box-header">
            <h3 class="box-title">총 <strong><asp:Literal runat="server" ID="lbTotal"></asp:Literal></strong> 건</h3><br />
		</div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
			<div class="box-header with-border section-search">
				<div class="form-inline">
					<asp:TextBox runat="server" ID="add_idx" Width="150px" CssClass="date form-control inline" Style="display:none;"></asp:TextBox>
					<div class="input-group" style="width: 230px">

						<input type="text" runat="server" ID="add_date" class="form-control inline date" style="margin:0 5px; width:200px;" placeholder="일자 등록" />&nbsp&nbsp&nbsp

					</div>

					<asp:LinkButton runat="server" ID="btn_add" CssClass="btn btn-primary" Style="width:100px;" OnClick="btn_add_Click" >추가</asp:LinkButton>
					<asp:LinkButton runat="server" ID="btn_control_modify" CssClass="btn btn-primary control_btn" Style="width:100px; display:none;" OnClick="btn_control_modify_Click">수정</asp:LinkButton>
					<a id="btm_control_cancel" class="btn btn-primary control_btn" style="width:100px; display:none;">취소</a>
				</div>
			</div>




			<table class="table table-hover table-bordered ">
				<colgroup>
					<col width="10%" />
					<col width="35%" />
					<col width="35%" />
					<col width="20%" />
				</colgroup>
				<thead>
					<tr>
						<th>번호</th>
						<th>편지쓰는날</th>
						<th>등록일</th>
						<th>버튼</th>
					</tr>
				</thead>
				<tbody>
				
					<asp:Repeater runat="server" ID="repeater" OnItemDataBound=ListBound>
						<ItemTemplate>
							<tr class="tr_link">
								<td><asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
								<td class="visitDate"><%#Eval("ld_date" , "{0:yy/MM/dd}") %></td>
								<td><%#Eval("ld_regdate" , "{0:yy/MM/dd}") %></td>
								<td>
									<a class="btn btn-primary btn-sm btn_modify" data-id="<%#Eval("ld_idx").ToString()%> ">수정하기</a>
									<asp:LinkButton runat="server" ID="btn_delete" CssClass="btn btn-danger btn-sm" CommandName=<%#Eval("ld_idx").ToString()%>  OnClick="btn_delete_Click">삭제하기</asp:LinkButton>
								</td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								<td colspan="6">데이터가 없습니다.</td>
							</tr>
						</FooterTemplate>
					</asp:Repeater>

				</tbody>
			</table>
		</div>


		<div class="box-footer clearfix text-center">
			<uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
			
		</div>

		<!--	
		<div class="box-footer clearfix text-center">
		</div>
		-->

	</div>

</asp:Content>