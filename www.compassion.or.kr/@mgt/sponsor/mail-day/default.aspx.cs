﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_mainpage_default : AdminBasePage {

	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;

	}

	protected override void OnAfterPostBack(){

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page){

		add_idx.Text = "";
		add_date.Value = "";


        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.sp_tLetterDate_list(page, paging.RowsPerPage, s_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "startdate"};
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_date.Text };
            var list = www6.selectSP("sp_tLetterDate_list", op1, op2).DataTableToList<sp_tLetterDate_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e){
		if (repeater != null){
			if (e.Item.ItemType != ListItemType.Footer){
		
				sp_tDolVisit_listResult entity = e.Item.DataItem as sp_tDolVisit_listResult;
				((Literal)e.Item.FindControl( "lbIdx" )).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();

			}
		}
	}

	protected void btn_add_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}



		var date = "";
		if (add_date.Value != "") { date = add_date.Value; } else { Response.Write( "<script>alert('일자를 입력하세요')</script>" ); return; }



        using (AdminDataContext dao = new AdminDataContext())
        {
            var entity = new tLetterDate() {   ld_date = Convert.ToDateTime(date)
                                             , ld_regdate = DateTime.Now
                                             , ld_a_id = AdminLoginSession.GetCookie(this.Context).identifier };

            //dao.tLetterDate.InsertOnSubmit(entity);
            www6.insert(entity);

            //dao.SubmitChanges();

        }
		this.GetList(1);

	}

	protected void btn_control_modify_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}

		var idx = Convert.ToInt32( add_idx.Text );


		var date = "";
		if (add_date.Value != "") { date = add_date.Value; } else { Response.Write( "<script>alert('일자를 입력하세요')</script>" ); return; }


        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tLetterDate.First(p => p.ld_idx == idx);
            var entity = www6.selectQF<tLetterDate>("ld_idx", idx);

            entity.ld_date = Convert.ToDateTime(date);
            entity.ld_regdate = DateTime.Now;
            entity.ld_a_id = AdminLoginSession.GetCookie(this.Context).identifier;

            //dao.SubmitChanges();
            //string wClause = string.Format("ld_idx = {0}", idx);
            www6.update(entity);

            base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

        }

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "수정되었습니다.";

		this.GetList( paging.CurrentPage );

	}

	protected void btn_delete_Click(object sender, EventArgs e){

		if (base.IsRefresh)
		{
			return;
		}

		LinkButton btn = sender as LinkButton;
		var idx = Convert.ToInt32(btn.CommandName);

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tLetterDate.First(p => p.ld_idx == idx);
            //dao.tLetterDate.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from tLetterDate where ld_idx = {0}", idx);
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "삭제되었습니다.";

		this.GetList( 1 );
	}
}
