﻿$(function () {

    $page.init();

});

var $page = {

    init: function () {
        $("#btn_remove").click(function () {
            return confirm("삭제하시겠습니까?");
        });

        $("ul.nav-tabs li").each(function (i) {
            if ($(this).hasClass("active")) {
                eval('$page.tab' + (i + 1) + '.init()');
            }
        })

    },

    tab1: {

        sponsor_types: null,
        campaigns: null,
        init: function () {

            $page.tab1.sponsor_types = $.parseJSON($("#hd_sponsor_types").val());
            $page.tab1.campaigns = $.parseJSON($("#hd_campaigns").val());

            var uploader_thumb = $page.tab1.attachUploader("thumb");
            var uploader_thumb_m = $page.tab1.attachUploader("thumb_m");
            var uploader_image = $page.tab1.attachUploader("image");
            var uploader_image_m = $page.tab1.attachUploader("image_m");

            uploader_thumb._settings.data.rename = uploader_thumb_m._settings.data.rename = uploader_image._settings.data.rename = uploader_image_m._settings.data.rename = "y";
            uploader_thumb._settings.data.fileType = uploader_thumb_m._settings.data.fileType = uploader_image._settings.data.fileType = uploader_image_m._settings.data.fileType = "image";
            uploader_thumb._settings.data.limit = uploader_thumb_m._settings.data.limit = 200;
            uploader_image._settings.data.limit = uploader_image_m._settings.data.limit = 500;
            image_path = uploader_thumb._settings.data.fileDir = uploader_thumb_m._settings.data.fileDir = uploader_image._settings.data.fileDir = uploader_image_m._settings.data.fileDir = $("#upload_root").val();

            initEditor(oEditors, "sf_content");
            initEditor(oEditors, "sf_content_m");

            $("#btn_update").click(function () {
                return $page.tab1.onSubmit();
            });

            $page.tab1.onChangeGroup();
            $("#accountClass").val($("#hd_accountClass").val());

            $("#accountClassGroup").change(function () {
                $page.tab1.onChangeGroup();
            })

            $("#campaignId").change(function () {
                $page.tab1.onChangeCampaign();
            })

            $("#startDate").dateRange({
                end: "#endDate",
                onClick: function () {

                }
            });

            $("#startDate").dateValidate({
                end: "#endDate",
                onSelect: function () {

                }
            });
        },

        attachUploader: function (button) {
            return new AjaxUpload(button, {
                action: '/common/handler/upload',
                responseType: 'json',
                onChange: function () {
                },
                onSubmit: function (file, ext) {
                    this.disable();
                },
                onComplete: function (file, response) {

                    this.enable();

                    if (response.success) {

                        var path = response.name;

                        $("#" + button).attr("src", path);
                        $("#" + button + "_msg").html(path.replace($("#upload_root").val(), ""));
                        $("#sf_" + button).val(path);

                    } else
                        alert(response.msg);
                }
            });
        },

        onChangeCampaign: function () {

            var campaignId = $("#campaignId").val();
            $.get("/api/special-funding.ashx", { t: "get-total-amount", campaignId: campaignId }, function (r) {

                if (r.success) {
                    $("#sf_current_amount").val(r.data);
                } else {
                    alert(r.message);
                }


            });

            if (confirm("선택한 캠페인의 기본정보를 사용하시겠습니까? \n(목표금액 , 캠페인명 , 요약 , 기간)")) {

                var entity = $.grep($page.tab1.campaigns, function (r) {
                    return r.CampaignID == campaignId;
                })[0];

                $("#sf_goal_amount").val(entity.sf_goal_amount);
                $("#campaignName").val(entity.CampaignName);
                $("#sf_summary").val(entity.sf_summary);
                $("#startDate").val(entity.StartDate.substr(0, 10));
                $("#endDate").val(entity.EndDate.substr(0, 10));

            };

        },

        onChangeGroup: function () {

            var group = $("#accountClassGroup").val();
            $("#accountClass").empty();
            $("#accountClass").append($("<option value=''>선택하세요</option>"));
            if (group != "") {
                var list = $.grep($page.tab1.sponsor_types, function (r) {
                    return r.groupType == group;
                });

                $.each(list, function (r) {
                    $("#accountClass").append($("<option value='" + this.codeId + "'>" + this.codeId + " (" + this.codeName + ")</option>"));

                });
            }

        },

        onSubmit: function () {

            if (!validateForm([
				{ id: "#accountClassGroup", msg: "대분류를 선택하세요" },
				{ id: "#accountClass", msg: "소분류를 선택하세요" },
				{ id: "#campaignId", msg: "캠페인을 선택하세요" },
				{ id: "#campaignName", msg: "캠페인명을 입력하세요" },
				{ id: "#sf_goal_amount", msg: "목표금액을 입력하세요" },
				{ id: "#sf_thumb", msg: "썸네일을 선택하세요" },
				{ id: "#sf_thumb_m", msg: "썸네일(모바일)을 선택하세요" },
				{ id: "#sf_image", msg: "상세이미지를 선택하세요" },
				{ id: "#sf_image_m", msg: "상세이미지(모바일)를 선택하세요" },
				{ id: "#sf_summary", msg: "요약내용을 입력하세요." },
				{ id: "#sf_order", msg: "노출순서를 입력하세요." }

            ])) {
                return false;
            }
            oEditors.getById["sf_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.
            oEditors.getById["sf_content_m"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

            if ($("#sf_content").val() == "") {
                alert("내용(웹)을 입력해주세요");
                return false;
            }

            if ($("#sf_content_m").val() == "") {
                alert("내용(모바일)을 입력해주세요");
                return false;
            }

            if (!$("#sf_is_regular").prop("checked") && !$("#sf_is_temporary").prop("checked")) {
                alert("정기결제 , 일시결제중에 하나 이상 선택해주세요.");
                return false;
            }

            $("#hd_accountClass").val($("#accountClass").val());

            if ($('#accountClass').val() == 'BG' || $('#accountClass').val() == 'CF' || $('#accountClass').val() == 'PG' || $('#accountClass').val() == 'FG' || $('#accountClass').val() == 'GG') {
                if ($("#sf_is_regular").prop("checked")) {
                    alert($('#accountClass').val() + "는 정기결제를 선택할 수 없습니다.");
                    return false;
                }
            }

            return confirm("등록 하시겠습니까?");
        }

        // 

    },

    tab2: {
        init: function () {



        },

        onSubmit: function () {



            return true;
        }

    }

}
