﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_about_us_report_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		base.BoardType = "storeNotice";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);

		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();


        // DropDownList 데이터 추가 
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var CampaignList = dao.tSpecialFunding.Where(p => p.sf_deleted == false).ToList();
            var CampaignList = www6.selectQ<tSpecialFunding>("sf_deleted", 0);
            foreach (var a in CampaignList)
            {
                CampaignID.Items.Add(new ListItem(a.CampaignName, a.AccountClassGroup + "_" + a.AccountClass + "_" + a.CampaignID));
            };

            CampaignID.Items.Insert(0, new ListItem("선택하세요", ""));
        }

		if (base.Action == "update"){
            using (AdminDataContext dao = new AdminDataContext())
            {
                //var row = dao.tSpecialFundingReport.First(p => p.sr_id == Convert.ToInt32(PrimaryKey));
                var row = www6.selectQF<tSpecialFundingReport>("sr_id", Convert.ToInt32(PrimaryKey));

                CampaignID.SelectedValue = row.AccountClassGroup + "_" + row.AccountClass + "_" + row.CampaignID;

                title.Text = row.sr_title;

                lb_filename.Text = row.sr_file;
                lnFileName.NavigateUrl = "/files/board/" + row.sr_file;
                display.SelectedValue = row.sr_display.ToString();
                //Response.Write( row.sr_display.ToString() );

            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
		}
		else {
			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {
		if (base.IsRefresh){
			return;
		}


		AdminEntity adm = AdminLoginSession.GetCookie( this.Context );

		var selectData = CampaignID.SelectedValue.Split( '_' );

		var arg = new tSpecialFundingReport() {
			AccountClassGroup = selectData[0],
			AccountClass = selectData[1],
			CampaignID = selectData[2],
			sr_title = title.Text,
			sr_a_id = adm.identifier,
			sr_file = System.IO.Path.GetFileName( temp_file_name.Value ),
			sr_regdate = DateTime.Now,
			sr_display = Convert.ToBoolean(display.SelectedValue )
		};

        using (AdminDataContext dao = new AdminDataContext())
        {
            if (base.Action == "update")
            {
                //var entity = dao.tSpecialFundingReport.First(p => p.sr_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<tSpecialFundingReport>("sr_id", Convert.ToInt32(PrimaryKey));

                entity.AccountClassGroup = arg.AccountClassGroup;
                entity.AccountClass = arg.AccountClass;
                entity.CampaignID = arg.CampaignID;
                entity.sr_title = arg.sr_title;
                entity.sr_a_id = arg.sr_a_id;
                entity.sr_display = arg.sr_display;
                if (arg.sr_file != "")
                {
                    entity.sr_file = arg.sr_file;
                }

                //dao.SubmitChanges();
                //string wClause = string.Format("sr_id = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";

            }
            else
            {

                // 등록
                //dao.tSpecialFundingReport.InsertOnSubmit(arg);
                www6.insert(arg);
                //dao.SubmitChanges();

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";

            }
        }
		

	}

	protected void btn_remove_Click(object sender, EventArgs e){
		if (base.IsRefresh){
			return;
		}
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tSpecialFundingReport.First(p => p.sr_id == Convert.ToInt32(PrimaryKey));
            //dao.tSpecialFundingReport.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from tSpecialFundingReport where sr_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "삭제되었습니다.";
        }

	}

	protected void btn_remove_file_click(object sender, EventArgs e)
	{
		base.FileRemove( sender );
		//this.ShowFiles();
	}

	void ShowFiles() {
		if(!string.IsNullOrEmpty(base.Thumb)) {
			lb_filename.Text = System.IO.Path.GetFileName(base.Thumb);
			
		}
	}
	
	protected void btn_update_temp_file_click( object sender, EventArgs e ) {
		base.Thumb = temp_file_name.Value;
		this.ShowFiles();

	}

	
}