﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_about_us_report_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        #b_type label {padding-right:10px;}
    </style>
	<script type="text/javascript">

		$(function () {

			// pdf
			var uploader = attachUploader("btn_upload");
			uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_special_funding)%>";
		    uploader._settings.data.rename = "y";

		});

	    var onSubmit = function () {
			
			if (!validateForm([
				{ id: "#title", msg: "제목을 입력하세요" },
				{ id: "#CampaignID", msg: "캠페인을 선택하세요" }
			])) {
				return false;
			}
			oEditors.getById["b_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
				
			}

		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass="btn_update_temp_file" OnClick="btn_update_temp_file_click"></asp:LinkButton>

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">


					<div class="form-group">
						<label class="col-sm-2 control-label">제목</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=title CssClass="form-control" Width=600 MaxLength="50"></asp:TextBox>
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-sm-2 control-label">노출 여부 </label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:RadioButtonList runat="server" ID="display" RepeatDirection="Horizontal" RepeatLayout="Flow" >
								<asp:ListItem Value="True" Text="노출&nbsp;&nbsp;&nbsp;" Selected="True"></asp:ListItem>
								<asp:ListItem Value="False" Text="비노출&nbsp;&nbsp;&nbsp;"></asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>


					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">캠페인 명</label>
						<div class="col-sm-10">
							<asp:DropDownList runat="server" ID="CampaignID" class="form-control" Width="300px"></asp:DropDownList>
						</div>
					</div>

					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">첨부파일</label>
						<div class="col-sm-10">
                            <button type="button" class="btn btn-default btn-xs" id="btn_upload" style="margin-top:5px;margin-bottom:2px;">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 파일선택
                            </button> 
							 <asp:HyperLink runat=server ID=lnFileName><asp:Literal runat="server" ID="lb_filename"/></asp:HyperLink>
						</div>
					</div>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove  OnClientClick="return onRemove();" OnClick="btn_remove_Click" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update  OnClientClick="return onSubmit();" OnClick="btn_update_click"  class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>