﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_mainpage_default : AdminBasePage {

	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;

	}

	protected override void OnAfterPostBack(){
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page){
		Master.IsSearch = false;
		if (s_gubun.SelectedValue != "" || s_region.SelectedValue != "" || s_pay.SelectedValue != "" || s_cancle.SelectedValue != "" || s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != ""){
			Master.IsSearch = true;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.sp_tCspDol_list(page, paging.RowsPerPage, s_gubun.SelectedValue, s_region.SelectedValue, s_pay.SelectedValue, s_cancle.SelectedValue, s_visitdate.Text, s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "gubun", "region", "PaymentYN", "CancelYN", "visitdate", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_gubun.SelectedValue, s_region.SelectedValue, s_pay.SelectedValue, s_cancle.SelectedValue, s_visitdate.Text, s_keyword.Text, s_b_date.Text, s_e_date.Text }; 
            var list = www6.selectSP("sp_tCspDol_list", op1, op2).DataTableToList<sp_tCspDol_listResult>();
            
            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();



        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e){
		if (repeater != null){
			if (e.Item.ItemType != ListItemType.Footer){
				sp_tCspDol_listResult entity = e.Item.DataItem as sp_tCspDol_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
				

			}
		}
	}



	protected void btn_paymentYN_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

		LinkButton obj = sender as LinkButton;
		var PaymentYN = obj.CommandArgument;
		var idx = Convert.ToInt32(obj.CommandName);

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tCspDol.First(p => p.Idx == idx);
            var entity = www6.selectQF<tCspDol>("Idx", idx);
            entity.PaymentYN = PaymentYN == "Y" ? 'N' : 'Y';
            //dao.SubmitChanges();
            www6.update(entity);

        }

		this.GetList(paging.CurrentPage);

	}


	protected void btn_cancelYN_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}

		LinkButton obj = sender as LinkButton;
        string[] commandValue = obj.CommandArgument.Split(',');

        var CancelYN = commandValue[0];
        var visitYN = commandValue[1];
        var idx = Convert.ToInt32(obj.CommandName);

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tCspDol.First(p => p.Idx == idx);
            var entity = www6.selectQF<tCspDol>("Idx", idx);

            entity.CancelYN = CancelYN == "Y" ? 'N' : 'Y';
            if (!string.IsNullOrEmpty(visitYN))
            {
                //var visit = dao.tDolVisitSelectDate.First(p => p.VisitDate == visitYN);
                var visit = www6.selectQF<tDolVisitSelectDate>("VisitDate", visitYN);

                visit.UseYN = CancelYN == "Y" ? 'Y' : 'N';
                www6.update(visit);
            }
            //dao.SubmitChanges();
            www6.update(entity);
        }

		this.GetList(paging.CurrentPage);

	}

}
