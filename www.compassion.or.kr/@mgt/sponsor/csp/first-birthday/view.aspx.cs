﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Web.UI.HtmlControls;

public partial class mgt_sponsor_csp_first_birthday_view : AdminBasePage {

    protected override void OnBeforePostBack() {

        base.OnBeforePostBack();
        v_admin_auth auth = base.GetPageAuth();
        base.PrimaryKey = Request["c"];

        btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

        this.GetData();

    }

    protected override void OnAfterPostBack() {
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();
    }

    void GetData() {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var item = dao.tCspDol.Where(p => p.Idx == Convert.ToInt16(PrimaryKey)).First();
            var item = www6.selectQF<tCspDol>("Idx", Convert.ToInt16(PrimaryKey));

            gubun.Text = item.Gubun.ToString() == "P" ? "온라인 신청" : "방문 신청";
            sponsorId.Text = item.SponsorID;
            sponsor_name.Text = item.SponsorName;
            pay_amount.Text = item.PayAmount.ToString("N0") + "원";
            email.Text = item.Email;
            tel.Text = item.Tel;
            baby_image.Src = item.BabyImage != "" ? Uploader.GetRoot(Uploader.FileGroup.image_cpsdol) + item.BabyImage : "/@mgt/common/img/1logo.png";
            baby_name.Text = item.BabyName;
            baby_birth.Text = item.BabyBirth;
            baby_gender.Text = item.BabyGender.ToString() == "M" ? "남자" : "여자";

            if (!string.IsNullOrEmpty(item.Zip1))
            {
                view_address.Visible = true;
                address.Text = "(" + item.Zip1 + ")" + item.Address1 + " " + item.Address2;
            }


            if (item.Gubun.ToString() != "P")
            {
                //var visitRegion = dao.tDolVisitSelectDate.Where(p => p.Idx == item.VisitIdx).First();
                var visitRegion = www6.selectQF<tDolVisitSelectDate>("Idx", item.VisitIdx);
                visit_region.Text = visitRegion.Region == "S" ? "서울" : "부산";
            }
            else
            {
                visit_region.Text = "-";
            }

        }

    }

}