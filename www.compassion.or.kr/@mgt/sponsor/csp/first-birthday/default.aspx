﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_mainpage_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
		$(function () {
		});
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	<input type="hidden" runat="server" id="sortColumn" value="mp_order" />
	<input type="hidden" runat="server" id="sortDirection" value="asc" />

	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>
		<div class="form-horizontal box-body" >
			
			<div class="form-group">
				<label for="s_gubun" class="col-sm-2 control-label">신청 구분</label>
				<div class="col-sm-10" style="margin-top:5px;">
					<asp:RadioButtonList runat="server" ID="s_gubun" RepeatDirection="Horizontal" RepeatLayout="Flow" >
						<asp:ListItem Selected="True"  Value="" Text="전체&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="P" Text="온라인&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="V" Text="방문&nbsp;&nbsp;&nbsp;"></asp:ListItem>
					</asp:RadioButtonList>
				</div>
			</div>

			<div class="form-group">
				<label for="s_gubun" class="col-sm-2 control-label">방문 위치</label>
				<div class="col-sm-10" style="margin-top:5px;">
					<asp:RadioButtonList runat="server" ID="s_region" RepeatDirection="Horizontal" RepeatLayout="Flow" >
						<asp:ListItem Selected="True"  Value="" Text="전체&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="S" Text="서울&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="B" Text="부산&nbsp;&nbsp;&nbsp;"></asp:ListItem>
					</asp:RadioButtonList>
				</div>
			</div>
			
			<div class="form-group">
				<label for="s_pay" class="col-sm-2 control-label">결제 여부</label>
				<div class="col-sm-10" style="margin-top:5px;">
					<asp:RadioButtonList runat="server" ID="s_pay" RepeatDirection="Horizontal" RepeatLayout="Flow" >
						<asp:ListItem Selected="True"  Value="" Text="전체&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="Y" Text="결제&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="N" Text="미결제&nbsp;&nbsp;&nbsp;"></asp:ListItem>
					</asp:RadioButtonList>
				</div>
			</div>
					
			<div class="form-group">
				<label for="s_cancle" class="col-sm-2 control-label">취소 여부</label>
				<div class="col-sm-10" style="margin-top:5px;">
					<asp:RadioButtonList runat="server" ID="s_cancle" RepeatDirection="Horizontal" RepeatLayout="Flow" >
						<asp:ListItem Selected="True"  Value="" Text="전체&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="Y" Text="신청 &nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="N" Text="취소 &nbsp;&nbsp;&nbsp;"></asp:ListItem>
					</asp:RadioButtonList>
				</div>
			</div>


			<div class="form-group">
				<label for="s_visitdate" class="col-sm-2 control-label">방문일</label>
				<div class="col-sm-10">
					<asp:TextBox runat="server" ID="s_visitdate" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>

				</div>
			</div>



			<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">기간 검색(등록일)</label>
					<div class="col-sm-10">
						<div class="inline">
							<asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							~
						<asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>

						<div class="inline">
							<button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							<div class="btn-group">
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								<a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								<a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							</div>
						</div>

					</div>

				</div>	
			
				
			<div class="form-group">
				<label for="s_keyword" class="col-sm-2 control-label">검색</label>
				<div class="col-sm-10">
					<asp:TextBox runat="server" ID="s_keyword" Width="415" cssClass="form-control inline"  OnTextChanged="search" AutoPostBack="true"></asp:TextBox>
					* 후원자명, 아기이름
				</div>
			</div>
			
			<div class="box-footer clearfix text-center">
                <asp:LinkButton runat="server" ID="LinkButton1" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
			</div>

		</div>
	</div>

	<br />


    <div class="box box-primary">
		<div class="box-header">
            <h3 class="box-title">총 <strong><asp:Literal runat="server" ID="lbTotal"></asp:Literal></strong> 건</h3>
        </div><!-- /.box-header -->
		<div class="box-body table-responsive no-padding">

			<table class="table table-hover table-bordered ">
				<colgroup>
					<col width="4%" />
				
					<col width="7%" />
				
					<col width="9%" />
					<col width="9%" />
					<col width="10%" />
					<col width="9%" />
					<col width="9%" />
					<col width="9%" />
					<col width="9%" />
					<col width="7%" />
					<col width="7%" />
				</colgroup>
				<thead>
					<tr>
						<th>번호</th>
					
						<th>구분</th>
					
						<th>후원자ID</th>
						<th>신청자명</th>
						<th>아기사진</th>
						<th>아기이름</th>
						<th>후원금액</th>
						<th>방문일</th>
						<th>신청일</th>
						<th>결재여부</th>
						<th>취소여부</th>
					</tr>
				</thead>
				<tbody>
				
					<asp:Repeater runat="server" ID="repeater" OnItemDataBound=ListBound>
						<ItemTemplate>
							<tr class="tr_link" onclick="goPage('view.aspx' , { c: '<%#Eval("idx") %>'})">
								<td><asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
								<td><%#Eval("Gubun") != null && Eval("Gubun").ToString() == "P" ? "온라인 신청" : "방문 신청"  + (Eval("Region") != null && Eval("Region").ToString() != "" ? (Eval("Region").ToString() == "S" ? "(서울)" : "(부산)") : "" )%><%#(Eval("BluedogYN") != null && Eval("BluedogYN").ToString() == "Y" ? "<br/>(블루독)" : "<br/>(컴패션)" )%></td>
								<td><%#Eval("SponsorID") %></td>
								<td><%#Eval("SponsorName") %></td>
								<td><a style="display:<%#Eval("Gubun").ToString() == "P" ? "" : "none"%>" href="<%#Uploader.GetRoot(Uploader.FileGroup.image_cpsdol)  + Eval("BabyImage") %>"><img src="<%#Uploader.GetRoot(Uploader.FileGroup.image_cpsdol)+Eval("BabyImage") %>" width="100"/></a></td>
								<td><%#Eval("BabyName") %></td>
								<td><%#Eval("PayAmount" ,"{0:N0}") %>원</td>
								<td><%#Eval("VisitDate" , "{0:yy/MM/dd}") %></td>
								<td><%#Eval("RegDate" , "{0:yy/MM/dd}") %></td>
								<td>
									<asp:LinkButton runat="server" CssClass="btn btn-primary btn-sm" style="width:102px" ID="btn_paymentYN" CommandName=<%#Eval("idx").ToString()%> CommandArgument=<%#Eval("PaymentYN").ToString()%> OnClick="btn_paymentYN_Click"><%#Eval("PaymentYN").ToString() == "Y" ? "결제완료 [취소]" : "- [결제]"  %></asp:LinkButton></td>
								<td>
									<asp:LinkButton runat="server" CssClass="btn btn-warning btn-sm" style="width:102px" ID="btn_cancelYN" CommandName=<%#Eval("idx").ToString()%> CommandArgument=<%#Eval("CancelYN").ToString() +"," +Eval("VisitDate")%> OnClick="btn_cancelYN_Click"><%#Eval("CancelYN").ToString() == "Y" ? "취소됨 [신청]" : "신청됨 [취소]"  %></asp:LinkButton> 
								</td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								<td colspan="10">데이터가 없습니다.</td>
							</tr>
						</FooterTemplate>
					</asp:Repeater>

				</tbody>
			</table>
		</div>


		<div class="box-footer clearfix text-center">
			<uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
			<!--
				<asp:LinkButton runat="server" href="#" ID="btn_add" OnClientClick="goPage('update.aspx' , {t:'insert'})" CssClass="btn btn-bitbucket pull-right">신규등록</asp:LinkButton>
			-->
		</div>

		<!--	
		<div class="box-footer clearfix text-center">
		</div>
		-->

	</div>

</asp:Content>