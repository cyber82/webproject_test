﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="mgt_sponsor_csp_first_birthday_view" MasterPageFile="~/@mgt/mgt_afterlogin.master" %>

<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        .col-sm-10 {
            padding-top: 7px;
        }
    </style>
    
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <div class="nav-tabs-custom">

        <div class="tab-content">

            <div class="active tab-pane" id="tab1" runat="server">
                <div class="form-horizontal">

                    <div class="col-xs-12">
                        <h2 class="page-header">상세 신청정보</h2>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">구분</label>
                        <div class="col-sm-10">
                            <asp:Literal runat="server" ID="gubun"></asp:Literal>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">방문신청 지사</label>
                        <div class="col-sm-10">
                            <asp:Literal runat="server" ID="visit_region"></asp:Literal>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">후원자ID</label>
                        <div class="col-sm-10">
                            <asp:Literal runat="server" ID="sponsorId"></asp:Literal>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">신청자명</label>
                        <div class="col-sm-10">
                            <asp:Literal runat="server" ID="sponsor_name"></asp:Literal>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">후원금액</label>
                        <div class="col-sm-10">
                            <asp:Literal runat="server" ID="pay_amount"></asp:Literal>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">이메일</label>
                        <div class="col-sm-10">
                            <asp:Literal runat="server" ID="email"></asp:Literal>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">전화번호</label>
                        <div class="col-sm-10">
                            <asp:Literal runat="server" ID="tel"></asp:Literal>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="view_address" visible="false">
                        <label class="col-sm-2 control-label">주소</label>
                        <div class="col-sm-10">
                            <asp:Literal runat="server" ID="address"></asp:Literal>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">아기사진</label>
                        <div class="col-sm-10">
                            <img id="baby_image" runat="server" src="" width="100" alt="등록된 사진이 없습니다."/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">아기이름</label>
                        <div class="col-sm-10">
                            <asp:Literal runat="server" ID="baby_name"></asp:Literal>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">아기성별</label>
                        <div class="col-sm-10">
                            <asp:Literal runat="server" ID="baby_gender"></asp:Literal>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">아기 생년월일</label>
                        <div class="col-sm-10">
                            <asp:Literal runat="server" ID="baby_birth"></asp:Literal>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="box-footer clearfix text-center">

        <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
    </div>

</asp:Content>
