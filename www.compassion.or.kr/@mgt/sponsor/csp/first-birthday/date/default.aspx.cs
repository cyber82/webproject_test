﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_mainpage_default : AdminBasePage {

	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;

	}

	protected override void OnAfterPostBack(){

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page)
    {
		add_idx.Text = "";
		add_visitDate.Value = "";
		add_visitOrder.Value = "";
        
        using (AdminDataContext dao = new AdminDataContext())
        {
            // var list = dao.sp_tDolVisit_list(page, paging.RowsPerPage, s_visitdate.Text, s_region.SelectedValue).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitdate", "region"};
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_visitdate.Text, s_region.SelectedValue };
            var list = www6.selectSP("sp_tDolVisit_list", op1, op2).DataTableToList<sp_tDolVisit_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e){
		if (repeater != null){
			if (e.Item.ItemType != ListItemType.Footer){
		
				sp_tDolVisit_listResult entity = e.Item.DataItem as sp_tDolVisit_listResult;
				((Literal)e.Item.FindControl( "lbIdx" )).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();

			}
		}
	}

	protected void btn_add_visit_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}



		var visitDate = "";
		if (add_visitDate.Value != "") { visitDate = add_visitDate.Value; } else { Response.Write( "<script>alert('방문 일자를 입력하세요')</script>" ); return; }


		var visitOrder = 1;
		if (add_visitOrder.Value != ""){ visitOrder = Convert.ToInt32( add_visitOrder.Value ); }

		var visitRegion = add_visitRegion.SelectedValue;

        using (AdminDataContext dao = new AdminDataContext())
        {
            var entity = new tDolVisitSelectDate() { DeleteYN = 'N', Region = visitRegion, UseYN = 'N', VisitDate = visitDate, VisitOrder = visitOrder };
            //dao.tDolVisitSelectDate.InsertOnSubmit(entity);
            //dao.SubmitChanges();
            //insert
            www6.insert(entity);

        }
		this.GetList(1);

	}

	protected void btn_control_modify_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}

		var idx = Convert.ToInt32( add_idx.Text );
		var visitDate = add_visitDate.Value;
		var visitOrder = 1;

		if (add_visitOrder.Value != ""){
			visitOrder = Convert.ToInt32( add_visitOrder.Value );
		}

		var visitRegion = add_visitRegion.SelectedValue;

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tDolVisitSelectDate.First(p => p.Idx == idx);
            var entity = www6.selectQF<tDolVisitSelectDate>("Idx", idx);

            entity.VisitDate = visitDate;
            entity.VisitOrder = visitOrder;
            entity.Region = visitRegion;
            //dao.SubmitChanges();
            //string wClause = string.Format("Idx = {0}", idx);
            www6.update(entity);

            base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
        }

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "수정되었습니다.";

		this.GetList( paging.CurrentPage );

	}

	protected void btn_delete_Click(object sender, EventArgs e){

		if (base.IsRefresh)
		{
			return;
		}

		LinkButton btn = sender as LinkButton;
		var idx = Convert.ToInt32(btn.CommandName);

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tDolVisitSelectDate.First(p => p.Idx == idx);
            //dao.tDolVisitSelectDate.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from tDolVisitSelectDate where Idx = {0}", idx);
            www6.cud(delStr);
        
            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "삭제되었습니다.";

		this.GetList( 1 );
	}
}
