﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_mainpage_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
		$(function () {
			$page.init();

		});

		$page = {

			init: function () {
				$(".btn_modify").click(function () {
					$page.changeBtn("show", $(this))
				})
				$("#btm_control_cancel").click(function () {
					$page.changeBtn("hide", '')
				})

			},

			changeBtn: function (flag, obj) {
				if (obj != "") {
					num = obj.data("id")
					date = obj.parent().parent().find(".visitDate").text();
					order = obj.parent().parent().find(".visitOrder").text();
				}

				if (flag == "show") {
					$("#add_idx").val(num);
					$("#add_visitDate").val(date);
					$("#add_visitOrder").val(order);

					$("#btn_add_visit").hide();
					$(".control_btn").show();
				} else {
					$("#add_idx").val("");
					$("#add_visitDate").val("");
					$("#add_visitOrder").val("");

					$(".control_btn").hide();
					$("#btn_add_visit").show();
				}
			}

		};

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	<input type="hidden" runat="server" id="sortColumn" value="mp_order" />
	<input type="hidden" runat="server" id="sortDirection" value="asc" />
	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>

		<div class="form-horizontal box-body" >
			
			

			
			<div class="form-group">
				<label for="s_region" class="col-sm-2 control-label">지역</label>
				<div class="col-sm-10" style="margin-top:5px;">
					<asp:RadioButtonList runat="server" ID="s_region" RepeatDirection="Horizontal" RepeatLayout="Flow" >
						<asp:ListItem Selected="True"  Value="" Text="전체&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="S" Text="서울 &nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="B" Text="부산 &nbsp;&nbsp;&nbsp;"></asp:ListItem>
					</asp:RadioButtonList>
				</div>
			</div>

			<div class="form-group" style="display:none">
				<label for="s_visitdate" class="col-sm-2 control-label">방문일</label>
				<div class="col-sm-10">
					<asp:TextBox runat="server" ID="s_visitdate" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>

				</div>
			</div>
		
			<div class="box-footer clearfix text-center">
                <asp:LinkButton runat="server" ID="LinkButton1" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
			</div>

		</div>
	</div>

	<br />

    <div class="box box-primary">
		<div class="box-header">
            <h3 class="box-title">총 <strong><asp:Literal runat="server" ID="lbTotal"></asp:Literal></strong> 건</h3><br />
		</div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
			<div class="box-header with-border section-search">
				<div class="form-inline">
					<asp:TextBox runat="server" ID="add_idx" Width="150px" CssClass="date form-control inline" Style="display:none;"></asp:TextBox>
					<div class="input-group" style="width: 600px">

						<input type="text" runat="server" ID="add_visitDate" class="form-control inline" style="margin:0 5px; width:200px;" placeholder="방문일자" />&nbsp&nbsp&nbsp

						<input type="text" runat="server" ID="add_visitOrder" class="form-control inline number_only" style="margin: 0 10px 0 5px; width:200px;" placeholder="방문일자순서"/>&nbsp&nbsp&nbsp
						
						<div style="margin-top: -10px;">
							<label for="add_visitRegion" class=" control-label">지역 &nbsp;&nbsp;&nbsp;</label> 
							<asp:RadioButtonList runat="server" ID="add_visitRegion" RepeatDirection="Horizontal" RepeatLayout="Flow" >
								<asp:ListItem Value="S" Text=" &nbsp;서울 &nbsp;&nbsp;&nbsp;" Selected="True"></asp:ListItem>
								<asp:ListItem Value="B" Text=" &nbsp;부산 &nbsp;&nbsp;&nbsp;"></asp:ListItem>
							</asp:RadioButtonList>
						</div>

					</div>

					<asp:LinkButton runat="server" ID="btn_add_visit" CssClass="btn btn-primary" Style="width:100px;" OnClick="btn_add_visit_Click" >추가</asp:LinkButton>
					<asp:LinkButton runat="server" ID="btn_control_modify" CssClass="btn btn-primary control_btn" Style="width:100px; display:none;" OnClick="btn_control_modify_Click">수정</asp:LinkButton>
					<a id="btm_control_cancel" class="btn btn-primary control_btn" style="width:100px; display:none;">취소</a>
				</div>
			</div>




			<table class="table table-hover table-bordered ">
				<colgroup>
					<col width="5%" />
					<col width="15%" />
					<col width="10%" />
					<col width="10%" />
					<col width="10%" />
					<col width="15%" />
				</colgroup>
				<thead>
					<tr>
						<th>번호</th>
						<th>방문일자</th>
						<th>방문일자순서</th>
						<th>사용여부</th>
						<th>지역</th>
						<th>버튼</th>
					</tr>
				</thead>
				<tbody>
				
					<asp:Repeater runat="server" ID="repeater" OnItemDataBound=ListBound>
						<ItemTemplate>
							<tr class="tr_link">
								<td><asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
								<td class="visitDate"><%#Eval("VisitDate" , "{0:yy/MM/dd}") %></td>
								<td class="visitOrder"><%#Eval("VisitOrder") %></td>
								<td><%#Eval("UseYN") %></td>
								<td><%#Eval("Region").ToString() == "S" ? "서울" : "부산"%></td>
								<td>
									<a class="btn btn-primary btn-sm btn_modify" data-id="<%#Eval("idx").ToString()%> ">수정하기</a>
									<asp:LinkButton runat="server" ID="btn_delete" CssClass="btn btn-danger btn-sm" CommandName=<%#Eval("idx").ToString()%>  OnClick="btn_delete_Click">삭제하기</asp:LinkButton>
								</td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								<td colspan="6">데이터가 없습니다.</td>
							</tr>
						</FooterTemplate>
					</asp:Repeater>

				</tbody>
			</table>
		</div>


		<div class="box-footer clearfix text-center">
			<uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
			<!--
				<asp:LinkButton runat="server" href="#" ID="btn_add" OnClientClick="goPage('update.aspx' , {t:'insert'})" CssClass="btn btn-bitbucket pull-right">신규등록</asp:LinkButton>
			-->
		</div>

		<!--	
		<div class="box-footer clearfix text-center">
		</div>
		-->

	</div>

</asp:Content>