﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_mainpage_default : AdminBasePage {

	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;

	}

	protected override void OnAfterPostBack(){
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page){
		Master.IsSearch = false;
		if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != ""){
			Master.IsSearch = true;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.sp_tCspWedding_list(page, paging.RowsPerPage, s_gubun.SelectedValue, s_pay.SelectedValue, s_cancle.SelectedValue, s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "gubun", "PaymentYN", "CancelYN", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_gubun.SelectedValue, s_pay.SelectedValue, s_cancle.SelectedValue, s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_tCspWedding_list", op1, op2).DataTableToList<sp_tCspWedding_listResult>().ToList();

            var total = 0;


            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();


        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e){
		if (repeater != null){
			if (e.Item.ItemType != ListItemType.Footer){
				sp_tCspWedding_listResult entity = e.Item.DataItem as sp_tCspWedding_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
			

			}
		}
	}



	protected void btn_paymentYN_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}

		LinkButton obj = sender as LinkButton;
		var PaymentYN = obj.CommandArgument;
		var idx = Convert.ToInt32( obj.CommandName );

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tCspWedding.First(p => p.Idx == idx);
            var entity = www6.selectQF<tCspWedding>("Idx", idx);

            entity.PaymentYN = PaymentYN == "Y" ? 'N' : 'Y';
            //dao.SubmitChanges();
            //string wClause = string.Format("Idx = {0}", idx);
            www6.update(entity);

        }

		this.GetList( paging.CurrentPage );

	}


	protected void btn_cancelYN_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}

		LinkButton obj = sender as LinkButton;
		var CancelYN = obj.CommandArgument;
		var idx = Convert.ToInt32( obj.CommandName );

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tCspWedding.First(p => p.Idx == idx);
            var entity = www6.selectQF<tCspWedding>("Idx", idx);

            entity.CancelYN = CancelYN == "Y" ? 'N' : 'Y';
            //dao.SubmitChanges();
            //string wClause = string.Format("Idx = {0}", idx);
            www6.update(entity);

        }

		this.GetList( paging.CurrentPage );

	}

}
