﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_user_funding_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        #b_type label {padding-right:10px;}
		.col-sm-10 {padding-top:7px;}
    </style>
	<script type="text/javascript">

		$(function () {


		});

		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

		var removeUser = function () {
			if (confirm("삭제하시겠습니까?")) {
				var str = "";
				var length = $(".ur_id:checked").length
				for (var i = 0 ; i < length ; i++) {
					str += $(".ur_id:checked").eq(i).val();
					if (i != length - 1) {
						str += ",";
					}
				}


				if (str == "") {
					alert("삭제할 항목을 선택해 주세요");
				} else {
					$("#delDate").val(str);
					return true;
				}
			}
			return false;
		};

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="1" >기본정보</asp:LinkButton></li>
			<li runat="server" id="tabm2"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="2" >댓글</asp:LinkButton></li>
			<li runat="server" id="tabm3"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="3" >업데이트소식</asp:LinkButton></li>
			<li runat="server" id="tabm4"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="4" >참여자</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server" >
				<div class="form-horizontal">


					<!--  펀딩인 경우  정보 노출 -->
					<asp:PlaceHolder runat="server" ID="ph_child" Visible="false">
						

						<div class="col-xs-12">
							<h2 class="page-header"><i class="fa fa-plus-square"></i> 어린이 정보</h2>
						</div>


						<div class="form-group">
							<label class="col-sm-2 control-label"> 사진 </label>
							<div class="col-sm-10">
								<img runat="server" id="c_img" style="max-width:100px" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label"> 이름 </label>
							<div class="col-sm-10">
								<asp:Literal runat="server" ID="c_name"  />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"> 나이 </label>
							<div class="col-sm-10">
								<asp:Literal runat="server" ID="c_age" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label"> 지역 </label>
							<div class="col-sm-10">
								<asp:Literal runat="server" ID="c_country" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"> 생일 </label>
							<div class="col-sm-10">
								<asp:Literal runat="server" ID="c_birth" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"> 특기 </label>
							<div class="col-sm-10">
							<asp:Literal runat="server" ID="c_favorites" />
							</div>
						</div>

					</asp:PlaceHolder>

					<asp:PlaceHolder runat="server" id="ph_campaign" Visible="false">

						
						<div class="col-xs-12">
							<h2 class="page-header"><i class="fa fa-plus-square"></i> 캠페인 정보</h2>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"> 캠페인ID</label>
							<div class="col-sm-10">
							<asp:Literal runat="server" ID="campaignID" />
							</div>
						</div>
					
						<div class="form-group">
							<label class="col-sm-2 control-label"> 캠페인명</label>
							<div class="col-sm-10">
							<asp:Literal runat="server" ID="CampaignName" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"> 캠페인 이미지</label>
							<div class="col-sm-10">
								<img runat="server" id="sf_image" style="max-width:400px" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label"> 요약</label>
							<div class="col-sm-10">
							<asp:Literal runat="server" ID="sf_summary" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label"> 기간</label>
							<div class="col-sm-10">
							<asp:Literal runat="server" ID="sf_date" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"> 현재금액 / 목표금액 </label>
							<div class="col-sm-10">
							<asp:Literal runat="server" ID="sf_amount" />
							</div>
						</div>

					</asp:PlaceHolder>

					
					<div class="col-xs-12">
						<h2 class="page-header"><i class="fa fa-plus-square"></i> 등록 정보</h2>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label"> 제목</label>
						<div class="col-sm-10">
							<asp:Literal runat="server" ID="uf_title" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"> 요약</label>
						<div class="col-sm-10">
							<asp:Literal runat="server" ID="uf_summary" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">메인 이미지</label>
						<div class="col-sm-10">
							<img runat="server" id="uf_image" style="max-width:500px" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"> 내용</label>
						<div class="col-sm-10">
							<asp:Literal runat="server" ID="uf_content" />
						</div>
					</div>

					<asp:PlaceHolder runat="server" ID="ph_content_image" Visible="false">
					<div class="form-group">
						<label class="col-sm-2 control-label"> 내용이미지</label>
						<div class="col-sm-10">
							<asp:Repeater runat="server" ID="repeater_content_image">
								<ItemTemplate>
									<img src="<%#Container.DataItem.ToString() %>" style="max-width:500px" /><br />
								</ItemTemplate>
							</asp:Repeater>
							
						</div>
					</div>
					</asp:PlaceHolder>

					<asp:PlaceHolder runat="server" ID="ph_movie" Visible="false">
					<div class="form-group">
						<label class="col-sm-2 control-label"> 영상</label>
						<div class="col-sm-10">
							<asp:Repeater runat="server" ID="repeater_movie">
								<ItemTemplate>
									<iframe src="<%#Container.DataItem.ToString().Replace("https://youtu.be/", "https://www.youtube.com/embed/") %>" width="500" height="250" frameborder="0" allowfullscreen></iframe><br />
								</ItemTemplate>
							</asp:Repeater>
							
						</div>
					</div>
					</asp:PlaceHolder>

					<div class="form-group">
						<label class="col-sm-2 control-label"> 기간</label>
						<div class="col-sm-10">
						<asp:Literal runat="server" ID="uf_date" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"> 목표금액</label>
						<div class="col-sm-10">
						<asp:Literal runat="server" ID="uf_goal_amount" /> 원
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"> 현재금액</label>
						<div class="col-sm-10">
						<asp:Literal runat="server" ID="uf_current_amount" /> 원
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">노출 여부 </label>
						<div class="col-sm-9" style="margin-top:5px;">
							<asp:RadioButtonList runat="server" ID="display" RepeatDirection="Horizontal" RepeatLayout="Flow" >
								<asp:ListItem Value="True" Text="&nbsp;노출&nbsp;&nbsp;&nbsp;" Selected="True"></asp:ListItem>
								<asp:ListItem Value="False" Text="&nbsp;비노출&nbsp;&nbsp;&nbsp;"></asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">관리자선정<br /></label>
						<div class="col-sm-9" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="uf_pick" /> 나눔펀딩 메인 상단비주얼노출 , 메인노출
						</div>
					</div>

					<div class="box-footer clearfix text-center">
						<asp:LinkButton runat=server ID="btn_update" class="btn btn-danger pull-right" OnClick="btn_update_Click" style="margin-right:5px">수정 </asp:LinkButton>
					</div>
				</div>
			</div>
			
			<div class="tab-pane" id="tab2" runat="server" >
				<input type="hidden" runat="server" id="delDate"/>		
				<div>

						<div class="box-header">
							<h4 class="box-title">총 <strong><asp:Literal runat="server" ID="lbTotal"></asp:Literal></strong> 건</h4>
						
							<div class="pull-right">
								<input type="text" runat="server" id="s_keyword" class="form-control inline" style="width:300px; margin-right:10px;" OnTextChanged="search" AutoPostBack="true" placeholder="아이디, 내용검색" />
								<asp:LinkButton runat="server"  class="btn btn-primary inline" style="width:70px;display:inline-block;vertical-align:top;margin-top:2px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
							</div>
						</div>
					
					<table class="table table-hover table-bordered">
						<colgroup>
							<col width="5%" />
							<col width="10%" />
							<col width="10%" />
							<col width="10%" />
							<col width="*" />
							<col width="10%" />
						</colgroup>
						<thead>
							<tr>
								<th style="text-align:center"><asp:LinkButton runat="server" ID="btn_remove_user" OnClick="btn_remove_reply_Click"  OnClientClick="return removeUser()" class="btn btn-danger" Style="height:23px;padding-top:2px;margin:0">선택삭제</asp:LinkButton></th>
								<th>번호</th>
								<th>아이디</th>
								<th>이름 </th>
								<th>내용 </th>
								<th>등록일</th>
							</tr>
						</thead>
						<tbody>

							<asp:Repeater runat="server" ID="repeater" OnItemDataBound=ListBound>
								<ItemTemplate>
									<tr>
										<td><input type="checkbox" class="ur_id" value="<%#Eval("ur_id") %>"/></td>
										<td><asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
										<td><%#Eval("UserID") %></td>
										<td><img src="<%#Eval("UserPic") %>" width="50"/><%#Eval("SponsorName") %></td>
										<td class="text-left"><%#Eval("ur_content") %></td>
										<td><%#Eval("ur_regdate", "{0:yy.MM.dd}") %></td>			
									</tr>
								</ItemTemplate>
								<FooterTemplate>
									<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
										<td colspan="6">데이터가 없습니다.</td>
									</tr>
								</FooterTemplate>
							</asp:Repeater>
						</tbody>

					</table>
					
					<div class="text-center">
						<uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />	
						
					</div>

				</div>

			</div>

			<div class="tab-pane" id="tab3" runat="server" >

				<div class="form-horizontal" style="padding:20px;">

					<ul class="timeline">
						<!-- timeline time label -->
						<li class="time-label">
							<span class="bg-green">
								<asp:Literal runat="server" ID="regdate"></asp:Literal>
							</span>
						</li>
						<!-- /.timeline-label -->

						<!-- timeline item -->
										
							<asp:Repeater runat="server" ID="repeater2">
								<ItemTemplate>
									<li>
										<!-- timeline icon -->
										<i class="fa fa-comments bg-yellow"></i>
										<div class="timeline-item">
											<span class="time"><i class="fa fa-clock-o"></i> <%#Eval("un_regdate", "{0:yy.MM.dd HH:mm}") %></span>

											<!--<h3 class="timeline-header"><a href="#">제목</a> ...</h3>-->

											<div class="timeline-body">
												<%#Eval("un_content") %><br />
												<img src="<%#Eval("un_image") %>"/>
											</div>

											<div class="timeline-footer">
												<asp:LinkButton runat="server" class="btn btn-danger btn-xs" OnClick="update_remove_Click" CommandArgument=<%#Eval("un_id") %>>삭제</asp:LinkButton>
											</div>
										</div>
									</li>
								</ItemTemplate>
								<FooterTemplate>
									<li runat="server" Visible="<%#repeater2.Items.Count == 0 %>">
										<!-- timeline icon -->
										<i class="fa fa-comments bg-yellow"></i>
										<div class="timeline-item">
											<span class="time"><i class="fa fa-clock-o"></i> 00.00.00</span>

											<!--<h3 class="timeline-header"><a href="#">제목</a> ...</h3>-->

											<div class="timeline-body">
												등록된 업데이트가 없습니다. 
												<img src=""/>
											</div>

											<div class="timeline-footer">
											</div>
										</div>
									</li>
								</FooterTemplate>
							</asp:Repeater>

						<!-- END timeline item -->
					</ul>

					
					<div class="text-center">
						<uc:paging runat=server ID=paging2 OnNavigate="paging_Navigate2" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />	
					</div>
				</div>
			</div>

			<div class="tab-pane" id="tab4" runat="server" >
				<div>

					<div class="box-header">
						<h4 class="box-title">총 <strong><asp:Literal runat="server" ID="lbTotal3"></asp:Literal></strong> 건</h4>
						
						<div class="pull-right">
							<input type="text" runat="server" id="s_keyword2" class="form-control inline" style="width:300px; margin-right:10px;" OnTextChanged="search" AutoPostBack="true" placeholder="아이디, 이름검색" />
							<asp:LinkButton runat="server"  class="btn btn-primary inline" style="width:70px;display:inline-block;vertical-align:top;margin-top:2px" OnClick="search2">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
						</div>
					</div>
					
					<table class="table table-hover table-bordered">
						<colgroup>
							<col width="5%" />
							<col width="15%" />
							<col width="15%" />
							<col width="20%" />
							<col width="20%" />
							<col width="10%" />
						</colgroup>
						<thead>
							<tr>
								<th>번호</th>
								<th>아이디</th>
								<th>이름 </th>
								<th>후원금액 </th>
								<th>후원일</th>
								<th>삭제</th>
							</tr>
						</thead>
						<tbody>

							<asp:Repeater runat="server" ID="repeater3" OnItemDataBound=ListBound3>
								<ItemTemplate>
									<tr>
										<td><asp:Literal runat="server" ID="lbIdx2"></asp:Literal></td>
										<td><%#Eval("UserID") %></td>
										<td><%#Eval("SponsorName") %></td>
										<td><%#Eval("uu_amount","{0:N0}") %>원</td>
										<td><%#Eval("uu_regdate", "{0:yy.MM.dd HH:mm}") %></td>
										<td><asp:LinkButton class="btn btn-sm btn-danger btn-flat" runat="server" ID="btn_uu_delete" OnClick="btn_uu_delete_Click" OnClientClick="return confirm('삭제 하시겠습니까?')" CommandArgument=<%#Eval("uu_id") %>>삭제</asp:LinkButton> </td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
									<tr runat="server" Visible="<%#repeater3.Items.Count == 0 %>">
										<td colspan="6">참여자가 없습니다.</td>
									</tr>
								</FooterTemplate>
							</asp:Repeater>
						</tbody>

					</table>
					
					<div class="text-center">
						<uc:paging runat=server ID=paging3 OnNavigate="paging_Navigate3" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />	
					</div>

				</div>


			</div>

			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	



	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove  OnClientClick="return onRemove();" OnClick="btn_remove_Click" class="btn btn-danger pull-right">삭제</asp:LinkButton>
		 <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>