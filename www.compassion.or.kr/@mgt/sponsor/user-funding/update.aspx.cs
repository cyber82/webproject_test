﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using CommonLib;

public partial class mgt_user_funding_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		base.BoardType = "storeNotice";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);

		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		getDefaultData();
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	void getDefaultData()
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.sp_tUserFunding_get(Convert.ToInt32(PrimaryKey)).FirstOrDefault();
            Object[] op1 = new Object[] { "uf_id" };
            Object[] op2 = new Object[] { Convert.ToInt32(PrimaryKey) };
            var list = www6.selectSP("sp_tUserFunding_get", op1, op2).DataTableToList<sp_tUserFunding_getResult>().ToList();
            var entity = list.FirstOrDefault();

            if (entity == null)
            {
                base.AlertWithJavascript("나눔펀딩 정보가 없습니다.", "goBack()");
                return;
            }
            
            //this.ViewState["entity"] = entity.ToJson();

            campaignID.Text = entity.CampaignID;
            CampaignName.Text = entity.CampaignName;
            sf_image.Attributes["src"] = entity.sf_image;
            sf_summary.Text = entity.sf_summary;
            sf_date.Text = string.Format("{0:yyyy년 MM월 dd일}", entity.StartDate) + " ~ " + string.Format("{0:yyyy년 MM월 dd일}", entity.EndDate);
            sf_amount.Text = string.Format("{0:N0}", entity.sf_current_amount) + "원 / " + string.Format("{0:N0}", entity.sf_goal_amount) + "원";

            uf_title.Text = entity.uf_title;
            uf_image.Attributes["src"] = entity.uf_image;
            uf_summary.Text = entity.uf_summary;
            uf_content.Text = entity.uf_content;
            regdate.Text = entity.uf_regdate.ToString("yyyy년 MM월 dd일");
            display.SelectedValue = entity.uf_display.ToString();
            uf_date.Text = string.Format("{0} ~ {1}", entity.uf_date_start.ToString("yyyy.MM.dd"), entity.uf_date_end.ToString("yyyy.MM.dd"));
            uf_current_amount.Text = entity.uf_current_amount.ToString("N0");
            uf_goal_amount.Text = entity.uf_goal_amount.ToString("N0");
            uf_pick.Checked = entity.uf_pick;

            if (!string.IsNullOrEmpty(entity.uf_movie))
            {
                ph_movie.Visible = true;
                repeater_movie.DataSource = entity.uf_movie.Split('|').ToList();
                repeater_movie.DataBind();
                //uf_movie1.Attributes["src"] = entity.uf_movie.Replace("https://youtu.be/", "https://www.youtube.com/embed/");
            }

            if (!string.IsNullOrEmpty(entity.uf_content_image))
            {
                ph_content_image.Visible = true;
                repeater_content_image.DataSource = entity.uf_content_image.Split('|').ToList();
                repeater_content_image.DataBind();
                //uf_movie1.Attributes["src"] = entity.uf_movie.Replace("https://youtu.be/", "https://www.youtube.com/embed/");
            }



            // 어린이 펀딩
            if (entity.uf_type == "child")
            {
                var actionResult = new ChildAction().GetChild(entity.ChildMasterID);
                if (actionResult.success)
                {

                    ph_child.Visible = true;
                    var child = (ChildAction.ChildItem)actionResult.data;
                    c_img.Attributes["src"] = child.Pic;
                    c_name.Text = child.Name;
                    c_age.Text = child.Age.ToString();
                    c_country.Text = child.CountryName;
                    c_birth.Text = child.BirthDate.ToString("yyyy년 MM월 dd일");
                }
            }
            else
            {
                ph_campaign.Visible = true;
            }

        }


	}

	protected override void GetList( int page ) {
        using (AdminDataContext dao = new AdminDataContext())
        {
            // 댓글 리스트 
            //var list = dao.sp_tUserFundingReply_list(page, paging.RowsPerPage, Convert.ToInt32(PrimaryKey), s_keyword.Value, "", "").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "uf_id", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, Convert.ToInt32(PrimaryKey), s_keyword.Value, "", "" };
            var list = www6.selectSP("sp_tUserFundingReply_list", op1, op2).DataTableToList<sp_tUserFundingReply_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }

	}

	protected void GetUpdateList( int page ) {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //참여자 리스트 
            //var list = dao.sp_tUserFundingNotice_list(page, paging2.RowsPerPage, Convert.ToInt32(PrimaryKey)).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "uf_id" };
            Object[] op2 = new Object[] { page, paging2.RowsPerPage, Convert.ToInt32(PrimaryKey) };
            var list = www6.selectSP("sp_tUserFundingNotice_list", op1, op2).DataTableToList<sp_tUserFundingNotice_listResult>().ToList();

            var total = 0;

            //Response.Write( "asd" + list.ToJson() );

            if (list.Count > 0)
                total = list[0].total.Value;


            //lbTotal2.Text = total.ToString();

            paging2.CurrentPage = page;
            paging2.Calculate(total);
            repeater2.DataSource = list;
            repeater2.DataBind();

        }

	}

	protected void GetUserList( int page )
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //참여자 리스트 
            //var list = dao.sp_tUserFundingUser_list(page, paging3.RowsPerPage, Convert.ToInt32(PrimaryKey), s_keyword2.Value).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "uf_id", "keyword" };
            Object[] op2 = new Object[] { page, paging3.RowsPerPage, Convert.ToInt32(PrimaryKey), s_keyword2.Value };
            var list = www6.selectSP("sp_tUserFundingUser_list", op1, op2).DataTableToList<sp_tUserFundingUser_listResult>().ToList();
            var total = 0;

            //Response.Write( "asd" + list.ToJson() );

            if (list.Count > 0)
                total = list[0].total.Value;


            lbTotal3.Text = total.ToString();

            paging3.CurrentPage = page;
            paging3.Calculate(total);
            repeater3.DataSource = list;
            repeater3.DataBind();

        }

	}

	protected void ListBound( object sender, RepeaterItemEventArgs e ) {
		if(repeater != null) {
			if(e.Item.ItemType != ListItemType.Footer) {
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
			}
		}
	}

	protected void ListBound3( object sender, RepeaterItemEventArgs e ) {
		if(repeater != null) {
			if(e.Item.ItemType != ListItemType.Footer) {
				((Literal)e.Item.FindControl("lbIdx2")).Text = (paging3.TotalRecords - ((paging3.CurrentPage - 1) * paging3.RowsPerPage) - e.Item.ItemIndex).ToString();
			}
		}
	}


	// 탭 컨트롤
	protected void btn_section_Click( object sender, EventArgs e ) {

		LinkButton obj = sender as LinkButton;
		var no = obj.CommandArgument;

		int tabCount = 4;

		for(int i = 1; i < tabCount + 1; i++) {

			if(no == i.ToString()) {
				((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "active";
				((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "active tab-pane";
			} else {
				((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "";
				((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "tab-pane";
			}


			if(no == "2") {
				GetList(1);
			} else if(no == "3") {
				GetUpdateList(1);
			} else if(no == "4") {
				GetUserList(1);
			}
		}

	}

	// 나눔펀딩 삭제 
	protected void btn_remove_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {
            var exist = www6.selectQ<tUserFundingUser>("uu_uf_id", Convert.ToInt32(PrimaryKey));

            //if (dao.tUserFundingUser.Any(p => p.uu_uf_id == Convert.ToInt32(PrimaryKey)))
            if(exist.Any())
            {
                Master.ValueMessage.Value = "참여자가 있어 삭제가 불가능합니다.";
                return;
            }

            AdminEntity adm = AdminLoginSession.GetCookie(this.Context);

            //var entity = dao.tUserFunding.First(p => p.uf_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<tUserFunding>("uf_id", Convert.ToInt32(PrimaryKey));

            entity.uf_deleted = true;
            entity.uf_a_id = adm.identifier;

            //dao.SubmitChanges();
            //string wClause = string.Format("uf_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.update(entity);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", entity.ToJson()));

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "삭제되었습니다.";
        }
	}

	// 나눔펀딩 수정 
	protected void btn_update_Click( object sender, EventArgs e ) {
		var uf_display = display.SelectedValue;

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tUserFunding.First(p => p.uf_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<tUserFunding>("uf_id", Convert.ToInt32(PrimaryKey));

            entity.uf_display = Convert.ToBoolean(uf_display);
            entity.uf_pick = uf_pick.Checked;

            //dao.SubmitChanges();
            //string wClause = string.Format("uf_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.update(entity);
        }
        base.AlertWithJavascript("수정 되었습니다.");
	}


	// 댓글삭제 
	protected void btn_remove_reply_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

		var del_list = delDate.Value.Split(',');

        using (AdminDataContext dao = new AdminDataContext())
        {
            AdminEntity adm = AdminLoginSession.GetCookie(this.Context);

            List<tUserFundingReply> list = new List<tUserFundingReply>();

            for (var i = 0; i < del_list.Length; i++)
            {
                //list.Add(dao.tUserFundingReply.First(p => p.ur_id == Convert.ToInt32(del_list[i])));
                var entity = www6.selectQF<tUserFundingReply>("ur_id", Convert.ToInt32(del_list[i]));
                list.Add(entity);
                list[i].ur_deleted = true;
                list[i].ur_a_id = adm.identifier;

                //string wClause = string.Format("ur_id = {0}", Convert.ToInt32(del_list[i]));
                www6.update(entity);
            }

            //dao.SubmitChanges();

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", list.ToJson()));

            GetList(paging.CurrentPage);
            Master.ValueMessage.Value = "삭제되었습니다.";


            /* 삭제가 아니라 업데이트로 해야됨 .. ur_deleted
				dao.tUserFundingReply.DeleteAllOnSubmit( list );
			*/
        };

	}

	//업데이트 소식 삭제
	protected void update_remove_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}


		LinkButton btn = sender as LinkButton;
		var id = Convert.ToInt32(btn.CommandArgument);


        using (AdminDataContext dao = new AdminDataContext())
        {
            AdminEntity adm = AdminLoginSession.GetCookie(this.Context);

            //var entity = dao.tUserFundingNotice.First(p => p.un_id == id);
            var entity = www6.selectQF<tUserFundingNotice>("un_id", id);

            entity.un_deleted = true;
            entity.un_a_id = adm.identifier;
            //dao.SubmitChanges();
            //string wClause = string.Format("un_id = {0}", id);
            www6.update(entity);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", entity.ToJson()));

            GetUpdateList(paging2.CurrentPage);
            Master.ValueMessage.Value = "삭제되었습니다.";

        }
	}

	// 참여자 검색 
	protected void search2( object sender, EventArgs e ) {
		this.GetUserList(1);
	}


	// 페이징 관려
	protected void paging_Navigate2( int page, string hash ) {
		this.GetUpdateList(page);
	}

	protected void paging_Navigate3( int page, string hash ) {
		this.GetUserList(page);
	}


	// 참여자-후원취소(삭제)
	protected void btn_uu_delete_Click( object sender, EventArgs e )
    {
		if(base.IsRefresh)
			return;
		var uu_id = Convert.ToInt32((sender as LinkButton).CommandArgument);

        using (AdminDataContext dao = new AdminDataContext())
        {
            var a_id = AdminLoginSession.GetCookie(this.Context).identifier;

            //dao.sp_tUserFundingUser_delete(uu_id, a_id);
            Object[] op1 = new Object[] { "uu_id", "uu_a_id" };
            Object[] op2 = new Object[] { uu_id, a_id };
            www6.selectSP("sp_tUserFundingUser_delete", op1, op2);
        }

		this.GetUserList(1);
	}
}