﻿$(function () {

    $page.init();

});

var $page = {

    init: function () {
        $("#btn_remove").click(function () {
            return confirm("삭제하시겠습니까?");
        });

        $("ul.nav-tabs li").each(function (i) {
            if ($(this).hasClass("active")) {
                eval('$page.tab' + (i + 1) + '.init()');
            }
        })


        $('#dn_subject').bind('keyup paste focus blur', function () {
            if ($(this).val().length > 30) {
                $(this).val($(this).val().substring(0, 30));
                alert('30자까지 가능합니다.');
            }
            $('#subjectLimit').text($(this).val().length);
        });

        //$('#dn_content').bind('keyup paste focus blur', function () {
        //    if ($(this).val().length > 140) {
        //        $(this).val($(this).val().substring(0, 140));
        //        alert('140자까지 가능합니다.');
        //    }
        //    $('#contentLimit').text($(this).val().length);
        //});

        $('#subjectLimit').text($('#dn_subject').val().length);
        //$('#contentLimit').text($('#dn_content').val().length);
    },

    tab1: {

        sponsor_types: null,
        campaigns: null,
        init: function () {
            $("#btn_update").click(function () {
                return $page.tab1.onSubmit();
            });
        },

        onSubmit: function () {
            if ($("#dn_subject").val() == "") {
                alert("제목을 입력해주세요");
                return false;
            }

            if ($("#dn_content").val() == "") {
                alert("본문을 입력해주세요");
                return false;
            }

            if ($("#dn_contentMobile").val() == "") {
                alert("본문(모바일)을 입력해주세요");
                return false;
            }

            return confirm("등록 하시겠습니까?");
        }
    }

}
