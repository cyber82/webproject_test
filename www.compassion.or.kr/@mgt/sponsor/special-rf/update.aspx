﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="_mgt_sponsor_special_rf_update" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript" src="update.js?v=1.0"></script>
</asp:Content>
<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	<input type=hidden runat=server id=upload_root value="" />
	<input type=hidden runat=server id="hd_sponsor_types" value="" />
	<input type=hidden runat=server id="hd_campaigns" value="" />
	<input type=hidden runat=server id="hd_accountClass" value="" />
	<input type="hidden" runat="server" id="sf_thumb" />
	<input type="hidden" runat="server" id="sf_thumb_m" />
	<input type="hidden" runat="server" id="sf_image" />
	<input type="hidden" runat="server" id="sf_image_m" />
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" CommandArgument="1" >기본정보</asp:LinkButton></li>
		</ul>
		<div class="tab-content">
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">
                    <div class="form-group" runat="server" id="divRegDate" visible="false">
						<label class="col-sm-2 control-label">등록일</label>
						<div class="col-sm-2" style="width:600px; padding-top: 5px;">
							<asp:Label runat="server" ID="lbRegDate"></asp:Label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">제목</label>
						<div class="col-sm-2" style="width:600px">
							<input type="text" class="form-control" id="dn_subject" runat=server style="width:600px" /><br /><div style="margin-top: -14px;">(<span id="subjectLimit"></span> / 30자)</div>
						</div>
					</div>
					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">본문</label>
						<div class="col-sm-10">
							<textarea runat="server" id="dn_content" name="dn_content" style="width:600px;height:100px"></textarea>
						</div>
					</div>
                    <div class="form-group" >
						<label class="col-sm-2 control-label control-label">본문<br />(모바일)</label>
						<div class="col-sm-10">
							<textarea runat="server" id="dn_contentMobile" name="dn_contentMobile" style="width:600px;height:100px"></textarea>
						</div>
					</div>
				</div>
				<div class="box-footer clearfix text-center">
					<asp:LinkButton runat=server ID="btn_update" OnClick="btn_update_Click" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
				</div>
			</div>
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
    </div>
	<!-- /.nav-tabs-custom -->
    <div class="box-footer clearfix text-center">
		<asp:LinkButton runat=server ID="btn_remove" OnClick="btn_remove_Click" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>