﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Web.UI.HtmlControls;

public partial class _mgt_sponsor_special_rf_update : AdminBasePage
{
    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();
        v_admin_auth auth = base.GetPageAuth();
        base.PrimaryKey = Request["c"];
        base.Action = Request["t"];
        
        btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();
        
        if (base.Action == "update")
        {
            using (AdminDataContext dao = new AdminDataContext())
            {
                var entity = www6.selectQF<tDisasterNews>("idx", Convert.ToInt32(PrimaryKey));
                dn_subject.Value = entity.subject;
                dn_content.Value = entity.content;
                dn_contentMobile.Value = entity.contentMobile;
                lbRegDate.Text = entity.regDate.ToString("yyyy-MM-dd hh:mm:ss");

                divRegDate.Visible = true;
            }
        }
    }

    protected override void OnAfterPostBack()
    {
        Master.ValueAction.Value = "";
        Master.ValueMessage.Value = "";
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();
    }

    protected void btn_update_Click(object sender, EventArgs e)
    {
        if (base.IsRefresh)
        {
            return;
        }

        using (AdminDataContext dao = new AdminDataContext())
        {
            var adminID = AdminLoginSession.GetCookie(this.Context).identifier.ToString();

            if (base.Action == "update")
            {
                var entity = www6.selectQF<tDisasterNews>("idx", Convert.ToInt32(PrimaryKey));
                entity.subject = dn_subject.Value;
                entity.content = dn_content.Value;
                entity.contentMobile = dn_contentMobile.Value;
                entity.updID = adminID;
                entity.updDate = DateTime.Now;
                www6.update(entity);

                base.AlertWithJavascript("수정되었습니다.", "goList()");
            }
            else
            {
                var entity = new tDisasterNews()
                {
                    subject = dn_subject.Value,
                    content = dn_content.Value,
                    contentMobile = dn_contentMobile.Value,
                    regID = adminID,
                    regDate = DateTime.Now,
                    updID = adminID,
                    updDate = DateTime.Now,
                    isDelete = false
                };
                www6.insert(entity);

                base.AlertWithJavascript("등록되었습니다.", "goList()");
                this.PrimaryKey = entity.idx.ToString();
            }
        }
    }

    protected void btn_remove_Click(object sender, EventArgs e)
    {
        if (base.IsRefresh)
        {
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            if (base.Action == "update")
            {
                var adminID = AdminLoginSession.GetCookie(this.Context).identifier.ToString();

                var entity = www6.selectQF<tDisasterNews>("idx", Convert.ToInt32(PrimaryKey));
                entity.isDelete = true;
                entity.updID = adminID;
                entity.updDate = DateTime.Now;
                www6.update(entity);

                base.AlertWithJavascript("삭제되었습니다.", "goList()");
            }
        }
    }
}