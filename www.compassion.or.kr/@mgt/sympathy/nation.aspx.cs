﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
using System.Text;

public partial class mgt_sympathy_nation : AdminBasePage {

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;
	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            string where = "1=1";

            if (s_display.SelectedValue.Trim() != "")
                where += string.Format(" and nc_display={0}", s_display.SelectedValue);

            //string query = string.Format("select * from nation_code where {0} order by nc_depth1 asc , nc_order asc", where);
            //var list = dao.ExecuteQuery<nation_code>(query);
            var list = www6.selectQ<nation_code>("nc_display", s_display.SelectedValue, "nc_depth1 asc , nc_order asc");

            int totalCount = list.Count; //dao.ExecuteQuery<nation_code>(query).Count();
            lbTotal.Text = totalCount.ToString();

            paging.Calculate(totalCount);
            repeater.DataSource = list.Skip((page - 1) * paging.RowsPerPage).Take(paging.RowsPerPage);
            repeater.DataBind();

        }
	}


	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		nation_code entity = e.Item.DataItem as nation_code;
		((CheckBox)e.Item.FindControl("nc_display")).Checked = entity.nc_display;

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var count = entity.nc_depth2 == "" ?
            //    dao.ssb_article.Where(p => p.sa_nc_depth1 == entity.nc_depth1).Count() :
            //    dao.ssb_article.Where(p => p.sa_nc_depth2.Contains(entity.nc_depth2)).Count();
            var count = 0;
            if (string.IsNullOrWhiteSpace(entity.nc_depth2))
                count = www6.selectQ<ssb_article>("sa_nc_depth1", entity.nc_depth1).Count();
            else
                count = www6.selectQ2 <ssb_article>("sa_nc_depth2 LIKE ", entity.nc_depth2.likeAll()).Count();

            ((Literal)e.Item.FindControl("lbContentCount")).Text = count.ToString();

            ((TextBox)e.Item.FindControl("nc_depth1")).Enabled = ((TextBox)e.Item.FindControl("nc_depth2")).Enabled = count > 0 ? false : true;
        }

	}


	protected void btn_update_Click(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var success = true;
            //var list = dao.nation_code;
            var list = www6.selectQ<nation_code>("nation_code");

            foreach (RepeaterItem item in repeater.Items)
            {
                var nc_id = Convert.ToInt32(((LinkButton)item.FindControl("btn_remove")).CommandArgument);
                var nc_depth1 = ((TextBox)item.FindControl("nc_depth1")).Text;
                var nc_depth2 = ((TextBox)item.FindControl("nc_depth2")).Text;
                var nc_name = ((TextBox)item.FindControl("nc_name")).Text;
                var nc_display = ((CheckBox)item.FindControl("nc_display")).Checked;
                var nc_order = Convert.ToInt32(((TextBox)item.FindControl("nc_order")).Text);

                var entity = list.First(p => p.nc_id == nc_id);

                entity.nc_name = nc_name;
                entity.nc_depth1 = nc_depth1;
                entity.nc_depth2 = nc_depth2;
                entity.nc_display = nc_display;
                entity.nc_order = nc_order;
                entity.nc_a_id = AdminLoginSession.GetCookie(Context).identifier;

                var exist = www6.selectQ2<tCspDol>("nc_depth2 =", entity.nc_depth2, "nc_depth1 =", entity.nc_depth1, "nc_id  !=", nc_id);
                //if (dao.nation_code.Any(p => (p.nc_depth2 == entity.nc_depth2) && (p.nc_depth1 == entity.nc_depth1) && p.nc_id != nc_id))
                if(exist.Any())
                {
                    success = false;
                    Master.ValueMessage.Value = "동일한 키가 사용중입니다.";
                    return;
                }
                else
                {
                    entity.nc_name = nc_name;
                    entity.nc_depth1 = nc_depth1;
                    entity.nc_depth2 = nc_depth2;
                    entity.nc_display = nc_display;
                    entity.nc_order = nc_order;
                    entity.nc_a_id = AdminLoginSession.GetCookie(Context).identifier;

                    if (success)
                    {
                        //dao.SubmitChanges();
                        //string wClause = string.Format("nc_id = {0}", entity.nc_id);
                        www6.update(entity);
                    }
                }

            }
            
        }

		Master.ValueMessage.Value = "수정되었습니다.";
		GetList(1);
	}

	protected void btn_add_Click(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<nation_code>("nc_depth2", new_nc_depth2.Text);
            //if (dao.nation_code.Any(p => p.nc_depth2 == new_nc_depth2.Text))
            if(exist.Any())
            {
                Master.ValueMessage.Value = "동일한 키가 사용중입니다.";
            }
            else
            {
                var entity = new nation_code()
                {
                    nc_depth1 = new_nc_depth1.Text,
                    nc_depth2 = new_nc_depth2.Text,
                    nc_name = new_nc_name.Text,
                    nc_order = 100,
                    nc_regdate = DateTime.Now,
                    nc_display = false,
                };

                //dao.nation_code.InsertOnSubmit(entity);
                www6.insert(entity);

                //dao.SubmitChanges();

                Master.ValueMessage.Value = "등록되었습니다.";
                GetList(1);

            }

        }

	}
	

	protected void btn_remove_Click(object sender, EventArgs e)
    {
		LinkButton obj = sender as LinkButton;
		var nc_id = Convert.ToInt32(obj.CommandArgument);
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.nation_code.First(p => p.nc_id == nc_id);
            //dao.nation_code.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from nation_code where nc_id = {0}", nc_id);
            www6.cud(delStr);
        }

		Master.ValueMessage.Value = "삭제되었습니다.";
		GetList(1);
	}

}
