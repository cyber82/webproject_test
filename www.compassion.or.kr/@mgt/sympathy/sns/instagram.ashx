﻿<%@ WebHandler Language="C#" Class="api_my_account" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;

using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Helpers;
using System.Net.Http;
using System.Threading.Tasks;





public class api_my_account : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();


    public override void OnRequest(HttpContext context) {
        if (!AdminLoginSession.HasCookie(context)) {
            context.Response.Write("<script>alert('로그인 후 이용하세요.');window.close();</script>");
            return;
        }

        var data = GetPostsWithAuth(20, "");
        List<sns_post> result = new List<sns_post>();
        string msg = "업데이트 할 포스트가 없습니다.";

        if (data != null)
        {
            using (AdminDataContext dao = new AdminDataContext())
            {
                foreach (var item in data.items)
                {
                    var exist = www6.selectQ<sns_post>("provider", item.provider, "id", item.id);
                    //if (!dao.sns_post.Any(p => p.provider == item.provider && p.id == item.id))
                    if(!exist.Any())
                    {
                        result.Add(new sns_post()
                        {
                            id = item.id,
                            provider = item.provider,
                            title = item.title.EmptyIfNull(),
                            content = item.content.EmptyIfNull(),
                            author = item.author.EmptyIfNull(),
                            comment_count = item.commentCount,
                            like_count = item.likeCount,
                            link = item.link.EmptyIfNull(),
                            picture = item.picture.EmptyIfNull(),
                            regdate = item.regdate
                        });
                    }

                }

                //if (result.Count > 0)
                //{
                //    //dao.sns_post.InsertAllOnSubmit(result);
                //    www6.insert(result);
                //    //dao.SubmitChanges();
                //
                //    msg = string.Format("{0}건의 신규데이타가 업데이트 되었습니다.", result.Count);
                //}

                foreach(sns_post sp in result)
                {
                    //dao.sns_post.InsertAllOnSubmit(result);
                    www6.insert(sp);
                    //dao.SubmitChanges();
                }
                msg = string.Format("{0}건의 신규데이타가 업데이트 되었습니다.", result.Count);
            }
        }


        context.Response.Write("<script>opener.callback('instagram' , '" + msg + "');window.close();</script>");

    }




    public void Authorize(string client_id , string redirect_uri) {
        ErrorLog.Write(HttpContext.Current, 0, "Authorize");
        try {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("https://api.instagram.com/oauth/authorize/?client_id={0}&redirect_uri={1}&response_type=code",
                client_id , redirect_uri
                ));

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {

                var nextUri = response.ResponseUri.ToString();
                nextUri = nextUri.Substring(0, nextUri.LastIndexOf("?")) + HttpUtility.UrlEncode(nextUri.Substring(nextUri.LastIndexOf("?")));
                HttpContext.Current.Response.Redirect(nextUri);

            }
        } catch (Exception e) {
            ErrorLog.Write(HttpContext.Current, 0, "Authorize = " + e.Message);
        }
    }

    Task<OAuthResponse> RequestAccessToken( string client_id , string client_secret , string code) {
        ErrorLog.Write(HttpContext.Current, 0, "RequestAccessToken");
        try {
            var client = new HttpClient {
                BaseAddress = new Uri("https://api.instagram.com/oauth/")
            };

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(client.BaseAddress, "access_token"));
            var myParameters = string.Format("client_id={0}&client_secret={1}&grant_type={2}&redirect_uri={3}&code={4}",
                    client_id.UrlEncode(),
                    client_secret.UrlEncode(),
                    "authorization_code".UrlEncode(),
                    (HttpContext.Current.Request.Domain() + HttpContext.Current.Request.Path).UrlEncode(),
                    code.UrlEncode());

            //request.Content = new StringContent(myParameters);
            request.Content = new StringContent(myParameters, System.Text.Encoding.UTF8, "application/x-www-form-urlencoded");

            return client.ExecuteAsync<OAuthResponse>(request);
        } catch (Exception e) {
            ErrorLog.Write(HttpContext.Current, 0, "RequestAccessToken = " + e.Message);
            return null;
        }
    }

    /*
 instagram 은 oauth 를 사용하기때문에, 사용자페이지에서 실시간으로 인스타그램 데이타를 가져올 수 없다. (반드시 사용자 인증과정을 거치기 때문)
 * 그래서, 관리자를 통해 최신 데이타를 oauth 인증 후 가져오고, 내부디비에 저장한다.
 * 사용자는 저장된 데이타를 조회한다.
 */
    public SocialData GetPosts(int size, string pageVal) {
        ErrorLog.Write(HttpContext.Current, 0, "GetPosts");

        try {
            var items = new List<SocialData.Post>();

            using (MainLibDataContext dao = new MainLibDataContext()) {
                var data = dao.sns_post.Where(p => p.provider == "instagram").OrderByDescending(p => p.regdate).Take(size).ToList();
                foreach (var item in data) {
                    items.Add(new SocialData.Post() {
                        provider = item.provider,
                        content = item.content,
                        link = item.link,
                        picture = item.picture,
                        author = item.author,
                        regdate = item.regdate,
                        likeCount = item.like_count,
                        commentCount = item.comment_count
                        //rawdata = Json.Encode(item)	// 성능을 위해 일단 막음
                    });
                }
            }

            return new SocialData() {
                next = null, prev = null, items = items
            };
        } catch (Exception e) {
            ErrorLog.Write(HttpContext.Current, 0, "GetPosts = " + e.Message);
            return null;
        }



    }

    public SocialData GetPostsWithAuth(int size, string pageVal) {
        ErrorLog.Write(HttpContext.Current, 0, "GetPostsWithAuth");

        try {

            var code = HttpContext.Current.Request["code"].EmptyIfNull();
            var client_id = ConfigurationManager.AppSettings["instagramClientId"];
            var client_secret = ConfigurationManager.AppSettings["instagramClientSecret"];

            // querystring에 code를 넘겨받고 나서 access_token 조회가 가능함.
            if (string.IsNullOrEmpty(code)) {
                this.Authorize(client_id, HttpContext.Current.Request.UrlEx());
                return null;
            }

            var access_token = this.RequestAccessToken(client_id, client_secret, code).Result.AccessToken;

            //	context.Response.Write("code > " + code + "<br>");
            //	context.Response.Write("access_token > " + access_token);

            if (string.IsNullOrEmpty(access_token)) {
                throw new Exception("access_token is null");
            }

            var url = string.Format("https://api.instagram.com/v1/users/self/media/recent?access_token={0}&count={1}", access_token, size);
            using (WebClient wc = new WebClient()) {
                wc.Encoding = System.Text.Encoding.UTF8;
                string str = wc.DownloadString(url);

                dynamic data = Json.Decode(str);

                var items = new List<SocialData.Post>();

                foreach (var item in data.data) {

                    items.Add(new SocialData.Post() {
                        provider = "instagram",
                        id = item.id,
                        content = item.caption.text,
                        link = item.link,
                        picture = item.images.low_resolution.url,   /*low_resolution = 306 , thumbnail = 150 , standard_resolution = 612*/
                        author = item.user.username,
                        regdate = DateExtension.UnixTimeStampToDateTime(Convert.ToInt64(item.created_time)),
                        likeCount = item.likes.count,
                        commentCount = item.comments.count
                        //rawdata = Json.Encode(item)	// 성능을 위해 일단 막음
                    });

                }


                return new SocialData() {
                    next = data.pagination.next_url,
                    prev = null,
                    items = items
                };


            }
        } catch (Exception e) {
            ErrorLog.Write(HttpContext.Current, 0, "GetPostsWithAuth = " + e.Message);
            return null;
        }
    }


    public class UserInfo {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        [JsonProperty("full_name")]
        public string FullName {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the profile picture.
        /// </summary>
        /// <value>
        /// The profile picture.
        /// </value>
        [JsonProperty("profile_picture")]
        public string ProfilePicture {
            get;
            set;
        }
    }

    public class OAuthResponse {
        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        public UserInfo User {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the access_ token.
        /// </summary>
        /// <value>
        /// The access_ token.
        /// </value>
        [JsonProperty("Access_Token")]
        public string AccessToken {
            get;
            set;
        }
    }

}