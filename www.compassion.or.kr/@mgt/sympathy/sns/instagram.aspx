﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="instagram.aspx.cs" Inherits="mgt_mainpage_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
	    $(function () {

	        $("#btn_update_1").click(function () {
	            window.open("/@mgt/sympathy/sns/instagram.aspx", "sns_frm", "width=800px,height=600px");
	            return false;
	        });


	    });

	    var callback = function (provider, msg) {
	        $("#" + provider + "_msg").val(msg);
	    }
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">


    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">인스타그램</label>
						<div class="col-sm-10" style="">
							<input type="text" id="instagram_msg" class="form-control" style="width:300px;float:left;margin-right:10px;" />
                            <button id="btn_update_1" runat="server" type="button" class="btn btn-danger">업데이트</button>
						</div>
					</div>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->


</asp:Content>