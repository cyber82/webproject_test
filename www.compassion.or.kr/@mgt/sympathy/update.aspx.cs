﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_sympathy_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");

		// 게시판 종류
		foreach (var a in StaticData.Code.GetList(this.Context).Where(p => p.cd_display == true && p.cd_group == "sympathy_type").OrderBy(p => p.cd_order)) {
			sympathy_type.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}
		sympathy_type.Items.Insert(0, new ListItem("선택하세요", ""));

        // 국가별 보기
        using (FrontDataContext dao = new FrontDataContext())
        {
            var list = www6.selectQ<nation_code>("nc_display", 1, "nc_depth2", "", "nc_order");
            //foreach (var a in dao.nation_code.Where(p => p.nc_display == true && p.nc_depth2 == "").OrderBy(p => p.nc_order))
            foreach(var a in list)
            {
                sa_nc_depth1.Items.Add(new ListItem(a.nc_name, a.nc_depth1));
            }
        }
		sa_nc_depth1.Items.Insert(0, new ListItem("국가별로 보기", ""));

		if (base.Action == "update")
        {
            best.Visible = true;

            using (FrontDataContext dao = new FrontDataContext())
            {
                //var entity = dao.ssb_article.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<ssb_article>("idx", Convert.ToInt32(PrimaryKey));
                title.Text = entity.title;
                body.InnerHtml = entity.body;
                display.Checked = !entity.yn1.Value;
                notice.Checked = entity.notice;
                reg_date.Text = entity.reg_date.ToString("yyyy.MM.dd HH:mm");
                sub_title.Text = entity.sub_title;
                sympathy_type.SelectedValue = entity.board_id;

                if (entity.board_id != "vision")
                {
                    sa_nc_depth1.SelectedValue = entity.sa_nc_depth1;
                    sa_nc_depth1_SelectedIndexChanged(null, null);
                    sa_nc_depth2.SelectedValue = entity.sa_nc_depth2;
                }
                else
                {
                    depth2_arr.Value = entity.sa_nc_depth2;
                    text_arr.Value = entity.sa_nc_text;
                    sa_nc_depth1_SelectedIndexChanged(null, null);
                }


                if (entity.board_id == "child" || entity.board_id == "vision" || entity.board_id == "individual")
                {
                    phNation.Visible = true;
                }

                // 에세이인 경우 유저가 작성할 수도 있다
                if (entity.email.EmptyIfNull() != "")
                {
                    phWriter.Visible = true;
                    user_id.Text = entity.user_id;
                    user_name.Text = entity.user_name;
                    phone.Text = entity.cell_1 + "-" + entity.cell_2 + "-" + entity.cell_3;
                    email.Text = entity.email;

                    if (StaticData.Code.GetList(this.Context, true).Any(p => p.cd_display == true && p.cd_group == "essay_type" && p.cd_key == entity.board_group))
                    {
                        board_group_name.Text = StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "essay_type" && p.cd_key == entity.board_group).First().cd_value;
                    }
                }

                this.Thumb = entity.thumb;

                ShowFiles();

            }
            
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			ph_regdate.Visible = true;
			
		} else {
			sa_nc_depth1_SelectedIndexChanged(null, null);

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}
	
	protected void btn_update_click(object sender, EventArgs e) {

		var thumb = base.Thumb;
		var cookie = AdminLoginSession.GetCookie(this.Context);

		var arg = new ssb_article() {
			title = title.Text,
			body = body.InnerHtml.ToHtml(false),
			sub_title = sub_title.Text,
			user_id = cookie.identifier.ToString(),
			user_name = cookie.name,
			yn1 = !display.Checked, 
            notice = notice.Checked,
            reg_date = DateTime.Now,
			view_date = DateTime.Now,
			board_id = sympathy_type.SelectedValue,
			board_group = "",
			status = "normal",
			view_num = 0,
			view_count = 0,
			@ref = 0,
			step = 0,
			lev = 0,
			ip_address = Request.ServerVariables["REMOTE_ADDR"].ToString(),
			yn2 = false,
			sa_nc_depth1 = sa_nc_depth1.SelectedValue,
			//sa_nc_depth2 = sa_nc_depth2.SelectedValue,

			thumb = this.Thumb
		};

        using (FrontDataContext dao = new FrontDataContext())
        {
            // 비전트립다이어리는 여러국가 선택
            if (sympathy_type.SelectedValue == "vision")
            {

                // 선택 국가의 대륙 조회
                //arg.sa_nc_depth1 = GetDepth1Code(dao);
                arg.sa_nc_depth1 = GetDepth1Code();
                arg.sa_nc_depth2 = depth2_arr.Value;
                arg.sa_nc_text = text_arr.Value;
            }
            else
            {
                arg.sa_nc_depth2 = sa_nc_depth2.SelectedValue;
                arg.sa_nc_text = sa_nc_depth2.SelectedItem.Text == "국가 선택" ? null : sa_nc_depth2.SelectedItem.Text;
            }

            if (base.Action == "update")
            {

                //var entity = dao.ssb_article.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<ssb_article>("idx", Convert.ToInt32(PrimaryKey));

                entity.user_id = arg.user_id;
                entity.user_name = arg.user_name;
                entity.yn1 = arg.yn1;
                entity.notice = arg.notice;
                entity.title = arg.title;
                entity.body = arg.body;
                entity.sub_title = arg.sub_title;
                entity.board_id = arg.board_id;
                entity.sa_nc_depth1 = arg.sa_nc_depth1;
                entity.sa_nc_depth2 = arg.sa_nc_depth2;
                entity.sa_nc_text = arg.sa_nc_text;
                entity.yn1 = !display.Checked;
                entity.notice = notice.Checked;

                entity.thumb = this.Thumb;

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
                //dao.SubmitChanges();
                //string wClause = string.Format("idx = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);



                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
            }
            else
            {
                //dao.ssb_article.InsertOnSubmit(arg);
                www6.insert(arg);

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";
                //dao.SubmitChanges();

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));
            }

        }

	}


    //protected string GetDepth1Code(FrontDataContext dao)
    protected string GetDepth1Code()
    {
		var depth2 = depth2_arr.Value.Split(',');
        //var nc_depth1_list = dao.nation_code.Where(p => depth2.Contains(p.nc_depth2)).GroupBy(p => p.nc_depth1).Select(p => p.First());
        var nc_depth1_list = www6.selectQ<nation_code>().AsQueryable().Where(p => depth2.Contains(p.nc_depth2)).GroupBy(p => p.nc_depth1).Select(p => p.First());

        return string.Join(",", nc_depth1_list.Select(p => p.nc_depth1));
	}

	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		base.Thumb = temp_file_name.Value;
		this.ShowFiles();

	}

	void ShowFiles() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			thumb.Src = base.Thumb.WithFileServerHost();
			thumb.Attributes["data-exist"] = "1";
		}
	}

	protected void btn_remove_click(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.ssb_article.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            //dao.ssb_article.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from ssb_article where idx = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}


	protected void sa_nc_depth1_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            sa_nc_depth2.Items.Clear();
            var list = www6.selectQ2<nation_code>("nc_display =", 1, "nc_depth1 =", sa_nc_depth1.SelectedValue, "nc_depth2 !=", "", "nc_order");
            //foreach (var a in dao.nation_code.Where(p => p.nc_display == true && p.nc_depth1 == sa_nc_depth1.SelectedValue && p.nc_depth2 != "").OrderBy(p => p.nc_order))
            foreach(var a in list)
            {
                sa_nc_depth2.Items.Add(new ListItem(a.nc_name, a.nc_depth2));
            }
            sa_nc_depth2.Items.Insert(0, new ListItem("국가 선택", ""));
        }

		search(sender, e);
	}

	protected void sympathy_type_SelectedIndexChanged(object sender, EventArgs e) {
		if(sympathy_type.SelectedValue == "child" || sympathy_type.SelectedValue == "vision" || sympathy_type.SelectedValue == "individual")  {
			phNation.Visible = true;
		} else {
			phNation.Visible = false;
		}
	}

    protected void notice_CheckedChanged( object sender, EventArgs e )
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.ssb_article.Where(p => p.notice && p.board_id == "bluebook");
            var list = www6.selectQ<ssb_article>("notice", 1, "board_id", "bluebook");


            if (list.Count() > 2)
            {


            }
        }
    }
}