﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_sympathy_default : AdminBasePage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var depth1 = Request["sa_nc_depth1"].EmptyIfNull();

		Master.Content.Controls.BindAllTextBox(this.Context);


		// 게시판 종류
		foreach (var a in StaticData.Code.GetList(this.Context).Where(p => p.cd_display == true && p.cd_group == "sympathy_type").OrderBy(p => p.cd_order)) {
			board_id.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}
		board_id.Items.Insert(0, new ListItem("전체", ""));


        using (FrontDataContext dao = new FrontDataContext())
        {
            // 국가별 보기
            var list = www6.selectQ<nation_code>("nc_display", 1, "nc_depth2", "", "nc_order");

            //foreach (var a in dao.nation_code.Where(p => p.nc_display == true && p.nc_depth2 == "").OrderBy(p => p.nc_order))
            foreach(var a in list)
            {
                sa_nc_depth1.Items.Add(new ListItem(a.nc_name, a.nc_depth1));
            }
            sa_nc_depth1.Items.Insert(0, new ListItem("국가별로 보기", ""));
            sa_nc_depth2.Items.Insert(0, new ListItem("국가 선택", ""));

            if (depth1 != "")
            {
                sa_nc_depth1.SelectedValue = depth1;
                sa_nc_depth1_SelectedIndexChanged(null, null);
            }

        }


		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_update;


	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.sortColumn = sortColumn;
		base.sortDirection = sortDirection;
		base.OnAfterPostBack();

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected override void GetList(int page) {


		Master.IsSearch = false;
		if (
			board_id.SelectedValue != ""
			|| sa_nc_depth1.SelectedValue != ""
			|| sa_nc_depth2.SelectedValue != ""
			|| s_keyword.Text != "" 
			|| s_b_date.Text != "" 
			|| s_e_date.Text != "") {
			Master.IsSearch = true;
		}


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_sympathy_list(page, paging.RowsPerPage, board_id.SelectedValue, s_keyword.Text.Trim(), s_b_date.Text, s_e_date.Text, sa_nc_depth1.SelectedValue, sa_nc_depth2.SelectedValue).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "board_id", "keyword", "startdate", "enddate", "depth1", "depth2" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, board_id.SelectedValue, s_keyword.Text.Trim(), s_b_date.Text, s_e_date.Text, sa_nc_depth1.SelectedValue, sa_nc_depth2.SelectedValue };
            var list = www6.selectSP("sp_sympathy_list", op1, op2).DataTableToList<sp_sympathy_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();


        }
	}
	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				sp_sympathy_listResult entity = e.Item.DataItem as sp_sympathy_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
			}
		}


		
	}

	protected void sa_nc_depth1_SelectedIndexChanged(object sender, EventArgs e) {

        using (FrontDataContext dao = new FrontDataContext())
        {
            sa_nc_depth2.Items.Clear();
            var list = www6.selectQ2<nation_code>("nc_display =", 1, "nc_depth1 =", sa_nc_depth1.SelectedValue, "nc_depth2 !=", "", "nc_order");
            //foreach (var a in dao.nation_code.Where(p => p.nc_display == true && p.nc_depth1 == sa_nc_depth1.SelectedValue && p.nc_depth2 != "").OrderBy(p => p.nc_order))
            foreach(var a in list)
            {
                sa_nc_depth2.Items.Add(new ListItem(a.nc_name, a.nc_depth2));
            }
            sa_nc_depth2.Items.Insert(0, new ListItem("국가 선택", ""));
        }

		search(sender, e);
	}
}
