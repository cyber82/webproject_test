﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_sympathy_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        #sympathy_type label{
            padding-right:10px;
        }
        
		#display label{margin-left:5px;width:50px}
    </style>
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript">

		$(function () {
		    image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_sympathy)%>";
		    initEditor(oEditors, "body");

            
	        // 썸네일
	        var thumbUploader = attachUploader("thumb");
	        thumbUploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.image_sympathy)%>";
		    thumbUploader._settings.data.rename = "y";

            
		    $("#sympathy_type").change(function () {
		        try {
		            if (oEditors) {
		                $.each(oEditors, function () {
		                    this.exec("UPDATE_CONTENTS_FIELD", []);
		                });
		            }
		        } catch (e) { }
		    });


		    if ($("#sympathy_type").val() == "vision") {
		        $(".form_nation").show();
		    } else {
		        $(".form_nation").hide();
		        $("#nation_arr").val("");
		    }


		    $("#sa_nc_depth1").change(function () {
		        try {
		            if (oEditors) {
		                $.each(oEditors, function () {
		                    this.exec("UPDATE_CONTENTS_FIELD", []);
		                });
		            }
		        } catch (e) { }
		    });

		    $page.init();
		});


	    var $page = {
	        init: function () {
	            var depth2_arr = $("#depth2_arr").val().split(",");
	            var text_arr = $("#text_arr").val().split(",");

	            $("#selected_nation_display").html("");

	            $.each(depth2_arr, function (i, v) {
	                $page.addItem(v, text_arr[i]);
	            });

	            $page.addEvent();
	        },

	        addEvent: function () {
	            // 나라 추가 버튼 
	        	$("#btn_nation").click(function () {

					
	                if ($("#sa_nc_depth1").val() == "") {
	                    alert("국가를 선택하세요");
	                    $("#sa_nc_depth1").focus();
	                    return false;
	                }
	                if ($("#sa_nc_depth2").val() == "") {
	                    alert("국가를 선택하세요");
	                    $("#sa_nc_depth2").focus();
	                    return false;
	                }
	                var code = $("#sa_nc_depth2").val();
	                var text = $("#sa_nc_depth2 option:selected").text();

	                if ($("#selected_nation_display a[data-code=" + code + "]").length > 0) {
	                    alert("선택된 국가입니다.");
	                    return false;;
	                }
	                $page.addItem(code, text);
	                $page.addCode(code, text);
	                return false;
	            });
	        },

	        addItem: function (code, text) {
	            var item = $('<a href="#" class="selected_nation" data-code="' + code + '" style="margin-right:10px;">' + text + '</a>');
	            item.click(function () {
	                $page.removeItem(item);
	            });
	            $("#selected_nation_display").append(item);

	        },

	        addCode: function (code, text) {

	            if ($("#depth2_arr").val() == "") {
	                $("#depth2_arr").val(code);
	            } else {
	                $("#depth2_arr").val($("#depth2_arr").val() + "," + code);
	            }


	            if ($("#text_arr").val() == "") {
	                $("#text_arr").val(text);
	            } else {
	                $("#text_arr").val($("#text_arr").val() + "," + text);
	            }
	        },

	        removeItem: function (item) {
	            var code = item.attr("data-code");
	            var text = item.text();
	            
	            var depth2_arr = $("#depth2_arr").val().split(",");
	            depth2_arr.splice(depth2_arr.indexOf(code), 1);
	            $("#depth2_arr").val(depth2_arr.join(","));


	            var text_arr = $("#text_arr").val().split(",");
	            text_arr.splice(text_arr.indexOf(text), 1);
	            $("#text_arr").val(text_arr.join(","));

	            item.remove();
	        }
	    }

	    var attachUploader = function (button) {
	        return new AjaxUpload(button, {
	            action: '/common/handler/upload',
	            responseType: 'json',
	            onChange: function () {
	                // 이미지 업로드 되면 에디터 초기화 되는 현상 방지
	                // oEditors가 없는 경우 에러남
	                try {
	                    if (oEditors) {
	                        $.each(oEditors, function () {
	                            this.exec("UPDATE_CONTENTS_FIELD", []);
	                        });
	                    }
	                } catch (e) { }
	            },
	            onSubmit: function (file, ext) {
	                this.disable();
	            },
	            onComplete: function (file, response) {
	                this.enable();
	                if (response.success) {
	                    var c = $("#" + button).attr("class").replace(" ", "");
	                    $(".temp_file_type").val(c.indexOf("image") > -1 ? "thumb" : "file");
	                    $(".temp_file_name").val(response.name);
	                    $(".temp_file_size").val(response.size);
	                    eval($(".btn_update_temp_file").attr("href").replace("javascript:", ""));
	                } else
	                    alert(response.msg);
	            }
	        });
	    }


		var onSubmit = function () {

			if (!validateForm([
			    { id: "#sympathy_type", msg: "게시판 종류를 선택하세요" },
				{ id: "#title", msg: "제목을 입력하세요" }
			])) {
				return false;
			}


			if (!$("#thumb").is('[data-exist]')) {
			    alert("썸네일 이미지를 등록하세요.");
			    $("#thumb").focus();
			    return false;
			}


			if ($("#sa_nc_depth1").length > 0) {
				if ($("#selected_nation_display").text() == "" && $("#sa_nc_depth2").val == "") {
					alert("국가를 선택하세요")
			        return false;
			    }
			}

			if ($("#sympathy_type").val() == "vision") {
			    if ($("#depth2_arr").val() == "") {
			        alert("추가하기 버튼으로 국가를 추가하세요.");
			        $("#btn_nation").focus();
			        return false;
			    }
			}

			oEditors.getById["body"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>

    <input type="hidden" runat="server" id="depth2_arr" value="" />
    <input type="hidden" runat="server" id="text_arr" value="" />

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="display" cssClass="form-control1" Checked="true" />
						</div>
					</div>

                    <div class="form-group" runat="server" ID="best" Visible="false">
						<label class="col-sm-2 control-label">베스트글 선정여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="notice" cssClass="form-control1" Checked="false"/>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">게시판 종류</label>
						<div class="col-sm-10">
							<asp:DropDownList runat="server" ID="sympathy_type" key="sympathy_type" class="form-control" style="width:150px;float:left;" AutoPostBack="true" OnSelectedIndexChanged="sympathy_type_SelectedIndexChanged"></asp:DropDownList>
						</div>
					</div>


                    <asp:PlaceHolder runat="server" ID="phNation" Visible="false">
                        <div class="form-group">
					        <label for="s_main" class="col-sm-2 control-label">국가</label>
					        <div class="col-sm-10">
					            <asp:DropDownList runat="server" ID="sa_nc_depth1" key="sa_nc_depth1" class="form-control" style="width:150px;float:left;" AutoPostBack="true" OnSelectedIndexChanged="sa_nc_depth1_SelectedIndexChanged"></asp:DropDownList>
					            <asp:DropDownList runat="server" ID="sa_nc_depth2" key="sa_nc_depth2" class="form-control" style="width:150px;float:left;margin-left:5px;margin-right:10px;"></asp:DropDownList>
                                <a id="btn_nation" class="btn btn-default pull-left form_nation" style="display:none;">추가하기</a>
					        </div>
				        </div>
                        
                        <div class="form-group form_nation" style="display:none;">
					        <label for="s_main" class="col-sm-2 control-label">선택 국가</label>
					        <div class="col-sm-10" style="margin-top:7px;" id="selected_nation_display">
					        </div>
				        </div>
                    </asp:PlaceHolder>


					<div class="form-group">
						<label class="col-sm-2 control-label control-label">제목</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=title CssClass="form-control title" Width=600 MaxLength="50"></asp:TextBox>
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-2 control-label control-label">추가정보</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=sub_title CssClass="form-control sub_title" Width=600></asp:TextBox>
                            꿈꾸는 어린이 : 글/사진<br />
                            꿈을 심는 후원자, 비전트립 다이어리 : 후원자<br />
                            함께쓰는 에세이 : 글쓴이
						</div>
					</div>
                    
                    
					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">썸네일</label>
						<div class="col-sm-10">
							<img id="thumb" class="thumb" runat=server src="/@mgt/common/img/empty_thumb.png" style="max-width:400px;" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10">
							<textarea name="body" id="body" runat="server" style="width:600px; height:212px; display:none;"></textarea>
                            <pre style="width:600px;">srook에서 코드복사를 하는 경우 컨텐츠 하단의 여백 조절을 위해<br />HTML모드에서 코드수정이 필요합니다.<br />수정 전 : &lt;div style="width:100%;<b style="color:red;">height:600px</b>;text-align:center;"&gt; <br />수정 후 : &lt;div style="width:100%;<b style="color:red;">height:20px</b>;text-align:center;"&gt;</pre>
                        </div>
					</div>

                    <asp:PlaceHolder runat="server" ID="phWriter" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">작성자</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="user_id"></asp:Label> / <asp:Label runat="server" ID="user_name"></asp:Label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">휴대폰번호</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="phone"></asp:Label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">이메일</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="email"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">주제</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="board_group_name"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>

                    <asp:PlaceHolder runat="server" ID="ph_regdate" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="reg_date"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>