﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_sympathy_bluebook_default : AdminBasePage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_update;


	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.sortColumn = sortColumn;
		base.sortDirection = sortDirection;
		base.OnAfterPostBack();

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected override void GetList(int page) {


		Master.IsSearch = false;
		if (
			s_keyword.Text != "" 
			|| s_b_date.Text != "" 
			|| s_e_date.Text != "") {
			Master.IsSearch = true;
		}


        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.sp_bluebook_apply_list(page, paging.RowsPerPage, s_keyword.Text.Trim(), s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_keyword.Text.Trim(), s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_bluebook_apply_list", op1, op2).DataTableToList<sp_bluebook_apply_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();


        }
	}
	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				sp_sympathy_listResult entity = e.Item.DataItem as sp_sympathy_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
			}
		}


		
	}
}
