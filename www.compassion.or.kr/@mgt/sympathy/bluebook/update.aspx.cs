﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_sympathy_bluebook_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");


		if (base.Action == "update") {

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.tBlueBook.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<tBlueBook>("idx", Convert.ToInt32(PrimaryKey));

                ConID.Text = entity.ConID;
                SponsorID.Text = entity.SponsorID;
                SponsorName.Text = entity.SponsorName;
                Tel.Text = entity.Tel;
                ZipCode.Text = entity.ZipCode;
                Address1.Text = entity.Address1;
                Address2.Text = entity.Address2;
                CommitInfo.Text = entity.CommitInfo;
                RegisterID.Text = entity.RegisterID;

                RegisterDate.Text = entity.RegisterDate.ToString("yyyy.MM.dd HH:mm");



            }
			
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			
		} else {
			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}



	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}


	
	protected void btn_update_click(object sender, EventArgs e) {

		var thumb = base.Thumb;
		var cookie = AdminLoginSession.GetCookie(this.Context);



        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tBlueBook.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<tBlueBook>("idx", Convert.ToInt32(PrimaryKey));

            entity.SponsorName = SponsorName.Text;
            entity.Tel = Tel.Text;
            entity.ZipCode = ZipCode.Text;
            entity.Address1 = Address1.Text;
            entity.Address2 = Address2.Text;
            entity.CommitInfo = CommitInfo.Text;


            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "수정되었습니다.";
            //dao.SubmitChanges();
            //string wClause = string.Format("idx = {0}", Convert.ToInt32(PrimaryKey));
            www6.update(entity);

            base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

        }

	}

	protected void btn_remove_click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.tBlueBook.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            //dao.tBlueBook.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from tBlueBook where idx = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

}