﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_sympathy_bluebook_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">

		$(function () {
		});
		
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label">ConID</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:Label runat="server" ID="ConID" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">스폰서ID</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:Label runat="server" ID="SponsorID" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">이름</label>
						<div class="col-sm-10" >
							<asp:TextBox runat="server" CssClass="form-control" style="width:150px;" ID="SponsorName" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">전화번호</label>
						<div class="col-sm-10" >
							<asp:TextBox runat="server" CssClass="form-control" style="width:150px;" ID="Tel" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">우편번호</label>
						<div class="col-sm-10" >
							<asp:TextBox runat="server" CssClass="form-control" style="width:150px;" ID="ZipCode" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">주소</label>
						<div class="col-sm-10">
							<asp:TextBox runat="server" CssClass="form-control" style="width:300px;" ID="Address1" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">나머지 주소</label>
						<div class="col-sm-10">
							<asp:TextBox runat="server" CssClass="form-control" style="width:300px;" ID="Address2" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">정보</label>
						<div class="col-sm-10">
							<asp:Label runat="server" ID="CommitInfo" CssClass="form-control" style="width:300px;"/>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">등록일</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:Label runat="server" ID="RegisterDate" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">아이디</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:Label runat="server" ID="RegisterID" />
						</div>
					</div>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>