﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="nation.aspx.cs" Inherits="mgt_sympathy_nation" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        .form-control {display:inline-block}
    </style>
	<script type="text/javascript">
		var new_nc_depth1_default = "1단계코드 입력";
		var new_nc_depth2_default = "2단계코드 입력";
		var new_nc_name_default = "국가명 입력";
		var defaultInputs = ["new_nc_depth1", "new_nc_depth2", "new_nc_name"];

		$(function () {
			
			$.each(defaultInputs, function () {
				
				var d = eval(this + "_default");
				$("#" + this).val(d);

				$("#" + this).focusin(function () {
					if ($(this).val() == d) {
						$(this).val("");
					}
				}).focusout(function () {
					if ($(this).val() == "") {
						$(this).val(d);
					}
				});
			});
			
		});

		var onAdd = function () {

			var success = true;
			$.each(defaultInputs, function () {
				var d = eval(this + "_default");
				if ($("#" + this).val() == "" || $("#" + this).val() == d) {
					alert(d);
					$("#" + this).focus();
					success = false;
					return false;
				}
			});
			
			if (!success) return false;
		}

		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">



     <div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>
		<div class="form-horizontal box-body" >
			<div class="box-body">

                <div class="form-group">
					<label for="s_target" class="col-sm-2 control-label">노출</label>
					<div class="col-sm-10" style="margin-top:5px;">
					    <asp:RadioButtonList runat="server" ID="s_display" RepeatDirection="Horizontal" RepeatLayout="Flow" OnSelectedIndexChanged="search" AutoPostBack="true">
						    <asp:ListItem Selected="True" Value="" Text="   전체   "></asp:ListItem>
						    <asp:ListItem Value="1" Text="   노출   "></asp:ListItem>
						    <asp:ListItem Value="0" Text="   미노출"></asp:ListItem>
					    </asp:RadioButtonList>
					</div>
				</div>

			</div>
			
		</div>
	</div><!-- /.box -->


    <div class="box box-primary">
		
		 <div class="box-header">
            <h3 class="box-title">총 <asp:Literal runat=server ID=lbTotal/> 건</h3>
            
        </div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
		
			<table class="table table-hover table-bordered ">
				<colgroup>
				    <col width="10%" />
				    <col width="10%" />
				    <col width="30%" />
				    <col width="5%" />
				    <col width="5%" />
				    <col width="5%" />
				    <col width="5%" />
				</colgroup>
				<thead>
					<tr>
				        <th>1단계코드</th>
				        <th>2단계코드</th>
				        <th>대륙or국가 명</th>
				        <th>노출여부</th>
				        <th>순서</th>
				        <th>적용횟수</th>
				        <th>삭제</th>
					</tr>
				</thead>
				<tbody>
			        <asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
				        <ItemTemplate>
					        <tr class="tr_link" style="cursor:default">
						        <td><asp:TextBox runat="server" ID="nc_depth1" Text=<%#Eval("nc_depth1") %> Width="100" CssClass="form-control"/></td>
						        <td><asp:TextBox runat="server" ID="nc_depth2" Text=<%#Eval("nc_depth2") %> Width="100" CssClass="form-control"/></td>
						        <td class="text-left"><%#!Eval("nc_depth2").ToString().Equals("") ? "<div style='width:50px;display:inline-block'>&nbsp;</div>" : "" %><asp:TextBox runat="server" ID="nc_name" Text=<%#Eval("nc_name") %> Width="200" CssClass="form-control"/></td>
						        <td><asp:CheckBox runat="server" ID="nc_display" Checked=<%#Eval("nc_display").ToString() == "True" %> /> </td>
						        <td><asp:TextBox runat="server" ID="nc_order" Width="50" Text=<%#Eval("nc_order") %> CssClass="form-control number_only"/> </td>
						        <td><asp:Literal runat="server" ID="lbContentCount"></asp:Literal></td>
						        <td>
                                    <asp:LinkButton runat="server" ID="btn_remove" CommandArgument=<%#Eval("nc_id") %> CssClass="btn btn-danger btn-sm" OnClick="btn_remove_Click" OnClientClick="return onRemove()">
                                        <i class="fa fa-remove"></i>삭제
                                    </asp:LinkButton>
						        </td>
					        </tr>
				        </ItemTemplate>
			        </asp:Repeater>		
				</tbody>
			</table>
		</div>
        
		<div class="box-footer clearfix text-center">
	        <uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="1000" PagesPerGroup="10" />
        </div>

        <div class="box-footer clearfix text-center">

            <div class="pull-right">

                <asp:TextBox runat="server" ID="new_nc_depth1" MaxLength="10" style="width:100px;height:32px;vertical-align:top"></asp:TextBox>
			    <asp:TextBox runat="server" ID="new_nc_depth2" MaxLength="10" style="width:100px;height:32px;vertical-align:top"></asp:TextBox>
			    <asp:TextBox runat="server" ID="new_nc_name" MaxLength="50" style="width:100px;height:32px;vertical-align:top"></asp:TextBox>
			    <asp:LinkButton runat="server" ID="btn_add" OnClick="btn_add_Click" OnClientClick="return onAdd()" CssClass="btn btn-danger" Text="등록"></asp:LinkButton>
			    <asp:LinkButton runat="server" ID="btn_update" CssClass="btn btn-bitbucket" OnClick="btn_update_Click" Text="업데이트"></asp:LinkButton>
			</div>

        </div>


	</div>








</asp:Content>