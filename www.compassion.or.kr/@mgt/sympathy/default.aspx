﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_sympathy_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
	    $(function () {
	        
			
		});

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>
		<div class="form-horizontal box-body" >
			<div class="box-body">

                <div class="form-group">
					<label for="s_type" class="col-sm-2 control-label">게시판 종류</label>
					<div class="col-sm-10">
					    <asp:DropDownList runat="server" ID="board_id" key="board_id" class="form-control" style="width:150px;float:left;" AutoPostBack="true" OnSelectedIndexChanged="search"></asp:DropDownList>
					</div>
				</div>
                
                <div class="form-group">
					<label for="s_main" class="col-sm-2 control-label">국가별로보기</label>
					<div class="col-sm-10">
					    <asp:DropDownList runat="server" ID="sa_nc_depth1" key="sa_nc_depth1" class="form-control" style="width:150px;float:left;" AutoPostBack="true" OnSelectedIndexChanged="sa_nc_depth1_SelectedIndexChanged"></asp:DropDownList>
					    <asp:DropDownList runat="server" ID="sa_nc_depth2" key="sa_nc_depth2" class="form-control" style="width:150px;float:left;margin-left:5px;" AutoPostBack="true" OnSelectedIndexChanged="search"></asp:DropDownList>
					</div>
				</div>

				<div class="form-group">
					<label for="s_keyword" class="col-sm-2 control-label">검색</label>
					<div class="col-sm-10">
                        <asp:TextBox runat="server" ID="s_keyword" key="k"  Width="415" cssClass="form-control inline" OnTextChanged="search" AutoPostBack="true"></asp:TextBox>
					    * 제목 , 내용
					</div>
				</div>



				<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">기간 검색(등록일)</label>
					<div class="col-sm-10">
						<div class="inline">
							<asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							~
						    <asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>

						<div class="inline">
							<button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							<div class="btn-group">
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								<a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								<a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							</div>
						</div>

					</div>

				</div>

				<div class="box-footer clearfix text-center">
                    <asp:LinkButton runat="server" ID="btn_search" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
				</div>
				
			</div>
			
		</div>
	</div><!-- /.box -->




    <div class="box box-primary">
		
		 <div class="box-header">
            <h3 class="box-title">총 <asp:Literal runat=server ID=lbTotal/> 건</h3>
            
        </div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
		
			<table class="table table-hover table-bordered ">
				<thead>
					<tr>
				        <th>번호</th>
                        <th>게시판</th>
				        <th>제목</th>
                        <th>지역</th>
                        <th>국가</th>
                        <th>조회수</th>
                        <th>노출</th>
				        <th>등록일</th>
					</tr>
				</thead>
				<tbody>
			        <asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
				        <ItemTemplate>
					        <tr class="tr_over">
						        <td><asp:Literal runat=server ID=lbIdx></asp:Literal></td>
                                <td><%#Eval("board_name")%></td>
						        <td class="text-left"><a href="#" onclick="goPage('update.aspx' , { c: <%#Eval("idx")%>, p: '<%#paging.CurrentPage %>', t: 'update' });return false;" style="color:black"><%#Eval("title")%></a></td>
                                <td><%#Eval("depth1_name")%></td>
                                <td><%#Eval("sa_nc_text")%></td>
						        <td><%#Eval("view_count")%></td>
                                <td><%#(bool)Eval("yn1") ? "X" : "O"%></td>
						        <td><%#Eval("reg_date" , "{0:yyyy/MM/dd}")%></td>
					        </tr>	
				        </ItemTemplate>
						 <FooterTemplate>
							 <tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								 <td colspan="8">데이터가 없습니다.</td>
							 </tr>
						</FooterTemplate>
			        </asp:Repeater>			


				</tbody>
			</table>
		</div>
        
		<div class="box-footer clearfix text-center">
            <uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
        </div>

        <div class="box-footer clearfix text-center">
            <asp:LinkButton runat="server" href="#" ID="btn_add" OnClientClick="goPage('update.aspx' , {t:'insert'})" CssClass="btn btn-bitbucket pull-right">신규등록</asp:LinkButton>
        </div>


	</div>

</asp:Content>
