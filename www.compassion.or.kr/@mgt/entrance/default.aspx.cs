﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using Newtonsoft.Json.Linq;

public partial class mgt_entrance_default : AdminBasePage {
    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.content.First(p => p.c_id == "main");
            //string sqlStr = string.Format("select * from content where c_id = {0}", "main");
            var entity = www6.selectQF<content>("c_id", "main");

            var data = JObject.Parse(entity.c_text.ToString());

            child.Text = data["child"].ToString();
            content.Text = data["content"].ToString();

        }

    }


    protected void btn_update_Click( object sender, EventArgs e ) {

        Dictionary<string, string> value = new Dictionary<string, string>();
        value.Add("child", child.Text);
        value.Add("content", content.Text);
        
        
        var main = value.ToJson();

        var list = new content();
        list.c_id = "main";
        list.c_text = main;

        list.c_regdate = DateTime.Now;
        list.c_a_id = AdminLoginSession.GetCookie(this.Context).identifier;

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.content.First(p => p.c_id == "main");
            var entity = www6.selectQF<content>("c_id", "main");

            entity.c_text = main;
            entity.c_regdate = DateTime.Now;
            entity.c_a_id = AdminLoginSession.GetCookie(this.Context).identifier;

            if (!(entity.c_id == "main"))
            {
                //dao.content.InsertOnSubmit(list);
                www6.insert(list);
            }

            //dao.SubmitChanges();
            www6.update(entity);
        }

        Master.ValueAction.Value = "none";
        Master.ValueMessage.Value = "적용되었습니다.";

    }
}