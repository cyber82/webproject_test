﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_entrance_default" MasterPageFile="~/@mgt/mgt_afterlogin.master" %>

<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <script src="/@mgt/common/js/flot/jquery.flot.min.js"></script>
    <script src="/@mgt/common/js/flot/jquery.flot.pie.min.js"></script>

    <script type="text/javascript">

        var onSubmit = function () {

            if (!validateForm([
				{ id: "#child", msg: "어린이 수를 입력해 주세요" },
				{ id: "#content", msg: "내용을 입력해 주세요" },
				
            ])) {
                return false;
            }

            return confirm("등록 하시겠습니까?");
        }

        $(function () {


        })
    </script>


</asp:Content>


<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <div class="nav-tabs-custom">

        <div class="tab-content">

            <div class="active tab-pane" id="tab1" runat="server">
                <div class="form-horizontal">

                    <div class="col-xs-12">
                        <h2 class="page-header"><i class="fa fa-plus-square"></i>메인화면관리</h2>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">어린이 수</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="child" MaxLength="20" class="number_only" CssClass="form-control" Width="250" placeholder="후원을 기다리고 있는 어린이 수"></asp:TextBox>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">내용<br />
                            </label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="content" TextMode="MultiLine" Rows="10" Width="600" CssClass="form-control"></asp:TextBox>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="box-footer clearfix text-center">
        <asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_Click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" Style="margin-right: 5px">등록</asp:LinkButton>
    </div>

</asp:Content>
