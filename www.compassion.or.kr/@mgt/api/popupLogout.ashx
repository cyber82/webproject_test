﻿<%@ WebHandler Language="C#" Class="api_popupLogout" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;


public class api_popupLogout : BaseHandler
{
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();
    
    public override void OnRequest(HttpContext context)
    {
        Auth auth = new Auth();

        auth.Logout(); 
    }
        

}
