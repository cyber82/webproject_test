﻿<%@ WebHandler Language="C#" Class="api_visiontrip_apply" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

public class api_visiontrip_apply : BaseHandler
{
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();
    //CommonLib.WWW6Service.SoaHelperSoap _www6Service;
    CommonLib.WWWService.ServiceSoap _wwwService;
    CommonLib.WWW6Service.SoaHelperSoap _www6Service;

    public override void OnRequest(HttpContext context)
    {
        var t = context.Request["t"].EmptyIfNull();
        var list = context.Request["list"].EmptyIfNull();

        if (t == "childlist")
        {
            this.GetChildList(context);
        }
        else if (t == "add_planapply")
        {
            this.Add_PlanApply(context);
        }
        else if (t == "add_individualapply")
        {
            this.Add_IndividualApply(context);
        }
    }
    public class Child
    {
        public string childkey;
        public string name;
        public string countrycode;
    }

    void GetChildList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var sponserid = context.Request["sponserid"].EmptyIfNull();
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();


        using (AdminDataContext dao = new AdminDataContext())
        {
            //후원어린이 정보
            Object[] objParam = new object[] { "page", "rowsPerPage", "sponsorId", "childKey", "orderby", "checkLetter" };
            Object[] objValue = new object[] { 1, 10000, sponserid, "", "", 0 };
            Object[] objSql = new object[] { "sp_web_mychild_list_f" };
            DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

            var data = new List<Child>();


            //DataTable dtNew = new DataTable();
            //dtNew.Columns.Add("childkey");
            //dtNew.Columns.Add("name");
            if(dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["SponsorTypeEng"].ToString() == "CHIMON" || dr["SponsorTypeEng"].ToString() == "CHISPO")
                    {
                        data.Add(new Child()
                        {
                            childkey = dr["ChildKey"].ToString(),
                            name = dr["Name"].ToString(),
                            countrycode = dr["CountryCode"].ToString()
                        });
                    }
                    //dtNew.Rows.Add(dr["ChildKey"].ToString(), dr["Name"].ToString());
                }
            }

            result.data = data;
        }

        JsonWriter.Write(result, context);
    }

    void Add_PlanApply(HttpContext context)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        JsonWriter result = new JsonWriter();
        result.success = true;
        result.data = 0;

        #region param
        var scheduleID = context.Request["scheduleID"].EmptyIfNull();
        var applyType = context.Request["applyType"].EmptyIfNull();

        var sponsorID = context.Request["sponsorID"].EmptyIfNull();
        var userID = context.Request["userID"].EmptyIfNull();
        var sponsorName = context.Request["sponsorName"].EmptyIfNull();
        var genderCode = context.Request["genderCode"].EmptyIfNull();
        var birthDate = context.Request["birthDate"].EmptyIfNull();

        var sponsorNameEng = context.Request["sponsorNameEng"].EmptyIfNull();
        var tel = context.Request["tel"].EmptyIfNull();
        var email = context.Request["email"].EmptyIfNull();
        //국내외 
        var location = context.Request["location"].EmptyIfNull();
        var country = context.Request["country"].EmptyIfNull();

        var address1 = context.Request["address1"].EmptyIfNull();
        var address2 = context.Request["address2"].EmptyIfNull();
        var zipcode = context.Request["zipcode"].EmptyIfNull();
        var changeYn = context.Request["changeYn"].EmptyIfNull();
        var religion = context.Request["religion"].EmptyIfNull();
        var church = context.Request["church"].EmptyIfNull();
        var emergencycontactName = context.Request["emergencycontactName"].EmptyIfNull();
        var emergencycontactTel = context.Request["emergencycontactTel"].EmptyIfNull();
        var emergencycontactRelation = context.Request["emergencycontactRelation"].EmptyIfNull();
        var childmeetYn = context.Request["childmeetYn"].EmptyIfNull();
        var scheduleagreeYn = context.Request["scheduleagreeYn"].EmptyIfNull();
        var nation = context.Request["nation"].EmptyIfNull();
        var englishLevel = context.Request["englishLevel"].EmptyIfNull();
        var visiontripHistory = context.Request["visiontripHistory"].EmptyIfNull();
        var job = context.Request["job"].EmptyIfNull();
        var military = context.Request["military"].EmptyIfNull();
        var roomType = context.Request["roomType"].EmptyIfNull();
        var roomDetail = context.Request["roomDetail"].EmptyIfNull();
        var cashreceiptType = context.Request["cashreceiptType"].EmptyIfNull();
        var cashreceiptName = context.Request["cashreceiptName"].EmptyIfNull();
        var cashreceiptTel = context.Request["cashreceiptTel"].EmptyIfNull();
        var cashreceiptRelation = context.Request["cashreceiptRelation"].EmptyIfNull();
        var acceptTerms = context.Request["acceptTerms"].EmptyIfNull();
        var applyAgree = context.Request["applyAgree"].EmptyIfNull();
        var childList = context.Request["childList"].EmptyIfNull();
        var attachFile = context.Request["attachFile"].EmptyIfNull();
        #endregion

        if (scheduleID == "")
        {
            result.success = false;
            result.message = "신청서 정보가 잘못되었습니다.";
            JsonWriter.Write(result, context);
            return;
        }


        using (FrontDataContext dao = new FrontDataContext())
        {
            #region Compass - tSponsorGroup : GroupType 가져오기 
            Object[] objSql = new object[1] { "VT_WebInfoInquiry" };
            Object[] objParam = new object[] { "DIVIS", "ConID" };
            Object[] objValue = new object[] { "GroupInfo", sponsorID };

            var grouptype = "";
            var grouplist = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
            if (grouplist.Rows.Count > 0)
            {
                grouptype = grouplist.Rows[0]["GroupType"].ToString();
            }
            #endregion

            #region 후원어린이 수 
            int iCommitmentCount = 0;
            iCommitmentCount = getCommitmentCount(sponsorID);
            #endregion

            //var list = dao.tVisionTripApply.Where(p => p.ScheduleID == Convert.ToInt32(scheduleID) && p.SponsorID == sponsorID && p.CurrentUse == 'Y').FirstOrDefault();
            var list = www6.selectQF<tVisionTripApply>("ScheduleID", Convert.ToInt32(scheduleID), "SponsorID", sponsorID, "CurrentUse", "Y");
            if (list != null)
            {
                result.success = false;
                result.message = "이미 신청하셨습니다.";
                JsonWriter.Write(result, context);
                return;
            }
            #region 신청비 가져오기
            //var scheduleData = dao.tVisionTripSchedule.Where(p => p.ScheduleID == Convert.ToInt32(scheduleID) && p.CurrentUse == 'Y').FirstOrDefault();
            var scheduleData = www6.selectQF<tVisionTripSchedule>("ScheduleID", Convert.ToInt32(scheduleID), "CurrentUse", "Y");
            decimal requestCost = 0;
            if (scheduleData != null)
            {
                if (scheduleData.RequestCost != null)
                {
                    bool check = decimal.TryParse(scheduleData.RequestCost, out requestCost);
                }
            }
            #endregion

            var vtApplyList = new List<tVisionTripApply>();
            vtApplyList.Add(new tVisionTripApply()
            {
                ScheduleID = Convert.ToInt32(scheduleID),
                ApplyType = applyType,
                SponsorID = sponsorID,
                UserID = userID,
                SponsorName = sponsorName,
                SponsorNameEng = sponsorNameEng,
                GenderCode = genderCode,
                BirthDate = birthDate != "" ? birthDate.Encrypt():"",
                Tel = tel.Encrypt(),
                Email = email.Encrypt(),
                LocationType = location,
                Address1 = (address1 + "$" + country).Encrypt(),
                Address2 = address2.Encrypt(),
                ZipCode = zipcode.Encrypt(),
                ChangeYN = changeYn != "Y" ? 'N' : 'Y',
                ReligionType = religion,
                ChurchName = church,
                EmergencyContactName = emergencycontactName,
                EmergencyContactTel = emergencycontactTel,
                EmergencyContactRelation = emergencycontactRelation,
                ChildMeetYN = childmeetYn != "Y" ? 'N' : 'Y',
                ApplyDate = DateTime.Now,
                CompassYN = 'N',
                GroupType = grouptype,
                CommitmentCount = iCommitmentCount, //후원어린이 수
                CurrentUse = 'Y',
                RegisterDate = DateTime.Now,
                RegisterName = AdminLoginSession.GetCookie(context).name,
                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
            });

            //dao.tVisionTripApply.InsertAllOnSubmit(vtApplyList);
            foreach (var arg in vtApplyList)
            {
                www6.insert(arg);
            }

            //dao.SubmitChanges();

            int iApplyID = 0;
            iApplyID = vtApplyList[0].ApplyID;
            try
            {
                if (iApplyID > 0)
                {
                    #region 상세
                    var vtPlanDetail = new List<tVisionTripPlanDetail>();
                    vtPlanDetail.Add(new tVisionTripPlanDetail()
                    {
                        ApplyID = iApplyID,
                        ScheduleAgreeYN = scheduleagreeYn != "Y" ? 'N' : 'Y',
                        Nation = Convert.ToChar(nation),
                        EnglishLevel = Convert.ToChar(englishLevel),
                        VisionTripHistory = Convert.ToChar(visiontripHistory),
                        Job = job,
                        MilitaryYN = Convert.ToChar(military),
                        RoomType = Convert.ToChar(roomType),
                        RoomDetail = roomDetail,
                        CashReceiptType = Convert.ToChar(cashreceiptType),
                        CashReceiptName = cashreceiptName,
                        CashReceiptTel = cashreceiptTel,
                        CashReceiptRelation = cashreceiptRelation,
                        AcceptTerms = "1111111",
                        ApplyAgree = 'Y',
                        ApplyState = 'A',
                        RequestCost = requestCost
                    });
                    //dao.tVisionTripPlanDetail.InsertAllOnSubmit(vtPlanDetail);
                    foreach (var arg in vtPlanDetail)
                    {
                        www6.insert(arg);
                    }
                    #endregion

                    #region 후원어린이정보
                    if (childList != "")
                    {
                        //dynamic results = JsonConvert.DeserializeObject<dynamic>(childList);  
                        var vtChildMeetList = new List<tVisionTripChildMeet>();
                        JArray jResult = JArray.Parse(childList);
                        for (int i = 0; i < jResult.Count; i++)
                        {
                            string childKey = jResult[i]["id"].ToString();
                            string childName = jResult[i]["name"].ToString();

                            vtChildMeetList.Add(new tVisionTripChildMeet()
                            {
                                ApplyID = iApplyID,
                                ChildKey = childKey,
                                ChildName = childName,
                                ChildMeetType = childName == "" ? 'I' : 'S', //(I 직접입력, S 선택) 
                                CurrentUse = 'Y',
                                RegisterDate = DateTime.Now,
                                RegisterName = AdminLoginSession.GetCookie(context).name,
                                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                            });
                        }
                        //dao.tVisionTripChildMeet.InsertAllOnSubmit(vtChildMeetList);
                        foreach (var arg in vtChildMeetList)
                        {
                            www6.insert(arg);
                        }
                    }
                    #endregion
                    #region 첨부파일
                    if (attachFile != "")
                    {
                        var vtAttachList = new List<tVisionTripAttach>();

                        JArray jResult = JArray.Parse(attachFile);
                        for (int i = 0; i < jResult.Count; i++)
                        {
                            string attachType = jResult[i]["type"].ToString();
                            string attachName = jResult[i]["name"].ToString();
                            string attachPath = jResult[i]["path"].ToString();

                            vtAttachList.Add(new tVisionTripAttach()
                            {
                                ApplyID = iApplyID,
                                AttachType = attachType,
                                AttachName = attachName,
                                AttachPath = attachPath,
                                CurrentUse = 'Y',
                                RegisterDate = DateTime.Now,
                                RegisterName = AdminLoginSession.GetCookie(context).name,
                                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                            });
                        }
                        //dao.tVisionTripAttach.InsertAllOnSubmit(vtAttachList);
                        foreach (var arg in vtAttachList)
                        {
                            www6.insert(arg);
                        }
                    }
                    #endregion

                }

                #region 사용자 정보 변경
                if (changeYn.Equals("Y"))
                {
                    try
                    {
                        //WEB
                        using (AuthLibDataContext daoAuth = new AuthLibDataContext())
                        {
                            //var entity = daoAuth.tSponsorMaster.First(p => p.SponsorID == sponsorID);
                            var entity = www6.selectQFAuth<tSponsorMaster>("SponsorID", sponsorID);
                            if (!string.IsNullOrEmpty(email))
                                entity.Email = email.Encrypt();
                            if (!string.IsNullOrEmpty(tel))
                                entity.Phone = tel.Encrypt();

                            //daoAuth.SubmitChanges();
                            www6.updateAuth(entity);
                        }

                        // 후원회원인경우 
                        if (!string.IsNullOrEmpty(sponsorID))
                        {
                            var userInfoResult = UpdateCompassUserInfo(userID, sponsorID, sponsorName, location, country, address1, address2, zipcode, tel, email);
                            if (!userInfoResult.success)
                            {
                                result.success = false;
                                result.message = userInfoResult.message;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                        result.success = false;
                        result.message = "사용자 정보를 수정하는 중 오류가 발생했습니다.(WEB)";
                        //return;
                    }
                }
                #endregion

                //dao.SubmitChanges();

                result.data = iApplyID;
            }
            catch (Exception ex)
            {
                using (FrontDataContext daoDel = new FrontDataContext())
                {
                    //   iApplyID
                    //var entity = daoDel.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                    //daoDel.tVisionTripApply.DeleteOnSubmit(entity);
                    //daoDel.SubmitChanges();
                    //www6.cud(MakeSQL.delQ(0, "tVisionTripApply", "ApplyID", Convert.ToInt32(iApplyID)));
                    var entity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(iApplyID));
                    www6.delete(entity);
                }
                ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                result.success = false;
                result.message = "1. 예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
            }
        }
        JsonWriter.Write(result, context);
    }



    void Add_IndividualApply(HttpContext context)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        JsonWriter result = new JsonWriter();
        result.success = true;
        result.data = 0;

        #region param 
        var applyType = context.Request["applyType"].EmptyIfNull();

        var sponsorID = context.Request["sponsorID"].EmptyIfNull();
        var userID = context.Request["userID"].EmptyIfNull();
        var sponsorName = context.Request["sponsorName"].EmptyIfNull();
        var genderCode = context.Request["genderCode"].EmptyIfNull();
        var birthDate = context.Request["birthDate"].EmptyIfNull();


        var sponsorNameEng = context.Request["sponsorNameEng"].EmptyIfNull();
        var tel = context.Request["tel"].EmptyIfNull();
        var email = context.Request["email"].EmptyIfNull();
        //국내외 
        var location = context.Request["location"].EmptyIfNull();
        var country = context.Request["country"].EmptyIfNull();

        var address1 = context.Request["address1"].EmptyIfNull();
        var address2 = context.Request["address2"].EmptyIfNull();
        var zipcode = context.Request["zipcode"].EmptyIfNull();
        var changeYn = context.Request["changeYn"].EmptyIfNull();
        var religion = context.Request["religion"].EmptyIfNull();
        var church = context.Request["church"].EmptyIfNull();
        var emergencycontactName = context.Request["emergencycontactName"].EmptyIfNull();
        var emergencycontactTel = context.Request["emergencycontactTel"].EmptyIfNull();
        var emergencycontactRelation = context.Request["emergencycontactRelation"].EmptyIfNull();

        var visitType = context.Request["visitType"].EmptyIfNull();

        var visitDate1 = context.Request["visitDate1"].EmptyIfNull();
        var visitDate2 = context.Request["visitDate2"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();
        var localAccommodation = context.Request["localAccommodation"].EmptyIfNull();
        var localTel = context.Request["localTel"].EmptyIfNull();
        var localAddress = context.Request["localAddress"].EmptyIfNull();
        var departureDate = context.Request["departureDate"].EmptyIfNull();
        var returnDate = context.Request["returnDate"].EmptyIfNull();

        var acceptTerms = context.Request["acceptTerms"].EmptyIfNull();
        var applyAgree = context.Request["applyAgree"].EmptyIfNull();
        //json
        var childList = context.Request["childList"].EmptyIfNull();
        var companionList = context.Request["companionList"].EmptyIfNull();
        var attachFile = context.Request["attachFile"].EmptyIfNull();
        #endregion 

        using (FrontDataContext dao = new FrontDataContext())
        {

            #region Compass - tSponsorGroup : GroupType 가져오기 
            Object[] objSql = new object[1] { "VT_WebInfoInquiry" };
            Object[] objParam = new object[] { "DIVIS", "ConID" };
            Object[] objValue = new object[] { "GroupInfo", sponsorID };

            var grouptype = "";
            var list = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
            if (list.Rows.Count > 0)
            {
                grouptype = list.Rows[0]["GroupType"].ToString();
            }
            #endregion
            #region 후원어린이 수 
            int iCommitmentCount = 0;
            iCommitmentCount = getCommitmentCount(sponsorID);
            #endregion

            var vtApplyList = new List<tVisionTripApply>();
            vtApplyList.Add(new tVisionTripApply()
            {
                ApplyType = applyType,
                UserID = userID,
                SponsorName = sponsorName,
                SponsorNameEng = sponsorNameEng,
                SponsorID = sponsorID,
                GenderCode = genderCode,
                BirthDate = birthDate != "" ? birthDate.Encrypt():"",
                Tel = tel.Encrypt(),
                Email = email.Encrypt(),
                LocationType = location,
                Address1 = (address1 + "$" + country).Encrypt(),
                Address2 = address2.Encrypt(),
                ZipCode = zipcode.Encrypt(),
                ChangeYN = changeYn != "Y" ? 'N' : 'Y',
                ReligionType = religion,
                ChurchName = church,
                EmergencyContactName = emergencycontactName,
                EmergencyContactTel = emergencycontactTel,
                EmergencyContactRelation = emergencycontactRelation,
                ApplyDate = DateTime.Now,
                CompassYN = 'N',
                GroupType = grouptype,
                CommitmentCount = iCommitmentCount, //후원어린이 수
                CurrentUse = 'Y',
                RegisterDate = DateTime.Now,
                RegisterName = AdminLoginSession.GetCookie(context).name,
                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
            });

            //dao.tVisionTripApply.InsertAllOnSubmit(vtApplyList);
            foreach (var arg in vtApplyList)
            {
                www6.insert(arg);
            }
            //dao.SubmitChanges();

            int iApplyID = 0;
            iApplyID = vtApplyList[0].ApplyID;

            try
            {
                #region 상세 
                var vtIndividualDetail = new List<tVisionTripIndividualDetail>();
                vtIndividualDetail.Add(new tVisionTripIndividualDetail()
                {
                    ApplyID = iApplyID,
                    VisitType = Convert.ToChar(visitType),
                    VisitDate1 = visitDate1,
                    VisitDate2 = visitDate2,
                    VisitCountry = visitCountry,
                    LocalAccommodation = localAccommodation,
                    LocalTel = localTel,
                    LocalAddress = localAddress,
                    DepartureDate = departureDate,
                    ReturnDate = returnDate,
                    AcceptTerms = "111111"
                });
                //dao.tVisionTripIndividualDetail.InsertAllOnSubmit(vtIndividualDetail);
                foreach (var arg in vtIndividualDetail)
                {
                    www6.insert(arg);
                }
                //dao.SubmitChanges();

                int iIndividualDetailID = 0;
                iIndividualDetailID = vtIndividualDetail[0].IndividualDetailID;

                #endregion
                if (iApplyID > 0 && iIndividualDetailID > 0)
                {
                    #region 동반인정보
                    if (companionList != "")
                    {
                        var vtCompanion = new List<tVisionTripCompanion>();
                        JArray jResult = JArray.Parse(companionList);
                        for (int i = 0; i < jResult.Count; i++)
                        {
                            string cNameKor = jResult[i]["name_kor"].ToString();
                            string cNameEng = jResult[i]["name_eng"].ToString();
                            string cBirth = jResult[i]["birth"].ToString();
                            string cGender = jResult[i]["gender"].ToString();
                            if (cNameKor != "" && cNameEng != "" && cBirth != "" && cGender != "")
                            {
                                vtCompanion.Add(new tVisionTripCompanion()
                                {
                                    IndividualDetailID = iIndividualDetailID,
                                    CompanionName = cNameKor,
                                    CompanionNameEng = cNameEng,
                                    BirthDate = cBirth,
                                    GenderCode = Convert.ToChar(cGender),
                                    CurrentUse = 'Y',
                                    RegisterDate = DateTime.Now,
                                    RegisterName = AdminLoginSession.GetCookie(context).name,
                                    RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                                });
                            }
                        }

                        //dao.tVisionTripCompanion.InsertAllOnSubmit(vtCompanion);
                        foreach (var arg in vtCompanion)
                        {
                            www6.insert(arg);
                        }
                    }
                    #endregion

                    #region 후원어린이정보
                    if (childList != "")
                    {
                        var vtChildMeetList = new List<tVisionTripChildMeet>();
                        JArray jResult = JArray.Parse(childList);
                        for (int i = 0; i < jResult.Count; i++)
                        {
                            string childKey = jResult[i]["id"].ToString();
                            string childName = jResult[i]["name"].ToString();

                            vtChildMeetList.Add(new tVisionTripChildMeet()
                            {
                                ApplyID = iApplyID,
                                ChildKey = childKey,
                                ChildName = childName,
                                ChildMeetType = childName == "" ? 'I' : 'S', //(I 직접입력, S 선택)
                                CurrentUse = 'Y',
                                RegisterDate = DateTime.Now,
                                RegisterName = AdminLoginSession.GetCookie(context).name,
                                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                            });
                        }

                        //dao.tVisionTripChildMeet.InsertAllOnSubmit(vtChildMeetList);
                        foreach (var arg in vtChildMeetList)
                        {
                            www6.insert(arg);
                        }
                    }
                    #endregion

                    #region 첨부파일
                    if (attachFile != "")
                    {
                        var vtAttachList = new List<tVisionTripAttach>();

                        JArray jResult = JArray.Parse(attachFile);
                        for (int i = 0; i < jResult.Count; i++)
                        {
                            string attachType = jResult[i]["type"].ToString();
                            string attachName = jResult[i]["name"].ToString();
                            string attachPath = jResult[i]["path"].ToString();

                            vtAttachList.Add(new tVisionTripAttach()
                            {
                                ApplyID = iApplyID,
                                AttachType = attachType,
                                AttachName = attachName,
                                AttachPath = attachPath,
                                CurrentUse = 'Y',
                                RegisterDate = DateTime.Now,
                                RegisterName = AdminLoginSession.GetCookie(context).name,
                                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                            });
                        }

                        //dao.tVisionTripAttach.InsertAllOnSubmit(vtAttachList);
                        foreach (var arg in vtAttachList)
                        {
                            www6.insert(arg);
                        }
                    }
                    #endregion

                }
                else
                {
                    using (FrontDataContext daoDel = new FrontDataContext())
                    {
                        //   iApplyID
                        //var entity = daoDel.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                        //daoDel.tVisionTripApply.DeleteOnSubmit(entity);
                        //daoDel.SubmitChanges();
                        //www6.cud(MakeSQL.delQ(0, "tVisionTripApply", "ApplyID", Convert.ToInt32(iApplyID)));
                        var entity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(iApplyID));
                        www6.delete(entity);

                        //   iApplyID

                        //var entity_individual = daoDel.tVisionTripIndividualDetail.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                        //daoDel.tVisionTripIndividualDetail.DeleteOnSubmit(entity_individual);
                        //daoDel.SubmitChanges();
                        //www6.cud(MakeSQL.delQ(0, "tVisionTripIndividualDetail", "ApplyID", Convert.ToInt32(iApplyID)));
                        var entity2 = www6.selectQF<tVisionTripIndividualDetail>("ApplyID", Convert.ToInt32(iApplyID));
                        www6.delete(entity2);
                    }

                    result.success = false;
                    result.message = "2. 예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
                }
                //dao.SubmitChanges();


                #region 사용자 정보 변경
                if (changeYn.Equals("Y"))
                {
                    try
                    {
                        //WEB
                        using (AuthLibDataContext daoAuth = new AuthLibDataContext())
                        {
                            //var entity = daoAuth.tSponsorMaster.First(p => p.UserID == sponsorID);
                            var entity = www6.selectQFAuth<tSponsorMaster>("UserID", sponsorID);

                            if (!string.IsNullOrEmpty(email))
                                entity.Email = email.Encrypt();
                            if (!string.IsNullOrEmpty(tel))
                                entity.Phone = tel.Encrypt();

                            //daoAuth.SubmitChanges();
                            www6.updateAuth(entity);
                        }
                        // 후원회원인경우 
                        if (!string.IsNullOrEmpty(sponsorID))
                        {
                            var userInfoResult = UpdateCompassUserInfo(userID, sponsorID, sponsorName, location, country, address1, address2, zipcode, tel, email);
                            if (!userInfoResult.success)
                            {
                                result.success = false;
                                result.message = userInfoResult.message;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                        result.success = false;
                        result.message = "회원님의 정보를 수정하는 중 오류가 발생했습니다.(WEB)";
                        //return;
                    }
                }
                #endregion

                result.data = iApplyID;
            }
            catch (Exception ex)
            {
                using (FrontDataContext daoDel = new FrontDataContext())
                {
                    //   iApplyID
                    //var entity = daoDel.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                    //daoDel.tVisionTripApply.DeleteOnSubmit(entity);
                    //daoDel.SubmitChanges();
                    //www6.cud(MakeSQL.delQ(0, "tVisionTripApply", "ApplyID", Convert.ToInt32(iApplyID)));
                    var entity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(iApplyID));
                    www6.delete(entity);
                    //   iApplyID

                    //var entity_individual = daoDel.tVisionTripIndividualDetail.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                    //daoDel.tVisionTripIndividualDetail.DeleteOnSubmit(entity_individual);
                    //daoDel.SubmitChanges();
                    //www6.cud(MakeSQL.delQ(0, "tVisionTripIndividualDetail", "ApplyID", Convert.ToInt32(iApplyID)));
                    var entity_individual = www6.selectQF<tVisionTripIndividualDetail>("ApplyID", Convert.ToInt32(iApplyID));
                    www6.delete(entity_individual);
                }
                ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                result.success = false;
                result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
            }
        }
        JsonWriter.Write(result, context);
    }





    //후원 어린이 수 
    protected int getCommitmentCount(string sponserid)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        int iCommitmentCount = 0;
        try
        {
            var objSql = new object[1] { "  SELECT COUNT(*) AS CommitmentCount " +
         "  FROM tCommitmentMaster ComM WITH (NOLOCK) " +
         "  LEFT OUTER JOIN tSponsorMaster SM WITH (NOLOCK) ON ComM.SponsorID = SM.SponsorID " +
         "  LEFT OUTER JOIN tChildMaster CM WITH (NOLOCK) ON ComM.ChildMasterID = CM.ChildMasterID " +
         "  WHERE ComM.SponsorID = '" + sponserid + "' " +
         "  AND ComM.SponsorItemEng IN ('DS', 'LS') " +
         "  AND (ComM.StopDate IS NULL OR ComM.StopDate > GETDATE()) " };

            DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

            if (ds2 == null || ds2.Tables.Count == 0 || ds2.Tables[0].Rows.Count == 0)
                iCommitmentCount = 0;
            else
                iCommitmentCount = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            iCommitmentCount = 0;
        }

        return iCommitmentCount;
    }


    JsonWriter UpdateCompassUserInfo(string userid, string sponsorId, string username, string location, string country, string addr1, string addr2, string zipcode, string mobile, string email)
    {
        //UserInfo sess = new UserInfo();

        JsonWriter result = new JsonWriter() { success = true, action = "" };
        var actionResult = new JsonWriter();

        if (!string.IsNullOrEmpty(location) && !string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(zipcode) && !string.IsNullOrEmpty(addr1) && !string.IsNullOrEmpty(addr2))
        {
            #region 주소수정
            actionResult = new SponsorAction().GetAddress();
            if (!actionResult.success)
            {
                result.message = actionResult.message;
                return result;
            }
            SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)actionResult.data;

            actionResult = new SponsorAction().UpdateAddress(true, userid, sponsorId, username, addr_data.AddressType, location, country, zipcode, addr1, addr2);
            if (!actionResult.success)
            {
                result.message = actionResult.message;
                return result;
            }
            #endregion
        }

        #region 이메일 & 휴대전화 수정
        var comm_result = new SponsorAction().UpdateCommunications(true, sponsorId, email, mobile, null);
        if (!comm_result.success)
        {
            result.message = comm_result.message;
            return result;
        }
        #endregion

        return result;
    }























    //트립명
    void GetTripName(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list =  dao.tVisionTripSchedule.Where(p => p.CurrentUse == 'Y'
            //        && p.StartDate.Substring(0, 4) == tripYear && (tripType == "" || p.VisionTripType == tripType)).OrderBy(p => p.VisitCountry).ToList();
            var list = www6.selectQ<tVisionTripSchedule>().Where(p => p.CurrentUse == 'Y'
                    && p.StartDate.Substring(0, 4) == tripYear && (tripType == "" || p.VisionTripType == tripType)).OrderBy(p => p.VisitCountry).ToList();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //주소 - 국가
    void GetCountry(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var actionResult = new CodeAction().Countries();
        result.data = actionResult.data;
        JsonWriter.Write(result, context);
    }
    //개인방문 - 방문국가
    void GetVisitCountry(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.country.OrderBy(p => p.c_name).ToList();
            var list = www6.selectQ<country>("c_name");
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    #region 비전트립 목록 
    //기획-요청트립 신청현황
    void GetApplyStateList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_Apply_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_Apply_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_Apply_listResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //기획-요청트립 신청정보
    void GetApplyInfoList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_Info_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_Info_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_Info_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //기획-요청트립 참가자정보
    void GetParticipantList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_User_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_User_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_User_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    //기획-요청트립 참가자정보
    void GetParticipantAddrList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_Addr_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_Addr_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_Addr_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //개인방문 SMS
    void GetParticipantSMSList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_SMS_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_SMS_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_SMS_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    //기획-요청트립 통계
    void GetStatistictList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_Statistics_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_Statistics_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_Statistics_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    #endregion

    #region 개인방문 목록 
    //개인방문 신청현황
    void GetApplyStateList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_Apply_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_Apply_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_Apply_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //개인방문 신청정보
    void GetApplyInfoList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_Info_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_Info_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_Info_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //개인방문 참가자정보
    void GetParticipantList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_User_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_User_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_User_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    //개인방문 주소
    void GetParticipantAddrList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_Addr_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_Addr_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_Addr_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //개인방문 SMS
    void GetParticipantSMSList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_SMS_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_SMS_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_SMS_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }


    //개인방문 통계
    void GetStatistictList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_Statistics_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_Statistics_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_Statistics_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    #endregion



    void Update_Apply(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var scheduleID = Convert.ToInt32(context.Request["scheduleID"].ValueIfNull("0"));
        var applyID = Convert.ToInt32(context.Request["applyID"].ValueIfNull("0"));
        var grid_type = context.Request["grid_type"].EmptyIfNull();

        #region apply param  
        var apply_state_cd = context.Request["apply_state_cd"].EmptyIfNull();
        var request_cost_yn = context.Request["request_cost_yn"].EmptyIfNull();
        var trip_cost_yn = context.Request["trip_cost_yn"].EmptyIfNull();
        var childmeet_cost_yn = context.Request["childmeet_cost_yn"].EmptyIfNull();
        var cancel_yn = context.Request["cancel_yn"].EmptyIfNull();
        var attachList = context.Request["attachlist"].EmptyIfNull();
        #endregion

        var trip_cost = context.Request["trip_cost"].EmptyIfNull();
        var childmeet_cost = context.Request["childmeet_cost"].EmptyIfNull();


        //

        var  tel = context.Request["tel"].EmptyIfNull();
        var  email = context.Request["email"].EmptyIfNull();
        var  remark = context.Request["remark"].EmptyIfNull();
        var  cashreceipt_name = context.Request["cashreceipt_name"].EmptyIfNull();
        var  cashreceipt_tel = context.Request["cashreceipt_tel"].EmptyIfNull();
        var  cashreceipt_relation = context.Request["cashreceipt_relation"].EmptyIfNull();
        var  emergencycontact_name = context.Request["emergencycontact_name"].EmptyIfNull();
        var  emergencycontact_relation = context.Request["emergencycontact_relation"].EmptyIfNull();
        var  emergencycontact_tel = context.Request["emergencycontact_tel"].EmptyIfNull();



        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            try
            {
                var exist = www6.selectQ<tVisionTripPlanDetail>("ApplyID", applyID);
                //if (!dao.tVisionTripPlanDetail.Any(p => p.ApplyID == applyID))
                if(!exist.Any())
                {
                    result.success = false;
                    result.message = "올바른 접근이 아닙니다.";
                    JsonWriter.Write(result, context);
                    return;
                }

                //var apply = dao.tVisionTripApply.Where(p => p.ApplyID == applyID && p.CurrentUse == 'Y' && p.ScheduleID == scheduleID).FirstOrDefault();
                var apply = www6.selectQF<tVisionTripApply>("ApplyID", applyID, "CurrentUse", "Y", "ScheduleID", scheduleID);

                var detail = exist[0]; //dao.tVisionTripPlanDetail.Where(p => p.ApplyID == applyID).FirstOrDefault();

                if (apply != null && detail != null)
                {
                    if (grid_type == "apply")
                    {
                        detail.ApplyState = apply_state_cd == "C" ? 'C' : 'A';
                        detail.RequestCostPaymentYN = request_cost_yn == "Y" ? 'Y' : 'N';
                        if (request_cost_yn == "Y")
                            detail.RequestCostPaymentDate = DateTime.Now;
                        detail.TripCostPaymentYN = trip_cost_yn == "Y" ? 'Y' : 'N';
                        if (trip_cost_yn == "Y")
                            detail.TripCostPaymentDate = DateTime.Now;
                        detail.ChildMeetCostPaymentYN = childmeet_cost_yn == "Y" ? 'Y' : 'N';
                        if (childmeet_cost_yn == "Y")
                            detail.ChildMeetCostPaymentDate = DateTime.Now;
                        if (cancel_yn == "Y")
                            apply.CancelDate = DateTime.Now;



                        #region 첨부파일
                        if (attachList != "")
                        {
                            var vtAttachList = new List<tVisionTripAttach>();

                            JArray jResult = JArray.Parse(attachList);
                            for (int i = 0; i < jResult.Count; i++)
                            {
                                string attachType = jResult[i]["type"].ToString();
                                string attachName = jResult[i]["name"].ToString();
                                string attachPath = jResult[i]["path"].ToString();
                                string delYN = jResult[i]["delyn"].ToString();
                                string attachID = jResult[i]["attachid"].ToString();

                                if(attachType == "passport")
                                {
                                    if (attachID == "" && delYN == "Y")
                                    {
                                        //var passport = dao.tVisionTripAttach.Where(p => p.ApplyID == applyID && p.AttachType == "passport" && p.CurrentUse == 'Y').FirstOrDefault();
                                        var passport = www6.selectQF<tVisionTripAttach>("ApplyID", applyID, "AttachType", "passport", "CurrentUse", "Y");
                                        if (passport != null)
                                        {
                                            passport.CurrentUse = delYN == "Y" ? 'N' : 'Y';
                                            passport.modifyDate = DateTime.Now;
                                            passport.ModifyName = AdminLoginSession.GetCookie(context).name;
                                            passport.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();

                                            www6.update(passport);
                                        }
                                        if(attachPath != "")
                                        {
                                            vtAttachList.Add(new tVisionTripAttach()
                                            {
                                                ApplyID = applyID,
                                                AttachType = "passport",
                                                AttachName = attachName,
                                                AttachPath = attachPath,
                                                CurrentUse = 'Y',
                                                RegisterDate = DateTime.Now,
                                                RegisterName = AdminLoginSession.GetCookie(context).name,
                                                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    if (attachPath != "" && attachID == "" && delYN == "N")
                                    {
                                        vtAttachList.Add(new tVisionTripAttach()
                                        {
                                            ApplyID = applyID,
                                            AttachType = attachType,
                                            AttachName = attachName,
                                            AttachPath = attachPath,
                                            CurrentUse = 'Y',
                                            RegisterDate = DateTime.Now,
                                            RegisterName = AdminLoginSession.GetCookie(context).name,
                                            RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                                        });
                                    }
                                    else
                                    {
                                        //var attach = dao.tVisionTripAttach.Where(p => p.ApplyID == applyID && p.AttachID == Convert.ToInt32(attachID) && p.CurrentUse == 'Y').FirstOrDefault();
                                        var attach = www6.selectQF<tVisionTripAttach>("ApplyID", applyID, "AttachID", Convert.ToInt32(attachID), "CurrentUse", "Y");
                                        if (attach != null)
                                        {
                                            attach.CurrentUse = delYN == "Y" ? 'N' : 'Y';
                                            attach.modifyDate = DateTime.Now;
                                            attach.ModifyName = AdminLoginSession.GetCookie(context).name;
                                            attach.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();

                                            www6.update(attach);
                                        }
                                    }
                                }
                            }

                            //dao.tVisionTripAttach.InsertAllOnSubmit(vtAttachList);
                            foreach (var arg in vtAttachList)
                            {
                                www6.insert(arg);
                            }
                        }
                        #endregion


                    }
                    else if(grid_type == "info")
                    {
                        detail.TripCost = trip_cost != "" ? Convert.ToDecimal(trip_cost) : 0;
                        detail.ChildMeetCost = childmeet_cost != "" ? Convert.ToDecimal(childmeet_cost) : 0;
                    }
                    else if(grid_type == "participant")
                    {
                        apply.Tel = tel.Encrypt();
                        apply.Email = email.Encrypt();
                        apply.EmergencyContactName = emergencycontact_name;
                        apply.EmergencyContactRelation = emergencycontact_relation;
                        apply.EmergencyContactTel = emergencycontact_tel;

                        detail.CashReceiptName = cashreceipt_name;
                        detail.CashReceiptRelation = cashreceipt_relation;
                        detail.CashReceiptTel = cashreceipt_tel;
                        detail.Remark = remark;

                    }

                    apply.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();
                    apply.ModifyDate = DateTime.Now;
                    apply.ModifyName = AdminLoginSession.GetCookie(context).name;


                }

                //dao.SubmitChanges();
                www6.update(apply);
                www6.update(detail);

            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                result.success = false;
                result.message = "3. 예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
            }
        }
        JsonWriter.Write(result, context);
    }

    void GetCompanionList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var applyID = Convert.ToInt32(context.Request["applyID"].ValueIfNull("0"));
        var individualDetailID = Convert.ToInt32(context.Request["individualDetailID"].ValueIfNull("0"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //동반인 정보
            //var list = dao.tVisionTripCompanion.Where(p => p.IndividualDetailID == individualDetailID && p.CurrentUse == 'Y').ToList();
            var list = www6.selectQ<tVisionTripCompanion>("IndividualDetailID", individualDetailID, "CurrentUse", "Y");
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    //후원어린이 만남 목록
    void GetChildMeetList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var applyID = Convert.ToInt32(context.Request["applyID"].ValueIfNull("0"));
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.tVisionTripChildMeet.Where(p => p.ApplyID == applyID && p.CurrentUse == 'Y').ToList();
            var list = www6.selectQ<tVisionTripChildMeet>("ApplyID", applyID, "CurrentUse", "Y");
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void GetAttach(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var applyID = Convert.ToInt32(context.Request["apply_id"].ValueIfNull("0"));
        var attachType = context.Request["attach_type"].EmptyIfNull();
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.tVisionTripAttach.Where(p => p.ApplyID == applyID && p.CurrentUse == 'Y' && p.AttachType == attachType).ToList();
            var list = www6.selectQ<tVisionTripAttach>("ApplyID", applyID, "CurrentUse", "Y", "AttachType", attachType);
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    #region 컴파스 연동 - 참가자 정보 전송
    void SendCompassIndividual(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        var applyid_list = context.Request["applyid_list"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var item = dao.sp_tVisionTripAdminIndi_Compass_list(applyid_list).ToJson();
            Object[] op1 = new Object[] { "Applyid_List" };
            Object[] op2 = new Object[] { applyid_list };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_Compass_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_Compass_listResult>();
            var item = list.ToJson();

            DataTable table = JsonConvert.DeserializeObject<DataTable>(item);

            DataSet ds = new DataSet();
            ds.Tables.Add(table);


            #region Compass - VT_Master 데이터 등록 : VisionTrip 일정 정보 등록 
            Object[] objSql = new object[1] { "VT_SponsorApplySave" };
            Object[] objParam = new object[28] { "DIVIS", "CountryCode", "VisitCategory",
                "ChildKey", "ChildName", "ConID", "SponsorName", "GenderCode",
                "AccompanyingPerson", "BirthDate", "GroupType",
                "CharacterType", "CommitmentCount", "Tel", "Email", "Nationality",
                "ReligionType", "Address", "ZipCode", "SpecialRequest",
                "ETC", "Remark", "Admin", "Progress", "RegisterID",
                "RegisterName", "SponsorID", "PersonalVisitDate"};

            //Object[] objValue = new object[28] { table.Rows[0]["DIVIS"],
            //    table.Rows[0]["CountryCode"],
            //    table.Rows[0]["VisitCategory"],
            //    table.Rows[0]["ChildKey"],
            //    table.Rows[0]["ChildName"],
            //    table.Rows[0]["ConID"],
            //    table.Rows[0]["SponsorName"],
            //    table.Rows[0]["GenderCode"],
            //    table.Rows[0]["AccompanyingPerson"],
            //    table.Rows[0]["BirthDate"], table.Rows[0]["GroupType"],
            //    table.Rows[0]["CharacterType"], table.Rows[0]["CommitmentCount"], table.Rows[0]["Tel"], table.Rows[0]["Email"], table.Rows[0]["Nationality"],
            //    table.Rows[0]["ReligionType"],
            //    table.Rows[0]["Address"],
            //    table.Rows[0]["ZipCode"],
            //    table.Rows[0]["SpecialRequest"],
            //    table.Rows[0]["ETC"],
            //    table.Rows[0]["Remark"],
            //    table.Rows[0]["Admin"],
            //    table.Rows[0]["Progress"], table.Rows[0]["RegisterID"],
            //    table.Rows[0]["RegisterName"], table.Rows[0]["SponsorID"], table.Rows[0]["PersonalVisitDate"]
            //};
            //var scheduleInsert = _www6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

            var scheduleInsert = _www6Service.Tx_ExecuteQueryDataSet("SqlCompass5", objSql, "SP", ds, objParam);
            if (scheduleInsert < 0)
            {
                result.success = false;
                result.message = "4. 예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
            }
            else
            {
                if (applyid_list.Trim() != "")
                {
                    foreach (var id in applyid_list.Split(','))
                    {
                        if (id != "")
                        {
                            int i;
                            bool check = int.TryParse(id, out i);
                            if (check)
                            {
                                //var apply = dao.tVisionTripApply.Where(p => p.ApplyID == Convert.ToInt16(id) && p.CurrentUse == 'Y').FirstOrDefault();
                                var apply = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt16(id), "CurrentUse", "Y");
                                if (apply != null)
                                {
                                    apply.CompassYN = 'Y';
                                    apply.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();
                                    apply.ModifyDate = DateTime.Now;
                                    apply.ModifyName = AdminLoginSession.GetCookie(context).name;

                                    //dao.SubmitChanges();
                                    www6.update(apply);
                                }
                            }
                        }
                    }
                }
            }
            #endregion
        }

        JsonWriter.Write(result, context);
    }

    void SendCompassPlan(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        var applyid_list = context.Request["applyid_list"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var item = dao.sp_tVisionTripAdminPlan_Compass_list(applyid_list).ToJson();
            Object[] op1 = new Object[] { "Applyid_List" };
            Object[] op2 = new Object[] { applyid_list };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_Compass_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_Compass_listResult>();
            var item = list.ToJson();

            DataTable table = JsonConvert.DeserializeObject<DataTable>(item);

            DataSet ds = new DataSet();
            ds.Tables.Add(table);


            #region Compass - VT_Member 데이터 등록 : 기획/요청트립 참가자정보
            Object[] objSql = new object[1] { "VT_SponsorApplySave" };
            Object[] objParam = new object[] { "DIVIS", "ScheduleID", "GroupType", "CharacterType",
                                                "CommitmentCount",
                                                "ConID", "SponsorName",
                                                "GenderCode", "BirthDate", "Tel", "Email", "Nationality",
                                                "ReligionType", "Address", "ZipCode", "Remark",
                                                "RegisterID", "RegisterName", "SponsorID" };

            var scheduleInsert = _www6Service.Tx_ExecuteQueryDataSet("SqlCompass5", objSql, "SP", ds, objParam);
            if (scheduleInsert < 0)
            {
                result.success = false;
                result.message = "5. 예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
            }
            else
            {
                if (applyid_list.Trim() != "")
                {
                    foreach (var id in applyid_list.Split(','))
                    {
                        if (id != "")
                        {
                            int i;
                            bool check = int.TryParse(id, out i);
                            if (check)
                            {
                                //var apply = dao.tVisionTripApply.Where(p => p.ApplyID == Convert.ToInt16(id) && p.CurrentUse == 'Y').FirstOrDefault();
                                var apply = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt16(id), "CurrentUse", "Y");
                                if (apply != null)
                                {
                                    apply.CompassYN = 'Y';
                                    apply.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();
                                    apply.ModifyDate = DateTime.Now;
                                    apply.ModifyName = AdminLoginSession.GetCookie(context).name;

                                    //dao.SubmitChanges();
                                    www6.update(apply);
                                }
                            }
                        }
                    }
                }
            }
            #endregion
        }

        JsonWriter.Write(result, context);
    }
    #endregion

    void SendSMS(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var title = context.Request["title"].EmptyIfNull();
        var message = context.Request["message"].EmptyIfNull();
        var sendtype = context.Request["sendtype"].EmptyIfNull();
        var senddatetime = context.Request["senddatetime"].EmptyIfNull();
        var unixtimestamp = context.Request["unixtimestamp"].EmptyIfNull();
        var messagetype = context.Request["messagetype"].EmptyIfNull();
        var tellist = context.Request["tellist"].EmptyIfNull();
        var applytype = context.Request["applytype"].EmptyIfNull();
        var sendtel = context.Request["sendtel"].EmptyIfNull(); //전화번호 목록


        using (FrontDataContext dao = new FrontDataContext())
        {
            if (tellist != "")
            {
                var vtMessage = new List<tVisionTripMessage>();

                JArray jResult = JArray.Parse(tellist);
                for (int i = 0; i < jResult.Count; i++)
                {
                    int applyid = Convert.ToInt16(jResult[i]["applyid"].ToString());
                    string userid = jResult[i]["userid"].ToString();
                    string sponsor_name = jResult[i]["sponsor_name"].ToString();
                    string tel = jResult[i]["tel"].ToString();


                    string[] strSMSContent = new string[5];
                    strSMSContent[0] = tel.Replace("-", "").ToString();
                    strSMSContent[1] = sponsor_name; // 수신자 이름 
                    strSMSContent[2] = sendtel; // 발신자 전화번호
                    strSMSContent[3] = message; // 메세지 내용
                    strSMSContent[4] = "sms"; //전송타입 - 80byte 이상이면 자동으로 MMS 전송하기 때문에 전송에는 사실상 의미 없음  

                    //SendSMS_Mate(strSMSContent, unixtimestamp != "" ? unixtimestamp : 0);
                    //성공여부 SendYN 에 값 저장 
                    vtMessage.Add(new tVisionTripMessage()
                    {
                        ApplyID = applyid,
                        UserID = userid,
                        Title = title,
                        Message = message,
                        ReceiveTel = tel,
                        ReceiveID = userid,
                        ReceiveName = sponsor_name,
                        SendType = sendtype == "R" ? 'R' : 'D', //R:예약, D:직접
                        MessageType = applytype + " 관리자",
                        ReservedDate = Convert.ToDateTime(senddatetime),
                        UnixTimeStamp = unixtimestamp,
                        //SendYN = smsSendYN ? 'Y' : 'N',
                        SendDate = DateTime.Now,
                        RegisterID = AdminLoginSession.GetCookie(context).email.ToString(),
                        RegisterName = AdminLoginSession.GetCookie(context).name,
                        RegisterDate = DateTime.Now
                    });

                    //dao.tVisionTripMessage.InsertAllOnSubmit(vtMessage);
                    foreach (var arg in vtMessage)
                    {
                        www6.insert(arg);
                    }
                    //dao.SubmitChanges();
                }

            }

        }

        JsonWriter.Write(result, context);
    }

    void GetPaymentList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        var paymentType = context.Request["paymentType"].EmptyIfNull();
        var scheduleID = Convert.ToInt32(context.Request["scheduleID"].ValueIfNull("0"));
        var amount = Convert.ToInt32(context.Request["amount"].ValueIfNull("0"));
        var depositor = context.Request["depositor"].EmptyIfNull();
        var startdate = context.Request["startdate"].EmptyIfNull();
        var enddate = context.Request["enddate"].EmptyIfNull();


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPayment_list(page, rowsPerPage, paymentType, scheduleID, amount, depositor, startdate, enddate).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "paymentType", "scheduleID", "amount", "depositor", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, rowsPerPage, paymentType, scheduleID, amount, depositor, startdate, enddate };
            var list = www6.selectSP("sp_tVisionTripAdminPayment_list", op1, op2).DataTableToList<sp_tVisionTripAdminPayment_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    void GetAmountList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var paymentType = context.Request["paymentType"].EmptyIfNull();
        var scheduleID = Convert.ToInt32(context.Request["scheduleID"].ValueIfNull("0"));
        var startdate = context.Request["startdate"].EmptyIfNull();
        var enddate = context.Request["enddate"].EmptyIfNull();


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPayment_Amount(scheduleID, startdate, enddate, paymentType).ToList();
            Object[] op1 = new Object[] { "scheduleID", "startdate", "enddate", "paymentType" };
            Object[] op2 = new Object[] { scheduleID, startdate, enddate, paymentType };
            var list = www6.selectSP("sp_tVisionTripAdminPayment_Amount", op1, op2).DataTableToList<sp_tVisionTripAdminPayment_AmountResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }


}
