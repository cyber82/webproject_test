﻿<%@ WebHandler Language="C#" Class="api_visiontrip" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;


public class api_visiontrip : BaseHandler
{
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();
    CommonLib.WWWService.ServiceSoap _wwwService;
    CommonLib.WWW6Service.SoaHelperSoap _www6Service;

    public override void OnRequest(HttpContext context)
    {

        var t = context.Request["t"].EmptyIfNull();
        var list = context.Request["list"].EmptyIfNull();

        if(t == "applyplay_list")//참가자관리(기획_요청)
        {
            switch (list)
            {
                case "state": this.GetApplyStateList_P(context); break;//참가자관리(기획_요청) : 신청현황
                case "info": this.GetApplyInfoList_P(context); ; break;//참가자관리(기획_요청) : 신청정보
                case "participant": this.GetParticipantList_P(context); break; //참가자관리(기획_요청) : 신청정보
                case "addr": this.GetParticipantAddrList_P(context); break;//참가자관리(기획_요청) : 참가자주소
                case "sms": this.GetParticipantSMSList_P(context); break;//참가자관리(기획_요청) : SMS
                case "statistics": this.GetStatistictList_P(context); break; //참가자관리(기획_요청) : 통계
            }
        }
        else if (t == "applyindividual_list")//개인방문
        {
            switch (list)
            {
                case "state": this.GetApplyStateList_I(context); break;//신청현황
                case "info": this.GetApplyInfoList_I(context); ; break;//신청정보
                case "participant": this.GetParticipantList_I(context); break;//참가자정보
                case "addr": this.GetParticipantAddrList_I(context); break; //참가자주소
                case "sms": this.GetParticipantSMSList_I(context); break; //SMS
                case "statistics": this.GetStatistictList_I(context); break;//통계
            }
        }
        else if (t == "update_apply") //기획/요청트립 신청서 수정
        { this.Update_Apply(context); }
        else if (t == "country")//local country
        { this.GetCountry(context); }
        else if (t == "visit_country")//visit country
        { this.GetVisitCountry(context); }
        else if (t == "companionlist")//동반인목록
        { this.GetCompanionList(context); }
        else if (t == "childmeetlist")//어린이만남 목록
        { this.GetChildMeetList(context); }
        else if (t == "attachlist")//첨부파일목록
        { this.GetAttach(context); }
        else if (t == "update_apply_individual")//개인방문 신청서 수정
        { this.Update_Apply_Individual(context); }
        else if (t == "compass_individual") //컴파스 전송
        { this.SendCompassIndividual(context); }
        else if (t == "compass_plan") //컴파스 전송
        { this.SendCompassPlan(context); }
        else if (t == "tripname") //트립명
        { this.GetTripName(context); }
        else if (t == "send_message") //sms 발송
        { this.SendSMS(context); }
        //입금확인
        else if (t == "paymentlist")
        { this.GetPaymentList(context); }
        else if (t == "payment_amount")
        { this.GetAmountList(context); }
    }

    //트립명
    void GetTripName(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.tVisionTripSchedule.Where(p => p.CurrentUse == 'Y'
            //       && p.StartDate.Substring(0, 4) == tripYear && (tripType == "" || p.VisionTripType == tripType)).OrderBy(p => p.VisitCountry).ToList();

            string query = string.Empty;
            if (tripType == "")
            {
                query = string.Format("select * from tVisionTripSchedule where 1=1  and CurrentUse = 'Y' and Substring(StartDate, 1, 4) = '{0}' order by VisitCountry asc ", tripYear);
            }
            else
            {
                query = string.Format("select * from tVisionTripSchedule where 1=1  and CurrentUse = 'Y' and Substring(StartDate, 1, 4) = '{0}' and VisionTripType = '{1}' order by VisitCountry asc ", tripYear, tripType);
            }
            var list = www6.selectText(query).DataTableToList<tVisionTripSchedule>();

            //www6.selectText
            //var list = www6.selectQ2<tVisionTripSchedule>("CurrentUse = ", "Y", "Substring(StartDate, 1, 4) = ", tripYear, "(", new Ept(), " - tripType = ", "", "+or VisionTripType = ", tripType, "- )", new Ept(), "VisitCountry");
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //주소 - 국가
    void GetCountry(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var actionResult = new CodeAction().Countries();
        result.data = actionResult.data;
        JsonWriter.Write(result, context);
    }
    //개인방문 - 방문국가
    void GetVisitCountry(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.country.OrderBy(p => p.c_name).ToList();
            var list = www6.selectQ<country>("c_name");
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    #region 비전트립 목록 
    //기획-요청트립 신청현황
    void GetApplyStateList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));

        var requestSubType = context.Request["requestSubType"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_Apply_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID", "requestSubType" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID, requestSubType };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_Apply_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_Apply_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //기획-요청트립 신청정보
    void GetApplyInfoList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));
        var requestSubType = context.Request["requestSubType"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_Info_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID", "requestSubType" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID, requestSubType };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_Info_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_Info_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //기획-요청트립 참가자정보
    void GetParticipantList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));
        var requestSubType = context.Request["requestSubType"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_User_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID", "requestSubType" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID, requestSubType };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_User_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_User_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    //기획-요청트립 참가자정보
    void GetParticipantAddrList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));
        var requestSubType = context.Request["requestSubType"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_Addr_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID", "requestSubType" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID, requestSubType };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_Addr_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_Addr_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //개인방문 SMS
    void GetParticipantSMSList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));
        var requestSubType = context.Request["requestSubType"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_SMS_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID", "requestSubType" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID, requestSubType };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_SMS_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_SMS_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    //기획-요청트립 통계
    void GetStatistictList_P(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var tripYear = context.Request["tripYear"].EmptyIfNull();
        var tripType = context.Request["tripType"].EmptyIfNull();
        var tripID = Convert.ToInt32(context.Request["tripID"].ValueIfNull("0"));
        var requestSubType = context.Request["requestSubType"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPlan_Statistics_list(page, rowsPerPage, tripType, tripYear, tripID).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "tripYear", "tripID", "requestSubType" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, tripYear, tripID, requestSubType };
            var list = www6.selectSP("sp_tVisionTripAdminPlan_Statistics_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_Statistics_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    #endregion

    #region 개인방문 목록 
    //개인방문 신청현황
    void GetApplyStateList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_Apply_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_Apply_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_Apply_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //개인방문 신청정보
    void GetApplyInfoList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_Info_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_Info_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_Info_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //개인방문 참가자정보
    void GetParticipantList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();
        var requestSubType = context.Request["requestSubType"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_User_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry", "requestSubType" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry, requestSubType };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_User_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_User_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    //개인방문 주소
    void GetParticipantAddrList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_Addr_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_Addr_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_Addr_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    //개인방문 SMS
    void GetParticipantSMSList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_SMS_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_SMS_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_SMS_listResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }


    //개인방문 통계
    void GetStatistictList_I(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;


        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        var visitType = context.Request["visitType"].EmptyIfNull();
        var visitYear = context.Request["visitYear"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminIndi_Statistics_list(page, rowsPerPage, visitType, visitYear, visitCountry).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "visitType", "visitYear", "visitCountry" };
            Object[] op2 = new Object[] { page, rowsPerPage, visitType, visitYear, visitCountry };
            var list = www6.selectSP("sp_tVisionTripAdminIndi_Statistics_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_Statistics_listResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    #endregion



    void Update_Apply(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var scheduleID = Convert.ToInt32(context.Request["scheduleID"].ValueIfNull("0"));
        var applyID = Convert.ToInt32(context.Request["applyID"].ValueIfNull("0"));
        var grid_type = context.Request["grid_type"].EmptyIfNull();

        #region apply param  
        var apply_state_cd = context.Request["apply_state_cd"].EmptyIfNull();
        var request_cost_yn = context.Request["request_cost_yn"].EmptyIfNull();
        var trip_cost_yn = context.Request["trip_cost_yn"].EmptyIfNull();
        var childmeet_cost_yn = context.Request["childmeet_cost_yn"].EmptyIfNull();
        var cancel_yn = context.Request["cancel_yn"].EmptyIfNull();
        var attachList = context.Request["attachlist"].EmptyIfNull();
        #endregion
        #region info param 
        var trip_cost = context.Request["trip_cost"].EmptyIfNull();
        var childmeet_cost = context.Request["childmeet_cost"].EmptyIfNull();
        var childList = context.Request["childmeet"].EmptyIfNull();  //후원어린이 선택
        var visiontrip_history_code = context.Request["visiontrip_history_code"].EmptyIfNull();
        #endregion         
        #region participant param
        var sponsor_name_eng = context.Request["sponsor_name_eng"].EmptyIfNull();
        var gendercode = context.Request["gendercode"].EmptyIfNull();
        var tel = context.Request["tel"].EmptyIfNull();
        var email = context.Request["email"].EmptyIfNull();
        var remark = context.Request["remark"].EmptyIfNull();
        var cashreceipt_name = context.Request["cashreceipt_name"].EmptyIfNull();
        var cashreceipt_tel = context.Request["cashreceipt_tel"].EmptyIfNull();
        var cashreceipt_relation = context.Request["cashreceipt_relation"].EmptyIfNull();
        var emergencycontact_name = context.Request["emergencycontact_name"].EmptyIfNull();
        var emergencycontact_relation = context.Request["emergencycontact_relation"].EmptyIfNull();
        var emergencycontact_tel = context.Request["emergencycontact_tel"].EmptyIfNull();
        #endregion
        #region addr param
        var zipcode = context.Request["zipcode"].EmptyIfNull();
        var address1 = context.Request["address1"].EmptyIfNull();
        var address2 = context.Request["address2"].EmptyIfNull();
        var location = context.Request["location"].EmptyIfNull();
        var country = context.Request["country"].EmptyIfNull();
        #endregion 

        using (FrontDataContext dao = new FrontDataContext())
        {
            using (AuthDataContext authDao = new AuthDataContext())
            {
                try
                {
                    var exist = www6.selectQ<tVisionTripPlanDetail>("ApplyID", applyID);
                    //if (!dao.tVisionTripPlanDetail.Any(p => p.ApplyID == applyID))
                    if(!exist.Any())
                    {
                        result.success = false;
                        result.message = "올바른 접근이 아닙니다.";
                        JsonWriter.Write(result, context);
                        return;
                    }

                    //var apply = dao.tVisionTripApply.Where(p => p.ApplyID == applyID && p.CurrentUse == 'Y' && p.ScheduleID == scheduleID).FirstOrDefault();
                    var apply = www6.selectQF<tVisionTripApply>("ApplyID", applyID, "CurrentUse", "Y", "ScheduleID", scheduleID);

                    //var userInfo = authDao.tSponsorMaster.Where(p => p.SponsorID == apply.SponsorID).FirstOrDefault();
                    var userInfo = www6.selectQFAuth<tSponsorMaster>("SponsorID", apply.SponsorID);


                    //var detail = dao.tVisionTripPlanDetail.Where(p => p.ApplyID == applyID).FirstOrDefault();
                    var detail = www6.selectQF<tVisionTripPlanDetail>("ApplyID", applyID);

                    if (apply != null && detail != null)
                    {
                        if (grid_type == "apply")
                        {
                            detail.ApplyState = apply_state_cd == "C" ? 'C' : 'A';
                            detail.RequestCostPaymentYN = request_cost_yn == "Y" ? 'Y' : 'N';
                            if (request_cost_yn == "Y")
                                detail.RequestCostPaymentDate = DateTime.Now;
                            detail.TripCostPaymentYN = trip_cost_yn == "Y" ? 'Y' : 'N';
                            if (trip_cost_yn == "Y")
                                detail.TripCostPaymentDate = DateTime.Now;
                            detail.ChildMeetCostPaymentYN = childmeet_cost_yn == "Y" ? 'Y' : 'N';
                            if (childmeet_cost_yn == "Y")
                                detail.ChildMeetCostPaymentDate = DateTime.Now;
                            if (cancel_yn == "Y")
                                apply.CancelDate = DateTime.Now;
                            else
                                apply.CancelDate = null;

                            #region 첨부파일
                            if (attachList != "")
                            {
                                var vtAttachList = new List<tVisionTripAttach>();

                                JArray jResult = JArray.Parse(attachList);
                                for (int i = 0; i < jResult.Count; i++)
                                {
                                    string attachType = jResult[i]["type"].ToString();
                                    string attachName = jResult[i]["name"].ToString();
                                    string attachPath = jResult[i]["path"].ToString();
                                    string delYN = jResult[i]["delyn"].ToString();
                                    string attachID = jResult[i]["attachid"].ToString();

                                    if (attachType == "passport")
                                    {
                                        if (attachID == "" && delYN == "Y")
                                        {
                                            //var passport = dao.tVisionTripAttach.Where(p => p.ApplyID == applyID && p.AttachType == "passport" && p.CurrentUse == 'Y').FirstOrDefault();
                                            var passport = www6.selectQF<tVisionTripAttach>("ApplyID", applyID, "AttachType", "passport", "CurrentUse", "Y");
                                            if (passport != null)
                                            {
                                                passport.CurrentUse = delYN == "Y" ? 'N' : 'Y';
                                                passport.modifyDate = DateTime.Now;
                                                passport.ModifyName = AdminLoginSession.GetCookie(context).name;
                                                passport.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();

                                                www6.update(passport);
                                            }
                                            if (attachPath != "")
                                            {
                                                vtAttachList.Add(new tVisionTripAttach()
                                                {
                                                    ApplyID = applyID,
                                                    AttachType = "passport",
                                                    AttachName = attachName,
                                                    AttachPath = attachPath,
                                                    CurrentUse = 'Y',
                                                    RegisterDate = DateTime.Now,
                                                    RegisterName = AdminLoginSession.GetCookie(context).name,
                                                    RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                                                });
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (attachPath != "" && attachID == "" && delYN == "N")
                                        {
                                            vtAttachList.Add(new tVisionTripAttach()
                                            {
                                                ApplyID = applyID,
                                                AttachType = attachType,
                                                AttachName = attachName,
                                                AttachPath = attachPath,
                                                CurrentUse = 'Y',
                                                RegisterDate = DateTime.Now,
                                                RegisterName = AdminLoginSession.GetCookie(context).name,
                                                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                                            });
                                        }
                                        else
                                        {
                                            //var attach = dao.tVisionTripAttach.Where(p => p.ApplyID == applyID && p.AttachID == Convert.ToInt32(attachID) && p.CurrentUse == 'Y').FirstOrDefault();
                                            var attach = www6.selectQF<tVisionTripAttach>("ApplyID", applyID, "AttachID", Convert.ToInt32(attachID), "CurrentUse", "Y");
                                            if (attach != null)
                                            {
                                                attach.CurrentUse = delYN == "Y" ? 'N' : 'Y';
                                                attach.modifyDate = DateTime.Now;
                                                attach.ModifyName = AdminLoginSession.GetCookie(context).name;
                                                attach.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();

                                                www6.update(attach);
                                            }
                                        }
                                    }
                                }
                                //dao.tVisionTripAttach.InsertAllOnSubmit(vtAttachList);
                                foreach (var al in vtAttachList)
                                {
                                    www6.insert(al);
                                }
                            }
                            #endregion


                        }
                        else if (grid_type == "info")
                        {
                            detail.TripCost = trip_cost != "" ? Convert.ToDecimal(trip_cost) : 0;
                            detail.ChildMeetCost = childmeet_cost != "" ? Convert.ToDecimal(childmeet_cost) : 0;
                            if(visiontrip_history_code != "")
                                detail.VisionTripHistory = Convert.ToChar(visiontrip_history_code);

                            #region 후원어린이정보
                            if (childList != "")
                            {
                                var vtChildMeetList = new List<tVisionTripChildMeet>();
                                JArray jResult = JArray.Parse(childList);
                                for (int i = 0; i < jResult.Count; i++)
                                {
                                    string childKey = jResult[i]["id"].ToString();
                                    string childName = jResult[i]["name"].ToString();
                                    string delYN = jResult[i]["delyn"].ToString();
                                    string childmeetid = jResult[i]["childmeetid"].ToString();
                                    string childmeetType = jResult[i]["childmeettype"].ToString();

                                    if (childmeetType == "I")
                                    {
                                        if (childKey != "" && childmeetid == "" && delYN == "N")
                                        {
                                            vtChildMeetList.Add(new tVisionTripChildMeet()
                                            {
                                                ApplyID = applyID,
                                                ChildKey = childKey,
                                                ChildName = childName,
                                                ChildMeetType = 'I', //(I 직접입력, S 선택) 
                                                CurrentUse = 'Y',
                                                RegisterDate = DateTime.Now,
                                                RegisterName = AdminLoginSession.GetCookie(context).name,
                                                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                                            });
                                        }
                                        else
                                        {
                                            //var childmeet = dao.tVisionTripChildMeet.Where(p => p.ApplyID == applyID && p.ChildMeetID == Convert.ToInt32(childmeetid) && p.CurrentUse == 'Y').FirstOrDefault();
                                            var childmeet = www6.selectQF<tVisionTripChildMeet>("ApplyID", applyID, "ChildMeetID", Convert.ToInt32(childmeetid), "CurrentUse", "Y");
                                            if (childmeet != null)
                                            {
                                                childmeet.ChildKey = childKey;
                                                childmeet.ChildName = childName;
                                                childmeet.CurrentUse = delYN == "Y" ? 'N' : 'Y';
                                                childmeet.ModifyDate = DateTime.Now;
                                                childmeet.ModifyName = AdminLoginSession.GetCookie(context).name;
                                                childmeet.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();

                                                www6.update(childmeet);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //var childmeet= dao.tVisionTripChildMeet.Where(p => p.ApplyID == applyID && p.ChildKey == childKey && p.CurrentUse == 'Y').FirstOrDefault();
                                        var childmeet = www6.selectQF<tVisionTripChildMeet>("ApplyID", applyID, "ChildKey", childKey, "CurrentUse", "Y");
                                        if (childmeet != null && delYN == "Y")
                                        {
                                            childmeet.CurrentUse = 'N';
                                            childmeet.ModifyDate = DateTime.Now;
                                            childmeet.ModifyName = AdminLoginSession.GetCookie(context).name;
                                            childmeet.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();

                                            www6.update(childmeet);
                                        }
                                        else
                                        {
                                            vtChildMeetList.Add(new tVisionTripChildMeet()
                                            {
                                                ApplyID = applyID,
                                                ChildKey = childKey,
                                                ChildName = childName,
                                                ChildMeetType = 'S', //(I 직접입력, S 선택) 
                                                CurrentUse = 'Y',
                                                RegisterDate = DateTime.Now,
                                                RegisterName = AdminLoginSession.GetCookie(context).name,
                                                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                                            });

                                        }
                                    }
                                }
                                //dao.tVisionTripChildMeet.InsertAllOnSubmit(vtChildMeetList);
                                foreach (var cml in vtChildMeetList)
                                {
                                    www6.insert(cml);
                                }

                            }
                            #endregion

                        }
                        else if (grid_type == "participant")
                        {
                            apply.SponsorNameEng = sponsor_name_eng;
                            apply.Tel = tel.Encrypt();
                            apply.Email = email.Encrypt();
                            apply.EmergencyContactName = emergencycontact_name;
                            apply.EmergencyContactRelation = emergencycontact_relation;
                            apply.EmergencyContactTel = emergencycontact_tel;

                            detail.GenderCode = gendercode == null ? Convert.ToChar("") : Convert.ToChar(gendercode.ToUpper());
                            detail.CashReceiptName = cashreceipt_name;
                            detail.CashReceiptRelation = cashreceipt_relation;
                            detail.CashReceiptTel = cashreceipt_tel;
                            detail.Remark = remark;

                        }
                        else if (grid_type == "addr")
                        {
                            apply.LocationType = location;
                            apply.Address1 = (address1 + "$" + country).Encrypt();
                            apply.Address2 = address2.Encrypt();
                            apply.ZipCode = zipcode.Encrypt();

                            #region 사용자 정보 변경 - 주소 
                            if (apply.ChangeYN.Equals("Y"))
                            {
                                try
                                {
                                    // 후원회원인경우 
                                    if (!string.IsNullOrEmpty(userInfo.SponsorID))
                                    {
                                        var userInfoResult = UpdateCompassUserInfo(userInfo.SponsorID, userInfo.UserID, userInfo.SponsorName, location, country, address1, address2, zipcode, "", "");
                                        if (!userInfoResult.success)
                                        {
                                            result.success = false;
                                            result.message = userInfoResult.message;
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                                    result.success = false;
                                    result.message = "회원님의 정보를 수정하는 중 오류가 발생했습니다.(WEB)";
                                    //return;
                                }
                            }
                            #endregion

                        }

                        apply.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();
                        apply.ModifyDate = DateTime.Now;
                        apply.ModifyName = AdminLoginSession.GetCookie(context).name;
                    }
                    //dao.SubmitChanges();
                    www6.update(apply);
                    //www6.updateAuth(userInfo);
                    www6.update(detail);

                }
                catch (Exception ex)
                {
                    ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                    result.success = false;
                    result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
                }
            }
        }

        JsonWriter.Write(result, context);
    }

    void GetCompanionList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var applyID = Convert.ToInt32(context.Request["applyID"].ValueIfNull("0"));
        var individualDetailID = Convert.ToInt32(context.Request["individualDetailID"].ValueIfNull("0"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //동반인 정보
            //var list = dao.tVisionTripCompanion.Where(p => p.IndividualDetailID == individualDetailID && p.CurrentUse == 'Y').ToList();
            var list = www6.selectQ<tVisionTripCompanion>("IndividualDetailID", individualDetailID, "CurrentUse", "Y");
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    //후원어린이 만남 목록
    void GetChildMeetList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var applyID = Convert.ToInt32(context.Request["applyID"].ValueIfNull("0"));
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.tVisionTripChildMeet.Where(p => p.ApplyID == applyID && p.CurrentUse == 'Y').ToList();
            var list = www6.selectQ<tVisionTripChildMeet>("ApplyID", applyID, "CurrentUse", "Y");
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void GetAttach(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var applyID = Convert.ToInt32(context.Request["apply_id"].ValueIfNull("0"));
        var attachType = context.Request["attach_type"].EmptyIfNull();
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.tVisionTripAttach.Where(p => p.ApplyID == applyID && p.CurrentUse == 'Y' && p.AttachType == attachType).ToList();
            var list = www6.selectQ<tVisionTripAttach>("ApplyID", applyID, "CurrentUse", "Y", "AttachType", attachType);
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void Update_Apply_Individual(HttpContext context)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        JsonWriter result = new JsonWriter();
        result.success = true;

        var applyID = Convert.ToInt32(context.Request["applyID"].ValueIfNull("0"));
        var individualdetailID = Convert.ToInt32(context.Request["individualdetailID"].ValueIfNull("0"));
        var grid_type = context.Request["grid_type"].EmptyIfNull();

        #region apply param  
        var visit_type = context.Request["visit_type"].EmptyIfNull();
        var visit_country = context.Request["visit_country"].EmptyIfNull();
        var visit_date1 = context.Request["visit_date1"].EmptyIfNull();
        var visit_date2 = context.Request["visit_date2"].EmptyIfNull();
        var companionList = context.Request["companion"].EmptyIfNull();
        var childList = context.Request["childmeet"].EmptyIfNull();
        var attachList = context.Request["attachlist"].EmptyIfNull();
        var cancel_yn = context.Request["cancel_yn"].EmptyIfNull();
        #endregion

        #region info param
        var local_accommodation = context.Request["local_accommodation"].EmptyIfNull();
        var local_address = context.Request["local_address"].EmptyIfNull();
        var local_tel = context.Request["local_tel"].EmptyIfNull();
        var departure_date = context.Request["departure_date"].EmptyIfNull();
        var return_date = context.Request["return_date"].EmptyIfNull();
        var emergencycontact_name = context.Request["emergencycontact_name"].EmptyIfNull();
        var emergencycontact_tel = context.Request["emergencycontact_tel"].EmptyIfNull();
        var emergencycontact_relation = context.Request["emergencycontact_relation"].EmptyIfNull();
        #endregion

        #region participant param
        var tel = context.Request["tel"].EmptyIfNull();
        var email = context.Request["email"].EmptyIfNull();
        var remark = context.Request["remark"].EmptyIfNull();
        #endregion

        #region addr param
        var zipcode = context.Request["zipcode"].EmptyIfNull();
        var address1 = context.Request["address1"].EmptyIfNull();
        var address2 = context.Request["address2"].EmptyIfNull();
        var location = context.Request["location"].EmptyIfNull();
        var country = context.Request["country"].EmptyIfNull();
        #endregion



        using (FrontDataContext dao = new FrontDataContext())
        {
            using (AuthDataContext authDao = new AuthDataContext())
            {

                try
                {
                    var exist = www6.selectQ<tVisionTripIndividualDetail>("ApplyID", applyID);
                    //if (!dao.tVisionTripIndividualDetail.Any(p => p.ApplyID == applyID))
                    if(!exist.Any())
                    {
                        result.success = false;
                        result.message = "올바른 접근이 아닙니다.";
                        JsonWriter.Write(result, context);
                        return;
                    }

                    //var apply = dao.tVisionTripApply.Where(p => p.ApplyID == applyID && p.CurrentUse == 'Y').FirstOrDefault();
                    var apply = www6.selectQF<tVisionTripApply>("ApplyID", applyID, "CurrentUse", "Y");

                    //var userInfo = authDao.tSponsorMaster.Where(p => p.SponsorID == apply.SponsorID).FirstOrDefault();
                    var userInfo = www6.selectQFAuth<tSponsorMaster>("SponsorID", apply.SponsorID);

                    //var detail = dao.tVisionTripIndividualDetail.Where(p => p.ApplyID == applyID && p.IndividualDetailID == individualdetailID).FirstOrDefault();
                    var detail = www6.selectQF<tVisionTripIndividualDetail>("ApplyID", applyID, "IndividualDetailID", individualdetailID);

                    if (apply != null && detail != null)
                    {
                        if (grid_type == "apply")
                        {
                            detail.VisitType = visit_type == "V" ? 'V' : 'M';
                            detail.VisitCountry = visit_country;
                            detail.VisitDate1 = visit_date1;
                            detail.VisitDate2 = visit_date2;

                            if (cancel_yn == "Y")
                                apply.CancelDate = DateTime.Now;

                            #region 동반인정보
                            if (companionList != "")
                            {
                                var vtCompanion = new List<tVisionTripCompanion>();
                                JArray jResult = JArray.Parse(companionList);
                                for (int i = 0; i < jResult.Count; i++)
                                {
                                    string companionINameKor = jResult[i]["name_kor"].ToString();
                                    string companionINameEng = jResult[i]["name_eng"].ToString();
                                    string companionIBirth = jResult[i]["birth"].ToString();
                                    string companionIGender = jResult[i]["gender"].ToString();
                                    string delYN = jResult[i]["delyn"].ToString();
                                    string companionID = jResult[i]["companionid"].ToString();

                                    if (companionINameKor != "" && companionINameEng != ""
                                            && companionIBirth != "" && companionIGender != ""
                                            && delYN == "N" && companionID == "")
                                    {
                                        vtCompanion.Add(new tVisionTripCompanion()
                                        {
                                            IndividualDetailID = individualdetailID,
                                            CompanionName = companionINameKor,
                                            CompanionNameEng = companionINameEng,
                                            BirthDate = companionIBirth,
                                            GenderCode = Convert.ToChar(companionIGender),
                                            CurrentUse = 'Y',
                                            RegisterName = AdminLoginSession.GetCookie(context).name,
                                            RegisterID = AdminLoginSession.GetCookie(context).email.ToString(),
                                            RegisterDate = DateTime.Now
                                        });
                                    }
                                    else
                                    {
                                        if (companionID != "" && delYN == "N")
                                        {
                                            //var companion = dao.tVisionTripCompanion.Where(p => p.IndividualDetailID == individualdetailID && p.CompanionID == Convert.ToInt32(companionID) && p.CurrentUse == 'Y').FirstOrDefault();
                                            var companion = www6.selectQF<tVisionTripCompanion>("IndividualDetailID", individualdetailID, "CompanionID", Convert.ToInt32(companionID), "CurrentUse", "Y");
                                            if (companion != null)
                                            {
                                                companion.CompanionName = companionINameKor;
                                                companion.CompanionNameEng = companionINameEng;
                                                companion.BirthDate = companionIBirth;
                                                companion.GenderCode = Convert.ToChar(companionIGender);
                                                companion.CurrentUse = delYN == "Y" ? 'N' : 'Y';
                                                companion.ModifyDate = DateTime.Now;
                                                companion.ModifyName = AdminLoginSession.GetCookie(context).name;
                                                companion.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();

                                                www6.update(companion);
                                            }
                                        }
                                    }
                                }
                                //dao.tVisionTripCompanion.InsertAllOnSubmit(vtCompanion);
                                foreach(var com in vtCompanion)
                                {
                                    www6.insert(com);
                                }
                            }
                            #endregion
                            #region 후원어린이정보
                            if (childList != "")
                            {
                                //dynamic results = JsonConvert.DeserializeObject<dynamic>(childList);  
                                var vtChildMeetList = new List<tVisionTripChildMeet>();
                                JArray jResult = JArray.Parse(childList);
                                for (int i = 0; i < jResult.Count; i++)
                                {
                                    string childKey = jResult[i]["id"].ToString();
                                    string childName = jResult[i]["name"].ToString();
                                    string delYN = jResult[i]["delyn"].ToString();
                                    string childmeetid = jResult[i]["childmeetid"].ToString();
                                    string childmeetType = jResult[i]["childmeettype"].ToString();

                                    if (childmeetType == "I")
                                    {
                                        if (childKey != "" && childmeetid == "" && delYN == "N")
                                        {
                                            vtChildMeetList.Add(new tVisionTripChildMeet()
                                            {
                                                ApplyID = applyID,
                                                ChildKey = childKey,
                                                ChildName = childName,
                                                ChildMeetType = 'I', //(I 직접입력, S 선택) 
                                                CurrentUse = 'Y',
                                                RegisterDate = DateTime.Now,
                                                RegisterName = AdminLoginSession.GetCookie(context).name,
                                                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                                            });
                                        }
                                        else
                                        {
                                            //var childmeet = dao.tVisionTripChildMeet.Where(p => p.ApplyID == applyID && p.ChildMeetID == Convert.ToInt32(childmeetid) && p.CurrentUse == 'Y').FirstOrDefault();
                                            var childmeet = www6.selectQF<tVisionTripChildMeet>("ApplyID", applyID, "ChildMeetID", Convert.ToInt32(childmeetid), "CurrentUse", "Y");
                                            if (childmeet != null)
                                            {
                                                childmeet.ChildKey = childKey;
                                                childmeet.ChildName = childName;
                                                childmeet.CurrentUse = delYN == "Y" ? 'N' : 'Y';
                                                childmeet.ModifyDate = DateTime.Now;
                                                childmeet.ModifyName = AdminLoginSession.GetCookie(context).name;
                                                childmeet.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();

                                                www6.update(childmeet);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //var childmeet= dao.tVisionTripChildMeet.Where(p => p.ApplyID == applyID && p.ChildKey == childKey && p.CurrentUse == 'Y').FirstOrDefault();
                                        var childmeet = www6.selectQF<tVisionTripChildMeet>("ApplyID", applyID, "ChildKey", childKey, "CurrentUse", "Y");
                                        if (childmeet != null && delYN == "Y")
                                        {
                                            childmeet.CurrentUse = 'N';
                                            childmeet.ModifyDate = DateTime.Now;
                                            childmeet.ModifyName = AdminLoginSession.GetCookie(context).name;
                                            childmeet.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();

                                            www6.update(childmeet);
                                        }
                                        else
                                        {
                                            vtChildMeetList.Add(new tVisionTripChildMeet()
                                            {
                                                ApplyID = applyID,
                                                ChildKey = childKey,
                                                ChildName = childName,
                                                ChildMeetType = 'S', //(I 직접입력, S 선택) 
                                                CurrentUse = 'Y',
                                                RegisterDate = DateTime.Now,
                                                RegisterName = AdminLoginSession.GetCookie(context).name,
                                                RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                                            });

                                        }
                                    }

                                    //if (childKey != "" && childmeetid == "" && delYN == "N")
                                    //{
                                    //    vtChildMeetList.Add(new tVisionTripChildMeet()
                                    //    {
                                    //        ApplyID = applyID,
                                    //        ChildKey = childKey,
                                    //        ChildName = childName,
                                    //        ChildMeetType = 'I', //(I 직접입력, S 선택) 
                                    //        CurrentUse = 'Y',
                                    //        RegisterDate = DateTime.Now,
                                    //        RegisterName = AdminLoginSession.GetCookie(context).name,
                                    //        RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                                    //    });
                                    //}
                                    //else
                                    //{
                                    //    var childmeet = dao.tVisionTripChildMeet.Where(p => p.ApplyID == applyID && p.ChildMeetID == Convert.ToInt32(childmeetid) && p.CurrentUse == 'Y').FirstOrDefault();
                                    //    if (childmeet != null)
                                    //    {
                                    //        childmeet.ChildKey = childKey;
                                    //        childmeet.ChildName = childName;
                                    //        childmeet.CurrentUse = delYN == "Y" ? 'N' : 'Y';
                                    //        childmeet.ModifyDate = DateTime.Now;
                                    //        childmeet.ModifyName = AdminLoginSession.GetCookie(context).name;
                                    //        childmeet.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();

                                    //    }
                                    //}
                                }
                                //dao.tVisionTripChildMeet.InsertAllOnSubmit(vtChildMeetList);
                                foreach (var cml in vtChildMeetList)
                                {
                                    www6.insert(cml);
                                }
                            }
                            #endregion
                            #region 첨부파일
                            if (attachList != "")
                            {
                                var vtAttachList = new List<tVisionTripAttach>();

                                JArray jResult = JArray.Parse(attachList);
                                for (int i = 0; i < jResult.Count; i++)
                                {
                                    string attachType = jResult[i]["type"].ToString();
                                    string attachName = jResult[i]["name"].ToString();
                                    string attachPath = jResult[i]["path"].ToString();
                                    string delYN = jResult[i]["delyn"].ToString();
                                    string attachID = jResult[i]["attachid"].ToString();

                                    if (attachPath != "" && attachID == "" && delYN == "N")
                                    {
                                        vtAttachList.Add(new tVisionTripAttach()
                                        {
                                            ApplyID = applyID,
                                            AttachType = attachType,
                                            AttachName = attachName,
                                            AttachPath = attachPath,
                                            CurrentUse = 'Y',
                                            RegisterDate = DateTime.Now,
                                            RegisterName = AdminLoginSession.GetCookie(context).name,
                                            RegisterID = AdminLoginSession.GetCookie(context).email.ToString()
                                        });
                                    }
                                    else
                                    {
                                        //var attach = dao.tVisionTripAttach.Where(p => p.ApplyID == applyID && p.AttachID == Convert.ToInt32(attachID) && p.CurrentUse == 'Y').FirstOrDefault();
                                        var attach = www6.selectQF<tVisionTripAttach>("ApplyID", applyID, "AttachID", Convert.ToInt32(attachID), "CurrentUse", "Y");
                                        if (attach != null)
                                        {
                                            attach.CurrentUse = delYN == "Y" ? 'N' : 'Y';
                                            attach.modifyDate = DateTime.Now;
                                            attach.ModifyName = AdminLoginSession.GetCookie(context).name;
                                            attach.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();

                                            www6.update(attach);
                                        }
                                    }
                                }
                                //dao.tVisionTripAttach.InsertAllOnSubmit(vtAttachList);
                                foreach (var al in vtAttachList)
                                {
                                    www6.insert(al);
                                }
                            }
                            #endregion

                        }
                        else if (grid_type == "info")
                        {
                            detail.LocalAccommodation = local_accommodation;
                            detail.LocalAddress = local_address;
                            detail.LocalTel = local_tel;
                            detail.DepartureDate = departure_date;
                            detail.ReturnDate = return_date;

                            apply.EmergencyContactName = emergencycontact_name;
                            apply.EmergencyContactRelation = emergencycontact_relation;
                            apply.EmergencyContactTel = emergencycontact_tel;
                        }
                        else if (grid_type == "participant")
                        {
                            apply.Tel = tel.Encrypt();
                            apply.Email = email.Encrypt();
                            detail.Remark = remark;

                            #region 사용자 정보 변경 - tel, email 
                            if (apply.ChangeYN.Equals("Y"))
                            {
                                try
                                {
                                    //WEB
                                    using (AuthLibDataContext daoAuth = new AuthLibDataContext())
                                    {
                                        //var entity = daoAuth.tSponsorMaster.First(p => p.UserID == AdminLoginSession.GetCookie(context).email.ToString());
                                        var entity = www6.selectQFAuth<tSponsorMaster>("UserID", AdminLoginSession.GetCookie(context).email.ToString());

                                        if (!string.IsNullOrEmpty(email))
                                            entity.Email = email.Encrypt();
                                        if (!string.IsNullOrEmpty(tel))
                                            entity.Phone = tel.Encrypt();

                                        //daoAuth.SubmitChanges();
                                        www6.updateAuth(entity);
                                    }

                                    // 후원회원인경우 
                                    if (!string.IsNullOrEmpty(userInfo.SponsorID))
                                    {
                                        var userInfoResult = UpdateCompassUserInfo(userInfo.SponsorID, userInfo.UserID, userInfo.SponsorName,
                                            "", "", "", "", "", tel, email);
                                        if (!userInfoResult.success)
                                        {
                                            result.success = false;
                                            result.message = userInfoResult.message;
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                                    result.success = false;
                                    result.message = "회원님의 정보를 수정하는 중 오류가 발생했습니다.(WEB)";
                                    //return;
                                }
                            }
                            #endregion
                        }
                        else if (grid_type == "addr")
                        {
                            apply.LocationType = location;
                            apply.Address1 = (address1 + "$" + country).Encrypt();
                            apply.Address2 = address2.Encrypt();
                            apply.ZipCode = zipcode.Encrypt();

                            #region 사용자 정보 변경 - 주소 
                            if (apply.ChangeYN.Equals("Y"))
                            {
                                try
                                {
                                    // 후원회원인경우 
                                    if (!string.IsNullOrEmpty(userInfo.SponsorID))
                                    {
                                        var userInfoResult = UpdateCompassUserInfo(userInfo.SponsorID, userInfo.UserID, userInfo.SponsorName, location, country, address1, address2, zipcode, "", "");
                                        if (!userInfoResult.success)
                                        {
                                            result.success = false;
                                            result.message = userInfoResult.message;
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                                    result.success = false;
                                    result.message = "회원님의 정보를 수정하는 중 오류가 발생했습니다.(WEB)";
                                    //return;
                                }
                            }
                            #endregion

                        }

                        apply.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();
                        apply.ModifyDate = DateTime.Now;
                        apply.ModifyName = AdminLoginSession.GetCookie(context).name;

                    }
                    //dao.SubmitChanges();
                    www6.update(apply);
                    www6.updateAuth(userInfo);
                    www6.update(detail);
                }
                catch (Exception ex)
                {
                    ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                    result.success = false;
                    result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
                }
            }
        }
        JsonWriter.Write(result, context);
    }

    JsonWriter UpdateCompassUserInfo(string sponsorId, string userid, string username, string location, string country, string addr1, string addr2, string zipcode, string mobile, string email)
    {
        UserInfo sess = new UserInfo();

        JsonWriter result = new JsonWriter() { success = true, action = "" };
        var actionResult = new JsonWriter();

        if (!string.IsNullOrEmpty(location) && !string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(zipcode) && !string.IsNullOrEmpty(addr1) && !string.IsNullOrEmpty(addr2))
        {
            #region 주소수정
            actionResult = new SponsorAction().GetAddress();
            if (!actionResult.success)
            {
                result.message = actionResult.message;
                return result;
            }
            SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)actionResult.data;

            actionResult = new SponsorAction().UpdateAddress(true, userid, sponsorId, username, addr_data.AddressType, location, country, zipcode, addr1, addr2);
            if (!actionResult.success)
            {
                result.message = actionResult.message;
                return result;
            }
            #endregion
        }

        #region 이메일 & 휴대전화 수정
        var comm_result = new SponsorAction().UpdateCommunications(true, sponsorId, email, mobile, null);
        if (!comm_result.success)
        {
            result.message = comm_result.message;
            return result;
        }
        #endregion

        return result;
    }

    #region 컴파스 연동 - 참가자 정보 전송
    void SendCompassIndividual(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        var applyid_list = context.Request["applyid_list"].EmptyIfNull();

        try
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                //var item = dao.sp_tVisionTripAdminIndi_Compass_list(applyid_list).ToJson();
                Object[] op1 = new Object[] { "applyId_List" };
                Object[] op2 = new Object[] { applyid_list };
                var item = www6.selectSP("sp_tVisionTripAdminIndi_Compass_list", op1, op2).DataTableToList<sp_tVisionTripAdminIndi_Compass_listResult>().ToJson();

                DataTable table = JsonConvert.DeserializeObject<DataTable>(item);

                DataSet ds = new DataSet();
                ds.Tables.Add(table);


                #region Compass - VT_Master 데이터 등록 : VisionTrip 일정 정보 등록 
                Object[] objSql = new object[1] { "VT_SponsorApplySave" };
                Object[] objParam = new object[28] { "DIVIS", "CountryCode", "VisitCategory",
                "ChildKey", "ChildName", "ConID", "SponsorName", "GenderCode",
                "AccompanyingPerson", "BirthDate", "GroupType",
                "CharacterType", "CommitmentCount", "Tel", "Email", "Nationality",
                "ReligionType", "Address", "ZipCode", "SpecialRequest",
                "ETC", "Remark", "Admin", "Progress", "RegisterID",
                "RegisterName", "SponsorID", "PersonalVisitDate"};

                //Object[] objValue = new object[28] { table.Rows[0]["DIVIS"],
                //    table.Rows[0]["CountryCode"],
                //    table.Rows[0]["VisitCategory"],
                //    table.Rows[0]["ChildKey"],
                //    table.Rows[0]["ChildName"],
                //    table.Rows[0]["ConID"],
                //    table.Rows[0]["SponsorName"],
                //    table.Rows[0]["GenderCode"],
                //    table.Rows[0]["AccompanyingPerson"],
                //    table.Rows[0]["BirthDate"], table.Rows[0]["GroupType"],
                //    table.Rows[0]["CharacterType"], table.Rows[0]["CommitmentCount"], table.Rows[0]["Tel"], table.Rows[0]["Email"], table.Rows[0]["Nationality"],
                //    table.Rows[0]["ReligionType"],
                //    table.Rows[0]["Address"],
                //    table.Rows[0]["ZipCode"],
                //    table.Rows[0]["SpecialRequest"],
                //    table.Rows[0]["ETC"],
                //    table.Rows[0]["Remark"],
                //    table.Rows[0]["Admin"],
                //    table.Rows[0]["Progress"], table.Rows[0]["RegisterID"],
                //    table.Rows[0]["RegisterName"], table.Rows[0]["SponsorID"], table.Rows[0]["PersonalVisitDate"]
                //};
                //var scheduleInsert = _www6Service.Tx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue);

                var scheduleInsert = _www6Service.Tx_ExecuteQueryDataSet("SqlCompass5", objSql, "SP", ds, objParam);
                if (scheduleInsert < 0)
                {
                    result.success = false;
                    result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
                }
                else
                {
                    if (applyid_list.Trim() != "")
                    {
                        foreach (var id in applyid_list.Split(','))
                        {
                            if (id != "")
                            {
                                int i;
                                bool check = int.TryParse(id, out i);
                                if (check)
                                {
                                    //var apply = dao.tVisionTripApply.Where(p => p.ApplyID == Convert.ToInt16(id) && p.CurrentUse == 'Y').FirstOrDefault();
                                    var apply = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt16(id), "CurrentUse", "Y");

                                    if (apply != null)
                                    {
                                        apply.CompassYN = 'Y';
                                        apply.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();
                                        apply.ModifyDate = DateTime.Now;
                                        apply.ModifyName = AdminLoginSession.GetCookie(context).name;

                                        //dao.SubmitChanges();
                                        www6.update(apply);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.success = false;
            result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
        }

        JsonWriter.Write(result, context);
    }

    void SendCompassPlan(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        var applyid_list = context.Request["applyid_list"].EmptyIfNull();

        try
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                //var item = dao.sp_tVisionTripAdminPlan_Compass_list(applyid_list).ToJson();
                Object[] op1 = new Object[] { "applyid_list" };
                Object[] op2 = new Object[] { applyid_list };
                var item = www6.selectSP("sp_tVisionTripAdminPlan_Compass_list", op1, op2).DataTableToList<sp_tVisionTripAdminPlan_Compass_listResult>().ToJson();
                DataTable table = JsonConvert.DeserializeObject<DataTable>(item);
                //ErrorLog.Write(HttpContext.Current, 0, "SendCompassPlan:2");
                DataSet ds = new DataSet();
                ds.Tables.Add(table);

                #region Compass - VT_Member 데이터 등록 : 기획/요청트립 참가자정보
                Object[] objSql = new object[1] { "VT_SponsorApplySave" };
                Object[] objParam = new object[] { "DIVIS", "ScheduleID", "GroupType", "CharacterType",
                                                "CommitmentCount",
                                                "ConID", "SponsorName",
                                                "GenderCode", "BirthDate", "Tel", "Email", "Nationality",
                                                "ReligionType", "Address", "ZipCode", "Remark",
                                                "RegisterID", "RegisterName", "SponsorID", "CancelYN" };

                var scheduleInsert = _www6Service.Tx_ExecuteQueryDataSet("SqlCompass5", objSql, "SP", ds, objParam);
                if (scheduleInsert < 0)
                {
                    result.success = false;
                    result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
                }
                else
                {
                    if (applyid_list.Trim() != "")
                    {
                        foreach (var id in applyid_list.Split(','))
                        {
                            if (id != "")
                            {
                                int i;
                                bool check = int.TryParse(id, out i);
                                if (check)
                                {
                                    //var apply = dao.tVisionTripApply.Where(p => p.ApplyID == Convert.ToInt16(id) && p.CurrentUse == 'Y').FirstOrDefault();
                                    var apply = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt16(id), "CurrentUse","Y");

                                    if (apply != null)
                                    {
                                        apply.CompassYN = 'Y';
                                        apply.ModifyID = AdminLoginSession.GetCookie(context).email.ToString();
                                        apply.ModifyDate = DateTime.Now;
                                        apply.ModifyName = AdminLoginSession.GetCookie(context).name;

                                        //dao.SubmitChanges();
                                        www6.update(apply);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            result.success = false;
            result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
        }

        JsonWriter.Write(result, context);
    }
    #endregion

    void SendSMS(HttpContext context)
    {
        CommonLib.WWW4Service.ServiceSoap _www4Service = new CommonLib.WWW4Service.ServiceSoapClient();

        JsonWriter result = new JsonWriter();
        result.success = true;

        var title = context.Request["title"].EmptyIfNull();
        var message = context.Request["message"].EmptyIfNull();
        var sendtype = context.Request["sendtype"].EmptyIfNull();
        var senddatetime = context.Request["senddatetime"].EmptyIfNull();
        var unixtimestamp = context.Request["unixtimestamp"].EmptyIfNull();
        var messagetype = context.Request["messagetype"].EmptyIfNull();
        var tellist = context.Request["tellist"].EmptyIfNull();//전화번호 목록
        //var applytype = context.Request["applytype"].EmptyIfNull();
        var sendtel = context.Request["sendtel"].EmptyIfNull();
        var groupno =  StringExtension.GetRandomString(3) + DateTime.Now.ToString("yyyyMMddHHmmss");//context.Request["groupno"].EmptyIfNull();
                                                                                                    //string GroupNo = StringExtension.GetRandomString(3) + DateTime.Now.ToString("yyyyMMddHHmmss");

        using (FrontDataContext dao = new FrontDataContext())
        {
            if (tellist != "")
            {
                var vtMessage = new List<tVisionTripMessage>();

                JArray jResult = JArray.Parse(tellist);
                for (int i = 0; i < jResult.Count; i++)
                {
                    int applyid = Convert.ToInt16(jResult[i]["applyid"].ToString());
                    string userid = jResult[i]["userid"].ToString();
                    string sponsor_name = jResult[i]["sponsor_name"].ToString();
                    string tel = jResult[i]["tel"].ToString();


                    string[] strSMSContent = new string[5];
                    strSMSContent[0] = tel.Replace("-", "").ToString(); //수신자 전화번호
                    strSMSContent[1] = sponsor_name; // 수신자 이름 
                    strSMSContent[2] = sendtel.Replace("-","");//발신자 전화번호
                    strSMSContent[3] = message; // 메세지 내용
                    strSMSContent[4] = "sms"; //전송타입 - 80byte 이상이면 자동으로 MMS 전송하기 때문에 전송에는 사실상 의미 없음   

                    string strResult = "";
                    var smsSendYN = 'N';
                    if (sendtype == "R")
                    {//예약 발송 

                        strResult = _www4Service.SendSMS_VisionTrip(strSMSContent, (unixtimestamp != "" ? unixtimestamp : "0"));
                    }
                    else
                    {
                        strResult = _www4Service.SendSMS(strSMSContent);
                    }

                    if (strResult.Substring(0, 2) == "OK")
                    {
                        smsSendYN = 'Y';
                    }
                    else
                    {
                        smsSendYN = 'N';
                    }
                    //성공여부 SendYN 에 값 저장 
                    vtMessage.Add(new tVisionTripMessage()
                    {
                        ApplyID = applyid,
                        UserID = userid,
                        Title = title,
                        GroupNo = groupno,
                        Message = message,
                        ReceiveTel = tel,
                        ReceiveID = userid,
                        ReceiveName = sponsor_name,
                        SendType = sendtype == "R" ? 'R' : 'D', //R:예약, D:직접
                        MessageType = messagetype,
                        ReservedDate = senddatetime != "" ? Convert.ToDateTime(senddatetime) : DateTime.Now,
                        UnixTimeStamp = unixtimestamp,
                        SendTel = sendtel,
                        SendYN = smsSendYN,
                        SendDate = DateTime.Now,
                        CurrentUse = 'Y',
                        RegisterID = AdminLoginSession.GetCookie(context).email.ToString(),
                        RegisterName = AdminLoginSession.GetCookie(context).name,
                        RegisterDate = DateTime.Now
                    });

                    //dao.tVisionTripMessage.InsertAllOnSubmit(vtMessage);
                    foreach (var ms in vtMessage)
                    {
                        www6.insert(ms);
                    }
                }
                //dao.SubmitChanges();


            }

        }

        JsonWriter.Write(result, context);
    }

    #region 입금목록
    void GetPaymentList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        var paymentType = context.Request["paymentType"].EmptyIfNull();
        var scheduleID = Convert.ToInt32(context.Request["scheduleID"].ValueIfNull("0"));
        var amount = Convert.ToInt32(context.Request["amount"].ValueIfNull("0"));
        var depositor = context.Request["depositor"].EmptyIfNull();
        var startdate = context.Request["startdate"].EmptyIfNull();
        var enddate = context.Request["enddate"].EmptyIfNull();


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPayment_list(page, rowsPerPage, paymentType, scheduleID, amount, depositor, startdate, enddate).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "paymentType", "scheduleID", "amount", "depositor", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, rowsPerPage, paymentType, scheduleID, amount, depositor, startdate, enddate };
            var list = www6.selectSP("sp_tVisionTripAdminPayment_list", op1, op2).DataTableToList<sp_tVisionTripAdminPayment_listResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    void GetAmountList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var paymentType = context.Request["paymentType"].EmptyIfNull();
        var scheduleID = Convert.ToInt32(context.Request["scheduleID"].ValueIfNull("0"));
        var startdate = context.Request["startdate"].EmptyIfNull();
        var enddate = context.Request["enddate"].EmptyIfNull();


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripAdminPayment_Amount(scheduleID, startdate, enddate, paymentType).ToList();
            Object[] op1 = new Object[] { "scheduleID", "startdate", "enddate", "paymentType" };
            Object[] op2 = new Object[] { scheduleID, startdate, enddate, paymentType };
            var list = www6.selectSP("sp_tVisionTripAdminPayment_Amount", op1, op2).DataTableToList<sp_tVisionTripAdminPayment_AmountResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }
    #endregion


}
