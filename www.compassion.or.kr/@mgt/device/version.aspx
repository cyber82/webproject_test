﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="version.aspx.cs" Inherits="mgt_device_version" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script src="/@mgt/common/js/flot/jquery.flot.min.js"></script>
	<script src="/@mgt/common/js/flot/jquery.flot.pie.min.js"></script>

	<script type="text/javascript">

		var onSubmit = function () {

			if (!validateForm([
				{ id: "#dv_min_android", msg: "최소 버전정보를 입력하세요", type: "version" },
				{ id: "#dv_max_android", msg: "최신 버전정보를 입력하세요", type: "version" },
				{ id: "#dv_link_android", msg: "최신버전 링크를 입력하세요", type: "url" },
				{ id: "#dv_min_iphone", msg: "최소 버전정보를 입력하세요", type: "version" },
				{ id: "#dv_max_iphone", msg: "최신 버전정보를 입력하세요", type: "version" },
				{ id: "#dv_link_iphone", msg: "최신버전 링크를 입력하세요", type: "url" }
				
			])) {
				return false;
			}

			return confirm("등록 하시겠습니까?");
		}

		$(function () {
			

		})
	</script>


</asp:Content>


<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	 <div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert">×</button>
		<h4><i class="icon fa fa-check"></i>Alert!</h4>
		최소 버전 : 앱 구동이 가능한 최소 버젼<br />
		 최신 버전 : 스토어에서 다운로드 가능한 최신 버젼<br />
		 최신 버전 링크 : 사용자가 다운로드 가능한 앱스토어 링크<br />
	</div>

	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
		</ul>
		<div class="tab-content">
            
			<div class="active tab-pane" id="tab1" runat="server">
                <div class="form-horizontal">

					<div class="col-xs-12">
						<h2 class="page-header"><i class="fa fa-plus-square"></i> 안드로이드</h2>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">최소 버전</label>
						<div class="col-sm-10" >
							<asp:TextBox runat="server" ID="dv_min_android" cssClass="form-control" Width="250" placeholder="앱 구동이 가능한 최소 버젼" ></asp:TextBox>
							
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">최신 버전</label>
						<div class="col-sm-10" >
							<asp:TextBox runat="server" ID="dv_max_android" cssClass="form-control" Width="250" placeholder="스토어에서 다운로드 가능한 최신 버젼"></asp:TextBox>
							
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">최신 버전 링크</label>
						<div class="col-sm-10" >
							<asp:TextBox runat="server" ID="dv_link_android" cssClass="form-control" Width="600" placeholder="사용자가 다운로드 가능한 앱스토어 링크"></asp:TextBox>
							
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">최신 버전 업데이트 내용<br />(optional)</label>
						<div class="col-sm-10">
							<asp:TextBox runat="server" ID="dv_comment_android" TextMode="MultiLine" Rows="10" Width="600" cssClass="form-control" ></asp:TextBox>
							
						</div>
					</div>

					<div class="col-xs-12">
						<h2 class="page-header"><i class="fa fa-plus-square"></i> 아이폰</h2>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">최소 버전</label>
						<div class="col-sm-10" >
							<asp:TextBox runat="server" ID="dv_min_iphone" cssClass="form-control" Width="250" placeholder="앱 구동이 가능한 최소 버젼" ></asp:TextBox>
							
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">최신 버전</label>
						<div class="col-sm-10" >
							<asp:TextBox runat="server" ID="dv_max_iphone" cssClass="form-control" Width="250" placeholder="스토어에서 다운로드 가능한 최신 버젼"></asp:TextBox>
							
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">최신 버전 링크</label>
						<div class="col-sm-10" >
							<asp:TextBox runat="server" ID="dv_link_iphone" cssClass="form-control" Width="600" placeholder="사용자가 다운로드 가능한 앱스토어 링크"></asp:TextBox>
							
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">최신 버전 업데이트 내용<br />(optional)</label>
						<div class="col-sm-10">
							<asp:TextBox runat="server" ID="dv_comment_iphone" TextMode="MultiLine" Rows="10" Width="600" cssClass="form-control" ></asp:TextBox>
							
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>

	<div class="box-footer clearfix text-center">
		<asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_Click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
	</div>

</asp:Content>