﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class mgt_device_version : AdminBasePage{
    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var list = dao.device_version.ToList();
            var list = www6.selectQ<device_version>();

            if (list.Any(p => p.dv_id == "android"))
            {
                var entity = list.First(p => p.dv_id == "android");
                dv_min_android.Text = entity.dv_min.ToString();
                dv_max_android.Text = entity.dv_max.ToString();
                dv_link_android.Text = entity.dv_link.ToString();
                dv_comment_android.Text = entity.dv_comment.ToString();
            }

            if (list.Any(p => p.dv_id == "iphone"))
            {
                var entity = list.First(p => p.dv_id == "iphone");
                dv_min_iphone.Text = entity.dv_min.ToString();
                dv_max_iphone.Text = entity.dv_max.ToString();
                dv_link_iphone.Text = entity.dv_link.ToString();
                dv_comment_iphone.Text = entity.dv_comment.ToString();
            }

        }

	}


	protected void btn_update_Click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            var has_android = false;
            var android = new device_version();
            
            var exist = www6.selectQ<device_version>("dv_id", "android");

            //if (dao.device_version.Any(p => p.dv_id == "android"))
            if (exist.Any())
            {
                //android = dao.device_version.First(p => p.dv_id == "android");
                android = www6.selectQF<device_version>("dv_id", "android");

                has_android = true;
            }

            android.dv_min = dv_min_android.Text;
            android.dv_max = dv_max_android.Text;
            android.dv_link = dv_link_android.Text;
            android.dv_comment = dv_comment_android.Text;
            android.dv_regdate = DateTime.Now;
            android.dv_a_id = AdminLoginSession.GetCookie(this.Context).identifier;

            if (!has_android)
            {
                android.dv_id = "android";
                //dao.device_version.InsertOnSubmit(android);
                www6.insert(android);
            }


            var has_iphone = false;
            var iphone = new device_version();
            
            var exist2 = www6.selectQ<device_version>("dv_id", "iphone");
            //if (dao.device_version.Any(p => p.dv_id == "iphone"))
            if (exist2.Any())
            {
                //iphone = dao.device_version.First(p => p.dv_id == "iphone");
                iphone = www6.selectQF<device_version>("dv_id", "iphone");
                has_iphone = true;
            }

            iphone.dv_min = dv_min_iphone.Text;
            iphone.dv_max = dv_max_iphone.Text;
            iphone.dv_link = dv_link_iphone.Text;
            iphone.dv_comment = dv_comment_iphone.Text;
            iphone.dv_regdate = DateTime.Now;
            iphone.dv_a_id = AdminLoginSession.GetCookie(this.Context).identifier;

            if (!has_iphone)
            {
                iphone.dv_id = "iphone";
                //dao.device_version.InsertOnSubmit(iphone);
                www6.insert(iphone);
            }

            //dao.SubmitChanges();
            www6.update(android);
            www6.update(iphone);
        }

		Master.ValueAction.Value = "none";
		Master.ValueMessage.Value = "적용되었습니다.";

	}
}