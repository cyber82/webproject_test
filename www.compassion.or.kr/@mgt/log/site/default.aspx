﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_log_site_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	

	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert">×</button>
		<h4><i class="icon fa fa-check"></i>Alert!</h4>
		사이트에서 발생하는 시스템오류를 조회하는 페이지입니다.
	</div>

    
	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>
		<div class="form-horizontal box-body" >
			<div class="box-body">
				<div class="form-group">
					<label for="s_keyword" class="col-sm-2 control-label">검색</label>
					<div class="col-sm-10">
						<asp:TextBox runat="server" ID="s_keyword" Width="415" cssClass="form-control inline" ></asp:TextBox>
						* 검색대상 : 코드 , URL , 내용
					</div>
				</div>
				<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">기간 검색(등록일)</label>
					<div class="col-sm-10">
						<div class="inline">
							<asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							~
						<asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>

						<div class="inline">
							<button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							<div class="btn-group">
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								<a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								<a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							</div>
						</div>

					</div>

				</div>

				<div class="box-footer clearfix text-center">
                    <asp:LinkButton runat="server" ID="btn_search" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
				</div>
				
			</div>
			
		</div>
	</div><!-- /.box -->
        
	<div class="box box-primary">
		
		 <div class="box-header">
            <h3 class="box-title">총 <asp:Literal runat=server ID=lbTotal/> 건</h3>
            
        </div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
		
			<table class="table table-hover table-bordered ">
				<colgroup>
					<col width="5%" />
					<col width="5%" />
					<col width="300px" />
					<col width="400px" />
					<col width="10%" />
				</colgroup>
				<thead>
					<tr>
						<th >번호</th>
						<th >코드</th>
						<th class="text-left">URL</th>
						<th class="text-left">메세지</th>
						<th >등록일</th>
					</tr>
				</thead>
				<tbody>
								
					<asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
						<ItemTemplate>
							<tr onclick="goPage('update.aspx' , { ls_id: '<%#Eval("ls_id")%>', p: '<%#paging.CurrentPage %>' , t: 'update' })" class="tr_link">
								<td ><asp:Literal runat=server ID=lbIdx></asp:Literal></td>
								<td ><%#Eval("ls_code")%></td>
								<td class="text-left">
									<div style="text-overflow:ellipsis; overflow:hidden;width:300px">
										<nobr><%#Eval("ls_url")%></nobr>
									</div>
								</td>
								<td class="text-left">
									<div style="text-overflow:ellipsis; overflow:hidden;width:400px">
										<nobr><%#Eval("ls_msg")%></nobr>
									</div>
								</td>

								<td ><%#Eval("ls_regdate" , "{0:MM.dd HH:mm:ss}")%></td>
							</tr>	
						</ItemTemplate>
						 <FooterTemplate>
							 <tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								 <td colspan="5" >데이터가 없습니다.</td>
							 </tr>
						</FooterTemplate>
					</asp:Repeater>	
				
				</tbody>
			</table>
		</div>

		 <div class="box-footer clearfix text-center">

			 <uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
			
        </div>

	</div>
	
	<!-- //partial page -->

</asp:Content>