﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;	
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class mgt_log_site_default : AdminBasePage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		Master.Content.Controls.BindAllTextBox(this.Context);

        string nowDate = DateTime.Now.ToString("yyyy-MM-dd");
        s_b_date.Text = nowDate;
        s_e_date.Text = nowDate;

        base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}
 
    protected override void GetList(int page) {

		Master.IsSearch = false;
		if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
			Master.IsSearch = true;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_log_site_error_list(page, paging.RowsPerPage, s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_log_site_error_list", op1, op2).DataTableToList<sp_log_site_error_listResult>().ToList();

            var total = 0;
            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();
        }
    }

    protected void ListBound(object sender, RepeaterItemEventArgs e) {
		
		if (e.Item.ItemType == ListItemType.Footer)
			return;

		log_site_error entity = e.Item.DataItem as log_site_error;
		((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
		
    }

}