﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_log_site_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	
	<script type="text/javascript">
		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1" data-toggle="tab">기본정보</a></li>
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1">
				<div class="form-horizontal">

					<div class="form-group">
						<label for="ls_regdate" class="col-sm-2 control-label control-label-text">등록일</label>
						<div class="col-sm-2"><asp:Literal runat=server ID=ls_regdate></asp:Literal>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label control-label-text">코드</label>
						<div class="col-sm-10">
							<asp:Literal runat=server ID=ls_code ></asp:Literal>
						</div>
					</div>
					<div class="form-group">
						<label for="inputName" class="col-sm-2 control-label control-label-text">URL</label>
						<div class="col-sm-10">
							<asp:Literal runat=server ID=ls_url ></asp:Literal>
						</div>
					</div>
					<div class="form-group">
						<label for="inputExperience" class="col-sm-2 control-label control-label-text">내용</label>
						<div class="col-sm-10">
							<asp:Literal runat=server ID=ls_msg ></asp:Literal>
						</div>
					</div>
					
				</div>
			</div>
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->


	 <div class="box-footer clearfix text-center">
		 <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>

</asp:Content>