﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class mgt_log_site_default : AdminBasePage {

	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["ls_id"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("ls_id", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context, "default.aspx");


		if (base.Action == "update") {
            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.log_site_error.First(p => p.ls_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<log_site_error>("ls_id", Convert.ToInt32(PrimaryKey));

                ls_code.Text = entity.ls_code;
                ls_url.Text = entity.ls_url;
                ls_msg.Text = entity.ls_msg;
                ls_regdate.Text = entity.ls_regdate.ToString("yyyy.MM.dd HH:mm:ss");
            }
			//btn_remove.Visible = auth.aa_auth_delete;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

}