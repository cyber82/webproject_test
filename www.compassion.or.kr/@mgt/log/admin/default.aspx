﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_log_site_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>


<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style type="text/css">
        .accordion.fold { height:14px;overflow:hidden;padding:0;}
        .accordion.open { height:auto;padding:0;}

    </style>
	<script type="text/javascript">
	    $(function () {
	        $(".btn-box-tool").click(function () {
	            if($(this).find('i').hasClass('fa-plus')){
	                close(this);
	            } else {
	                open(this);
	            }
	            return false;
	        });

	        var open = function(li){

	            $(li).find("i").addClass('fa-plus');
	            $(li).find("i").removeClass('fa-minus');

	            $(li).parentsUntil("td").removeClass('open');
	            $(li).parentsUntil("td").addClass('fold');

	            
	        }

	        var close = function (li) {
	            $(li).find("i").removeClass('fa-plus');
	            $(li).find("i").addClass('fa-minus');

	            $(li).parentsUntil("td").removeClass('fold');
	            $(li).parentsUntil("td").addClass('open');
	        }
	    });
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert">×</button>
		<h4><i class="icon fa fa-check"></i>Alert!</h4>
		관리자의 관리자페이지 활동 정보를 조회하는 페이지 입니다.
	</div>

	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>

		</div>


		<div class="form-horizontal box-body" >
			<div class="box-body">
				<div class="form-group">
					<label for="s_keyword" class="col-sm-2 control-label">검색</label>
					<div class="col-sm-10">
						<asp:TextBox runat="server" ID="s_keyword" Width="415" cssClass="form-control inline" ></asp:TextBox>
						* 검색대상 : 내용 , 관리자명
					</div>
				</div>
				<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">타입</label>
					<div class="col-sm-10">
						<asp:DropDownList runat="server" ID="la_type" class="form-control " style="width:150px">
							<asp:ListItem Value="" Text="전체"></asp:ListItem>
							<asp:ListItem Value="select" Text="조회"></asp:ListItem>
							<asp:ListItem Value="update" Text="수정"></asp:ListItem>
							<asp:ListItem Value="delete" Text="삭제"></asp:ListItem>
							<asp:ListItem Value="insert" Text="등록"></asp:ListItem>
						</asp:DropDownList>

					</div>
					
				</div>
				<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">메뉴</label>
					<div class="col-sm-10">
						<asp:DropDownList runat="server" ID="am_menu0" class="form-control inline" style="width:150px;" OnSelectedIndexChanged="am_menu0_SelectedIndexChanged" AutoPostBack="true">
							<asp:ListItem Value="" Text="전체"></asp:ListItem>
						</asp:DropDownList>
						
						<asp:DropDownList runat="server" ID="am_menu1" class="form-control inline" style="width:150px;" >
							<asp:ListItem Value="" Text="전체"></asp:ListItem>
						</asp:DropDownList>

					</div>
					
				</div>
			
				<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">기간 검색(등록일)</label>
					<div class="col-sm-10">
						<div class="inline">
							<asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							~
						<asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>

						<div class="inline">
							<button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							<div class="btn-group">
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								<a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								<a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							</div>
						</div>

					</div>

				</div>

				<div class="box-footer clearfix text-center">
                    <asp:LinkButton runat="server" ID="btn_search" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
				</div>
				
			</div>
			<!--
			<div class="box-footer">
				<button type="submit" class="btn btn-default">Cancel</button>
				<button type="submit" class="btn btn-info pull-right">Sign in</button>
			</div>
			-->
		</div>
	</div><!-- /.box -->
        
	<div class="box box-primary">
		
		 <div class="box-header">
            <h3 class="box-title">총 <asp:Literal runat=server ID=lbTotal/> 건</h3>
            
        </div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
		
			<table class="table table-hover table-bordered ">
				<colgroup>
					<col width="5%" />
					<col width="5%" />
					<col width="10%" />
					<col width="10%" />
				
					<col width="*" />
					<col width="15%" />
				</colgroup>
				<thead>
					<tr>
						<th>번호</th>
						<th>구분</th>
						<th>메뉴</th>
						<th>관리자명</th>
						<th class="text-left">내용</th>
						<th>등록일</th>
					</tr>
				</thead>
				<tbody>
								
					<asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
						<ItemTemplate>
							<tr>
								<td><asp:Literal runat=server ID=lbIdx></asp:Literal></td>
								<td><%#AdminLog.GetTypeName(Eval("la_type"))%></td>
								<td><%#Eval("am_name")%></td>
								<td><%#Eval("a_name")%></td>
								<td class="text-left" style="word-break:break-word;">
                                    <div class="accordion fold">
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" style="padding-top:0" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                        </div>
                                        <%#Eval("la_comment")%>
                                    </div>
								</td>
								<td><%#Eval("la_regdate" , "{0:MM.dd HH:mm:ss}")%></td>
							</tr>	
						</ItemTemplate>
						 <FooterTemplate>
							 <tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								 <td colspan="6">데이터가 없습니다.</td>
							 </tr>
						</FooterTemplate>
					</asp:Repeater>	
				
				</tbody>
			</table>
		</div>

		 <div class="box-footer clearfix text-center">

			 <uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
			 
        </div>

	</div>
	
	<!-- //partial page 

</asp:Content>