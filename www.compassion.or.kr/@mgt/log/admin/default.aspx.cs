﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;	
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class mgt_log_site_default : AdminBasePage {
	


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
		
		List<admin_menu> list = GetAdminMenu(0, "");
		setAdminMenu0(list);
	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected List<admin_menu> GetAdminMenu(int depth, string group)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            List<admin_menu> list = null;
            if (depth == 0)
            {
                //list = dao.admin_menu.Where(p => p.am_depth == depth).ToList();
                list = www6.selectQ<admin_menu>("am_depth", depth);
            }
            else
            {
                //list = dao.admin_menu.Where(p => p.am_depth == depth && p.am_group == group).ToList();
                list = www6.selectQ<admin_menu>("am_depth", depth, "am_group", group);
            }
            return list;
        }
	}

	protected void setAdminMenu0(List<admin_menu> list) {
		foreach (var a in list) {
			var item = new ListItem(a.am_name, a.am_code);
			am_menu0.Items.Add(item);
		}
	}

	protected void setAdminMenu1(List<admin_menu> list) {
		am_menu1.Items.Clear();
		am_menu1.Items.Add(new ListItem("전체", ""));
		foreach (var a in list) {
			var item = new ListItem(a.am_name, a.am_code);
			am_menu1.Items.Add(item);
		}
	}
 

    protected override void GetList(int page) {

		Master.IsSearch = false;
		if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "" || la_type.SelectedValue != "" || am_menu1.SelectedValue != "") {
			Master.IsSearch = true;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_log_admin_action_list(page, paging.RowsPerPage, s_keyword.Text, la_type.SelectedValue, am_menu1.SelectedValue, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword", "la_type", "am_code", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_keyword.Text, la_type.SelectedValue, am_menu1.SelectedValue, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_log_admin_action_list", op1, op2).DataTableToList<sp_log_admin_action_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();
        }

    }

    protected void ListBound(object sender, RepeaterItemEventArgs e) {
		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				log_admin_action entity = e.Item.DataItem as log_admin_action;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
			}
		}

    }

	protected void am_menu0_SelectedIndexChanged(object sender, EventArgs e) {
		List<admin_menu> list = GetAdminMenu(1, am_menu0.SelectedValue);
		setAdminMenu1(list);
	}
	
}