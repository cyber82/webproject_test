﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="adminUpdate.aspx.cs" Inherits="mgt_adminUpdate" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">

	<script type="text/javascript">

		$(function () {
			$(".readonly").attr("readonly", "readonly");
		})
		

		var onSubmit = function () {

			return true;
		}


	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	<input type="hidden" runat="server" id="s_pg_id" />

	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">		
			<li runat="server" id="tabm1" class="active"><a>기본정보</a></li>
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">아이디</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=a_id ReadOnly="true" CssClass=form-control Width=500 placeholder="" MaxLength="10"></asp:TextBox>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">새로운 비밀번호</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=pwd TextMode="Password" CssClass=form-control Width=500 placeholder="변경시만 입력" MaxLength="20"></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">이름</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=a_name CssClass=form-control Width=500  placeholder="" MaxLength="50"></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">부서</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=a_part  CssClass=form-control Width=500 placeholder="" MaxLength="50"></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">직위</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=a_position  CssClass=form-control Width=500 placeholder="" MaxLength="50"></asp:TextBox>
						</div>
					</div>
					
				
					
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">

		 <asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>

	</div>


</asp:Content>