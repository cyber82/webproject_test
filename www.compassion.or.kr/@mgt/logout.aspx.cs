﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class mgt_logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		HttpContext.Current.Session.Abandon();
		//context.Session[Constants.ADMIN_MENU_SESSION_NAME] 
		AdminLoginSession.ClearCookie(this.Context);
		Response.Redirect("/@mgt/?auth=" + ConfigurationManager.AppSettings["admin_auth_key"]);
    }
}