﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_advocate_download_packet_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		base.BoardType = "download_packet";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);

		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");

		if (base.Action == "update") {
            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));

                b_title.Text = entity.b_title;
                b_display.Checked = entity.b_display;
                b_summary.Value = entity.b_summary;


                // thumb파일 가져오기
                var entity2 = www6.selectQ<file>("f_group", Uploader.FileGroup.image_board.ToString(), "f_ref_id", PrimaryKey.ToString());
                if (entity2.Any())
                {
                    //var f = dao.file.First(p => p.f_group == Uploader.FileGroup.image_board.ToString() && p.f_ref_id == PrimaryKey.ToString());
                    var f = www6.selectQF<file>("f_group", Uploader.FileGroup.image_board.ToString(), "f_ref_id", PrimaryKey.ToString());
                    Thumb = f.f_name;
                }

            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			

			this.LoadFiles();
			ShowFiles();

		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}




	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {


		var arg = new board() {
			b_title = b_title.Text, b_content = "", 
			b_a_id = AdminLoginSession.GetCookie(this.Context).identifier,
			b_display = b_display.Checked,
			b_summary = b_summary.Value,
			b_meta_keyword = "",
			b_regdate = DateTime.Now
		};


		
		var fileList = base.GetTemporaryFiles();

        using (AdminDataContext dao = new AdminDataContext())
        {
            if (Action == "update")
            {
                //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));
                entity.b_title = arg.b_title;
                entity.b_content = arg.b_content;
                entity.b_a_id = arg.b_a_id;
                entity.b_display = arg.b_display;
                entity.b_hot = arg.b_hot;
                entity.b_main = arg.b_main;
                entity.b_regdate = arg.b_regdate;
                entity.b_meta_keyword = arg.b_meta_keyword;
                entity.b_summary = arg.b_summary;
                //dao.SubmitChanges();
                www6.update(entity);

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
            }
            else
            {
                arg.b_hits = 0;
                arg.b_type = this.BoardType;
                //dao.SubmitChanges();
                //dao.board.InsertOnSubmit(arg);
                www6.insert(arg);

                PrimaryKey = arg.b_id.ToString();

                foreach (var file in fileList)
                {
                    file.f_ref_id = PrimaryKey;
                }
                SaveTemporaryFiles(fileList);


                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";
            }


            // 썸네일
            //dao.file.DeleteAllOnSubmit(dao.file.Where(p => p.f_ref_id == PrimaryKey && p.f_group == Uploader.FileGroup.image_board.ToString()));
            string delStr = string.Format("delete from [file] where f_ref_id='{0}' and f_group='{1}'", PrimaryKey, Uploader.FileGroup.image_board.ToString());
            www6.cud(delStr);

            if (Thumb != "")
            {
                var thumb = new file()
                {
                    f_group = Uploader.FileGroup.image_board.ToString(),
                    f_group2 = "",
                    f_ref_id = PrimaryKey,
                    f_name = Thumb,
                    f_size = 0,
                    f_regdate = DateTime.Now

                };
                //dao.file.InsertOnSubmit(thumb);
                www6.insert(thumb);
            }

            // pdf
            //dao.file.DeleteAllOnSubmit(dao.file.Where(p => p.f_ref_id == PrimaryKey && p.f_group == Uploader.FileGroup.file_board.ToString()));
            string delStr2 = string.Format("delete from [file] where f_ref_id='{0}' and f_group='{1}'", PrimaryKey, Uploader.FileGroup.file_board.ToString());
            www6.cud(delStr2);

            if (fileList.Count > 0)
            {
                //dao.file.InsertAllOnSubmit(fileList);

                foreach (file fi in fileList)
                {
                    www6.insert(fi);
                }
            }

            //dao.SubmitChanges();
        }


	}

	protected void LoadFiles()
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            var list = base.GetTemporaryFiles("files");
            //var fileList = (from p in dao.file
            //                orderby p.f_order ascending
            //                where p.f_ref_id == PrimaryKey && p.f_group == Uploader.FileGroup.file_board.ToString()
            //                select p).ToList();
            var fileList = www6.selectQ<file>("f_ref_id", PrimaryKey, "f_group", Uploader.FileGroup.file_board.ToString(), "f_order asc");

            list.AddRange(fileList);
            this.SaveTemporaryFiles(list, "files");
        }
	}

	// file
	void ShowFiles() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			b_image.Src = base.Thumb.WithFileServerHost();
			b_image.Attributes["data-exist"] = "1";
		}

		repeater.DataSource = base.GetTemporaryFiles();
		repeater.DataBind();
	}

	protected void btn_remove_file_click(object sender, EventArgs e) {
		base.FileRemove(sender);
		this.ShowFiles();
	}



	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		switch (temp_file_type.Value) {
			case "thumb":
				base.Thumb = temp_file_name.Value;
				break;
			default:
				base.FileClear("files");
				base.FileAdd(Convert.ToInt32(temp_file_size.Value), temp_file_name.Value);
				break;
		}
		this.ShowFiles();

	}

	protected void btn_remove_click(object sender, EventArgs e) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
            //dao.board.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            var delStr = string.Format("Delete from board where b_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}
}