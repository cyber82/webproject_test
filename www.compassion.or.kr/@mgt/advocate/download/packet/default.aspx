﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_particitipation_video_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
<script type="text/javascript" src="/assets/angular/1.4.8/angular.js"></script>
<script type="text/javascript" src="/@mgt/advocate/download/packet/default.js"></script>
<script type="text/javascript">

    var onSubmit = function () {

        var scope = angular.element($("#defaultCtrl")).scope();
        if (scope.list.length > 7) {
            alert("노출갯수는 7개입니다");
            return false;
        }
        return confirm("적용하시겠습니까?");
    }

    function allowDrop(ev) {
        ev.preventDefault();
    }

    function drag(ev) {

        ev.dataTransfer.setData("text", ev.target.id);
    }

    function drop(ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");

        var moveElement = $("#" + data);
        var targetElement = $(ev.target.closest("li"))

        var moveIndex = $("#preview li").index(moveElement);
        var targetIndex = $("#preview li").index(targetElement);

        swapElements(moveElement[0], targetElement[0]);

        var scope = angular.element($("#defaultCtrl")).scope();
        var temp = scope.list[moveIndex];
        scope.list[moveIndex] = scope.list[targetIndex];
        scope.list[targetIndex] = temp;

        $("#visible_list").val(JSON.stringify(scope.list));
    }

    function swapElements(elm1, elm2) {
        var parent1, next1,
            parent2, next2;

        if (elm1 && elm2) {
            parent1 = elm1.parentNode;
            next1 = elm1.nextSibling;
            parent2 = elm2.parentNode;
            next2 = elm2.nextSibling;

            parent1.insertBefore(elm2, next1);
            parent2.insertBefore(elm1, next2);
        }

    }


   
</script>

</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

<div ng-app="defaultApp" ng-controller="defaultCtrl" id="defaultCtrl">
    <input type="hidden" runat="server" id="visible_list" />

    
	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>
		<div class="form-horizontal box-body" >
			<div class="box-body">
				<div class="form-group">
					<label for="s_display" class="col-sm-2 control-label">노출</label>
					<div class="col-sm-3" style="margin-top:5px;">
					    <asp:RadioButtonList runat="server" ID="s_display" RepeatDirection="Horizontal" RepeatLayout="Flow" >
						    <asp:ListItem Selected="True"  Value="-1" Text="전체&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						    <asp:ListItem Value="1" Text="노출&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						    <asp:ListItem Value="0" Text="미노출&nbsp;&nbsp;&nbsp;"></asp:ListItem>
					    </asp:RadioButtonList>
					</div>
				</div>
                <!--
				<div class="form-group">
					<label for="s_main" class="col-sm-2 control-label">메인노출</label>
                    <div class="col-sm-3" style="margin-top:5px;">
                        <input type="checkbox" class="flat-red" runat="server" id="s_main">
                    </div>
                </div>
                -->
				<div class="form-group">
					<label for="s_keyword" class="col-sm-2 control-label">검색</label>
					<div class="col-sm-10">
						<asp:TextBox runat="server" ID="s_keyword" Width="415" cssClass="form-control inline"  OnTextChanged="search" AutoPostBack="true"></asp:TextBox>
						* 검색대상 : 제목
					</div>
				</div>
				<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">기간 검색(등록일)</label>
					<div class="col-sm-10">
						<div class="inline">
							<asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							~
						<asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>

						<div class="inline">
							<button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							<div class="btn-group">
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								<a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								<a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							</div>
						</div>

					</div>

				</div>

				<div class="box-footer clearfix text-center">
                    <asp:LinkButton runat="server" ID="btn_search" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
				</div>
				
			</div>
			
		</div>
	</div><!-- /.box -->


    <div class="box box-primary">
		
		 <div class="box-header">
            <h3 class="box-title">총 <asp:Literal runat=server ID=lbTotal/> 건</h3>
        </div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
		
			<table class="table table-hover table-bordered ">
				<colgroup>
                    <col width="5%" />
				    <col width="5%" />
				    <col width="25%" />
				    <col width="10%" />
				    <col width="10%" />
				    <col width="10%" />
				</colgroup>
				<thead>
					<tr>
                        <th>선택</th>
				        <th>번호</th>
				        <th>제목</th>
				        <th>등록일</th>
				        <th>조회수</th>
				        <th>노출여부</th>

					</tr>
				</thead>
				<tbody>
								
					<asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
				        <ItemTemplate>
					        <tr onclick="goPage('update.aspx' , { c: <%#Eval("b_id")%>, p: '<%#paging.CurrentPage %>' , t: 'update' })" class="tr_link">
                                <td><input type=checkbox class="selected" data-id="<%#Eval("b_id") %>" data-title="<%#Eval("b_title") %>" data-thumb="<%#Eval("b_thumb") %>"/> </td>
						        <td><asp:Literal runat=server ID=lbIdx></asp:Literal></td>
						        <td><div style="max-width:1000px; white-space:pre-line; "><%#Eval("b_title")%></div></td>
						        <td><%#Eval("b_regdate" , "{0:yy.MM.dd}")%></td>
						        <td><%#Eval("b_hits")%></td>
						        <td><asp:Literal runat=server ID=lbDisplay></asp:Literal></td>
					        </tr>	
				        </ItemTemplate>
				        <FooterTemplate>
					        <tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
						        <td colspan="6">데이터가 없습니다.</td>
					        </tr>
				        </FooterTemplate>

			        </asp:Repeater>		
				
				</tbody>
			</table>
		</div>
        
		<div class="box-footer clearfix text-center">
            <uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />  
        </div>
        

        <div class="box-footer clearfix text-center">
            <asp:LinkButton runat="server" href="#" ID="btn_add" OnClientClick="goPage('update.aspx' , {t:'insert'})" CssClass="btn btn-bitbucket pull-right">신규등록</asp:LinkButton>
        </div>

	</div>


    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">적용하기</h3>
            <!--
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            -->
        </div>

        <!-- /.box-body -->
        <div class="box-body">
            <ul class="mailbox-attachments clearfix" id="preview" ondrop="drop(event)" ondragover="allowDrop(event)">
                <li ng-repeat="item in list track by $index" data-id="{{item.b_id}}" id="{{item.b_id}}" draggable="true" ondragstart="drag(event)">
                    <span class="mailbox-attachment-icon has-img" draggable="false">
                        <img ng-src="{{item.b_thumb}}" id="{{item.b_id}}">
                    </span>
                    <div class="mailbox-attachment-info" draggable="false">
                        <span class="mailbox-attachment-name">
                            {{item.b_title}}
                            <a href="javascript:;" class="btn btn-default btn-xs pull-right " ng-click="uncheck(item.b_id)"><i class="fa fa-remove"></i></a>
                        </span>
                    </div>
                </li>
            </ul>
            
           <span  ng-if="list != null && !list.length">리스트의 체크박스로 선택할 수 있습니다.</span>
        </div>
        <!-- /.box-footer -->
        <div class="box-footer">
            <asp:LinkButton runat="server" ID="btnSave" OnClick="btnSave_Click" OnClientClick="return onSubmit()" class="btn btn-bitbucket"><i class="fa fa-save"></i> 적용</asp:LinkButton>
        </div>
        <!-- /.box-footer -->
    </div>
</div>

</asp:Content>