﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_particitipation_video_default : AdminBoardPage{


	const int VISIBLE_COUNT = 7;

    protected override void OnBeforePostBack(){
        base.OnBeforePostBack();

        Master.Content.Controls.BindAllTextBox(this.Context);

        base.LoadComplete += new EventHandler(list_LoadComplete);
        v_admin_auth auth = base.GetPageAuth();
        btn_add.Visible = auth.aa_auth_create;


        // 노출중인 컨텐츠만 가져오기
        using (AdminDataContext dao = new AdminDataContext())
        {
            //visible_list.Value = dao.sp_board_file_list(1, VISIBLE_COUNT, "download_packet", 1, "", "", "").ToJson();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "b_type", "display", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { 1, VISIBLE_COUNT, "download_packet", 1, "", "", "" };
            visible_list.Value = www6.selectSP("sp_board_file_list", op1, op2).DataTableToList<sp_board_file_listResult>().ToJson();
        }
    }

    protected override void OnAfterPostBack(){
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();
    }

    protected override void GetList(int page) {

		Master.ValueMessage.Value = "";

		Master.IsSearch = false;
		if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
			Master.IsSearch = true;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_board_file_list(page, paging.RowsPerPage, "download_packet", Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "b_type", "display", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, "download_packet", Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_board_file_list", op1, op2).DataTableToList<sp_board_file_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();



        }

    }


    protected void ListBound(object sender, RepeaterItemEventArgs e){

        if (e.Item.ItemType == ListItemType.Footer)
            return;

		sp_board_file_listResult entity = e.Item.DataItem as sp_board_file_listResult;
        ((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
        ((Literal)e.Item.FindControl("lbDisplay")).Text = entity.b_display ? "O" : "X";


    }


	protected void btnSave_Click(object sender, EventArgs e) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var hide_list = dao.board.Where(p => p.b_display && p.b_type == "download_packet");
            var hide_list = www6.selectQ<board>("b_display", 1, "b_type", "download_packet");
           
            foreach (var entity in hide_list)
            {
                entity.b_display = false;
            }

            foreach (var showEntity in visible_list.Value.ToObject<List<board>>())
            {
                //var entity = dao.board.First(p => p.b_id == showEntity.b_id);
                //entity.b_display = true;
                string updateStr = string.Format("update board set b_display = 1 where b_id = {0}", showEntity.b_id);
                www6.cud(updateStr);
            }


            dao.SubmitChanges();
            Master.ValueMessage.Value = "적용되었습니다.";
        }
			
	}
}
