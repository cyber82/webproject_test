﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_advocate_notice_update : AdminBoardPage {

	
	protected string Thumb {
		set {
			this.ViewState["thumb"] = value;
		}
		get {
			return (this.ViewState["thumb"] != null) ? this.ViewState["thumb"].ToString() : string.Empty;
		}
	}



	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];
		base.BoardType = "advocate";

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");




		// 게시판 종류
		foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "board_sub").OrderBy(p => p.cd_order)) {
			b_sub_type.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}


		if (base.Action == "update")
        {

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));

                b_title.Text = entity.b_title;
                b_content.InnerHtml = entity.b_content;
                b_display.Checked = entity.b_display;
                b_main.Checked = entity.b_main;
                b_sub_type.SelectedValue = entity.b_sub_type;
                b_summary.Text = entity.b_summary;

                b_regdate.Text = entity.b_regdate.ToString("yyyy.MM.dd HH:mm");

                //var file = dao.file.FirstOrDefault(p => p.f_ref_id == PrimaryKey);
                var file = www6.selectQF<file>("f_ref_id", PrimaryKey);

                if (file != null)
                {
                    Thumb = file.f_name;
                }

            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			ph_regdate.Visible = true;
            
			this.ShowFiles();


		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}


	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var thumb = this.Thumb;

		var arg = new board() {
			b_title = b_title.Text, b_content = b_content.InnerHtml.ToHtml(),
			b_a_id = AdminLoginSession.GetCookie(this.Context).identifier,
			b_display = b_display.Checked,
			b_hot = b_sub_type.SelectedValue == "all",
			b_main = b_main.Checked,
			b_type = base.BoardType,
			b_summary = b_summary.Text,
			b_sub_type = b_sub_type.SelectedValue,
			b_regdate = DateTime.Now,
			b_hits = 0
		};


		using (AdminDataContext dao = new AdminDataContext()) {
			if (base.Action == "update") {

                //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));

                entity.b_title = arg.b_title;
                entity.b_content = arg.b_content;
                entity.b_a_id = arg.b_a_id;
                entity.b_display = arg.b_display;
                entity.b_hot = arg.b_hot;
                entity.b_main = arg.b_main;
                entity.b_regdate = arg.b_regdate;
                entity.b_type = arg.b_type;
                entity.b_sub_type = arg.b_sub_type;
                entity.b_summary = arg.b_summary;
                //dao.SubmitChanges();

                www6.update(entity);

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

				Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "수정되었습니다.";
			} else {
                
				arg.b_hits = 0;
                //dao.board.InsertOnSubmit(arg);
                //dao.SubmitChanges();
                
                www6.insert(arg);


                PrimaryKey = arg.b_id.ToString();

				base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

				Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "등록되었습니다.";
			}


            //dao.file.DeleteAllOnSubmit(dao.file.Where(p => p.f_ref_id == PrimaryKey && p.f_group == FileGroup.ToString()));
            //www6.cud(MakeSQL.delQ(0, "file", "f_ref_id", PrimaryKey, "f_group", FileGroup.ToString()));
            var fiList = www6.selectQ<file>("f_ref_id", PrimaryKey, "f_group", FileGroup.ToString());
            www6.delete(fiList);

            if (this.Thumb.EmptyIfNull() != "")
            {
				var file = new file()
                {
					f_group = "file_board",
					f_ref_id = PrimaryKey,
					f_name = this.Thumb,
					f_size = 0,
					f_order = 0,
					f_regdate = DateTime.Now
				};

                //dao.file.InsertOnSubmit(file);
                //dao.SubmitChanges();
                
                www6.insert(file);
            }
			
			//dao.SubmitChanges();
		}

			

	}


	protected void btn_remove_click(object sender, EventArgs e) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
            string delStr = string.Format("delete from board where b_id={0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            //dao.board.DeleteOnSubmit(entity);
            //dao.SubmitChanges();

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		switch (temp_file_type.Value) {
			case "thumb":
				this.Thumb = temp_file_name.Value;
				break;
			default:
				base.FileClear("files");
				base.FileAdd(Convert.ToInt32(temp_file_size.Value), temp_file_name.Value);
				break;
		}
		this.ShowFiles();
	}


	// 파일
	void ShowFiles() {
		if (string.IsNullOrEmpty(this.Thumb)) {
			b_thumb.Src = "/@mgt/common/img/empty_thumb.png";
			b_thumb.Attributes["data-exist"] = "0";
			btn_remove_file.Visible = false;
		} else {
			b_thumb.Src = this.Thumb.WithFileServerHost();
			b_thumb.Attributes["data-exist"] = "1";
			btn_remove_file.Visible = true;
		}
	}


	protected void btn_remove_file_Click(object sender, EventArgs e) {
		this.Thumb = "";
		ShowFiles();
	}
}