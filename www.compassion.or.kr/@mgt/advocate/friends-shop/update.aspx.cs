﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_advocate_friends_shop_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.BoardType = "sponsor_news";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");

		if (base.Action == "update") {

            using (StoreDataContext dao = new StoreDataContext())
            {
                //var entity = dao.friends_shop.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQFStore<friends_shop>("idx", Convert.ToInt32(PrimaryKey));

                del_flag.Checked = !(bool)entity.del_flag;
                writer.Text = entity.writer;
                writer_tel.Text = entity.writer_tel;
                writer_email.Text = entity.writer_email;
                shop_name.Text = entity.shop_name;
                shop_type.SelectedValue = entity.shop_type;
                shop_tel.Text = entity.shop_tel;
                shop_zip.Text = entity.shop_zip;
                shop_addr.Text = entity.shop_addr;
                shop_homepage.Text = entity.shop_homepage;
                is_online.Checked = entity.is_online;
                if (entity.part.EmptyIfNull() != "")
                {
                    var pros = entity.part.Split(',');
                    foreach (var val in pros)
                    {
                        if (part.Items.FindByValue(val) != null)
                        {
                            part.Items.FindByValue(val).Selected = true;
                        }
                    }
                }
                shop_intro.Text = entity.shop_intro;
                Thumb = entity.shop_img;
                reg_date.Text = ((DateTime)entity.reg_date).ToString("yyyy.MM.dd HH:mm");

            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			ph_regdate.Visible = true;

            ShowFiles(); 

			
		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var thumb = base.Thumb;


        using (StoreDataContext dao = new StoreDataContext())
        {
            //var entity = dao.friends_shop.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQFStore<friends_shop>("idx", Convert.ToInt32(PrimaryKey));
            entity.del_flag = !del_flag.Checked;
            entity.writer = writer.Text;
            entity.writer_tel = writer_tel.Text;
            entity.writer_email = writer_email.Text;
            entity.shop_name = shop_name.Text;
            entity.shop_type = shop_type.SelectedValue;
            entity.shop_tel = shop_tel.Text;
            entity.shop_zip = shop_zip.Text;
            entity.shop_addr = shop_addr.Text;
            entity.shop_homepage = shop_homepage.Text;
            string selectedItems = String.Join(",", part.Items.OfType<ListItem>().Where(r => r.Selected).Select(r => r.Value));
            entity.part = selectedItems;
            entity.shop_intro = shop_intro.Text;
            entity.shop_img = Thumb;
            entity.is_online = is_online.Checked;

            //dao.SubmitChanges();
            www6.updateStore(entity);

            base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "수정되었습니다.";
        }


	}

	protected void btn_remove_click(object sender, EventArgs e) {

		using (StoreDataContext dao = new StoreDataContext()) {
            //var entity = dao.friends_shop.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            //dao.friends_shop.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from friends_shop where idx={0}", Convert.ToInt32(PrimaryKey));
            www6.cudStore(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
		}

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}


	void ShowFiles() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			shop_img.Src = base.Thumb.WithFileServerHost();
			shop_img.Attributes["data-exist"] = "1";
		}
	}


	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		base.Thumb = temp_file_name.Value;
		this.ShowFiles();

	}

}