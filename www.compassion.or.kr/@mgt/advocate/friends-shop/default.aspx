﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_advocate_friends_shop_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
<script type="text/javascript">
    $(function () {

    });
</script>

</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    <input type="hidden" runat="server" id="showSearch" />
    
	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>
		<div class="form-horizontal box-body" >
			<div class="box-body">
				<div class="form-group">
					<label for="s_display" class="col-sm-2 control-label">노출</label>
					<div class="col-sm-3" style="margin-top:5px;">
					    <asp:RadioButtonList runat="server" ID="s_display" RepeatDirection="Horizontal" RepeatLayout="Flow" >
						    <asp:ListItem Selected="True"  Value="-1" Text="전체&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						    <asp:ListItem Value="1" Text="노출&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						    <asp:ListItem Value="0" Text="미노출&nbsp;&nbsp;&nbsp;"></asp:ListItem>
					    </asp:RadioButtonList>
					</div>
				</div>
                
				<div class="form-group">
					<label for="s_display" class="col-sm-2 control-label">지역</label>
					<div class="col-sm-3" style="margin-top:5px;">
					    <asp:DropDownList runat="server" ID="addr" class="form-control" style="width:80px;">
					        <asp:ListItem Text="지역" Value=""></asp:ListItem>
					        <asp:ListItem Text="서울" Value="서울"></asp:ListItem>
					        <asp:ListItem Text="부산" Value="부산"></asp:ListItem>
					        <asp:ListItem Text="경기" Value="경기"></asp:ListItem>
					        <asp:ListItem Text="인천" Value="인천"></asp:ListItem>
					        <asp:ListItem Text="세종" Value="세종"></asp:ListItem>
					        <asp:ListItem Text="제주" Value="제주"></asp:ListItem>
					        <asp:ListItem Text="대전" Value="대전"></asp:ListItem>
					        <asp:ListItem Text="충청" Value="충청"></asp:ListItem>
					        <asp:ListItem Text="강원" Value="강원"></asp:ListItem>
					        <asp:ListItem Text="경상" Value="경상"></asp:ListItem>
					        <asp:ListItem Text="대구" Value="대구"></asp:ListItem>
					        <asp:ListItem Text="울산" Value="울산"></asp:ListItem>
					        <asp:ListItem Text="광주" Value="광주"></asp:ListItem>
					        <asp:ListItem Text="전라" Value="전라"></asp:ListItem>
					    </asp:DropDownList>
					</div>
				</div>
                
				<div class="form-group">
					<label for="s_display" class="col-sm-2 control-label">업종</label>
					<div class="col-sm-3" style="margin-top:5px;">
                        <asp:DropDownList runat="server" id="type" class="form-control" style="width:80px;" >
					        <asp:ListItem Text="업종" Value=""></asp:ListItem>
					        <asp:ListItem Text="외식" Value="외식"></asp:ListItem>
					        <asp:ListItem Text="문화,생활" Value="문화,생활"></asp:ListItem>
					        <asp:ListItem Text="쇼핑" Value="쇼핑"></asp:ListItem>
					        <asp:ListItem Text="여가스포츠" Value="여가스포츠"></asp:ListItem>
					        <asp:ListItem Text="의료" Value="의료"></asp:ListItem>
					        <asp:ListItem Text="숙박" Value="숙박"></asp:ListItem>
					        <asp:ListItem Text="교육" Value="교육"></asp:ListItem>
					        <asp:ListItem Text="온라인" Value="온라인"></asp:ListItem>
					        <asp:ListItem Text="기타" Value="기타"></asp:ListItem>
				         </asp:DropDownList>
					</div>
				</div>
                

				<div class="form-group">
					<label for="s_keyword" class="col-sm-2 control-label">검색</label>
					<div class="col-sm-10">
						<asp:TextBox runat="server" ID="s_keyword" Width="415" cssClass="form-control inline"  OnTextChanged="search" AutoPostBack="true"></asp:TextBox>
						* 검색대상 : 작성자, 샵 이름
					</div>
				</div>
				<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">기간 검색(등록일)</label>
					<div class="col-sm-10">
						<div class="inline">
							<asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							~
						<asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>

						<div class="inline">
							<button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							<div class="btn-group">
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								<a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								<a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							</div>
						</div>

					</div>

				</div>

				<div class="box-footer clearfix text-center">
                    <asp:LinkButton runat="server" ID="btn_search" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
				</div>
				
			</div>
			
		</div>
	</div><!-- /.box -->


    <div class="box box-primary">
		
		 <div class="box-header">
            <h3 class="box-title">총 <asp:Literal runat=server ID=lbTotal/> 건</h3>
        </div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
		
			<table class="table table-hover table-bordered ">
				<colgroup>
				    <col width="5%" />
				    <col width="15%" />
				    <col width="10%" />
				    <col width="*" />
                    <col width="7%" />
				    <col width="15%" />
				    <col width="10%" />
				    <col width="7%" />
				    <col width="7%" />
				</colgroup>
				<thead>
					<tr>
				        <th>번호</th>
				        <th class="text-left">샵 이름</th>
                        <th>대표번호</th>
                        <th>홈페이지</th>
				        <th>작성자</th>
				        <th>작성자 이메일</th>
				        <th>작성자 전화번호</th>
                        <th>등록일</th>
                        <th>노출여부</th>
					</tr>
				</thead>
				<tbody>
								
					<asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
				        <ItemTemplate>
					        <tr onclick="goPage('update.aspx' , { c: <%#Eval("idx")%>, p: '<%#paging.CurrentPage %>' , t: 'update' })" class="tr_link">
						        <td><asp:Literal runat=server ID=lbIdx></asp:Literal></td>
						        <td class="text-left"><div style="max-width:1000px; white-space:pre-line; "><%#Eval("shop_name")%></div></td>
                                <td><%#Eval("shop_tel")%></td>
                                <td><%#Eval("shop_homepage")%></td>
                                <td><%#Eval("writer")%></td>
                                <td><%#Eval("writer_email")%></td>
                                <td><%#Eval("writer_tel")%></td>
						        <td><%#Eval("reg_date" , "{0:yy.MM.dd}")%></td>
						        <td><asp:Literal runat=server ID=lbDisplay></asp:Literal></td>
					        </tr>	
				        </ItemTemplate>
				        <FooterTemplate>
					        <tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
						        <td colspan="9">데이터가 없습니다.</td>
					        </tr>
				        </FooterTemplate>

			        </asp:Repeater>		
				
				</tbody>
			</table>
		</div>
        
		<div class="box-footer clearfix text-center">
            <uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />  
        </div>
        
	</div>

</asp:Content>