﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_advocate_friends_shop_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">

	    $(function () {
            
	        // 썸네일
	        var thumbUploader = attachUploader("shop_img");
	        thumbUploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.image_advocate)%>";
        	thumbUploader._settings.data.rename = "y";
        	thumbUploader._settings.data.limit = 300;

	    });

		var onSubmit = function () {

			if (!validateForm([
				{ id: "#writer", msg: "이름을 입력하세요" },
				{ id: "#writer_tel", msg: "연락처를 입력하세요"},
				{ id: "#writer_email", msg: "이메일을 입력하세요"},
				{ id: "#shop_name", msg: "프렌즈샵명을 입력하세요" },
				{ id: "#shop_tel", msg: "삽 대표번호를 입력하세요" },
				{ id: "#shop_zip", msg: "우편번호를 입력하세요" },
				{ id: "#shop_addr", msg: "주소를 입력하세요" },
				{ id: "#shop_intro", msg: "샵 소개를 입력하세요" }
			])) {
				return false;
			}

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<style type="text/css">
						#b_display label{margin-left:5px;width:50px}
					</style>
					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="del_flag" cssClass="form-control1" Checked="true" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">이름</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=writer CssClass="form-control" Width=200></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">연락처</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=writer_tel CssClass="form-control" Width=200></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">이메일</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=writer_email CssClass="form-control" Width=200></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">프렌즈샵 명</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=shop_name CssClass="form-control" Width=600></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">업종</label>
						<div class="col-sm-10">
                            <asp:RadioButtonList runat="server" ID="shop_type" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Text="외식" Value="외식"></asp:ListItem>
                                <asp:ListItem Text="문화생활" Value="문화생활"></asp:ListItem>
                                <asp:ListItem Text="쇼핑" Value="쇼핑"></asp:ListItem>
                                <asp:ListItem Text="여가스포츠" Value="여가스포츠"></asp:ListItem>
                                <asp:ListItem Text="의료" Value="의료"></asp:ListItem>
                                <asp:ListItem Text="교육" Value="교육"></asp:ListItem>
                                <asp:ListItem Text="숙박" Value="숙박"></asp:ListItem>
                                <asp:ListItem Text="온라인" Value="온라인"></asp:ListItem>
                                <asp:ListItem Text="기타" Value="기타"></asp:ListItem>
                            </asp:RadioButtonList>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">샵 대표번호</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=shop_tel CssClass="form-control" Width=200></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">온라인샵</label>
						<div class="col-sm-10">
							<asp:CheckBox runat="server" ID="is_online" cssClass="form-control1" Checked="true" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">우편번호</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=shop_zip CssClass="form-control" Width=200></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">주소</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=shop_addr CssClass="form-control" Width=600></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">홈페이지</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=shop_homepage CssClass="form-control" Width=600></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">참여가능 영역</label>
						<div class="col-sm-10">
                            <asp:CheckBoxList  runat="server" ID="part" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Text="컴패션 홍보 물 비치" Value="컴패션 홍보 물 비치" ></asp:ListItem>
                                <asp:ListItem Text="후원자 혜택 제공" Value="후원자 혜택 제공" ></asp:ListItem>
                                <asp:ListItem Text="블루보드 설치" Value="블루보드 설치" ></asp:ListItem>
                                <asp:ListItem Text="컴패션 후원자 소규모 행사 장소" Value="컴패션컴패션 후원자 소규모 행사 장소" ></asp:ListItem>
                            </asp:CheckBoxList>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">샵 소개</label>
						<div class="col-sm-10">
                            <asp:TextBox runat="server" ID="shop_intro" TextMode="MultiLine" Rows="10" Width="600" cssClass="form-control"></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">샵 이미지 <br /> (740*494px) </label>
						<div class="col-sm-10">
                            <img id="shop_img" class="shop_img" runat=server src="/@mgt/common/img/empty_thumb.png" style="max-width:400px;" />
						</div>
					</div>

                    <asp:PlaceHolder runat="server" ID="ph_regdate" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="reg_date"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>