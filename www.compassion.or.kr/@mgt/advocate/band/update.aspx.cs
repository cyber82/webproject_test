﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_advocate_friends_shop_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.BoardType = "sponsor_news";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");

		if (base.Action == "update") {

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.BAND.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<BAND>("idx", Convert.ToInt32(PrimaryKey));

                name.Text = entity.name;
                user_id.Text = entity.user_id;
                sponsor.Text = entity.sponsor;
                tel_call_time.Text = entity.tel_call_time;
                up_file2.HRef = GetOldPath(entity.up_file2);
                ucc.HRef = GetOldPath(entity.ucc);
                writeday.Text = ((DateTime)entity.writeday).ToString("yyyy.MM.dd HH:mm");

            }
			//btn_remove.Visible = auth.aa_auth_delete;
			ph_regdate.Visible = true;

		} else {

			//btn_remove.Visible = false;
		}

	}


	protected string GetOldPath(string file) {
		if (file.IndexOf("files/advocate") < 0) {
			file = "/upload/band/" + file;
		}
		return file;
	}


	protected void btn_remove_click(object sender, EventArgs e)
    {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.BAND.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            //dao.BAND.DeleteOnSubmit(entity);
            //dao.SubmitChanges();

            var delStr = string.Format("Delete from BAND where idx = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	
}