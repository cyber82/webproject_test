﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_advocate_friends_shop_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">

	    $(function () {
	    });



		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">이름</label>
						<div class="col-sm-10">
							<asp:Literal runat=server ID=name ></asp:Literal>
						</div>
					</div>
                    

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">아이디</label>
						<div class="col-sm-10">
							<asp:Literal runat=server ID=user_id ></asp:Literal>
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-2 control-label control-label">스폰서ID</label>
						<div class="col-sm-10">
							<asp:Literal runat=server ID=sponsor></asp:Literal>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">연락가능시간</label>
						<div class="col-sm-10">
							<asp:Literal runat=server ID=tel_call_time></asp:Literal>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">지원서</label>
						<div class="col-sm-10" style="margin-top:7px;">
							<a runat="server" id="up_file2" target="_blank">다운로드</a>
						</div>
					</div>

                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">첨부자료</label>
						<div class="col-sm-10" style="margin-top:7px;">
							<a runat="server" id="ucc" target="_blank">다운로드</a>
						</div>
					</div>


                    <asp:PlaceHolder runat="server" ID="ph_regdate" Visible="false">
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-10" style="margin-top:7px;">
                                <asp:Label runat="server" ID="writeday"></asp:Label>
                            </div>
                        </div>
                    </asp:PlaceHolder>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">

		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>