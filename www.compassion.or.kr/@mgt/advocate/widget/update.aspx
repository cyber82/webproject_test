﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_advocate_widget_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>

	<script type="text/javascript" defer="defer">


		$(function () {
            

		    var uploader = attachUploader("thumb");
			uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.image_advocate)%>";
		    uploader._settings.data.rename = "y";
		});

	    var attachUploader = function (button) {
	        return new AjaxUpload(button, {
	            action: '/common/handler/upload',
	            responseType: 'json',
	            onChange: function () {
	                // 이미지 업로드 되면 에디터 초기화 되는 현상 방지
	                // oEditors가 없는 경우 에러남
	                try {
	                    if (oEditors) {
	                        $.each(oEditors, function () {
	                            this.exec("UPDATE_CONTENTS_FIELD", []);
	                        });
	                    }
	                } catch (e) { }
	            },
	            onSubmit: function (file, ext) {
	                this.disable();
	            },
	            onComplete: function (file, response) {

	                this.enable();

	                if (response.success) {

	                    var c = $("#" + button).attr("class").replace(" ", "");
	                    $(".temp_file_type").val("thumb");
	                    $(".temp_file_name").val(response.name);
	                    $(".temp_file_size").val(response.size);

	                    eval($(".btn_update_temp_file").attr("href").replace("javascript:", ""));

	                } else
	                    alert(response.msg);
	            }
	        });
	    }


		var onSubmit = function () {
            return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

		</script>

		

	</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    
    
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>
	
    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="title_section_1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="w_display" Checked="true" />

						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">이름</label>
						<div class="col-sm-4" style="margin-top:7px;">
							<asp:TextBox runat=server ID=w_name CssClass="form-control" style="width:600px;"></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">링크</label>
						<div class="col-sm-4" style="margin-top:7px;">
							<asp:TextBox runat=server ID=w_link CssClass="form-control" style="width:600px;" placeholder="http://"></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">이미지<br />(170X600)</label>
						<div class="col-sm-10">
							<img id="thumb" class="thumb" runat=server src="/@mgt/common/img/empty_thumb.png" style="max-width:400px;" />
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	
	 <div class="box-footer clearfix text-center">
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>

</asp:Content>