﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
public partial class mgt_advocate_widget_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");




		if (base.Action == "update")
        {
            using (AdminDataContext dao = new AdminDataContext())
            {
                // var entity = dao.widget.First(p => p.w_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<widget>("w_id", Convert.ToInt32(PrimaryKey));

                w_display.Checked = entity.w_display;
                w_name.Text = entity.w_name;
                w_link.Text = entity.w_link;

                this.Thumb = entity.w_image;
                ShowFiles();
            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            if (base.Action == "update")
            {
                //var entity = dao.widget.First(p => p.w_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<widget>("w_id", Convert.ToInt32(PrimaryKey));
                entity.w_display = w_display.Checked;
                entity.w_name = w_name.Text;
                entity.w_image = this.Thumb;
                entity.w_link = w_link.Text;
                //dao.SubmitChanges();
                www6.update(entity);

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
                //dao.SubmitChanges();

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
            }
            else
            {

                var entity = new widget();

                entity.w_display = w_display.Checked;
                entity.w_name = w_name.Text;
                entity.w_image = this.Thumb;
                entity.w_link = w_link.Text;
                entity.w_regdate = DateTime.Now;
                entity.w_a_id = AdminLoginSession.GetCookie(this.Context).identifier;

                //dao.widget.InsertOnSubmit(entity);
                www6.insert(entity);
                //dao.SubmitChanges();


                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";


                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", entity.ToJson()));
            }


        }
	
	}

	protected void btn_remove_click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.widget.First(p => p.w_id == Convert.ToInt32(PrimaryKey));
            //dao.widget.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from widget where w_id={0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }
		
		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		base.Thumb = temp_file_name.Value;
		this.ShowFiles();

	}

	void ShowFiles() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			thumb.Src = base.Thumb.WithFileServerHost();
			thumb.Attributes["data-exist"] = "1";
		}
	}
}
