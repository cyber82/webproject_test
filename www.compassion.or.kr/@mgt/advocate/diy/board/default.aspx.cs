﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_advocate_diy_board_default : AdminBoardPage{

    protected override void OnBeforePostBack(){
        base.OnBeforePostBack();

        Master.Content.Controls.BindAllTextBox(this.Context);

        base.LoadComplete += new EventHandler(list_LoadComplete);
        v_admin_auth auth = base.GetPageAuth();
    }

    protected override void OnAfterPostBack(){
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();
    }

    protected override void GetList(int page) {
		Master.IsSearch = false;
		if ( s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
			Master.IsSearch = true;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_diy_board_list(page, paging.RowsPerPage, Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "display", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_diy_board_list", op1, op2).DataTableToList<sp_diy_board_listResult>().ToList();


            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }

    }


    protected void ListBound(object sender, RepeaterItemEventArgs e){

        if (e.Item.ItemType == ListItemType.Footer)
            return;

        sp_diy_board_listResult entity = e.Item.DataItem as sp_diy_board_listResult;
        ((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
        ((Literal)e.Item.FindControl("lbDisplay")).Text = (bool)entity.db_display ? "X" : "O";


    }

}
