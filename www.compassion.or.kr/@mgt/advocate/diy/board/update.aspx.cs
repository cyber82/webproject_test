﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
public partial class mgt_advocate_diy_board_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");


		if (base.Action == "update") {

			using (AdminDataContext dao = new AdminDataContext()) {
                //var entity = dao.diy_board.First(p => p.db_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<diy_board>("db_id", Convert.ToInt32(PrimaryKey));

                db_title.Text = entity.db_title;
				db_content.InnerHtml = entity.db_content;
				db_display.Checked = entity.db_display;
				db_name.Text = entity.db_name;


				this.Thumb = entity.db_thumb;
				ShowFiles();
			}
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;


		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var arg = new diy_board() {
			db_title = db_title.Text,
			db_content = db_content.InnerHtml.ToHtml(),
			db_a_id = AdminLoginSession.GetCookie(this.Context).identifier,
			db_display = db_display.Checked,
			db_regdate = DateTime.Now,
			db_name = db_name.Text,
			db_thumb = Thumb,
			db_hits = 0
		};

		using (AdminDataContext dao = new AdminDataContext()) {
			if (base.Action == "update") {

                //var entity = dao.diy_board.First(p => p.db_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<diy_board>("db_id", Convert.ToInt32(PrimaryKey));

                entity.db_title = arg.db_title;
                entity.db_content = arg.db_content;
                entity.db_a_id = arg.db_a_id;
                entity.db_display = arg.db_display;
                entity.db_regdate = arg.db_regdate;
                entity.db_name = arg.db_name;
                entity.db_hits = arg.db_hits;
                entity.db_thumb = arg.db_thumb;

                www6.update(entity);

                Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "수정되었습니다.";
				//dao.SubmitChanges();

				base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
			} else {
                //dao.diy_board.InsertOnSubmit(arg);
                www6.insert(arg);

                Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "등록되었습니다.";
				//dao.SubmitChanges();

				base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));
			}

		}
	
	}

	protected void btn_remove_click(object sender, EventArgs e) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.diy_board.First(p => p.db_id == Convert.ToInt32(PrimaryKey));
            //dao.diy_board.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            var delStr = string.Format("Delete from diy_board where db_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }
		
		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		base.Thumb = temp_file_name.Value;
		this.ShowFiles();

	}

	void ShowFiles() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			thumb.Src = base.Thumb.WithFileServerHost();
			thumb.Attributes["data-exist"] = "1";
		}
	}
}
