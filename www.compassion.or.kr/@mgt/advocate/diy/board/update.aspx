﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_advocate_diy_board_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>

	<script type="text/javascript" defer="defer">
	    var attachUploader = function (button) {
	        return new AjaxUpload(button, {
	            action: '/common/handler/upload',
	            responseType: 'json',
	            onChange: function () {
	                // 이미지 업로드 되면 에디터 초기화 되는 현상 방지
	                // oEditors가 없는 경우 에러남
	                try {
	                    if (oEditors) {
	                        $.each(oEditors, function () {
	                            this.exec("UPDATE_CONTENTS_FIELD", []);
	                        });
	                    }
	                } catch (e) { }
	            },
	            onSubmit: function (file, ext) {
	                this.disable();
	            },
	            onComplete: function (file, response) {

	                this.enable();

	                if (response.success) {

	                    var c = $("#" + button).attr("class").replace(" ", "");
	                    $(".temp_file_type").val(c.indexOf("thumb") > -1 ? "thumb" : "file");
	                    $(".temp_file_name").val(response.name);
	                    $(".temp_file_size").val(response.size);

	                    eval($(".btn_update_temp_file").attr("href").replace("javascript:", ""));

	                } else
	                    alert(response.msg);
	            }
	        });
	    }

		$(function () {
		    image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_popup)%>";
		    initEditor(oEditors, "db_content");

            
	        // 썸네일
	        var thumbUploader = attachUploader("thumb");
	        thumbUploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.image_sympathy)%>";
	        thumbUploader._settings.data.rename = "y";

		});

	    var isValid = function () {

	        oEditors.getById["db_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.


	        if (!validateForm([
				{ id: "#db_title", msg: "제목을 입력하세요." }
	        ])) {
	            return false;
	        }

	        if ($("#db_content").val() == "") {
	            alert("내용을 입력하세요");
	            return false;
	        }

	        if ($("#thumb").attr("data-exist") != "1") {
	            alert("썸네일 이미지를 등록해주세요.");
	            $("#thumb").focus();
	            return false;
	        }

	        if (!validateForm([
				{ id: "#db_name", msg: "이름을 입력하세요" }
	        ])) {
	            return false;
	        }
	        return true;
	    }


		var onSubmit = function () {

			if (isValid()) {
			    return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
			}
		    return false;

		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}



		

		</script>

		

	</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    
    
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>
	
    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="title_section_1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="db_display" Checked="true" />

						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">제목</label>
						<div class="col-sm-4">
							<asp:TextBox runat=server ID=db_title CssClass=form-control Width=600 MaxLength="50"></asp:TextBox>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10">
							<textarea name="db_content" id="db_content" runat="server" style="width:600px; height:212px; display:none;"></textarea>
						</div>
					</div>
                    
					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">썸네일<br />(636*477px)</label>
						<div class="col-sm-10">
							<img id="thumb" class="thumb" runat=server src="/@mgt/common/img/empty_thumb.png" style="max-width:400px;" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">후원자명</label>
						<div class="col-sm-4">
							<asp:TextBox runat=server ID=db_name CssClass=form-control Width=200></asp:TextBox>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	
	 <div class="box-footer clearfix text-center">
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>

</asp:Content>