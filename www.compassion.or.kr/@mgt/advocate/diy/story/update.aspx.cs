﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_advocate_friends_shop_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context , "default.aspx");


        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.diy_story.First(p => p.ds_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<diy_story>("ds_id", Convert.ToInt32(PrimaryKey));

            ds_name.Text = entity.ds_name;
            ds_phone.Text = entity.ds_phone;
            ds_content.Text = entity.ds_content;
            ds_user.Text = entity.ds_user_id + " / " + entity.ds_user_name;
            ds_regdate.Text = entity.ds_regdate.ToString("yyyy.MM.dd");
        }

		btn_remove.Visible = auth.aa_auth_delete;
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}


	protected void btn_remove_click(object sender, EventArgs e)
    {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.diy_story.First(p => p.ds_id == Convert.ToInt32(PrimaryKey));
            //dao.diy_story.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            var delStr = string.Format("Delete from diy_board where db_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

}