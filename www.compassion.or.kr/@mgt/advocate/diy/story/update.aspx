﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_advocate_friends_shop_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">

	    $(function () {
	    });

		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">후원자명</label>
						<div class="col-sm-10" style="margin-top:7px;">
							<asp:Literal runat=server ID=ds_name></asp:Literal>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">연락처</label>
						<div class="col-sm-10" style="margin-top:7px;">
							<asp:Literal runat=server ID=ds_phone></asp:Literal>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10" style="margin-top:7px;">
                            <asp:Literal runat="server" ID="ds_content"></asp:Literal>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">회원정보</label>
						<div class="col-sm-10" style="margin-top:7px;">
                            <asp:Literal runat="server" ID="ds_user"></asp:Literal>
						</div>
					</div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">등록일</label>
						<div class="col-sm-10" style="margin-top:7px;">
                            <asp:Label runat="server" ID="ds_regdate"></asp:Label>
                        </div>
                    </div>

					
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>