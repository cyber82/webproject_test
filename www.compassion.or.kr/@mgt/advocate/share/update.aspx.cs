﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
public partial class mgt_advocate_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");



        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.user_board.First(p => p.ub_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<user_board>("ub_id", Convert.ToInt32(PrimaryKey));

            ub_display.Checked = entity.ub_display;
            ub_top.Checked = entity.ub_top;
            ub_title.Text = entity.ub_title;
            ub_content.Text = entity.ub_content;
            ub_user_info.Text = entity.ub_user_name + " / " + entity.ub_user_id;
            if (StaticData.Code.GetList(Context, true).Any(p => p.cd_group == "board_sub" && p.cd_key == entity.ub_category))
            {
                ub_category_name.Text = StaticData.Code.GetList(Context, true).First(p => p.cd_group == "board_sub" && p.cd_key == entity.ub_category).cd_value;
            }


            this.Thumb = entity.ub_thumb;
            ShowFiles();
        }
		btn_update.Visible = auth.aa_auth_update;
		btn_remove.Visible = auth.aa_auth_delete;
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.user_board.First(p => p.ub_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<user_board>("ub_id", Convert.ToInt32(PrimaryKey));
            entity.ub_display = ub_display.Checked;
            entity.ub_top = ub_top.Checked;

            www6.update(entity);

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "수정되었습니다.";
            //dao.SubmitChanges();

            base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

        }
	
	}

	protected void btn_remove_click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.user_board.First(p => p.ub_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<user_board>("ub_id", Convert.ToInt32(PrimaryKey));
            entity.ub_deleted = true;
            //dao.SubmitChanges();

            www6.update(entity);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", entity.ToJson()));
        }
		
		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		base.Thumb = temp_file_name.Value;
		this.ShowFiles();

	}

	void ShowFiles() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			thumb.Src = base.Thumb.WithFileServerHost();
			thumb.Attributes["data-exist"] = "1";
		}
	}
}
