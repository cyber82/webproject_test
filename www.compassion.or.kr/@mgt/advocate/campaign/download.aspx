﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="download.aspx.cs" Inherits="mgt_advocate_campaign_download"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head runat="server">
    <title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
		html{background:#fff}
		table { border-collapse:initial; }  
		th, td { border:1px solid #000000;}
		td { mso-number-format:\@; }
	</style>
</head>
<body>
    <form id="form" runat="server">
		<table class="listBoard">
			<tbody>	
				<tr>
					<td style="width:50px">번호</td>
					<td style="width:150px">이름</td>
					<td style="width:200px">회원정보</td>
					<td style="width:150px">전화번호</td>
				    <th style="width:150px">우편번호</th>
				    <th style="width:300px">주소</th>
                    <th style="width:300px">활동계획</th>
                    <th style="width:150px">등록일</th>

					
				</tr>		
				<asp:Repeater runat=server ID=repeater >
					<ItemTemplate>
						<tr>
							<td><%#Container.ItemIndex + 1 %></td>
						    <td><%#Eval("sca_name")%></td>
                            <td><%#Eval("sca_user_id") %> / <%#Eval("sca_user_name") %></td>
                            <td><%#Eval("sca_phone") %></td>
                            <td><%#Eval("sca_zipcode") %></td>
                            <td><%#Eval("sca_addr1") %> <%#Eval("sca_addr2") %> </td>
                            <td><%#Eval("sca_plan") %></td>
                            <td><%#Eval("sca_regdate" , "{0:yy.MM.dd}")%></td>
						</tr>	
					</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								<td colspan="8">데이터가 없습니다.</td>
							</tr>
					</FooterTemplate>
				</asp:Repeater>	
				
			</tbody>
		</table>
    </form>
</body>
</html>
