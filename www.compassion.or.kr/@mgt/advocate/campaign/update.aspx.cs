﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
using System.Web.UI.HtmlControls;
public partial class mgt_advocate_campaign_board_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context , "default.aspx");


		if (base.Action == "update") {

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.seasonal_campaign.First(p => p.sc_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<seasonal_campaign>("sca_id", Convert.ToInt32(PrimaryKey));

                if(entity != null)
                {
                    sc_display.Checked = entity.sc_display;
                    sc_title.Text = entity.sc_title;
                    sc_content.InnerHtml = entity.sc_content;
                    sc_begin.Text = entity.sc_begin.ToString("yyyy-MM-dd");
                    sc_end.Text = entity.sc_end.ToString("yyyy-MM-dd");
                    sc_offer.Text = entity.sc_offer;
                    sc_closed.Checked = entity.sc_closed;
                    sc_hits.Text = entity.sc_hits.ToString();
                    sc_regdate.Text = entity.sc_regdate.ToString("yyyy-MM-dd");

                    if (entity.sc_announce != null)
                        sc_announce.Text = ((DateTime)entity.sc_announce).ToString("yyyy-MM-dd");

                    phUpdate.Visible = true;

                    this.Thumb = entity.sc_thumb;
                    ShowFiles();
                    
                    tabm2.Visible = true;
                    btn_apply_download.Attributes["data-id"] = PrimaryKey;
                    btn_apply_download.Attributes["data-title"] = entity.sc_title;
                }
            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;


		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var arg = new seasonal_campaign() {
			sc_title = sc_title.Text,
			sc_content = sc_content.InnerHtml.ToHtml(),
			sc_begin = DateTime.Parse(sc_begin.Text),
			sc_end = DateTime.Parse(sc_end.Text + " 23:59:59"),
			//sc_announce = DateTime.Parse(sc_announce.Text),
			sc_offer = sc_offer.Text,
			sc_closed = sc_closed.Checked,
			sc_a_id = AdminLoginSession.GetCookie(this.Context).identifier,
			sc_display = sc_display.Checked,
			sc_regdate = DateTime.Now,
			sc_thumb = Thumb,
			sc_hits = 0
		};

		if (sc_announce.Text.EmptyIfNull() == "") {
			arg.sc_announce = null;
		} else {
			arg.sc_announce = DateTime.Parse(sc_announce.Text);
		}

        using (AdminDataContext dao = new AdminDataContext())
        {
            if (base.Action == "update")
            {

                //var entity = dao.seasonal_campaign.First(p => p.sc_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<seasonal_campaign>("sc_id", Convert.ToInt32(PrimaryKey));
                entity.sc_title = arg.sc_title;
                entity.sc_content = arg.sc_content;
                entity.sc_begin = arg.sc_begin;
                entity.sc_end = arg.sc_end;
                entity.sc_announce = arg.sc_announce;
                entity.sc_offer = arg.sc_offer;
                entity.sc_closed = arg.sc_closed;
                entity.sc_a_id = arg.sc_a_id;
                entity.sc_display = arg.sc_display;
                entity.sc_regdate = arg.sc_regdate;
                entity.sc_hits = arg.sc_hits;
                entity.sc_thumb = arg.sc_thumb;
                
                www6.update(entity);

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
                //dao.SubmitChanges();
                

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));
            }
            else
            {
                //dao.seasonal_campaign.InsertOnSubmit(arg);
                www6.insert(arg);

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";
                //dao.SubmitChanges();

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));
            }

        }
	
	}

	protected void btn_remove_click(object sender, EventArgs e) {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.seasonal_campaign.First(p => p.sc_id == Convert.ToInt32(PrimaryKey));
            //dao.seasonal_campaign.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            var delStr = string.Format("Delete from seasonal_campaign_apply where sac_id ={0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }
		
		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		base.Thumb = temp_file_name.Value;
		this.ShowFiles();

	}

	void ShowFiles() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			thumb.Src = base.Thumb.WithFileServerHost();
			thumb.Attributes["data-exist"] = "1";
		}
	}

	protected void btn_section_Click(object sender, EventArgs e) {
		LinkButton obj = sender as LinkButton;
		var no = obj.CommandArgument;
		int tabCount = 2;
		int current = 0;

		for (int i = 1; i < tabCount + 1; i++) {
			if (no == i.ToString()) {
				current = i;
				((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "active";
				((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "active tab-pane";
			} else {
				((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "";
				((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "tab-pane";
			}


		}

		if (current == 2) {
			GetApplyList(1);
		}
	}


	protected void paging_apply_Navigate(int page, string hash) {
		this.GetApplyList(page);
	}

	protected void GetApplyList(int page) {


        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_seasonal_campaign_apply_list(page, paging_apply.RowsPerPage, Convert.ToInt32(PrimaryKey), "", "", "").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "sc_id", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging_apply.RowsPerPage, Convert.ToInt32(PrimaryKey), "", "", "" };
            var list = www6.selectSP("sp_seasonal_campaign_apply_list", op1, op2).DataTableToList<sp_seasonal_campaign_apply_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging_apply.CurrentPage = page;
            paging_apply.Calculate(total);
            repeater_apply.DataSource = list;
            repeater_apply.DataBind();

        }
	}


	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (e.Item.ItemType == ListItemType.Footer)
			return;

		sp_seasonal_campaign_listResult entity = e.Item.DataItem as sp_seasonal_campaign_listResult;
		((Literal)e.Item.FindControl("lbIdx")).Text = (paging_apply.TotalRecords - ((paging_apply.CurrentPage - 1) * paging_apply.RowsPerPage) - e.Item.ItemIndex).ToString();
	}
}
