﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_advocate_campaign_board_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>

	<script type="text/javascript" defer="defer">


		$(function () {
		    image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_advocate)%>";
		    initEditor(oEditors, "sc_content");

            
	        // 썸네일
	        var thumbUploader = attachUploader("thumb");
	        thumbUploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.image_sympathy)%>";
		    thumbUploader._settings.data.rename = "y";


		    // 날짜 선택
		    $("#sc_begin , #sc_end , #sc_announce").focus(function () {
		        var className = $(this).data("range");

		        $("." + className).trigger("click");
		        $("." + className).focus();
		        return false;
		    })

            //다운로드
		    $(".btn_apply_download").click(function () {

		        var id = $(this).attr("data-id");
		        var title = $(this).attr("data-title");

		        location.href = "/@mgt/advocate/campaign/download?c=" + id + "&title=" + escape(title);
		        return false;
		    })
		});

	    var isValid = function () {

	        oEditors.getById["sc_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.


	        if (!validateForm([
				{ id: "#sc_title", msg: "제목을 입력하세요." }
	        ])) {
	            return false;
	        }

	        if ($("#sc_content").val() == "") {
	            alert("내용을 입력하세요");
	            return false;
	        }

	        if ($("#thumb").attr("data-exist") != "1") {
	            alert("썸네일 이미지를 등록해주세요.");
	            $("#thumb").focus();
	            return false;
	        }

	        if (!validateForm([
				{ id: "#sc_begin", msg: "참여기간을 입력하세요" },
                { id: "#sc_end", msg: "참여기간을 입력하세요" },
                //{ id: "#sc_announce", msg: "당첨자 발표일을 입력하세요" },
                { id: "#sc_offer", msg: "제공 아이탬을 입력하세요" }
	        ])) {
	            return false;
	        }
	        return true;
	    }


		var onSubmit = function () {

			if (isValid()) {
			    return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
			}
		    return false;

		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

		</script>

		

	</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    
    
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>
	
    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="title_section_1" CommandArgument="1" OnClick="btn_section_Click">기본정보</asp:LinkButton></li>
            <li runat="server" id="tabm2" class="" visible="false"><asp:LinkButton runat="server" id="title_section_2" CommandArgument="2" OnClick="btn_section_Click">신청</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="sc_display" Checked="true" />

						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">마감여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="sc_closed" />

						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">제목</label>
						<div class="col-sm-4">
							<asp:TextBox runat=server ID=sc_title CssClass=form-control Width=600 MaxLength="50"></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10">
							<textarea name="sc_content" id="sc_content" runat="server" style="width:600px; height:212px; display:none;"></textarea>
						</div>
					</div>
                    
					<div class="form-group" >
						<label class="col-sm-2 control-label control-label">썸네일<br />(410 * 170px)</label>
						<div class="col-sm-10">
							<img id="thumb" class="thumb" runat=server src="/@mgt/common/img/empty_thumb.png" style="max-width:400px;" />
						</div>
					</div>

                    <div class="form-group">
						<label class="col-sm-2 control-label control-label">참여 기간</label>
						<div class="col-sm-10">
							<button class="btn btn-primary btn-sm dayrange dayrange1 " data-from="sc_begin" data-end="sc_end"><i class="fa fa-calendar"></i></button>
							<asp:TextBox runat="server" ID="sc_begin" Width="100px"  data-range="dayrange1" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
								~
							<asp:TextBox runat="server" ID="sc_end" Width="100px"  data-range="dayrange1" CssClass="form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group announce_form">
						<label class="col-sm-2 control-label control-label">당첨자 발표일<br />(선택사항)</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=sc_announce CssClass="date form-control" style="float:left;" Width=100></asp:TextBox>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">제공 아이템</label>
						<div class="col-sm-4">
							<asp:TextBox runat=server ID=sc_offer CssClass=form-control Width=200></asp:TextBox>
						</div>
					</div>
                    

                    <asp:PlaceHolder runat="server" ID="phUpdate" Visible="false">

					    <div class="form-group">
						    <label class="col-sm-2 control-label control-label">조회수</label>
						    <div class="col-sm-4" style="margin-top:7px;">
							    <asp:Literal runat=server ID=sc_hits></asp:Literal>
						    </div>
					    </div>

					    <div class="form-group">
						    <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-4" style="margin-top:7px;">
							    <asp:Literal runat=server ID=sc_regdate></asp:Literal>
						    </div>
					    </div>
                    </asp:PlaceHolder>

				</div>
			</div>


            

            <div class="tab-pane" id="tab2" runat="server" >


				<div class="box-header">
					<h3 class="box-title">총 <asp:Literal runat="server" ID="lbTotal" /> 건</h3>
					<span class="pull-right">
						<uc:paging runat="server" ID="paging_apply" OnNavigate="paging_apply_Navigate" EnableViewState="true" RowsPerPage="20" PagesPerGroup="10" Hash="p2" />
						<a href="#" class="btn btn-bitbucket pull-right btn_apply_download" runat="server" id="btn_apply_download" style="margin-left:5px">다운로드</a>
					</span>
				</div>
				<!-- /.box-header -->

				<div class="box-body table-responsive no-padding">
					
					<table class="table table-hover table-bordered ">
				        <colgroup>
				            <col width="5%" />
				            <col width="*" />
				            <col width="10%" />
				            <col width="10%" />
				            <col width="10%" />
				            <col width="10%" />
				        </colgroup>
				        <thead>
					        <tr>
				                <th>번호</th>
				                <th class="text-left">이름</th>
                                <th>회원정보</th>
                                <th>전화번호</th>
                                <th>우편번호</th>
                                <th>등록일</th>
					        </tr>
				        </thead>
				        <tbody>
								
					        <asp:Repeater runat=server ID=repeater_apply OnItemDataBound=ListBound>
				                <ItemTemplate>
					                <tr onclick="goPage('update.aspx' , { c: <%#Eval("sca_id")%>, p: '<%#paging_apply.CurrentPage %>' , t: 'update' })" class="tr_link">
						                <td><asp:Literal runat=server ID=lbIdx></asp:Literal></td>
						                <td class="text-left"><div style="max-width:1000px; white-space:pre-line; "><%#Eval("sca_name")%></div></td>
                                        <td><%#Eval("sca_user_id") %> / <%#Eval("sca_user_name") %></td>
                                        <td><%#Eval("sca_phone") %></td>
                                        <td><%#Eval("sca_zipcode") %></td>
                                        <td><%#Eval("sca_regdate" , "{0:yy.MM.dd}")%></td>
					                </tr>	
				                </ItemTemplate>
				                <FooterTemplate>
					                <tr runat="server" Visible="<%#repeater_apply.Items.Count == 0 %>">
						                <td colspan="6">데이터가 없습니다.</td>
					                </tr>
				                </FooterTemplate>

			                </asp:Repeater>		
				
				        </tbody>
			        </table>
				</div>

			</div>

		</div>
	</div>

	
	 <div class="box-footer clearfix text-center">
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>

</asp:Content>