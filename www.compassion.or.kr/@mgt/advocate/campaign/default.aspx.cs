﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_advocate_campaign_board_default : AdminBoardPage{

    protected override void OnBeforePostBack(){
        base.OnBeforePostBack();

        Master.Content.Controls.BindAllTextBox(this.Context);

        base.LoadComplete += new EventHandler(list_LoadComplete);
        v_admin_auth auth = base.GetPageAuth();
    }

    protected override void OnAfterPostBack(){
        Master.Content.Controls.EscapeAllTextBox();
        base.OnAfterPostBack();
    }

    protected override void GetList(int page) {
		Master.IsSearch = false;
		if ( s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "") {
			Master.IsSearch = true;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_seasonal_campaign_list(page, paging.RowsPerPage, Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "display", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, Convert.ToInt32(s_display.SelectedValue), s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSP("sp_seasonal_campaign_list", op1, op2).DataTableToList<sp_seasonal_campaign_listResult>().ToList();

            var total = 0;
            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }

    }


    protected void ListBound(object sender, RepeaterItemEventArgs e){

        if (e.Item.ItemType == ListItemType.Footer)
            return;

		sp_seasonal_campaign_listResult entity = e.Item.DataItem as sp_seasonal_campaign_listResult;
        ((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
		//((Literal)e.Item.FindControl("lbDisplay")).Text = entity.sc_display ? "X" : "O";


		string status = "";
		if (!entity.sc_display) {
			status = "미노출(설정됨)";
		} else if (entity.sc_begin.CompareTo(DateTime.Now) > 0) {
			status = string.Format("노출전 ({0})", entity.sc_begin.TimeIn());
		} else if (entity.sc_end.CompareTo(DateTime.Now) < 0) {
			status = "미노출 (기간종료)";
		} else {
			status = "노출중";
		}
		((Literal)e.Item.FindControl("lbDisplay")).Text = status;


	}

}
