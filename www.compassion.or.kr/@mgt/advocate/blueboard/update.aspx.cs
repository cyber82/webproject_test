﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_advocate_blueboard_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context , "default.aspx");


        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.blue_board_apply.First(p => p.bba_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<blue_board_apply>("bba_id", Convert.ToInt32(PrimaryKey));

            bba_name.Text = entity.bba_name;
            bba_phone.Text = entity.bba_phone;
            bba_zipcode.Text = entity.bba_zipcode;
            bba_addr1.Text = entity.bba_addr1;
            bba_addr2.Text = entity.bba_addr2;
            bba_plan.Text = entity.bba_plan;
            bba_user.Text = entity.bba_user_id + " / " + entity.bba_user_name;
            bba_regdate.Text = entity.bba_regdate.ToString("yyyy.MM.dd");

        }

		btn_remove.Visible = auth.aa_auth_delete;
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}


	protected void btn_remove_click(object sender, EventArgs e) {

		using (AdminDataContext dao = new AdminDataContext()) {
            //var entity = dao.blue_board_apply.First(p => p.bba_id == Convert.ToInt32(PrimaryKey));
            //dao.blue_board_apply.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            var delStr = string.Format("Delete from blue_board_apply where bba_id ={0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
		}

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

}