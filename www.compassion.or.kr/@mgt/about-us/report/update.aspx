﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_about_us_report_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false" %>

<%@ MasterType VirtualPath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        #b_type label {
            padding-right: 10px;
        }
    </style>
    <script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
    <script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
    <script type="text/javascript">

        $(function () {
            // 에디터
            image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
            initEditor(oEditors, "b_content");

            // 썸네일
            var thumbUploader = attachUploader("b_image");
            thumbUploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
	        thumbUploader._settings.data.rename = "y";

            // pdf
	        var uploader = attachUploader("btn_upload");
	        uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_board)%>";
		    uploader._settings.data.rename = "y";
		    uploader._settings.data.limit = 10240; // 10메가


		    setTimeout(function () {
		        $("[name=userfile]").attr("accept", ".pdf")
		    }, 300)
        });




        var attachUploader = function (button) {
            return new AjaxUpload(button, {
                action: '/common/handler/upload',
                responseType: 'json',
                onChange: function () {
                    // 이미지 업로드 되면 에디터 초기화 되는 현상 방지
                    // oEditors가 없는 경우 에러남
                    try {
                        if (oEditors) {
                            $.each(oEditors, function () {
                                this.exec("UPDATE_CONTENTS_FIELD", []);
                            });
                        }
                    } catch (e) { }
                },
                onSubmit: function (file, ext) {
                    this.disable();
                },
                onComplete: function (file, response) {
                    this.enable();
                    if (response.success) {
                        var c = $("#" + button).attr("class").replace(" ", "");
                        $(".temp_file_type").val(c.indexOf("image") > -1 ? "thumb" : "file");
                        $(".temp_file_name").val(response.name);
                        $(".temp_file_size").val(response.size);
                        eval($(".btn_update_temp_file").attr("href").replace("javascript:", ""));
                    } else
                        alert(response.msg);
                }
            });
        }




        var onSubmit = function () {

            if (!validateForm([
				{ id: "#b_title", msg: "제목을 입력하세요" },
				{ id: "#b_summary", msg: "목차를 입력하세요" },
                { id: "#b_meta_keyword", msg: "년도를 입력하세요" }
            ])) {
                return false;
            }
            oEditors.getById["b_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.


            if ($("#b_image").attr("data-exist") != "1") {
                alert("썸네일을 등록하세요");
                $("#b_image").focus();
                return false;
            }

            if ($("#lnFileName").length < 1) {
                alert("pdf파일을 등록하세요");
                $("#btn_upload").focus();
                return false;
            }

            return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
        }


        var onRemove = function () {
            return confirm("삭제하시겠습니까?");
        }
    </script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

    <input type="hidden" runat="server" id="temp_file_type" class="temp_file_type" value="" />
    <input type="hidden" runat="server" id="temp_file_name" class="temp_file_name" value="" />
    <input type="hidden" runat="server" id="temp_file_size" class="temp_file_size" value="" />
    <asp:LinkButton runat="server" ID="btn_update_temp_file" CssClass="btn_update_temp_file" OnClick="btn_update_temp_file_click"></asp:LinkButton>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">

            <li runat="server" id="tabm1" class="active">
                <asp:LinkButton runat="server" ID="LinkButton1" CommandArgument="1">기본정보</asp:LinkButton></li>

        </ul>
        <div class="tab-content">

            <div class="active tab-pane" id="tab1" runat="server">
                <div class="form-horizontal">

                    <style type="text/css">
                        #b_display label {
                            margin-left: 5px;
                            width: 50px;
                        }
                    </style>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">노출여부</label>
                        <div class="col-sm-10" style="margin-top: 5px;">
                            <asp:CheckBox runat="server" ID="b_display" CssClass="form-control1" Checked="true" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">제목</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="b_title" CssClass="form-control b_title" Width="600" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">내용</label>
                        <div class="col-sm-10">
                            <textarea name="b_content" id="b_content" runat="server" style="width: 600px; height: 212px; display: none;"></textarea>
                            <pre style="width: 600px;">srook에서 코드복사를 하는 경우 컨텐츠 하단의 여백 조절을 위해<br />HTML모드에서 코드수정이 필요합니다.<br />수정 전 : &lt;div style="width:100%;<b style="color:red;">height:600px</b>;text-align:center;"&gt; <br />수정 후 : &lt;div style="width:100%;<b style="color:red;">height:20px</b>;text-align:center;"&gt;</pre>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">목차</label>
                        <div class="col-sm-10">
                            <textarea name="b_summary" id="b_summary" runat="server" style="width: 600px; height: 100px;"></textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">년도</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="b_meta_keyword" CssClass="form-control b_title number_only" Width="200"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">썸네일<br />(232 * 233px)</label>
                        <div class="col-sm-10">
                            <img id="b_image" class="b_image" runat="server" src="/@mgt/common/img/empty_thumb.png" style="max-width: 400px;" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label control-label">PDF</label>
                        <div class="col-sm-10">
                            <button type="button" class="btn btn-default btn-xs" id="btn_upload" style="margin-top: 5px; margin-bottom: 2px;">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>파일선택
                            </button>
                            <br />

                            <asp:Repeater runat="server" ID="repeater" OnItemDataBound="FileListBound">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="btn_remove_file" CssClass="btn btn-default btn-xs" Style="margin-bottom: 2px" CommandArgument='<%#Eval("f_name")%>' OnClick="btn_remove_file_click"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 삭제</asp:LinkButton>
                                    <asp:HyperLink runat="server" ID="lnFileName">
                                        <asp:Literal runat="server" ID="fileName"></asp:Literal>
                                    </asp:HyperLink>
                                    |
                                    <asp:Literal runat="server" ID="fileSize"></asp:Literal>
                                    |
                                    <asp:Literal runat="server" ID="regdate"></asp:Literal>

                                    <br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            PDF 파일은 최대 10MB를 넘을 수 없습니다.
                        </div>
                    </div>
                </div>
            </div>


            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->


    <div class="box-footer clearfix text-center">

        <asp:LinkButton runat="server" ID="btn_remove" OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
        <asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" Style="margin-right: 5px">등록</asp:LinkButton>
        <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
    </div>
</asp:Content>
