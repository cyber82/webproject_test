﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_about_us_report_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		base.BoardType = "newsletter";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);

		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");

		if (base.Action == "update") {
			using (AdminDataContext dao = new AdminDataContext()) {
                //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));

                b_title.Text = entity.b_title;
                b_sub_type.Text = entity.b_sub_type;
                b_content.InnerHtml = entity.b_content;
				b_display.Checked = entity.b_display;

                // thumb파일 가져오기
                var exist = www6.selectQ<file>("f_group", Uploader.FileGroup.image_board.ToString(), "f_ref_id", PrimaryKey.ToString());

                //if (dao.file.Any(p => p.f_group == Uploader.FileGroup.image_board.ToString() && p.f_ref_id == PrimaryKey.ToString())) 
                if (exist.Any())
                {
                    //var f = dao.file.First(p => p.f_group == Uploader.FileGroup.image_board.ToString() && p.f_ref_id == PrimaryKey.ToString());
                    var f = exist[0];
                    Thumb = f.f_name;
				}

			}
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			
			ShowFiles();

		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}




	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {


		var arg = new board() {
			b_title = b_title.Text,
            b_sub_type = b_sub_type.Text,
            b_content = b_content.InnerHtml.ToHtml(), 
			b_a_id = AdminLoginSession.GetCookie(this.Context).identifier,
			b_display = b_display.Checked,
			b_regdate = DateTime.Now
		};



        using (AdminDataContext dao = new AdminDataContext())
        {
            if (Action == "update")
            {
                //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));
                entity.b_title = arg.b_title;
                entity.b_sub_type = arg.b_sub_type;
                entity.b_content = arg.b_content;
                entity.b_a_id = arg.b_a_id;
                entity.b_display = arg.b_display;
                entity.b_hot = arg.b_hot;
                entity.b_main = arg.b_main;
                entity.b_regdate = arg.b_regdate;
                entity.b_meta_keyword = arg.b_meta_keyword;
                entity.b_summary = arg.b_summary;
                //dao.SubmitChanges();
                www6.update(entity);
                

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
            }
            else
            {
                arg.b_hits = 0;
                arg.b_type = this.BoardType;

                //dao.board.InsertOnSubmit(arg);
                www6.insert(arg);
                //dao.SubmitChanges();


                PrimaryKey = arg.b_id.ToString();
                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";
            }


            // 썸네일
            //dao.file.DeleteAllOnSubmit(dao.file.Where(p => p.f_ref_id == PrimaryKey && p.f_group == Uploader.FileGroup.image_board.ToString()));
            //www6.cud(MakeSQL.delQ(0, "file", "f_ref_id", PrimaryKey, "f_group", Uploader.FileGroup.image_board.ToString()));
            var fiList = www6.selectQ<file>("f_ref_id", PrimaryKey, "f_group", Uploader.FileGroup.image_board.ToString());
            www6.delete(fiList);

            if (Thumb != "")
            {
                var thumb = new file()
                {
                    f_group = Uploader.FileGroup.image_board.ToString(),
                    f_group2 = "",
                    f_ref_id = PrimaryKey,
                    f_name = Thumb,
                    f_size = 0,
                    f_regdate = DateTime.Now

                };
                
                //dao.file.InsertOnSubmit(thumb);
                www6.insert(thumb);
            }
            //dao.SubmitChanges();
        }


	}


	// file
	void ShowFiles() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			b_image.Src = base.Thumb.WithFileServerHost();
			b_image.Attributes["data-exist"] = "1";
		}
	}

	protected void btn_remove_file_click(object sender, EventArgs e) {
		base.FileRemove(sender);
		this.ShowFiles();
	}



	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		base.Thumb = temp_file_name.Value;
		this.ShowFiles();

	}

	protected void btn_remove_click(object sender, EventArgs e) {

		using (AdminDataContext dao = new AdminDataContext()) {
            //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));
            www6.delete(entity);
            //www6.cud(MakeSQL.delQ(0, "board", "b_id", Convert.ToInt32(PrimaryKey)));
            //dao.board.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format(@"delete from board where b_id = {0}", Convert.ToInt32(PrimaryKey));
            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
		}

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}
}