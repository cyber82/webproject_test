﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_about_us_subscription_download : AdminBoardPage{

    protected override void OnBeforePostBack(){

		var keyword = Request["k"].EmptyIfNull();
		var b_date = Request["b"].EmptyIfNull();
		var e_date = Request["e"].EmptyIfNull();
		base.OnBeforePostBack();

		this.GetList(keyword, b_date, e_date);


		// 익스에서 한글, 띄어쓰기 깨짐
		HttpBrowserCapabilities brObject = Request.Browser;
		var browser = brObject.Type.ToLower();
		var fileName = "뉴스레터_구독자_"+ DateTime.Now.ToString("yyyyMMdd")+ ".xls";
		if (browser.Contains("ie")) {
			fileName = System.Web.HttpUtility.UrlEncode(fileName);
		}


		Response.Clear();
		Response.Charset = "utf-8";
		Response.ContentType = "application/vnd.ms-excel";
		Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
	}


	protected void GetList(string keyword, string b_date, string e_date) {

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_subscription_list(1, 60000, keyword, b_date, e_date).ToList();
            Object[] op1 = new Object[] {   "page"
                                           , "rowsPerPage"
                                           , "keyword"
                                           , "startdate"
                                           , "enddate"};
            Object[] op2 = new Object[] {   1
                                          , 60000
                                          , keyword
                                          , b_date
                                          , e_date };
            try
            {
                var list = www6.selectSP("sp_subscription_list", op1, op2).DataTableToList<sp_subscription_listResult>().ToList();

                repeater.DataSource = list;
                repeater.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, "보드 정보 가져오기 에러" + ex.Message);
            }
            
            base.WriteLog(AdminLog.Type.select, string.Format("뉴스레터 구독자 다운로드 ({0}, {1}, {2})", keyword, b_date, e_date));
        }
	}

}
