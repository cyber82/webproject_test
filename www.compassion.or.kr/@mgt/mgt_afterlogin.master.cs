﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;


public partial class mgt_afterlogin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
		/*
		if (Request["mode"].EmptyIfNull() == "pop") {
			contents.Style["padding"] = "0";
			contents.Style["margin"] = "0";
			append_css.Attributes["href"] = "/admin/common/css/pop.css";
			leftMenu.Visible = ph_history.Visible = ph_header.Visible = false;
			
		}
		*/

		url.Value = Request.RawUrl;

		if (!AdminLoginSession.HasCookie(this.Context)) 
			return;

		AdminEntity admin = AdminLoginSession.GetCookie(this.Context);

		userId.Text = admin.name;
		adminName.Text = admin.name;
		adminDept.Text = admin.dept;
		adminEmail.Text = admin.email;
		adminPhone.Text = admin.rawData.a_phone_office;

		adminType.Text = "일반관리자";
        if (admin.rawData.a_type == "super") {
			adminType.Text = "슈퍼관리자";
        }
		

		List<CommonLib.admin_menu> top_menus = null;

		var depth1 = (from p in admin.auth_menus
					  group p by new {
						  p.am_group
					  } into g
					  select g.Key.am_group).ToList();

		var depth2 = (from p in admin.auth_menus
					  where p.am_depth == 2 && p.am_display == true
					  group p by new {
						  p.am_group
					  } into g
					select g.Key.am_group).ToList();

		depth2 = (from p in admin.menus
				  where depth2.Contains(p.am_code)
				  select p.am_group).ToList();
		
		depth1 = depth1.Concat(depth2).ToList();
		top_menus = (from p in admin.menus
				  where depth1.Contains(p.am_code) && p.am_display == true && p.am_depth == 0
				  orderby p.am_group_order ascending
				  select p).ToList();

		rpt_menu.DataSource = top_menus;
		rpt_menu.DataBind();


		var cur_menu_group = this.GetCurrentMenuGroup();
		if (cur_menu_group != null) {

			var cur_depth1 = admin.menus.First(p => p.am_group_order == cur_menu_group.am_group_order && p.am_depth == 0);
            historyDepth1.Text = cur_depth1.am_name;

			pageTitle.Text = cur_menu_group.am_name;

		}
    }

	public ContentPlaceHolder Content {
		get {
			return this.content;
		}
	}

	public HtmlInputHidden ValueAction {
		get {
			return this.action;
		}
	}

	public Literal PageTitle {
		get {
			return this.pageTitle;
		}
	}

	public Literal SubPageTitle {
		get {
			return this.subPageTitle;
		}
	}
	

	public HtmlInputHidden ValueMessage {
		get {
			return this.msg;
		}
	}


	public HtmlInputHidden ValueUrl {
		get {
			return this.url;
		}
	}

	public bool IsSearch {
		set {
			is_search.Value = (value ? "1" : "0");
        }
	}

	public PlaceHolder LeftMenu {
		get {
			return this.leftMenu;
		}
	}



	CommonLib.admin_menu GetCurrentMenuGroup() {

		if (AdminLoginSession.GetCookie(this.Context).menus.Any(p => p.am_path == Request.OnlyPath() && p.am_display == true)) {
			return AdminLoginSession.GetCookie(this.Context).menus.First(p => p.am_path == Request.OnlyPath() && p.am_display == true);
		}
		
		return null;
	}

	protected void rpt_menu_ItemDataBound(object sender, RepeaterItemEventArgs e) {

		CommonLib.admin_menu entity = e.Item.DataItem as CommonLib.admin_menu;
		AdminEntity admin = AdminLoginSession.GetCookie(this.Context);

		Repeater sub_menu = (Repeater)e.Item.FindControl("rpt_submenu");
		HtmlControl li = (HtmlControl)e.Item.FindControl("li");
		
		var cur_menu = this.GetCurrentMenuGroup();
		
		try {
			
			if (cur_menu.am_group == entity.am_group) {
				li.Attributes["class"] = "active";
				
			}else if (cur_menu.am_depth == 2) {
				var admin_group = admin.menus.First(p => p.am_depth == 1 && p.am_code == cur_menu.am_group && p.am_display == true).am_group;
				if (entity.am_group == admin_group) {
					li.Attributes["class"] = "active";
					
				}
			}
		} catch(Exception ex) {
//			Response.Write(ex.ToString());
		}

		List<CommonLib.v_admin_auth> menus = null;
		menus = (from p in admin.auth_menus
				 where p.am_group_order == entity.am_group_order && p.am_depth == 1 && p.am_display == true
					orderby p.am_order ascending
					select p).ToList();

		// 3depth 에 권한이 있는 메뉴
		var thirdMenuGroup = (from p in admin.auth_menus
							  where p.am_group_order == entity.am_group_order && p.am_depth == 2 && p.am_display == true
				  select p.am_group).Distinct().ToList();

		var menus2 = from p in admin.menus
					 where thirdMenuGroup.Contains(p.am_code) && p.am_display == true
					 select new CommonLib.v_admin_auth() {
						 am_path = p.am_path, am_group = p.am_group, am_group_order = p.am_group_order, am_depth = p.am_depth, am_code = p.am_code , 
						 am_name = p.am_name , am_order = p.am_order
					 }
					;

		menus.AddRange(menus2);

		sub_menu.DataSource = menus.OrderBy(p=>p.am_order);
		sub_menu.DataBind();
		
	}

	protected void rpt_submenu_ItemDataBound(object sender, RepeaterItemEventArgs e) {

		CommonLib.v_admin_auth entity = e.Item.DataItem as CommonLib.v_admin_auth;
		AdminEntity admin = AdminLoginSession.GetCookie(this.Context);

		Repeater sub_menu = (Repeater)e.Item.FindControl("rpt_thirdmenu");
		Literal menu = (Literal)e.Item.FindControl("menu");
		Literal menu2 = (Literal)e.Item.FindControl("menu2");
		bool isActive = false;

		var cur_menu = this.GetCurrentMenuGroup();
		if (cur_menu != null) {
			if (cur_menu.am_code == entity.am_code) {
				isActive = true;
			} else if (cur_menu.am_depth == 2) {
				var admin_group = admin.menus.First(p => p.am_depth == 2 && p.am_code == cur_menu.am_code && p.am_display == true).am_group;
				//Response.Write(admin_group + " : " + cur_menu.am_group + " : " + entity.am_group + "\n");
				if (entity.am_code == admin_group) {
					isActive = true;
				}
			}
		}

		if (string.IsNullOrEmpty(entity.am_path)) {
			menu.Text = string.Format("<li class='treeview {1}'><a href='#'><i class='fa fa-circle-o'></i> {0} <i class='fa fa-angle-left pull-right'></i></a>  <ul class='treeview-menu'>", entity.am_name , isActive ? "active" : "");
			menu2.Text = "</ul></li>";
		} else {
			menu.Text = string.Format("<li class='{2}'><a href='{0}'><i class='fa fa-circle-o'></i> {1}</a><li>", entity.am_path, entity.am_name, isActive ? "active" : "");
		}


		List<CommonLib.v_admin_auth> menus = null;
		menus = (from p in admin.auth_menus
				 where p.am_group == entity.am_code && p.am_depth == 2 && p.am_display == true
				 orderby p.am_order ascending
				 select p).ToList();

		sub_menu.DataSource = menus;
		sub_menu.DataBind();

	}

	protected void rpt_thirdmenu_ItemDataBound(object sender, RepeaterItemEventArgs e) {

		CommonLib.v_admin_auth entity = e.Item.DataItem as CommonLib.v_admin_auth;

		HtmlControl li = (HtmlControl)e.Item.FindControl("li");
		// 현재메뉴 활성

		if (li != null) {
			var cur_menu = this.GetCurrentMenuGroup();
			
			if (cur_menu != null && cur_menu.am_code == entity.am_code) {
				
				li.Attributes["class"] = "active";
			}
		}

	}
}