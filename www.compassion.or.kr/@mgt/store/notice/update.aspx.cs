﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_about_us_report_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		base.BoardType = "storeNotice";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);

		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		if (base.Action == "update")
        {
            using (StoreDataContext dao = new StoreDataContext())
            {
                // 데이터 셋팅 
                //var row = dao.notice.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var row = www6.selectQFStore<notice>("idx", Convert.ToInt32(PrimaryKey));

                b_title.Text = row.title;
                b_content.InnerText = row.body;
                lb_filename.Text = row.fileName;
                lnFileName.NavigateUrl = row.saveFileName;

            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
		}
		else {
			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		AdminEntity adm = AdminLoginSession.GetCookie( this.Context );

        var arg = new notice() {
            fileName = System.IO.Path.GetFileName(temp_file_name.Value),
            saveFileName = temp_file_name.Value.WithFileServerHost(),
            title = b_title.Text,
            body = b_content.InnerText,
            writer = adm.name,
            dtReg = DateTime.Now            

        };


        using (StoreDataContext dao = new StoreDataContext())
        {
            if (base.Action == "update")
            {
                // 수정
                //var entity = dao.notice.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQFStore<notice>("idx", Convert.ToInt32(PrimaryKey));

                entity.title = arg.title;
                entity.body = arg.body;
                entity.dtReg = arg.dtReg;
                entity.writer = arg.writer;
                if (arg.saveFileName != "")
                {
                    entity.saveFileName = arg.saveFileName.WithFileServerHost();
                    entity.fileName = arg.fileName.WithFileServerHost();
                }
                //dao.SubmitChanges();
                //string wClause = string.Format("idx = {0}", Convert.ToInt32(PrimaryKey));
                www6.updateStore(entity);
                
                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다. ";
            }
            else
            {

                // 등록
                arg.viewCount = 0;
                //dao.notice.InsertOnSubmit(arg);
                www6.insertStore(arg);
                //dao.SubmitChanges();

                //PrimaryKey = arg.idx.ToString();

                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "등록되었습니다.";

            }
        }

	}

	protected void btn_remove_Click(object sender, EventArgs e)
    {
        using (StoreDataContext dao = new StoreDataContext())
        {
            //var entity = dao.notice.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            //dao.notice.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from notice where idx = {0}", Convert.ToInt32(PrimaryKey));
            www6.cudStore(delStr);


            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "삭제되었습니다.";
        }

	}

	protected void btn_remove_file_click(object sender, EventArgs e)
	{
		base.FileRemove( sender );
		//this.ShowFiles();
	}

	void ShowFiles() {
		if(!string.IsNullOrEmpty(base.Thumb)) {
			lb_filename.Text = System.IO.Path.GetFileName(base.Thumb);
			
		}
	}
	
	protected void btn_update_temp_file_click( object sender, EventArgs e ) {
		base.Thumb = temp_file_name.Value;
		this.ShowFiles();

	}

	
}