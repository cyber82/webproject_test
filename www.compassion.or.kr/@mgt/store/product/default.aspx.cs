﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_mainpage_default : AdminBasePage {

	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;

	}

	protected override void OnAfterPostBack(){
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page) {
		Master.IsSearch = false;
		if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != ""){
			Master.IsSearch = true;
		}

        using (StoreDataContext dao = new StoreDataContext())
        {
            //var list = dao.sp_product_list(page, paging.RowsPerPage, s_keyword_type.SelectedValue, s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword_type", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_keyword_type.SelectedValue, s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSPStore("sp_product_list", op1, op2).DataTableToList<sp_product_listResult>().ToList();

            var total = 0;


            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e){
		if (repeater != null){
			if (e.Item.ItemType != ListItemType.Footer){
			//	((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
			

			}
		}
	}

}
