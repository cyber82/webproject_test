﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_store_product_update" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	
	<style type="text/css">
	.item {float:left;width:100px;height:100px;position:absolute;background:#463b39;}
	
	
	.empty {background:#fff !important;display:none}
	.dropContainer .first {}
	.dropContainer .last {}
	.dropContainer .over {background:#79949b}
	.dropContainer .new {vertical-align:middle;text-align:center;}
	.dropContainer .new img {padding-top:31px}
	.dropContainer .remove {vertical-align:middle;text-align:center;}
	.dropContainer .remove img {padding-top:45px}
	.draggable img {width:100%;}
	.draggable {cursor:pointer}

	.item_opt{float:left;width:80px;height:28px;position:absolute;background:#fff;padding:2px;text-align:center;line-height:30px;}
	.item_opt img {padding-top:5px !important;width:18px;vertical-align:top}
	.item_opt.remove {background:#463b39;}

	</style>

	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dragAndDrop.js"></script>
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript" src="update.js"></script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	<input type=hidden runat=server id=upload_root value="" />
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>

	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="1" >기본정보</asp:LinkButton></li>
			<li runat="server" id="tabm2"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="2" >옵션</asp:LinkButton></li>
			<li runat="server" id="tabm3"><asp:LinkButton runat="server" OnClick="btn_section_Click" CommandArgument="3" >상품이미지</asp:LinkButton></li>

		</ul>
		<div class="tab-content">
			
		

			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">
					
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert"></button>
						<h4><i class="icon fa fa-check"></i>Alert!</h4>
						썸네일은 정사각형 비율로 등록해주셔야 됩니다.
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">어린이 상품</label>
						<div class="col-sm-10" style="margin-top:7px;">
							<asp:CheckBox runat="server" ID="gift"/>			
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-2 control-label">상품명 *</label>
						<div class="col-sm-10">
							<input type="text" runat="server" id="name" class="form-control" style="width:400px;" />		
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-2 control-label">썸네일</label>
						<div class="col-sm-10">
							<img style="max-width:100px" id="thumb" class="thumb" src="/@mgt/common/img/1logo.png" runat=server />
							<br />
							<asp:Literal runat="server" ID="thumb_msg" ></asp:Literal>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">소비자 가격(원)</label>
						<div class="col-sm-10">
							<input type="text" runat="server" id="retail_price" class="form-control number_only" style="width:400px;"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">판매가격(원) *</label>
						<div class="col-sm-10">
							<input type="text" runat="server" id="selling_price" class="form-control number_only" style="width:400px;"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">재고량 *</label>
						<div class="col-sm-10">
							<input type="text" runat="server" id="inventory" class="form-control number_only" style="width:400px;"/>
						</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-2 control-label">기부금 비율(%)</label>
						<div class="col-sm-10">
							<input type="text" runat="server" id="donation_rate" class="form-control number_only" style="width:400px;"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">배송비(원) *</label>
						<div class="col-sm-10">
							<input type="text" runat="server" id="delivery_charge" class="form-control number_only" style="width:400px;"/> 
						</div>
					</div>
					
								
					<div class="form-group">
						<label class="col-sm-2 control-label">상품진열</label>
						<div class="col-sm-10">
							<asp:RadioButtonList runat="server" ID="display_flag" RepeatDirection="Horizontal" style="width:400px;">
								<asp:ListItem Value="0">&nbsp;진열함&nbsp;</asp:ListItem>
								<asp:ListItem Value="1">&nbsp;진열안함</asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">상품 설명 *</label>
						<div class="col-sm-10">
							<textarea name="product_detail" id="product_detail" runat="server" style="width:800px; height:500px; display:none;"></textarea>
						</div>
					</div>

					
					<!-- 사용 안해서 display noone; -->
					<div class="form-group" style="display:none;">
						<label class="col-sm-2 control-label">배송 결제 방법 *</label>
						<div class="col-sm-10">
							<select runat="server" id="delivery_type" class="form-control" style="width:400px;">
								<option value="1">선불 배송(결제시 포함)</option>
								<option value="2">무료 배송</option>
								<option value="3">착불 배송</option>
							</select>
						</div>
					</div>
			
					<div class="form-group" style="display:none;">
						<label class="col-sm-2 control-label">상품 메뉴 분류 *</label>
						<div class="col-sm-10">
							<select id="p_type" runat="server">
								<option Value="1">Gift of Compassion</option>
								<option Value="2">CIV</option>
								<option Value="3">후원자 비자</option>
							</select>
						</div>
					</div>
			
					<div class="form-group" style="display:none;">
						<label class="col-sm-2 control-label">상품 분류 *</label>
						<div class="col-sm-10">
								대분류 <select runat="server" id="p_menu_idx"></select>
								소분류 <select runat="server" id="s_menu_idx"></select>
						</div>
					</div>
					
					<!-- //사용 안해서 display noone; -->

				</div>


				<div class="box-footer clearfix text-center">
					<asp:LinkButton runat=server ID="btn_update" OnClick="btn_update_Click" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
				</div>

			</div>
	


			<div class="tab-pane" id="tab2" runat="server" >

				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert"></button>
					<h4><i class="icon fa fa-check"></i>Alert!</h4>
					등록을 클릭해야 최종 반영됩니다. 
					<br /> 옵션명이 중복되면 등록이 되지 않습니다.
				</div>

				<div class="box box-default collapsed-box search_container">
					<div class="box-header with-border section-search">
						<div class="form-inline">

							<div class="input-group" style="width: 630px">
								<input type="text" id="opt_name" runat="server" placeholder="제목" class="form-control" style="display:inline-block;width:400px;margin-right:10px">
								<input type="text" id="opt_price" runat="server" placeholder="금액" class="form-control number_only" style="display:inline-block;width:200px">
							</div>
							<asp:LinkButton type="button" runat="server" id="btn_opt_add" OnClick="btn_opt_add_Click" class="btn btn-danger">추가</asp:LinkButton>
						</div>
					</div>
				</div>

				<div class="form-horizontal">
		
					<!-- /.box-header -->
					<div class="box-body">
						
						<ul id="list" data-role="list-container" class="todo-list">

							<asp:Repeater runat="server" ID="repeater_option">
								<ItemTemplate>
									<input type="hidden" runat="server" id="idx" value=<%#Eval("idx") %>/>
									
									<li data-role="item" data-id="">
										<span class="handle">
											<i class="fa fa-ellipsis-v"></i>
											<i class="fa fa-ellipsis-v"></i>
										</span>
					
										<span class="text" data-role="title" style="width:90%">
											<span style="width:60%;display:inline-block"><input type="text" id="option_name" value=<%#Eval("option_name") %> runat="server" style="width:100%" class="form-control"/></span>
											<span style="width:20%;display:inline-block"><input type="text" id="option_price" value=<%#Eval("option_price") %> runat="server" style="width:100%" class="form-control "/></span>
											<span style="width:10%;display:inline-block"><input type="checkbox" id="display" checked=<%#Eval("display").ToString() == "True" %> runat="server" /> 노출</span>
											<input type="hidden" class="option_priority" runat="server" id="option_priority" value=<%#Eval("option_priority") %>/>
										</span>
										<div class="tools" style="margin-top:10px">
											<asp:LinkButton runat="server" ID="btn_opt_remove" CommandArgument=<%#Eval("option_name") %> CommandName=<%#Eval("idx") %> OnClick="btn_opt_remove_Click">
											<i class="fa fa-trash-o" data-role="btn_remove"></i>
											</asp:LinkButton>
										</div>
									</li>

								</ItemTemplate>
							</asp:Repeater>

						</ul>
					</div>
	
				</div>

				<div class="box-footer clearfix text-center">
					<asp:LinkButton runat=server ID=btn_option_update OnClick="btn_option_update_Click" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
				</div>

			</div>

			<div class="tab-pane" id="tab3" runat="server" >
				
				
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert2"></button>
					<h4><i class="icon fa fa-check"></i>Alert!</h4>
					등록을 클릭해야 최종 반영됩니다. <br />
					이미지 추가 및 드래그 & 드롭을 통해 제품이미지 정보를 변경(순서,삭제)할 수 있습니다.<br />
					상품 이미지는 정사각형 비율로 등록해주셔야 됩니다.
					
				</div>

				<div style="padding-left:20px">
		
					<input type="hidden" runat="server" id="hd_images" value="" />
		
					<div id="pd_images" style="width:800px">
						<div style="position:absolute" class="dropContainer">
				
							<asp:Repeater runat="server" ID="pd_image_list_droppable">
								<ItemTemplate>
									<div class="droppable item" ></div>
								</ItemTemplate>
							</asp:Repeater>
				
							<div class="item new"><img id="btn_add_image" src="/@mgt/common/img/btn_image_add.gif" /></div>
							<div class="droppable item remove"><img src="/@mgt/common/img/btn_image_remove.gif" /></div>
						</div>

						<div style="position:absolute" class="dragContainer">
							<asp:Repeater runat="server" ID="pd_image_list_draggable">
								<ItemTemplate>
									<div class="draggable item"><img src="<%#(Uploader.GetRoot(Uploader.FileGroup.image_shop) + Eval("saved_file_name").ToString()).WithFileServerHost() %>" data-filename="<%# Eval("saved_file_name").ToString() %>" class="thumb" /></div>
								</ItemTemplate>
							</asp:Repeater>
						</div>

					</div>
									

				</div>

				<div class="box-footer clearfix text-center">
					<asp:LinkButton runat=server ID=btn_image_update OnClick="btn_image_update_Click" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
				</div>

			</div>

			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	

	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID="btn_remove" OnClick="btn_remove_Click" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>

</asp:Content>