﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Web.UI.HtmlControls;

public partial class mgt_store_product_update : AdminBasePage {

	List<product_option> ProductOptions {
		get
        {
			if(this.ViewState["product_option"] == null)
            {
				var list = new List<product_option>();
				if(base.Action == "update")
                {
                    using (StoreDataContext dao = new StoreDataContext())
                    {
                        //list = dao.product_option.Where(p => p.product_idx == Convert.ToInt32(PrimaryKey) && p.del_flag == false).OrderBy(p => p.option_priority).ToList();
                        list = www6.selectQStore<product_option>("product_idx", Convert.ToInt32(PrimaryKey), "del_flag", 0, "option_priority");
                    }
				}
				this.ViewState["product_option"] = list.ToJson();
			}
			return this.ViewState["product_option"].ToString().ToObject<List<product_option>>();
		}
		set {
			if(value == null)
				this.ViewState.Remove("product_option");
			else
				this.ViewState["product_option"] = value.ToJson();
		}
	}

	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];


		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();
		upload_root.Value = Uploader.GetRoot(Uploader.FileGroup.image_shop);

		if(base.Action == "update") {
            using (StoreDataContext dao = new StoreDataContext())
            {
                //var row = dao.product.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var row = www6.selectQFStore<product>("idx", Convert.ToInt32(PrimaryKey));

                gift.Checked = Convert.ToBoolean(row.gift_flag);
                p_type.SelectedIndex = row.p_type;
                p_menu_idx.SelectedIndex = row.p_menu_idx;
                s_menu_idx.SelectedIndex = row.s_menu_idx;
                name.Value = row.name;
                retail_price.Value = row.retail_price.ToString();
                selling_price.Value = row.selling_price.ToString();
                inventory.Value = row.inventory.ToString();
                donation_rate.Value = row.donation_rate.ToString();
                delivery_type.Value = row.delivery_type.ToString();
                delivery_charge.Value = row.delivery_charge.ToString();
                display_flag.SelectedValue = row.display_flag == true ? "0" : "1";
                product_detail.Value = row.product_detail;

                this.ShowImages();

                this.ShowOptions(false);

                base.Thumb = row.name_img;

                this.ShowThumb();
            }
		} else {

			tabm2.Visible = tabm3.Visible = false;

		}

	}

	protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	void ShowThumb() {
		if(!string.IsNullOrEmpty(base.Thumb)) {
			thumb.Src = (Uploader.GetRoot(Uploader.FileGroup.image_shop) + base.Thumb).WithFileServerHost();
			thumb_msg.Text = base.Thumb;
		}
	}

	void ShowImages()
    {
        using (StoreDataContext dao = new StoreDataContext())
        {
            //pd_image_list_draggable.DataSource = pd_image_list_droppable.DataSource = dao.product_image.Where(p => p.product_idx == Convert.ToInt32(PrimaryKey));
            pd_image_list_draggable.DataSource = pd_image_list_droppable.DataSource = www6.selectQStore<product_image>("product_idx", Convert.ToInt32(PrimaryKey));
            pd_image_list_droppable.DataBind();
            pd_image_list_draggable.DataBind();
        }
	}

	void ShowOptions(bool force) {

		if(force) {
			this.ViewState["product_option"] = null;
		}

		repeater_option.DataSource = this.ProductOptions;
		repeater_option.DataBind();

	}

	protected void btn_update_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
				return;
		}


		var arg = new product() {
			gift_flag = gift.Checked,
			p_type = 1,//p_type.SelectedIndex,
			//p_menu_idx = 1,//p_menu_idx.SelectedIndex,
			//s_menu_idx = 1,//s_menu_idx.SelectedIndex,
			//delivery_type = 1,//Convert.ToChar(delivery_type.Value),
			name = name.Value,
			retail_price = Convert.ToInt32(retail_price.Value.ValueIfNull("0")),
			selling_price = Convert.ToInt32(selling_price.Value.ValueIfNull("0")),
			inventory = Convert.ToInt32(inventory.Value.ValueIfNull("0")),
			donation_rate = Convert.ToInt32(donation_rate.Value.ValueIfNull("0")),
			delivery_charge = Convert.ToInt32(delivery_charge.Value.ValueIfNull("0")),
			display_flag = Convert.ToInt32(display_flag.SelectedValue) == 0 ? true : false,
			product_detail = product_detail.Value,
			reg_date = DateTime.Now , 
			name_img = base.Thumb
		};

        //Response.Write(arg);

        using (StoreDataContext dao = new StoreDataContext())
        {
            if (base.Action == "update")
            {
                //var entity = dao.product.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQFStore<product>("idx", Convert.ToInt32(PrimaryKey));

                entity.gift_flag = gift.Checked;
                //entity.p_type = p_type.SelectedIndex;
                //entity.p_menu_idx = p_menu_idx.SelectedIndex;
                //entity.p_menu_idx = p_menu_idx.SelectedIndex;
                //entity.s_menu_idx = s_menu_idx.SelectedIndex;
                entity.name = name.Value;
                entity.retail_price = Convert.ToInt32(retail_price.Value.ValueIfNull("0"));
                entity.selling_price = Convert.ToInt32(selling_price.Value.ValueIfNull("0"));
                entity.inventory = Convert.ToInt32(inventory.Value.ValueIfNull("0"));
                entity.donation_rate = Convert.ToInt32(donation_rate.Value.ValueIfNull("0"));
                //entity.delivery_type = Convert.ToChar(delivery_type.Value);
                entity.delivery_charge = Convert.ToInt32(delivery_charge.Value.ValueIfNull("0"));
                entity.display_flag = Convert.ToInt32(display_flag.SelectedValue) == 0 ? true : false;
                entity.product_detail = product_detail.Value;
                entity.name_img = base.Thumb;

                //dao.SubmitChanges();
                www6.updateStore(entity);

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";

            }
            else
            {
                //dao.product.InsertOnSubmit(arg);
                www6.insertStore(arg);
                //dao.SubmitChanges();

                PrimaryKey = arg.idx.ToString();
                base.WriteLog(AdminLog.Type.insert, string.Format("{0}", arg.ToJson()));

                Master.ValueAction.Value = "";
                Master.ValueMessage.Value = "등록되었습니다.";

                base.Action = "update";
                tabm2.Visible = tabm3.Visible = true;
            }
        }
	}

	protected void btn_remove_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

		var idx = Convert.ToInt32(this.PrimaryKey);

        using (StoreDataContext dao = new StoreDataContext())
        {
            var exist = www6.selectQStore<orderDetail>("productIDX", idx);

            //if (dao.orderDetail.Any(p => p.productIDX == idx))
            if(exist.Any())
            {
                Master.ValueAction.Value = "";
                Master.ValueMessage.Value = "주문된 상품은 삭제할 수 없습니다.";
                return;
            }

            //var entity = dao.product.First(p => p.idx == idx);
            //dao.product.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from product where idx = {0}", idx);
            www6.cudStore(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "삭제되었습니다.";
        }
	}
	
	protected void btn_section_Click( object sender, EventArgs e ) {
		LinkButton obj = sender as LinkButton;
		var no = obj.CommandArgument;

		int tabCount = 3;

		for(int i = 1; i < tabCount + 1; i++) {

			if(no == i.ToString()) {
				((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "active";
				((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "active tab-pane";
			} else {
				((HtmlControl)this.FindAnyControl("tabm" + i.ToString())).Attributes["class"] = "";
				((HtmlControl)this.FindAnyControl("tab" + i.ToString())).Attributes["class"] = "tab-pane";

			}

			if(no == "2") {
				
			}

			if(no == "3") {
				
			}


		}

	}

	// 썸네일
	protected void btn_update_temp_file_click( object sender, EventArgs e ) {

		switch(temp_file_type.Value) {
			case "thumb":
				base.Thumb = temp_file_name.Value.Replace(Uploader.GetRoot(Uploader.FileGroup.image_shop) , "");
				this.ShowThumb();
				break;
			default:
				break;
		}

	}

	// 이미지
	protected void btn_image_update_Click( object sender, EventArgs e )
    {
		if(base.IsRefresh) {
			return;
		}

        using (StoreDataContext dao = new StoreDataContext())
        {
            if (base.Action == "update")
            {
                var images = hd_images.Value; // | 구분자
                var product_images = new List<product_image>();
                foreach (var img in images.Split('|'))
                {

                    if (!string.IsNullOrEmpty(img))
                    {
                        product_images.Add(new product_image()
                        {
                            product_idx = Convert.ToInt32(PrimaryKey),
                            file_ext = "",
                            dvn = "",
                            file_size = 0,
                            height = 0,
                            img_flag = true,
                            ori_file_name = "",
                            saved_file_name = img,
                            width = 0
                        });
                    }

                }
                //dao.product_image.DeleteAllOnSubmit(dao.product_image.Where(p => p.product_idx == Convert.ToInt32(PrimaryKey)));
                //dao.SubmitChanges();
                string delStr = string.Format("delete from product_image where product_idx = {0}", Convert.ToInt32(PrimaryKey));
                www6.cudStore(delStr);

                //dao.product_image.InsertAllOnSubmit(product_images);
                foreach (var pi in product_images)
                {
                    www6.insertStore(pi);
                }
                //dao.SubmitChanges();

                base.WriteLog(AdminLog.Type.update, string.Format("{0}", images));
                Master.ValueMessage.Value = "수정되었습니다.";

                ShowImages();

            }
        }

		
	}


	// 옵션
	protected void btn_opt_add_Click( object sender, EventArgs e ) {
		var entity = new product_option() {
			idx = -1 , 
			option_level = 0,
			option_name = opt_name.Value,
			option_price = Convert.ToInt32(opt_price.Value == "" ? "0" : opt_price.Value),
			option_priority = this.ProductOptions.Count + 1,
			del_flag = false, parent_option_idx = 0,
			product_idx = Convert.ToInt32(PrimaryKey)
		};

		var list = this.ProductOptions;
		list.Add(entity);

		this.ProductOptions = list;

		this.ShowOptions(false);

		opt_name.Value = "";
		opt_price.Value = "";
	}

	protected void btn_opt_remove_Click( object sender, EventArgs e ) {

		if(base.IsRefresh) {
			return;
		}

		LinkButton obj = sender as LinkButton;
		var option_name = obj.CommandArgument;
		var idx = Convert.ToInt32(obj.CommandName);

		var list = this.ProductOptions;
		list.Remove(list.First(p => p.option_name == option_name && p.idx == idx));

		this.ProductOptions = list;

		this.ShowOptions(false);

	}

	protected void btn_option_update_Click( object sender, EventArgs e ) {
		if(base.IsRefresh) {
			return;
		}

		List<product_option> currents;
		var opts = this.ProductOptions;


		// 옵션명 중복 검사
		List<string> options_names = new List<string>();
		foreach(RepeaterItem item in repeater_option.Items) {
			var option_name = item.FindControl("option_name") as HtmlInputText;
			options_names.Add(option_name.Value);

		}
		if (options_names.Distinct().Count() != options_names.Count) {
			Master.ValueMessage.Value = "중복되는 옵션명이 존재합니다.";
			return;
		}


		List<product_option> added = new List<product_option>();
        using (StoreDataContext dao = new StoreDataContext())
        {
            //currents = dao.product_option.Where(p => p.product_idx == Convert.ToInt32(PrimaryKey)).ToList();
            currents = www6.selectQStore<product_option>("product_idx", Convert.ToInt32(PrimaryKey));

            // 삭제
            var deleteList = new List<product_option>();
            foreach (var po in currents.Where(p => !opts.Select(q => q.idx).Contains(p.idx)))
            {

                // 한번이상 사용된 옵션이면 업데이트
                var exist = www6.selectQStore<orderDetail>("optionIDX", po.idx);
                //if (dao.orderDetail.Any(p => p.optionIDX == po.idx))
                if(exist.Any())
                {
                    po.del_flag = true;
                }
                else
                {
                    deleteList.Add(po);
                }
            }

            //if (deleteList.Count > 0)
            //    dao.product_option.DeleteAllOnSubmit(deleteList);
            foreach(var del in deleteList)
            {
                string delStr = string.Format("delete from product_option where idx = {0}", del.idx);
                www6.cudStore(delStr);
            }

            // 추가, 수정
            foreach (RepeaterItem item in repeater_option.Items)
            {
                var idx = item.FindControl("idx") as HtmlInputHidden;
                var option_name = item.FindControl("option_name") as HtmlInputText;
                var option_price = item.FindControl("option_price") as HtmlInputText;
                var option_priority = item.FindControl("option_priority") as HtmlInputHidden;
                var display = item.FindControl("display") as HtmlInputCheckBox;

                if (Convert.ToInt32(idx.Value) > -1)
                {
                    //var po = dao.product_option.First(p => p.idx == Convert.ToInt32(idx.Value));
                    var po = www6.selectQFStore<product_option>("idx", Convert.ToInt32(idx.Value));

                    po.option_name = option_name.Value;
                    po.option_price = Convert.ToInt32(option_price.Value == "" ? "0" : option_price.Value);
                    po.option_priority = Convert.ToInt32(option_priority.Value == "" ? "0" : option_priority.Value);
                    po.display = display.Checked;

                    www6.updateStore(po);
                }
                else
                {
                    var po = opts.First(p => p.option_name == option_name.Value);
                    po.option_priority = Convert.ToInt32(option_priority.Value == "" ? "0" : option_priority.Value);
                    po.display = display.Checked;
                    

                    added.Add(po);
                }

            }

            if (added.Count > 0)
            {
                //dao.product_option.InsertAllOnSubmit(added);
                foreach (var add in added)
                {
                    www6.insertStore(add);
                }
            }

            //dao.SubmitChanges();

            base.WriteLog(AdminLog.Type.update, string.Format("{0}", opts.ToJson()));
            Master.ValueMessage.Value = "수정되었습니다.";

            this.ShowOptions(true);
        }
	}
}