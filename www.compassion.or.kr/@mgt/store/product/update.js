﻿$(function () {

	$page.init();

});

var $page = {

	init: function () {
		$("#btn_remove").click(function () {
			return confirm("삭제하시겠습니까?");
		});

		$("ul.nav-tabs li").each(function (i) {
			if ($(this).hasClass("active")) {
				eval('$page.tab' + (i + 1) + '.init()');
			}
		})

	},

	tab1: {
		init: function () {

			var uploader_thumb = attachUploader("thumb");
			uploader_thumb._settings.data.rename = "y";
			uploader_thumb._settings.data.fileType = "image";
			uploader_thumb._settings.data.limit = 500;
			image_path = uploader_thumb._settings.data.fileDir = $("#upload_root").val();

			initEditor(oEditors, "product_detail");

			$("#delivery_type").change(function () {
				if ($(this).val() == 2) {
					$("#delivery_charge").val(0)
				}
			})

			$("#btn_update").click(function () {
				return $page.tab1.onSubmit();
			})

		},
		onSubmit: function () {

			if (!validateForm([
				{ id: "#b_title", msg: "제목을 입력하세요" },
				{ id: "#b_summary", msg: "목차를 입력하세요" }
			])) {
				return false;
			}
			oEditors.getById["product_detail"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			return confirm("등록 하시겠습니까?");

		}


	},

	tab2: {
		init: function () {

			$("#list").sortable({
				placeholder: "sort-highlight",
				handle: ".handle",
				forcePlaceholderSize: true,
				zIndex: 999999
			}).disableSelection();

			$("#btn_option_update").click(function () {
				return $page.tab2.onSubmit();
			})

		},

		onSubmit: function () {

			$.each($("#list li"), function (i) {
				$(this).find(".option_priority").val(i + 1);
			});

			return true;
		}
	},

	tab3: {
		init: function () {

			$("#pd_images").dragAndDrop({
				mode: "slide",	// eachother , slide
				width: 800,
				item_width: 110,
				item_height: 147
			});

			var uploader_thumb = $page.tab3.attachUploader("btn_add_image");
			uploader_thumb._settings.data.rename = "y";
			uploader_thumb._settings.data.fileType = "image";
			uploader_thumb._settings.data.limit = 1000;
			image_path = uploader_thumb._settings.data.fileDir = $("#upload_root").val();

			$("#btn_image_update").click(function () {
				return $page.tab3.onSubmit();
			})

		},
		attachUploader: function (button) {
			return new AjaxUpload(button, {
				action: '/common/handler/upload',
				responseType: 'json',
				onChange: function () {
				},
				onSubmit: function (file, ext) {
					this.disable();
				},
				onComplete: function (file, response) {

					this.enable();
					
					if (response.success) {

						var path = response.name;
						$page.tab3.file_uploaded(file , path);

					} else
						alert(response.msg);
				}
			});
		},

		file_uploaded: function (file , path) {
			
			var droppable = "<div class='droppable item'></div>"
			var draggable = "<div class='draggable item' data-name=''><img src='" + path + "' class='thumb' data-filename='" + file + "' /></div>"

			$("#pd_images").data("ui-dragAndDrop").append(droppable, draggable);

		},

		onSubmit: function () {

			var obj = $("#pd_images").data("ui-dragAndDrop");
			var data = "";
			$.each(obj.getElements(), function () {

				data += "|" + $($(this).find("img")).data("filename");
			});

			if (data.length > 0)
				data = data.substr(1);
			$("#hd_images").val(data);
			return true;
		}


	}

}
