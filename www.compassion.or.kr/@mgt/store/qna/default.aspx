﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_mainpage_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	 <style type="text/css">
        .accordion.fold { height:14px;overflow:hidden;padding:0;}
        .accordion.open { height:auto;padding:0;}

    </style>
	<script type="text/javascript">
		$(function () {
			$(".btn-box-tool").click(function () {
				if($(this).find('i').hasClass('fa-plus')){
					close(this);
				} else {
					open(this);
				}
				return false;
			});

			var open = function(li){

				$(li).find("i").addClass('fa-plus');
				$(li).find("i").removeClass('fa-minus');

				$(li).parentsUntil("td").removeClass('open');
				$(li).parentsUntil("td").addClass('fold');

			}

			var close = function (li) {
				$(li).find("i").removeClass('fa-plus');
				$(li).find("i").addClass('fa-minus');

				$(li).parentsUntil("td").removeClass('fold');
				$(li).parentsUntil("td").addClass('open');
			}
		});
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	<input type="hidden" runat="server" id="sortColumn" value="mp_order" />
	<input type="hidden" runat="server" id="sortDirection" value="asc" />

	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>
		<div class="form-horizontal box-body" >
					
			<div class="form-group">
				<label for="s_answer" class="col-sm-2 control-label">답변 여부</label>
				<div class="col-sm-10" style="margin-top:5px;">
					<asp:RadioButtonList runat="server" ID="s_answer" RepeatDirection="Horizontal" RepeatLayout="Flow" >
						<asp:ListItem Selected="True"  Value="-1" Text="전체&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="1" Text="답변 &nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="0" Text="미답변 &nbsp;&nbsp;&nbsp;"></asp:ListItem>
					</asp:RadioButtonList>
				</div>
			</div>


			<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">기간 검색(등록일)</label>
					<div class="col-sm-10">
						<div class="inline">
							<asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							~
						<asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>

						<div class="inline">
							<button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							<div class="btn-group">
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								<a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								<a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							</div>
						</div>

					</div>

				</div>	
			
				
			<div class="form-group">
				<label for="s_keyword" class="col-sm-2 control-label">검색</label>
				<div class="col-sm-10">
					<asp:TextBox runat="server" ID="s_keyword" Width="415" cssClass="form-control inline"  OnTextChanged="search" AutoPostBack="true"></asp:TextBox>
					* 문의내용, 작성자
				</div>
			</div>
			
			<div class="box-footer clearfix text-center">
                <asp:LinkButton runat="server" ID="LinkButton1" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
			</div>

		</div>
	</div>

	<br />


    <div class="box box-primary">
		<div class="box-header">
            <h3 class="box-title">총 <strong><asp:Literal runat="server" ID="lbTotal"></asp:Literal></strong> 건</h3>
        </div><!-- /.box-header -->
		<div class="box-body table-responsive no-padding">

			<table class="table table-hover table-bordered ">
				<colgroup>
					<col width="5%" />
                    <col width="5%" />
					<col width="10%" />
					<col width="48%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
				
				</colgroup>
				<thead>
					<tr>
						<th>번호</th>
                        <th>이미지</th>
						<th>상품명</th>
						<th>문의내용</th>
						<th>작성자</th>
						<th>문의일</th>
						<th>답변여부</th>
						<th>답변일</th>
					</tr>
				</thead>
				<tbody>
				
					<asp:Repeater runat="server" ID="repeater" OnItemDataBound=ListBound>
						<ItemTemplate>
							<tr class="tr_link" onclick="goPage('update.aspx' , { c: <%#Eval("idx")%>, p: '<%#paging.CurrentPage %>' , t: 'update' })">
								<td><asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
                                <td><img src="<%# (Uploader.GetRoot(Uploader.FileGroup.image_shop) + Eval("name_img").ToString()).WithFileServerHost()%>" width="50"/></td>
								<td class="text-left"><%#Eval("name") %></td>
								<td class="text-left" style="word-break:break-all;">
                                    <div class="accordion fold">
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" style="padding-top:0" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                        </div>
                                        <%#Eval("question")%>
                                    </div>
								</td>
								<td><%#Eval("user_name") %> <br /> <%#Eval("user_id") %></td>
								<td><%#Eval("reg_date","{0:yy/MM/dd}") %></td>
								<td><asp:Literal runat="server" ID="lbAnswer" /></td>
								<td><%#Eval("answer_date", "{0:yy/MM/dd}") %></td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								<td colspan="8">데이터가 없습니다.</td>
							</tr>
						</FooterTemplate>
					</asp:Repeater>

				</tbody>
			</table>
		</div>


		<div class="box-footer clearfix text-center">
			<uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
			<!--
				<asp:LinkButton runat="server" href="#" ID="btn_add" OnClientClick="goPage('update.aspx' , {t:'insert'})" CssClass="btn btn-bitbucket pull-right">신규등록</asp:LinkButton>
			-->
		</div>

		<!--	
		<div class="box-footer clearfix text-center">
		</div>
		-->

	</div>

</asp:Content>