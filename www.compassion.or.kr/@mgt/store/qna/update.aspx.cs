﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class mgt_about_us_report_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();


		base.BoardType = "stroeNotice";
		base.FileGroup = Uploader.FileGroup.file_board;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);

		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		getData();
	}
	

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	void getData()
    {
        using (StoreDataContext dao = new StoreDataContext())
        {
            //var row = dao.sp_qna_get(Convert.ToInt32(PrimaryKey)).First();
            Object[] op1 = new Object[] { "idx" };
            Object[] op2 = new Object[] { Convert.ToInt32(PrimaryKey) };
            var row = www6.selectSPStore("sp_qna_get", op1, op2).DataTableToList<sp_qna_getResult>().ToList().First();

            var product_idx = row.product_idx;

            txt_user.InnerText = row.user_name + " / " + row.user_id;

            txt_regdate.InnerText = Convert.ToString(row.reg_date);
            txt_question.InnerText = row.question.ToHtml().Replace("<br>", "\r\n").Replace("<br/>", "\r\n").StripHtml();
            txt_answer.InnerText = row.answer;

            //var productRow = dao.product.First(p => p.idx == product_idx);
            var productRow = www6.selectQFStore<product>("idx", product_idx);

            txt_product.InnerText = productRow.name;
            img_product.Src = (Uploader.GetRoot(Uploader.FileGroup.image_shop) + productRow.name_img).WithFileServerHost();

        }
	}

	protected void btn_update_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}

        using (StoreDataContext dao = new StoreDataContext())
        {
            //var entity = dao.qna.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQFStore<qna>("idx", Convert.ToInt32(PrimaryKey));

            entity.answer = txt_answer.InnerText;
            entity.answer_date = DateTime.Now;
            //dao.SubmitChanges();
            //string wClause = string.Format("idx = {0}", Convert.ToInt32(PrimaryKey));
            www6.updateStore(entity);

            base.WriteLog(AdminLog.Type.update, string.Format("{0}", entity.ToJson()));

            Master.ValueAction.Value = "list";
            Master.ValueMessage.Value = "수정되었습니다.";
        }
	}

	protected void btn_remove_Click(object sender, EventArgs e){

		if (base.IsRefresh){
			return;
		}

        using (StoreDataContext dao = new StoreDataContext())
        {
            //var entity = dao.qna.First(p => p.idx == Convert.ToInt32(PrimaryKey));            
            //dao.qna.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            string delStr = string.Format("delete from qna where idx = {0}", Convert.ToInt32(PrimaryKey));
            www6.cudStore(delStr);

            base.WriteLog(AdminLog.Type.delete, string.Format("{0}", delStr));
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";

	}
}