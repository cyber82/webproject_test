﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_about_us_report_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
  	<script type="text/javascript">

		$(function () {

		});

		var onSubmit = function () {
			if ($("#txt_answer").val().length < 1) {
				alert("답변을 입력해주세요.")
				return false;
			}
			return true;
		}

		var onRemove = function () {
			return confirm("삭제하시겠습니까?")
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file></asp:LinkButton>

    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					

					<div class="form-group">
						<label class="col-sm-2 control-label">상품</label>
						<div class="col-sm-10">
							<img runat="server" id="img_product" width="200"/>
						</div>
					</div>



					<div class="form-group">
						<label class="col-sm-2 control-label">상품명</label>
						<div class="col-sm-10">
							<label runat="server" id="txt_product" class="form-control" style="width:600px;"></label>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">문의일</label>
						<div class="col-sm-10">
							<label runat="server" id="txt_regdate" class="form-control" style="width:200px;"></label>
						</div>
					</div>

					

					<div class="form-group">
						<label class="col-sm-2 control-label">작성자</label>
						<div class="col-sm-10">
							<label runat="server" id="txt_user" class="form-control" style="width:600px;"></label>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">문의 내용 </label>
						<div class="col-sm-10">
							<textarea runat="server" id="txt_question"  class="form-control" style="width:600px; height:200px" readonly></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">답변 내용</label>
						<div class="col-sm-10">
							<textarea runat="server" id="txt_answer" class="form-control" style="width:600px; height:200px" ></textarea>
                        </div>
					</div>         
				</div>
			</div>

			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<asp:LinkButton runat=server ID=btn_remove  OnClick="btn_remove_Click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update  OnClick="btn_update_Click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px" >등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>