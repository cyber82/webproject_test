﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class mgt_store_order_update : AdminBasePage {

	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();


		using (StoreDataContext dao = new StoreDataContext()){
            // 데이터 셋팅 

            // 주문자 정보 
            //var deliveryRow = dao.orderDelivery.First( p => p.orderNo == PrimaryKey );
            var deliveryRow = www6.selectQFStore<orderDelivery>("orderNo", PrimaryKey);
            
            //결제 내역
            //var orderMaster = dao.orderMaster.First( p => p.orderNo == PrimaryKey );
            var orderMaster = www6.selectQFStore<orderMaster>("orderNo", PrimaryKey);
            
            // 주문자 정보 
            orderNo.InnerText = PrimaryKey;
			orderName.InnerText = deliveryRow.orderName + " / " +orderMaster.UserID;
			//dtReg.InnerText = deliveryRow.deliveryDate.ToString();
			email.InnerText = deliveryRow.orderEmail;
			addr.InnerText = "("+ deliveryRow.orderZipCode + ") " + deliveryRow.orderAddr + " " + deliveryRow.orderAddrDetail;
			phone.InnerText = deliveryRow.orderPhone;
			memo.InnerText = deliveryRow.memo;

			// 주문내역
			//var orderList = dao.sp_orderDetail_list_f( PrimaryKey ).ToList();
            Object[] op1 = new Object[] { "orderNo" };
            Object[] op2 = new Object[] { PrimaryKey };
            var orderList = www6.selectSPStore("sp_orderDetail_list_f", op1, op2).DataTableToList<sp_orderDetail_list_fResult>().ToList();

            repeater.DataSource = orderList;
			repeater.DataBind();
			

			// 결제 내역 
			price.InnerText = String.Format( "{0:N0}", orderMaster.orderPrice ) + "원";
            
			var paymethod = "";
			if(orderMaster.payMethod == "100000000000") {
				paymethod = "신용카드";
			} else if(orderMaster.payMethod == "010000000000") {
				paymethod = "계좌이체";
			} else if(orderMaster.payMethod == "000010000000") {
				paymethod = "휴대폰";
			} else if(orderMaster.payMethod == "payco") {
				paymethod = "페이코";
			} else if(orderMaster.payMethod == "kakaopay") {
				paymethod = "카카오페이";
			}

			means.InnerText = paymethod;

			//주문자 정보 - 주문하신 날짜 
			dtReg.InnerText = orderMaster.dtReg.ToString();

            totalPrice.InnerText = "주문 금액 : " + String.Format( "{0:N0}", orderMaster.orderPrice ) + "원 , 배송비 :" + String.Format( "{0:N0}", orderMaster.deliveryFee ) + "원 , 총금액 : " + String.Format( "{0:N0}", (orderMaster.orderPrice + orderMaster.deliveryFee) ) + "원";
			
		}
		
	}

	protected override void OnAfterPostBack(){
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	

	protected void ListBound(object sender, RepeaterItemEventArgs e)
	{
		if (repeater != null)
		{
			if (e.Item.ItemType != ListItemType.Footer)
			{

			}
		}
	}


}