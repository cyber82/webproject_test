﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="mgt_store_order_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
		$(function () {
		});
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	<input type="hidden" runat="server" id="sortColumn" value="mp_order" />
	<input type="hidden" runat="server" id="sortDirection" value="asc" />

	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>
		<div class="form-horizontal box-body" >
			
			<div class="form-group">
				<label for="s_state" class="col-sm-2 control-label">신청 구분</label>
				<div class="col-sm-10" style="margin-top:5px;">
					<asp:RadioButtonList runat="server" ID="s_state" RepeatDirection="Horizontal" RepeatLayout="Flow" >
						<asp:ListItem Selected="True"  Value="-1" Text="전체&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="2" Text="주문완료&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="3" Text="취소&nbsp;&nbsp;&nbsp;"></asp:ListItem>
						<asp:ListItem Value="5" Text="출고완료&nbsp;&nbsp;&nbsp;"></asp:ListItem>
					</asp:RadioButtonList>
				</div>
			</div>



			<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">기간 검색(등록일)</label>
					<div class="col-sm-10">
						<div class="inline">
							<asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							~
						<asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>

						<div class="inline">
							<button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							<div class="btn-group">
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								<a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								<a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							</div>
						</div>

					</div>

				</div>	
			
				
			<div class="form-group">
				<label for="s_keyword" class="col-sm-2 control-label">검색</label>
				<div class="col-sm-10">
					<asp:TextBox runat="server" ID="s_keyword" Width="415" cssClass="form-control inline"  OnTextChanged="search" AutoPostBack="true"></asp:TextBox>
					* 주문자
				</div>
			</div>
			
			<div class="box-footer clearfix text-center">
                <asp:LinkButton runat="server" ID="LinkButton1" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
			</div>

		</div>
	</div>

	<br />


    <div class="box box-primary">
		<div class="box-header">
            <h3 class="box-title">총 <strong><asp:Literal runat="server" ID="lbTotal"></asp:Literal></strong> 건</h3>
        </div><!-- /.box-header -->
		<div class="box-body table-responsive no-padding">

			<table class="table table-hover table-bordered ">
				<colgroup>
					<col width="5%" />
					<col width="10%" />
					<col width="12%" />
					<col width="20%" />
					<col width="8%" />
					<col width="8%" />
					<col width="13%" />
					<col width="12%" />
					<col width="12%" />
				</colgroup>
				<thead>
					<tr>
						<th>번호</th>
						<th>주문일</th>
						<th>주문번호</th>
						<th>상품</th>
						<th>주문자</th>
						<th>결제방법</th>
						<th>운송장번호</th>
						<th>택배</th>
						<th>진행</th>
					</tr>
				</thead>
				<tbody>
				
					<asp:Repeater runat="server" ID="repeater" OnItemDataBound=ListBound>
						<ItemTemplate>
							<tr class="tr_link">
								<td><asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
								<td><%#Eval("dtReg", "{0:yy/MM/dd}") %></td>
								<td><%#Eval("orderNo") %><%# Eval("repCorrespondenceId") == null ? "" : "<br/>(선물편지)" %></td>
								<td class="text-left" onclick="goPage('update.aspx' , { c: '<%#Eval("orderNo")%>', p: '<%#paging.CurrentPage %>' , t: 'update' })" >
									<img src="<%# Uploader.GetRoot(Uploader.FileGroup.image_shop) + Eval("img") %>" width="50" />
									<%#Eval("productTitle") %>
									<%#Eval("cnt").ToString() == "1" ? "" : "..등 "+Eval("cnt")+"건" %>
								</td>
								<td><%#Eval("orderName") %> <br /> <%#Eval("UserID") %></td>
								<td><asp:Literal runat="server" ID="paymethod"></asp:Literal></td>
								<td><%#Eval("deliveryNum") %><br />

									<asp:TextBox runat="server" ID="txt_deliveryNum" class=""></asp:TextBox>
								</td>
								<td>

									<asp:DropDownList runat="server" ID="deliveryCo">
										<asp:ListItem Value="0">선택</asp:ListItem>
										<asp:ListItem Value="1">우체국</asp:ListItem>
										<asp:ListItem Value="2">로젠</asp:ListItem>
										<asp:ListItem Value="3">현대</asp:ListItem>
										<asp:ListItem Value="5">CJ대한통운</asp:ListItem>
										<asp:ListItem Value="4">기타</asp:ListItem>
									</asp:DropDownList>

									<asp:LinkButton runat="server" Class="btn-sm btn-primary " ID="btn_deliveryCo_save" CommandName=<%#Eval("orderNo")%> OnClick="btn_deliveryCo_save_Click">저장</asp:LinkButton></td>

								</td>
								<td>
									
									<asp:DropDownList runat="server" ID="deliveryState">
										<asp:ListItem Value="2">주문완료</asp:ListItem>
										<asp:ListItem Value="3">취소</asp:ListItem>
										<asp:ListItem Value="5">출고완료</asp:ListItem>
									</asp:DropDownList>
									
									<asp:LinkButton runat="server" Class="btn-sm btn-primary " ID="btn_state_save" CommandName=<%#Eval("orderNo")%> OnClick="btn_state_save_Click">저장</asp:LinkButton></td>

								</td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								<td colspan="9">데이터가 없습니다.</td>
							</tr>
						</FooterTemplate>
					</asp:Repeater>

				</tbody>
			</table>
		</div>


		<div class="box-footer clearfix text-center">
			<uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
			<!--
				<asp:LinkButton runat="server" href="#" ID="btn_add" OnClientClick="goPage('update.aspx' , {t:'insert'})" CssClass="btn btn-bitbucket pull-right">신규등록</asp:LinkButton>
			-->
		</div>

		<!--	
		<div class="box-footer clearfix text-center">
		</div>
		-->

	</div>

</asp:Content>