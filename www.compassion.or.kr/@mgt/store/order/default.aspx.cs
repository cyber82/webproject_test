﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_store_order_default : AdminBasePage {

	protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;

	}

	protected override void OnAfterPostBack(){
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page){
		Master.IsSearch = false;
		if (s_state.SelectedValue != "-1" || s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != ""){
			Master.IsSearch = true;
		}

        using (StoreDataContext dao = new StoreDataContext())
        {
            //var list = dao.sp_order_list(page, paging.RowsPerPage, Convert.ToInt32(s_state.SelectedValue), "", s_keyword.Text, s_b_date.Text, s_e_date.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "state", "keyword_type", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, Convert.ToInt32(s_state.SelectedValue), "", s_keyword.Text, s_b_date.Text, s_e_date.Text };
            var list = www6.selectSPStore("sp_order_list", op1, op2).DataTableToList<sp_order_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();


            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();

        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e){

		if(e.Item.ItemType == ListItemType.Footer)
			return;

		sp_order_listResult entity = e.Item.DataItem as sp_order_listResult;

		((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();

		var paymethod = "";
		if(entity.payMethod == "100000000000") {
			paymethod = "신용카드";
		} else if(entity.payMethod == "010000000000") {
			paymethod = "계좌이체";
		}else  if(entity.payMethod == "000010000000") {
			paymethod = "휴대폰";
		} else if(entity.payMethod == "payco") {
			paymethod = "페이코";
		} else if(entity.payMethod == "kakaopay") {
			paymethod = "카카오페이";
		}
		((Literal)e.Item.FindControl("paymethod")).Text = paymethod;

		//<%#Eval("payMethod").ToString() == "100000000000" ? "카드결제" : "계좌이체" %>
		var deliveryCo = (DropDownList)e.Item.FindControl( "deliveryCo" );
		deliveryCo.SelectedValue = entity.deliveryCo.Value.ToString();


		var deliveryState = (DropDownList)e.Item.FindControl( "deliveryState" );
		deliveryState.SelectedValue = entity.state.ToString();

	}

	protected void btn_deliveryCo_save_Click(object sender, EventArgs e){
		if (base.IsRefresh){
			return;
		}


		LinkButton obj = sender as LinkButton;
		var orderNo =  obj.CommandName;

		var deliveryCo = (DropDownList)obj.Parent.FindControl("deliveryCo");
		var deliveryState = (DropDownList)obj.Parent.FindControl( "deliveryState" );
		//Response.Write(deliveryCo.SelectedValue);

		var deliveryNum = (TextBox)obj.Parent.FindControl( "txt_deliveryNum" );
		//Response.Write( deliveryNum.Text );


		// 운송장 번호와 택배가 지정된 상태면 출고완료 
		var is_deliver = false;
		if(deliveryNum.Text != "" && Convert.ToInt32( deliveryCo.SelectedValue ) != 0 && Convert.ToInt32( deliveryState.SelectedValue ) == 2) {
			is_deliver = true;
		}

        using (StoreDataContext dao = new StoreDataContext())
        {
            //var entity = dao.orderDelivery.First(p => p.orderNo == orderNo);
            var entity = www6.selectQFStore<orderDelivery>("orderNo", orderNo);

            entity.deliveryCo = Convert.ToInt32(deliveryCo.SelectedValue);
            if (deliveryNum.Text != "")
            {
                entity.deliveryNum = deliveryNum.Text;
            }

            if (is_deliver)
            {
                //var state_entity = dao.orderMaster.First(p => p.orderNo == orderNo);
                var state_entity = www6.selectQFStore<orderMaster>("orderNo", orderNo);

                state_entity.State = 5;

                www6.updateStore(state_entity);
            }


            //dao.SubmitChanges();
            www6.updateStore(entity);
        }

		GetList(paging.CurrentPage);
	}

	protected void btn_state_save_Click(object sender, EventArgs e){
		if (base.IsRefresh){
			return;
		}

		LinkButton obj = sender as LinkButton;
		var orderNo = obj.CommandName ;


		var deliveryState = (DropDownList)obj.Parent.FindControl( "deliveryState" );
        //Response.Write( deliveryState.SelectedValue);

        using (StoreDataContext dao = new StoreDataContext())
        {
            //var entity = dao.orderMaster.First(p => p.orderNo == orderNo);
            var entity = www6.selectQFStore<orderMaster>("orderNo", orderNo);

            entity.State = Convert.ToInt32(deliveryState.SelectedValue);
            //dao.SubmitChanges();
            www6.updateStore(entity);
        }

	}
}
