﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_store_order_update" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	
	<script type="text/javascript">
		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1" data-toggle="tab">기본정보</a></li>
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1">
				<div class="form-horizontal">

					<div class="col-xs-12">
						<h2 class="page-header"><i class="fa fa-plus-square"></i> 주문자 정보</h2>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">주문번호</label>
						<div class="col-sm-10">
							<span runat=server ID=orderNo class="form-control" style="width:300px;" ></span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">받으시는 분</label>
						<div class="col-sm-10">
							<span runat=server ID=orderName class="form-control" style="width:300px;" ></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">주문하신 날짜</label>
						<div class="col-sm-10">
							<span runat=server ID=dtReg class="form-control" style="width:300px;" ></span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">이메일</label>
						<div class="col-sm-10">
							<span runat=server ID=email class="form-control" style="width:300px;" ></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">배송지 주소</label>
						<div class="col-sm-10">
							<span runat=server ID=addr class="form-control" style="width:500px;height:50px" ></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">휴대폰 번호</label>
						<div class="col-sm-10">
							<span runat=server ID=phone class="form-control" style="width:300px;" ></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">메모</label>
						<div class="col-sm-10">
							<span runat=server ID=memo class="form-control" style="width:300px;" ></span>
						</div>
					</div>

					


					<div class="col-xs-12">
						<h2 class="page-header"><i class="fa fa-plus-square"></i> 주문 내역</h2>
					</div>


					<table class="table table-bordered" style="width:1000px; margin-left:50px;">
						<colgroup>
							<col width="42%" />
							<col width="14%" />
							<col width="14%" />
							<col width="14%" />
							<col width="14%" />
				        </colgroup>
						<thead>
							<tr>
								<th>상품명/선택사항</th>
								<th>판매가</th>
								<th>수량</th>
								<th>상품구분</th>
								<th>구매가격</th>
							</tr>
						</thead>
				        <tbody>
							<asp:Repeater runat="server" ID="repeater" OnItemDataBound=ListBound>
								<ItemTemplate>
									<tr>
										<td class="text-left"><%#Eval("productTitle") %><br />
											옵션 : <%#Eval("optionDetail") %><br />
											<%#Eval("childName").ToString() != "" ? "어린이 번호 : " + Eval("childID") + "<br />" : "" %>
											<%#Eval("childName").ToString() != "" ? "선물받을 어린이 : " + Eval("childName") : "" %>
										</td>
										<td><%#Eval("totalPrice" ,"{0:N0}") %>원</td>
										<td><%#Eval("qty") %></td>
										<td><%#Eval("childName").ToString() == "" ? "일반상품" : "어린이에게 보내는 선물" %></td>
										<td><%#Eval("totalPrice","{0:N0}") %>원</td>
									</tr>
								</ItemTemplate>
							</asp:Repeater>
							<tr>
								<td colspan="5" runat="server" id="totalPrice" class="text-right" style="text-align: right;">
										
								</td>
							</tr>
						</tbody>
					</table>
					
					<div class="col-xs-12">
						<h2 class="page-header"><i class="fa fa-plus-square"></i> 결제 내역</h2>
					</div>

					
					<div class="form-group">
						<label class="col-sm-2 control-label">총 결제금액</label>
						<div class="col-sm-10">
							<span runat=server ID=price class="form-control" style="width:300px;" ></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">결제수단</label>
						<div class="col-sm-10">
							<span runat=server ID=means class="form-control" style="width:300px;" ></span>
						</div>
					</div>
					
					
				</div>
			</div>
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->


	 <div class="box-footer clearfix text-center">
		 <a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>

</asp:Content>