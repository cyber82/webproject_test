﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_recruit_faq_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript">
		
		$(function () {
			initEditor(oEditors, "f_answer");
			image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
		});


	    var onSubmit = function () {

	        oEditors.getById["f_answer"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			if (!validateForm([
				{ id: "#f_question", msg: "제목을 입력하세요" },
				{ id: "#f_order", msg: "정렬 순서를 입력하세요" , type : "numeric" }
			])) {
				return false;
			}

			if ($("#s_type").val() == "") {
				alert("구분을 선택하세요");
				$("#s_type").focus();
				return false;
			}

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}

		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

		
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="LinkButton1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<style type="text/css">
						#b_display label, #f_top label{margin-left:5px;width:50px}
					</style>
					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
                            <asp:CheckBox runat="server" ID="f_display" cssClass="form-control1" Checked="true" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">순서</label>
						<div class="col-sm-3">
                            <div class="input-group">
                                <asp:TextBox runat="server" ID="f_order" CssClass="number_only form-control" Text="10000" Width="600" MaxLength="3"></asp:TextBox> 
                                <div> * 낮은 숫자가 위로</div>
                            </div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">제목</label>
						<div class="col-sm-5">
							<asp:TextBox runat="server" ID="f_question" CssClass="f_question form-control" Width="600" MaxLength="100"></asp:TextBox>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10">
                            <textarea name="content" id="f_answer" runat="server" rows="10" cols="100" style="width:600px; height:412px; display:none;"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	 <div class="box-footer clearfix text-center">
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right ">삭제</asp:LinkButton>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>