﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="admin_recruit_volunteer_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ Register Src="~/@mgt/recruit/volunteer/view.ascx" TagPrefix="uc" TagName="recruit" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        #s_process_status label, #s_result label, #job_result label, aptitude_result label {
            padding-right:10px;
        }

    </style>
	<script type="text/javascript">

	    var onSubmit = function () {
	        if($("#s_recruit").val() == "-1") {
	            alert("채용공고를 선택해주세요.");
	            $("#s_recruit").focus();
	            return false;
	        }


            
	        if($("#s_process_status input[type=radio]:checked").length < 1){
	            alert("평가단계를 선택해주세요.");
	            return false;
	        }

			return confirm("수정하시겠습니까?");
		}

		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

		
		$(function(){
			$(".btn_print").click(function () {
				var ra_id = <%:this.PrimaryKey.ToString()%>;
				window.open("/@mgt/recruit/volunteer/print?r_id=" + ra_id, "printFrm", "width=810px,height=800px,scrollbars=yes");
				return false;
			});
		});

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    
    <div class="row">
        <div class="col-md-12">
            <uc:recruit runat="server" id="recruitCtrl"></uc:recruit>

            <!-- 평가 / 결과 -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>평가단계 이동</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
				        <tbody>
                            <tr>
                                <th>평가자</th>
                                <td><asp:Literal runat="server" ID="a_name"></asp:Literal>(<asp:Literal runat="server" ID="a_email"></asp:Literal>)</td>
                            </tr>
                            <tr>
						        <th rowspan="2">평가단계 이동</th>
						        <td>
                                    <p>채용공고 선택</p>
                                    <asp:DropDownList runat="server" ID="s_recruit" key="s_recruit" class="form-control" style="width:150px;" ></asp:DropDownList>
						        </td>
					        </tr>	
                            <tr>
                                <td>
                                    <asp:RadioButtonList runat="server" ID="s_process_status" RepeatLayout="Flow" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
						        <th>비고</th>
						        <td>
                                    <asp:TextBox runat="server" ID="r_internal_comment" CssClass="form-control" TextMode="MultiLine" Rows="3" Width="450"></asp:TextBox>
						        </td>
					        </tr>	
                        </tbody>
                    </table>
                </div>
            </div><!-- /.box -->
        </div>
    </div>

    
    
	<div class="box-footer clearfix text-center">
        <a runat="server" id="btnList" class="btn btn-default pull-left">목록</a>
        
		<asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" Style="margin-right: 5px">업데이트</asp:LinkButton>
        <a class="btn btn-warning btn_print pull-right" style="margin-right: 5px;"><i class="fa fa-print"></i> 인쇄</a>
	</div>
	
</asp:Content>