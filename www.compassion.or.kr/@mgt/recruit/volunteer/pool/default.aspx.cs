﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
using System.Collections;

public partial class admin_recruit_volunteer_pool_default : AdminBasePage {

	List<recruit_jobcode> Jobcode
	{
		get
		{
			if (this.ViewState["recruit_jobcode"] == null)
            {
                using (AdminDataContext dao = new AdminDataContext())
                {
                    //this.ViewState["recruit_jobcode"] = dao.recruit_jobcode.Where(p => p.rj_display == true).ToJson();
                    var list = www6.selectQ<recruit_jobcode>("rj_display", 1);
                    this.ViewState["recruit_jobcode"] = list.ToJson();
                }
			}
			return this.ViewState["recruit_jobcode"].ToString().ToObject<List<recruit_jobcode>>();
		}
		set
		{
			this.ViewState["recruit_jobcode"] = value.ToJson();
		}
	}

	protected override void OnBeforePostBack()
    {
		base.OnBeforePostBack();

		base.sortColumn = sortColumn;
		base.sortDirection = sortDirection;
		//var lang = Request["s_lang"].ValueIfNull("ko");
		var work = Request["s_work"].EmptyIfNull();

		Master.Content.Controls.BindAllTextBox(this.Context);

        using (AdminDataContext dao = new AdminDataContext())
        {
            var cookie = AdminLoginSession.GetCookie(Context);
            int a_id = cookie.identifier;

            ArrayList arrArjList = new ArrayList();

            //foreach (var arj in dao.admin_recruit_jobcode.Where(a => a.aj_a_id == a_id))
            var list = www6.selectQ<admin_recruit_jobcode>("aj_a_id", a_id);
            foreach (var arj in list)
            {
                arrArjList.Add(arj.aj_rj_depth1);
            }

            //foreach (var a in dao.recruit_jobcode.Where(p => p.rj_display == true && p.rj_depth2 == "").OrderBy(p => p.rj_order))
            var list2 = www6.selectQ<recruit_jobcode>("rj_display", 1, "rj_depth2", "", "rj_order");
            foreach (var a in list2)
            {
                int idx = arrArjList.IndexOf(a.rj_depth1);
                if (idx >= 0 || cookie.type == "super")
                {
                    s_work.Items.Add(new ListItem(a.rj_name, a.rj_depth1));
                }
            }
            s_work.Items.Insert(0, new ListItem("직무선택", ""));
            s_work_sub.Items.Insert(0, new ListItem("세부 직무선택", ""));

            if (work != "")
            {
                s_work.SelectedValue = work;
                s_work_SelectedIndexChanged(null, null);
            }

        }


		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
		btn_set_fail.Visible = btn_send_mail.Visible = auth.aa_auth_update;


	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.sortColumn = sortColumn;
		base.sortDirection = sortDirection;
		base.OnAfterPostBack();

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected override void GetList(int page) {


		Master.IsSearch = false;
		if (
			s_work.SelectedValue != ""
			|| s_work_sub.SelectedValue != ""
			|| s_keyword.Text != "" 
			|| s_b_date.Text != "" 
			|| s_e_date.Text != "") {
			Master.IsSearch = true;
		}


        using (AdminDataContext dao = new AdminDataContext())
        {
            var cookie = AdminLoginSession.GetCookie(Context);

            //var list = dao.sp_pool_list(
            //    page, paging.RowsPerPage, s_keyword.Text.Trim(), s_b_date.Text, s_e_date.Text,
            //    "done", s_work.SelectedValue, s_work_sub.SelectedValue, cookie.type == "super" ? -1 : cookie.identifier).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword", "startdate", "enddate", "r_regist_status", "r_rj_depth1", "r_rj_depth2", "a_id" };
            Object[] op2 = new Object[] {page, paging.RowsPerPage, s_keyword.Text.Trim(), s_b_date.Text, s_e_date.Text, "done", s_work.SelectedValue, s_work_sub.SelectedValue, cookie.type == "super" ? -1 : cookie.identifier };
            var list = www6.selectSP("sp_pool_list", op1, op2).DataTableToList<sp_pool_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();


        }
	}


	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				sp_pool_listResult entity = e.Item.DataItem as sp_pool_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();

				var jobcode = this.Jobcode;
				try {
					((Literal)e.Item.FindControl("work")).Text = string.Format("{0}/{1}",
						jobcode.First(p => p.rj_depth1 == entity.r_rj_depth1 && p.rj_depth2 == "").rj_name,
						jobcode.First(p => p.rj_depth1 == entity.r_rj_depth1 && p.rj_depth2 == entity.r_rj_depth2).rj_name
						);
				} catch { }
			}
		}


		
	}

	protected void btn_set_doc1_Click(object sender, EventArgs e)
    {
		var a_id = AdminLoginSession.GetCookie(this.Context).identifier;
        using (AdminDataContext dao = new AdminDataContext())
        {
            foreach (RepeaterItem i in repeater.Items)
            {
                if (((CheckBox)i.FindControl("check")).Checked)
                {
                    var ra_id = Convert.ToInt32(((HtmlInputHidden)i.FindControl("ra_id")).Value);

                    //var r = dao.resume.First(p => p.r_id == ra_id);
                    var r = www6.selectQF<resume>("r_id", ra_id);

                    if (r.r_process_status == "standby")
                    {
                        r.r_process_status = "doc1";
                        r.r_a_id = a_id;

                        //dao.SubmitChanges();
                        //string wClause = string.Format("r_id = {0}", ra_id);
                        www6.update(r);
                    }
                }
            }
        }

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "적용되었습니다.";
		this.GetList(1);
	}

	protected void s_process_status_SelectedIndexChanged(object sender, EventArgs e) {

		v_admin_auth auth = base.GetPageAuth();
		search(sender, e);
	}

	protected void s_work_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            var cookie = AdminLoginSession.GetCookie(Context);
            int a_id = cookie.identifier;

            ArrayList arrArjList = new ArrayList();

            //foreach (var arj in dao.admin_recruit_jobcode.Where(a => a.aj_a_id == a_id && a.aj_rj_depth1 == s_work.SelectedValue))
            var list = www6.selectQ<admin_recruit_jobcode>("aj_a_id", a_id, "aj_rj_depth1", s_work.SelectedValue);
            foreach (var arj in list)
            {
                arrArjList.Add(arj.aj_rj_depth2);
            }

            s_work_sub.Items.Clear();

            //foreach (var a in dao.recruit_jobcode.Where(p => p.rj_display == true && p.rj_depth1 == s_work.SelectedValue && p.rj_depth2 != "").OrderBy(p => p.rj_order))
            var list2 = www6.selectQ2<recruit_jobcode>("rj_display = ", 1, "rj_depth1 = ", s_work.SelectedValue, "rj_depth2 != ", "", "rj_order");
            foreach (var a in list2)
            {
                int idx = arrArjList.IndexOf(a.rj_depth2);
                if (idx >= 0 || cookie.type == "super")
                {
                    s_work_sub.Items.Add(new ListItem(a.rj_name, a.rj_depth2));
                }
            }
            s_work_sub.Items.Insert(0, new ListItem("세부 직무선택", ""));
        }

		search(sender, e);
	}


	protected void btn_set_fail_Click(object sender, EventArgs e)
    {
		var a_id = AdminLoginSession.GetCookie(this.Context).identifier;
        using (AdminDataContext dao = new AdminDataContext())
        {
            foreach (RepeaterItem i in repeater.Items)
            {
                if (((CheckBox)i.FindControl("check")).Checked)
                {
                    var ra_id = Convert.ToInt32(((HtmlInputHidden)i.FindControl("ra_id")).Value);

                    //var r = dao.resume.First(p => p.r_id == ra_id);
                    var r = www6.selectQF<resume>("r_id", ra_id);

                    if (r.r_process_status == "standby")
                    {
                        r.r_process_status = "fail";
                        r.r_result_date = DateTime.Now;
                        r.r_a_id = a_id;
                        
                        //dao.SubmitChanges();
                        //string wClause = string.Format("r_id = {0}", ra_id);
                        www6.update(r);
                    }
                }
            }
        }

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "적용되었습니다.";
		this.GetList(1);
	}

	protected void s_recruit_status_SelectedIndexChanged(object sender, EventArgs e) {
		GetList(1);
	}
}
