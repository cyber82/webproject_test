﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using CommonLib;

public partial class admin_recruit_volunteer_update : AdminBasePage {

	Uploader.FileGroup file_group = Uploader.FileGroup.recruit;

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		v_admin_auth auth = base.GetPageAuth();

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.AlphaNumeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context, "default.aspx");

		base.PrimaryKey = Request["c"];
		recruitCtrl.PrimaryKey = Request["c"];

		a_name.Text = AdminLoginSession.GetCookie(this.Context).name;
		a_email.Text = AdminLoginSession.GetCookie(this.Context).email;

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var r = dao.resume.First(p => p.r_id == Convert.ToInt32(PrimaryKey));
            var r = www6.selectQF<resume>("r_id", Convert.ToInt32(PrimaryKey));

            // 평가단계
            r_internal_comment.Text = r.r_internal_comment;
            foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "recruit_process_status" && p.cd_key != "standby").OrderBy(p => p.cd_order))
            {
                s_process_status.Items.Add(new ListItem(a.cd_value, a.cd_key));
            }

            s_process_status.SelectedValue = r.r_process_status;



            // 이동할 채용
            //var list = dao.recruit.OrderByDescending(p => p.b_id);
            var list = www6.selectQ<recruit>("b_id");

            s_recruit.Items.Clear();
            foreach (var i in list)
            {
                s_recruit.Items.Add(new ListItem(i.b_title, i.b_id.ToString()));
            }
            s_recruit.Items.Insert(0, new ListItem("선택하세요", "-1"));
        }

		btn_update.Visible = auth.aa_auth_update;



	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";

	}


	protected void btn_update_click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var r = dao.resume.First(p => p.r_id == Convert.ToInt32(PrimaryKey));
            var r = www6.selectQF<resume>("r_id", Convert.ToInt32(PrimaryKey));

            var exist = www6.selectQ<resume>("r_b_id", Convert.ToInt32(s_recruit.SelectedValue), "r_ap_id", r.r_ap_id);
            //if (dao.resume.Any(p => p.r_b_id == Convert.ToInt32(s_recruit.SelectedValue) && p.r_ap_id == r.r_ap_id))
            if (exist.Any())
            {
                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "이미 해당공고에 지원한 지원자 입니다.";
            }
            else
            {
                var entity = r.Clone();

                entity.r_b_id = Convert.ToInt32(s_recruit.SelectedValue);
                entity.r_internal_comment = r_internal_comment.Text;
                entity.r_a_id = r.r_a_id = AdminLoginSession.GetCookie(this.Context).identifier;
                entity.r_process_status = s_process_status.SelectedValue;
                entity.r_regdate = DateTime.Now;
                entity.r_moddate = DateTime.Now;
                entity.r_r_id = Convert.ToInt32(PrimaryKey);

                // 첨부파일 경로 설정
                if (r.r_file.EmptyIfNull() != "") entity.r_file = r.r_file + "?path=" + r.r_id.ToString();
                if (r.r_txt_aboutme_4.EmptyIfNull() != "") entity.r_txt_aboutme_4 = r.r_txt_aboutme_4 + "?path=" + r.r_id.ToString();

                //dao.resume.InsertOnSubmit(entity);
                //www6.insert(entity);
                www6.update(entity);
                //dao.SubmitChanges();


                Master.ValueAction.Value = "list";
                Master.ValueMessage.Value = "수정되었습니다.";
            }


        }


	}

}