﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="admin_recruit_volunteer_pool_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
	    $(function () {
	        $("#checkall").on("ifChanged", function (sender) {
	            var val = $(this).attr("data-val");
	            var checked = $(this).prop("checked");

	            $.each($(".item_check"), function () {
	                $($(this).find("input")).iCheck(checked ? "check" : "uncheck");
	            });

	        })


			$("#btn_excel").click(function () {
				location.href = "excel.ashx?" + buildQuery(false, true);
				return false;
			});
			
			$("#btn_send_mail").click(function () {
				var c = false;

				var list = "";
				$.each($(".item_check"), function () {
					var r_id = $(this).attr("data-id");
					if ($(this).find("input").prop("checked")) {
						c = true;
						list += "," + r_id;
					}
				});

				if (!c) {
					alert("메일 대상자를 한명이상 선택해주세요.");
					return;
				}

				window.open("/@mgt/recruit/volunteer/mail?arg=" + list.substring(1,list.length), "mailFrm", "width=670px,height=690px");
			});

			$(".btn_print").click(function () {
				var r_id = $(this).attr("data-id");
				window.open("/@mgt/recruit/volunteer/print?r_id=" + r_id, "printFrm", "width=810px,height=800px,scrollbars=yes");
				return false;
			});

			$("#btn_print").click(function () {
				var ids = "";
				$.each($(".item_check"), function () {
					if ($(this).find("input").prop("checked")) {
						ids += "," + $(this).attr("data-id");
					}
				});

				if (ids == "") {
					alert("인쇄할 지원자를 체크해주세요.");
					return false;
				}

				window.open("/@mgt/recruit/volunteer/print?r_id=" + ids.substring(1), "printFrm", "width=810px,height=800px,scrollbars=yes");
				return false;
			});


			$("#btn_appraisal").click(function () {
			    var ids = "";
			    $.each($(".item_check"), function () {
			        if ($(this).find("input").prop("checked")) {
			            ids += "," + $(this).attr("data-id");
			        }
			    });

			    if (ids == "") {
			        alert("평가처리할 지원자를 체크해주세요.");
			        return false;
			    }

			    window.open("/@mgt/recruit/volunteer/appraisal?id=" + ids.substring(1), "appraisalFrm", "width=500px,height=260px,scrollbars=yes");
			    return false;
			});
			
		});

		var onPass = function () {
			return confirm("인사 서류전형에 합격시키겠습니까?");
		}

		var onFail = function () {
			if ($("#r_result").val() != "standby") {
				alert("합격여부가 미분류 단계에서만 불합격처리가 가능합니다");
				return false;
			}
			return confirm("불합격 시키겠습니까?");
		}
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

<input type=hidden runat=server id=sortColumn value="ra_id" />
<input type=hidden runat=server id=sortDirection value="desc" />
<input type=hidden runat=server id=r_result value="" />

    <div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>
		<div class="form-horizontal box-body" >
			<div class="box-body">

                
                <div class="form-group">
					<label for="s_main" class="col-sm-2 control-label">모집부문</label>
					<div class="col-sm-10">
					    <asp:DropDownList runat="server" ID="s_work" key="s_work" class="form-control" style="width:150px;float:left;" AutoPostBack="true" OnSelectedIndexChanged="s_work_SelectedIndexChanged"></asp:DropDownList>
					    <asp:DropDownList runat="server" ID="s_work_sub" key="s_work_sub" class="form-control" style="width:150px;float:left;margin-left:5px;" AutoPostBack="true" OnSelectedIndexChanged="search"></asp:DropDownList>
					</div>
				</div>

				<div class="form-group">
					<label for="s_keyword" class="col-sm-2 control-label">검색</label>
					<div class="col-sm-10">
                        <asp:TextBox runat="server" ID="s_keyword" key="k"  Width="415" cssClass="form-control inline" OnTextChanged="search" AutoPostBack="true"></asp:TextBox>
					    * 이름 , 이메일
					</div>
				</div>



				<div class="form-group">
					<label for="s_b_date" class="col-sm-2 control-label">기간 검색(수정일)</label>
					<div class="col-sm-10">
						<div class="inline">
							<asp:TextBox runat="server" ID="s_b_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
							~
						    <asp:TextBox runat="server" ID="s_e_date" Width="100px" CssClass="date form-control inline" Style="margin-right: 5px;"></asp:TextBox>
						</div>

						<div class="inline">
							<button class="btn btn-primary btn-sm daterange" data-from="s_b_date" data-end="s_e_date"><i class="fa fa-calendar"></i></button>

							<div class="btn-group">
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="0">오늘</a>
								<a href="#" class="btn  btn-default dateRange" data-day="7" data-month="0">1주</a>
								<a href="#" class="btn  btn-default dateRange" data-day="15" data-month="0">15일</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="1">1개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="2">2개월</a>
								<a href="#" class="btn  btn-default dateRange" data-day="0" data-month="3">3개월</a>
							</div>
						</div>

					</div>

				</div>

				<div class="box-footer clearfix text-center">
                    <asp:LinkButton runat="server" ID="btn_search" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
				</div>
				
			</div>
			
		</div>
	</div><!-- /.box -->




    <div class="box box-primary">
		
		 <div class="box-header">
            <h3 class="box-title">총 <asp:Literal runat=server ID=lbTotal/> 건</h3>
            
        </div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
		
			<table class="table table-hover table-bordered ">
				<colgroup>
				    <col width="2%" />
				    <col width="5%" />
				    <col width="10%" />
				    <col width="5%" />
				    <col width="10%" />
				    <col width="10%" />
				    <col width="7%" />
				    <col width="10%" />
				    <col width="10%" />
				    <col width="10%" />
				    <col width="3%" />
				</colgroup>
				<thead>
					<tr>
				        <th><input type="checkbox" id="checkall" /></th>
				        <th>번호</th>
				        <th>모집부문</th>
				        <th><asp:LinkButton runat=server ID=LinkButton3 OnClick="sort" CommandArgument="r_name" Text="이름" ></asp:LinkButton></th>
				        <th>이메일</th>
				        <th><asp:LinkButton runat=server ID=LinkButton7 OnClick="sort" CommandArgument="r_education" Text="최종학력" ></asp:LinkButton></th>
				        <th><asp:LinkButton runat=server ID=LinkButton1 OnClick="sort" CommandArgument="r_moddate" Text="마지막수정일" ></asp:LinkButton></th>
				        <th>인쇄</th>
					</tr>
				</thead>
				<tbody>
			        <asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
				        <ItemTemplate>
					        <tr class="tr_over">
						        <input type="hidden" runat="server" id="ra_id" value=<%#Eval("r_id")%> />
						        <td><asp:CheckBox runat="server" ID="check" data-id=<%#Eval("r_id")%> CssClass="item_check" /></td>
						        <td><asp:Literal runat=server ID=lbIdx></asp:Literal></td>
						        <td><asp:Literal runat="server" ID="work"></asp:Literal></td>
						        <td><a href="#" onclick="goPage('update.aspx' , { c: <%#Eval("r_id")%>, p: '<%#paging.CurrentPage %>', t: 'update' });return false;" style="text-decoration:underline"><%#Eval("r_name")%></a></td>
						        <td><%#Eval("r_email")%></td>
						        <td><%#Eval("r_education")%></td>
						        <td><%#Eval("r_moddate" , "{0:yyyy/MM/dd}")%></td>
						        <td><a class="btn btn-default btn-sm btn_print" data-id=<%#Eval("r_id")%>><i class="fa fa-print"></i> 인쇄</a></td>
					        </tr>	
				        </ItemTemplate>
						 <FooterTemplate>
							 <tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								 <td colspan="8">데이터가 없습니다.</td>
							 </tr>
						</FooterTemplate>
			        </asp:Repeater>			


				</tbody>
			</table>
		</div>
        
		<div class="box-footer clearfix text-center">
            <uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
        </div>

        <div class="box-footer clearfix text-center">

            <div class="pull-left" style="display:none;">
                <asp:LinkButton runat="server" ID="btn_set_fail" Visible="false" OnClick="btn_set_fail_Click" OnClientClick="return onFail();" CssClass="btn btn-danger">불합격</asp:LinkButton>
            </div>

            <div class="pull-right">
                <a class="btn btn-primary" id="btn_excel" style="display:none">엑셀다운로드</a>
			    <a class="btn btn-warning" id="btn_print" style="display:none">인쇄</a>
			    <a class="btn btn-default" id="btn_send_mail" runat="server">메일발송</a>
			</div>
        </div>


	</div>

</asp:Content>