﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using CommonLib;

public partial class admin_recruit_volunteer_mail : AdminBasePage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		v_admin_auth auth = base.GetPageAuth();
		mail_from.Text = ConfigurationManager.AppSettings["recruitEmailSender"];
		btn_update.Visible = auth.aa_auth_update;

		var r_ids = Request["arg"];

        using (AdminDataContext dao = new AdminDataContext())
        {
            //string query = string.Format("select * from [resume] where r_id in ({0}) order by r_name ", r_ids);
            //var list = dao.ExecuteQuery<resume>(query);
            var list = www6.selectQ2<resume>("r_id IN ", utils.include(r_ids), "r_name");

            var names = "";
            var emails = "";
            foreach (var i in list)
            {
                names += "," + i.r_name;
                emails += "," + i.r_email;
            }

            mail_to_name.Text = names.Substring(1);
            mail_to.Value = emails.Substring(1);

        }
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();


	}



	protected void btn_update_click(object sender, EventArgs e) {

		this.SendMail();

		action.Value = "send";
	}


	void SendMail() {

		try {

			//var names = mail_to_name.Text.Split(',');
			var emails = mail_to.Value.Split(',');

			for (int i = 0; i < emails.Length; i++) {
				var to = emails[i];
				//var name = names[i];
				if (string.IsNullOrEmpty(to))
					return;

				/*
				var args = new List<KeyValuePair<string, string>>() {
					new KeyValuePair<string,string>("{u_name}" , name ) , 
					new KeyValuePair<string,string>("{body}" , b_content.InnerHtml.ToHtml() ) , 
				};
				*/

				var args = new Dictionary<string, string> {
					{"{body}" , b_content.InnerHtml.ToHtml()  }
				};

				// to user
				Email.Send(this.Context, mail_from.Text, new List<string>() { to },
					mail_title.Text, "/mail/recruit_notice.html",
					args
				, null);
			}

		} catch (Exception e) {
			throw e;
		}

	}

}