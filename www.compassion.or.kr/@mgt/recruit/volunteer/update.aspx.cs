﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using CommonLib;

public partial class admin_recruit_volunteer_update : AdminBasePage {

	Uploader.FileGroup file_group = Uploader.FileGroup.recruit;

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		v_admin_auth auth = base.GetPageAuth();

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.AlphaNumeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context, "default.aspx");

		base.PrimaryKey = Request["c"];
		recruitCtrl.PrimaryKey = Request["c"];

        using (AdminDataContext dao = new AdminDataContext())
        {
            string sqlStr = string.Empty;

            //var r = dao.resume.First(p => p.r_id == Convert.ToInt32(PrimaryKey));
            var r = www6.selectQF<resume>("r_id", Convert.ToInt32(PrimaryKey));

            // 결과
            r_internal_comment.Text = r.r_internal_comment;
            foreach (var a in StaticData.Code.GetList(this.Context, true).Where(p => p.cd_display == true && p.cd_group == "recruit_process_status").OrderBy(p => p.cd_order))
            {
                s_process_status.Items.Add(new ListItem(a.cd_value, a.cd_key));
            }

            if (r.r_result_date.HasValue) r_result_date.Text = r.r_result_date.Value.ToString("yyyy.MM.dd HH:mm");

            s_process_status.SelectedValue = r.r_process_status;
            s_result.SelectedValue = r.r_result;
            default_result.Value = r.r_result;

            //자동 메일 발송 해제
            r_is_freeze_email.Checked = r.r_is_freeze_email == null ? false : (bool)r.r_is_freeze_email;
            r_is_freeze_email.Enabled = auth.aa_auth_create || auth.aa_auth_update;


            // 직무테스트 결과
            var exist = www6.selectQ<resume_process>("rp_r_id", Convert.ToInt32(PrimaryKey), "rp_process_status", "job", "rp_a_id", AdminLoginSession.GetCookie(Context).identifier);

            //if (dao.resume_process.Any(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "job" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier))
            if(exist.Any())
            {
                //var resume_process = dao.resume_process.First(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "job" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier);
                var resume_process = exist[0];

                job_result.SelectedValue = resume_process.rp_result;
            }

            // 인성심리검사 결과
            var exist2 = www6.selectQ<resume_process>("rp_r_id", Convert.ToInt32(PrimaryKey), "rp_process_status", "aptitude", "rp_a_id", AdminLoginSession.GetCookie(Context).identifier);

            //if (dao.resume_process.Any(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "aptitude" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier))
            if (exist2.Any())
            {
                //var resume_process = dao.resume_process.First(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "aptitude" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier);
                var resume_process = exist2[0];

                aptitude_result.SelectedValue = resume_process.rp_result;
                aptitude_comment.Value = resume_process.rp_comment;
            }


            // 인적성 파일
            var exist3 = www6.selectQ<file>("f_group", file_group.ToString(), "f_group2", Recruit.FileGroup2.aptitude.ToString(), "f_ref_id", r.r_id.ToString());
            //if (dao.file.Any(p => p.f_group == file_group.ToString() && p.f_group2 == Recruit.FileGroup2.aptitude.ToString() && p.f_ref_id == r.r_id.ToString()))
            if (exist3.Any())
            {
                //var f = dao.file.First(p => p.f_group == file_group.ToString() && p.f_group2 == Recruit.FileGroup2.aptitude.ToString() && p.f_ref_id == r.r_id.ToString());
                var f = exist3[0];

                r_aptitude_file.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, r.r_id.ToString()), f.f_name, r.r_id, f.f_name);
            }

            // 직무테스트 파일
            var exist4 = www6.selectQ<file>("f_group", file_group.ToString(), "f_group2", Recruit.FileGroup2.job.ToString(), "f_ref_id", r.r_id.ToString());
            //if (dao.file.Any(p => p.f_group == file_group.ToString() && p.f_group2 == Recruit.FileGroup2.job.ToString() && p.f_ref_id == r.r_id.ToString()))
            if (exist4.Any())
            {
                //var f = dao.file.First(p => p.f_group == file_group.ToString() && p.f_group2 == Recruit.FileGroup2.job.ToString() && p.f_ref_id == r.r_id.ToString());
                var f = exist4[0];
                r_job_file.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, r.r_b_id.ToString()), f.f_name, r.r_id, f.f_name);
            }

            //repeater_process.DataSource = dao.resume_process.Where(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status != "aptitude" && p.rp_process_status != "job").OrderByDescending(p => p.rp_regdate);
            repeater_process.DataSource = www6.selectQ2<resume_process>("rp_r_id = ", Convert.ToInt32(PrimaryKey), "rp_process_status !=", "aptitude", "rp_process_status != ", "job", "rp_regdate desc");

            repeater_process.DataBind();
        }

		btn_update.Visible = auth.aa_auth_update;


		//ShowData();

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";

	}


	protected void btn_update_click(object sender, EventArgs e)
    {
		string filename = null;
		long filesize = -1;

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var r = dao.resume.First(p => p.r_id == Convert.ToInt32(PrimaryKey));
            var r = www6.selectQF<resume>("r_id", Convert.ToInt32(PrimaryKey));

            r.r_internal_comment = r_internal_comment.Text;
            r.r_a_id = r.r_a_id = AdminLoginSession.GetCookie(this.Context).identifier;

            bool isFreezeEmail = r_is_freeze_email.Checked;
            r.r_is_freeze_email = isFreezeEmail;

            if (!isFreezeEmail)
            {
                var sentMail = false;

                //s_result : standby(진행중), pass(최종합격), fail(불합격)
                //s_process_status : doc1(서류심사), interview1(현업인터뷰), doc2(통합역량검사), interview2(HR인터뷰), interview3(경영진인터뷰)

                string bTitle = "";
                if (r.r_b_id != null)
                {
                    //bTitle = dao.recruit.First(p => p.b_id == r.r_b_id).b_title;
                    bTitle = www6.selectQF<recruit>("b_id", r.r_b_id).b_title;
                }

                string mailContens = "";

                if (s_process_status.SelectedValue.Equals("doc1") && s_result.SelectedValue.Equals("standby"))
                {
                    mailContens += "한국컴패션 채용에 관심을 갖고 지원해 주셔서 감사 드립니다.<br/>";
                    mailContens += "현재 지원해주신 서류를 검토 중에 있습니다.<br/>";
                    mailContens += "서류전형 결과는 이메일로 전달되며 이메일 발송에 대해 문자 안내가 있을 예정입니다.<br/>";
                    mailContens += "서류 전형 결과는 <span style=\"color:#333;font-weight:bold;font-family:Dotum;\">\'한국컴패션 홈페이지 > 인재채용 > 지원결과 조회\' 에서도 확인하실 수 있습니다.</span>";
                }
                else if (s_process_status.SelectedValue.Equals("doc1") && s_result.SelectedValue.Equals("fail"))
                {
                    mailContens += "이번 " + bTitle + " 채용 전형에 귀한 시간을 할애하여 지원해 주셔서 감사 드립니다.<br/><br/>";
                    mailContens += "컴패션의 어린이 사역에 대한 열정을 지닌 지원자 모두와 귀한 만남을 지속하고 싶지만, 아쉽게도 이번 전형에서는 함께 할 수 없음을 알려드리게 되었습니다.<br/><br/>";
                    mailContens += "이번에는 함께 하지 못하게 되었지만, 컴패션 사역과 가난 가운데 있는 어린이들을 기도로 응원하고 지원해 주시길 부탁 드립니다. 어느 곳에 계시던지 주님과 동행하는 은혜가 가득하길 소망합니다.";
                }
                else if (s_process_status.SelectedValue.Equals("interview1") && s_result.SelectedValue.Equals("standby"))
                {
                    mailContens += r.r_name + "님께서는 " + bTitle + " 채용 서류 전형에 합격 하셨습니다.<br/>";
                    mailContens += "서류 전형에 합격하심에 따라, 인터뷰를 통해 " + r.r_name + "님의 컴패션 사역에 대한 열정과 관심, 경험 및 역량 등에 대해 알아 가는 시간을 갖고자 합니다.<br/>";
                    mailContens += "인터뷰 일정 및 장소 안내는 유선으로 별도 연락 드리겠습니다.";
                }
                else if ((s_process_status.SelectedValue.Equals("interview1") || s_process_status.SelectedValue.Equals("interview2") || s_process_status.SelectedValue.Equals("interview3")) && s_result.SelectedValue.Equals("fail"))
                {
                    mailContens += "이번 " + bTitle + " 채용 인터뷰에 귀한 시간을 할애하여 주셔서 감사합니다.<br/>";
                    mailContens += "컴패션의 어린이 사역에 대한 열정과 역량을 보여주신 지원자 모두와 귀한 만남을 지속하고 싶지만, 아쉽게도 이번 전형에서는 함께 할 수 없음을 알려드리게 되었습니다.<br/><br/>";
                    mailContens += "이번에 함께 하지 못하게 되었지만, 컴패션 사역과 가난 가운데 있는 어린이들을 기도로 응원하고 지원해 주시길 부탁 드립니다. 어느 곳에 계시던지 주님과 동행하는 은혜가 가득하길 소망합니다.";
                }
                else if ((s_process_status.SelectedValue.Equals("interview1") || s_process_status.SelectedValue.Equals("interview2") || s_process_status.SelectedValue.Equals("interview3")) && s_result.SelectedValue.Equals("pass"))
                {
                    mailContens += "이번 " + bTitle + " 채용 인터뷰에 귀한 시간을 할애하여 주셔서 감사합니다.<br/>";
                    mailContens += r.r_name + "님께서는 모든 채용 전형에서 최종 합격하셨습니다. <br />";
                    mailContens += "컴패션의 어린이 사역에 대한 열정과 역량을 보여주신 " + r.r_name + "님과의 귀한 만남을 지속하게 되어 기쁘게 생각합니다.<br/>";
                    mailContens += "다음 과정에 대한 자세한 안내는 유선으로 연락 드리겠습니다.<br/>";
                    mailContens += "궁금하신 사항은 언제든지 편하게 문의해 주세요. (<a href=\"mailto: recruit @compassion.or.kr\">recruit@compassion.or.kr</a>)";
                }
                else if ((s_process_status.SelectedValue.Equals("interview2") || s_process_status.SelectedValue.Equals("interview3")) && s_result.SelectedValue.Equals("standby"))
                {
                    mailContens += "이번 " + bTitle + " 채용 인터뷰에 귀한 시간을 할애하여 주셔서 감사합니다.<br/>";
                    mailContens += r.r_name + "님께서는 해당 인터뷰 전형에 합격하셨습니다.<br/>";
                    mailContens += "다음 전형 관련한 자세한 안내는 유선으로 연락 드리도록 하겠습니다.";
                }
                else
                {
                    mailContens += "한국컴패션 인사총무팀입니다.<br/>";
                    mailContens += "지원하신 채용 전형 결과는 <span style = \"color:#333;font-weight:bold;font-family:Dotum;\"> \'한국컴패션 홈페이지 > 인재채용 > 지원결과 조회\' </span> 에서 <br/>";
                    mailContens += "확인하실 수 있습니다.";
                }

                // 최종합격여부 변경시 메일 발송
                //if (r.r_result != s_result.SelectedValue && s_result.SelectedValue != "standby")
                if (r.r_result != s_result.SelectedValue)
                {
                    SendMail(r.r_email, r.r_name, mailContens, bTitle);
                    sentMail = true;
                }

                // 평가단계 변경시 메일 발송
                if (!sentMail && r.r_process_status != s_process_status.SelectedValue && s_process_status.SelectedValue != "doc1" && s_process_status.SelectedValue != "doc2")
                {
                    SendMail(r.r_email, r.r_name, mailContens, bTitle);
                }
            }


            r.r_process_status = s_process_status.SelectedValue;

            r.r_result = s_result.SelectedValue;
            r.r_result_date = DateTime.Now;




            // 직무 결과 저장
            if (job_result.SelectedValue != "")
            {
                var exist = www6.selectQ<resume_process>("rp_r_id", Convert.ToInt32(PrimaryKey), "rp_process_status", "job", "rp_a_id", AdminLoginSession.GetCookie(Context).identifier);
                //if (dao.resume_process.Any(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "job" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier))
                if (exist.Any())
                {
                    //dao.resume_process.DeleteAllOnSubmit(dao.resume_process.Where(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "job" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier));
                    www6.delete(exist);
                }

                var rp = new resume_process();
                rp.rp_comment = "직무테스트 합격여부";
                rp.rp_r_id = Convert.ToInt32(PrimaryKey);
                rp.rp_a_id = AdminLoginSession.GetCookie(this.Context).identifier;
                rp.rp_regdate = DateTime.Now;
                rp.rp_result = job_result.SelectedValue;
                rp.rp_process_status = "job";

                //dao.resume_process.InsertOnSubmit(rp);
                //www6.cud(MakeSQL.insertQ2(rp));
                www6.insert(rp);
                //dao.SubmitChanges();

            }

            // 인성심리검사 결과 저장

            // 결과 저장
            if (aptitude_result.SelectedValue != "")
            {
                var exist = www6.selectQ<resume_process>("rp_r_id", Convert.ToInt32(PrimaryKey), "rp_process_status", "aptitude", "rp_a_id", AdminLoginSession.GetCookie(Context).identifier);
                //if (dao.resume_process.Any(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "aptitude" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier))
                if (exist.Any())
                {
                    //dao.resume_process.DeleteAllOnSubmit(dao.resume_process.Where(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "aptitude" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier));
                    www6.delete(exist);
                }

                var rp = new resume_process();
                rp.rp_comment = aptitude_comment.Value;
                rp.rp_r_id = Convert.ToInt32(PrimaryKey);
                rp.rp_a_id = AdminLoginSession.GetCookie(this.Context).identifier;
                rp.rp_regdate = DateTime.Now;
                rp.rp_result = aptitude_result.SelectedValue;
                rp.rp_process_status = "aptitude";

                //dao.resume_process.InsertOnSubmit(rp);
                //www6.cud(MakeSQL.insertQ2(rp));
                www6.insert(rp);
                //dao.SubmitChanges();

            }
            // 파일 저장
            string file_root = Uploader.GetRoot(file_group, PrimaryKey.ToString());

            // 통합역량(구)인적성) 검사 파일
            if (s_aptitude_file.HasFile)
            {
                filesize = s_aptitude_file.FileContent.Length / 1024;   // KB
                //filename = string.Format("{0}_인적성_{1}{2}", r.r_name, DateTime.Now.ToString("yyyyMMddHHmmss"), Path.GetExtension(s_aptitude_file.FileName));
                filename = string.Format("{0}_통합역량_{1}{2}", r.r_name, DateTime.Now.ToString("yyyyMMddHHmmss"), Path.GetExtension(s_aptitude_file.FileName));
                filename = filename.GetUniqueName(this.Context, file_root);
                s_aptitude_file.PostedFile.SaveAs(this.Context, file_root, filename);


                //dao.file.DeleteAllOnSubmit(dao.file.Where(p => p.f_ref_id == r.r_id.ToString() && p.f_group == file_group.ToString() && p.f_group2 == Recruit.FileGroup2.aptitude.ToString()));
                var fiList = www6.selectQ<file>("f_ref_id", r.r_id.ToString(), "f_group", file_group.ToString(), "f_group2", Recruit.FileGroup2.aptitude.ToString());
                www6.delete(fiList);

                var fi = new file()
                {
                    f_display_name = filename,
                    f_group = file_group.ToString(),
                    f_group2 = Recruit.FileGroup2.aptitude.ToString(),
                    f_ref_id = r.r_id.ToString(),
                    f_name = filename,
                    f_order = 0,
                    f_regdate = DateTime.Now,
                    f_size = Convert.ToInt32(filesize)
                };

                //dao.file.InsertOnSubmit(fi);
                www6.insert(fi);

            }

            // 직무테스트파일
            if (s_job_file.HasFile)
            {
                filesize = s_job_file.FileContent.Length / 1024;   // KB
                filename = string.Format("{0}_직무테스트_{1}{2}", r.r_name, DateTime.Now.ToString("yyyyMMddHHmmss"), Path.GetExtension(s_job_file.FileName));
                filename = filename.GetUniqueName(this.Context, file_root);
                s_job_file.PostedFile.SaveAs(this.Context, file_root, filename);


                //dao.file.DeleteAllOnSubmit(dao.file.Where(p => p.f_ref_id == r.r_id.ToString() && p.f_group == file_group.ToString() && p.f_group2 == Recruit.FileGroup2.job.ToString()));
                var fiList = www6.selectQ<file>("f_ref_id", r.r_id.ToString(), "f_group", file_group.ToString(), "f_group2", Recruit.FileGroup2.job.ToString());
                www6.delete(fiList);

                var fi = new file()
                {
                    f_display_name = filename,
                    f_group = file_group.ToString(),
                    f_group2 = Recruit.FileGroup2.job.ToString(),
                    f_ref_id = r.r_id.ToString(),
                    f_name = filename,
                    f_order = 0,
                    f_regdate = DateTime.Now,
                    f_size = Convert.ToInt32(filesize)
                };

                //dao.file.InsertOnSubmit();
                www6.insert(fi);

            }
            //dao.SubmitChanges();
            www6.update(r);
        }

		Master.ValueAction.Value = Request["mode"] == "pop" ? "close" : "list";
		Master.ValueMessage.Value = "수정되었습니다.";

	}

	protected void repeater_process_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
		if (e.Item.ItemType == ListItemType.Footer)
			return;
        
		resume_process entity = e.Item.DataItem as resume_process;

		var process_txt = "";
		switch (entity.rp_process_status) {
			case "doc1": process_txt = "서류심사"; break;
			case "doc2": process_txt = "인성심리검사"; break;
			case "interview1": process_txt = "현업인터뷰"; break;
			case "interview2": process_txt = "HR인터뷰"; break;
			case "interview3": process_txt = "경영진인터뷰"; break;
		}

		((Literal)e.Item.FindControl("process")).Text = process_txt;
		((Literal)e.Item.FindControl("rp_result")).Text = entity.rp_result == "pass" ? "합격" : (entity.rp_result == "fail" ? "불합격" : entity.rp_result);


        using (AdminDataContext dao = new AdminDataContext())
        {
            try
            {
                //((Literal)e.Item.FindControl("a_name")).Text = dao.admin.First(p => p.a_id == entity.rp_a_id).a_name;
                var item = www6.selectQF<CommonLib.admin>("a_id", entity.rp_a_id);
                ((Literal)e.Item.FindControl("a_name")).Text = item.a_name;
            }
            catch
            {
            }
        }
	}

	void SendMail(string to, string name, string mailContens, string bTitle) {

		try {

			var from = ConfigurationManager.AppSettings["recruitEmailSender"];

			var args = new Dictionary<string, string> {
				{ "{name}" , name },
                { "{mailContens}", mailContens },
                { "{title}", "지원하신 전형의 결과가 발표되었습니다." }
            };
            //var title = "[한국컴패션_인재채용] " + name + "님, 지원하신 전형의 결과가 발표되었습니다.";
            var title = "[한국컴패션] " + name + "님, 지원하신 전형의 결과가 발표되었습니다.";



            Email.Send(this.Context, from, new List<string>() { to },
				title, "/mail/recruit_result.html",
				args
			, null);

		} catch (Exception e) {
			ErrorLog.Write(this.Context, 0, e.Message);
			throw e;
		}

	}

}