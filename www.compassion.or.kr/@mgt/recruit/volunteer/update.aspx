﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="admin_recruit_volunteer_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ Register Src="~/@mgt/recruit/volunteer/view.ascx" TagPrefix="uc" TagName="recruit" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
    <style>
        #s_process_status label, #s_result label, #job_result label, aptitude_result label {
            padding-right:10px;
        }

    </style>
	<script type="text/javascript">

		var onSubmit = function () {

			if (!validateForm([
				
			])) {
				return false;
			}

			var confirm_msg = "수정하시겠습니까";

			if($("#default_result").val() != $("#s_result input[type=radio]:checked").val()){
			    var result = $("#s_result input[type=radio]:checked").val();
			    if(result == "pass"){
			        confirm_msg = "최종합격 시키시겠습니까?";
			    }else if(result == "fail"){
			        confirm_msg = "불합격 시키시겠습니까?";
			    }
			}

			return confirm(confirm_msg);

		}

		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

		
		$(function(){


			if ($("#action").val() == "close"){
				if ($("#msg").val() != ""){
					alert($("#msg").val());
				}
				opener.location.reload();
				window.close();
			}

			$(".btn_print").click(function () {
				var ra_id = <%:this.PrimaryKey.ToString()%>;
				window.open("/@mgt/recruit/volunteer/print?r_id=" + ra_id, "printFrm", "width=810px,height=800px,scrollbars=yes");
				return false;
			});
		});

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
    
    <div class="row">
        <div class="col-md-12">

            <uc:recruit runat="server" id="recruitCtrl"></uc:recruit>



            <!-- 평가 / 결과 -->
            <div class="box" ㄴ>
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>평가 / 결과</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
				        <colgroup>
				        </colgroup>
				        <tbody>
					        <tr>
						        <th>수정일시</th>
						        <td>
                                    <asp:Literal runat="server" ID="r_result_date" />
						        </td>
                            </tr>
					        <tr>
						        <th>직무테스트</th>
                                <td>
                                    <asp:RadioButtonList runat="server" ID="job_result" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="합격" Value="합격"></asp:ListItem>
                                        <asp:ListItem Text="불합격" Value="불합격"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
						        <td style="display:none;">
                                    <asp:Literal runat="server" ID="r_job_file" /><asp:FileUpload runat="server" ID="s_job_file" />
						        </td>
                            </tr>
					        <tr>
						        <th>통합역량검사</th>
                                <td>
                                    <asp:RadioButtonList runat="server" ID="aptitude_result" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="채용 결격 사유 없음" Value="채용 결격 사유 없음"></asp:ListItem>
                                        <asp:ListItem Text="전문가 의견 있음" Value="전문가 의견 있음"></asp:ListItem>
                                        <asp:ListItem Text="채용 결격 사유 있음" Value="채용 결격 사유 있음"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <input type="text" class="form-control" runat="server" id="aptitude_comment" style="margin-top:2px;"/>
                                    <asp:Literal runat="server" ID="r_aptitude_file"></asp:Literal><asp:FileUpload runat="server" ID="s_aptitude_file" />
                                </td>
						        <td style="display:none;">
                                    
						        </td>
                            </tr>
                            <tr>
						        <th>평가단계</th>
						        <td>
                                    <asp:RadioButtonList runat="server" ID="s_process_status" RepeatLayout="Flow" RepeatDirection="Horizontal"></asp:RadioButtonList>
						        </td>
					        </tr>	
                                            
					        <tr>
						        <th>최종합격여부</th>
						        <td>
                                    <input type="hidden" runat="server" id="default_result" />
                                    <asp:RadioButtonList runat="server" ID="s_result" RepeatLayout="Flow" RepeatDirection="Horizontal">
							            <asp:ListItem Text="진행중" Value="standby"></asp:ListItem>
							            <asp:ListItem Text="최종합격" Value="pass"></asp:ListItem>
							            <asp:ListItem Text="불합격" Value="fail"></asp:ListItem>
						            </asp:RadioButtonList>
						        </td>
                            </tr>
                            <tr>
						        <th>비고</th>
						        <td style="width:1000px;">
                                    <asp:TextBox runat="server" ID="r_internal_comment" CssClass="form-control" TextMode="MultiLine" Rows="3" Width="450"></asp:TextBox>
						        </td>
					        </tr>	

                            <tr>
						        <th>자동 메일 발송 해제</th>
						        <td>
                                    <asp:CheckBox runat="server" ID="r_is_freeze_email" CssClass="form-control" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div><!-- /.box -->
        </div>
    </div>

    
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>평가 / 결과</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
				        <colgroup>
				        </colgroup>
				        <tbody>
					        <asp:Repeater runat=server ID=repeater_process OnItemDataBound="repeater_process_ItemDataBound">
					            <ItemTemplate>
							        <tr>
                                        <th rowspan="2"><asp:Literal runat="server" ID="process"></asp:Literal></th>
                                        <th>면접관</th>
								        <td>
                                            <asp:Literal runat="server" ID="a_name"></asp:Literal>
								        </td>
								        <th>심사일</th>
								        <td>
                                            <%#Eval("rp_regdate" , "{0:yyyy/MM/dd HH:mm}")%>
								        </td>
							        </tr>	

							        <tr>
								        <th>결과/점수</th>
								        <td>
                                            <asp:Literal runat="server" ID="rp_result"></asp:Literal>
								        </td>
								        <th>비고</th>
								        <td>
                                            <%#Eval("rp_comment").ToString().ToHtml()%>
								        </td>
							        </tr>	
                                </ItemTemplate>
                                            
                                <FooterTemplate>
							        <tr runat="server" Visible="<%#repeater_process.Items.Count == 0 %>">
                                        <td colspan="5" style="text-align:center;">심사 결과가 없습니다.</td>
                                    </tr>
                                </FooterTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div><!-- /.box -->
        </div>
    </div>

    
	<div class="box-footer clearfix text-center">
        <a runat="server" id="btnList" class="btn btn-default pull-left">목록</a>
        
		<asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" Style="margin-right: 5px">등록</asp:LinkButton>
        <a class="btn btn-warning btn_print pull-right" style="margin-right: 5px;"><i class="fa fa-print"></i> 인쇄</a>
	</div>
	
</asp:Content>