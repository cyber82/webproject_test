﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="print.aspx.cs" Inherits="admin_recruit_volunteer_print" ValidateRequest="false"%>
<%@ Register Src="~/@mgt/recruit/volunteer/view.ascx" TagPrefix="uc" TagName="recruit" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">

<head id="Head1" runat="server">
    <title>compassion Admin</title>
    
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/@mgt/template/bootstrap/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="/@mgt/template/dist/css/AdminLTE.min.css" />
    <!-- iCheck -->
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/square/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/minimal/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/flat/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/dist/css/skins/skin-green.min.css" />
	<!-- Date Picker -->
    <link rel="stylesheet" href="/@mgt/template/plugins/datepicker/datepicker3.css" />
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/@mgt/template/plugins/daterangepicker/daterangepicker-bs3.css" /><link rel="stylesheet" href="/@mgt/common/css/admin.css" /><link rel="stylesheet" href="/@mgt/common/js/bootstrap-switch/bootstrap-switch.min.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- jQuery 2.1.4 -->
    <script src="/@mgt/template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!-- Bootstrap 3.3.5 -->
    <script src="/@mgt/template/bootstrap/js/bootstrap.min.js"></script>
	<script src="/@mgt/common/js/bootstrap-switch/bootstrap-switch.min.js"></script>

    <!-- iCheck -->
    <script src="/@mgt/template/plugins/iCheck/icheck.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="/@mgt/template/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->

    <script src="/@mgt/template/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="/@mgt/template/plugins/datepicker/locales/bootstrap-datepicker.kr.js"></script>

	<script type="text/javascript" src="/@mgt/common/js/common.js" defer="defer"></script>
	<script type="text/javascript" src="/@mgt/common/js/message.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/jquery.json-2.4.min.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/jquery.json-3.3.2.min.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/form.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/cookie.js" defer="defer"></script>
	
	<script type="text/javascript" src="/assets/jquery/wisekit/function.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.search.js" defer="defer"></script>
	<script type="text/javascript" src="/@mgt/common/js/ajaxupload.3.6.wisekit.js" defer="defer"></script> 
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.bootstrap.js"></script>

	<script type="text/javascript">

		var margins = [];
		$(function () {

			$.each($("body").find(".no-print"), function () {
				$(this).css("display", "none");

			})

			$(".view_item").each(function (i) {

				var pages = Math.ceil($(this).height() / $("body").height());
				$(this).height(29.7 * pages + "cm");
			})

			window.print();

		});


	</script>
	 <style>


	html{background:#fff}
    body {
		width: 21cm;
		height: 29.7cm;
		margin: 0;
        border: initial;
        border-radius: initial;
        min-height: initial;
        box-shadow: initial;
        background-color: #fff;
        page-break-after: always;
    }

	.view_item {
		width: 100%;
		width: 21cm;
		overflow:hidden;
		margin: 0;
        border: initial;
        border-radius: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
	}

	 h5 {font-size:14px}
    </style>
</head>
           
<body>
<form runat="server" id="form">
	<asp:Repeater runat=server ID=repeater OnItemDataBound="repeater_ItemDataBound">
		<ItemTemplate>
			<div class="view_item" style="margin:0;padding:0">
			    <uc:recruit runat="server" id="recruit"></uc:recruit>
			</div>
		</ItemTemplate>
	</asp:Repeater>	
</form>

</body>    
</html>
