﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="admin_recruit_volunteer_interview1_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript">
	    $(function () {

	        $("#checkall").on("ifChanged", function (sender) {
	            var val = $(this).attr("data-val");
	            var checked = $(this).prop("checked");

	            $.each($(".item_check"), function () {
	                $($(this).find("input")).iCheck(checked ? "check" : "uncheck");
	            });

	        })

			$(".btn_print").click(function () {
				var r_id = $(this).attr("data-id");
				window.open("/@mgt/recruit/volunteer/interview1/print?r_id=" + r_id, "printFrm", "width=810px,height=800px,scrollbars=yes");
				return false;
			});

			var onPass = function () {
				return confirm("실무 서류전형에 합격시키겠습니까?");
			}

		});
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

<input type=hidden runat=server id=sortColumn value="r_id" />
<input type=hidden runat=server id=sortDirection value="desc" />



    <div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>
		<div class="form-horizontal box-body" >
			<div class="box-body">

                <div class="form-group">
					<label for="s_target" class="col-sm-2 control-label">채용공고</label>
					<div class="col-sm-10">
					    <asp:DropDownList runat="server" ID="s_recruit" key="s_recruit" CssClass="form-control" style="width:150px;" AutoPostBack="true" OnSelectedIndexChanged="search"></asp:DropDownList>
					</div>
				</div>


				<div class="form-group">
					<label for="s_keyword" class="col-sm-2 control-label">검색</label>
					<div class="col-sm-10">
						<asp:TextBox runat="server" ID="s_keyword" Width="415" cssClass="form-control inline"  OnTextChanged="search" AutoPostBack="true"></asp:TextBox>
						* 이름
					</div>
				</div>

				<div class="box-footer clearfix text-center">
                    <asp:LinkButton runat="server" ID="btn_search" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
				</div>
				
			</div>
			
		</div>
	</div><!-- /.box -->



     <div class="box box-primary">
		
		 <div class="box-header">
            <h3 class="box-title">총 <asp:Literal runat=server ID=lbTotal></asp:Literal> 건</h3>
            
        </div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
		
			<table class="table table-hover table-bordered ">
				<colgroup>
				    <col width="3%" />
				    <col width="10%" />
				    <col width="10%" />
				    <col width="*" />
				    <col width="10%" />
				    <col width="10%" />
				    <col width="10%" />
				
				    <col width="10%" />
                    <col width="10%" />
				    <col width="10%" />
				</colgroup>
				<thead>
					<tr>
				        <th><input type="checkbox" id="checkall" /></th>
				        <th>번호</th>
				        <th>모집부문</th>
				        <th>모집분야</th>
				        <th><asp:LinkButton runat=server ID=LinkButton3 OnClick="sort" CommandArgument="r_name" Text="이름" ></asp:LinkButton></th>
				        <th>나이</th>
				        <th><asp:LinkButton runat=server ID=LinkButton7 OnClick="sort" CommandArgument="r_education" Text="최종학력" ></asp:LinkButton></th>
				        <th>평가단계</th>
                        <th>마지막수정일</th>
				        <th>인쇄</th>
					</tr>
				</thead>
				<tbody>
			        <asp:Repeater runat=server ID=repeater OnItemDataBound=ListBound>
				        <ItemTemplate>
					        <tr>
						        <input type="hidden" runat="server" id="r_id" value=<%#Eval("r_id")%> />
						        <td><asp:CheckBox runat="server" ID="check" data-id=<%#Eval("r_id")%> CssClass="item_check" /></td>
						        <td><asp:Literal runat=server ID=lbIdx></asp:Literal></td>
						        <td><asp:Literal runat="server" ID="work"></asp:Literal></td>
						        <td class="text-left"><%#Eval("b_title") %></td>
						        <td><a href="#" onclick="goPage('update.aspx' , { c: <%#Eval("r_id")%>, p: '<%#paging.CurrentPage %>', t: 'update' });return false;" style="text-decoration:underline"><%#Eval("r_name")%></a></td>
						        <td><%#(DateTime.Now.Year - int.Parse(Eval("r_birth").ToString().Substring(0,4))).ToString()%></td>
						        <td><%#Eval("r_education")%></td>
						        <td><asp:Literal runat="server" ID="processStatus"></asp:Literal></td>
                                <td><%#Eval("r_moddate" , "{0:yyyy/MM/dd}")%></td>
						        <td><a class="btn btn-default btn-sm btn_print" data-id=<%#Eval("r_id")%>><i class="fa fa-print"></i>인쇄</a></td>
					        </tr>	
				        </ItemTemplate>
						 <FooterTemplate>
							 <tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
								 <td colspan="10">데이터가 없습니다.</td>
							 </tr>
						</FooterTemplate>
			        </asp:Repeater>	
				</tbody>
			</table>
		</div>
        
		<div class="box-footer clearfix text-center">
            <uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="100" PagesPerGroup="10" />

        </div>


	</div>

</asp:Content>