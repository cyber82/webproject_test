﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using CommonLib;

public partial class admin_recruit_volunteer_interview1_update : AdminBasePage {

	Uploader.FileGroup file_group = Uploader.FileGroup.recruit;

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		v_admin_auth auth = base.GetPageAuth();

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.AlphaNumeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context, "default.aspx");

		base.PrimaryKey = Request["c"];

		recruitCtrl.PrimaryKey = Request["c"];

		btn_update.Visible = auth.aa_auth_update;

		a_name.Text = AdminLoginSession.GetCookie(this.Context).name;
		a_email.Text = AdminLoginSession.GetCookie(this.Context).email;
		regdate.Text = DateTime.Now.ToString("yyyy-MM-dd");

		foreach (var a in StaticData.Code.GetList(this.Context).Where(p => p.cd_display == true && p.cd_group == "recruit_grade").OrderBy(p => p.cd_order)) {
			recruit_grade.Items.Add(new ListItem(a.cd_key + " (" + a.cd_value + ")", a.cd_key));
		}

		GetResumeProcess();

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";

	}


	protected void GetResumeProcess()
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            var exist = www6.selectQ<resume_process>("rp_r_id", Convert.ToInt32(PrimaryKey), "rp_process_status", "interview1", "rp_a_id", AdminLoginSession.GetCookie(Context).identifier);
            //if (dao.resume_process.Any(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "interview1" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier))
            if (exist.Any())
            {
                //var resume_process = dao.resume_process.First(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "interview1" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier);
                var resume_process = exist[0];

                recruit_grade.SelectedValue = resume_process.rp_result;
                rp_comment.Text = resume_process.rp_comment;
            }
        }
	}



	protected void btn_update_click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            var exist = www6.selectQ<resume_process>("rp_r_id", Convert.ToInt32(PrimaryKey), "rp_process_status", "interview1", "rp_a_id", AdminLoginSession.GetCookie(Context).identifier);

            //if (dao.resume_process.Any(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "interview1" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier))
            if(exist.Any())
            {
                //dao.resume_process.DeleteAllOnSubmit(dao.resume_process.Where(p => p.rp_r_id == Convert.ToInt32(PrimaryKey) && p.rp_process_status == "interview1" && p.rp_a_id == AdminLoginSession.GetCookie(Context).identifier));
                www6.delete(exist);
            }

            var rp = new resume_process();
            rp.rp_comment = rp_comment.Text;
            rp.rp_r_id = Convert.ToInt32(PrimaryKey);
            rp.rp_a_id = AdminLoginSession.GetCookie(this.Context).identifier;
            rp.rp_regdate = DateTime.Now;
            rp.rp_result = recruit_grade.SelectedValue;
            rp.rp_process_status = "interview1";

            //dao.resume_process.InsertOnSubmit(rp);
            //www6.cud(MakeSQL.insertQ2(rp));
            www6.insert(rp);
            //dao.SubmitChanges();

        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "적용되었습니다.";


	}

}