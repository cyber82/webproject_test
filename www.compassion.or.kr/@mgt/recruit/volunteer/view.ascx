﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="view.ascx.cs" Inherits="admin.recruit.view" %>
    <style>
        .table tr td{text-align:inherit ;}
        .table-striped tr td{text-align:center;}
        .form-title {padding-left:150px }
        .form-title.popup {padding:inherit;margin-left:30px }
    </style>
	<script type="text/javascript">

		$(function () {
			var hash = $("#hash").val();
			if (hash != "") {
				location.href = "#" + hash;
			}


			if ($("#view_type").val() == "popup") {
			    $(".box-container").removeClass("col-sm-6");
			    $(".box-container").addClass("col-sm-12");

			    $(".form-title").addClass("popup");
			}
		});
	</script>
<input type="hidden" runat="server" id="hash" value="" />
<input type="hidden" runat="server" id="view_type" value="" />


    <!-- 기본정보 -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><strong>기본정보</strong></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="box-header"><h3 class="box-title">지원사항</h3></div>
            <div class="box-body">
                <table class="table table-bordered">
				    <colgroup>
					    <col width="15%" />
					    <col width="35%" />
					    <col width="15%" />
					    <col width="35%" />
				    </colgroup>
				    <tbody>
					    <tr>
						    <th>모집부문</th>
						    <td>
                                <asp:Literal runat="server" ID="r_job"></asp:Literal>
						    </td>
						    <th>모집분야</th>
						    <td>
                                <asp:Literal runat="server" ID="r_b_title"></asp:Literal>
						    </td>
					    </tr>	
                        <tr>
						    <th>등록단계</th>
						    <td>
							    <asp:Literal runat="server" ID="r_regist_status"></asp:Literal>
						    </td>
						    <th>평가단계</th>
						    <td>
							    <asp:Literal runat="server" ID="r_process_status"></asp:Literal>
						    </td>
                        </tr>
                        <tr>
						    <th>등록일</th>
						    <td>
							    <asp:Literal runat="server" ID="r_regdate"></asp:Literal>
						    </td>
						    <th>마지막수정일</th>
						    <td>
							    <asp:Literal runat="server" ID="r_moddate"></asp:Literal>
						    </td>
                        </tr>
                                    
                        <tr>
						    <th>이메일</th>
						    <td colspan="3">
							    <asp:Literal runat="server" ID="r_email"></asp:Literal>
						    </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- /.box -->

    <!-- 기본 이력사항 -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><strong>기본 이력사항</strong></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">

            <!-- 기본 인적사항 -->
            <div class="box-header "><h3 class="box-title">기본 인적사항</h3></div>
            <div class="box-body">
                <table class="table table-bordered">
				    <colgroup>
					    <col width="15%" />
					    <col width="35%" />
					    <col width="15%" />
					    <col width="35%" />
				    </colgroup>
				    <tbody>
						<tr>
							<th>이름(한글)</th>
							<td>
                                <asp:Literal runat="server" ID="r_name"></asp:Literal>
							</td>
							<th>이름(영문)</th>
							<td>
                                <asp:Literal runat="server" ID="r_name_en"></asp:Literal> , <asp:Literal runat="server" ID="r_familyname_en"></asp:Literal>
							</td>
						</tr>		
						<tr>
							<th>생년월일</th>
							<td>
                                <asp:Literal runat="server" ID="r_birth"></asp:Literal>
							</td>
                            <th>성별</th>
                            <td><asp:Literal runat="server" ID="r_gender"></asp:Literal></td>
						</tr>	
						<tr>
							<th>전화번호(휴대폰)</th>
							<td>
                                <asp:Literal runat="server" ID="r_phone"></asp:Literal>
							</td>
						</tr>
                        <tr>
							<th>현거주지 주소</th>
							<td colspan="3">
                                <asp:Literal runat="server" ID="r_addr"></asp:Literal>
							</td>
                        </tr>
                        <tr>
                            <th>현재연봉</th>
                            <td><asp:Literal runat="server" ID="r_salary" /></td>
                            <th>희망연봉</th>
                            <td><asp:Literal runat="server" ID="r_hope_salary" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            
            <!-- 병역사항 -->
            <div class="box-header "><h3 class="box-title">병역사항</h3></div>
            <div class="box-body">
                <table class="table table-bordered">
				    <colgroup>
					    <col width="15%" />
					    <col width="35%" />
					    <col width="15%" />
					    <col width="35%" />
				    </colgroup>
				    <tbody>
					    <tr>
						    <th>병역구분</th>
						    <td colspan="3">
                                <asp:Literal runat="server" ID="r_military_status"></asp:Literal>
						    </td>
					    </tr>	
                    </tbody>
                </table>
            </div>

            <!-- 보훈사항 -->
            <div class="box-header "><h3 class="box-title">보훈사항</h3></div>
            <div class="box-body">
                <table class="table table-bordered">
				    <colgroup>
					    <col width="15%" />
					    <col width="35%" />
					    <col width="15%" />
					    <col width="35%" />
				    </colgroup>
				    <tbody>
						<tr>
							<th>보훈여부</th>
							<td>
                                <asp:Literal runat="server" ID="r_veterans_is"></asp:Literal>
							</td>
							<th>보훈번호</th>
							<td>
                                <asp:Literal runat="server" ID="r_veterans_no"></asp:Literal>
							</td>
						</tr>	
							                
                    </tbody>
                </table>
            </div>

            <!-- 장애사항 -->
            <div class="box-header "><h3 class="box-title">장애사항</h3></div>
            <div class="box-body">
                <table class="table table-bordered">
				    <colgroup>
					    <col width="15%" />
					    <col width="35%" />
					    <col width="15%" />
					    <col width="35%" />
				    </colgroup>
				    <tbody>
						<tr>
							<th>장애구분</th>
							<td colspan="3">
                                <asp:Literal runat="server" ID="r_disability_status"></asp:Literal>
							</td>
                        </tr>
                        <tr>
							<th>장애유형</th>
							<td colspan="3">
                                <asp:Literal runat="server" ID="r_disability_type"></asp:Literal>
							</td>
						</tr>	
						<tr>
							<th>장애등급</th>
							<td colspan="3">
                                <asp:Literal runat="server" ID="r_disability_level"></asp:Literal>
							</td>
						</tr>
                    </tbody>
                </table>
            </div>


            <!-- 출석교회 정보 -->
            <div class="box-header "><h3 class="box-title">출석교회 정보</h3></div>
            <div class="box-body">
                <table class="table table-bordered">
				    <colgroup>
					    <col width="15%" />
					    <col width="35%" />
					    <col width="15%" />
					    <col width="35%" />
				    </colgroup>
				    <tbody>
						<tr>
							<th>출석 교회명</th>
							<td>
                                <asp:Literal runat="server" ID="r_church_name"></asp:Literal>
							</td>
							<th>소속교단명</th>
							<td>
                                <asp:Literal runat="server" ID="r_church_team"></asp:Literal>
							</td>
						</tr>	
						<tr>
							<th>컴패션 후원여부</th>
							<td colspan="3">
                                <asp:Literal runat="server" ID="r_is_sponser"></asp:Literal>
							</td>
                        </tr>
                        <tr>
							<th>컴패션 후원과 관련된 특이사항</th>
							<td colspan="3" style="white-space:pre-wrap"><asp:Literal runat="server" ID="r_sponsor_remark"></asp:Literal></td>
						</tr>
                    </tbody>
                </table>
            </div>

            
            <!-- 학력사항 -->
            <div class="box-header "><h3 class="box-title">학력사항</h3></div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>학력구분</th>
						<th>학교명</th>
                        <th>전공</th>
						<th>입학년월</th>
						<th>졸업년월</th>
						<th>소재지</th>
                    </tr>
					<asp:Repeater runat=server ID=repeater_edu >
						<ItemTemplate>
							<tr>
								<td><%#Eval("ru_grade")%></td>
								<td><%#Eval("ru_name")%></td>
								<td><%#Eval("ru_major")%></td>
								<td><%#Eval("ru_date_begin")%></td>
								<td><%#Eval("ru_date_end")%></td>
								<td><%#Eval("ru_location")%></td>
							</tr>	
						</ItemTemplate>
                        <FooterTemplate>
                            <tr runat="server" Visible="<%#repeater_edu.Items.Count == 0 %>">
                                <td colspan="6">결과가 없습니다.</td>
                            </tr>
                        </FooterTemplate>
					</asp:Repeater>		
                </table>
            </div>



            <!-- 어학능력 -->
            <div class="box-header "><h3 class="box-title">어학능력</h3></div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>외국어명</th>
						<th>인증기관</th>
						<th>점수/등급</th>
						<th>취득일</th>
                    </tr>
					<asp:Repeater runat=server ID=repeater_lang_exam>
						<ItemTemplate>
							<tr>
								<td><%#Eval("rle_name") %></td>
								<td><%#Eval("rle_organization")%></td>
								<td><%#Eval("rle_point")%>점수(급)</td>
								<td><%#Eval("rle_date")%></td>
                            </tr>
                            <t>
                                <td colspan="4">
                                    <div class="box-body" style="text-align:left;white-space:pre-wrap;"><%#Eval("rle_comment")%></div>
                                </td>
                            </t>
						</ItemTemplate>
                        <FooterTemplate>
                            <tr runat="server" Visible="<%#repeater_lang_exam.Items.Count == 0 %>">
                                <td colspan="4">결과가 없습니다.</td>
                            </tr>
                        </FooterTemplate>
					</asp:Repeater>				
                </table>
            </div>

            <!-- 자격 및 면허 사항 -->
            <div class="box-header "><h3 class="box-title">자격 및 면허 사항</h3></div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>자격증명</th>
						<th>발급기관</th>
						<th>자격증번호</th>
						<th>취득일</th>
                    </tr>
					<asp:Repeater runat=server ID=repeater_certificate>
						<ItemTemplate>
							<tr>
								<td><%#Eval("rc_name")%></td>
								<td><%#Eval("rc_issue")%></td>
								<td><%#Eval("rc_no")%></td>
								<td><%#Eval("rc_date")%></td>
							</tr>	
						</ItemTemplate>
                        <FooterTemplate>
                            <tr runat="server" Visible="<%#repeater_certificate.Items.Count == 0 %>">
                                <td colspan="4">결과가 없습니다.</td>
                            </tr>
                        </FooterTemplate>
					</asp:Repeater>			
                </table>
            </div>

            

            <!-- 경력사항 -->
            <div class="box-header "><h3 class="box-title">경력사항</h3></div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>회사명</th>
						<th>고용형태</th>
						<th>근무기간</th>
                        <th>근무부서</th>
						<th>직급</th>
						<th>담당업무</th>
                    </tr>
					                            
					<asp:Repeater runat=server ID=repeater_work >
						<ItemTemplate>
							<tr>
								<td><%#Eval("rw_name")%></td>
								<td><%#Eval("rw_status")%></td>
								<td><%#Eval("rw_date_begin")%> ~ <%#Eval("rw_date_end")%></td>
								<td><%#Eval("rw_role")%></td>
								<td><%#Eval("rw_position")%></td>
								<td><%#Eval("rw_work")%></td>
							</tr>	
						</ItemTemplate>
                        <FooterTemplate>
                            <tr runat="server" Visible="<%#repeater_work.Items.Count == 0 %>">
                                <td colspan="6">결과가 없습니다.</td>
                            </tr>
                        </FooterTemplate>
					</asp:Repeater>				
                </table>
            </div>


            <!-- 경력참고인 -->
            <div class="box-header "><h3 class="box-title">포트폴리오</h3></div>
            <div class="box-body">
                <table class="table table-bordered">
				    <colgroup>
					    <col width="15%" />
					    <col width="35%" />
				    </colgroup>
				    <tbody>
						<tr>
							<th>URL</th>
							<td>
                                <a id="portfolio_link" runat="server" target="_blank">
                                    <asp:Literal runat="server" ID="r_portfolio"></asp:Literal>
                                </a>
							</td>
                        </tr>
                        <tr>
							<th>첨부파일</th>
							<td>
                                <asp:Literal runat="server" ID="r_file"></asp:Literal>
							</td>
						</tr>	
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- /.box -->

    <!-- 자기소개 -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><strong>자기소개</strong></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="box-body">
                <asp:Literal runat="server" ID="r_txt_aboutme_1"> </asp:Literal>
            </div>
        </div>
    </div><!-- /.box -->

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><strong>경력기술서</strong></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="box-body">
                <asp:Literal runat="server" ID="r_txt_career"> </asp:Literal>
            </div>
        </div>
    </div><!-- /.box -->


    <!-- 신앙간증문/추천서 -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><strong>신앙간증문/추천서</strong></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="box-body">
                <asp:Literal runat="server" ID="r_txt_aboutme_2"> </asp:Literal>
            </div>
            <div class="box-footer">
                추천서 : <asp:Literal runat="server" ID="r_file_recommend"></asp:Literal>
            </div>
        </div>
    </div><!-- /.box -->
