﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
using System.Collections;

public partial class admin_recruit_volunteer_default : AdminBasePage {

	List<recruit_jobcode> Jobcode
	{
		get
		{
			if (this.ViewState["recruit_jobcode"] == null)
            {

                using (AdminDataContext dao = new AdminDataContext())
                {
                    //this.ViewState["recruit_jobcode"] = dao.recruit_jobcode.Where(p => p.rj_display == true).ToJson();
                    this.ViewState["recruit_jobcode"] = www6.selectQ<recruit_jobcode>("rj_display", 1).ToJson();
                }
			}
			return this.ViewState["recruit_jobcode"].ToString().ToObject<List<recruit_jobcode>>();
		}
		set
		{
			this.ViewState["recruit_jobcode"] = value.ToJson();
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		var work = Request["s_work"].EmptyIfNull();

		foreach (var a in Recruit.RegistStatus()) {
			s_regist_status.Items.Add(new ListItem(a.Value, a.Key));
		}
		s_regist_status.Items.Insert(0, new ListItem("전체", "-1"));
		s_regist_status.Items[5].Selected = true;

		foreach (var a in StaticData.Code.GetList(this.Context).Where(p => p.cd_display == true && p.cd_group == "recruit_process_status").OrderBy(p => p.cd_order)) {
			s_process_status.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}
		s_process_status.Items.Insert(0, new ListItem("전체", ""));
		s_process_status.Items[0].Selected = true;

		ShowRecruit();
		Master.Content.Controls.BindAllTextBox(this.Context);

        using (AdminDataContext dao = new AdminDataContext())
        {
            var cookie = AdminLoginSession.GetCookie(Context);
            int a_id = cookie.identifier;

            ArrayList arrArjList = new ArrayList();

            //foreach (var arj in dao.admin_recruit_jobcode.Where(a => a.aj_a_id == a_id))
            var list = www6.selectQ<admin_recruit_jobcode>("aj_a_id", a_id);
            foreach (var arj in list)
            {
                arrArjList.Add(arj.aj_rj_depth1);
            }
            
            //foreach (var a in dao.recruit_jobcode.Where(p => p.rj_display == true && p.rj_depth2 == "").OrderBy(p => p.rj_order))
            var list2 = www6.selectQ<recruit_jobcode>("rj_display", 1, "rj_depth2", "", "rj_order");
            foreach (var a in list2)
            {
                int idx = arrArjList.IndexOf(a.rj_depth1);
                if (idx >= 0 || cookie.type == "super")
                {
                    s_work.Items.Add(new ListItem(a.rj_name, a.rj_depth1));
                }
            }
            s_work.Items.Insert(0, new ListItem("직무선택", ""));
            s_work_sub.Items.Insert(0, new ListItem("세부 직무선택", ""));

            if (work != "")
            {
                s_work.SelectedValue = work;
                s_work_SelectedIndexChanged(null, null);
            }

        }


		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
		btn_set_fail.Visible = btn_send_mail.Visible = auth.aa_auth_update;


	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected override void GetList(int page) {


		Master.IsSearch = false;
		if (
			s_recruit_status.SelectedValue != "" 
			|| s_result.SelectedValue != ""
			|| s_work.SelectedValue != ""
			|| s_work_sub.SelectedValue != ""
			|| s_regist_status.SelectedValue != ""
			|| s_process_status.SelectedValue != ""
			|| s_keyword.Text != "" 
			|| s_b_date.Text != "" 
			|| s_e_date.Text != "")
        {
			Master.IsSearch = true;
		}


		r_result.Value = s_result.SelectedValue;
        using (AdminDataContext dao = new AdminDataContext())
        {

            var b_completed = -1;

            if (s_recruit_status.SelectedValue == "ing")
                b_completed = 0;
            else if (s_recruit_status.SelectedValue == "completed")
                b_completed = 1;



            var cookie = AdminLoginSession.GetCookie(Context);

            //var list = dao.sp_resume_list(page, paging.RowsPerPage, s_keyword.Text.Trim(), s_b_date.Text, s_e_date.Text, b_completed, "done", s_result.SelectedValue,
            //    Convert.ToInt32(s_recruit.SelectedValue), s_work.SelectedValue, s_work_sub.SelectedValue, s_process_status.SelectedValue, cookie.type == "super" ? -1 : cookie.identifier).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword", "startdate", "enddate", "b_completed", "r_regist_status", "r_result", "r_b_id", "r_rj_depth1", "r_rj_depth2", "r_process_status", "a_id" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_keyword.Text.Trim(), s_b_date.Text, s_e_date.Text, b_completed, "done", s_result.SelectedValue, Convert.ToInt32(s_recruit.SelectedValue), s_work.SelectedValue, s_work_sub.SelectedValue, s_process_status.SelectedValue, cookie.type == "super" ? -1 : cookie.identifier };
            var list = www6.selectSP("sp_resume_list", op1, op2).DataTableToList<sp_resume_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();


        }
	}

	void ShowRecruit() {
		using (AdminDataContext dao = new AdminDataContext()) {

			//var list = dao.recruit.OrderByDescending(p => p.b_id);
            var list = www6.selectQ<recruit>("b_id desc");

            if (s_recruit_status.SelectedValue == "ing")
            {
                //list = list.Where(p => !p.b_completed).OrderByDescending(p => p.b_id);
                list = www6.selectQ<recruit>("b_completed", 0, "b_id desc");
            }
            else if (s_recruit_status.SelectedValue == "completed")
            {
                //list = dao.recruit.Where(p => p.b_completed).OrderByDescending(p => p.b_id);
                list = www6.selectQ<recruit>("b_completed", 1, "b_id desc");
            }

			s_recruit.Items.Clear();
			foreach (var i in list)
            {
				s_recruit.Items.Add(new ListItem(i.b_title, i.b_id.ToString()));
			}
		}
		s_recruit.Items.Insert(0, new ListItem("선택하세요", "-1"));
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				sp_resume_listResult entity = e.Item.DataItem as sp_resume_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();

				((Literal)e.Item.FindControl("registStatus")).Text = Recruit.GetRegistStatusText(entity.r_regist_status);
				((Literal)e.Item.FindControl("processStatus")).Text = StaticData.Code.GetList(this.Context).First(p => p.cd_display == true && p.cd_group == "recruit_process_status" && p.cd_key == entity.r_process_status).cd_value;

				var result = "";
				if (entity.r_result == "pass")
					result = "합격";
				else if (entity.r_result == "fail")
					result = "불합격";
				else
					result = "-";
				((Literal)e.Item.FindControl("resultStatus")).Text = result;

				var jobcode = this.Jobcode;
				try {
					((Literal)e.Item.FindControl("work")).Text = string.Format("{0}/{1}",
						jobcode.First(p => p.rj_depth1 == entity.r_rj_depth1 && p.rj_depth2 == "").rj_name,
						jobcode.First(p => p.rj_depth1 == entity.r_rj_depth1 && p.rj_depth2 == entity.r_rj_depth2).rj_name
						);
				} catch { }
			}
		}


		
	}

	protected void btn_set_doc1_Click(object sender, EventArgs e)
    {

		var a_id = AdminLoginSession.GetCookie(this.Context).identifier;

        using (AdminDataContext dao = new AdminDataContext())
        {
            foreach (RepeaterItem i in repeater.Items)
            {
                if (((CheckBox)i.FindControl("check")).Checked)
                {
                    var ra_id = Convert.ToInt32(((HtmlInputHidden)i.FindControl("ra_id")).Value);

                    //var r = dao.resume.First(p => p.r_id == ra_id);
                    var r = www6.selectQF<resume>("r_id", ra_id);

                    if (r.r_process_status == "standby")
                    {
                        r.r_process_status = "doc1";
                        r.r_a_id = a_id;

                        //dao.SubmitChanges();
                        //string wClause = string.Format("r_id = {0}", ra_id);
                        www6.update(r);

                    }
                }
            }
        }

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "적용되었습니다.";
		this.GetList(1);
	}

	protected void s_process_status_SelectedIndexChanged(object sender, EventArgs e) {

		v_admin_auth auth = base.GetPageAuth();
		search(sender, e);
	}

	protected void s_work_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            var cookie = AdminLoginSession.GetCookie(Context);
            int a_id = cookie.identifier;

            ArrayList arrArjList = new ArrayList();

            //foreach (var arj in dao.admin_recruit_jobcode.Where(a => a.aj_a_id == a_id && a.aj_rj_depth1 == s_work.SelectedValue))
            var list = www6.selectQ<admin_recruit_jobcode>("aj_a_id", a_id, "aj_rj_depth1", s_work.SelectedValue);
            foreach (var arj in list)
            {
                arrArjList.Add(arj.aj_rj_depth2);
            }

            s_work_sub.Items.Clear();
                        
            //foreach (var a in dao.recruit_jobcode.Where(p => p.rj_display == true && p.rj_depth1 == s_work.SelectedValue && p.rj_depth2 != "").OrderBy(p => p.rj_order))
            var list2 = www6.selectQ2<recruit_jobcode>("rj_display = ", 1, "rj_depth1 = ", s_work.SelectedValue, "rj_depth2 != ", "", "rj_order");
            foreach (var a in list2)
            {
                int idx = arrArjList.IndexOf(a.rj_depth2);
                if (idx >= 0 || cookie.type == "super")
                {
                    s_work_sub.Items.Add(new ListItem(a.rj_name, a.rj_depth2));
                }
            }
            s_work_sub.Items.Insert(0, new ListItem("세부 직무선택", ""));
        }

		search(sender, e);
	}


	protected void btn_set_fail_Click(object sender, EventArgs e)
    {
		var a_id = AdminLoginSession.GetCookie(this.Context).identifier;

        using (AdminDataContext dao = new AdminDataContext())
        {
            foreach (RepeaterItem i in repeater.Items)
            {
                if (((CheckBox)i.FindControl("check")).Checked)
                {
                    var ra_id = Convert.ToInt32(((HtmlInputHidden)i.FindControl("ra_id")).Value);

                    //var r = dao.resume.First(p => p.r_id == ra_id);
                    string sqlStr = string.Format("select * from resume where r_id = {0}", ra_id);
                    var r = www6.selectQF<resume>("r_id", ra_id);

                    if (r.r_process_status == "standby")
                    {
                        r.r_process_status = "fail";
                        r.r_result_date = DateTime.Now;
                        r.r_a_id = a_id;

                        //dao.SubmitChanges();
                        //string wClause = string.Format("r_id = {0}", ra_id);
                        www6.update(r);
                    }
                }
            }
        }

		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "적용되었습니다.";
		this.GetList(1);
	}

	protected void s_recruit_status_SelectedIndexChanged(object sender, EventArgs e) {
		ShowRecruit();
		GetList(1);
	}
}
