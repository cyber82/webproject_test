﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="admin_recruit_volunteer_doc1_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ Register Src="~/@mgt/recruit/volunteer/view.ascx" TagPrefix="uc" TagName="recruit" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">

	<script type="text/javascript">

		var onSubmit = function () {

			if($("#rp_result input[type=radio]:checked").length < 1){
			    alert("평가 결과를 선택해주세요.");
			    return false;
			}

			return confirm("저장하시겠습니까?");
		}

		$(function(){
			$(".btn_print").click(function () {
				var r_id = <%:this.PrimaryKey.ToString()%>;
			    window.open("/@mgt/recruit/volunteer/print?r_id=" + r_id, "printFrm", "width=810px,height=800px,scrollbars=yes");
				return false;
			});
		});
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	
    
    <div class="row">
        <div class="col-md-12">


	        <uc:recruit runat="server" id="recruitCtrl"></uc:recruit>


            
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-plus-square"></i><strong> 평가 / 결과</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box-header"><h3 class="box-title">지원사항</h3></div>
                    <div class="box-body">
                        <table class="table table-bordered">
				            <colgroup>
					            <col width="30%" />
					            <col width="70%" />
					            <col width="30%" />
					            <col width="70%" />
				            </colgroup>
				            <tbody>
							    <tr>
                                    <th>평가자</th>
								    <td>
                                        <asp:Literal runat="server" ID="a_name"></asp:Literal>(<asp:Literal runat="server" ID="a_email"></asp:Literal>)
								    </td>
								    <th>평가일</th>
								    <td>
                                        <asp:Literal runat="server" ID="regdate"></asp:Literal>
								    </td>
							    </tr>	

							    <tr>
								    <th>결과(점수)</th>
								    <td colspan="3">
                                        <asp:RadioButtonList runat="server" ID="rp_result">
                                            <asp:ListItem Text="합격" Value="합격"></asp:ListItem>
                                            <asp:ListItem Text="불합격" Value="불합격"></asp:ListItem>
                                        </asp:RadioButtonList>
								    </td>
							    </tr>	

							    <tr>
								    <th>비고</th>
								    <td colspan="3">
                                        <asp:TextBox runat="server" ID="rp_comment" CssClass="form-control" TextMode="MultiLine" Rows="3" Width="500"></asp:TextBox>
								    </td>
							    </tr>
                                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- /.box -->
            
        </div>
    </div>


	<div class="box-footer clearfix text-center">
        <a runat="server" id="btnList" class="btn btn-default pull-left">목록</a>
        
		<asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" Style="margin-right: 5px">등록</asp:LinkButton>
        <a class="btn btn-warning btn_print pull-right" style="margin-right: 5px;">인쇄</a>
	</div>
	
</asp:Content>