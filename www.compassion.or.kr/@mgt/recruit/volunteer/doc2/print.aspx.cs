﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;

public partial class admin_recruit_volunteer_print : AdminBasePage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		// 1,8
		var r_ids = Request["r_id"].Split(',');
		repeater.DataSource = r_ids;
		repeater.DataBind();
		
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();


	}



	protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e) {
		string entity = e.Item.DataItem as string;
		var view = (ASP._mgt_recruit_volunteer_view_ascx)e.Item.FindControl("recruit");
		view.PrimaryKey = entity;
		view.ShowAll = true;
		view.Print = true;
	}
}