﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using CommonLib;

public partial class mgt_recruit_volunteer_appraisal : AdminBasePage {

	
	protected string ids{
		set {
			this.ViewState.Add("ids", value);
		}
		get {
			if (this.ViewState["ids"] == null) return "";
			return this.ViewState["ids"].ToString();
		}
	}


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		v_admin_auth auth = base.GetPageAuth();
		btn_update.Visible = auth.aa_auth_update;

		ids = Request["id"];

        using (AdminDataContext dao = new AdminDataContext())
        {
            //string query = string.Format("select * from [resume] where r_id in ({0}) order by r_name ", ids);
            //var list = dao.ExecuteQuery<resume>(query);
            var list = www6.selectQ2<resume>("r_id IN", utils.include(ids), "r_name");
            
            var names = "";
            foreach (var i in list)
            {
                names += "," + i.r_name;
            }
            lbNames.Text = names.Substring(1);



            foreach (var a in StaticData.Code.GetList(this.Context).Where(p => p.cd_display == true && p.cd_group == "recruit_process_status").OrderBy(p => p.cd_order))
            {
                s_process_status.Items.Add(new ListItem(a.cd_value, a.cd_key));
            }
            s_process_status.Items.Insert(0, new ListItem("선택하세요", ""));
            s_process_status.Items[0].Selected = true;
        }

		btn_update.Visible = auth.aa_auth_update;
	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();


	}

	protected void btn_update_click(object sender, EventArgs e)
    {

        using (AdminDataContext dao = new AdminDataContext())
        {
            //string query = string.Format("select * from [resume] where r_id in ({0}) order by r_name ", ids);
            //var list = dao.ExecuteQuery<resume>(query);
            var list = www6.selectQ2<resume>("r_id IN", utils.include(ids));

            foreach (var entity in list)
            {
                entity.r_process_status = s_process_status.SelectedValue;

                if (s_process_status.SelectedValue != "doc1" && s_process_status.SelectedValue != "doc2")
                {
                    SendMail(entity.r_email, entity.r_name);
                }

                //string wClause = string.Format("r_id = {0}", entity.r_id);
                www6.update(entity);
            }

            //dao.SubmitChanges();


            Response.Write("<script>alert('저장되었습니다.'); opener.location.reload(); window.close();</script>");
        }
	}




	void SendMail(string to, string name) {

		try {

			var from = ConfigurationManager.AppSettings["recruitEmailSender"];

			var args = new Dictionary<string, string> {
				{"{name}" , name }
			};
			var title = "[한국컴패션_인재채용] "+name+"님, 지원하신 전형의 결과가 발표되었습니다.";



			Email.Send(this.Context, from, new List<string>() { to },
				title, "/mail/recruit_result.html",
				args
			, null);

		} catch (Exception e) {
			ErrorLog.Write(this.Context, 0, e.Message);
			throw e;
		}

	}

}