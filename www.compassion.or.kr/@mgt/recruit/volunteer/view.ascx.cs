﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;


namespace admin.recruit {
	public partial class view : System.Web.UI.UserControl {

		Uploader.FileGroup file_group = Uploader.FileGroup.recruit;

		public string PrimaryKey{
			get{
				if (this.ViewState["PrimaryKey"] == null)
					this.ViewState["PrimaryKey"] = "";
				return this.ViewState["PrimaryKey"].ToString();
			}
			set{
				this.ViewState["PrimaryKey"] = value;
			}
		}

		public bool ShowAll{
			get;
			set;
		}

		public bool Print{
			get{
				if (this.ViewState["Print"] == null)
					this.ViewState["Print"] = false;
				return Convert.ToBoolean(this.ViewState["Print"]);
			}
			set{
				this.ViewState["Print"] = value;
			}
		}


		public bool Popup{
			get{
				if (this.ViewState["Popup"] == null)
					this.ViewState["Popup"] = false;
				return Convert.ToBoolean(this.ViewState["Popup"]);
			}
			set{
				this.ViewState["Popup"] = value;
			}
		}

		protected override void OnLoad(EventArgs e) {
			base.OnLoad(e);

			//	if (!IsPostBack) {

			//	}
			this.ShowData();

			hash.Value = "";

			if (ShowAll) {
			}

			if (Print || Popup) {
				// 팝업일 때 class변경
				view_type.Value = "popup";
			}
		}

		void ShowData() {

			if (PrimaryKey.ToString() == "")
				return;

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var r = dao.resume.First(p => p.r_id == Convert.ToInt32(PrimaryKey));
                var r = www6.selectQF<resume>("r_id", Convert.ToInt32(PrimaryKey));

                if (r.r_b_id != null)
                {
                    //r_b_title.Text = dao.recruit.First(p => p.b_id == r.r_b_id).b_title;
                    r_b_title.Text = www6.selectQF<CommonLib.recruit>("b_id", r.r_b_id).b_title;
                }

                r_process_status.Text = StaticData.Code.GetList(this.Context).First(p => p.cd_display == true && p.cd_group == "recruit_process_status" && p.cd_key == r.r_process_status).cd_value;
                r_regist_status.Text = Recruit.GetRegistStatusText(r.r_regist_status);
                r_moddate.Text = r.r_moddate.ToString("yyyy-MM-dd HH:mm");
                r_regdate.Text = r.r_regdate.ToString("yyyy-MM-dd HH:mm");
                r_name.Text = r.r_name;
                r_email.Text = r.r_email;
                r_birth.Text = string.Format("{0}년 {1}월 {2}일", r.r_birth.Substring(0, 4), r.r_birth.Substring(5, 2), r.r_birth.Substring(8, 2));
                r_gender.Text = r.r_gender;

                r_name_en.Text = r.r_name_en;
                r_familyname_en.Text = r.r_familyname_en;
                r_addr.Text = r.r_addr;
                r_phone.Text = r.r_phone;

                r_salary.Text = r.r_salary;
                r_hope_salary.Text = r.r_hope_salary;

                r_portfolio.Text = r.r_portfolio;
                portfolio_link.HRef = "http://" + r.r_portfolio;


                if (r.r_file.EmptyIfNull() != "")
                {
                    if (r.r_file.IndexOf("?path=") > -1)
                    {
                        var fileName = r.r_file.Substring(0, r.r_file.IndexOf("?path="));
                        var id = r.r_file.Substring(r.r_file.IndexOf("?path=") + 6);
                        r_file.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, id), fileName, id, fileName);
                    }
                    else
                    {
                        r_file.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, r.r_id.ToString()), r.r_file, r.r_id, r.r_file);
                    }
                }


                //직무
                try
                {
                    //var job_depth1 = dao.recruit_jobcode.First(p => p.rj_depth1 == r.r_rj_depth1 && p.rj_depth2 == "").rj_name;
                    //var job_depth2 = dao.recruit_jobcode.First(p => p.rj_depth1 == r.r_rj_depth1 && p.rj_depth2 == r.r_rj_depth2).rj_name;
                    var job_depth1 = www6.selectQF<recruit_jobcode>("rj_depth1", r.r_rj_depth1, "rj_depth2", "").rj_name;
                    var job_depth2 = www6.selectQF<recruit_jobcode>("rj_depth1", r.r_rj_depth1, "rj_depth2", r.r_rj_depth2).rj_name;
                    
                    r_job.Text = string.Format("{0}/{1}", job_depth1, job_depth2);
                }
                catch
                {

                }
                // 병역
                r_military_status.Text = r.r_military_status;


                // 보훈
                r_veterans_is.Text = r.r_veterans_is.HasValue && r.r_veterans_is.Value ? "대상" : "비대상";
                r_veterans_no.Text = r.r_veterans_no;

                // 장애
                r_disability_status.Text = r.r_disability_status;
                r_disability_type.Text = r.r_disability_type;
                r_disability_level.Text = r.r_disability_level;

                //출석교회 정보
                r_church_name.Text = r.r_church_name;
                r_church_team.Text = r.r_church_team;
                r_is_sponser.Text = (bool)r.r_is_sponsor ? "예" : "아니요";


                // 상시에서 넘어온 경우 원본 아이디로 조회
                var r_r_id = r.r_r_id == null ? r.r_id : r.r_r_id;

                // 학력
                var edu = www6.selectQ<resume_edu>("ru_r_id", r_r_id);

                repeater_edu.DataSource = edu;
                repeater_edu.DataBind();

                // 경력
                var work = www6.selectQ<resume_work>("rw_r_id", r_r_id);

                repeater_work.DataSource = work;
                repeater_work.DataBind();


                // 외국어 사항
                var lang = www6.selectQ<resume_lang_exam>("rle_r_id", r_r_id);

                repeater_lang_exam.DataSource = lang;
                repeater_lang_exam.DataBind();


                // 자격증 사항
                var cert = www6.selectQ<resume_certificate>("rc_r_id", r_r_id);

                repeater_certificate.DataSource = cert;
                repeater_certificate.DataBind();



                // 자기소개/경력기술서/신앙간증문/추천서
                r_txt_aboutme_1.Text = r.r_txt_aboutme_1.ToHtml();
                r_txt_aboutme_2.Text = r.r_txt_aboutme_2.ToHtml();
                r_txt_career.Text = r.r_txt_career.ToHtml();

                // 추천서 파일
                if (r.r_txt_aboutme_4.EmptyIfNull() != "")
                {

                    if (r.r_txt_aboutme_4.IndexOf("?path=") > -1)
                    {
                        var fileName = r.r_txt_aboutme_4.Substring(0, r.r_txt_aboutme_4.IndexOf("?path="));
                        var id = r.r_txt_aboutme_4.Substring(r.r_txt_aboutme_4.IndexOf("?path=") + 6);
                        r_file_recommend.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, id), fileName, id, fileName);
                    }
                    else
                    {
                        r_file_recommend.Text = string.Format("<a href='{0}{1}?id={2}'>{3}</a>", Uploader.GetRoot(file_group, r.r_id.ToString()), r.r_txt_aboutme_4, r.r_id, r.r_txt_aboutme_4);
                    }


                }

                r_sponsor_remark.Text = r.r_sponsor_remark;
            }
		}

	}
}