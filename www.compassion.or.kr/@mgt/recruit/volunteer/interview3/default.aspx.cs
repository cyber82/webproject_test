﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class admin_recruit_volunteer_interview3_default : AdminBasePage {

	List<recruit_jobcode> Jobcode
	{
		get
		{
			if (this.ViewState["recruit_jobcode"] == null)
            {
                using (AdminDataContext dao = new AdminDataContext())
                {
                    //this.ViewState["recruit_jobcode"] = dao.recruit_jobcode.Where(p => p.rj_display == true).ToJson();
                    this.ViewState["recruit_jobcode"] = www6.selectQ<recruit_jobcode>("rj_display", 1).ToJson();
                }
			}
			return this.ViewState["recruit_jobcode"].ToString().ToObject<List<recruit_jobcode>>();
		}
		set
		{
			this.ViewState["recruit_jobcode"] = value.ToJson();
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		base.sortColumn = sortColumn;
		base.sortDirection = sortDirection;

		ShowRecruit();
		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.sortColumn = sortColumn;
		base.sortDirection = sortDirection;
		base.OnAfterPostBack();
	}

	protected override void GetList(int page) {


		Master.IsSearch = false;
		if (
			s_keyword.Text != ""
			|| s_recruit.SelectedValue != "-1"
			) {
			Master.IsSearch = true;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {
            var cookie = AdminLoginSession.GetCookie(Context);
            //var list = dao.sp_resume_list(page, paging.RowsPerPage, s_keyword.Text.Trim(), "", "", 0, "done", "", Convert.ToInt32(s_recruit.SelectedValue), "", "", "interview3", cookie.type == "super" ? -1 : cookie.identifier).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword", "startdate", "enddate", "b_completed", "r_regist_status", "r_result", "r_b_id", "r_rj_depth1", "r_rj_depth2", "r_process_status", "a_id" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_keyword.Text.Trim(), "", "", 0, "done", "", Convert.ToInt32(s_recruit.SelectedValue), "", "", "interview3", cookie.type == "super" ? -1 : cookie.identifier };
            var list = www6.selectSP("sp_resume_list", op1, op2).DataTableToList<sp_resume_listResult>().ToList();

            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();


        }
	}

	void ShowRecruit()
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            s_recruit.Items.Clear();

            //foreach (var i in dao.recruit.Where(p => p.b_display == true && !p.b_completed).OrderByDescending(p => p.b_id))
            var list = www6.selectQ<recruit>("b_display", 1, "b_completed", 0, "b_id desc");
            foreach (var i in list)
            {
                s_recruit.Items.Add(new ListItem(i.b_title, i.b_id.ToString()));
            }

        }
		s_recruit.Items.Insert(0, new ListItem("선택하세요", "-1"));
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				sp_resume_listResult entity = e.Item.DataItem as sp_resume_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();


				((Literal)e.Item.FindControl("processStatus")).Text = StaticData.Code.GetList(this.Context).First(p => p.cd_display == true && p.cd_group == "recruit_process_status" && p.cd_key == entity.r_process_status).cd_value;


				var jobcode = this.Jobcode;
				try {
					((Literal)e.Item.FindControl("work")).Text = string.Format("{0}/{1}",
						jobcode.First(p => p.rj_depth1 == entity.r_rj_depth1 && p.rj_depth2 == "").rj_name,
						jobcode.First(p => p.rj_depth1 == entity.r_rj_depth1 && p.rj_depth2 == entity.r_rj_depth2).rj_name
						);
				} catch { }
			}
		}

	}

}
