﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="mail.aspx.cs" Inherits="admin_recruit_volunteer_mail" ValidateRequest="false"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">

<head id="Head1" runat="server">
    <title>compassion Admin</title>
    
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/@mgt/template/bootstrap/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="/@mgt/template/dist/css/AdminLTE.min.css" />
    <!-- iCheck -->
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/square/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/minimal/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/plugins/iCheck/flat/blue.css" />
    <link rel="stylesheet" href="/@mgt/template/dist/css/skins/skin-green.min.css" />
	<!-- Date Picker -->
    <link rel="stylesheet" href="/@mgt/template/plugins/datepicker/datepicker3.css" />
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/@mgt/template/plugins/daterangepicker/daterangepicker-bs3.css" /><link rel="stylesheet" href="/@mgt/common/css/admin.css" /><link rel="stylesheet" href="/@mgt/common/js/bootstrap-switch/bootstrap-switch.min.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- jQuery 2.1.4 -->
    <script src="/@mgt/template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!-- Bootstrap 3.3.5 -->
    <script src="/@mgt/template/bootstrap/js/bootstrap.min.js"></script>
	<script src="/@mgt/common/js/bootstrap-switch/bootstrap-switch.min.js"></script>

    <!-- iCheck -->
    <script src="/@mgt/template/plugins/iCheck/icheck.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="/@mgt/template/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->

    <script src="/@mgt/template/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="/@mgt/template/plugins/datepicker/locales/bootstrap-datepicker.kr.js"></script>

	<script type="text/javascript" src="/@mgt/common/js/common.js" defer="defer"></script>
	<script type="text/javascript" src="/@mgt/common/js/message.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/jquery.json-2.4.min.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/jquery.json-3.3.2.min.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/form.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/cookie.js" defer="defer"></script>
	
	<script type="text/javascript" src="/assets/jquery/wisekit/function.js" defer="defer"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.search.js" defer="defer"></script>
	<script type="text/javascript" src="/@mgt/common/js/ajaxupload.3.6.wisekit.js" defer="defer"></script> 
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateRange.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.bootstrap.js"></script>
    
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript">

	    $(function () {
            
		    image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
	        initEditor(oEditors, "b_content");


			if ($("#action").val() != "") {
				if ($("#action").val() == "send") {
					alert("발송되었습니다.");
					window.close();
				}
			}
			
			$("#btn_close").click(function () {
				window.close();
			});

		});

		var onSubmit = function () {

			if (!validateForm([
				{ id: "#mail_from", msg: "보내는 사람을 입력하세요" },
				{ id: "#mail_title", msg: "제목을 입력하세요" } , 
			])) {
				return false;
			}


			oEditors.getById["b_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			return confirm("전송하시겠습니까?");
		}

		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

	</script>

</head>
           
<body>
<form runat="server" id="form">
	<input type="hidden" id="action" runat="server"  value="" />
    <input type="hidden" runat="server" id="mail_to" />




    

    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary" >
                    <div class="box-header with-border">
                        <h3 class="box-title">메일발송</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                        <div class="form-group">
                            <label for="mail_to_name">받는사람 : </label><br />
                            <asp:Literal runat="server" ID="mail_to_name"></asp:Literal>
                        </div>

                        <div class="form-group">
                            <label for="mail_from">보내는 사람</label>
                            <asp:TextBox runat="server" ID="mail_from" CssClass="form-control" style="width:600px;"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            제목<asp:TextBox runat="server" ID="mail_title" CssClass="form-control" style="width:600px;"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <textarea name="b_content" id="b_content" runat="server" style="width:600px; height:260px; display:none;"></textarea>
                        </div>
                        
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                           <asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-primary"><i class="fa fa-envelope-o"></i> 전송</asp:LinkButton>
                        </div>
                        <button class="btn btn-default" runat="server" id="btn_close"><i class="fa fa-times"></i> 닫기</button>
                    </div><!-- /.box-footer -->
                </div><!-- /. box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    
    </section><!-- /.content -->

</form>

</body>    
</html>
