﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;

public partial class admin_recruit_notice_update : AdminBasePage {

	List<recruit_jobcode> Jobcode {
		get
        {
			if (this.ViewState["recruit_jobcode"] == null)
            {
                using (AdminDataContext dao = new AdminDataContext())
                {
                    //this.ViewState["recruit_jobcode"] = dao.recruit_jobcode.Where(p => p.rj_display == true).ToJson();
                    this.ViewState["recruit_jobcode"] = www6.selectQ<recruit_jobcode>("rj_display", 1).ToJson();
                }
			}
			return this.ViewState["recruit_jobcode"].ToString().ToObject<List<recruit_jobcode>>();
		}
		set {
			this.ViewState["recruit_jobcode"] = value.ToJson();
		}
	}

	List<recruit_job> BoardJob
	{
		get
		{
			if (this.ViewState["recruit_job"] == null) {
				this.ViewState["recruit_job"] = new List<recruit_job>().ToJson();
			}
			return this.ViewState["recruit_job"].ToString().ToObject<List<recruit_job>>();
		}
		set
		{
			this.ViewState["recruit_job"] = value.ToJson();
		}
	}

	public class recruit_job_without_location {
		public string bj_rj_depth1 {
			get;
			set;
		}
		public string bj_rj_depth2 {
			get;
			set;
		}

		public class Comparer : IEqualityComparer<recruit_job_without_location> {
			public bool Equals(recruit_job_without_location x, recruit_job_without_location y) {
				return x.bj_rj_depth1 == y.bj_rj_depth1 && x.bj_rj_depth2 == y.bj_rj_depth2;
			}
			public int GetHashCode(recruit_job_without_location obj) {
				unchecked  // overflow is fine
				{
					int hash = 17;
					hash = hash * 23 + (obj.bj_rj_depth1 ?? "").GetHashCode();
					hash = hash * 23 + (obj.bj_rj_depth2 ?? "").GetHashCode();
					return hash;
				}
			}
		}
	}


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();

		base.FileGroup = Uploader.FileGroup.file_recruit_notice;
		base.FileRoot = Uploader.GetRoot(base.FileGroup);
		base.PrimaryKey = Request["c"].ValueIfNull("-1");
		base.Action = Request["t"];

		b_id.Value = PrimaryKey;

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");

		b_regdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
		//b_content.FilebrowserImageUploadUrl += "&fileDir=" + Uploader.GetRoot(Uploader.FileGroup.image_board);


		foreach (var a in this.Jobcode.Where(p => p.rj_depth2 == "").OrderBy(p => p.rj_order)) {
			bj_rj_depth1.Items.Add(new ListItem(a.rj_name, a.rj_depth1));
		}
		bj_rj_depth1_SelectedIndexChanged(null, null);

		if (base.Action == "update") {

			using (AdminDataContext dao = new AdminDataContext()) {
                //var entity = dao.recruit.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<recruit>("b_id", Convert.ToInt32(PrimaryKey));

                b_completed.Checked = entity.b_completed;
				if(entity.b_date_delete != null) {
					b_date_delete.Text = "지원자 삭제 일시 : " + Convert.ToDateTime(entity.b_date_delete).ToString("yyyy-MM-dd HH:mm");
				}
				b_timeless.Checked = entity.b_timeless;
				b_date_begin.Text = entity.b_date_begin.ToString("yyyy-MM-dd HH:mm");
				b_date_end.Text = entity.b_date_end.ToString("yyyy-MM-dd HH:mm");
				b_title.Text = entity.b_title;
				b_content.InnerHtml = entity.b_content;
				b_display.Checked = entity.b_display;
				b_regdate.Text = entity.b_regdate.ToString("yyyy-MM-dd");

				if (entity.b_timeless)
					b_date_end.Text = b_timeless.Value;

                // job
                //this.BoardJob = dao.recruit_job.Where(p => p.bj_b_id == entity.b_id).ToList();
                this.BoardJob = www6.selectQ<recruit_job>("bj_b_id", entity.b_id);
                this.ShowJob();

			}
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			base.FileLoad();
			this.ShowFiles();


		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var arg = new recruit() {
			b_title = b_title.Text, b_content = b_content.InnerHtml,
			b_a_id = AdminLoginSession.GetCookie(this.Context).identifier, b_display = b_display.Checked,
			b_regdate = DateTime.Parse(b_regdate.Text), 
			b_date_begin = DateTime.ParseExact(b_date_begin.Text, "yyyy-MM-dd HH:mm", null),
			b_date_end = b_timeless.Checked ? DateTime.MaxValue : DateTime.ParseExact(b_date_end.Text, "yyyy-MM-dd HH:mm", null),
			b_timeless = b_timeless.Checked,
			b_ip = Request.UserHostAddress,
			b_completed = b_completed.Checked
		};


		var fileList = base.GetTemporaryFiles();

        using (AdminDataContext dao = new AdminDataContext())
        {
            if (Action == "update")
            {
                //var entity = dao.recruit.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<recruit>("b_id", Convert.ToInt32(PrimaryKey));

                entity.b_title = arg.b_title;
                entity.b_content = arg.b_content;
                entity.b_a_id = arg.b_a_id;
                entity.b_display = arg.b_display;

                entity.b_hot = arg.b_hot;
                entity.b_main = arg.b_main;
                entity.b_regdate = arg.b_regdate;
                entity.b_date_begin = arg.b_date_begin;
                entity.b_date_end = arg.b_date_end;
                entity.b_meta_keyword = arg.b_meta_keyword;
                entity.b_meta_desc = arg.b_meta_desc;
                entity.b_target = arg.b_target;
                entity.b_timeless = arg.b_timeless;

                // 종료가 아니였던 경우만 종료로 변경하면 종료일 변경
                if (b_completed.Checked && !entity.b_completed)
                {
                    entity.b_completed = arg.b_completed;
                    entity.b_date_delete = DateTime.Now.AddDays(5);
                }

                // 데이터 삭제 전까지만 체크 해제 가능
                if (!b_completed.Checked && entity.b_completed)
                {
                    if (entity.b_date_delete < DateTime.Now)
                    {
                        b_completed.Checked = true;
                        Master.ValueMessage.Value = "데이터 삭제일이 지났습니다. 마감을 해제할 수 없습니다.";
                        return;
                    }
                    else
                    {
                        entity.b_date_delete = null;
                        entity.b_completed = false;
                    }
                }

                www6.update(entity);
                Master.ValueMessage.Value = "수정되었습니다.";

            }
            else
            {
                arg.b_hits = 0;
                //dao.recruit.InsertOnSubmit(arg);
                www6.insert(arg);
                //dao.SubmitChanges();

                PrimaryKey = arg.b_id.ToString();

                foreach (var file in fileList)
                {
                    file.f_ref_id = PrimaryKey;
                }
                SaveTemporaryFiles(fileList);

                Master.ValueMessage.Value = "등록되었습니다.";
            }

            // category
            //dao.recruit_job.DeleteAllOnSubmit(dao.recruit_job.Where(p => p.bj_b_id == Convert.ToInt32(PrimaryKey)));
            //www6.cud(MakeSQL.delQ(0, "recruit_job", "bj_b_id", Convert.ToInt32(PrimaryKey)));
            var list = www6.selectQ<recruit_job>("bj_b_id", Convert.ToInt32(PrimaryKey));
            www6.delete(list);

            var boardjob = this.BoardJob;
            for (int i = 0; i < boardjob.Count; i++)
            {
                var c = boardjob[i];
                //dao.recruit_job.InsertOnSubmit(new recruit_job()
                var entity = new recruit_job()
                {
                    bj_b_id = Convert.ToInt32(PrimaryKey),
                    bj_regdate = DateTime.Now,
                    bj_rj_depth1 = c.bj_rj_depth1,
                    bj_rj_depth2 = c.bj_rj_depth2
                };
                www6.insert(entity);
            }

            // file
            //dao.file.DeleteAllOnSubmit(dao.file.Where(p => p.f_ref_id == PrimaryKey && p.f_group == FileGroup.ToString()));
            //www6.cud(MakeSQL.delQ(0, "file", "f_ref_id", PrimaryKey, "f_group", FileGroup.ToString()));
            var list2 = www6.selectQ<file>("f_ref_id", PrimaryKey, "f_group", FileGroup.ToString());
            www6.delete(list2);


            //if (fileList.Count > 0)
            //{
            //    dao.file.InsertAllOnSubmit(fileList);
            //}
            foreach (var fi in fileList)
            {
                www6.insert(fi);
            }

            //dao.SubmitChanges();
        }

		Master.ValueAction.Value = "list";
		
	}

	// file
	void ShowFiles() {

		repeater.DataSource = base.GetTemporaryFiles();
		repeater.DataBind();
	}

	protected void btn_remove_file_click(object sender, EventArgs e) {
		base.FileRemove(sender);
		this.ShowFiles();
	}

	protected void btn_remove_click(object sender, EventArgs e)
    {
        using (AdminDataContext dao = new AdminDataContext())
        {
            //var entity = dao.recruit.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
            //dao.recruit.DeleteOnSubmit(entity);
            //www6.cud(MakeSQL.delQ(0, "recruit", "b_id", Convert.ToInt32(PrimaryKey)));
            var entity = www6.selectQF<recruit>("b_id", Convert.ToInt32(PrimaryKey));
            www6.delete(entity);
            //var resumeList = dao.resume.Where(p => p.r_b_id == Convert.ToInt32(PrimaryKey));
            //dao.resume.DeleteAllOnSubmit(resumeList);
            //www6.cud(MakeSQL.delQ(0, "resume", "r_b_id", Convert.ToInt32(PrimaryKey)));
            var resumeList = www6.selectQ<resume>("r_b_id", Convert.ToInt32(PrimaryKey));
            www6.delete(entity);
            //dao.SubmitChanges();
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		
		base.FileAdd(Convert.ToInt32(temp_file_size.Value), temp_file_name.Value);
		this.ShowFiles();
		
	}

	// job
	void ShowJob() {

		jobs.DataSource = this.BoardJob.Select(p => new recruit_job_without_location() {
			bj_rj_depth1 = p.bj_rj_depth1,
			bj_rj_depth2 = p.bj_rj_depth2
		}).Distinct(new recruit_job_without_location.Comparer());
		jobs.DataBind();

	}
	protected void btn_remove_job_Click(object sender, EventArgs e) {
		LinkButton obj = sender as LinkButton;
		var bj_rj_depth1 = obj.CommandArgument.Split('_')[0];
		var bj_rj_depth2 = obj.CommandArgument.Split('_')[1];

		var list = this.BoardJob;
		if (list.Any(p => p.bj_rj_depth1 == bj_rj_depth1 && p.bj_rj_depth2 == bj_rj_depth2)) {
			list.RemoveAll(p => p.bj_rj_depth1 == bj_rj_depth1 && p.bj_rj_depth2 == bj_rj_depth2);
		}

		this.BoardJob = list;

		this.ShowJob();
	}
	protected void jobs_ItemDataBound(object sender, RepeaterItemEventArgs e) {
		recruit_job_without_location entity = e.Item.DataItem as recruit_job_without_location;

		var boardjob = this.BoardJob;

		var depth1 = Jobcode.FirstOrDefault(p => p.rj_depth1 == entity.bj_rj_depth1 && p.rj_depth2 == "");
		var depth2 = Jobcode.FirstOrDefault(p => p.rj_depth1 == entity.bj_rj_depth1 && p.rj_depth2 == entity.bj_rj_depth2);

		var text = string.Format("{0}-{1}",
			depth1 == null ? "" : depth1.rj_name,
			depth2 == null ? "" : depth2.rj_name
		);
		((Literal)e.Item.FindControl("rj_name")).Text = text;
	}

	protected void btn_add_job_Click(object sender, EventArgs e) {
		var list = this.BoardJob;


		if (!list.Any(p => p.bj_rj_depth1 == bj_rj_depth1.SelectedValue && p.bj_rj_depth2 == bj_rj_depth2.SelectedValue && p.bj_b_id == Convert.ToInt32(PrimaryKey))) {

			list.Add(new recruit_job() {
				bj_b_id = Convert.ToInt32(PrimaryKey),
				bj_rj_depth1 = bj_rj_depth1.SelectedValue,
				bj_rj_depth2 = bj_rj_depth2.SelectedValue,
				bj_regdate = DateTime.Now
			});
		}

		this.BoardJob = list;
		this.ShowJob();
	}

	protected void bj_rj_depth1_SelectedIndexChanged(object sender, EventArgs e) {

		bj_rj_depth2.Items.Clear();

		var list = this.Jobcode.Where(p => p.rj_depth1 == bj_rj_depth1.SelectedValue && p.rj_depth2 != "").OrderBy(p => p.rj_order);
		if (list.Count() > 0) {
			foreach (var a in list) {
				bj_rj_depth2.Items.Add(new ListItem(a.rj_name, a.rj_depth2));
			}

			bj_rj_depth2.Visible = true;
		} else {
			bj_rj_depth2.Visible = false;
		}

	}

}