﻿<%@ WebHandler Language="C#" Class="api_board" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_board : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();
        if(t == "count") {
            this.Count(context);
        }
    }


    void Count(HttpContext context) {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        var b_id = context.Request["b_id"].EmptyIfNull();

        JsonWriter result = new JsonWriter();
        result.success = true;

        //using (FrontDataContext dao = new FrontDataContext()) {

        string dbName = "SqlCompassionWeb";
        object[] objSql = new object[1] { "SELECT count(*) as count from resume where r_b_id = '" + Convert.ToInt32(b_id) +"'" };
        string dbType = "Text";
        DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, null, null);
        int count = (int)ds.Tables[0].Rows[0]["count"];

        //var count = dao.resume.Count(p => p.r_b_id == Convert.ToInt32(b_id));

        result.data = count;
        //}
        JsonWriter.Write(result, context);
    }
}