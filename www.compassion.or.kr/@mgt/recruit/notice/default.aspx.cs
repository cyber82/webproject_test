﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
using System.Data;


public partial class admin_recruit_notice_default : AdminBasePage {

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();
		
		base.sortColumn = sortColumn;
		base.sortDirection = sortDirection;

		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;
	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page) {

        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        Master.IsSearch = false;
		if (s_keyword.Text != "" || s_b_date.Text != "" || s_e_date.Text != "" || s_display.SelectedValue != "-1" || s_display.SelectedValue != "-1") {
			Master.IsSearch = true;
		}
        
		using (AdminDataContext dao = new AdminDataContext()) {


			//var list = dao.sp_recruit_list(page, paging.RowsPerPage, s_keyword.Text, Convert.ToInt32(s_display.SelectedValue), "무관", s_b_date.Text, s_e_date.Text).ToList();

			var total = 0;

            string dbName = "SqlCompassionWeb";
            object[] objSql = new object[1] { "sp_recruit_list" };
            string dbType = "SP";
            object[] objParam = new object[7] { "page", "rowsPerPage", "keyword", "display", "target", "startdate", "enddate" };
            object[] objValue = new object[7] { page, paging.RowsPerPage, s_keyword.Text, Convert.ToInt32(s_display.SelectedValue), "무관", s_b_date.Text, s_e_date.Text };
            DataSet ds = _www6Service.NTx_ExecuteQuery(dbName, objSql, dbType, objParam, objValue);
            List<sp_recruit_listResult> list = ds.Tables[0].DataTableToList<sp_recruit_listResult>();

            if (list.Count > 0)
				total = list[0].total.Value;

			lbTotal.Text = total.ToString();

			paging.CurrentPage = page;
			paging.Calculate(total);
			repeater.DataSource = list;
			repeater.DataBind();

		}
	}


	protected void ListBound(object sender, RepeaterItemEventArgs e) {


		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {


				sp_recruit_listResult entity = e.Item.DataItem as sp_recruit_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
				((Literal)e.Item.FindControl("lbDisplay")).Text = entity.b_display ? "노출" : "미노출";
				((Literal)e.Item.FindControl("lbDateEnd")).Text = entity.b_timeless ? "채용시까지" : entity.b_date_end.ToString("yyyy-MM-dd");
				((Literal)e.Item.FindControl("lbStatus")).Text = entity.b_completed ? "채용마감" : entity.b_date_end.CompareTo(DateTime.Now) > -1 ? "진행중" : "접수마감";
			}
		}
	}

}
