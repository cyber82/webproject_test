﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="admin_recruit_notice_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/@mgt/common/js/initEditor.js"></script>
	<script type="text/javascript">
		$(function () {
		    image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.file_recruit_notice)%>";
			initEditor(oEditors, "b_content");

			var uploader = attachUploader("btn_upload");
			uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.file_recruit_notice)%>";
		    uploader._settings.data.rename = "y";
            

		    $('#b_timeless').on('ifClicked', function (event) {
			    var checked = $(this).prop("checked");
				if (checked) {
				    $("#b_date_end").val("");
				} else {
				    $("#b_date_end").val($(this).val());
				}

			})

		    $("#b_date_begin , #b_date_end").focus(function () {
		        $(".datetimerange").trigger("click");
		        $(".datetimerange").focus();
		        return false;
		    })


		    $("#btn_remove").click(function () {
		        $.get("update.ashx?t=count&b_id=" + $("#b_id").val(), function (r) {
		            if (r.data > 0) {
		                if (confirm(r.data + "명의 지원자가 있습니다.\n삭제하시겠습니까?")) {
		                    eval($("#hidden_btn_remove").attr("href").replace("javascript:", ""));
		                }
		            } else {
		                if (confirm("삭제하시겠습니까?")) {
		                    eval($("#hidden_btn_remove").attr("href").replace("javascript:", ""));
		                }
		            }
		        })
		        return false;

		    });

		});

		var attachUploader = function (button) {
			return new AjaxUpload(button, {
				action: '/common/handler/upload',
				responseType: 'json',
				onChange: function () {
				    // 이미지 업로드 되면 에디터 초기화 되는 현상 방지
				    // oEditors가 없는 경우 에러남
				    try {
				        if (oEditors) {
				            $.each(oEditors, function () {
				                this.exec("UPDATE_CONTENTS_FIELD", []);
				            });
				        }
				    } catch (e) { }
				},
				onSubmit: function (file, ext) {
					this.disable();
				},
				onComplete: function (file, response) {

					this.enable();

					if (response.success) {
						
						var c = $("#" + button).attr("class").replace(" " ,"");
						$(".temp_file_type").val(c.indexOf("img_1") > -1 ? "thumb" : "file");
						$(".temp_file_name").val(response.name);
						$(".temp_file_size").val(response.size);

						eval($(".btn_update_temp_file").attr("href").replace("javascript:", ""));

					} else
						alert(response.msg);
				}
			});
		}

		var onSubmit = function () {

		    if (!validateForm([
				{ id: "#b_date_begin", msg: "시작일을 입력하세요" },
				{ id: "#b_date_end", msg: "종료일을 입력하세요" }
			])) {
				return false;
			}

			if ($(".btnJob").length < 1) {
			    alert("선택버튼을 눌러 모집부문를 추가하세요.");
			    $("#bj_rj_depth1").focus();
			    return false;
			}
			if (!validateForm([
				{ id: "#b_title", msg: "모집분야을 입력하세요" }
			])) {
			    return false;
			}


			oEditors.getById["b_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


	    var saveContent = function () {
	        oEditors.getById["b_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.
	        return true;
	    }

	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>
    
    <input type="hidden" runat="server" id="b_id" />
    <asp:LinkButton runat=server ID=hidden_btn_remove OnClick="btn_remove_click"></asp:LinkButton>


    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="title_section_1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">

					<style type="text/css">
						#b_display label{margin-left:5px;width:50px}
					</style>


					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="b_display" cssClass="form-control1" Checked="true" />
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">채용마감</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="b_completed" cssClass="form-control1" Checked="false" />
                            *합격자 선정이 끝나고 나서 체크해 주십시오<br />

                            <asp:Literal runat="server" ID="b_date_delete"></asp:Literal>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">기간</label>
						<div class="col-sm-10">
                            <!--
							<button class="btn btn-primary btn-sm datetimerange" data-from="b_date_begin" data-end="b_date_end"><i class="fa fa-calendar"></i></button>
                            -->
							<asp:TextBox runat="server" ID="b_date_begin" Width="150px" CssClass="datetime form-control inline" Style="margin-right: 5px;"></asp:TextBox>
								~
							<asp:TextBox runat="server" ID="b_date_end" Width="150px" CssClass="datetime form-control inline" Style="margin-right: 5px;"></asp:TextBox>
                            <input type="checkbox" runat="server" id="b_timeless" value="채용시까지" />채용시까지
						</div>
					</div>
                    
			        <asp:PlaceHolder runat=server ID=pnRegdate Visible=false>
					    <div class="form-group">
						    <label class="col-sm-2 control-label control-label">등록일</label>
						    <div class="col-sm-10">
							    <asp:TextBox runat=server ID=b_regdate CssClass="form-control b_regdate" Width=200></asp:TextBox>
						    </div>
					    </div>
                    </asp:PlaceHolder>


                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">모집부문</label>
						<div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="bj_rj_depth1" CssClass="form-control" style="width:150px;float:left;" OnSelectedIndexChanged="bj_rj_depth1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
							<asp:DropDownList runat="server" ID="bj_rj_depth2" CssClass="form-control" style="width:150px;float:left;margin-left:5px;"></asp:DropDownList>

                            <asp:LinkButton runat=server ID=btn_add_job OnClick="btn_add_job_Click" class="btn btn-default btn-sm" style="margin-left:5px;margin-top:2px;" OnClientClick="return saveContent()">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 선택
                            </asp:LinkButton>


                        </div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label"></label>
						<div class="col-sm-10">
                            <asp:Repeater runat="server" ID="jobs" OnItemDataBound="jobs_ItemDataBound">
							    <ItemTemplate>
								    <asp:LinkButton runat=server OnClientClick="return saveContent()" ID=btn_remove_job OnClick="btn_remove_job_Click" CommandArgument=<%#Eval("bj_rj_depth1")+"_"+Eval("bj_rj_depth2")%> class="btnJob btn btn-default" style="width:auto"><asp:Literal runat="server" ID="rj_name"></asp:Literal></asp:LinkButton>
							    </ItemTemplate>
						    </asp:Repeater>
                        </div>
					</div>
                    

					<div class="form-group">
						<label class="col-sm-2 control-label control-label">모집분야</label>
						<div class="col-sm-10">
							<asp:TextBox runat=server ID=b_title CssClass="form-control b_title" Width=600></asp:TextBox>
						</div>
					</div>

                    


					
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10">
							<textarea name="b_content" id="b_content" runat="server" style="width:600px; height:212px; display:none;"></textarea>
                        </div>
					</div>
                    
					<div class="form-group" style="display:none;">
						<label class="col-sm-2 control-label control-label">파일</label>
						<div class="col-sm-10">
							
                            <button type="button" class="btn btn-default btn-xs" id="btn_upload" style="margin-top:5px;margin-bottom:2px;">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 파일추가
                            </button><br />

					        <asp:Repeater runat=server ID=repeater OnItemDataBound=FileListBound>
						        <ItemTemplate>
							        <asp:LinkButton runat=server ID=btn_remove_file CssClass="btn btn-default btn-xs" Style="margin-bottom:2px" CommandArgument=<%#Eval("f_name")%> OnClick="btn_remove_file_click"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 삭제</asp:LinkButton>
							        <asp:HyperLink runat=server ID=lnFileName><asp:Literal runat=server ID=fileName></asp:Literal></asp:HyperLink>
							        | <asp:Literal runat=server ID=fileSize></asp:Literal>
							        | <asp:Literal runat=server ID=regdate></asp:Literal>
						
							        <br />
						        </ItemTemplate>
					        </asp:Repeater>	


                        </div>
					</div>
				</div>
			</div>

		
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->

	
	 <div class="box-footer clearfix text-center">
         
		<a id=btn_remove runat="server" class="btn btn-danger pull-right ">삭제</a>
		<asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">업데이트</asp:LinkButton>
		<a id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
	</div>
</asp:Content>