﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class admin_recruit_code_job_default : AdminBasePage {

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);
		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;
	}

	protected override void OnAfterPostBack() {
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page) {

        using (AdminDataContext dao = new AdminDataContext())
        {

            string where = "1=1";

            if (s_display.SelectedValue.Trim() != "")
                where += string.Format(" and rj_display={0}", s_display.SelectedValue);

            string query = string.Format("select * from recruit_jobcode where {0} order by rj_depth1 asc , rj_order asc", where);
            //var list = dao.ExecuteQuery<recruit_jobcode>(query);
            var list = www6.selectText(query).DataTableToList<recruit_jobcode>();

            int totalCount = list.Count; //dao.ExecuteQuery<recruit_jobcode>(query).Count();
            lbTotal.Text = totalCount.ToString();

            paging.Calculate(totalCount);
            repeater.DataSource = list.Skip((page - 1) * paging.RowsPerPage).Take(paging.RowsPerPage);
            repeater.DataBind();

        }
	}


	protected void ListBound(object sender, RepeaterItemEventArgs e) {

		recruit_jobcode entity = e.Item.DataItem as recruit_jobcode;
		((CheckBox)e.Item.FindControl("rj_display")).Checked = entity.rj_display;

        using (AdminDataContext dao = new AdminDataContext())
        {
            //var count = entity.rj_depth2 == "" ? 
            //	dao.recruit_job.Where(p => p.bj_rj_depth1 == entity.rj_depth1).Select(p=>p.bj_b_id).Distinct().Count() : 
            //	dao.recruit_job.Where(p => p.bj_rj_depth1 == entity.rj_depth1 && p.bj_rj_depth2 == entity.rj_depth2).Select(p=>p.bj_b_id).Distinct().Count();

            var count = 0;
            if (entity.rj_depth2 == "")
            {
                //var count = dao.recruit_job.Where(p => p.bj_rj_depth1 == entity.rj_depth1).Select(p => p.bj_b_id).Distinct().Count() : 
                count = www6.selectQ<recruit_job>("bj_rj_depth1", entity.rj_depth1).Select(p => p.bj_b_id).Distinct().Count();
            }
            else
            {
                //var count = dao.recruit_job.Where(p => p.bj_rj_depth1 == entity.rj_depth1 && p.bj_rj_depth2 == entity.rj_depth2).Select(p => p.bj_b_id).Distinct().Count();
                count = www6.selectQ<recruit_job>("bj_rj_depth1", entity.rj_depth1, "bj_rj_depth2", entity.rj_depth2).Select(p => p.bj_b_id).Distinct().Count();
            }

            ((Literal)e.Item.FindControl("lbContentCount")).Text = count.ToString();
            ((LinkButton)e.Item.FindControl("btn_remove")).Attributes["data-count"] = count.ToString();
            //((LinkButton)e.Item.FindControl("btn_remove")).Visible = count < 1;


        }

	}


	protected void btn_update_Click(object sender, EventArgs e) {

		using (AdminDataContext dao = new AdminDataContext()) {

            //var list = dao.recruit_jobcode;
            string sqlStr = string.Format("select * from recruit_jobcode");
            var list = www6.selectQ<recruit_jobcode>();

            foreach (RepeaterItem item in repeater.Items) {
				var rj_id = Convert.ToInt32(((LinkButton)item.FindControl("btn_remove")).CommandArgument);
				var rj_depth1 = ((TextBox)item.FindControl("rj_depth1")).Text;
				var rj_depth2 = ((TextBox)item.FindControl("rj_depth2")).Text;
				if (rj_depth2 == "2단계코드 입력") rj_depth2 = "";
				var rj_name = ((TextBox)item.FindControl("rj_name")).Text;
				var rj_display = ((CheckBox)item.FindControl("rj_display")).Checked;
				var rj_order = Convert.ToInt32(((TextBox)item.FindControl("rj_order")).Text);
				
				var entity = list.First(p => p.rj_id == rj_id);
				entity.rj_name = rj_name;
				entity.rj_depth1 = rj_depth1;
				entity.rj_depth2 = rj_depth2;
				entity.rj_display = rj_display;
				entity.rj_order = rj_order;

                //string wClause = string.Format("rj_id = {0}", rj_id);
                www6.update(entity);
            }

			//dao.SubmitChanges();
		}

		Master.ValueMessage.Value = "수정되었습니다.";
		GetList(1);
	}

	protected void btn_add_Click(object sender, EventArgs e) {
		using (AdminDataContext dao = new AdminDataContext()) {

			var depth2 = new_rj_depth2.Text == "2단계코드 입력" ? "" : new_rj_depth2.Text;
            
            var exist = www6.selectQ<recruit_jobcode>("rj_depth1", new_rj_depth1.Text, "rj_depth2", depth2);
            //if (dao.recruit_jobcode.Any(p => p.rj_depth1 == new_rj_depth1.Text && p.rj_depth2 == depth2))
            if (exist.Any())
            {
				Master.ValueMessage.Value = "동일한 키가 사용중입니다.";
			}
            else
            {

				var entity = new recruit_jobcode() {
					rj_depth1 = new_rj_depth1.Text,
					rj_depth2 = depth2,
					rj_name = new_rj_name.Text,
					rj_order = 100,
					rj_regdate = DateTime.Now,
					rj_display = false,
				};

                //dao.recruit_jobcode.InsertOnSubmit(entity);
                //dao.SubmitChanges();
                www6.insert(entity);

                Master.ValueMessage.Value = "등록되었습니다.";
				GetList(1);

			}

		}

	}
	

	protected void btn_remove_Click(object sender, EventArgs e) {
		LinkButton obj = sender as LinkButton;
		var rj_id = Convert.ToInt32(obj.CommandArgument);
		using (AdminDataContext dao = new AdminDataContext()) {
            //var entity = dao.recruit_jobcode.First(p => p.rj_id == rj_id);
            //dao.recruit_jobcode.DeleteOnSubmit(entity);
            //dao.SubmitChanges();

            string delStr = string.Format("delete from recruit_jobcode where rj_id = {0}", rj_id);
            www6.cud(delStr);
        }

		Master.ValueMessage.Value = "삭제되었습니다.";
		GetList(1);
	}

}
