﻿<%@ WebHandler Language="C#" Class="admin_common_api_naverApi" %>

using System;
using System.Web;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using drawing = System.Drawing;
using NLog;
using System.Net;

public class admin_common_api_naverApi : IHttpHandler {


    public class GPS_PARAMS {

        public double? lat;

        public double? lng;
    }


    public void ProcessRequest(HttpContext context) {


        if (!AdminLoginSession.HasCookie(context))
            return;

        var type = context.Request["t"];

        if (type == "geocode") {
            JsonWriter.Write(this.GetGeocode(context), context);
        }else if(type == "reversegeocode"){
            JsonWriter.Write(this.ReverseGeocode(context), context);
        }

    }



    string GetGeocode(HttpContext context) {
            
        var address = context.Request["k"];

        string result = "";

        string clientId = ConfigurationManager.AppSettings["naver_app_id"];
        string clientSecret = ConfigurationManager.AppSettings["naver_app_secret"];
        string url = string.Format(ConfigurationManager.AppSettings["naver_geocode_url"], address);



        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

        req.Headers.Add("X-Naver-Client-Id", clientId);
        req.Headers.Add("X-Naver-Client-Secret", clientSecret);

        HttpWebResponse res;
        try{
            res = req.GetResponse() as HttpWebResponse;
        }catch (WebException ex){
            res = ex.Response as HttpWebResponse;
        }


        using (res) {
            using (StreamReader sr = new StreamReader(res.GetResponseStream())) {
                result = sr.ReadToEnd();
            }
        }

        return result;
    }


        

    string ReverseGeocode(HttpContext context) {
        var query = context.Request["k"];

        string result = "";

        string clientId = ConfigurationManager.AppSettings["naver_app_id"];
        string clientSecret = ConfigurationManager.AppSettings["naver_app_secret"];
        string url = string.Format(ConfigurationManager.AppSettings["naver_reversegeocode_url"], query);



        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

        req.Headers.Add("X-Naver-Client-Id", clientId);
        req.Headers.Add("X-Naver-Client-Secret", clientSecret);

        HttpWebResponse res;
        try{
            res = req.GetResponse() as HttpWebResponse;
        }catch (WebException ex){
            res = ex.Response as HttpWebResponse;
        }


        using (res) {
            using (StreamReader sr = new StreamReader(res.GetResponseStream())) {
                result = sr.ReadToEnd();
            }
        }

        return result;
    }

    public bool IsReusable {
        get {
            return false;
        }
    }
}