﻿
var msg_ko = {
	
	fc_yyyy_mm_dd: "년(4자리).월(2자리).일(2자리) 포맷으로 입력해주세요",
	fc_yyyy_mm: "년(4자리).월(2자리) 포맷으로 입력해주세요",
	fc_yyyy: "년(4자리) 포맷으로 입력해주세요",
	fc_mm: "월(2자리) 포맷으로 입력해주세요",
	fc_dd: "일(2자리) 포맷으로 입력해주세요",
	cm_submit: "전송하시겠습니까? ",
	cm_exception: "예상치 못한 장애가 발생했습니다. 잠시후 다시 이용해주세요.",
	cm_msg_auth_required: "로그인이 필요한 서비스입니다.",
	cm_msg_updated: "수정되었습니다.",
	cm_empty_search_word: "  검색어를 입력하세요",
	cm_require_login: "로그인이 필요한 서비스입니다.\n로그인 페이지로 이동하시겠습니까?",
	fc_numeric: "숫자만 입력 가능합니다.",
	fc_decimal: "숫자와 '.'만 입력 가능합니다.",
	fc_alpha_numeric: "영문,숫자만 입력 가능합니다.",
	fc_alphabet: "영문만 입력 가능합니다.",
	fc_phone: "+-숫자만 입력 가능합니다.",
	fc_email: "올바른 이메일 형식이 아닙니다.",
	fc_url: "올바른 URL형식이 아닙니다.",
	fc_nodata: "값을 입력하세요",
	fc_pwd_at_least_4digit: "최소 4자리 이상 입력해주세요"

}

var msg = function (key) {
	
	return eval('msg_ko.' + key);
	
}
