﻿
var image_path = "";

var getImagePath = function () {
	return image_path;
}

var oEditors = [];
// 추가 글꼴 목록
//var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];

var initEditor = function (ref, holder) {
	nhn.husky.EZCreator.createInIFrame({
		oAppRef: ref,
		elPlaceHolder: holder,
		sSkinURI: "/common/smartEditor/SmartEditor2Skin.html",
		htParams: {
			bUseToolbar: true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseVerticalResizer: true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseModeChanger: true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
			//aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
			fOnBeforeUnload: function () {
				//alert("완료!");
			}
		}, //boolean
		fOnAppLoad: function () {
			//oEditors.getById["content"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
		},
		fCreator: "createSEditor2"
	});
}