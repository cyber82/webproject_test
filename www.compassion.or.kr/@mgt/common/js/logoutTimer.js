var logoutTimer = {
	options: {
		minute: 10,
		cookieName: "kdmall.admin.user",
		bindEvent: "resize mousemove scroll click dblclick"
	},

	checker: null,
	clock: null,
	seconds: 0,
	limit: 0,
	init: function () {
		return;
		this.limit = 1000 * 60 * this.options.minute;
		this.seconds = 60 * this.options.minute;

		if (this.getCookie(this.options.cookieName) != "") {
			this.start();
			this.clock = setTimeout(logoutTimer.timer, 1000);
		}


		$("html").bind(logoutTimer.options.bindEvent, function (e) {
			logoutTimer.reset();
		});

	},
	start: function () {

		this.checker = setTimeout(logoutTimer.logout, logoutTimer.limit);

	},

	reset: function () {
		clearTimeout(logoutTimer.checker);
		//clearTimeout(logoutTimer.clock);
		$(".logout-timer").hide();
		this.seconds = 60 * this.options.minute;
		this.start();
	},

	logout: function () {
	//	console.log("logout");
		clearTimeout(logoutTimer.clock);
		clearTimeout(logoutTimer.checker);
		location.href = "/admin/logout.aspx";
	},

	timer: function () {
		this.clock = setTimeout(logoutTimer.timer, 1000);

		if (logoutTimer.seconds-- <= 60) {
			$(".logout-timer > b").text(logoutTimer.seconds);
			$(".logout-timer").show();
		}
		
		//console.log(logoutTimer.seconds);
	},

	getCookie: function (cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
		}
		return "";
	}
}

logoutTimer.init();

