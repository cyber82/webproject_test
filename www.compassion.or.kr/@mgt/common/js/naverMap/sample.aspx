﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<script type="text/javascript" src="/assets/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/jquery/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="http://openapi.map.naver.com/openapi/naverMap.naver?ver=2.0&key=<%=ConfigurationManager.AppSettings["naver_app_id"] %>"></script>
	<script type="text/javascript" src="widget.ui.naverMap.js"></script>
	<script type="text/javascript">

		$(function () {

			var option = {
				title: "제목입니다.",
				lat: 37.384294, lng: 127.120264,
				w: 500, h: 400,
				zoom: 12
			}
			$("#map").naverMap(option);

			/*
			$("#addr").focusout(function () {

			});
			*/

		});
</script>

</head>
<body>
    <form id="form1" runat="server">
    <div>

		<div id="map" style="border:1px solid #000;"></div>
    </div>
    </form>
</body>
</html>
