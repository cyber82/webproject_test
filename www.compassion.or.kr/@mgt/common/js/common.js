﻿var goBack = function () {
	history.back();
}

var goList = function () {
	location.href = $("#btnList").attr("href");
}

$(function () {

	$("input").keydown(function (e) {
		if (e.keyCode == 13 ) {
			return false;
		}
		return true;
	})

	$("input.form-control").keydown(function (e) {
		if (e.keyCode == 13 && $("#btn_search").length > 0) {
			eval($("#btn_search").attr("href").replace("javascript:", ""));
		}
	})

	$('.date').datepicker({
		orientation: 'bottom',
		language: 'kr',
		format: 'yyyy-mm-dd',
		todayHighlight : true ,
	}).on("changeDate", function (e) {
		
		if ($(".btn_date_search").length > 0) {
			eval($(".btn_date_search").attr("href").replace("javascript:", ""));
		}
		$(this).datepicker('hide');
		
	});

	$(".date").keydown(function (e) {

		if (e.keyCode == 8) {
			$(this).val("");
			$(this).datepicker("hide");
		}
		return false;
	});


	$('.datetime').daterangepicker({

		singleDatePicker: true,
		showDropdowns: true,
		timePicker: true,

		format: 'YYYY-MM-DD HH:mm',
		locale: {
			applyLabel: '적용',
			cancelLabel: '취소',
			fromLabel: '시작일',
			toLabel: '종료일',
			weekLabel: 'W',
			customRangeLabel: '사용자 지정',
			daysOfWeek: ['일', '월', '화', '수', '목', '금', '토'],
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
		},
	},
    function (start, end, label) {
    });



	$('.daterange').daterangepicker({
		ranges: {
			'오늘': [moment(), moment()],
			'어제': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'최근 7일': [moment().subtract(6, 'days'), moment()],
			'최근 30일': [moment().subtract(29, 'days'), moment()],
			'이번달': [moment().startOf('month'), moment().endOf('month')],
			'지난달': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},

		format: 'YYYY-MM-DD',
		locale: {
			applyLabel: '적용',
			cancelLabel: '취소',
			fromLabel: '시작일',
			toLabel: '종료일',
			weekLabel: 'W',
			customRangeLabel: '사용자 지정',
			daysOfWeek: ['일', '월', '화', '수', '목', '금', '토'],
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
		},
		startDate: moment().subtract(29, 'days'),
		endDate: moment()
	}, function (start, end) {

		var el_from = $(this.element).attr("data-from");
		var el_end = $(this.element).attr("data-end");
		if (el_from)
			$("#" + el_from).val(start.format('YYYY-MM-DD'));
		if (el_end)
			$("#" + el_end).val(end.format('YYYY-MM-DD'));

		if ($(".btn_date_search").length > 0) {
			eval($(".btn_date_search").attr("href").replace("javascript:", ""));
		}

	});


	$(".daterange , .datetimerange, .dayrange").click(function () {
		return false;
	})

	if ($("#s_b_date").length > 0 && $("#s_e_date").length > 0) {
		$("#s_b_date").dateRange({
			buttons: ".dateRange",	// preset range
			end: "#s_e_date",
			onClick: function () {
				//eval($(".btn_date_search").attr("href").replace("javascript:", ""));
			}
		});

		$("#s_b_date").dateValidate({
			end: "#s_e_date",
			onSelect: function () {
				//eval($(".btn_date_search").attr("href").replace("javascript:", ""));
			}
		});
	}
	
	$('.datetimerange').daterangepicker({
		timePicker: true, timePickerIncrement: 1, format: 'YYYY-MM-DD HH:mm',
		
		locale: {
			applyLabel: '적용',
			cancelLabel: '취소',
			fromLabel: '시작일',
			toLabel: '종료일',
			weekLabel: 'W',
			customRangeLabel: '사용자 지정',
			daysOfWeek: ['일', '월', '화', '수', '목', '금', '토'],
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
		},

		startDate: moment(),

		endDate: moment().add(7, 'days')

	}, function (start, end) {
		/*
		var el_from = $(this.element).attr("data-from");
		var el_end = $(this.element).attr("data-end");
		if (el_from)
			$("#" + el_from).val(start.format('YYYY-MM-DD HH:mm'));
		if (el_end)
			$("#" + el_end).val(end.format('YYYY-MM-DD HH:mm'));

		if ($(".btn_date_search").length > 0) {
			eval($(".btn_date_search").attr("href").replace("javascript:", ""));
		}
		*/
	});

	$('.datetimerange').on('show.daterangepicker', function (ev,picker) {
		
		var el_from = $(picker.element).attr("data-from");
		var el_end = $(picker.element).attr("data-end");
		
		if (el_from && el_end && $("#" + el_from).val() != "" && $("#" + el_end).val() != "") {
			$(picker.element).data('daterangepicker').setStartDate($("#" + el_from).val());
			$(picker.element).data('daterangepicker').setEndDate($("#" + el_end).val());
		}
			
	});

	$('.datetimerange').on('hide.daterangepicker', function (ev, picker) {
		console.log("hide");
	});

	$('.datetimerange').on('cancel.daterangepicker', function (ev, picker) {
		console.log("cancel");
	});

	$('.datetimerange').on('apply.daterangepicker', function (ev, picker) {
		
		var el_from = $(picker.element).attr("data-from");
		var el_end = $(picker.element).attr("data-end");
		if (el_from)
			$("#" + el_from).val(picker.startDate.format('YYYY-MM-DD HH:mm'));
		if (el_end)
			$("#" + el_end).val(picker.endDate.format('YYYY-MM-DD HH:mm'));

		if ($(".btn_date_search").length > 0) {
			eval($(".btn_date_search").attr("href").replace("javascript:", ""));
		}

		//$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
	});



	$('.dayrange').daterangepicker({
		timePicker: false, timePickerIncrement: 1, format: 'YYYY-MM-DD',

		locale: {
			applyLabel: '적용',
			cancelLabel: '취소',
			fromLabel: '시작일',
			toLabel: '종료일',
			weekLabel: 'W',
			customRangeLabel: '사용자 지정',
			daysOfWeek: ['일', '월', '화', '수', '목', '금', '토'],
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		},

		startDate: moment(),

		endDate: moment().add(7, 'days')

	}, function (start, end) {
	});

	$('.dayrange').on('show.daterangepicker', function (ev, picker) {

		var el_from = $(picker.element).attr("data-from");
		var el_end = $(picker.element).attr("data-end");

		if (el_from && el_end && $("#" + el_from).val() != "" && $("#" + el_end).val() != "") {
			$(picker.element).data('daterangepicker').setStartDate($("#" + el_from).val());
			$(picker.element).data('daterangepicker').setEndDate($("#" + el_end).val());
		}

	});

	$('.dayrange').on('hide.daterangepicker', function (ev, picker) {
		console.log("hide");
	});

	$('.dayrange').on('cancel.daterangepicker', function (ev, picker) {
		console.log("cancel");
	});

	$('.dayrange').on('apply.daterangepicker', function (ev, picker) {

		var el_from = $(picker.element).attr("data-from");
		var el_end = $(picker.element).attr("data-end");
		if (el_from)
			$("#" + el_from).val(picker.startDate.format('YYYY-MM-DD'));
		if (el_end)
			$("#" + el_end).val(picker.endDate.format('YYYY-MM-DD'));

		if ($(".btn_date_search").length > 0) {
			eval($(".btn_date_search").attr("href").replace("javascript:", ""));
		}

		//$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
	});



	/* LNB 1뎁스 아이콘 */
	$.each($("ul.sidebar-menu .depth1-item"), function () {
		var am_code = $(this).attr("data-code");
		var icon = "fa-link";
		if (am_code == "A00000") {		// 관리자 관리
			icon = "fa-user-secret";
		} else if (am_code == "G00000") {		// 고객 서비스
			icon = "fa-user";
		} else if (am_code == "F00000") {		// 회원관리
			icon = "fa-users";
		} else if (am_code == "C00000") {		// 컨텐츠
			icon = "fa-file-text";
		}

		
		
		$(this).addClass(icon);

		
	})


	$(".number_only").keydown(function (e) {

		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
			// Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
			// Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
			// Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}

		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	}).keyup(function (e) {
		var self = $(this);
		if (self.attr("maxlength")) {
			var maxlength = parseInt(self.attr("maxlength"));

			if (self.val().length > maxlength)
				self.val(self.val().slice(0, maxlength));
		}
	});



	// 페이지별 안내문구 닫기 클릭 처리
	if ($("#alert").length > 0 && cookie.get(location.pathname) != null) {
		$("#alert").parent().hide();
	}

	$("#alert").click(function () {
		cookie.set(location.pathname, "1", 120);
	})
})



//script로 추가되는 text에 사용
$.fn.numberOnly = function () {
	this.keydown(function (e) {

	    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	    	// Allow: Ctrl+A
			(e.keyCode == 65 && e.ctrlKey === true) ||
	    	// Allow: Ctrl+C
			(e.keyCode == 67 && e.ctrlKey === true) ||
	    	// Allow: Ctrl+X
			(e.keyCode == 88 && e.ctrlKey === true) ||
	    	// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
	    	// let it happen, don't do anything
	    	return;
	    }

	    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	    	e.preventDefault();
	    }
	}).keyup(function (e) {
	    var self = $(this);
	    if (self.attr("maxlength")) {
	    	var maxlength = parseInt(self.attr("maxlength"));

	    	if (self.val().length > maxlength)
	    		self.val(self.val().slice(0, maxlength));
	    }
	});
	return this;
};
