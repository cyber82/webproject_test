﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="paging.ascx.cs" Inherits="Mgt.Common.Paging" %>

<ul class="pagination pagination-sm no-margin ">
<li class="firstPrev"><asp:LinkButton runat=server ID=first OnClick=Move>&laquo;</asp:LinkButton></li>
<li class="prev"><asp:LinkButton runat=server ID=prev OnClick=Move>&lsaquo;</asp:LinkButton></li>
<asp:PlaceHolder runat=server ID=container></asp:PlaceHolder>	
<li class="next"><asp:LinkButton runat=server ID=next OnClick=Move>&rsaquo;</asp:LinkButton></li>
<li class="lastNext"><asp:LinkButton runat=server ID=last OnClick=Move>&raquo;</asp:LinkButton></li>
</ul>



		