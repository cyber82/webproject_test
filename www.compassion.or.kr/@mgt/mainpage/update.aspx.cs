﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using CommonLib;

public partial class mgt_mainpage_update : AdminBasePage{

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];
		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();
			
		// querystring 유효성 검사 후 문제시 리다이렉트 
		var isValid = new RequestValidator()
			.Add("t", RequestValidator.Type.Alphabet)
			.Validate(this.Context , "default.aspx");

		foreach (var a in StaticData.Code.GetList(this.Context).Where(p => p.cd_group == "visual" && p.cd_display).OrderBy(p => p.cd_order)) {
			var item = new ListItem(a.cd_value, a.cd_key);
			mp_position.Items.Add(item);
		}

		if (base.Action == "update") {

			base.PrimaryKey = Request["c"];

            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.mainpage.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<mainpage>("idx", Convert.ToInt32(PrimaryKey));

                mp_image_link.Text = entity.mp_image_link;
                mp_title.Text = entity.mp_title;
                mp_order.Text = entity.mp_order.ToString();
                mp_display.SelectedValue = entity.mp_display ? "Y" : "N";

                mp_position.SelectedValue = entity.mp_position;
                mp_content.InnerHtml = entity.mp_content;
                mp_type.SelectedValue = entity.mp_type;
                mp_is_new_window.Checked = entity.mp_is_new_window;

                base.Thumb = entity.mp_image;

            }

			this.ShowImage();

			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;

		} else {
			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
			
		}

		mp_type_SelectedIndexChanged(null, null);

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		if(base.IsRefresh) {
			return;
		}

		var thumb = base.Thumb;

		var arg = new mainpage() {
			mp_a_id = AdminLoginSession.GetCookie(this.Context).identifier, 
			mp_display = mp_display.SelectedValue == "Y" , mp_image = this.Thumb , mp_order = Convert.ToInt32(mp_order.Text) ,
			mp_regdate = DateTime.Now, mp_title = mp_title.Text,  mp_position = mp_position.SelectedValue, mp_image_link = mp_image_link.Text , mp_type = mp_type.SelectedValue , 
			mp_content = mp_content.InnerHtml.ToHtml(),
            mp_is_new_window = mp_is_new_window.Checked
		};

        using (AdminDataContext dao = new AdminDataContext())
        {
            if (Action == "update")
            {
                //var entity = dao.mainpage.First(p => p.idx == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<mainpage>("idx", Convert.ToInt32(PrimaryKey));
                entity.mp_a_id = arg.mp_a_id;
                entity.mp_display = arg.mp_display;
                entity.mp_image = arg.mp_image;

                entity.mp_order = arg.mp_order;
                entity.mp_title = arg.mp_title;
                entity.mp_position = arg.mp_position;
                entity.mp_image_link = arg.mp_image_link;
                entity.mp_type = arg.mp_type;
                entity.mp_content = arg.mp_content;
                entity.mp_is_new_window = arg.mp_is_new_window;

                www6.update(entity);
            }
            else
            {
                // .mainpage.InsertOnSubmit(arg);
                www6.insert(arg);
            }

            //dao.SubmitChanges();

        }

		if (base.Action == "update") {
			Master.ValueAction.Value = "list";
			Master.ValueMessage.Value = "수정되었습니다.";
		} else {

			Master.ValueAction.Value = "list";
			Master.ValueMessage.Value = "등록되었습니다.";
		}
		
	}

	protected void btn_remove_click(object sender, EventArgs e) {

		if(base.IsRefresh) {
			return;
		}

        using (AdminDataContext dao = new AdminDataContext())
        {
            //dao.mainpage.DeleteOnSubmit(dao.mainpage.First(p => p.idx == Convert.ToInt32(PrimaryKey)));
            //dao.SubmitChanges();

            string delStr = string.Format("delete from mainpage where idx = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

	void ShowImage() {
		if (!string.IsNullOrEmpty(base.Thumb)) {
			mp_image.Src = base.Thumb.WithFileServerHost();
			mp_image_msg.Text = base.Thumb;
		}
	}

	protected void btn_update_temp_file_click(object sender, EventArgs e) {
		//base.Thumb = string.Format("{0}{1}", Uploader.GetRoot(Uploader.FileGroup.image_mainpage), temp_file_name.Value);
		base.Thumb = temp_file_name.Value;
		this.ShowImage();
	}

	protected void mp_type_SelectedIndexChanged(object sender, EventArgs e) {
		ph_type_image.Visible = ph_type_html.Visible = false;
		if (mp_type.SelectedValue == "image") {
			ph_type_image.Visible = true;
		} else {
			ph_type_html.Visible = true;
		}
	}
}