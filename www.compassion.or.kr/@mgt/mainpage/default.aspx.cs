﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;

public partial class mgt_mainpage_default : AdminBasePage {

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();

		foreach (var a in StaticData.Code.GetList(this.Context).Where(p => p.cd_group == "visual" ).OrderBy(p => p.cd_order)) {
			var item = new ListItem(a.cd_value, a.cd_key);
			s_position.Items.Add(item);
		}

		s_position.Items.Insert(0 , new ListItem("선택하세요", ""));


		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;

	}

	protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page) {

        using (AdminDataContext dao = new AdminDataContext())
        {

            //var list = dao.sp_mainpage_list(page, paging.RowsPerPage, s_position.SelectedValue, Convert.ToInt32(s_display.SelectedValue), s_keyword.Text).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "position", "display", "keyword" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_position.SelectedValue, Convert.ToInt32(s_display.SelectedValue), s_keyword.Text };
            var list = www6.selectSP("sp_mainpage_list", op1, op2).DataTableToList<sp_mainpage_listResult>();


            var total = 0;

            if (list.Count > 0)
                total = list[0].total.Value;

            lbTotal.Text = total.ToString();

            paging.CurrentPage = page;
            paging.Calculate(total);
            repeater.DataSource = list;
            repeater.DataBind();



        }
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e) {
		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {
				sp_mainpage_listResult entity = e.Item.DataItem as sp_mainpage_listResult;
				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
				((Literal)e.Item.FindControl("lbDisplay")).Text = entity.mp_display ? "O" : "X";
				((Literal)e.Item.FindControl("lbPosition")).Text = s_position.Items.FindByValue(entity.mp_position).Text;

			}
		}
	}





}
