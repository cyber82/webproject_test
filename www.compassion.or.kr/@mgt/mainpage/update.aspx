﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="mgt_mainpage_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript">
		var image_path = "";

		var getImagePath = function () {
			return image_path;
		}

		$(function () {
			if ($(".mp_image").length > 0) {
				var uploader = attachUploader($(".mp_image").attr("id"));
				image_path = uploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.image_mainpage)%>";
			} else {

				initEditor(oEditors, "mp_content");

				image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_mainpage)%>";
			}
			
		});


		var oEditors = [];
		// 추가 글꼴 목록
		//var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];

		var initEditor = function (ref, holder) {
			nhn.husky.EZCreator.createInIFrame({
				oAppRef: ref,
				elPlaceHolder: holder,
				sSkinURI: "/common/smartEditor/SmartEditor2Skin.html",
				htParams: {
					bUseToolbar: true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
					bUseVerticalResizer: true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
					bUseModeChanger: true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
					//aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
					fOnBeforeUnload: function () {
						//alert("완료!");
					}
				}, //boolean
				fOnAppLoad: function () {
					//oEditors.getById["content"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
				},
				fCreator: "createSEditor2"
			});
		}
	</script>
	<script type="text/javascript">

		
		
		var attachUploader = function (button) {
			return new AjaxUpload(button, {
				action: '/common/handler/upload',
				responseType: 'json',
				onChange: function () {
				    // 이미지 업로드 되면 에디터 초기화 되는 현상 방지
				    // oEditors가 없는 경우 에러남
				    try {
				        if (oEditors) {
				            $.each(oEditors, function () {
				                this.exec("UPDATE_CONTENTS_FIELD", []);
				            });
				        }
				    } catch (e) { }
				},
				onSubmit: function (file, ext) {
					this.disable();
				},
				onComplete: function (file, response) {

					this.enable();

					if (response.success) {
						$(".temp_file_name").val(response.name);
						$(".temp_file_size").val(response.size);

						eval($(".btn_update_temp_file").attr("href").replace("javascript:", ""));

					} else
						alert(response.msg);
				}
			});
		}

		var onSubmit = function () {

			if (!validateForm([
				{ id: "#mp_title", msg: "제목을 입력하세요" },
				{ id: "#mp_order", type : "numeric" , msg: "노출순서는 숫자만 입력가능합니다." }
			])) {
				return false;
			}

			oEditors.getById["mp_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}

		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}
		
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
	<asp:LinkButton runat=server ID=btn_update_temp_file CssClass=btn_update_temp_file OnClick="btn_update_temp_file_click"></asp:LinkButton>


	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" id="title_section_1" CommandArgument="1" >기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			<div class="active tab-pane" id="tab1" runat="server">

                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="alert"></button>
                    <h4><i class="icon fa fa-check"></i>Alert!</h4>
                    PC 메인 이벤트, PC 캠페인/이벤트 상단 배너 : 1920 * 481px<br />
                    PC 메인 GNB : 204 * 276px<br />
                    PC 나눔펀딩 메인 비주얼 : 2000 * 500px<br />
                    PC 스토어 메인 비주얼 : 980 * 380px<br />
                    모바일 메인 이벤트 : 720 * 740px<br />
                    모바일 스토어 메인 비주얼 : 720 * 480px<br />
                    APP 더보기 하단 배너 : 660 * 185px<br />


                </div>

				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:RadioButtonList runat="server" ID="mp_display" RepeatDirection="Horizontal" RepeatLayout="Flow">
								<asp:ListItem Value="Y" Selected="True" Text="노출&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"></asp:ListItem>
								<asp:ListItem Value="N" Text="미노출"></asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">위치</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:DropDownList runat="server" ID="mp_position" CssClass="selectbox form-control" style="width:400px;">
                                <asp:ListItem>위치를 선택해 주세요</asp:ListItem>
							</asp:DropDownList>				
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">노출순서</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:TextBox runat="server" ID="mp_order" CssClass="mp_order form-control number_only" style="width:400px;" Width="50" Text="0"></asp:TextBox>
							* 숫자만입력 , 낮은숫자가 상위에 노출 
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">제목</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:TextBox runat="server" ID="mp_title" CssClass="mp_title form-control" Width="400" MaxLength="50"></asp:TextBox>
						</div>
					</div>
					<div class="form-group" style="display:none">
						<label class="col-sm-2 control-label">구분</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:RadioButtonList runat="server" ID="mp_type" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="mp_type_SelectedIndexChanged">
								<asp:ListItem Value="image" Text="image&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" Selected="True"></asp:ListItem>
								<asp:ListItem Value="html" Text="html" ></asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>				

					<asp:PlaceHolder runat="server" ID="ph_type_html" Visible="true" >
					<div class="form-group">
						<label class="col-sm-2 control-label">내용</label>
						<div class="col-sm-10" style="margin-top:5px;">
								<textarea name="content" id="mp_content" runat="server" row
									s="10" cols="100" style="width:700px; height:412px; display:none;"></textarea>
						</div>
					</div>
					</asp:PlaceHolder>

					<asp:PlaceHolder runat="server" ID="ph_type_image">
					<div class="form-group">
						<label class="col-sm-2 control-label">이미지(웹)</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<img style="max-width: 100%" id="mp_image" class="mp_image" src="/@mgt/common/img/empty_thumb.png" runat="server" />
							<br />
							<asp:Label runat="server" ID="mp_image_msg"></asp:Label>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">링크</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:TextBox runat="server" ID="mp_image_link" CssClass="mp_image_link form-control" Width="300"></asp:TextBox>
						</div>
					</div>		

					<div class="form-group">
						<label class="col-sm-2 control-label">새창 여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:CheckBox runat="server" ID="mp_is_new_window" Text="새창" />
						</div>
					</div>
					</asp:PlaceHolder>


				</div>
			</div>
		</div>
	</div>

	<div class="box-footer clearfix text-center">
		<asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();" class="btn btn-danger pull-right">삭제</asp:LinkButton>
		<asp:LinkButton runat="server" ID="btn_update" OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
		<a runat="server" id="btnList" class="btn btn-default pull-left">목록</a>
	</div>

</asp:Content>