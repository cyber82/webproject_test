﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CommonLib;
public partial class admin_popup_update : AdminBoardPage {

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		v_admin_auth auth = base.GetPageAuth();
		
		base.PrimaryKey = Request["c"];
		base.Action = Request["t"];

		btnList.HRef = Request.OnlyPath() + "?" + this.ViewState["q"].ToString();

		// querystring 유효성 검사 후 문제시 리다이렉트
		var isValid = new RequestValidator()
			.Add("c", RequestValidator.Type.Numeric)
			.Add("t", RequestValidator.Type.Alphabet)
			.Add("s_main", RequestValidator.Type.AlphaNumeric)
			.Validate(this.Context , "default.aspx");

		foreach (var a in StaticData.Code.GetList(this.Context).Where(p => p.cd_group == "popup_type").OrderBy(p => p.cd_order)) {
			var item = new ListItem(a.cd_value, a.cd_key);
			p_type.Items.Add(item);

		}

		p_type.SelectedValue = "layer";

		if (base.Action == "update")
        {
            using (AdminDataContext dao = new AdminDataContext())
            {
                //var entity = dao.popup.First(p => p.p_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<popup>("p_id", Convert.ToInt32(PrimaryKey));

                p_name.Text = entity.p_name;
                p_content.InnerHtml = entity.p_content;
                p_display.SelectedIndex = entity.p_display ? 0 : 1;
                p_begin.Value = entity.p_begin.ToString("yyyy-MM-dd HH:mm");
                p_end.Value = entity.p_end.ToString("yyyy-MM-dd HH:mm");

                p_type.SelectedValue = entity.p_type;
                p_left.Value = entity.p_left.ToString();
                p_top.Value = entity.p_top.ToString();
                p_width.Value = entity.p_width.ToString();
                p_height.Value = entity.p_height.ToString();
                p_device.SelectedValue = entity.p_device;

            }
			btn_update.Visible = auth.aa_auth_update;
			btn_remove.Visible = auth.aa_auth_delete;
			
		} else {

			btn_update.Visible = auth.aa_auth_create;
			btn_remove.Visible = false;
		}

	}

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
	}

	protected void btn_update_click(object sender, EventArgs e) {

		var arg = new popup() {
			p_name = p_name.Text, p_content = p_content.InnerHtml.ToHtml(), 
			p_a_id = AdminLoginSession.GetCookie(this.Context).identifier, p_display = p_display.SelectedValue == "Y",
			p_regdate = DateTime.Now,
			p_device = p_device.SelectedValue , 
			//p_type = p_type.SelectedValue ,
			p_type = "layer" , 
			p_left = Convert.ToInt32(p_left.Value.ValueIfNull("0")) ,
			p_top = Convert.ToInt32(p_top.Value.ValueIfNull("0")),
			p_width = Convert.ToInt32(p_width.Value.ValueIfNull("0")),
			p_height = Convert.ToInt32(p_height.Value.ValueIfNull("0")), 
			p_begin = DateTime.Parse(p_begin.Value), p_end = DateTime.Parse(p_end.Value)
			
		};

		using (AdminDataContext dao = new AdminDataContext()) {
			if (base.Action == "update") {

                //var entity = dao.popup.First(p => p.p_id == Convert.ToInt32(PrimaryKey));
                var entity = www6.selectQF<popup>("p_id", Convert.ToInt32(PrimaryKey));

                entity.p_a_id = arg.p_a_id;
				entity.p_display = arg.p_display;

				entity.p_begin = arg.p_begin;
				entity.p_end = arg.p_end;
				entity.p_name = arg.p_name;
				entity.p_type = arg.p_type;
				entity.p_content = arg.p_content;
				entity.p_left = arg.p_left;
				entity.p_top = arg.p_top;
				entity.p_width = arg.p_width;
				entity.p_height = arg.p_height;
                entity.p_device = arg.p_device;

				Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "수정되었습니다.";

                //string wClause = string.Format("p_id = {0}", Convert.ToInt32(PrimaryKey));
                www6.update(entity);

            } else {
                //dao.popup.InsertOnSubmit(arg);
                www6.insert(arg);

                Master.ValueAction.Value = "list";
				Master.ValueMessage.Value = "등록되었습니다.";
			}

			//dao.SubmitChanges();
		}
	
	}

	protected void btn_remove_click(object sender, EventArgs e) {

		using (AdminDataContext dao = new AdminDataContext()) {
            //dao.popup.DeleteOnSubmit(dao.popup.First(p => p.p_id == Convert.ToInt32(PrimaryKey)));
            //dao.SubmitChanges();
            //Delete
            string delStr = string.Format("delete from popup where p_id = {0}", Convert.ToInt32(PrimaryKey));
            www6.cud(delStr);
        }

		Master.ValueAction.Value = "list";
		Master.ValueMessage.Value = "삭제되었습니다.";
	}

}
