﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="update.aspx.cs" Inherits="admin_popup_update" MasterPageFile="~/@mgt/mgt_afterlogin.master" ValidateRequest="false"%>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>
<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.dateValidate.js" defer="defer"></script>
	<script type="text/javascript" src="/popup/popupManager.js"></script>
	<script type="text/javascript" defer="defer">

		var image_path = "";
		var getImagePath = function () {
			return image_path;
		}

		$(function () {
			image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_mainpage)%>";

			/*
			$("#p_begin").dateValidate({
				hasTime: true,
				end: "#p_end"
			});

			*/

			if ($("#p_device").val() == "mobile" || $("#p_device").val() == "app") {
				$(".pc_only").hide();

			}

			$("#p_device").change(function () {
				
				var val = $(this).val();
				
				if (val == "mobile" || val == "app") {
					
					$(".pc_only").hide();
				} else {
					$(".pc_only").show();
				}
			})

			$("#btn_preview").click(function () {

				oEditors.getById["p_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

				var data = {
					p_type: $("#p_type").val(),
					p_name: $("#p_name").val(),
					p_content: $("#p_content").val() ,
					p_left: $("#p_left").val(),
					p_top: $("#p_top").val(),
					p_width: $("#p_width").val(),
					p_height: $("#p_height").val(),
					p_id: "temp"
				}
				console.log(data);
			
				popupManager.open(data, false);
				if (data.p_type == "top")
					$("body").scrollTop(0);

				return false;
			});

			initEditor(oEditors, "p_content");

		});


		var image_path = "";

		var getImagePath = function () {
			return image_path;
		}


		var onSubmit = function () {

			oEditors.getById["p_content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.

			if ($("#p_content").val() == "") {
				alert("내용을 입력하세요");
				return false;
			}

			if (!validateForm([
				{ id: "#p_name", msg: "팝업명을 입력하세요" },
				{ id: "#p_content", msg: "내용명을 입력하세요" },
				{ id: "#p_begin", msg: "시작일을 입력하세요" },
				{ id: "#p_end", msg: "종료일을 입력하세요" }

			])) {
				return false;
			}

			if ($("#p_type").val() != "top" && $("#p_device").val() == "pc") {
				if (!validateForm([
					{ id: "#p_left", msg: "위치를 입력하세요", type: "numeric" },
					{ id: "#p_top", msg: "위치를 입력하세요", type: "numeric" },
					{ id: "#p_width", msg: "크기를 입력하세요", type: "numeric" },
					{ id: "#p_height", msg: "크기를 입력하세요", type: "numeric" }

				])) {
					return false;
				}
			}

			return confirm(("<%:base.Action%>" == "update" ? "수정" : "추가") + " 하시겠습니까?");
		}


		var onRemove = function () {
			return confirm("삭제하시겠습니까?");
		}

		</script>

		<script type="text/javascript">

			var oEditors = [];
			// 추가 글꼴 목록
			//var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];

			var initEditor = function (ref, holder) {
				nhn.husky.EZCreator.createInIFrame({
					oAppRef: ref,
					elPlaceHolder: holder,
					sSkinURI: "/common/smartEditor/SmartEditor2Skin.html",
					htParams: {
						bUseToolbar: true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
						bUseVerticalResizer: true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
						bUseModeChanger: true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
						//aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
						fOnBeforeUnload: function () {
							//alert("완료!");
						}
					}, //boolean
					fOnAppLoad: function () {
						//oEditors.getById["content"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
					},
					fCreator: "createSEditor2"
				});
			}
		</script>

	</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">
	
    <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			
			<li runat="server" id="tabm1" class="active"><asp:LinkButton runat="server" CommandArgument="1">기본정보</asp:LinkButton></li>
		
		</ul>
		<div class="tab-content">
			
			<div class="active tab-pane" id="tab1" runat="server">
				<div class="form-horizontal">
					<div class="form-group" style="display:none">
						<label class="col-sm-2 control-label">구분</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:DropDownList runat="server" ID="p_type" CssClass="form-control" style="width:200px">
								<asp:ListItem Value="" Text=""></asp:ListItem>
							</asp:DropDownList>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">PC/모바일</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:DropDownList runat="server" ID="p_device" CssClass="form-control" style="width:200px">
								<asp:ListItem Value="pc" Text="   PC   " Selected="True"></asp:ListItem>
								<asp:ListItem Value="mobile" Text="   모바일"></asp:ListItem>
								<asp:ListItem Value="app" Text="   앱"></asp:ListItem>
							</asp:DropDownList>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">팝업명</label>
						<div class="col-sm-10" style="margin-top:5px;">
							<asp:TextBox runat=server ID=p_name class="form-control inline" Width=300></asp:TextBox>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">노출여부</label>
						<div class="col-sm-10" style="margin-top:5px;">
                            <asp:RadioButtonList runat=server ID=p_display RepeatDirection=Horizontal RepeatLayout=Flow>
								<asp:ListItem Value="Y" Selected=True Text="   노출   "></asp:ListItem>
								<asp:ListItem Value="N"  Text="   미노출"></asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">기간</label>
						<div class="col-sm-10">
							<button class="btn btn-primary btn-sm datetimerange" data-from="p_begin" data-end="p_end"><i class="fa fa-calendar"></i></button>
							<input type="text" name="p_begin" id="p_begin" readonly="readonly" runat="server" class="form-control inline" style="width:150px;margin-right:5px"/>
							~
							<input type="text" name="p_end" id="p_end"  readonly="readonly" runat="server" class="form-control inline" style="width:150px;margin-right: 5px"/>

						</div>
					</div>

                    
                    
					<div class="form-group pc_only">
						<label class="col-sm-2 control-label control-label">위치</label>
						<div class="col-sm-10">
							left : <input id="p_left" name="p_left" runat="server" class="text" style="width:50px" value="0" />
							top : <input id="p_top" name="p_top" runat="server" class="text" style="width:50px"  value="0" />
						
						</div>
					</div>

                    <div class="form-group pc_only">
                        <label class="col-sm-2 control-label control-label">팝업 크기</label>
                        <div class="col-sm-10">
                        	width : <input id="p_width" name="p_width" runat="server" class="text" style="width:50px"  value="0" />
							height : <input id="p_height" name="p_height" runat="server" class="text" style="width:50px"  value="0" />
                        </div>
                    </div>
                    
                    
					<div class="form-group">
						<label class="col-sm-2 control-label control-label">내용</label>
						<div class="col-sm-10">
							<textarea name="p_content" id="p_content" runat="server" style="width:600px; height:212px; display:none;"></textarea>
                        </div>
					</div>
                    
				</div>

                
	
	             <div class="box-footer clearfix text-center">
					 <a  id="btnList" runat="server" class="btn btn-default pull-left">목록</a>
					 <asp:LinkButton runat=server ID=btn_remove OnClick="btn_remove_click" OnClientClick="return onRemove();"  class="btn btn-danger pull-right " style="margin-right:5px">삭제</asp:LinkButton>
					 <asp:LinkButton runat=server ID=btn_update OnClick="btn_update_click" OnClientClick="return onSubmit();" class="btn btn-danger pull-right" style="margin-right:5px">등록</asp:LinkButton>
					 <a href="#" id="btn_preview" class="btn btn-info pull-right pc_only" style="margin-right:5px">미리보기</a>
	            </div>
			</div>

			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>



</asp:Content>