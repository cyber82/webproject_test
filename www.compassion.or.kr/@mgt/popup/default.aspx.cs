﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
public partial class admin_popup_default : AdminBasePage {

    protected override void OnBeforePostBack(){
		base.OnBeforePostBack();
		
		Master.Content.Controls.BindAllTextBox(this.Context);

		base.LoadComplete += new EventHandler(list_LoadComplete);

		v_admin_auth auth = base.GetPageAuth();
		btn_add.Visible = auth.aa_auth_create;

	}

	protected override void OnAfterPostBack() {
		Master.ValueAction.Value = "";
		Master.ValueMessage.Value = "";
		Master.Content.Controls.EscapeAllTextBox();
		base.OnAfterPostBack();
	}

	protected override void GetList(int page) {

		using (AdminDataContext dao = new AdminDataContext()) {

            //var list = dao.sp_popup_list(page, paging.RowsPerPage, s_keyword.Text ,  "" , s_device.SelectedValue , Convert.ToInt32(s_display.SelectedValue), "", "").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword", "p_type", "p_device", "display", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, paging.RowsPerPage, s_keyword.Text, "", s_device.SelectedValue, Convert.ToInt32(s_display.SelectedValue), "", "" };
            var list = www6.selectSP("sp_popup_list", op1, op2).DataTableToList<sp_popup_listResult>().ToList();

            var total = 0;

			if (list.Count > 0)
				total = list[0].total.Value;

			lbTotal.Text = total.ToString();

			paging.CurrentPage = page;
			paging.Calculate(total);
			repeater.DataSource = list;
			repeater.DataBind();



		}
	}

	protected void ListBound(object sender, RepeaterItemEventArgs e) {
		if (repeater != null) {
			if (e.Item.ItemType != ListItemType.Footer) {

				sp_popup_listResult entity = e.Item.DataItem as sp_popup_listResult;

				var display = "";
				if (entity.p_begin > DateTime.Now) {
					display = "<span style='color:#7e2287'>노출전</span>";
				}else if (DateTime.Now > entity.p_end){
					display = "<span style='color:#7e2287'>미노출(기간종료)</span>";
				} else {
					display = "노출중";
				}

				if (!entity.p_display)
					display = "<span style='color:#7e2287'>미노출(노출선택안됨)</span>";

				((Literal)e.Item.FindControl("lbIdx")).Text = (paging.TotalRecords - ((paging.CurrentPage - 1) * paging.RowsPerPage) - e.Item.ItemIndex).ToString();
				((Literal)e.Item.FindControl("lbDisplay")).Text = display;

				var device = "PC";
				if (entity.p_device == "mobile")
					device = "모바일";
				else if(entity.p_device == "app")
					device = "앱";

				((Literal)e.Item.FindControl("lbDevice")).Text = device;

				if (entity.p_type == "layer"){
					((Literal)e.Item.FindControl("lbPosition")).Text = string.Format("L:{0} , T:{1} , W:{2} , H:{3}", entity.p_left, entity.p_top, entity.p_width, entity.p_height);
				}

			}
		}
	}





}
