﻿var $preview = {
	template: null,
	option : null,
	modal: function (option) {
		this.option = option;
		$.get("preview.html", function (html) {
			$preview.template = $(html);
			$preview.open();
		});
	},

	open: function () {
		$preview.template.find(".modal-body").html($preview.getContent());

		var id = $preview.template.attr("id");
		$('body').append($preview.template);
		
		$('#' + id).modal();
		$('#' + id).on('shown.bs.modal', function () {
			var body_height = $('#' + id).find('.modal-body').height();
			var height = $('#' + id).find(".thumbnail").height();
			var padding_top = (body_height - height) / 2;
			$('#' + id).find(".modal-body").css("padding-top", padding_top + "px");
			$('#' + id).find(".thumbnail").show();
		})

		$('#' + id).on('hidden.bs.modal', function () {
			$('#' + id).remove();
		});
	},
	getContent: function () {
		oEditors.getById[$preview.option.id].exec("UPDATE_CONTENTS_FIELD", []);
		var $content = $('<div style="width:' + $preview.option.width + ';text-align:center;margin:auto;display:none;" class="thumbnail">');
		$content.append($("#" + $preview.option.id).val());
		return $content;
	}
}