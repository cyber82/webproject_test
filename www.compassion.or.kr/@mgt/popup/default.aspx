﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="admin_popup_default" MasterPageFile="~/@mgt/mgt_afterlogin.master"%>
<%@ Register Src="~/@mgt/common/paging.ascx" TagPrefix="uc" TagName="paging" %>
<%@ MasterType virtualpath="~/@mgt/mgt_afterlogin.master" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="header">
	<script type="text/javascript" src="/popup/popupManager.js"></script>

	<script type="text/javascript">
		$(function () {
			
			$("#btnpop").bind("click", function () {
				popupManager.openAll(false, 'layer');		// 쿠키체크안함
			});


		});
	</script>
</asp:Content>

<asp:Content ID="content" runat="server" ContentPlaceHolderID="content">

	<div class="box box-default collapsed-box search_container">
		<div class="box-header with-border section-search" >
			<div class="pull-left ">
				<button class="btn btn-primary btn-sm pull-left" data-widget="collapse" style="margin-right: 5px;" ><i class="fa fa-plus search_toggle"></i></button>
				<h3 class="box-title" style="padding-top:5px">검색</h3>
			</div>
			
		</div>
		<div class="form-horizontal box-body" >
			<div class="box-body">

				<div class="form-group">
					<label for="s_keyword" class="col-sm-2 control-label">검색</label>
					<div class="col-sm-10">
						<asp:TextBox runat="server" ID="s_keyword" Width="415" CssClass="form-control inline"></asp:TextBox>
						* 제목
					</div>
				</div>
				<div class="form-group">
					<label for="s_device" class="col-sm-2 control-label">대상</label>
					<div class="col-sm-10">
						<asp:RadioButtonList runat="server" ID="s_device" RepeatDirection="Horizontal" RepeatLayout="Flow" >
							<asp:ListItem Selected="True"  Value="" Text="   전체   "></asp:ListItem>
							<asp:ListItem Value="pc" Text="   PC   "></asp:ListItem>
							<asp:ListItem Value="mobile" Text="   모바일"></asp:ListItem>
							<asp:ListItem Value="app" Text="   앱"></asp:ListItem>
						</asp:RadioButtonList>

					</div>

				</div>

				<div class="form-group">
					<label for="s_device" class="col-sm-2 control-label">노출</label>
					<div class="col-sm-10">
						<asp:RadioButtonList runat="server" ID="s_display" RepeatDirection="Horizontal" RepeatLayout="Flow" >
							<asp:ListItem Selected="True"  Value="-1" Text="   전체   "></asp:ListItem>
							<asp:ListItem Value="1" Text="   노출   "></asp:ListItem>
							<asp:ListItem Value="0" Text="   미노출"></asp:ListItem>
						</asp:RadioButtonList>

					</div>

				</div>

				<div class="box-footer clearfix text-center">
                    <asp:LinkButton runat="server" ID="btn_search" class="btn btn-primary" style="width:100px" OnClick="search">검색 <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
				</div>
				
			</div>
			
		</div>
	</div><!-- /.box -->


    <div class="box box-primary">
		
		 <div class="box-header">
            <h3 class="box-title">총 <asp:Literal runat="server" ID="lbTotal"></asp:Literal> 건</h3>
        </div><!-- /.box-header -->

		<div class="box-body table-responsive no-padding">


		<table class="table table-hover table-bordered ">

			<colgroup>
				<col width="5%" />
				<col width="7%" />
				<col width="*" />
				<col width="17%" />
				<col width="20%" />
				<col width="10%" />
				<col width="10%" />
			</colgroup>
			<thead>
				<tr>
					<th>번호</th>
					<th>PC/모바일</th>
					<th>제목</th>
					<th>위치</th>
					<th>게시기간</th>
					<th>상태</th>
					<th>등록일</th>
				</tr>
			</thead>
			<tbody>
				
				<asp:Repeater runat="server" ID="repeater" OnItemDataBound="ListBound">
					<ItemTemplate>
						<tr onclick="goPage('update.aspx' , { c: '<%#Eval("p_id")%>', p: '<%#paging.CurrentPage %>' , t: 'update' })" class="tr_link">
							<td><asp:Literal runat="server" ID="lbIdx"></asp:Literal></td>
							<!--<td><asp:Literal runat="server" ID="lbLang"></asp:Literal></td>-->
							<td><asp:Literal runat="server" ID="lbDevice"></asp:Literal></td>
							<td class="titCell"><%#Eval("p_name") %></td>
							<td><asp:Literal runat="server" ID="lbPosition"></asp:Literal></td>
							<td><%#Eval("p_begin" , "{0:MM/dd HH:mm}")%> ~ <%#Eval("p_end" , "{0:MM/dd HH:mm}")%></td>
							<td><asp:Literal runat="server" ID="lbDisplay"></asp:Literal></td>
							<td><%#Eval("p_regdate" , "{0:yy/MM/dd}")%></td>
							

						</tr>
					</ItemTemplate>
					<FooterTemplate>
						<tr runat="server" Visible="<%#repeater.Items.Count == 0 %>">
							<td colspan="8">데이터가 없습니다.</td>
						</tr>
					</FooterTemplate>
				</asp:Repeater>

			</tbody>
		</table>
		</div>
        
		<div class="box-footer clearfix text-center">
	        <uc:paging runat=server ID=paging OnNavigate="paging_Navigate" EnableViewState=true RowsPerPage="10" PagesPerGroup="10" />
        </div>

		 <div class="box-footer clearfix text-center">
            
            <div class="pull-right">
                
				<a id="btnpop" style="width:120px" class="btn btn-info">팝업테스트(PC만)</a>
				<asp:LinkButton runat="server" href="#" ID="btn_add" OnClientClick="goPage('update.aspx' , {t:'insert'})" CssClass="btn btn-bitbucket">신규등록</asp:LinkButton>
            </div>
            
        </div>

	<div class="boardController">
		<div class="btnRight">
			
		</div>
	</div>

</asp:Content>