﻿<%@ WebHandler Language="C#" Class="api_my_commitment" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_my_commitment : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "list") {
            this.GetList(context);
        }
    }

    void GetList( HttpContext context ) {

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var fundingfrequency = context.Request["fundingfrequency"].EmptyIfNull();
        var sponsorItemEng = context.Request["sponsorItemEng"].ValueIfNull("");

        new CommitmentAction().GetCommitmentList(page, rowsPerPage , fundingfrequency, sponsorItemEng).Write(context);

    }

}