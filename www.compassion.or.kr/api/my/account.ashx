﻿<%@ WebHandler Language="C#" Class="api_my_account" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_my_account : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "send-email") {
            this.SendEmail(context);
            // 이메일 인증확인
        } else if(t == "check-email") {
            this.CheckEmail(context);
            // 휴대폰 인증요청
        } else if(t == "send-phone") {
            this.SendPhone(context);
            // 이메일 인증확인
        } else if(t == "check-phone") {
            this.CheckPhone(context);

            // 휴대폰 인증요청 , 기존 회원조회안함, 기업회원에서 사용
        } else if(t == "send-phone-nocheck") {
            this.SendPhoneNoCheck(context);
            // 휴대폰 인증확인 , 기존 회원조회안함, 기업회원에서 사용
        } else if(t == "check-phone-nocheck") {
            this.SendPhoneNoCheck(context);
            // 이메일 인증요청 , 기존 회원조회안함, 기업회원에서 사용
        } else if(t == "send-email-nocheck") {
            this.SendEmailNoCheck(context);
            // 이메일 인증확인 , 기존 회원조회안함, 기업회원에서 사용
        } else if(t == "check-email-nocheck") {
            this.CheckEmailNoCheck(context);
            // 뉴스레터 수신여부
        } else if(t == "check-newsletter") {
            this.CheckNewsletter(context);
        }
    }

    // 이메일 인증요청 메일 보내기
    // success : true = 메일 전송완료
    void SendEmailNoCheck(HttpContext context ) {
        var c = context.Request["c"].EmptyIfNull();     // email
        var result = new JsonWriter();
        result.success = true;

        if(string.IsNullOrEmpty(c)) {
            result.Write(context);
            return;
        }

        try {
            var code = "";
            using (AuthDataContext dao = new AuthDataContext())
            {
                //code = dao.sp_user_email_auth_insert_f(c).First().ue_id.ToString();
                Object[] op1 = new Object[] { "ue_email" };
                Object[] op2 = new Object[] { c };
                code = www6.selectSPAuth("sp_user_email_auth_insert_f", op1, op2).DataTableToList<sp_user_email_auth_insert_fResult>().First().ue_id.ToString();
            }

            var args = new Dictionary<string, string>() {
                { "{code}" , code }
            };

            // sendEmailCode = join 에서도 같이 사용하는 변수명 
            context.Session["sendEmailCode"] = code;

            // to user
            Email.Send(context, Email.SystemSender, new List<string>() { c }
                , ConfigurationManager.AppSettings["mail_title_email_confirm"]
                , "/common/mail_template/email-confirm.html"
                , args
            , null);

            result.Write(context);

        } catch(Exception e) {

            ErrorLog.Write(context , 0 , e.ToString());
            result.success = false;
            result.Write(context);
        }

    }

    // success : true = 인증완료
    void CheckEmailNoCheck(HttpContext context ) {

        var c = context.Request["c"].EmptyIfNull();
        var result = new JsonWriter();
        result.success = false;

        if(string.IsNullOrEmpty(c) || context.Session["sendEmailCode"] == null) {
            result.Write(context);
            return;
        }

        using (AuthDataContext dao = new AuthDataContext())
        {
            try
            {
                //var entity = dao.user_email_auth.First(p => p.ue_email == c && p.ue_id == Guid.Parse(context.Session["sendEmailCode"].ToString()));
                var entity = www6.selectQFAuth<user_email_auth>("ue_email", c, "ue_id", Guid.Parse(context.Session["sendEmailCode"].ToString()));
                result.success = entity.ue_active;
                result.data = new Dictionary<string, string> { { "email", entity.ue_email } };
                result.Write(context);
            }
            catch
            {
                result.success = false;
                result.Write(context);
            }
        }
    }

    // 휴대폰 인증요청 메일 보내기
    // success : true = 문자 전송완료
    void SendPhoneNoCheck(HttpContext context ) {
        var n = context.Request["n"].EmptyIfNull();     // 이름
        var c = context.Request["c"].EmptyIfNull();     // phone

        var result = new JsonWriter();
        result.success = true;

        if(string.IsNullOrEmpty(c)) {
            result.Write(context);
            return;
        }

        try {

            var code = this.MakeCertificationNumber(4);

            if (ConfigurationManager.AppSettings["stage"] == "dev")
                code = "1111";
            else {
                // SMS 발송로직 추가	
                var sMobileRandom = util.SendSMS.SMSInfoSearch(c, n, code);
                //휴대폰전송을 성공했을때 
                if (sMobileRandom == "30"){
                    result.success = false;
                    result.message = "SMS 발송 실패";
                }

            }
            context.Session["findSendPhoneCode"] = code;
            context.Session["findSendPhoneNo"] = c;

            result.Write(context);

        } catch(Exception e) {

            ErrorLog.Write(context , 0 , e.ToString());
            result.success = false;
            result.Write(context);
        }

    }

    // success : true = 인증완료
    void CheckPhoneNoCheck(HttpContext context ) {

        var c = context.Request["c"].EmptyIfNull();
        var result = new JsonWriter();
        result.success = false;

        if(string.IsNullOrEmpty(c) || context.Session["findSendPhoneCode"] == null) {
            result.Write(context);
            return;
        }

        if ( context.Session["findSendPhoneCode"].ToString() == c) {
            result.success = true;
            result.data = new Dictionary<string, string> { { "phone", context.Session["findSendPhoneNo"].ToString() } };
            result.Write(context);
            return;
        }

    }

    // 휴대폰 인증요청 메일 보내기
    // success : true = 문자 전송완료
    void SendPhone(HttpContext context ) {
        var n = context.Request["n"].EmptyIfNull();     // 이름
        var c = context.Request["c"].EmptyIfNull();     // phone
        var b = context.Request["b"].EmptyIfNull();     // 생년월일 yyyy-MM-dd

        new JoinAction().SendPhone(n, c, b).Write(context);
    }

    // success : true = 인증완료
    void CheckPhone(HttpContext context ) {

        var c = context.Request["c"].EmptyIfNull();

        new JoinAction().CheckPhone(c).Write(context);

    }

    // 이메일 인증요청 메일 보내기
    // success : true = 메일 전송완료
    void SendEmail(HttpContext context ) {
        var c = context.Request["c"].EmptyIfNull();     // email
        var n = context.Request["n"].EmptyIfNull();     // 이름
        var b = context.Request["b"].EmptyIfNull();     // 생년월일 yyyy-MM-dd
        new JoinAction().SendEmail(c, n, b).Write(context);
    }

    // success : true = 인증완료
    void CheckEmail(HttpContext context ) {

        var c = context.Request["c"].EmptyIfNull();
        new JoinAction().CheckEmail(c).Write(context);
    }


    void CheckNewsletter(HttpContext context) {
        JsonWriter.Write(new SponsorAction().GetAgreeCommunications(), context);
    }



    string MakeCertificationNumber( int length ) {
        StringBuilder result = new StringBuilder();

        int i;
        Random random = new Random();
        for(i = 0; i < length; i++)
            result.Append(random.Next(0, 10));

        return result.ToString();
    }

}