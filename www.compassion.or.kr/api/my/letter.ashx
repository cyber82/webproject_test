﻿<%@ WebHandler Language="C#" Class="api_my_letter" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_my_letter : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "list") {
            this.GetList(context);
        } else if(t == "detail") {
            this.GetDetail(context);
        } else if(t == "cancel") {
            this.Cancel(context);
        } else if(t == "countries") {
            this.GetCountries(context);
        } else if(t == "children") {
            this.GetChildren(context);
        } else if(t == "sample-letter") {
            this.GetSampleLetterList(context);
        } else if(t == "add") {
            this.Add(context);
        } else if(t == "unread-count") {
            this.UnreadCount(context);
        } else if(t == "update") {
            this.Update(context);
        } else if(t == "file-delete") {
            this.FileDelete(context);
        } else if(t == "file-download") {
            this.FileDownload(context);
        } else if(t == "set-read") {
            this.SetRead(context);
        }
    }

    void SetRead( HttpContext context ) {

        var childMasterId = context.Request["childMasterId"].EmptyIfNull();
        var correspondenceID = context.Request["correspondenceID"].EmptyIfNull();
        new LetterAction().SetRead(correspondenceID , childMasterId).Write(context);

    }


    void UnreadCount( HttpContext context ) {

        new LetterAction().GetUnreadCount().Write(context);

    }

    void FileDownload( HttpContext context ) {

        var url = context.Request["url"].EmptyIfNull();

        try {
            new Download().OverUrl(url);

        } catch(Exception ex) {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
        }


    }

    void FileDelete( HttpContext context ) {

        //var file_path = context.Request["file_path"].EmptyIfNull().Replace(ConfigurationManager.AppSettings["domain_file"], "");
        var file_path = context.Request["file_path"].EmptyIfNull();
        new LetterAction().FileDelete(file_path).Write(context);

    }

    void Update( HttpContext context ) {

        var samplelettercode = context.Request["samplelettercode"].EmptyIfNull();
        var file_path = context.Request["file_path"].EmptyIfNull();
        var file_size = Convert.ToInt32(context.Request["file_size"].ValueIfNull("0"));
        var lang = context.Request["lang"].EmptyIfNull();
        var repCorrespondenceID = context.Request["repCorrespondenceID"].EmptyIfNull();
        var letter_comment = context.Request["letter_comment"].EmptyIfNull();
        var letter_type = context.Request["letter_type"].EmptyIfNull();
        var children = context.Request["children"].EmptyIfNull().ToObject<List<LetterAction.SaveEntity.child>>();
        var status = context.Request["status"].EmptyIfNull();
        // 한글편지 예문 변경 여부 추가 문희원 2017-09-26
        var isTempChange = context.Request["isTempChange"].ValueIfNull("0");

        var giftSeq = context.Request["giftSeq"].ValueIfNull("0");
        var giftIsStoreItem = context.Request["giftIsStoreItem"].ValueIfNull("");

        LetterAction.SaveEntity entity = new LetterAction.SaveEntity() {
            fileNameWithPath = file_path, children = children, fileSize = file_size, lang = lang, letterComment = letter_comment, letterType = letter_type,
            repCorrespondenceID = repCorrespondenceID, samplelettercode = samplelettercode , status = status, isTempChange = Convert.ToInt32(isTempChange),
            newFile = context.Request["newFile"].ValueIfNull("N"),
            giftSeq = giftSeq,
            giftIsStoreItem = giftIsStoreItem
        };

        new LetterAction().Update(entity).Write(context);

    }

    void Add( HttpContext context ) {

        var samplelettercode = context.Request["samplelettercode"].EmptyIfNull();
        var file_path = context.Request["file_path"].EmptyIfNull();
        var file_size = Convert.ToInt32(context.Request["file_size"].ValueIfNull("0"));
        var lang = context.Request["lang"].EmptyIfNull();
        var status = context.Request["status"].EmptyIfNull();
        var letter_comment = context.Request["letter_comment"].EmptyIfNull();
        var letter_type = context.Request["letter_type"].EmptyIfNull();
        var children = context.Request["children"].EmptyIfNull().ToObject<List<LetterAction.SaveEntity.child>>();
        var is_pic_letter = context.Request["is_pic_letter"].ValueIfNull("N");
        // 한글편지 예문 변경 여부 추가 문희원 2017-04-26
        var isTempChange = context.Request["isTempChange"].ValueIfNull("0");


        //file_path.Normalize(NormalizationForm.FormC);

        var giftSeq = context.Request["giftSeq"].ValueIfNull("0");
        var giftIsStoreItem = context.Request["giftIsStoreItem"].ValueIfNull("");

        LetterAction.SaveEntity entity = new LetterAction.SaveEntity() {
            is_pic_letter = is_pic_letter == "Y",
            fileNameWithPath = file_path,
            children = children,
            fileSize = file_size,
            lang = lang,
            letterComment = letter_comment,
            letterType = letter_type,
            status = status,
            samplelettercode = samplelettercode,
            isTempChange = Convert.ToInt32(isTempChange),
            giftSeq = giftSeq,
            giftIsStoreItem = giftIsStoreItem
        };

        new LetterAction().Add(entity).Write(context);

    }

    void GetSampleLetterList( HttpContext context ) {

        var type = context.Request["type"].EmptyIfNull();
        new LetterAction().GetSampleLetters(type).Write(context);

    }

    void Cancel( HttpContext context ) {

        var correspondenceId = context.Request["c"].EmptyIfNull();
        if(string.IsNullOrEmpty(correspondenceId)) {
            new JsonWriter() { success = false, message = "편지아이디가 필요합니다." }.Write(context);
            return;
        }
        new LetterAction().Cancel(correspondenceId).Write(context);

    }

    void GetList( HttpContext context ) {
        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var type = context.Request["type"].EmptyIfNull();
        var childMasterId = context.Request["childMasterId"].EmptyIfNull();
        var include_ready = context.Request["include-ready"].ValueIfNull("Y");      // 대기중 , 임시보관함도 보여줄지 여부

        new LetterAction().GetList(page, rowsPerPage, type, childMasterId, include_ready).Write(context);

    }


    void GetDetail( HttpContext context ) {

        var corrId = context.Request["corrId"].EmptyIfNull();
        var attach_path = context.Request["attach_path"].EmptyIfNull();

        new LetterAction().GetDetail(corrId, attach_path).Write(context);

    }

    void GetCountries( HttpContext context ) {

        var type = context.Request["type"].EmptyIfNull();
        new LetterAction().GetCountries(type).Write(context);

    }

    void GetChildren( HttpContext context ) {

        var type = context.Request["type"].EmptyIfNull();
        var countryName = context.Request["country"].EmptyIfNull();

        new LetterAction().GetChildren(type, countryName).Write(context);

    }

}