﻿<%@ WebHandler Language="C#" Class="api_my_child" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_my_child : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest( HttpContext context ) {

		var t = context.Request["t"].EmptyIfNull();

		if(t == "list") {
			this.GetList(context);
		} else if(t == "giftable-list") {
			this.GetGiftableList(context);
		} else if(t == "detail") {
			this.GetDetail(context);
		} else if(t == "get-child-country") {
			this.GetChildCountries(context);
		}
	}

	void GetChildCountries( HttpContext context ) {
		new ChildAction().MyChildCountries().Write(context);

	}

	void GetList( HttpContext context ) {
		var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
		var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
		var checkLetter = context.Request["letter"].ValueIfNull("0") == "1";
		new ChildAction().MyChildren("", page, rowsPerPage , "old" , checkLetter).Write(context);
	}

	void GetGiftableList( HttpContext context ) {
		new ChildAction().GetGitfableList().Write(context);

	}

	void GetDetail( HttpContext context ) {
		var childKey = context.Request["childKey"].EmptyIfNull();
		new ChildAction().MyChild(childKey).Write(context);

	}
}