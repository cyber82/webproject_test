﻿<%@ WebHandler Language="C#" Class="api_my_user_funding" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;
	
public class api_my_user_funding : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest( HttpContext context ) {

		var t = context.Request["t"].EmptyIfNull();

		if(t == "create-list") {
			this.GetCreateList(context);
		} else if(t == "join-list") {
			this.GetJoinList(context);
		} else if(t == "summary") {
			this.GetSummary(context);
		}
	}

	void GetCreateList( HttpContext context ) {

		var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
		var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
		var startdate = context.Request["date_begin"].EmptyIfNull();
		var enddate = context.Request["date_end"].EmptyIfNull();

		new UserFundingAction().GetCreateList(page, rowsPerPage, startdate, enddate).Write(context);

	}

	void GetJoinList( HttpContext context ) {

		var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
		var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
		var startdate = context.Request["date_begin"].EmptyIfNull();
		var enddate = context.Request["date_end"].EmptyIfNull();

		new UserFundingAction().GetJoinList(page, rowsPerPage, startdate, enddate).Write(context);

	}

	void GetSummary( HttpContext context ) {

		new UserFundingAction().GetUserSummary().Write(context);

	}
}