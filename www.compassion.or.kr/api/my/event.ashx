﻿<%@ WebHandler Language="C#" Class="api_my_event" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_my_event : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "list") {
            this.GetList(context);
        }
    }

    void GetList( HttpContext context ) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if(!UserInfo.IsLogin) {
            result.message = "로그인이 필요합니다.";
            result.success = false;
            JsonWriter.Write(result, context);
        }

        var userInfo = new UserInfo();
        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_event_apply_list_f(page, rowsPerPage, userInfo.UserId).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "user_id" };
            Object[] op2 = new Object[] { page, rowsPerPage, userInfo.UserId };
            var list = www6.selectSP("sp_event_apply_list_f", op1, op2).DataTableToList<sp_event_apply_list_fResult>().ToList();

            result.data = list;
        }
        JsonWriter.Write(result, context);

    }
}