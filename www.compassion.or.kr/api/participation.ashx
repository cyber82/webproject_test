﻿<%@ WebHandler Language="C#" Class="api_sympathy" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_sympathy : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();
        if(t == "promotional_video") {
            this.GetPromotionVideo(context);
        }else if(t == "partnership_list") {
            this.GetPartnershipList(context);
        }else if(t == "sponsor_list") {
            this.GetSponsorList(context);
        }else if(t == "prayer_list") {
            GetPrayerList(context);
        }else if(t == "prayer_picture_list") {
            GetPrayerPictureList(context);
        }


        // 기도제목 조회수 증가
        else if(t == "hits_prayer") {
            HitsPrayer(context);
        }


        // 기도제목 조회수 증가
        else if(t == "hits_event") {
            HitsEvent(context);
        }
        


        // 기도했어요
        else if(t == "pray") {
            Pray(context);
        }
        // 구독정보 가져오기
        else if(t == "subscription") {
            GetSubscription(context);
        }else if (t == "unsubscript") {
            Unsubscript(context);
        }

        else if(t == "hits_prayer_picture") {
            HitsPrayerPicture(context);
        }
        else if(t == "pray_picture") {
            PrayPicture(context);
        }
    }


    void GetPromotionVideo(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("3"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_promotional_video_list_f(page, rowsPerPage).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage" };
            Object[] op2 = new Object[] { page, rowsPerPage };
            var list = www6.selectSP("sp_promotional_video_list_f", op1, op2).DataTableToList<sp_promotional_video_list_fResult>().ToList();

            foreach (var item in list)
            {
                if (item.pv_thumb != "")
                {
                    item.pv_thumb = item.pv_thumb.WithFileServerHost();
                }
            }
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }




    // 파트너십
    void GetPartnershipList(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10000"));
        var location = context.Request["location"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_partnership_list_f(page, rowsPerPage, location).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "location" };
            Object[] op2 = new Object[] { page, rowsPerPage, location };
            var list = www6.selectSP("sp_partnership_list_f", op1, op2).DataTableToList<sp_partnership_list_fResult>().ToList();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    // 함께하는 기업
    void GetSponsorList(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10000"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_company_list_f(page, rowsPerPage).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage" };
            Object[] op2 = new Object[] { page, rowsPerPage };
            var list = www6.selectSP("sp_company_list_f", op1, op2).DataTableToList<sp_company_list_fResult>();

            /*
            List<sp_company_list_fResult> list = new List<sp_company_list_fResult>();
            foreach(var entity in dao.sp_company_list_f(page, rowsPerPage).ToList()) {
                entity.c_thumb = entity.c_thumb.WithFileServerHost();
                list.Add(entity);
            }
            */
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    // 샬롬 기도운동 기도제목   
    void GetPrayerList(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var keyword = context.Request["k_word"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_prayer_list_f(page, rowsPerPage, keyword).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword" };
            Object[] op2 = new Object[] { page, rowsPerPage, keyword};
            var list = www6.selectSP("sp_prayer_list_f", op1, op2).DataTableToList<sp_prayer_list_fResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    // 샬롬 기도운동 기도영상
    void GetPrayerPictureList(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var keyword = context.Request["k_word"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_prayer_list_f(page, rowsPerPage, keyword).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword" };
            Object[] op2 = new Object[] { page, rowsPerPage, keyword};
            var list = www6.selectSP("sp_prayer_picture_list_f", op1, op2).DataTableToList<sp_prayer_picture_list_fResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void HitsPrayer(HttpContext context) {
        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        if(id != -1) {
            using (FrontDataContext dao = new FrontDataContext())
            {
                //var entity = dao.TB_PRAYER_DOC.First(p => p.PD_SEQ == id);
                var entity = www6.selectQF<TB_PRAYER_DOC>("PD_SEQ", id);
                entity.PD_VIEW_CNT++;

                //dao.SubmitChanges();
                www6.update(entity);
            }
        }
    }

    void HitsPrayerPicture(HttpContext context) {
        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        if(id != -1) {
            using (FrontDataContext dao = new FrontDataContext())
            {
                //var entity = dao.TB_PRAYER_DOC.First(p => p.PD_SEQ == id);
                var entity = www6.selectQF<TB_PRAYER_PICTURE>("PP_SEQ", id);
                entity.PP_VIEW_CNT++;

                //dao.SubmitChanges();
                www6.update(entity);
            }
        }
    }

    void HitsEvent(HttpContext context)
    {
        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        if(id != -1)
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                //var entity = dao.@event.First(p => p.e_id == Convert.ToInt32(id));
                var entity = www6.selectQF<@event>("e_id", Convert.ToInt32(id));
                entity.e_hits++;

                //dao.SubmitChanges();
                www6.update(entity);
            }
        }
    }



    void Pray(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        if(id == -1) {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.TB_PRAYER_DOC.First(p => p.PD_SEQ == id);
            var entity = www6.selectQF<TB_PRAYER_DOC>("PD_SEQ", id);

            if (entity.PD_PRAYER_CNT == null)
            {
                entity.PD_PRAYER_CNT = 1;
            }
            else
            {
                entity.PD_PRAYER_CNT++;
            }

            //dao.SubmitChanges();
            //www6.insert(entity);
            www6.update(entity);    // 2018-06-04 : Insert --> Update 로 변경


            result.data = ((int)entity.PD_PRAYER_CNT).ToString("N0");
        }
        JsonWriter.Write(result, context);
    }

    void PrayPicture(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        if(id == -1) {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.TB_PRAYER_DOC.First(p => p.PD_SEQ == id);
            var entity = www6.selectQF<TB_PRAYER_PICTURE>("PP_SEQ", id);

            if (entity.PP_PRAYER_CNT == null)
            {
                entity.PP_PRAYER_CNT = 1;
            }
            else
            {
                entity.PP_PRAYER_CNT++;
            }

            //dao.SubmitChanges();
            //www6.insert(entity);  
            www6.update(entity);    // 2018-06-04 : Insert --> Update 로 변경

            result.data = ((int)entity.PP_PRAYER_CNT).ToString("N0");
        }
        JsonWriter.Write(result, context);
    }


    // 구독정보 가져오기
    void GetSubscription(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var userInfo = new UserInfo();
        using (FrontDataContext dao = new FrontDataContext())
        {
            var resultData = new Dictionary<string, string>();
            var exist = www6.selectQ<nk_subscription>("ns_user_id", userInfo.UserId, "ns_type", "sms");
            //if (dao.nk_subscription.Any(p => p.ns_user_id == userInfo.UserId && p.ns_type == "sms"))
            if(exist.Any())
            {
                resultData.Add("sms", "sms");
            }

            var exist2 = www6.selectQ<nk_subscription>("ns_user_id", userInfo.UserId, "ns_type", "email");
            //if (dao.nk_subscription.Any(p => p.ns_user_id == userInfo.UserId && p.ns_type == "email"))
            if(exist2.Any())
            {
                resultData.Add("email", "email");
            }
            resultData.Add("length", resultData.Count.ToString());

            result.data = resultData;

            JsonWriter.Write(result, context);
            return;
        }
    }




    void Unsubscript(HttpContext context) {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var type = context.Request["type"].EmptyIfNull();
        if(type == "") {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var userInfo = new UserInfo();
        using (FrontDataContext dao = new FrontDataContext())
        {
            if (type == "all" || type == "sms")
            {
                //var entity = dao.nk_subscription.FirstOrDefault(p => p.ns_user_id == userInfo.UserId && p.ns_type == "sms");
                var entity = www6.selectQF<nk_subscription>("ns_user_id", userInfo.UserId, "ns_type", "sms");
                if (entity != null)
                {
                    //dao.nk_subscription.DeleteOnSubmit(entity);
                    www6.delete(entity);
                }
            }

            if (type == "all" || type == "email")
            {
                //var entity = dao.nk_subscription.FirstOrDefault(p => p.ns_user_id == userInfo.UserId && p.ns_type == "email");
                var entity = www6.selectQF<nk_subscription>("ns_user_id", userInfo.UserId, "ns_type", "email");
                if (entity != null)
                {
                    //dao.nk_subscription.DeleteOnSubmit(entity);
                    www6.delete(entity);
                }
            }

            //dao.SubmitChanges();
            JsonWriter.Write(result, context);
            return;
        }
    }
}