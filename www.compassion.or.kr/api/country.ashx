﻿<%@ WebHandler Language="C#" Class="api_country" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_country : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest( HttpContext context ) {

		var t = context.Request["t"].EmptyIfNull();

		if(t == "info") {
			this.GetInfo(context);
		}

	}

	void GetInfo( HttpContext context ) {

		var code = context.Request["code"].EmptyIfNull().EscapeSqlInjection();
		JsonWriter.Write(new CountryAction().GetInfoById(code), context);
	}

}