﻿<%@ WebHandler Language="C#" Class="api_about_us" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_about_us : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {
        //hits 추가
        var t = context.Request["t"].EmptyIfNull();
        if (t == "list") {
            this.GetList(context);
        } else if (t == "hits") {
            Hits(context);
        }
    }


    void GetList(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var k_word = context.Request["k_word"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_press_list_f(page, rowsPerPage, k_word).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword" };
            Object[] op2 = new Object[] { page, rowsPerPage, k_word };
            var list = www6.selectSP("sp_press_list_f", op1, op2).DataTableToList<sp_press_list_fResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    //hits 추가
    void Hits(HttpContext context)
    {
        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        if(id != -1)
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                //var entity = dao.press.First(p => p.p_id == id);
                var entity = www6.selectQF<press>("p_id", id);
                entity.p_hits++;

                //dao.SubmitChanges();
                www6.update(entity);

            }
        }
    }
}