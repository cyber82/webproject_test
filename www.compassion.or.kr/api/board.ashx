﻿<%@ WebHandler Language="C#" Class="api_board" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_board : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();
        if(t == "list") {
            this.GetList(context);
        }else if(t == "hits") {
            Hits(context);
        }else if(t == "file_list") {
            GetFileList(context);
        }
    }


    void GetList(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var type = context.Request["b_type"].EmptyIfNull();
        var sub_type = context.Request["b_sub_type"].EmptyIfNull();
        var k_word = context.Request["k_word"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_board_list_f(page, rowsPerPage, type, sub_type, "both", k_word, -1, "", "").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "sub_type", "keyword_type", "keyword", "main", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, rowsPerPage, type, sub_type, "both", k_word, -1, "", "" };
            var list = www6.selectSP("sp_board_list_f", op1, op2).DataTableToList<sp_board_list_fResult>().ToList();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }



    void GetFileList(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var type = context.Request["b_type"].EmptyIfNull();
        var sub_type = context.Request["b_sub_type"].EmptyIfNull();
        var k_word = context.Request["k_word"].EmptyIfNull();
        var except = Convert.ToInt32(context.Request["except"].ValueIfNull("-1"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_board_file_list_f(page, rowsPerPage, type, sub_type, k_word, except).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "b_type", "sub_type", "keyword", "except"};
            Object[] op2 = new Object[] { page, rowsPerPage, type, sub_type, k_word, except };
            var list = www6.selectSP("sp_board_file_list_f", op1, op2).DataTableToList<sp_board_file_list_fResult>().ToList();

            foreach (var item in list)
            {
                if (item.b_thumb.EmptyIfNull() != "")
                {
                    item.b_thumb = item.b_thumb.WithFileServerHost();
                }

                if (item.b_file.EmptyIfNull() != "")
                {
                    item.b_file = item.b_file.WithFileServerHost();
                }

            }
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void Hits(HttpContext context)
    {
        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        if(id != -1)
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                //var entity = dao.board.First(p => p.b_id == id);
                var entity = www6.selectQF<board>("b_id", id);
                entity.b_hits++;

                //dao.SubmitChanges();
                www6.update(entity);
            }
        }
    }

}