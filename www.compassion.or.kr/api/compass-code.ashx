﻿<%@ WebHandler Language="C#" Class="api_compass_code" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_compass_code : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest( HttpContext context ) {

		var t = context.Request["t"].EmptyIfNull();

		if(t == "banks") {
			this.GetBankList(context);
		} 
	}

	void GetBankList( HttpContext context ) {

		new CodeAction().Banks().Write(context);

	}

}