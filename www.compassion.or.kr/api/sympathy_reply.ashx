﻿<%@ WebHandler Language="C#" Class="api_sympathy_reply" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_sympathy_reply : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();

        if (t == "list") {
            this.GetList(context);
        } else if (t == "remove") {
            this.Remove(context);
        } else if (t == "update") {
            this.Update(context);
        } else if(t == "add") {
            this.Add(context);
        }
    }

    public class sp_sympathy_reply_list_fResultEx : sp_sympathy_reply_list_fResult {
        public bool is_owner { get; set; }
    }

    void GetList(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        UserInfo sess = new UserInfo();
        var userId = sess.UserId;

        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var type = context.Request["s_type"].EmptyIfNull();

        if (id == -1) {
            result.success = false;
            result.message = "error";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_sympathy_reply_list_f(id, page, rowsPerPage, type).ToList();
            Object[] op1 = new Object[] { "idx", "page", "rowsPerPage", "type" };
            Object[] op2 = new Object[] { id, page, rowsPerPage, type };
            var list = www6.selectSP("sp_sympathy_reply_list_f", op1, op2).DataTableToList<sp_sympathy_reply_list_fResult>();

            List<sp_sympathy_reply_list_fResultEx> data = new List<sp_sympathy_reply_list_fResultEx>();
            foreach (var entity in list)
            {

                data.Add(new sp_sympathy_reply_list_fResultEx()
                {
                    body = entity.body,
                    idx = entity.idx,
                    ip_address = entity.ip_address,
                    reg_date = entity.reg_date,
                    rownum = entity.rownum,
                    total = entity.total,
                    user_name = entity.user_name,
                    is_owner = entity.user_id == userId,
                    user_id = entity.user_id.Mask("*", 3)
                });
            }
            result.data = data;
        }
        JsonWriter.Write(result, context);
    }



    void Remove(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }


        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        if (id == -1) {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }



        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();
            var exist = www6.selectQ<ssb_reply>("idx", id, "user_id", userInfo.UserId);
            //if (!dao.ssb_reply.Any(p => p.idx == id && p.user_id == userInfo.UserId))
            if(!exist.Any())
            {
                result.success = false;
                result.message = "올바른 접근이 아닙니다.";
                JsonWriter.Write(result, context);
                return;
            }

            //var entity = dao.ssb_reply.First(p => p.idx == id && p.user_id == userInfo.UserId);
            var entity = www6.selectQF<ssb_reply>("idx", id, "user_id", userInfo.UserId);
            entity.del_flag = true;

            //dao.SubmitChanges();
            www6.update(entity);
        }

        JsonWriter.Write(result, context);
    }



    void Update(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }


        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var content = context.Request["content"].EmptyIfNull().EscapeSqlInjection();
        if (id == -1 || content == "") {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();
            var exist = www6.selectQ<ssb_reply>("idx", id, "user_id", userInfo.UserId);
            //if (!dao.ssb_reply.Any(p => p.idx == id && p.user_id == userInfo.UserId))
            if(!exist.Any())
            {
                result.success = false;
                result.message = "올바른 접근이 아닙니다.";
                JsonWriter.Write(result, context);
                return;
            }


            //var entity = dao.ssb_reply.First(p => p.idx == id && p.user_id == userInfo.UserId);
            var entity = www6.selectQF<ssb_reply>("idx", id, "user_id", userInfo.UserId);
            entity.body = content;

            //dao.SubmitChanges();
            www6.update(entity);
        }

        JsonWriter.Write(result, context);
    }


    void Add(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }


        var article_idx = Convert.ToInt32(context.Request["article_idx"].ValueIfNull("-1"));
        var content = context.Request["content"].EmptyIfNull().EscapeSqlInjection();
        if (article_idx == -1 || content == "") {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            var entity = new ssb_reply()
            {
                article_idx = article_idx,
                user_id = userInfo.UserId,
                user_name = userInfo.UserName,
                body = content,
                del_flag = false,
                reg_date = DateTime.Now,
                ip_address = context.Request.ServerVariables["REMOTE_ADDR"].ToString()
            };
            
            //dao.ssb_reply.InsertOnSubmit(entity);
            www6.insert(entity);

            //dao.SubmitChanges();
        }

        JsonWriter.Write(result, context);
    }
}