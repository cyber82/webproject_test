﻿<%@ WebHandler Language="C#" Class="api_tcpt" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;
using Newtonsoft.Json;
using TCPTModel.Response.Beneficiary;

public class api_tcpt : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "list") {
            this.GetChildren(context);
        }else if(t == "get") {
            this.GetChild(context);
        }else if(t == "get-casestudy") {
            this.GetChildCaseStudy(context);
        }else if(t == "get-user-funding") {
            this.GetChildUserFunding(context);
        }else if(t == "user-funding") {
            this.GetUserFundingChildren(context);
        }else if(t == "user-funding-select") {
            this.GetUserFundingChildrenSelect(context);
        }else if(t == "wedding-child") {
            this.GetWeddingChild(context);
        } else if(t == "event-child") {
            this.GetEventChild(context);
        }else if(t == "get-random-child") {
            this.GetRandomChild(context);
        }else if(t == "get-child-image") {
            this.GetChildImage(context);
        }
    }


    void GetChildImage( HttpContext context ) {
        var childMasterId = context.Request["childMasterId"];
        var childKey = context.Request["childKey"];

        /*
        var img = new ChildAction().GetChildImage(childMasterId, childKey);
        JsonWriter result = new JsonWriter() { success = true, data = img };
        result.Write(context);
		*/

        //String sqlText = " SELECT top 1  tChildImage_EN.image_file as ChildImage " +
        //		"      , tChildImage.ChildImage AS ChildImage_2 " +
        //		"      , ChildSmallImage " +
        //		"      , Convert(varchar(10), ImageDate, 121) AS CaseStudyDate " +
        //		" FROM  kr_Compass4_image.dbo.tChildImage tChildImage WITH (NOLOCK) " +
        //		" INNER JOIN   compass_image.dbo.need_image tChildImage_EN WITH (NOLOCK) " +
        //		"   ON  tChildImage_EN.image_date = tChildImage.ImageDate " +
        //		"  AND  tChildImage_EN.need_key   = tChildImage.ChildKey COLLATE Korean_Wansung_CI_AS " +
        //		"  WHERE ChildMasterID IS NOT NULL " +
        //		"  AND  ChildMasterID = '" + childMasterId + "'" +
        //		"  ORDER BY ImageDate DESC ";

        //CommonLib.WWW6Service.SoaHelperSoap _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();
        //Object[] objSql = new object[1] { sqlText };
        //DataSet obj = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

        //string img = "";

        //if (obj == null) {
        //	img = ChildAction.GetPicByChildKey(childKey);
        //} else {
        //	var dt = obj.Tables[0];
        //	if (dt.Rows.Count < 1) {
        //		img = ChildAction.GetPicByChildKey(childKey);
        //	} else {
        //		DataRow dr = dt.Rows[0];

        //		var bytes = (byte[])dr["ChildImage"]; //어린이 이미지
        //		string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
        //		img = "data:image/jpeg;base64," + base64String;

        //	}
        //}

        // #12563 : url에서 읽도록 변경
        //string img = "";
        //img = ChildAction.GetPicByChildKey(childKey);

        // #12770 : Web.Config 설정에 따라 Connection, DB 결정
        string img = "";
        img = new ChildAction().GetChildImage(childMasterId, childKey);

        JsonWriter result = new JsonWriter() { success = true, data = img };
        result.Write(context);

    }

    void GetRandomChild( HttpContext context ) {

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        int? minAge = Convert.ToInt32(context.Request["minAge"].ValueIfNull("-1"));
        if(minAge == -1) {
            minAge = null;
        }
        int? maxAge = Convert.ToInt32(context.Request["maxAge"].ValueIfNull("-1"));
        if(maxAge == -1) {
            maxAge = null;
        }
        var country = context.Request["country"];
        var gender = context.Request["gender"];
        var today = context.Request["today"].EmptyIfNull();
        var mm = context.Request["mm"];
        var dd = context.Request["dd"];
        var orphan = context.Request["orphan"];
        var specialNeed = context.Request["specialNeed"];
        var orderby = context.Request["orderby"].EmptyIfNull();

        int? birthM = null;
        int? birthD = null;

        if (today == "Y") {
            birthM = Convert.ToInt32(DateTime.Now.ToString("MM"));
            birthD = Convert.ToInt32(DateTime.Now.ToString("dd"));
        } else {
            if(!string.IsNullOrEmpty(mm)) {
                birthM = Convert.ToInt32(mm);
            }
            if(!string.IsNullOrEmpty(dd)) {
                birthD = Convert.ToInt32(dd);
            }
        }

        bool? isOrphan = null;
        if (orphan == "Y") {
            isOrphan = true;
        }
        bool? isSpecialNeed = null;
        if (specialNeed == "Y") {
            isSpecialNeed = true;
        }

        //[이종진]수정 - 신규조회방법(GlobalPool조회) 추가
        var actionResult = new JsonWriter();
        if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            actionResult = new ChildAction().GetChildren(country, gender, birthM, birthD, minAge, maxAge, isOrphan, isSpecialNeed, page, rowsPerPage, orderby, -1, -1);
        }
        else
        {
            actionResult = new ChildAction().GetChildrenGp(country, gender, birthM, birthD, minAge, maxAge, isOrphan, isSpecialNeed, page, rowsPerPage, orderby, -1, -1);
        }

        if(actionResult.success) {

            var list = (List<ChildAction.ChildItem>)actionResult.data;
            if (list.Count < 1) {
                actionResult.data = "";
                actionResult.Write(context);
                return;
            }
            Random rnd = new Random();
            var index = rnd.Next(list.Count);
            //Response.Write(list.ToJson());

            var item = list[index];
            actionResult.data = item;
        }

        actionResult.Write(context);
    }

    void GetChildCaseStudy( HttpContext context ) {
        var childMasterId = context.Request["childMasterId"];
        var childKey = context.Request["childKey"];
        new ChildAction().GetCaseStudy(childMasterId , childKey).Write(context);

    }

    void GetChild( HttpContext context ) {
        var childMasterId = context.Request["childMasterId"];

        if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            new ChildAction().GetChild(childMasterId).Write(context);
        }
        else if (ConfigurationManager.AppSettings["dbgp_kind"] == "2")
        {
            new ChildAction().GetChildGp(childMasterId).Write(context);
        }
    }

    void GetChildUserFunding( HttpContext context ) {
        var childMasterId = context.Request["childMasterId"];

        new ChildAction().GetChild(childMasterId).Write(context);
    }

    void GetChildGp( HttpContext context ) {
        var childMasterId = context.Request["childMasterId"];
        new ChildAction().GetChildGp(childMasterId).Write(context);
    }

    void GetChildren( HttpContext context ) {
        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        int? minAge = Convert.ToInt32(context.Request["minAge"].ValueIfNull("-1"));
        if(minAge == -1) {
            minAge = null;
        }
        int? maxAge = Convert.ToInt32(context.Request["maxAge"].ValueIfNull("-1"));
        if(maxAge == -1) {
            maxAge = null;
        }
        var country = context.Request["country"];
        var gender = context.Request["gender"];
        var today = context.Request["today"].EmptyIfNull();
        var mm = context.Request["mm"];
        var dd = context.Request["dd"];
        var orphan = context.Request["orphan"];
        var specialNeed = context.Request["specialNeed"];
        var orderby = context.Request["orderby"].EmptyIfNull();

        int? birthM = null;
        int? birthD = null;

        if (today == "Y") {
            birthM = Convert.ToInt32(DateTime.Now.ToString("MM"));
            birthD = Convert.ToInt32(DateTime.Now.ToString("dd"));
        } else {
            if(!string.IsNullOrEmpty(mm)) {
                birthM = Convert.ToInt32(mm);
            }
            if(!string.IsNullOrEmpty(dd)) {
                birthD = Convert.ToInt32(dd);
            }
        }

        bool? isOrphan = null;
        if (orphan == "Y") {
            isOrphan = true;
        }
        bool? isSpecialNeed = null;
        if (specialNeed == "Y") {
            isSpecialNeed = true;
        }

        if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            new ChildAction().GetChildren(country, gender, birthM, birthD, minAge, maxAge, isOrphan, isSpecialNeed, page, rowsPerPage , orderby,-1,-1).Write(context);
        }
        else if (ConfigurationManager.AppSettings["dbgp_kind"] == "2")
        {
            new ChildAction().GetChildrenGp(country, gender, birthM, birthD, minAge, maxAge, isOrphan, isSpecialNeed, page, rowsPerPage , orderby,-1,-1).Write(context);
        }
    }

    // event/캠페인 1:1 후원 사용인경우 사용
    void GetEventChild( HttpContext context ) {
        new ChildAction().GetChildren(null, null, null, null, null, null, null, null, 1, 1 , null,-1,-1).Write(context);
    }

    void GetWeddingChild( HttpContext context ) {

        var birth = context.Request["birth"].ValueIfNull(DateTime.Now.ToString("yyyy-MM-dd"));     // yyyy-MM-dd
        var mm = Convert.ToInt32(birth.Split('-')[1]);
        var dd = Convert.ToInt32(birth.Split('-')[2]);
        var action = new ChildAction();
        var actionResult = new JsonWriter() { success = false };
        //[이종진] 신규 방법일 경우, GlobalPool에서 가져오도록 수정
        if(ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            actionResult = action.GetChildren(null, null, mm, dd, null, null, null, null, 1, 1 , null,-1,-1);
        }
        else
        {
            actionResult = action.GetChildrenGp(null, null, mm, dd, null, null, null, null, 1, 1 , null,-1,-1);
        }
        //var actionResult = action.getchildren
        if(!actionResult.success) {
            actionResult.Write(context);
            return;
        }

        var list = (List<ChildAction.ChildItem>)actionResult.data;

        if(list.Count < 1) {
            //[이종진] 신규 방법일 경우, GlobalPool에서 가져오도록 수정
            if(ConfigurationManager.AppSettings["dbgp_kind"] == "1")
            {
                actionResult = action.GetChildren(null, null, mm, null, null, null, null, null, 1, 1,null,-1,-1);
            }
            else
            {
                actionResult = action.GetChildrenGp(null, null, mm, null, null, null, null, null, 1, 1,null,-1,-1);
            }

            actionResult.message = "죄송합니다! \n해당 날짜에 태어난 어린이가 없어 해당월에 태어난 어린이를 추천드립니다 ";
            list = (List<ChildAction.ChildItem>)actionResult.data;

        }

        if(list.Count < 1) {
            //[이종진] 신규 방법일 경우, GlobalPool에서 가져오도록 수정
            if(ConfigurationManager.AppSettings["dbgp_kind"] == "1")
            {
                actionResult = action.GetChildren(null, null, mm, null, null, null, null, null, 1, 1,null,-1,-1);
            }
            else
            {
                actionResult = action.GetChildrenGp(null, null, mm, null, null, null, null, null, 1, 1,null,-1,-1);
            }

            actionResult.message = "죄송합니다! \n해당 날짜와 해당 월에 태어난 어린이가 없어 후원이 필요한 어린이를 추천드립니다";
            list = (List<ChildAction.ChildItem>)actionResult.data;

        }

        if(actionResult.success) {

            var childMasterId = list[0].ChildMasterId;
            var child_result = new JsonWriter() { success = false };
            //[이종진] 신규 방법일 경우, GlobalPool에서 E-CommerceHold 
            if(ConfigurationManager.AppSettings["dbgp_kind"] == "1")
            {
                child_result = new ChildAction().Ensure(childMasterId);
            }
            else
            {
                string isOrphan = list[0].IsOrphan == false ? "N" : "Y";
                string isHandicapped = list[0].IsHandicapped == false ? "N" : "Y";
                child_result = new ChildAction().EnsureGp(childMasterId, list[0].ChildKey, list[0].Pic, isOrphan, isHandicapped, list[0].WaitingDays.ToString(), list[0].Age.ToString(), list[0].BirthDate.ToString(), list[0].CountryCode, list[0].Gender, list[0].HangulName, list[0].HangulPreferredName, list[0].FullName, list[0].PreferredName);
                //child_result = new ChildAction().EnsureGp(childMasterId);
            }
            if(!child_result.success) {
                actionResult.message = child_result.message;
                actionResult.success = false;
            }
        }

        actionResult.Write(context);



    }

    void GetUserFundingChildren( HttpContext context ) {

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        int? minAge = Convert.ToInt32(context.Request["minAge"].ValueIfNull("-1"));
        if(minAge == -1) {
            minAge = null;
        }

        //[이종진] 신규조회방법(GlobalPool 조회) 이면 GP에서 조회함
        if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            new ChildAction().GetChildren(null, null, null, null, minAge, 20, null, null, page, rowsPerPage, null, 16, 40).Write(context);
        }
        else
        {
            new ChildAction().GetChildrenGp(null, null, null, null, minAge, 20, null, null, page, rowsPerPage, null, 16, 40, true).Write(context);
        }

    }

    //[이종진]추가 - 나눔펀딩 어린이 선택 시,
    //BeneficiaryKit조회 (어린이 상세정보 조회)
    void GetUserFundingChildrenSelect( HttpContext context ) {

        JsonWriter result = new JsonWriter() { success = false };

        //ChildAction.ChildItem item = JsonConvert.DeserializeObject<ChildAction.ChildItem>(context.Request["item"]);

        string childMasterId = context.Request["ChildMasterId"].ValueIfNull("");
        string childKey = context.Request["ChildKey"].ValueIfNull("");
        string childAge = context.Request["Age"].ValueIfNull(""); // Benekit조회 시, 나이가 오지 않기 때문에 화면단에서 조회한 데이터를 넘겨줌
        int age = string.IsNullOrEmpty(childAge) ? 0 : Int32.Parse(childAge);

        ChildAction.ChildItem item = new ChildAction.ChildItem();

        //오류
        if(string.IsNullOrEmpty(childMasterId) || string.IsNullOrEmpty(childKey))
        {
            result.Write(context);
            return;
        }

        //[이종진] 기존방법이면 data에 db를 넣고 바로 리턴
        if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            result.success = true;
            result.data = "db";
            result.Write(context);
            return;
        }
        else //신규방법조회 - GlobalPool에서 BeneficiaryKit조회 (어린이 상세정보조회)
        {
            result = new ChildAction().Beneficiary_GET_UserFunding(childMasterId, age);

            result.Write(context);
        }

    }

}