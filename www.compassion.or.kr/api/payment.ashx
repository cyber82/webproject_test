﻿<%@ WebHandler Language="C#" Class="api_payment" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_payment : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest( HttpContext context ) {

		var t = context.Request["t"].EmptyIfNull();

		if(t == "check-cms-owner") {
			this.CheckCMSOwner(context);
		}
	}

	void CheckCMSOwner( HttpContext context ) {

		var bankCode = context.Request["bankCode"].EmptyIfNull();
		var bankAccount = context.Request["bankAccount"].EmptyIfNull();
		var birth = context.Request["birth"].EmptyIfNull();
		var owner = context.Request["owner"].EmptyIfNull();

		var actionResult = new HSCMSAction().Verify(bankCode, bankAccount, birth, owner);
		//var actionResult = new CMSAccount().Verify(bankCode, bankAccount, birth, owner);
		actionResult.Write(context);

	}

	

}