﻿<%@ WebHandler Language="C#" Class="api_special_funding" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_special_funding : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest( HttpContext context ) {

		var t = context.Request["t"].EmptyIfNull();

		if(t == "get") {
			this.GetItem(context);
		} else if(t == "report") {
			this.Report(context);
		} else if(t == "get-total-amount") {
			this.GetTotalCurrentAmount(context);
		} else if(t == "update-total-amount") {
			this.UpdateTotalCurrentAmount(context);
		}
		if(t == "complete-list") {
			this.GetCompleteList(context);
		}
	}

	void GetCompleteList( HttpContext context ) {

		var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
		var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

		new SpecialFundingAction().GetCompleteList(page, rowsPerPage).Write(context);

	}

	void GetItem( HttpContext context ) {

		var campaignId = context.Request["campaignId"].EmptyIfNull();

		new SpecialFundingAction().GetItem(campaignId).Write(context);

	}

	void Report( HttpContext context ) {

		var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
		var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
		var campaignId = context.Request["campaignId"].EmptyIfNull();
		var update_root = Uploader.GetRoot(Uploader.FileGroup.file_special_funding);

		new SpecialFundingAction().GetReportList(page, rowsPerPage, campaignId, update_root).Write(context);
	}

	void GetTotalCurrentAmount( HttpContext context ) {

		var campaignId = context.Request["campaignId"].EmptyIfNull();
		new PaymentAction().GetTotalAmountByCampaign(campaignId).Write(context);
	}

	void UpdateTotalCurrentAmount( HttpContext context ) {

		var campaignId = context.Request["campaignId"].EmptyIfNull();

		new SpecialFundingAction().UpdateTotalCurrentAmount(campaignId).Write(context);
	}
}