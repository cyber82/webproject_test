﻿<%@ WebHandler Language="C#" Class="api_cad" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_cad : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest( HttpContext context ) {

		var t = context.Request["t"].EmptyIfNull();

		if(t == "list") {
			this.GetList(context);
		}

	}

		/// <summary>
		///  CAD 목록
		/// </summary>
		/// <param name="context"></param>
	 void GetList( HttpContext context ) {
		var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
		var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
		

		var list = new CADAction().GetList(page, rowsPerPage);

		JsonWriter.Write(list, context);
	}

}