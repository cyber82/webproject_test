﻿<%@ WebHandler Language="C#" Class="api_recruit" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_recruit : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "check_email") {
            CheckEmail(context);
        }else if(t == "get_depth2") {
            GetDepth2(context);
        }
    }

    void CheckEmail(HttpContext context) {
        var email = context.Request["email"].EmptyIfNull();
        if (email == "") return;
        var result = new JsonWriter() {
            success = true
        };
        using (RecruitDataContext dao = new RecruitDataContext())
        {
            var exist = www6.selectQ<applicant>("ap_email", email);
            //if (dao.applicant.Any(p => p.ap_email == email))
            if(exist.Any())
                result.success = false;
        }
        JsonWriter.Write(result, context);
    }



    void GetDepth2(HttpContext context)
    {
        var depth1 = context.Request["depth1"].EmptyIfNull();
        if (depth1 == "") return;

        var result = new JsonWriter()
        {
            success = true
        };

        using (RecruitDataContext dao = new RecruitDataContext())
        {
            //var list = dao.recruit_jobcode.Where(p => p.rj_display && p.rj_depth1 == depth1 && p.rj_depth2 != "").OrderBy(p => p.rj_order);
            var list = www6.selectQ2<recruit_jobcode>("rj_display = ", 1, "rj_depth1 = ", depth1, "rj_depth2 != ", "").OrderBy(p => p.rj_order);
            result.data = list.ToJson();
        }
        JsonWriter.Write(result, context);
    }
}