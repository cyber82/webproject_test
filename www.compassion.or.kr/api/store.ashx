﻿<%@ WebHandler Language="C#" Class="api_store" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_store : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "set_basket") {
            this.SetBasket(context);
        } else if(t == "get_children") {
            this.GetChildren(context);
        } else if(t == "get_basket") {
            this.GetBasket(context);
        } else if(t == "get_order_temp") {
            this.GetOrderTemp(context);
        } else if(t == "set_order_temp") {
            this.SetOrderTemp(context);
        } else if(t == "remove_basket") {
            this.RemoveBasket(context);
        } else if(t == "update_basket") {
            this.UpdateBasket(context);
        } else if(t == "review_list") {
            this.GetReviewList(context);
        } else if(t == "qna_list") {
            this.GetQnaList(context);
        } else if(t == "notice_list") {
            this.GetNoticeList(context);
        } else if(t == "add_qna") {
            this.AddQna(context);
        } else if(t == "add_review") {
            this.AddReview(context);
        } else if(t == "reviewable_list") {
            this.GetReviewableList(context);
        } else if(t == "order_list") {
            this.GetOrderList(context);
        } else if(t == "order_detail_list") {
            this.GetOrderDetailList(context);
        } else if(t == "get_store_list") {
            this.GetStoreList(context);
        } else if(t == "update_qna") {
            this.UpdateQna(context);
        } else if(t == "delete_qna") {
            this.DeleteQna(context);
        } else if(t == "increase_notice_hit") {
            this.IncreaseNoticeHit(context);
        } else if(t == "check_review"){
            this.CheckReview(context);
        }
    }

    void IncreaseNoticeHit(HttpContext context ) {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var idx = Convert.ToInt32(context.Request["idx"].ValueIfNull("-1"));

        if(idx == -1) {
            result.success = false;
            result.message = "잘못된 접근입니다.";
        }

        using (StoreDataContext dao = new StoreDataContext())
        {
            //var entity = dao.notice.First(p => p.idx == idx);
            var entity = www6.selectQFStore<notice>("idx", idx);
            entity.viewCount++;

            //dao.SubmitChanges();
            www6.updateStore(entity);
        }

        JsonWriter.Write(result, context);
    }


    void GetStoreList( HttpContext context ) {
        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var gift_flag = Convert.ToInt32(context.Request["gift_flag"].ValueIfNull("-1"));
        var keyword = context.Request["keyword"].EmptyIfNull().EscapeSqlInjection();
        var imagePath = Uploader.GetRoot(Uploader.FileGroup.image_shop);

        JsonWriter.Write(new StoreAction().GetStoreList(page, rowsPerPage, gift_flag, keyword , imagePath), context);
    }


    void GetReviewableList( HttpContext context ) {

        JsonWriter.Write(new StoreAction().GetReviewableList(), context);
    }

    void GetOrderList( HttpContext context ) {
        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var startdate = context.Request["date_begin"].ValueIfNull("").EscapeSqlInjection();
        var enddate = context.Request["date_end"].EmptyIfNull().EscapeSqlInjection();
        var imagePath = Uploader.GetRoot(Uploader.FileGroup.image_shop);

        JsonWriter.Write(new StoreAction().GetOrderList(page, rowsPerPage, startdate, enddate, imagePath), context);
    }

    void GetOrderDetailList( HttpContext context ) {

        var orderNo = context.Request["orderNo"].EmptyIfNull().EscapeSqlInjection();
        var imagePath = Uploader.GetRoot(Uploader.FileGroup.image_shop);
        JsonWriter.Write(new StoreAction().GetOrderDetailList(orderNo, imagePath), context);
    }


    void CheckReview(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var product_idx = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));

        if(product_idx == -1 ) {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (StoreDataContext dao = new StoreDataContext())
        {
            var userInfo = new UserInfo();


            // 상품 구매 여부 체크
            //var checkPass = dao.sp_review_check_f(product_idx, userInfo.UserId).ToList();
            Object[] op1 = new Object[] { "product_idx", "user_id" };
            Object[] op2 = new Object[] { product_idx, userInfo.UserId };
            var checkPass = www6.selectSPStore("sp_review_check_f", op1, op2).DataTableToList<sp_review_check_fResult>();

            if (checkPass[0].result == "N")
            {
                result.success = false;
                result.message = "구매하신 상품만 후기 작성이 가능합니다.";
                JsonWriter.Write(result, context);
                return;
            }

            // 중복 리뷰 등록 체크 
            var exist = www6.selectQStore<review>("product_idx", product_idx, "user_id", userInfo.UserId);
            //if (dao.review.Any(p => p.product_idx == product_idx && p.user_id == userInfo.UserId))
            if(exist.Any())
            {
                result.success = false;
                result.message = "이미 등록한 리뷰가 있습니다.";
                JsonWriter.Write(result, context);
                return;
            }
        }


        JsonWriter.Write(result, context);

    }


    void AddReview( HttpContext context ) {

        JsonWriter result = new JsonWriter();
        result.success = true;


        CheckReview( context );


        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var product_idx = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var body = context.Request["content"].EmptyIfNull();
        var title = body;

        if(product_idx == -1 || body == "") {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }



        using (StoreDataContext dao = new StoreDataContext())
        {
            var userInfo = new UserInfo();


            // 상품 구매 여부 체크
            //var checkPass = dao.sp_review_check_f(product_idx, userInfo.UserId).ToList();
            Object[] op1 = new Object[] { "product_idx", "user_id" };
            Object[] op2 = new Object[] { product_idx, userInfo.UserId };
            var checkPass = www6.selectSPStore("sp_review_check_f", op1, op2).DataTableToList<sp_review_check_fResult>();

            if (checkPass[0].result == "N")
            {
                result.success = false;
                result.message = "구매하신 상품만 후기 작성이 가능합니다.";
                JsonWriter.Write(result, context);
                return;
            }

            // 중복 리뷰 등록 체크 
            var exist = www6.selectQStore<review>("product_idx", product_idx, "user_id", userInfo.UserId);
            //if (dao.review.Any(p => p.product_idx == product_idx && p.user_id == userInfo.UserId))
            if(exist.Any())
            {
                result.success = false;
                result.message = "이미 등록한 리뷰가 있습니다.";
                JsonWriter.Write(result, context);
                return;
            }

            var entity = new review()
            {
                product_idx = product_idx,
                title = title,
                body = body,
                user_id = userInfo.UserId,
                user_name = userInfo.UserName,
                reg_date = DateTime.Now,
                isdel = 'N'
            };
            //dao.review.InsertOnSubmit(entity);
            www6.insertStore(entity);
            //dao.SubmitChanges();
        }

        JsonWriter.Write(result, context);
    }

    void AddQna( HttpContext context ) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var product_idx = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var question = context.Request["content"].EmptyIfNull();
        var isOpen = context.Request["isOpen"].ValueIfNull("-1");

        var title = question;

        if(product_idx == -1 || question == "") {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (StoreDataContext dao = new StoreDataContext())
        {
            var userInfo = new UserInfo();

            var entity = new qna()
            {
                product_idx = product_idx,
                user_id = userInfo.UserId,
                reg_date = DateTime.Now,
                title = title,
                question = question,
                isOpen = isOpen == "-1" ? false : true
            };
            //dao.qna.InsertOnSubmit(entity);
            www6.insertStore(entity);
            //dao.SubmitChanges();
        }

        JsonWriter.Write(result, context);
    }



    void UpdateQna(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var idx = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var question = context.Request["content"].EmptyIfNull();

        var title = question;

        if(idx == -1 || question == "") {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (StoreDataContext dao = new StoreDataContext())
        {
            var userInfo = new UserInfo();

            //var entity = dao.qna.First(p => p.idx == Convert.ToInt32(idx));
            var entity = www6.selectQFStore<qna>("idx", Convert.ToInt32(idx));

            var owner_id = entity.user_id;

            // 로그인한 유저가 맞는지 다시 확인 
            if (userInfo.UserId != owner_id)
            {
                result.success = false;
                result.message = "정상적인 접근이 아닙니다.";
                JsonWriter.Write(result, context);
                return;
            }

            // 수정 
            entity.question = question;
            entity.title = title;
            //dao.SubmitChanges();
            www6.updateStore(entity);
        }

        JsonWriter.Write(result, context);

    }


    void DeleteQna(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var idx = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));

        if(idx == -1) {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (StoreDataContext dao = new StoreDataContext())
        {
            var userInfo = new UserInfo();

            //var entity = dao.qna.First(p => p.idx == Convert.ToInt32(idx));
            var entity = www6.selectQFStore<qna>("idx", Convert.ToInt32(idx));

            var owner_id = entity.user_id;

            // 로그인한 유저가 맞는지 다시 확인 
            if (userInfo.UserId != owner_id)
            {
                result.success = false;
                result.message = "정상적인 접근이 아닙니다.";
                JsonWriter.Write(result, context);
                return;
            }

            // 삭제
            //dao.qna.DeleteOnSubmit(entity);
            www6.deleteStore(entity);
            //dao.SubmitChanges();
        }

        result.message = "삭제 되었습니다.";

        JsonWriter.Write(result, context);
    }


    void GetNoticeList( HttpContext context ) {

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var idx = Convert.ToInt32(context.Request["idx"].ValueIfNull("-1"));
        var type = context.Request["type"].ValueIfNull("").EscapeSqlInjection();
        var keyword = context.Request["keyword"].EmptyIfNull().EscapeSqlInjection();

        JsonWriter.Write(new StoreAction().GetNoticeList(page, rowsPerPage, idx, type, keyword), context);
    }



    void GetQnaList( HttpContext context ) {

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var product_idx = Convert.ToInt32(context.Request["product_idx"].ValueIfNull("-1"));
        var type = context.Request["type"].ValueIfNull("").EscapeSqlInjection();
        var imagePath = Uploader.GetRoot(Uploader.FileGroup.image_shop);

        var list = new StoreAction().GetQnaList(page, rowsPerPage, product_idx, type , imagePath);
        if(list != null) {
            foreach(StoreAction.sp_qna_list_fResultEx item in (List<StoreAction.sp_qna_list_fResultEx>)list.data) {
                //item.question = item.question.Replace("<br>", "\r\n");
                item.question = item.question == null ? "" : item.question.Replace("<br>", "\r\n");
            }
        }

        JsonWriter.Write(list, context);
    }

    void GetReviewList( HttpContext context ) {

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var product_idx = Convert.ToInt32(context.Request["product_idx"].ValueIfNull("-1"));
        var type = context.Request["type"].ValueIfNull("").EscapeSqlInjection();
        var user_id = "";
        var imagePath = Uploader.GetRoot(Uploader.FileGroup.image_shop);

        var userinfo = new UserInfo();
        if (type != "all"){
            user_id = userinfo.UserId;
        }


        var list = new StoreAction().GetReviewList(page, rowsPerPage, product_idx, user_id);

        foreach(sp_review_list_fResult item in (List<sp_review_list_fResult>)list.data) {
            item.name_img = (imagePath + item.name_img).WithFileServerHost();
            if(item.user_id.Length > 3)
                item.user_id = item.user_id.Mask("*", 3);
        }

        JsonWriter.Write(list, context);
    }

    void GetBasket( HttpContext context ) {
        var imagePath = Uploader.GetRoot(Uploader.FileGroup.image_shop);
        JsonWriter.Write(new StoreAction().GetBasket(imagePath), context);
    }

    void GetOrderTemp( HttpContext context ) {
        var imagePath = Uploader.GetRoot(Uploader.FileGroup.image_shop);
        JsonWriter.Write(new StoreAction().GetOrderTemp(imagePath), context);
    }

    void SetOrderTemp( HttpContext context ) {
        var data = context.Request["data"].EmptyIfNull();
        if(data == "")
            return;

        var basket_ids = data.ToObject<List<int>>();
        JsonWriter.Write(new StoreAction().SetOrderTemp(basket_ids), context);
    }

    void RemoveBasket( HttpContext context ) {
        var data = context.Request["data"].EmptyIfNull();
        if(data == "")
            return;

        var basket_ids = data.ToObject<List<int>>();
        JsonWriter.Write(new StoreAction().RemoveBasket(basket_ids), context);
    }

    void UpdateBasket( HttpContext context ) {
        var basket_id = Convert.ToInt32(context.Request["basket_id"].ValueIfNull("0"));
        var ea = Convert.ToInt32(context.Request["ea"].ValueIfNull("1"));
        JsonWriter.Write(new StoreAction().UpdateBasket(basket_id, ea), context);
    }

    void SetBasket( HttpContext context ) {
        var data = context.Request["data"].EmptyIfNull();
        if(data == "")
            return;

        JsonWriter.Write(new StoreAction().SetBasket(data), context);
    }

    void GetChildren( HttpContext context ) {
        JsonWriter result = new JsonWriter() { success = false};

        var list = new ChildAction().GetGitfableList();
        if(list.success) {
            var data = (Dictionary<string, object>)list.data;
            var children = data["list"];
            result.success = true;
            result.data = children;
        } else {
            result.message = list.message;
        }

        result.Write(context);
        //JsonWriter.Write(new StoreAction().GetChildren(), context);
    }

}