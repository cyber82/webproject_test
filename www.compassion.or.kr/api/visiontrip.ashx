﻿<%@ WebHandler Language="C#" Class="api_visiontrip" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

public class api_visiontrip : BaseHandler
{
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();
    //CommonLib.WWW6Service.SoaHelperSoap _www6Service;
    CommonLib.WWWService.ServiceSoap _wwwService;
    CommonLib.WWW6Service.SoaHelperSoap _www6Service;

    public override void OnRequest(HttpContext context)
    {

        var t = context.Request["t"].EmptyIfNull();

        if (t == "list")
        {
            this.GetList(context);
        }
        else if (t == "schedulelist")
        {
            this.GetScheduleList(context);
        }
        else if (t == "schedule_check")
        {
            this.GetScheduleCheck(context);
        }
        else if (t == "childlist")
        {
            this.GetChildList(context);
        }
        else if (t == "country")
        {
            this.GetContryList(context);
        }
        else if (t == "add_planapply")
        {
            this.Add_PlanApply(context);
        }
        else if (t == "add_individualapply")
        {
            this.Add_IndividualApply(context);
        }
        // my visiontrip 
        else if (t == "my_visiontriplist")
        {
            this.GetMyVisionTripList(context);
        }
        else if (t == "my_individualtriplist")
        {
            this.GetMyIndividualTripList(context);
        }
        else if (t == "my_historylist")
        {
            this.GetMyTripHistoryList(context);
        }
        else if (t == "attachlist")
        {
            this.GetAttach(context);
        }
        else if (t == "update_my_attach")
        {
            this.Update_Attach(context);
        }
        else if (t == "delete_my_attach")
        {
            this.Delete_Attach(context);
        }
        else if (t == "childmeetlist")
        {
            this.GetChildMeetList(context);
        }
        else if (t == "set-virtual-account")
        {
            this.SetVirtualAccount(context);
        }
        else if (t == "virtual-account")
        {
            this.GetVirtualAccount(context);
        }
    }

    // PayItemSession 에 값이 있는 경우만 가능  
    void GetList(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var type = context.Request["type"].EmptyIfNull();
        var year = context.Request["year"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_visiontrip_list_f(1, 9999, type, year).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "year" };
            Object[] op2 = new Object[] { 1, 9999, type, year };
            var list = www6.selectSP("sp_visiontrip_list_f", op1, op2).DataTableToList<sp_visiontrip_list_fResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void GetContryList(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.country.OrderBy(p => p.c_name).ToList();
            var list = www6.selectQ<country>("c_name");
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    // PayItemSession 에 값이 있는 경우만 가능
    void GetScheduleList(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var type = context.Request["type"].EmptyIfNull();
        var year = context.Request["year"].EmptyIfNull();
        var requestkey = context.Request["requestkey"].EmptyIfNull();
        var userid = context.Request["userid"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripSchedule_list_f(1, 9999, type, year, requestkey, userid).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "year", "requestkey", "userid" };
            Object[] op2 = new Object[] { 1, 9999, type, year, requestkey, userid };
            var list = www6.selectSP("sp_tVisionTripSchedule_list_f", op1, op2).DataTableToList<sp_tVisionTripSchedule_list_fResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void GetScheduleCheck(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var scheduleid = Convert.ToInt32(context.Request["scheduleid"].ValueIfNull("0"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.tVisionTripSchedule.Where(p => p.ScheduleID == scheduleid && p.CurrentUse == 'Y').FirstOrDefault();
            var list = www6.selectQF<tVisionTripSchedule>("ScheduleID", scheduleid, "CurrentUse", "Y");
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void GetChildList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;
        new ChildAction().MyChildren("", 1, 10000, false).Write(context);
    }

    void SendSMS(tVisionTripApply applyEntity, HttpContext context)
    {
        var userInfo = new UserInfo();

        try
        {
            var smsMessage = "";
            if (applyEntity.ApplyType == "Plan")
            {
                smsMessage = "[컴패션] " + userInfo.UserName + "님, 비전트립 신청이 완료되었습니다. 감사합니다";
            }
            else
            {
                smsMessage = "[컴패션] " + userInfo.UserName + "님, 개인방문 신청이 완료되었습니다. 감사합니다";
            }

            var smsSendYN = util.SendSMS.SMSSend(applyEntity.Tel.Decrypt(), userInfo.UserName, smsMessage);
            using (FrontDataContext dao = new FrontDataContext())
            {
                var entity = new tVisionTripMessage()
                {
                    ApplyID = applyEntity.ApplyID,
                    UserID = userInfo.UserId,
                    SponsorID = applyEntity.SponsorID,
                    Message = smsMessage,
                    GroupNo = StringExtension.GetRandomString(3) + DateTime.Now.ToString("yyyyMMddHHmmss"),
                    ReceiveTel = applyEntity.Tel.Decrypt(),
                    ReceiveID = applyEntity.UserID,
                    ReceiveName = userInfo.UserName,
                    SendType = 'D', //즉시발송
                    MessageType = applyEntity.ApplyType == "Plan" ? "PlanApply" : "IndividualApply",
                    SendTel = ConfigurationManager.AppSettings["SMSSendNo"].ToString().Trim(),
                    SendYN = smsSendYN ? 'Y' : 'N',
                    SendDate = DateTime.Now,
                    CurrentUse = 'Y',
                    RegisterID = userInfo.UserId,
                    RegisterName = userInfo.UserName,
                    RegisterDate = DateTime.Now
                };
                //dao.tVisionTripMessage.InsertOnSubmit(entity);
                www6.insert(entity);
                //dao.SubmitChanges();
            }
        }
        catch (Exception e)
        {
            ErrorLog.Write(context, 0, e.ToString());
        }
    }

    void Add_PlanApply(HttpContext context)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        JsonWriter result = new JsonWriter();
        result.success = true;
        result.data = 0;

        if (!UserInfo.IsLogin)
        {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        #region param
        var scheduleID = context.Request["scheduleID"].EmptyIfNull();
        var applyType = context.Request["applyType"].EmptyIfNull();
        var sponsorNameEng = context.Request["sponsorNameEng"].EmptyIfNull();
        var tel = context.Request["tel"].EmptyIfNull();
        var email = context.Request["email"].EmptyIfNull();
        //국내외 
        var location = context.Request["location"].EmptyIfNull();
        var country = context.Request["country"].EmptyIfNull();

        var address1 = context.Request["address1"].EmptyIfNull();
        var address2 = context.Request["address2"].EmptyIfNull();
        var zipcode = context.Request["zipcode"].EmptyIfNull();
        var changeYn = context.Request["changeYn"].EmptyIfNull();
        var religion = context.Request["religion"].EmptyIfNull();
        var church = context.Request["church"].EmptyIfNull();
        var emergencycontactName = context.Request["emergencycontactName"].EmptyIfNull();
        var emergencycontactTel = context.Request["emergencycontactTel"].EmptyIfNull();
        var emergencycontactRelation = context.Request["emergencycontactRelation"].EmptyIfNull();
        var childmeetYn = context.Request["childmeetYn"].EmptyIfNull();
        var scheduleagreeYn = context.Request["scheduleagreeYn"].EmptyIfNull();
        var gender = context.Request["gender"].EmptyIfNull();
        var nation = context.Request["nation"].EmptyIfNull();
        var englishLevel = context.Request["englishLevel"].EmptyIfNull();
        var visiontripHistory = context.Request["visiontripHistory"].EmptyIfNull();
        var job = context.Request["job"].EmptyIfNull();
        var military = context.Request["military"].EmptyIfNull();
        var roomType = context.Request["roomType"].EmptyIfNull();
        var roomDetail = context.Request["roomDetail"].EmptyIfNull();
        var cashreceiptType = context.Request["cashreceiptType"].EmptyIfNull();
        var cashreceiptName = context.Request["cashreceiptName"].EmptyIfNull();
        var cashreceiptTel = context.Request["cashreceiptTel"].EmptyIfNull();
        var cashreceiptRelation = context.Request["cashreceiptRelation"].EmptyIfNull();
        var acceptTerms = context.Request["acceptTerms"].EmptyIfNull();
        var applyAgree = context.Request["applyAgree"].EmptyIfNull();
        var childList = context.Request["childList"].EmptyIfNull();
        var attachFile = context.Request["attachFile"].EmptyIfNull();
        #endregion

        if (scheduleID == "")
        {
            result.success = false;
            result.message = "신청서 정보가 잘못되었습니다.";
            JsonWriter.Write(result, context);
            return;
        }

        //string location = "국내";
        //string country = "한국";

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            #region Compass - tSponsorGroup : GroupType 가져오기 
            Object[] objSql = new object[1] { "VT_WebInfoInquiry" };
            Object[] objParam = new object[] { "DIVIS", "ConID" };
            Object[] objValue = new object[] { "GroupInfo", userInfo.SponsorID };

            var grouptype = "";
            var grouplist = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
            if (grouplist.Rows.Count > 0)
            {
                grouptype = grouplist.Rows[0]["GroupType"].ToString();
            }
            #endregion

            #region 후원어린이 수 
            int iCommitmentCount = 0;
            iCommitmentCount = getCommitmentCount(userInfo.SponsorID);
            #endregion

            //var list = dao.tVisionTripApply.Where(p => p.ScheduleID == Convert.ToInt32(scheduleID) && p.UserID == userInfo.UserId && p.CurrentUse == 'Y').FirstOrDefault();
            var list = www6.selectQF<tVisionTripApply>("ScheduleID", Convert.ToInt32(scheduleID), "UserID", userInfo.UserId, "CurrentUse", "Y");
            if (list != null)
            {
                result.success = false;
                result.message = "이미 신청하셨습니다.";
                JsonWriter.Write(result, context);
                return;
            }
            #region 신청비 가져오기
            //var scheduleData = dao.tVisionTripSchedule.Where(p => p.ScheduleID == Convert.ToInt32(scheduleID) && p.CurrentUse == 'Y').FirstOrDefault();
            var scheduleData = www6.selectQF<tVisionTripSchedule>("ScheduleID", Convert.ToInt32(scheduleID), "CurrentUse", "Y");
            decimal requestCost = 0;
            if (scheduleData != null)
            {
                if (scheduleData.RequestCost != null)
                {
                    bool check = decimal.TryParse(scheduleData.RequestCost, out requestCost);
                }
            }
            #endregion


            #region SponsorID가 없을 경우 tempSponsorID 가져오기
            var tempSponsorID = userInfo.SponsorID;
            if (string.IsNullOrEmpty(userInfo.SponsorID))
            {
                using (AuthLibDataContext daoAuth = new AuthLibDataContext())
                {
                    //var entity = daoAuth.tSponsorMaster.First(p => p.UserID == userInfo.UserId);
                    var entity = www6.selectQFAuth<tSponsorMaster>("UserID", userInfo.UserId);
                    tempSponsorID = entity.tempSponsorID;
                }
            }
            #endregion

            string strBirthDate =  userInfo.Birth != "" ? userInfo.Birth.Substring(0, 10).Replace("-","") : "";
            var vtApplyList = new List<tVisionTripApply>();
            vtApplyList.Add(new tVisionTripApply()
            {
                ScheduleID = Convert.ToInt32(scheduleID),
                ApplyType = applyType,
                SponsorID = tempSponsorID,//userInfo.SponsorID,
                UserID = userInfo.UserId,
                SponsorName = userInfo.UserName,
                SponsorNameEng = sponsorNameEng,
                GenderCode = userInfo.GenderCode,
                BirthDate = strBirthDate.Encrypt(),
                Tel = tel.Encrypt(),
                Email = email.Encrypt(),
                LocationType = location,
                Address1 = (address1 + "$" + country).Encrypt(),
                Address2 = address2.Encrypt(),
                ZipCode = zipcode.Encrypt(),
                ChangeYN = changeYn != "Y" ? 'N' : 'Y',
                ReligionType = religion,
                ChurchName = church,
                EmergencyContactName = emergencycontactName,
                EmergencyContactTel = emergencycontactTel,
                EmergencyContactRelation = emergencycontactRelation,
                ChildMeetYN = childmeetYn != "Y" ? 'N' : 'Y',
                ApplyDate = DateTime.Now,
                CompassYN = 'N',
                GroupType = grouptype,
                CommitmentCount = iCommitmentCount, //후원어린이 수
                CurrentUse = 'Y',
                RegisterID = userInfo.UserId,
                RegisterName = userInfo.UserName,
                RegisterDate = DateTime.Now
            });

            //dao.tVisionTripApply.InsertAllOnSubmit(vtApplyList);
            foreach(var vta in vtApplyList)
            {
                www6.insert(vta);
            }
            //dao.SubmitChanges();

            int iApplyID = 0;
            iApplyID = vtApplyList[0].ApplyID;
            try
            {
                if (iApplyID > 0)
                {
                    #region 상세
                    var vtPlanDetail = new List<tVisionTripPlanDetail>();
                    vtPlanDetail.Add(new tVisionTripPlanDetail()
                    {
                        ApplyID = iApplyID,
                        ScheduleAgreeYN = scheduleagreeYn != "Y" ? 'N' : 'Y',
                        GenderCode = Convert.ToChar(gender),
                        Nation = Convert.ToChar(nation),
                        EnglishLevel = Convert.ToChar(englishLevel),
                        VisionTripHistory = Convert.ToChar(visiontripHistory),
                        Job = job,
                        MilitaryYN = Convert.ToChar(military),
                        RoomType = Convert.ToChar(roomType),
                        RoomDetail = roomDetail,
                        CashReceiptType = Convert.ToChar(cashreceiptType),
                        CashReceiptName = cashreceiptName,
                        CashReceiptTel = cashreceiptTel,
                        CashReceiptRelation = cashreceiptRelation,
                        AcceptTerms = acceptTerms,
                        ApplyAgree = Convert.ToChar(applyAgree),
                        ApplyState = 'A',
                        RequestCost = requestCost
                    });
                    //dao.tVisionTripPlanDetail.InsertAllOnSubmit(vtPlanDetail);
                    foreach(var vta in vtPlanDetail)
                    {
                        www6.insert(vta);
                    }
                    #endregion

                    #region 후원어린이정보
                    if (childList != "")
                    {
                        var vtChildMeetList = new List<tVisionTripChildMeet>();
                        JArray jResult = JArray.Parse(childList);
                        for (int i = 0; i < jResult.Count; i++)
                        {
                            string childKey = jResult[i]["id"].ToString();
                            string childName = jResult[i]["name"].ToString();

                            vtChildMeetList.Add(new tVisionTripChildMeet()
                            {
                                ApplyID = iApplyID,
                                ChildKey = childKey,
                                ChildName = childName,
                                ChildMeetType = childName == "" ? 'I' : 'S', //(I 직접입력, S 선택) 
                                CurrentUse = 'Y',
                                RegisterDate = DateTime.Now,
                                RegisterName = userInfo.UserName,
                                RegisterID = userInfo.UserId
                            });
                        }
                        //dao.tVisionTripChildMeet.InsertAllOnSubmit(vtChildMeetList);
                        foreach(var vta in vtChildMeetList)
                        {
                            www6.insert(vta);
                        }
                    }
                    #endregion
                    #region 첨부파일
                    if (attachFile != "")
                    {
                        var vtAttachList = new List<tVisionTripAttach>();

                        JArray jResult = JArray.Parse(attachFile);
                        for (int i = 0; i < jResult.Count; i++)
                        {
                            string attachType = jResult[i]["type"].ToString();
                            string attachName = jResult[i]["name"].ToString();
                            string attachPath = jResult[i]["path"].ToString();

                            vtAttachList.Add(new tVisionTripAttach()
                            {
                                ApplyID = iApplyID,
                                AttachType = attachType,
                                AttachName = attachName,
                                AttachPath = attachPath,
                                CurrentUse = 'Y',
                                RegisterDate = DateTime.Now,
                                RegisterName = userInfo.UserName,
                                RegisterID = userInfo.UserId
                            });
                        }
                        //dao.tVisionTripAttach.InsertAllOnSubmit(vtAttachList);
                        foreach(var vta in vtAttachList)
                        {
                            www6.insert(vta);
                        }
                    }
                    #endregion

                }

                #region 사용자 정보 변경
                if (changeYn.Equals("Y"))
                {
                    try
                    {
                        //WEB
                        using (AuthLibDataContext daoAuth = new AuthLibDataContext())
                        {
                            //var entity = daoAuth.tSponsorMaster.First(p => p.UserID == userInfo.UserId);
                            var entity = www6.selectQFAuth<tSponsorMaster>("UserID", userInfo.UserId);
                            if (!string.IsNullOrEmpty(email))
                                entity.Email = email.Encrypt();
                            if (!string.IsNullOrEmpty(tel))
                                entity.Phone = tel.Encrypt();

                            //if (!string.IsNullOrEmpty(zipcode))
                            //    entity.ZipCode = zipcode.Encrypt();
                            //if (!string.IsNullOrEmpty(address1))
                            //    entity.Addr1 = (address1 + "$" + country).Encrypt();
                            ////entity.Addr1 = addr1.Encrypt();
                            //if (!string.IsNullOrEmpty(address2))
                            //    entity.Addr2 = address2.Encrypt();

                            //daoAuth.SubmitChanges();
                            www6.updateAuth(entity);
                        }

                        // 후원회원인경우 
                        if (!string.IsNullOrEmpty(userInfo.SponsorID))
                        {
                            var userInfoResult = UpdateCompassUserInfo(userInfo.SponsorID, location, country, address1, address2, zipcode, tel, email);
                            if (!userInfoResult.success)
                            {
                                result.success = false;
                                result.message = userInfoResult.message;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                        result.success = false;
                        result.message = "회원님의 정보를 수정하는 중 오류가 발생했습니다.(WEB)";
                        //return;
                    }
                }
                #endregion

                //dao.SubmitChanges();

                result.data = iApplyID;
                //sms발송
                //var applyEntity = dao.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                var applyEntity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(iApplyID));
                SendSMS(applyEntity, context);
            }
            catch (Exception ex)
            {
                using (FrontDataContext daoDel = new FrontDataContext())
                {
                    //   iApplyID
                    //var entity = daoDel.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                    var entity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(iApplyID));
                    //daoDel.tVisionTripApply.DeleteOnSubmit(entity);
                    //daoDel.SubmitChanges();
                    www6.delete(entity);
                }
                ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                result.success = false;
                result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
            }
        }
        JsonWriter.Write(result, context);
    }

    void Add_IndividualApply(HttpContext context)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        JsonWriter result = new JsonWriter();
        result.success = true;
        result.data = 0;

        if (!UserInfo.IsLogin)
        {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        #region param 
        var applyType = context.Request["applyType"].EmptyIfNull();
        var sponsorNameEng = context.Request["sponsorNameEng"].EmptyIfNull();
        var tel = context.Request["tel"].EmptyIfNull();
        var email = context.Request["email"].EmptyIfNull();
        //국내외 
        var location = context.Request["location"].EmptyIfNull();
        var country = context.Request["country"].EmptyIfNull();

        var address1 = context.Request["address1"].EmptyIfNull();
        var address2 = context.Request["address2"].EmptyIfNull();
        var zipcode = context.Request["zipcode"].EmptyIfNull();
        var changeYn = context.Request["changeYn"].EmptyIfNull();
        var religion = context.Request["religion"].EmptyIfNull();
        var church = context.Request["church"].EmptyIfNull();
        var emergencycontactName = context.Request["emergencycontactName"].EmptyIfNull();
        var emergencycontactTel = context.Request["emergencycontactTel"].EmptyIfNull();
        var emergencycontactRelation = context.Request["emergencycontactRelation"].EmptyIfNull();

        var visitType = context.Request["visitType"].EmptyIfNull();

        var visitDate1 = context.Request["visitDate1"].EmptyIfNull();
        var visitDate2 = context.Request["visitDate2"].EmptyIfNull();
        var visitCountry = context.Request["visitCountry"].EmptyIfNull();
        var localAccommodation = context.Request["localAccommodation"].EmptyIfNull();
        var localTel = context.Request["localTel"].EmptyIfNull();
        var localAddress = context.Request["localAddress"].EmptyIfNull();
        var departureDate = context.Request["departureDate"].EmptyIfNull();
        var returnDate = context.Request["returnDate"].EmptyIfNull();

        var acceptTerms = context.Request["acceptTerms"].EmptyIfNull();
        var applyAgree = context.Request["applyAgree"].EmptyIfNull();
        //json
        var childList = context.Request["childList"].EmptyIfNull();
        var companionList = context.Request["companionList"].EmptyIfNull();
        var attachFile = context.Request["attachFile"].EmptyIfNull();
        #endregion 

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            #region Compass - tSponsorGroup : GroupType 가져오기 
            Object[] objSql = new object[1] { "VT_WebInfoInquiry" };
            Object[] objParam = new object[] { "DIVIS", "ConID" };
            Object[] objValue = new object[] { "GroupInfo", userInfo.SponsorID };

            var grouptype = "";
            var list = _www6Service.NTx_ExecuteQuery("SqlCompass5", objSql, "SP", objParam, objValue).Tables[0];
            if (list.Rows.Count > 0)
            {
                grouptype = list.Rows[0]["GroupType"].ToString();
            }
            #endregion
            #region 후원어린이 수 
            int iCommitmentCount = 0;
            iCommitmentCount = getCommitmentCount(userInfo.SponsorID);
            #endregion

            string strBirthDate =  userInfo.Birth != "" ? userInfo.Birth.Substring(0, 10).Replace("-","") : "";
            var vtApplyList = new List<tVisionTripApply>();
            vtApplyList.Add(new tVisionTripApply()
            {
                ApplyType = applyType,
                SponsorID = userInfo.SponsorID,
                UserID = userInfo.UserId,
                SponsorName = userInfo.UserName,
                SponsorNameEng = sponsorNameEng,
                GenderCode = userInfo.GenderCode,
                BirthDate = strBirthDate.Encrypt(),
                Tel = tel.Encrypt(),
                Email = email.Encrypt(),
                LocationType = location,
                Address1 = (address1 + "$" + country).Encrypt(),
                Address2 = address2.Encrypt(),
                ZipCode = zipcode.Encrypt(),
                ChangeYN = changeYn != "Y" ? 'N' : 'Y',
                ReligionType = religion,
                ChurchName = church,
                EmergencyContactName = emergencycontactName,
                EmergencyContactTel = emergencycontactTel,
                EmergencyContactRelation = emergencycontactRelation,
                ApplyDate = DateTime.Now,
                CompassYN = 'N',
                GroupType = grouptype,
                CommitmentCount = iCommitmentCount, //후원어린이 수
                CurrentUse = 'Y',
                RegisterID = userInfo.UserId,
                RegisterName = userInfo.UserName,
                RegisterDate = DateTime.Now
            });

            //dao.tVisionTripApply.InsertAllOnSubmit(vtApplyList);
            foreach(var vta in vtApplyList)
            {
                www6.insert(vta);
            }
            //dao.SubmitChanges();

            int iApplyID = 0;
            iApplyID = vtApplyList[0].ApplyID;

            try
            {
                #region 상세 
                var vtIndividualDetail = new List<tVisionTripIndividualDetail>();
                vtIndividualDetail.Add(new tVisionTripIndividualDetail()
                {
                    ApplyID = iApplyID,
                    VisitType = Convert.ToChar(visitType),
                    VisitDate1 = visitDate1,
                    VisitDate2 = visitDate2,
                    VisitCountry = visitCountry,
                    LocalAccommodation = localAccommodation,
                    LocalTel = localTel,
                    LocalAddress = localAddress,
                    DepartureDate = departureDate,
                    ReturnDate = returnDate,

                    AcceptTerms = acceptTerms
                });
                //dao.tVisionTripIndividualDetail.InsertAllOnSubmit(vtIndividualDetail);
                foreach(var vta in vtIndividualDetail)
                {
                    www6.insert(vta);
                }
                //dao.SubmitChanges();

                int iIndividualDetailID = 0;
                iIndividualDetailID = vtIndividualDetail[0].IndividualDetailID;

                #endregion
                if (iApplyID > 0 && iIndividualDetailID > 0)
                {
                    #region 동반인정보
                    if (companionList != "")
                    {
                        var vtCompanion = new List<tVisionTripCompanion>();
                        JArray jResult = JArray.Parse(companionList);
                        for (int i = 0; i < jResult.Count; i++)
                        {
                            string cNameKor = jResult[i]["name_kor"].ToString();
                            string cNameEng = jResult[i]["name_eng"].ToString();
                            string cBirth = jResult[i]["birth"].ToString();
                            string cGender = jResult[i]["gender"].ToString();
                            if (cNameKor != "" && cNameEng != "" && cBirth != "" && cGender != "")
                            {
                                vtCompanion.Add(new tVisionTripCompanion()
                                {
                                    IndividualDetailID = iIndividualDetailID,
                                    CompanionName = cNameKor,
                                    CompanionNameEng = cNameEng,
                                    BirthDate = cBirth,
                                    GenderCode = Convert.ToChar(cGender),
                                    CurrentUse = 'Y',
                                    RegisterName = userInfo.UserName,
                                    RegisterID = userInfo.UserId,
                                    RegisterDate = DateTime.Now
                                });
                            }
                        }
                        //dao.tVisionTripCompanion.InsertAllOnSubmit(vtCompanion);
                        foreach(var vta in vtCompanion)
                        {
                            www6.insert(vta);
                        }
                    }
                    #endregion

                    #region 후원어린이정보
                    if (childList != "")
                    {
                        var vtChildMeetList = new List<tVisionTripChildMeet>();
                        JArray jResult = JArray.Parse(childList);
                        for (int i = 0; i < jResult.Count; i++)
                        {
                            string childKey = jResult[i]["id"].ToString();
                            string childName = jResult[i]["name"].ToString();

                            vtChildMeetList.Add(new tVisionTripChildMeet()
                            {
                                ApplyID = iApplyID,
                                ChildKey = childKey,
                                ChildName = childName,
                                ChildMeetType = childName == "" ? 'I' : 'S', //(I 직접입력, S 선택)
                                CurrentUse = 'Y',
                                RegisterName = userInfo.UserName,
                                RegisterID = userInfo.UserId,
                                RegisterDate = DateTime.Now
                            });
                        }
                        //dao.tVisionTripChildMeet.InsertAllOnSubmit(vtChildMeetList);
                        foreach(var vta in vtChildMeetList)
                        {
                            www6.insert(vta);
                        }
                    }
                    #endregion

                    #region 첨부파일
                    if (attachFile != "")
                    {
                        var vtAttachList = new List<tVisionTripAttach>();

                        JArray jResult = JArray.Parse(attachFile);
                        for (int i = 0; i < jResult.Count; i++)
                        {
                            string attachType = jResult[i]["type"].ToString();
                            string attachName = jResult[i]["name"].ToString();
                            string attachPath = jResult[i]["path"].ToString();

                            vtAttachList.Add(new tVisionTripAttach()
                            {
                                ApplyID = iApplyID,
                                AttachType = attachType,
                                AttachName = attachName,
                                AttachPath = attachPath,
                                CurrentUse = 'Y',
                                RegisterDate = DateTime.Now,
                                RegisterName = userInfo.UserName,
                                RegisterID = userInfo.UserId
                            });
                        }
                        //dao.tVisionTripAttach.InsertAllOnSubmit(vtAttachList);
                        foreach(var vta in vtAttachList)
                        {
                            www6.insert(vta);
                        }
                    }
                    #endregion

                }
                else
                {
                    using (FrontDataContext daoDel = new FrontDataContext())
                    {
                        //   iApplyID
                        //var entity = daoDel.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                        var entity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(iApplyID));
                        //daoDel.tVisionTripApply.DeleteOnSubmit(entity);
                        //daoDel.SubmitChanges();
                        www6.delete(entity);

                        //var entity_individual = daoDel.tVisionTripIndividualDetail.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                        var entity_individual = www6.selectQF<tVisionTripIndividualDetail>("ApplyID", Convert.ToInt32(iApplyID));
                        //daoDel.tVisionTripIndividualDetail.DeleteOnSubmit(entity_individual);
                        //daoDel.SubmitChanges();
                        www6.delete(entity_individual);
                    }

                    result.success = false;
                    result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
                }
                //dao.SubmitChanges();


                #region 사용자 정보 변경
                if (changeYn.Equals("Y"))
                {
                    try
                    {
                        //WEB
                        using (AuthLibDataContext daoAuth = new AuthLibDataContext())
                        {
                            //var entity = daoAuth.tSponsorMaster.First(p => p.UserID == userInfo.UserId);
                            var entity = www6.selectQFAuth<tSponsorMaster>("UserID", userInfo.UserId);

                            if (!string.IsNullOrEmpty(email))
                                entity.Email = email.Encrypt();
                            if (!string.IsNullOrEmpty(tel))
                                entity.Phone = tel.Encrypt();

                            //if (!string.IsNullOrEmpty(zipcode))
                            //    entity.ZipCode = zipcode.Encrypt();
                            //if (!string.IsNullOrEmpty(address1))
                            //    entity.Addr1 = (address1 + "$" + country).Encrypt();
                            ////entity.Addr1 = addr1.Encrypt();
                            //if (!string.IsNullOrEmpty(address2))
                            //    entity.Addr2 = address2.Encrypt();

                            //daoAuth.SubmitChanges();
                            www6.updateAuth(entity);
                        }

                        // 후원회원인경우 
                        if (!string.IsNullOrEmpty(userInfo.SponsorID))
                        {
                            var userInfoResult = UpdateCompassUserInfo(userInfo.SponsorID, location, country, address1, address2, zipcode, tel, email);
                            if (!userInfoResult.success)
                            {
                                result.success = false;
                                result.message = userInfoResult.message;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                        result.success = false;
                        result.message = "회원님의 정보를 수정하는 중 오류가 발생했습니다.(WEB)";
                        //return;
                    }
                }
                #endregion

                result.data = iApplyID;
                //sms발송
                //var applyEntity = dao.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                var applyEntity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(iApplyID));
                SendSMS(applyEntity, context);
            }
            catch (Exception ex)
            {
                using (FrontDataContext daoDel = new FrontDataContext())
                {
                    //   iApplyID
                    //var entity = daoDel.tVisionTripApply.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                    var entity = www6.selectQF<tVisionTripApply>("ApplyID", Convert.ToInt32(iApplyID));
                    //daoDel.tVisionTripApply.DeleteOnSubmit(entity);
                    //daoDel.SubmitChanges();
                    www6.delete(entity);
                    //   iApplyID

                    //var entity_individual = daoDel.tVisionTripIndividualDetail.First(p => p.ApplyID == Convert.ToInt32(iApplyID));
                    var entity_individual = www6.selectQF<tVisionTripIndividualDetail>("ApplyID", Convert.ToInt32(iApplyID));
                    //daoDel.tVisionTripIndividualDetail.DeleteOnSubmit(entity_individual);
                    //daoDel.SubmitChanges();
                    www6.delete(entity_individual);
                }
                ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                result.success = false;
                result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
            }
        }
        JsonWriter.Write(result, context);
    }

    JsonWriter UpdateCompassUserInfo(string sponsorId, string location, string country, string addr1, string addr2, string zipcode, string mobile, string email)
    {
        UserInfo sess = new UserInfo();

        JsonWriter result = new JsonWriter() { success = true, action = "" };
        var actionResult = new JsonWriter();

        if (!string.IsNullOrEmpty(location) && !string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(zipcode) && !string.IsNullOrEmpty(addr1) && !string.IsNullOrEmpty(addr2))
        {
            #region 주소수정
            actionResult = new SponsorAction().GetAddress();
            if (!actionResult.success)
            {
                result.message = actionResult.message;
                return result;
            }
            SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)actionResult.data;

            actionResult = new SponsorAction().UpdateAddress(true, sess.UserId, sponsorId, sess.UserName, addr_data.AddressType, location, country, zipcode, addr1, addr2);
            if (!actionResult.success)
            {
                result.message = actionResult.message;
                return result;
            }
            #endregion
        }

        #region 이메일 & 휴대전화 수정
        var comm_result = new SponsorAction().UpdateCommunications(true, sponsorId, email, mobile, null);
        if (!comm_result.success)
        {
            result.message = comm_result.message;
            return result;
        }
        #endregion

        return result;
    }
    //후원 어린이 수 
    protected int getCommitmentCount(string sponserid)
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        int iCommitmentCount = 0;
        try
        {
            var objSql = new object[1] { "  SELECT COUNT(*) AS CommitmentCount " +
         "  FROM tCommitmentMaster ComM WITH (NOLOCK) " +
         "  LEFT OUTER JOIN tSponsorMaster SM WITH (NOLOCK) ON ComM.SponsorID = SM.SponsorID " +
         "  LEFT OUTER JOIN tChildMaster CM WITH (NOLOCK) ON ComM.ChildMasterID = CM.ChildMasterID " +
         "  WHERE ComM.SponsorID = '" + sponserid + "' " +
         "  AND ComM.SponsorItemEng IN ('DS', 'LS') " +
         "  AND (ComM.StopDate IS NULL OR ComM.StopDate > GETDATE()) " };

            DataSet ds2 = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

            if (ds2 == null || ds2.Tables.Count == 0 || ds2.Tables[0].Rows.Count == 0)
                iCommitmentCount = 0;
            else
                iCommitmentCount = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());
        }
        catch (Exception ex)
        {
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            iCommitmentCount = 0;
        }

        return iCommitmentCount;
    }

    void GetMyVisionTripList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        UserInfo sess = new UserInfo();
        var userId = sess.UserId;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripMyTrip_list_f(page, rowsPerPage, userId).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "userid" };
            Object[] op2 = new Object[] { page, rowsPerPage, userId };
            var list = www6.selectSP("sp_tVisionTripMyTrip_list_f", op1, op2).DataTableToList<sp_tVisionTripMyTrip_list_fResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void GetMyIndividualTripList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        UserInfo sess = new UserInfo();
        var userId = sess.UserId;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripMyIndividual_list_f(page, rowsPerPage, userId).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "userid" };
            Object[] op2 = new Object[] { page, rowsPerPage, userId };
            var list = www6.selectSP("sp_tVisionTripMyIndividual_list_f", op1, op2).DataTableToList<sp_tVisionTripMyIndividual_list_fResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void GetMyTripHistoryList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        UserInfo sess = new UserInfo();
        var userId = sess.UserId;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("5"));
        var tripType = context.Request["tripType"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripMyHistory_list_f(page, rowsPerPage, tripType, userId).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "tripType", "userid" };
            Object[] op2 = new Object[] { page, rowsPerPage, tripType, userId };
            var list = www6.selectSP("sp_tVisionTripMyHistory_list_f", op1, op2).DataTableToList<sp_tVisionTripMyHistory_list_fResult>();
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void GetAttach(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var applyID = Convert.ToInt32(context.Request["apply_id"].ValueIfNull("0"));
        var attachType = context.Request["attach_type"].EmptyIfNull();
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.tVisionTripAttach.Where(p => p.ApplyID == applyID && p.CurrentUse == 'Y' && p.AttachType == attachType).ToList();
            var list = www6.selectQ<tVisionTripAttach>("ApplyID", applyID, "CurrentUse", "Y", "AttachType",  attachType);
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void Update_Attach(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin)
        {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        #region param  
        var applyID = Convert.ToInt32(context.Request["applyId"].ValueIfNull("0"));
        var attachType = context.Request["attachType"].EmptyIfNull();
        var attachFile = context.Request["attachFile"].EmptyIfNull();
        #endregion


        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            try
            {
                //if (!dao.tVisionTripAttach.Any(p => p.ApplyID == applyID && p.CurrentUse == 'Y' && p.AttachType == attachType))
                //{
                //    result.success = false;
                //    result.message = "올바른 접근이 아닙니다.";
                //    JsonWriter.Write(result, context);
                //    return;
                //}

                if (attachType == "passport")
                {
                    //var list = dao.tVisionTripAttach.Where(p => p.ApplyID == applyID && p.CurrentUse == 'Y' && p.AttachType == attachType).ToList();
                    var list = www6.selectQ<tVisionTripAttach>("ApplyID", applyID, "CurrentUse", "Y", "AttachType", attachType);
                    foreach (tVisionTripAttach att in list)
                    {
                        att.CurrentUse = 'N';
                        att.modifyDate = DateTime.Now;
                        att.ModifyID = userInfo.UserId;
                        att.ModifyName = userInfo.UserName;

                        www6.update(att);
                    }
                }

                var vtAttachList = new List<tVisionTripAttach>();
                JArray jResult = JArray.Parse(attachFile);
                for (int i = 0; i < jResult.Count; i++)
                {
                    string attachName = jResult[i]["name"].ToString();
                    string attachPath = jResult[i]["path"].ToString();

                    vtAttachList.Add(new tVisionTripAttach()
                    {
                        ApplyID = applyID,
                        AttachType = attachType,
                        AttachName = attachName,
                        AttachPath = attachPath,
                        CurrentUse = 'Y',
                        RegisterDate = DateTime.Now,
                        RegisterName = userInfo.UserName,
                        RegisterID = userInfo.UserId
                    });
                }
                //dao.tVisionTripAttach.InsertAllOnSubmit(vtAttachList);
                foreach (var vta in vtAttachList)
                {
                    www6.insert(vta);
                }
                //dao.SubmitChanges();

            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                result.success = false;
                result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
            }
        }
        JsonWriter.Write(result, context);
    }

    void Delete_Attach(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin)
        {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        #region param  
        var applyID = Convert.ToInt32(context.Request["applyId"].ValueIfNull("0"));
        var attachID = Convert.ToInt32(context.Request["attachID"].ValueIfNull("0"));
        #endregion

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            try
            {
                var exist = www6.selectQ<tVisionTripAttach>("ApplyID", applyID, "CurrentUse", "Y", "AttachID",attachID);
                //if (!dao.tVisionTripAttach.Any(p => p.ApplyID == applyID && p.CurrentUse == 'Y' && p.AttachID == attachID))
                if(!exist.Any())
                {
                    result.success = false;
                    result.message = "올바른 접근이 아닙니다.";
                    JsonWriter.Write(result, context);
                    return;
                }

                //var list = dao.tVisionTripAttach.Where(p => p.ApplyID == applyID && p.CurrentUse == 'Y' && p.AttachID == attachID).ToList();
                var list = www6.selectQ<tVisionTripAttach>("ApplyID", applyID, "CurrentUse", "Y", "AttachID",attachID);
                foreach (tVisionTripAttach att in list)
                {
                    att.CurrentUse = 'N';
                    att.modifyDate = DateTime.Now;
                    att.ModifyID = userInfo.UserId;
                    att.ModifyName = userInfo.UserName;

                    www6.update(att);
                }

                //dao.SubmitChanges();

            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, ex.Message);
                result.success = false;
                result.message = "예외오류가 발생했습니다. 담당자에게 문의 바랍니다.";
            }
        }
        JsonWriter.Write(result, context);
    }


    void GetChildMeetList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var applyID = Convert.ToInt32(context.Request["apply_id"].ValueIfNull("0"));
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.tVisionTripChildMeet.Where(p => p.ApplyID == applyID).ToList();
            var list = www6.selectQ<tVisionTripChildMeet>("ApplyID", applyID);
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }



    void SetVirtualAccount(HttpContext context)
    {
        //CommonLib.PAY4Service.ServiceSoapClient _pay4Service = new CommonLib.PAY4Service.ServiceSoapClient(); 

        var bankCode = context.Request["bankCode"].EmptyIfNull();
        var bankName = context.Request["bankName"].EmptyIfNull();
        var planDetailID = Convert.ToInt32(context.Request["planDetailID"].ValueIfNull("0"));
        var amountType = context.Request["amountType"].EmptyIfNull();
        var amount = context.Request["amount"].EmptyIfNull();



        if (string.IsNullOrEmpty(bankCode) || string.IsNullOrEmpty(bankName))
        {
            new JsonWriter() { success = false, message = "은행정보가 필요합니다." }.Write(context);
            return;
        }

        //var action = new VisionTripPaymentAction();
        var result = new VisionTripPaymentAction().SetVirtualAccount(bankCode, bankName, planDetailID, amountType, amount);
        if (result.success)
        {
            var list = new VisionTripPaymentAction().GetVirtualAccounts(planDetailID, amountType);
            result.data = list.data;
            result.Write(context);
        }
        else
        {
            result.Write(context);
        }

    }
    void GetVirtualAccount(HttpContext context)
    {
        //CommonLib.PAY4Service.ServiceSoapClient _pay4Service = new CommonLib.PAY4Service.ServiceSoapClient(); 

        JsonWriter result = new JsonWriter();
        result.success = true;

        var bankCode = context.Request["bankCode"].EmptyIfNull();
        var bankName = context.Request["bankName"].EmptyIfNull();
        var planDetailID = Convert.ToInt32(context.Request["planDetailID"].ValueIfNull("0"));
        var amountType = context.Request["amountType"].EmptyIfNull();

        var list = new VisionTripPaymentAction().GetVirtualAccounts(planDetailID, amountType);
        result.data = list.data;

        JsonWriter.Write(result, context);
    }



}
