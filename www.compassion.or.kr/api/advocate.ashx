﻿<%@ WebHandler Language="C#" Class="api_advocate" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_advocate : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context)
    {

        var t = context.Request["t"].EmptyIfNull();
        if(t == "band_apply") {
            BandApply(context);
        }else if (t == "friends_shop_list") {
            GetFriendsShopList(context);
        }else if(t == "friends_shop_apply") {
            FriendsShopApply(context);
        }else if(t == "diy_list") {
            GetDiyList(context);
        }else if(t == "hits") {
            Hits(context);
        }else if(t == "add_story") {
            AddStory(context);
        }else if(t == "campaign_list") {
            CampaignList(context);
        }else if(t == "campaign_apply") {
            CampaignApply(context);
        }else if(t == "share_list") {
            GetShareList(context);
        }else if(t == "widget_list") {
            GetWidgetList(context);
        }else if(t == "blueboard_apply") {
            BlueBoardApply(context);
        }else if(t == "download") {
            Download(context);
        } else if(t == "check_user") {
            BlueBoardCheckUser(context);
        }
    }


    /// <summary>
    ///  스토리 등록
    /// </summary>
    /// <param name="context"></param>
    void AddStory(HttpContext context) {
        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }



        var userInfo = new UserInfo();
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<advocate_user>("au_user_id", userInfo.UserId);
            //if (!dao.advocate_user.Any(p => p.au_user_id == userInfo.UserId))
            if(!exist.Any())
            {
                var isAdvocate = new VOCAction().CheckUser();
                if (!isAdvocate.success)
                {
                    result.success = false;
                    result.message = "애드보킷 회원만 이용가능합니다. 온라인 애드보킷 지원페이지를 이용하세요.";
                    JsonWriter.Write(result, context);
                    return;
                }
            }

            var ds_name = context.Request["ds_name"].EmptyIfNull();
            var ds_phone = context.Request["ds_phone"].EmptyIfNull();
            var ds_content = context.Request["ds_content"].EmptyIfNull().EscapeSqlInjection();

            if (ds_name == "" || ds_phone == "" || ds_content == "")
            {
                result.success = false;
                result.message = "필수정보가 부족합니다.";
                JsonWriter.Write(result, context);
                return;
            }

            /*
            if(dao.diy_story.Any(p => p.ds_user_id == userInfo.UserId)) {

                result.success = false;
                result.message = "이미 등록되었습니다. 감사합니다.";
                JsonWriter.Write(result, context);
                return;
            }
            */


            var entity = new diy_story()
            {
                ds_name = ds_name,
                ds_phone = ds_phone,
                ds_content = ds_content,
                ds_user_id = userInfo.UserId,
                ds_user_name = userInfo.UserName,
                ds_regdate = DateTime.Now
            };

            //dao.diy_story.InsertOnSubmit(entity);
            www6.insert(entity);
            //dao.SubmitChanges();
            JsonWriter.Write(result, context);
            return;
        }
    }



    void BandApply(HttpContext context) {
        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var up_file2 = context.Request["up_file2"].EmptyIfNull();
        var ucc= context.Request["ucc"].EmptyIfNull();
        var tel_call_time= context.Request["tel_call_time"].EmptyIfNull();

        if(up_file2 == "" || ucc == "" ||tel_call_time == "" ) {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }


        var userInfo = new UserInfo();
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<BAND>("user_id", userInfo.UserId);
            //if (dao.BAND.Any(p => p.user_id == userInfo.UserId))
            if(exist.Any())
            {
                result.success = false;
                result.message = "이미 등록되었습니다. 감사합니다.";
                JsonWriter.Write(result, context);
                return;
            }

            var entity = new BAND()
            {
                up_file2 = up_file2,
                special = "",
                gyegi = "",
                hakryuk = "",
                name = userInfo.UserName,
                brithday = "",
                sponsor = userInfo.ConId,
                hand = "",
                email = userInfo.Email,
                church = "",
                zip = "",
                address = "",
                content = "",
                ucc = ucc,
                tel_call_time = tel_call_time,
                writeday = DateTime.Now,
                user_id = userInfo.UserId
            };

            //dao.BAND.InsertOnSubmit(entity);
            www6.insert(entity);
            //dao.SubmitChanges();

            JsonWriter.Write(result, context);
            return;
        }
    }




    void GetFriendsShopList(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("12"));
        var k_word = context.Request["k_word"].EmptyIfNull();
        var addr = context.Request["addr"].EmptyIfNull();
        var type = context.Request["type"].EmptyIfNull();

        using (StoreDataContext dao = new StoreDataContext())
        {
            //var list = dao.sp_friends_shop_list_f(page, rowsPerPage, k_word, addr, type).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword", "addr", "type" };
            Object[] op2 = new Object[] { page, rowsPerPage, k_word, addr, type };
            var list = www6.selectSPStore("sp_friends_shop_list_f", op1, op2).DataTableToList<sp_friends_shop_list_fResult>().ToList();

            foreach (var item in list)
            {
                item.shop_img = item.shop_img.WithFileServerHost();
            }
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }


    void FriendsShopApply(HttpContext context) {
        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var writer = context.Request["writer"].EmptyIfNull();
        var writer_tel= context.Request["writer_tel"].EmptyIfNull();
        var writer_email= context.Request["writer_email"].EmptyIfNull();
        var shop_name= context.Request["shop_name"].EmptyIfNull();
        var shop_type= context.Request["shop_type"].EmptyIfNull();
        var shop_tel= context.Request["shop_tel"].EmptyIfNull();
        var shop_zip= context.Request["shop_zip"].EmptyIfNull();
        var shop_addr= context.Request["shop_addr"].EmptyIfNull();
        var shop_homepage= context.Request["shop_homepage"].EmptyIfNull();
        var part= context.Request["part"].EmptyIfNull();
        var shop_intro= context.Request["shop_intro"].EmptyIfNull();
        var shop_img= context.Request["shop_img"].EmptyIfNull();
        var online = context.Request["online"].EmptyIfNull();

        if(
            writer == "" ||
            writer_tel == "" ||
            writer_email == "" ||
            shop_name == ""||
            shop_type == ""||
            shop_tel == ""||
            shop_zip == ""||
            shop_addr == ""||
            part == ""||
            shop_intro == ""||
            shop_img == ""
                ) {

            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }


        var userInfo = new UserInfo();
        using (StoreDataContext dao = new StoreDataContext())
        {
            var entity = new friends_shop()
            {
                writer = writer,
                writer_tel = writer_tel,
                writer_email = writer_email,
                shop_name = shop_name,
                shop_type = shop_type,
                shop_tel = shop_tel,
                shop_zip = shop_zip,
                shop_addr = shop_addr,
                shop_homepage = shop_homepage,
                part = part,
                shop_intro = shop_intro,
                shop_img = shop_img,
                reg_date = DateTime.Now,
                user_id = userInfo.UserId,
                user_name = userInfo.UserName,
                shop_main_flag = false,
                del_flag = true,
                is_online = online == "1"
            };

            //dao.friends_shop.InsertOnSubmit(entity);
            www6.insertStore(entity);
            //dao.SubmitChanges();
            JsonWriter.Write(result, context);
            return;
        }
    }



    void GetDiyList(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("12"));
        var k_word = context.Request["k_word"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_diy_board_list_f(page, rowsPerPage, k_word).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword" };
            Object[] op2 = new Object[] { page, rowsPerPage, k_word };
            var list = www6.selectSP("sp_diy_board_list_f", op1, op2).DataTableToList<sp_diy_board_list_fResult>().ToList();

            foreach (var item in list)
            {
                item.db_thumb = item.db_thumb.WithFileServerHost();
            }
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }


    void Hits(HttpContext context)
    {
        var type = context.Request["type"].EmptyIfNull();
        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        if(id != -1) {

            using (FrontDataContext dao = new FrontDataContext())
            {
                if (type == "diy")
                {
                    //var entity = dao.diy_board.First(p => p.db_id == id);
                    var entity = www6.selectQF<diy_board>("db_id", id);
                    entity.db_hits++;

                    //dao.SubmitChanges();
                    //string wClause = string.Format("db_id = {0}", id);
                    www6.update(entity);
                }
                else if (type == "campaign")
                {
                    //var entity = dao.seasonal_campaign.First(p => p.sc_id == id);
                    var entity = www6.selectQF<seasonal_campaign>("sc_id", id);
                    entity.sc_hits++;

                    //dao.SubmitChanges();
                    //string wClause = string.Format("sc_id = {0}", id);
                    www6.update(entity);
                }
                else if (type == "share")
                {
                    //var entity = dao.user_board.First(p => p.ub_id == id);
                    var entity = www6.selectQF<user_board>("ub_id", id);
                    entity.ub_hits++;

                    //dao.SubmitChanges();                    
                    //string wClause = string.Format("ub_id = {0}", id);
                    www6.update(entity);
                }
            }
        }
    }



    void CampaignList(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("12"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_seasonal_campaign_list_f(page, rowsPerPage, "").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword"};
            Object[] op2 = new Object[] { page, rowsPerPage, "" };
            var list = www6.selectSP("sp_seasonal_campaign_list_f", op1, op2).DataTableToList<sp_seasonal_campaign_list_fResult>().ToList();

            foreach (var item in list)
            {
                item.sc_thumb = item.sc_thumb.WithFileServerHost();
            }
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void CampaignApply(HttpContext context) {
        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var userInfo = new UserInfo();

        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<advocate_user>("au_user_id", userInfo.UserId);
            //if (!dao.advocate_user.Any(p => p.au_user_id == userInfo.UserId))
            if(!exist.Any())
            {
                var isAdvocate = new VOCAction().CheckUser();
                if (!isAdvocate.success)
                {
                    result.success = false;
                    result.message = "애드보킷 회원만 이용가능합니다. 온라인 애드보킷 지원페이지를 이용하세요.";
                    JsonWriter.Write(result, context);
                    return;
                }
            }


            var id = context.Request["id"].EmptyIfNull();
            var name = context.Request["name"].EmptyIfNull();
            var phone = context.Request["phone"].EmptyIfNull();
            var zipcode = context.Request["zipcode"].EmptyIfNull();
            var addr1 = context.Request["addr1"].EmptyIfNull();
            var addr2 = context.Request["addr2"].EmptyIfNull();
            var plan = context.Request["plan"].EmptyIfNull();
            var hfAddressType = context.Request["hfAddressType"].EmptyIfNull();


            if (
                id == "" ||
                name == "" ||
                phone == "" ||
                zipcode == "" ||
                addr1 == "" ||
                addr2 == "" ||
                plan == ""
                    )
            {

                result.success = false;
                result.message = "필수정보가 부족합니다.";
                JsonWriter.Write(result, context);
                return;
            }


            // 최초 한번 컴파스에 주소를 업데이트 한다.
            if (userInfo.LocationType == "국내")
            {
                new SponsorAction().UpdateAddress(false, hfAddressType, userInfo.LocationType, "한국", zipcode, addr1, addr2);
            }


            //var checkApply = dao.seasonal_campaign_apply.FirstOrDefault(p => p.sca_user_id == userInfo.UserId && p.sca_sc_id == Convert.ToInt32(id));
            var checkApply = www6.selectQF<seasonal_campaign_apply>("sca_user_id",userInfo.UserId, "sca_sc_id", Convert.ToInt32(id));
            if (checkApply != null)
            {
                result.success = false;
                result.message = "패킷은 캠페인당 1회만 신청 가능합니다.";
                JsonWriter.Write(result, context);
                return;
            }


            var entity = new seasonal_campaign_apply()
            {
                sca_sc_id = Convert.ToInt32(id),
                sca_name = name,
                sca_phone = phone,
                sca_zipcode = zipcode,
                sca_addr1 = addr1,
                sca_addr2 = addr2,
                sca_plan = plan,
                sca_regdate = DateTime.Now,
                sca_user_id = userInfo.UserId,
                sca_user_name = userInfo.UserName
            };

            //dao.seasonal_campaign_apply.InsertOnSubmit(entity);
            www6.insert(entity);
            //dao.SubmitChanges();
            JsonWriter.Write(result, context);
            return;
        }
    }


    void GetShareList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("12"));
        var k_word = context.Request["k_word"].EmptyIfNull();
        var type = context.Request["type"].EmptyIfNull();
        var category = context.Request["category"].EmptyIfNull();
        var top = Convert.ToInt32(context.Request["top"].ValueIfNull("-1"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_user_board_list_f(page, rowsPerPage, type, category, top, k_word).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "category", "top", "keyword" };
            Object[] op2 = new Object[] { page, rowsPerPage, type, category, top, k_word };
            var list = www6.selectSP("sp_user_board_list_f", op1, op2).DataTableToList<sp_user_board_list_fResult>().ToList();

            foreach (var item in list)
            {
                item.ub_thumb = item.ub_thumb.WithFileServerHost();
                item.ub_user_id = item.ub_user_id.Mask("*", 3);
            }
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void GetWidgetList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("999"));

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_widget_list_f(page, rowsPerPage).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage" };
            Object[] op2 = new Object[] { page, rowsPerPage };
            var list = www6.selectSP("sp_widget_list_f", op1, op2).DataTableToList<sp_widget_list_fResult>().ToList();

            foreach (var item in list)
            {
                item.w_image = item.w_image.WithFileServerHost();
            }
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }


    void BlueBoardApply(HttpContext context) {
        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var userInfo = new UserInfo();

        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<advocate_user>("au_user_id", userInfo.UserId);
            //if (!dao.advocate_user.Any(p => p.au_user_id == userInfo.UserId))
            if(!exist.Any())
            {
                var isAdvocate = new VOCAction().CheckUser();
                if (!isAdvocate.success)
                {
                    result.success = false;
                    result.message = "애드보킷 회원만 이용가능합니다. 온라인 애드보킷 지원페이지를 이용하세요.";
                    JsonWriter.Write(result, context);
                    return;
                }
            }


            var name = context.Request["name"].EmptyIfNull();
            var phone = context.Request["phone"].EmptyIfNull();
            var zipcode = context.Request["zipcode"].EmptyIfNull();
            var addr1 = context.Request["addr1"].EmptyIfNull();
            var addr2 = context.Request["addr2"].EmptyIfNull();
            var plan = context.Request["plan"].EmptyIfNull();
            var hfAddressType = context.Request["hfAddressType"].EmptyIfNull();


            if (
                name == "" ||
                phone == "" ||
                zipcode == "" ||
                addr1 == "" ||
                addr2 == "" ||
                plan == ""
                    )
            {

                result.success = false;
                result.message = "필수정보가 부족합니다.";
                JsonWriter.Write(result, context);
                return;
            }



            // 최초 한번 컴파스에 주소를 업데이트 한다.
            if (userInfo.LocationType == "국내")
            {
                new SponsorAction().UpdateAddress(false, hfAddressType, userInfo.LocationType, "한국", zipcode, addr1, addr2);
            }

            //하루한번

            //var checkDay = dao.blue_board_apply.Where(p => p.bba_user_id == userInfo.UserId).OrderByDescending(p => p.bba_regdate).ToList();
            var checkDay = www6.selectQ<blue_board_apply>("bba_user_id", userInfo.UserId, "bba_regdate desc");

            if (checkDay.Count > 0)
            {
                if (DateTime.Now.Subtract(checkDay[0].bba_regdate).TotalDays < 1)
                {

                    result.success = false;
                    result.message = "패킷은 하루 1회만 신청 가능합니다.";
                    JsonWriter.Write(result, context);
                    return;
                }
            }


            var entity = new blue_board_apply()
            {
                bba_name = name,
                bba_phone = phone,
                bba_zipcode = zipcode,
                bba_addr1 = addr1,
                bba_addr2 = addr2,
                bba_plan = plan,
                bba_regdate = DateTime.Now,
                bba_user_id = userInfo.UserId,
                bba_user_name = userInfo.UserName
            };

            //dao.blue_board_apply.InsertOnSubmit(entity);
            www6.insert(entity);
            //dao.SubmitChanges();
            JsonWriter.Write(result, context);
            return;
        }
    }

    void BlueBoardCheckUser( HttpContext context )
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        if(!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var userInfo = new UserInfo();
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<advocate_user>("au_user_id", userInfo.UserId);
            //if (!dao.advocate_user.Any(p => p.au_user_id == userInfo.UserId))
            if(!exist.Any())
            {
                var isAdvocate = new VOCAction().CheckUser();
                if (!isAdvocate.success)
                {
                    result.success = false;
                    result.message = "애드보킷 회원만 이용가능합니다. 온라인 애드보킷 지원페이지를 이용하세요.";
                    JsonWriter.Write(result, context);
                    return;
                }
            }
        }

        JsonWriter.Write(result, context);
        return;

    }

    void Download(HttpContext context) {
        var url = context.Request["url"].EmptyIfNull();

        try {
            new Download().OverUrl(url);

        } catch(Exception ex) {
            ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
        }
    }
}