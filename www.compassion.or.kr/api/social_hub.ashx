﻿<%@ WebHandler Language="C#" Class="customer_social_hub_default_ashx" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;

public class customer_social_hub_default_ashx : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var m = Convert.ToInt32(context.Request["m"].ValueIfNull("10"));
        TimeSpan refresh = new TimeSpan(0, m, 0);

        //base.SetCache(context , refresh);

        var data = new List<SocialData.Post>();

        try {
            data.AddRange((new Youtube(context)).GetPosts(45, "").items);
        } catch {
        }
        try {

            data.AddRange((new Facebook(context)).GetPosts(45, "").items);
        } catch {
        }
        try {

            data.AddRange((new Instagram(context)).GetPosts(45, "").items);
        } catch {
        }

        /*
        try {

            data.AddRange((new Kakaostory(context)).GetPosts(10, "").items);
        } catch {
        }
        */

        //data = data.OrderByDescending(p => p.regdate).ToList();
        //context.Response.Write(data.ToJson());

        JsonWriter.Write(data.ToList(), context , false);	

    }

}