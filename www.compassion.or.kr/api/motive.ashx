﻿<%@ WebHandler Language="C#" Class="api_motive" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_motive : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest(HttpContext context) {

		var t = context.Request["t"].EmptyIfNull();

		if(t == "list") {
			this.GetList(context);
		}
	}

	void GetList(HttpContext context) {
			
		WWWService.Service _wwwService = new WWWService.Service();

		DataSet ds = _wwwService.getSystemCategoryCode_L("N", "MotiveCode", "");
		
		Dictionary<string, object> result = new Dictionary<string, object>();

		foreach(DataRow row in ds.Tables[0].Rows) {
			var codeId = row["codeid"].ToString();
			DataSet ds2 = _wwwService.getSystemCategoryCode_S("N", "MotiveCode", codeId, "");

			result.Add(codeId , ds2.Tables[0]);
		}
		JsonWriter.Write(result, context);



	}
}