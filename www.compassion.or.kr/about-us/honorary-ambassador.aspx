﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="honorary-ambassador.aspx.cs" Inherits="honorary_ambassador" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script>
		$(function () {
		
		})

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>홍보<em>대사</em></h1>
				<span class="desc">한국컴패션 홍보대사는 삶으로 컴패션을 말합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents aboutus">

			<div class="promote w980">
				<div class="conWrap conWrap1 clear2">
					<p class="conL">맨발로 흙길을 걷던 <em>두 어린이</em>가<br />
					저를 위해 <em>길가의 가시</em>를 치워 주었습니다.</p>
					<p class="conR">
						에티오피아의 광활한 벌판은 지평선 끝에서 파란 하늘과 맞닿아 있었습니다. 그곳에서 맨발로 흙길을 걸으며 저를 졸졸 따라다니는 두 소녀를 만났습니다. 어린이들은 뭐가 그리 좋은지 하루종일 저를 따라다녔습니다. 튼튼한 신발을 신고 걸었음에도 길가의 날카로운 가시가 제 신발을 뚫고 들어왔습니다. 제 발이 가시에 찔리자 아이들은 맨발로 가시를 치워주었습니다. 이 어린이들은 그날 저의 또 다른 가족이 되었습니다.<br />
						<br />
						컴패션을 통해 저와 제 가족은 그 전에는 꿈꿀 수도 없었던 전혀 새로운 세상을 선물 받았습니다. 수 많은 후원어린이들을 가족으로 맞아들였고 또 그 전에는 몰랐던 다른 후원자님들과의 만남도	가졌습니다. 처음에는 저희가 어린이를 돕는다고 생각했는데, 어린이를 통해 하나님의 사랑을 만났고 삶의 목적도 깨달았습니다. 무엇보다 가난으로 절망이 가득한 그곳에서 어린이들의 환한 웃음을 만날 수 있다는 것에 감사했습니다. 어린이들이 웃을 수 있도록 컴패션과 함께 전 세계 어린이들에게 꿈과 희망을 심어주시는 후원자님들에게 깊은 감사를 드립니다.<br />
						<br />
						<em>홍보대사</em> <strong>신애라</strong>
					</p>
				</div>
				<div class="conWrap conWrap2 clear2">
					<p class="conL">
						“<em>I LOVE MOMMY…</em>”,<br />
						이 말이 많은 것을 바꾸었습니다.
					</p>
					<p class="conR">
						2008년에 6명의 어린이들과 결연을 맺었습니다. 한 아이로부터 편지가 왔습니다. 편지 내용이 ‘I LOVE YOU Mommy, 정혜영’이었는데, 굉장한 감동이 있었습니다. 그 편지를 받은 아내는 필리핀 행을 결정했습니다.<br />
						<br />
						그때 우리 부부에게는 작은 꿈이 있었습니다. 내 집 마련이지요. 그런데 필리핀에 갔다 온 아내는 내 집 마련의 꿈을 잠시 뒤로하고, 100명의 아이들을 도와주자는 제의를 했습니다. 그렇게 해서 저희는 2008년부터 100명의 아이들을 후원하기 시작했고, 지금은 300명 컴패션어린이들의 부모가 되었습니다.<br />
						<br />
						한 어린이가 가난에서 벗어나고, 한 가족이 변화되며, 한 나라가 성장하는 기적을 꿈꿀 수 있었습니다. 그런데 이보다 더 큰 기적은 바로 이 작은 사랑을 행하는 우리가 하나님 아버지의 마음을 조금 더 알아가면서, 삶에 감사가 늘어나고 행복이 더해진다는 것입니다. 나누면 행복합니다. 그	행복, 여러분도 누리면서 살고 싶지 않으세요? 그 행복은 나눔을 실천할 때, 우리 모두의 삶으로 다가올 것입니다.<br />
						<br />
						<em>홍보대사</em> <strong>션, 정혜영 부부</strong>
					</p>
				</div>

			</div>

		</div>
		

		<!--// e: sub contents -->

		<div class="h40"></div>


    </section>
    <!--// sub body -->


</asp:Content>
