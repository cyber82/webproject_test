﻿$(function () {

	/* 수혜국이름(benefit>a), 지도상위치(map>area), 수혜국소개(nation_info)의 인덱스를 동일하게 */

	$(".nationWrap > .benefit > a").addClass("nEvt");
	$(".nationWrap > map > area").addClass("nEvt");

	var nation = $(".nationWrap > .benefit > a");

	$(".nEvt").hover(function () {
		areaNum = $(this).index();
		nation.removeClass("on");
		nation.eq(areaNum).addClass("on");
	}, function () {
		nation.removeClass("on");
	}).click(function () {
		if ($(".contentsWrap > .nation_info").eq(areaNum).hasClass("on") == true) {
			$(".contentsWrap").removeClass("on");
			$(".contentsWrap > .nation_info").removeClass("on");
			nation.removeClass("fix");
		} else {
			$(".contentsWrap").addClass("on");
			$(".contentsWrap > .nation_info").removeClass("on");
			$(".contentsWrap > .nation_info").eq(areaNum).addClass("on");
			nation.removeClass("fix");
			nation.eq(areaNum).addClass("fix");

			// position 이동
			var pos = $(".contentsWrap").offset().top;
			//$('body,html').animate({ scrollTop: pos }, 400);

			scrollTo($("#l"));
		}
		return false;
	}).focus(function () {
		$(this).css({ "outline": "none" })
	});

});


(function () {
	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $filter, popup , paramService) {

		
		$scope.row = [];
		//$scope.detail = [];
		$scope.nomore = false;
		$scope.nodata = false;

		$scope.params = {
			orderby: "new",
			page: 1,
			rowsPerPage: 5,
			country: "",
			minAge: "4",
			maxAge: "16",
			gender: "",		// 성별 Y,N
			today: "",		// 오늘이 생일인경우 Y,N
			mm: "",			// 특별한 기념일 월 mm
			dd: "",			// 특별한 기념일 일 dd
			orphan: "",		// 고아 Y,N
			specialNeed: ""	// 장애 Y,N
		};

		// list

		$scope.getList = function (params) {

			$scope.params = $.extend($scope.params, params);
			//console.log($scope.params);
			$scope.nomore = true;
			$http.get("/api/tcpt.ashx?t=get-random-child", { params: $scope.params }).success(function (r) {
				//console.log(r);
				if (r.success) {

					var data = r.data;

					$scope.nomore = data.length < 1;
					
					// 저장 
					if ($scope.nomore) return;
					$scope.row = data;

					// 데이터 변환
					$scope.row.countryname = $scope.row.countryname.replace(" ", "");
					$scope.row.birthdate = new Date($scope.row.birthdate);
					
					if ($scope.params.page == 1) {
						$scope.nodata = r.data.length < 1;
					}

				} else {

					//alert(r.message);
				}
			});


		}

		$scope.showChildPop = function ($event, item) {
			loading.show();
			if ($event) $event.preventDefault();
			popup.init($scope, "/common/child/pop/" + item.countrycode + "/" + item.childmasterid + "/" + item.childkey , function (modal) {
				modal.show();
				
				initChildPop($http, $scope, modal, item);

			}, { top: 0, iscroll: true, removeWhenClose: true });
		}
		
	//	$scope.getList();



		$(".btn_country").click(function () {
			idx = $(this).data("idx");
			if (!$(this).hasClass("fix")) {
				$scope.getList({ 'country': idx });
			}
		

			return false;
		})

	});


})();
