﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class about_us_press_view : FrontBasePage {

	const string listPath = "/about-us/press/";


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}
		base.PrimaryKey = requests[0];
	}


	protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<press>("p_id", Convert.ToInt32(PrimaryKey), "p_display", 1);
            //if (!dao.press.Any(p => p.p_id == Convert.ToInt32(PrimaryKey) && p.p_display))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var entity = dao.press.First(p => p.p_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<press>("p_id", Convert.ToInt32(PrimaryKey));

            p_title.Text = entity.p_title;
            p_regdate.Text = entity.p_regdate.ToString("yyyy.MM.dd");
            p_hits.Text = entity.p_hits.ToString("N0");
            p_content.Text = entity.p_content;
            p_url.HRef = entity.p_url;
            if (entity.p_url != "")
            {
                p_url.Visible = true;
            }
        }
	}
}