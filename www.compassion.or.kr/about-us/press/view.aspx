﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="about_us_press_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <!-- sub body -->
	<section class="sub_body">
 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>보도<em>자료</em></h1>
				<span class="desc">언론에 보도된 한국컴패션 관련 소식입니다</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->
				
		<!-- s: sub contents -->
		<div class="subContents aboutus">

			<div class="reporting w980">
				
				<!-- 게시판 상세 -->
				<div class="boardView_1 line1">
					<div class="tit_box noline">
						<span class="tit">
                            <asp:Literal runat="server" ID="p_title" />
						</span>
                        <span class="txt">
							<span>
                                <asp:Literal runat="server" ID="p_regdate" />
							</span>
							<span class="bar"></span>
							<span class="hit">
                                <asp:Literal runat="server" ID="p_hits" />
							</span>
						</span>
					</div>
					<div class="view_contents">
                        <div><a href="#" class="view" runat="server" ID="p_url" target="_blank" visible="false">[기사원문보기]</a></div>
						<asp:Literal runat="server" ID="p_content" />

					</div>
					
				</div>
				<!--// 게시판 상세 -->

				<div class="tar">
                    <a href="#" class="btn_type4" title="목록" runat="server" id="btnList">목록</a>
				</div>

			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->
    
</asp:Content>