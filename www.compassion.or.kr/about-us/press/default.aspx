﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="about_us_press_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="default.js"></script>
</asp:Content>
<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>언론<em>보도</em></h1>
				<span class="desc">언론에 보도된 한국컴패션 관련 소식입니다</span>

                <uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents aboutus" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

			<div class="reporting w980">

				<div class="sortWrap clear2 mb20">
					<div class="fr relative">
						<label for="search" class="hidden">검색어 입력</label>
						<input type="text" name="k_word" id="k_word" id="search" class="input_search1" style="width:245px" placeholder="검색어를 입력해 주세요"  ng-enter="search()"/>
						<a href="#" class="search_area1" ng-click="search()">검색</a>
					</div>
				</div>

				<div class="boardList_2" id="l">
					<ul class="list">
						<li ng-repeat="item in list" ng-click="goView(item.p_id)">
							<span class="lst_box">
								<span class="whole_box">
									<a class="lst_tit"><span class="news_agency" ng-if="item.p_press">[{{item.p_press}}]</span>{{item.p_title  | limitHtml : 60}}</a>
								</span>
								<span class="lst_txt">
									<span>{{parseDate(item.p_regdate) | date:'yyyy.MM.dd'}}</span>
									<span class="lst_bar"></span>
									<span class="lst_hit">{{item.p_hits}}</span>
								</span>
                                <a class="list_link" ng-bind-html="item.p_content | limitHtml: 150"></a>
							</span>
						</li>
						
						<!-- 등록된 글이 없을때 -->
                        <li class="no_content" ng-if="total == 0 && !isSearch">등록된 글이 없습니다.</li>
						<!--//  -->

						<!-- 검색결과 없을때 -->
                        <li class="no_content" ng-if="total == 0 && isSearch">검색 결과가 없습니다.</li>
						<!--//  -->

                    </ul>
                </div>

                <!-- page navigation -->
	            <div class="tac mb60">
		            <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
	            </div>


			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

     

    
</asp:Content>