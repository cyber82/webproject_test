﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="history.aspx.cs" Inherits="about_us_history" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>컴패션<em> 소개</em></h1>
				<span class="desc">함께 아파하는 마음, 컴패션(COMPASSION)을 소개합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 aboutus">

			<!--소개 공통 비주얼-->
			<div class="visual_aboutus">
				<h2 class="subTit_d">함께 아파하는 마음,<br />컴패션(COMPASSION)</h2>
				<span class="bar"></span>
				<p class="con_wh">
					국제어린이양육기구 컴패션은<br />
					전 세계 도움이 필요한 어린이들을 1:1로 결연하여 자립 가능한 성인이 될 때까지 양육하며<br />
					현재 25개국 180만 명 이상의 어린이들과 함께하고 있습니다.
				</p>
			</div>
			<!-- 소개 탭메뉴 -->
			<div class="tab_area">
				<ul class="w980 clear2">
					<li><a href="/about-us/about/">컴패션은</a></li>
					<li><a href="/about-us/about/soul">컴패션의 정신</a></li>
					<li><a href="/about-us/about/policy-child">어린이 보호 정책</a></li>
					<li class="on" ><a href="/about-us/about/history">연혁</a></li>
					<li><a href="/about-us/about/ci">한국컴패션 CI</a></li>
					<li><a href="/about-us/about/map">찾아오시는 길</a></li>
				</ul>
			</div>
			<!--//-->

			<!-- 연혁 -->
			<div class="history w980 pt60">
				<!-- 탭메뉴 -->
				<ul class="tab_type1 historyMenu font15">
					<li style="width:33%" class="on"><a href="#">한국컴패션- 후원국 (2003 ~ 현재)</a></li>
					<li style="width:33%"><a href="#">한국컴패션- 수혜국 (1970 ~ 2002)</a></li>
					<li style="width:34%"><a href="#">컴패션의 시작 (1950 ~ 1969)</a></li>
				</ul>
				<!--//-->

				<script type="text/javascript">
					$(function () {
						$(".tab_type1.historyMenu > li").click(function () {
							var idx = $(this).index() + 1;
							$(".tab_type1.historyMenu > li").removeClass("on");
							$(this).addClass("on");
							$(".tc").hide();
							$(".tc" + idx).show();

							return false;
						});
					})
				</script>

				<!-- 탭메뉴 컨텐츠 -->
				<div class="tab_contents">
					<div class="dist">
						<span class="korea">한국컴패션</span>
						<span class="global">국제컴패션</span>
					</div>
					<!-- 한국컴패션- 후원국 (2003 ~ 현재) -->
					<table class="tc tc1">
						<caption>한국컴패션 연혁</caption>
						<colgroup>
							<col width="50%" />
							<col width="50%" />
						</colgroup>
						<tbody>
                            <tr>
								<td colspan="2" class="year">2016</td>
							</tr>
							<tr>
								<td class="kor tdL">
									<ul>
										<li><span class="rect">바이럴 캠페인 ‘내안에 컴패션’ 론칭</span></li>
										<li><span class="rect">컴패션밴드 결성 10주년 기념 콘서트 ‘그의 열매’ 개최</span></li>
										<li><span class="rect">북한어린이 전인적 양육을 위한 컴패션사역훈련 <br />진행 및 교재 개발</span></li>
										<li>
											<span class="rect">나눔으로 시작하는 동행 ‘결혼 첫 나눔’ 론칭</span>
											<img src="/common/img/page/about-us/history2016_img_1.jpg" alt="연혁이미지" />
										</li>
										<li><span class="rect">한국컴패션 홈페이지, 모바일 앱 개편</span></li>
									</ul>
								</td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">비영리 신용도평가기구인 채리티내비게이터로부터 <br />‘재정투명성과 책무성(Accountability and Transparency)’<br />항목에서 최고점 획득</span></li>
										<li><span class="rect">전 세계 수혜국 · 후원국 온라인 운영 시스템 통합</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2015</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">비영리 신용도 평가기구인 채리티내비게이터로부터<br />14년 연속 상위 1% 랭크</span></li>
										<li><span class="rect">170만 명 이상의 어린이 양육</span></li>
									</ul>
								</td>
								<td class="kor tdR">
									<ul>
										<li>
											<span class="rect">북한사역서밋 “소망의 땅, 북한” 개최</span>
											<img src="/common/img/page/about-us/history2003_img_1.jpg" alt="연혁이미지" />
										</li>
										<li><span class="rect">유산기부 프로그램, ‘믿음의 유산’ 론칭</span></li>
										<li><span class="rect">후원자가 만든 다이어리 캠페인 ‘컴패션블루북’ 론칭</span></li>
										<li><span class="rect">컴패션 바자회 'Love & Passion Charity Bazar' 3년 연속 진행</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2014</td>
							</tr>
							<tr>
								<td class="kor tdL">
									<ul>
										<li><span class="rect">CHANGE 가난에서 희망으로, 컴패션체험전 오픈</span></li>
										<li><span class="rect">컴패션 바자회 'Love & Passion Charity Bazar' 2년 연속 진행</span></li>
									</ul>
								</td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">비영리 신용도 평가기구인 채리티내비게이터로부터<br />13년 연속 상위 1% 랭크</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2013</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">130만 명 이상의 어린이 양육</span></li>
										<li><span class="rect">노르덴(노르웨이, 스웨덴, 핀란드, 덴마크),<br />12번째 후원국으로 컴패션 사역 동참</span></li>
										<li><span class="rect">비영리 신용도 평가기구인 채리티내비게이터로부터<br />12년 연속 상위 1% 랭크</span></li>
										<li>
											<span class="rect">산티아고 지미 메야도(Santiago Jimmy Mellado)는 국제컴패션 총재 선출,<br />
											지미 메야도는 엘살바도르 출신으로 7개 문화권에서 성장</span>
											<img src="/common/img/page/about-us/history2003_img_2.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
								<td class="kor tdR">
									<ul>
										<li><span class="rect">컴패션북한사역(North Korea Readiness Project) 시작</span></li>
										<li><span class="rect">한국컴패션 10주년 기념 컴패션위크 '가슴 속에 있는 이야기' 개최</span></li>
										<li>
											<span class="rect">한국컴패션 서정인 대표 저서 &lt;고맙다&gt; 출간</span>
											<img src="/common/img/page/about-us/history2003_img_3.jpg" alt="연혁이미지" />
										</li>
										<li><span class="rect">컴패션밴드 2집 앨범 &lt;그의 열매&gt; 발매</span></li>
										<li><span class="rect">부산지사 설립</span></li>
										<li><span class="rect">컴패션 바자회 'Love & Passion Charity Bazar' 개최</span></li>
										<li><span class="rect">첫 생일맞이 나눔, '첫 생일 첫 나눔' 론칭</span></li>
										<li><span class="rect">YVOC 대한민국 청소년 박람회 여성가족부장관상 2년 연속 수상</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2012</td>
							</tr>
							<tr>
								<td class="kor tdL">
									<ul>
										<li>
											<span class="rect">컴패션 60주년 감사예배</span>
											<img src="/common/img/page/about-us/history2003_img_4.jpg" alt="연혁이미지" />
										</li>
										<li><span class="rect">한국, 11만 명 이상 어린이를 양육하며 11개 후원국 가운데<br />(미국에 이어) 2위 등극</span></li>
										<li><span class="rect">SBS ‘힐링캠프-기쁘지 아니한가-차인표 후원자’편 방송</span></li>
										<li><span class="rect">한국컴패션 공식 애플리케이션 론칭</span></li>
										<li><span class="rect">컴패션밴드 콘서트 ‘황홀한 고백’ 개최</span></li>
										<li><span class="rect">CBS ‘세상을 바꾸는 시간 15분’ 특집 LOVE&COMPASSION 진행</span></li>
										<li><span class="rect">YVOC 대한민국 청소년 박람회 여성가족부장관상 수상</span></li>
									</ul>
								</td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">비영리 신용도 평가기구인 채리티내비게이터로부터<br />11년 연속 상위 1% 랭크</span></li>
										<li><span class="rect">컴패션 사역 60주년</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2011</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">비영리 신용도 평가기구인 채리티내비게이터로부터<br />10년 연속 상위 1% 랭크</span></li>
									</ul>
								</td>
								<td class="kor tdR">
									<ul>
										<li><span class="rect">제1회 일러스트 공모전</span></li>
										<li>
											<span class="rect">KBS'사랑의 리퀘스트' 방송</span>
											<img src="/common/img/page/about-us/history2003_img_5.jpg" alt="연혁이미지" />
										</li>
										<li><span class="rect">지속가능성 보고서 최초 발간</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2010</td>
							</tr>
							<tr>
								<td class="kor tdL">
									<ul>
										<li><span class="rect">사무실 통합 이전(컴패션 하우스 통합)</span></li>
										<li><span class="rect">물가 인상으로 최초 후원금 인상</span></li>
										<li><span class="rect">한국, 8만 명 이상의 어린이를 양육하며 11개 후원 국가 중<br />
											(미국, 호주에 이어) 후원 규모 3위 등극</span></li>
										<li><span class="rect">'나! 너! 우리! 컴패션' 후원자 월드컵 응원 모임 개최</span></li>
										<li><span class="rect">부산 & 미주 컴패션 사진전 개최</span></li>
										<li><span class="rect">션 · 정혜영 홍보대사 위촉</span></li>
										<li><span class="rect">한국, 유엔글로벌콤팩트(UNGC, United Nations Global Compact) 가입</span></li>
										<li><span class="rect">아이티 대지진 기금 모금 참여</span></li>
									</ul>
								</td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">비영리 신용도 평가기구인 채리티내비게이터로부터<br />9년 연속 상위 1% 랭크</span></li>
										<li><span class="rect">120만 명 이상 어린이 양육</span></li>
										<li><span class="rect">아이티 대지진으로 기금모금,<br />긴급구호 및 중·장기 계획을 통해 지속적 도움 제공</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2009</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">비영리 신용도 평가기구인 채리티내비게이터로부터<br />8년 연속 상위 1% 랭크</span></li>
										<li><span class="rect">스리랑카, 수혜국으로 컴패션 사역 동참(26번째 컴패션 수혜국)</span></li>
										<li>
											<span class="rect">백만 번째 후원어린이 탄생(토고의 펠로우 크포도 어린이,<br />
											후원자는 한국의 장미란 역도 올림픽 금메달리스트)</span>
											<img src="/common/img/page/about-us/history2003_img_6.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
								<td class="kor tdR">
									<ul>
										<li><span class="rect">부산 VOC(Voice of Compassion) 발족</span></li>
										<li><span class="rect">한국 11개 후원국가 중 (미국, 호주, 캐나다에 이어) 후원 규모 4위 등극</span></li>
										<li><span class="rect">Friends of Compassion 컴패션 사진전 개최</span></li>
										<li>
											<span class="rect">컴패션밴드 첫 앨범 &lt;사랑하기 때문에&gt; 발매</span>
											<img src="/common/img/page/about-us/history2003_img_7.jpg" alt="연혁이미지" />
										</li>
										<li><span class="rect">제3회 한국컴패션 후원자의 밤 개최</span></li>
										<li><span class="rect">한국, 7만 명 이상의 어린이 양육</span></li>
										<li><span class="rect">중·고등학생 홍보대사 YVOC(Youth Voice of Compassion) 발족</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2008</td>
							</tr>
							<tr>
								<td class="kor tdL">
									<ul>
										<li><span class="rect">Friends of Compassion 컴패션 사진전 개최</span></li>
										<li><span class="rect">제2회 한국컴패션 후원자의 밤 개최</span></li>
										<li><span class="rect">ARS 후원자 통합 서비스 개시</span></li>
										<li><span class="rect">홈페이지 개편</span></li>
										<li><span class="rect">한국, 4만 명 이상의 어린이 양육</span></li>
										<li><span class="rect">세계식량위기기금모금 캠페인 동참</span></li>
									</ul>
								</td>
								<td class="int tdR">
									<ul>
										<li>
											<span class="rect">세계식량위기기금모금 캠페인</span>
											<img src="/common/img/page/about-us/history2003_img_8.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2007</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">독일, 11번째 후원국으로 컴패션 사역 동참</span></li>
									</ul>
								</td>
								<td class="kor tdR">
									<ul>
										<li><span class="rect">한국컴패션 하우스 오픈</span></li>
										<li><span class="rect">한국컴패션 사무실 확장</span></li>
										<li><span class="rect">일반인 홍보대사 VOC(Voice of Compassion) 발족</span></li>
										<li><span class="rect">한국, 2만 명 이상 어린이 양육</span></li>
										<li><span class="rect">Friends of Compassion 컴패션 사진전 개최</span></li>
										<li>
											<span class="rect">제1회 후원자의 밤 개최</span>
											<img src="/common/img/page/about-us/history2003_img_9.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2006</td>
							</tr>
							<tr>
								<td class="kor tdL">
									<ul>
										<li><span class="rect">신애라 홍보대사 위촉</span></li>
										<li><span class="rect">컴패션밴드 결성 및 활동</span></li>
										<li><span class="rect">한국컴패션 미주지사 설립 (2012년 미국컴패션으로 업무이관)</span></li>
										<li><span class="rect">후원 관리 시스템 추가 개발 및 보완</span></li>
										<li><span class="rect">KBS 다큐멘터리 '2006 기적' 방송</span></li>
										<li>
											<span class="rect">제1회 Friends of Compassion 사진전 개최</span>
											<img src="/common/img/page/about-us/history2003_img_10.jpg" alt="연혁이미지" />
										</li>
										<li><span class="rect">한국, 후원어린이 1만 5천 명 양육</span></li>
									</ul>
								</td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">국제컴패션 본부(GMC : Global Ministry Center) 구축</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2005</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li>
											<span class="rect">가나, 수혜국으로 컴패션 사역 동참<br />
											(총 24개국에서 약 70만 명 이상의 어린이 양육)</span>
											<img src="/common/img/page/about-us/history2003_img_11.jpg" alt="연혁이미지" />
										</li>
										<li><span class="rect">비영리 신용도 평가기구인 채리티내비게이터(Charity Navigator)로부터<br />
											4년 연속 상위 1% 랭크</span></li>
									</ul>
								</td>
								<td class="kor tdR">
									<ul>
										<li><span class="rect">사무실 확장 이전</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2004</td>
							</tr>
							<tr>
								<td class="kor tdL">
									<ul>
										<li><span class="rect">한국, 미주 한인 후원자 대상 후원서비스 시작</span></li>
									</ul>
								</td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">부르키나파소, 수혜국으로 컴패션 사역 동참</span></li>
										<li><span class="rect">인도네시아에서 일어난 지진과 쓰나미로 피해 입은 어린이와 가족을 위해<br />
											83개의 새로운 주거지 건설</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2003</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">스위스, 후원국으로 컴패션 사역 동참</span></li>
										<li><span class="rect">총 10개 후원국과 총 23개 수혜국 규모로 성장</span></li>
										<li><span class="rect">방글라데시, 수혜국으로 컴패션 사역 동참</span></li>
										<li><span class="rect">엄마 뱃속의 태아와 아기, 엄마를 돌보는 태아·영아생존프로그램 시작,<br />
											가정에 초점을 맞춰 구체적인 혜택 제공</span></li>
									</ul>
								</td>
								<td class="kor tdR">
									<ul>
										<li>
											<span class="rect"><em>한국, 10번째 후원국으로 컴패션 사역 동참,</em><br />
											이로써 한국은 컴패션 안에서 처음으로 도움을 받던 나라에서<br />
											도움을 주는 나라가 됨</span>
											<img src="/common/img/page/about-us/history2003_img_12.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
							</tr>
						</tbody>
					</table>
					<!--//한국컴패션- 후원국 (2003 ~ 현재) -->

					<!-- 한국컴패션- 수혜국 (1970 ~ 2002) -->
					<table class="tc tc2" style="display:none;">
						<caption>한국컴패션 연혁</caption>
						<colgroup>
							<col width="50%" />
							<col width="50%" />
						</colgroup>
						<tbody>
							<tr>
								<td colspan="2" class="year">2002</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">컴패션 사역 50주년</span></li>
										<li>
											<span class="rect">니카라과, 수혜국으로 컴패션 사역 동참</span>
											<img src="/common/img/page/about-us/history1970_img_1.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">2001</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">21개 수혜국에서 40만 명 이상의 어린이 양육</span></li>
										<li><span class="rect">영국과 이탈리아, 후원국으로 컴패션 사역 동참</span></li>
										<li><span class="rect">미국의 유명 경제 전문지 포브스지(Forbes)로부터 후원금을 효율적으로<br />
											운영하는 대표적 기관으로 선정됨(2001.12.10 기획보도)</span></li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">2000</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li>
											<span class="rect">필리핀에서 1:1리더십결연프로그램 국제대회 개최,<br />제1기 1:1리더십결연프로그램 졸업생 배출</span>
											<img src="/common/img/page/about-us/history1970_img_2.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1999</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li>
											<span class="rect">탄자니아, 수혜국으로 컴패션 사역 동참</span>
											<img src="/common/img/page/about-us/history1970_img_3.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1998</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li>
											<span class="rect">25만 명 이상의 어린이 양육</span>
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1996</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li>
											<span class="rect">1:1리더십결연프로그램, 40명의 필리핀 학생으로 첫 출범<br />
											(컴패션 졸업생들 중, 우수한 학생을 선발, 대학에 갈 수 있도록 돕고<br />
											숙소와 크리스천 리더십 훈련, 멘토링 등 지원)</span>
										</li>
										<li><span class="rect">네덜란드, 후원국으로 컴패션 사역 동참</span></li>
										<li><span class="rect">컴패션 인터넷 사이트 개설</span></li>
										<li><span class="rect">20만 명 이상의 어린이 양육</span></li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1993</td>
							</tr>
							<tr>
								<td class="kor tdL">
									<ul>
										<li>
											<span class="rect"><em>한국의 경제성장을 축하하며 41년 동안의<br />한국 어린이 양육 활동을 마치고 컴패션 철수</em></span>
											<img src="/common/img/page/about-us/history1970_img_4.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
								<td class="int tdR">
									<ul>
										<li>
											<span class="rect">에티오피아, 수혜국으로 컴패션 사역 동참(20번째 수혜국)</span>
											<img src="/common/img/page/about-us/history1970_img_5.jpg" alt="연혁이미지" />
										</li>
										<li>
											<span class="rect">웨스 스태포드(Wess Stafford) 국제컴패션 총재 선출<br />
											웨스 스태포드는 아프리카 서부의 코트디부아르(아이보리코스트) 출생<br />
											다양한 국제구호단체에서 아이티를 섬겼고, 컴패션에서는 21년 동안<br />총재직 수행</span>
											<img src="/common/img/page/about-us/history1970_img_6.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1991</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li>
											<span class="rect">해외 장학금 프로그램(Overseas Scholarship Grant Program)을 만들어<br />
											정규 교육 과정을 이수한 후 고등교육, 직업 교육, 대학 진학 등을<br />
											원하는 학생들에게 필요한 자금을 지원</span>
										</li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1990</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">프랑스, 후원국으로 사역 동참. 4천 명 이상의 어린이들이<br />프랑스의 후원을 받음</span></li>
										<li><span class="rect">BAK(Bibles for All Kids)도입, 어린이들에게 성경 지원</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1987</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">10만 명 이상의 어린이 양육</span></li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1986</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">뉴질랜드 티어펀드(TEAR FUND), 컴패션 사역에 동참</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1985</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">페루, 수혜국으로 컴패션 사역 동참</span></li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1984</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">컴패션기금(Compassion Fund)을 조성하여 컴패션어린이들에게<br />
											특수의료 및 교육 서비스를 제공, 미결연 어린이 기금<br />
											(Unsponsored Children's Fund)을 조성하여 후원을 기다리는 신규<br />
											등록 어린이 지원</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1983</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">양육 프로그램의 영양 및 건강 관련 지원 분야를 보완하기 위하여<br />
											'기근·건강 기금'을 조성</span></li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1980</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li>
											<span class="rect">미국 콜로라도 주의 콜로라도 스프링스(Colorado Springs)로 사옥 이전</span>
											<img src="/common/img/page/about-us/history1970_img_7.jpg" alt="연혁이미지" />
										</li>
										<li><span class="rect">비정규교육 프로그램을 도입하여 정규교육 외의 각종 교육 활동 및 기술<br />
											훈련 등으로 어린이들의 자립을 도움</span></li>
										<li><span class="rect">케냐, 우간다, 르완다, 수혜국으로 사역 동참</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1979</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">멕시코, 수혜국으로 컴패션 사역 동참</span></li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1978</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">호주, 후원국으로 컴패션 사역 동참</span></li>
										<li><span class="rect">2만7천 명 이상의 어린이 양육</span></li>
										<li><span class="rect">긴급 의료혜택, 깨끗한(안전한) 식수 공급, 위생관리 향상, 재난으로부터의<br />
											보호, 말라리아로부터 보호하는 모기장, HIV와 AIDS 예방과 각종 치료<br />
											프로그램 제공으로 더욱 연약한 어린이 지원</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1977</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li>
											<span class="rect">엘살바도르, 수혜국으로 컴패션 사역 동참</span>
											<img src="/common/img/page/about-us/history1970_img_8.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1976</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">과테말라, 수혜국으로 컴패션 사역 동참</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1975</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">영국 기독교구호기관인 티어펀드(TEAR FUND), 컴패션 사역에 동참</span></li>
										<li><span class="rect">볼리비아, 브라질, 수혜국으로 컴패션 사역 동참</span></li>
										<li>
											<span class="rect">월리 에릭슨(Wally Erickson), 3번째 국제컴패션 총재 선출.<br />
											윌리 에릭슨은 18년 동안 컴패션 총재 역임</span>
											<img src="/common/img/page/about-us/history1970_img_9.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1974</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">교육의 기회가 없던 어린이들을 위한 어린이센터 설립, 교사의 급여,<br />
											교재비, 물품비, 의료비, 식비 등을 제공</span></li>
										<li><span class="rect">온두라스,  에콰도르, 수혜국으로 컴패션 사역 동참</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1972</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">필리핀, 수혜국으로 컴패션 사역 동참</span></li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1970</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">장애 또는 특정 질병을 앓고 있는 어린이들을 위한<br />
											'특별관리센터(Special Care Center)'를 설치하여 수술, 물리 치료, 영양<br />
											공급, 특수 의료 장비지원 등의 서비스를 제공</span></li>
										<li>
											<span class="rect">도미니카공화국, 수혜국으로 컴패션 사역 동참</span>
											<img src="/common/img/page/about-us/history1970_img_10.jpg" alt="연혁이미지" />
										</li>
										<li><span class="rect">콜롬비아, 태국, 수혜국으로 컴패션 사역 동참</span></li>
									</ul>
								</td>
							</tr>
						</tbody>
					</table>
					<!--//한국컴패션- 수혜국 (1970 ~ 2002) -->

					<!-- 컴패션의 시작 (1950 ~ 1969) -->
					<table class="tc tc3" style="display:none;">
						<caption>한국컴패션 연혁</caption>
						<colgroup>
							<col width="50%" />
							<col width="50%" />
						</colgroup>
						<tbody>
							<tr>
								<td colspan="2" class="year">1968</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">남아시아 담당 현장 관리자이자 한국컴패션 대표인 에드워드 킴벌<br />
											(Edward Kimball)에 의해 인도네시아, 싱가포르, 아이티, 인도로<br />
											사역 확대, 가구지원프로그램(Family Helper Plan) 도입</span></li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1966</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">헨리 하비(Henry L. Harvey) 국제컴패션 총재 선출,<br />
											헨리 하비, 8년 동안 재임(부총재, 스완슨 목사 아내인 미리암 스완슨)</span></li>
										<li>
											<span class="rect">에버렛 스완슨 추모 기금 조성, 기금은 서울 주재 한국컴패션 사무실<br />
											건축비로 사용</span>
											<img src="/common/img/page/about-us/history1950_img_1.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1965</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">에버렛 스완슨 목사,<br />
											컴패션 사역을 시작한 지 13년째인 당해 11월 15일 소천</span></li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1964</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li>
											<span class="rect">미리암 스완슨, 한국의 후원어린이들과 미국 80개 도시의 교회들을<br />
											순회하면서 찬양, 성경 낭독, 판토마임 등의 공연을 선보이며 많은<br />
											후원자들이 컴패션에 동참</span>
											<img src="/common/img/page/about-us/history1950_img_2.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1963</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">캐나다, 두 번째 컴패션 후원국으로 사역 동참</span></li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1962</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">에버렛 스완슨 목사, 마태복음 15장 32절<br />
											"내가 무리를 불쌍히[compassion] 여기노라"에 영감을 얻어 법인 명칭을<br />
											에버렛스완슨전도협회에서 컴패션(Compassion)으로 개명</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1961</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li>
											<span class="rect">에버렛스완슨전도협회, 한국의 108개 보육원과 가정 지원</span>
											<img src="/common/img/page/about-us/history1950_img_3.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1956</td>
							</tr>
							<tr>
								<td class="kor tdL"></td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">에버렛스완슨전도협회(The Everett Swanson Evangelistic Association)<br />
											라는 비영리법인 설립, 본격적인 후원금 관리 및 복음 전파 활동 시작</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1954</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li><span class="rect">1:1결연을 통한 후원 프로그램 개발, 이로써 후원자 개인, 가족,<br />
											교회 단위로 한국의 부모를 잃은 어린이들을 후원할 수 있게 됨<br />
											(매월 2~3달러씩 지원)</span></li>
										<li>
											<span class="rect">후원자와의 1:1결연으로 성경 공부, 의·식·주 지원,<br />
											정기 의료 검진 등의 혜택을 받게 됨</span>
											<img src="/common/img/page/about-us/history1950_img_4.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
							<tr>
								<td colspan="2" class="year">1953</td>
							</tr>
							<tr>
								<td class="kor tdL"</td>
								<td class="int tdR">
									<ul>
										<li><span class="rect">에버렛 스완슨 목사로부터 한국의 참담한 현실을 듣게 된<br />
											미국 크리스천들이 한국 어린이들에게 쌀과 연료 등 일차적 필요를 지원</span></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="year">1952</td>
							</tr>
							<tr>
								<td class="int tdL">
									<ul>
										<li>
											<span class="rect">에버렛 스완슨(Everett Swanson) 목사, 한국 전쟁고아들의<br />
											참혹한 실상을 목격하고 한국 어린이들을 돕기로 결심,<br />
											부인 미리암 스완슨(Miriam Swanson)과 친구,<br />
											동료들의 격려와 지원으로 어린이 지원 및 선교 활동 시작</span>
											<img src="/common/img/page/about-us/history1950_img_5.jpg" alt="연혁이미지" />
										</li>
									</ul>
								</td>
								<td class="kor tdR"></td>
							</tr>
						</tbody>
					</table>
					<!--//컴패션의 시작 (1950 ~ 1969) -->
				</div>
				<!--// 탭메뉴 컨텐츠 -->

				<!-- 배너 -->
				<div class="pt60">
                    <script>
                        function getHost() {
                            var host = '';
                            var loc = String(location.href);
                            if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
                            else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
                            else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
                            return host;
                        }

                        function goLink() {
                            var host = getHost();
                            if (host != '') location.href = 'http://' + host + '/popup/60_anniversary/main.asp';
                        }
                    </script>
					<a href="javascript:goLink()" target="_blank"><img src="/common/img/page/about-us/banner_1.jpg" alt="컴패션 60주년 발자취 바로가기" /></a>
				</div>
				<!--// -->
			</div>
			<!-- //연혁 -->


		</div>
		

		<!--// e: sub contents -->

		<div class="h100"></div>


    </section>
    <!--// sub body -->

</asp:Content>