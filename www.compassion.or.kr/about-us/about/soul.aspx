﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="soul.aspx.cs" Inherits="about_us_soul" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>컴패션<em> 소개</em></h1>
				<span class="desc">함께 아파하는 마음, 컴패션(COMPASSION)을 소개합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 aboutus">

			<!--소개 공통 비주얼-->
			<div class="visual_aboutus">
				<h2 class="subTit_d">함께 아파하는 마음,<br />컴패션(COMPASSION)</h2>
				<span class="bar"></span>
				<p class="con_wh">
					국제어린이양육기구 컴패션은<br />
					전 세계 도움이 필요한 어린이들을 1:1로 결연하여 자립 가능한 성인이 될 때까지 양육하며<br />
					현재 25개국 180만 명 이상의 어린이들과 함께하고 있습니다.
				</p>
			</div>
			<!-- 소개 탭메뉴 -->
			<div class="tab_area">
				<ul class="w980 clear2">
					<li><a href="/about-us/about/">컴패션은</a></li>
					<li class="on"><a href="/about-us/about/soul">컴패션의 정신</a></li>
					<li><a href="/about-us/about/policy-child">어린이 보호 정책</a></li>
					<li><a href="/about-us/about/history">연혁</a></li>
					<li><a href="/about-us/about/ci">한국컴패션 CI</a></li>
					<li><a href="/about-us/about/map">찾아오시는 길</a></li>
				</ul>
			</div>
			<!--//-->

			<!-- 컴패션의 정신-->
			<div class="spirit tac ">

				<div class="promise">
					<h2 class="subTit_m">컴패션의 약속</h2>
					<span class="bar"></span>
					<p class="s_tit6">컴패션 사명선언문</p>
					<p class="s_con3">컴패션은 꿈을 잃은 어린이들을 그리스도의 사랑으로 양육하기 위해 존재합니다.</p>
				</div>

				<div class="focus">
					<h2 class="subTit_m wh">컴패션 사역의 3가지 기준</h2>
					<span class="bar"></span>
					<p class="s_tit6">컴패션 3C</p>
					<ul class="clear2 w980">
						<li>
							<p class="tit">
								그리스도 중심<br />
								<span class="eng">christ-centered</span>
							</p>
							<p class="con">예수님을 중심으로 일합니다.</p>
						</li>
						<li>
							<p class="tit">
								어린이 대상<br />
								<span class="eng">Child-focused</span>
							</p>
							<p class="con">어린이를 대상으로 합니다.</p>
						</li>
						<li>
							<p class="tit">
								교회 기반<br />
								<span class="eng">church-based</span>
							</p>
							<p class="con">지역교회를 기반으로 어린이를 양육합니다.</p>
						</li>
					</ul>
				</div>

				<div class="coreValue w980">
					<h2 class="subTit_m">컴패션 핵심가치</h2>
					<span class="bar"></span>
					<p class="copy">“ 컴패션은 <em>지역교회와 협력하는(Committed to the Church)<br />
					그리스도 중심(Christ-centered)의 기관</em>으로 핵심가치를 가지고 있습니다.”</p>
					<ul class="clear2">
						<li>
							<div class="curcle">
								<span class="txt">정직</span>
								<span class="eng">Integrity</span>
							</div>
							<p class="con">정직을 최우선으로 합니다.</p>
						</li>
						<li>
							<div class="curcle">
								<span class="txt">탁월</span>
								<span class="eng">Excellence</span>
							</div>
							<p class="con">항상 탁월한 성과를 향한 끊임없는<br />시도와 노력을 추구합니다.</p>
						</li>
						<li>
							<div class="curcle">
								<span class="txt">청지기 정신</span>
								<span class="eng">Stewardship</span>
							</div>
							<p class="con">하나님의 청지기로서<br />투명하고 성실하게 일합니다.</p>
						</li>
						<li>
							<div class="curcle">
								<span class="txt">존귀</span>
								<span class="eng">Dignity</span>
							</div>
							<p class="con">어린이를 비롯한 모든 사람들을 하나님의<br />사랑하는 자녀로서 존귀하게 여깁니다.</p>
						</li>
					</ul>
				</div>

			</div>
			<!-- //컴패션의 정신-->


		</div>
		

		<!--// e: sub contents -->

		<div class="h100"></div>


    </section>
    <!--// sub body -->

</asp:Content>
