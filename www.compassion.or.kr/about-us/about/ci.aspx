﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ci.aspx.cs" Inherits="about_us_ci" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>컴패션<em> 소개</em></h1>
				<span class="desc">함께 아파하는 마음, 컴패션(COMPASSION)을 소개합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 aboutus">

			<!--소개 공통 비주얼-->
			<div class="visual_aboutus">
				<h2 class="subTit_d">함께 아파하는 마음,<br />컴패션(COMPASSION)</h2>
				<span class="bar"></span>
				<p class="con_wh">
					국제어린이양육기구 컴패션은<br />
					전 세계 도움이 필요한 어린이들을 1:1로 결연하여 자립 가능한 성인이 될 때까지 양육하며<br />
					현재 25개국 180만 명 이상의 어린이들과 함께하고 있습니다.
				</p>
			</div>
			<!-- 소개 탭메뉴 -->
			<div class="tab_area">
				<ul class="w980 clear2">
					<li><a href="/about-us/about/">컴패션은</a></li>
					<li><a href="/about-us/about/soul">컴패션의 정신</a></li>
					<li><a href="/about-us/about/policy-child">어린이 보호 정책</a></li>
					<li><a href="/about-us/about/history">연혁</a></li>
					<li class="on" ><a href="/about-us/about/ci">한국컴패션 CI</a></li>
					<li><a href="/about-us/about/map">찾아오시는 길</a></li>
				</ul>
			</div>
			<!--//-->

			<!-- 한국컴패션CI -->
			<div class="identity">

				<div class="ci_logo w980">
					<h2 class="subTit_m">컴패션 로고</h2>
					<p class="desc">컴패션 양육을 진정성 있게 전하기 위해 전 세계 컴패션이 일관성 있게 소통합니다</p>
					<p class="s_con3">컴패션 로고는 전 세계 모든 컴패션에서 동일하게 사용하며, 로고의 각 요소는 단독으로 사용하지 않습니다.<br />
					모든 컴패션의 커뮤니케이션에 표시되어야 합니다. 로고는 어떤 일이 있어도 변형되거나 재창조 되어서는 안되며,<br />
					어떠한 첨가나 삭제도 있어서는 안 됩니다.</p>
				</div>
				
				<div class="logo_mean">
					<ul class="w980">
						<li>
							<div class="logo_part"><img src="/common/img/page/about-us/logo_2.jpg" alt="컴패션 로고 의미" /></div>
							<div class="meaning">
								<span class="tit">컴패션</span><span class="eng">COMPASSION</span>
								<p class="s_con3">
									“예수께서 제자들을 불러 이르시되 내가 무리를 불쌍히 여기노라<br />
									(compassion, 컴패션) (마태복음 15:32)”는 성경 말씀을 나타냅니다.<br />
									예수님이 이 땅에서 우리에게 품으셨던 마음, ‘함께 아파하는 마음’을<br />의미합니다.
								</p>
							</div>
						</li>
						<li>
							<div class="logo_part"><img src="/common/img/page/about-us/logo_3.jpg" alt="컴패션 로고 의미" /></div>
							<div class="meaning">
								<span class="tit">캐치프레이즈</span><span class="eng">CATCHPHRASE</span>
								<p class="s_con3">
									“꿈을 잃은 어린이들에게 그리스도의 사랑을”이라는 캐치프레이즈는<br />
									컴패션이 가난 때문에 잠재력을 발휘하지 못하고 있는 연약한 어린이를<br />
									대상으로 하며,  하나님의 사랑을 바탕으로 하는 어린이 양육 사역임을<br />
									분명히 하고 있습니다.
								</p>
							</div>
						</li>
						<li>
							<div class="logo_part"><img src="/common/img/page/about-us/logo_4.jpg" alt="컴패션 로고 의미" /></div>
							<div class="meaning">
								<span class="tit">스키피</span><span class="eng">SKIPPY</span>
								<p class="s_con3">
									꿈을 잃은 어린이가 꿈을 찾고 높이 도약하는 모습(SKIP)을 담고 있습니다.<br />
									스키피는 단독으로 사용하지 않습니다.
								</p>
							</div>
							
						</li>
					</ul>
					
				</div>
				
				<div class="bluecorner w980 relative">
					<span id="comBlueCorner"></span>
					<h2 class="subTit_m">컴패션 블루코너(BLUE CORNER)</h2>
					<span class="bar"></span>
					<p class="s_tit7">도움이 필요한 어린이를 위해 일한다는 컴패션의 정체성을 드러냅니다.</p>
					<p class="s_con3">추수할 때, 가난한 자와 객을 위하여 밭 모퉁이까지 다 베지 말라는 말씀에서 ‘밭 모퉁이’를 상징하는 컴패션 블루코너는,<br />
					우리가 도움이 필요한 어린이들을 돕고 대변하는 목소리임을 나타내고자 하는 컴패션 브랜드의 중요한 요소입니다.<br />
					이를 컴패션이 소통하는 모든 매체 앞 표지 오른쪽 위에 새겨 넣어 하나님이 성경에서 이를 말씀으로 명하고 계심과<br />
					우리가 누구인지를 잊지 않고자 합니다.</p>
					<div class="bible mt40">
						<p class="mb10">
							너희 땅의 곡물을 벨 때에 밭 모퉁이(코너, the corners)까지 다 베지 말며 떨어진 것을 줍지 말고<br />
							너는 그것을 가난한 자와 객을 위하여 버려두라 나는 너희 하나님 여호와니라
						</p>
						<span>레위기 23:22</span>
					</div>
				</div>

				<div class="blue">
					<div class="w980 tac">
						<h2 class="subTit_m wh">컴패션 블루</h2>
						<span class="bar"></span>
						<p class="con_wh">컴패션은 깊고 강한 파란색입니다</p>
						<div class="desc_wrap clear2">
							<p class="panton">팬톤 286<span class="eng">(PANTON 286)</span></p>
							<p class="desc">
								컴패션은 색을 통해 우리가 경험하고 있는 세계의 진짜 모습을 표현하고자 합니다.<br />
								또한 컴패션을 나타내는 대표색은 짙은 파란색이며, 일반적으로 파란색은 평온과 책임,<br />
								신뢰를 의미합니다.<br />
								<br />
								그 중에서도 컴패션의 파란색, ‘팬톤 286’은 깊고 강한 파란색입니다.<br />
								자신감 있고, 희망적이며, 신뢰할 만하고 효과적이라는 의미를 담고자 합니다.<br />
								또한 컴패션이 하나님의 사랑에 의해 어린이를 양육하고 있음을 보여줍니다.<br />
								<br />
								(문화에 따라 파란 색은 기독교성을 나타냅니다.)
							</p>
						</div>
					</div>
				</div>

			</div>
			<!-- //한국컴패션CI -->


		</div>
		

		<!--// e: sub contents -->

    </section>
    <!--// sub body -->

</asp:Content>
