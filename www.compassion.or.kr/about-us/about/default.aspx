﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="about_us_intro" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>컴패션<em> 소개</em></h1>
				<span class="desc">함께 아파하는 마음, 컴패션(COMPASSION)을 소개합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 aboutus">

			<!--소개 공통 비주얼-->
			<div class="visual_aboutus">
				<h2 class="subTit_d">함께 아파하는 마음,<br />컴패션(COMPASSION)</h2>
				<span class="bar"></span>
				<p class="con_wh">
					국제어린이양육기구 컴패션은<br />
					전 세계 도움이 필요한 어린이들을 1:1로 결연하여 자립 가능한 성인이 될 때까지 양육하며<br />
					현재 25개국 180만 명 이상의 어린이들과 함께하고 있습니다.
				</p>
			</div>
			<!-- 소개 탭메뉴 -->
			<div class="tab_area">
				<ul class="w980 clear2">
					<li class="on" ><a href="/about-us/about/">컴패션은</a></li>
					<li ><a href="/about-us/about/soul">컴패션의 정신</a></li>
					<li ><a href="/about-us/about/policy-child">어린이 보호 정책</a></li>
					<li ><a href="/about-us/about/history">연혁</a></li>
					<li ><a href="/about-us/about/ci">한국컴패션 CI</a></li>
					<li ><a href="/about-us/about/map">찾아오시는 길</a></li>
				</ul>
			</div>
			<!--//-->

			<!-- 컴패션은 -->
			<div class="compassion_is tac ">

				<div class="start w980">
					<h2 class="subTit_m">컴패션의 시작</h2>
					<div class="conWrap clear2">
						<p class="conL"><em>컴패션은 </em>한국에서 시작되었습니다.</p>
						<p class="conR">
							<strong>컴패션의 시작은 ‘한 사람’이었습니다.</strong><br />
							1952년 겨울, 미국인 에버렛 스완슨(Everett Swanson) 목사는 차가운 새벽 거리를 걷던 중,<br />
							길가에 널려진 쓰레기 더미를 군용트럭으로 던지는 인부들을 만났습니다.<br />
							<br />
							그들을 도우려고 다가간 순간, 스완슨 목사는 그것이 쓰레기가 아니라 밤새 혹독한 추위와<br />
							배고픔을 견디지 못해 얼어 죽은 어린이들이라는 것을 알게 되었습니다. 큰 충격을 받은 스완슨 목사의 마음에 “너는 이것을 보았다, 이제 무엇을 할 것인가?”라는 질문이 들려왔습니다.<br />
							미국으로 돌아	간 스완슨 목사는 전 지역을 돌아다니며 외쳤습니다.<br />
							<br />
							<em>“한국의 어린이를 잊지 말아 주세요.<br />
							배고픔과 질병으로 죽어가는 한국 어린이의 후원자가 되어주세요!”</em><br />
							<br />
							이것이 컴패션의 시작이었습니다.
						</p>
					</div>
					<div class="bible mt40">
						<p class="mb10">예수께서 제자들을 불러 이르시되 내가 무리를 불쌍히 여기노라 (compassion, 컴패션) 그들이 나와 함께 있은 지<br />
						이미 사흘이매 먹을 것이 없도다 길에서 기진할까 하여 굶겨 보내지 못하겠노라</p>
						<span>마태복음 15:32</span>
					</div>
				</div>

				<div class="about_movie">
					<h2 class="subTit_m wh">컴패션 히스토리</h2>
					<span class="bar"></span>
					<div><iframe width="980" height="551" title="영상" src="https://www.youtube.com/embed/cNX_DzClfZ4?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe></div>
				</div>

				<div class="bringup w980">
					<h2 class="subTit_m">컴패션의 양육</h2>
					<p class="copy">“ 컴패션은 한 어린이를 <em>전인적으로 끝까지</em> 양육합니다. ”</p>
					<div class="con_box">
						<p class="s_tit5">컴패션은 가난으로 인해 하나님께서 주신 고유한 잠재력을 발휘할 기회를 송두리째 빼앗겨 버린 어린이들에게<br />
						기회와 소망을 줌으로써, 어린이가 공동체를 변화시킬 수 있는 주인공으로 자라날 것을 믿습니다.</p>
						<p class="s_con3">1952년 어린이 양육을 시작한 컴패션은, 1954년 1:1결연 방식으로 다양한 영역에서 도움을 주기 시작했습니다.<br />
						후원자와의 1:1 만남을 통해 어린이에게 자신이 중요한 사람이라는 것과 하나님이 자신을 소중한 사람으로 창조하셨다는 믿음을 심어 주고자 한 것입니다.<br />
						이는 부모가 자녀를 돌보는 것처럼 지적, 사회·정서적, 신체적, 영적 영역이라는 온전한 영역에서 균형 잡힌 양육을 제공,<br />
						자립 가능한 성인이 될 때까지 도움을 받는 전인적 양육(Holistic Child Development Model)을 가능케 했습니다.</p>
					</div>
					<div class="bible mt40">
						<p class="mb10">
							예수는 지혜(지적 영역)와 키가 자라가며(신체적 영역) 하나님과(영적 영역) 사람에게(사회·정서적 영역) 더욱 사랑스러워 가시더라
						</p>
						<span>누가복음 2:52</span>
					</div>
				</div>

				<div class="greeting_bg">
					<h2 class="subTit_d">인사말</h2>
					<span class="bar"></span>
					<p class="con_wh">한국컴패션을 방문해 주신 분들께 감사드립니다.</p>
				</div>

				<div class="greeting w980">
					<div class="conWrap clear2">
						<p class="conL"><em>경이로운 대상</em>, 어린이</p>
						<p class="conR">
							전 세계 컴패션어린이들과 만나면서 저는 항상 놀라움을 경험합니다. 상상할 수 없이 힘든 환경 속에서도 어린이들이 티 없이 맑은 미소를 잃지 않기 때문입니다. 아프리카와 남미의 거대한 슬럼가와 필리핀의 쓰레기 마을, 무덤 마을 등에서 발견하는 어린이들의 환한 웃음은 기적과도 같습니다.<br />
							<br />
							이 모든 기적이 후원자님의 사랑에서 시작되었습니다. 지금도 수많은 한국후원자 분들께서 꿈을 잃은 어린이들의 연약한 손을 잡아주고 계십니다. 컴패션은 전인적인 양육으로 어린이를 섬기며, 보내주신 사랑을 전하는 기적의 통로가 되겠습니다.<br />
							<br />
							감사합니다.<br />
							한국컴패션 대표  <strong>서정인</strong> 드림 <img src="/common/img/page/about-us/sign.jpg" alt="서명" />
						</p>
					</div>
					<div class="bible mt40">
						<p class="mb10">
							어린아이들이 내게 오는 것을 용납하고 금하지 말라 하나님의 나라가 이런 자의 것이니라
						</p>
						<span>마가복음 10:14</span>
					</div>
				</div>

			</div>


			<!-- @Cream : 조직도 추가 Start -->
			<!-- 2018-02-22 PENTABREED MODIFY : S -->
			<style type="text/css">
			.organization { margin:95px 0 0; text-align:center; }
			.organization .title { height:430px; margin:0 0 60px; color:#fff; text-align:center; background:url('http://www.compassion.or.kr/common/img/page/about-us/about_organization.jpg') center center no-repeat; background-size:cover; }
			.organization .title:before { content:''; display:inline-block; width:1px; height:100%; margin-right:-6px; vertical-align:middle; }
			.organization .title .txt{display:inline-block;padding-bottom:30px;font-size:15px;line-height:24px;font-family:'noto_d';color:#fff;vertical-align:middle;word-wrap:break-word; word-break:keep-all;}
			.organization .title .txt>strong{display:block;font-weight:normal;font-size:30px;line-height:30px;}
			.organization .title .txt>strong:after { content:''; display:block; width:2px; height:40px; margin:42px auto 36px; background-color:#b5b5b5; }
			.organization .chart span { font-family:'noto_r'; }
			.organization .chart .bg1 { display:inline-block; width:196px; height:52px; vertical-align:middle; color:#f4faff; font-size:22px; background-color:#005dab; }
			.organization .chart .bg1.h2 { height:86px; }
			.organization .chart .bg2 { display:inline-block; width:100px; height:36px; vertical-align:middle; color:#333; font-size:16px; background-color:#f0f7fe; }
			.organization .chart .bg2.h2 { width:60px; height:80px; }
			.organization .chart .bg2.w2 { width:110px;}
			.organization .chart .bg1:before,
			.organization .chart .bg2:before { content:''; display:inline-block; height:100%; vertical-align:middle; margin-right:0; }
			.organization .chart .mid { display:inline-block; vertical-align:middle; }
			.organization .row { width:980px; overflow:hidden; margin:53px auto 0; text-align:left; }
			.organization .row h3 { margin-bottom:18px; font-size:30px; color:#333; font-family:'noto_m'; }
			.organization .row .name { float:left; width:80px; font-size:18px; color:#333; }
			.organization .row .jobs { float:left; width:338px; margin-top:2px; font-size:15px; color:#767676; }
			.organization .row li { overflow:hidden; margin-top:5px; }
			.organization .directors { float:left; width:498px; }
			.organization .directors .name { width:160px; }
			.organization .auditors { float:right; width:418px; }
			.organization .chart li { position:relative; }
			.organization .chart .child { overflow:hidden; }
			.organization .chart .child > li { display:inline-block; *display:block; *zoom:0; vertical-align:top; }
			.organization .chart .child.ty1 > li { margin:0 15px; }
			.organization .chart .child.ty2 > li { margin:0 3px; }
			.organization .chart .line:before, .organization .chart .line:after { content:''; background:#e8e8e8; }
			.organization .chart .line.l1:before { display:block; width:2px; height:80px; margin:20px auto; }
			.organization .chart .line.l2:before { display:inline-block; width:80px; height:2px; margin-right:20px; vertical-align:middle; }
			.organization .chart .line.l3:after { display:inline-block; width:25px; height:2px; vertical-align:middle; }
			.organization .chart .line.l4:before { display:block; width:2px; height:50px; margin:20px auto; }
			.organization .chart .line.l5:before { display:block; width:2px; height:28px; margin:0 auto 12px; }
			.organization .chart .line.l5:after { position:absolute; width:100%; height:2px; top:0; right:50%; padding-left:8px; }
			.organization .chart .line.l5:first-child:after, .organization .chart .line.l6:first-child:after { right:auto; left:50%; }
			.organization .chart .line.l6:before { display:block; width:2px; height:80px; margin:0 auto 20px; }
			.organization .chart .line.l6:after { position:absolute; width:100%; height:2px; top:0; right:50%; padding-left:36px; }
			.organization .chart .line.l7:after { position:absolute; left:-25px; top:20px; display:inline-block; width:25px; height:2px; vertical-align:middle; }
            .organization .chart .line.l8:after { position:absolute; right:-25px; top:20px; display:inline-block; width:25px; height:2px; vertical-align:middle; }
			.organization .chart .abs { position:absolute; }
			.organization .chart .abs.a1 { left:50%; margin:43px 0 0 21px; }
			.organization .chart .abs.a2 { right:50%; margin:-64px 0 0 0; }
			.organization .chart .abs.a3 { left:50%; margin:-64px 0 0 25px; }
            .organization .chart .abs.a4 { right: 50%; margin: -64px 25px 0 0; }
			</style>
			<!-- 2018-02-22 PENTABREED MODIFY : E -->
			<div class="organization">
				<h2 class="title">
					<span class="txt"><strong>조직도</strong>컴패션은 꿈을 잃은 어린이들을 그리스도의 사랑으로 양육하기 위해 존재합니다.</span>
				</h2>

				<ol class="chart">
					<li>
						<span class="bg1">이사회</span>
					</li>
					<li class="line l1">
						<span class="bg1"><span class="mid">대표</span></span>
						<ol class="abs a1">
							<li class="line l2"><span class="bg2"><span class="mid">비서실</span></span></li>
						</ol>
					</li>
					<li class="line l1">
						<ul class="child ty1">
							<li class="line l6"><span class="bg1 h2"><span class="mid">사역<br />개발실</span></span>
								<ol class="line l4">
									<li class="abs a2 line l3"><span class="bg2"><span class="mid">사역개발본부</span></span></li>
									<li>
										<ul class="child ty2">
											<li class="line l5"><span class="bg2 h2"><span class="mid">교회<br />협력<br />1</span></span></li>
											<li class="line l5"><span class="bg2 h2"><span class="mid">교회<br />협력<br />2</span></span></li>
											<li class="line l5"><span class="bg2 h2"><span class="mid">애드<br />보킷</span></span></li>
											<li class="line l5"><span class="bg2 h2"><span class="mid">비전<br />트립</span></span></li>
										</ul>
									</li>
								</ol>
							</li>
							<li class="line l6"><span class="bg1 h2 fs"><span class="mid">Marketing &<br />Engagement</span></span>
								<ul class="child ty2 line l4">
									<!-- @Cream : 조직도 수정 20170407 Start -->
									<!-- 2018-02-22 PENTABREED MODIFY : S -->
									<li class="line l5"><span class="bg2 h2"><span class="mid">마케팅</span></span></li>
									<li class="line l5"><span class="bg2 h2"><span class="mid">PR</span></span></li>
									<li class="line l5"><span class="bg2 h2"><span class="mid">프로<br />모션</span></span></li>
									<!-- 2018-02-22 PENTABREED MODIFY : E -->
									<!-- @Cream : 조직도 수정 End -->
								</ul>
							</li>
							<li class="line l6"><span class="bg1 h2"><span class="mid">후원<br />지원실</span></span>
								<ul class="child ty2 line l4">
									<!-- 2018-02-22 PENTABREED MODIFY : S -->
									<li class="line l5"><span class="bg2 h2 w2"><span class="mid">Engagement</span></span></li>
									<!-- 2018-02-22 PENTABREED MODIFY : E -->
									<li class="line l5"><span class="bg2 h2"><span class="mid">후원자<br />서비스</span></span></li>
									<li class="line l5"><span class="bg2 h2"><span class="mid">후원<br />지원</span></span></li>
									<li class="line l5"><span class="bg2 h2"><span class="mid">IT</span></span></li>
								</ul>
							</li>
							<li class="line l6"><span class="bg1 h2"><span class="mid">경영<br />지원실</span></span>
								<!-- 2018-02-22 PENTABREED MODIFY : S -->
								<ol class="line l4">
                                    <li class="abs a4 line l8"><span class="bg2"><span class="mid">정보보안</span></span></li>
									<li class="abs a3 line l7"><span class="bg2"><span class="mid">경영관리</span></span></li>
									<li>
										<ul class="child ty2">
											<li class="line l5"><span class="bg2 h2"><span class="mid">인사<br />총무</span></span></li>
											<li class="line l5"><span class="bg2 h2"><span class="mid">재경</span></span></li>
										</ul>
									</li>
								</ol>
								<!-- 2018-02-22 PENTABREED MODIFY : E -->
							</li>
						</ul>
					</li>
				</ol>

				<div class="row">
					<div class="directors">
						<h3>이사</h3>
						<ul>
							<li><span class="name">이범 (이사장)</span> <span class="jobs">(주) 그라찌에 회장 / 한국사회과학자료원 이사장</span></li>
							<li><span class="name">서정인 (대표이사)</span> <span class="jobs">한국컴패션 대표</span></li>
							<li><span class="name">김명호</span> <span class="jobs">일산 대림교회 담임목사</span></li>
							<li><span class="name">남창호</span> <span class="jobs">(주) 범창종합기술 대표</span></li>
							<li><span class="name">Ed Anderson</span> <span class="jobs">전 국제컴패션 부총재</span></li>
							<li><span class="name">김도형</span> <span class="jobs">(유) 하이코스 대표</span></li>
							<li><span class="name">남기형</span> <span class="jobs">용산가정폭력관련상담소 소장</span></li>
							<li><span class="name">김홍은</span> <span class="jobs">구립 청파어린이집 원장</span></li>
						</ul>
					</div>
					<div class="auditors">
						<h3>감사</h3>
						<ul>
							<li><span class="name">서윤</span> <span class="jobs">한영회계법인 전무, 미국 공인회계사</span></li>
							<li><span class="name">허규만</span> <span class="jobs">딜로이트 안진회계법인 공인회계사</span></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- @Cream : 조직도 추가 End -->



			<!--// 컴패션은 -->
		
		</div>
		

		<!--// e: sub contents -->

		<div class="h100"></div>


    </section>
    <!--// sub body -->

</asp:Content>
