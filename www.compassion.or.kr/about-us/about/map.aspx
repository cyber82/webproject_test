﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="map.aspx.cs" Inherits="about_us_map" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script>
		$(function () {
		

			appendGoogleMapApi();

		})

		
		function appendGoogleMapApi() {
			if (typeof google === 'object' && typeof google.maps === 'object') {
				initMap();
			} else {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.src = "https://maps.googleapis.com/maps/api/js?language=ko&region=kr&key=<%:this.ViewState["googleMapApiKey"].ToString() %>&callback=initMap";
				document.body.appendChild(script);
			}
		}

		function initMap() {

			map = new google.maps.Map(document.getElementById('map'), {
				scrollwheel: true,
				center: { lat: 37.537772136407604, lng: 127.00514361262321 },
				zoom: 16,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});

			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(37.537772136407604, 127.00514361262321),
				icon: "/common/img/icon/pin.png",
				map: map
			});


			map2 = new google.maps.Map(document.getElementById('map2'), {
				scrollwheel: true,
				center: { lat: 37.5418203, lng: 127.0013571 },
				zoom: 16,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});

			var marker2 = new google.maps.Marker({
			    position: new google.maps.LatLng(37.5418161, 127.0035511),
				icon: "/common/img/icon/pin.png",
				map: map2
			});

		
		};
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>컴패션<em> 소개</em></h1>
				<span class="desc">함께 아파하는 마음, 컴패션(COMPASSION)을 소개합니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents padding0 aboutus">

			<!--소개 공통 비주얼-->
			<div class="visual_aboutus">
				<h2 class="subTit_d">함께 아파하는 마음,<br />컴패션(COMPASSION)</h2>
				<span class="bar"></span>
				<p class="con_wh">
					국제어린이양육기구 컴패션은<br />
					전 세계 도움이 필요한 어린이들을 1:1로 결연하여 자립 가능한 성인이 될 때까지 양육하며<br />
					현재 25개국 180만 명 이상의 어린이들과 함께하고 있습니다.
				</p>
			</div>
			<!-- 소개 탭메뉴 -->
			<div class="tab_area">
				<ul class="w980 clear2">
					<li><a href="/about-us/about/">컴패션은</a></li>
					<li><a href="/about-us/about/soul">컴패션의 정신</a></li>
					<li><a href="/about-us/about/policy-child">어린이 보호 정책</a></li>
					<li><a href="/about-us/about/history">연혁</a></li>
					<li><a href="/about-us/about/ci">한국컴패션 CI</a></li>
					<li class="on" ><a href="/about-us/about/map">찾아오시는 길</a></li>
				</ul>
			</div>
			<!--//-->

			<!-- 찾아 오시는 길 -->
			<div class="contact">

				<div class="conWrap">
					<div class="con_top clear2">
						<div class="info_bg">
							<div class="info">
								<h2 class="subTit_m wh">본사</h2>
								<table style="width:470px;">
									<caption>연락 정보</caption>
									<colgroup>
										<col width="75px" />
										<col width="395px" />
									</colgroup>
									<tbody>
										<tr>
											<th>주소</th>
											<td>서울시 용산구 한남대로 102-5(한남동 723-41) 석전빌딩</td>
										</tr>
										<tr>
											<th>대표번호</th>
											<td>02-740-1000 (평일 09:00 ~18:00/토,일 휴무)</td>
										</tr>
										<tr>
											<th>팩스</th>
											<td>02-740-1001</td>
										</tr>
										<tr>
											<th>이메일</th>
											<td><a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div id="map" class="map" style="height:480px;background:#e8e8e8;">지도영역</div>
					</div>
					<div class="con_bottom w980">
						<div class="con subway">
							<h3 class="subTit_m">지하철 이용 시</h3>
							<ul class="clear2">
								<li>
									<span class="line_num six">6</span>
									호선 한강진역 2번 출구 → 한남 외국인 아파트/한남초교 방면 도보로<br />
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10~15분
								</li>
								<li>
									<span class="line_num six">6</span>
									호선 한강진역 2번 출구 → 110B 버스로 환승 → 순천향 대학 병원 정류장<br />
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;하차 → 육교 건너 도보로 5분
								</li>
							</ul>
						</div>
						<div class="con bus">
							<h3 class="subTit_m">버스 이용 시</h3>
							<ul class="clear2">
								<li>
									<span class="sort blue">간선</span>
									110A, 110B, 140, 142, 144, 402, 405, 407, 408, 420, 470, 471, 472
								</li>
								<li>
									<span class="sort yellow">순환</span>
									03
								</li>
								<li>
									<span class="sort green">지선</span>
									0018, 0213, 3011, 6211, 8620
								</li>
								<li>
									<span class="sort red">직행</span>
									1005-2,1500, 5000, 5005, 5007, 5500-1, 5500-2, 8100, 8150, 8130,<br />
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									8200, 9000, 9001, 9003
								</li>
								<li>
									<span class="sort red">광역</span>
									9401, 9409
								</li>
								<li>
									<span class="sort sky">공항</span>
									6030
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="conWrap">
					<div class="con_top clear2">
						<div class="info_bg">
							<div class="info">
								<h2 class="subTit_m wh">별관</h2>
								<table style="width:470px;">
									<caption>연락 정보</caption>
									<colgroup>
										<col width="75px" />
										<col width="395px" />
									</colgroup>
									<tbody>
										<tr>
											<th>주소</th>
											<td>서울특별시 용산구 한남대로 150<br />신동빌딩 5층</td>
										</tr>
										<tr>
											<th>대표번호</th>
											<td>02-740-1000  (평일 09:00 ~18:00/토,일 휴무)</td>
										</tr>
										<tr>
											<th>팩스</th>
											<td>02-740-1001</td>
										</tr>
										<tr>
											<th>이메일</th>
											<td><a href="mailto:info@compassion.or.kr">info@compassion.or.kr</a></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div id="map2" class="map" style="height:480px;background:#e8e8e8;">지도영역</div>
					</div>
					<div class="con_bottom w980">
						<div class="con subway">
							<h3 class="subTit_m">지하철 이용 시</h3>
							<ul class="clear2">
								<li>
									<span class="line_num one">6</span>
									호선 한강진역 2번 출구 → 한남 외국인 아파트/한남초교 방면 도보로<br />
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5~10분
								</li>
							</ul>
						</div>
						<div class="con bus">
							<h3 class="subTit_m">버스 이용 시</h3>
							<ul class="clear2">
                                <li>
									<span class="sort blue">간선</span>
									142, 144, 400, 402, 407, 420, N13, 110B
								</li>

								<li>
									<span class="sort green">지선</span>
									3011, 6211
								</li>
							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- //찾아 오시는 길-->

		</div>
		

		<!--// e: sub contents -->

    </section>
    <!--// sub body -->

</asp:Content>
