﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="about_us_notice_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <!-- sub body -->
	<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>공지<em>사항</em></h1>
				<span class="desc">한국컴패션에서 알려드립니다</span>
				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents aboutus">

			<div class="notice w980">
				
				<!-- 게시판 상세 -->
				<div class="boardView_1 line1">
					<div class="tit_box noline">
						<span class="tit" style="word-break:break-all;">
                            <asp:Literal runat="server" ID="b_title" />
						</span>
						<span class="txt">
							<span>
                                <asp:Literal runat="server" ID="b_regdate" />
							</span>
							<span class="bar"></span>
							<span class="hit">
                                <asp:Literal runat="server" ID="b_hits" />
							</span>
						</span>
					</div>
					<div class="view_contents" style="word-break:break-all;">
						<asp:Literal runat="server" ID="b_content" />
					</div>
					
				</div>
				<!--// 게시판 상세 -->

				<div class="tar">
                    <a href="#" class="btn_type4" title="목록" runat="server" id="btnList">목록</a>
				</div>
                

			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->
    
</asp:Content>