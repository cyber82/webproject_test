﻿(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $location, paramService) {

		$scope.total = -1;
		$scope.isSearch = false;

		$scope.list = [];
		$scope.params = {
			page: 1,
			rowsPerPage: 10 , 
			b_type: 'notice'
		};

		// 파라미터 초기화
		$scope.params = $.extend($scope.params, paramService.getParameterValues());
		if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);

		// 검색
		$scope.search = function () {
			
			$scope.params.page = 1;
			$scope.params.k_word = $("#k_word").val();
			if ($scope.params.k_word && $scope.params.k_word != "") { $scope.isSearch = true; } else { $scope.isSearch = false; }
			$scope.getList();

		}

		// list -- 수정
		$scope.getList = function (params) {


			$scope.params = $.extend($scope.params, params);
			$http.get("/api/board.ashx?t=list", { params: $scope.params }).success(function (result) {
			    $scope.list = result.data;
			    $scope.total = result.data.length > 0 ? result.data[0].total : 0;
			    console.log($scope.list);
				if (params)
					scrollTo($("#l"), 10);
			});
		}


		// 상세페이지
		$scope.goView = function (id) {
			$http.post("/api/board.ashx?t=hits&id=" + id).then().finally(function () {
				location.href = "/about-us/notice/view/" + id + "?" + $.param($scope.params);
			});
		}

		$scope.parseDate = function (datetime) {
			return new Date(datetime);
		}


		$scope.getList();
		

	});

})();


