﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class about_us_notice_view : FrontBasePage {

	const string listPath = "/about-us/notice/";


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();

		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if (requests.Count < 1) {
			Response.Redirect(listPath, true);
		}
		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if (!requests[0].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}
		base.PrimaryKey = requests[0];
	}


	protected override void loadComplete(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<board>("b_id", Convert.ToInt32(PrimaryKey), "b_display" , 1);
            //if (!dao.board.Any(p => p.b_id == Convert.ToInt32(PrimaryKey) && p.b_display))
            if(!exist.Any())
            {
                Response.Redirect(listPath, true);
            }

            //var entity = dao.board.First(p => p.b_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<board>("b_id", Convert.ToInt32(PrimaryKey));

            b_title.Text = entity.b_title;
            b_regdate.Text = entity.b_regdate.ToString("yyyy.MM.dd");
            b_hits.Text = entity.b_hits.ToString("N0");
            b_content.Text = entity.b_content;

        }
	} 


}