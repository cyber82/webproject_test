﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="about_us_notice_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/about-us/notice/default.js"></script>
    
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    

   <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>공지<em>사항</em></h1>
				<span class="desc">한국컴패션에서 알려드립니다</span>

				<uc:breadcrumb runat="server"/>
			</div>

			
		</div>

		<!-- s: sub contents -->
		<div class="subContents aboutus">

			<div class="notice w980">

				<div class="sortWrap clear2 mb20">
					<div class="fr relative">
						<label for="search" class="hidden">검색어 입력</label>
						<input type="text" name="k_word" id="k_word" class="input_search1" style="width:245px" placeholder="검색어를 입력해 주세요" ng-enter="search()"/>
						<a href="#" ng-click="search()" class="search_area1">검색</a>
					</div>
				</div>
			
				<div class="boardList_2" id="l">
					<ul class="list">
						<li ng-repeat="item in list" ng-click="goView(item.b_id)">
							<span class="lst_box">
								<span class="whole_box">
									<a class="lst_tit" ng-bind-html="item.b_title | limitHtml: 45"></a>
								</span>
								<span class="lst_txt">
									<span>{{parseDate(item.b_regdate) | date:'yyyy.MM.dd'}}</span>
									<span class="lst_bar"></span>
									<span class="lst_hit">{{item.b_hits}}</span>
								</span>
								<a class="list_link" ng-bind-html="item.b_content | limitHtml: 150"></a>
							</span>
						</li>
						
						<!-- 등록된 글이 없을때 -->
                        <li class="no_content" ng-if="total == 0 && !isSearch">등록된 글이 없습니다.</li>
						<!--//  -->

						<!-- 검색결과 없을때 -->
                        <li class="no_content" ng-if="total == 0 && isSearch">검색 결과가 없습니다.</li>
						<!--//  -->

					</ul>
				</div>

                <!-- page navigation -->
	            <div class="tac mb60">
		            <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
	            </div>
	            <!--// page navigation -->

			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->



    


    
    

    
</asp:Content>