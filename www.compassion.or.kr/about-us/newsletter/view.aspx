﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="about_us_notice_view" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>뉴스<em>레터</em></h1>
                <span class="desc">어린이들과 후원자님들의 감동적인 이야기와 다양한 컴패션 소식을 전해드립니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents aboutus">

            <div class="newsletter w980">

                <!-- 게시판 상세 -->
                <div class="boardView_1 line1">
                    <div class="tit_box noline">
                        <span class="tit">
                            <asp:Literal runat="server" ID="b_title" /></span>
                    </div>
                    <div class="view_contents">
                        <asp:Literal runat="server" ID="b_content" />
                    </div>

                </div>
                <!--// 게시판 상세 -->

                <div class="tar"><a href="#" class="btn_type4" runat="server" id="btnList">목록</a></div>


            </div>

        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>



    <!--            
    <asp:Literal runat="server" ID="b_regdate" />
    <asp:Literal runat="server" ID="b_hits" />
    -->




</asp:Content>
