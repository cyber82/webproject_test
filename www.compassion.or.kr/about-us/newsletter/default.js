﻿(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

        $scope.total = -1;


        $scope.list = [];
        var rowsPerPage = 6
        $scope.params = {
            page: 1,
            rowsPerPage: rowsPerPage,
            b_type: 'newsletter'
        };

        // 파라미터 초기화
        $scope.params = $.extend($scope.params, paramService.getParameterValues());


        // list
        $scope.getList = function (params) {

            $scope.params = $.extend($scope.params, params);
            $http.get("/api/board.ashx?t=file_list", { params: $scope.params }).success(function (result) {
                $scope.list = $.merge($scope.list, result.data)
                $scope.total = result.data.length > 0 ? result.data[0].total : 0;

                //if (params)
                //scrollTo($("#l"), 10);
                //console.log($scope.params)
            
            });
        }

        // 뷰에서 돌아왔을때 처리
        if ($scope.params.page > 1) {
            var page = $scope.params.page;
            $scope.params.rowsPerPage = rowsPerPage * page;
            $scope.params.page = 1;
            $scope.getList({}, function () {
                $scope.params.rowsPerPage = rowsPerPage;
                $scope.params.page = parseInt(page);
            });

        } else {
            $scope.getList({ page: 1 });
        }


        // 더보기
        $scope.showMore = function ($event) {
            $scope.getList({ page: $scope.params.page + 1 });
        }


        // 상세페이지
        $scope.goView = function (id) {
            $http.post("/api/board.ashx?t=hits&id=" + id).then().finally(function () {
                location.href = "/about-us/newsletter/view/" + id + "?" + $.param($scope.params);
            });
        }

        $scope.parseDate = function (datetime) {
            return new Date(datetime);
        }

        // 레이어 팝업
        $scope.modal = {
            instance: null,

            init: function () {
                // 팝업
                popup.init($scope, "/about-us/newsletter/apply", function (modal) {
                    $scope.modal.instance = modal;
                });
            },

            show: function () {
                if (!$scope.modal.instance)
                    return;

                if (common.checkLogin()) {
                    if ($("#locationType").val() != "국내") {
                        alert("국내거주 회원만 신청가능합니다.");
                        return;
                    }
                    $(".apply_news").show();
                    $("#apply_complete").hide();
                    $scope.modal.instance.show();

                	// 컴파스의 데이타를 불러오는경우 
                    if ($("#dspAddrDoro").val() != "") {
                    	$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
                    	if ($("#dspAddrJibun").val() != "") {
                    		$("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
                    	}

                    } else if ($("#addr1").val() != "") {

                    	addr_array = $("#addr1").val().split("//");
                    	if (addr_array[0] != "") {
                    		$("#addr_road").text("[도로명주소] " + addr_array[0] + " " + $("#addr2").val());
                    	}
                    	if (addr_array[1]) {
                    		$("#addr_jibun").text("[지번주소] " + addr_array[1] + " " + $("#addr2").val());
                    	}
                    }

                }

            },

            request: function ($event) {
                if (!validateForm([
					{ id: "#zipcode", msg: "우편번호를 입력하세요" },
					{ id: "#addr1", msg: "주소를 입력하세요" },
					{ id: "#addr2", msg: "주소를 입력하세요" }
                ])) {
                    return;
                }

                var params = {
                    name: $("#name").text(),
                    phone: $("#phone").text(),
                    zipcode: $("#zipcode").val(),
                    addr1: $("#addr1").val(),
                    addr2: $("#addr2").val(),
                    hfAddressType: $("#hfAddressType").val()
                }
                //console.log(params)

                $.post("/api/newsletter.ashx?t=add", params, function (r) {
                    //console.log(r);
                    if (r.success) {
                        $("#cancel").show();
                        $("#phAdd").hide();
                        //alert("신청이 완료되었습니다.\n추가적인 문의사항이 있으시면 언제든 문의주시기 바랍니다. 후원자님의 귀한 섬김에 언제나 감사드립니다.");
                        $(".apply_news").hide();
                        $("#apply_complete").show();


                    } else {
                        alert(r.message);
                    }
                    //$scope.modal.close($event);
                });

            },

            close: function ($event) {
                $event.preventDefault();

                if (!$scope.modal.instance)
                    return;

                $scope.modal.instance.hide();
           
            },

            popup: function ($event) {
                $event.preventDefault();
                cert_setDomain();
				window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");

            }

        }
        

        // 구독관리 팝업
        $scope.cancelModal = {
            instance: null,

            init: function () {
                // 팝업
                popup.init($scope, "/about-us/newsletter/cancel", function (cancelModal) {
                    $scope.cancelModal.instance = cancelModal;
                });
            },

            show: function () {
                if (!$scope.cancelModal.instance)
                    return;

                if (common.checkLogin()) {
                    $("#news_unsubscribe").hide();
                    $(".news_subscribe").show();
                    $scope.cancelModal.instance.show();
                }

            	// 컴파스의 데이타를 불러오는경우 
                if ($("#dspAddrDoro").val() != "") {
                	$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
                	if ($("#dspAddrJibun").val() != "") {
                		$("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
                	}

                } else if ($("#addr1").val() != "") {
                	addr_array = $("#addr1").val().split("//");
                	if (addr_array[0] != "") {
                		$("#addr_road_cancel").text("[도로명주소] " + addr_array[0] + " " + $("#addr2").val());
                	}
                	if (addr_array[1]) {
                		$("#addr_jibun_cancel").text("[지번주소] " + addr_array[1] + " " + $("#addr2").val());
                	}
                }



            },

            request: function ($event) {
                $event.preventDefault();

                //if (confirm("현재 " + $("#name").text() + " 후원자님은 오프라인 뉴스레터를 구독하고 계십니다. 정말 구독을 해지하실 건가요?")) {
                    $("#phAdd").show();
                    $("#cancel").hide();
                    $("#sp_name").text($("#name").text());
                    $("#news_unsubscribe").show();
                    $(".news_subscribe").hide();
                    
                    $("#confirm_unsubscribe").unbind("click").bind("click", function () {
                        
                        $.post("/api/newsletter.ashx?t=cancel", function (r) {
                            //console.log(r);
                            if (r.success) {
                                alert("구독해지가 완료되었습니다");
                                $scope.cancelModal.close($event);  
                            } else {
                                alert(r.message);
                            }
                        });
                    });
                    //$scope.cancelModal.close($event);
                
            },

            close: function ($event) {
                $event.preventDefault();

                if (!$scope.cancelModal.instance)
                    return;

                $scope.cancelModal.instance.hide();

            }

        }
        
        if (common.isLogin()) {
            $scope.modal.init()
            $scope.cancelModal.init();
            
        }
    });

})();






