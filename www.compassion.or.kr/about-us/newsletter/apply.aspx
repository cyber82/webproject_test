﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="apply.aspx.cs" Inherits="about_us_apply" %>

<div style="background-color: transparent; width: 600px;">
	
	
    <input type="hidden" id="addr1" runat="server" class="addr1" />
    <input type="hidden" id="addr2" runat="server" class="addr2" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />


    <div class="pop_type1 w600 relative">
        <div class="pop_title apply_news">
            <span>오프라인 뉴스레터 신청</span>
            <button class="pop_close">
                <span>
                    <img src="/common/img/btn/close_1.png" alt="팝업닫기" ng-click="modal.close($event)" /></span></button>
        </div>

        <div class="pop_content newspop apply_news">

            <div class="tableWrap2">
                <table class="tbl_type1">
                    <caption>정보입력 테이블</caption>
                    <colgroup>
                        <col style="width: 14%" />
                        <col style="width: 86%" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th scope="row">
                                <label for="name">이름</label></th>
                            <td>
                                <p type="text" name="name" class="name con_bl" runat="server" id="name"></p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label for="phone">휴대폰</label></th>
                            <td>
                                <p name="phone" class="con_bl phone" runat="server" id="phone"></p>
                            </td>
                        </tr>
                        <tr class="address">
                            <th scope="row"><span>주소</span></th>
                            <td>
                                <div class="codeNum">
                                    <label for="zipcode" class="hidden">주소찾기</label>
                                    <input type="text" name="zipcode" runat="server" id="zipcode" class="input_type2 zipcode" style="width: 150px; background: #fdfdfd; border: 1px solid #d8d8d8;" disabled />
                                    <a href="#" class="btn_s_type2 ml5" ng-click="modal.popup($event)">주소찾기</a>
                                </div>

                                <p id="addr_road" class="fs14 mt15"></p>
                                <p id="addr_jibun" class="fs14 mt10"></p>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="conb">
                <p class="s_con3 mb25">
                    한국컴패션 뉴스레터에는 후원자님의 사랑으로 변화된<br />
                    어린이 이야기들이 생생하게 담겨 있습니다.<br />
                    뉴스레터는 어린이들을 향한 사랑으로 더욱 행복하고 풍성한 삶을 누리시는<br />
                    후원자님께 또 다른 기쁨의 통로가 될 것입니다.
                </p>
                <p class="s_con3">
                    입력된 개인정보가 일치하지 않는 경우,<br />
                    <a href="/my/account/" class="fc_black">마이컴패션>개인정보 수정</a>으로 이동하여 변경해주시기 바랍니다.
                </p>
            </div>

            <div class="tac">
                <a href="#" class="btn_type1" ng-click="modal.request($event)">신청하기</a>
            </div>

            <!-- 시스템 팝업 -->

        </div>
        <div class="systempop_ok popShadow" id="apply_complete" style="display:none">
            <p class="s_tit5">신청이 완료되었습니다.</p>
            <p class="s_con3">
                추가적인 질문이 있으시면 언제든<br />
                문의 주시기 바랍니다.<br />
                후원자님의 귀한 섬김에 언제나 감사드립니다.
            </p>
            <button class="close" ng-click="modal.close($event)">닫기</button>
        </div>
    </div>
    
    


</div>
