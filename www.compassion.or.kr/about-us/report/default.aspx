﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="about_us_notice_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/about-us/report/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	<input type="hidden" runat="server" id="except" />

    
    <!-- sub body -->
	<section class="sub_body"  ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>연례<em>보고</em></h1>
				<span class="desc">한국컴패션의 일 년간의 사업활동에 대한 보고입니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents aboutus w980">

			<!-- 탭메뉴 -->
			<ul class="tab_type1 mb55">
				<li style="width:50%" class="on"><a href="#">사업성과보고서</a></li>
				<li style="width:50%"><a href="/about-us/revenue/">재무/감사보고</a></li>
			</ul>
			<!--//-->

			<div class="performance">

				
				<div class="contit">
					<p class="tit">전인적 양육을 위한 <em>한국컴패션의 활동</em></p>
					<p class="s_con3">한국컴패션은 재정의 건실함과 투명성을 유지하기 위해 매년 KPMG(삼정회계법인)을 통한 외부 감사를 받고 있으며<br />
					국제컴패션은 후원자님의 후원금이 각 수혜 지역에서 적절하게 사용되고 있는지를 확실하게 검증하기 위해 양육 사업 국가별로 외부감사와 내부감사를 철저히 실시하고 있습니다.</p>
				</div>

				<!-- 최신 보고서 -->
				<div class="latest_report">
					<div class="clear2">
						<div class="imgBox"><span><img runat="server" id="b_thumb" alt="보고서 이미지" /></span></div>
						<div class="txtBox">
							<p class="eng">COMPASSION ANNUAL REPORT</p>
							<p class="report_tit"><asp:Literal runat="server" ID="b_title" /></p>
							<p class="report_con" style="white-space:pre-wrap;">&lt;목차&gt;<br /><asp:Literal runat="server" ID="b_summary" /></p>
							<div class="btn_group">
								<a runat="server" ID="btn_view" class="btn_s_type1 mr5"><span class="ic_look"></span>보고서 보기</a>
								<a runat="server" ID="btn_download" class="btn_s_type4" target="_blank"><span class="ic_down"></span>Download</a>
							</div>
						</div>
					</div>
				</div> 
				<!--// 최신 보고서 -->
				.
				<!-- 지난 보고서 리스트 -->
				<div class="report_list">
					<ul class="clear2"  id="l">
						<li  ng-repeat="item in list">
							<div class="imgBox"><span style="background:url('{{item.b_thumb}}') no-repeat center top;background-size:cover;"></span></div>
							<div class="txtBox">
								<p class="eng">COMPASSION ANNUAL REPORT</p>
								<p class="report_tit">{{item.b_title}}</p>
								<div class="btn_group">
									<a ng-click="goView(item.b_id)" class="btn_s_type1"><span class="ic_look"></span>보고서 보기</a>
									<a href="{{item.b_file}}" class="btn_s_type4" target="_blank"><span class="ic_down"></span>Download</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<!--// 지난 보고서 리스트 -->

				<!-- page navigation -->
				<div class="tac">
					<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
				</div>
				<!--// page navigation -->

			</div>
		
							
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->
			
    
</asp:Content>