﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class about_us_notice_default : FrontBasePage {


	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		GetRecently();
	}



	protected void GetRecently() {

        using (AdminDataContext dao = new AdminDataContext())
        {
            // 가장 최신 하나 가져옴
            var exist = www6.selectQ<board>("b_type", "report", "b_display", 1);
            //if (dao.board.Any(p => p.b_type == "report" && p.b_display))
            if(exist.Any())
            {
                //var recentlyEntity = dao.board.Where(p => p.b_type == "report" && p.b_display).OrderByDescending(p => p.b_regdate).First();
                var recentlyEntity = www6.selectQF<board>("b_type", "report", "b_display", 1, "b_regdate desc");

                except.Value = recentlyEntity.b_id.ToString();


                b_title.Text = recentlyEntity.b_title;
                b_summary.Text = recentlyEntity.b_summary;

                btn_view.HRef = "/about-us/report/view/" + recentlyEntity.b_id;

                var exist1 = www6.selectQ<file>("f_group", "image_board", "f_ref_id", recentlyEntity.b_id.ToString());
                //if (dao.file.Any(p => p.f_group == "image_board" && p.f_ref_id == recentlyEntity.b_id.ToString()))
                if(exist1.Any())
                {
                    //b_thumb.Src = dao.file.First(p => p.f_group == "image_board" && p.f_ref_id == recentlyEntity.b_id.ToString()).f_name.WithFileServerHost();
                    b_thumb.Src = www6.selectQF<file>("f_group", "image_board", "f_ref_id", recentlyEntity.b_id.ToString()).f_name.WithFileServerHost();
                }

                var exist2 = www6.selectQ<file>("f_group", "file_board", "f_ref_id", recentlyEntity.b_id.ToString());
                if (exist2.Any())
                {
                    //btn_download.HRef = dao.file.First(p => p.f_group == "file_board" && p.f_ref_id == recentlyEntity.b_id.ToString()).f_name.WithFileServerHost();
                    btn_download.HRef = www6.selectQF<file>("f_group", "file_board", "f_ref_id", recentlyEntity.b_id.ToString()).f_name.WithFileServerHost();
                }
            }



        }
	}
	

}