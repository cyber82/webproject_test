﻿(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		$scope.total = -1;
		$scope.page = 1;
		$scope.rowsPerPage = 5;

		$scope.list = [];
		$scope.params = {
			page: $scope.page,
			rowsPerPage: $scope.rowsPerPage,
			b_type: 'revenue'
		};

		// 파라미터 초기화
		$scope.params = $.extend($scope.params, paramService.getParameterValues());
		if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);


		// 검색
		$scope.search = function (params) {
			$scope.params = $.extend($scope.params, params);
			$scope.params.k_word = $("#k_word").val();
			$scope.getList();
		}


		// list
		$scope.getList = function (params) {
			$scope.params = $.extend($scope.params, params);
			$http.get("/api/board.ashx?t=file_list", { params: $scope.params }).success(function (result) {
				$scope.list = result.data;
				$scope.total = result.data.length > 0 ? result.data[0].total : 0;

				if (params)
					scrollTo($("#l"), 10);
			});
		}

		$scope.sorting = function (type) {
			$scope.params.page = 1;
			$("#k_word").val("");

			$scope.search();
		}


		// 상세페이지
		$scope.goView = function (id) {
			$http.post("/api/board.ashx?t=hits&id=" + id).then().finally(function () {
				location.href = "/about-us/revenue/view/" + id + "?" + $.param($scope.params);
			});
		}

		$scope.parseDate = function (datetime) {
			return new Date(datetime);
		}


		$scope.getList();



	});

})();



$(function () {
	$page.init();
});

var $page = {

	modal : null,
	init: function () {

		// 구독신청
		$("#btn_subscription").click(function () {
			if (common.isLogin) {
				// 회원정보 수정페이지로 이동
			} else {
				common.checkLogin();
			}
		});


		// 신청하기
		$("#btn_popup").click(function () {
			if (common.isLogin) {
				$.get("/api/userInfo.ashx?t=get", function (r) {
					modalShow($("#popupUi"), function (obj) {
						var entity = $.parseJSON(r.data);
						$page.modal = obj;
						$page.modal.find(".name").val(entity.UserName);
						$page.modal.find(".phone").val();

						// 주소찾기
						$page.modal.find(".find").click(function () {
							var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=601,height=675, scrollbars=no, resizable=no");
						});

						// 구독 신청
						$page.modal.find(".btn_add").click(function () {
							if ($page.modal.find(".name").val() == "") {
								alert("이름을 입력해주세요.");
								$page.modal.find(".name").focus();
								return false;
							}
							if ($page.modal.find(".phone").val() == "") {
								alert("연락처를 입력해주세요.");
								$page.modal.find(".phone").focus();
								return false;
							}

							if ($page.modal.find(".zipcode").val() == "") {
								alert("주소찾기를 이용해 우편번호를 검색해주세요.");
								$page.modal.find(".zipcode").focus();
								return false;
							}

							if ($page.modal.find(".addr1").val() == "") {
								alert("주소찾기를 이용해 우편번호를 검색해주세요.");
								$page.modal.find(".addr1").focus();
								return false;
							}

							if ($page.modal.find(".addr2").val() == "") {
								alert("상세주소를 입력해주세요.");
								$page.modal.find(".addr2").focus();
								return false;
							}

							var params = {
								name: $page.modal.find(".name").val(),
								phone: $page.modal.find(".phone").val(),
								zipcode: $page.modal.find(".zipcode").val(),
								addr1: $page.modal.find(".addr1").val(),
								addr2: $page.modal.find(".addr2").val(),
								hfAddressType: $("#hfAddressType").val()
							}
							$.post("/api/newsletter.ashx?t=add", params, function (r) {
								console.log(r);
								if (r.success) {
									alert("신청이 완료되었습니다.\n추가적인 문의사항이 있으시면 언제든 문의주시기 바랍니다. 후원자님의 귀한 섬김에 언제나 감사드립니다.");
									closeModal();
								} else {
									alert(r.message);
								}
							});
						});


					}, true, false);
				});
			} else {
				common.checkLogin();
			}
		});

		
	},

	callback: function () {

	}

}

function jusoCallback(zipNo, addr1, addr2) {
	$page.modal.find(".zipcode").val(zipNo);
	$page.modal.find(".addr1").val(addr1);
	$page.modal.find(".addr2").val(addr2);
}




