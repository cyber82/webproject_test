﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="about_us_notice_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    
	
    <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>연례<em>보고</em></h1>
				<span class="desc">한국컴패션의 일 년간의 사업활동에 대한 보고입니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents aboutus w980">

			<!-- 탭메뉴 -->
			<ul class="tab_type1 mb55">
				<li style="width:50%"><a href="/about-us/report/">사업성과보고서</a></li>
				<li style="width:50%" class="on"><a href="#">재무/감사보고</a></li>
			</ul>
			<!--//-->

			<div class="finance">
				<div class="con_top tac">
					<a href="http://www.ecfa.org/MemberProfile.aspx?ID=4466" class="logo_ecfa" target="_blank"><img src="/common/img/page/about-us/logo_ecfa.jpg" alt="ECFA Charter" /></a>
					<a href="https://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=3555" class="logo_charity" target="_blank"><img src="/common/img/page/about-us/logo_charity.jpg" alt="Charity Navigator" /></a>
					<a href="http://www.give.org/charity-reviews/national/child-sponsorship/compassion-international-in-colorado-springs-co-3017" class="logo_bbb" target="_blank"><img src="/common/img/page/about-us/logo_bbb.jpg" alt="BBB" /></a>
					<br />
					<span class="bar"></span>
					<p class="s_tit7 mb15">효율적이고 정직하게 지켜 나가겠습니다!</p>
					<p class="s_con3">
						컴패션은 후원금의 80% 이상을 반드시 어린이양육을 위해 사용한다는 원칙을 철저히 지킵니다.<br />
						후원금을 효율적이고 정직하게 사용하고자 주력하며 이를 위해 전 세계 컴패션의 각 수혜국과 후원국은 정기적으로 엄격한 내·외부 감사를 받고 있습니다.<br />
						이로써 컴패션은 ECFA(Evangelical Council for Financial Accountability), 채리티내비게이터(charitynavigator),<br />
						BBB(Better Business Bureau) 등 국제적인 평가기관에서 좋은 평가를 받고 있습니다.<br />
						채리티내비게이터는 정직성과 관련된 <em class="fc_blue">‘재정투명성과 책무성(Accountability and Transparency)’ 항목에서<br />
						가장 높은 점수인 100점으로 최고점</em>을 받았으며, <em class="fc_blue">전체 평가에서도 14년 연속 별 4개</em>를 획득했습니다.<br />
						<br />
						<em class="fc_black">80:20원칙은 정직하고 효율적인 후원금 사용을 위한 컴패션의 약속입니다.</em>
					</p>
				</div>

				<div class="box_type6">
					<img src="/common/img/page/about-us/report_img2.png" alt="KMPG 로고" />
					<p>한국컴패션은 재정의 건실함과 투명성을 유지하기 위하여 삼정회계법인(KPMG)으로부터 외부감사를 받으며 각 수혜국은 내·외부 감사를<br />실시하고 있습니다.</p>
				</div>

				<!-- 재무보고 리스트 -->
				<div class="fn_report">					
					<div class="tableWrap1">
						<table class="tbl_type1 mb40" id="l">
							<caption>재무보고 미리보기와 다운로드 테이블</caption>
							<colgroup>
								<col style="width:73%" />
								<col style="width:27%" />
							</colgroup>
							<tbody>
								<tr ng-repeat="item in list">
									<td>{{item.b_title}}</td>
									<td>
										<a href="#" ng-click="goView(item.b_id)" class="btn_s_type1 mr5"><span class="ic_look"></span>보고서 보기</a>
										<a href="{{item.b_file}}"  class="btn_s_type4" target="_blank"><span class="ic_down"></span>Download</a>
									</td>
								</tr>
								<tr ng-if="total == 0">
									<td colspan="5">데이터가 없습니다.</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!--// 재무보고 리스트 -->

				<!-- page navigation -->
				<div class="tac">
					<paging class="small" page="params.age" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
				</div>
				<!--// page navigation -->

			</div>
						
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->

    
</asp:Content>