﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default2.aspx.cs" Inherits="_default2" MasterPageFile="~/top.Master" %>

<%@ MasterType VirtualPath="~/top.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

	<meta name="keywords" content="한국컴패션,어린이후원"  />
	<meta name="description" content="꿈을 잃은 어린이들에게 그리스도의 사랑을" />
	<meta property="og:url" content="http://www.compassionkr.com" />
	<meta property="og:title" content="한국컴패션" />
	<meta property="og:description" content="꿈을 잃은 어린이들에게 그리스도의 사랑을" />
	<meta property="og:image" content="http://www.compassion.or.kr/common/img/sns_default_image.png" />

</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
    <script type="text/javascript" src="/default.js?v=1"></script>
    <script type="text/javascript" src="/default.sympathy.js"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/cookie.js"></script>
    <script type="text/javascript" src="/assets/jquery/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="/popup/popupManager.js"></script>
	<script type="text/javascript" src="/popup/custom/pwd-notice-check"></script>
    <script type="text/javascript" src="/popup/custom/info-change-check"></script>
    <script type="text/javascript">
        $(function () {
            if ($(".intro_visual").is(":visible")) {
                var total = $("#children").val();
                $({ count: 0 }).animate({ count: total }, {
                    duration: 2000,
                    easing: "swing",
                    step: function () {
                        return $("#children_count").text(Math.round(this.count));
                    },
                    complete: function () {
                        return $("#children_count").text(total);
                    }
                });
            }

        })
    </script>

    <!--NSmart Track Tag Script-->
		 <script type='text/javascript'>
            callbackFn = function() {};
            var _nsmart = _nsmart || [];
            _nsmart.host = _nsmart.host || (('https:' == document.location.protocol) ? 'https://' : 'http://');
            _nsmart.push([12490, 32022]); // 캠페인 번호와 페이지 번호를 배열 객체로 전달
            //document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            // document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            /* v 1.4 ---------------------------------*
            *  Nasmedia Track Object, version 1.4
            *  (c) 2009 nasmedia,mizcom (2009-12-24)
            *---------------------------------------*/
            var nasmedia = {};
            var NTrackObj = null;
            nasmedia.track = function(){
                this.camp, this.img = null;
            };
            nasmedia.track.prototype = {
                callTrackTag : function(p, f, c){
                    if(Object.prototype.toString.call(_nsmart) != '[object Array]' || _nsmart.host == undefined || _nsmart.host == '') return false;
                    if(f != undefined && typeof(f) == 'function') {
                        this.img.onload = this.img.onerror = this.img.onabort = f;
                        var no = ((parseInt(c, 10) % 100) < 10 ? '0' + (parseInt(c, 10) % 100): (parseInt(c, 10) % 100));
                        this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + c + '&page=' + p + '&r=' + Math.random();
                    }
                    else this.callNext(0);
                    return false;
                },
                callNext : function(i) {
                    if(i == this.camp.length) return false;
                    this.img.onload = this.img.onerror = this.img.onabort = function(o, i){return function(){o.callNext(i);};}(this, i+1);
                    var no = ((parseInt(this.camp[i][0], 10) % 100) < 10 ? '0' + (parseInt(this.camp[i][0], 10) % 100): (parseInt(this.camp[i][0], 10) % 100));
                    this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + this.camp[i][0] + '&page=' + this.camp[i][1] + '&r=' + Math.random();
                },
                init : function(c) {
                    this.camp = c, this.img = new Image;
                }
            };
            (function(){
                NTrackObj = new nasmedia.track();
            })();
             NTrackObj.init(_nsmart);
             NTrackObj.callTrackTag();
		</script>
<!--NSmart Track Tag Script End..-->
</asp:Content>

<asp:Content runat="server" ID="header_top" ContentPlaceHolderID="header_top">
<!-- intro -->
    <section class="intro_visual" style="display: none">
        <input type="hidden" id="children" runat="server" value="0" />
        
        <div class="visual_img"></div>
        <span class="logo">
            <img src="/common/img/common/logo_1.png" alt="Compassion" /></span>
            <script>
                function getHost() {
                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
                    else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';
                    return host;
                }

                function goLink() {
                    var host = getHost();
                    //if (host != '') location.href = 'http://' + host + '/sponsor/children/';//?e_id=2&e_src=WEB_intro&utm_source=WEB_intro&utm_campaign=20170401_easter&utm_medium=WEB_intro';
                    if (host != '') location.href = 'http://' + host + '/flower/?e_id=13&e_src=2017flower_WEB_intro&utm_source=WEB_intro&utm_campaign=2017flower​&utm_medium=WEB';//';
                }
            </script>
            <a style="left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute;" href="javascript:goLink()" onclick="javascript:NTrackObj.callTrackTag('32023', callbackFn, 12490);"></a>
        <%--<span class="intro_count">
            <div id="children_count" class="countNum">0</div>
            
                                <span class="txt">
                <asp:Literal runat="server" ID="main_content"></asp:Literal></span><br />
            <br />

            <span class="btn_go"><a href="/sponsor/children/" class="btn_b_type2">어린이 만나러 가기</a></span>
        </span>--%>
        <span class="scroll">
            <img src="/common/img/main/scroll.png" alt="scroll down" /></span>
    </section>
    <!--// intro -->



</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">



    <!-- main -->
    <section class="main">

        <!-- 후원 중인 어린이 정보 -->
        <asp:PlaceHolder runat="server" ID="ph_sec1" Visible="false">
            <div class="main_recmd member">
                <div class="wrap" style="background: url('/common/img/temp/visual_2.jpg') no-repeat center top">
                    <div class="w980 relative">
                        <div class="picWrap">
                            <!-- 사진사이즈 : 242 * 333 -->
                            <div class="pic"><span>
                                <img runat="server" id="childPic" alt="후원어린이" width="242" /></span></div>
                        </div>

                        <div class="textWrap">
                            <span class="title"><em>
                                <asp:Literal runat="server" ID="userName" /></em> 후원자님, 안녕하세요!</span><br />
                            <p class="con">
                                후원자님의 소식을 <em>
                                    <asp:Literal runat="server" ID="childName" /></em> 어린이와 함께<br />
                                나누어 보시겠어요?
                            </p>
                            <div>
                                <a href="/my/letter/write/?c={childmasterid}" runat="server" id="btn_write_letter" class="btn_more3 mr8">편지쓰기</a>
                                <a href="/my/children/view/{childkey}/{childmasterid}?album=Y" runat="server" id="btn_album" class="btn_more4">어린이 성장앨범 보기</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <!--// 후원 중인 어린이 정보 -->

        <!-- 기도나눔 -->
        <asp:PlaceHolder runat="server" ID="ph_sec2" Visible="false">
            <div class="main_prayer" runat="server" id="pn_prayer2">
                <div class="w980">
                    <div class="prayer_tit">
                        <span>기도나눔</span>
                    </div>
                    <div class="prayer_txt">
                        <p runat="server" id="prayer_content2" class="pray">
                        </p>

                        <asp:PlaceHolder ID="prayer_check2" Visible="false" runat="server">
							<div class="pb30">
								<span class="icon">후원 어린이에게 편지 쓰는 날</span>
								<a href="/my/letter/write/" class="btn_go">바로가기</a>
							</div>
                        </asp:PlaceHolder>
                    </div>
                    <div class="prayer_day">
                        <span runat="server" id="prayer_date2"></span>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <!--// 기도나눔 -->

        <!-- 어린이 추천 -->
        <asp:PlaceHolder runat="server" ID="ph_sec3" Visible="false">

            <!-- 생일이 어제인 -->
            <div class="main_recmd" id="main_recmd1" runat="server" visible="false">
                <div class="wrap" style="background: url('/common/img/temp/visual_1.jpg') no-repeat center top">
                    <div class="w980 relative">
                        <div class="picWrap">
                            <!-- 사진사이즈 : 242 * 333 -->
                            <div class="pic"><span>
                                <img id="main_recmd1_pic" runat="server" width="242" alt="추천어린이" /></span></div>
                            <div class="info">
                                <span class="nation">
                                    <asp:Literal runat="server" ID="main_recmd1_country" /></span>
                                <span class="age">
                                    <asp:Literal runat="server" ID="main_recmd1_age" />세</span>
                                <span class="sex">
                                    <asp:Literal runat="server" ID="main_recmd1_gender" /></span>
                            </div>
                        </div>

                        <div class="textWrap">
                            <span class="label">1:1어린이양육</span><br />
                            <span class="title">어제는 <em>저의 생일</em>이었어요</span><br />
                            <p class="con">
                                안녕하세요,
                                <asp:Literal runat="server" ID="main_recmd1_country2" />에 사는
                                <asp:Literal runat="server" ID="main_recmd1_name" />입니다.<br />
                                저는 오늘로
                                <asp:Literal runat="server" ID="main_recmd1_waitingdays" />일째 후원자님을 기다리고 있어요. 지난 생일은 후원자님과<br />
                                함께하지 못했지만,
                                <asp:Literal runat="server" ID="main_recmd1_age2" />살이 된 오늘은 함께할 수 있을까요?<br />
                            </p>
                            <div>
                                <a href="/sponsor/children/?c={childMasterId}" runat="server" id="main_recmd1_link" class="btn_more1 mr8">
                                    <asp:Literal runat="server" ID="main_recmd1_name2" />에 대해 자세히 알고 싶어요</a>
                                <a href="/sponsor/children/" class="btn_more2">다른 어린이도 만나볼래요</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 부모님이 안계신 -->
            <div class="main_recmd" id="main_recmd2" runat="server" visible="false">
                <div class="wrap" style="background: url('/common/img/temp/visual_1.jpg') no-repeat center top">
                    <div class="w980 relative">
                        <div class="picWrap">
                            <!-- 사진사이즈 : 242 * 333 -->
                            <div class="pic"><span>
                                <img id="main_recmd2_pic" runat="server" width="242" alt="추천어린이" /></span></div>
                            <div class="info">
                                <span class="nation">
                                    <asp:Literal runat="server" ID="main_recmd2_country" /></span>
                                <span class="age">
                                    <asp:Literal runat="server" ID="main_recmd2_age" />세</span>
                                <span class="sex">
                                    <asp:Literal runat="server" ID="main_recmd2_gender" /></span>
                            </div>
                        </div>

                        <div class="textWrap">
                            <span class="label">1:1어린이양육</span><br />
                            <span class="title">저의 <em>부모님</em>이 되어 주세요</span><br />
                            <p class="con">
                                안녕하세요,
                                <asp:Literal runat="server" ID="main_recmd2_country2" />에 사는
                                <asp:Literal runat="server" ID="main_recmd2_name" />입니다.<br />
                                저는 오늘로
                                <asp:Literal runat="server" ID="main_recmd2_waitingdays" />일째 후원자님을 기다리고 있어요.<br />
                                저희 엄마 아빠는 너무 일찍 먼 여행을 가셨지만,<br />
                                하나님께서 후원자님을 통해 그 자리를 채워주실 거라 믿어요.<br />
                                저의 부모님이 되어 주실래요?
							<br />
                            </p>
                            <div>
                                <a href="/sponsor/children/?c={childMasterId}" runat="server" id="main_recmd2_link" class="btn_more1 mr8">
                                    <asp:Literal runat="server" ID="main_recmd2_name2" />에 대해 자세히 알고 싶어요</a>
                                <a href="/sponsor/children/" class="btn_more2">다른 어린이도 만나볼래요</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 랜덤 -->
            <div class="main_recmd" id="main_recmd3" runat="server" visible="false">
                <div class="wrap" style="background: url('/common/img/temp/visual_1.jpg') no-repeat center top">
                    <div class="w980 relative">
                        <div class="picWrap">
                            <!-- 사진사이즈 : 242 * 333 -->
                            <div class="pic"><span>
                                <img id="main_recmd3_pic" runat="server" width="242" alt="추천어린이" /></span></div>
                            <div class="info">
                                <span class="nation">
                                    <asp:Literal runat="server" ID="main_recmd3_country" /></span>
                                <span class="age">
                                    <asp:Literal runat="server" ID="main_recmd3_age" />세</span>
                                <span class="sex">
                                    <asp:Literal runat="server" ID="main_recmd3_gender" /></span>
                            </div>
                        </div>

                        <div class="textWrap">
                            <span class="label">1:1어린이양육</span><br />
                            <span class="title">내일을 꿈꾸고 싶어요</span><br />
                            <p class="con">
                                안녕하세요,
                                <asp:Literal runat="server" ID="main_recmd3_country2" />에 사는
                                <asp:Literal runat="server" ID="main_recmd3_name" />입니다.<br />
                                저는 오늘로
                                <asp:Literal runat="server" ID="main_recmd3_waitingdays" />일째 후원자님을 기다리고 있어요.<br />
                                아직 후원자님을 만나지는 못했지만,<br />
                                얼른 후원자님을 만나 편지도 주고 받고 학교도 다니고 싶어요!<br />
                                즐거운 내일을 꿈꾸며 잠드는 밤, 저에게도 찾아 올까요?
							<br />
                            </p>
                            <div>
                                <a href="/sponsor/children/?c={childMasterId}" runat="server" id="main_recmd3_link" class="btn_more1 mr8">
                                    <asp:Literal runat="server" ID="main_recmd3_name2" />에 대해 자세히 알고 싶어요</a>
                                <a href="/sponsor/children/" class="btn_more2">다른 어린이도 만나볼래요</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <!--// 어린이 추천 -->

        <!-- 후원 탭 -->
        <asp:PlaceHolder runat="server" ID="ph_sec4" Visible="false">
            <div class="main_program">
                <div class="tab">
                    <div class="w980 relative">
                        <ul class="mainPrgramList">
                            <li data-prm-id="1" class="on"><a href="#" class="prm1"><span>1:1어린이양육</span></a></li>
                            <li data-prm-id="2"><a href="#" class="prm2"><span>특별한 나눔</span></a></li>
                            <li data-prm-id="3"><a href="#" class="prm3"><span>나눔펀딩</span></a></li>
                        </ul>
                    </div>
                </div>

                <div class="w980">
                    <!-- 일대일어린이양육 -->
                    <div class="prm_con prm_con1">
                        <p class="desc">
                            1:1어린이양육은 한 어린이의 인생에 희망을 줄 수 있는 사랑의 시작입니다.<br />
                            키움과 돌봄의 감동적인 1:1만남, 지금 시작하세요
                        </p>

                        <!-- 박스 -->
                        <div class="boxWrap n3">

                            <asp:Repeater runat="server" ID="repeater_children">
                                <ItemTemplate>
                                    <div class="box">
                                        <div class="snsWrap">
                                            <span class="day"><%#Eval("waitingdays")%>일</span>
                                            <span class="sns_ani">
                                                <span class="common_sns_group">
                                                    <span class="wrap">
                                                        <a href="#" title="페이스북" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/children/?c=" + Eval("childmasterId") %>" data-provider="fb" class="sns_facebook">페이스북</a>
                                                        <a href="#" title="카카오스토리" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/children/?c=" + Eval("childmasterId") %>" data-provider="ks" class="sns_story">카카오스토리</a>
                                                        <a href="#" title="트위터" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/children/?c=" + Eval("childmasterId") %>" data-title="<%#"1:1어린이양육-" + Eval("namekr") %>" data-provider="tw" class="sns_twitter">트위터</a>
                                                        <a href="#" title="url 공유" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/children/?c=" + Eval("childmasterId") %>" data-provider="copy" class="sns_url">url 공유</a>
                                                    </span>
                                                </span>
                                                <button class="common_sns_share">공유하기</button>
                                            </span>
                                        </div>

                                        <div class="child_info">
                                            <span class="pic" style="background: url('<%#Eval("pic")%>') no-repeat center top;background-size:cover;">양육어린이사진</span>
                                            <span class="name"><%#Eval("name")%></span>
                                            <p class="info">
                                                <span>국가 : <%#Eval("countryName")%></span><br />
                                                <span class="bar">생일 : <%#Eval("birthDate" , "{0:yyyy.MM.dd}")%> (<%#Eval("age")%>세)</span><span>성별 : <%#Eval("gender")%></span>
                                            </p>
                                        </div>

                                        <div class="more"><a href="/sponsor/children/?c=<%#Eval("childmasterid") %>">더 알아보기</a></div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>

                        </div>
                        <!--// 박스 -->

                        <div class="mtm clear2">
                            <span class="tit">더욱 특별한 어린이를 만나보세요!</span>
                            <ul class="list">
                                <li><a href="/sponsor/children/?orderby=waiting" class="btn1">오래 기다린 어린이</a></li>
                                <li><a href="/sponsor/children/?specialNeed=Y" class="btn2">몸이 불편한 어린이</a></li>
                                <li><a href="/sponsor/children/?orphan=Y" class="btn3">부모님이 안 계시는 어린이</a></li>
                                <li><a href="/sponsor/children/" class="btn4">1:1어린이양육 자세히 보기</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--// -->

                    <!-- 특별한나눔 -->
                    <div class="prm_con prm_con2" style="display: none;">
                        <p class="desc">
                            특별한 나눔은 긴급구호, 학교 건축, 깨끗한 식수 지원, 직업교육, 의료지원, 엄마와 아이 지원 등<br />
                            원하는 테마를 선택해 후원할 수 있는 프로그램입니다.
                        </p>

                        <!-- 박스 -->
                        <div class="boxWrap n2">
                            <asp:Repeater runat="server" ID="repeater_special_funding">
                                <ItemTemplate>

                                    <div class="box <%#Container.ItemIndex % 2 == 0 ? "fl" : "fr" %>">
                                        <div class="img" style="background: url('<%# Eval("sf_image").ToString()%>') no-repeat center top;"></div>
                                        <div class="bg_dim"></div>
                                        <div class="snsWrap">
                                            <span class="d_day"><%#Eval("enddate") != null ? "D-" + Convert.ToInt32(Convert.ToDateTime(Eval("EndDate")).Subtract(DateTime.Now).TotalDays).ToString() : "" %></span>
                                            <span class="sns_ani">
                                                <span class="common_sns_group">
                                                    <span class="wrap">
                                                        <a href="#" title="페이스북" data-role="sns" data-provider="fb" data-url="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="sns_facebook">페이스북</a>
                                                        <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" data-url="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="sns_story">카카오스토리</a>
                                                        <a href="#" title="트위터" data-role="sns" data-provider="tw" data-title="특별한후원-<%#Eval("campaignName") %>" data-url="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="sns_twitter">트위터</a>
                                                        <a href="#" title="url 공유" data-role="sns" data-provider="copy" data-url="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="sns-copy sns_url">url 공유</a>
                                                    </span>
                                                </span>
                                                <button class="common_sns_share">공유하기</button>
                                            </span>
                                        </div>
                                        <a href="<%#Request.Domain() + "/sponsor/special/view/" + Eval("campaignId").ToString() %>" class="tit"><%# Eval("campaignname").ToString()%></a>

                                        <!-- graph -->
                                        <div class="graph_wrap" style="display: <%# Convert.ToInt32(Eval("sf_goal_amount")) < 1? "none" : "block" %>">
                                            <span class="heart" style="left: <%# Convert.ToInt32(Eval("sf_current_amount")).ToPercent( Convert.ToInt32(Eval("sf_goal_amount")) )%>%">
                                                <span class="txt"><em><%# Convert.ToInt32(Eval("sf_current_amount")).ToPercent( Convert.ToInt32(Eval("sf_goal_amount")) )%>%</em> 달성중</span>
                                            </span>
                                            <div class="graph">
                                                <span class="bg"></span>
                                                <span class="per" style="width: <%# Convert.ToInt32(Eval("sf_current_amount")).ToPercent( Convert.ToInt32(Eval("sf_goal_amount")) )%>%"></span>
                                            </div>
                                        </div>
                                        <!--// -->
                                    </div>

                                </ItemTemplate>
                            </asp:Repeater>

                        </div>
                        <!--// 박스 -->

                        <div class="special clear2">
                            <span class="tit">더욱 다양한 나눔을 만나보세요</span>
                            <ul class="list">
                                <li><a href="/sponsor/special/first-birthday/" class="btn1">첫 생일 첫 나눔</a></li>
                                <li><a href="/sponsor/special/wedding/" class="btn2">결혼 첫 나눔</a></li>
                                <li><a href="/sponsor/special/fixed/legacy/" class="btn3">믿음의 유산</a></li>
                                <li><a href="/sponsor/special/fixed/NK/" class="btn4">북한사역 후원</a></li>
                                <li><a href="/sponsor/special/" class="btn5">전체 보기</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--// -->

                    <!-- 나눔펀딩 -->
                    <div class="prm_con prm_con3" style="display: none;">
                        <p class="desc">
                            나눔펀딩은 작은 손길을 모아 큰 사랑을 실천할 수 있는<br />
                            컴패션만의 후원 펀딩 프로그램입니다.
                        </p>

                        <!-- 박스 -->
                        <div class="boxWrap n2">

                            <asp:Repeater runat="server" ID="repeater_user_funding">
                                <ItemTemplate>
                                    <div class="box <%#Container.ItemIndex % 2 == 0 ? "fl" : "fr" %>">
                                        <div class="img" style="background: url('<%# Eval("uf_image").ToString()%>') no-repeat center; background-size: cover;"></div>
                                        <div class="bg_dim"></div>
                                        <div class="snsWrap">
                                            <span class="d_day">D-<%#Convert.ToInt32(Convert.ToDateTime(Eval("uf_date_end")).Subtract(DateTime.Now).TotalDays).ToString() %></span>
                                            <span class="sns_ani">
                                                <span class="common_sns_group">
                                                    <span class="wrap">
                                                        <a href="#" title="페이스북" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/user-funding/view/" + Eval("uf_id") %>" data-provider="fb" class="sns_facebook">페이스북</a>
                                                        <a href="#" title="카카오스토리" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/user-funding/view/" + Eval("uf_id") %>" data-provider="ks" class="sns_story">카카오스토리</a>
                                                        <a href="#" title="트위터" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/user-funding/view/" + Eval("uf_id") %>" data-title="<%#"나눔펀딩-" + Eval("uf_title") %>" data-provider="tw" class="sns_twitter">트위터</a>
                                                        <a href="#" title="url 공유" data-role="sns" data-url="<%#Request.Domain() + "/sponsor/user-funding/view/" + Eval("uf_id") %>" data-provider="copy" class="sns_url">url 공유</a>
                                                    </span>
                                                </span>
                                                <button class="common_sns_share">공유하기</button>
                                            </span>
                                        </div>
                                        <a href="<%#Request.Domain() + "/sponsor/user-funding/view/" + Eval("uf_id") %>" class="tit"><%# Eval("uf_title").ToString()%></a>

                                        <!-- graph -->
                                        <div class="graph_wrap">
                                            <span class="heart" style="left: <%# Convert.ToInt32(Eval("uf_current_amount")).ToPercent( Convert.ToInt32(Eval("uf_goal_amount")) )%>%">
                                                <span class="txt"><em><%# Convert.ToInt32(Eval("uf_current_amount")).ToPercent( Convert.ToInt32(Eval("uf_goal_amount")) )%>%</em> 달성<%# Convert.ToInt32(Eval("uf_current_amount")).ToPercent( Convert.ToInt32(Eval("uf_goal_amount"))) == 100  ? "" : "중" %></span>
                                            </span>
                                            <div class="graph">
                                                <span class="bg"></span>
                                                <span class="per" style="width: <%# Convert.ToInt32(Eval("uf_current_amount")).ToPercent( Convert.ToInt32(Eval("uf_goal_amount")) )%>%"></span>
                                            </div>
                                        </div>
                                        <!--// -->
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>


                        </div>
                        <!--// 박스 -->

                        <div class="funding clear2">
                            <span class="tit">어떤 펀딩에 참여하고 싶으신가요?</span>
                            <ul class="list">
                                <li><a href="/sponsor/user-funding/?type=child" class="btn1">결연이 끊긴 어린이를 돕고 싶어요</a></li>
                                <li><a href="/sponsor/user-funding/?type=normal" class="btn2">다양한 스토리를 보고 돕고 싶어요</a></li>
                                <li><a href="/sponsor/user-funding/" class="btn3">전체 펀딩 보기</a></li>
                            </ul>
                        </div>

                    </div>
                    <!--// -->
                </div>
            </div>
        </asp:PlaceHolder>
        <!--// 후원 탭 -->

        <!-- 캠페인 배너 3개 롤링 영역 -->
        <asp:PlaceHolder runat="server" ID="ph_sec5" Visible="false">
            <div class="main_roll_banner" id="event_container">
                <!-- sliding set -->
                <div class="bnr_list" id="event_visual">

                    <asp:Repeater runat="server" ID="repeater_event">
                        <ItemTemplate>

                            <a href="<%#Eval("mp_image_link") %>" target="<%#Eval("mp_is_new_window").ToString() == "True" ? "_blank" : "_self" %>" onclick="javascript:NTrackObj.callTrackTag('32024', callbackFn, 12490);">
                                <div class="bnr_list item" style="background: url('<%#Eval("mp_image").ToString().WithFileServerHost()%>') no-repeat center top; position: absolute"></div>
                            </a>

                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <div class="btn_group" style="z-index: 10">
                    <div class="indi_wrap"></div>
                    <button class="prev">
                        <img src="/common/img/btn/prev_1.png" alt="previous" /></button>
                    <button class="next">
                        <img src="/common/img/btn/next_1.png" alt="next" /></button>
                </div>

            </div>
        </asp:PlaceHolder>
        <!--// 캠페인 배너 3개 롤링 영역 -->

        <!-- 후원추천 -->
        <asp:PlaceHolder runat="server" ID="ph_sec6" Visible="false">
            <div class="main_banner">
                <div class="w980">
                    <div class="tit">
                        <p><em>후원</em> 추천</p>
                        <span class="bar"></span>
                    </div>

                    <div class="bnr_wrap clear2">
                        <ul>
                            <li class="bnr1"><a href="/advocate/together/compassion-a-day/"><span>Compassion A Day</span><br />
                                후원을 함께하고 싶은 분께 마음을 전하세요</a></li>
                            <li class="bnr2"><a href="/sympathy/bluebook"><span>컴패션블루북</span><br />
                                어린이를 만나며 겪은 아름다운<br />
                                변화를 나눠 주세요</a></li>
                            <li class="bnr3"><a href="/sponsor/user-funding/"><span>나눔펀딩</span><br />
                                후원 펀딩을 만들어 보세요</a></li>
                            <li class="bnr4"><a href="/advocate/together/diy/"><span>Donate It Your<br />
                                Way</span><br />
                                후원자님만의 특별한 컴패션을<br />
                                보여 주세요</a></li>
                            <li class="bnr5"><a href="/advocate/together/notify"><span>컴패션 천만 명에게<br />
                                알리기</span><br />
                                감동적인 스토리로 소중한 사람들에게<br />
                                컴패션을 소개해 보세요</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <!--// 후원추천 -->

        <!-- 꿈을 잃은 어린이들에게 그리스도의 사랑을 -->
        <asp:PlaceHolder runat="server" ID="ph_sec7" Visible="false">
            <div class="main_welove">
                <div class="w980 relative">
                    <p class="tit">꿈을 잃은 어린이들에게 그리스도의 사랑을</p>
                    <div class="list love1">
                        <p class="sub_tit">WE LOVE JESUS</p>
                        <p class="con">
                            컴패션은 예수님이 맡겨주신 사명에 따라,<br />
                            그 분의 사랑을 바탕으로<br />
                            어린이들을 가난으로부터 자유롭게 합니다.
                        </p>
                        <p class="con">
                            가난으로 인해 꺼져가는<br />
                            한 생명을 살리는 위대한 일은<br />
                            예수님의 사랑으로부터 시작됩니다.
                        </p>
                    </div>
                    <div class="list love2">
                        <p class="sub_tit">WE LOVE CHILDREN</p>
                        <p class="con">
                            컴패션의 모든 사역은<br />
                            어린이를 중심으로 진행됩니다.
                        </p>
                        <p class="con">
                            엄마 뱃속에서부터 자립 가능한 성인이 될 때까지,<br />
                            한 어린이와 끝까지 함께합니다.<br />
                            희망을 붙잡고 일어선 어린이들을 통해<br />
                            가정과 지역 사회가 변화될 것입니다.
                        </p>
                    </div>
                    <div class="list love3">
                        <p class="sub_tit">WE LOVE THE CHURCH</p>
                        <p class="con">
                            컴패션은 교회를 통해 일합니다.<br />
                            25개 수혜국들의 현지 협력교회와 파트너십을 맺고 현지<br />
                            교회가 어린이들을 직접 양육할 수 있도록 돕습니다.
                        </p>
                        <p class="con">
                            어린이들은 교회를 통해<br />
                            전인적인 혜택을 받을 뿐만 아니라<br />
                            조건 없는 사랑을 배워갑니다.
                        </p>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <!--// 꿈을 잃은 어린이들에게 그리스도의 사랑을 -->

        <!-- 컴패션후원금은 이렇게 사용됩니다. -->
        <asp:PlaceHolder runat="server" ID="ph_sec8" Visible="false">
            <div class="main_clear">
                <div class="w980">
				<div class="clear2">
					<div class="img">
						<img src="/common/img/main/clear_img_1.png" class="inline" alt="87.5%" />
						<p class="txt1">
							어린이 양육프로그램(87.5%)<br />
							보조활동비(12.5%)
						</p>
					</div>
					<div class="con">
						<p class="tit"><em>컴패션 후원금</em>은 이렇게 <em>사용</em>됩니다!</p>
						<p class="txt1"><em>정직하고 효율적인</em> 후원금 사용을 위한 <em>80:20 원칙</em></p>
						<p class="txt2">
							컴패션은 후원금의 80% 이상을 반드시 어린이양육을 위해 사용한다는 원칙을 철저히 지킴으로<br />
							후원금을 효율적이고 정직하게 사용하고자 합니다. 이를 지키기 위해 전 세계 컴패션의 각<br />
							수혜국과 후원국은 <em>정기적으로 엄격한 내·외부 감사</em>를 받고 있습니다.
						</p>
						<a href="/customer/faq/?k_word=후원금" class="btn_b_type3">후원금 관련 FAQ</a>
					</div>
				</div>
			</div>
            </div>
        </asp:PlaceHolder>
        <!--// 컴패션후원금은 이렇게 사용됩니다. -->

        <!-- 후원이 가져온 변화 -->
        <asp:PlaceHolder runat="server" ID="ph_sec9" Visible="false">
            <div class="main_story">

                <p class="tit"><em>후원이 가져온 변화</em>를 느껴 보세요</p>

                <div class="slide_story_wrap" id="sympathy_container" style="overflow: hidden">
                    <div class="w980 relative">
                        <!-- set -->
                        <div style="position: relative; width: 1720px; height: 446px; margin-left: -370px" id="sympathy_visual">
                            <asp:Repeater runat="server" ID="repeater_sympathy">
                                <ItemTemplate>

                                    <div class="list" style="width: 430px; position: absolute">
                                        <div class="conBox">
                                            <!-- 사진 사이즈 : 396 * 262 -->
                                            <div class="img" style="background: url('<%#Eval("thumb") == null ? "" : Eval("thumb").ToString().WithFileServerHost() %>'); background-size: cover;"></div>
                                            <div class="txt">
                                                <p class="title elps"><%#Eval("title") %></p>
                                                <p class="content elps"><%#Eval("sub_title") %></p>
                                                <p class="category"><%#Eval("board_name") %></p>
                                            </div>
                                            <a href="/sympathy/view/<%#Eval("board_id") %>/<%#Eval("idx") %>?page=1&rowsPerPage=9&s_type=<%#Eval("board_id") %>" class="over"><span class="btn_view1">VIEW</span></a>
                                        </div>
                                    </div>

                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div class="btn" style="z-index: 10; margin-top: -446px">
                        <button class="btn_com_prev1" id="sympathy_visual_left">이전</button>
                        <button class="btn_com_next1" id="sympathy_visual_right">다음</button>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <!--// 후원이 가져온 변화 -->

        <!-- 컴패션은 국제어린이양육기구입니다 -->
        <asp:PlaceHolder runat="server" ID="ph_sec10" Visible="false">
            <div class="main_copy">
                <div class="w980">
                    <p class="txt1">
                        <em>후원금은 양육비입니다.<br />
                            컴패션은 한 어린이가 스스로 독립할 수 있을 때까지 함께합니다.</em>
                    </p>

                    <a href="/sponsor/children/" class="btn_b_type2">1:1어린이양육 시작하기</a>
                </div>
            </div>
        </asp:PlaceHolder>
        <!--// 컴패션은 국제어린이양육기구입니다 -->

        <!-- 컴패션 블루코너 -->
        <asp:PlaceHolder runat="server" ID="ph_sec13" Visible="false">
            <div class="main_blueCorner">
                <div class="w980">
                    <span class="id_blue">
                        <img src="/common/img/common/identity_blue.png" alt="컴패션 블루코너 identity 아이콘" /></span>
                    <p class="tit">컴패션 블루코너</p>
                    <p class="con">
                        너희 땅의 곡물을 벨 때에 밭 모퉁이까지 다 베지 말며 떨어진 것을 줍지 말고 그것을 가난한 자와 거류민을 위하여 남겨두라 나는 너희의 하나님 여호와이니라
					<br />
                        <span class="txt">레위기 23:22</span>
                    </p>
                    <a href="/about-us/about/ci#comBlueCorner" class="btn_b_type2">자세히 보기</a>
                </div>
            </div>
        </asp:PlaceHolder>
        <!-- 컴패션 블루코너 -->

        <!-- 기도나눔 -->
        <asp:PlaceHolder runat="server" ID="ph_sec11" Visible="false">
            <div class="main_prayer" runat="server" id="pn_prayer">
                <div class="w980">
                    <div class="prayer_tit">
                        <span>기도나눔</span>
                    </div>
                    <div class="prayer_txt">
                        <p runat="server" id="prayer_content" class="pray">
                        </p>
                        <asp:PlaceHolder ID="prayer_check" Visible="false" runat="server">
							<div class="pb30">
								<span class="icon">후원 어린이에게 편지 쓰는 날</span>
								<a href="/my/letter/write/" class="btn_go">바로가기</a>
							</div>
                        </asp:PlaceHolder>
                    </div>
                    <div class="prayer_day">
                        <span runat="server" id="prayer_date"></span>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <!--// 기도나눔 -->

        <!-- 꿈을 잃은 어린이들에게 그리스도의 사랑을 -->
        <asp:PlaceHolder runat="server" ID="ph_sec12" Visible="false"></asp:PlaceHolder>
        <!--// 꿈을 잃은 어린이들에게 그리스도의 사랑을 -->

    </section>
    <!--// main -->

</asp:Content>

