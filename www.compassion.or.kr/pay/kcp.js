﻿

// 결제 중 새로고침 방지 샘플 스크립트
function noRefresh() {
	/* CTRL + N키 막음. */
	if ((event.keyCode == 78) && (event.ctrlKey == true)) {
		event.keyCode = 0;
		return false;
	}
	/* F5 번키 막음. */
	if (event.keyCode == 116) {
		event.keyCode = 0;
		return false;
	}
}
document.onkeydown = noRefresh;

function EventSet() {
	event.keyCode = 0;
	event.cancelBubble = true;
	event.returnValue = false;
}

/* 2. 플러그인 체크 함수 변경 플러그인 설치(확인) */
kcpTx_install();

/* Payplus Plug-in 실행 */
function jsf__pay() {
	try {

		var form = document.ex_frm;
		KCP_Pay_Execute(form);
		
	}catch (e) {
		/* IE 에서 결제 정상종료시 throw로 스크립트 종료 */
	}
}


function showSetupMsg() {
	$("#display_setup_message").show();
}


$(function () {

	//if ($("#noname").length > 0 && $("#noname").prop("checked")) {
	//	$("#user_name").val("무기명");
	//}
    //
    //$("#buyr_name").val($("#user_name").val());

    //[jun.heo] 2017-12 : 결제 모듈 통합 - 해외결제 카드인 경우 주문번호 생성(계좌관리용)
    if ($("#payPageType").val() == "PayAccount") {
        var today = new Date();
        var year = today.getFullYear();
        var month = today.getMonth() + 1;
        var date = today.getDate();
        var time = today.getHours() + "" + today.getMinutes() + "" + today.getSeconds() + "" + today.getMilliseconds(); // 밀리초 가져오기

        if (parseInt(month) < 10) {
            month = "0" + month;
        }

        var order_idxx = year + "" + month + "" + date + "" + time;

        $("#ordr_idxx").val(order_idxx);
    }

	setTimeout("jsf__pay();", 300);
})