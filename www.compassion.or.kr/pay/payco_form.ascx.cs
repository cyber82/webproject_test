﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using CommonLib;

public partial class pay_payco_form : System.Web.UI.UserControl {
	
	
	public void Show( StateBag state) {

		this.ViewState["month"] = state["month"] == null ? "" : state["month"];     // 선물금결제시 사용
		this.ViewState["returnUrl"] = state["returnUrl"];
		this.ViewState["jumin"] = state["jumin"];
		this.ViewState["ci"] = state["ci"];
		this.ViewState["di"] = state["di"];
		this.ViewState["good_name"] = state["good_name"];
        this.ViewState["good_mny"] = state["good_mny"];
        this.ViewState["user_name"] = state["buyr_name"] == null ? "" : state["buyr_name"];

        this.ViewState["payItem"] = state["payItem"];
        this.ViewState["firstPay"] = state["firstPay"];

        this.ViewState["payPageType"] = state["payPageType"];   //[jun.heo] 2017-12 : 결제 모듈 통합

        string userName = "", mobile = "";
        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            userName = sess.UserName;
            mobile = sess.Mobile;
        }

        var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 페이지별 처리
        switch (state["payPageType"].ToString())
        {

            case "PayDelay":    // 지연된 후원금
                //var payInfo = payItem.data; // payInfo 의 구조는 후원결제에서 사용하는 포맷과 다르다. dataset
                //this.ViewState["payInfo"] = payInfo;
                break;
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 스토어에서 Kakaopay, Payco 사용하지 않음.
            //case "Store":   // 스토어
            //    var oi = state["order_item"].ToString().ToObject<StoreAction.orderItem>();
            //    CustomerOrderNumber.Value = oi.orderNo;

            //    productName.Value = state["good_name"].ToString();
            //    amount.Value = state["good_mny"].ToString();
            //    break;
            case "Temporary":   // 일시후원 : 특별한 모금 및 나머지
            default:
                var payInfo = payItem.data.ToObject<PayItemSession.Entity>();
                payInfo.extra = new Dictionary<string, object>() {
                    {"jumin" , state["jumin"].ToString() } ,
                    {"ci" , state["ci"].ToString() },
                    {"di" , state["di"].ToString() },
                    {"buyr_name" , state["buyr_name"].ToString() },
                    {"userName" , userName },

                }.ToJson().Encrypt();

                payItem = new PayItemSession.Store().Update(payItem.orderId, payInfo, null);
                break;
        }
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 페이지별 처리

		//	this.ViewState["payInfo"] = payInfo;
		this.ViewState["orderId"] = payItem.orderId;

        ph_content.Visible = true;


        //Pay Log 생성 (결제모듈 요청)
        var sponsorId = "";
        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
        }
        string codeId = "", group = "";
        var payInfo2 = payItem.data.ToObject<PayItemSession.Entity>();
        if (payInfo2 != null)
        {
            codeId = payInfo2.codeId.EmptyIfNull();
            group = payInfo2.group.EmptyIfNull();
        }
        string strLog = string.Empty;
        strLog += "sponsorId:" + sponsorId + ", ";
        strLog += "orderId:" + payItem.orderId + ", ";
        strLog += "good_mny:" + state["good_mny"] + ", ";
        strLog += "codeId:" + codeId + ", ";
        strLog += "group:" + group + ", ";
        strLog += "buyr_name:" + state["buyr_name"] + ", ";
        strLog += "userName:" + userName + ", ";
        strLog += "mobile:" + mobile + ", ";

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 로그 생성
        //-----------------------------------------------------------------------------
        // USER-AGENT 구분
        //-----------------------------------------------------------------------------
        string WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
        {
            WebMode = "MOBILE";
        }
        else
        {
            WebMode = "PC";
        }
        strLog += "WebMode:" + WebMode + " ";
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 로그 생성

        ErrorLog.Write(HttpContext.Current, 602, strLog);
    }

	public void Hide( ) {

		ph_content.Visible = false;
	}

	protected override void OnLoad(EventArgs e) {
		
		base.OnLoad(e);
		
	}


}
