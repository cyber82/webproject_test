﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;


public partial class pay_kcp_no_form : System.Web.UI.UserControl {
	
	public void Show(StateBag state) {	
		this.ViewState["order_item"] = state["order_item"];
		this.ViewState["good_name"] = state["good_name"];
		this.ViewState["good_mny"] = state["good_mny"];
		this.ViewState["buyr_name"] = state["buyr_name"];
		this.ViewState["ordr_idxx"] = state["ordr_idxx"];

		ph_content.Visible = true;
	}

	public void Hide( ) {
		ph_content.Visible = false;
	}

	protected override void OnLoad(EventArgs e) {	
		base.OnLoad(e);	
	}
}
