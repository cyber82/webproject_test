﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

using KCP.PP_CLI_COM.LIB;
using CommonLib;
using System.Data.Linq;
using System.Linq;
using System.Collections.Generic;
using System.Text;

public partial class pay_kcp : FrontBasePage {

    #region KCP
    /* ==================================================================================== */
    /* +    변수                                                                          + */
    /* - -------------------------------------------------------------------------------- - */
    private string m_strCFG_paygw_url;      // 결제 GW URL
    private string m_strCFG_paygw_port;     // 결제 GW PORT
    private string m_strCFG_key_path;       // KCP 모듈 KEY 경로
    private string m_strCFG_log_path;       // KCP 모듈 LOG 경로 
    private string m_strCFG_site_key;       // 상점 키
    protected string m_strCFG_site_cd;      // 상점 코드
                                            /* - -------------------------------------------------------------------------------- - */
    private string m_strCustIP;             // 요청 IP
    private string m_strTxCD;               // 처리 종류
    private string m_strTxMony;
    private string m_strSet_user_type;      // PG1,NETPG 구분
                                            /* - -------------------------------------------------------------------------------- - */
    protected string amount;                // 실제 거래금액
    protected string req_tx;                // 요청 종류
    protected string bSucc;                 // 업체 처리 성공 유무
    protected string use_pay_method;        // 사용한 결제 수단
    protected string ordr_idxx;             // 주문 번호
    protected string good_name;             // 상품명
    protected string good_mny;              // 금액
    protected string buyr_name;             // 구매자명
    protected string buyr_mail;             // 구매자 메일
    protected string buyr_tel1;             // 구매자 연락처 1
    protected string buyr_tel2;             // 구매자 연락처 2
                                            /* - -------------------------------------------------------------------------------- - */
    protected string trad_numb;
    protected string enct_info;
    protected string enct_data;
    protected string ordr_mony;
    protected string soc_no;
    protected string tran_cd;


    /* - -------------------------------------------------------------------------------- - */
    protected string res_cd;
    protected string res_msg;
    /* - -------------------------------------------------------------------------------- - */
    protected string mod_type;              // 변경TYPE(승인취소시 필요)
    protected string tno;                   // KCP 거래 고유 번호
    protected string mod_desc;              // 변경 사유
    protected string mod_mny;               // 취소요청금액
    protected string rem_mny;               // 취소가능잔액
    protected string panc_mod_mny;          // 부분취소 금액
    protected string panc_rem_mny;          // 부분취소 가능 금액]
    protected string mod_tax_mny;           // 공급가 부분 취소 요청 금액
    protected string mod_vat_mny;           // 부과세 부분 취소 요청 금액
    protected string mod_free_mny;          // 비과세 부분 취소 요청 금액
    protected string tax_flag;              // 복합과세 구분
                                            /* - -------------------------------------------------------------------------------- - */
    protected string m_strResCD;            // 응답코드
    protected string m_strResMsg;           // 응답메시지
                                            /* - -------------------------------------------------------------------------------- - */
    protected string commid;                // 통신사 코드
    protected string mobile_no;             // 휴대폰 번호
                                            /* - -------------------------------------------------------------------------------- - */
    protected string tk_van_code;           // 발급사 코드
    protected string tk_app_no;             // 승인 번호
    protected string tk_app_time;           // 상품권 승인시간
    protected string shop_user_id;          // 가맹점 고객 아이디
                                            /* - -------------------------------------------------------------------------------- - */
    protected string card_cd;               // 카드사 코드
    protected string card_name;             // 카드사 명
    protected string app_time;              // 승인시간
    protected string app_no;                // 승인번호
    protected string noinf;                 // 무이자 여부
    protected string quota;                 // 할부 개월 수
    protected string partcanc_yn;           // 부분취소가능여부
    protected string card_bin_type_01;      // 카드구분1
    protected string card_bin_type_02;      // 카드구분2
    protected string card_mny;              // 카드결제금액 
                                            /* - -------------------------------------------------------------------------------- - */
    protected string add_pnt;               // 발생 포인트
    protected string use_pnt;               // 사용가능 포인트
    protected string rsv_pnt;               // 적립 포인트
    protected string pnt_app_time;          // 승인시간
    protected string pnt_app_no;            // 승인번호
    protected string pnt_amount;            // 적립금액 or 사용금액
    protected string pnt_issue;             // 포인트 결제사
                                            /* - -------------------------------------------------------------------------------- - */
    protected string bank_name;             // 은행명
    protected string bank_code;             // 은행코드
    protected string bk_mny;                // 계좌이체결제금액 
                                            /* - -------------------------------------------------------------------------------- - */
    protected string bankname;              // 입금할 은행 이름
    protected string depositor;             // 입금할 계좌 예금주
    protected string account;               // 입금할 계좌 번호
    protected string va_date;               // 가상계좌 입금마감시간
                                            /* - -------------------------------------------------------------------------------- - */
    protected string hp_app_time;           // 휴대폰 승인시간
    protected string hp_commid;             // 통신사 코드
    protected string hp_mobile_noo;         // 휴대폰 번호
                                            /* - -------------------------------------------------------------------------------- - */
    protected string ars_app_time;          // ARS 승인시간
                                            /* - -------------------------------------------------------------------------------- - */
    protected string cash_yn;               // 현금 영수증 등록 여부
    protected string cash_authno;           // 현금 영수증 승인 번호
    protected string cash_tr_code;          // 현금 영수증 발행 구분
    protected string cash_id_info;          // 현금 영수증 등록 번호     

    //[jun.heo] 2017-12 : 결제 모듈 통합
    protected string payPageType;           // 결제 페이지 종류

    /* - -------------------------------------------------------------------------------- - */
    //vitiontrip
    protected string plandetail_id;          // 기획트립ID
    protected string amount_type;          // 기획트립ID
    protected bool bPaySuccess = false;
    string order_item;
    /* - -------------------------------------------------------------------------------- - */

    #endregion

    string ci;
    string di;
    string jumin;

    string campaignId;
    string codeId;
    string codeName;

    string overseas_card;   // 해외카드 여부 Y,N
    string month;
    string motiveCode;          // 동기코드
    string motiveName;          // 동기명
    string sDescription, sPaymentDay;
    string pi;

    string isFirstPay;          // 첫 결제 여부

    string paymentType;

    public override bool RequireSSL {
        get {
            return true;
        }
    }

    public override bool IgnoreProtocol
    {
        get
        {
            return true;
        }
    }

    WWWService.Service _wwwService = new WWWService.Service();
    WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
    /* - -------------------------------------------------------------------------------- - */
    protected override void OnBeforePostBack() {
        /* -------------------------------------------------------------------------------- */
        /* +    DATA 설정                                                                 + */
        /* - ---------------------------------------------------------------------------- - */
        m_f__get_param();
        /* -------------------------------------------------------------------------------- */
        /* +    가맹점 정보 READ                                                          + */
        /* - ---------------------------------------------------------------------------- - */
        /*
		m_f__load_env();    // 가맹점 고유 환경설정
							

		m_f__do_tx();

		result.Value = "N";

		if(res_cd == "0000") {
			
			if(DoSave()) {
				result.Value = "Y";
			}

		} else {
			base.AlertWithJavascript(res_msg);
			return;
		}
		*/

        result.Value = "N";

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리
        // 결제 종류별 사전 처리 및 결제 종류에 맞는 "DoSave_XXXX()" 호출
        switch (payPageType)
        {
            case "VisionTrip":  // 비전트립
                m_f__load_env();    // 가맹점 고유 환경설정
                m_f__do_tx();

                if (res_cd == "0000")
                {
                    bSucc = "Y";
                    if (DoSave_VisionTrip())
                    {
                        result.Value = "Y";
                    }
                }
                else
                {
                    base.AlertWithJavascript(res_msg);
                    return;
                }
                break;
            case "Store":   // 스토어
                m_f__load_env();    // 가맹점 고유 환경설정
                m_f__do_tx();

                if (res_cd == "0000")
                {
                    // 스토어는 m_f__to_do_shop_pay() 에서 DoSave() 를 호출함.
                    //bSucc = "Y";
                    //if (DoSave_Store())
                    //{
                    //    result.Value = "Y";
                    //}
                }
                else
                {
                    base.AlertWithJavascript(res_msg);
                    return;
                }
                break;
            case "PayAccount":  // 계좌관리
                overseas_card = "Y";
                if (DoSave_PayAccount())
                {
                    result.Value = "Y";
                }
                break;
            case "PayDelay":    // 지연된 후원금
                if (DoSave_PayDelay())
                {
                    result.Value = "Y";
                }
                break;
            case "Temporary":   // 일시후원 : 특별한 모금 및 나머지
            default:
                if (DoSave_Tempoaray())
                {
                    //[이종진] 추가 - 신규방법일 경우(GlobalPool조회 방식), 성공 처리함
                    result.Value = "Y";
                }
                else
                {
                    //[이종진] 추가 - 신규방법일 경우(GlobalPool조회 방식), 실패 처리함
                }
                break;
        }
    }

    //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리 - 일시후원용 DoSave()
    bool DoSave_Tempoaray() {
        
        var sponsorId = "";
        var sUserID = "";
        var sUserName = "";
        string[] sResult = new string[3];
        var sGiroYN = "N";
        string refUrl = "";
        if (Request.UrlReferrer != null)
            refUrl = Request.UrlReferrer.ToString();

        string firstPay = "0";
        if (refUrl.IndexOf("pay_again") > 0)
        {
            firstPay = "1";
        }
        else if (isFirstPay == "1")
            firstPay = "1";

        //신규 GlobalPool 조회방법인지 체크
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();

        var action = new CommitmentAction();
        JsonWriter result = new JsonWriter();
        var payInfo = pi.ToObject<PayItemSession.Entity>();
        DataSet dsResult = new DataSet();

        string mobile = "";
        if (UserInfo.IsLogin) {
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
            sUserID = sess.UserId;
            sUserName = sess.UserName;

            payInfo.sponsorId = sponsorId;
            payInfo.sponsorName = sUserName;
            payInfo.userId = sUserID;
            mobile = sess.Mobile;
        } else {

            #region  로그인 되어있지 않으면 비회원 결제를 위해 sponsorId 구하기

            if (buyr_name == "무기명") {
                sponsorId = "20160621142650372";

            } else {
                // 해외회원 , 무기명인경우는 CI가 없음
                if (!string.IsNullOrEmpty(ci)) {
                    Object[] objSql = new object[1] { string.Format("SELECT SponsorID FROM tSponsorMaster with(nolock) WHERE CurrentUse = 'Y' and (userId is null or userId = '') and CI <> '' and CI = '{0}'", ci) };
                    DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

                    if (ds.Tables[0].Rows.Count > 0) {
                        sponsorId = ds.Tables[0].Rows[0]["SponsorID"].ToString().Trim();
                    }
                }

                // sponsorId 가 없으면 새로 생성
                if (string.IsNullOrEmpty(sponsorId)) {

                    try {

                        sponsorId = _wwwService.GetDate().ToString("yyyyMMddHHmmssff2");
                        /*
						objSql = new object[1] { string.Format(@"insert into tSponsorMaster (sponsorId , sponsorName ,juminId ,channelType , currentUse ,userDate , userClass , registerDate , modifyDate , di , ci) values ('{0}','{1}','{2}','Web' , 'Y' , getdate() , '비회원' , getdate() , getdate() , '{3}' , '{4}') ",
							sponsorId , buyr_name , jumin , di , ci) };
						//	Response.Write(objSql[0].ToJson());
						//	return;
						_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
						*/

                        // tSponsorMaster에 LetterPreference, CorrReceiveType 컬럼은 default값으로 처리 ('Digital', 'online')
                        var sponsorResult = _wwwService.registerDATSponsor2(sponsorId, buyr_name, jumin, "", "", "", "", (string.IsNullOrEmpty(ci)) ? "국외" : "국내", "", "", ""
                                           , "", "", CodeAction.GetGenderByJumin(jumin) /*gender*/
                                           , "", "", "", "", ""
                                           , "", "", "", "", DateTime.Now.ToString(), "14세이상", string.IsNullOrEmpty(ci) ? "" : DateTime.Now.ToString()
                                           , string.IsNullOrEmpty(ci) ? "" : "서울신용평가", sponsorId, buyr_name, "", "", "", "", "", "", di, ci, "0");

                        if (sponsorResult.Substring(0, 2) == "30") {

                            base.AlertWithJavascript("회원님의 정보를 등록하지 못했습니다..(DAT) \\r\\n" + sponsorResult.Substring(2).ToString().Replace("\n", ""));
                            return false;
                        }
                    } catch (Exception ex) {
                        ErrorLog.Write(this.Context, 0, ex.Message);
                        base.AlertWithJavascript("비회원 정보를 등록하는 중에 에러가 발생했습니다.");
                        return false;
                    }

                }
            }
            #endregion

            payInfo.sponsorId = sponsorId;
            payInfo.sponsorName = buyr_name;
            payInfo.userId = "";
        }

        //Pay Log 생성 (결제모듈 응답)
        StringBuilder sb = new StringBuilder();
        sb.Append("sponsorId:" + sponsorId + ", ");
        sb.Append("sUserID:" + sUserID + ", ");
        sb.Append("payInfo.campaignId:" + "" + ", ");
        sb.Append("payInfo.group:" + "CDSP" + ", ");
        sb.Append("sponsorName:" + payInfo.sponsorName + ", ");
        sb.Append("mobile:" + mobile + ", ");
        sb.Append("param_01:" + m_f__get_post_data("param_opt_1"));
        ErrorLog.Write(HttpContext.Current, 705, sb.ToString());

        //1:1어린이 후원일 경우
        if (payInfo.group == "CDSP") {
            //[이종진] 추가 - 첫 결제의 경우는(결제 실패 후, 재결제가 아닌경우.(pay-again)) 결연로직 수행
            if(firstPay == "1") //pay-again - 실패 후 재결제이므로, 이미 결연처리는 되어있으므로 validation 체크만함
            {
                //결연 가능여부만 체크
                result = action.DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName, motiveCode, motiveName, payInfo.amount, payInfo.month, firstPay);
            }
            else if (payPageType.ToLower() == "wedding") 
            {
                // 특별한나눔 > 결혼 기념일 > 후원 (wedding) 일 경우
                result = action.DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName, motiveCode, motiveName, payInfo.amount, payInfo.month, true);
            }
            else
            {
                // 1:1 어린이 후원. 해외카드결제
                result = action.DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName, motiveCode, motiveName, payInfo.amount, payInfo.month);
            }
        } else if (payInfo.group == "GIFT") {
            result = action.DoGIFT(sponsorId, payInfo.childMasterId, payInfo.amount.ToString(), use_pay_method, month, "일시", payInfo.codeId);
        } else {
            // 일반후원 첫 결제의 경우 일반 결연목록 조회 후 commitmentid 가져온다.
            if (firstPay == "1")
            {
                DataSet dsActiveChild = _wwwService.listActiveCommitment(sponsorId, "0000000000", "", "");
                DataRow[] alreadyRow = dsActiveChild.Tables[0].Select("SponsorItemEng = '" + payInfo.codeId + "' and FundingFrequency <> '1회' and CampaignID='" + payInfo.campaignId + "'");
                payInfo.commitmentId = alreadyRow[0]["CommitmentID"].ToString();

                //정기후원이면서 이미 납부중인 양육보완프로그램이 있을경우 (일시후원은 항상 등록가능)
                if (alreadyRow.Length > 0)
                {
                    result.success = true;
                    payInfo.commitmentId = alreadyRow[0]["CommitmentID"].ToString();
                }
                else
                {
                    result.success = false;
                    result.message = "후원 정보 조회에 실패했습니다.";
                }
            }
            else {
                result = action.DoCIV(sponsorId, payInfo.amount, payInfo.month, use_pay_method, payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.childMasterId, payInfo.campaignId, "", "");

                if (result.success)
                    payInfo.commitmentId = result.data.ToString();
                else
                {
                    result.success = false;
                    result.message = "후원 정보 저장에 실패했습니다.";
                }
            }
        }
        //	Response.Write(result.ToJson());
        ErrorLog.Write(HttpContext.Current, 0, "refUrl=========" + refUrl);
        ErrorLog.Write(HttpContext.Current, 0, "firstPay=========" + firstPay);
        ErrorLog.Write(HttpContext.Current, 0, "KCP payInfo=========" + payInfo.group + "::" + payInfo.childMasterId + "::" + payInfo.campaignId + "::" + payInfo.codeName + "::" + payInfo.amount + "::" + payInfo.month + "::" + payInfo.frequency + "::" + use_pay_method + "::" + motiveCode);
        ErrorLog.Write(HttpContext.Current, 0, "KCP RESULT.success=========" + result.success);
        //ErrorLog.Write(HttpContext.Current, 0, "KCP result.data.ToString()=========" + result.data.ToString());

        if (!result.success) {

            //action.DeleteCommitment(result.data.ToString());
            base.AlertWithJavascript(result.message);
            return false;
        }

        if (payInfo.group == "CDSP")
        {
            // CommitmentID 조회
            ChildAction child = new ChildAction();
            var childInfo = child.GetChild(payInfo.childMasterId);

            if (childInfo.success)
            {
                ChildAction.ChildItem item = (ChildAction.ChildItem)childInfo.data;
                string childKey = item.ChildKey;

                var childDetail = child.MyChildren(childKey, 1, 1);
                if (childDetail.success)
                {
                    List<ChildAction.MyChildItem> detail = (List<ChildAction.MyChildItem>)childDetail.data;
                    if (detail == null || detail.Count == 0)
                    {
                        ErrorLog.Write(this.Context, 0, "어린이 결연 정보 조회에 실패했습니다. " + payInfo.sponsorId + " " + payInfo.sponsorName + " " + payInfo.childMasterId);
                        base.AlertWithJavascript("어린이 결연 정보 조회에 실패했습니다.");
                        return false;
                    }
                    payInfo.commitmentId = detail[0].commitmentId;
                }
                else
                {
                    ErrorLog.Write(this.Context, 0, "어린이 결연 정보 조회에 실패했습니다. " + payInfo.sponsorId + " " + payInfo.sponsorName + " " + payInfo.childMasterId);
                    base.AlertWithJavascript("어린이 결연 정보 조회에 실패했습니다.");
                    return false;
                }
            }
            else
            {
                ErrorLog.Write(this.Context, 0, "어린이 정보 조회에 실패했습니다. " + payInfo.sponsorId + " " + payInfo.sponsorName + " " + payInfo.childMasterId);
                base.AlertWithJavascript("어린이 정보 조회에 실패했습니다.");
                return false;
            }
        }

        // 후원아이디와 비회원의 경우 sponsorId,  이름 업데이트
        //payInfo.commitmentId = result.data.ToString();
        new PayItemSession.Store().Update(ordr_idxx, payInfo, null);

        //returnUrl.Value = "complete/" + result.data;	// commitmentId
        //returnUrl.Value = "complete/" + ordr_idxx;    // notificationId


        if (use_pay_method == "100000000000") {
            string testLog = "미납후원금결제  ";
            #region 신용카드
            if (overseas_card == "Y") {

                //sGiroYN = payInfo.frequency == "정기" ? "Y" : "N";
                dsResult = _wwwService.PaymentWebCard_Foreign(req_tx, use_pay_method, ordr_idxx, good_name, good_mny
                                                        , buyr_name, buyr_mail, buyr_tel1, buyr_tel2, soc_no
                                                        , trad_numb, tran_cd, enct_info, enct_data, mod_desc
                                                        , mod_type, tno, m_strCustIP, sponsorId, sUserID
                                                        , sUserName, sDescription, sPaymentDay, sGiroYN, CodeAction.ChannelType);

                // 결제수단이 없는 경우 해외카드 결제수단 추가
                if (payInfo.frequency == "정기") {

                    DataSet dsAddress = _wwwService.getSponsorAddress(sponsorId, "", "", "Y");

                    string sAddress = "";

                    if (dsAddress.Tables[0].Rows.Count > 0)
                        sAddress = Convert.ToString(dsAddress.Tables[0].Rows[0]["AddressID"]);

                    _wwwService.RegisterPaymentAccount_Foreign(""
                                                    , CodeAction.ChannelType
                                                    , sponsorId
                                                    , card_cd
                                                    , sAddress
                                                    , buyr_name
                                                    , ordr_idxx
                                                    , "한국컴패션"
                                                    , soc_no
                                                    , sUserID
                                                    , sUserName);
                }

            } else {
                // 국내카드
                dsResult = _wwwService.PaymentWebCard_NEW(req_tx, use_pay_method, ordr_idxx, good_name, good_mny
                                               , buyr_name, buyr_mail, buyr_tel1, buyr_tel2, soc_no
                                               , trad_numb, tran_cd, enct_info, enct_data, mod_desc
                                               , mod_type, tno, m_strCustIP, sponsorId, sUserID
                                               , sUserName, sDescription, sPaymentDay, sGiroYN, "N", CodeAction.ChannelType);
            }
            #endregion

        } else if (use_pay_method == "010000000000") {

            #region 실시간 계좌이체
            dsResult = _wwwService.PaymentACCOUNT(req_tx, use_pay_method, ordr_idxx, good_name, good_mny
                                           , buyr_name, buyr_mail, buyr_tel1, buyr_tel2, soc_no
                                           , trad_numb, tran_cd, enct_info, enct_data, mod_desc
                                           , mod_type, tno, m_strCustIP, sponsorId, sUserID
                                           , sUserName, sDescription, sPaymentDay, sGiroYN, CodeAction.ChannelType);
            #endregion

        } else if (use_pay_method == "000010000000") {

            #region 휴대폰
            dsResult = _wwwService.PaymentPHONE(req_tx, use_pay_method, ordr_idxx, good_name, good_mny
                                           , buyr_name, buyr_mail, buyr_tel1, buyr_tel2, soc_no
                                           , trad_numb, tran_cd, enct_info, enct_data, mod_desc
                                           , mod_type, tno, m_strCustIP, sponsorId, sUserID
                                           , sUserName, sDescription, sPaymentDay, sGiroYN, CodeAction.ChannelType);
            #endregion

        }

        ErrorLog.Write(this.Context, 705, "kcp payment result:::::::::::::::::" + dsResult.ToJson());   // [jun.heo] 2017-12 : 결제 모듈 통합 - 코드를 705 로 변경

        sResult[0] = dsResult.Tables[0].Rows[0]["ResCd"].ToString();
        sResult[1] = dsResult.Tables[0].Rows[0]["ResMsg"].ToString();
        sResult[2] = ordr_idxx;

        if (sResult[0] == "0000") {

            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
            string strLog = string.Empty;
            strLog += "SponsorID:" + payInfo.sponsorId + ", ";
            strLog += "OrderID:" + dsResult.Tables[0].Rows[0]["OrderIdxx"].ToString() + ", ";
            strLog += "UserID:" + payInfo.userId + ", ";
            strLog += "CampaignID:" + payInfo.campaignId + ", ";
            strLog += "Group:" + payInfo.group + ", ";
            strLog += "ResultCode:" + dsResult.Tables[0].Rows[0]["ResCd"].ToString() + ", ";
            strLog += "mid:" + m_strCFG_site_cd + ", ";   // 가맹점 ID
            strLog += "tid:" + dsResult.Tables[0].Rows[0]["OrderTNo"].ToString() + ", ";   // 거래 ID
            strLog += "moid:" + dsResult.Tables[0].Rows[0]["OrderIdxx"].ToString() + ", ";  // 주문번호
            strLog += "amt:" + dsResult.Tables[0].Rows[0]["GoodMny"].ToString() + ", ";
            strLog += "cardcode:" + dsResult.Tables[0].Rows[0]["CardCd"].ToString() + ", ";  // 카드사코드
            strLog += "cardname:" + dsResult.Tables[0].Rows[0]["CardName"].ToString() + ", ";  // 결제카드사명
            strLog += "cardbin:" + "" + ", ";   // 카드 BIN 번호
            strLog += "cardpoint:" + "" + ", "; // 카드 포인트 사용여부(0-미사용, 1-포인트사용, 2-세이브포인트사용)
            strLog += "codeID:" + payInfo.codeId + ", ";
            strLog += "buyr_name:" + dsResult.Tables[0].Rows[0]["BuyerName"].ToString() + ", ";
            strLog += "UserName:" + payInfo.sponsorName + ", ";
            strLog += "Mobile:" + mobile + ", ";
            strLog += "IsUser:" + dsResult.Tables[0].Rows[0]["BuyerName"].ToString() + ", ";    // 회원(회원명), 비회원, 무기명
            strLog += "codename:" + payInfo.codeName + ", ";
            strLog += "Childkey:" + payInfo.childKey + ", ";
            strLog += "ChildMasterID:" + payInfo.childMasterId + ", ";
            strLog += "month:" + payInfo.month + ", ";
            strLog += "frequency:" + payInfo.frequency + ", ";
            strLog += "TypeName:" + payInfo.TypeName + ", ";

            //-----------------------------------------------------------------------------
            // USER-AGENT 구분
            //-----------------------------------------------------------------------------
            string WebMode = Request.UserAgent.ToLower();
            if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
            {
                WebMode = "MOBILE";
            }
            else
            {
                WebMode = "PC";
            }
            strLog += "WebMode:" + WebMode + " ";

            ErrorLog.Write(HttpContext.Current, 705, strLog);
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성

            string cmtID = string.IsNullOrEmpty(payInfo.commitmentId) ? result.data.ToString() : payInfo.commitmentId;

            registerPartitioning_CommitmentGroup(sponsorId, cmtID);

            //1:1 어린이 후원이면
            if (payInfo.group == "CDSP") {

                updatePTD_Temporary(cmtID);
                //[이종진] 기존방법과 신규방법 if
                if (strdbgp_kind == "1")    //기존
                {
                    new ChildAction().Release(payInfo.childMasterId);
                }
                else //if(strdbgp_kind == "2")
                {   //신규
                    //GlobalSponsorID 생성, GlobalCommitment생성(킷발송), NomoneyHold및 임시결연정보삭제, tCommitmentMaster Update
                    result = action.ProcSuccessPay(cmtID);
                    if (!result.success) return false;
                    new ChildAction().Release(payInfo.childMasterId, false);
                }
            }

        } else {
            //[이종진]2018-03-19 - 1:1 후원 시, 결제 실패면 결연삭제 추가
            //추가로 TCPT용 삭제 작업이 필요함.
            //->nomoneyHold를 ecommerceHold로 변경, compass4 DB에 nomoneyHold 및 임시결연데이터 삭제
            if (payInfo.group == "CDSP")
            {
                string cmtID = string.IsNullOrEmpty(payInfo.commitmentId) ? result.data.ToString() : payInfo.commitmentId;
                action.DeleteCommitment(cmtID);
            }
            base.AlertWithJavascript(sResult[1]);
            return false;
        }

        if (firstPay == "1")
        {
            string tmp = dsResult.Tables[0].Rows[0]["ResultCode"].ToString();
        }

        return true;

    }

    //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리 - 비전트립용 DoSave()
    bool DoSave_VisionTrip()
    {


        JsonWriter result = new JsonWriter();

        var sponsorId = "";
        var sUserID = "";
        var sUserName = "";
        var sess = new UserInfo();
        sponsorId = sess.SponsorID;
        sUserID = sess.UserId;
        sUserName = sess.UserName;

        //Pay Log 생성 (결제모듈 응답)
        StringBuilder sb = new StringBuilder();
        sb.Append("sponsorId:" + sponsorId + ", ");
        sb.Append("sUserID:" + sUserID + ", ");
        sb.Append("payInfo.campaignId:" + "" + ", ");
        sb.Append("payInfo.group:" + "store" + ", ");
        sb.Append("param_01:" + m_f__get_post_data("param_opt_1"));
        ErrorLog.Write(HttpContext.Current, 705, sb.ToString());

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
        string strLog = string.Empty;
        strLog += "SponsorID:" + sponsorId + ", ";
        strLog += "OrderID:" + ordr_idxx + ", ";
        strLog += "UserID:" + sUserID + ", ";
        strLog += "CampaignID:" + "" + ", ";
        strLog += "Group:" + "" + ", ";
        strLog += "ResultCode:" + m_strResCD + ", ";
        strLog += "mid:" + m_strCFG_site_cd + ", ";   // 가맹점 ID
        strLog += "tid:" + tno + ", ";   // 거래 ID
        strLog += "moid:" + "" + ", ";  // 주문번호
        strLog += "amt:" + bk_mny + ", ";
        strLog += "cardcode:" + card_cd + ", ";  // 카드사코드
        strLog += "cardname:" + card_name + ", ";  // 결제카드사명
        strLog += "cardbin:" + "" + ", ";   // 카드 BIN 번호
        strLog += "cardpoint:" + "" + ", "; // 카드 포인트 사용여부(0-미사용, 1-포인트사용, 2-세이브포인트사용)
        strLog += "codeID:" + "" + ", ";
        strLog += "buyr_name:" + "" + ", ";
        strLog += "UserName:" + sUserName + ", ";
        strLog += "Mobile:" + "" + ", ";
        strLog += "IsUser:" + "" + ", ";    // 회원(회원명), 비회원, 무기명
        strLog += "codename:" + "" + ", ";
        strLog += "Childkey:" + "" + ", ";
        strLog += "ChildMasterID:" + "" + ", ";
        strLog += "month:" + "" + ", ";
        strLog += "frequency:" + "" + ", ";
        strLog += "TypeName:" + "" + ", ";
        strLog += "param_01:" + m_f__get_post_data("param_opt_1") + ", ";
        strLog += "plandetail_id:" + plandetail_id + ", ";

        //-----------------------------------------------------------------------------
        // USER-AGENT 구분
        //-----------------------------------------------------------------------------
        string WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
        {
            WebMode = "MOBILE";
        }
        else
        {
            WebMode = "PC";
        }
        strLog += "WebMode:" + WebMode + " ";

        ErrorLog.Write(HttpContext.Current, 705, strLog);
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성

        ErrorLog.Write(this.Context, 705, string.Format("비전트립 결제 KCP 데이타 tno={0},card_name={1},card_cd={2},m_strCFG_site_cd={3},req_tx={4}", tno, card_name, card_cd, m_strCFG_site_cd, req_tx));


        ErrorLog.Write(HttpContext.Current, 0, "va_date : " + va_date);


        ErrorLog.Write(HttpContext.Current, 0, "good_name : " + good_name);

        if (UserInfo.IsLogin)
        {
            try
            {
                decimal dAmount = new decimal();
                bool check = Decimal.TryParse(bk_mny, out dAmount);

                using (FrontDataContext dao = new FrontDataContext())
                {
                    tVisionTripPayment payment = new tVisionTripPayment();

                    payment.PlanDetailID = Convert.ToInt32(plandetail_id);
                    payment.OrderNo = ordr_idxx;
                    payment.SponsorID = sponsorId;
                    payment.AmountType = Convert.ToChar(amount_type);
                    payment.PaymentType = use_pay_method;
                    payment.PaymentName = use_pay_method == "010000000000" ? "실시간 계좌이체" : "가상계좌";
                    payment.Amount = dAmount;
                    payment.PaymentDate = DateTime.Now;
                    payment.BankCode = bank_code;
                    payment.BankName = use_pay_method == "001000000000" ? bankname : bank_name;
                    payment.Account = account;
                    payment.KCPTNo = tno;
                    payment.Requester = sUserName;
                    if (use_pay_method == "001000000000")
                    {
                        payment.PaymentResultYN = 'N';
                        //payment.DueDate = Convert.ToDateTime(va_date); 
                        payment.DueDate = DateTime.ParseExact(va_date, "yyyyMMddHHmmss", null);
                        payment.Depositor = depositor;
                    }
                    else
                    {
                        payment.Depositor = sUserName;
                        payment.PaymentResultYN = bPaySuccess ? 'Y' : 'N';//bSucc != null ? Convert.ToChar(bSucc) : 'N';
                    }
                    payment.RegisterID = sUserID;
                    payment.RegisterName = sUserName;
                    payment.RegisterDate = DateTime.Now;

                    //dao.tVisionTripPayment.InsertOnSubmit(payment);
                    www6.insert(payment);

                    int iID = 0;
                    bool id = int.TryParse(plandetail_id, out iID);
                    //var plandetail = dao.tVisionTripPlanDetail.Where(p => p.PlanDetailID == iID).FirstOrDefault();
                    var plandetail = www6.selectQF<tVisionTripPlanDetail>("PlanDetailID", iID);

                    if (plandetail != null && bPaySuccess)
                    {
                        if (amount_type == "R")
                        {
                            plandetail.RequestCostPaymentYN = 'Y';
                            plandetail.RequestCostPaymentDate = DateTime.Now;
                        }
                        else if (amount_type == "T")
                        {
                            plandetail.TripCostPaymentYN = 'Y';
                            plandetail.TripCostPaymentDate = DateTime.Now;
                        }
                        else if (amount_type == "C")
                        {
                            plandetail.ChildMeetCostPaymentYN = 'Y';
                            plandetail.ChildMeetCostPaymentDate = DateTime.Now;
                        }
                    }

                    //dao.SubmitChanges();
                    www6.update(plandetail);

                    paymentID.Value = payment.PaymentID.ToString();

                    #region 발급 완료 SMS
                    if (use_pay_method == "001000000000")
                    {
                        var amounttype = amount_type == "R" ? "신청비" : amount_type == "T" ? "트립비" : amount_type == "C" ? "어린이 만남비" : "";

                        //var detailEntity = dao.tVisionTripPlanDetail.Where(p => p.PlanDetailID == Convert.ToInt32(plandetail_id)).FirstOrDefault();
                        var detailEntity = www6.selectQF<tVisionTripPlanDetail>("PlanDetailID", Convert.ToInt32(plandetail_id));

                        if (detailEntity != null)
                        {
                            //var applyEntity = dao.tVisionTripApply.Where(p => p.ApplyID == detailEntity.ApplyID).FirstOrDefault();
                            var applyEntity = www6.selectQF<tVisionTripApply>("ApplyID", detailEntity.ApplyID);

                            UserInfo userInfo = new UserInfo();
                            var smsMessage = "[컴패션] " + userInfo.UserName + "님, 비전트립 " + amounttype + " 전용 가상계좌가 발급되었습니다.";

                            smsMessage += bankname + " 계좌번호 :" + account;

                            var smsSendYN = util.SendSMS.SMSSend(applyEntity.Tel.Decrypt(), userInfo.UserName, smsMessage);
                            var entity = new tVisionTripMessage()
                            {
                                ApplyID = applyEntity.ApplyID,
                                UserID = userInfo.UserId,
                                SponsorID = applyEntity.SponsorID,
                                Message = smsMessage,
                                GroupNo = StringExtension.GetRandomString(3) + DateTime.Now.ToString("yyyyMMddHHmmss"),
                                ReceiveTel = applyEntity.Tel.Decrypt(),
                                ReceiveID = applyEntity.UserID,
                                ReceiveName = userInfo.UserName,
                                SendType = 'D', //즉시발송
                                MessageType = "가상계좌발급",
                                SendTel = ConfigurationManager.AppSettings["SMSSendNo"].ToString().Trim(),
                                SendYN = smsSendYN ? 'Y' : 'N',
                                SendDate = DateTime.Now,
                                CurrentUse = 'Y',
                                RegisterID = userInfo.UserId,
                                RegisterName = userInfo.UserName,
                                RegisterDate = DateTime.Now
                            };
                            //dao.tVisionTripMessage.InsertOnSubmit(entity);
                            www6.insert(entity);
                            //dao.SubmitChanges();
                        }
                    }
                    #endregion



                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.Write(this.Context, 500, "DoSave 오류 : " + ex.Message);
                return false;
            }

            //       StoreAction.orderItem oi = order_item.ToObject<StoreAction.orderItem>();
            //	orderNo.Value = oi.orderNo;
            //if(UserInfo.IsLogin) {


            //           oi.authDate = app_time;
            //		oi.authNumber = app_no;
            //		oi.bank = bank_name.EmptyIfNull();
            //		oi.bankCode = bank_code.EmptyIfNull();
            //		oi.cardName = card_name;
            //		oi.cardPeriod = quota;
            //		oi.cardType = card_cd;
            //		//oi.settlePrice = Convert.ToInt32(good_mny);
            //		oi.site_code = m_strCFG_site_cd;
            //		oi.payKey = tno;
            //		oi.payMethod = use_pay_method;
            //		oi.req_tx = req_tx;
            //		oi.resCD = m_strResCD;
            //		oi.resMsg = m_strResMsg;
            //		result = new StoreAction().Order(oi);
            //		if(result.success) {
            //			return true;
            //		} else {
            //			return false;
            //		}


        }
        else
        {


            return false;
        }


    }

    //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리 - 지연된 후원금 DoSave()
    bool DoSave_PayDelay()
    {

        string testLog = "미납후원금결제  ";
        ErrorLog.Write(this.Context, 0, testLog);
        var sponsorId = "";
        var sUserID = "";
        var sUserName = "";
        string[] sResult = new string[3];
        DataSet dsResult = new DataSet();
        var sGiroYN = "N";

        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
            sUserID = sess.UserId;
            sUserName = sess.UserName;

        }
        else
        {
            return false;
        }

        //Pay Log 생성 (결제모듈 응답)
        StringBuilder sb = new StringBuilder();
        sb.Append("sponsorId:" + sponsorId + ", ");
        sb.Append("sUserID:" + sUserID + ", ");
        sb.Append("payInfo.campaignId:" + "" + ", ");
        sb.Append("payInfo.group:" + "CDSP" + ", ");
        sb.Append("param_01:" + m_f__get_post_data("param_opt_1"));
        ErrorLog.Write(HttpContext.Current, 705, sb.ToString());

        if (use_pay_method == "100000000000")
        {

            testLog = "신용카드  ";
            ErrorLog.Write(this.Context, 0, testLog);

            #region 신용카드
            if (overseas_card == "Y")
            {

                testLog = "해외카드  ";
                ErrorLog.Write(this.Context, 0, testLog);

                //sGiroYN = payInfo.frequency == "정기" ? "Y" : "N";
                dsResult = _wwwService.PaymentWebCard_Foreign(req_tx, use_pay_method, ordr_idxx, good_name, good_mny
                                                        , buyr_name, buyr_mail, buyr_tel1, buyr_tel2, soc_no
                                                        , trad_numb, tran_cd, enct_info, enct_data, mod_desc
                                                        , mod_type, tno, m_strCustIP, sponsorId, sUserID
                                                        , sUserName, sDescription, sPaymentDay, sGiroYN, CodeAction.ChannelType);


                testLog = "해외카드 끝";
                ErrorLog.Write(this.Context, 0, testLog);

            }
            else
            {
                // 국내카드
                dsResult = _wwwService.PaymentWebCard_NEW(req_tx, use_pay_method, ordr_idxx, good_name, good_mny
                                               , buyr_name, buyr_mail, buyr_tel1, buyr_tel2, soc_no
                                               , trad_numb, tran_cd, enct_info, enct_data, mod_desc
                                               , mod_type, tno, m_strCustIP, sponsorId, sUserID
                                               , sUserName, sDescription, sPaymentDay, sGiroYN, "N", CodeAction.ChannelType);
            }
            #endregion

        }
        else if (use_pay_method == "010000000000")
        {

            #region 실시간 계좌이체
            dsResult = _wwwService.PaymentACCOUNT(req_tx, use_pay_method, ordr_idxx, good_name, good_mny
                                           , buyr_name, buyr_mail, buyr_tel1, buyr_tel2, soc_no
                                           , trad_numb, tran_cd, enct_info, enct_data, mod_desc
                                           , mod_type, tno, m_strCustIP, sponsorId, sUserID
                                           , sUserName, sDescription, sPaymentDay, sGiroYN, CodeAction.ChannelType);
            #endregion

        }
        else if (use_pay_method == "000010000000")
        {

            #region 휴대폰
            dsResult = _wwwService.PaymentPHONE(req_tx, use_pay_method, ordr_idxx, good_name, good_mny
                                           , buyr_name, buyr_mail, buyr_tel1, buyr_tel2, soc_no
                                           , trad_numb, tran_cd, enct_info, enct_data, mod_desc
                                           , mod_type, tno, m_strCustIP, sponsorId, sUserID
                                           , sUserName, sDescription, sPaymentDay, sGiroYN, CodeAction.ChannelType);

            #endregion

        }

        ErrorLog.Write(this.Context, 705, "kcp payment result:::::::::::::::::" + dsResult.ToJson());   // [jun.heo] 2017-12 : 결제 모듈 통합 - 코드를 705 로 변경

        sResult[0] = dsResult.Tables[0].Rows[0]["ResCd"].ToString();
        sResult[1] = dsResult.Tables[0].Rows[0]["ResMsg"].ToString();
        sResult[2] = ordr_idxx;
        
        ErrorLog.Write(this.Context, 0, "RESULT");
        ErrorLog.Write(this.Context, 0, sResult[0]);
        ErrorLog.Write(this.Context, 0, dsResult.Tables[0].Rows[0]["ResMsg"].ToString());

        if (sResult[0] == "0000")
        {
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
            string strLog = string.Empty;
            strLog += "SponsorID:" + sponsorId + ", ";
            strLog += "OrderID:" + dsResult.Tables[0].Rows[0]["OrderIdxx"].ToString() + ", ";
            strLog += "UserID:" + sUserID + ", ";
            strLog += "CampaignID:" + "" + ", ";
            strLog += "Group:" + "" + ", ";
            strLog += "ResultCode:" + dsResult.Tables[0].Rows[0]["ResCd"].ToString() + ", ";
            strLog += "mid:" + "" + ", ";   // 가맹점 ID
            strLog += "tid:" + dsResult.Tables[0].Rows[0]["OrderTNo"].ToString() + ", ";   // 거래 ID
            strLog += "moid:" + dsResult.Tables[0].Rows[0]["OrderIdxx"].ToString() + ", ";  // 주문번호
            strLog += "amt:" + dsResult.Tables[0].Rows[0]["GoodMny"].ToString() + ", ";
            strLog += "cardcode:" + dsResult.Tables[0].Rows[0]["CardCd"].ToString() + ", ";  // 카드사코드
            strLog += "cardname:" + dsResult.Tables[0].Rows[0]["CardName"].ToString() + ", ";  // 결제카드사명
            strLog += "cardbin:" + "" + ", ";   // 카드 BIN 번호
            strLog += "cardpoint:" + "" + ", "; // 카드 포인트 사용여부(0-미사용, 1-포인트사용, 2-세이브포인트사용)
            strLog += "codeID:" + "" + ", ";
            strLog += "buyr_name:" + dsResult.Tables[0].Rows[0]["BuyerName"].ToString() + ", ";
            strLog += "UserName:" + "" + ", ";
            strLog += "Mobile:" + "" + ", ";
            strLog += "IsUser:" + dsResult.Tables[0].Rows[0]["BuyerName"].ToString() + ", ";    // 회원(회원명), 비회원, 무기명
            strLog += "codename:" + "" + ", ";
            strLog += "Childkey:" + "" + ", ";
            strLog += "ChildMasterID:" + "" + ", ";
            strLog += "month:" + "" + ", ";
            strLog += "frequency:" + "" + ", ";
            strLog += "TypeName:" + "" + ", ";

            //-----------------------------------------------------------------------------
            // USER-AGENT 구분
            //-----------------------------------------------------------------------------
            string WebMode = Request.UserAgent.ToLower();
            if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
            {
                WebMode = "MOBILE";
            }
            else
            {
                WebMode = "PC";
            }
            strLog += "WebMode:" + WebMode + " ";

            ErrorLog.Write(HttpContext.Current, 705, strLog);
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성

            ErrorLog.Write(this.Context, 0, "SetDeliquentTodayPayment 시작");
            bool b = SetDeliquentTodayPayment(ordr_idxx);
            ErrorLog.Write(this.Context, 0, b.ToString());
            return b;

        }
        else
        {

            base.AlertWithJavascript("에러가 발생했습니다.");
            return false;

        }

        return false;

    }

    //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리 - 스토어 DoSave()
    bool DoSave_Store()
    {


        JsonWriter result = new JsonWriter();

        StoreAction.orderItem oi = order_item.ToObject<StoreAction.orderItem>();
        orderNo.Value = oi.orderNo;

        ErrorLog.Write(this.Context, 0, string.Format("쇼핑몰결제 KCP 데이타 tno={0},card_name={1},card_cd={2},m_strCFG_site_cd={3},req_tx={4}", tno, card_name, card_cd, m_strCFG_site_cd, req_tx));
        if (UserInfo.IsLogin)
        {

            var sponsorId = "";
            var sUserID = "";
            var sUserName = "";
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
            sUserID = sess.UserId;
            sUserName = sess.UserName;

            //Pay Log 생성 (결제모듈 응답)
            StringBuilder sb = new StringBuilder();
            sb.Append("sponsorId:" + sponsorId + ", ");
            sb.Append("sUserID:" + sUserID + ", ");
            sb.Append("payInfo.campaignId:" + "" + ", ");
            sb.Append("payInfo.group:" + "store" + ", ");
            sb.Append("param_01:" + m_f__get_post_data("param_opt_1"));
            ErrorLog.Write(HttpContext.Current, 705, sb.ToString());

            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
            string strLog = string.Empty;
            strLog += "SponsorID:" + sponsorId + ", ";
            strLog += "OrderID:" + ordr_idxx + ", ";
            strLog += "UserID:" + sUserID + ", ";
            strLog += "CampaignID:" + "" + ", ";
            strLog += "Group:" + "" + ", ";
            strLog += "ResultCode:" + m_strResCD + ", ";
            strLog += "mid:" + m_strCFG_site_cd + ", ";   // 가맹점 ID
            strLog += "tid:" + tno + ", ";   // 거래 ID
            strLog += "moid:" + "" + ", ";  // 주문번호
            strLog += "amt:" + bk_mny + ", ";
            strLog += "cardcode:" + card_cd + ", ";  // 카드사코드
            strLog += "cardname:" + card_name + ", ";  // 결제카드사명
            strLog += "cardbin:" + "" + ", ";   // 카드 BIN 번호
            strLog += "cardpoint:" + "" + ", "; // 카드 포인트 사용여부(0-미사용, 1-포인트사용, 2-세이브포인트사용)
            strLog += "codeID:" + "" + ", ";
            strLog += "buyr_name:" + "" + ", ";
            strLog += "UserName:" + sUserName + ", ";
            strLog += "Mobile:" + "" + ", ";
            strLog += "IsUser:" + "" + ", ";    // 회원(회원명), 비회원, 무기명
            strLog += "codename:" + "" + ", ";
            strLog += "Childkey:" + "" + ", ";
            strLog += "ChildMasterID:" + "" + ", ";
            strLog += "month:" + "" + ", ";
            strLog += "frequency:" + "" + ", ";
            strLog += "TypeName:" + "" + ", ";

            //-----------------------------------------------------------------------------
            // USER-AGENT 구분
            //-----------------------------------------------------------------------------
            string WebMode = Request.UserAgent.ToLower();
            if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
            {
                WebMode = "MOBILE";
            }
            else
            {
                WebMode = "PC";
            }
            strLog += "WebMode:" + WebMode + " ";

            ErrorLog.Write(HttpContext.Current, 705, strLog);
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성

            oi.authDate = app_time;
            oi.authNumber = app_no;
            oi.bank = bank_name.EmptyIfNull();
            oi.bankCode = bank_code.EmptyIfNull();
            oi.cardName = card_name;
            oi.cardPeriod = quota;
            oi.cardType = card_cd;
            oi.settlePrice = Convert.ToInt32(good_mny);
            oi.site_code = m_strCFG_site_cd;
            oi.payKey = tno;
            oi.payMethod = use_pay_method;
            oi.req_tx = req_tx;
            oi.resCD = m_strResCD;
            oi.resMsg = m_strResMsg;
            result = new StoreAction().Order(oi);
            if (result.success)
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        else
        {


            return false;
        }


    }

    //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리 - 결제 정보 관리 DoSave()
    bool DoSave_PayAccount()
    {
        string[] sResult = new string[3];
        var sGiroYN = "N";

        // 해외카드만 해당
        var action = new CommitmentAction();
        JsonWriter result = new JsonWriter();

        if (!UserInfo.IsLogin)
        {
            base.AlertWithJavascript("로그인이 필요한 서비스입니다.");
            return false;
        }

        var sess = new UserInfo();
        var sponsorId = sess.SponsorID;
        var sUserID = sess.UserId;
        var sUserName = sess.UserName;
        DataSet dsResult = new DataSet();

        var actionResult = new PaymentAction().GetReservations(1, 1000);
        if (!actionResult.success)
        {
            ErrorLog.Write(this.Context, 0, "해외카드 정기후원 저장에러 : " + actionResult.message);
            base.AlertWithJavascript(actionResult.message);
            return false;
        }

        DataTable data = (DataTable)actionResult.data;

        var tmon = DateTime.Now.Month.ToString().Replace("0", "");

        //정기 결제건
        List<DataRow> list = data.Select("FundingFrequency = '매월'").ToList();

        // 선물금 중 현재 월에 해당하는 것만
        List<DataRow> list2 = new List<DataRow>();
        foreach (DataRow dr in data.Rows)
        {
            if (dr["FundingFrequency"].ToString() != "매월")
            {
                if (dr["FundingFrequency"].ToString().Replace("매년 ", "").Replace("월", "") == tmon)
                {
                    list2.Add(dr);
                }
            }
        }

        var commitmentAction = new CommitmentAction();

        // 정기후원용
        var commitmentIds = "";
        // 선물금용
        var commitmentIds2 = "";

        foreach (DataRow dr in list)
        {
            commitmentIds += dr["CommitmentID"].ToString() + ",";
        }

        foreach (DataRow dr in list2)
        {
            commitmentIds2 += dr["CommitmentID"].ToString() + ",";
        }

        if (commitmentIds.Length > 0)
        {
            commitmentIds = commitmentIds.Substring(0, commitmentIds.Length - 1);
        }

        if (commitmentIds2.Length > 0)
        {
            commitmentIds2 = commitmentIds2.Substring(0, commitmentIds2.Length - 1);
        }

        var success = true;

        //list2는? 
        if (list.Count == 0)
        {
            ErrorLog.Write(this.Context, 1001, "정기후원 목록 조회 되지 않음 ");
            base.AlertWithJavascript("정기 후원 목록이 조회되지 않았습니다.");
            return false;
        }

        #region previous coding annotation
        // 기존 정기건(DataRow)과 신규 결제 commitmentId를 쌍으로 저장
        //Dictionary<DataRow, string> commitmentPairs = new Dictionary<DataRow, string>();

        //#region Commitment
        //foreach(DataRow dr in data.Rows) {


        //          var childMasterId = dr["childMasterId"].ToString();
        //	var commitmentId = dr["commitmentId"].ToString();
        //	var sponsorAmount = Convert.ToInt32(dr["amount"].ToString());
        //	var sponsorMonth = Convert.ToInt32(month);
        //	//use_pay_method
        //	var codeID = dr["AccountClass"].ToString();
        //	var campaignID = dr["campaignID"].ToString();

        //	if (codeID == "DS") {
        //		if (!commitmentAction.RegisterCDSPCommitmentOverseaCardAddOn(childMasterId, campaignID, sponsorId, sponsorAmount, sponsorMonth, ref actionResult)) {

        //			if (!string.IsNullOrEmpty(actionResult.data.ToString()))
        //				commitmentIds += "," + actionResult.data.ToString();

        //			ErrorLog.Write(this.Context, 0, "해외카드 정기후원 저장에러 : " + actionResult.message);
        //			base.AlertWithJavascript(actionResult.message);
        //			success = false;
        //			break;
        //		} else {
        //			commitmentIds += "," + actionResult.data.ToString();

        //			commitmentPairs.Add(dr, actionResult.data.ToString());
        //		}


        //	} else {
        //		if(!commitmentAction.RegisterCIVCommitment(sponsorId , "" , "" , sponsorAmount, sponsorMonth, use_pay_method , "1회" , codeID , childMasterId, campaignID, ref actionResult)) {

        //			if(!string.IsNullOrEmpty(actionResult.data.ToString()))
        //				commitmentIds += "," + actionResult.data.ToString();

        //			ErrorLog.Write(this.Context, 0, "해외카드 정기후원 저장에러 : " + actionResult.message);
        //			base.AlertWithJavascript(actionResult.message);
        //			success = false;
        //			break;

        //		} else {
        //			commitmentIds += "," + actionResult.data.ToString();

        //			commitmentPairs.Add(dr, actionResult.data.ToString());

        //		}

        //	}

        //}
        //#endregion

        //if(!success) {
        //	action.DeleteCommitment(commitmentIds);
        //	return false;
        //}

        // 결제 테스트용 
        //try
        //{
        //    // TODO 결제내역 저장
        //    _wwwService.RegisterPaymentMaster_Foreign(sponsorId
        //                                    , good_mny
        //                                    , "CVSF"   //parm_c_PP_CLI.m_f__get_res("card_cd")
        //                                    , "해외비자"    //parm_c_PP_CLI.m_f__get_res("card_name")
        //                                    , "00321275"    //parm_c_PP_CLI.m_f__get_res("app_no")
        //                                    , DateTime.Now.ToString("yyyyMMddhhmmss")    //parm_c_PP_CLI.m_f__get_res("app_time")
        //                                    , "10"
        //                                    , "20170707540174"    //parm_c_PP_CLI.m_f__get_res("tno")
        //                                    , "20170707540174"     //ordr_idxx
        //                                    , ""     //parm_c_PP_CLI.m_f__get_res("quota")
        //                                    , ""     //parm_c_PP_CLI.m_f__get_res("noinf")
        //                                    , ""
        //                                    , "Type:" + "test" + ", Desc:" + "test"
        //                                    , sUserID
        //                                    , sUserName);

        //    ordr_idxx = "20170707540174";
        //}
        //catch (Exception ex)
        //{
        //    ErrorLog.Write(this.Context, 0, ex.Message);
        //}


        //commitmentIds = commitmentIds.Substring(1);
        #endregion

        dsResult = _wwwService.PaymentWebCard_Foreign(req_tx, use_pay_method, ordr_idxx, good_name, good_mny
                                                        , buyr_name, buyr_mail, buyr_tel1, buyr_tel2, soc_no
                                                        , trad_numb, tran_cd, enct_info, enct_data, mod_desc
                                                        , mod_type, tno, m_strCustIP, sponsorId, sUserID
                                                        , sUserName, sDescription, sPaymentDay, sGiroYN, CodeAction.ChannelType);
        
        ErrorLog.Write(this.Context, 705, "kcp payment result:::::::::::::::::" + dsResult.ToJson());   // [jun.heo] 2017-12 : 결제 모듈 통합 - 코드를 705 로 변경

        //sResult[0] = dsResult.Tables[0].Rows[0]["ResCd"].ToString();
        sResult[0] = dsResult.Tables[0].Rows[0]["ResCd"].ToString();
        sResult[1] = dsResult.Tables[0].Rows[0]["ResMsg"].ToString();
        sResult[2] = ordr_idxx;

        //Response.Write(dsResult.ToJson());
        if (sResult[0] == "0000")
        {
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
            string strLog = string.Empty;
            strLog += "SponsorID:" + sponsorId + ", ";
            strLog += "OrderID:" + dsResult.Tables[0].Rows[0]["OrderIdxx"].ToString() + ", ";
            strLog += "UserID:" + sUserID + ", ";
            strLog += "CampaignID:" + "" + ", ";
            strLog += "Group:" + "" + ", ";
            strLog += "ResultCode:" + dsResult.Tables[0].Rows[0]["ResCd"].ToString() + ", ";
            strLog += "mid:" + "" + ", ";   // 가맹점 ID
            strLog += "tid:" + dsResult.Tables[0].Rows[0]["OrderTNo"].ToString() + ", ";   // 거래 ID
            strLog += "moid:" + dsResult.Tables[0].Rows[0]["OrderIdxx"].ToString() + ", ";  // 주문번호
            strLog += "amt:" + dsResult.Tables[0].Rows[0]["GoodMny"].ToString() + ", ";
            strLog += "cardcode:" + dsResult.Tables[0].Rows[0]["CardCd"].ToString() + ", ";  // 카드사코드
            strLog += "cardname:" + dsResult.Tables[0].Rows[0]["CardName"].ToString() + ", ";  // 결제카드사명
            strLog += "cardbin:" + "" + ", ";   // 카드 BIN 번호
            strLog += "cardpoint:" + "" + ", "; // 카드 포인트 사용여부(0-미사용, 1-포인트사용, 2-세이브포인트사용)
            strLog += "codeID:" + "" + ", ";
            strLog += "buyr_name:" + dsResult.Tables[0].Rows[0]["BuyerName"].ToString() + ", ";
            strLog += "UserName:" + "" + ", ";
            strLog += "Mobile:" + "" + ", ";
            strLog += "IsUser:" + dsResult.Tables[0].Rows[0]["BuyerName"].ToString() + ", ";    // 회원(회원명), 비회원, 무기명
            strLog += "codename:" + "" + ", ";
            strLog += "Childkey:" + "" + ", ";
            strLog += "ChildMasterID:" + "" + ", ";
            strLog += "month:" + "" + ", ";
            strLog += "frequency:" + "" + ", ";
            strLog += "TypeName:" + "" + ", ";

            //-----------------------------------------------------------------------------
            // USER-AGENT 구분
            //-----------------------------------------------------------------------------
            string WebMode = Request.UserAgent.ToLower();
            if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
            {
                WebMode = "MOBILE";
            }
            else
            {
                WebMode = "PC";
            }
            strLog += "WebMode:" + WebMode + " ";

            ErrorLog.Write(HttpContext.Current, 705, strLog);
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성

            //- CMS로 납부방법 변경시 당월 납부내역 있으면 당월 출금정지 처리 (중복 출금 제외)
            string sPaymentTypeName = paymentType;
            string stopResult = setPaymentStop(sess.SponsorID, sess.UserId, sess.UserName, sPaymentTypeName);

            // 파티셔닝 용 PaymentMasterID 조회
            string paymentMasterID = null;
            var sql = new object[1] { " SELECT top 1 PaymentMasterID FROM tPaymentMaster WHERE NotificationID = '" + ordr_idxx + "'" };
            DataSet pmi = _www6Service.NTx_ExecuteQuery("SqlCompass4", sql, "Text", null, null);
            if (pmi != null && pmi.Tables.Count > 0 && pmi.Tables[0].Rows.Count > 0)
                paymentMasterID = pmi.Tables[0].Rows[0][0].ToString();

            // PTD 업데이트 및 파티셔닝 처리
            updatePTD_PayAccount(list, Convert.ToInt32(month), paymentMasterID, "month"); //매월
            if (list2.Count > 0)
            {
                updatePTD_PayAccount(list2, 1, paymentMasterID, "year"); //매년
            }
        }
        else
        {
            //action.DeleteCommitment(commitmentIds);
            //base.AlertWithJavascript("에러가 발생했습니다." + "\\r\\n" + sResult[0] + "\\r\\n" + sResult[1] + "\\r\\n" + sResult[2]);

            ErrorLog.Write(this.Context, 1001, "해외카드결제오류:" + "\\r\\n" + sResult[0] + "\\r\\n" + sResult[1] + "\\r\\n" + sResult[2]);
            base.AlertWithJavascript("에러가 발생했습니다.");
            return false;
        }

        return true;
    }

    protected bool registerPartitioning_CommitmentGroup(string sponsorId, string sCommitmentIDs) {
        //----- Define --------------------------------------------------------
        string sNotificationID = ordr_idxx.ToString(); //NotifyCationID을 등록한다.
        string sResult = string.Empty;

        //----- Do Something --------------------------------------------------
        try {

            sResult = _wwwService.paymentPartitiong_CommitmentGroup(sNotificationID, sCommitmentIDs.Split(','), sponsorId, buyr_name);

            ErrorLog.Write(this.Context, 0, "kcp payment registerPartitioning_CommitmentGroup:::::::::::::::::" + sResult);

            return true;
        } catch (Exception ex) {
            ErrorLog.Write(this.Context, 0, ex.Message);
            return false;
        }
    }

    private bool updatePTD_Temporary(string sCommitmentIDs) {
        String sResult = String.Empty;

        try {


            var payInfo = pi.ToObject<PayItemSession.Entity>();

            string PTD = Convert.ToDateTime(DateTime.Now.AddMonths(payInfo.month).ToString("yyyy-MM-01")).AddDays(-1).ToString("yyyy-MM-dd");

            WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

            foreach (var sCommitmentID in sCommitmentIDs.Split(',')) {

                Object[] objSql = new object[1] { " SELECT SponsorItemEng " +
                                                " FROM   tCommitmentMaster " +
                                                " WHERE  CommitmentID = '" + sCommitmentID + "' " };

                DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

                if (ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "DS" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "LS" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "DSADD" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "LSADD") {

                        Object[] objSql2 = new object[1] { " UPDATE tCommitmentMaster " +
                                                        " SET    PTD = '" + PTD + "'" +
                                                        " WHERE  CommitmentID = '" + sCommitmentID + "' " };

                        DataSet iResult = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "Text", null, null);

                    }
                }

            }

            return true;
        } catch (Exception ex) {
            ErrorLog.Write(this.Context, 0, ex.Message);
            return false;
        }
    }

    private bool updatePTD_PayAccount(List<DataRow> commitmentPairs, int payMonth, string paymentMasterID, string periodType)
    {
        String sResult = String.Empty;
        UserInfo sess = new UserInfo();
        string sNotificationID = ordr_idxx.ToString(); //NotifyCationID을 등록한다.

        var PTD = "";

        try
        {
            var isUpdate = true;
            foreach (DataRow dr in commitmentPairs)
            {
                //DataRow dr = item.Key;
                //var commitmentId = dr["commitmentId"];
                //var new_commitmentId = item.Value;

                //PTD = Convert.ToDateTime(Convert.ToDateTime(dr["PTD"]).AddMonths(Convert.ToInt32(payMonth) + 1).ToString("yyyy-MM-01")).AddDays(-1).ToString("yyyy-MM-dd");
                if (periodType.Equals("month"))
                {
                    PTD = Convert.ToDateTime(Convert.ToDateTime(dr["PTD"])
                        .AddMonths(Convert.ToInt32(payMonth) + 1).ToString("yyyy-MM-01")).AddDays(-1).ToString("yyyy-MM-dd");
                }
                else if (periodType.Equals("year"))
                {
                    PTD = Convert.ToDateTime(Convert.ToDateTime(dr["PTD"])
                        .AddMonths(Convert.ToInt32(payMonth) + 1).ToString("yyyy-MM-01")).AddDays(-1).ToString("yyyy-MM-dd");
                }

                string commitmentId = dr["CommitmentID"].ToString();

                string testLog = "결제정보관리 해외카드 결제 : ";
                //testLog +=
                //    (string.IsNullOrEmpty(sess.SponsorID) ? "" : sess.SponsorID) + " | " +
                //    (commitmentId == null ? "null" : string.IsNullOrEmpty(commitmentId.ToString()) ? "" : commitmentId) + " | " +
                //    (string.IsNullOrEmpty(new_commitmentId) ? "" : new_commitmentId) + " | " +
                //    (string.IsNullOrEmpty(sNotificationID) ? "" : sNotificationID);
                //ErrorLog.Write(this.Context, 0, testLog);

                #region PTD 업데이트
                if (!string.IsNullOrEmpty(commitmentId))
                {
                    int iResult = 0;
                    if (periodType.Equals("month"))
                    {
                        Object[] objSql2 = new object[1] { "sp_update_PTD" };
                        Object[] objParam2 = new object[] { "DIVIS", "CommitmentID", "SponsorID", "PTD" };
                        Object[] objValue2 = new object[] { "COMMIT", commitmentId, sess.SponsorID, PTD };
                        iResult = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2);
                    }

                    var amount = Convert.ToInt32(dr["Amount"].ToString()) * payMonth; //?
                    _wwwService.registerDATPaymentPartitioning(sNotificationID, dr["ChildMasterID"].ToString(), dr["AccountClass"].ToString(), amount.ToString(), "temp", sess.UserName);

                    if (paymentMasterID != null)
                    {
                        var objSql = new object[1] { " UPDATE TPAYMENTPARTITIONING SET CommitmentID = '"+dr["CommitmentID"].ToString()+"', RegisterID = '"+sess.SponsorID+"' " +
                                            " WHERE PaymentMasterID = '"+paymentMasterID+"' AND SponsorID = '"+sess.SponsorID+"' AND ChildMasterID = '"+dr["ChildMasterID"].ToString()+"' " +
                                         " AND AccountClass='"+dr["AccountClass"].ToString()+"' AND PaymentDate='"+ DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00" +"'" +
                                         " AND CommitmentID IS NULL AND RegisterID ='temp' "};

                        _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
                    }





                    //objSql2 = new object[1] { "sp_update_PTD" };
                    //objParam2 = new object[] { "DIVIS", "CommitmentID", "SponsorID", "PTD" };
                    //objValue2 = new object[] { "COMMIT", new_commitmentId, sess.SponsorID, PTD };
                    //iResult = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2);

                    if (iResult > 0)
                    {

                        //Object[] objSql3 = new object[1] { "sp_update_PTD" };
                        //Object[] objParam3 = new object[] { "DIVIS", "NotificationID", "CommitmentID", "SponsorID", "PTD" };
                        //Object[] objValue3 = new object[] { "Partitioning", sNotificationID, new_commitmentId, sess.SponsorID, PTD };

                        //iResult = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql3, "SP", objParam3, objValue3);

                        //if(iResult < 1) {

                        //	isUpdate = false;
                        //	ErrorLog.Write(this.Context, 0, "PTD 정보를 업데이트 하던 도중 오류가 발생했습니다. (2)");
                        //	base.AlertWithJavascript("PTD 정보를 업데이트 하던 도중 오류가 발생했습니다. (2)");
                        //	break;
                        //}
                    }
                    else
                    {

                        isUpdate = false;

                        ErrorLog.Write(this.Context, 0, "PTD 정보를 업데이트 하던 도중 오류가 발생했습니다. (1)");
                        base.AlertWithJavascript("PTD 정보를 업데이트 하던 도중 오류가 발생했습니다. (1)");

                        break;
                    }
                }
                #endregion

            }

            if (paymentMasterID != null)
                _wwwService.UpdatePaymentMaster(paymentMasterID, "Y", DateTime.Now.ToString("yyyy-MM-dd"), sess.SponsorID, sess.UserName);

            //if(isUpdate) {

            //	Object[] objSql5 = new object[1] { "sp_update_PTD" };
            //	Object[] objParam5 = new object[] { "DIVIS", "SponsorID", "PTD" };
            //	Object[] objValue5 = new object[] { "UPDATE_Sponsor", sess.SponsorID, PTD };

            //	int iResult = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql5, "SP", objParam5, objValue5);

            //	if(iResult < 1) {
            //		isUpdate = false;

            //		ErrorLog.Write(this.Context, 0, "PTD 정보를 업데이트 하던 도중 오류가 발생했습니다. (4)");
            //		base.AlertWithJavascript("PTD 정보를 업데이트 하던 도중 오류가 발생했습니다. (4)");
            //		return false;
            //	}
            //}

            return true;
        }
        catch (Exception ex)
        {
            ErrorLog.Write(this.Context, 0, ex.Message);
            return false;
        }
    }

    private bool SetDeliquentTodayPayment(string xOrderNo)
    {
        //----- 세션선언
        UserInfo sess = new UserInfo();
        bool sResult_Bool = false;
        //----- 결제
        try
        {
            string UserID = string.Empty;
            string UserName = string.Empty;

            UserID = sess.SponsorID.ToString();
            UserName = sess.UserName.ToString();

            //수정 2013-01-03
            //DataSet ds1 = (DataSet)Session["ds1"];

            ErrorLog.Write(HttpContext.Current, 0, pi);
            var ds1 = pi.ToObject<DataSet>();
            //DataSet ds1 = (DataSet)Session["DataSet_" + sess.SponsorID];

            DataTable xPayOrder = new DataTable();
            xPayOrder.TableName = "PayOrderNo";
            xPayOrder.Columns.Add("PayOrderNo", typeof(string));
            xPayOrder.Rows.Add(xOrderNo);
            ds1.Tables.Add(xPayOrder);
            xPayOrder.Dispose();

            //수정 2013-01-03
            //Session.Remove("ds1");
            //	Session.Remove("DataSet_" + sess.SponsorID);

            //실행
            ErrorLog.Write(this.Context, 0, "_wwwService.SetDeliquentTodayPayment start");
            //string sResult = _wwwService.SetDeliquentTodayPayment(ds1, UserID, UserName);
            //UserName으로 테이블 생성시 오류로 인해 SponsorID로 테이블 생성으로 변경
            string sResult = _wwwService.SetDeliquentTodayPayment(ds1, UserID, UserID);
            ErrorLog.Write(this.Context, 0, "_wwwService.SetDeliquentTodayPayment finish " + sResult);
            if (sResult == "10")
            {
                sResult_Bool = true;
            }

        }
        catch (Exception ex)
        {

            ErrorLog.Write(this.Context, 0, ex.ToString());
            base.AlertWithJavascript("미납금 처리에 실패했습니다.");
            return false;
        }

        return sResult_Bool;
    }

    public string setPaymentStop(string sSponsorID, string sLoginID, string sLoginName, string sPaymentType)
    {
        WWWService.Service _service = new WWWService.Service();
        DataSet dsPaymentRequest = new DataSet();
        DataSet dsPaymentPaymentPartioning = new DataSet();
        string sPaymentMasterID = string.Empty;
        string sResult = string.Empty;

        DateTime dtNow = DateTime.Now;
        // DateTime.DaysInMonth(dtNow.Year, dtNow.Month).ToString() -- 월의 마지막일
        string sFromDate = dtNow.ToShortDateString().Substring(0, 8) + "01";
        string sToDate = dtNow.ToShortDateString().Substring(0, 10);

        //getData
        dsPaymentRequest = _service.listPaymentRequest_period(sSponsorID, sFromDate, sToDate, sPaymentType);

        if (dsPaymentRequest.Tables[0].Rows.Count > 0)
        {
            string sCommitmentID = string.Empty;
            string sRefuseID = string.Empty;

            for (int i = 0; i < dsPaymentRequest.Tables[0].Rows.Count; i++)
            {
                sPaymentMasterID = dsPaymentRequest.Tables[0].Rows[i]["PaymentMasterID"].ToString();

                dsPaymentPaymentPartioning = _service.getPaymentPartioning_paymentStop(sPaymentMasterID, "", "");

                //- 파티셔닝된 commitmentId 추출 
                if (dsPaymentPaymentPartioning.Tables[0].Rows.Count > 0)
                {
                    string sRefuseFrom = dtNow.ToShortDateString().Substring(0, 10) + " 00:00:00";
                    string sRefuseTo = dtNow.ToShortDateString().Substring(0, 8) + DateTime.DaysInMonth(dtNow.Year, dtNow.Month).ToString() + " 23:59:59";
                    string sPaymentDate = string.Empty;
                    string sCurrentUse = string.Empty;

                    foreach (DataRow row in dsPaymentPaymentPartioning.Tables[0].Rows)
                    {
                        sCommitmentID = row["CommitmentID"].ToString();

                        if (sPaymentType == "CARD")
                        {
                            if (row["AccountClass"].Equals("DS") || row["AccountClass"].Equals("LS"))
                            {
                                //- 당월 DS, LS 출금정지
                                #region 기존  N update
                                //- 'Y'면 정지, 'N'면 출금가능
                                sResult = _service.modifyPaymentRefuse_paymentChange(sCommitmentID, "N", sLoginID, sLoginName, "Reg");
                                sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "N", sLoginID, sLoginName);
                                #endregion

                                #region 신규  insert
                                //- 신규 출금정기기간ID 생성
                                //- 'Y'면 정지, 'N'면 출금가능
                                sRefuseID = _service.getTimeStamp_Number("0");
                                sResult = _service.registerPaymentRefuse(sCommitmentID, sRefuseID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                //----- tCommitmentMaster Update
                                if (sResult == "10")
                                {
                                    sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                }
                                #endregion
                            }
                            else
                            {
                                //- 당월 기타 후원금 출금정지 
                                #region 기존  N update
                                //- 'Y'면 정지, 'N'면 출금가능
                                sResult = _service.modifyPaymentRefuse_paymentChange(sCommitmentID, "N", sLoginID, sLoginName, "Reg");
                                sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "N", sLoginID, sLoginName);
                                #endregion

                                #region 신규  insert
                                //- 신규 출금정기기간ID 생성
                                //- 'Y'면 정지, 'N'면 출금가능
                                sRefuseID = _service.getTimeStamp_Number("0");
                                sResult = _service.registerPaymentRefuse(sCommitmentID, sRefuseID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                //----- tCommitmentMaster Update
                                if (sResult == "10")
                                {
                                    sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                }
                                #endregion
                            }
                        }
                        else if (sPaymentType == "CMS")
                        {
                            sPaymentDate = row["PaymentDate"].ToString();

                            if (sPaymentDate != null && sPaymentDate != "") //- 정산일자가 있는 것만 처리 
                            {
                                if (row["AccountClass"].Equals("DS") || row["AccountClass"].Equals("LS"))
                                {
                                    //- 당월 DS, LS 출금정지
                                    #region 기존  N update
                                    //- 'Y'면 정지, 'N'면 출금가능
                                    sResult = _service.modifyPaymentRefuse_paymentChange(sCommitmentID, "N", sLoginID, sLoginName, "Reg");
                                    sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "N", sLoginID, sLoginName);
                                    #endregion

                                    #region 신규  insert
                                    //- 신규 출금정기기간ID 생성
                                    //- 'Y'면 정지, 'N'면 출금가능
                                    sRefuseID = _service.getTimeStamp_Number("0");
                                    sResult = _service.registerPaymentRefuse(sCommitmentID, sRefuseID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                    //----- tCommitmentMaster Update
                                    if (sResult == "10")
                                    {
                                        sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    //- 당월 기타 후원금 출금정지 
                                    #region 기존  N update
                                    //- 'Y'면 정지, 'N'면 출금가능
                                    sResult = _service.modifyPaymentRefuse_paymentChange(sCommitmentID, "N", sLoginID, sLoginName, "Reg");
                                    sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "N", sLoginID, sLoginName);
                                    #endregion

                                    #region 신규  insert
                                    //- 신규 출금정기기간ID 생성
                                    //- 'Y'면 정지, 'N'면 출금가능
                                    sRefuseID = _service.getTimeStamp_Number("0");
                                    sResult = _service.registerPaymentRefuse(sCommitmentID, sRefuseID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                    //----- tCommitmentMaster Update
                                    if (sResult == "10")
                                    {
                                        sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
        }

        return sResult;
    }

    #region KCP 
    /* ==================================================================================== */
    /* +    METHOD : GET POST DATA                                                        + */
    /* - -------------------------------------------------------------------------------- - */
    private void m_f__get_param() {

        returnUrl.Value = m_f__get_post_data("returnUrl");
        overseas_card = m_f__get_post_data("overseas_card");

        ci = m_f__get_post_data("ci");
        di = m_f__get_post_data("di");
        jumin = m_f__get_post_data("jumin");

        campaignId = m_f__get_post_data("campaignId");
        codeId = m_f__get_post_data("codeId");
        codeName = m_f__get_post_data("codeName");

        month = m_f__get_post_data("month");
        motiveCode = m_f__get_post_data("motiveCode");
        motiveName = m_f__get_post_data("motiveName");
        pi = m_f__get_post_data("payInfo");

        req_tx = m_f__get_post_data("req_tx");
        use_pay_method = m_f__get_post_data("use_pay_method");
        ordr_idxx = m_f__get_post_data("ordr_idxx");
        good_name = m_f__get_post_data("good_name");
        good_mny = m_f__get_post_data("good_mny");

        card_cd = m_f__get_post_data("card_cd");

        buyr_name = m_f__get_post_data("buyr_name");
        buyr_tel1 = m_f__get_post_data("buyr_tel1");
        buyr_tel2 = m_f__get_post_data("buyr_tel2");
        buyr_mail = m_f__get_post_data("buyr_mail");
        soc_no = m_f__get_post_data("soc_no");

        /* - ---------------------------------------------------------------------------- - */
        trad_numb = m_f__get_post_data("trace_no");
        tran_cd = m_f__get_post_data("tran_cd");
        enct_info = m_f__get_post_data("enc_info");
        enct_data = m_f__get_post_data("enc_data");
        /* - ---------------------------------------------------------------------------- - */
        mod_type = m_f__get_post_data("mod_type");
        mod_mny = m_f__get_post_data("mod_mny");
        rem_mny = m_f__get_post_data("rem_mny");
        mod_tax_mny = m_f__get_post_data("mod_tax_mny");  // 공급가 부분 취소 요청 금액
        mod_vat_mny = m_f__get_post_data("mod_vat_mny");  // 부과세 부분 취소 요청 금액
        mod_free_mny = m_f__get_post_data("mod_free_mny"); // 비과세 부분 취소 요청 금액
        tno = m_f__get_post_data("tno");
        mod_desc = m_f__get_post_data("mod_desc");
        pnt_issue = m_f__get_post_data("pnt_issue");       // 포인트(OK캐쉬백,복지포인트)
                                                           /* - ---------------------------------------------------------------------------- - */
        m_strCustIP = Request.ServerVariables.Get("REMOTE_ADDR");
        /* -------------------------------------------------------------------------------- */
        cash_yn = m_f__get_post_data("cash_yn");         // 현금 영수증 등록 여부
        cash_authno = m_f__get_post_data("cash_authno");     // 현금 영수증 승인 번호
        cash_tr_code = m_f__get_post_data("cash_tr_code");    // 현금 영수증 발행 구분
        cash_id_info = m_f__get_post_data("cash_id_info");    // 현금 영수증 등록 번호
        isFirstPay = m_f__get_post_data("firstPay");              // 첫 결제 여부 추가 문희원

        //[jun.heo] 2017-12 : 결제 모듈 통합
        payPageType = m_f__get_post_data("payPageType");              // 결제 페이지 종류
        order_item = m_f__get_post_data("order_item");      // json
        //vitiontrip
        plandetail_id = m_f__get_post_data("plandetail_id");
        amount_type = m_f__get_post_data("amount_type");

        paymentType = m_f__get_post_data("paymentType");
    }
    /* ==================================================================================== */

    /* ==================================================================================== */
    /* +    METHOD : GET POST DATA                                                        + */
    /* - -------------------------------------------------------------------------------- - */
    private string m_f__get_post_data(string parm_strName) {
        string strRT;

        strRT = Request.Form[parm_strName];

        if (strRT == null)
            strRT = "";

        return strRT;
    }
    /* ==================================================================================== */

    /* ==================================================================================== */
    /* +    METHOD : 환경 설정                                                            + */
    /* - -------------------------------------------------------------------------------- - */
    private void m_f__load_env() {
        /* -------------------------------------------------------------------------------- */
        /* +    환경설정 DATA 설정                                                        + */
        /* - ---------------------------------------------------------------------------- - */
        /* GET WEB.CONFIG DATA */
        m_strCFG_paygw_url = ConfigurationManager.AppSettings["g_conf_gw_url"];
        m_strCFG_paygw_port = ConfigurationManager.AppSettings["g_conf_gw_port"];
        m_strCFG_key_path = ConfigurationManager.AppSettings["g_kcp_key_path"];
        m_strCFG_log_path = ConfigurationManager.AppSettings["g_kcp_log_path"];
        m_strSet_user_type = ConfigurationManager.AppSettings["g_conf_user_type"];

        // 신용카드
        if (use_pay_method == "100000000000") {

            if (overseas_card == "Y") {
                //[jongjin.lee] 2018-04 : 스토어에서 해외카드결제 추가. site_code, site_key 따로 사용
                if (payPageType == "Store")    // 스토어인 경우
                {
                    m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_site_cd_web_card_Foreign_Store"];
                    m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_site_key_web_card_Foreign_Store"];
                }
                else
                {
                    m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_site_cd_web_card_Foreign"];
                    m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_site_key_web_card_Foreign"];
                }
            } else {
                //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
                if (payPageType == "Store")    // 스토어인 경우
                {
                    m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_site_cd_shop_card"];
                    m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_site_key_shop_card"];
                }
                else
                {
                    m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_site_cd_web_card"];
                    m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_site_key_web_card"];
                }
            }

            // 실시간 계좌이체
        } else if (use_pay_method == "010000000000") {
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
            if (payPageType == "VisionTrip")    // 비전 트립인 경우
            {
                m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_site_cd_visiontrip_account"];
                m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_site_key_visiontrip_account"];
            }
            else if (payPageType == "Store")    // 스토어인 경우
            {
                m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_site_cd_shop_account"];
                m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_site_key_shop_account"];
            }
            else
            {
                m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_site_cd_account"];
                m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_site_key_account"];
            }
            // 휴대폰
        } else if (use_pay_method == "000010000000") {
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
            if (payPageType == "Store")    // 스토어인 경우
            {
                m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_site_cd_shop_phone"];
                m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_site_key_shop_phone"];
            }
            else
            {
                m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_site_cd_phone"];
                m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_site_key_phone"];
            }
        }
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
        else if (use_pay_method == "001000000000")  // 가상계좌인 경우
        {
            if (payPageType == "VisionTrip")    // 비전 트립인 경우
            {
                m_strCFG_site_cd = ConfigurationManager.AppSettings["g_conf_site_cd_visiontrip_virtual"];
                m_strCFG_site_key = ConfigurationManager.AppSettings["g_conf_site_key_visiontrip_virtual"];
            }
        }
        /* - ---------------------------------------------------------------------------- - */
    }
    /* ==================================================================================== */

    /* ==================================================================================== */
    /* +    METHOD : 요청 거래 처리                                                       + */
    /* - -------------------------------------------------------------------------------- - */
    private void m_f__do_tx() {
        C_PP_CLI_COM c_PP_CLI = new C_PP_CLI_COM();
        int nDataSetInx_req;
        bool bRT = false;
        bool bNetCan = false;

        /* -------------------------------------------------------------------------------- */
        /* +    초기화                                                                    + */
        /* - ---------------------------------------------------------------------------- - */
        m_strTxCD = "";
        nDataSetInx_req = 0;
        /* - ---------------------------------------------------------------------------- - */
        c_PP_CLI.m_f__set_env(m_strCFG_paygw_url, m_strCFG_paygw_port,
                               m_strCFG_log_path, m_strCFG_key_path);

        m_strCustIP = Request.ServerVariables.Get("REMOTE_ADDR");
        /* - ---------------------------------------------------------------------------- - */
        c_PP_CLI.m_f__init();
        /* -------------------------------------------------------------------------------- */

        /* -------------------------------------------------------------------------------- */
        /* +    요청 처리                                                                 + */
        /* - ---------------------------------------------------------------------------- - */
        if (req_tx.Equals("pay")) {
            nDataSetInx_req = m_f__set_dataset_pay(ref c_PP_CLI);
        }
        /* - ---------------------------------------------------------------------------- - */
        if (!m_strTxCD.Equals("")) {
            c_PP_CLI.m_f__do_tx(req_tx, m_strTxCD, nDataSetInx_req, "",
                                 m_strCFG_site_cd, m_strCFG_site_key, ordr_idxx);

            m_strResCD = c_PP_CLI.m_strResCD;
            m_strResMsg = c_PP_CLI.m_strResMsg;
        } else {
            m_strResCD = "9562";
            m_strResMsg = "지불모듈 연동 오류 (TX_CD) 가 정의되지 않았습니다.";
        }
        /* -------------------------------------------------------------------------------- */

        /* -------------------------------------------------------------------------------- */
        /* +    결과 처리                                                                 + */
        /* - ---------------------------------------------------------------------------- - */
        if (m_strResCD.Equals("0000")) {
            /* - ------------------------------------------------------------------------ - */
            if (req_tx.Equals("pay")) {
                /* - -------------------------------------------------------------------- - */
                bRT = m_f__to_do_shop_pay();          /* 정상 적립/조회/사용 거래 결과 처리 */
                                                      /* - -------------------------------------------------------------------- - */
                if (bRT == false) {
                    /* - ---------------------------------------------------------------- - */
                    /* +    TODO (주위) : 망상 취소 처리                                  + */
                    /* -- -------------------------------------------------------------- -- */
                    /* +    적립/사용 결과 처리 중 오류가 발생한 경우 자동 취소를         + */
                    /* +    원하실 경우 아래의 bNetCan 값을 true로 설정하여 주시기        + */
                    /* +    바랍니다. 취소 처리는 거래는 원복을 할 수 없으므로 주위       + */
                    /* +    하여 주시기 바랍니다.                                         + */
                    /* -- -------------------------------------------------------------- -- */

                    //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
                    if (payPageType == "Store")    // 스토어인 경우
                    {
                        bNetCan = true;
                    }
                    else
                    {
                        bNetCan = false;
                    }
                    /* - ---------------------------------------------------------------- - */
                }
                /* - -------------------------------------------------------------------- - */
                if (bNetCan == true) {
                    ErrorLog.Write(this.Context, 0, "KCP Store 결제 취소 : " + order_item); //[jun.heo] 2017-12 : 결제 모듈 통합
                    m_f__do_net_can(ref c_PP_CLI);
                } else {
                    m_f__disp_rt_pay_succ(ref c_PP_CLI);
                    //[jun.heo] 2017-12 : 결제 모듈 통합
                    bPaySuccess = true; // 비전트립인 경우 사용
                }
                /* - -------------------------------------------------------------------- - */
            }
            /* - ------------------------------------------------------------------------ - */
        } else {
            /* - ------------------------------------------------------------------------ - */
            m_f__to_do_shop_fail();
            /* - ------------------------------------------------------------------------ - */
            m_f__disp_rt_fail();
        }
        /* -------------------------------------------------------------------------------- */
    }
    /* ==================================================================================== */

    /* ==================================================================================== */
    /* +    METHOD : 망상 취소 처리                                                       + */
    /* - -------------------------------------------------------------------------------- - */
    private bool m_f__do_net_can(ref C_PP_CLI_COM parm_c_PP_CLI) {
        int nDataSetInx_req;
        bool bDoNetCan = false;

        /* -------------------------------------------------------------------------------- */
        /* +    망상 취소 DATA 설정                                                       + */
        /* - ---------------------------------------------------------------------------- - */
        mod_type = "STSC";
        tno = parm_c_PP_CLI.m_f__get_res("tno");
        /* - ---------------------------------------------------------------------------- - */
        parm_c_PP_CLI.m_f__init();
        /* - ---------------------------------------------------------------------------- - */
        if (req_tx.Equals("pay")) {

            bDoNetCan = true;
            mod_desc = "처리 오류로 인한 거래 자동 취소";
        }
        /* -------------------------------------------------------------------------------- */

        /* -------------------------------------------------------------------------------- */
        /* +    자동 취소 처리                                                            + */
        /* - ---------------------------------------------------------------------------- - */
        if (bDoNetCan == true) {
            nDataSetInx_req = m_f__set_dataset_mod(ref parm_c_PP_CLI);
            parm_c_PP_CLI.m_f__do_tx(req_tx, m_strTxCD, nDataSetInx_req, "",
                                      m_strCFG_site_cd, m_strCFG_site_key, ordr_idxx);
            m_strResCD = parm_c_PP_CLI.m_strResCD;
            m_strResMsg = parm_c_PP_CLI.m_strResMsg;

            m_f__disp_rt_can(ref parm_c_PP_CLI);
        }
        /* -------------------------------------------------------------------------------- */

        return bDoNetCan;
    }
    /* ==================================================================================== */

    /* ==================================================================================== */
    /* +    METHOD : 요청 DATA 생성                                                       + */
    /* - -------------------------------------------------------------------------------- - */
    private int m_f__set_dataset_pay(ref C_PP_CLI_COM parm_c_PP_CLI) {
        /* -------------------------------------------------------------------------------- */
        /* +    지불 요청 DATA 구성                                                       + */
        /* - ---------------------------------------------------------------------------- - */
        m_strTxCD = m_f__get_post_data("tran_cd");

        /* -------------------------------------------------------------------------------- */
        /* +    금액 위변조 방지 관련 추가 (2010.12)                                           + */
        /* - ---------------------------------------------------------------------------- - */
        int nDataSetInx_req;
        int nDataSetInx_ordr_no;

        if (good_mny.Trim().Length > 0) {

            nDataSetInx_req = parm_c_PP_CLI.m_f__get_dataset("plan_data");
            nDataSetInx_ordr_no = parm_c_PP_CLI.m_f__get_dataset("ordr_data");

            parm_c_PP_CLI.m_f__set_data(nDataSetInx_ordr_no, "ordr_mony", "1004");

            parm_c_PP_CLI.m_f__add_data(nDataSetInx_req, nDataSetInx_ordr_no, "\x1c");

        }

        return parm_c_PP_CLI.m_f__set_axdataset(trad_numb, enct_info, enct_data);
    }

    /* ==================================================================================== */
    /* +    METHOD : 요청 DATA 생성                                                       + */
    /* - -------------------------------------------------------------------------------- - */
    private int m_f__set_dataset_mod(ref C_PP_CLI_COM parm_c_PP_CLI) {
        int nDataSetInx_req;
        int nDataSetInx_mod;


        /* -------------------------------------------------------------------------------- */
        /* +    변경 요청 DATA 구성                                                       + */
        /* - ---------------------------------------------------------------------------- - */
        m_strTxCD = "00200000";
        /* - ---------------------------------------------------------------------------- - */
        nDataSetInx_req = parm_c_PP_CLI.m_f__get_dataset("plan_data");
        nDataSetInx_mod = parm_c_PP_CLI.m_f__get_dataset("mod_data");
        /* - ---------------------------------------------------------------------------- - */
        parm_c_PP_CLI.m_f__set_data(nDataSetInx_mod, "mod_type", mod_type);
        parm_c_PP_CLI.m_f__set_data(nDataSetInx_mod, "tno", tno);
        parm_c_PP_CLI.m_f__set_data(nDataSetInx_mod, "mod_ip", m_strCustIP);
        parm_c_PP_CLI.m_f__set_data(nDataSetInx_mod, "mod_desc", mod_desc);
        /* - ---------------------------------------------------------------------------- - */
        parm_c_PP_CLI.m_f__add_data(nDataSetInx_req, nDataSetInx_mod, "\x1c");
        /* -------------------------------------------------------------------------------- */

        return nDataSetInx_req;
    }
    /* ==================================================================================== */

    /* ==================================================================================== */
    /* +    METHOD : 정상 결제 결과 처리                                                  + */
    /* - -------------------------------------------------------------------------------- - */
    private bool m_f__to_do_shop_pay() {
        bool bRT = true;

        /* -------------------------------------------------------------------------------- */
        /* +    결과 처리                                                                 + */
        /* - ---------------------------------------------------------------------------- - */

        /* -------------------------------------------------------------------------------- */
        /* +    TODO (필수) : 처리 결과가 정상인 경우에는 반드시 bRT 값을 true로,         + */
        /* +                  오류가 발생한 경우에는 false 로 설정하여 주시기 바랍니다.   + */
        /* - ---------------------------------------------------------------------------- - */
        bRT = true;                  /* 정상 처리인 경우 : true, 오류가 발생한 경우 : false */
                                     /* -------------------------------------------------------------------------------- */

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
        if (payPageType == "Store")    // 스토어인 경우
        {
            bRT = DoSave_Store();
        }

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 비전트립인 경우
        if (bRT && (payPageType == "VisionTrip"|| payPageType == "Store"))
        {
            result.Value = "Y";
        }

        return bRT;
	}
	/* ==================================================================================== */


	/* ==================================================================================== */
	/* +    METHOD : 오류 결과 처리                                                       + */
	/* - -------------------------------------------------------------------------------- - */
	private bool m_f__to_do_shop_fail() {
		bool bRT = true;

		/* -------------------------------------------------------------------------------- */
		/* +    거래 종류 별 오류 결과 처리                                               + */
		/* - ---------------------------------------------------------------------------- - */
		if(req_tx.Equals("pay")) {
			/* - ------------------------------------------------------------------------ - */
			/* +    TODO : 승인 거래 오류 시 처리                                         + */
			/* - ------------------------------------------------------------------------ - */
		} else if(req_tx.Equals("mod")) {
			/* - ------------------------------------------------------------------------ - */
			/* +    TODO : 취소 거래 오류 시 처리                                         + */
			/* - ------------------------------------------------------------------------ - */
		}
		/* -------------------------------------------------------------------------------- */

		return bRT;
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 결과 출력 (적립/조회/사용 정상)                                      + */
	/* - -------------------------------------------------------------------------------- - */
	private void m_f__disp_rt_pay_succ( ref C_PP_CLI_COM parm_c_PP_CLI ) {
		/* -------------------------------------------------------------------------------- */
		/* +    정상 결과 출력                                                            + */
		/* - ---------------------------------------------------------------------------- - */
		res_cd = m_strResCD;
		res_msg = m_strResMsg;
		/* - ---------------------------------------------------------------------------- - */
		ordr_idxx = ordr_idxx;
		tno = parm_c_PP_CLI.m_f__get_res("tno");
		good_mny = good_mny;
		good_name = good_name;
		buyr_name = buyr_name;
		buyr_tel1 = buyr_tel1;
		buyr_tel2 = buyr_tel2;
		buyr_mail = buyr_mail;
		//coupon_mny = parm_c_PP_CLI.m_f__get_res("coupon_mny");  // 쿠폰금액

		/* - ---------------------------------------------------------------------------- - */
		if(use_pay_method == "100000000000") {
			//신용카드
			card_cd = parm_c_PP_CLI.m_f__get_res("card_cd");       // 카드사 코드
			card_name = parm_c_PP_CLI.m_f__get_res("card_name");     // 카드사 명
			app_time = parm_c_PP_CLI.m_f__get_res("app_time");      // 승인시간
			app_no = parm_c_PP_CLI.m_f__get_res("app_no");        // 승인번호
			noinf = parm_c_PP_CLI.m_f__get_res("noinf");         // 무이자 여부
			quota = parm_c_PP_CLI.m_f__get_res("quota");         // 할부 개월 수
			partcanc_yn = parm_c_PP_CLI.m_f__get_res("partcanc_yn");            //  부분취소가능여부
			card_bin_type_01 = parm_c_PP_CLI.m_f__get_res("card_bin_type_01");  //  카드구분1
			card_bin_type_02 = parm_c_PP_CLI.m_f__get_res("card_bin_type_02");  //  카드구분2
			card_mny = parm_c_PP_CLI.m_f__get_res("card_mny");       // 카드결제금액 
			pnt_issue = parm_c_PP_CLI.m_f__get_res("pnt_issue");       //포인트 서비스사

			if(pnt_issue == "SCSK" || pnt_issue == "SCWB") {
				// 복합합결제
				add_pnt = parm_c_PP_CLI.m_f__get_res("add_pnt");           // 발생 포인트
				use_pnt = parm_c_PP_CLI.m_f__get_res("use_pnt");           // 사용가능 포인트
				rsv_pnt = parm_c_PP_CLI.m_f__get_res("rsv_pnt");           // 적립 포인트
				pnt_app_time = parm_c_PP_CLI.m_f__get_res("pnt_app_time");      // 승인시간
				pnt_app_no = parm_c_PP_CLI.m_f__get_res("pnt_app_no");        // 승인번호
				pnt_amount = parm_c_PP_CLI.m_f__get_res("pnt_amount");        // 적립금액 or 사용금액
				pnt_issue = parm_c_PP_CLI.m_f__get_res("pnt_issue");
			}
		} else if(use_pay_method == "010000000000") {
			//계좌이체
			bank_name = parm_c_PP_CLI.m_f__get_res("bank_name");           // 은행명
			bank_code = parm_c_PP_CLI.m_f__get_res("bank_code");           // 은행코드
			bk_mny = parm_c_PP_CLI.m_f__get_res("bk_mny");           // 계좌이체결제금액
			app_time = parm_c_PP_CLI.m_f__get_res("app_time");            // 승인시간
		} else if(use_pay_method == "001000000000") {
			//가상계좌
			bankname = parm_c_PP_CLI.m_f__get_res("bankname");            // 입금할 은행 이름
			depositor = parm_c_PP_CLI.m_f__get_res("depositor");           // 입금할 계좌 예금주
			account = parm_c_PP_CLI.m_f__get_res("account");             // 입금할 계좌 번호
			va_date = parm_c_PP_CLI.m_f__get_res("va_date");               // 가상계좌 입금마감시간

            //[jun.heo] 2017-12 : 결제 모듈 통합
            bank_code = parm_c_PP_CLI.m_f__get_res("bankcode");           // 은행코드
            bk_mny = parm_c_PP_CLI.m_f__get_res("amount");               // 계좌이체결제금액
        } else if(use_pay_method == "000100000000") {
			//포인트
			add_pnt = parm_c_PP_CLI.m_f__get_res("add_pnt");          // 발생 포인트
			use_pnt = parm_c_PP_CLI.m_f__get_res("use_pnt");          // 사용가능 포인트
			rsv_pnt = parm_c_PP_CLI.m_f__get_res("rsv_pnt");          // 적립 포인트
			pnt_app_time = parm_c_PP_CLI.m_f__get_res("pnt_app_time");     // 승인시간
			pnt_app_no = parm_c_PP_CLI.m_f__get_res("pnt_app_no");       // 승인번호
			pnt_amount = parm_c_PP_CLI.m_f__get_res("pnt_amount");       // 적립금액 or 사용금액
			pnt_issue = parm_c_PP_CLI.m_f__get_res("pnt_issue");        // 포인트 결제사
		} else if(use_pay_method == "000010000000") {
			//휴대폰
			hp_app_time = parm_c_PP_CLI.m_f__get_res("hp_app_time");       // 휴대폰 승인시간
			commid = parm_c_PP_CLI.m_f__get_res("commid");              // 통신사 코드
			mobile_no = parm_c_PP_CLI.m_f__get_res("mobile_no");            // 휴대폰 번호
			app_time = parm_c_PP_CLI.m_f__get_res("app_time");             // 승인시간
		} else if(use_pay_method == "000000001000") {
			//상품권
			tk_van_code = parm_c_PP_CLI.m_f__get_res("tk_van_code");        // 발급사 코드
			tk_app_no = parm_c_PP_CLI.m_f__get_res("tk_app_no");          // 승인 번호
			app_time = parm_c_PP_CLI.m_f__get_res("tk_app_time");        // 승인시간
		} else if(use_pay_method == "000000000010") {
			//ARS
			ars_app_time = parm_c_PP_CLI.m_f__get_res("ars_app_time");      // ARS 승인시간
		}

		//현금영수증
		cash_yn = cash_yn;                                                  // 현금 영수증 등록 여부
		if(cash_yn == "Y") {

			cash_authno = parm_c_PP_CLI.m_f__get_res("cash_authno");       // 현금 영수증 승인 번호
			cash_tr_code = cash_tr_code;                                    // 현금 영수증 발행 구분
			cash_id_info = cash_id_info;                                    // 현금 영수증 등록 번호
		}
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 결과 출력 (오류)                                                     + */
	/* - -------------------------------------------------------------------------------- - */
	private void m_f__disp_rt_fail() {
		/* -------------------------------------------------------------------------------- */
		/* +    오류 결과 출력                                                            + */
		/* - ---------------------------------------------------------------------------- - */
		res_cd = m_strResCD;
		res_msg = m_strResMsg;
		/* -------------------------------------------------------------------------------- */
	}
	/* ==================================================================================== */

	/* ==================================================================================== */
	/* +    METHOD : 결과 출력 (취소)                                                     + */
	/* - -------------------------------------------------------------------------------- - */
	private void m_f__disp_rt_can( ref C_PP_CLI_COM parm_c_PP_CLI ) {
		/* -------------------------------------------------------------------------------- */
		/* +    오류 결과 출력                                                            + */
		/* - ---------------------------------------------------------------------------- - */
		res_cd = m_strResCD;
		res_msg = m_strResMsg;
		/* -------------------------------------------------------------------------------- */
	}
	/* ==================================================================================== */

	#endregion

}
