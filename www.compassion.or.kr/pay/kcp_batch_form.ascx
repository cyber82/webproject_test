﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="kcp_batch_form.ascx.cs" Inherits="pay_kcp_batch_form" %>
<asp:PlaceHolder runat="server" ID="ph_content" Visible="false">
		<div id="display_setup_message" style="display:none">
	<p class="txt">
	결제를 계속 하시려면 상단의 노란색 표시줄을 클릭 하시거나 <a href="http://pay.kcp.co.kr/plugin_new/file/KCPUXWizard.exe"><span>[수동설치]</span></a>를 눌러
	Payplus Plug-in을 설치하시기 바랍니다.
	[수동설치]를 눌러 설치하신 경우 새로고침(F5)키를 눌러 진행하시기 바랍니다.
	</p>
	</div>

		<script type="text/javascript">

		function m_Completepayment( FormOrJson, closeEvent ) {
			var frm = document.ex_frm;
            GetField( frm, FormOrJson ); // 위에서 만든 폼데이터에 결제창의 인증데이터 담기.
            
            if (frm.res_cd.value == "0000") {
            	frm.target = "hd_ex_frm";
                frm.submit(); 
            } else {

              //  alert( "[" + frm.res_cd.value + "] " + frm.res_msg.value );
                closeEvent();
            }
        }

	</script>

 <!--  테스트 시 : src="http://pay.kcp.co.kr/plugin/payplus_test.js"
						  src="https://pay.kcp.co.kr/plugin/payplus_test.js"
			  실결제 시 : src="http://pay.kcp.co.kr/plugin/payplus.js"
						  src="https://pay.kcp.co.kr/plugin/payplus.js"
	   테스트 시(UTF-8) : src="http://pay.kcp.co.kr/plugin/payplus_test_un.js"
						  src="https://pay.kcp.co.kr/plugin/payplus_test_un.js"
	   실결제 시(UTF-8) : src="http://pay.kcp.co.kr/plugin/payplus_un.js"
						  src="https://pay.kcp.co.kr/plugin/payplus_un.js"      -->

        <%--[jun.heo] 2017-12 : 결제 모듈 통합 - 표준 Web 결제 모듈 적용--%>
	    <script language='javascript' src='<%:this.ViewState["appFormUrl"].ToString() %>'></script> <%--표준 Web 결제 모듈 : 개발/운영에 따라 URL 처리--%>
        <%--<script language='javascript' src='https://pay.kcp.co.kr/plugin/payplus_web.jsp'></script>--%> <%--표준 Web 결제 모듈 운영 --%>
    	
		<%--<script language='javascript' src='https://pay.kcp.co.kr/plugin/payplus_test_un.js'></script>--%>

        <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
		<script type="text/javascript" src="/pay/kcp_batch.js"></script>
        <%--<script type="text/javascript" src="/sponsor/pay/regular/kcp_batch.js"></script>--%>

		<div style="display:none">
	<iframe id="hd_ex_frm" name="hd_ex_frm"></iframe>
    <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
	<form id="ex_frm" name="ex_frm" method="post" action="/pay/kcp_batch">
    <%--<form id="ex_frm" name="ex_frm" method="post" action="/sponsor/pay/regular/kcp_batch">--%>
		<!-- KCP -->
		<input type="hidden" id="ordr_idxx" name="ordr_idxx" value="<%:this.ViewState["orderId"].ToString() %>" />
		<input type="hidden" name="buyr_name" value="<%:this.ViewState["user_name"].ToString() %>"/>
		<input type="hidden" name="kcpgroup_id" value="<%:this.ViewState["kcpgroup_id"].ToString() %>" />
		<!-- 필수 항목 : 요청구분 -->
		<input type="hidden" name="req_tx"      value="pay"/>
		<!-- 결제 방법 : 인증키 요청(AUTH:CARD) -->
		<input type='hidden' name='pay_method' value='AUTH:CARD'>
		<!-- 인증 방식 : 공인인증(BCERT) -->
		<input type='hidden' name='card_cert_type' value='BATCH'>
		<!-- 테스트 결제시 : BA001 으로 설정, 리얼 결제시 : 부여받은 사이트코드 입력 -->
		<input type='hidden' name='site_cd' value='<%:this.ViewState["site_cd"].ToString() %>'>
		<!-- 필수 항목 : PULGIN 설정 정보 변경하지 마세요 -->
		<input type='hidden' name='module_type' value='01'>
		<!-- 필수 항목 : PLUGIN에서 값을 설정하는 부분으로 반드시 포함되어야 합니다. ※수정하지 마십시오.-->
		<input type='hidden' id="res_cd" name='res_cd'         value=''>
		<input type='hidden' id="res_msg" name='res_msg'        value=''>
		<input type='hidden' name='trace_no'       value=''>
		<input type='hidden' name='enc_info'       value=''>
		<input type='hidden' name='enc_data'       value=''>
		<input type='hidden' name='tran_cd'        value=''>
		<!-- 배치키 발급시 주민번호 입력을 결제창 안에서 진행 -->
		<input type='hidden' name='batch_soc'      value='Y'>
		<input type='hidden' id="payday" name='payday'      value=''>
		
		<input type='hidden' name='returnUrl'      value='<%:this.ViewState["returnUrl"].ToString() %>'>
		<input type='hidden' name='failUrl'      value='<%:this.ViewState["failUrl"].ToString() %>'>
		<input type='hidden' id="motiveCode" name='motiveCode'      value='<%:this.ViewState["motiveCode"].ToString() %>'>
		<input type='hidden' id="motiveName" name='motiveName'      value='<%:this.ViewState["motiveName"].ToString() %>'>
		<input type='hidden' id="payInfo" name='payInfo'      value='<%:this.ViewState["payInfo"].ToString() %>'>

        <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
        <input type='hidden' id="payPageType" name='payPageType'      value='<%:this.ViewState["payPageType"].ToString() %>'>
        <input type="hidden" name="paymentType" value="<%:this.ViewState["paymentType"].ToString() %>" />
		
        <%--이종진 2018-03-12 - 결제실패 시, pay-again 페이지로 가기위한 url을 들고다님--%>
        <input type='hidden' id="againUrl" name='againUrl'      value='<%:this.ViewState["againUrl"].ToString() %>'>

		<!--// KCP -->
	</form>
			</div>
</asp:PlaceHolder>