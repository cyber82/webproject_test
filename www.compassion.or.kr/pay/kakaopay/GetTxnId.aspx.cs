﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;

using LGCNS.KMpay.Merchant.Certification;
using System.Text;

public partial class pay_kakaopay_GetTxnId : System.Web.UI.Page
{
    protected String resultCode = String.Empty;
    protected String resultMsg = String.Empty;
    protected String txnId = String.Empty;
    protected String merchantTxnNum = String.Empty;
    protected String prDt = String.Empty;
    protected String channelType = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //인증서버 url
        String requestDealApproveUrl = ConfigurationManager.AppSettings["RequestDealApproveUrl"] + ConfigurationManager.AppSettings["RequestDealPage"];

        String PR_TYPE = Request.Form["prType"];
        String MERCHANT_ID = Request.Form["MID"];
        String MERCHANT_TXN_NUM = Request.Form["merchantTxnNum"];

        //use UTF-8 encoding for Korean
        String PRODUCT_NAME = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(Request.Form["GoodsName"]));
        String AMOUNT = Request.Form["Amt"];
        String SUPPLY_AMT = Request.Form["SupplyAmt"];
        String channelType = Request.Form["channelType"];
        String CURRENCY = Request.Form["currency"];
        String RETURN_URL = Request.Form["returnUrl"];

        String merchantEncKey = Request.Form["merchantEncKey"];
        String merchantHashKey = Request.Form["merchantHashKey"];
        String CERTIFIED_FLAG = Request.Form["certifiedFlag"];

        String POSSI_CARD = Request.Form["possiCard"];
        String MAX_INT = Request.Form["maxInt"];
        String FIXED_INT = Request.Form["fixedInt"];
        String NO_INT_YN = Request.Form["noIntYN"];
        String NO_INT_OPT = Request.Form["noIntOpt"];

        String OFFER_PERIOD = Request.Form["offerPeriod"];
        String OFFER_PERIOD_FLAG = Request.Form["offerPeriodFlag"];

        if (string.IsNullOrEmpty(POSSI_CARD) || string.IsNullOrEmpty(FIXED_INT) || FIXED_INT == "00" || NO_INT_YN != "Y")
        {
            NO_INT_OPT = "";
        }
        else
        {
            NO_INT_OPT = "CC" + POSSI_CARD + FIXED_INT; 
        }

        String pointUseYN = Request.Form["pointUseYn"];
        String blockCard = Request.Form["blockCard"];

        if (string.IsNullOrEmpty(MERCHANT_TXN_NUM)) MERCHANT_TXN_NUM = Request.Form["Moid"];
            
        //인증요청 정보
        Dictionary<string, string> requestData = new Dictionary<string, string>();

        requestData.Add("PR_TYPE", PR_TYPE);                    //결제 요청 타입
        requestData.Add("MERCHANT_ID", MERCHANT_ID);            //가맹점 ID
        requestData.Add("MERCHANT_TXN_NUM", MERCHANT_TXN_NUM);  //가맹점 거래번호
        requestData.Add("PRODUCT_NAME", PRODUCT_NAME);          //상품명
        requestData.Add("AMOUNT", AMOUNT);                      //상품금액(총거래금액) (총거래금액 = 공급가액 + 부가세 + 봉사료)
        requestData.Add("SUPPLY_AMT", SUPPLY_AMT);
        requestData.Add("GOODS_VAT", "0");
        requestData.Add("SERVICE_AMT", "0");
        requestData.Add("channelType", channelType);

        requestData.Add("CURRENCY", CURRENCY);                  //거래통화(KRW/USD/JPY 등)
        requestData.Add("CERTIFIED_FLAG", CERTIFIED_FLAG);      //가맹점 인증 구분값 ("N","NC")

        requestData.Add("OFFER_PERIOD", OFFER_PERIOD);          //상품제공기간
        requestData.Add("OFFER_PERIOD_FLAG", OFFER_PERIOD_FLAG);//상품제공기간플래그

        requestData.Add("NO_INT_YN", NO_INT_YN);                //무이자 설정
        requestData.Add("NO_INT_OPT", NO_INT_OPT);              //무이자 옵션
        requestData.Add("MAX_INT", MAX_INT);                    //최대할부개월
        requestData.Add("FIXED_INT", FIXED_INT);                //고정할부개월

        requestData.Add("POINT_USE_YN", pointUseYN);            //카드사포인트사용여부
        requestData.Add("POSSI_CARD", POSSI_CARD);              //결제가능카드설정
        requestData.Add("BLOCK_CARD", blockCard);               //금지카드설정

        //requestData.Add("PAYMENT_HASH", string.Empty);          //dll 내부에서 처리

        requestData.Add("RETURN_URL", Request.Form["ReturnUrl"]);   //금지카드설정

        //웹서비스 객체 생성
        MPayCallWebService webService = new MPayCallWebService(merchantEncKey, merchantHashKey, requestDealApproveUrl);

        Dictionary<string, string> resultObj = webService.DealConfirmMerchant(requestData);

        // 결과값을 담는 부분
        resultCode = resultObj["RESULT_CODE"];
        resultMsg = resultObj["RESULT_MSG"];
        txnId = resultObj["TXN_ID"];
        prDt = resultObj["PR_DT"];
    }
}
