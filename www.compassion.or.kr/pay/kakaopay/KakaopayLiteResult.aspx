﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="KakaopayLiteResult.aspx.cs" Inherits="pay_kakaopay_KakaopayLiteResult" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CNSPay 결제 요청 결과  페이지</title>
	<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript">

		// 결제 중 새로고침 방지 샘플 스크립트
		function noRefresh() {
			/* CTRL + N키 막음. */
			if ((event.keyCode == 78) && (event.ctrlKey == true)) {
				event.keyCode = 0;
				return false;
			}
			/* F5 번키 막음. */
			if (event.keyCode == 116) {
				event.keyCode = 0;
				return false;
			}
		}
		document.onkeydown = noRefresh;

		$(function () {

			if ($("#result").val() == "Y") {
				//var url = ($("#returnUrl").val() == "") ? "complete" : $("#returnUrl").val();
			
				parent.location.href = $("#returnUrl").val();
			}

		})
	</script>

</head>
<body>
	<form runat="server" id="frm">
	<input type="hidden" id="result" runat="server" value="" />
	<input type="hidden" id="returnUrl" runat="server" value="" />

    <b>결제 내역입니다.</b><br />
    <ul>
        <li>결과 내용 : [<%=(resultCode)%>] <%=resultMsg%></li>
        <li>결제 수단 : <%=(payMethod) %></li>
        <li>상품명 : <%=goodsName %></li>
        <li>금액 : <%=amt %> 원</li>
        <li>TID : <%=tid%></li>
        <li>MID : <%= mid %></li>
        <li>가맹점거래번호 : <%= moid %></li> <!-- moid = merchant_txn_num -->
        <li>카드사명 : <%= cardName %></li>
        <li>할부개월 : <%= cardQuota %></li>
        <li>카드사 코드 : <%= cardCode %></li>
        <li>무이자여부 : <%= cardInterest %></li>
        <li>체크카드여부 : <%= cardCl %></li>
        <li>카드BIN번호 : <%= cardBin %></li>
        <li>카드사포인트사용여부 : <%= cardPoint %></li>
        <li>승인일시 : <%= authDate %></li>
        <li>승인번호 : <%= authCode %></li>
        <li>부인방지토큰 : <textarea rows="5" cols="45" readonly="readonly" style="resize:none;" name="contents"><%=nonRepToken %>&nbsp;</textarea></li>
    </ul>
	</form>
</body>
</html>
