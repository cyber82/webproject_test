﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using LGCNS.CNSPay.Service;
using System.Data;
using System.Text;

public partial class pay_kakaopay_KakaopayLiteResult : FrontBasePage{
    protected String buyerName = String.Empty;       // 구매자명
    protected String goodsName = String.Empty;      // 상품명

    protected String resultCode = String.Empty;         // 결과코드 (정상 :3001 , 그 외 에러)
    protected String resultMsg = String.Empty;          // 결과메시지
    protected String authDate = String.Empty;           // 승인일시 YYMMDDHH24mmss
    protected String authCode = String.Empty;           // 승인번호
    protected String payMethod = String.Empty;       // 결제수단
    protected String mid = String.Empty;                // 가맹점ID
    protected String tid = String.Empty;                  // 거래ID
    protected String moid = String.Empty;               // 주문번호
    protected String amt = String.Empty;                 // 금액

    protected String cardCode = String.Empty;         // 카드사 코드
    protected String cardName = String.Empty;        // 결제카드사명
    protected String cardQuota = String.Empty;        // 00:일시불,02:2개월
    protected String cardInterest = String.Empty;      // 무이자 여부  (0:일반, 1:무이자)
    protected String cardCl = String.Empty;             // 체크카드여부 (0:일반, 1:체크카드)
    protected String cardBin = String.Empty;           // 카드BIN번호
    protected String cardPoint = String.Empty;         // 카드사포인트사용여부 (0:미사용, 1:포인트사용, 2:세이브포인트사용)
    protected String cardNo = String.Empty;

    protected String nonRepToken = String.Empty;

    protected string isFirstPay = string.Empty;     // 첫 결제 여부

	string sellerOrderReferenceKey, orderCertifyKey;
	
	public string pi, jumin, ci, di, month, buyr_name;

    //[jun.heo] 2017-12 : 결제 모듈 통합
    public string payPageType;
    string ordr_idxx;
    public uint amount;

    WWWService.Service _wwwService = new WWWService.Service();
	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

	public override bool RequireSSL {
		get {
			return true;
		}
	}
	
	protected override void OnBeforePostBack() {

		result.Value = "N";
		returnUrl.Value = Request.Form["returnUrl"];
		KakaoPayResult();
    }

    public void KakaoPayResult()
    {
        CnsPayWebConnector connector = new CnsPayWebConnector();
        //결제 처리 경로(DB 또는 Config 파일로 관리한다.)
        connector.RequestUrl = ConfigurationManager.AppSettings["RequestDealPaymentUrl"];

        // 1. 로그 디렉토리 생성 : cnsPayHome/log 로 생성
        connector.SetCnsPayHome();

        // 2. 요청 페이지 파라메터 셋팅
        connector.SetRequestData();

        // 3. 추가 파라메터 셋팅
        connector.AddRequestData("actionType", "PY0");  // actionType : CL0 취소, PY0 승인, CI0 조회
        connector.AddRequestData("MallIP", Request.ServerVariables["LOCAL_ADDR"]);  // 가맹점 고유 ip
        connector.AddRequestData("CancelPwd", "123456");

        // 필요 시 전문 항목 추가
        connector.AddRequestData("SupplyAmt", Request["SupplyAmt"]);
        connector.AddRequestData("GoodsVat", "0");
        connector.AddRequestData("ServiceAmt", "0");

        //가맹점키 셋팅 (MID 별로 틀림) - 가맹점키 값은 가맹점에서 DB 또는 Config 파일로 관리한다.
        String EncodeKey = ConfigurationManager.AppSettings["EncodeKey"];
        connector.AddRequestData("EncodeKey", EncodeKey);

        // 4. CNSPAY Lite 서버 접속하여 처리
        connector.RequestAction();

        // 5. 결과 처리
        resultCode = connector.GetResultData("ResultCode");     // 결과코드 (정상 :3001 , 그 외 에러)
        resultMsg = connector.GetResultData("ResultMsg");       // 결과메시지
        authDate = connector.GetResultData("AuthDate");         // 승인일시 YYMMDDHH24mmss
        authCode = connector.GetResultData("AuthCode");         // 승인번호
        buyerName = connector.GetResultData("BuyerName");       // 구매자명
        goodsName = connector.GetResultData("GoodsName");       // 상품명
        payMethod = connector.GetResultData("PayMethod");       // 결제수단
        mid = connector.GetResultData("MID");                   // 가맹점ID
        tid = connector.GetResultData("TID");                   // 거래ID
        moid = connector.GetResultData("Moid");                 // 주문번호
        amt = connector.GetResultData("Amt");                   // 금액
        cardCode = connector.GetResultData("CardCode");		      // 카드사 코드
        cardName = connector.GetResultData("CardName");         // 결제카드사명
        cardQuota = connector.GetResultData("CardQuota");       // 할부개월수 ex) 00:일시불,02:2개월
        cardInterest = connector.GetResultData("CardInterest"); // 무이자 여부 (0:일반, 1:무이자)
        cardCl = connector.GetResultData("CardCl");             // 체크카드여부 (0:일반, 1:체크카드)
        cardBin = connector.GetResultData("CardBin");           // 카드BIN번호
        cardPoint = connector.GetResultData("CardPoint");       // 카드사포인트사용여부 (0:미사용, 1:포인트사용, 2:세이브포인트사용)
													//부인방지토큰값
		nonRepToken = Request.Form["NON_REP_TOKEN"];

        isFirstPay = Request.Form["firstPay"];

        payPageType = Request.Form["payPageType"];  //[jun.heo] 2017-12 : 결제 모듈 통합

        Boolean paySuccess = false;		// 결제 성공 여부

        /** 위의 응답 데이터 외에도 전문 Header와 개별부 데이터 Get 가능 */
        //신용카드
        if (payMethod.Equals("CARD"))
        {
            // 결과코드 (정상 :3001 , 그 외 에러)
            if (resultCode.Equals("3001"))
            {
                paySuccess = true;
		
			}
		}

		if (paySuccess)
        {
            pi = Request["payInfo"];
            jumin = Request["jumin"];
            ci = Request["ci"];
            di = Request["di"];
            month = Request["month"];
            buyr_name = Request["buyr_name"];

            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리
            // 결제 종류별 사전 처리 및 결제 종류에 맞는 "DoSave_XXXX()" 호출
            switch (payPageType)
            {
                case "PayDelay":    // 지연된 후원금
                    ordr_idxx = Request["ordr_idxx"];

                    if (this.DoSave_PayDelay())
                    {
                        result.Value = "Y";
                    }
                    break;
                default:
                case "Temporary":   // 일시후원 : 특별한 모금 등
                    orderCertifyKey = tid;
                    sellerOrderReferenceKey = Request["ordr_idxx"];

                    if (this.DoSave_Temporary())
                    {
                        result.Value = "Y";
                    }
                    break;
            }
        }
        else
        {
            ErrorLog.Write(HttpContext.Current, 0, "kakako 에러 : " +  resultCode + " : " + resultMsg + " : "+ amt);
			result.Value = "N";
			base.AlertWithJavascript(resultMsg);
		}
    }
	
	bool DoSave_Temporary() {

		var sponsorId = "";
		var sUserID = "";
		var sUserName = "";

        string refUrl = "";
        if (Request.UrlReferrer != null)
            refUrl = Request.UrlReferrer.ToString();

        string firstPay = "0";
        if (refUrl.IndexOf("pay_again") > 0 || isFirstPay == "1")
            firstPay = "1";

        //신규 GlobalPool 조회방법인지 체크
        string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();


        var action = new CommitmentAction();
		JsonWriter result = new JsonWriter();
		var payInfo = pi.ToObject<PayItemSession.Entity>();
		DataSet dsResult = new DataSet();

        string mobile = "";
		if(UserInfo.IsLogin) {
			var sess = new UserInfo();
			sponsorId = sess.SponsorID;
			sUserID = sess.UserId;
			sUserName = sess.UserName;

			payInfo.sponsorId = sponsorId;
			payInfo.sponsorName = sUserName;
			payInfo.userId = sUserID;
            mobile = sess.Mobile;
		} else {

			#region  로그인 되어있지 않으면 비회원 결제를 위해 sponsorId 구하기

			if(buyr_name == "무기명") {
				sponsorId = "20160621142650372";

			} else {
				// 해외회원 , 무기명인경우는 CI가 없음
				if(!string.IsNullOrEmpty(ci)) {
					Object[] objSql = new object[1] { string.Format("SELECT SponsorID FROM tSponsorMaster WHERE CurrentUse = 'Y' and (userId is null or userId = '') and CI <> '' and CI = '{0}'", ci) };
					DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

					if(ds.Tables[0].Rows.Count > 0) {
						sponsorId = ds.Tables[0].Rows[0]["SponsorID"].ToString().Trim();
					}
				}

				// sponsorId 가 없으면 새로 생성
				if(string.IsNullOrEmpty(sponsorId)) {

					try {

						sponsorId = _wwwService.GetDate().ToString("yyyyMMddHHmmssff2");
                        /*
						objSql = new object[1] { string.Format(@"insert into tSponsorMaster (sponsorId , sponsorName ,juminId ,channelType , currentUse ,userDate , userClass , registerDate , modifyDate , di , ci) values ('{0}','{1}','{2}','Web' , 'Y' , getdate() , '비회원' , getdate() , getdate() , '{3}' , '{4}') ",
							sponsorId , buyr_name , jumin , di , ci) };
						//	Response.Write(objSql[0].ToJson());
						//	return;
						_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
						*/

                        // tSponsorMaster에 LetterPreference, CorrReceiveType 컬럼은 default값으로 처리 ('Digital', 'online')
                        var sponsorResult = _wwwService.registerDATSponsor2(sponsorId, buyr_name, jumin, "", "", "", "", (string.IsNullOrEmpty(ci)) ? "국외" : "국내", "", "", ""
										   , "", "", CodeAction.GetGenderByJumin(jumin) /*gender*/
										   , "", "", "", "", ""
										   , "", "", "", "", DateTime.Now.ToString(), "14세이상", string.IsNullOrEmpty(ci) ? "" : DateTime.Now.ToString()
										   , string.IsNullOrEmpty(ci) ? "" : "서울신용평가", sponsorId, buyr_name, "", "", "", "", "", "", di, ci, "0");

						if(sponsorResult.Substring(0, 2) == "30") {

							base.AlertWithJavascript("회원님의 정보를 등록하지 못했습니다..(DAT) \\r\\n" + sponsorResult.Substring(2).ToString().Replace("\n", ""));
							return false;
						}
					} catch(Exception ex) {
						ErrorLog.Write(this.Context, 0, ex.Message);
						base.AlertWithJavascript("비회원 정보를 등록하는 중에 에러가 발생했습니다.");
						return false;
					}

				}
			}
			#endregion

			payInfo.sponsorId = sponsorId;
			payInfo.sponsorName = buyr_name;
			payInfo.userId = "";
		}

        //Pay Log 생성 (결제모듈 응답)
        StringBuilder sb = new StringBuilder();
        sb.Append("sponsorId:" + sponsorId + ", ");
        sb.Append("sUserID:" + sUserID + ", ");
        sb.Append("payInfo.campaignId:" + payInfo.campaignId.EmptyIfNull() + ", ");
        sb.Append("payInfo.group:" + payInfo.group.EmptyIfNull() + ", ");
        sb.Append("resultCode:" + resultCode + ", ");
        sb.Append("resultMsg:" + resultMsg + ", ");
        sb.Append("authDate:" + authDate + ", ");
        sb.Append("authCode:" + authCode + ", ");
        sb.Append("buyerName:" + buyerName + ", ");
        sb.Append("goodsName:" + goodsName + ", ");
        sb.Append("payMethod:" + payMethod + ", ");
        sb.Append("mid:" + mid + ", ");
        sb.Append("tid:" + tid + ", ");
        sb.Append("moid:" + moid + ", ");
        sb.Append("amt:" + amt + ", ");
        sb.Append("cardCode:" + cardCode + ", ");
        sb.Append("cardName:" + cardName + ", ");
        sb.Append("cardQuota:" + cardQuota + ", ");
        sb.Append("cardInterest:" + cardInterest);
        sb.Append("cardCl:" + cardCl + ", ");
        sb.Append("cardBin:" + cardBin + ", ");
        sb.Append("cardPoint:" + cardPoint + ", ");
        sb.Append("codeId:" + payInfo.codeId.EmptyIfNull() + ", ");
        sb.Append("userName:" + sUserName + ", ");
        sb.Append("mobile:" + mobile + ", ");

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
        sb.Append("Childkey:" + payInfo.childKey + ", ");
        sb.Append("ChildMasterID:" + payInfo.childMasterId + ", ");
        sb.Append("month:" + payInfo.month + ", ");
        sb.Append("frequency:" + payInfo.frequency + ", ");
        sb.Append("TypeName:" + payInfo.TypeName + ", ");

        //-----------------------------------------------------------------------------
        // USER-AGENT 구분
        //-----------------------------------------------------------------------------
        string WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
        {
            WebMode = "MOBILE";
        }
        else
        {
            WebMode = "PC";
        }
        sb.Append("WebMode:" + WebMode + " ");
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성

        ErrorLog.Write(HttpContext.Current, 601, sb.ToString());

        if (payInfo.group == "CDSP") {
			result = action.DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName, "", "", payInfo.amount, payInfo.month, firstPay);
		} else if(payInfo.group == "GIFT") {
			result = action.DoGIFT(sponsorId, payInfo.childMasterId, payInfo.amount.ToString(), "kakaopay", month, "일시", payInfo.codeId);
            if (result.success)
                payInfo.commitmentId = result.data.ToString();
        } else {

            // 일반후원 첫 결제의 경우 일반 결연목록 조회 후 commitmentid 가져온다.
            if (firstPay == "1" || string.IsNullOrEmpty(payInfo.commitmentId))
            {
                int cnt = 0;
                string commitmentID = string.Empty;
                try
                {
                    DataSet dsActiveChild = _wwwService.listActiveCommitment(sponsorId, "0000000000", "", "");
                    if (dsActiveChild == null || dsActiveChild.Tables.Count == 0 || dsActiveChild.Tables[0].Rows.Count == 0)
                        cnt = 0;
                    else
                    {
                        DataRow[] alreadyRow = dsActiveChild.Tables[0].Select("SponsorItemEng = '" + payInfo.codeId + "' and FundingFrequency <> '1회' and CampaignID='" + payInfo.campaignId + "'");
                        cnt = alreadyRow.Length;
                        commitmentID = alreadyRow[0]["CommitmentID"].ToString();
                    }

                }
                catch (Exception ex)
                {
                    ErrorLog.Write(HttpContext.Current, 8000, ex.Message);
                }

                if (cnt == 0)
                {
                    result = action.DoCIV(sponsorId, payInfo.amount, payInfo.month, "kakaopay", payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.childMasterId, payInfo.campaignId, "", "");
                    if (result.success)
                        payInfo.commitmentId = result.data.ToString();
                    else
                    {
                        result.success = false;
                        result.message = "후원 정보 저장에 실패했습니다.";
                    }
                }
                else
                {
                    //정기후원이면서 이미 납부중인 양육보완프로그램이 있을경우 (일시후원은 항상 등록가능)
                    payInfo.commitmentId = commitmentID;
                    result.success = true;
                }
            }
            else
            {
                result = action.DoCIV(sponsorId, payInfo.amount, payInfo.month, "kakaopay", payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.childMasterId, payInfo.campaignId, "", "");
                if (result.success)
                    payInfo.commitmentId = result.data.ToString();
                else
                {
                    result.success = false;
                    result.message = "후원 정보 저장에 실패했습니다.";
                }
            }
		}
		//	Response.Write(result.ToJson());
		if(!result.success) {

			//action.DeleteCommitment(result.data.ToString());
			base.AlertWithJavascript(result.message);
			return false;
		}
        ErrorLog.Write(this.Context, 0, "payInfo.group == " + payInfo.group);
        if (payInfo.group == "CDSP")
        {
            // CommitmentID 조회
            ChildAction child = new ChildAction();
            var childInfo = child.GetChild(payInfo.childMasterId);

            if (childInfo.success)
            {
                ChildAction.ChildItem item = (ChildAction.ChildItem)childInfo.data;
                string childKey = item.ChildKey;

                var childDetail = child.MyChildren(childKey, 1, 1);
                if (childDetail.success)
                {
                    List<ChildAction.MyChildItem> detail = (List<ChildAction.MyChildItem>)childDetail.data;
                    if (detail == null || detail.Count == 0)
                    {
                        ErrorLog.Write(this.Context, 0, "어린이 결연 정보 조회에 실패했습니다. " + payInfo.sponsorId + " " + payInfo.sponsorName + " " + payInfo.childMasterId);
                        base.AlertWithJavascript("어린이 결연 정보 조회에 실패했습니다.");
                        return false;
                    }
                    payInfo.commitmentId = detail[0].commitmentId;
                }
                else
                {
                    ErrorLog.Write(this.Context, 0, "어린이 결연 정보 조회에 실패했습니다. " + payInfo.sponsorId + " " + payInfo.sponsorName + " " + payInfo.childMasterId);
                    base.AlertWithJavascript("어린이 결연 정보 조회에 실패했습니다.");
                    return false;
                }
            }
            else
            {
                ErrorLog.Write(this.Context, 0, "어린이 정보 조회에 실패했습니다. " + payInfo.sponsorId + " " + payInfo.sponsorName + " " + payInfo.childMasterId);
                base.AlertWithJavascript("어린이 정보 조회에 실패했습니다.");
                return false;
            }
        }


		// 후원아이디와 비회원의 경우 sponsorId,  이름 업데이트
		//payInfo.commitmentId = result.data.ToString();
		new PayItemSession.Store().Update(sellerOrderReferenceKey, payInfo , null);
		
		string sResult = "";
		try { 
			sResult = _wwwService.RegisterPaymentMasterEasypay("0025" , sponsorId
												, amt
												, ""
												, ""
												, ""
												, DateTime.Now.ToString("yyyyMMddHHmmss")
												, "10"
												, orderCertifyKey
												, sellerOrderReferenceKey
												, ""
												, ""
												, CodeAction.ChannelType
												, "Type:" + "" + ", Desc:" + ""
												, sUserID
												, sUserName);
			
			if(sResult.Length >= 2 && sResult.Substring(0, 2).Equals("30")) {
                //action.DeleteCommitment(payInfo.commitmentId.ToString());
				return false;
			} else {
                registerPartitioning_CommitmentGroup(sponsorId, payInfo.commitmentId.ToString());
			}

			if(payInfo.group == "CDSP") {
                updatePTD(payInfo.commitmentId.ToString());
                //[이종진] 기존방법과 신규방법 if
                if (strdbgp_kind == "1")    //기존
                {
                    new ChildAction().Release(payInfo.childMasterId);
                }
                else //if(strdbgp_kind == "2")
                {   //신규
                    //GlobalSponsorID 생성, GlobalCommitment생성(킷발송), NomoneyHold및 임시결연정보삭제, tCommitmentMaster Update
                    result = action.ProcSuccessPay(payInfo.commitmentId.ToString());
                    if (!result.success) return false;
                    new ChildAction().Release(payInfo.childMasterId, false);
                }
			}

			return true;
		} catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current, 0, ex.ToString());
			return false;
		}
	}

    bool DoSave_PayDelay()
    {

        var sponsorId = "";
        var sUserID = "";
        var sUserName = "";
        string sResult = "";
        DataSet dsResult = new DataSet();

        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
            sUserID = sess.UserId;
            sUserName = sess.UserName;

        }
        else
        {
            return false;
        }

        //Pay Log 생성 (결제모듈 응답)
        StringBuilder sb = new StringBuilder();
        sb.Append("sponsorId:" + sponsorId + ", ");
        sb.Append("sUserID:" + sUserID + ", ");
        sb.Append("payInfo.campaignId:" + "" + ", ");
        sb.Append("payInfo.group:" + "CDSP" + ", ");
        sb.Append("resultCode:" + resultCode + ", ");
        sb.Append("resultMsg:" + resultMsg + ", ");
        sb.Append("authDate:" + authDate + ", ");
        sb.Append("authCode:" + authCode + ", ");
        sb.Append("buyerName:" + buyerName + ", ");
        sb.Append("goodsName:" + goodsName + ", ");
        sb.Append("payMethod:" + payMethod + ", ");
        sb.Append("mid:" + mid + ", ");
        sb.Append("tid:" + tid + ", ");
        sb.Append("moid:" + moid + ", ");
        sb.Append("amt:" + amt + ", ");
        sb.Append("cardCode:" + cardCode + ", ");
        sb.Append("cardName:" + cardName + ", ");
        sb.Append("cardQuota:" + cardQuota + ", ");
        sb.Append("cardInterest:" + cardInterest);
        sb.Append("cardCl:" + cardCl + ", ");
        sb.Append("cardBin:" + cardBin + ", ");
        sb.Append("cardPoint:" + cardPoint + ", ");

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
        sb.Append("Childkey:" + "" + ", ");
        sb.Append("ChildMasterID:" + "" + ", ");
        sb.Append("month:" + "" + ", ");
        sb.Append("frequency:" + "" + ", ");
        sb.Append("TypeName:" + "" + ", ");

        //-----------------------------------------------------------------------------
        // USER-AGENT 구분
        //-----------------------------------------------------------------------------
        string WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
        {
            WebMode = "MOBILE";
        }
        else
        {
            WebMode = "PC";
        }
        sb.Append("WebMode:" + WebMode + " ");
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
        ErrorLog.Write(HttpContext.Current, 601, sb.ToString());


        sResult = _wwwService.RegisterPaymentMasterEasypay("0025", sponsorId
                                                , amt.ToString()
                                                , ""
                                                , ""
                                                , ""
                                                , DateTime.Now.ToString("yyyyMMddHHmmss")
                                                , "10"
                                                , tid
                                                , ordr_idxx
                                                , ""
                                                , ""
                                                , CodeAction.ChannelType
                                                , "Type:" + "" + ", Desc:" + ""
                                                , sUserID
                                                , sUserName);



        if (sResult.Length >= 2 && sResult.Substring(0, 2).Equals("30"))
        {

            return false;


        }
        else
        {

            SetDeliquentTodayPayment(ordr_idxx);
            return true;

        }



    }

    protected bool registerPartitioning_CommitmentGroup( string sponsorId, string sCommitmentIDs ) {
		//----- Define --------------------------------------------------------
		string sNotificationID = sellerOrderReferenceKey;
		string sResult = string.Empty;

		//----- Do Something --------------------------------------------------
		try {

			sResult = _wwwService.paymentPartitiong_CommitmentGroup(sNotificationID, sCommitmentIDs.Split(','), sponsorId, buyr_name);
			return true;
		} catch(Exception ex) {
			ErrorLog.Write(this.Context, 0, ex.Message);
			return false;
		}
	}

	private bool updatePTD( string sCommitmentIDs ) {
		String sResult = String.Empty;

		try {


			var payInfo = pi.ToObject<PayItemSession.Entity>();

			string PTD = Convert.ToDateTime(DateTime.Now.AddMonths(payInfo.month).ToString("yyyy-MM-01")).AddDays(-1).ToString("yyyy-MM-dd");

			WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();

			foreach(var sCommitmentID in sCommitmentIDs.Split(',')) {
				
				Object[] objSql = new object[1] { " SELECT SponsorItemEng " +
												" FROM   tCommitmentMaster " +
												" WHERE  CommitmentID = '" + sCommitmentID + "' " };

				DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

				if(ds.Tables[0].Rows.Count > 0) {
					if(ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "DS" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "LS" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "DSADD" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "LSADD") {
					
				Object[] objSql2 = new object[1] { " UPDATE tCommitmentMaster " +
														" SET    PTD = '" + PTD + "'" +
														" WHERE  CommitmentID = '" + sCommitmentID + "' " };

				DataSet iResult = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "Text", null, null);
				
					}
				}
				
			}

			return true;
		} catch(Exception ex) {
			ErrorLog.Write(this.Context, 0, ex.Message);
			return false;
		}
	}

    private bool SetDeliquentTodayPayment(string xOrderNo)
    {
        //----- 세션선언
        UserInfo sess = new UserInfo();
        bool sResult_Bool = false;
        //----- 결제
        try
        {
            string UserID = string.Empty;
            string UserName = string.Empty;

            UserID = sess.SponsorID.ToString();
            UserName = sess.UserName.ToString();

            //수정 2013-01-03
            //DataSet ds1 = (DataSet)Session["ds1"];

            //ErrorLog.Write(HttpContext.Current, 0, pi);
            var ds1 = pi.ToObject<DataSet>();
            //DataSet ds1 = (DataSet)Session["DataSet_" + sess.SponsorID];

            DataTable xPayOrder = new DataTable();
            xPayOrder.TableName = "PayOrderNo";
            xPayOrder.Columns.Add("PayOrderNo", typeof(string));
            xPayOrder.Rows.Add(xOrderNo);
            ds1.Tables.Add(xPayOrder);
            xPayOrder.Dispose();

            //수정 2013-01-03
            //Session.Remove("ds1");
            //	Session.Remove("DataSet_" + sess.SponsorID);

            //실행
            //string sResult = _wwwService.SetDeliquentTodayPayment(ds1, UserID, UserName);
            //UserName으로 테이블 생성시 오류로 인해 SponsorID로 테이블 생성으로 변경
            string sResult = _wwwService.SetDeliquentTodayPayment(ds1, UserID, UserID);

            if (sResult == "10")
            {
                sResult_Bool = true;
            }

        }
        catch (Exception ex)
        {

            ErrorLog.Write(this.Context, 0, ex.ToString());
            base.AlertWithJavascript("미납금 처리에 실패했습니다.");
            return false;
        }

        return sResult_Bool;
    }
}
