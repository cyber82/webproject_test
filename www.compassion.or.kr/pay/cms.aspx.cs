﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

using CommonLib;
using System.Data.Linq;
using System.Linq;

public partial class pay_cms : FrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	string signed_data = "";
	string src = "";
	string relation = "";
	string cms_owner = "";
	string cms_account = "";
	string birth = "";
	string bank_name = "";
	string bank_code = "";
	string cmsday = "";
	string ordr_idxx = "";

	public string motiveCode;          // 동기코드
	public string motiveName;          // 동기명
	string pi;

    //[jun.heo] 2017-12 : 결제 모듈 통합
    string paymentType;
    string payPageType;

    WWWService.Service _wwwService = new WWWService.Service();
    
    protected override void OnBeforePostBack() {

		failUrl.Value = Request["failUrl"];
		returnUrl.Value = Request["returnUrl"];
		signed_data = Request.Form["signed_data"];
		src = Request["src"];
		relation = Request["relation"];
		cms_owner = Request["cms_owner"];
		cms_account = Request["cms_account"];
		birth = Request["birth"];
		bank_name = Request["bank_name"];
		bank_code = Request["bank_code"];
		cmsday = Request["cmsday"];
		ordr_idxx = Request["ordr_idxx"];

		motiveCode = Request["motiveCode"];
		motiveName = Request["motiveName"];
		pi = Request["payInfo"];

        //[jun.heo] 2017-12 : 결제 모듈 통합
        paymentType = Request["paymentType"];
        payPageType = Request["payPageType"];

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리
        // 결제 종류별 사전 처리 및 결제 종류에 맞는 "DoSave_XXXX()" 호출
        switch (payPageType)
        {
            case "PayAccount":  // 계좌관리
                returnUrl.Value = "/my/sponsor/pay-account/";

                //인증서 검사 및 cmd데이터 insert
                if (this.CheckCert())
                {
                    this.DoSave_PayAccount();
                }
                break;
            case "Regular":   // 정기후원 : 1:1 후원 및 결혼 후원 등
            default:
                //인증서 검사 및 cmd데이터 insert
                if (this.CheckCert())
                {
                    this.DoSave_Regular();
                }
                break;
        }
	}
	
    //인증서 검사
	bool CheckCert() {

	
		AXCROSSCERT2Lib.CrossCert2 CrossCert = new AXCROSSCERT2Lib.CrossCert2();

		string OriginData = CrossCert.VerifyData(signed_data);
		
		if(OriginData == "") {
			var msg = "에러코드  : " + CrossCert.GetErrorCode() + "\n";
			msg += "에러메시지 : " + CrossCert.GetErrorMsg();
			base.AlertWithJavascript(msg);
			result.Value = "N";
			return false;

		} else {
			
			//인증서 추출
			string UseCert = CrossCert.GetSignCert();
			
			if(UseCert == "") {
				var msg = "인증서 추출 에러코드  : " + CrossCert.GetErrorCode() + "\n";
				msg += "인증서 추출 에러메시지 : " + CrossCert.GetErrorMsg();

				base.AlertWithJavascript(msg);
				result.Value = "N";
				return false;
				
			} else {
				 
				// 인증서 검증
				string Policies = "";
				

				/* 개인상호연동용(범용) */
				Policies += "1.2.410.200004.5.2.1.2" + "|";          // 한국정보인증               개인                                             
				Policies += "1.2.410.200004.5.1.1.5" + "|";          // 한국증권전산               개인                                             
				Policies += "1.2.410.200005.1.1.1" + "|";          // 금융결제원                 개인                                             
				Policies += "1.2.410.200004.5.3.1.4" + "|";          // 한국전산원           개인(국가기관, 공공기관 및 법인의 소속직원 등)   
				Policies += "1.2.410.200004.5.4.1.1" + "|";          // 한국전자인증               개인                                             
				Policies += "1.2.410.200012.1.1.1" + "|";          // 한국무역정보통신           개인  

				//추가 2012-08-03
				// 개인 용도제한용 인증서정책(OID)                 용도                    공인인증기관
				Policies += "1.2.410.200004.5.4.1.101|";        // 은행거래용/보험용       한국전자인증
				Policies += "1.2.410.200004.5.4.1.102|";        // 증권거래용              한국전자인증
				Policies += "1.2.410.200004.5.4.1.103|";        // 신용카드용              한국전자인증
				Policies += "1.2.410.200004.5.4.1.104|";        // 전자민원용              한국전자인증
				Policies += "1.2.410.200004.5.4.1.66" + "|";   // 한국과학문화재단 전용               개인 

				Policies += "1.2.410.200004.5.2.1.7.1|";        // 은행거래용/보험용       한국정보인증
				Policies += "1.2.410.200004.5.2.1.7.2|";        // 증권거래용/보험용       한국정보인증
				Policies += "1.2.410.200004.5.2.1.7.3|";        // 신용카드용              한국정보인증

				Policies += "1.2.410.200004.5.1.1.9|";          // 증권거래용/보험용       한국증전산
				Policies += "1.2.410.200004.5.1.1.9.2|";        // 신용카드용              한국증전산

				Policies += "1.2.410.200005.1.1.4|";            // 은행거래용/보험용       금융결제원
				Policies += "1.2.410.200005.1.1.6.2|";          // 신용카드용              금융결제원

				Policies += "1.2.410.200012.1.1.101|";          // 은행거래용/보험용       한국무역정보통신
				Policies += "1.2.410.200012.1.1.103|";          // 증권거래용/보험용       한국무역정보통신
				Policies += "1.2.410.200012.1.1.105";           // 신용카드용              한국무역정보통신

				int nRet = CrossCert.VerifyCert(UseCert, 1, Policies);

				if(nRet != 0) {

					if(ConfigurationManager.AppSettings["stage"] == "dev") {

                        bool isInsert = false;

                        isInsert = InsertSignData(signed_data, src);

                        return isInsert;

                        //return true;
                    } else {
						var msg = "인증서 검증 에러:" + CrossCert.GetErrorMsg() + "(" + CrossCert.GetErrorCode() + ")";

						base.AlertWithJavascript(msg);
						result.Value = "N";
						return false;
					}
				} else {

                    bool isInsert = false;

                    isInsert = InsertSignData(signed_data, src);

                    return isInsert;
                }
			}
		}
		
	}

    //인증서 검사 후, tSponsorCMS_ProofData insert
    private bool InsertSignData(String signeddata, String OriginData) {
        UserInfo sess = new UserInfo();

        string sSponsorID = string.Empty;
        string sRegisterName = string.Empty;
        string sResult = string.Empty;

        if (sess.SponsorID != null && sess.SponsorID != "") {
            sSponsorID = sess.SponsorID;
            sRegisterName = sess.UserName;

            try {
                string timeStamp = _wwwService.GetTimeStamp();
                string sproofID = "DER" + timeStamp + sSponsorID;
                string PDName = sproofID + ".der";

                sResult = _wwwService.InsertSignData(sproofID, sRegisterName, signeddata, OriginData, sSponsorID, PDName);

                if (sResult != "10") {
                    base.AlertWithJavascript("후원자님의 인증정보를 확인하던 도중 오류가 발생하였습니다.");
                    result.Value = "N";

                    return false;
                }
            } catch (Exception ex) {

                base.AlertWithJavascript("후원자님의 인증정보를 확인하던 도중 오류가 발생하였습니다.");
                result.Value = "N";

                return false;
            }

            return true;
        } else {

            base.AlertWithJavascript("후원자님의 인증정보를 확인하던 도중 오류가 발생하였습니다.");
            result.Value = "N";

            return false;
        }
    }

    bool DoSave_Regular() {
		
		if(!UserInfo.IsLogin) {
			base.AlertWithJavascript("로그인 사용자만 결제 가능합니다.");
			result.Value = "N";
			return false;
		}

		UserInfo sess = new UserInfo();

		if(string.IsNullOrEmpty(sess.SponsorID)) {
			base.AlertWithJavascript("후원회원만 등록가능합니다.");
			result.Value = "N";
			return false;
		}

		var action = new CommitmentAction();
		JsonWriter actionResult = new JsonWriter();
		var payInfo = pi.ToObject<PayItemSession.Entity>();

        // 1:1 어린이 후원 : CDSP
        // 그 외, CIV 나눔펀딩 등
        if (payInfo.group == "CDSP") {
            //CDSP하기위한 스폰서의 Validation 체크(미납금여부 등)와 
            //어린이정보insert(신규방법일 경우만), tCommitmentMaster insert
            //NomoneyHold, 임시결연정보 insert
            //[이종진] wedding일 경우
            if (!string.IsNullOrEmpty(payPageType) && payPageType.ToLower() == "wedding")
            {
                actionResult = action.DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName, motiveCode, motiveName, true);
            }
            else
            {
                actionResult = action.DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName, motiveCode, motiveName);
            }
        } else {
			actionResult = action.DoCIV(sess.SponsorID, payInfo.amount, 1, "", payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.childMasterId, payInfo.campaignId, motiveCode, motiveName);
		}
		
		if(!actionResult.success) {
			
			base.AlertWithJavascript(actionResult.message);
			return false;
		}

		var commitmentId = actionResult.data.ToString();

        //기존납부방법이 있는지 조회.TPAYMENTACCOUNT 테이블의 데이터 조회
        if (!this.CheckRegisteredAccount(sess.SponsorID , bank_code , cms_account.Replace("-", ""))) {
            //오류 시 return
			return false;
		}

        // 증빙서류 등록 HS
        /*
		 String dataSec 증빙 구분 : ‘01’: 서면, 일반전자서명(300KB 이하) ‘02’: 녹취(200KB 이하) ‘03’: 공인전자서명(5KB 이하
		 String dataType 증빙 데이터 유형 : 서면, 일반전자서명: JPG, GIF, TIF 녹취: MP3, WAV, WMA 공인전자서명: DER
		String dataLength  인코딩 전 데이터 총 길이
		Byte[] proofFile  증빙데이터
		*/

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
        string strLog = string.Empty;
        strLog += "SponsorID:" + payInfo.sponsorId + ", ";
        strLog += "OrderID:" + ordr_idxx + ", ";   // 주문번호
        strLog += "UserID:" + payInfo.userId + ", ";
        strLog += "CampaignID:" + payInfo.campaignId + ", ";
        strLog += "Group:" + payInfo.group + ", ";
        strLog += "ResultCode:" + "" + ", ";
        strLog += "mid:" + "" + ", ";   // 가맹점 ID
        strLog += "tid:" + "" + ", ";   // 거래 ID
        strLog += "moid:" + "" + ", ";  // 주문번호
        strLog += "amt:" + payInfo.amount.ToString() + ", ";
        strLog += "cardcode:" + bank_code + ", ";  // 카드사코드/은행코드
        strLog += "cardname:" + bank_name + ", ";  // 결제카드사명/은행명
        strLog += "cardbin:" + cms_account + ", ";   // 카드 BIN 번호/계좌번호
        strLog += "cardpoint:" + "" + ", "; // 카드 포인트 사용여부(0-미사용, 1-포인트사용, 2-세이브포인트사용)
        strLog += "codeID:" + payInfo.codeId + ", ";
        strLog += "buyr_name:" + sess.UserName + ", ";
        strLog += "UserName:" + payInfo.sponsorName + ", ";
        strLog += "Mobile:" + sess.Mobile + ", ";
        strLog += "IsUser:" + sess.UserName + ", ";    // 회원(회원명), 비회원, 무기명
        strLog += "codename:" + payInfo.codeName + ", ";
        strLog += "Childkey:" + payInfo.childKey + ", ";
        strLog += "ChildMasterID:" + payInfo.childMasterId + ", ";
        strLog += "month:" + payInfo.month + ", ";
        strLog += "frequency:" + payInfo.frequency + ", ";
        strLog += "TypeName:" + payInfo.TypeName + ", ";

        //-----------------------------------------------------------------------------
        // USER-AGENT 구분
        //-----------------------------------------------------------------------------
        string WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
        {
            WebMode = "MOBILE";
        }
        else
        {
            WebMode = "PC";
        }
        strLog += "WebMode:" + WebMode + " ";

        ErrorLog.Write(HttpContext.Current, 607, strLog);
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성

        //CheckRegisteredAccount() 에서 조회한 기존납부 방법이 있다면
        if (existAccount) {

			// 즉시결제
			return this.DirectPay(commitmentId, action, payInfo);

		} else {
			//var proofFile = System.Text.Encoding.ASCII.GetBytes(signed_data);
			//new HSCMSAction().RegistProof(cms_account, "03", "DER", proofFile.Length.ToString(), proofFile);

			// 결제계좌등록
			/*
			string sResult = _wwwService.RegisterPaymentAccountBase("0004", cmsday, CodeAction.ChannelType, sess.SponsorID, bank_code, cms_account.Replace("-", ""), cms_owner
				, ordr_idxx, cms_owner, birth, sess.UserId, sess.UserName);

			//===== 성공시
			if(sResult.Substring(0, 2).Equals("10")) {

				if(!RegistCMS()) {
					return false;
				}

				// 즉시결제
				return this.DirectPay(commitmentId, action, payInfo);

			} else {
				action.DeleteCommitment(commitmentId);
				base.AlertWithJavascript("에러가 발생했습니다.");
				return false;
			}
			*/

            //CMS 등록
			bool isCmsComplete = RegistCMS();

			if(isCmsComplete) {
				//기존결제방법 가져오기 
				DataSet dsPaymentType = new DataSet();
				if(!GetCheckPaymentAccountData(sess.SponsorID, ref dsPaymentType)) {
					return false;   //오류 시 return
				}

				//추가 지로초기추가 (2012-02-15)
				if(dsPaymentType.Tables[0].Rows.Count == 0) {

					string sTimeStamp = _wwwService.GetTimeStamp(); //타임스탬프
					sTimeStamp = sTimeStamp.Substring(0, sTimeStamp.Length - 1) + "0";

					//지로 저장하는 웹서비스
					_wwwService.CMSInitRegisterWeb(sess.SponsorID, "", "", sTimeStamp, sess.UserName, "", sess.Jumin
						, CodeAction.ChannelType, DateTime.Now.ToString("yyyy-MM-dd"), sess.UserId, sess.UserName);
				}

				this.DirectPay(commitmentId, action, payInfo);
				result.Value = "Y";
				return true;

			}


			return isCmsComplete;

		}


	}

    bool DoSave_PayAccount()
    {

        if (!UserInfo.IsLogin)
        {
            base.AlertWithJavascript("로그인 사용자만 결제 가능합니다.");
            result.Value = "N";
            return false;
        }

        UserInfo sess = new UserInfo();
        if (string.IsNullOrEmpty(sess.SponsorID))
        {
            base.AlertWithJavascript("후원회원만 등록가능합니다.");
            result.Value = "N";
            return false;
        }

        bool isCmsComplete = RegistCMS();

        if (isCmsComplete)
        {

            //- CMS로 납부방법 변경시 당월 납부내역 있으면 당월 출금정지 처리 (중복 출금 제외)
            string sPaymentTypeName = paymentType;
            string stopResult = setPaymentStop(sess.SponsorID, sess.UserId, sess.UserName, sPaymentTypeName);


            //기존결제방법 가져오기 
            DataSet dsPaymentType = new DataSet();
            if (!GetCheckPaymentAccountData(sess.SponsorID, ref dsPaymentType))
            {
                return false;
            }

            //추가 지로초기추가 (2012-02-15)
            if (dsPaymentType.Tables[0].Rows.Count == 0)
            {

                string sTimeStamp = _wwwService.GetTimeStamp(); //타임스탬프
                sTimeStamp = sTimeStamp.Substring(0, sTimeStamp.Length - 1) + "0";

                //지로 저장하는 웹서비스
                _wwwService.CMSInitRegisterWeb(sess.SponsorID, "", "", sTimeStamp, sess.UserName, "", sess.Jumin
                    , CodeAction.ChannelType, DateTime.Now.ToString("yyyy-MM-dd"), sess.UserId, sess.UserName);
            }

            //	this.DirectPay(commitmentId, action, payInfo);
            result.Value = "Y";
            return true;

        }

        return isCmsComplete;
    }

    bool DirectPay( string commitmentId, CommitmentAction action, PayItemSession.Entity payInfo ) {
		// 즉시결제

		var actionResult = new BatchPay().Pay("" , commitmentId, ordr_idxx, "한국컴패션후원금", payInfo.amount, "CMS", cms_account.Replace("-", ""), "", bank_code, birth, cms_owner, payInfo.group);

		result.Value = "Y";
        //[이종진]2017-12-20 : Global Pool에서 홀딩푸는 로직은 수행하지 않음
		//new ChildAction().Release(payInfo.childMasterId, false);

        bool success = true;

        if (actionResult.message != null)
        {
            success = false;
            msg.Value =
                //string.Format(@"정기 후원 신청이 완료되었으나,{0}첫 후원금 결제가 정상적으로 처리되지 않았습니다. 첫 후원금을 다시 결제하시겠습니까?.",
                //string.IsNullOrEmpty(actionResult.message) ? "" : string.Format("[{0}]의 사유로 ", actionResult.message));
            string.Format(@"정기 후원 신청이 완료되었으나, 첫 후원금 결제가 정상적으로 처리되지 않았습니다. 첫 후원금을 다시 결제하시겠습니까?");

            failUrl.Value = "/sponsor/pay/regular/pay_again?motiveCode=" + motiveCode + "&motiveName=a";// + HttpUtility.JavaScriptStringEncode(motiveName);
        }
        
		if(actionResult.success && success) {
			return true;
		} else {
            //[이종진] 결연삭제 부분은 제거. -> 다시되살림
            //추가로 TCPT용 삭제 작업이 필요함.
            //->nomoneyHold를 ecommerceHold로 변경, compass4 DB에 nomoneyHold 및 임시결연데이터 삭제
            action.DeleteCommitment(commitmentId);
			return false;
            /*첫 결재 실패 로직 변경 2017-01-13 이석호 과장 요청*/
            //1.메시지 변경
            //2. 메시지 표시 후 
		}


		// 즉시결제 실패해도 결연 성공으로 처리 
		// 2016-10-24 이석호 과장 요청


	}

	bool existAccount = false;
    //기존납부방법이 있는지 조회. TPAYMENTACCOUNT 테이블의 데이터 조회
    bool CheckRegisteredAccount( string sSponsorID , string sBankCode , string sBankAccount) {
		UserInfo sess = new UserInfo();

		try {
			var dsPaymentType = _wwwService.selectUsingPaymentAccount(sSponsorID, "0004");
			if(dsPaymentType.Tables[0].Rows.Count > 0) {
				var account = dsPaymentType.Tables[0].Rows[0]["Account"].ToString().Trim();
				var bankCode = dsPaymentType.Tables[0].Rows[0]["bankCode"].ToString().Trim();

				if (sBankCode == bankCode && sBankAccount == account) {
					existAccount = true;
				}
			}

			return true;
		} catch {
			base.AlertWithJavascript("기존 납부방법을 가져오는 중 오류가 발생했습니다.");
			result.Value = "N";
			return false;
		}
	}

    public string setPaymentStop(string sSponsorID, string sLoginID, string sLoginName, string sPaymentType)
    {
        WWWService.Service _service = new WWWService.Service();
        DataSet dsPaymentRequest = new DataSet();
        DataSet dsPaymentPaymentPartioning = new DataSet();
        string sPaymentMasterID = string.Empty;
        string sResult = string.Empty;

        DateTime dtNow = DateTime.Now;
        // DateTime.DaysInMonth(dtNow.Year, dtNow.Month).ToString() -- 월의 마지막일
        string sFromDate = dtNow.ToShortDateString().Substring(0, 8) + "01";
        string sToDate = dtNow.ToShortDateString().Substring(0, 10);

        //getData
        dsPaymentRequest = _service.listPaymentRequest_period(sSponsorID, sFromDate, sToDate, sPaymentType);
        //base.AlertWithJavascript(dsPaymentRequest.Tables[0].Rows.Count.ToString());
        if (dsPaymentRequest.Tables[0].Rows.Count > 0)
        {
            string sCommitmentID = string.Empty;
            string sRefuseID = string.Empty;
            string mmsg = "";
            for (int i = 0; i < dsPaymentRequest.Tables[0].Rows.Count; i++)
            {
                sPaymentMasterID = dsPaymentRequest.Tables[0].Rows[i]["PaymentMasterID"].ToString();

                dsPaymentPaymentPartioning = _service.getPaymentPartioning_paymentStop(sPaymentMasterID, "", "");

                //- 파티셔닝된 commitmentId 추출 
                if (dsPaymentPaymentPartioning.Tables[0].Rows.Count > 0)
                {
                    string sRefuseFrom = dtNow.ToShortDateString().Substring(0, 10) + " 00:00:00";
                    string sRefuseTo = dtNow.ToShortDateString().Substring(0, 8) + DateTime.DaysInMonth(dtNow.Year, dtNow.Month).ToString() + " 23:59:59";
                    string sPaymentDate = string.Empty;
                    string sCurrentUse = string.Empty;

                    foreach (DataRow row in dsPaymentPaymentPartioning.Tables[0].Rows)
                    {
                        sCommitmentID = row["CommitmentID"].ToString();

                        if (sPaymentType == "CARD")
                        {
                            if (row["AccountClass"].Equals("DS") || row["AccountClass"].Equals("LS"))
                            {
                                //- 당월 DS, LS 출금정지
                                #region 기존  N update
                                //- 'Y'면 정지, 'N'면 출금가능
                                sResult = _service.modifyPaymentRefuse_paymentChange(sCommitmentID, "N", sLoginID, sLoginName, "Reg");
                                sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "N", sLoginID, sLoginName);
                                #endregion

                                #region 신규  insert
                                //- 신규 출금정기기간ID 생성
                                //- 'Y'면 정지, 'N'면 출금가능
                                sRefuseID = _service.getTimeStamp_Number("0");
                                sResult = _service.registerPaymentRefuse(sCommitmentID, sRefuseID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                //----- tCommitmentMaster Update
                                if (sResult == "10")
                                {
                                    sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                }
                                #endregion
                            }
                            else
                            {
                                //- 당월 기타 후원금 출금정지 
                                #region 기존  N update
                                //- 'Y'면 정지, 'N'면 출금가능
                                sResult = _service.modifyPaymentRefuse_paymentChange(sCommitmentID, "N", sLoginID, sLoginName, "Reg");
                                sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "N", sLoginID, sLoginName);
                                #endregion

                                #region 신규  insert
                                //- 신규 출금정기기간ID 생성
                                //- 'Y'면 정지, 'N'면 출금가능
                                sRefuseID = _service.getTimeStamp_Number("0");
                                sResult = _service.registerPaymentRefuse(sCommitmentID, sRefuseID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                //----- tCommitmentMaster Update
                                if (sResult == "10")
                                {
                                    sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                }
                                #endregion
                            }
                        }
                        else if (sPaymentType == "CMS")
                        {
                            sPaymentDate = row["PaymentDate"].ToString();

                            if (sPaymentDate != null && sPaymentDate != "") //- 정산일자가 있는 것만 처리 
                            {
                                if (row["AccountClass"].Equals("DS") || row["AccountClass"].Equals("LS"))
                                {
                                    mmsg += "sCommitmentID:" + sCommitmentID + "\r\n";

                                    //- 당월 DS, LS 출금정지
                                    #region 기존  N update
                                    //- 'Y'면 정지, 'N'면 출금가능
                                    sResult = _service.modifyPaymentRefuse_paymentChange(sCommitmentID, "N", sLoginID, sLoginName, "Reg");
                                    sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "N", sLoginID, sLoginName);
                                    #endregion

                                    #region 신규  insert
                                    //- 신규 출금정기기간ID 생성
                                    //- 'Y'면 정지, 'N'면 출금가능
                                    sRefuseID = _service.getTimeStamp_Number("0");
                                    sResult = _service.registerPaymentRefuse(sCommitmentID, sRefuseID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                    //----- tCommitmentMaster Update
                                    if (sResult == "10")
                                    {
                                        sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    //- 당월 기타 후원금 출금정지 
                                    #region 기존  N update
                                    //- 'Y'면 정지, 'N'면 출금가능
                                    sResult = _service.modifyPaymentRefuse_paymentChange(sCommitmentID, "N", sLoginID, sLoginName, "Reg");
                                    sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "N", sLoginID, sLoginName);
                                    #endregion

                                    #region 신규  insert
                                    //- 신규 출금정기기간ID 생성
                                    //- 'Y'면 정지, 'N'면 출금가능
                                    sRefuseID = _service.getTimeStamp_Number("0");
                                    sResult = _service.registerPaymentRefuse(sCommitmentID, sRefuseID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                    //----- tCommitmentMaster Update
                                    if (sResult == "10")
                                    {
                                        sResult = _service.modifyCommitmentRefuse(sCommitmentID, sRefuseFrom, sRefuseTo, "Y", sLoginID, sLoginName);
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
        }

        return sResult;
    }

    bool GetCheckPaymentAccountData( string sSponsorID, ref DataSet dsPaymentType ) {
		UserInfo sess = new UserInfo();

		try {
			dsPaymentType = _wwwService.selectUsingPaymentAccount(sSponsorID, "");
			return true;
		} catch {
			base.AlertWithJavascript("납부방법을 가져오는 중 오류가 발생했습니다.");
			result.Value = "N";
			return false;
		}
		
	}

	bool RegistCMS() {

		UserInfo sess = new UserInfo();

		//----- Defien, set Data --------------------------------------------------------------
		DataSet dsPaymentAccount = new DataSet();

		//추가 2013-01-28
		DataSet dsPayWay = new DataSet();

		string sResCD = string.Empty;
		string sSign = string.Empty;
		string sCount = string.Empty;
		string sConID = string.Empty;

		//추가 2013-01-28
		string sJiroNo = string.Empty;
		string sRefuseNewID = DateTime.Now.ToString("yyyyMMddHHmmssff2");
		string payway = string.Empty;
		string paycode = string.Empty;

		//- 시스템코드 View
		DataView dvCode = new DataView(); //View생성

		WWWService.Service _wwwService = new WWWService.Service();
        WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

        Object[] objSql = new object[1] { string.Format("select top 1 codename from tSystemCode with(nolock) where codeType='BankCode' and currentUse = 'Y' and codeId='{0}'" , bank_code) };
        DataSet dsBank = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
        bank_name = dsBank.Tables[0].Rows[0]["codename"].ToString();

        //추가 2013-01-28
        //----- 전체에서 결제방법을 가져온다 ---------------------------------------------------
        try {
            //결제정보 (tPaymentAccount조회)
            dsPayWay = _wwwService.Payway(sess.SponsorID.ToString().Trim());
            //어린이 정보
            DataSet data = _wwwService.MypageApplyChildInfo(sess.SponsorID.Trim(), "", "", 0, 9999, "");
            //tCommitmentMaster 조회
            DataSet dsList = _wwwService.ComplementaryApplySearch(sess.SponsorID.Trim());
			int dataCnt = data.Tables[0].Rows.Count;
			int dsListCnt = dsList.Tables[0].Rows.Count;

			if(dsPayWay != null) {
				if(dsPayWay.Tables["BankT"].Rows.Count > 0 && (dataCnt > 0 || dsListCnt > 0)) {
					payway = dsPayWay.Tables["BankT"].Rows[0]["PaymentName"].ToString().Trim();

					switch(dsPayWay.Tables["BankT"].Rows[0]["PaymentName"].ToString().Trim()) {
						case "신용카드자동결제":
							paycode = "0003";
							break;

						case "지로":
							paycode = "0005";
							break;

						case "CMS":
							paycode = "0004";
							break;

						case "가상계좌":
							paycode = "0001";
							break;

						case "미주":
							paycode = "0008";
							break;

						case "휴대폰자동결제":
							paycode = "0020";
							break;

						default:
							paycode = "9999";
							break;
					}
				} else {
					paycode = "9999";
				}
			} else {
				paycode = "9999";
			}
		} catch(Exception ex) {
			//Exception Error Insert
			_wwwService.writeErrorLog("CompassWeb4", ex.TargetSite.ReflectedType.Name, ex.TargetSite.Name, ex.Message);
			result.Value = "N";
			base.AlertWithJavascript("납부방법을 가져오는중 오류입니다.");
			
			return false;
		}


		//----- 결제방법을 가져온다 -----------------------------------------------------------
		if(sess.SponsorID != "" && sess.SponsorID != null) {
			try {
				dsPaymentAccount = _wwwService.selectUsingPaymentAccount(sess.SponsorID, "");

				if(dsPaymentAccount.Tables.Count <= 0) {
					result.Value = "N";
					base.AlertWithJavascript("결제방법 가져오기에 실패했습니다.");
					return false;
				}
			} catch(Exception ex) {
				result.Value = "N";
				base.AlertWithJavascript("결제방법 가져오기에 실패했습니다.");
				return false;
			}
			

			DateTime dtDate2 = _wwwService.GetDate();
			//---- 데이터가 없으면 CMS신청만 한다. ------------------------------------------------
			if(dsPaymentAccount.Tables[0].Rows.Count <= 0) {


				try {
					//- 카운트 가져오기
					sCount = _wwwService.countTableTerm(dtDate2);

					//- 7자리 맞추기
					sCount = Convert.ToString(Convert.ToInt32(sCount) + 1).PadLeft(7, '0');
                    
					//- CMS신청시 수정
					sResCD = _wwwService.insertCMSApplication(dtDate2
															, sCount
															, sess.SponsorID
															, cms_owner
															, bank_code
															, bank_name
															, cms_account.Replace("-", "")
															, cmsday
															, birth
															, sess.SponsorID
															, sess.UserName);

					//- CMS 자동이체 신청시 전자서명값 등록
					try {
						sSign = _wwwService.updateCmsSignData(sess.SponsorID, signed_data);

                        var proofFile = System.Text.Encoding.UTF8.GetBytes(signed_data);
                        new HSCMSAction().RegistProof(cms_account, "03", "DER", proofFile.Length.ToString(), proofFile);
                        /*
                        var proofResult = _wwwService.registSponsorCMS_ProofData(sess.SponsorID, sess.UserName, signed_data, src);
                        if (proofResult != "10") {
                            base.AlertWithJavascript("전자서명값 저장을 실패했습니다.");
                            result.Value = "N";
                            return false;
                        }
                        */
                        //추가 2013-01-28  결제방법이 지로일때 지로 수신거부 설정 (결제방법이 없을때에도 포함)
                        if ((paycode == "0004") || (paycode == "0005") || (paycode == "9999")) {
							try {
								sJiroNo = _wwwService.CMSJiroNoSet(sess.SponsorID, sRefuseNewID, sess.UserName);
								return true;
							} catch {
								base.AlertWithJavascript("지로 수신거부 등록이 실패했습니다.");
								result.Value = "N";
								return false;
							}
						} else {
							return true;
						}
					} catch(Exception ex) {
						base.AlertWithJavascript("전자서명값 등록이 실패했습니다.");
						result.Value = "N";
						return false;
					}
				} catch(Exception ex) {
					base.AlertWithJavascript("CMS계좌 등록이 실패했습니다.");
					result.Value = "N";

					return false;
				}
			} else {
				//- CMS사용중이면 CMStoCMS를 한다.
				try {

					sResCD = _wwwService.insertCMStoCMSApplication(sess.SponsorID
																, cms_owner
																, bank_code
																, bank_name
																, cms_account
																, birth
																, cmsday
																, dtDate2
																, dsPaymentAccount.Tables[0].Rows[0]["PaymentType"].ToString()
																, dsPaymentAccount.Tables[0].Rows[0]["BankCode"].ToString()
																, dsPaymentAccount.Tables[0].Rows[0]["Account"].ToString()
																, dsPaymentAccount.Tables[0].Rows[0]["PaymentAccountID"].ToString()
																, sess.SponsorID
																, sess.UserName
																);

					//- CMS 자동이체 신청시 전자서명값 등록
					try {
						sSign = _wwwService.updateCmsSignData(sess.SponsorID, signed_data);

                        var proofFile = System.Text.Encoding.UTF8.GetBytes(signed_data);
                        new HSCMSAction().RegistProof(cms_account, "03", "DER", proofFile.Length.ToString(), proofFile);
                        /*
                        var proofResult = _wwwService.registSponsorCMS_ProofData(sess.SponsorID, sess.UserName, signed_data, src);
                        if (proofResult != "10") {
                            base.AlertWithJavascript("전자서명값 저장을 실패했습니다.");
                            result.Value = "N";
                            return false;
                        }
                        */
                        try {
							sJiroNo = _wwwService.CMSJiroNoSet(sess.SponsorID, sRefuseNewID, sess.UserName);
							return true;
						} catch {
							base.AlertWithJavascript("지로 수신거부 등록이 실패했습니다.");
							result.Value = "N";
							return false;
						}

					} catch(Exception ex) {
						base.AlertWithJavascript("전자서명값 등록이 실패했습니다.");
						result.Value = "N";
						return false;
					}
				} catch(Exception ex) {
					base.AlertWithJavascript("CMS계좌 등록이 실패했습니다.");
					result.Value = "N";
					return false;
				}

			}

			//----- 결과 출력 ---------------------------------------------------------------------
			if(sResCD == "10") {
				return true;
			} else {
				base.AlertWithJavascript("CMS계좌 등록이 실패했습니다.");
				result.Value = "N";
				return false;
			}

			if(sSign == "10") {
				return true;
			} else {
				base.AlertWithJavascript("전자서명값 등록이 실패했습니다.");
				result.Value = "N";
				return false;
			}

			if(sJiroNo == "10") {
				return true;
			} else {
				base.AlertWithJavascript("지로 수신거부 등록이 실패했습니다.");
				result.Value = "N";
				return false;
			}

		} else {

			base.AlertWithJavascript("연결이 끊어졌습니다. 다시 로그인하여 주시기 바랍니다.");
			result.Value = "N";

			return false;
		}


		return true;
	}


}
