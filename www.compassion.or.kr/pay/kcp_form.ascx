﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="kcp_form.ascx.cs" Inherits="pay_kcp_form" %>
<asp:PlaceHolder runat="server" ID="ph_content" Visible="false">

	
	<div id="display_setup_message" style="display:none">
	<p class="txt">
	결제를 계속 하시려면 상단의 노란색 표시줄을 클릭 하시거나 <a href="http://pay.kcp.co.kr/plugin_new/file/KCPUXWizard.exe"><span>[수동설치]</span></a>를 눌러
	Payplus Plug-in을 설치하시기 바랍니다.
	[수동설치]를 눌러 설치하신 경우 새로고침(F5)키를 눌러 진행하시기 바랍니다.
	</p>
	</div>

	<script type="text/javascript">

		function m_Completepayment( FormOrJson, closeEvent ) {
			var frm = document.ex_frm;
            GetField( frm, FormOrJson ); // 위에서 만든 폼데이터에 결제창의 인증데이터 담기.
            
            if (frm.res_cd.value == "0000") {
            	frm.target = "hd_ex_frm";
                frm.submit(); 
            } else {

               // alert( "[" + frm.res_cd.value + "] " + frm.res_msg.value );
                closeEvent();
            }
        }

	</script>

	<!--  테스트 시 : src="http://pay.kcp.co.kr/plugin/payplus_test.js"
						src="https://pay.kcp.co.kr/plugin/payplus_test.js"
			실결제 시 : src="http://pay.kcp.co.kr/plugin/payplus.js"
						src="https://pay.kcp.co.kr/plugin/payplus.js"
	테스트 시(UTF-8) : src="http://pay.kcp.co.kr/plugin/payplus_test_un.js"
						src="https://pay.kcp.co.kr/plugin/payplus_test_un.js"
	실결제 시(UTF-8) : src="http://pay.kcp.co.kr/plugin/payplus_un.js"
						src="https://pay.kcp.co.kr/plugin/payplus_un.js"      -->

    <%--[jun.heo] 2017-12 : 결제 모듈 통합 - 표준 Web 결제 모듈 적용--%>
	<script language='javascript' src='<%:this.ViewState["appFormUrl"].ToString() %>'></script> <%--표준 Web 결제 모듈 : 개발/운영에 따라 URL 처리--%>
    <%--<script language='javascript' src='https://pay.kcp.co.kr/plugin/payplus_web.jsp'></script>--%> <%--표준 Web 결제 모듈 운영--%>
    <%--<script language='javascript' src='https://pay.kcp.co.kr/plugin/payplus_test_un.js'></script>--%> <%--ActiveX 결제 모듈--%>

    <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
	<script type="text/javascript" src="/pay/kcp.js?v=1"></script>
    <%--<script type="text/javascript" src="/sponsor/pay/temporary/kcp.js"></script>--%>
	<div style="display:none">
	<iframe id="hd_ex_frm" name="hd_ex_frm"></iframe>
    <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
    <form id="ex_frm" name="ex_frm" method="post" action="/pay/kcp">
	<%--<form id="ex_frm" name="ex_frm" method="post" action="/sponsor/pay/temporary/kcp">--%>

		<!-- KCP -->
		<input type="hidden" id="ordr_idxx" name="ordr_idxx" value="<%:this.ViewState["ordr_idxx"].ToString() %>" />
		<input type="hidden" name="good_name" class="w100" value="<%:this.ViewState["good_name"].ToString() %>"/>
		<input type="hidden" name="good_mny" class="w100" value="<%:this.ViewState["good_mny"].ToString() %>" maxlength="9"/>
		<input type="hidden" id="buyr_name" name="buyr_name" value="<%:this.ViewState["buyr_name"].ToString() %>"/> <%--[jun.heo] 2017-12 : 결제 모듈 통합 - buyr_name 추가--%>
		<input type="hidden" name="buyr_mail" class="w200" value="" maxlength="30" />
		<input type="hidden" name="buyr_tel1" class="w100" value=""/>
		<input type="hidden" name="buyr_tel2" class="w100" value=""/>
		
		<input type="hidden" name="pay_method"      value="<%:this.ViewState["pay_method"].ToString() %>" />
		<input type="hidden" name="req_tx"          value="pay" />
		<input type="hidden" name="site_cd"         value="<%:this.ViewState["site_cd"].ToString() %>" />
		<input type="hidden" name="site_name"       value="Compassion" />
		<input type="hidden" name="quotaopt"        value="<%:this.ViewState["quotaopt"].ToString() %>"/>   <%--[jun.heo] 2017-12 : 결제 모듈 통합 - 카드할부 개월수--%>
		<input type="hidden" name="currency"        value="WON"/>
		<input type="hidden" name="module_type"     value="01"/>
		<input type="hidden" name="res_cd"          value=""/>
		<input type="hidden" name="res_msg"         value=""/>
		<input type="hidden" name="tno"             value=""/>
		<input type="hidden" name="trace_no"        value=""/>
		<input type="hidden" name="enc_info"        value=""/>
		<input type="hidden" name="enc_data"        value=""/>
		<input type="hidden" name="ret_pay_method"  value=""/>
		<input type="hidden" name="tran_cd"         value=""/>
		<input type="hidden" name="bank_name"       value=""/>
		<input type="hidden" name="bank_issu"       value=""/>
		<input type="hidden" name="use_pay_method"  value=""/>

        <input type="hidden"  name="used_card_YN"     value="<%:this.ViewState["used_card_YN"].ToString() %>"/>
        <input type="hidden"  name="used_card"        value="<%:this.ViewState["used_card"].ToString() %>"/>  


		<!--  현금영수증 관련 정보 : Payplus Plugin 에서 설정하는 정보입니다 -->
		<input type="hidden" name="cash_tsdtime"    value=""/>
		<input type="hidden" name="cash_yn"         value=""/>
		<input type="hidden" name="cash_authno"     value=""/>
		<input type="hidden" name="cash_tr_code"    value=""/>
		<input type="hidden" name="cash_id_info"    value=""/>
		<input type="hidden" name="good_expr" value="0">

        <!--[jun.heo] 2017-12 : 현금영수증 숨기기 - 고객 요청-->
        <input type="hidden" name="disp_tax_yn" value="N">

		<!-- 가맹점에서 관리하는 고객 아이디 설정을 해야 합니다.(필수 설정) -->
		<input type="hidden" name="shop_user_id"    value=""/>
		<!-- 복지포인트 결제시 가맹점에 할당되어진 코드 값을 입력해야합니다.(필수 설정) -->
		<input type="hidden" name="pt_memcorp_cd"   value=""/>

		<input type='hidden' name='returnUrl'      value='<%:this.ViewState["returnUrl"].ToString() %>'>
		
		<input type='hidden' name='jumin'      value='<%:this.ViewState["jumin"].ToString() %>'>
		<input type='hidden' name='ci'      value='<%:this.ViewState["ci"].ToString() %>'>
		<input type='hidden' name='di'      value='<%:this.ViewState["di"].ToString() %>'>
		<input type='hidden' name='overseas_card'      value='<%:this.ViewState["overseas_card"].ToString() %>'>
		<input type='hidden' name='month'      value='<%:this.ViewState["month"].ToString() %>'>
        <input type="hidden" name="paymentType" value="<%:this.ViewState["paymentType"].ToString() %>" />   <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
		<input type='hidden' id="motiveCode" name='motiveCode'      value='<%:this.ViewState["motiveCode"].ToString() %>'>
		<input type='hidden' id="motiveName" name='motiveName'      value='<%:this.ViewState["motiveName"].ToString() %>'>
		<input type='hidden' id="payInfo" name='payInfo'      value='<%:this.ViewState["payInfo"].ToString() %>'>
        <input type='hidden' id="firstPay" name='firstPay'      value='<%: this.ViewState["firstPay"] == null ? "" : this.ViewState["firstPay"].ToString() %>'>

        <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
        <input type='hidden' id='payPageType' name='payPageType' value='<%:this.ViewState["payPageType"].ToString() %>'>

        <!-- vitiontrip add -->
		<input type="hidden" id="plandetail_id" name="plandetail_id" value="<%:this.ViewState["plandetail_id"].ToString() %>"/><!-- 기획트립 ID --> 
        <input type="hidden" id="amount_type" name="amount_type" value="<%:this.ViewState["amount_type"].ToString() %>"/><!-- 기획트립 ID --> 
        <!-- end vitiontrip add -->
        <input type="hidden" name="vcnt_expire_term" value="3"/>    <!-- 가상계좌 유효기간 --> 
		<input type="hidden" id="order_item" name="order_item" value="<%:this.ViewState["order_item"].ToString() %>" />
        <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>

		<!--// KCP -->
	</form>
		</div>
</asp:PlaceHolder>