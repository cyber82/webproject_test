﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete_giftmoney.aspx.cs" Inherits="pay_complete_giftmoney"%>

<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>

	<asp:PlaceHolder runat="server" ID="ph_complete" Visible="false">
		<script type="text/javascript">
			
			$(function () {
				alert("결제가 완료되었습니다.");
				location.href = "/my/sponsor/gift-money/";
			});

		</script>
	</asp:PlaceHolder>
