﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using CommonLib;
using LGCNS.KMpay.Merchant.Certification;

using LGCNS.CNSPay.Service;

public partial class pay_kakaopay_form : System.Web.UI.UserControl {
	protected String RequestDealApproveUrl = String.Empty;
	protected String RequestDealPage = String.Empty;
	protected String Amt = String.Empty;
	protected String MID = String.Empty;
	protected String EncodeKey = String.Empty;
	protected String MerchantEncKey = String.Empty;
	protected String MerchantHashKey = String.Empty;
	protected String TargetUrl = String.Empty;
	protected String EdiDate = String.Empty;
	protected String Hash_String = String.Empty;
	protected String RequestDealPaymentUrl = String.Empty;
	protected String merchantTxnNum = String.Empty;
	protected String channelType = String.Empty;


	public void Show( StateBag state) {

		MerchantEncKey = ConfigurationManager.AppSettings["MerchantEncKey"];
		MerchantHashKey = ConfigurationManager.AppSettings["MerchantHashKey"];
		RequestDealApproveUrl = ConfigurationManager.AppSettings["RequestDealApproveUrl"];
		RequestDealPage = ConfigurationManager.AppSettings["RequestDealPage"];
		EncodeKey = ConfigurationManager.AppSettings["EncodeKey"];
		MID = ConfigurationManager.AppSettings["MID"];
		RequestDealPaymentUrl = ConfigurationManager.AppSettings["RequestDealPaymentUrl"];
		channelType = ConfigurationManager.AppSettings["channelType"];
		merchantTxnNum = DateTime.Now.ToString("yyyyMMddHH24mmss");

        //this.ViewState["orderItem"] = state["order_item"] == null ? "" : state["order_item"].ToString();

        this.ViewState["month"] = state["month"] == null ? "" : state["month"];     // 선물금결제시 사용
		this.ViewState["returnUrl"] = state["returnUrl"];
		this.ViewState["jumin"] = state["jumin"];
		this.ViewState["ci"] = state["ci"];
		this.ViewState["di"] = state["di"];
		this.ViewState["good_name"] = state["good_name"];
        this.ViewState["good_mny"] = state["good_mny"];

        this.ViewState["firstPay"] = state["firstPay"];

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 페이지별 처리
        this.ViewState["payPageType"] = state["payPageType"];
        this.ViewState["user_name"] = state["user_name"] == null ? "" : state["user_name"];
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 페이지별 처리

        var payItem = state["payItem"].ToString().ToObject<pay_item_session>();
        var payInfo = payItem.data;
        this.ViewState["payInfo"] = payInfo;
        this.ViewState["orderId"] = payItem.orderId;

        this.ViewState["payItem"] = state["payItem"];

        //Amt = (payInfo.ToObject<PayItemSession.Entity>()).amount.ToString();
        Amt = state["good_mny"].ToString();
		

		EdiDate = DateTime.Now.ToString("yyyyMMddHHmmss");  //전문생성일시
		String md_src = EdiDate + MID + Amt;  //위변조 처리
		
		CnsPayCipher chiper = new CnsPayCipher();
		Hash_String = chiper.SHA256Salt(md_src, EncodeKey);

		ph_content.Visible = true;

        //Pay Log 생성 (결제모듈 요청)
        string sponsorId = "", userName = "", mobile = "";
        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
            userName = sess.UserName;
            mobile = sess.Mobile;
        }
        string codeId = "", group = "";
        var payInfo2 = payItem.data.ToObject<PayItemSession.Entity>();
        if (payInfo2 != null)
        {
            codeId = payInfo2.codeId.EmptyIfNull();
            group = payInfo2.group.EmptyIfNull();
        }
        string strLog = string.Empty;
        strLog += "sponsorId:" + sponsorId + ", ";
        strLog += "orderId:" + payItem.orderId + ", ";
        strLog += "good_mny:" + Request.Form["good_mny"] + ", ";
        strLog += "codeId:" + codeId + ", ";

        strLog += "group:" + group + ", ";
        strLog += "buyr_name:" + state["buyr_name"] + ", ";
        strLog += "userName:" + userName + ", ";
        strLog += "mobile:" + mobile + ", ";

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제전 로그 생성
        //-----------------------------------------------------------------------------
        // USER-AGENT 구분
        //-----------------------------------------------------------------------------
        string WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
        {
            WebMode = "MOBILE";
        }
        else
        {
            WebMode = "PC";
        }
        strLog += "WebMode:" + WebMode + " ";
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제전 로그 생성

        ErrorLog.Write(HttpContext.Current, 600, strLog);
    }

    public void Hide( ) {

		ph_content.Visible = false;
	}

	protected override void OnLoad(EventArgs e) {
		
		base.OnLoad(e);
		
	}


}
