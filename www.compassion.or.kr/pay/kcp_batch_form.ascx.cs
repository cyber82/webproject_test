﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using CommonLib;


public partial class pay_kcp_batch_form : System.Web.UI.UserControl {
	
	public void Show( StateBag state) {

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 개발/운영에 따라 결제 Form URL 처리
        if (ConfigurationManager.AppSettings["stage"] == "dev")
        {   // 개발
            this.ViewState["appFormUrl"] = "https://testpay.kcp.co.kr/plugin/payplus_web.jsp";
        }
        else
        {   // 운영
            this.ViewState["appFormUrl"] = "https://pay.kcp.co.kr/plugin/payplus_web.jsp";
        }
        //[jun.heo] 2017-12 : 결제 모듈 통합

        this.ViewState["returnUrl"] = state["returnUrl"] == null ? "" : state["returnUrl"];
		this.ViewState["failUrl"] = state["failUrl"] == null ? "" : state["failUrl"];
		this.ViewState["site_cd"] = state["site_cd"];
		this.ViewState["user_name"] = state["user_name"];
		this.ViewState["kcpgroup_id"] = state["kcpgroup_id"];
		//this.ViewState["childMasterId"] = state["childMasterId"];
		this.ViewState["motiveCode"] = state["motiveCode"] == null ? "" : state["motiveCode"];
		this.ViewState["motiveName"] = state["motiveName"] == null ? "" : state["motiveName"];
        //this.ViewState["campaignId"] = state["campaignId"];
        //	this.ViewState["codeId"] = state["codeId"];
        //	this.ViewState["codeName"] = state["codeName"];

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = state["payPageType"];
        this.ViewState["paymentType"] = state["paymentType"] == null ? "" : state["paymentType"];

        this.ViewState["payItem"] = "";
        this.ViewState["payInfo"] = "";
        this.ViewState["orderId"] = "";

        //이종진 2018-03-12 - 결제실패 시, pay-again 페이지로 가기위한 url을 들고다님
        this.ViewState["againUrl"] = state["againUrl"] == null ? "" : state["againUrl"];

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리
        string codeId = "", group = "", campaignId = "", good_mny = "", code_name = "", month = "", frequency = "", childKey = "", childMasterId = "", TypeName = "";
        switch (state["payPageType"].ToString())
        {
            case "PayAccount":

                break;
            case "Regular":
            default:
                this.ViewState["payItem"] = state["payItem"];
                var payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
                var payInfo = payItem.data;
                this.ViewState["payInfo"] = payInfo;
                this.ViewState["orderId"] = payItem.orderId;

                // sponsorID가 없는 상태에서 해외회원으로 체크하고 결제하는 경우 sponsorID로 ensure 되지 않기때문에,
                // 다시한번 ensure 한다.
                var payObj = payInfo.ToObject<PayItemSession.Entity>();
                if (payObj.type == PayItemSession.Entity.enumType.CDSP)
                {
                    var childAction = new ChildAction();
                    childAction.Ensure(payObj.childMasterId);
                }

                codeId = payInfo.ToObject<PayItemSession.Entity>().codeId.EmptyIfNull();
                group = payInfo.ToObject<PayItemSession.Entity>().group.EmptyIfNull();
                campaignId = payInfo.ToObject<PayItemSession.Entity>().campaignId.EmptyIfNull();
                good_mny = payInfo.ToObject<PayItemSession.Entity>().amount.ToString().EmptyIfNull();
                code_name = payInfo.ToObject<PayItemSession.Entity>().codeName.EmptyIfNull();
                month = payInfo.ToObject<PayItemSession.Entity>().month.ToString().EmptyIfNull();
                frequency = payInfo.ToObject<PayItemSession.Entity>().frequency.EmptyIfNull();
                childKey = payInfo.ToObject<PayItemSession.Entity>().childKey.EmptyIfNull();
                childMasterId = payInfo.ToObject<PayItemSession.Entity>().childMasterId.EmptyIfNull();
                TypeName = payInfo.ToObject<PayItemSession.Entity>().TypeName.EmptyIfNull();

                break;
        }
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제전 로그 생성
        //Pay Log 생성 (결제모듈 요청)
        string sponsorId = "", userName = "", mobile = "", sponsorUserId = "";
        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
            userName = sess.UserName;
            mobile = sess.Mobile;
            sponsorUserId = sess.UserId;
        }

        string strLog = string.Empty;
        strLog += "SponsorID:" + sponsorId + ", ";
        strLog += "OrderID:" + this.ViewState["orderId"].ToString() + ", ";
        strLog += "UserID:" + sponsorUserId + ", ";
        strLog += "CampaignID:" + campaignId + ", ";
        strLog += "Group:" + group + ", ";
        strLog += "ResultCode:" + "" + ", ";
        strLog += "mid:" + "" + ", ";
        strLog += "tid:" + "" + ", ";
        strLog += "moid:" + "" + ", ";
        strLog += "amt:" + good_mny + ", ";
        strLog += "cardcode:" + "" + ", ";
        strLog += "cardname:" + "" + ", ";
        strLog += "cardbin:" + "" + ", ";
        strLog += "cardpoint:" + "" + ", ";
        strLog += "codeID:" + codeId + ", ";
        strLog += "buyr_name:" + userName + ", ";
        strLog += "UserName:" + userName + ", ";
        strLog += "Mobile:" + mobile + ", ";
        strLog += "IsUser:" + userName + ", ";    // 회원(회원명), 비회원, 무기명
        strLog += "codename:" + code_name + ", ";
        strLog += "Childkey:" + childKey + ", ";
        strLog += "ChildMasterID:" + childMasterId + ", ";
        strLog += "month:" + month + ", ";
        strLog += "frequency:" + frequency + ", ";
        strLog += "TypeName:" + TypeName + ", ";

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 비전트립인 경우 로그 추가
        if (state["payPageType"].ToString() == "VisionTrip")
        {
            strLog += "plandetail_id:" + this.ViewState["plandetail_id"] + ", ";
        }

        //-----------------------------------------------------------------------------
        // USER-AGENT 구분
        //-----------------------------------------------------------------------------
        string WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
        {
            WebMode = "MOBILE";
        }
        else
        {
            WebMode = "PC";
        }
        strLog += "WebMode:" + WebMode + ", ";

        strLog += "payPageType:" + state["payPageType"] + " ";  //[jun.heo] 2017-12 : 결제 모듈 통합 - Log 에 결제 종류 추가
        ErrorLog.Write(HttpContext.Current, 604, strLog);
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제전 로그 생성

        ph_content.Visible = true;

	}

	public void Hide() {

		ph_content.Visible = false;
	}

	protected override void OnLoad(EventArgs e) {
		
		base.OnLoad(e);
		
	}


}
