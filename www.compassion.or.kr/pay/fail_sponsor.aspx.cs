﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class pay_fail_sponsor : FrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		
		
		if(PayItemSession.HasCookie(this.Context)) {

			var payInfo = PayItemSession.GetCookie(this.Context);


			if(payInfo == null) {
				Response.Redirect("/");
			}
			
			lb_title.Text = payInfo.TypeName;
			
			var r = Request["r"].ValueIfNull("/");
			goPay.HRef = r;
		}

	}


}