﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete_firstbirthday.aspx.cs" Inherits="pay_complete_firstbirthday" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script>

</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
<section class="sub_body">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>첫 생일 <em>첫 나눔</em></h1>
				<span class="desc">우리 아기의 첫 나눔이 또 다른 귀한 생명의 희망이 됩니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents sponsor">

			<div class="special first">
				
					<div class="w980">

						<div class="sub_tit mb40">
							<h2 class="tit blue mb50"><em>첫 생일 첫 나눔 신청</em>이 완료되었습니다.</h2>

							<p class="apply_complete">
								첫 생일 첫 나눔을 통해 기부해주신 후원금은<br />
								엄마와 아기들의 생명을 살리는<br />
								태아·영아생존 후원에 귀하게 사용됩니다.<br />
								생명을 살리는 소중한 일에 동참해주셔서 감사합니다.
							</p>
						</div>

						<div class="tac">
							<a href="/" class="btn_type1">메인</a>
						</div>
						
					</div>
				
				
			</div>
			<div class="h100"></div>
			
		</div>	
		<!--// e: sub contents -->

		
    </section>
</asp:Content>
