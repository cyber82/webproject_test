﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using CommonLib;


public partial class pay_kcp_form : System.Web.UI.UserControl {
	
	public void Show( StateBag state) {
        
        
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 개발/운영에 따라 결제 Form URL 처리
        if (ConfigurationManager.AppSettings["stage"] == "dev")
        {   // 개발
            this.ViewState["appFormUrl"] = "https://testpay.kcp.co.kr/plugin/payplus_web.jsp";
        }
        else
        {   // 운영
            this.ViewState["appFormUrl"] = "https://pay.kcp.co.kr/plugin/payplus_web.jsp";
        }
        //[jun.heo] 2017-12 : 결제 모듈 통합

        this.ViewState["site_cd"] = state["site_cd"];
        this.ViewState["used_card_YN"] = state["used_card_YN"];
        this.ViewState["used_card"] = state["used_card"];
        this.ViewState["good_name"] = state["good_name"];
        this.ViewState["good_mny"] = state["good_mny"];
        this.ViewState["pay_method"] = state["pay_method"];
        this.ViewState["buyr_name"] = state["buyr_name"] == null ? "" : state["buyr_name"];  // 추가

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 페이지별 처리
        this.ViewState["amount_type"] = state["amount_type"] == null ? "" : state["amount_type"];
        this.ViewState["plandetail_id"] = state["plandetail_id"] == null ? "" : state["plandetail_id"]; // 기획트립ID
        this.ViewState["order_item"] = state["order_item"] == null ? "" : state["order_item"];

        this.ViewState["month"] = state["month"] == null ? "" : state["month"];		// 선물금결제시 사용
		this.ViewState["returnUrl"] = state["returnUrl"] == null ? "" : state["returnUrl"]; //[jun.heo] 2017-12 : 결제 모듈 통합 - 예외처리
        this.ViewState["overseas_card"] = state["overseas_card"] == null ? "" : state["overseas_card"]; //[jun.heo] 2017-12 : 결제 모듈 통합 - 예외처리

        this.ViewState["jumin"] = state["jumin"] == null ? "" : state["jumin"]; //[jun.heo] 2017-12 : 결제 모듈 통합 - 예외처리
        this.ViewState["ci"] = state["ci"] == null ? "" : state["ci"]; //[jun.heo] 2017-12 : 결제 모듈 통합 - 예외처리
        this.ViewState["di"] = state["di"] == null ? "" : state["di"]; //[jun.heo] 2017-12 : 결제 모듈 통합 - 예외처리

        this.ViewState["motiveCode"] = state["motiveCode"] == null ? "" : state["motiveCode"];
		this.ViewState["motiveName"] = state["motiveName"] == null ? "" : state["motiveName"];

		this.ViewState["payItem"] = state["payItem"] == null ? "" : state["payItem"];   //[jun.heo] 2017-12 : 결제 모듈 통합 - 예외처리
        this.ViewState["firstPay"] = state["firstPay"] == null ? "" : state["firstPay"];   //[jun.heo] 2017-12 : 결제 모듈 통합 - 예외처리

        this.ViewState["user_name"] = state["user_name"];

        //[jun.heo] 2017-12 : 결제 모듈 통합
        this.ViewState["payPageType"] = state["payPageType"] == null ? "" : state["payPageType"].ToString();  // 결제 페이지 종류

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 페이지별 처리
        string codeId = "", group = "";

        pay_item_session payItem;
        var payInfo = "";

        this.ViewState["quotaopt"] = "0";
        this.ViewState["payInfo"] = "";
        switch (this.ViewState["payPageType"].ToString())
        {
            case "VisionTrip":  // 비전트립
                this.ViewState["ordr_idxx"] = state["ordr_idxx"];
                this.ViewState["quotaopt"] = "12";  // 카드할부 개월수
                this.ViewState["paymentType"] = "";
                break;
            case "Store":   // 스토어
                this.ViewState["ordr_idxx"] = state["ordr_idxx"];
                this.ViewState["quotaopt"] = "12";  // 카드할부 개월수
                this.ViewState["paymentType"] = "";
                break;
            case "PayAccount":  // 계좌관리
                this.ViewState["ordr_idxx"] = "";
                this.ViewState["quotaopt"] = "0";  // 카드할부 개월수
                this.ViewState["paymentType"] = state["paymentType"];
                break;
            case "PayDelay":    // 지연된 후원금
                payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
                payInfo = payItem.data;
                this.ViewState["payInfo"] = payInfo;

                //[jun.heo] 2017-12 : 결제 모듈 통합 - 다른 결제 Form 과 통일을 위해 orderId --> ordr_idxx 로 변경
                this.ViewState["ordr_idxx"] = payItem.orderId;
                this.ViewState["quotaopt"] = "0";  // 카드할부 개월수
                this.ViewState["paymentType"] = "";
                break;
            case "Temporary":   // 일시후원 : 특별한 모금 및 나머지
            default:
                payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
                if (payItem == null)
                {
                    this.Hide();
                    return;
                }
                payInfo = payItem.data;
                this.ViewState["payInfo"] = payInfo;

                //[jun.heo] 2017-12 : 결제 모듈 통합 - 다른 결제 Form 과 통일을 위해 orderId --> ordr_idxx 로 변경
                this.ViewState["ordr_idxx"] = payItem.orderId;
                this.ViewState["quotaopt"] = "0";  // 카드할부 개월수
                this.ViewState["paymentType"] = "";
                break;
        }
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 페이지별 처리

        ph_content.Visible = true;

        //Pay Log 생성 (결제모듈 요청)
        string sponsorId = "", userName = "", mobile = "", sponsorUserId = "";
        if (UserInfo.IsLogin)
        {
            var sess = new UserInfo();
            sponsorId = sess.SponsorID;
            userName = sess.UserName;
            mobile = sess.Mobile;
            sponsorUserId = sess.UserId;
        }
        
        payItem = this.ViewState["payItem"].ToString().ToObject<pay_item_session>();
        if (payItem != null)
        {
            var payInfo2 = payItem.data.ToObject<PayItemSession.Entity>();
            if (payInfo2 != null)
            {
                codeId = payInfo2.codeId.EmptyIfNull();
                group = payInfo2.group.EmptyIfNull();
            }
        }

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제전 로그 생성
        string strLog = string.Empty;
        strLog += "SponsorID:" + sponsorId + ", ";
        strLog += "OrderID:" + this.ViewState["ordr_idxx"] + ", ";
        strLog += "UserID:" + sponsorUserId + ", ";
        strLog += "CampaignID:" + state["campaignId"] + ", ";
        strLog += "Group:" + group + ", ";
        strLog += "ResultCode:" + "" + ", ";
        strLog += "mid:" + "" + ", ";
        strLog += "tid:" + "" + ", ";
        strLog += "moid:" + "" + ", ";
        strLog += "amt:" + state["good_mny"] + ", ";
        strLog += "cardcode:" + "" + ", ";
        strLog += "cardname:" + "" + ", ";
        strLog += "cardbin:" + "" + ", ";
        strLog += "cardpoint:" + "" + ", ";
        strLog += "codeID:" + codeId + ", ";
        strLog += "buyr_name:" + state["buyr_name"] + ", ";
        strLog += "UserName:" + userName + ", ";
        strLog += "Mobile:" + mobile + ", ";
        strLog += "IsUser:" + state["buyr_name"] + ", ";    // 회원(회원명), 비회원, 무기명
        strLog += "codename:" + "" + ", ";
        strLog += "Childkey:" + "" + ", ";
        strLog += "ChildMasterID:" + "" + ", ";
        strLog += "month:" + "" + ", ";
        strLog += "frequency:" + "" + ", ";
        strLog += "TypeName:" + state["codeName"] + ", ";

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 비전트립인 경우 로그 추가
        if (state["payPageType"].ToString() == "VisionTrip")
        {
            strLog += "plandetail_id:" + this.ViewState["plandetail_id"] + ", ";
        }

        //-----------------------------------------------------------------------------
        // USER-AGENT 구분
        //-----------------------------------------------------------------------------
        string WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
        {
            WebMode = "MOBILE";
        }
        else
        {
            WebMode = "PC";
        }
        strLog += "WebMode:" + WebMode + ", ";

        strLog += "payPageType:" + state["payPageType"] + " ";  //[jun.heo] 2017-12 : 결제 모듈 통합 - Log 에 결제 종류 추가
        strLog += "good_mny:" + state["good_mny"] + " ";
        ErrorLog.Write(HttpContext.Current, 604, strLog);
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제전 로그 생성
    }

    public void Hide( ) {

		ph_content.Visible = false;
	}

	protected override void OnLoad(EventArgs e) {
		
		base.OnLoad(e);
		
	}


}
