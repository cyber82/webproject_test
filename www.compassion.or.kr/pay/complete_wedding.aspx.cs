﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class pay_complete_wedding : FrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		var requests = Request.GetFriendlyUrlSegments();
		var orderId = "";
		if(requests.Count > 0) {
			orderId = requests[0];
		}

		if(!string.IsNullOrEmpty(orderId)) {

			var payItem = new PayItemSession.Store().Get(orderId);
			//if(payItem == null || payItem.is_completed) {
			if(payItem == null) {
				Response.Redirect("/");
			}
			
			var payInfo = payItem.data.ToObject<PayItemSession.Entity>();
			if(payInfo.type == PayItemSession.Entity.enumType.CSP_WEDDING) {

				//		Response.Write(payInfo.ToJson());
				if(!payItem.is_completed)
					DoCSPWeddingWork(payInfo);
			}

            /*         이벤트 정보 등록  -- 20170327 이정길       */
            var eventID = Session["eventID"];
            var eventSrc = Session["eventSrc"];
            if (eventID == null)
            {
                if (Request.Cookies["e_id"] != null)
                {
                    eventID = Request.Cookies["e_id"].Value;
                    eventSrc = Request.Cookies["e_src"].Value;
                }
            }
            try
            {
                CommonLib.WWW6Service.SoaHelperSoap _www6Service;
                _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

                using (FrontDataContext dao = new FrontDataContext())
                {
                    if (eventID != null && eventSrc != null && !eventID.Equals(""))
                    {
                        //dao.sp_tEventApply_insert_f(int.Parse(eventID.ToString()),
                        //                            new UserInfo().UserId,
                        //                            new UserInfo().SponsorID,
                        //                            new UserInfo().ConId,
                        //                            eventSrc.ToString(),
                        //                            "결혼첫나눔_특별한나눔" + "(" + payInfo.frequency + ")",
                        //                            "EVENT_SUBMIT",
                        //                            payInfo.amount,
                        //                            payInfo.commitmentId);
                        //dao.SubmitChanges();
                        Object[] objSql = new object[1] { "sp_tEventApply_insert_f" };
                        Object[] objParam = new object[] {   "evt_id"
                                                           , "UserID"
                                                           , "SponsorID"
                                                           , "ConID"
                                                           , "evt_route"
                                                           , "SponsorType"
                                                           , "SponsorTypeEng"
                                                           , "SponsorAmount"
                                                           , "CommitmentID" };
                        Object[] objValue = new object[] {   int.Parse(eventID.ToString())
                                                           , new UserInfo().UserId
                                                           , new UserInfo().SponsorID
                                                           , new UserInfo().ConId
                                                           , eventSrc.ToString()
                                                           , "결혼첫나눔_특별한나눔" + "(" + payInfo.frequency + ")"
                                                           , "EVENT_SUBMIT"
                                                           , payInfo.amount
                                                           , payInfo.commitmentId };
                        string dbName = "SqlCompassionWeb";
                        string dbType = "SP";
                        int insertResult = _www6Service.Tx_ExecuteQuery(dbName, objSql, dbType, objParam, objValue);

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, "이벤트 정보 등록 error :: " + ex.Message);
            }

            new PayItemSession.Store().Complete(orderId);
		}

	}


	// 결혼첫나눔
	void DoCSPWeddingWork( PayItemSession.Entity payInfo )
    {
        CommonLib.WWW6Service.SoaHelperSoap _www6Service;
        _www6Service = new CommonLib.WWW6Service.SoaHelperSoapClient();

        using (FrontDataContext dao = new FrontDataContext())
        {
            if (payInfo.relation_key > 0)
            {
                //var entity = dao.tCspWedding.First(p => p.Idx == payInfo.relation_key && p.SponsorID == payInfo.sponsorId);
                try
                {
                    string dbType = "Text";
                    string dbName = "SqlCompassionWeb";
                    Object[] objSql = new object[1] { "Update tCspWedding set PaymentYN = 'Y' where Idx = '" + payInfo.relation_key + "' and SponsorID = '" + payInfo.sponsorId + "'" };
                    int updateResult = _www6Service.Tx_ExecuteQuery(dbName, objSql, dbType, null, null);
                }
                catch (Exception ex)
                {
                    ErrorLog.Write(HttpContext.Current, 0, "PaymentYN 수정 error :: " + ex.Message);
                }
                
                new SpecialFunding().AfterWork(payInfo);
            }

        }

	}

	// 특별한후원 메일
	class SpecialFunding
    {
		public void AfterWork( PayItemSession.Entity payInfo ) {

			var sf_id = payInfo.relation_key;
			
			if(UserInfo.IsLogin) {

				// 특별한나눔 스탬프
				var added = new StampAction().Add("sf").success;

				this.SendMailPayComplete(payInfo);
			}
		}

		void SendMailPayComplete(PayItemSession.Entity payInfo ) {

			try {

				var actionResult = new SponsorAction().GetCommunications();
				if(!actionResult.success) {
					return;
				}

				var email = ((SponsorAction.CommunicationResult)actionResult.data).Email;
				if(string.IsNullOrEmpty(email))
					return;

				var from = ConfigurationManager.AppSettings["emailSender"];

				string amountMonth = "원";
				if(payInfo.frequency == "정기") {
					amountMonth = "원/월";

				}

				var args = new Dictionary<string, string> {
					{"{pic}" , "/common/img/mail/mail_img_WD.jpg".WithFileServerHost() } ,
					{"{title}" , "결혼 첫 나눔"} ,
					{"{userName}" , new UserInfo().UserName} ,
					{"{frequency}" , payInfo.frequency } ,
					{"{pay_method}" , payInfo.PayMethodName } ,
					{"{amount}" , payInfo.amount.ToString("N0") } ,
					{"{amountMonth}" , amountMonth }

				};

				Email.Send(HttpContext.Current, from, new List<string>() { email },
				string.Format("[한국컴패션] 후원 신청이 완료되었습니다"),
				"/mail/sf-pay-complete.html",
				args
			, null);

			} catch(Exception e) {
				ErrorLog.Write(HttpContext.Current, 0, e.Message);
				throw e;
			}

		}

	}

}