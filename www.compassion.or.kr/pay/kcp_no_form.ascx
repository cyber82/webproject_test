﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="kcp_no_form.ascx.cs" Inherits="pay_kcp_no_form" %>
<asp:PlaceHolder runat="server" ID="ph_content" Visible="false">
    <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
    <script type="text/javascript" src="/pay/kcp.js"></script>
    <%--<script type="text/javascript" src="/store/pay/kcp.js"></script>--%>
	<div style="display:none">
            <iframe id="hd_ex_frm" name="hd_ex_frm"></iframe>
            <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
            <form id="ex_frm" name="ex_frm" method="post" action="/pay/kcp_no">
            <%--<form id="ex_frm" name="ex_frm" method="post" action="/store/pay/kcp_no">--%>
	            <input type="hidden" name="buyr_mail" class="w200" value="" maxlength="30" />
	            <input type="hidden" name="buyr_tel1" class="w100" value=""/>
	            <input type="hidden" name="buyr_tel2" class="w100" value=""/>
		
	            <input type="hidden" id="order_item" name="order_item" value="<%:this.ViewState["order_item"].ToString() %>" />
            </form>

            <script type="text/javascript">
                var frm = document.ex_frm;
                frm.target = "hd_ex_frm";
                frm.submit();
            </script>
    </div>
</asp:PlaceHolder>