﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="cms_form.ascx.cs" Inherits="pay_cms_form" %>
<asp:PlaceHolder runat="server" ID="ph_content" Visible="false">

	<script type="text/javascript" src="/common/js/site/cms/CC_Object.js?v=1"></script>
	<script type="text/javascript" src="/common/js/site/cms/init.js"></script>

    <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
	<script type="text/javascript" src="/pay/cms.js?v=1"></script>
    <%--<script type="text/javascript" src="/sponsor/pay/regular/cms.js"></script>--%>

		<div style="display:none">
	<iframe id="hd_ex_frm" name="hd_ex_frm" style="width:1000px;height:500px;border:1px solid red"></iframe>

    <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
	<form id="ex_frm" name="ex_frm" method="post" action="/pay/cms">
    <%--<form id="ex_frm" name="ex_frm" method="post" action="/sponsor/pay/regular/cms">--%>

		<textarea id="src" name="src" rows="20" cols="40" ><%:this.ViewState["src"].ToString() %></textarea>
		<textarea id="signed_data" name="signed_data" rows="20" cols="40" ></textarea>

		<input type="hidden" id="relation" name="relation" value="<%:this.ViewState["relation"].ToString() %>"/>
        <input type="hidden" id="cms_owner" name="cms_owner" value="<%:this.ViewState["cms_owner"].ToString() %>"/>
		<input type="hidden" id="cms_account" name="cms_account" value="<%:this.ViewState["cms_account"].ToString() %>"/>
		<input type="hidden" id="birth" name="birth" value="<%:this.ViewState["birth"].ToString() %>"/>
		<input type="hidden" id="bank_name" name="bank_name" value="<%:this.ViewState["bank_name"].ToString() %>"/>
		<input type="hidden" id="bank_code" name="bank_code" value="<%:this.ViewState["bank_code"].ToString() %>"/>
		<input type="hidden" id="cmsday" name="cmsday" value="<%:this.ViewState["cmsday"].ToString() %>"/>

		<input type='hidden' name='returnUrl'      value='<%:this.ViewState["returnUrl"].ToString() %>'>
		<input type='hidden' name='failUrl'      value='<%:this.ViewState["failUrl"].ToString() %>'>
		<input type='hidden' id="motiveCode" name='motiveCode'      value='<%:this.ViewState["motiveCode"].ToString() %>'>
		<input type='hidden' id="motiveName" name='motiveName'      value='<%:this.ViewState["motiveName"].ToString() %>'>
		<input type='hidden' id="payInfo" name='payInfo'      value='<%:this.ViewState["payInfo"].ToString() %>'>
		<input type="hidden" id="ordr_idxx" name="ordr_idxx" value="<%:this.ViewState["orderId"].ToString() %>" />

        <%--[jun.heo] 2017-12 : 결제 모듈 통합--%>
        <input type="hidden" name="paymentType" value="<%:this.ViewState["paymentType"].ToString() %>" />
        <input type="hidden" id="payPageType" name="payPageType" value="<%:this.ViewState["payPageType"].ToString() %>" />

		<!--// KCP -->
	</form>
			</div>
</asp:PlaceHolder>