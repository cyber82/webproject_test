﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Text;

public partial class pay_payco_callback : System.Web.UI.Page {

	protected string Result;
	protected string WebMode;                     //USER-AGENT를 분석하여 호출 모드를 설정
	
	string sellerOrderReferenceKey, reserveOrderNo, orderNo, orderCertifyKey, memberName;
	int totalOrderAmt, totalDeliveryFeeAmt, totalRemoteAreaDeliveryFeeAmt, totalPaymentAmt;
	
	public uint amount;
    public string pi, jumin, ci, di, month, buyr_name, isFirstPay;
	string sponsorId, userId , userName;
    public string payPageType;  //[jun.heo] 2017-12 : 결제 모듈 통합

    WWWService.Service _wwwService = new WWWService.Service();
	WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();


	private payco_util pu = new payco_util();

    string m_readValue = string.Empty;

    protected void Page_Load( object sender, EventArgs e ) {
		
		//-----------------------------------------------------------------------------
		// 이 문서는 text/html 형태의 데이터를 반환합니다. ( OK 또는 ERROR 만 반환 )
		//-----------------------------------------------------------------------------
		Response.ContentType = "text/html";

		//-----------------------------------------------------------------------------
		// (로그) 호출 시점과 호출값을 파일에 기록합니다.
		//-----------------------------------------------------------------------------
		string receive_str = "payco_callback.aspx is Called - ";

		foreach(string key in Request.Form.Keys) {
			receive_str += key + " : " + Request.Form[key] + ", ";
		}
		pu.Write_Log(receive_str);


		Response.Charset = "UTF-8";

		//-----------------------------------------------------------------------------
		// USER-AGENT 구분
		//-----------------------------------------------------------------------------
		WebMode = Request.UserAgent.ToLower();

		if(!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0)) {
			WebMode = "MOBILE";
		} else {
			WebMode = "PC";
		}
		EXEC_API();
	}

	protected void EXEC_API() {
		//-----------------------------------------------------------------------------
		// 오류가 발생했는지 기억할 변수와 결과를 담을 변수를 선언합니다.
		//-----------------------------------------------------------------------------
		Boolean ErrBoolean;
		string readValue, resultValue;
		ErrBoolean = false;                                         //기본적으로 오류가 아닌것으로 설정

		readValue = Request.Form["response"];
        m_readValue = readValue;
        //-----------------------------------------------------------------------------
        // response 값이 없으면 에러(ERROR)를 돌려주고 로그를 기록한 뒤 API를 종료합니다.
        //-----------------------------------------------------------------------------
        if (readValue.Equals("")) {
			resultValue = "Parameter is nothing.";
			pu.Write_Log("payco_callback.aspx send Result : ERROR (" + resultValue + ")");
			Response.Write("ERROR");
			Response.End();
		}

		try {
			//-----------------------------------------------------------------------------
			// Payco 에서 송신하는 값(response)을 JSON 형태로 변경
			// 데이터 확인에 필요한 값을 변수에 담아 처리합니다.
			//-----------------------------------------------------------------------------
			JObject Read_Data = JObject.Parse(readValue);
			pu.Write_Log("payco_callback.aspx receive json data : " + Read_Data.ToString());            // 디버그용

			//-----------------------------------------------------------------------------
			// 이곳에 가맹점에서 필요한 데이터 처리를 합니다.
			// 예) 재고 체크, 매출금액 확인, 주문서 생성 등등
			//-----------------------------------------------------------------------------
			//-----------------------------------------------------------------------------
			// 수신 데이터 사용 예제( 재고 체크 )
			//-----------------------------------------------------------------------------
			string ItemCode = "", ItemName = "";
			int ItemStock;

			foreach(JToken orderProducts in Read_Data["orderProducts"]) {
				ItemCode = orderProducts["orderProductNo"].ToString();  //상품 코드
																		//-----------------------------------------------------------------------------
																		// ItemCode 로 DB 에서 재고 수량 체크
																		// ( DB 에서 상품명과 재고를 읽어와 ItemName과 ItemStock 에 넣었다고 가정 )
																		//-----------------------------------------------------------------------------
				ItemName = orderProducts["sellerOrderProductReferenceKey"].ToString();
				ItemStock = 10;                 //연동 실패를 테스트 하시려면 값을 0 으로 설정하시고 정상으로 테스트 하시려면 1보다 큰 값을 넣으세요.
				if(ItemStock < 1) {         //재고가 1보다 작다면 오류로 설정
					ErrBoolean = true;
					break;
				}
			}
			
			

			//-----------------------------------------------------------------------------
			// 수신 데이터 사용 호출 예제( 주문서 데이터 )
			//-----------------------------------------------------------------------------


			sellerOrderReferenceKey = Read_Data["sellerOrderReferenceKey"].ToString();                  // 가맹점에서 발급했던 주문 연동 Key , customerOrderNumber
			reserveOrderNo = Read_Data["reserveOrderNo"].ToString();                                    // PAYCO에서 발급한 주문예약번호
			orderNo = Read_Data["orderNo"].ToString();                                                  // PAYCO에서 발급한 주문번
			orderCertifyKey = Read_Data["orderCertifyKey"].ToString();                                  // PAYCO에서 발급한 인증값
			memberName = Read_Data["memberName"].ToString();                                            // 주문자명
			totalOrderAmt = (int)Read_Data["totalOrderAmt"];                                            // 총 주문 금액
			totalDeliveryFeeAmt = (int)Read_Data["totalDeliveryFeeAmt"];                                // 총 배송비 금액
			totalRemoteAreaDeliveryFeeAmt = (int)Read_Data["totalRemoteAreaDeliveryFeeAmt"];            // 총 추가배송비 금액
			totalPaymentAmt = (int)Read_Data["totalPaymentAmt"];                                        // 총 결제 금액
                                                                     

            pi = new PayItemSession.Store().Get(sellerOrderReferenceKey).data;
            
            month = Request["month"];
            userId = Request["userId"];
            sponsorId = Request["sponsorId"];
            isFirstPay = Request["firstPay"];
            payPageType = Request["payPageType"];  //[jun.heo] 2017-12 : 결제 모듈 통합

            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
            switch (payPageType)
            {
                case "PayDelay":    // 지연된 후원금인 경우
                    jumin = Request["jumin"];
                    ci = Request["ci"];
                    di = Request["di"];
                    buyr_name = Request["buyr_name"];
                    userName = Request["userName"];
                    break;
                case "Temporary":   // 일시후원 및 나머지
                default:
                    // 20161206 수정
                    var payInfo = pi.ToObject<PayItemSession.Entity>();
                    var extra = payInfo.extra.Decrypt().ToObject<Dictionary<string, object>>();
                    ci = extra["ci"].ToString();
                    di = extra["di"].ToString();
                    jumin = extra["jumin"].ToString();
                    buyr_name = extra["buyr_name"].ToString();
                    userName = extra["userName"].ToString();
                    break;
            }


		//	ErrorLog.Write(this.Context, 0, "extra : " + extra.ToJson());
			pu.Write_Log("payco_callback.aspx pay_item : " + pi);

            string orderProductNo, sellerOrderProductReferenceKey, orderProductStatusCode;
			string orderProductStatusName = "", productKindCode, productPaymentAmt, originalProductPaymentAmt;

			foreach(JObject orderProduct in Read_Data["orderProducts"]) {
				orderProductNo = orderProduct["orderProductNo"].ToString();                                    //주문상품번호
				sellerOrderProductReferenceKey = orderProduct["sellerOrderProductReferenceKey"].ToString();    //가맹점에서 보낸 상품키값
				orderProductStatusCode = orderProduct["orderProductStatusCode"].ToString();                    //주문상품상태코드
				orderProductStatusName = orderProduct["orderProductStatusName"].ToString();                    //주문상품상태명
				productKindCode = orderProduct["productKindCode"].ToString();                                  //상품종류코드
				productPaymentAmt = orderProduct["productPaymentAmt"].ToString();                              //상품금액
				originalProductPaymentAmt = orderProduct["originalProductPaymentAmt"].ToString();              //상품원금액
			}
			pu.Write_Log("orderProductStatusName : " + orderProductStatusName);               //변수 읽기 샘플

			string paymentTradeNo, paymentMethodCode, paymentAmt, paymentMethodName;
			JToken nonBankbookSettleInfo;
			string bankName, bankCode, accountNo, paymentExpirationYmd;
			JToken cardSettleInfo;
			string cardCompanyName, cardCompanyCode, cardNo = "", cardInstallmentMonthNumber;
			JToken couponSettleInfo;
			JToken realtimeAccountTransferSettleInfo;
			string discountAmt, discountConditionAmt;

			foreach(JObject paymentDetail in Read_Data["paymentDetails"]) {
				paymentTradeNo = paymentDetail["paymentTradeNo"].ToString();                                                //결제수단별거래번호
				paymentMethodCode = paymentDetail["paymentMethodCode"].ToString();                                          //결제수단코드
				paymentAmt = paymentDetail["paymentAmt"].ToString();                                                        //결제수단 사용금액
				paymentMethodName = paymentDetail["paymentMethodName"].ToString();                                          //결제수단명
				switch(paymentMethodCode) {
					case "02":                                                                                              //무통장입금
						nonBankbookSettleInfo = paymentDetail["nonBankbookSettleInfo"];                                     //무통장입금 결제정보
						bankName = nonBankbookSettleInfo["bankName"].ToString();                                            //은행명
						bankCode = nonBankbookSettleInfo["bankCode"].ToString();                                            //은행코드 
						accountNo = nonBankbookSettleInfo["accountNo"].ToString();                                          //계좌번호
						paymentExpirationYmd = nonBankbookSettleInfo["paymentExpirationYmd"].ToString();                    //입금만료일 
						break;
					case "31":                                                                          //신용카드(일반) '신용카드
						cardSettleInfo = paymentDetail["cardSettleInfo"];
						cardCompanyName = cardSettleInfo["cardCompanyName"].ToString();                                  //카드사명
						cardCompanyCode = cardSettleInfo["cardCompanyCode"].ToString();                                  //카드사코드 
						cardNo = cardSettleInfo["cardNo"].ToString();                                                    //카드번호	
						cardInstallmentMonthNumber = cardSettleInfo["cardInstallmentMonthNumber"].ToString();            //할부개월(MM)
						break;
					case "35":                                                                          //계좌이체 '바로이체
						realtimeAccountTransferSettleInfo = paymentDetail["realtimeAccountTransferSettleInfo"];  //실시간계좌이체 결제정보
						bankName = realtimeAccountTransferSettleInfo["bankName"].ToString();                   //은행명
						bankCode = realtimeAccountTransferSettleInfo["bankCode"].ToString();                   //은행코드 
						break;
					case "76":                                                                          //쿠폰사용정보
						couponSettleInfo = paymentDetail["couponSettleInfo"];
						discountAmt = couponSettleInfo["discountAmt"].ToString();                                          //쿠폰사용금액
						discountConditionAmt = couponSettleInfo["discountConditionAmt"].ToString();                        //쿠폰사용조건금액
						break;
					case "98":                                                                          //포인트 사용정보
						break;
				}
			}
			pu.Write_Log("paymentDetails's CardNumber : " + cardNo);                                       //변수 읽기 샘플


            //-----------------------------------------------------------------------------
            // 기타 주문서 생성에 필요한 정보를 가지고 주문서를 작성합니다.
            // SERVICE API 가 처음 호출 되었을 때 PAYCO 주문번호로 주문서가 이미 만들어져 있다면 오류(ERROR) 입니다.
            // SERVICE API 가 가결제건으로 인해 재 호출 되었을 때 PAYCO 주문번호로 주문서가 이미 만들어져 있다면 정상처리(OK)를 합니다.
            //-----------------------------------------------------------------------------

            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 구분
            switch (payPageType)
            {
                case "PayDelay":    // 지연된 후원금인 경우
                    ErrBoolean = !this.DoSave_PayDelay();
                    break;
                case "Temporary":   // 일시후원 및 나머지
                default:
                    ErrBoolean = !this.DoSave_Temporary();
                    break;
            }
            

			// todo

			//-----------------------------------------------------------------------------
			// 결과값을 생성
			//-----------------------------------------------------------------------------
			if(ErrBoolean) {
				resultValue = "ERROR";  //오류가 있으면 ERROR를 설정
			} else {
				resultValue = "OK";     //오류가 없으면 OK 설정
			}

			//-----------------------------------------------------------------------------
			//오류일 경우 상세내역을 기록하고 전체 취소 API( payco_cancel.asp )를 호출 합니다.
			//-----------------------------------------------------------------------------
			if(resultValue.Equals("ERROR")) {

				JObject cancelOrder;

				cancelOrder = new JObject();
				cancelOrder.Add("sellerKey", JToken.FromObject(ConfigurationManager.AppSettings["pc_sellerKey"]));                             //가맹점 코드. payco_config.asp 에 설정 (필수)
				cancelOrder.Add("sellerOrderReferenceKey", JToken.FromObject(sellerOrderReferenceKey)); //취소주문연동키. ( 파라메터로 넘겨 받은 값 ) (필수)
				cancelOrder.Add("cancelTotalAmt", JToken.FromObject(totalPaymentAmt));                  //주문서의 총 금액을 입력합니다. (전체취소, 부분취소 전부다) (필수)
				cancelOrder.Add("orderCertifyKey", JToken.FromObject(orderCertifyKey));                 //PAYCO에서 발급한 인증값 (필수)

				pu.Write_Log("payco_callback.aspx is Item Error : try cancel : " + cancelOrder.ToString());

				Result = pu.payco_cancel(cancelOrder.ToString());
				pu.Write_Log("payco_callback.aspx cancel API Result : " + Result);
				//-----------------------------------------------------------------------------
				// 취소 결과가 오류인 경우 가결제건이 생성되니 PAYCO로 문의 부탁드립니다.
				//-----------------------------------------------------------------------------
				if(Result.Equals("200")) {
					//취소 결과가 정상
				} else {
					//취소 실패
				}
			}

		} catch(Exception ex) {
			resultValue = "ERROR";
			pu.Write_Log("payco_callback.aspx Logical Error : Number - 9999, Description - " + ex.ToString());
			Response.Write(resultValue);
			Response.End();
		}

		//-----------------------------------------------------------------------------
		// 결과값을 파일에 기록한다.( 디버그용 )
		//-----------------------------------------------------------------------------
		pu.Write_Log("payco_service.aspx send result : " + resultValue);
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 결과를 PAYCO 쪽에 리턴 ( OK 또는 ERROR )
		//-----------------------------------------------------------------------------
		Response.Write(resultValue);
	}

    //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리 - 일시후원용 DoSave()
    bool DoSave_Temporary() {

        pu.Write_Log("DoSave : payInfo.group : " + "Start");

        try {
			string sResult = "";

            string refUrl = "";
            if (Request.UrlReferrer != null)
                refUrl = Request.UrlReferrer.ToString();

            string firstPay = "0";
            if (refUrl.IndexOf("pay_again") > 0 || isFirstPay == "1")
                firstPay = "1";

            //신규 GlobalPool 조회방법인지 체크
            //string strdbgp_kind = HttpContext.Current.Request.Cookies["sdbgp_kind"].Value;
            string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();

            var action = new CommitmentAction();
			JsonWriter result = new JsonWriter();
			var payInfo = pi.ToObject<PayItemSession.Entity>();
			//ErrorLog.Write(HttpContext.Current, 0, "DoSave : " + payInfo.ToJson());
			DataSet dsResult = new DataSet();

            string mobile = "";
			if(string.IsNullOrEmpty(sponsorId)) {

				#region  로그인 되어있지 않으면 비회원 결제를 위해 sponsorId 구하기

				if(buyr_name == "무기명") {
					sponsorId = "20160621142650372";

				} else {
					// 해외회원 , 무기명인경우는 CI가 없음
					if(!string.IsNullOrEmpty(ci)) {
						Object[] objSql = new object[1] { string.Format("SELECT SponsorID FROM tSponsorMaster WHERE CurrentUse = 'Y' and (userId is null or userId = '') and CI <> '' and CI = '{0}'", ci) };
						DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

						if(ds.Tables[0].Rows.Count > 0) {
							sponsorId = ds.Tables[0].Rows[0]["SponsorID"].ToString().Trim();
						}
					}

					// sponsorId 가 없으면 새로 생성
					if(string.IsNullOrEmpty(sponsorId)) {

						try {

							sponsorId = _wwwService.GetDate().ToString("yyyyMMddHHmmssff2");
                            /*
							objSql = new object[1] { string.Format(@"insert into tSponsorMaster (sponsorId , sponsorName ,juminId ,channelType , currentUse ,userDate , userClass , registerDate , modifyDate , di , ci) values ('{0}','{1}','{2}','Web' , 'Y' , getdate() , '비회원' , getdate() , getdate() , '{3}' , '{4}') ",
								sponsorId , buyr_name , jumin , di , ci) };
							//	Response.Write(objSql[0].ToJson());
							//	return;
							_www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);
							*/

                            // tSponsorMaster에 LetterPreference, CorrReceiveType 컬럼은 default값으로 처리 ('Digital', 'online')
                            var sponsorResult = _wwwService.registerDATSponsor2(sponsorId, buyr_name, jumin, "", "", "", "", (string.IsNullOrEmpty(ci)) ? "국외" : "국내", "", "", ""
											   , "", "", CodeAction.GetGenderByJumin(jumin) /*gender*/
											   , "", "", "", "", ""
											   , "", "", "", "", DateTime.Now.ToString(), "14세이상", string.IsNullOrEmpty(ci) ? "" : DateTime.Now.ToString()
											   , string.IsNullOrEmpty(ci) ? "" : "서울신용평가", sponsorId, buyr_name, "", "", "", "", "", "", di, ci, "0");

							if(sponsorResult.Substring(0, 2) == "30") {

								//base.AlertWithJavascript("회원님의 정보를 등록하지 못했습니다..(DAT) \\r\\n" + sponsorResult.Substring(2).ToString().Replace("\n", ""));
								return false;
							}
						} catch(Exception ex) {
							ErrorLog.Write(this.Context, 0, ex.Message);
							//base.AlertWithJavascript("비회원 정보를 등록하는 중에 에러가 발생했습니다.");
							return false;
						}
					}
				}
				#endregion
				
				payInfo.sponsorId = sponsorId;
				payInfo.sponsorName = buyr_name;
				payInfo.userId = "";

			} else {
				payInfo.sponsorId = sponsorId;
				payInfo.sponsorName = userName;
				payInfo.userId = userId;
                if (UserInfo.IsLogin)
                {
                    var sess = new UserInfo();
                    mobile = sess.Mobile;
                }
            }

            pu.Write_Log("DoSave : payInfo.group : " + payInfo.group);

            //Pay Log 생성 (결제모듈 응답)
            StringBuilder sb = new StringBuilder();
            sb.Append("sponsorId:" + sponsorId + ", ");
            sb.Append("sUserID:" + userId + ", ");
            sb.Append("payInfo.campaignId:" + payInfo.campaignId.EmptyIfNull() + ", ");
            sb.Append("payInfo.group:" + payInfo.group.EmptyIfNull() + ", ");
            sb.Append("response:" + m_readValue + ", ");
            sb.Append("codeId:" + payInfo.codeId.EmptyIfNull() + ", ");
            sb.Append("userName:" + payInfo.sponsorName + ", ");
            sb.Append("mobile:" + mobile + ", ");

            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
            sb.Append("Childkey:" + payInfo.childKey + ", ");
            sb.Append("ChildMasterID:" + payInfo.childMasterId + ", ");
            sb.Append("month:" + payInfo.month + ", ");
            sb.Append("frequency:" + payInfo.frequency + ", ");
            sb.Append("TypeName:" + payInfo.TypeName + ", ");

            //-----------------------------------------------------------------------------
            // USER-AGENT 구분
            //-----------------------------------------------------------------------------
            string WebMode = Request.UserAgent.ToLower();
            if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
            {
                WebMode = "MOBILE";
            }
            else
            {
                WebMode = "PC";
            }
            sb.Append("WebMode:" + WebMode + " ");
            //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성

            ErrorLog.Write(HttpContext.Current, 603, sb.ToString());

            if (payInfo.group == "CDSP") {
				result = action.DoCDSP(payInfo.childMasterId, payInfo.childKey, payInfo.campaignId, payInfo.codeName, "", "", payInfo.amount, payInfo.month, firstPay);
                result.success = true;
			} else if(payInfo.group == "GIFT") {
				result = action.DoGIFT(sponsorId, payInfo.childMasterId, payInfo.amount.ToString(), "payco", month, "일시", payInfo.codeId);
			} else {
                // 일반후원 첫 결제의 경우 일반 결연목록 조회 후 commitmentid 가져온다.
                if (string.IsNullOrEmpty(payInfo.commitmentId))
                {
                    DataSet dsActiveChild = _wwwService.listActiveCommitment(sponsorId, "0000000000", "", "");
                    DataRow[] alreadyRow = dsActiveChild.Tables[0].Select("SponsorItemEng = '" + payInfo.codeId + "' and FundingFrequency <> '1회' and CampaignID='" + payInfo.campaignId + "'");

                    if (alreadyRow.Length == 0)
                    {
                        result = action.DoCIV(sponsorId, payInfo.amount, payInfo.month, "payco", payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.childMasterId, payInfo.campaignId, "", "");
                        if (result.success)
                            payInfo.commitmentId = result.data.ToString();
                        else
                        {
                            result.success = false;
                            result.message = "후원 정보 저장에 실패했습니다.";
                        }
                    }
                    else
                    {
                        payInfo.commitmentId = alreadyRow[0]["CommitmentID"].ToString();

                        //정기후원이면서 이미 납부중인 양육보완프로그램이 있을경우 (일시후원은 항상 등록가능)
                        if (alreadyRow.Length > 0)
                        {
                            result.success = true;
                            payInfo.commitmentId = alreadyRow[0]["CommitmentID"].ToString();
                        }
                        else
                        {
                            result.success = false;
                            result.message = "후원 정보 조회에 실패했습니다.";
                        }
                    }
                }
                else
                {
                    result = action.DoCIV(sponsorId, payInfo.amount, payInfo.month, "payco", payInfo.frequency, payInfo.codeId, payInfo.codeName, payInfo.childMasterId, payInfo.campaignId, "", "");
                    if (result.success)
                        payInfo.commitmentId = result.data.ToString();
                    else
                    {
                        result.success = false;
                        result.message = "후원 정보 저장에 실패했습니다.";
                    }
                }
			}
			//	Response.Write(result.ToJson());
			if(!result.success) {
				//action.DeleteCommitment(result.data.ToString());
                //base.AlertWithJavascript(result.message);
				return false;
			}

            if (payInfo.group == "CDSP")
            {

                DataSet dsActiveChild = _wwwService.listActiveCommitment(sponsorId, payInfo.childMasterId, "CHISPO", "DS");
                if (dsActiveChild == null || dsActiveChild.Tables.Count <= 0 || dsActiveChild.Tables[0].Rows.Count == 0 )
                    result.message = "후원 정보 조회에 실패했습니다.";
                else
                    payInfo.commitmentId = dsActiveChild.Tables[0].Rows[0]["CommitmentID"].ToString();
            }

			// 후원아이디와 비회원의 경우 sponsorId,  이름 업데이트
			//payInfo.commitmentId = result.data.ToString();
			new PayItemSession.Store().Update(sellerOrderReferenceKey, payInfo,null);

			//	returnUrl.Value = "complete/" + result.data;    // commitmentId

			sResult = _wwwService.RegisterPaymentMasterEasypay("0024",sponsorId
												, totalOrderAmt.ToString()
												, ""
												, ""
												, ""
												, DateTime.Now.ToString("yyyyMMddHHmmss")
												, "10"
												, orderCertifyKey
												, sellerOrderReferenceKey
												, ""
												, ""
												, CodeAction.ChannelType
												, "Type:" + "" + ", Desc:" + ""
												, userId
												, userName);


			ErrorLog.Write(this.Context , 0 , "sResult>" + sResult);

			if(sResult.Length >= 2 && sResult.Substring(0, 2).Equals("30")) {

                action.DeleteCommitment(payInfo.commitmentId.ToString());
				return false;
				

			} else {


                if (payInfo.group == "GIFT")
                {
                    string commitmentIds = result.data.ToString();
                    ErrorLog.Write(this.Context, 0, "sponsorId>" + sponsorId + " , data=" + commitmentIds);
                    registerPartitioning_CommitmentGroup(sponsorId, commitmentIds);
                    updatePTD(commitmentIds);
                }
                else
                {
                    ErrorLog.Write(this.Context, 0, "sponsorId>" + sponsorId + " , data=" + payInfo.commitmentId.ToString());
                    registerPartitioning_CommitmentGroup(sponsorId, payInfo.commitmentId.ToString());
                    updatePTD(payInfo.commitmentId.ToString());
                }
			}

			if(payInfo.group == "CDSP") {
				
                //[이종진] 기존방법과 신규방법 if
                if (strdbgp_kind == "1")    //기존
                {
                    new ChildAction().Release(payInfo.childMasterId);
                }
                else //if(strdbgp_kind == "2")
                {   //신규
                    //GlobalSponsorID 생성, GlobalCommitment생성(킷발송), NomoneyHold및 임시결연정보삭제, tCommitmentMaster Update
                    result = action.ProcSuccessPay(payInfo.commitmentId.ToString());
                    if (!result.success) return false;
                    new ChildAction().Release(payInfo.childMasterId, false);
                }
            }

			return true;
		}catch(Exception ex) {
			ErrorLog.Write(HttpContext.Current , 0 , ex.ToString());
			return false;
		}
	}

    //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제 종류별 처리 - 지연된 후원금용 DoSave()
    bool DoSave_PayDelay()
    {

        //Pay Log 생성 (결제모듈 응답)
        StringBuilder sb = new StringBuilder();
        sb.Append("sponsorId:" + sponsorId + ", ");
        sb.Append("sUserID:" + userId + ", ");
        sb.Append("payInfo.campaignId:" + "" + ", ");
        sb.Append("payInfo.group:" + "CDSP" + ", ");
        sb.Append("response:" + m_readValue);

        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성
        sb.Append("Childkey:" + "" + ", ");
        sb.Append("ChildMasterID:" + "" + ", ");
        sb.Append("month:" + "" + ", ");
        sb.Append("frequency:" + "" + ", ");
        sb.Append("TypeName:" + "" + ", ");

        //-----------------------------------------------------------------------------
        // USER-AGENT 구분
        //-----------------------------------------------------------------------------
        string WebMode = Request.UserAgent.ToLower();
        if (!(WebMode.IndexOf("android") < 0 && WebMode.IndexOf("iphone") < 0 && WebMode.IndexOf("mobile") < 0))
        {
            WebMode = "MOBILE";
        }
        else
        {
            WebMode = "PC";
        }
        sb.Append("WebMode:" + WebMode + " ");
        //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제모듈 응답 로그 생성

        ErrorLog.Write(HttpContext.Current, 603, sb.ToString());

        string sResult = "";
        sResult = _wwwService.RegisterPaymentMasterEasypay("0024", sponsorId
                                                , totalOrderAmt.ToString()
                                                , ""
                                                , ""
                                                , ""
                                                , DateTime.Now.ToString("yyyyMMddHHmmss")
                                                , "10"
                                                , orderCertifyKey
                                                , sellerOrderReferenceKey
                                                , ""
                                                , ""
                                                , CodeAction.ChannelType
                                                , "Type:" + "" + ", Desc:" + ""
                                                , userId
                                                , userName);



        if (sResult.Length >= 2 && sResult.Substring(0, 2).Equals("30"))
        {

            return false;


        }
        else
        {

            SetDeliquentTodayPayment(sellerOrderReferenceKey);
            return true;

        }




    }

    protected bool registerPartitioning_CommitmentGroup( string sponsorId, string sCommitmentIDs ) {
		//----- Define --------------------------------------------------------
		string sNotificationID = sellerOrderReferenceKey;
		string sResult = string.Empty;

		//----- Do Something --------------------------------------------------
		try {

			sResult = _wwwService.paymentPartitiong_CommitmentGroup(sNotificationID, sCommitmentIDs.Split(','), sponsorId, buyr_name);
			return true;
		} catch(Exception ex) {
			ErrorLog.Write(this.Context, 0, ex.Message);
			return false;
		}
	}

	private bool updatePTD( string sCommitmentIDs ) {
		String sResult = String.Empty;

		try {
			
			var payInfo = pi.ToObject<PayItemSession.Entity>();

			string PTD = Convert.ToDateTime(DateTime.Now.AddMonths(payInfo.month).ToString("yyyy-MM-01")).AddDays(-1).ToString("yyyy-MM-dd");

			WWW6Service.SoaHelper _WWW6Service = new WWW6Service.SoaHelper();
            string now = DateTime.Now.ToString("yyyy-MM-dd");
			foreach(var sCommitmentID in sCommitmentIDs.Split(',')) {
			
				Object[] objSql = new object[1] { " SELECT SponsorItemEng " +
												" FROM   tCommitmentMaster " +
												" WHERE  CommitmentID = '" + sCommitmentID + "' " };

				DataSet ds = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

				if(ds.Tables[0].Rows.Count > 0) {
					if(ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "DS" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "LS" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "DSADD" || ds.Tables[0].Rows[0]["SponsorItemEng"].ToString() == "LSADD") {
				
				Object[] objSql2 = new object[1] { " UPDATE tCommitmentMaster " +
														" SET    PTD = '" + PTD + "'" +
                                                        " , FirstFundedDate = '" + now + "' " +
														" WHERE  CommitmentID = '" + sCommitmentID + "' " };

				DataSet iResult = _WWW6Service.NTx_ExecuteQuery("SqlCompass4", objSql2, "Text", null, null);
				
					}
				}
				
			}

			return true;
		} catch(Exception ex) {
			ErrorLog.Write(this.Context, 0, ex.Message);
			return false;
		}
	}

    private bool SetDeliquentTodayPayment(string xOrderNo)
    {
        //----- 세션선언

        bool sResult_Bool = false;
        //----- 결제
        try
        {


            //수정 2013-01-03
            //DataSet ds1 = (DataSet)Session["ds1"];

            //ErrorLog.Write(HttpContext.Current, 0, pi);
            var ds1 = pi.ToObject<DataSet>();
            //DataSet ds1 = (DataSet)Session["DataSet_" + sess.SponsorID];

            DataTable xPayOrder = new DataTable();
            xPayOrder.TableName = "PayOrderNo";
            xPayOrder.Columns.Add("PayOrderNo", typeof(string));
            xPayOrder.Rows.Add(xOrderNo);
            ds1.Tables.Add(xPayOrder);
            xPayOrder.Dispose();

            //수정 2013-01-03
            //Session.Remove("ds1");
            //	Session.Remove("DataSet_" + sess.SponsorID);

            //실행
            //string sResult = _wwwService.SetDeliquentTodayPayment(ds1, userId, userName);
            //UserName으로 테이블 생성시 오류로 인해 SponsorID로 테이블 생성으로 변경
            string sResult = _wwwService.SetDeliquentTodayPayment(ds1, sponsorId, sponsorId);

            if (sResult == "10")
            {
                sResult_Bool = true;
            }

        }
        catch (Exception ex)
        {
            
            ErrorLog.Write(this.Context, 0, ex.Message);
            return false;
        }

        return sResult_Bool;
    }
}
