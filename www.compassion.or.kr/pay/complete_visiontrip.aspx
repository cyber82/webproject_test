﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete_visiontrip.aspx.cs" Inherits="pay_complete_visiontrip" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	
	<script type="text/javascript">
		$(function () {

		});

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	 <!-- sub body -->
	<section class="sub_body">

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>나의 현지방문 보러가기</h1> 

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents store padding0">

			<div class="order_complete">
				<div class="wrap"> 
					<span class="con">
						<span class="tit mb10">비전트립
                            <asp:Literal runat="server" ID="spAmountType" />
                            <asp:Literal runat="server" ID="payLiteral0" /></span>
                        <br />
                        <br /> 
                    </span>
                    <div class="w980">
                        <div class="total_sum">
                            <span><asp:Literal runat="server" ID="payLiteral1" /><em><asp:Literal runat="server" ID="pay_method" /></em></span>
                            <span><asp:Literal runat="server" ID="payLiteral2" /><em class="fc_blue"><asp:Literal runat="server" ID="total" /></em></span>
                        </div>
                    </div>
                </div>

            </div> 
        </div>
        <!--// e: sub contents -->

        <div class="mb30">
            <div style="margin: 0 auto; text-align: center;">
                <a class="btn_s_type1" href="/my/activity/visiontrip/">나의 현지방문 바로가기</a>
            </div>
        </div>
        <div class="h100"></div>


    </section>
    <!--// sub body -->
     
</asp:Content>
