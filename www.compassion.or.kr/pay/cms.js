﻿
$(function () {

    //[jun.heo] 2017-12 : 결제 모듈 통합 - 결제정보관리(PayAccount)일 경우 주문번호 생성
    if ($("#ordr_idxx").val() == "PayAccount") {
        var today = new Date();
        var year = today.getFullYear();
        var month = today.getMonth() + 1;
        var date = today.getDate();
        var time = today.getHours() + "" + today.getMinutes() + "" + today.getSeconds() + "" + today.getMilliseconds(); // 밀리초 가져오기

        if (parseInt(month) < 10) {
            month = "0" + month;
        }

        var order_idxx = year + "" + month + "" + date + "" + time;

        $("#ordr_idxx").val(order_idxx);
    }

	setTimeout(function () {

		SignData();
		
	}, 300);
})


function SignData() {
    loading.show("결연 및 결제를 처리중입니다.");
    if (!init()) {
        loading.hide();
		return;
	}
	
	var ret;
	var signeddata, textin;
	var userdn;

	if (document.ex_frm.src.value == null || document.ex_frm.src.value == "") {
	    loading.hide();
	    alert("서명할 데이타가 없습니다.");
		return;
	}



	// 인증서 선택창 초기화 및 선택된 인증서의 DN 추출
	// DN은 인증기관에서 유니크한 것임.
	userdn = document.CC_Object_id.GetUserDN();
	if (userdn == null || userdn == "") {
	    loading.hide();
	    alert("사용자 DN 선택이 취소 되었습니다.");

	}
	else {

		// 전자서명 생성
		// BSTR *SignData(BSTR Source, BSTR HashAlgo, BSTR Password);
		// parameters : 
		//   Source : 전자서명할 메세지
		//   HashAlgo : 서명 알고리즘 ("SHA1", "MD5") ==> SHA1이 표준
		//   Password : 개인키 복호를 위한 패스워드
		// return value : 생성된 전자서명 값
		// 참 고 : 암호를 넣지 않았을 경우에는 암호 입력 다이얼로그 박스에 입력한다.
		signeddata = document.CC_Object_id.SignData(document.ex_frm.src.value, "SHA1", "");

		if (signeddata == null || signeddata == "") {
		    loading.hide();
		    errmsg = document.CC_Object_id.GetErrorContent();
			errcode = document.CC_Object_id.GetErrorCode();
			alert("SignData :" + errmsg);
			return;
		}
		else {

			getR = CC_Object_id.GetRFromKey(userdn, "");
			if (getR == "") {
			    loading.hide();
			    alert("주민번호/사업자번호를 확인할 수 없는 인증서입니다.");
				return;
			}

			document.ex_frm.signed_data.value = signeddata;
			document.ex_frm.target = "hd_ex_frm";
			document.ex_frm.submit();

		}
	}
}