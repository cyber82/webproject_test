﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete_wedding.aspx.cs" Inherits="pay_complete_wedding" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/assets/ZeroClipboard/ZeroClipboard.js" ></script>
	<script type="text/javascript">

		$(function () {

			ZeroClipboard.config({ swfPath: "/assets/ZeroClipboard/ZeroClipboard.swf" });

			var client = new ZeroClipboard($('#btn_copy'));
			client.on('ready', function (event) {

				client.on('copy', function (event) {
					var msg = "이 부부는 예식 비용의 일부를 한국컴패션에 기부하였습니다.\n나눔으로 시작한 두 사람의 동행을 진심으로 축복합니다.";
					event.clipboardData.setData('text/plain', msg);
				});

				client.on('aftercopy', function (event) {
					alert("안내문이 복사됐습니다.  \n원하는 곳에 붙여넣기(CTRL-V) 해주세요");
				});
			});

			client.on('error', function (event) {
				ZeroClipboard.destroy();
			});



			$("#btn_copy").click(function () {
				ZeroClipboard.create();
				return false;

			});

		});

		(function () {
			var app = angular.module('cps.page', []);
			app.controller("defaultCtrl", function ($scope, $http, $filter, popup) {

			});

		})();

	</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
<section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
		 
		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1><em>후원/결제</em></h1>
				<span class="desc">후원자님 사랑에 감사드립니다</span>

				<uc:breadcrumb runat="server"/>
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents sponsor">

			<div class="special first">
				
					<div class="w980">

						<div class="sub_tit mb40">
							<h2 class="tit blue"><em>결혼 첫 나눔 신청</em>이 완료되었습니다.</h2>
							<p class="mb50"><em class="fs18">두 사람이 하나되는 출발점에서 컴패션 어린이의 손을 잡아주셔서 감사합니다.</em></p>

							<div class="marriage_complete">
								
								<p class="s_comment"><span class="s_con9">샘플이미지입니다.</span></p>

								<p class="txt1">
									하나님의 자녀 홍길동, 신인선이 하나님 안에서 하나가 되어 온전히 사랑하겠습니다.<br />
									바쁘시더라도 귀한 걸음 하시어 축복해 주시기 바랍니다.<br />
									<span class="date">2016년 11월 6일 일요일 오후 1시 약수교회</span><br />

									<span class="name">
										<em>홍상직 &middot; 옥영향</em> 의 큰 아들<em class="ml15">길동</em><br />
										<em>신명화 &middot; 이사온</em> 의 막내 딸<em class="ml15">인선</em>
									</span>
								</p>

								<p class="txt2">
									<em>청첩장에 '결혼 첫 나눔' 안내문구를 넣어보세요!</em><br />
									<span class="copy_text">
										"이 부부는 예식 비용의 일부를 한국컴패션에 기부하였습니다.<br />
										&nbsp;&nbsp;나눔으로 시작한 두 사람의 동행을 진심으로 축복합니다."
									</span>
								</p>

								<div class="btn">
									<a href="#" id="btn_copy" class="btn_s_type2 mr5">안내문구 복사하기</a>
									<a href="/common/download/Logo.zip" class="btn_s_type2">컴패션 로고 다운로드</a>
								</div>
								
							</div>
						</div>

						<div class="tac">
							<a href="/" class="btn_type1">메인</a>
						</div>
						
					</div>
				
				
			</div>
			<div class="h100"></div>
			
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
</asp:Content>
