﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class pay_complete_visiontrip : FrontBasePage {

	public override bool RequireSSL {
		get {
			return true;
		}
	}

	public override bool NoCache {
		get {
			return true;
		}
	}

	public override bool RequireLogin {
		get {
			return true;
		}
	}

	const string listPath = "/my/activity/visiontrip/";

	protected override void OnBeforePostBack() {
		
		var requests = Request.GetFriendlyUrlSegments();
		if(requests.Count < 1) {
			Response.Redirect(listPath, true);
		}

		UserInfo sess = new UserInfo();
		base.PrimaryKey = requests[0]; 
        string sPayType= requests[1];
        int paymentID = Convert.ToInt32(requests[2]);

        int ID = 0;
        bool check = int.TryParse(base.PrimaryKey.ToString(), out ID);

        try
        {
            using (FrontDataContext dao = new FrontDataContext())
            {
                //var payInfo = dao.tVisionTripPayment.Where(p => p.PlanDetailID == ID && p.PaymentID == paymentID).FirstOrDefault();
                var payInfo = www6.selectQF<tVisionTripPayment>("PlanDetailID", ID, "PaymentID", paymentID);
                //var detail = dao.tVisionTripPlanDetail.Where(p => p.PlanDetailID == ID).FirstOrDefault();
                var detail = www6.selectQF<tVisionTripPlanDetail>("PlanDetailID", ID);

                if (payInfo.PaymentType.Equals("001000000000"))
                {
                    //가상계좌
                    spAmountType.Text = "";

                    payLiteral0.Text = "가상계좌 발급되었습니다.";
                    payLiteral1.Text = "은행 " + payInfo.BankName;
                    payLiteral2.Text = "가상계좌 " + payInfo.Account + "(가상계좌 발급일로부터 3일간 유효합니다.)";
                }
                else
                {
                    if (payInfo.PaymentResultYN == 'Y')
                    {
                        //실시간 계좌이체
                        string sPaymentName = string.Empty;
                        decimal dAmount = 0;
                        if (sPayType == "R")
                        {
                            sPaymentName = "신청비";
                            dAmount = Convert.ToDecimal(detail.RequestCost);
                        }
                        else if (sPayType == "T")
                        {
                            sPaymentName = "참가비용(트립비용-신청비)";
                            dAmount = Convert.ToDecimal(detail.TripCost);
                        }
                        else if (sPayType == "C")
                        {
                            sPaymentName = "후원 어린이 만남비";
                            dAmount = Convert.ToDecimal(detail.ChildMeetCost);
                        }


                        spAmountType.Text = sPaymentName;
                        total.Text = dAmount.ToString("C"); //String.Format("{0:C}", dAmount.ToString());  

                        if (payInfo.PaymentType == "100000000000")
                        {
                            pay_method.Text = "신용카드";
                        }
                        else if (payInfo.PaymentType == "010000000000")
                        {
                            pay_method.Text = "실시간 계좌이체";
                        }
                        else if (payInfo.PaymentType == "000010000000")
                        {
                            pay_method.Text = "휴대폰";
                        }
                        else if (payInfo.PaymentType == "payco")
                        {
                            pay_method.Text = "페이코";
                        }
                        else if (payInfo.PaymentType == "kakaopay")
                        {
                            pay_method.Text = "카카오페이";
                        }
                        //var imagePath = Uploader.GetRoot(Uploader.FileGroup.image_shop); 

                        payLiteral0.Text = "납부 완료 되었습니다.";
                        payLiteral1.Text = "결제 수단";
                        payLiteral2.Text = "납부 금액";
                    }
                }






            }
		} catch(Exception ex) {
            ErrorLog.Write(this.Context, 0, "Complete 오류 : " + ex.Message);
            Response.Write(ex);
			Response.Redirect(listPath , true);
		}

	}

}