﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="complete_store.aspx.cs" Inherits="pay_complete_store" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/pay/complete_store.js?v=1.0"></script>
	<script type="text/javascript">
		$(function () {

		});

	</script>
</asp:Content>

<asp:Content ID="head_script_GA" runat="server" ContentPlaceHolderID="head_script_GA">
    <script>
    //GA Product Start
    var orderDetailList = JSON.parse('<%=ViewState["orderDetailList"].ToString()%>');
    console.log('orderDetailList', orderDetailList);
    var productList = [];
    $.each(orderDetailList, function () {
        productList.push({
            'id': this.product_idx,  // 상품 코드           
            'name': this.productTitle, // 상품 이름           
            'brand': '한국컴패션', // 브랜드           
            'category': this.product_gift_flag ? '어린이선물' : '일반상품', // 상품 카테고리           
            'price': this.price, // 가격(\)           
            'quantity': this.qty, // 제품 수량           
            'variant': this.optionDetail // 상품 옵션 
        });
    });

    dataLayer.push({
        'ecommerce': {
            'currencyCode': 'KRW',
            'purchase': {
                'actionField': {
                    'id': '<%=this.ViewState["hdn_orderNo"].ToString()%>', // 주문번호         
                    'revenue': '<%=this.ViewState["hdn_revenue"].ToString()%>',   // 총 결제금액(\)         
                    //'tax': '<%=this.ViewState["hdn_discount"].ToString()%>',  // 할인금액(\)         
                    'shipping': '<%=this.ViewState["hdn_deliveryFee"].ToString()%>'  // 배송비(\)       
                },
                'products': productList
            }
        }
    });
    console.log('dataLayer', dataLayer);
    //GA Product End
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	 <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
    <input type="hidden" id="hdn_orderNo" runat="server" />
    <input type="hidden" id="hdn_revenue" runat="server" />
    <input type="hidden" id="hdn_discount" runat="server" />
    <input type="hidden" id="hdn_deliveryFee" runat="server" />

		<!-- 타이틀 -->
		<div class="page_tit">
			<div class="titArea">
				<h1>주문완료</h1>
				<span class="desc">마음이 담긴 따뜻한 손길로 희망을 선물해주세요</span>

				<uc:breadcrumb runat="server" />
			</div>
		</div>
		<!--// -->

		<!-- s: sub contents -->
		<div class="subContents store padding0">

			<div class="order_complete">
				<div class="wrap">
					<span class="icon">주문완료</span>
					<span class="con">
						<span class="tit mb10">회원님의 주문이 정상적으로 완료 되었습니다.</span><br />
						<span class="mb30">한국컴패션 스토어를 이용해주셔서 감사합니다.</span><br />
						<a href="/my/store/order/" class="btn_s_type4 fc_black">주문내역확인</a>
					</span>			
				</div>
				
			</div>

			<div class="w980">

				<!-- 장바구니 리스트 -->
				<div class="tableWrap3 mb40">
					<table class="tbl_type4 order complete">
						<caption>장바구니 리스트 테이블</caption>
						<colgroup>
							<col style="width:5%">
							<col style="width:13%">
							<col style="width:42%">
							<col style="width:20%">
							<col style="width:20%">
						</colgroup>
						<thead>
							<tr>
								<th scope="col"></th>
								<th scope="col" colspan="2">상품정보</th>
								<th scope="col">수량</th>
								<th scope="col">주문금액</th>
							</tr>
						</thead>
						<tbody>
                            <asp:Repeater ID="repeater" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <!-- 제품이미지 사이즈 : 98 * 98 -->
                                            <div class="prdt_pic">
                                                <a href="<%# Context.Href("/store/item/" , Eval("product_idx")) %>">
                                                    <img src='<%# Eval("name_img").ToString().WithFileServerHost()%>' style="width: 98px;" /></a>
                                            </div>
                                        </td>
                                        <td class="tal">
                                            <span class="prdt_name"><%# Eval("productTitle")%></span>
                                            <p class="prdt_option">(옵션) <%# Eval("optionDetail")%></p>
                                        </td>
                                        <td><%# Eval("qty")%></td>
                                        <td class="tit"><%# Eval("totalPrice" , "{0:N0}")%>원</td>
                                    </tr>
                                    <tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
					</table>
				</div>
				<!--// 장바구니 리스트 -->

				<!-- 주문금액 -->
				<div class="total_sum">
					<span>주문금액 합계<em><asp:Literal runat="server" ID="subtotal" /></em></span>
					<span class="pluse"></span>
					<span>배송비<em><asp:Literal runat="server" ID="delivery_fee" /></em></span>
					<span class="sum"></span>
					<span>총 주문 합계<em class="fc_blue"><asp:Literal runat="server" ID="total" /></em></span>
				</div>
				<!--// 주문금액 -->

				
			</div>
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->



	<asp:PlaceHolder runat="server" ID="ph_hidden" Visible="false" >
	<h4>배송정보</h4>
	<div>주문번호 <asp:Literal runat="server" ID="orderNo"/></div>
	<div>받는사람 <asp:Literal runat="server" ID="receiver"/></div>
	<div>주문일 <asp:Literal runat="server" ID="order_date"/></div>
	<div>휴대전화 <asp:Literal runat="server" ID="mobile"/></div>
	<div>배송지주소 <asp:Literal runat="server" ID="addr"/></div>
	
	<h4>주문정보</h4>

	<ul>

		<li>
			<a href="<%# Context.Href("/store/item/" , Eval("product_idx")) %>">
			<img style="max-width:50px" src=<%# Eval("name_img").ToString().WithFileServerHost()%> />
			<%# Eval("productTitle")%> / <%# Eval("optionDetail")%>	/ <%# Eval("childName")%>
			판매가  :<%# (Convert.ToInt32(Eval("totalPrice")) / Convert.ToInt32(Eval("qty"))).ToString("N0") %>
			수량 : <%# Eval("qty")%>
				상품구분 : <%# Eval("childID").ToString().Trim() == "" ? "일반" : "어린이에게<br />보내는 선물"%>
				구매가격 : <%# Eval("totalPrice" , "{0:N0}")%>
			</a>
		</li>	

	</ul>

	subtotal : 
	delivery : 
	total : 

	<h4>결제내역</h4>
	결제금액 : <asp:Literal runat="server" ID="total2" />
	결제수단 : <asp:Literal runat="server" ID="pay_method" />
	</asp:PlaceHolder>
</asp:Content>
