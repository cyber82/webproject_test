﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="kcp_no.aspx.cs" Inherits="pay_kcp_no" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>*** KCP Online Payment System [ASP.NET Version] ***</title>
	<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript">
		// 결제 중 새로고침 방지 샘플 스크립트
	    function noRefresh() {
	        /* CTRL + N키 막음. */
	        if ((event.keyCode == 78) && (event.ctrlKey == true)) {
	            event.keyCode = 0;
	            return false;
	        }
	        /* F5 번키 막음. */
	        if (event.keyCode == 116) {
	            event.keyCode = 0;
	            return false;
	        }
	    }

		document.onkeydown = noRefresh;

		$(function () {
            if ($("#result").val() == "Y") {
                //[jun.heo] 2017-12 : 결제 모듈 통합
                parent.location.href = "/pay/complete_store/" + $("#orderNo").val();
				//parent.location.href = "/store/complete/" + $("#orderNo").val();
			}
		})
	</script>
</head>
<body>
	<form runat="server" id="frm">
		<input type="hidden" id="orderNo" runat="server" value="" />
		<input type="hidden" id="result" runat="server" value="" />
	</form>
</body>
</html>

