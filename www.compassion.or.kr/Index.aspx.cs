﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CommonLib;
using Newtonsoft.Json.Linq;
using TCPTModel;
using TCPTModel.Response.Supporter;
using Newtonsoft.Json;
using System.Text;
using System.Configuration;

public partial class Index : FrontBasePage
{
    public WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

    protected override void OnBeforePostBack()
    {




        /*         이벤트 정보 등록  -- 20170327 이정길       */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;

        //김태형 2017.06.01
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else
        {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }
        
        base.OnBeforePostBack();
        
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.content.First(p => p.c_id == "main");
            var entity = www6.selectQF<content>("c_id", "main");
            var data = JObject.Parse(entity.c_text.ToString());

            children.Value = data["child"].ToString();
            main_content.Text = data["content"].ToString();

        }

        bool hasCDSP = false;

        if (UserInfo.IsLogin)
        {

            UserInfo sess = new UserInfo();

            DataTable childData = null;
            var actionResult = new SponsorAction().HasCDSP();
            if (actionResult.success)
            {

                childData = (DataTable)actionResult.data;
                if (childData.Rows.Count > 0)
                {
                    hasCDSP = true;

                    //	Response.Write(childData.ToJson());
                    var row = childData.Rows[0];
                    //childPic.Src = ChildAction.GetPicByChildKey(row["childKey"].ToString());
                    childPic.Src = new ChildAction().GetChildImage(row["childmasterId"].ToString(), row["childKey"].ToString());
                    userName.Text = sess.UserName;
                    childName.Text = row["namekr"].ToString();
                    btn_write_letter.HRef = btn_write_letter.HRef.Replace("{childmasterid}", row["childmasterId"].ToString());
                    btn_album.HRef = btn_album.HRef.Replace("{childmasterid}", row["childmasterId"].ToString()).Replace("{childkey}", row["childkey"].ToString());

                }
            }

            if (hasCDSP)
            {
                ph_sec1.Visible = ph_sec2.Visible = ph_sec5.Visible = ph_sec6.Visible = ph_sec8.Visible = ph_sec9.Visible = ph_sec12.Visible = ph_sec13.Visible = true;
            }
            else
            {
                ph_sec3.Visible = ph_sec4.Visible = ph_sec5.Visible = ph_sec6.Visible = ph_sec7.Visible = ph_sec8.Visible = ph_sec9.Visible = ph_sec10.Visible = ph_sec11.Visible = true;
            }

        }
        else
        {

            ph_sec3.Visible = ph_sec4.Visible = ph_sec5.Visible = ph_sec7.Visible = ph_sec8.Visible = ph_sec9.Visible = ph_sec10.Visible = ph_sec11.Visible = true;

        }

        this.GetPrayer(hasCDSP);

        this.GetSympathy();

        this.GetEvent();

        if (ph_sec3.Visible)
        {

            Random rnd = new Random();
            var index = rnd.Next(3);
            this.GetMainVisualChild(index);
        }

        if (ph_sec4.Visible)
        {
            this.GetChildren();
        }

        try
        {
            this.GetUserFunding();

            this.GetSpecialFunding();
        }
        catch { }
    }

    void GetSympathy()
    {

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_sympathy_list_latest_f();
            Object[] op1 = new Object[] {  };
            Object[] op2 = new Object[] {  };
            var list = www6.selectSP("sp_sympathy_list_latest_f", op1, op2).DataTableToList<sp_sympathy_list_latest_fResult>();

            repeater_sympathy.DataSource = list;
            repeater_sympathy.DataBind();
        }
    }

    void GetEvent()
    {

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_mainpage_list_f(10, "main_event");
            Object[] op1 = new Object[] { "count", "position", "display", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { 10, "main_event" };
            var list = www6.selectSP("sp_mainpage_list_f", op1, op2).DataTableToList<sp_mainpage_list_fResult>();

            repeater_event.DataSource = list;
            repeater_event.DataBind();
        }
    }

    void GetPrayer(bool hasCDSP)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.prayer.FirstOrDefault(p => p.p_date == DateTime.Today);
            var entity = www6.selectQF<prayer>("p_date", DateTime.Today);
            if (entity == null)
            {
                pn_prayer.Visible = pn_prayer2.Visible = false;
                return;
            }

            prayer_content2.InnerHtml = prayer_content.InnerHtml = entity.p_content.ToHtml();
            prayer_date2.InnerText = prayer_date.InnerText = entity.p_date.ToString("M월 d일 (ddd)");

            if (hasCDSP)
            {
                prayer_check2.Visible = prayer_check.Visible = entity.p_check;
            }
            else
            {
                //pn_prayer.Style.Add("height", "125px;");
                //pn_prayer2.Style.Add("height", "125px;");
            }
        }
    }

    // 비로그인 나오는 어린이 추천 3종 , 일별 캐싱
    void GetMainVisualChild(int index)
    {
        if (index == 0)
        {
            if (this.GetMainVisualType1())
            {
                main_recmd1.Visible = true;
            }
            else
            {
                this.GetMainVisualType3();
                main_recmd3.Visible = true;
            }
        }
        else if (index == 1)
        {
            if (this.GetMainVisualType2())
            {
                main_recmd2.Visible = true;
            }
            else
            {
                this.GetMainVisualType3();
                main_recmd3.Visible = true;
            }
        }
        else
        {
            this.GetMainVisualType3();
            main_recmd3.Visible = true;
        }


    }

    // 생일이 어제
    bool GetMainVisualType1()
    {

        var action = new ChildAction();
        var yesterday = DateTime.Now.AddDays(-1);
        var month = Convert.ToInt32(yesterday.ToString("MM"));
        var day = Convert.ToInt32(yesterday.ToString("dd"));

        var actionResult = action.GetChildren(null, null, month, day, null, 20, null, null, 1, 3, "new", -1, -1);

        //Response.Write("actionResult.success = " + actionResult.success);


        if (actionResult.success)
        {
            Random rnd = new Random();
            var items = (List<ChildAction.ChildItem>)actionResult.data;
            if (items.Count < 1)
                return false;
            var index = rnd.Next(items.Count);

            var item = items[index];

            main_recmd1_pic.Src = item.Pic;
            main_recmd1_country.Text = main_recmd1_country2.Text = item.CountryName;
            main_recmd1_age.Text = main_recmd1_age2.Text = item.Age.ToString();
            main_recmd1_gender.Text = item.Gender;
            main_recmd1_waitingdays.Text = item.WaitingDays.ToString();
            main_recmd1_name.Text = item.Name;
            main_recmd1_name2.Text = item.Name.Length > 5 ? item.Name.Substring(0, 5) + ".." : item.Name;
            main_recmd1_link.HRef = main_recmd1_link.HRef.Replace("{childMasterId}", item.ChildMasterId);

            return true;
        }
        else
        {
            return false;
        }
    }

    // 부모님이 안계신
    bool GetMainVisualType2()
    {

        var action = new ChildAction();

        var actionResult = action.GetChildren(null, null, null, null, null, 20, true, null, 1, 3, "new", -1, -1);
        if (actionResult.success)
        {
            Random rnd = new Random();
            //Response.Write(actionResult.data.ToJson());
            var items = (List<ChildAction.ChildItem>)actionResult.data;
            if (items.Count < 1)
                return false;
            var index = rnd.Next(items.Count);

            var item = items[index];

            main_recmd2_pic.Src = item.Pic;
            main_recmd2_country.Text = main_recmd2_country2.Text = item.CountryName;
            main_recmd2_age.Text = item.Age.ToString();
            main_recmd2_gender.Text = item.Gender;
            main_recmd2_waitingdays.Text = item.WaitingDays.ToString();
            main_recmd2_name.Text = item.Name;
            main_recmd2_name2.Text = item.Name.Length > 5 ? item.Name.Substring(0, 5) + ".." : item.Name;
            main_recmd2_link.HRef = main_recmd2_link.HRef.Replace("{childMasterId}", item.ChildMasterId);

            return true;
        }
        else
        {
            return false;
        }
    }

    // 랜덤
    bool GetMainVisualType3()
    {


        var action = new ChildAction();

        var actionResult = action.GetChildren(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);
        if (actionResult.success)
        {
            Random rnd = new Random();
            var items = (List<ChildAction.ChildItem>)actionResult.data;
            if (items.Count < 1)
                return false;
            var index = rnd.Next(items.Count);

            var item = items[index];

            main_recmd3_pic.Src = item.Pic;
            main_recmd3_country.Text = main_recmd3_country2.Text = item.CountryName;
            main_recmd3_age.Text = item.Age.ToString();
            main_recmd3_gender.Text = item.Gender;
            main_recmd3_waitingdays.Text = item.WaitingDays.ToString();
            main_recmd3_name.Text = item.Name;
            main_recmd3_name2.Text = item.Name.Length > 5 ? item.Name.Substring(0, 5) + ".." : item.Name;
            main_recmd3_link.HRef = main_recmd3_link.HRef.Replace("{childMasterId}", item.ChildMasterId);
            return true;
        }
        else
        {
            return false;
        }


    }

    // 1:1 어린이 양육 목록 , 일별 캐싱
    void GetChildren()
    {

        var action = new ChildAction();

        //var actionResult = action.GetChildren(null, null, null, null, null, 6, null, null, 1, 16, "waiting", -1, -1);
        /*20160207 후원>1:1어린이양육 기준으로 검색조건 수정*/
        var actionResult = action.GetChildren(null, null, null, null, null, 20, null, null, 1, 16, "new", -1, -1);
        if (actionResult.success)
        {

            var items = (List<ChildAction.ChildItem>)actionResult.data;

            if (items.Count > 0)
            {
                Random rnd = new Random();
                var start_index = rnd.Next(items.Count);
                var end_index = 3;
                List<ChildAction.ChildItem> selected_list = new List<ChildAction.ChildItem>();

                if (items.Count < 3)
                {
                    end_index = items.Count;
                }

                for (int i = 0; i < end_index; i++)
                {

                    var index = start_index + i;
                    if (index >= items.Count)
                    {
                        index = index - items.Count;
                    }

                    selected_list.Add(items[index]);
                }

                repeater_children.DataSource = selected_list;
                repeater_children.DataBind();
            }
        }

    }

    // 특별한나눔 , 시별 캐싱
    void GetSpecialFunding()
    {

        var today = DateTime.Now.ToString("yyyyMMddHH");
        var action = new ChildAction();

        {

            var exist = false;
            if (Application["main_special_funding"] != null)
            {
                var data = (KeyValuePair<string, object>)Application["main_special_funding"];
                if (data.Key == today)
                {
                    exist = true;
                }
                else
                {
                    Application["main_special_funding"] = null;
                }
            }

            if (!exist)
            {

                using (FrontDataContext dao = new FrontDataContext())
                {
                    //var items = dao.sp_tSpecialFunding_list_f("", "", "N", "", "Y").ToList();
                    Object[] op1 = new Object[] { "accountClassGroup", "accountClass", "fixedYN", "ufYN", "pick" };
                    Object[] op2 = new Object[] { "", "", "N", "", "Y" };
                    var items = www6.selectSP("sp_tSpecialFunding_list_f", op1, op2).DataTableToList<sp_tSpecialFunding_list_fResult>();
                    
                    var data = new KeyValuePair<string, object>(today, items);
                    Application["main_special_funding"] = data;


                }

            }

            if (Application["main_special_funding"] != null)
            {
                var data = (KeyValuePair<string, object>)Application["main_special_funding"];
                var items = (List<sp_tSpecialFunding_list_fResult>)data.Value;

                if (items.Count > 0)
                {
                    Random rnd = new Random();
                    var start_index = rnd.Next(items.Count);

                    List<sp_tSpecialFunding_list_fResult> selected_list = new List<sp_tSpecialFunding_list_fResult>();
                    for (int i = 0; i < 2; i++)
                    {

                        var index = start_index + i;
                        if (index >= items.Count)
                        {
                            index = index - items.Count;
                        }

                        Object[] objParam = new object[] { "CampaignID" };
                        Object[] objValue = new object[] { items[index].CampaignID };
                        Object[] objSql = new object[] { "sp_S_Campaign_PaymentTotal" };
                        DataSet ds = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["TOTAL"] != DBNull.Value)
                            {
                                long total = Convert.ToInt32(ds.Tables[0].Rows[0]["TOTAL"]);
                                items[index].sf_current_amount = total;
                            }
                            else
                            {
                                items[index].sf_current_amount = 0;
                            }
                        }


                        selected_list.Add(items[index]);
                    }
                    repeater_special_funding.DataSource = selected_list;
                    repeater_special_funding.DataBind();
                }

            }
        }


    }

    // 나눔펀딩 , 시간별 캐싱
    void GetUserFunding()
    {

        var today = DateTime.Now.ToString("yyyyMMddHH");
        var action = new ChildAction();

        {

            var exist = false;
            if (Application["main_user_funding"] != null)
            {
                var data = (KeyValuePair<string, object>)Application["main_user_funding"];
                if (data.Key == today)
                {
                    exist = true;
                }
                else
                {
                    Application["main_user_funding"] = null;
                }
            }

            if (!exist)
            {
                using (FrontDataContext dao = new FrontDataContext())
                {
                    //var items = dao.sp_tUserFunding_pick_list_f().ToList();
                    Object[] op1 = new Object[] {  };
                    Object[] op2 = new Object[] {  };
                    var items = www6.selectSP("sp_tUserFunding_pick_list_f", op1, op2).DataTableToList<sp_tUserFunding_pick_list_fResult>();

                    var data = new KeyValuePair<string, object>(today, items);
                    Application["main_user_funding"] = data;
                }
            }

            if (Application["main_user_funding"] != null)
            {
                var data = (KeyValuePair<string, object>)Application["main_user_funding"];
                var items = (List<sp_tUserFunding_pick_list_fResult>)data.Value;

                if (items.Count > 0)
                {
                    Random rnd = new Random();
                    var start_index = rnd.Next(items.Count);

                    List<sp_tUserFunding_pick_list_fResult> selected_list = new List<sp_tUserFunding_pick_list_fResult>();
                    for (int i = 0; i < 2; i++)
                    {

                        var index = start_index + i;
                        if (index >= items.Count)
                        {
                            index = index - items.Count;
                        }

                        selected_list.Add(items[index]);
                        if (items.Count == 1)
                        {
                            break;
                        }
                    }
                    repeater_user_funding.DataSource = selected_list;
                    repeater_user_funding.DataBind();
                }


            }
        }


    }
}