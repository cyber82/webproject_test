﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Common {
	public delegate void Nagivated(int page , string hash);
}

/// <remarks>
/// page에서 사용할때 주의할점!
/// 페이지 렌더링 순서때문에, LoadComplete 이벤트에서 처리해야 함. 
/// if (!Page.IsPostBack) this.LoadComplete += new EventHandler(list_LoadComplete);
/// </remarks>
/// 
namespace Common {
	public partial class Paging : System.Web.UI.UserControl {

		// 페이지이동 이벤트
		public event Nagivated Navigate;

		// 총 데이타수
		public int TotalRecords {
			get {
				if (this.ViewState["TotalRecords"] == null) this.ViewState["TotalRecords"] = 0;
				return Convert.ToInt32(this.ViewState["TotalRecords"]);
			}
			set { this.ViewState["TotalRecords"] = value; }

		}

		// 화면에 노출되는 데이타수
		public int RowsPerPage {
			get {
				if (this.ViewState["rowsPerPage"] == null) this.ViewState["rowsPerPage"] = 20;
				return Convert.ToInt32(this.ViewState["rowsPerPage"]);
			}
			set { this.ViewState["rowsPerPage"] = value; }
		}

		// 화면에 노출되는 페이지 갯수
		public int PagesPerGroup {
			get {
				if (this.ViewState["PagesPerGroup"] == null) this.ViewState["PagesPerGroup"] = 5;
				return Convert.ToInt32(this.ViewState["PagesPerGroup"]);
			}
			set { this.ViewState["PagesPerGroup"] = value; }
		}

		// 현재페이지
		public int CurrentPage {
			get {
				if (this.ViewState["CurrentPage"] == null) this.ViewState["CurrentPage"] = 1;
				return Convert.ToInt32(this.ViewState["CurrentPage"]);
			}
			set { this.ViewState["CurrentPage"] = (value < 1) ? 1 : value; }
		}

		public string Hash {
			get {
				if (this.ViewState["Hash"] == null)
					this.ViewState["Hash"] = "c";
				return this.ViewState["Hash"].ToString();
			}
			set {
				this.ViewState["Hash"] = value;
			}
		}

		public void SetPage(int page) {
			this.CurrentPage = page;
			this.Render();
		}

		// 화면의 시작페이지
		private int StartPage {
			get { return (this.CurrentPage - 1) / this.PagesPerGroup * this.PagesPerGroup + 1; }
		}

		// 화면의 마지막페이지
		private int EndPage {
			get { return StartPage + this.PagesPerGroup - 1; }
		}

		// 페이징 UI출력
		void Render() {
			container.Controls.Clear();
			for (int i = StartPage; i < EndPage + 1; i++) {
				LinkButton link = new LinkButton();
				link.CssClass = "cnt";
				link.ID = "btn_" + i.ToString();
				link.Text = i.ToString() + Environment.NewLine;
				link.Visible = false;
				link.CommandArgument = i.ToString();
				link.Click += new EventHandler(Move);
				
				container.Controls.Add(link);

				//container.Controls.Add(new LiteralControl("<br />"));

				var span = new HtmlGenericControl("span");
				span.Visible = false;
				span.ID = "span_" + i.ToString();
				span.InnerHtml = i.ToString() + Environment.NewLine;
				span.Attributes["class"] = "on";
				container.Controls.Add(span);
			}


		}

		protected override void OnLoad(EventArgs e) {
			this.Render();
			base.OnLoad(e);

		}

		// 페이징 계산
		public void Calculate(int total) {
			this.Render();

			try {
				if (total < 1) {
					next.Visible = prev.Visible = first.Visible = last.Visible = false;
					container.Visible = false;
					total = 1;
				} else {
					next.Visible = prev.Visible = first.Visible = last.Visible = true;
					container.Visible = true;
				}

				this.TotalRecords = total;
				int pageCount = total / this.RowsPerPage;
				if (total % this.RowsPerPage > 0) pageCount++;

				if (pageCount < EndPage) {
					for (int i = pageCount; i < EndPage + 1; i++) {
						((HtmlGenericControl)container.FindControl("span_" + i.ToString())).Visible = false;
						((LinkButton)container.FindControl("btn_" + i.ToString())).Visible = false;
					}
				}

				int endPage = (pageCount < EndPage) ? pageCount : EndPage;
				for (int i = StartPage; i < endPage + 1; i++) {
					if (this.CurrentPage.Equals(i)) {
						((HtmlGenericControl)container.FindControl("span_" + i.ToString())).Visible = true;
						((LinkButton)container.FindControl("btn_" + i.ToString())).Visible = false;
					} else {
						((HtmlGenericControl)container.FindControl("span_" + i.ToString())).Visible = false;
						((LinkButton)container.FindControl("btn_" + i.ToString())).Visible = true;
					}
				}

				// next
				next.CommandName = "next";
				next.CommandArgument = (CurrentPage < endPage)
					? (CurrentPage + 1).ToString()
					: "0";

				// prev
				prev.CommandName = "prev";
				prev.CommandArgument = (CurrentPage > 1)
					? (CurrentPage-1).ToString()
					: "0";

				// first
				first.CommandName = "first";
				first.CommandArgument = "1";

				// last
				last.CommandName = "last";
				last.CommandArgument = pageCount.ToString();

				if (endPage <= 1) {
				//	next.Visible = prev.Visible = first.Visible = last.Visible = false;
				}

				if (CurrentPage == 1) {
					first.OnClientClick = "return false;";
					prev.OnClientClick = "return false";
				} else {
					first.OnClientClick = "";
					prev.OnClientClick = "";
				}

				
				if (CurrentPage >= endPage) {
					next.OnClientClick = "return false";
					last.OnClientClick = "return false";
				} else {
					next.OnClientClick = "";
					last.OnClientClick = "";
				}

			} catch (Exception ex) {
				throw ex;
			}
		}

		// 페이지 이동
		protected void Move(object sender, EventArgs e) {
			LinkButton obj = sender as LinkButton;
			if (obj.CommandArgument.Equals("0")) return;
			this.CurrentPage = Convert.ToInt32(obj.CommandArgument);
			if (obj.CommandName.Equals("prev") || obj.CommandName.Equals("next") || obj.CommandName.Equals("first") || obj.CommandName.Equals("last")) {
				this.Render();
			}
			if (Navigate != null) Navigate(Convert.ToInt32(obj.CommandArgument) , this.Hash);
		}

	}
}