﻿<%@ WebHandler Language="C#" Class="common_handler_upload" %>

using System;
using System.Web;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;

public class common_handler_upload : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context)
    {
        // 선물편지 보내기할때 이미지 저장 (db의 byte이미지를 파일 생성하여 path에 저장)
        string path = context.Request["path"];
        int giftSeq = Convert.ToInt32(context.Request["giftSeq"]);

        Uploader.UploadResult result = new Uploader.UploadResult();
        result = new Uploader().UploadLetter(context, true, path, giftSeq);
        JsonWriter.Write(result, context);
    }


}