﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;

public partial class common_handler_upload : FrontBasePage {
	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack(){

		HttpPostedFile file = Request.Files[0];

		if(file != null && file.ContentLength > 0) {

			bool rename = Request["rename"].ValueIfNull("y") == "y";
			string path = Request["fileDir"];
			int limit = Convert.ToInt32(Request["limit"].ValueIfNull("2048"));
			string fileType = Request["fileType"].EmptyIfNull();
			string tempDir = ConfigurationManager.AppSettings["temp"];
			string fileName = file.FileName.ToLower();
            
            Uploader.UploadResult result = new Uploader.UploadResult();

			if(file.ContentLength / 1024 > limit) {
				result.success = false;
				result.msg = string.Format("최대 {0}kb까지 업로드 가능합니다. 업로드 파일 {1}kb", limit, file.ContentLength / 1024);
				msg.Text = result.ToJson();
				return;
			}

			if (fileName.IndexOf(".exe") > -1) {
				result.success = false;
				result.msg = "exe 확장자는 업로드할 수 없습니다.";
				msg.Text = result.ToJson();
				return;
			}

			if(fileType == "image") {
				if(fileName.IndexOf(".jpg") < 0 && fileName.IndexOf(".jpeg") < 0 && fileName.IndexOf(".gif") < 0 && fileName.IndexOf(".png") < 0) {

					result.success = false;
					result.msg = "jpg , jpeg , gif , png 확장자만 업로드 가능합니다.";
					msg.Text = result.ToJson();
					return;
				}
			} else if(fileType == "movie") {
				if(fileName.IndexOf(".zip") < 0 && fileName.IndexOf(".mp4") < 0 && fileName.IndexOf(".wmv") < 0 && fileName.IndexOf(".avi") < 0) {

					result.success = false;
					result.msg = "ZIP , MP4, WMV , AVI 확장자만 업로드 가능합니다.";
					msg.Text = result.ToJson();
					return;
				}
			}

			
			result = new Uploader().Upload(this.Context, rename, path, file);

			msg.Text = result.ToJson();

		}

	}
	
}