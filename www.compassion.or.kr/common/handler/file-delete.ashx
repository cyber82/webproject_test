﻿<%@ WebHandler Language="C#" Class="common_handler_file_delete" %>

using System;
using System.Web;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;

public class common_handler_file_delete : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest( HttpContext context ) {

		var fileNameWithPath = context.Request["path"];
		if(string.IsNullOrEmpty(fileNameWithPath))
			return;

		JsonWriter result = new JsonWriter() { success = false};

		if(!FrontLoginSession.HasCookie(context)) {
			result.message = "로그인정보가 없습니다.";
			result.Write(context);
			return;
		}

		var extension = fileNameWithPath.Substring(fileNameWithPath.LastIndexOf('.')).ToLower();
		if(!new List<string>() { ".jpg", ".png", "jpeg", "gif" }.Contains(extension)) {
			result.message = "허용되지 않은 확장자 입니다.";
			result.Write(context);
			return;
		}

		var path = HttpContext.Current.Server.MapPath(fileNameWithPath);
		if(File.Exists(path)) {
			File.Delete(path);
		}

		result.success = true;
		result.Write(context);

	}


}