﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="addressApi.aspx.cs" Inherits="_common_popup_addressApi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	
	<link rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" href="/common/js/jquery/jquery-ui-1.10.4.min.css" type="text/css" media="screen" />
	<link href="/common/css/common.css" rel="stylesheet" type="text/css" media="screen,print"/>
	<link href="/common/css/layout.css" rel="stylesheet" type="text/css" media="screen,print"/>
	<link href="/common/css/screen.ui.css" rel="stylesheet" type="text/css" media="screen,print"/>



	<script type="text/javascript" src="/assets/angular/1.4.8/angular.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0-beta.2/angular-sanitize.js"></script>

	<script type="text/javascript" src="/assets/moment/moment-with-locales.min.js"></script>
	<!--https://github.com/urish/angular-moment-->
	<script type="text/javascript" src="/assets/angular/angular-moment.min.js"></script>

	<script type="text/javascript" src="/common/js/angular/paging.js"></script>
	<script type="text/javascript" src="/common/js/site/angular-app.js?v=1.1"></script>
	<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>

	<script type="text/javascript">

		var onComplete = function (zipCode, addr1, addr2 , jibun) {
			var callback = $("#callback").val();
			opener[callback](zipCode, addr1, addr2, jibun);
			window.close();
		};

		(function () {

			var app = angular.module('cps.page', []);
			app.controller("defaultCtrl", function ($scope, $http, $filter, popup, paramService, $address) {

				var containerEl = $("body");
				$scope.address = {
					curStep: 1,
					addr1: "",
					addr2: "",
					addrRoad: null,
					addrJibun: null,

					instance: null,
					list: [],
					total: -1,
					//hasMore: false,

					params: {
						currentPage: 1,
						countPerPage: 10,
						confmKey: "<%:this.ViewState["confirmKey"].ToString()%>"
					},

					search: function (reload , params) {
						if (containerEl.find(".keyword").val() == "") {
							alert("도로명주소, 건물명 또는 지번을 입력하세요.");
							containerEl.find(".keyword").focus();
							return false;
						}

						$scope.address.curStep = 1;
						$scope.address.params.keyword = containerEl.find(".keyword").val();

						if (reload) {
							$scope.address.params.currentPage = 1;
							$scope.address.list = [];
						}

						$scope.address.params = $.extend($scope.address.params, params);
						
						$address.search($scope.address.params, function (result) {

							$scope.$apply(function () {
								if (result.success) {

									$scope.address.total = result.total;

									if (result.total > 0) {

										//console.log(result.data);
										//$scope.address.list = $.merge($scope.address.list, result.data);
										$scope.address.list = result.data;
										//$scope.address.hasMore = result.hasMore;

									}

								} else {
									alert("오류가 발생하였습니다. 잠시 뒤 다시 시도해주세요.");
								}

							});
						});


					},

					// 결과 선택
					selectAddress: function (item) {
						
						$scope.address.zipcode = item.zipcode;
						$scope.address.addr1 = item.road;// + "(" + item.jibun + ")"
						$scope.address.addrRoad = item.road;
						$scope.address.addrJibun = item.jibun;
						$scope.address.roadAddrPart1 = item.roadAddrPart1;	// 도로명 주소2
						$scope.address.roadAddrPart2 = item.roadAddrPart2;	// reference 
						$scope.address.rnMgtSn = item.rnMgtSn;
						$scope.address.bdMgtSn = item.bdMgtSn;
						$scope.address.curStep = 2;

					},

					// 완료버튼
					setAddress: function ($event) {

					    if ($("#addr_detail").val() == "") {
							alert("상세주소를 입력해주세요");
							$(".guide_comment2").show();
							$("#addr_detail").focus();
							return false;
						} else {
							$(".guide_comment2").hide();
						}

					    $scope.address.addr2 = $("#addr_detail").val();

						if (location.hostname.indexOf("compassionko.org") > -1)
							document.domain = "compassionko.org";
						else if (location.hostname.indexOf("compassionkr.com") > -1)
							document.domain = "compassionkr.com";
						else
							document.domain = "compassion.or.kr";

						//eval('opener.' + callback + '("' + $scope.address.zipCode + "','" + $scope.address.addr1 + "','" + $scope.address.addr2 + "')");
						//opener[callback]($scope.address.zipCode, $scope.address.addr1, $scope.address.addr2);
						//opener.callback(roadFullAddr, roadAddrPart1, addrDetail, roadAddrPart2, engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn);
						
						$.post("/common/popup/addressSession.ashx", {
							zipCode: $scope.address.zipcode, addrRoad: $scope.address.addrRoad, addrJibun: $scope.address.addrJibun, addr2: $scope.address.addr2
							, roadAddrPart1: $scope.address.roadAddrPart1
							, roadAddrPart2: $scope.address.roadAddrPart2
							, rnMgtSn: $scope.address.rnMgtSn
							, bdMgtSn: $scope.address.bdMgtSn
						}, function () {
							onComplete($scope.address.zipcode, $scope.address.addr1, $scope.address.addr2, $scope.address.addrJibun);
						});

						//fn($scope.address.zipcode, $scope.address.addr1, $scope.address.addr2);
						
					},

					// 더보기
					more: function ($event) {
						$event.preventDefault();
						$scope.address.params.currentPage++;
						$scope.address.search(false , {});
					}

				};


			});

		})();


	</script>

</head>
<body ng-app="cps" ng-cloak ng-controller="defaultCtrl">
	<form id="frm" name="form" runat="server">
		<input type="hidden" runat="server" name="callback" id="callback" />

		<!-- 일반팝업 width : 600 -->
	<div class="pop_type1 w600" style="height:675px">
		<div class="pop_title">
			<span>주소 찾기</span>
			<button type="button" class="pop_close" onclick="window.close();"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		<div class="pop_content address">
			
			<div class="search clear2">
				<span>
					<label for="addr" class="hidden">도로명 주소, 건물명 또는 지번입력</label>
					<input type="text" id="addr" class="input_type2 keyword" ng-enter="address.search(true,{})" style="width:495px;" placeholder="도로명 주소, 건물명 또는 지번입력" />
				</span>
				<a ng-click="address.search(true , {})" class="fr"><img src="/common/img/btn/search.png" alt="검색" /></a>
			</div>
			<div class="area1">
				<!--<p class="guide_comment2">검색어를 입력해주세요.</p>-->
				<p class="txt1">검색어 예 : 도로명(반포대로 58), 건물명(독립기념관), 지번(삼성동 25)</p>
			</div>

			<!-- 검색결과 -->
			<div ng-show="address.total != 0 && address.total != -1 && address.curStep == 1 ">
				<div class="area2">
					<p class="tit">도로명주소 검색 결과 <span class="fc_blue">{{address.total}}</span>건</p>

					<!-- 리스트 -->
					<div class="addr_list">
						<table>
							<caption>도로명주소 검색결과 리스트</caption>
							<colgroup>
								<col style="width:70%" />
								<col style="width:30%" />
							</colgroup>
							<thead>
								<tr>
									<th>도로명주소</th>
									<th>우편번호</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in address.list" ng-click="address.selectAddress(item)">
									<td class="result">
										<a href="#">
											<p class="addr1">{{item.road}}</p>
											<p class="addr2">{{item.jibun}}</p>
										</a>
									</td>
									<td>{{item.zipcode}}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--// 리스트 -->

					<!-- page navigation -->
					<div class="tac">
						<paging ng-if="total != 0" class="small" page="address.params.currentPage" page-size="address.params.countPerPage" total="address.total" show-prev-next="true" show-first-last="true" paging-action="address.search(false,{currentPage : page});"></paging>  
					</div>
					<!--// page navigation -->

				</div>
			</div>
			<!--// 검색결과 -->

			<!-- 검색결과 없음 -->
			<div ng-show="address.total == 0">
				<div class="area2">
					<p class="no_result">검색된 내용이 없습니다.</p>
				</div>
			</div>
			<!--// 검색결과 없음 -->

			<!-- 상세주소 입력 -->
			<div ng-show="address.curStep == 2">
				<div class="area2">
					<p class="tit">상세주소 입력</p>

					<div class="detail_wrap">
						<dl class="clear2">
							<dt>도로명주소</dt>
							<dd>{{address.addr1}}</dd>
						</dl>
						<dl class="clear2">
							<dt><label for="addr_detail">상세주소입력</label></dt>
							<dd>
								<input type="text" id="addr_detail" class="input_type2" style="width:300px;" placeholder="" />
								<p class="guide_comment2" style="display:none;">상세주소를 입력해주세요.</p>
							</dd>
						</dl>
					</div>
				</div>

				<div class="tac">
					<!--<a href="#" class="btn_type2 mr5">다시검색</a>-->
					<a  ng-click="address.setAddress($event)" class="btn_type1">주소입력</a>
				</div>
			</div>
			<!--// 상세주소 입력 -->
			
		</div>
	</div>
	<!--// popup -->





	</form>
</body>
</html>
