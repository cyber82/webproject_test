﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PopOrganization.aspx.cs" Inherits="popup_PopOrganization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="UTF-8">
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title>
    <link rel="stylesheet" type="text/css" media="screen">
    <link rel="stylesheet" href="/common/js/jquery/jquery-ui-1.10.4.min.css" type="text/css" media="screen" />
    <link href="/common/css/common.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="/common/css/layout.css" rel="stylesheet" type="text/css" media="screen,print">
    <link href="/common/css/screen.ui.css" rel="stylesheet" type="text/css" media="screen,print">
    <script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/assets/jquery/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/assets/jquery/wisekit/function.js" ></script>
    <script type="text/javascript" src="/common/js/jquery/placeholders.min.js"></script>
    <script type="text/javascript" src="/common/js/util/jquery.selectbox-0.2/jquery.selectbox-0.2.js"></script>
    <script type="text/javascript" src="/common/js/site/common.js"></script>

    <script type="text/javascript">
    	function setData(obj, id, name) {

        	if (location.hostname.indexOf("compassionko.org") > -1)
        		document.domain = "compassionko.org";
        	else if (location.hostname.indexOf("compassionkr.com") > -1)
        		document.domain = "compassionkr.com";
        	else
        		document.domain = "compassion.or.kr";

            opener.document.getElementById('hdOrganizationID').value = id;
            opener.document.getElementById('church_name').value = name;


            $('.church_list >li').css('background-color', '#ffffff');
    	    $(obj).closest('li').css('background-color', '#eeeeee');
            $('.church_list >li').css('border', '0');
            $(obj).closest('li').css('border', '1px solid #999999');
    	}

    	function setDirectInsert() {
    	    if (location.hostname.indexOf("compassionko.org") > -1)
    	        document.domain = "compassionko.org";
    	    else if (location.hostname.indexOf("compassionkr.com") > -1)
    	        document.domain = "compassionkr.com";
    	    else
    	        document.domain = "compassion.or.kr";

    	    opener.document.getElementById('church_name').disabled = false;
    	    opener.document.getElementById('church_name').focus();
    	    self.close();
    	}

        $(function () {
        	var sb = $("#ddlOrganizationType").attr("sb");
        	$("#sbSelector_" +sb).unbind("click")
        })

    </script>
</head>
<body>

   

    <!-- 일반팝업 width : 600 -->
    <div class="pop_type1 w600">
        <form id="form1" runat="server">
            <div class="pop_title">
                <span>교회 찾기</span>
                <button type="button" class="pop_close" onclick="javascript:self.close();"><span>
                    <img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
            </div>

            <div class="pop_content">

                <!-- 검색어 입력 -->
                <p class="fs15 mb10">찾으시는 교회/단체 명을 정확히 입력해 주세요.</p>
                <div class="clear2 mb20">
                    <span class="sel_type2 fl mr10" style="width: 120px">
                        <label for="ddlOrganizationType" class="hidden"></label>
                        <asp:DropDownList ID="ddlOrganizationType" runat="server" class="custom_sel" >
                            <asp:ListItem Value="교회" Selected="True">교회</asp:ListItem>
                            <asp:ListItem Value="기업">기업</asp:ListItem>
                            <asp:ListItem Value="단체">단체</asp:ListItem>
                        </asp:DropDownList>
                        <!--
                        <span class="sel_type2 fl mr10" style="width: 120px">
                            <label for="sel_1" class="hidden"></label>
                            <select class="custom_sel" name="" id="sel_1">
                                <option value="교회">교회</option>
                                <option value="교회">교회</option>
                                <option value="교회">교회</option>
                                <option value="교회">교회</option>
                            </select>
                            -->

                        </span>

                    <span class="fl">
                        <label for="txtSearch" class="hidden"></label>
                        <asp:TextBox ID="txtSearch" runat="server" class="input_type2" value="" placeholder="교회명" Style="width: 170px;" />
                        <asp:TextBox ID="txtSearch2" runat="server" class="input_type2" value="" placeholder="주소" Style="width: 170px;" />
                    </span>

                    <asp:Button ID="btnSearch" runat="server" class="btn_s_type2 fr" OnClick="btnSearch_Click" Text="검색" />
                    <span style="float:right; margin-right:70px; margin-top:5px;"><asp:CheckBox ID="chkReSearch" runat="server" /><label for="chkReSearch">결과 내 재검색</label></span>
                </div>
                <!--// 검색어 입력 -->

                <!-- 검색결과 -->
                <p class="fs15 mb10">해당 교회/단체를 선택해주세요.</p>

                <% if (ViewState["type"].ToString() == "Church") { %>
                <p class="fs15 mb10" style="display:inline-block;">직접입력을 원하시는 경우에는 <a href="javascript:void(0);" onclick="setDirectInsert();"><span style="text-decoration:underline;">이곳</span></a>를 클릭하세요.</p>
                <a class="btn_s_type1" style="float:right; margin-top:-10px;" href="javascript:self.close();">확인</a>
                <% } %>
                
			<div class="box_type4 search_church">
				<div class="field clear2">
					<span style="width:40%;">교회/단체 명</span>
					<span style="width:60%;">교회/단체 주소</span>
                    </div>
                    <style>
                        .church_list > li { display:flex; }
                    </style>
                    <ul class="church_list">
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <li>
                                    <span class="td-num id" style="width:40%;" onclick="setData(this, '<%#DataBinder.Eval(Container.DataItem, "SponsorID")%>','<%#DataBinder.Eval(Container.DataItem, "SponsorName")%>');">
                                        <a href="javascript:void(0);" onclick="setData(this, '<%#DataBinder.Eval(Container.DataItem, "SponsorID")%>','<%#DataBinder.Eval(Container.DataItem, "SponsorName")%>');">
                                            <%#DataBinder.Eval(Container.DataItem, "SponsorName")%>
                                        </a>
                                    </span>
                                    <span class="td-link name" style="width:60%;" onclick="setData(this, '<%#DataBinder.Eval(Container.DataItem, "SponsorID")%>','<%#DataBinder.Eval(Container.DataItem, "SponsorName")%>');">
                                        <a href="javascript:void(0);" onclick="setData(this, '<%#DataBinder.Eval(Container.DataItem, "SponsorID")%>','<%#DataBinder.Eval(Container.DataItem, "SponsorName")%>');">
                                            (<%#DataBinder.Eval(Container.DataItem, "ZipCode")%>) <%#DataBinder.Eval(Container.DataItem, "Address1")%> <%--<%#DataBinder.Eval(Container.DataItem, "Address2")%>--%>
                                        </a>
                                    </span>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>

                        <li class="no_content" runat="server" id="search_result" visible="false">
                            검색결과가 없습니다.
                        </li>
                        
                        
                    </ul>
                </div>
                <!--// 검색결과 -->

            </div>
    </div>
    <!--// popup -->

    <asp:HiddenField ID="hdOrganizationID" runat="server" />
    <asp:HiddenField ID="txtOrganizationName" runat="server" />
    </form>
</body>
</html>
