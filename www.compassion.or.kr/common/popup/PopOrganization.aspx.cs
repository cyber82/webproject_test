﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;

public partial class popup_PopOrganization : FrontBasePage
{
    WWWService.Service _wwwService = new WWWService.Service();

    public static DataSet m_ds = new DataSet();


    public override bool IgnoreProtocol
    {
        get
        {
            return true;
        }
    }

    protected override void OnBeforePostBack()
    {

        hdOrganizationID.Value = Request.QueryString["hdOrganizationID"].ToString();
        txtOrganizationName.Value = Request.QueryString["txtOrganizationName"].ToString();



        if (!IsPostBack)
        {
            ViewState["type"] = Convert.ToString(Request.Params["Type"]);

            if (Convert.ToString(Request.Params["Type"]) == "Church")
            {
                ddlOrganizationType.SelectedValue = "교회";
                ddlOrganizationType.Enabled = true;

                Form.DefaultFocus = this.txtSearch.ClientID; //포커스

                ClientScriptManager cs2 = Page.ClientScript;
                txtSearch.Attributes.Add("onkeydown", "if(event.keyCode == 13){"
                    + cs2.GetPostBackEventReference(btnSearch, "") + "};"); //EnterKey
            }


            if (!string.IsNullOrEmpty(Convert.ToString(Request.Params["SearchTxt"])))
            {
                txtSearch.Text = Convert.ToString(Request.Params["SearchTxt"]);

                BindList();
            }
        }
    }

    private void BindList()
    {
        DataSet ds = new DataSet();
        try
        {
            string sConID = string.Empty;
            string sOrganizationName = string.Empty;
            string sAddr = string.Empty;

            sOrganizationName = txtSearch.Text.Trim();
            if (sOrganizationName.IndexOf(" 교회") == -1)
            {
                sOrganizationName = sOrganizationName.Replace("교회", " 교회");
            }
            sAddr = txtSearch2.Text.Trim();

            ds = _wwwService.listOrganizationInformation(sConID, sOrganizationName, ddlOrganizationType.Text, sAddr);
            m_ds = ds.Copy();

            if (ds.Tables[0].Rows.Count == 0)
            {
                search_result.Visible = true;
            }
            else
            {
                search_result.Visible = false;
            }

            Repeater1.DataSource = ds;
            Repeater1.DataBind();

        }
        catch (Exception)
        {
            base.AlertWithJavascript("데이터를 가져오던 중 에러가 발생했습니다. 관리자에게 문의하세요.");
            return;
        }
    }

    private void ReBindList()
    {
        //결과 내 재검색

        string sOrganizationName = txtSearch.Text.Trim();
        if (sOrganizationName.IndexOf(" 교회") == -1)
        {
            sOrganizationName = sOrganizationName.Replace("교회", " 교회");
        }

        string sAddr = txtSearch2.Text.Trim();

        string filterExp = "1 = 1";
        if (sOrganizationName != "")
        {
            filterExp += "and (SponsorName like '%" + sOrganizationName + "%' ";
            filterExp += "or SponsorName like '%" + sOrganizationName.Replace(" ", "") + "%') ";
        }
        if (sAddr != "")
        {
            filterExp += "and (Address1 like '%" + sAddr + "%' ";
            filterExp += "or Address2 like '%" + sAddr + "%') ";
        }

        DataRow[] drs = m_ds.Tables[0].Select(filterExp);

        DataSet ds = new DataSet();
        ds = m_ds.Copy();
        ds.Tables[0].Rows.Clear();

        foreach (DataRow dr in drs)
        {
            DataRow newDr = ds.Tables[0].NewRow();
            newDr["SponsorID"] = dr["SponsorID"].ToString();
            newDr["SponsorName"] = dr["SponsorName"].ToString();
            newDr["ZipCode"] = dr["ZipCode"].ToString();
            newDr["Address1"] = dr["Address1"].ToString();
            newDr["Address2"] = dr["Address2"].ToString();
            ds.Tables[0].Rows.Add(newDr);
        }

        if (ds.Tables[0].Rows.Count == 0)
        {
            search_result.Visible = true;
        }
        else
        {
            search_result.Visible = false;
        }

        Repeater1.DataSource = ds;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (chkReSearch.Checked)
        {
            ReBindList();
        }
        else
        {
            BindList();
        }

    }
}