﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="address.aspx.cs" Inherits="_common_popup_address" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<script type="text/javascript" src="/assets/jquery/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">
    $(function () {
        init();
    });

    function init(){
	    var url = location.href;
	    
	    var inputYn = $("#inputYn").val();
	    if (inputYn != "Y") {
	        
	        $("#returnUrl").val(url);
	        $("#frm").attr("action", "http://www.juso.go.kr/addrlink/addrLinkUrl.do");
	        $("#frm").submit();
	    } else {
	        var roadFullAddr = $("#roadFullAddr").val();
	        var roadAddrPart1 = $("#roadAddrPart1").val();
	        var addrDetail = $("#addrDetail").val();
	        var roadAddrPart2 = $("#roadAddrPart2").val();
	        var engAddr = $("#engAddr").val();
	        var jibunAddr = $("#jibunAddr").val();
	        var zipNo = $("#zipNo").val();
	        var admCd = $("#admCd").val();
	        var rnMgtSn = $("#rnMgtSn").val();
	        var bdMgtSn = $("#bdMgtSn").val();
	        var callback = $("#callback").val();

	        var addr1 = roadAddrPart1 + "(" + jibunAddr + ")";
	        var addr2 = addrDetail + " " + roadAddrPart2;
			
	        if (location.hostname.indexOf("compassionko.org") > -1)
	        	document.domain = "compassionko.org";
	        else if (location.hostname.indexOf("compassionkr.com") > -1)
	        	document.domain = "compassionkr.com";
	        else
	        	document.domain = "compassion.or.kr";

	        opener[callback](zipNo , addr1 , addr2 , roadFullAddr, roadAddrPart1, addrDetail, roadAddrPart2, engAddr, jibunAddr, admCd, rnMgtSn, bdMgtSn);
	        //opener.callback(roadFullAddr, roadAddrPart1, addrDetail, roadAddrPart2, engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn);
		    window.close();
	    }
    }
    </script>
</head>
<body>
    <form id="frm" name="form" runat="server">
    <div>
		
		<input type="hidden" id="returnUrl" name="returnUrl" value=""/>

        <input type="hidden" runat="server" name="roadFullAddr" id ="roadFullAddr"/>
		<input type="hidden" runat="server" name="roadAddrPart1" id ="roadAddrPart1"/>
		<input type="hidden" runat="server" name="addrDetail" id ="addrDetail"/>
		<input type="hidden" runat="server" name="roadAddrPart2" id ="roadAddrPart2"/>
		<input type="hidden" runat="server" name="engAddr" id ="engAddr"/>
		<input type="hidden" runat="server" name="jibunAddr" id ="jibunAddr"/>
		<input type="hidden" runat="server" name="zipNo" id ="zipNo"/>
		<input type="hidden" runat="server" name="admCd" id ="admCd"/>
		<input type="hidden" runat="server" name="rnMgtSn" id ="rnMgtSn"/>
		<input type="hidden" runat="server" name="bdMgtSn" id ="bdMgtSn"/>
		<input type="hidden" runat="server" name="inputYn" id ="inputYn"/>

		<input type="hidden" runat="server" id="confmKey" name="confmKey" value=""/>
        <input type="hidden" runat="server" name="callback" id="callback" />
		<input type="hidden" runat="server" id="store_session" name="store_session" value="Y"/>	<!-- address 에 저장하기 위한 session 생성여부 -->
    </div>
    </form>
</body>
</html>
