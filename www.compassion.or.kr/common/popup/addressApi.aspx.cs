﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class _common_popup_addressApi :FrontBasePage
{


	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}

	protected override void OnBeforePostBack() {

		this.ViewState["confirmKey"] = ConfigurationManager.AppSettings["addressApiKey"];
		callback.Value = Request["callback"].EmptyIfNull();

	}
}