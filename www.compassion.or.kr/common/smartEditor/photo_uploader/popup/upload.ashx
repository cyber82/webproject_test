﻿<%@ WebHandler Language="C#" Class="smartEditor_photo_uploader_popup_upload" %>

using System;
using System.Web;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using drawing = System.Drawing;
using NLog;
//using Ucloudbiz.Storage;
using System.Configuration;
using System.Drawing;
using System.Net;


public class smartEditor_photo_uploader_popup_upload : IHttpHandler {

	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();
	public void ProcessRequest(HttpContext context) {

		string fileName = context.Request.Headers["file-name"];
		fileName = HttpUtility.UrlDecode(fileName);
		string path = context.Request.Headers["file-path"];
		string temp_file;

		var stream = context.Request.InputStream;

		if (stream.Length > 1024 * Convert.ToInt32(ConfigurationManager.AppSettings["limit_editor_image_size"])) {
			context.Response.Write("NOTSIZE_" + fileName);
			return;
		}
		
		using (Bitmap bitmap = new Bitmap(stream)) {

			temp_file = context.Server.MapPath(ConfigurationManager.AppSettings["temp"]) + fileName;
			bitmap.Save(temp_file);

		}

		var uploadResult = new Uploader().Upload(context, true, path, fileName , temp_file , "");

		//string result = string.Format("&bNewLine=true&sFileName={0}&sFileURL={1}", fileName, ConfigurationManager.AppSettings["domain_image"] + path + uploadResult.name);
		string result = string.Format("&bNewLine=true&sFileName={0}&sFileURL={1}", fileName, ConfigurationManager.AppSettings["domain_image"] + uploadResult.name);
		context.Response.Write(result);
		
    }

	
	public bool IsReusable {
		get {
			return false;
		}
	}

	
}