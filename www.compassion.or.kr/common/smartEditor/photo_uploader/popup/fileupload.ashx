﻿<%@ WebHandler Language="C#" Class="smartEditor_photo_uploader_popup_upload" %>

using System;
using System.Web;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using drawing = System.Drawing;
using NLog;
//using Ucloudbiz.Storage;
using System.Configuration;
using System.Drawing;
using System.Net;


public class smartEditor_photo_uploader_popup_upload : IHttpHandler {

	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();
	public void ProcessRequest(HttpContext context) {
		
		string url = context.Request["callback"]+"?callback_func="+context.Request["callback_func"]+"";
		
		
		HttpPostedFile file = context.Request.Files[context.Request.Files.AllKeys[0]];
		var filename = file.FileName;
		filename = file.FileName.Split('\\')[file.FileName.Split('\\').Length - 1];

		if (file.ContentLength > 1024 * 500) {
			url += "&sFileURL=NOTSIZE&sFileName=" + filename;
			context.Response.Redirect(url);
			return;
		}
		
		string temp_file = context.Server.MapPath(ConfigurationManager.AppSettings["temp"]) + filename;
		file.SaveAs(temp_file);


		string path = context.Request["path"];

		var uploadResult = new Uploader().Upload(context, true, path, filename, temp_file , null);
		
		url += "&bNewLine=true";
        url += "&sFileName=" + uploadResult.name;
        url += "&sFileURL=" + ConfigurationManager.AppSettings["domain_image"] + uploadResult.name;

		context.Response.Redirect(url);

	}

	
	public bool IsReusable {
		get {
			return false;
		}
	}

	
}