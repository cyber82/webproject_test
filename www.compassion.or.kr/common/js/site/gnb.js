﻿var m1, m2, m3, m4, m5, m6;	// 후원,참여,공감,소개,애드보킷

$(function () {

    var getMenu = function (key) {

        return $.grep(_menu.sub, function (r) {
            return r.key == key;
        })[0];
    }

    m1 = getMenu("sponsor");			// 후원
    m2 = getMenu("participation");		// 참여
    m3 = getMenu("sympathy");			// 공감
    m4 = getMenu("about-us");			// 소개
    m5 = getMenu("activity");			// 애드보킷
    m6 = getMenu("my");					// 마이컴패션

    gnb.setCurrent();

    gnb.init();

    footer.init();

    totalMenu.init();

});

var footer = {

    list: null,

    init: function () {
        this.list = [m1, m2, m3, m4, m5];

        var container = $("ul.ft_map");
        $.each(this.list, function (i) {

            var d2 = $("<li/>");
            container.append(d2);
            d2.append('<p class="fmap_tit">' + this.name + '</p>');

            var d3 = $('<ul class="fmap_lst"></ul>');
            d2.append(d3);
            $.each(this.sub, function () {

                d3.append($('<li><a href="' + this.url + '">' + this.name + '</a></li>'));

            });

        });

    }

}

var totalMenu = {
    list: null,
    isShow: false,
    onHeader: false,
    clicked: false,// 클릭시에는 scroll이벤트 비활성화
    bottomScroll: 0,
    init: function () {

        this.list = [m1, m2, m3, m4, m5, m6];

        $.each(this.list, function (i) {
            var container = $("div.menuList ul.m" + (i + 1));
            totalMenu.bindEvent(this, container, 2);

        });

        // scroll to
        $(".gnb_totalmenu .menuNav li").each(function (i) {
            $(this).click(function () {
                totalMenu.clicked = true;

                $(".gnb_totalmenu .menuAllBtn").removeClass("on");
                $($(".gnb_totalmenu .menuAllBtn")[i]).addClass("on");
                var target = $($(".gnb_totalmenu .menuList ul.Ad1")[i]);

                //console.log(target.offset().top, (target.offset().top - $(".gnb_totalmenu").offset().top));
                $(".gnb_totalmenu .menuList").animate({ "scrollTop": (target.offset().top - $(".gnb_totalmenu").offset().top + $(".gnb_totalmenu .menuList").scrollTop() - 10) + "px" }, 500, function () {
                    totalMenu.clicked = false;
                });

                return false;
            })
        })

        // 스크롤시 메뉴 활성화
        $(".gnb_totalmenu .menuList").scroll(function () {
            if (!totalMenu.clicked) {
                // 마지막 메뉴는 스크롤이 바닥에 닿았을 때 활성화
                if (totalMenu.isBottom($(this).find(".wrap").offset().top)) {
                    var last = $(".gnb_totalmenu .menuAllBtn").length - 1;
                    //console.log(last);
                    $(".gnb_totalmenu .menuAllBtn").removeClass("on");
                    $($(".gnb_totalmenu .menuAllBtn")[last]).addClass("on");
                } else {
                    $(".gnb_totalmenu .menuList .Ad1").each(function () {
                        var top = $(this).offset().top - $(".gnb_totalmenu .menuList").offset().top;
                        //console.log(top);
                        // header_top안보이는 경우
                        //if ($(".header_bottom").css("position") == "fixed") top = top - 65;

                        if (top > 15 && top < 45) {
                            var index = $(".gnb_totalmenu .menuList .Ad1").index(this);
                            $(".gnb_totalmenu .menuAllBtn").removeClass("on");
                            $($(".gnb_totalmenu .menuAllBtn")[index]).addClass("on");
                        }
                    });
                }

            }
        });

        // 해더영역 밖에서 스크롤 하면 서브레이어 감춤
        $(".header_bottom").mouseenter(function () {
            totalMenu.onHeader = true;
        }).mouseleave(function () {
            totalMenu.onHeader = false;
        });
    },

    isBottom: function (top) {
        top = top - $(".gnb_totalmenu .menuList").offset().top;
        return totalMenu.bottomScroll + top < 20;
    },

    bindEvent: function (obj, container, depth) {

        var root = $('<ul class="Ad' + depth + '"/>');
        container.append(root);

        if (obj.selected && obj.sub && obj.sub.length) {
            root.addClass("gnb_selected");
        }

        $.each(obj.sub, function (i) {
            var item = $('<li><a href="' + this.url + '" class="tit' + depth + ' a' + depth + '">' + ((depth == 4) ? "- " : "") + this.name + '</a></li>');

            if (depth == 2 && i > 0 && i % 4 == 0) {

                root = $('<ul class="Ad' + depth + '"/>');
                container.append(root);
            }

            root.append(item);

            if (this.selected) {
                item.find("a").addClass("gnb_selected");
            }

            if (this.sub && this.sub.length) {
                totalMenu.bindEvent(this, item, depth + 1);
            }
        })

    },


    toggle: function () {

        totalMenu.isShow = $("#mask").length > 0;
        if (totalMenu.isShow) {
            totalMenu.hide();
            $(".gnb_totalmenu .menuAllBtn").removeClass("on");
            $($(".gnb_totalmenu .menuAllBtn")[0]).addClass("on");
        } else {
            totalMenu.show();
        }
    },

    show: function () {

        $(".gnbsub_wrap").trigger("mouseleave");

        modalShow($(".gnb_totalmenu"), function () {
            setTimeout(function () {
                var menuHeight = $(".gnb_totalmenu .menuList").height();
                var scrollHeight = $(".gnb_totalmenu .menuList .wrap").height();
                var top = $(".gnb_totalmenu").position().top;
                totalMenu.bottomScroll = scrollHeight - menuHeight - top;
            }, 100);
        }, true, false, function () {
            $(".gnb_wrap .menu").removeClass("on")
        });

    },

    hide: function () {

        closeModal(function () {
            $("body").css({ "overflow": "auto" })
        });
    }

}

var gnb = {

    enable: true,

    timeout: null,

    list: null,

    header_bottom: null,

    getHost: function () {
        var host = '';
        var loc = String(location.href);
        if (loc.indexOf('local') != -1) host = 'www.local.compassionkr.com';
        else if (loc.indexOf('compassion.or.kr') != -1) host = 'www.compassion.or.kr';
        else if (loc.indexOf('compassionkr.com') != -1) host = 'www.compassionkr.com';
        else if (loc.indexOf('compassionko.org') != -1) host = 'www.compassionkr.com';
        else if (loc.indexOf('localhost') != -1) host = 'localhost:53720';

        return host;
    },

    getUrl: function (host, url) {
        var retUrl = '';
        if (url.indexOf('local') != -1) retUrl = url.replace('www.local.compassionkr.com', host);
        else if (url.indexOf('www.compassion.or.kr') != -1) retUrl = url.replace('www.compassion.or.kr', host);
        else if (url.indexOf('www.compassionkr.com') != -1) retUrl = url.replace('www.compassionkr.com', host);
        else if (url.indexOf('www.compassionko.org') != -1) retUrl = url.replace('www.compassionko.org', host);
        else if (url.indexOf('localhost:53720') != -1) retUrl = url.replace('localhost:53720', host);

        return retUrl;
    },

    bindEvent: function (obj, container, depth) {

        $(window).scroll(function () {

            // 서브해더에서 스크롤시 메뉴닫기
            //console.log(totalMenu.onHeader);
            if (!totalMenu.onHeader) {
                $("a.gnb_top").removeClass("on");
                $(".gnbsub_wrap").hide();
                $("div.gnb").hide();
                //gnb.setDefault();
            }

            if (!gnb.enable) return;

            var top = $(window).scrollTop();
            if (top >= 35) {

                if (gnb.header_bottom != "fixed") {
                    gnb.header_bottom = "fixed";
                    $(".header_bottom").css({ "position": "fixed", "top": "0px" });
                    $(".gnbsub_wrap , .gnb_totalmenu").css({ "top": "95px" })
                }
            } else {
                if (gnb.header_bottom != "relative") {
                    gnb.header_bottom = "relative";
                    $(".header_bottom").css({ "position": "relative" });
                    $(".gnbsub_wrap , .gnb_totalmenu").css({ "top": "130px" })
                }
            }

        })

        var root = $('<ul class="d' + depth + '"/>');
        container.append(root);

        if (obj.selected && obj.sub) {
            root.addClass("gnb_selected");
        }

        var host = gnb.getHost();

        $.each(obj.sub, function () {
            var url = gnb.getUrl(host, this.url);

            //var item = $('<li><a name="asdfasfd" href="' + this.url + '" class="a'+depth+ (this.sub && this.sub.length ? "" : " none") + '" >' + this.name + '</a></li>');
            var item = $('<li><a name="asdfasfd" href="' + url + '" class="a' + depth + (this.sub && this.sub.length ? "" : " none") + '" >' + this.name + '</a></li>');
            root.append(item);

            if (this.selected) {
                item.find("a").addClass("gnb_selected");
            }

            item.mouseenter(function () {
                $("a.a" + (depth)).removeClass("on");
                $(this).find("a.a" + (depth)).addClass("on")
                clearTimeout(gnb.timeout);

                var target = $(this).find("ul.d" + (depth + 1));
                if (target.length > 0)
                    target.show();
                else {
                    gnb.timeout = setTimeout((function () {
                        root.find("ul").hide();
                    }), 200);
                }

            }).mouseleave(function () {
                clearTimeout(gnb.timeout);
                $(this).find("a").removeClass("on")
                root.find("ul").hide();
                /*
				gnb.timeout = setTimeout((function () {
					root.find("ul").hide();
				}), 200);
				*/
            })

            if (this.sub && this.sub.length) {
                gnb.bindEvent(this, item, depth + 1);
            }
        })

    },

    init: function () {


        // 현재 메뉴를 포함하여 상위뎁스까지 selected 표시를 한다.
        this.list = [m1, m2, m3, m4, m5];

        $.each(this.list, function (i) {

            $("a.gnb_top.m" + (i + 1)).attr("href", this.url);
            var container = $("div.gnb.m" + (i + 1));
            gnb.bindEvent(this, container, 2);
        });

        // 전체메뉴의 타이틀 링크
        $.each([m1, m2, m3, m4, m5, m6], function (i) {
            $(".menuList .m" + (i + 1) + " .tit a").attr("href", this.url);
        });

        var host = gnb.getHost();
        $.each($('.area.banner a'), function (i) {
            var url = gnb.getUrl(host, $(this).attr('href'));
            $(this).attr('href', url);
        });

        gnb.setDefault();

        $("a.gnb_top").each(function (i) {
            $(this).mouseenter(function () {

                if (!gnb.enable) return;
                if ($("#mask").length > 0) return;

                $("a.gnb_top").removeClass("on");
                $(this).addClass("on");
                clearTimeout(gnb.timeout);
                $("div.gnb").hide();
                $("div.gnb.m" + (i + 1)).show();
                $("div.gnb.m" + (i + 1) + " ul.d3").hide();
                $("div.gnb.m" + (i + 1) + " ul.d4").hide();

                if ($(this).hasClass("gnb_selected")) {
                    $(".gnb_selected").show();
                }

                $(".gnbsub_wrap").show();

                // 배너이미지 노출
                var id = $(this).attr("data-id");
                $(".gnbsub_wrap .banner").hide();
                $(".gnbsub_wrap .banner[data-id='" + id + "']").show();

            }).mouseleave(function () {

                /*
				clearTimeout(gnb.timeout);
				gnb.timeout = setTimeout((function () {
					$(".gnbsub_wrap").hide();
					$("div.gnb").hide();
				}), 500);
				*/
            })
        })

        $(".gnbsub_wrap , .header_bottom").mouseenter(function () {

            clearTimeout(gnb.timeout);

        }).mouseleave(function () {
            gnb.hide();
        })

        $(".gnb_wrap .menu").click(function () {
            totalMenu.toggle();
            if ($(this).hasClass("on")) {
                $(this).removeClass("on");
            } else {
                $(this).addClass("on");
            }
            return false;
        })

        //totalMenu.toggle();

    },

    setDefault: function () {
        $.each(gnb.list, function (i) {
            if (this.selected) {
                $("a.gnb_top.m" + (i + 1)).addClass("on").addClass("gnb_selected");
            }
        });

        var targets = $(".gnbsub_wrap .gnb_selected");
        targets.addClass("on");
        targets.show();
    },

    setCurrent: function () {

        if ($("#sitemap_resourcekey").length < 1) return;

        var cur_key = $("#sitemap_resourcekey").val();
        //console.log(cur_key);
        var setSelected = function (obj) {

            $.each(cur_key.split('|'), function () {
                if (this.startsWith(obj.key)) {
                    //console.log(obj.key+'', this);
                    obj.selected = true;
                }

            })

            if (obj.sub) {

                $.each(obj.sub, function () {
                    setSelected(this);

                });
            }

        }

        $.each(_menu.sub, function () {
            setSelected(this);
        });
    },


    hide: function () {
        clearTimeout(gnb.timeout);
        gnb.timeout = setTimeout((function () {
            $("a.gnb_top").removeClass("on");
            $(".gnbsub_wrap").hide();
            $("div.gnb").hide();
            gnb.setDefault();


        }), 200);
    }

}
