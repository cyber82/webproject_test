﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class common_js_site_menu : FrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}


	protected override void OnBeforePostBack(){

		Response.ContentType = "text/javascript";
		Response.Clear();
		var root = SiteMap.Providers["web"].RootNode;

		data.Text = root.ToMenu().ToJson();
	}
	
	
}