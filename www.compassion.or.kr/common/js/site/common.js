﻿$.ajaxSetup({ cache: false });

var app = null;		// 
var kakaoKey = "dd71aedea41f82616eca293786f838f5";
var facebookKey = "330228967342270";
var debugging = true;
if (!debugging || typeof console == "undefined" || typeof console.log == "undefined") var console = { log: function () { } };

$(function () {

	$.each($("button"), function () {
		if (!$(this).attr("type")) {
			$(this).attr("type", "button");
		}
	})

	// gnb 마우스오버
	$(".depth1").mouseenter(function () {
		//$(".depth2").animate({ "top": "130px" });
	});

    // 마우스오버 공통
	$(".img_over").mouseenter(function () {
	    var img_num = $(this).find("img");
	    img_num.attr("src", img_num.attr("src").replace(".png", "_on.png"))
	}).mouseleave(function () {
	    var img_num = $(this).find("img");
	    img_num.attr("src", img_num.attr("src").replace("_on.png", ".png"))
	});


	$(".custom_sel").selectbox("detach");
	$(".custom_sel").selectbox();

	common.bindSNSAction();

	setNumberOnly();

	setCurrenyFormat();


	// ie에서 포커스된 인풋이 해더에 가림
	$("input").bind("focus", function () {
		var scrollTop = $(window).scrollTop(),
		elementOffset = $(this).offset().top,
		distance = (elementOffset - scrollTop);
		if (distance < 95) {
			$("body,html").animate({
				scrollTop: $(this).offset().top - 100
			}, 600);
		}
	});

})

var common = {

	isLogin : function () {
		return $("#is_login").val() == "Y";	// top.master 
	},

	getUserId: function () {
		return $("#_userid").val();
	},

	checkLogin: function () {
		if (!common.isLogin()) {
			if (confirm("로그인이 필요합니다. \n로그인 페이지로 이동하시겠습니까?")) {
				location.href = "/login";
			}
			return false;
		}

		return true;
	},

	bindSNSAction: function () {
		
		var timeout = null;

		$(".sns_ani").unbind("mouseenter").unbind("mouseleave");

		$.each($(".sns_ani"), function () {

			if ($(this).hasClass("down")) {

				$(".sns_ani").mouseenter(function () {
					if (timeout != null)
						clearTimeout(timeout);
					$(this).find(".common_sns_group").stop().animate({ "height": "185px", "opacity": "1" }, 300);
				}).mouseleave(function () {
					if (timeout != null)
						clearTimeout(timeout);
					timeout = setTimeout(function () {
						$(".common_sns_group").stop().animate({ "height": "0", "opacity": "0" }, 300);
					}, 100);

				});
			} else {
				$(this).mouseenter(function () {
					if (timeout != null)
						clearTimeout(timeout);
					$(this).find(".common_sns_group").stop().animate({ "width": "185px", "opacity": "1" }, 400);
					$(this).parent().find(".day").animate({ "opacity": "0" }, 400);
				}).mouseleave(function () {
					if (timeout != null)
						clearTimeout(timeout);
					timeout = setTimeout(function () {
						$(".common_sns_group").stop().animate({ "width": "0px", "opacity": "0" }, 400);
						$(".snsWrap .day").animate({ "opacity": "1" }, 400);
					}, 100);

				});
			}
		})

		
		

		// widget 이 이미 생성되었으면 이벤트 새로 적용
		try{
			if ($("body").is(":data('ui-sns')")) {
				var sns = $("body").data("ui-sns");
				sns.reload();

			} else {
				
				$("body").sns({
					kakaoKey: kakaoKey,
					facebookKey: facebookKey , 
					onSuccess: function (provider, response) {
						if (provider == "ks" || provider == "fb") {
							alert("공유되었습니다.");
						}
						//	console.log("success", provider, response);
					}
				});
			}
		}catch(e){}
	}

}

var goBack = function () {
	history.back();
}

var goList = function () {
	location.href = $("#btnList").attr("href");
}

var setDatePicker = function (obj) {

	obj.datepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: '',
		numberOfMonths: 1,
		monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		showTime: false,
		showHour: false,
		showMinute: false,
		closeText: '닫기',
		currentText: '오늘',

		buttonImageOnly: false,
		changeYear: true,
		onSelect: function (text, e) {
			$(this).datepicker("hide");
			
		}
	});

}

var setPlaceholder = function (obj, str) {

	obj.focus(function () {
		if ($(this).val() == str) {
			$(this).val("")
		}
	}).focusout(function () {
		if ($(this).val() == "") {
			$(this).val(str)
		}
	})

};

var setNumberOnly = function () {

	$(".number_only").unbind("keydown");
	$(".number_only").keydown(function (e) {

		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
			// Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
			// Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
			// Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}

		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

};

var setCurrenyFormat = function () {
	$(".use_digit").currencyFormat();
};

var scrollTo = function (obj, offset, retryWhenFindObj, cb, count) {

	if (retryWhenFindObj && obj.length < 1) {

		if (!count) count = 0;
		count++;
		if (count > 3) return;

		setTimeout(function () {

			scrollTo(obj, offset, retryWhenFindObj, cb, count);
		}, 300)
		return;
	}

	offset = offset || 0;
	if (obj && obj.offset) {
		var top = obj.offset().top - $(".header_bottom").height() - offset;
		//$("html,body").scrollTop(top);
		$("html,body").animate({ "scrollTop": top }, 300, function () {
			if (cb)
				cb();
		})

		//console.log(top);
	}
};

var loading = {
	show: function (msg) {

		$(window).resize(function () {
			loading._resize();
		});


		$("#loading_bg").css({ opacity: 0, display: "block" });
		$("#loading_bg").animate({
			opacity: 0.6
		}, 'fast', function () {
			$("body").css({ overflow: "hidden" })

			var container = $("#loading_container");

			if (msg)
				$("#loading_container .msg").html(msg);
			else {
				$("#loading_container .msg").html("로딩 중입니다. 잠시만 기다려주세요.");	// 임시 , 이미지로 처리예정
			}
			container.show();

		})


	},

	hide: function () {

		$(window).unbind("resize", loading._resize);

		$("#loading_bg").animate({
			opacity: 0.0
		}, 'fast', function () {
			$("#loading_bg").css({ opacity: 0, display: "none" });

			$("body").css({ overflow: "auto" })

			var container = $("#loading_container");
			container.hide();

			$("#loading_container .msg").html("");
		});


	},

	_resize: function () {
		var windowHeight = window.innerHeight ? window.innerHeight : $(window).height();
		$('#loading_bg').css({ 'width': $(window).width(), 'height': windowHeight });

		// hegiht delay 현상이 있어서 1초뒤 다시 계산
		$("#loading_container").css({ top: "40%", height: windowHeight });
		setTimeout(function () {
			$("#loading_container").css({ top: "40%", height: windowHeight });
		}, 1000)
	}
};


// 한글 x 
$.fn.setHangulBan = function () {
	$(this).keyup(function (e) {
		$(this).val($(this).val().replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, ''));
	});
}

// 숫자 x 
$.fn.setNumberBan = function () {
	$(this).keydown(function (e) {
		//alert(e.keyCode)
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
			// Allow: Ctrl+A
			(e.keyCode == 65 && e.ctrlKey === true) ||
			// Allow: Ctrl+C
			(e.keyCode == 67 && e.ctrlKey === true) ||
			// Allow: Ctrl+X
			(e.keyCode == 88 && e.ctrlKey === true) ||
			// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}

		if ((47 < e.keyCode && e.keyCode < 58) || (95 < e.keyCode && e.keyCode < 106)) {
			e.preventDefault();
		}
	}).keyup(function () {
		$(this).val($(this).val().replace(/[^ㄱ-ㅎ가-힣a-zA-Z]/gi, ''));
	})
}





