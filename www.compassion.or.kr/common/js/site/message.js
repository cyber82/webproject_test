﻿
var msg_en = {
	product_search_empty_search_word: "Enter the product name",
	product_search_empty_search_msg: "type keyword",
	fc_yyyy_mm_dd: "invalid yyyy.MM.dd foramt",
	fc_yyyy_mm: "invalid yyyy.MM foramt",
	fc_yyyy: "invalid yyyy(yyyy) foramt",
	fc_mm: "invalid month(MM) foramt",
	fc_dd: "invalid day(dd) foramt",
	fc_numeric: "You can enter only numbers",
	fc_decimal: "You can enter only numbers or '.'",
	fc_alpha_numeric: "You can enter only numbers,english.",
	fc_alphabet: "You can enter only english.",
	fc_phone: "+-You can enter only numbers.",
	fc_email: "invalid email format.",
	fc_url: "invalid URL format.",
	fc_nodata: "Please enter the value.",
	fc_pwd_at_least_4digit: "최소 4자리 이상 입력해주세요",
	cm_submit: "Would you like to send? en",
	cm_exception: "Unexpected failure occurred. Please use again later.",
	cm_msg_auth_required: "It is a service that requires a login.",
	cm_msg_updated: "update completed",
	cm_empty_search_word: "  Enter the keyword",
	cm_require_login: "It is a service that requires a login.\nAre you sure you want to go to the login page?",
	

	qna_select_type: "please select type",
	qna_input_name: "please enter name",
	qna_input_email: "please enter email",
	qna_input_title: "please enter title",
	qna_input_content: "please enter content",

	community_input_title: "제목을 입력해 주세요.",
	community_input_type: "구분을 선택해 주세요.",
	community_input_content: "내용을 입력해 주세요.",
	community_input_image: "이미지를 선택해 주세요.",
	community_input_youtube: "유튜브 동영상을 선택해 주세요.",

	data_card_input_name: "찾고 있는 선수명을 입력하세요.",

	input_player_name: "please enter player name",
	login_required: "로그인을 해야 게임을 실행 할 수 있습니다.",
	login_required_write: "로그인을 해야 글을 쓰실 수 있습니다.",

	community_poll_vote: "투표해주세요",

	costomer_faq_input_keyword: "키워드를 입력해주세요.",

	input_comment: "댓글을 입력해주세요.",
	want_delete: "삭제하시겠습니까?",
	want_regist: "등록하시겠습니까?",
	want_update: "수정하시겠습니까?"


}

var msg_ko = {
	product_search_empty_search_word: "검색하실 제품명을 입력하세요",
	product_search_empty_search_msg: "검색하실 제품명을 입력하세요",
	fc_yyyy_mm_dd: "년(4자리).월(2자리).일(2자리) 포맷으로 입력해주세요",
	fc_yyyy_mm: "년(4자리).월(2자리) 포맷으로 입력해주세요",
	fc_yyyy: "년(4자리) 포맷으로 입력해주세요",
	fc_mm: "월(2자리) 포맷으로 입력해주세요",
	fc_dd: "일(2자리) 포맷으로 입력해주세요",
	cm_submit: "전송하시겠습니까? ",
	cm_exception: "예상치 못한 장애가 발생했습니다. 잠시후 다시 이용해주세요.",
	cm_msg_auth_required: "로그인이 필요한 서비스입니다.",
	cm_msg_updated: "수정되었습니다.",
	cm_empty_search_word: "  검색어를 입력하세요",
	cm_require_login: "로그인이 필요한 서비스입니다.\n로그인 페이지로 이동하시겠습니까?",
	fc_numeric: "숫자만 입력 가능합니다.",
	fc_decimal: "숫자와 '.'만 입력 가능합니다.",
	fc_alpha_numeric: "영문,숫자만 입력 가능합니다.",
	fc_alphabet: "영문만 입력 가능합니다.",
	fc_phone: "+-숫자만 입력 가능합니다.",
	fc_email: "올바른 이메일 형식이 아닙니다.",
	fc_url: "올바른 URL형식이 아닙니다.",
	fc_nodata: "값을 입력하세요",
	fc_pwd_at_least_4digit: "최소 4자리 이상 입력해주세요",
	qna_select_type: "구분 선택해 주세요.",
	qna_input_name: "이름을 입력하세요.",
	qna_input_email: "이메일주소를 입력하세요.",
	qna_input_title: "제목을 입력해 주세요.",
	qna_input_content: "내용을 입력해 주세요",

	community_input_title: "제목을 입력해 주세요.",
	community_input_type: "구분을 선택해 주세요.",
	community_input_content: "내용을 입력해 주세요.",
	community_input_image: "이미지를 선택해 주세요.",
	community_input_youtube: "유튜브 동영상을 선택해 주세요.",

	data_card_input_name : "찾고 있는 선수명을 입력하세요.",

	input_player_name: "선수명을 입력하세요.",
	login_required: "로그인을 해야 게임을 실행 할 수 있습니다.",
	login_required_write: "로그인을 해야 글을 쓰실 수 있습니다.",

	community_poll_vote: "투표해주세요",

	costomer_faq_input_keyword: "키워드를 입력해주세요.",
	
	input_comment: "댓글을 입력해주세요.",

	want_delete: "삭제하시겠습니까?",
	want_regist: "등록하시겠습니까?",
	want_update: "수정하시겠습니까?"
}

var msg = function (key) {
	if (location.pathname.indexOf("/en/") == 0) {
		return eval('msg_en.' + key);
	} else if (location.pathname.indexOf("/ko/") == 0) {
		return eval('msg_ko.' + key);
	} else {
		return eval('msg_ko.' + key);
	}
}
