﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="album.aspx.cs" Inherits="common_child_album"  %>

	<div class="pop_type1 w800" style="width:800px;height:800px">
		<script src="/assets/download/download.js"></script>
		<script type="text/javascript">
	
			var activateAlbum = function ($scope, childkey , modal) {
				
				$scope.modalAlbum = {
			
					images : $.parseJSON($("#album_data").val()) , 
					index: 0,
					modal : modal , 

					close : function($event){
						$event.preventDefault();
						$scope.modalAlbum.modal.hide();
 					} , 
					prev: function () {
						if ($scope.modalAlbum.index - 1 < 0)
							return;

						$scope.modalAlbum.index--;
					},

					next: function () {
						if ($scope.modalAlbum.index + 1 >= $scope.modalAlbum.images.length)
							return;

						$scope.modalAlbum.index++;
						
					},

					download: function ($event) {
						$event.preventDefault();

						var image = $scope.modalAlbum.images[$scope.modalAlbum.index].image;
						download(image, childkey + "_" + ($scope.modalAlbum.index+1) + ".jpg", "image/jpg");

					}
				};

				$scope.$apply();

				loading.hide();
			}

	
		</script>
		<input type="hidden" runat="server" id="album_data" />
		<div class="pop_title">
			<span>{{modalAlbum.images[modalAlbum.index].date}}</span>
			<button class="pop_close" ng-click="modalAlbum.close($event)"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
		</div>

		
		<div class="pop_content growth_pic">
			
			<!-- 사진영역 : 628 * 600 -->
			<!-- css에서 사진 자동 정렬 -->
			
			<div class="gp_wrap" style="background:url('{{item.image}}') no-repeat center;" ng-repeat="item in modalAlbum.images" ng-show="$index == modalAlbum.index"></div>
			
			<div class="gp_wrap" ng-show="modalAlbum.images.length == 0">데이타가 없습니다.</div>

			<div class="btn">
				<button class="btn_prev" type="button" ng-click="modalAlbum.prev()" ng-show="modalAlbum.index > 0"><span><img src="/common/img/btn/prev_7.png" alt="이전" /></span></button>
				<button class="btn_next" type="button" ng-click="modalAlbum.next()" ng-show="modalAlbum.index +1 < modalAlbum.images.length"><span><img src="/common/img/btn/next_7.png" alt="다음" /></span></button>
			</div>

			<div class="tac" ng-show="modalAlbum.images.length > 0"><button type="button" ng-click="modalAlbum.download($event)"><span class="btn_type1">다운로드</span></button></div>
			
		</div>
	</div>
