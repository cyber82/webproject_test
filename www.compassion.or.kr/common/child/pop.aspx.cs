﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using System.IO;
using CommonLib;
using TCPTModel;
using Newtonsoft.Json;
using TCPTModel.Response.Supporter;

public partial class common_child_pop : FrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}


	protected override void OnBeforePostBack() {
        if (Request["fn"].EmptyIfNull() == "hide") {
			btn_sns.Visible = btn_commitment.Visible = false;
			picWrap.Style["height"] = "350px";
		}

		this.ViewState["googleMapApiKey"] = ConfigurationManager.AppSettings["googleMapApiKey"];




        ViewState["weather_icon"] = "";
		ViewState["temperature"] = "";
		ViewState["weather"] = "";
		ViewState["time"] = "";
        ViewState["lat"] = "";
        ViewState["lng"] = "";
        var requests = Request.GetFriendlyUrlSegments();
		
		if(requests.Count < 3) {
			return;
		}
		var countryCode = requests[0];
		var childMasterId = requests[1];
		var childKey = requests[2];

        string strReq = "";

        for (int i = 0; i < requests.Count; i++)
        {
            strReq += i.ToString() + " : " + requests[i] + " ";
        }
//        AlertWithJavascript("TEST_003 : " + requests.Count.ToString(), "");

//        HttpCookie ckChildKey = new HttpCookie("Ensure_childKey");
//        ckChildKey.Value = childKey;
//        HttpContext.Current.Response.Cookies.Set(ckChildKey);

//        HttpCookie ckPic = new HttpCookie("Ensure_Pic");
////        ckChildKey.Value = picWrap.
//        HttpContext.Current.Response.Cookies.Set(ckPic);

        this.ViewState["childKey"] = childKey;
		this.ViewState["childMasterId"] = childMasterId;

		var path = Server.MapPath("/common/child/country-info/" + countryCode + ".html");

		using(StreamReader stream = File.OpenText(path)){
			content.Text = stream.ReadToEnd();
		}

		var actionResult = new CountryAction().GetInfoById(countryCode);
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message);
			return;
		}


        // 문희원 테스트용 추가
        //string test = string.Empty;

        //try
        //{
        //    var sess = new UserInfo();
        //    string sponsorid = sess.SponsorID;

        //    test += sponsorid + " " + childMasterId + " ";

        //    WWWTCPTService.Service svc = new WWWTCPTService.Service();
        //    test += svc.Url + " ";
        //    DataSet ds = svc.GetCommitmentInfoForTCPT(sponsorid, childMasterId);

        //    if (ds == null || ds.Tables.Count == 0)
        //    {
        //        // 조회 오류
        //    }
        //    else
        //    {
        //        DataRow dr = ds.Tables[0].Rows[0];
        //        bool regSuccess = false;
        //        string result = string.Empty;
        //        string userid = "";
        //        string userName = "";
        //        string GlobalSponsorID = string.Empty;
        //        string ChildGlobalID = dr["ChildGlobalID"].ToString();
        //        string CommitmentID = dr["CommitmentID"].ToString();
        //        string holdID = dr["HoldID"].ToString();
        //        string holdUID = dr["HoldUID"].ToString();
        //        string holdType = dr["HoldType"].ToString();
        //        string conid = "54-" + dr["ConID"].ToString();
        //        string firstName = dr["FirstName"].ToString();
        //        string lastName = dr["LastName"].ToString();
        //        string genderCode = dr["GenderCode"].ToString();
        //        if (dr["SponsorGlobalID"].ToString() != "")
        //        {
        //            GlobalSponsorID = dr["SponsorGlobalID"].ToString();
        //            regSuccess = true;
        //        }

        //        if (genderCode == "M")
        //            genderCode = "Male";
        //        else if (genderCode == "F")
        //            genderCode = "Female";

        //        if (string.IsNullOrEmpty(GlobalSponsorID))
        //        {
        //            result = svc.RegisterSponsorProfile(conid, firstName, lastName, genderCode, userid);

        //            TCPTResponseMessage msg = JsonConvert.DeserializeObject<TCPTResponseMessage>(result);
        //            if (msg.IsSuccessStatusCode)
        //            {
        //                SupporterProfileResponse_Kit response = JsonConvert.DeserializeObject<SupporterProfileResponse_Kit>(msg.RequestMessage.ToString());
        //                GlobalSponsorID = response.SupporterProfileResponse[0].GlobalID;

        //                // 스폰서  Global id update
        //                svc.UpdateSponsorGlobalID(sponsorid, childMasterId);

        //                regSuccess = true;
        //            }
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //    ViewState["test"] = test + " " + ex.Message;
        //}





        var entity = (country)actionResult.data;

		ViewState["weather_icon"] = entity.c_weather_icon;
		ViewState["temperature"] = entity.c_temperature.HasValue ? Convert.ToInt32(entity.c_temperature.Value).ToString() : "";
		ViewState["weather"] = entity.c_weather.ToUpper();
		ViewState["lat"] = entity.c_lat.ToString();
		ViewState["lng"] = entity.c_lng.ToString();

		//Response.Write(entity.ToJson());
		if(entity.c_timeoffset.HasValue) {
			ViewState["time"] = DateTime.Now.AddSeconds(entity.c_timeoffset.Value).ToString("tt h:mm").Replace("오전" , "am").Replace("오후", "pm");
			
		}

	}
	
}
