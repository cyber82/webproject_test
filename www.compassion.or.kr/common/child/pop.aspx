﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pop.aspx.cs" Inherits="common_child_pop" %>

<div style="background: transparent;" class="fn_pop_container" id="childModalview">

    <script type="text/javascript">

        function initChildPop($http, $scope, modal, item) {

            $scope.modalChild = {
                instance: modal,
                item: item,

                hide: function ($event) {
                    $event.preventDefault();
                    $scope.modalChild.instance.hide();
                },



                goPay_old1: function ($event) {
                    $event.preventDefault();
                    
                    $http.get("/sponsor/pay-gateway.ashx?t=go-cdsp", { params: { childMasterId: $scope.modalChild.item.childmasterid } }).success(function (r) {
                       
                        if (r.success) {
//                            alert('1');
   
<%--                            r.message = "childmasterid : " + $scope.modalChild.item.childmasterid;
                            r.message += " childKey : " + '<%:this.ViewState["childKey"].ToString()%>';
                            r.message += " Pic : " + $scope.modalChild.item.pic;
                            r.message += " IsOrphan : " + $scope.modalChild.item.isorphan;
                            r.message += " IsHandicapped : " + $scope.modalChild.item.ishandicapped;
                            r.message += " WaitingDays : " + $scope.modalChild.item.waitingdays;

                            r.message += " Age : " + $scope.modalChild.item.age;
                            r.message += " BirthDate : " + $scope.modalChild.item.birthdate;
                            r.message += " CountryCode : " + $scope.modalChild.item.countrycode;
                            r.message += " Gender : " + $scope.modalChild.item.gender;
                            r.message += " HangulName : " + $scope.modalChild.item.hangulname;
                            r.message += " HangulPreferredName : " + $scope.modalChild.item.hangulpreferredname;
                            r.message += " FullName : " + $scope.modalChild.item.fullname;
                            r.message += " PreferredName : " + $scope.modalChild.item.preferredname;

                            alert("STEST_" + r.message);--%>

                            alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
                            location.href = r.data;
                        } else {
                            if (r.action == "nonpayment") {		// 미납금

                                if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
                                    location.href = "/my/sponsor/pay-delay/";
                                }

                            } else {
                                alert("TEST_" + r.message);
                            }
                        }

                    });
                },

                goPay: function ($event) {
                    $event.preventDefault();
                    //[이종진]로딩추가 
                    var zIndexBg = $("#loading_bg").css("z-index");
                    var zIndexCon = $("#loading_container").css("z-index");
                    $("#loading_bg").css("z-index", "1000000");
                    $("#loading_container").css("z-index", "1000001");
                    loading.show();

                    $http.get("/sponsor/pay-gateway.ashx?t=go-cdsp", { params: { 
                        childMasterId: $scope.modalChild.item.childmasterid, childKey: '<%:this.ViewState["childKey"].ToString()%>', Pic: $scope.modalChild.item.pic
                        , IsOrphan: $scope.modalChild.item.isorphan == true ? "Y" : "N", IsHandicapped: $scope.modalChild.item.ishandicapped == true ? "Y" : "N", WaitingDays: $scope.modalChild.item.waitingdays
                        , Age: $scope.modalChild.item.age, BirthDate: $scope.modalChild.item.birthdate, CountryCode: $scope.modalChild.item.countrycode
                        , Gender: $scope.modalChild.item.gender == "남자" ? "M" : "F", HangulName: $scope.modalChild.item.hangulname, HangulPreferredName: $scope.modalChild.item.hangulpreferredname
                        , FullName: $scope.modalChild.item.fullname, PreferredName: $scope.modalChild.item.preferredname
                    }
                    }).success(function (r) {

                        if (r.success) {
<%--                            r.message = "childmasterid : " + $scope.modalChild.item.childmasterid;
                            r.message += " childKey : " + '<%:this.ViewState["childKey"].ToString()%>';
                            r.message += " Pic : " + $scope.modalChild.item.pic;
                            r.message += " IsOrphan : " + $scope.modalChild.item.isorphan;
                            r.message += " IsHandicapped : " + $scope.modalChild.item.ishandicapped;
                            r.message += " WaitingDays : " + $scope.modalChild.item.waitingdays;

                            r.message += " Age : " + $scope.modalChild.item.age;
                            r.message += " BirthDate : " + $scope.modalChild.item.birthdate;
                            r.message += " CountryCode : " + $scope.modalChild.item.countrycode;
                            r.message += " Gender : " + $scope.modalChild.item.gender;
                            r.message += " HangulName : " + $scope.modalChild.item.hangulname;
                            r.message += " HangulPreferredName : " + $scope.modalChild.item.hangulpreferredname;
                            r.message += " FullName : " + $scope.modalChild.item.fullname;
                            r.message += " PreferredName : " + $scope.modalChild.item.preferredname;
                            alert("STEST_" + r.message);--%>

//                            alert(r.message);
                            alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
                            location.href = r.data;
                        } else {
                            if (r.action == "nonpayment") {		// 미납금

                                if (confirm("지연된 후원금이 존재합니다.\n지연된 후원금이 있는 경우 1:1 어린이 양육 프로그램을 후원하실 수 없습니다. 지연된 후원금을 결제하시겠습니까?")) {
                                    location.href = "/my/sponsor/pay-delay/";
                                }

                            } else {
//                                alert("TEST_" + r.message);
                                alert(r.message);
                            }
                        }
                        //[이종진]로딩추가
                        $("#loading_bg").css("z-index", zIndexBg);
                        $("#loading_container").css("z-index", zIndexCon);
                        loading.hide();
                    });
                    
                }

            }

            $scope.$apply();

            setTimeout(function () {
                common.bindSNSAction();
            }, 500);

        }

        $(function () {
            setTimeout(function () {
                common.bindSNSAction();
            }, 500);

            appendGoogleMapApi();

            getCaseStudy();
        })

        function getCaseStudy() {
            //console.log("childKey : " + '<%:this.ViewState["childKey"].ToString()%>');
		    $.get("/api/tcpt.ashx", { t: "get-casestudy", childKey: '<%:this.ViewState["childKey"].ToString()%>', childMasterId: '<%:this.ViewState["childMasterId"].ToString()%>' }, function (r) {
		        loading.hide();

		        console.log("getCaseStudy", r);
		        if (r.success) {

		            if (r.data.family) {
		                $("#pop_family").html("<em>" + r.data.family + "</em>께서 저를 돌봐주세요.");
		            }
		            if (r.data.hobby) {
		                $("#pop_hobby").html("저는 <em>" + r.data.hobby + "</em>(을)를 좋아해요.");
		            }
		            if (r.data.health) {
		                $("#pop_health").html("(" + r.data.health + ")");
		            } else {
		                //$("#pop_health").html("없음");
		                if ($("#pop_ishandicapped").val() == "없음") {
		                    //$("#pop_disease").hide();
		                }
		            }



		        }
		    })
		}

		function appendGoogleMapApi() {
		    if (typeof google === 'object' && typeof google.maps === 'object') {
		        initMap();
		    } else {
		        var script = document.createElement("script");
		        script.type = "text/javascript";
		        script.src = "https://maps.googleapis.com/maps/api/js?language=ko&region=kr&key=<%:this.ViewState["googleMapApiKey"].ToString() %>&callback=initMap";
				document.body.appendChild(script);
            }
        }


        function initMap() {

            lat = parseFloat(<%:this.ViewState["lat"].ToString() %>);
		    lng = parseFloat(<%:this.ViewState["lng"].ToString() %>);

		    map = new google.maps.Map(document.getElementById('map'), {
		        scrollwheel: false,
		        center: { lat: lat, lng: lng },
		        zoom: 5,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		    });

		    var marker = new google.maps.Marker({
		        position: new google.maps.LatLng(lat, lng),
		        icon: "/common/img/icon/pin.png",
		        map: map
		    });

		};


    </script>

    <!--NSmart Track Tag Script-->
        <script type='text/javascript'>
            callbackFn = function() {};
            var _nsmart = _nsmart || [];
            _nsmart.host = _nsmart.host || (('https:' == document.location.protocol) ? 'https://' : 'http://');
//document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            // document.write(unescape("%3Cscript src='" + _nsmart.host + "n00.nsmartad.com/etc?id=10' type='text/javascript'%3E%3C/script%3E"));
            /* v 1.4 ---------------------------------*
            *  Nasmedia Track Object, version 1.4
            *  (c) 2009 nasmedia,mizcom (2009-12-24)
            *---------------------------------------*/
            var nasmedia = {};
            var NTrackObj = null;
            nasmedia.track = function(){
                this.camp, this.img = null;
            };
            nasmedia.track.prototype = {
                callTrackTag : function(p, f, c){
                    if(Object.prototype.toString.call(_nsmart) != '[object Array]' || _nsmart.host == undefined || _nsmart.host == '') return false;
                    if(f != undefined && typeof(f) == 'function') {
                        this.img.onload = this.img.onerror = this.img.onabort = f;
                        var no = ((parseInt(c, 10) % 100) < 10 ? '0' + (parseInt(c, 10) % 100): (parseInt(c, 10) % 100));
                        this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + c + '&page=' + p + '&r=' + Math.random();
                    }
                    else this.callNext(0);
                    return false;
                },
                callNext : function(i) {
                    if(i == this.camp.length) return false;
                    this.img.onload = this.img.onerror = this.img.onabort = function(o, i){return function(){o.callNext(i);};}(this, i+1);
                    var no = ((parseInt(this.camp[i][0], 10) % 100) < 10 ? '0' + (parseInt(this.camp[i][0], 10) % 100): (parseInt(this.camp[i][0], 10) % 100));
                    this.img.src = this.camp.host + 'n' + no + '.nsmartad.com' + '/track?camp=' + this.camp[i][0] + '&page=' + this.camp[i][1] + '&r=' + Math.random();
                },
                init : function(c) {
                    this.camp = c, this.img = new Image;
                }
            };
            (function(){
                NTrackObj = new nasmedia.track();
            })();
            NTrackObj.init(_nsmart);
        NTrackObj.callTrackTag();
        </script>
        <!--NSmart Track Tag Script End..-->


    <div class="pop_type1 w980 fn_pop_content" style="width: 980px; height: 2200px; padding-top: 50px;">
        <div class="pop_title">
            <span>어린이 정보</span>
            <button class="pop_close" ng-click="modalChild.hide($event)"><span>
                <img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
        </div>

        <div class="pop_content padding1 bg1">

            <!-- 어린이 정보 -->
            <div class="subContents sponsor">

                <div class="childInfoDetail">

                    <!-- 어린이 정보 -->
                    <div class="child_info clear2">
                        <div class="child_card">
                            <div class="picWrap relative" runat="server" id="picWrap">
                                <span class="pic" id ="picChild" background-img="{{modalChild.item.pic}}" data-default-image="/common/img/page/my/no_pic.png" style="background: no-repeat center top; background-size: cover; background-size: 220px;">어린이 사진</span>
                                <div class="info">
                                    <span class="nation">{{modalChild.item.countryname}}</span>
                                    <span class="age">{{modalChild.item.age}}세</span>
                                    <span class="sex">{{modalChild.item.gender}}</span>
                                </div>

                                <a href="#" ng-click="modalChild.goPay($event)" onclick="javascript:NTrackObj.callTrackTag(32036, callbackFn, 12490);" runat="server" id="btn_commitment" class="btn_type1">결연하기</a>
                            </div>
                        </div>

                        <div class="txt_con">
                            <div class="label"><span>{{modalChild.item.waitingdays}}일</span>동안 후원자님을 기다리고 있어요.</div>
                            <p class="hello">안녕하세요?<br />
                                제 이름은 <em>{{modalChild.item.namekr}}</em>입니다.</p>
                            <p class="story">
                                제 나이는 <em>{{modalChild.item.age}}살</em>이고요, 생일은 <em>{{modalChild.item.birthdate | date:'yyyy년 M월 d일'}}</em>이에요. 저는 <em>{{modalChild.item.countryname}}</em>에 살아요.<br />
                                <span id="pop_family"></span>&nbsp;<span id="pop_hobby"></span>
                                <br />
                                후원자님과 함께 컴패션에서 꿈을 키우고 싶어요.<br />
                                <input type="hidden" id="pop_ishandicapped" value='{{modalChild.item.ishandicapped ? "있음" : "없음"}}' />
                                <span class="disease" id="pop_disease"><span id="pop_health"></span></span>

                            </p>

                            <p class="recom" runat="server" id="btn_sns">
                                <span class="txt">이 어린이가 후원자를 만날 수 있도록 SNS로 친구에게 추천해보시겠어요?</span><br />
                                <!-- sns 공유하기 -->
                                <span class="sns_ani mt10">
                                    <span class="common_sns_group">
                                        <span class="wrap">
                                            <a href="#" title="페이스북" data-role="sns" data-provider="fb" data-url="" class="sns_facebook" child-item="{{modalChild.item}}">페이스북</a>
                                            <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" data-url="" class="sns_story" child-item="{{modalChild.item}}">카카오스토리</a>
                                            <a href="#" title="트위터" data-role="sns" data-provider="tw" data-url="" class="sns_twitter" child-item="{{modalChild.item}}">트위터</a>
                                            <a href="#" title="url 공유" data-role="sns" data-provider="copy" data-url="" class="sns_url" child-item="{{modalChild.item}}">url 공유</a>
                                        </span>
                                    </span>
                                    <button class="common_sns_share">공유하기</button>
                                </span>
                                <!--// -->
                            </p>

                            <!-- 날씨 -->
                            <div class="weather">
                                <span class="nation">지금 <%:ViewState["time"].ToString() %><br />
                                    {{modalChild.item.countryname}}는(은)요,</span>
                                <span class="icon">
                                    <img src="/common/img/page/sponsor/weather/<%:ViewState["weather_icon"].ToString() %>.png" alt="흐림" /></span>
                                <span class="temperature"><%:ViewState["weather"].ToString() %><span><%:ViewState["temperature"].ToString() %>˚C</span></span>
                            </div>
                        </div>
                    </div>
                    <!--// 어린이 정보 -->

                    <!-- 구글지도 -->
                    <div class="map" id="map"></div>

                    <!--
					<div class="map" style="background:url('/common/img/temp/img_24.jpg') no-repeat center;">
						<span class="pin" style="position:absolute;left:46%;top:25%;"><img src="/common/img/icon/pin.png" alt="위치" /></span>
					</div>
					-->
                    <!--// 구글지도 -->

                    <!-- 텍스트 정보 -->
                    <div class="nation_info fn_country_info">

                        <asp:literal runat="server" id="content"></asp:literal>
                    </div>
                    <!--// 텍스트 정보 -->

                    <div class="bg_bottom"></div>
                </div>

            </div>

        </div>
    </div>



</div>
