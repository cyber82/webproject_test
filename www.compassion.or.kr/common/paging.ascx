﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="paging.ascx.cs" Inherits="Common.Paging" %>


<div class="tac mb60">
	<div class="paging_type1">
			<asp:LinkButton runat=server ID=first OnClick=Move CssClass="bg_page first">처음</asp:LinkButton>
			<asp:LinkButton runat=server ID=prev OnClick=Move CssClass="bg_page prev">이전</asp:LinkButton>
	

			<asp:PlaceHolder runat=server ID=container></asp:PlaceHolder>	

		
			<asp:LinkButton runat=server ID=next OnClick=Move CssClass="bg_page next">다음</asp:LinkButton>
			<asp:LinkButton runat=server ID=last OnClick=Move CssClass="bg_page last">마지막</asp:LinkButton>
	</div>
	<div class="clear h30"></div>
</div>

