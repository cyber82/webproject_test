﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CommonLib;
using Newtonsoft.Json.Linq;
using TCPTModel;
using TCPTModel.Response.Supporter;
using Newtonsoft.Json;
using System.Text;
using TCPTModel.Request.Beneficiary;
using TCPTModel.Response.Beneficiary;
using System.Configuration;

public partial class childRandom : FrontBasePage
{
    protected override void OnBeforePostBack()
    {
        GetMainVisualType3();

        /* 이벤트 정보 등록 -- 20170327 이정길 */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;
        Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));

        //김태형 2017.06.01 
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else
        {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }
    }

    bool GetMainVisualType3()
    {
        var action = new ChildAction();

        //		var actionResult = action.GetChildren(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);
        //        var actionResult = action.GetChildrenGp(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);

        var actionResult = new JsonWriter();
        if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
        {
            actionResult = action.GetChildren(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);
        }
        else if (ConfigurationManager.AppSettings["dbgp_kind"] == "2")
        {
            actionResult = action.GetChildrenGp(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);
        }

        if (actionResult.success)
        {
            Random rnd = new Random();
            var items = (List<ChildAction.ChildItem>)actionResult.data;
            if (items.Count < 1)
                return false;
            var index = rnd.Next(items.Count);

            var item = items[index];

            main_recmd3_pic.Src = item.Pic;
            main_recmd3_country.Text = item.CountryName;
            main_recmd3_age.Text = item.Age.ToString();
            main_recmd3_gender.Text = item.Gender;
            main_recmd3_name.Text = item.Name;
            //main_recmd3_name2.Text = item.Name.Length > 5 ? item.Name.Substring(0, 5) + ".." : item.Name;
            main_recmd3_link.HRef = main_recmd3_link.HRef.Replace("{childMasterId}", item.ChildMasterId);
            return true;
        }
        else
        {
            return false;
        }


    }
}