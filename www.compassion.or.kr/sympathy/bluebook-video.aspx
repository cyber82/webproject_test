﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="bluebook-video.aspx.cs" Inherits="bluebook_video" %>

<div style="background: transparent; width: 800px;">

    <!-- 일반팝업 width : 800 -->

    <div class="pop_type1 w800">
        <div class="pop_title">
            <span>블루북 소개 영상</span>
            <button class="pop_close" ng-click="modalVideo.close($event)"><span>
                <img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
        </div>

        <div class="pop_content movie">
            <iframe width="800" height="450" title="영상" id="bluebook_movie" src="https://www.youtube.com/embed/tTnkK5CblOs?rel=0&showinfo=0&wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <!--// popup -->

</div>
