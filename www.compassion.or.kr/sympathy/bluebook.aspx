﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="bluebook.aspx.cs" Inherits="sympathy_bluebook" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/sympathy/default.js?v=1.1"></script>
    <script type="text/javascript">

        function jusoCallback(zipNo, addr1, addr2, jibun) {
        	// 실제 저장 데이타
        	$(".addr1").val(addr1 + "//" + jibun);
        	$(".addr2").val(addr2);

        	// 화면에 표시
        	$(".zipcode").val(zipNo);

        	$("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
        	$("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);

        };
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
    <input type="hidden" id="s_type" value="bluebook" />
    <input type="hidden" id="sponsor_id" value="" runat="server"/>
    <input type="hidden" id="register_book" value="" runat="server"/>
    <!-- sub body -->
    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>컴패션<em>블루북</em></h1>
                <span class="desc">함께 나누고 싶은 후원자님들의 블루북을 소개합니다</span>

                <uc:breadcrumb runat="server" />

            </div>
        </div>
        <!--// 타이틀 -->

        <!-- s: sub contents -->
        <div class="subContents sympathy padding0">

            <!-- visual -->
            <div class="visual_bb">
                <div class="w980">
                    <div class="titWrap">
                        <p class="tit">
                            어린이를 만난 후,
                            <br />
                            후원자님의 삶에는 어떤 변화가 있었나요?
                        </p>
                        <span class="bar"></span>
                        <span class="txt">후원자님이 겪은 아름다운 변화를 블루북을 통해 더 많은 분들에게 나눠주세요.</span>
                        <a href="javascript:;" class="btn_b_type2" ng-click="modalVideo.show()"><span class="play"></span>블루북 영상 보기</a>
                    </div>
                </div>
            </div>
            <!--// -->

            <!-- best -->
            <div class="bestWrap">
                <div class="w980 relative">
                    <div class="bestList ml20">
                        <!-- 이미지 사이즈 : 308 * 226 -->
                        <asp:Repeater runat="server" ID="repeater">
                            <ItemTemplate>
                                <a ng-click="goView(<%#Eval("idx") %>)" class="blst mr20">
                                    <span class="img_wh" style="background:url('<%#Eval("thumb") %>') no-repeat center top;"></span>
                                    <span class="con">
                                        <span class="tit elps"><%#Eval("title") %></span>
                                        <%#Eval("sub_title") %>
                                    </span>
                                </a>
                            </ItemTemplate>

                        </asp:Repeater>
                        
                    </div>
                    <div class="banner">
                        <a href="javascript:;" ng-click="modalBluebookIs.show()" class="bnr bnr1">
                            <span class="tit">블루북이란?</span>
                            <span class="sub">후원자님들의 블루북을<br />
                                소개합니다.</span>
                        </a>
                        <a href="javascript:;" ng-click="modalMessage.show()" class="bnr bnr2">
                            <span class="tit">블루북신청</span>
                            <span class="sub">지금 블루북 프로젝트에<br />
                                참여하세요!</span>
                        </a>
                    </div>
                </div>
            </div>
            <!--// best -->

            <div class="w980">

                <!-- 게시판 리스트 -->
                <div class="boardList_1" id="l">

                    <div class="sortWrap">
                        <div class="sort">
                            <a ng-class="{'recent on':params.s_column == 'reg_date','recent':params.s_column != 'reg_date'}" ng-click="sort('reg_date')">최신순</a>
                            <span class="bar"></span>
                            <a ng-class="{'interest on':params.s_column == 'view_count','interest':params.s_column != 'view_count'}" ng-click="sort('view_count')">인기순</a>
                        </div>
                        <div class="fr relative">
                            <label for="k_word" class="hidden">검색어 입력</label>
                            <input type="text" name="k_word" id="k_word" class="input_search1" ng-enter="search()" style="width: 245px" placeholder="검색어를 입력해 주세요" />
                            <a ng-click="search()" class="search_area1">검색</a>
                        </div>
                    </div>

                    <ul class="list">
                        <li ng-repeat="item in list">
                            <span class="img_wh" style="background:url('{{item.thumb}}') no-repeat center top;"></span>
                            <span class="tit_box">
                                <span class="tit">{{item.title}}</span>
                                <span class="name">{{item.sub_title | limitHtml: 20}}</span>
                                <span class="bar"></span>
                                <span class="day">{{parseDate(item.reg_date) | date:'yyyy.MM.dd'}}</span>
                            </span>

                            <!-- 마우스 오버 시 -->
                            <span class="over"><a class="btn_view1" ng-click="goView(item.idx)">VIEW</a></span>
                        </li>
                        <li ng-hide="total" class="no_content">
                            <span>검색 결과가 없습니다.</span>
                        </li>

                    </ul>

                </div>
                <!--// 게시판 리스트 -->

                <!-- page navigation -->
                <div class="tac">
                    <paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>
                </div>
                <!--//page navigation -->

            </div>
            <!---//w980 -->

        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>

    </section>
    <!--// sub body -->


</asp:Content>
