﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Configuration;

public partial class sympathy_bluebook : FrontBasePage {


	protected override void OnBeforePostBack() {
        base.OnBeforePostBack();
        this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];
		base.LoadComplete += new EventHandler(list_LoadComplete);

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.ssb_article.Where(p => p.notice == true && p.board_id == "bluebook" && p.del_flag == false && p.yn1 != true).OrderByDescending(p => p.reg_date).Take(2);
            var list = www6.selectQ<ssb_article>("notice", 1, "board_id", "bluebook", "del_flag", 0, "yn1", 0, "reg_date desc").Take(2);

            repeater.DataSource = list;
            repeater.DataBind();

        }

        UserInfo sess = new UserInfo();

        if(sess.ConId.Length != 6 && sess.ConId.Length != 5) {
            sponsor_id.Value = "N";

        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            var exist = www6.selectQ<tBlueBook>("RegisterID", sess.UserId);
            //if (dao.tBlueBook.Any(p => p.RegisterID == sess.UserId))
            if(exist.Any())
            {
                register_book.Value = "N";
                return;
            }

        }

    }


    protected override void OnAfterPostBack() {
	}

   


    }