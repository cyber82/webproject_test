﻿(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, paramService, $timeout) {

        // 비전트립 일정에서 넘어온 경우 뒤로가기 또는 목록으로 갔을 때 해당 year 선택되기위해 추가
        if (getParameterByName("year") != null && getParameterByName("year") != undefined && getParameterByName("year") != "")
            cookie.set("visiontripYear", getParameterByName("year"), 1);

        $scope.isLogin = common.isLogin();
        $scope.userId = common.getUserId();

        $scope.total = -1;
        $scope.page = 1;
        $scope.rowsPerPage = 10;
        $scope.content = "";
        $scope.selectItem = null; // 선택된 댓글(수정될 항목)

        $scope.list = [];
        $scope.params = {
            id: $("#id").val(),
            page: $scope.page,
            rowsPerPage: $scope.rowsPerPage,
            type: "story",


        };
        
        $scope.paramValue = $.extend($scope.paramValue, paramService.getParameterValues());

        $scope.getList = function (params) {
            $scope.params = $.extend($scope.params, params);
            $http.get("/api/sympathy_reply.ashx?t=list", { params: $scope.params }).success(function (result) {
                if (result.success) {
                    $scope.list = result.data;
                    $scope.total = result.data.length > 0 ? result.data[0].total : 0;
               
                }
            });


            if (params)
                scrollTo($("#l"), 10);
        }

        $scope.checkLogin = function ($event) {

            if (!common.checkLogin()) {
                $event.preventDefault();
                return;
            }

        },

        $scope.add = function ($event) {
        	$event.preventDefault();

            if (!common.checkLogin()) {
                return;
            }

            if ($scope.content == "") {
                alert("댓글을 입력해주세요.");
                $("#reply").focus();
                $event.preventDefault();
                return;
            }
 
            $http.post("/api/sympathy_reply.ashx", { t: 'add', article_idx: $("#id").val(), content: $scope.content }).success(function (result) {
                //console.log(result);
            	if (result.success) {
            		$scope.content = "";
            		$scope.getList({ page: 1 });
            		$("#count").text(0);
                } else {
                    alert("댓글을 입력해주세요.");
                }
            })
         
        }

        $scope.update = function ($event, index) {

        	$event.preventDefault();
        	if (!common.checkLogin()) {
        		$event.preventDefault();
        		return;
        	}

        	if ($scope.selectItem.contentModify == "") {
        	    alert("댓글을 입력해주세요.");
        		return;
        	}

        	$http.post('/api/sympathy_reply.ashx', { t: 'update', id: $scope.selectItem.idx, content: $scope.selectItem.contentModify }).success(function (result) {
        		if (result.success) {
        			$scope.getList();

        		} else {
        			alert(result.message);
        		}

        	})
        	
        }

        $scope.remove = function (id) {
            if (confirm("정말 삭제하실 건가요?")) {
            	$http.post("/api/sympathy_reply.ashx", {t : "remove" , "id" : id}).success(function (result) {
                    if (result.success) {
                        alert("삭제 되었습니다.");
                        $scope.getList();
                    } else {
                        alert(result.message);
                    }
                });
            } else {
                alert("삭제가 취소되었습니다.");
            }
        }
        
        $scope.showModify = function (entity) {
        	$scope.selectItem = entity;
        	entity.is_show_update = true;
        	entity.contentModify = entity.body;
        }

        $scope.cancel = function (entity) {
            entity.is_show_update = false;
        }

        $scope.getList();

        // 페이지별 제목
        $scope.showTitle = function () {
        	$("#tit_" + $("#hidden_s_type").val()).show();
            return;
        }
        $scope.showTitle();

    	// 페이지별 네비게이션
    	// sitemap에서 제대로 가져오지 못함
        $scope.setNavi = function () {
        	var current = "";
        	if ($("#hidden_s_type").val() == "child") {
        		current = "꿈꾸는 어린이";
        	} else if ($("#hidden_s_type").val() == "sponsor") {
        		current = "꿈을 심는 후원자";
        	} else if ($("#hidden_s_type").val() == "vision") {
        		current = "비전트립 다이어리";
        	} else if ($("#hidden_s_type").val() == "essay") {
        		current = "함께 쓰는 에세이";
        	} else if ($("#hidden_s_type").val() == "bluebook") {
        		current = "컴패션블루북";
        	}

        	$(".current").text(current);
        }
        $scope.setNavi();

        $scope.textLimit = function () {
            $timeout(function () {
            if ($scope.selectItem.contentModify.length >= 300) {
                alert("300자 까지 입력할 수 있습니다.");
            }
            }, 0);
        }


    });

})();