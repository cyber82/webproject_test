﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="child.aspx.cs" Inherits="sympathy_child" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/sympathy/default.js?v=1"></script>
    <script type="text/javascript">
        $(function () {
            $(".btn_nation").click(function () {
                $(".tbl2").slideToggle();
                var a = $(".btn_nation .more");
                if (a.hasClass("on") == true) {
                    a.removeClass("on");
                }
                else {
                    a.addClass("on");
                }
            })
        })
    </script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" id="s_type" value="child" />
    <input type="hidden" runat="server" id="nation_value" value="" />

    <!-- sub body -->
    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>꿈꾸는 <em>어린이</em></h1>
                <span class="desc">꿈꾸며 성장하는 컴패션어린이들의 이야기</span>

               <uc:breadcrumb runat="server"/>
            </div>
        </div>
        <!--// 타이틀 -->

        <!-- s: sub contents -->
        <div class="subContents sympathy">
            <div class="w980">

                <!-- buttonWrap -->
                <div class="viewNation">
                    <button type="button" class="btn_nation"><span>국가별로 보기<span class="more"></span></span></button>
                    <div class="tbl2">
                        <table class="nation_lst">
                            <caption>국가별 리스트</caption>
                            <colgroup>
                                <col style="width: 20%;" />
                                <col style="width: 80%;" />
                            </colgroup>
                            <tbody>

                                <tr ng-repeat="item in nationList">
                                    <th scope="row">{{item.nc_name}}</th>
                                    <td>
                                        <a ng-click="getList({depth1 : country.nc_depth1, depth2 : country.nc_depth2})" ng-repeat="country in item.countries">{{country.nc_name}}</a>

                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!--// buttonWrap -->

                <!-- 게시판 리스트 -->
                <div style="position:absolute;top:270px" id="l"></div>
                <div class="boardList_1">

                    <div class="sortWrap">
                        <div class="sort">
                            <a ng-class="{'recent on':params.s_column == 'reg_date','recent':params.s_column != 'reg_date'}" ng-click="sort('reg_date')">최신순</a>
                            <span class="bar"></span>
                            <a ng-class="{'interest on':params.s_column == 'view_count','interest':params.s_column != 'view_count'}" ng-click="sort('view_count')">인기순</a>
                        </div>
                        <div class="fr relative">
                            <label for="k_word" class="hidden">검색어 입력</label>
                            <input type="text" id="k_word" ng-enter="search()"  class="input_search1" name="k_word" style="width:245px" placeholder="검색어를 입력해 주세요" />
                           
                            <a class="search_area1" ng-click="search()">검색</a>

                        </div>
                    </div>
                    <ul class="list angular_area" style="display:none">
                        <li ng-repeat="item in list">
                            <span class="img_wh" style="background:url('{{item.thumb}}') no-repeat center top;"></span>
                            <span class="tit_box box2">
                                <span class="tit tit2">{{item.title}}</span>
                                <span class="day">{{parseDate(item.reg_date) | date:'yyyy.MM.dd'}}</span>
                            </span>
                            <span class="nation" style="height:38px" ng-hide="!item.depth2_name">{{item.depth2_name}}</span>
                            <div style="height:38px" ng-show="!item.depth2_name"></div>
                            <!-- 마우스 오버 시 -->
                            <span class="over"><a class="btn_view1" ng-click="goView(item.idx)">VIEW</a></span>
                        </li>
                        <li ng-hide="total" class="no_content">
                            <span>검색 결과가 없습니다.</span>
                        </li>
                    </ul>
                </div>
                <!--// 게시판 리스트 -->

				<!-- page navigation -->
				<div class="tac mb60">
					 <paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>  
                </div>
                <!--// page navigation -->

                <div class="h_gallery">
                    <span>컴패션과 함께하는 <em>허호아이 갤러리</em>에서 다양한 어린이들의 모습을 만나 보세요</span>
                    <span class="b_gallery">
                        <a href="http://www.huheye.com/" class="btn_s_type6" title="btn_s_type6">허호아이 갤러리</a>
                    </span>
                </div>
            </div>
        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>

    </section>
    <!--// sub body -->


</asp:Content>
