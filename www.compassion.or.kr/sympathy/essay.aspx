﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="essay.aspx.cs" Inherits="sympathy_essay" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/sympathy/default.js?v=1"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnWrite").click(function () {
                if (common.isLogin()) {
                    location.href = "/sympathy/write/";
                } else {
                    common.checkLogin();
                }


                return false;
            });

        });
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
        <input type="hidden" id="s_type" value="essay" />
        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>함께 쓰는 <em>에세이</em></h1>
                <span class="desc">후원자님과 함께 만들어 가는 컴패션 이야기</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// 타이틀 -->

        <!-- s: sub contents -->
        <div class="subContents sympathy">
            <div class="w980">

                <!-- 게시판 리스트 -->
                <div style="position: absolute; top: 180px" id="l"></div>
                <div class="boardList_1">

                    <div class="sortWrap">
                        <div class="sort">
                            <a ng-class="{'recent on':params.s_column == 'reg_date','recent':params.s_column != 'reg_date'}" ng-click="sort('reg_date')">최신순</a>
                            <span class="bar"></span>
                            <a ng-class="{'interest on':params.s_column == 'view_count','interest':params.s_column != 'view_count'}" ng-click="sort('view_count')">인기순</a>
                        </div>
                        <div class="fr relative">
                            <label for="k_word" class="hidden">검색어 입력</label>
                            <input type="text" id="k_word" name="k_word" ng-enter="search()" class="input_search1" style="width: 245px" placeholder="검색어를 입력해 주세요" />
                            <a ng-click="search()" class="search_area1">검색</a>
                        </div>
                    </div>
                    <ul class="list">
                        <li ng-repeat="item in list">
                            <span class="img_wh" style="background:url('{{item.thumb}}') no-repeat center top;"></span>
                            <span class="tit_box">
                                <span class="tit tit2">{{item.title}}</span>
                                <span class="day">{{parseDate(item.reg_date) | date:'yyyy.MM.dd'}}</span>
                            </span>

                            <!-- 마우스 오버 시 -->
                            <span class="over"><a class="btn_view1" ng-click="goView(item.idx)">VIEW</a></span>
                        </li>
                        <li ng-hide="total" class="no_content">
                            <span>검색 결과가 없습니다.</span>
                        </li>
                    </ul>

                </div>
                <!--// 게시판 리스트 -->

                <!-- page navigation -->
                <div class="tac mb60">
                    <paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>
                </div>
                <!--// -->

                <div class="h_gallery">
                    <span>다른 후원자님들과 <em>함께 나누고 싶은 이야기</em>가 있으신가요?</span>
                    <span class="b_gallery">
                        <a href="/sympathy/write" class="btn_s_type6">함께 쓰는 에세이 참여하기 </a>
                    </span>
                </div>
            </div>
        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>
    <!--// sub body -->


</asp:Content>
