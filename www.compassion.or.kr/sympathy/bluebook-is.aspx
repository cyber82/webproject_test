﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="bluebook-is.aspx.cs" Inherits="bluebook_is" %>

<div style="background: transparent;" class="fn_pop_container" id="bluebookWrapper">

    <!-- 일반팝업 width : 800 -->
    <div class="pop_type1 w800 fn_pop_content" style="height:1976px;padding-top:50px">
        <div class="pop_title">
            <span>블루북이란?</span>
            <button class="pop_close" ng-click="modalBluebookIs.close($event)"><span>
                <img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
        </div>

        <div class="pop_content bookinfo">
            <p class="desc1"><em>‘블루북’</em>은 후원자가 직접 만드는 <em>세상에 단 하나뿐인 다이어리</em>입니다.</p>
            <div class="img">
                <span class="bg"></span>
                <span class="txt">
                    <em>블루북은요,</em><br />
                    글, 그림, 사진…어떤 형식이든 좋아요.<br />
                    꾸미는 능력이 없어도 괜찮아요.<br />
                    후원자님의 이야기는 그 자체로도<br />
                    충분히 가치가 있으니까요.
                </span>
            </div>

            <p class="desc2">’블루북’은 미국의 예술 프로젝트인 ‘더 스케치북 프로젝트’에서 영감을 받아 시작한 프로젝트로, 이를 통해 수집된 블루북은 컴패션 체험전 및 다양한 통로로 전시되어 더 많은 분들을 만나게 됩니다.</p>
            
            <div class="progress">
                <div class="left">
                    <p class="tit">블루북, 이렇게 진행됩니다</p>
                    
                    <ol>
                        <li>
                            <span>1. 블루북 작성</span>
                            <ul>
                                <li>글, 그림, 사진 등 다양한 방법으로 블루북을 완성해 주세요.</li>
                                <li>노트 표지 및 내지의 자유로운 변형이 가능합니다. 다만,
                                    <br />
                                    내가 만든 블루북이 여러 사람에게 읽힐 수 있으니, 최대한
                                    <br />
                                    튼튼하게 만들어  주세요!</li>
                            </ul>
                        </li>
                        <li>
                            <span>2. 지인에게 소개</span>
                            <ul>
                                <li>블루북에는 아직 후원자님을 만나지 못한 컴패션어린이<br />
                                    정보가 함께 들어있습니다. 이 어린이가 후원자를 만날 수<br />
                                    있도록 기도해 주세요.</li>
                                <li>가장 소중한 분에게 블루북과 함께 어린이를 소개해 주세요.</li>
                            </ul>
                        </li>
                        <li>
                            <span>3. 완성된 블루북 반송</span>
                            <ul>
                                <li>지인이 결연을 원하는 경우, 블루북과 결연서를 함께 반송<br />
                                    봉투에 넣어 보내주세요.</li>
                                <li>지인이 결연을 원하지 않는 경우, 블루북만 반송봉투에 넣어<br />
                                    보내주세요.</li>
                            </ul>
                        </li>
                    </ol>
                </div>
                <div class="right">
                    <div class="cover">
                        <div>
                            <img src="/common/img/page/sympathy/bookcover.jpg" alt="블루북 표지 이미지" /></div>
                        <p class="txt">블루북 마지막 페이지에 담긴, <em>후원을 기다리고 있는<br />
                            어린이</em>에요. 어린이를 위해 기도해주시고, 후원자님을<br />
                            함께 찾아주세요.</p>
                    </div>
                    <p class="noti">* 블루북을 작성해주신 모든 분들에게는 컴패션에서 준비한<br />
                        &nbsp;&nbsp;&nbsp;선물을 보내드립니다.</p>
                </div>
            </div>

            <div class="qna">
                <p class="tit">자주 묻는 질문</p>
                <ul>
                    <li>
                        <span>완성된 블루북은 언제까지 보내면 되나요?</span>
                        <p>
                            <em class="fc_blue">블루북을 받으신 날짜로부터 한 달 동안 즐거운 시간을 보내신 뒤, 컴패션에 보내주세요.</em> 그러나 블루북 완성이<br />
                            늦어지는 경우에도 걱정하지 마세요. 완성되는 대로 반송봉투에 넣어서 컴패션으로 보내주시면 됩니다. 단,<br />
                            어린이가 오랫동안 후원자를 기다리지 않도록 하기 위해 <em class="fc_blue">어린이 결연은 결연서에 기재된 날짜까지만 가능합니다.</em>
                        </p>
                    </li>
                    <li>
                        <span>블루북만 보내도 되나요?</span>
                        <p>
                            물론입니다! 후원자님의 블루북을 통해 더 많은 분들이 후원의 가치를 알 수 있도록 컴패션 행사와 체험전 등을<br />
                            통해 블루북을 전시할 예정입니다. 소개하고 싶은 지인이 없다면, 불루북만 반송봉투에 넣어서 보내주세요.
                        </p>
                    </li>
                    <li>
                        <span>블루북을 여러 지인에게 소개하고 싶은데 어쩌죠?</span>
                        <p>
                            동봉된 어린이 정보와 결연서가 더 필요하신 경우, 한국컴패션으로 연락해주세요. 담당자 확인 후, 후원자님<br />
                            댁으로 발송해드리겠습니다.
                        </p>
                    </li>
                </ul>
            </div>
            
            <div class="tac">
                <a ng-click="modalBluebookIs.showBluebook($event)" class="btn_type1 mb20">블루북 신청하기</a><br />
                <span class="fs15 fc_gray">블루북은 <em class="fc_blue">기존 후원자</em>만 신청가능합니다.</span>
            </div>

        </div>
    </div>
    <!--// popup -->

</div>
