﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="sympathy_view" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

	<script type="text/javascript" src="/common/js/site/moveToMobile.js"></script>
    <script type="text/javascript" src="/sympathy/view.js?v=1.1"></script>
    <script type="text/javascript">
        $(function () {
            // 조회수 증가
            $.post("/api/sympathy.ashx?t=hits&idx=" + $("#id").val());
            
            $("#reply").keyup(function () {
                if ($("#count").text() >= 300) {
                    alert("300자 까지 입력할 수 있습니다.");
                }

            });


        });

    </script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <!-- sub body -->
    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <input type="hidden" id="id" runat="server" />
        <input type="hidden" id="hidden_s_type" runat="server" />
        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <div id="tit_bluebook" style="display: none">
                    <h1>컴패션<em>블루북</em></h1>
                    <span class='desc'>함께 나누고 싶은 후원자님들의 블루북을 소개합니다</span>
                </div>

                <div id="tit_child" style="display: none">
                    <h1>꿈꾸는 <em>어린이</em></h1>
                    <span class='desc'>꿈꾸며 성장하는 컴패션어린이들의 이야기</span>
                </div>

                <div id="tit_essay" style="display: none">
                    <h1>함께 쓰는 <em>에세이</em></h1>
                    <span class='desc'>후원자님과 함께 만들어 가는 컴패션 이야기</span>
                </div>

                <div id="tit_individual" style="display: none">
                    <h1><em>비전트립 다이어리</em></h1>
                    <span class='desc'>컴패션 양육이 이루어지는 현지로 떠나는 여행</span>
                </div>

                <div id="tit_vision" style="display: none">
                    <h1><em>비전트립 다이어리</em></h1>
                    <span class='desc'>컴패션 양육이 이루어지는 현지로 떠나는 여행</span>
                </div>

                <div id="tit_sponsor" style="display: none">
                    <h1>꿈을 심는 <em>후원자</em></h1>
                    <span class='desc'>어린이와 함께 꿈을 키워 가는 후원자님들의 이야기</span>
                </div>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents sympathy">
            <div class="w980">

                <!-- 게시판 상세 -->
                <div class="boardView_1">
                    <div class="tit_box noline">
                        <span class="tit"><asp:Literal runat="server" ID="title" /></span>
                        <span class="sub_tit"><asp:Literal runat="server" ID="sub_title" /></span>
                        <span class="txt">

                            <asp:PlaceHolder runat="server" ID="phNation">
                                <span><asp:Literal runat="server" ID="nation" /></span>
                                <span class="bar"></span>
                            </asp:PlaceHolder>

                            <span><asp:Literal runat="server" ID="reg_date" /></span>
                            <span class="bar"></span>
                            <span class="hit"><asp:Literal runat="server" ID="view_count" /></span>
                        </span>
                    </div>
                    <div class="view_contents">


                        <asp:Literal runat="server" ID="article" />

                    </div>

                    <!-- sns 공유하기 -->
                    <div class="share_box">
                        <span class="sns_ani">
                            <span class="common_sns_group">
                                <span class="wrap">
                                    <a href="#" title="페이스북" data-role="sns" data-provider="fb" class="sns_facebook">페이스북</a>
                                    <a href="#" title="카카오스토리" data-role="sns" data-provider="ks" class="sns_story">카카오스토리</a>
                                    <a href="#" title="트위터" data-role="sns" data-provider="tw" class="sns_twitter">트위터</a>
                                    <a href="#" title="url 공유" data-role="sns" data-provider="copy" class="sns_url">url 공유</a>
                                </span>
                            </span>
                            <button class="common_sns_share">공유하기</button>
                        </span>
                    </div>
                    <!--// -->

                    <!-- 댓글 -->
                    <div id="l">
                        <div class="comment">
                            <fieldset>
                                <legend>댓글 입력창</legend>
                                <span class="tit">댓글</span>
                                <span class="tit" ng-hide="total == 0">&nbsp;{{total}}</span>
                                <span id="count" style="position: absolute; top: 10px; right: 165px" ng-bind="content.length || 0">0</span>
                                <span style="position: absolute; top: 10px; right: 120px">/ 300자</span>
                                <div class="clear2">
                                    <label for="reply" class="hidden">댓글입력</label>
                                    <textarea class="textarea_type1" id="reply" name="content" ng-model="content" ng-focus="checkLogin($event)" maxlength="300"></textarea>

                                    <button type="submit" class="registration" ng-click="add($event)">등록</button>
                                </div>
                            </fieldset>
                        </div>

                        
                    <ul class="comment_lst mb20">
                        <li ng-repeat="item in list">
                            <div class="clear2">
                                <div class="id_box">
                                    <span class="id">{{item.user_id}}</span>
                                    <span class="txt">{{item.body}}</span>
                                </div>
                                <div class="modify_box">

                                    <a class="modify" ng-click="showModify(item)" ng-show="item.is_owner">수정</a>
                                    <span class="bar" ng-show="item.is_owner"></span>
                                    <a class="delete" ng-click="remove(item.idx)" ng-show="item.is_owner">삭제</a>
                                    <span class="day">{{item.reg_date | date : 'yyyy-MM-dd HH:mm:ss' }}</span>
                                </div>
                            </div>
                            <!-- 수정하기 -->
                            <div class="modifyArea" ng-show="item.is_show_update">
                                <label for="modify_{{$index}}" class="hidden">댓글 수정하기</label>
                                <div class="tar mb10"><span ng-bind="item.contentModify.length || 0">0</span>/300자</div>
                                <textarea id="modify_{{$index}}" class="textarea_type1 mb20" style="width: 100%;" maxlength="300" rows="4" ng-keyup="textLimit()" ng-model="item.contentModify"></textarea>

                                <div class="tac">
                                    <button class="btn_type2 mr5" ng-click="cancel(item)"><span>수정취소</span></button>
                                    <button type="submit" class="btn_type1" ng-click="update($event)"><span>수정하기</span></button>
                                </div>
                            </div>
                            <!--// -->
                        </li>
						<!-- 댓글 없을때 -->
						<li class="no_content" ng-if="total == 0 ">등록된 댓글이 없습니다.</li>
						<!--//  -->
                    </ul>
                    </div>

                    <!--// 댓글-->

                    <!-- page navigation -->
                    <div class="tac mb60">
                        <paging class="small" page="page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>
                    </div>
                    <!--// -->

                    <!-- 이전/다음글 -->
                    <ul class="view_list">
                        <li runat="server" id="liPrev">
                            <span class="pre">이전글</span>
                            <span class="bar"></span>
                            <a href="#" class="txt" runat="server" id="btnPrev">
                                <asp:Literal runat="server" ID="prevTitle"></asp:Literal></a>
                        </li>
                        <li runat="server" id="liNext">
                            <span class="next">다음글</span>
                            <span class="bar"></span>
                            <a href="#" class="txt" runat="server" id="btnNext">
                                <asp:Literal runat="server" ID="nextTitle"></asp:Literal></a>
                        </li>
                    </ul>
                    <!--// -->
                </div>
                <!--// 게시판 상세 -->

                <div class="tar"><a href="#" class="btn_type4 " title="목록" runat="server" id="btnList">목록</a></div>
            </div>
        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>

</asp:Content>
