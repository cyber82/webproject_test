﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="bluebook-message.aspx.cs" Inherits="sympathy_bluebook_message" %>

<div style="background: transparent;" class="fn_pop_container" id="bluebookWrapper">

    <!-- 일반팝업 width : 800 -->
    <div class="pop_type1 w800 fn_pop_content" style="height:1976px;padding-top:50px">
        <div class="pop_title">
            <span>&lt;컴패션블루북 마감 안내&gt;</span>
            <button class="pop_close" ng-click="modalMessage.close($event)"><span>
                <img src="/common/img/btn/close_1.png" alt="팝업닫기" /></span></button>
        </div>

        <div class="pop_content bookinfo" style="text-align:center; font-size:12pt;">
            컴패션블루북에 많은 관심과 사랑을 가져 주셔서 감사합니다.<br />
            블루북은 2016년 10월을 기점으로 시즌1을 마무리하고,<br />
            시즌2를 준비하려고 합니다.<br />
            더 새로워진 블루북 시즌2를 기대해주세요.<br />
            한국컴패션 마케팅팀 드림.<br />

            <br /><br />
            <a class="btn_type1 mb20" ng-click="modalMessage.goLink($event)">블루북 시즌1 모음집 바로가기</a>
            
        </div>
    </div>
    <!--// popup -->

</div>