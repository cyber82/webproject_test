﻿(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, $location, paramService, $element) {

		$scope.youtube = [];
		$scope.facebook = [];
		$scope.instagram = [];
		$scope.tab = "all";
		$scope.list = {};
		$scope.page = 0;

		$scope.initData = function () {
			$http.get("/api/social_hub.ashx").success(function (result) {

				$scope.youtube = $.grep(result, function (e) { return e.provider == 'youtube'; });
				$scope.facebook = $.grep(result, function (e) { return e.provider == 'facebook'; });
				$scope.instagram = $.grep(result, function (e) { return e.provider == 'instagram'; });

				$scope.getList();
			});
		}

		$scope.getList = function () {


			$scope.list.youtube = $scope.youtube.slice();
			$scope.list.facebook = $scope.facebook.slice();
			$scope.list.instagram = $scope.instagram.slice();
			$scope.list.all = [];



			// 페이스북 4개
			while ($scope.facebook.length > 5 && $scope.instagram.length > 2 && $scope.youtube.length > 2) {
				var index = 0;
				while (index < 4) {
					$scope.list.all.push($scope.facebook.shift());
					index++;
				}

				// 인스타 1개
				$scope.list.all.push($scope.instagram.shift());

				// 유튜브 2개
				$scope.list.all.push($scope.youtube.shift());
				$scope.list.all.push($scope.youtube.shift());

				// 페이스북 1개
				$scope.list.all.push($scope.facebook.shift());

				// 인스타 1개
				$scope.list.all.push($scope.instagram.shift());
			}

			//console.log($scope.list.all);
		}

		$scope.getClass = function (item, index) {
			var result = "";

			if (item.provider == "youtube") {
				result = "w380";
			} else if (item.provider == "facebook") {
				result = "w320";
			} else {
				result = "w214";
			}

			if (index % 3 != 2) {
				result += index % 9 > 2 ? " mr33" : " mr10";
			}
		
			result += " paging-" + Math.floor(index / 9);
			
			return result;
		}

		$scope.getPagingClass = function (index) {
			return "paging-" + Math.floor(index / 9);
		}

		$scope.goLink = function (item) {
			if (item.provider == "youtube") {
				window.open('http://www.youtube.com/watch?v=' + item.link);
			} else if (item.provider == "facebook") {
				window.open('https://www.facebook.com/compassion.Korea/posts/'+item.id);
			} else {
				window.open(item.link);
			}
		}

		$scope.loadMore = function () {
			if ($scope.page > 3 || $(".socialListWrap ." + $scope.tab + " .paging-" + (++$scope.page)).length < 1) {
				if ($scope.tab == "all" || $scope.tab == "facebook") {
					window.open("http://www.facebook.com/compassion.Korea");
				} else if ($scope.tab == "youtube") {
					window.open("http://www.youtube.com/compassionkr");
				} else if ($scope.tab == "instagram") {
					window.open("https://www.instagram.com/compassionkorea");
				}
				return;
			}

			$(".socialListWrap ." + $scope.tab + " .paging-" + ($scope.page), $element).show();

			//console.log($(".socialListWrap ." + $scope.tab + " li:hidden", $element).length);

			if ($(".socialListWrap ." + $scope.tab + " li:hidden", $element).length < 1) {
				//$(".social_more", $element).hide();
			}
		}

		$scope.changeTab = function (type) {
			$(".subContents .sns li", $element).removeClass("on");
			$(".subContents .sns .tab-" + type, $element).addClass("on");

			$(".socialListWrap li", $element).hide();
			$(".socialListWrap ." + type + " .paging-0", $element).show();

			$(".social_more", $element).show();

			$scope.tab = type;
			$scope.page = 0;
		}


		$scope.initData();
		

		$scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
			$(".socialListWrap ."+$scope.tab+" .paging-0", $element).show();
		});

	});

	app.directive("onFinishRender", function ($timeout) {
		return {
			restrict: 'A',
			scope: true,
			link: function (scope, element, attr) {
				if (scope.$last===true) {
					$timeout(function () {
						scope.$emit(attr.onFinishRender);
					})
				}
			}
		};
	});
})();


