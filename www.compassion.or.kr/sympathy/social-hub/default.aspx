﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="sympathy_social_hub_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="default.js"></script>
	<script type="text/javascript">
		$(function () {
						
			// 꿈을나누는 SNS : sns 버튼 스크롤 고정
			$(window).scroll(function () {

				var top = $(".tab_type1.sns").offset().top;
							
				if ($(this).scrollTop() > top) {
					$(".sns_link").addClass("fixed");
				}
				else {
					$(".sns_link").removeClass("fixed");
				}

			});

		})
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    

   <!-- sub body -->
	<section class="sub_body" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1><em>꿈을 나누는 SNS</em></h1>
                <span class="desc">컴패션의 다양한 소식을 지금 만나보세요!</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// 타이틀 -->

        <!-- s: sub contents -->
		<div class="subContents sympathy">
			<div class="w980 relative">

				<!-- tab menu -->
				<ul class="tab_type1 sns">
					<li style="width:25%" class="tab-all on"><a href="#" ng-click="changeTab('all')"><span class="all"></span>전체</a></li>
					<li style="width:25%" class="tab-facebook"><a href="#" ng-click="changeTab('facebook')"><span class="fa"></span>페이스북</a></li>
					<li style="width:25%" class="tab-youtube"><a href="#" ng-click="changeTab('youtube')"><span class="you"></span>유튜브</a></li>
					<li style="width:25%" class="tab-instagram"><a href="#" ng-click="changeTab('instagram')"><span class="insta"></span>인스타그램</a></li>
				</ul>
				<!--// tab menu -->

				<!-- sns 링크 -->
				<div class="sns_link">
					<div class="w980 relative">
						<ul class="btn">
							<li><a target="_blank" href="http://www.facebook.com/compassion.Korea"><span class="sns3 facebook"></span></a></li>
							<li><a target="_blank" href="http://www.youtube.com/compassionkr"><span class="sns3 youtube"></span></a></li>
							<li><a target="_blank" href="https://www.instagram.com/compassionkorea/" class="noline"><span class="sns3 insta"></span></a></li>
						</ul>
					</div>
				</div>
				<!--// -->

				<!-- 게시판 리스트 -->
				<div class="socialListWrap">

					<ul class="all">
						<li ng-class="getClass(item, $index)" style="background:url('{{item.picture}}') no-repeat;display:none" ng-repeat="item in list.all track by $index" on-finish-render="ngRepeatFinished">
							<span class="icon" ng-class="{true:'insta',false:item.provider}[item.provider == 'instagram']"></span>
							<!-- 마우스 오버 시 -->
							<a href="#" class="over" ng-click="goLink(item)">
								<p class="txt">{{item.title? item.title : item.content}}</p>
								<span class="btn_view1">VIEW</span>
							</a>
						</li>
					</ul>


                    <ul class="facebook">
						<li ng-class="getPagingClass($index)" style="background:url('{{item.picture}}') no-repeat;display:none" ng-repeat="item in list.facebook track by $index">
							<span class="icon facebook"></span>

							<!-- 마우스 오버 시 -->
							<a href="#" class="over" ng-click="goLink(item)">
								<p class="txt">{{item.title? item.title : item.content}}</p>
								<span class="btn_view1">VIEW</span>
							</a>
						</li>
                    </ul>

                    <ul class="youtube">
						<li ng-class="getPagingClass($index)" style="background:url('{{item.picture}}') no-repeat;display:none" ng-repeat="item in list.youtube track by $index">
							<span class="icon youtube"></span>

							<!-- 마우스 오버 시 -->
							<a href="#" class="over" ng-click="goLink(item)">
								<p class="txt">{{item.title? item.title : item.content}}</p>
								<span class="btn_view1">VIEW</span>
							</a>
						</li>
                    </ul>
                    
					<ul class="instagram insta">
						<li ng-class="getPagingClass($index)" style="background:url('{{item.picture}}') no-repeat;display:none" ng-repeat="item in list.instagram track by $index">
							<span class="icon insta"></span>

							<!-- 마우스 오버 시 -->
							<a href="#" class="over" ng-click="goLink(item)">
								<p class="txt">{{item.title? item.title : item.content}}</p>
								<span class="btn_view1">VIEW</span>
							</a>
						</li>
                    </ul>


					<!-- 더보기 -->
					<button type="button" class="social_more" ng-click="loadMore()"></button>

				</div>		
				<!--// 게시판 리스트 -->

				
			</div>
			<!--// w980 -->
		</div>	
		<!--// e: sub contents -->

		<div class="h100"></div>
		

    </section>
    <!--// sub body -->



    


    
    

    
</asp:Content>