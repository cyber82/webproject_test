﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class sympathy_view : FrontBasePage {
	// const string listPath = "/sympathy/child";


	protected override void OnBeforePostBack() {
		var s_type = Request["s_type"];

        string listPath = s_type;
        if(listPath == "vision" || listPath == "individual") {
            listPath = "/sympathy/vision/" + s_type;
        } else {
            listPath = "/sympathy/" + s_type;
        }

		base.OnBeforePostBack();


		// check param
		var requests = Request.GetFriendlyUrlSegments();
		if(requests.Count < 2) {
			Response.Redirect(listPath, true);
		}

		if(s_type.EmptyIfNull() == "") {
			s_type = requests[0];
		}

		hidden_s_type.Value = s_type;


		var isValid = new RequestValidator()
			.Add("p", RequestValidator.Type.Numeric)
			.Validate(this.Context, listPath);

		if(!requests[1].CheckNumeric()) {
			Response.Redirect(listPath, true);
		}

		base.PrimaryKey = requests[1];
		id.Value = PrimaryKey.ToString();


		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();


	}
	
	protected override void loadComplete( object sender, EventArgs e )
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.ssb_article.First(p => p.idx == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<ssb_article>("idx", Convert.ToInt32(PrimaryKey));
            title.Text = entity.title;
            sub_title.Text = entity.sub_title;
            reg_date.Text = entity.reg_date.ToString("yyyy.MM.dd");
            view_count.Text = entity.view_count.HasValue ? entity.view_count.Value.ToString("N0") : "0";

            article.Text = entity.body;

            var nc_depth1 = entity.sa_nc_depth1;
            var nc_depth2 = entity.sa_nc_depth2;
            var nc_text = entity.sa_nc_text.EmptyIfNull();

            if (nc_text == "")
            {
                //var nationEntity = dao.nation_code.FirstOrDefault(p => p.nc_depth1 == nc_depth1 && p.nc_depth2 == nc_depth2);
                var nationEntity = www6.selectQF<nation_code>("nc_depth1", nc_depth1, "nc_depth2", nc_depth2);
                if (nationEntity == null)
                {
                    phNation.Visible = false;
                }
                else
                {
                    nation.Text = nationEntity.nc_name;
                }
            }
            else
            {
                nation.Text = nc_text;
            }


            //bindPrevNext(dao);
            bindPrevNext();

            // SNS
            this.ViewState["meta_url"] = Request.UrlWithoutQueryString();
            this.ViewState["meta_title"] = string.Format("[한국컴패션-공감] {0}", entity.title);

            // optional(설정 안할 경우 주석)
            //this.ViewState["meta_description"] = "";
            //this.ViewState["meta_keyword"] = "";
            // meta_image 가 없는 경우 제거 (기본이미지로 노출됨,FrontBasePage)
            this.ViewState["meta_image"] = entity.thumb.WithFileServerHost();

        }
	}


    //이전 다음
    //void bindPrevNext( FrontDataContext dao )
    void bindPrevNext()
    {

		//string s_type = Request["s_type"].EmptyIfNull().EscapeSqlInjection();
		string s_type = hidden_s_type.Value;
		string k_type = Request["k_type"].ValueIfNull("both").EscapeSqlInjection();
		string k_word = Request["k_word"].EmptyIfNull().EscapeSqlInjection();
		string s_column = Request["s_column"].EmptyIfNull().EscapeSqlInjection();
		string depth1 = Request["depth1"].EmptyIfNull().EscapeSqlInjection();
		string depth2 = Request["depth2"].EmptyIfNull().EscapeSqlInjection();

        Object[] op1 = new Object[] { "idx", "board_id", "keyword_type", "keyword", "sort_column", "depth1", "depth2" };
        Object[] op2 = new Object[] { Convert.ToInt32(PrimaryKey), s_type, k_type, k_word, s_column, depth1, depth2 };
        var list = www6.selectSP("sp_sympathy_prevNext_f", op1, op2).DataTableToList<sp_sympathy_prevNext_fResult>();

        //sp_sympathy_prevNext_fResult result = dao.sp_sympathy_prevNext_f(Convert.ToInt32(PrimaryKey), s_type, k_type, k_word, s_column, depth1, depth2).First();
        sp_sympathy_prevNext_fResult result = list.First();

        if (result.prev_idx.HasValue) {
			var href = string.Format("view/" + s_type + "/" + "{0}?{1}#c", result.prev_idx.ToString(), this.ViewState["q"].ToString());
			btnPrev.HRef = href;
			prevTitle.Text = result.prev_title;
		} else {
			liPrev.Visible = false;
		}

		if(result.next_idx.HasValue) {
			var href = string.Format("view/" + s_type + "/" + "{0}?{1}#c", result.next_idx.ToString(), this.ViewState["q"].ToString());
			btnNext.HRef = href;
			nextTitle.Text = result.next_title;
		} else {
			liNext.Visible = false;
		}

	}


}