﻿(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, popup, paramService) {

        $scope.requesting = false;
        $scope.total = -1;
        $scope.rowsPerPage = 9;

        $scope.list = [];
        $scope.nationList = [];

        $scope.params = {
            page: 1,
            rowsPerPage: $scope.rowsPerPage,
            s_type: $("#s_type").val(),
            s_column: 'reg_date'
        };

        // 파라미터 초기화
        $scope.params = $.extend($scope.params, paramService.getParameterValues());
        if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);

        // 검색
        $scope.search = function (params) {
            $scope.params = $.extend($scope.params, params);
            $scope.params.page = 1;
            $scope.params.k_word = $("#k_word").val();

            $scope.getList();
            //console.log("검색", $scope.getList());
        }

        // 국가별로 보기
        $scope.getNationList = function () {
            var nationJson = $("#nation_value").val();
            if (nationJson && nationJson != "") {
                //$scope.nationList = $.parseJSON(nationJson);
                var data = $.parseJSON(nationJson);
                //console.log("data" , data);
                var regions = $.grep(data, function (r) {
                    return r.nc_depth2 == "";
                });

                $.each(regions, function () {

                    var depth1 = this.nc_depth1;

                    var countries = $.grep(data, function (r) {
                        return r.nc_depth1 == depth1 && r.nc_depth2 != ""
                    });

                    //console.log(countries);

                    this.countries = countries;

                })

                $scope.nationList = regions;
                //console.log("regions" , regions);
                //console.log(data);
            }
        }


        // list
        $scope.getList = function (params) {

            $scope.params = $.extend($scope.params, params);
            //console.log("getList",$scope.params);
            $http.get("/api/sympathy.ashx?t=list", { params: $scope.params }).success(function (result) {
                $scope.list = result.data;
                $scope.total = result.data.length > 0 ? result.data[0].total : 0;
                $.each($scope.list, function () {
                    console.log(this.depth2_name == null && this.sa_nc_text != "")
                    if (this.depth2_name == null && this.sa_nc_text != "") {
                        this.depth2_name = this.sa_nc_text;
                    }

                });

                $(".angular_area").show();
            });

            if (params)
                scrollTo($("#l"), 10);
        }

        // 상세페이지
        $scope.goView = function (idx) {
            //$http.post("/api/sympathy.ashx?t=hits&idx=" + idx).then().finally(function () {
            //    location.href = "/sympathy/view/" + $scope.params.s_type + "/" + idx + "?" + $.param($scope.params);
            //});

            location.href = "/sympathy/view/" + $scope.params.s_type + "/" + idx + "?" + $.param($scope.params);
        }

        // 정렬
        $scope.sort = function (s_column) {
            $scope.params.s_column = s_column;
            $scope.params.page = 1;
            $scope.getList();
            // console.log('최신', $scope.sort);
        }

        $scope.getNationList();

        $scope.getList();

        // 블루북 신청
        $scope.modal = {
            instance: null,

            init: function () {
                // 팝업
                popup.init($scope, "/sympathy/bluebook-apply", function (modal) {
                    $scope.modal.instance = modal;
                });

            },

            show: function () {
                if ($("#sponsor_id").val() == "N") {
                    alert("블루북은 기존 후원자만 신청 가능합니다.");
                    return false;
                }

                if ($("#register_book").val() == "N") {
                    alert("이미 신청하셨습니다.");
                    return false;
                }
                if (!$scope.modal.instance)
                    return;

                if (common.checkLogin()) {

                    if ($("#locationType").val() != "국내") {
                        alert("국내거주 회원만 신청가능합니다.");
                        return;
                    }

                    $scope.modal.instance.show();

                    // 컴파스의 데이타를 불러오는경우 
                    if ($("#dspAddrDoro").val() != "") {
                        $("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
                        if ($("#dspAddrJibun").val() != "") {
                            $("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
                        }

                    } else if ($("#addr1").val() != "") {
                        addr_array = $("#addr1").val().split("//");
                        if (addr_array[0] != "") {
                            $("#addr_road").text("[도로명주소] " + addr_array[0] + " " + $("#addr2").val());
                        }
                        if (addr_array[1]) {
                            $("#addr_jibun").text("[지번주소] " + addr_array[1] + " " + $("#addr2").val());
                        }
                    }
                }

            },

            request: function ($event) {

                if (!validateForm([
					{ id: "#name", msg: "이름을 입력하세요" },
					{ id: "#phone", msg: "휴대폰번호를 입력하세요", type: "phone" },
					{ id: "#zipcode", msg: "우편번호를 입력하세요" },
					{ id: "#addr1", msg: "주소를 입력하세요" },
					{ id: "#addr2", msg: "주소를 입력하세요" }
                ])) {
                    return;
                }

                if ($scope.requesting) return;
                $scope.requesting = true;


                var name = $("#name").val();
                var phone = $("#phone").val();
                var zipcode = $("#zipcode").val();
                var addr1 = $("#addr1").val();
                var addr2 = $("#addr2").val();
                var addressType = $("#addressType").val();

                $http.post("/api/sympathy.ashx?t=apply", { name: name, phone: phone, zipcode: zipcode, addr1: addr1, addr2: addr2, addressType: addressType }).success(function (r) {
                    $scope.requesting = false;
                    if (r.success) {
                        alert("신청이 완료되었습니다.\n후원자님의 귀한 섬김에 언제나 감사드립니다.");
                    } else {
                        alert(r.message);
                    }
                    $scope.modal.close($event);

                });
            },

            close: function ($event) {

                if (!$scope.modal.instance)
                    return;

                $scope.modal.instance.hide();
                $event.preventDefault();
            },

            popup: function ($event) {
                cert_setDomain();
                var pop = window.open("/common/popup/addressApi?callback=jusoCallback", "pop", "width=570,height=420, scrollbars=yes, resizable=yes");

                $event.preventDefault();
            }

        }
        $scope.modal.init();

        // 영상 팝업
        $scope.modalVideo = {
            instance: null,

            show: function () {
                popup.init($scope, "/sympathy/bluebook-video", function (modalVideo) {
                    $scope.modal.instance = modalVideo;
                    modalVideo.show();
                }, { removeWhenClose: true });

            },

            close: function ($event) {
                $event.preventDefault();
                if (!$scope.modalVideo.instance)
                    return;
                // 영상재상 초기화
                $("#bluebook_movie").attr("src", $("#bluebook_movie").attr("src"));
                $scope.modalVideo.instance.hide();

            },
        }
        //$scope.modalVideo.init();

        // 블루북이란 팝업
        $scope.modalBluebookIs = {
            instance: null,
            init: function () {
                // 팝업

                popup.init($scope, "/sympathy/bluebook-is", function (modalBluebookIs) {
                    $scope.modalBluebookIs.instance = modalBluebookIs;

                }, { top: 0, iscroll: true });

            },

            show: function () {

                if (!$scope.modalBluebookIs.instance)
                    return;
                $scope.modalBluebookIs.instance.show();

            },

            close: function ($event) {
                $event.preventDefault();
                if (!$scope.modalBluebookIs.instance)
                    return;
                $scope.modalBluebookIs.instance.hide();

            },

            showBluebook: function ($event) {
                $event.preventDefault();
                common.checkLogin();
                if ($("#sponsor_id").val() == "N") {
                    alert("블루북은 기존 후원자만 신청 가능합니다.");
                    return false;
                }

                if ($("#register_book").val() == "N") {
                    alert("이미 신청하셨습니다.");
                    return false;
                }

                if (!$scope.modalBluebookIs.instance)
                    return;

                $scope.modalBluebookIs.instance.hide();
                $scope.modal.instance.show();

            }


        }

        $scope.parseDate = function (datetime) {
            return new Date(datetime);
        }

        $scope.modalBluebookIs.init();






        $scope.modalMessage = {
            instance: null,
            init: function () {
                // 팝업

                popup.init($scope, "/sympathy/bluebook-message", function (modalMessage) {
                    $scope.modalMessage.instance = modalMessage;
                }, { top: 0, iscroll: true });


            },

            show: function () {

                if (!$scope.modalMessage.instance)
                    return;
                $scope.modalMessage.instance.show();

            },

            close: function ($event) {
                $event.preventDefault();
                if (!$scope.modalMessage.instance)
                    return;
                $scope.modalMessage.instance.hide();

            },

            showMessage: function ($event) {
                $event.preventDefault();

                if (!$scope.modalMessage.instance)
                    return;

                $scope.modalMessage.instance.hide();
                $scope.modal.instance.show();

            },

            goLink: function ($event) {
                $event.preventDefault();

                location.href = "/sympathy/view/bluebook/2550?page=1&rowsPerPage=9&s_type=bluebook&s_column=reg_date";
            }


        }

        $scope.modalMessage.init();
    });

})();