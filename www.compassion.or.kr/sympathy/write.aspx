﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="write.aspx.cs" Inherits="sympathy_write" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/common/smartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
    <script type="text/javascript">
        var image_path = "";

        var getImagePath = function () {
            return image_path;
        }

        var oEditors = [];
        // 추가 글꼴 목록
        //var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];

        var initEditor = function (ref, holder) {
            nhn.husky.EZCreator.createInIFrame({
                oAppRef: ref,
                elPlaceHolder: holder,
                sSkinURI: "/common/smartEditor/SmartEditor2Skin.html",
                htParams: {
                    bUseToolbar: true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
                    bUseVerticalResizer: true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
                    bUseModeChanger: true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
                    //aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
                    fOnBeforeUnload: function () {
                        //alert("완료!");
                    }
                }, //boolean
                fOnAppLoad: function () {
                    //oEditors.getById["content"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
                },
                fCreator: "createSEditor2"
            });
        }


        $(function () {
            image_path = "<%:Uploader.GetRoot(Uploader.FileGroup.image_board)%>";
		    initEditor(oEditors, "body");

		    $("#phone").mobileFormat();

		});


        function onSubmit() {

            if ($("#essay_type input:checked").length < 1) {
                alert("주제를 선택해주세요.");
                $("#essay_type input").focus();
                return false;
            }


            if (!validateForm([
				{ id: "#b_title", msg: "제목을 입력해주세요" },
                { id: "#name", msg: "이름을 입력하세요" },
                { id: "#email", msg: "이메일을 입력해주세요" , type : "email" },
                { id: "#phone", msg: "연락처를 입력해주세요" , type : "numeric" }
            ])) {
                return false;
            }

            oEditors.getById["body"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용됩니다.
            
            if ($("#body").val() == "<p>&nbsp;</p>") {
                alert("내용을 입력해 주세요")
                oEditors.getById["body"].exec('FOCUS', []);
                return false;
            }

            if (!/(01[016789])[-](\d{4}|\d{3})[-]\d{4}$/g.test($("#phone").val())) {
                alert("올바른 휴대폰 형식이 아닙니다.");
                $("#phone").focus();
                return false;
            }

            return true;
        }

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <!-- sub body -->
    <section class="sub_body">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>함께 쓰는 <em>에세이</em></h1>
                <span class="desc">후원자님과 함께 만들어 가는 컴패션 이야기</span>

                <uc:breadcrumb runat="server"/>
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents sympathy">
            <div class="w980">

                <!-- 에세이쓰기 테이블 -->
                <div class="tableWrap1">
                    <table class="tbl_type1 mb40">
                        <caption>에세이 쓰기 테이블</caption>
                        <colgroup>
                            <col style="width: 18%" />
                            <col style="width: 82%" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <label for="subject">제목</label></th>
                                <td>
                                    <asp:TextBox runat="server" ID="b_title" class="input_type2" Style="width: 77%" />
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">주제</th>
                                <td>

                                    <asp:RadioButtonList CssClass="radio_ui" RepeatDirection="Horizontal" RepeatLayout="Flow" runat="server" ID="essay_type"></asp:RadioButtonList>
                                    <script type="text/javascript">
                                        $("#essay_type input").addClass("css_radio");
                                        $("#essay_type label").addClass("css_label mr30");
                                    </script>
                                </td>
                            </tr>
                            <tr class="padding3">
                                <td colspan="2">
                                    <textarea name="body" id="body" runat="server" style="width: 975px; height: 350px; display: none;"></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--// 에세이쓰기 테이블 -->

                <div class="essay_guide">
                    <div class="information">
                        <fieldset>
                            <legend>개인정보입력</legend>
                            <div class="info">
                                <label for="name">이름</label>
                                <asp:TextBox runat="server" ID="name" class="input_type1"></asp:TextBox>

                            </div>
                            <span class="bar"></span>
                            <div class="info">
                                <label for="email">이메일</label>
                                <asp:TextBox runat="server" ID="email" class="input_type1"></asp:TextBox>
                            </div>
                            <span class="bar"></span>
                            <div class="info">
                                <label for="phone">휴대폰</label>
                                <asp:TextBox runat="server" ID="phone" class="input_type1"></asp:TextBox>
                            </div>
                        </fieldset>
                    </div>
                    <div class="txt">
                        <p>
                            작성해 주신 글은 잘 다듬어 홈페이지에 게시할 예정입니다. 풍부한 이야기 표현을 위해 사진 자료 등을 추가 요청드릴 수 있으니<br />
                            위의 <em>연락처가 틀리지 않은지 한 번 더 확인 후 등록해 주세요.</em>
                        </p>
                    </div>
                </div>
                <!--// essay_guide -->

                <div class="btn_ac">
                    <div>
                        <a href="#" class="btn_type2 fl mr10" title="취소" runat="server" id="btnList">취소</a>
                        <asp:LinkButton runat="server" ID="btnAdd" OnClick="btnAdd_Click" OnClientClick="return onSubmit()" class="btn_type1 fl">확인</asp:LinkButton>
                    </div>
                </div>

            </div>
            <!--// w980 -->
        </div>
        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>
    <!--// sub body -->





</asp:Content>
