﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="bluebook-apply.aspx.cs" Inherits="bluebook_apply"  %>

<div style="background:transparent;width:400px;height:600px">
	
		<!-- 일반팝업 width : 800 -->
	<div class="pop_type1 w400">
	<input type="hidden" runat="server" id="addressType" value="" />
	<input type="hidden" runat="server" id="locationType" value="" />
	<input type="hidden" id="addr1" class="addr1" runat="server" />
	<input type="hidden" id="addr2" class="addr2" runat="server" />
	<input type="hidden" runat="server" id="dspAddrJibun" value="" />
	<input type="hidden" runat="server" id="dspAddrDoro" value="" />

		<div class="pop_title">
			<span>블루북 신청</span>
			<button class="pop_close"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" class="btnClose" ng-click="modal.close($event)"/></span></button>
		</div>

		<div class="pop_content bookApply">
			
			<table class="tbl_type1 mb30">
				<caption>블루북 신청 테이블</caption>
				<colgroup>
					<col style="width:22%" />
					<col style="width:78%" />
				</colgroup>
				<tbody>
					<tr>
						<th><label for="name">이름</label></th>
						<td><input type="text" runat="server" name="name" id="name" class="input_type2" /></td>
                       
					</tr>
					<tr>
						<th><label for="mobile">휴대폰</label></th>
						<td><input type="text" runat="server" name="phone" id="phone" class="input_type2 number_only" maxlength="11"/></td>
					</tr>
					<tr>

						<th><label for="address">주소</label></th>
						<td>
							
							<input type="text" runat="server" name="zipcode" id="zipcode" class="input_type2 zipcode" style="width:172px"/>

                            <button class="btn_s_type2 ml10" runat="server" id="popup" ng-click="modal.popup($event)" >주소찾기</button>
							
							<p id="addr_road" class="fs14 mt15"></p>
							<p id="addr_jibun" class="fs14 mt10"></p>

						</td>
					</tr>
				</tbody>
			</table>

			<div class="tac">
				<p class="fs15 fc_gray mb30">배송받으실 주소와 연락처가 정확한지 다시 한 번<br />확인 부탁드립니다. 신청하시겠습니까?</p>
				<a href="javascript:;" ng-click="modal.request($event)" class="btn_type1">확인</a><br />
			</div>

		</div>
	</div>
	<!--// popup -->
</div>