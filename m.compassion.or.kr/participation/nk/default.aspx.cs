﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class participation_nk_default : MobileFrontBasePage {
	
	protected override void OnBeforePostBack() {
        btn_pc.HRef = ConfigurationManager.AppSettings["domain_file"] + "/participation/nk/";

    }

}