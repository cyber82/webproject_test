﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="participation_nk_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

    <script type="text/javascript">
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
  
<div class="wrap-sectionsub">
<!---->
    
    <div class="participation-north">
    	
        <div class="bg-child sectionsub-margin1">
            <div class="youtube">
                <iframe src="https://www.youtube.com/embed/VbYXXtAUm6Q?rel=0&amp;showinfo=0wmode=transparent" frameborder="0" allowfullscreen></iframe>
            </div>
            <p class="txt-sub">컴패션의 북한사역은, 교회와 함께 가난한 어린이에게 전인적인 양육을 제공하는 것이 허용되는 때를 위하여 미리 준비하는 사역으로, 비전을 함께하는 교회와 합력하여 북한어린이를 위한 전인적양육 프로그램과 운영 시스템, 전문역량을 개발하고 기도로 마음을 모읍니다.</p>
		</div> 
        
        <div class="box-source">
        	너희는 세상의 빛이라…
            이같이 너희 빛이 사람 앞에 비치게 하여 그들로 너희 착한 행실을 보고 하늘에 계신 너희 아버지께 영광을 돌리게 하라
			<p class="txt">(마태복음 5:14, 16)</p>            
        </div>
        
        <p class="txt-caption">북한어린이를 위한<br />
			소망의 여정으로의 초대</p>

		<div class="sectionsub-margin1"><img src="/common/img/page/participation/img-north.jpg" alt="" width="100%" />
        	<span class="invisible">
            교회-컴패션  북한사역 파트너십<br />
			샬롬기도운동<br />
            컴패션 사역훈련<br />
            북한사역 후원
            </span>
        </div>
        
		 <div class="box-tel">
        	<p class="txt-title">문의</p>
            <a class="tel" href="tel:02-3668-3535"><strong>전화</strong> 02)3668-3535</a>
            <a class="email" href="mailto:nkhope@compassion.or.kr"><strong>이메일</strong> nkhope@compassion.or.kr</a>
        </div>

		<div class="box-linkpc">
        	<span class="link-pc"><a href="#" runat="server" id="btn_pc">PC 버전 바로가기</a></span><br />
            더욱 자세한 내용은 PC 버전에서 확인하실 수 있습니다.
        </div>
        
        
    </div>
    
<!--//-->
</div>

</asp:Content>