﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="event.aspx.cs" Inherits="participation_event_event" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/moment/moment.min.js"></script>
    <script type="text/javascript" src="/assets/moment/locales.min.js"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <script type="text/javascript" src="/participation/event/view.js?v=1.5"></script>
    <script type="text/javascript">
        $(function () {
            $event.init();
        });

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" id="id" runat="server" />

    <input type="hidden" id="scheduleData" runat="server" />
    <input type="hidden" id="eventEntity" runat="server" />
    <input type="hidden" id="applyData" runat="server" />


    <input type="hidden" id="count" runat="server" />
    <input type="hidden" id="route" runat="server" />
    <input type="hidden" id="date" runat="server" />
    <input type="hidden" id="location" runat="server" />
    <input type="hidden" id="action" runat="server" />
    <input type="hidden" id="target" runat="server" />
    <input type="hidden" id="er_request_contents" runat="server" />
    
    <input type="hidden" runat="server" id="picked_date" />
    <input type="hidden" id="e_maximum" runat="server" />
    <input type="hidden" id="isSponsor" runat="server" value="true"/>

    <input type="hidden" id="titleHidden" runat="server" />

    <div class="wrap-sectionsub">
        <!---->

        <div class="participation-campaign fn_pop_content">

			<div class="apply">
				<div class="step-flow content-1">
					<ol>
						<li class="recent"><span class="bg"></span><em>STEP 01</em>정보 입력</li>
						<li><span class="bg"></span><em>STEP 02</em>신청 완료</li>
					</ol>
				</div>

				<div class="step-flow content content-2" style="display: none;">
					<ol>
						<li><span class="bg"></span><em>STEP 01</em>정보 입력</li>
						<li class="recent"><span class="bg"></span><em>STEP 02</em>신청 완료</li>
					</ol>
				</div>
			</div>

            <fieldset class="frm-input frm-input-topline content-1">
                <legend>체험전 예약정보 입력</legend>
                <div class="row-table2">
                    <div class="col-th" style="width: 34%">이름</div>
                    <div class="col-td" style="width: 66%">
                        <input type="text" maxlength="20" id="name" runat="server" class="input_type2 name" value="" style="width: 100%" />
                    </div>
                </div>
                <div class="row-table2">
                    <div class="col-th" style="width: 34%">휴대폰</div>
                    <div class="col-td" style="width: 66%">
                        <input type="text" runat="server" id="phone" class="phone number_only" maxlength="13" style="width: 100%" />
                    </div>
                </div>
                <div class="row-table2 count_section" style="display:none;">
                    <div class="col-th" style="width: 34%">
                        <span class="text2row">신청 인원<br />
                            (본인 포함)</span>
                    </div>
                    <div class="col-td" style="width: 66%">
                        <span class="count">
                            <a class="count-minus-event minus" href="#">수량 빼기</a>
                            <input type="number" id="number" class="input_type3 fl" value="1" style="width:50px;"/>
                            <a class="count-plus-event pluse" href="#">수량 더하기</a>
                            &nbsp;&nbsp;&nbsp;
                            <span style="font-size: 13px;vertical-align: middle;line-height: 30px;margin-left:30px;">최대 <span id="maximun_request"></span>명</span>
                        </span>
                    </div>
                </div>
                <div class="row-table2 date_section" style="display: none;">
                    <div class="col-th" style="width: 34%">신청날짜</div>
                    <div class="col-td" style="width: 66%">
                        <select class="date" style="width: 100%" name="" id="sel_1"></select>
                    </div>
                </div>
                <div class="row-table2" style="display: none;">
                    <div class="col-th" style="width: 34%">
                        <span class="text2row">컴패션 이벤트를<br />
                            알게 된 경로</span>
                    </div>
                    <div class="col-td" style="width: 66%">
                        <select style="width: 100%" class="route" name="route" id="sel_2">
                            <option value="">선택해 주세요</option>
                            <option value="교회">교회</option>
                            <option value="이메일(뉴스레터)">이메일(뉴스레터)</option>
                            <option value="우편(DM)">우편(DM)</option>
                            <option value="SNS">SNS</option>
                            <option value="문자(SMS)">문자(SMS)</option>
                            <option value="컴패션 홈페이지">컴패션 홈페이지</option>
                            <option value="컴패션 모바일앱">컴패션 모바일앱</option>
                            <option value="지인 소개">지인 소개</option>
                            <option value="신문/방송">신문/방송</option>
                            <option value="잡지">잡지</option>
                        </select>
                    </div>
                </div>
                <div class="row-table2 request-contents" style="display:none;">
                    <div class="col-th" style="width: 34%">신청 내용<br />(최대 200자)</div>
                    <div class="col-td" style="width: 66%">
                        <textarea Rows="2" maxlength="200" class="textarea_type2 request-contents" runat="server" ID="request_contents" placeholder="신청 내용을 입력해 주세요" style="width:100%;"></textarea>
                    </div>
                </div>
            </fieldset>

            <p class="txt-sub align-center mt20 content content-1">
                위 정보로 캠페인/이벤트 신청이 진행되오니,<br />
                입력하신 정보가 정확한지 다시 한 번 확인 부탁드립니다.
            </p>

            <div class="wrap-bt btnAdd content content-1" style="display: none">
                <a class="bt-type6" style="width: 100%;" onclick="$event.apply()">확인</a>
            </div>

            <!--정보 변경으로 진입 시-->
            <div class="wrap-bt btnEdit content content-1" style="display: none">
                <a class="bt-type6 " style="width: 100%;" onclick="$event.apply()">정보 변경하기</a>
            </div>

            <!-- step2 -->
            <div class="box2-gray color2 content-2" style="display: none;" id="is_period">
                <span class="period"></span><br />
                <span class="location"></span>
            </div>

            <p class="txt-sub align-center mt20 content content-2" style="display: none;">
                <span class="color2 finish_name"></span>님 포함 총 
                <span class="color2">
                    <span class="finish_count"></span>명의
                </span>
                
                <br />
                신청이 완료되었습니다.<br />
                전체 캠페인/이벤트 참여 내역은<br />
                <a href="/my/activity/event/">마이컴패션</a>에서 확인하실 수 있습니다.<br />
                감사합니다.
            </p>

            <div class="wrap-bt content content-2" style="display: none;">
                <a class="bt-type6" style="width: 100%" href="#" id="btn_confirm">확인</a>
            </div>

            <div class="linebar content-2" style="display: none;"></div>
            <p class="txt-sub align-center content-2" style="display: none;">지인들에게 컴패션 이벤트를 알려 주세요!</p>

            <div class="share-sns content-2" style="display: none; margin: 20px 0">

                <a class="sns_control"  ng-click="sns.show($event)">
                    <img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
            </div>


        </div>

        <!--//-->
    </div>









</asp:Content>
