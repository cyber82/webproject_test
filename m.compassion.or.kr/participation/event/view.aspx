﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="participation_event_view" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/moment/moment.min.js"></script>
    <script type="text/javascript" src="/assets/moment/locales.min.js"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <script type="text/javascript" src="/participation/event/view.js?v=2.9"></script>
    <script type="text/javascript">

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" id="e_show_child" runat="server" value="N" /><!-- 1:1어린이후원 사용여부 -->
    <input type="hidden" id="e_show_program" runat="server" value="N" /><!-- 특별한 후원 사용여부 -->
    <input type="hidden" id="campaignId" runat="server" /><!-- 특별한 후원 아이디 -->
    <input type="hidden" id="e_sf_prices" runat="server" /><!-- 특별한 후원 금액 -->

    <input type="hidden" id="id" runat="server" />
    <input type="hidden" id="eventEntity" runat="server" />
    <input type="hidden" id="scheduleData" runat="server" />
    <input type="hidden" id="applyData" runat="server" />

    <input type="hidden" id="name" runat="server" />
    <input type="hidden" id="phone" runat="server" />
    <input type="hidden" id="count" runat="server" />
    <input type="hidden" id="route" runat="server" />
    <input type="hidden" id="date" runat="server" />
    <input type="hidden" id="location" runat="server" />
    <input type="hidden" id="action" runat="server" />

    <input type="hidden" id="isWin" runat="server" />
    <input type="hidden" id="er_request_contents" runat="server" />

    
    <input type="hidden" id="isSponsor" runat="server" value="true"/>
    <input type="hidden" runat="server" id="picked_date" />

    <input type="hidden" id="titleHidden" runat="server" />
    <input type="hidden" id="e_confirm_display" runat="server" />

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl" id="defaultCtrl">
        <!---->

        <div class="participation-campaign">

            <div class="wrap-board">

                <div class="view-board">
                    <div class="txt-title sectionsub-margin1">
                        <span class="txt-blue" ng-if="target == 'sponsor'">후원자 전용</span>
                        <asp:Literal runat="server" ID="title" />
                    </div>
                    <div class="sympathy-info">
                        <p class="txt3">
                            <asp:PlaceHolder runat="server" ID="phJoinDate" Visible="false">
							    <span><asp:Literal runat="server" ID="joinDate" /><br /></span>
                            </asp:PlaceHolder>
                            <span runat="server" id="announce_date">당첨자 발표 : <asp:Literal runat="server" ID="announce"></asp:Literal></span>
                        </p>
                    </div>

                    <div class="editor-html">
                        <!--당첨자발표-->
                        <div class="winner-report" runat="server" id="winner_div" visible="false">
                            <p class="txt-caption" style="display:<%=ViewState["winnerTitle"]%>">당첨자 발표</p>
                            <span class="list-winner">
                                <asp:PlaceHolder runat="server" ID="winner_excel" Visible="false">
                                    <ul>
                                        <asp:Repeater runat="server" ID="repeater_winner">
                                            <ItemTemplate>
                                                <li><%#Eval("ew_user_id") %></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="winner_editor" Visible="false">
                                    <asp:Literal runat="server" ID="lbWinner"></asp:Literal>
                                </asp:PlaceHolder>
                            </span>
                        </div>
                        <!--//당첨자발표-->

                        <asp:Literal runat="server" ID="article" />
                    </div>

                    <div class="share-sns">
                        <a href="#" ng-click="sns.show($event)">
                            <img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
                    </div>


                </div>

            </div>

            <!--어린이 후원 기능 설정 시-->
            <div class="box-gray" id="pn_child" style="display: none">
                <p class="txt-title">후원자님을 기다리는 어린이의 손을 잡아 주세요</p>
                <div class="thumbil-child">
                    <div class="pos-photo">
						<span id="c_img" class="photo" style="background:url('/common/img/temp/temp1.jpg') no-repeat"></span>
						<em class="txt-day"><span id="c_waitingdays"></span></em>
                    </div>
                    <div class="info">
                        <p class="txt-name" id="c_name">비쉬노비쉬노</p>
                        <p><span class="color1">국가 : </span><span id="c_country"></span></p>
                        <p><span class="color1">생일 : </span><span id="c_birthdate"></span> (<span id="c_age"></span>)</p>
                        <p><span class="color1">성별 : </span><span id="c_gender"></span></p>
                        <!--
                        <a class="bt-type6 btn_sponsor_checkout" style="width: 80px" data-type="CDSP" data-frequency="정기" data-group="" data-codeid="" data-codename="" data-childmasterid="" data-campaignid="" data-relation_key="">더 알아보기</a>
                        -->
                        <a class="bt-type6" id="show_child" style="width: 80px" data-childmasterid="" ng-click="showChildPop($event)">더 알아보기</a>
                    </div>
                </div>
            </div>

            <!--캠페인 후원 기능 설정 시-->
            <div class="box-gray" id="pn_special_funding" style="display: none">
                <p class="txt-title" id="sf_title"></p>
                <fieldset class="frm-input">
                    <legend>후원정보 입력</legend>
                    <div class="row-table">
                        <div class="col-th" style="width: 34%">후원 유형</div>
                        <div class="col-td" style="width: 66%"><span class="txt">일시 후원</span></div>
                    </div>
                    <div class="row-table">
                        <div class="col-th" style="width: 34%">후원금 입력</div>
                        <div class="col-td" style="width: 66%">
                            <span class="row">
                                <select style="width: 100%" id="special_funding_price_opt"></select>

                            </span>
                            <span class="row" id="direct_input">
                                <input type="text" class="number_only" id="special_funding_price" style="width: 100%" placeholder="금액을 입력해 주세요" /></span>
                            <!--직접입력시 경우만 display-->

                            <span class="row">
                                <p class="txt-error" id="msg_funding" style="display:none"></p>
                            </span>
                        </div>
                    </div>
                </fieldset>
                <div class="linebar"></div>
                <div class="wrap-bt">
                    <a style="width: 76px" class="bt-type6 btn_sponsor_checkout" data-type="special" data-frequency="일시" data-group="" data-codeid="" data-codename="" data-childmasterid="" data-campaignid="" data-relation_key="">후원하기</a>
                </div>
            </div>


            <!--//////-->



            <div class="linebar"></div>
            <!--위 기능설정 표시시 해당 라인 안나옴,  하단 버튼 나올때 나오는 라인 css입니다.-->

            <div class="wrap-bt">
                <a id="btnApply" runat="server" style="width: 100%; display:none;" class="bt-type6" ng-click="book.goApply()">예약</a>
                <a id="btnUpdate" runat="server" style="width: 49%; display:none;" class="bt-type6 fl" ng-click="book.goApply()">예약 정보 변경</a>
                <a id="btnCancel" runat="server" style="width: 49%; display:none;" class="bt-type8 fr" onclick="$exper.cancel()">예약 취소</a>
                <a id="btnconfirm"               style="width: 100%; display:none; background-color :#C20000;" class="bt-type6 mt10" ng-click="confirmClick()">신청 확정</a>
                <a title="목록" runat="server" id="btnList" style="width: 100%" class="bt-type5 mt10">목록</a>
            </div>
        </div>

        <!--//-->
    </div>


</asp:Content>

