﻿(function () {

    var app = angular.module('cps.page', []);
    var first = true;
    app.controller("defaultCtrl", function ($scope, $http, popup, paramService) {

        $eventEntity = $.parseJSON($("#eventEntity").val());

        $scope.target = $("#target").val();

        // 일정 팝업
        $scope.book = {
            init: function () {
                // 체험전
                if ($eventEntity.e_type == "experience") {
                    $exper.init();
                } else if ($eventEntity.e_type != "general") {
                    $event.init();
                }
            },
            goApply: function () {

                if (!common.checkLogin()) {
                    return;
                }


                if ($("#isSponsor").val() == "false") {
                    alert("후원자만 신청 가능한 이벤트입니다.");
                    return false;
                }


                if ($eventEntity.close == "True") {
                    alert("지원이 마감되었습니다.");
                    return false;
                }

                if ($eventEntity.e_type == "experience") {
                    location.href = "/participation/event/experience/" + $("#id").val();


                } else if ($eventEntity.e_type != "general") {
                    if (!common.checkLogin()) {
                        return;
                    }
                    location.href = "/participation/event/event/" + $("#id").val();

                }
            }

        }
        $scope.book.init();

        //어린이 팝업
        $scope.showChildPop = function ($event) {
            loading.show();

            $.get("/api/tcpt.ashx?t=get", { childMasterId: $("#show_child").data("childmasterid") }).success(function (r) {
                //console.log(r);
                if (r.success) {
                    var item = r.data;
                    item.birthdate = new Date(item.birthdate);

                    if ($event) $event.preventDefault();
                    popup.init($scope, "/common/child/pop/" + item.countrycode + "/" + item.childmasterid + "/" + item.childkey, function (modal) {

                        modal.show();

                        initChildPop($http, $scope, modal, item);


                        if (!first) {
                            var center = map.getCenter();
                            google.maps.event.trigger(map, "resize");
                            map.setCenter(center);
                        }
                        first = false;

                    }, { top: 0, iscroll: true, removeWhenClose: true });
                }
            });
        }

        // 참석 확정 클릭 이벤트 추가 문희원 2017-06-28
        $scope.confirmClick = function () {
            if (confirm("신청을 확정하시겠습니까? ")) {

                var param = {
                    id: $("#id").val(),
                };

                $.post("/api/event.ashx?t=confirm", param, function (r) {
                    if (r.success) {
                        $("#btnCancel").hide();
                        $("#btnCancel2").hide();
                        $(".btnEdit").hide();
                        $("#btnUpdate").hide();
                        $("#btnUpdate2").hide();
                        $("#btnconfirm").hide();
                        $("#btnconfirm2").hide();

                        alert(r.message + "홈페이지로 이동합니다.");

                        var host = '';
                        var loc = String(location.href);
                        if (loc.indexOf('compassion.or.kr') != -1) host = 'm.compassion.or.kr';
                        else if (loc.indexOf('compassionkr.com') != -1) host = 'm.compassionkr.com';
                        else if (loc.indexOf('localhost') != -1) host = 'localhost:35563';
                        if (host != '') location.href = 'http://' + host;
                    } else {
                        alert(r.message);
                    }
                });

            }
        }

    });

})();

var $eventEntity = {};

$(function () {
    $eventEntity = $.parseJSON($("#eventEntity").val());
    $page.init();

});

// 시사회
$exper = {
    type: "",
    scheduleData: {},
    scheduleArray: [],
    applyData: [],
    page: 1,
    rows: 7,
    hasNext: false,
    open: "",
    close: "",
    template: null,
    info: {
        date: null,
        name: null,
        phone: null,
        count: null,
        route: null,
        applycount: null
    },
    action: null,

    init: function () {

        moment.locale('ko');
        $exper.info.date = $("#date").val();


        if ($("#applyData").val() != "") {
            $exper.applyData = $.parseJSON($("#applyData").val());
        }

        // 템플릿
        $exper.template = $(".fn_pop_content").find(".schedule").clone();
        $(".fn_pop_content").find(".schedule").remove();

        if ($("#winner_div")) {
            //console.log("test");
        }

        $exper.setSchduleData();
        $exper.setScheduleArray();

        $exper.setMode();
        $exper.initPopup();



        $(".full").unbind('click');

        // 닫기
        $("#btn_confirm").click(function (e) {
            location.href = "/participation/event/view/" + $("#id").val();
        });

        $("#close_pop").click(function () {
            $(".confirm_pop").hide();
        })

        $(".schedule .date").click(function () {
            if ($(this).parent().hasClass("selected")) {
                $(this).parent().removeClass("selected");
                $(this).parent().children(".sub-time").hide();

            } else {
                $(this).parent().addClass("selected");

                var container = $(this).parent().find(".sub-time");
                container.css("display", "block");
                var sly = new Sly(container, {    // Call Sly on frame
                    horizontal: 1,
                    itemNav: 'centered',
                    smart: 1,
                    activateOn: 'click',
                    mouseDragging: 1,
                    touchDragging: 1,
                    releaseSwing: 1,
                    //startAt: 130,

                    activatePageOn: 'click',
                    speed: 300,
                    elasticBounds: 1,
                    easing: 'easeOutExpo',
                    dragHandle: 1,
                    dynamicHandle: 1,
                    clickBar: 1,
                })

                sly.one('load', function () {
                    setTimeout(function () {
                        sly.activate(4);
                    }, 300)

                });

                sly.init();
            }
        })

        $(".schedule .unbind").unbind('click');



    },

    setMode: function () {
        var action = $("#action").val();
        //console.log(action)

        // 신청 여부에 따라 버튼 노출
        if (action == "add") {
            if (!$eventEntity.e_closed) {
                $(".btnAdd").show();
                $("#btnApply").show();
                $("#btnApply2").show();
            }
            $exper.step1();
        } else if (action == "edit") {

            if (!$eventEntity.e_closed) {
                $("#btnUpdate").show();
                $("#btnUpdate2").show();
                $("#btnCancel").css("width", "49%");
            } else {
                $("#btnCancel").css("width", "100%");
            }
            $(".btnEdit").show();
            $("#btnCancel").show();
            $("#btnCancel2").show();

            $exper.step2();
        }


        $("#btnApply").text("예약");
        $("#btnUpdate").text("예약 정보 변경");
        $("#btnCancel").text("예약 취소");
        $("#winner_btn_cancle").text("예약 취소");

    },

    // 노출 데이터
    setScheduleArray: function () {
        var today = moment();
        var open = moment($eventEntity.e_open);
        var close = moment($eventEntity.e_close).add(1, 'days');

        // 오늘 이전 데이터는 사용할 수 없음
        if (open < today) {
            open = today;
        }
        var index = 0;
        while (open < close) {
            var key = open.format("YYYYMMDD");
            if ($exper.scheduleData[key]) {
                $exper.scheduleArray[index++] = $exper.scheduleData[key];
            } else {
                // 휴관 일도 날짜는 표시해야함
                $exper.scheduleArray[index++] = open.format("YYYY/MM/DD");
            }

            open = open.add(1, 'days');
        }
    },

    // db데이터 
    setSchduleData: function () {
        var loadedData = $.parseJSON($("#scheduleData").val());
        console.log(loadedData);
        var list = [];
        var date = "";
        $.each(loadedData, function (i, v) {

            if (i == 0) date = moment(this.et_date).format("YYYYMMDD");

            if (date != moment(this.et_date).format("YYYYMMDD")) {
                $exper.scheduleData[date] = list;
                list = [];

                date = moment(this.et_date).format("YYYYMMDD");
            }


            var entity = {
                date: moment(this.et_date).format("YYYY/MM/DD HH:mm:ss"),
                closed: this.et_closed
            }
            list.push(entity);
        });
        $exper.scheduleData[date] = list;
    },

    initPopup: function () {
        var popup = $(".fn_pop_content");

        // 전화번호 형식
        popup.find(".phone").mobileFormat();

        popup.find(".name").val($("#name").val());
        popup.find(".phone").val($("#phone").val());

        popup.find(".location").text($("#location").val());

        popup.find(".open").text(moment($eventEntity.e_open).format("YYYY년 MM월 DD일"));
        popup.find(".close").text(moment($eventEntity.e_close).format("YYYY년 MM월 DD일"));


        if ($("#action").val() == "edit") {
            $("#number").val($("#count").val());
            $(".route").val($("#route").val());
        }


        // +, -버튼
        popup.find("#number").buttonCounter(popup.find(".minus"), popup.find(".pluse"), { max: 100, min: 0 });
        $("#number").focusin(function () {
            $("#number").val("");
        });
        $("#number").focusout(function () {
            if ($(this).val() == "") $("#number").val("0");
        });


        $exper.paging();

        $(".custom_sel").selectbox({});
    },

    step1: function () {
        var popup = $(".fn_pop_content");

        popup.find(".content").hide();
        popup.find(".content-1").show();
        popup.find(".content-2").hide();
        popup.find(".content-3").hide();

        popup.find(".period").text(moment($eventEntity.e_open).format("YYYY년 MM월 DD일") + '~' + moment($eventEntity.e_close).format("YYYY년 MM월 DD일"));


        $(".ex_info").removeClass("mb30");
        popup.find(".stepWrap li").removeClass("on").eq(0).addClass("on");
        $("#select_time").text("선택가능기간");
    },

    step2: function () {
        $(".confirm_pop").hide();
        var popup = $(".fn_pop_content");

        if (!$exper.info.date) {
            alert("일정을 선택해주세요.");
            return false;
        }

        popup.find(".content").hide();
        popup.find(".content-1").hide();
        popup.find(".content-2").show();
        popup.find(".content-3").hide();

        $(".ex_info").addClass("mb30");

        $("#select_time").text("선택일시");
        popup.find(".period").text(moment($exper.info.date).format("YYYY년 MM월 DD일 a HH:mm"));

        popup.find(".stepWrap li").removeClass("on").eq(1).addClass("on");
    },

    step3: function () {
        var popup = $(".fn_pop_content");
        $("#possible_select").hide();
        popup.find(".finish_name").text(popup.find(".name").val());
        popup.find(".finish_count").text(popup.find("#number").val());

        popup.find(".content").hide();
        popup.find(".content-1").hide();
        popup.find(".content-2").hide();
        popup.find(".content-3").show();

        $(".ex_info").addClass("mb30");

        popup.find(".period").text($eventEntity.e_title + " / " + moment($exper.info.date).format("YYYY년 MM월 DD일 a HH:mm"));

        popup.find(".stepWrap li").removeClass("on").eq(2).addClass("on");
    },

    getPagedData: function () {
        var startIndex = ($exper.page - 1) * $exper.rows;
        //var endIndex = $exper.page * $exper.rows;
        var endIndex = $exper.scheduleArray.length;
        $exper.hasNext = endIndex < $exper.scheduleArray.length;

        return $exper.scheduleArray.slice(startIndex, endIndex);
    },

    makeTimetable: function () {

        var data = $exper.getPagedData()

        $(".fn_pop_content").find(".schedule").remove();

        $.each(data, function (index) {

            var scheduleEl = $exper.template.clone();

            var date = null;

            if ($.isArray(this)) {

                date = moment(this[0].date);

                // 10 ~ 20:30까지 
                var printTime = new Date(date.format("YYYY/MM/DD") + " 10:00");
                var endTime = new Date(date.format("YYYY/MM/DD") + " 21:00");

                var $el = $("<li class='item begin' style='height:10px'></li>");
                scheduleEl.find(".timetable").append($el);

                while (printTime < endTime) {
                    var $scuedule = $.grep(this, function (v, i) {
                        return v.date == moment(printTime).format("YYYY/MM/DD HH:mm:ss");
                    })[0]

                    // 해당 시간에 일정이 있으면
                    if ($scuedule) {
                        $date = moment(printTime);

                        var $el = $("<li class='item' data-date='" + $date.format("YYYY/MM/DD HH:mm") + "'>" + $date.format("HH:mm") + "</li>");

                        // 마감이면 disabled
                        var applyCount = $.grep($exper.applyData, function (v, i) {
                            return moment(v.er_date).format("YYYY/MM/DD HH:mm:ss") == $scuedule.date;
                        })[0];

                        //if ($scuedule.closed || (applyCount && applyCount.apply_count >= $eventEntity.e_maximum)) {

                        //    $el.addClass("full");
                        //    if ($exper.info.date == $date.format("YYYY/MM/DD HH:mm")) {
                        //        $exper.info.date = null;
                        //    }

                        //    // 선택일이면 on
                        //} else {
                        //    if ($exper.info.date == $date.format("YYYY/MM/DD HH:mm")) {
                        //        $el.addClass("selected-time");
                        //    }
                        //}

                        var applyCnt = 0;
                        var applyPersonCnt = 0;
                        if (applyCount) {
                            applyCnt = applyCount.apply_count;
                            applyPersonCnt = applyCount.apply_person_count;
                        }

                        var $el = $("<li class='item' data-date='" + $date.format("YYYY/MM/DD HH:mm") + "' data-applycount='" + applyCnt + "' data-applypersoncount='" + applyPersonCnt + "'>" + $date.format("HH:mm") + "</li>");

                        if ($scuedule.closed || (applyCount
                            && (
                                applyCount.apply_count >= $eventEntity.e_maximum
                                ||
                                applyCount.apply_person_count >= $eventEntity.e_maximum_person
                            ))) {

                            $el.addClass("full");
                            if ($exper.info.date == $date.format("YYYY/MM/DD HH:mm")) {
                                $exper.info.date = null;
                            }

                            // 선책일이면 on
                        } else {
                            if ($exper.info.date == $date.format("YYYY-MM-DD HH:mm")) {
                                $el.addClass("on");
                            }
                        }

                        scheduleEl.find(".timetable").append($el);

                    } else {
                        var $el = $("<li class='item disabled'></li>");
                        scheduleEl.find(".timetable").append($el);
                    }

                    //printTime = new Date(printTime.getTime() + (30 * 60000));
                    printTime = new Date(printTime.getTime() + (parseInt($eventEntity.e_time_interval, 10) * 60000));


                }

                var $el = $("<li class='item end'></li>");
                scheduleEl.find(".timetable").append($el);

                scheduleEl.find(".date").html('<span>' + date.format("YYYY-MM-DD") + ' (<span class="day">' + date.format("ddd") + "</span>)" + '<span class="arrow"></span></span>');



            } else {

                // 날짜에 시간이 비어있으면 휴관일
                date = moment(String(this));
                scheduleEl.addClass("closed");

                scheduleEl.find(".date").html('<span>' + date.format("YYYY-MM-DD") + ' (<span class="day">' + date.format("ddd") + "</span>)" + '<span class="arrow">휴관</span></span>');
                scheduleEl.find(".date").addClass("unbind");
            }

            $(".fn_pop_content").find(".wrapper").append(scheduleEl);
        });



    },

    paging: function () {

        $exper.makeTimetable();

        // 페이징 버튼
        if ($exper.hasNext) {
            $(".fn_pop_content").find(".btnRight").show();
        } else {
            $(".fn_pop_content").find(".btnRight").hide();
        }
        if ($exper.page > 1) {
            $(".fn_pop_content").find(".btnLeft").show();
        } else {
            $(".fn_pop_content").find(".btnLeft").hide();
        }

        $exper.setClickEvent();
    },

    // 시간 선택
    setClickEvent: function () {
        $(".fn_pop_content").find(".item").click(function () {
            if (!$(this).hasClass("disabled") && !$(this).hasClass("full")) {
                $(".fn_pop_content").find(".item").removeClass("selected-time");
                $(this).addClass("selected-time");

                $exper.info.date = $(this).attr("data-date");
                $exper.info.applycount = $(this).attr("data-applycount");
                $exper.info.applypersoncount = $(this).attr("data-applypersoncount");

                $(".fn_pop_content").find(".period_pop").text('선택 일시 : ' + moment($exper.info.date).format("YYYY년 MM월 DD일 a HH:mm"));

                var margin_top = $(window).scrollTop() + ($(window).height() - $("#confirm_pop").height()) / 2;
                $("#confirm_pop").css({ "top": margin_top })
                $(".confirm_pop").show();
            }
            return false;
        });
    },

    apply: function () {
        var obj = $(".fn_pop_content");

        if (obj.find(".name").val() == "") {
            alert("이름을 입력해주세요.");
            obj.find(".name").focus();
            return false;
        }

        if (obj.find(".phone").val() == "") {
            alert("연락처를 입력해주세요.");
            obj.find(".phone").focus();
            return false;
        }

        if (!$.isNumeric(obj.find("#number").val())) {
            obj.find("#number").val(0);
        }

        if (obj.find("#number").val() == 0) {
            alert("신청인원을 입력해주세요.");
            //obj.find("#number").focus();
            return false;
        }

        if ($("#action").val() == "edit") {
            var originCount = parseInt($("#count").val(), 10);
            if (parseInt($eventEntity.e_maximum, 10) - parseInt($exper.applyData[0].apply_count, 10) < 0) {
                alert("선택하신 일시에 지원할 수 있는 팀 수를 초과하였습니다.");
                return false;
            }

            var applyCount = $.grep($exper.applyData, function (v, i) {
                return moment(v.er_date).format("YYYY/MM/DD HH:mm:ss") == moment($exper.info.date).format("YYYY/MM/DD HH:mm:ss");
            })[0];

            var applyPersonCnt = 0;
            if (applyCount) {
                applyPersonCnt = applyCount.apply_person_count;
            }

            var isDateChange = (moment($exper.info.date).format("YYYY/MM/DD HH:mm:ss") != moment($('#date').val()).format("YYYY/MM/DD HH:mm:ss"));

            var remainPersonCount = parseInt($eventEntity.e_maximum_person, 10) - (parseInt(applyPersonCnt, 10) - (originCount));
            if (isDateChange) {
                remainPersonCount = parseInt($eventEntity.e_maximum_person, 10) - (parseInt(applyPersonCnt, 10));
            }

            if (obj.find("#number").val() > remainPersonCount) {
                alert("선택하신 일시에 지원할 수 있는 인원 수를 초과하였습니다.\n남은 수는 " + remainPersonCount + "명입니다.");
                return false;
            }
        }
        else {
            if (parseInt($eventEntity.e_maximum, 10) - parseInt($exper.info.applycount, 10) < 1) {
                alert("선택하신 일시에 지원할 수 있는 팀 수를 초과하였습니다.");
                return false;
            }
            var remainPersonCount = parseInt($eventEntity.e_maximum_person, 10) - parseInt($exper.info.applypersoncount, 10);
            if (obj.find("#number").val() > remainPersonCount) {
                alert("선택하신 일시에 지원할 수 있는 인원 수를 초과하였습니다.\n남은 수는 " + remainPersonCount + "명입니다.");
                return false;
            }
        }

        //if (obj.find("#number").val() > 5 || obj.find("#number").val() < 0) {
        //    alert("5명이하로 선택해주세요");
        //    obj.find("#number").focus();
        //    return false;
        //}



        //if (obj.find(".route").val() == "") {
        //    alert("컴패션체험전을 알게 된 경로를 선택해주세요.")
        //    obj.find(".route").focus();
        //    return false;
        //}

        //$("#loading_bg").css('z-index', '111990');
        loading.show("처리중입니다.");

        var param = {
            date: $exper.info.date,
            name: obj.find(".name").val(),
            phone: obj.find(".phone").val(),
            count: obj.find("#number").val(),
            id: $("#id").val(),
            route: obj.find(".route").val(),
            title: $("#titleHidden").val()
        }

        $.post("/api/event.ashx?t=apply", param, function (r) {
            loading.hide();
            if (r.success) {

                $exper.step3();

            } else {
                alert(r.message);
            }
        });
        return true;
    },

    cancel: function () {

        if (common.checkLogin()) {
            if (confirm("취소하시겠습니까?")) {
                $.post("/api/event.ashx?t=cancel&id=" + $("#id").val(), function (r) {
                    if (r.success) {
                        alert("예약이 취소되었습니다.");
                        location.reload();
                    } else {
                        alert(r.message);
                    }
                });
            }
        }
    }

}


// 캠페인 / 이벤트
$event = {
    type: "",
    open: "",
    close: "",
    e_close_datetime: "",
    $modal: null,
    template: null,
    info: {
        date: null,
        name: null,
        phone: null,
        count: null,
        route: null
    },
    action: null,
    init: function () {

        moment.locale('ko');
        $event.action = $("#action").val();

        $event.setMode();
        $event.initPopup();

        // 닫기
        $("#btn_confirm").click(function (e) {
            location.href = "/participation/event/view/" + $("#id").val();
        });


    },

    setMode: function () {
        var action = $("#action").val();

        // 신청 여부에 따라 버튼 노출
        if (action == "add") {
            if (!$eventEntity.e_closed) {
                $("#btnApply").show();
                $("#btnApply2").show();
                $(".btnAdd").show();
            }
        } else if (action == "edit") {
            // 신청기간 종료후에는 수정 버튼 미노출
            if (!$eventEntity.e_closed) {
                $("#btnUpdate").show();
                $("#btnCancel").css("width", "49%");
            } else {
                $("#btnCancel").css("width", "49%");
            }
            $("#btnCancel").show();
            $("#btnCancel2").show();
            $("#btnUpdate2").show();
            $("#btnUpdate").show();

            // 확정버튼 추가 문희원 2017-06-29
            $(".btnEdit").show();
            if ($('#e_confirm_display').val() == 'Y') {
                $("#btnconfirm").show();
                $("#btnconfirm2").show(); //코딩에 없음, 캠페인 등록시 html로 등록(이석호)			

                $("#btnUpdate").show();
                $("#btnUpdate2").show();
                $("#btnCancel").show();
                $("#btnCancel2").show();
            }
        }

        if ($("#isWin").val() == "1") {
            $("#btnCancel").hide();
            $("#btnCancel2").hide();
            $(".btnEdit").hide();
            $("#btnUpdate").hide();
            $("#btnUpdate2").hide();
            $("#btnconfirm").hide();
            $("#btnconfirm2").hide();
        }

        if ($("#id").val() == "43") {
            $("#btnCancel").hide();
            $("#btnCancel2").hide();
            $(".btnEdit").hide();
            $("#btnUpdate").hide();
            $("#btnUpdate2").hide();
            $("#btnconfirm").hide();
            $("#btnconfirm2").hide();
        }

        $("#btnApply").text("신청 하기");
        $("#btnUpdate").text("신청 정보 변경");
        $("#btnCancel").text("신청 취소");
        $("#winner_btn_cancle").text("신청 취소")
    },

    initPopup: function () {
        var popup = $(".fn_pop_content");

        // 전화번호 형식
        popup.find(".phone").mobileFormat();
        popup.find(".location").text($("#location").val());

        popup.find(".name").val($("#name").val());
        popup.find(".phone").val($("#phone").val());


        if ($("#action").val() == "edit") {
            $("#number").val($("#count").val());
            $(".route").val($("#route").val());
            $("#request_contents").val($("#er_request_contents").val());
        }


        // +, -버튼
        popup.find("#number").buttonCounter(popup.find(".minus"), popup.find(".pluse"), { max: $eventEntity.e_maximum, min: 1 });
        $("#number").focusin(function () {
            $("#number").val("");
        });
        $("#number").focusout(function () {
            if ($(this).val() == "") $("#number").val("0");
        });

        $("#maximun_request").text($eventEntity.e_maximum);

        // 신청날짜
        if ($eventEntity.e_type == "date" || $eventEntity.e_type == "complex") {
            popup.find(".date_section").show();

            if ($("#picked_date").val() != "") {
                var list = $.parseJSON($("#picked_date").val());
                popup.find(".date").append('<option value="">선택하세요</option>');
                $.each(list, function () {
                    var pickDate = moment(this.date.toString());
                    if (moment() < pickDate) {
                        var selected = this.date == $("#date").val() ? "selected" : "";
                        var closed = this.limit <= this.apply;
                        var optionTag = '<option value="' + pickDate.format("YYYY/MM/DD HH:mm") + '" ' + (selected ? 'selected data-selected="1" ' : '') + (closed ? 'data-closed="1" ' : '') + '>' + pickDate.format("M월D일(ddd) HH시mm분") + (closed ? " - 마감" : "") + '</option>';
                        popup.find(".date").append(optionTag);
                    }
                });
            }
        }

        //신청 내용
        if (($eventEntity.e_type == "count" || $eventEntity.e_type == "complex" || $eventEntity.e_type == "date")
            && $eventEntity.e_request_contents == true) {
            popup.find(".request-contents").show();
        }

        if ($eventEntity.e_type == "count" || $eventEntity.e_type == "complex") {
            popup.find(".count_section").show();
        }

        if ($event.action == "edit") {
            popup.find("#number").val($("#count").val());
            popup.find(".route option[value='" + $("#route").val() + "']").attr("selected", "selected");
            popup.find(".date option[value='" + moment($("#date").val()).format("YYYY/MM/DD") + "']").attr("selected", "selected");
        }


        //   $(".custom_sel").selectbox({});



    },

    apply: function () {
        var obj = $(".fn_pop_content");
        if (obj.find(".name").val() == "") {
            alert("이름을 입력해주세요.");
            obj.find(".name").focus();
            return false;
        }

        if (obj.find(".name").val().length > 20) {
            alert("이름을 정확히 입력해주세요.");
            obj.find(".name").focus();
            return false;
        }

        if (obj.find(".phone").val() == "") {
            alert("연락처를 입력해주세요.");
            obj.find(".phone").focus();
            return false;
        }

        //if (obj.find("#number:visible").length > 0 && obj.find("#number").val() == 0) {
        //    alert("방문인원을 입력해주세요.");
        //    obj.find("#number").focus();
        //    return false;
        //}

        if (obj.find("#number:visible").length > 0 && (obj.find("#number").val() == 0 || obj.find("#number").val() > $eventEntity.e_maximum)) {
            alert("신청 인원을 1~" + $eventEntity.e_maximum + "명 사이로 입력해주세요.");
            obj.find("#number").focus();
            return false;
        }

        if (obj.find(".date_section:visible").length > 0) {
            if (obj.find(".date").val() == "") {
                alert("신청날짜를 선택하세요.");
                obj.find(".date").focus();
                return false;
            }

            var selectedOption = obj.find(".date option:selected");
            if (selectedOption.attr("data-selected") != "1" && selectedOption.attr("data-closed") == "1") {
                alert("선택한 시간은 마감되었습니다.");
                obj.find(".date").focus();
                return false;
            }
        }

        if (obj.find("#request_contents:visible").length > 0 && ($.trim(obj.find("#request_contents").val()) == "" || obj.find("#request_contents").val().length > 200)) {
            alert("신청 내용을 200자 이내로 정확히 입력해주세요");
            obj.find("#request_contents").focus();
            return false;
        }



        //if (obj.find(".route").val() == "") {
        //    alert("컴패션체험전을 알게 된 경로를 선택해주세요.")
        //    obj.find(".route").focus();
        //    return false;
        //}
        //obj.find(".period").text($eventEntity.e_title + " / " + moment(obj.find(".date").val()).format("YYYY년 MM월 DD일"));
        var periodText = obj.find(".date").val() ? $eventEntity.e_title + " / " + moment(obj.find(".date").val()).format("YYYY년 MM월 DD일") : $eventEntity.e_title;
        obj.find(".period").text(periodText);
        var param = {
            date: obj.find(".date").val(),
            name: obj.find(".name").val(),
            phone: obj.find(".phone").val(),
            count: obj.find("#number").val(),
            id: $("#id").val(),
            route: obj.find(".route").val(),
            title: $("#titleHidden").val(),
            request_contents: $("#request_contents").val()
        }

        $.post("/api/event.ashx?t=apply", param, function (r) {
            if (r.success) {

                obj.find(".finish_name").text(obj.find(".name").val());
                obj.find(".finish_count").text(obj.find("#number").val() == "-1" ? "1" : obj.find("#number").val());

                obj.find(".content-1").hide();
                obj.find(".content-2").show();

                /*
                if($(".date").val() == null){
					$("#is_period").hide();
                }
				*/
                obj.find(".stepWrap li").removeClass("on").eq(1).addClass("on");

            } else {
                alert(r.message);
            }
        });
        return true;
    },

    cancel: function () {

        if (common.checkLogin()) {
            if (confirm("취소하시겠습니까?")) {
                $.post("/api/event.ashx?t=cancel&id=" + $("#id").val(), function (r) {
                    if (r.success) {
                        alert("취소가 완료되었습니다.");
                        location.reload();

                    } else {
                        alert(r.message);
                    }
                });
            }
        }
    }
}

$page = {
    init: function () {

        if ($("#e_show_child").val() == "Y") {
            $page.showChild();
        } else if ($("#e_show_program").val() == "Y") {
            $page.showSpecialFunding();
        }

        $(".btn_sponsor_checkout").click(function () {

            if ($("#pn_special_funding").css("display") != "none" && $("#special_funding_price").val() == "") {
                $("#msg_funding").html("금액을 입력해주세요.").show();
                return false;
            }

            var amount = parseInt($("#special_funding_price").val());
            if (amount < 1000) {
                alert("후원금은 천원 이상 입력 가능합니다.");
                return;
            }

            if (amount % 1000 > 0) {
                alert("죄송합니다.\n천원단위로 후원 가능합니다.");
                return;
            }

            var type = $(this).data("type");
            var frequency = $(this).data("frequency");
            var group = $(this).data("group");
            var codeId = $(this).data("codeid");
            var codeName = $(this).data("codename");
            var childMasterId = $(this).data("childmasterid");
            var campaignId = $(this).data("campaignid");
            var relation_key = $(this).data("relation_key");
            var amount = $("#special_funding_price").val();

            // cdsp : childmasterId , special : frequency + amount + campaignId
            var t = (type == "CDSP") ? "go-cdsp" : "go-special";

            $.post("/sponsor/pay-gateway.ashx", {
                t: t,
                frequency: frequency,
                amount: amount,
                childMasterId: childMasterId,
                campaignid: campaignId
            }).success(function (r) {
                //console.log(r);
                if (r.success) {
                    location.href = r.data;

                } else {
                    alert(r.message);
                }
            });

            return true;
        })

        $("#special_funding_price_opt").change(function () {

            if ($("#special_funding_price_opt").val() != "") {
                $("#direct_input").hide();
            } else {
                $("#direct_input").show();
            }
            console.log($("#special_funding_price_opt").val())
        })

    },

    // 1:1 후원인경우 
    showChild: function () {

        $.get("/api/tcpt.ashx?t=event-child", {}).success(function (r) {
            console.log(r.data);
            if (r.success) {

                var list = r.data;
                if (list.length > 0) {
                    $.each(list, function () {
                        this.birthdate = new Date(this.birthdate);
                    });

                    var entity = list[0];

                    $("#pn_child").show();

                    $("#c_name").text(entity.name);
                    $("#c_img").css("background", 'url("' + entity.pic + '") no-repeat center');
                    $("#c_gender").text(entity.gender);
                    $("#c_age").text(entity.age + "세");
                    $("#c_birthdate").text(moment(new Date(entity.birthdate)).format('YYYY.MM.DD'))
                    $("#c_country").text(entity.countryname);
                    $("#c_waitingdays").text(entity.waitingdays + "일");
                    $("#show_child").attr("data-childMasterId", entity.childmasterid);

                    var btn = $(".btn_sponsor_checkout");
                    btn.attr("data-type", "CDSP");
                    btn.attr("data-frequency", "정기");
                    btn.attr("data-childMasterId", entity.childmasterid);
                    console.log(list)
                }

            } else {
                alert(r.message);
            }
        });
    },

    showSpecialFunding: function () {

        var prices = $("#e_sf_prices").val();
        console.log(prices)
        $("#special_funding_price_opt").append($("<option value=''>직접입력</option>"));
        $.each(prices.split(','), function () {
            $("#special_funding_price_opt").append($("<option value='" + this + "'>" + this.format() + "원</option>"));
        })
        $("#special_funding_price_opt").change(function () {
            $("#special_funding_price").val($(this).val());
        })

        $(".custom_sel").selectbox({

            onOpen: function (inst) {
            }
        });

        $.get("/api/special-funding.ashx?t=get", { campaignId: $("#campaignId").val() }).success(function (r) {
            //console.log(r);
            if (r.success) {

                var entity = r.data;
                if (entity) {

                    $("#pn_special_funding").show();
                    $("#sf_title").text(entity.campaignname);


                    var btn = $(".btn_sponsor_checkout");
                    btn.attr("data-type", "special");
                    btn.attr("data-frequency", "일시");
                    btn.attr("data-group", entity.accountclassgroup);
                    btn.attr("data-codeId", entity.accountclass);
                    btn.attr("data-codeName", entity.campaignname);
                    btn.attr("data-childMasterId", "");
                    btn.attr("data-campaignId", entity.campaignid);
                    btn.attr("data-relation_key", entity.sf_id);

                }
            } else {
                alert(r.message);
            }
        });
    }
}

