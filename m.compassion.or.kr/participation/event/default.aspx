﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="participation_event_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
    <script type="text/javascript" src="/participation/event/default.js"></script>

    <script type="text/javascript"> 

  var _ntp = {}; 

  _ntp.host = (('https:' == document.location.protocol) ? 'https://' : 'http://') 

  _ntp.dID = 779; 

  document.write(unescape("%3Cscript src='" + _ntp.host + "nmt.nsmartad.com/content?cid=1' type='text/javascript'%3E%3C/script%3E")); 

</script> 

<script>

 callback = function(){}

</script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl"  id="l">
        <!---->

        <div class="participation-campaign">

            <fieldset class="frm-search sectionsub-margin1">
                <legend>검색입력</legend>
                <input type="text" name="k_word" id="k_word" ng-enter="search()" placeholder="검색어를 입력해 주세요" />
                <span class="search" ng-click="search()">검색</span>
            </fieldset>

            <!--썸네일 리스트-->
            <div class="wrap-thumbil">

                <!--결과없음-->
                <div class="no-result" ng-if="total == 0">
                    <strong class="txt-title">검색 결과가 없습니다.</strong>
                </div>
                <!--//결과없음-->
                <ul class="list-thumbil width100">
                    <li ng-repeat="item in list|orderBy :'closed':false"><a href="#" class="box-block" ng-click="_nto.callTrack('5569', callback()); goView(item.e_id)">
                        <span class="photo">
                            <img ng-src="{{item.e_thumb_m}}" alt="이벤트 이미지" />
                            <span class="period1" ng-if="!item.event_closed">진행중</span>
                            <span class="period2" ng-if="item.event_closed">마감</span>
                        </span>
                        <span class="advocate-campaigninfo">
                            <span class="txt-supporter" ng-if="item.e_target == 'sponsor'">후원자 전용</span>
                            <span class="txt-title">{{item.e_title}}</span>
                            <span class="txt-etc">
                                <div ng-if="item.e_type == 'general'">
                                </div>
                                <div ng-if="item.e_type == 'date' || item.e_type == 'count' || item.e_type == 'complex'">
                                    신청 기간 : {{parseDate(item.e_begin) | date:'yyyy년 MM월 dd일'}} ~ {{parseDate(item.e_end) | date:'yyyy년 MM월 dd일'}}
                                </div>
                                <div ng-if="item.e_type == 'experience'">
                                    체험전 기간 : {{parseDate(item.e_open) | date:'yyyy년 MM월 dd일'}} ~ {{parseDate(item.e_close) | date:'yyyy년 MM월 dd일'}}
                                </div>

                                <span ng-if="item.e_announce">당첨자 발표 : {{parseDate(item.e_announce) | date:'yyyy년 MM월 dd일'}}</span>
                            </span>
                        </span>
                    </a></li>
                </ul>
            </div>


            <paging class="small" ng-if="total != 0" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>


        </div>

        <!--//-->
    </div>



</asp:Content>
