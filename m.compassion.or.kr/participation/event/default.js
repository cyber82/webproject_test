﻿(function () {


	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

        $scope.total = -1;
        $scope.rowsPerPage = 5;

        $scope.list = [];
        $scope.params = {
        	page: 1,
        	rowsPerPage: $scope.rowsPerPage
        };

		// 파라미터 초기화
        $scope.params = $.extend($scope.params, paramService.getParameterValues());
        if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);

        // 검색
        $scope.search = function (params) {
        	$scope.params.page = 1;
        	$scope.params = $.extend($scope.params, params);
        	$scope.params.k_word = $("#k_word").val();
        	$scope.getList();
        }

        // list
        $scope.getList = function (params) {
        	$scope.params = $.extend($scope.params, params);
        	$http.get("/api/event.ashx?t=list", { params: $scope.params }).success(function (result) {
        		$scope.list = result.data;
        		$scope.total = result.data.length > 0 ? result.data[0].total : 0;
        		if (params)
        			scrollTo($("header"));
        	});


        	if (params)
        	    scrollTo($("header"));
        }

        // 상세페이지
        $scope.goView = function (id) {
        	$http.post("/api/participation.ashx?t=hits_event&id=" + id).then().finally(function () {
        		location.href = "/participation/event/view/" + id + "?" + $.param($scope.params);
        	});
        	
        }

        $scope.parseDate = function (datetime) {
        	return new Date(datetime);
        }



        $scope.getList();

        
    });

	
})();

