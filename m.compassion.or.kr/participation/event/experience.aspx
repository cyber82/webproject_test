﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="experience.aspx.cs" Inherits="participation_event_experience" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/moment/moment.min.js"></script>
    <script type="text/javascript" src="/assets/moment/locales.min.js"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <script type="text/javascript" src="/participation/event/view.js?v=3"></script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" id="id" runat="server" />

    <input type="hidden" id="scheduleData" runat="server" />
    <input type="hidden" id="eventEntity" runat="server" />
    <input type="hidden" id="applyData" runat="server" />


    <input type="hidden" id="count" runat="server" />
    <input type="hidden" id="route" runat="server" />
    <input type="hidden" id="date" runat="server" />
    <input type="hidden" id="location" runat="server" />
    <input type="hidden" id="action" runat="server" />
    <input type="hidden" id="target" runat="server" />
    <input type="hidden" id="isSponsor" runat="server" value="true"/>

    <input type="hidden" id="titleHidden" runat="server" />

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <!---->

        <div class="participation-campaign fn_pop_content">

            <div class="step-flow content-1">
                <ol>
                    <li class="recent"><span class="bg"></span><em>STEP 01</em>일정 선택</li>
                    <li><span class="bg"></span><em>STEP 02</em>정보 입력</li>
                    <li><span class="bg"></span><em>STEP 03</em>예약 완료</li>
                </ol>
            </div>
            <div class="step-flow content-2" style="display: none;">
                <ol>
                    <li><span class="bg"></span><em>STEP 01</em>일정 선택</li>
                    <li class="recent"><span class="bg"></span><em>STEP 02</em>정보 입력</li>
                    <li><span class="bg"></span><em>STEP 03</em>예약 완료</li>
                </ol>
            </div>

            <div class="step-flow content-3" style="display: none;">
                <ol>
                    <li><span class="bg"></span><em>STEP 01</em>일정 선택</li>
                    <li><span class="bg"></span><em>STEP 02</em>정보 입력</li>
                    <li class="recent"><span class="bg"></span><em>STEP 03</em>예약 완료</li>
                </ol>
            </div>

            <div class="box2-gray" id="possible_select">
                <table class="info">
                    <caption>체험전 선택가능기간,장소</caption>
                    <colgroup>
                        <col style="width: 33%" />
                        <col style="width: 69%" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th id="select_time">선택가능기간</th>
                            <td class="period"></td>
                        </tr>
                        <tr>
                            <th>장소</th>
                            <td class="location"></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <!-- step1 -->

            <div class="wrap-timeline content-1">
                <p class="txt-info">FULL</p>
                <span class="clear"></span>

                <ul class="list-timeline content content-1 wrapper">
                    <li class="schedule">
                        <!--현재날짜 선택상태-->
                        <span class="link-block date"></span>
                        <div class="sub-time" style="display: none;">

                            <ul style="left: 0px; position:absolute" class="timetable">
                                <!--absolute 레이어 (임시 inline  css) : 추후 jquery 시 변경하시면 됩니다-->

                            </ul>
                            <!--시간선택 display-->
                            <div class="left-smoth"></div>
                            <!--왼쪽 smoth css-->
                            <div class="right-smoth"></div>
                            <!--오른쪽 smoth css-->
                        </div>


                    </li>

                </ul>
            </div>

            <!-- step1 -->

            <!-- step2 -->

            <fieldset class="frm-input frm-input-topline content content-2" style="display: none;">
                <legend>체험전 예약정보 입력</legend>
                <div class="row-table2">
                    <div class="col-th" style="width: 34%">이름</div>
                    <div class="col-td" style="width: 66%">
                        <input type="text" id="name" runat="server" class="name" value="" style="width: 100%;" />
                    </div>
                </div>
                <div class="row-table2">
                    <div class="col-th" style="width: 34%">휴대폰</div>
                    <div class="col-td" style="width: 66%">
                        <input type="text" id="phone" runat="server" class="phone number_only" maxlength="13" value="" style="width: 100%;" />
                    </div>
                </div>
                <div class="row-table2">
                    <div class="col-th" style="width: 34%">
                        <span class="text2row">방문 인원<br />
                            (본인 포함)</span>
                    </div>
                    <div class="col-td" style="width: 66%">
                        <span class="count">
                            <a class="count-minus minus">수량 빼기</a>
                            <input type="number" id="number" class="input_type3 fl" value="1" />
                            <a class="count-plus pluse">수량 더하기</a>

                        </span>
                    </div>
                </div>

                <div class="row-txt">
                    <%--<p class="bu">방문 인원 6~10명은? : 두 세팀으로 나누어 각 팀의 대표자가 개별적으로 신청 </p>
                    <p class="bu">방문 인원 10명 이상은? : 단체로 구분되오니 아래 연락처로 개별 문의 주세요 02-3668-3443 / change@compassion.or.kr</p>--%>
                    <p class="bu">윈터스쿨은 15분 단위로 한 팀씩 참여합니다.</p>
                    <p class="bu">한 팀은 최대 6명까지 신청 가능합니다.</p>
                    <p class="bu">프로그램의 원활한 진행을 위해 단체 예약은 받지 않는 점 양해 부탁드립니다.</p>
                    <p class="bu">6명 이상인 경우, 여러 시간대를 예약해 주시기 바랍니다.</p>
                </div>

                <div class="row-table2" style="display:none;">
                    <div class="col-th" style="width: 34%">
                        <span class="text2row">컴패션체험전을<br />
                            알게 된 경로</span>
                    </div>
                    <div class="col-td" style="width: 66%">
                        <select class="route" name="" id="sel_1" style="width: 100%">
                            <option value="">선택해 주세요</option>
                            <option value="교회">교회</option>
                            <option value="이메일(뉴스레터)">이메일(뉴스레터)</option>
                            <option value="우편(DM)">우편(DM)</option>
                            <option value="SNS">SNS</option>
                            <option value="문자(SMS)">문자(SMS)</option>
                            <option value="컴패션 홈페이지">컴패션 홈페이지</option>
                            <option value="컴패션 모바일앱">컴패션 모바일앱</option>
                            <option value="지인 소개">지인 소개</option>
                            <option value="신문/방송">신문/방송</option>
                            <option value="잡지">잡지</option>
                        </select>
                    </div>
                </div>
            </fieldset>

            <p class="txt-sub align-center mt20 content content-2" style="display: none;">
                위 정보로 체험전 예약이 진행되오니,<br />
                입력하신 정보가 정확한지 다시 한 번 확인 부탁드립니다.
            </p>

            <div class="wrap-bt content content-2" style="display: none;">
                <a class="bt-type3 fl btnAdd" style="width: 49%; display: none;" onclick="$exper.step1()" >이전</a>
                <a class="bt-type6 fr btnAdd" style="width: 49%; display: none;" onclick="$exper.apply()" >확인</a>

                <a class="bt-type3 fl btnEdit" style="width: 49%; display: none;" onclick="$exper.step1()">일정 변경</a>
                <a class="bt-type6 fr btnEdit" style="width: 49%; display: none;" onclick="$exper.apply()">정보 변경하기</a>
            </div>

            <!-- step2 -->

            <!-- step3 -->
            <div class="box2-gray color2 content content-3" style="display: none;">
                <span class="period"></span>
                <br />
                <span class="location"></span>
            </div>

            <p class="txt-sub align-center mt20 content content-3" style="display: none;">
                <span class="color2 finish_name"></span>님 포함 총 
                <span class="color2 count_section">
                    <span class="finish_count"></span>명의
                </span>

                <br />
                신청이 완료되었습니다.<br />
                전체 캠페인/이벤트 참여 내역은<br />
                <a href="/my/activity/event/">마이컴패션</a>에서 확인하실 수 있습니다.<br />
                감사합니다.
 
            </p>

            <div class="wrap-bt content content-3" style="display: none;">
                <a class="bt-type6" style="width: 100%" id="btn_confirm">확인</a>
            </div>

            <div class="linebar content content-3" style="display: none;"></div>
            <p class="txt-sub align-center content-3" style="display: none;">지인들에게 컴패션체험전을 알려 주세요!</p>

            <div class="share-sns content content-3 sns_ani" style="display: none;">
                <a class="sns_control" ng-click="sns.show($event)">
                    <img src="/common/img/icon/share2.png" alt="SNS 공유" />
                </a>
            </div>
            <!-- step3 -->

        </div>
    </div>

    <!--//-->

    <!--레이어 안내 팝업-->
    <div style="position: absolute; width: 80%; left: 10%; top: 60px; z-index: 991; display: none" class="confirm_pop" id="confirm_pop">
        <!--페이지 임시좌표-->

        <div class="wrap-layerpop ">
            <div class="header">
                <p class="txt-title">일정 확인</p>
            </div>
            <div class="contents fn_pop_content">
                <strong>
                    <span class="period_pop"></span>
                    <br />
                    <span>장소 : </span><span class="location"></span>
                </strong>
                <br />
                <br />
                선택한 시간으로 예약을 진행하시겠습니까?
            
            <div class="wrap-bt"><a href="#" class="bt-type6" onclick="$exper.step2()">다음</a></div>
            </div>
            <div class="close"><span class="ic-close" id="close_pop">레이어 닫기</span></div>
        </div>

    </div>
    <!--//레이어 안내 팝업-->

    <div class="confirm_pop" style="width: 100%; height: 100%; position: fixed; left: 0; top: 0; z-index: 990; opacity: 0.8; background: #000; display: none"></div>

</asp:Content>
