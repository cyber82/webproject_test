﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;
using System.Configuration;

public partial class participation_event_experience : MobileFrontBasePage
{

    const string listPath = "/participation/event/experience";

    protected override void OnBeforePostBack()
    {
        base.OnBeforePostBack();

        // check param
        var requests = Request.GetFriendlyUrlSegments();
        if (requests.Count < 1)
        {
            Response.Redirect(listPath, true);
        }
        var isValid = new RequestValidator()
            .Add("p", RequestValidator.Type.Numeric)
            .Validate(this.Context, listPath);

        if (!requests[0].CheckNumeric())
        {
            Response.Redirect(listPath, true);
        }

        base.PrimaryKey = requests[0];

        id.Value = PrimaryKey.ToString();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var entity = dao.@event.First(p => p.e_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<@event>("e_id", Convert.ToInt32(PrimaryKey));

            titleHidden.Value = entity.e_title;
        }
    }

    void ShowChild()
    {

    }

    void ShowSpecialFunding()
    {

    }

    protected override void loadComplete(object sender, EventArgs e)
    {

        using (FrontDataContext dao = new FrontDataContext())
        {

            //var entity = dao.@event.First(p => p.e_id == Convert.ToInt32(PrimaryKey));
            var entity = www6.selectQF<@event>("e_id", Convert.ToInt32(PrimaryKey));

            target.Value = entity.e_target;

            location.Value = entity.e_location;


            // SNS
            this.ViewState["meta_url"] = ConfigurationManager.AppSettings["domain"] + "participation/event/view/" + Convert.ToInt32(PrimaryKey);
            this.ViewState["meta_title"] = string.Format("[한국컴패션-캠패인/이벤트] {0}", entity.e_title);
            this.ViewState["meta_image"] = entity.e_thumb_m.WithFileServerHost();

            eventEntity.Value = new Dictionary<string, Object> {
                {"e_id" , entity.e_id},
                {"e_title", entity.e_title },
                { "e_type" , entity.e_type},
                { "e_maximum" , entity.e_maximum},
                { "e_closed" , IsClosed(entity)},
                { "e_limit" , entity.e_limit},
                { "e_open" , entity.e_open == null ? "" : Convert.ToDateTime(entity.e_open).ToString("yyyy-MM-dd")},
                { "e_close" , entity.e_close == null ? "" : Convert.ToDateTime(entity.e_close).ToString("yyyy-MM-dd")},
                { "e_maximum_person" , entity.e_maximum_person == null ? -1 : entity.e_maximum_person},
                { "e_time_interval" , entity.e_time_interval == null ? 0 : entity.e_time_interval}
            }.ToJson();


            // 후원자 전용
            if (entity.e_target == "sponsor")
            {
                UserInfo sess = new UserInfo();
                if (sess != null)
                {
                    var commitInfo = new SponsorAction().GetCommitInfo();
                    if (!commitInfo.success)
                    {
                        isSponsor.Value = "false";
                    }
                    if (sess.ConId.Length != 5 && sess.ConId.Length != 6)
                    {
                        isSponsor.Value = "false";
                    }
                }
            }

            // 체험전인 경우 일정 검색
            if (entity.e_type == "experience")
            {
                //var timetableList = dao.event_timetable.Where(p => p.et_e_id == Convert.ToInt32(PrimaryKey)).Select(p => new { p.et_closed, p.et_date, p.et_ymd }).OrderBy(p => p.et_ymd).ToList();
                var timetableList = www6.selectQ<event_timetable>("et_e_id", Convert.ToInt32(PrimaryKey)).Select(p => new { p.et_closed, p.et_date, p.et_ymd }).OrderBy(p => p.et_ymd).ToList();

                scheduleData.Value = timetableList.ToJson();

                //var applyList = dao.sp_event_apply_count_list_f(Convert.ToInt32(PrimaryKey));
                Object[] op1 = new Object[] { "e_id" };
                Object[] op2 = new Object[] { Convert.ToInt32(PrimaryKey) };
                var applyList = www6.selectSP("sp_event_apply_count_list_f", op1, op2).DataTableToList<sp_event_apply_count_list_fResult>();

                applyData.Value = applyList.ToJson();
            }

            if (entity.e_type != "general")
            {

                if (UserInfo.IsLogin)
                {
                    // 신청내역 있는경우 저장된 값
                    var exist = www6.selectQ<event_apply>("er_user_id", new UserInfo().UserId, "er_e_id", Convert.ToInt32(PrimaryKey));
                    //if (dao.event_apply.Any(p => p.er_user_id == new UserInfo().UserId && p.er_e_id == Convert.ToInt32(PrimaryKey)))
                    if(exist.Any())
                    {
                        var data = exist[0];//dao.event_apply.First(p => p.er_user_id == new UserInfo().UserId && p.er_e_id == Convert.ToInt32(PrimaryKey));
                        name.Value = data.er_name;
                        phone.Value = data.er_phone;
                        route.Value = data.er_route;
                        count.Value = data.er_count.ToString();

                        if (data.er_date != null)
                            date.Value = ((DateTime)data.er_date).ToString("yyyy-MM-dd HH:mm");

                        action.Value = "edit";

                    }
                    else
                    {
                        GetUserInfo();
                        action.Value = "add";
                    }
                }
                else
                {
                    action.Value = "add";
                }

            }



        }

    }



    protected void GetUserInfo()
    {
        UserInfo sess = new UserInfo();

        // 이름
        if (sess.UserName != "")
        {
            name.Value = sess.UserName;
        }
        // 국내거주인경우 연락처를 컴파스에서 가져온다
        if (sess.LocationType == "국내")
        {

            var comm_result = new SponsorAction().GetCommunications();
            if (comm_result.success)
            {
                SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
                phone.Value = comm_data.Mobile.TranslatePhoneNumber();
            }
        }
    }


    protected bool IsClosed(@event entity)
    {
        var result = entity.e_closed;
        if (!entity.e_closed)
        {
            if (entity.e_type == "count")
            {
                var apply_count = 0;
                var exist = www6.selectQ2<event_apply>("er_e_id = ", entity.e_id, "er_count !=", -1);
                //if (dao.event_apply.Any(p => p.er_e_id == entity.e_id && p.er_count != -1))
                if (exist.Any())
                {
                    apply_count = exist.Sum(p => p.er_count); //dao.event_apply.Where(p => p.er_e_id == entity.e_id && p.er_count != -1).Sum(p => p.er_count);
                }
                if (entity.e_limit != -1 && entity.e_limit <= apply_count)
                {
                    result = true;
                }
            }

            if (entity.e_type != "general")
            {
                if (Convert.ToDateTime(entity.e_end).CompareTo(DateTime.Now) < 0)
                {
                    result = true;
                }
            }
        }
        return result;
    }




}