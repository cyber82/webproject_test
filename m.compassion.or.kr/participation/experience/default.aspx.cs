﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Configuration;

public partial class participation_visiontrip_default : MobileFrontBasePage {


	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);
        
        btn_pc.HRef = ConfigurationManager.AppSettings["domain_file"];
    }

	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
	}
   
}