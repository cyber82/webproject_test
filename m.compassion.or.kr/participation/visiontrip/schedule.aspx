﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="schedule.aspx.cs" Inherits="participation_visiontrip_schedule" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>


<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/participation/visiontrip/schedule.js?v=1.0"></script>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>

    <script type="text/javascript">
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
<!---->
    
    <div class="participation-visiontrip">

        <div class="wrap-year">
        	<span class="prev-year" id="last_year">이전 년도 보기</span>
            <span class="next-year" id="next_year">다음 년도 보기</span>
            
            <div class="crop" id="slide">
                <ul class="list-year">   <!--임시 style-->
                    <li ng-click="getSchedule(y.year, $event)" ng-if="y.year" ng-repeat="y in years" data-year="{{y.year}}"  on-finish-render-filters><span>{{y.year}}<em>년</em></span></li>
                    <!--
                    <li ng-click="getSchedule(2013, $event)"><span>2013<em>년</em></span></li>
                    <li ng-click="getSchedule(2014, $event)"><span>2014<em>년</em></span></li>
                    <li ng-click="getSchedule(2015, $event)"><span>2015<em>년</em></span></li>
                    <li ng-click="getSchedule(2016, $event)" class="selected"><span>2016<em>년</em></span></li>
                    <li ng-click="getSchedule(2017, $event)"><span>2017<em>년</em></span></li>
                    <li ng-click="getSchedule(2018, $event)"><span>2018<em>년</em></span></li>
                    <li ng-click="getSchedule(2019, $event)"><span>2019<em>년</em></span></li>
                    <li ng-click="getSchedule(2020, $event)"><span>2020<em>년</em></span></li>
                    -->
                    <li><span><em></em></span></li>
                </ul>
            </div>    
        </div>
        
        <p class="bu">일정은 항공 및 현지 상황에 따라 변경될 수 있습니다.</p>
        <p class="bu" ng-show="curYear > params.year">음영표시는 교회, 학교, 애드보킷 그룹 등 외부 단체의 요청으로 진행된 비전트립입니다.</p>    <!--과거~전년도 페이지에서만 나타나는 안내 문구-->
        
        <div class="box-schedule" ng-repeat="item in list">
        	<%--<div class="row-header" ng-class="{color1 : item.v_type == 'request'}"> <!--상단 텍스트 흐린유형--> <!--"color1" class 추가-->
            	<strong>{{item.visit_nation}}</strong>
                <p>{{item.start_date | date:'M/d'}}~{{item.end_date| date:'M/d'}}</p>
                <span class="{{item.status}}" ng-bind-html="item.result"></span>
            </div>
            <div class="row-table" ng-show="curYear <= params.year">
            	<span class="th-field" style="width:50%" >예상 트립비용<br /><em>신청비 20만 원 포함</em></span>
                <span class="td-field" style="width:50%">{{item.trip_cost}}</span>
            </div>
            <div class="row-table" ng-show="curYear <= params.year">
            	<span class="th-field" style="width:65%">신청 마감일</span>
                <span class="td-field" style="width:35%">{{item.close_date | lastYear}}</span>
            </div>
            <a class="postview" ng-class="{mt20 : curYear <= params.year}" ng-href="{{item.trip_review}}"  ng-show="item.trip_review">후기 보러 가기</a>--%>
            <div class="row-header" ng-class="{color1 : item.v_type == 'request'}"> <!--상단 텍스트 흐린유형--> <!--"color1" class 추가-->
            	<strong>{{item.visitcountry}}</strong>
                <p>{{item.startdate | date:'M/d'}}~{{item.enddate| date:'M/d'}}</p>
                <span class="{{item.status}}" ng-bind-html="item.result"></span>
            </div>
            <div class="row-table" ng-show="curYear <= params.year">
            	<span class="th-field" style="width:50%" >예상 트립비용<br /><em>신청비 20만 원 포함</em></span>
                <span class="td-field" style="width:50%">{{item.tripcost == '' ? '-' : item.tripcost}}</span>
            </div>
            <div class="row-table" ng-show="curYear <= params.year">
            	<span class="th-field" style="width:65%">신청 마감일</span>
                <span class="td-field" style="width:35%">{{item.closedate | lastYear}}</span>
            </div>
            <a class="postview" ng-class="{mt20 : curYear <= params.year}" ng-href="{{item.trip_review}}"  ng-show="item.trip_review">후기 보러 가기</a>
        </div>
        <div class="box-schedule" ng-hide="list.length">
            데이터가 없습니다.
        </div>
        
		<div class="box-linkpc">
        	<span class="link-pc"><a href="#" id="btn_pc" runat="server">PC 버전 바로가기</a></span><br />
            더욱 자세한 내용은 PC 버전에서 확인하실 수 있습니다.
        </div>
        
    </div>
    
<!--//-->
</div>

</asp:Content>
