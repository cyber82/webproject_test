﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Configuration;

public partial class participation_visiontrip_schedule : MobileFrontBasePage {


	protected override void OnBeforePostBack() {
        btn_pc.HRef = ConfigurationManager.AppSettings["domain_file"] + "/participation/visiontrip/schedule/";
        /*
               base.OnBeforePostBack();
               base.LoadComplete += new EventHandler(list_LoadComplete);

               // 올해
               var year = Request["year"].ValueIfNull(DateTime.Now.ToString("yyyy"));
               current_year.Text = year;
               search_year.Value = year;


               // 작년
               var last = (Convert.ToInt32(year) - 1).ToString();
               if (CheckLastYear(last)) {
                   //last_year.Visible = true;
                   last_year.InnerText = last + "년";
                   //last_year.HRef = "/participation/visiontrip/?year=" + last;
               } else {
                   //last_year.Visible = false;
               }



               // 내년
               var next = (Convert.ToInt32(year) + 1).ToString();
               if (CheckNextYear(next)) {
                   //next_year.Visible = true;
                   next_year.InnerText = next + "년";
                   //next_year.HRef = "/participation/visiontrip/?year=" + next;
               } else {
                   //next_year.Visible = false;
               }


           }


           protected override void OnAfterPostBack() {
               base.OnAfterPostBack();
           }

           protected bool CheckNextYear(string next) {
               bool result = false;
               using (FrontDataContext dao = new FrontDataContext()) {
                   if(dao.visiontrip.Count(p => p.v_type == "vision" && p.open_date.Substring(0, 4) == next) > 0) {
                       result = true;
                   }
               }
               return result;
           }


           protected bool CheckLastYear(string last) {
               bool result = false;
               using (FrontDataContext dao = new FrontDataContext()) {
                   if (dao.visiontrip.Count(p => p.v_type == "vision" && p.open_date.Substring(0, 4) == last) > 0) {
                       result = true;
                   }
               }
               return result;
           */
    }

}