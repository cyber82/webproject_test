﻿$(function () {


});

(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http) {

        $scope.requesting = false;
        $scope.page = 1;
        $scope.rowsPerPage = 100;
        $scope.curYear = new Date().getFullYear();

        $scope.userId = common.getUserId();
        
        $scope.params = {
            page: $scope.page,
            rowsPerPage: $scope.rowsPerPage,
            year: new Date().getFullYear()
        };

        // list
        $scope.getList = function (params) {
            console.log($scope.params.year)
            //$http.get("/api/visiontrip.ashx?t=list", { params: { year: $scope.params.year, type: "" } }).success(function (result) {
            $http.get("/api/visiontrip.ashx?t=schedulelist", { params: { year: $scope.params.year, type: "", userid: $scope.userId } }).success(function (result) {
                $scope.list = [];
                $.each(result.data, function () {
                    if (this.v_type != "pastor") {

                        if (this.open_date) {
                            this.open_date = new Date(this.open_date.replace(/-/g, '/'));
                            this.close_date = new Date(this.close_date.replace(/-/g, '/'));
                            //var now = new Date();
                            //this.status = "state2"
                            //this.result = "신청중"

                            //if (this.open_date > now) {
                            //    this.status = "state3"
                            //    this.result = "예정";
                            //} else if (this.close_date < now) {
                            //    this.status = "state1"
                            //    this.result = "종료";
                            //}
                        } else {
                            //this.status = "state1"
                            //this.result = "종료";
                        }

                        // 2017.06.07 김태형 : 상태(Y:신청중, S:예정, N:마감, E:종료)
                        //switch (this.trip_state) {
                        //    case 'Y': this.status = 'state2'; this.result = '신청중'; break;
                        //    case 'S': this.status = 'state3'; this.result = '예정'; break;
                        //    case 'N': this.status = 'state1'; this.result = '마감'; break;
                        //    case 'E': this.status = 'state1'; this.result = '종료'; break;
                        //    default: this.status = ''; this.result = ''; break;
                        //}
                        switch (this.tripstate) {
                            case 'Apply': this.status = 'state2'; this.result = '신청중'; break;
                            case 'Expect': this.status = 'state3'; this.result = '예정'; this.tripnotice_view = false; break;
                            case 'Deadline': this.status = 'state1'; this.result = '마감'; break;
                            case 'EarlyDeadline': this.status = 'state1'; this.result = '조기마감'; break;
                            case 'Close': this.status = 'state1'; this.result = '종료'; this.tripnoticestatus = "a-disabled"; break;
                            default: this.status = ''; this.result = ''; break;
                        }


                        this.answerClass = "expected"

                        if (this.trip_review == "http://") this.trip_review = "";
                        if (this.trip_review != "" && this.trip_review != null) {
                            this.trip_review = this.trip_review.replace("http://www.", "http://m.")
                        }

                        $scope.list.push(this)
                    }
                })
                console.log($scope.list)
            });

        }


        $scope.getSchedule = function (param, $event) {
            $(".list-year li").removeClass("selected");
            $scope.params.year = param;
            $scope.params.page = 1;
            $scope.getList();

            $($event.currentTarget).addClass("selected");
        }


        $scope.getYears = function () {
            $http.get("/api/visiontrip.ashx?t=get-years", {}).success(function (result) {
                $scope.years = result.data;

                //console.log("getYears",$scope.years);
            })
        }

        $scope.getYears();
        $scope.getList();

        $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {

            var sly = new Sly($("#slide"), {    // Call Sly on frame
                horizontal: 1,
                itemNav: 'centered',
                smart: 1,
                activateOn: 'click',
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 3,
                activatePageOn: 'click',
                speed: 200,
                elasticBounds: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                prev: '#last_year',
                next: '#next_year'
            });
            sly.init();

            $.each($("ul.list-year li"), function (i) {
                var y = $(this).data("year");
                if (y == $scope.curYear) {
                    sly.activate(i);
                    $(this).addClass("selected");
                }
            })


        });

    });

    app.directive('onFinishRenderFilters', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit('ngRepeatFinished');
                    });
                }
            }
        };
    });

    // 신청마감일(올해가 아닌경우 년도 표시)
    app.filter('lastYear', function ($filter) {
        return function (close) {
            var result = $filter('date')(close, "M/d", "ko")
            if (new Date(close).getFullYear() < new Date().getFullYear()) {
                result = $filter('date')(close, "M/d(yyyy)", "ko")
            }
            return result;
        };
    });

})();
