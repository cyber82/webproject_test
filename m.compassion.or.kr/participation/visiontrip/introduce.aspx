﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="introduce.aspx.cs" Inherits="participation_visiontrip_introduce" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub">
        <!---->

        <div class="participation-visiontrip">

            <div class="mb20">
                <img src="/common/img/page/participation/img-visiontrip.jpg" alt="" width="100%" /></div>
            <p class="txt-caption">컴패션을 통해 양육받고 있는 어린이를 직접 만나고 사랑을 나눌 수 있는 컴패션비전트립을 소개합니다.</p>
            <p class="txt-sub">비전트립을 통해 후원자님들은 어린이가 살고 있는 나라를 방문하셔서 어린이가 매일 먹는 음식, 어린이의 가정 환경, 컴패션 양육현장인 컴패션어린이센터 수업 광경을 직접 경험할 뿐 아니라, 사랑스러운 어린이를 직접 안아주실 수 있고 손을 잡아주실 수도 있습니다. 후원자 여러분의 방문은 어린이들의 삶에 있어 그 무엇보다 귀중한 경험이 될 것이며, 후원자 여러분에게도 잊지 못할 시간이 될 것입니다.</p>

            <div class="youtube">
                <iframe src="https://www.youtube.com/embed/PR3qzJIYweQ?rel=0&showinfo=0&wmode=transparent" frameborder="0" allowfullscreen></iframe>
                <!--유투브 샘플url 추후 제작 수정바랍니다-->
            </div>

            <p class="txt-caption2">EXPERIENCE COMPASSION</p>
            <p class="txt-sub">
                비전트립을 통해서 후원자님들은 어린이들이 매일의 삶에서 겪고 있는 ‘가난’이 구체적으로 어떤 모습인지 경험하게 될 것입니다. 또한 후원어린이들의 가정을 방문하여 부모님과 가족을 만나고, 어린이들에게 컴패션 양육을 제공하고 있는 교회의 리더십, 그리고 가장 가까이에서 어린이들을 양육하고 있는 선생님들 및 컴패션 직원들과 직접 대화하면서 가난에 대해 더 깊이 이해하고 어린이들을 돕는다는 것의 의미를 더 깊이 체험하게 될 것입니다.
                <br />
                <br />
                무엇보다 컴패션의 양육을 통해 변화된 어린이들과 가족, 지역사회에서의 선한 열매를 직접 보면서 열악한 환경을 뛰어넘어 일하시는 하나님의 역사를 경험하실 귀한 기회가 될 것입니다.
            </p>

            <p class="txt-caption2">비전트립 프로그램</p>
            <p class="txt-sub">비전트립은 방문 국가, 항공 일정 등에 따라 4박 6일~7박 9일 정도의 기간 동안 진행됩니다. 컴패션 수혜국 국가사무실 방문, 컴패션어린이센터 방문, 1:1리더십결연 학생 및 졸업생들과의 저녁식사, 나눔과 교제(디브리핑) 시간이 일정에 포함되며, 수혜국 상황에 따라 태아·영아생존프로그램 및 다양한 양육보완프로그램을 살펴봅니다. 또한, 후원하고 있는 어린이를 직접 만나 근처 놀이공원, 동물원, 쇼핑몰 등에서 함께 시간을 보내는 후원어린이와의 만남(Fun Day)이 진행되기도 합니다.</p>

            <p class="txt-caption3">비전트립 프로그램 예시 - 필리핀 4박 6일</p>
            <ul class="list-program">
                <li><strong class="txt-day">Day 1</strong>
                    <span class="row-table"><em class="th-field" style="width: 76px">오전</em><span class="td-field">인천 출발, 필리핀 도착</span></span>
                    <span class="row-table"><em class="th-field" style="width: 76px">오후</em><span class="td-field">점심식사, 국가사무실 방문</span></span>
                    <span class="row-table"><em class="th-field" style="width: 76px">저녁</em><span class="td-field">저녁식사, 디브리핑 1)</span></span>
                </li>
                <li><strong class="txt-day">Day 2~4</strong>
                    <span class="row-table"><em class="th-field" style="width: 76px">오전</em><span class="td-field">컴패션어린이센터 방문</span></span>
                    <span class="row-table"><em class="th-field" style="width: 76px">오후</em><span class="td-field">점심식사, 가정방문</span></span>
                    <span class="row-table"><em class="th-field" style="width: 76px">저녁</em><span class="td-field">저녁식사, 디브리핑, LDP 디너 2) (트립 중 1회)</span></span>
                </li>
                <li><strong class="txt-day">Day 5</strong>
                    <span class="row-table"><em class="th-field" style="width: 76px">오전~오후</em><span class="td-field">Fun Day 3)</span></span>
                    <span class="row-table"><em class="th-field" style="width: 76px">저녁</em><span class="td-field">필리핀 출발</span></span>
                </li>
            </ul>
            <p class="txt-bottom">
                1) 디브리핑 - 나눔과 교제의 시간<br />
                2) LDP디너 - 1:1리더십결연 학생 및 졸업생들과의 저녁식사<br />
                3) Fun Day - 후원어린이와의 만남의 시간
            </p>

            <div class="wrap-bt"><a class="bt-type6" style="width: 100%" href="/participation/event?k_word=비전트립">진행 중인 비전트립 이벤트 보기</a></div>


            <p class="txt-caption3 mt30">비전트립의 자세한 후기가 궁금하신가요?</p>
            <a href="/sympathy/vision/vision/" runat="server" id="btn_vision">
                <img src="/common/img/page/participation/banner-visiontrip.jpg" alt="비전트립 후기 보러가기" /></a>

            <div class="box-tel">
                <p class="txt-title">문의</p>
                <a class="tel" href="tel:02-3668-3453"><strong>전화</strong> 02)3668-3453, 3447</a>
                <a class="fax" href="tel:02-3668-3501"><strong>팩스</strong> 02)3668-3501</a>
                <a class="email" href="mailto:visiontrip@compassion.or.kr"><strong>이메일</strong> visiontrip@compassion.or.kr</a>
                <div class="inner-line">
                    <p class="bu">자주 묻는 질문을 이용하시면 더 많은 궁금증을 해결하실 수 있습니다</p>
                    <div class="wrap-bt"><a class="bt-type4" style="width: 130px" href="/customer/faq/?s_type=J">FAQ 바로가기</a></div>
                </div>
            </div>

            <div class="box-linkpc">
                <span class="link-pc"><a href="#" runat="server" id="btn_pc">PC 버전 바로가기</a></span><br />
                더욱 자세한 내용은 PC 버전에서 확인하실 수 있습니다.
            </div>


        </div>

        <!--//-->
    </div>

</asp:Content>
