﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class participation_visiontrip_default : MobileFrontBasePage {
	
	protected override void OnBeforePostBack() {
		if(Request.UrlReferrer != null && Request["lnb"] != "Y") {
			if(Request.UrlReferrer.AbsolutePath.IndexOf("/participation/visiontrip/") > -1) {
				Response.Redirect("/participation/");
			}
		}
		Response.Redirect("/participation/visiontrip/introduce");

	}

}