﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="participation_sponsor_default " Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script>
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
   <div class="wrap-sectionsub">
<!---->
    
    <div class="participation-company">
    	
        <div class="top-intro sectionsub-margin1">
            <div class="div-table">
            	<div class="div-cell">
                	Great Power Great Responsibility<br />
					<span>좋은 기업에서 위대한 기업으로</span>
                </div>
            </div>
        </div>

        <p class="txt-caption">컴패션과 함께하는<br />
			사회공헌 프로젝트
        </p>
        <p class="txt-caption2">기업사회공헌 참여 방법</p>
        <div class="photo"><img src="/common/img/page/participation/img-company.jpg" alt="" width="100%" /></div>
        
        <ul class="list-bu">
        	<li><strong>기업 명의 1:1 어린이 양육 :</strong> 기업의 이름으로 어린이를 양육·후원하는 프로그램입니다.</li>
            <li><strong>임직원 참여 후원(1:1 어린이 양육/편지 후원/끝전 모으기) :</strong> 기업 
                강연 및 사내 캠페인을 통해 임직원의 자발적인 참여로 진행되는 
                후원 프로그램입니다. 급여의 일부를 후원하거나, 편지 후원으로 
                함께할 수 있습니다.
            </li>
            <li><strong>매칭 기프트 :</strong> 임직원들이 후원하는만큼 기업도 함께 후원하는 프로그램입니다.</li>
            <li><strong class="color1">기업 시그니처 프로젝트 :</strong> 기업 맞춤형 단독 후원으로 기업 사업 
                분야 또는 사회공헌 방향에 맞춰 단독으로 프로젝트를 후원하고 
                현지에 기업 명판을 설치할 수 있습니다.
            </li>
        </ul>
        
        <p class="txt-caption2 mt10">기업사회공헌 협력 프로그램</p>
        <div class="photo"><img src="/common/img/page/participation/img-company2.jpg" alt="" width="100%" /></div>
        
        <ul class="list-bu">
        	<li><strong>Compassion Weekday(기업 강연) :</strong> 임직원을 대상으로 나눔에 대한 의미를 전달하고 참여를 독려합니다.<br />
				* 강사는 일정에 따라 변동 가능
            </li>
            <li><strong>컬래버레이션 :</strong> 브랜드 제품과의 컬래버레이션을 통해 가치를 담은 제품 출시로 후원에 동참합니다.</li>
        </ul>
        
        <p class="txt-caption mt10">기업의 가치 상승과 지속 가능한<br />
			사회공헌 캠페인
        </p>
        <p class="txt-caption2">25개국 어린이들의 자립을 위한 기업사회공헌 캠페인</p>
        
        <ul class="list-bu2">
        	<li>수료(졸업)를 3~5년 남겨둔 학생들을 위한 Finishing well   프로그램</li>
            <li>졸업을 앞두고 후원이 중단된 10대 학생들을 기업의 이름으로 
                지정 후원하여 그들이 학교를 마치고 자립하여 경제활동을 하고, 
                나아가 지역사회의 리더로 성장할 수 있도록 돕는 것이 본 
                캠페인의 목표입니다.
            </li>
        </ul>
        
        <p class="txt-caption2 mt10">참여방법</p>
        <div class="box-gray">
        	<ul class="list-bu2">
                <li>후원 국가/지역을 선발</li>
                <li>후원 기간 지정</li>
                <li>모니터링 트립</li>
            </ul>	
        </div>
		<div class="sectionsub-margin1"><img src="/common/img/page/participation/img-company3.jpg" alt="" width="100%" /></div>
        
		 <div class="box-tel">
        	<p class="txt-title">문의</p>
            <a class="tel" href="tel:02-3668-3457"><strong>전화</strong> 02)3668-3457</a>
            <a class="email" href="mailto:csv@compassion.or.kr"><strong>이메일</strong> csv@compassion.or.kr</a>
        </div>

		<div class="box-linkpc">
        	<span class="link-pc"><a href="#" runat="server" id="btn_pc">PC 버전 바로가기</a></span><br />
            더욱 자세한 내용은 PC 버전에서 확인하실 수 있습니다.
        </div>
        
    </div>
    
<!--//-->
</div>
</asp:Content>
