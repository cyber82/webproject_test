﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Configuration;

public partial class participation_sponsor_default : MobileFrontBasePage {


	protected override void OnBeforePostBack() {

        btn_pc.HRef = ConfigurationManager.AppSettings["domain_file"] + "/participation/sponsor/";

    }


    protected override void OnAfterPostBack() {
	}
}