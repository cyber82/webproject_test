﻿var passwordValidation = {

    checkWithAlert: function (obj1, obj2, min, max) {


        var result = passwordValidation.check(obj1, obj2, min, max);


        if (!result.result) {
            alert(result.msg);
            obj1.focus();
            return false;
        }

        return true;
    },

    check: function (obj1, obj2, minv, maxv) {

        var pw = obj1.val();
        var validResult = 0;

        var opt = {
            lower: 0,
            upper: 0,
            alpha: 1, /* lower + upper */
            numeric: 1,
            special: 1,
            lngth: [minv, maxv],
            custom: [ /* regexes and/or functions */],
            badWords: ["< > , . / [ ] { } \\ ; : \' \" ?", "<", ">", ",", ".", "/", "[", "]", "{", "}", "\\", ";", ":", "\'", "\"", "?"],
            badSequenceLength: 4,
            noQwertySequences: false,
            noSequential: false
        };

        /*
        for (var property in options)
        opt[property] = options[property];
        */
        var re = {
            lower: /[a-z]/g,
            upper: /[A-Z]/g,
            alpha: /[A-Z]/gi,
            numeric: /[0-9]/g,
            special: /[\W_]/g
        },
         rule, i;

        // enforce min/max length
        if (pw.length < opt.lngth[0] || pw.length > opt.lngth[1]) {
            validResult = 1;
        }


        // enforce lower/upper/alpha/numeric/special rules

        for (rule in re) {
            if ((pw.match(re[rule]) || []).length < opt[rule]) {
                validResult = 2;
                break;
            }
        }

        // enforce word ban (case insensitive)
        for (i = 0; i < opt.badWords.length; i++) {
            if (pw.toLowerCase().indexOf(opt.badWords[i].toLowerCase()) > -1) {
                validResult = 3;
                break;
            }
        }

        // enforce the no sequential, identical characters rule
        if (opt.noSequential && /([\S\s])\1/.test(pw)) {
            validResult = 4;
        }


        // enforce alphanumeric/qwerty sequence ban rules
        if (opt.badSequenceLength) {
            var lower = "abcdefghijklmnopqrstuvwxyz",
             upper = lower.toUpperCase(),
             numbers = "0123456789",
             qwerty = "qwertyuiopasdfghjklzxcvbnm",
             start = opt.badSequenceLength - 1,
             seq = "_" + pw.slice(0, start);
            for (i = start; i < pw.length; i++) {
                seq = seq.slice(1) + pw.charAt(i);
                if (
                 lower.indexOf(seq) > -1 ||
                 upper.indexOf(seq) > -1 ||
                 numbers.indexOf(seq) > -1 ||
                 (opt.noQwertySequences && qwerty.indexOf(seq) > -1)
                ) {
                    validResult = 5;
                    break;
                }
            }
        }

        // enforce custom regex/function rules
        for (i = 0; i < opt.custom.length; i++) {
            rule = opt.custom[i];
            if (rule instanceof RegExp) {
                if (!rule.test(pw)) {
                    validResult = 5;
                    break;
                }
            } else if (rule instanceof Function) {
                if (!rule(pw)) {
                    validResult = 5;
                    break;
                }
            }
        }


        if (validResult == 0) {
            return { result: true, msg: "" };

        } else {
            if (validResult == 3) {
                return { result: false, msg: opt.badWords[0] + " 문자는 사용할 수 없습니다." };
            } else if (validResult == 4 || validResult == 5) {
                return { result: false, msg: "연속된 4자리 이상의 문자와 숫자는 사용할 수 없습니다." };
            } else {
                return { result: false, msg: "띄어쓰기 없이 영문, 숫자, 특수문자 3가지 조합으로 " + minv + "~" + maxv + "자이내(대소문자 구별)로 입력해주세요." + validResult };
            }
        }
    }

}
