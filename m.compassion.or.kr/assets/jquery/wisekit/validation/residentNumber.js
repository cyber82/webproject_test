﻿/*
남성
1: 국내 1900년대 생
3: 국내 2000년대 생
5: 외국 1900년대 생
7: 외국 2000년대 생 또는 (외국인등록증 및 외국국적동포 국내거소신고증이 없을 경우) 조합주민번호
9: 국내 1800년대 생
 
여성
2: 국내 1900년대 생
4: 국내 2000년대 생
6: 외국 1900년대 생
8: 외국 2000년대 생 또는 (외국인등록증 및 외국국적동포 국내거소신고증이 없을 경우) 조합주민번호
0: 국내 1800년대 생
*/

var residentNumberValidation = {
	
	result : {korean:1,foreign:2,invalid:-1,invalid_NaN:-2} , 
	gender : {male : 'M',female : 'F' , invalid : -1} , 
	
	msg: {
		ko: {
			invalid: "잘못된 주민번호입니다.",
			invalid_NaN : "숫자만 입력해주세요." , 
		},
		en: {
			invalid: "invalid resident number format",
			invalid_NaN: "You can enter only numbers",
		}
	},

	checkWithAlert: function (lang, obj1, obj2) {
		var val = obj1.val() + obj2.val();
		var result = residentNumberValidation.check(val);

		var returnVal = {
			success: false,
			result: result,
			gender: residentNumberValidation.gender.invalid
		};

		switch (result) {
			case residentNumberValidation.result.korean:
				returnVal.success = true;
				returnVal.gender = residentNumberValidation.getGender(val);
				return returnVal;
			case residentNumberValidation.result.foreign:
				returnVal.success = true;
				returnVal.gender = residentNumberValidation.getGender(val);
				return returnVal;
			case residentNumberValidation.result.invalid_NaN:
				alert(eval("residentNumberValidation.msg." + lang + ".invalid_NaN"));
				obj1.focus();
				returnVal.success = false;
				return returnVal;
			default:
				alert(eval("residentNumberValidation.msg." + lang + ".invalid"));
				obj1.focus();
				returnVal.success = false;
				return returnVal;
		}

	},

	check : function (no) {
		
		if (!residentNumberValidation.isNumeric(no))
			return residentNumberValidation.result.invalid_NaN;
		
		if (residentNumberValidation.isKorean(no))
			return residentNumberValidation.result.korean;
		
		if (residentNumberValidation.isForeign(no))
			return residentNumberValidation.result.foreign;

		return residentNumberValidation.result.invalid;
	},

	getGender : function(no){
		if (no.length != 13)
			return residentNumberValidation.gender.invalid;

		var s = parseInt(no.substr(6, 1));
		return (s % 2 == 0) ? residentNumberValidation.gender.female : residentNumberValidation.gender.male;
	} , 

	isKorean: function (no) {
		if (no.length != 13)
			return false;
		var s1 = no.substr(0, 6);
		var s2 = no.substr(6, 7);

		n = 2;
		sum = 0;
		for (i=0; i<s1.length; i++)
			sum += parseInt(s1.substr(i, 1)) * n++;
		for (i=0; i<s2.length-1; i++) {
			sum += parseInt(s2.substr(i, 1)) * n++;
			if (n == 10) n = 2;
		}
		c = 11 - sum % 11;
		if (c == 11) c = 1;
		if (c == 10) c = 0;
		if (c != parseInt(s2.substr(6, 1))) return false;
		else return true;
	},

	isForeign: function (no) {
		if (no.length != 13)
			return false;
		var sum = 0;
		var odd = 0;
		buf = new Array(13);
		for (i = 0; i < 13; i++) { buf[i] = parseInt(no.charAt(i)); }
		odd = buf[7] * 10 + buf[8];
		if (odd % 2 != 0) { return false; }
		if ((buf[11] != 6) && (buf[11] != 7) && (buf[11] != 8) && (buf[11] != 9)) {
			return false;
		}
		multipliers = [2, 3, 4, 5, 6, 7, 8, 9, 2, 3, 4, 5];
		for (i = 0, sum = 0; i < 12; i++) { sum += (buf[i] *= multipliers[i]); }
		sum = 11 - (sum % 11);
		if (sum >= 10) { sum -= 10; }
		sum += 2;
		if (sum >= 10) { sum -= 10; }
		if (sum != buf[12]) { return false }
		return true;
	},

	isNumeric : function(s) {
		for (i=0; i<s.length; i++) {
			c = s.substr(i, 1);
			if (c < "0" || c > "9") return false;
		}
		return true;
	}
}
