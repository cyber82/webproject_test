﻿/*
사용법
$(".u_is_agree_sms").customRadio();


$(".u_is_agree_sms").customRadio(
	{options}
);

id로 묶여있는 label태그에 radio태그의 이름이 data-group값으로 들어가야합니다.
<input type="radio" name="name" id="id"/><label for="id" data-group="name"></label>
*/
(function ($) {
	$.widget("ui.customRadio", {
		options: {
			on: '/common/image/common/radio_on.gif',
			off: '/common/image/common/radio_off.gif',
			width:'10px',
			height: '10px',
			style: ''
		},


		_init: function (options) {

			var self = this, o = this.options;

			$.each(this.element, function () {
			    var $element = $(this);

			    $(this).hide();

				var id = $(this).attr("id");
				var name = $(this).attr("name");
				var src = $("#"+name).val() == $(this).val() ? o.on : o.off;
				var group = $(this).attr("name");

				o.group = group;



				var img = $('<img src="' + src + '" class="custom-radio-img" data-group="' + group + '" data-custom-radio-id="' + id + '"  style="display:inline-block;width:' + o.width + ';height:' + o.height + ';' + o.style + '" />');
				img.click(function () {
				    //disabled인 경우 클릭이벤트 없음
				    if (!$element.is(":disabled")) {
				        self.clear(o);
				        self.check(this, o);
				        self.radioControl(id);
				    }
				});


				$('label[for="' + id + '"]').click(function () {
				    //disabled인 경우 클릭이벤트 없음
				    if (!$element.is(":disabled")) {
				        self.clear(o);
				        self.check($("img[data-custom-radio-id='" + id + "']"), o);
				        self.radioControl(id);
				    }
                });


				$(this).after(img);
			});




		},

		clear: function (options) {
			$.each($(".custom-radio-img[data-group='"+options.group+"']"), function () {
				$(this).attr('src', options.off);
			});
		},

		check: function (obj, options) {
			$(obj).attr('src', options.on);
		},

		uncheck: function (obj, options) {
			$(obj).attr('src', options.off);
		},

		radioControl: function (radio_id) {
			$("#" + radio_id).trigger("click");
		}
    });
}(jQuery));