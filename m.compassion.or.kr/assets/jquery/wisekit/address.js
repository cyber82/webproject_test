(function ($) {
	$.searchAddress = function (options) {
		var settings = $.extend({
			url: 'https://www.juso.go.kr/addrlink/addrLinkApiJsonp.do',
			currentPage: 1,
			countPerPage: 5,
			confirmKey: 'TESTJUSOGOKR'
		}, options);

		var result = {
			success: false,
			message: "",
			total : 0,
			data : []
		}

		$.ajax({
			url: settings.url
				, type: "post"
				, data: settings
				, dataType: "jsonp"
				, crossDomain: true
				, success: function (xml) {

					if (xml != null) {
						var data = $.parseXML(xml.returnXml);

						//에러
						if ($(data).find("errorCode").text() != "0") {
							result.message = $(data).find("errorMessage").text();
							return result;
						}

						//결과없음
						var totalCount = $(data).find("totalCount").text();
						if (totalCount < 1) {
							result.success = true;
							return result;
						}

						result.total = totalCount;
						result.data = data;
						return result;
					}
				}

				, error: function (xhr, status, error) {
					result.message = "error";
					return result;
				}
		});
	};
})(jQuery);