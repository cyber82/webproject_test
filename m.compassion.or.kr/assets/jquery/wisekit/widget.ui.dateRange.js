﻿
(function ($) {

	$.widget("ui.dateRange", {

		_init: function (begin, end) {

			var self = this, o = this.options;
			
			$.each($(o.buttons), function () {
				var day = parseInt($(this).attr("data-day"));
				var month = parseInt($(this).attr("data-month"));

				var sender = $(this);

				$(this).click(function () {
					self.setDate(self, month, day);

					if (o.onClick) {
						o.onClick(sender);
					}
					return o.eventBubble || false;
				});
			});

		},
		
		setDate: function (self , month , day) {
			var o = self.options;

			var d = new Date();
			d.setMonth(d.getMonth() - month);
			d.setDate(d.getDate() - day);
			
			self.element.val(self._dateToString(o.format , d));
			if (o.end && $(o.end)) {
				$(o.end).val(self._dateToString(o.format , new Date()));
			}
		},

		_dateToString: function (format, d) {
			if (!format)
				format = "ymd";

			var months = new Array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
			var date = ((d.getDate() < 10) ? "0" : "") + d.getDate();
			var result = d.getFullYear() + "-" + months[d.getMonth()] + "-" + date;
			if (format == "ym")
				result = d.getFullYear() + "-" + months[d.getMonth()];
			return result;
		}

	});


})(jQuery);



