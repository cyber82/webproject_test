﻿/*
data : [{id:"#id" , type:"numeric"},{id:"#id" , type:"alpha-numeric"},{id:"#id" , type:"alphabet"} , {id:"#id"}]
*/
var validateForm = function (data) {

	var result = true;

	$.each(data, function () {
		
		if (!result)
			return false;
		var obj = $(this.id);

		if (!this.empty) {
			if (obj.val() == "") {
				if (this.msg) {
					alert(this.msg);
				} else {
					alert(msg("fc_nodata"));
				}
				obj.focus();

				result = false;
			}
		}

		if (!result)
			return false;

		if (this.type && obj.val() != "") {
			switch (this.type) {
				case "no":
					var val = obj.val().match(/[0-9]+/g);
					if (obj.val() != val) {
						alert(msg("fc_numeric"));
						obj.val("");
						obj.focus();
						result = false;
					}
					break;
				case "numeric":
					var val = obj.val().match(/[0-9\-]+/g);
					if (obj.val() != val) {
						alert(msg("fc_numeric"));
						obj.val("");
						obj.focus();
						result = false;
					}
					break;
				case "decimal":
					//var val = obj.val().match(/[0-9.]+/g);
					var val = obj.val().match(/^[0-9]+\.?[0-9]*$/g);
					if (obj.val() != val) {
						alert(msg("fc_decimal"));
						obj.val("");
						obj.focus();
						result = false;
					}
					break;
				case "version":
					//var val = obj.val().match(/[0-9.]+/g);
					var val = obj.val().match(/^[0-9\.]+/g);
					if (obj.val() != val) {
						alert(msg("fc_decimal"));
						obj.val("");
						obj.focus();
						result = false;
					}
					break;
				case "alpha-numeric":
					var val = obj.val().match(/[a-zA-Z0-9]+/g);
					if (obj.val() != val) {
						alert(msg("fc_alpha_numeric"));
						obj.focus();
						result = false;
					}
					break;
				case "alphabet":
					var val = obj.val().match(/[a-zA-Z]+/g);
					if (obj.val() != val) {
						alert(msg("fc_alphabet"));
						obj.focus();
						result = false;
					}
					break;
				case "phone":
					var val = obj.val().match(/[0-9\+\-\s]+/g);
					if (obj.val() != val) {
						alert(msg("fc_phone"));
						obj.focus();
						result = false;
					}
					break;
				case "phone_local":
					var val = obj.val().match(/[0-9]{10,11}/g);
					if (obj.val() != val) {
						
						alert(msg("fc_mphone_local"));
						obj.focus();
						result = false;
					}
					break;
				case "email":
					
					if (!/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(obj.val())) {
						alert(msg("fc_email"));
						obj.focus();
						result = false;
					}
					break;

				case "url":

					if (!/^(https?):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(obj.val())) {
						alert(msg("fc_url"));
						obj.focus();
						result = false;
					}
					break;

				case "pwd":
					if (obj.val().length < 4) {
						alert(msg("fc_pwd_at_least_4digit"));
						obj.focus();
						result = false;
					}
					break;

				case "yyyy.mm.dd":
					if (!/^[0-9]{4}\.[0-9]{2}\.[0-9]{2}$/.test(obj.val())) {
						alert(msg("fc_yyyy_mm_dd"));
						obj.focus();
						result = false;
					}
					break;

				case "yyyy.mm":
					if (!/^[0-9]{4}\.[0-9]{2}$/.test(obj.val())) {
						alert(msg("fc_yyyy_mm"));
						obj.focus();
						result = false;
					}
					break;

				case "yyyy":
					if (!/^[0-9]{4}$/.test(obj.val())) {
						alert(msg("fc_yyyy"));
						obj.focus();
						result = false;
					}
					break;

				case "mm":
					if (!/^[0-9]{2}$/.test(obj.val())) {
						alert(msg("fc_mm"));
						obj.focus();
						result = false;
					}
					break;

				case "dd":
					if (!/^[0-9]{2}$/.test(obj.val())) {
						alert(msg("fc_mm"));
						obj.focus();
						result = false;
					}
					break;
			}
		}
	});

	return result;
}

var buildQuery = function (arg, auto_binding) {

	var data = {};

	if (arg) {
		$.each(Object.keys(arg), function (i, v) {
			eval("data." + v + "=" + "arg." + v);
		});
	}

	if (auto_binding) {
		
		$.each($("select"), function () {
			var obj = $(this);

			if (obj.hasClass("ignore_binding")) return;

			if (obj.attr("key")) {
				eval("data." + obj.attr("key") + "='" + obj.val() + "'");

			}
		});

		$.each($("input"), function () {

			var obj = $(this);

			if (obj.hasClass("ignore_binding")) return;

			try {
				var key = obj.attr("name").substr(obj.attr("name").lastIndexOf("$") + 1, obj.attr("name").length - obj.attr("name").lastIndexOf("$") - 1);
				if (obj.attr("key")) {
					key = obj.attr("key");
				}
				var name = obj.attr("name");

				switch (obj.attr("type")) {
					case "text":

						if (obj.val() != "")
							eval("data." + key + "='" + escape(obj.val()) + "'");
						break;
					case "radio":

						var value = $("input[name='" + name + "']:checked").val();
						eval("data." + key + "='" + value + "'");
						break;
					case "checkbox":

						var value = "";
						if (obj.attr("key")) {
							key = obj.attr("key");
							$("#" + obj.attr("id")).attr("name", key);
							name = obj.attr("name");
							$.each($("input[name='" + name + "']:checked"), function () {
								if ($(this).val() != "")
									value += "," + $(this).val();
							});
							if (value.length > 0)
								value = value.substring(1);

						} else {
							var selected = $("#" + obj.attr("id") + ":checked");
							if (selected.length > 0) {
								value = selected.val();
							}
							if (value == "on") value = "1";
						}

						eval("data." + key + "='" + value + "'");
						break;
				}
			} catch (e) { }
		});
	}

	return decodeURIComponent($.param(data));
}

var goPage = function (uri, arg, arg2 , hash) {
	var auto_binding = arg2 == false ? false : true;
	var url = uri + "?" + buildQuery(arg, auto_binding);
	if (hash) url += "#" + hash;
	location.href = url;
	return false;
}

var openPage = function (uri, arg, o) {
	var path = uri + "?" + buildQuery(arg, false);
	window.open(path, path, o);
}

var href = function (path) {
	location.href = "/" + lang() + path;
}

/**
asp.net 은 checkboxList의  name 에 index 를 부여하기때문에, querystring 으로 넘길때 
각각 별개의 값을 넘어가게 된다.
key 속성을 부여해서 같은 name 으로 처리되게 한다.
**/
var addCheckBoxKey = function (id) {
	$.each($("#" + id).find("input[type='checkbox']"), function () {
		$(this).attr("key", id);
	});
}
