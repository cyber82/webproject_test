String.prototype.string = function (len) { var s = '', i = 0; while (i++ < len) { s += this; } return s; };
String.prototype.zf = function (len) { return "0".string(len - this.length) + this; };
Number.prototype.zf = function (len) { return this.toString().zf(len); };

(function ($) {

	// 폼 유효성
	// options { match: true, message: 오류시 메세지, pattern: 정규식 }
	// $(input).formatValidate({ match: true, message: "숫자만 입력 가능합니다.", pattern: /[0-9\-]+/g });
	$.fn.formatValidate = function (options) {
		return this.each(function () {

			$(this).focusout(function () {
				if ($(this).val() != "") {
					var fail = false;
					if (options.match) {
						var val = $(this).val().match(options.pattern);
						if ($(this).val() != val) fail = true;
					} else {
						if (!options.pattern.test($(this).val())) fail = true;
					}

					if (fail) {
						alert(options.message);
						$(this).val("");
						$(this).focus();
					}
				}
			});
		});
	};


	// 글자 카운트 
	// options { limit: 5000 }
	// $(textarea).textCount($(카운트표시엘리먼트), { limit: 제한될 글자수 });
	$.fn.textCount = function (countViewer, options) {
		countViewer.text($(this).val().length);
		return $(this).keyup(function () {

			var length = $(this).val().length;
			var value = $(this).val();
			if (options.limit && $(this).val().length > options.limit) {
				length = options.limit;
				value = value.substr(0, options.limit);
			}
			countViewer.text(length);
			$(this).val(value);
		}).focus(function () {
			var length = $(this).val().length;
			var value = $(this).val();
			if (options.limit && $(this).val().length > options.limit) {
				length = options.limit;
				value = value.substr(0, options.limit);
			}
			countViewer.text(length);
			$(this).val(value);

		}).blur(function () {
			var length = $(this).val().length;
			var value = $(this).val();
			if (options.limit && $(this).val().length > options.limit) {
				length = options.limit;
				value = value.substr(0, options.limit);
			}
			countViewer.text(length);
			$(this).val(value);

		})
	};


	// byte 카운트 
	// options { limit: 5000 }
	// $(textarea).byteCount($(카운트표시엘리먼트), { limit: 제한될 바이트수 });
	$.fn.byteCount = function (countViewer, options) {
		countViewer.text(getByteLength($(this).val()));
		return $(this).keyup(function () {

			countViewer.text(getByteLength($(this).val()));

		}).focus(function () {
			var length = getByteLength($(this).val());
			var value = $(this).val();
			if (options && options.limit && length > options.limit) {
				length = options.limit;
				value = value.substr(0, options.limit);
			}
			countViewer.text(length);
			$(this).val(value);

		}).blur(function () {

			var length = getByteLength($(this).val());
			var value = $(this).val();
			if (options && options.limit && length > options.limit) {
				length = options.limit;
				value = value.substr(0, options.limit);
			}
			countViewer.text(length);
			$(this).val(value);

		})
	};


	// 날자에 . 추가
	// options "yyyy.mm" or "yyyy.mm.dd"
	// $(input).dateFormat("yyyy.mm");
	$.fn.dateFormat = function (options) {
		return $(this).keyup(function (e) {


			var $this = $(this);
			var val = $this.val().replace(/[^0-9]/gi, "")

			// 정상적인 위치에 점 찍은경우
			if ((val.length == 4 || val.length == 6) && e.which == 190) return;

			var array = [val.substr(0, 4), val.substr(4, 2), val.substr(6, 2)];


			// yyyy.mm 이면 dd제거
			for (var i = 0; i < array.length; i++) {
				if (array[i] == "") {
					array.splice(i, 1);
					i--;
				}
			}
			$(this).val(array.join("."));
		});
	};

	// 금액에 ',' 추가
	// $(input).currencyFormat();
	$.fn.currencyFormat = function () {

		return this.each(function () {
			var $this = $(this);
			$(this).keyup(function (e) {
				var val = $(this).val().replace(/[^0-9]/gi, "");
				if (val == "") return;

				if (isNaN(val) && val != 0) {
					$(this).val("");
					alert("숫자만 입력가능합니다.");
					return;
				}

				$this.val(Number(val).toLocaleString('en'));
			});
		});
	}

	// 전화번호에 '-' 추가
	// $(input).mobileFormat();
	$.fn.mobileFormat = function () {
		$(this).keydown(function (e) {
			//alert(e.keyCode)
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
				// Allow: Ctrl+A
				(e.keyCode == 65 && e.ctrlKey === true) ||
				// Allow: Ctrl+C
				(e.keyCode == 67 && e.ctrlKey === true) ||
				// Allow: Ctrl+X
				(e.keyCode == 88 && e.ctrlKey === true) ||
				// Allow: home, end, left, right
				(e.keyCode >= 35 && e.keyCode <= 39)) {
				// let it happen, don't do anything
				return;
			}

			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		}).keyup(function () {
			var value = $(this).val().replace(/[^0-9]/gi, "")
			if (value.length < 4) {
				$(this).val(value);
			} else if (value.length < 7) {
				$(this).val(value.substr(0, 3) + "-" + value.substr(3, 3));
			} else if (value.length < 11) {
				value = value.substr(0, 3) + "-" + value.substr(3, 3) + "-" + value.substr(6, 4);
				$(this).val(value);
			} else if (value.length < 12) {
				value = value.substr(0, 3) + '-' + value.substr(3, 4) + '-' + value.substr(7, 4);
				$(this).val(value);
			} else {
				value = value.substr(0, 12);
				value = value.substr(0, 3) + '-' + value.substr(3, 4) + '-' + value.substr(7, 4);
				$(this).val(value);
			}
		})

		/* 진성 수정 
		this.keyup(function (e) {
			var value = $(this).val().replace(/[^0-9]/gi, "")
			if (value.length < 4) {
				$(this).val(value);
			} else if (value.length < 7) {
				$(this).val(value.substr(0, 3) + "-" + value.substr(3, 3));
			} else if (value.length < 11) {
				value = value.substr(0, 3) + "-" + value.substr(3, 3) + "-" + value.substr(6, 4);
				$(this).val(value);
			} else if (value.length < 12) {
				value = value.substr(0, 3) + '-' + value.substr(3, 4) + '-' + value.substr(7, 4);
				$(this).val(value);
			} else {
				value = value.substr(0, 12);
				value = value.substr(0, 3) + '-' + value.substr(3, 4) + '-' + value.substr(7, 4);
				$(this).val(value);
			}
		});
		*/
	}

	// 카운트 버튼
	// $(input).buttonCounter($(minus), $(plus), {max:5, min:0});
	$.fn.buttonCounter = function (minus, plus, option) {

		var count = parseInt($(this).val()) || 0;
		var $this = $(this);

		minus.click(function () {
			if (count - 1 >= option.min) $this.val(--count);
		});

		plus.click(function () {
			if (count + 1 <= option.max)  $this.val(++count);
			
		})

		return this;
	}

	
}(jQuery));