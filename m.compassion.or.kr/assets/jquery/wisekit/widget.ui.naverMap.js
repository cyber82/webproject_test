﻿
(function ($) {
	$.widget("ui.naverMap", {

		options: {
			icon: 'http://static.naver.com/maps2/icons/pin_spot2.png',
			icon_width:28,
			icon_height: 37,
			circle: {
				show: false,
				radius : 100
			},
			enableZoom: true,
			addMarker : []
		},

		_init: function (options) {
			var self = this, o = this.options;
			if (!o.title)
				o.title = "";

			var oPoint = new nhn.api.map.LatLng(o.lat, o.lng);

			nhn.api.map.setDefaultPoint('LatLng');
			oMap = new nhn.api.map.Map(self.element[0], {
				point: oPoint,
				zoom: o.zoom,
				enableWheelZoom: true,
				enableDragPan: true,
				enableDblClickZoom: false,
				mapMode: 0,
				activateTrafficMap: false,
				activateBicycleMap: false,
				minMaxLevel: [1, 14],
				size: new nhn.api.map.Size(o.w, o.h)
			});


			if (o.enableZoom) {
				var mapZoom = new nhn.api.map.ZoomControl();
				mapZoom.setPosition({ left: 40, bottom: 40 });
				oMap.addControl(mapZoom);
			}
            

			var oSize = new nhn.api.map.Size(o.icon_width, o.icon_height);
			var oOffset = new nhn.api.map.Size(14, 37);
			var oIcon = new nhn.api.map.Icon(o.icon, oSize, oOffset);
			var oMarker = new nhn.api.map.Marker(oIcon, { title: o.title });
			oMarker.setPoint(oPoint);
			oMap.addOverlay(oMarker);

			if (o.title != "") {
				var oLabel = new nhn.api.map.MarkerLabel();
				oMap.addOverlay(oLabel);
				oLabel.setVisible(true, oMarker);
			}

			if (o.callback) {
				o.callback();
			}
		}

    });

    
   

}(jQuery));