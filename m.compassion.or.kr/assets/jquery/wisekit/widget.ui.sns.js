﻿/*
사용법
<script type="text/javascript" src="/assets/jquery/wisekit/widget.ui.sns.js"></script>

0. el 정의
<a href="#"  data-role="sns" data-provider="tw" data-url="">트위터공유</a>
: data-url 이 없는 경우는 property="og:url" 의 값으로 대체

1. 생성
$("body").sns({
	kakaoKey: kakaoKey,
	facebookKey: facebookKey,
	onSuccess: function (provider, response) {
		if (provider == "ks" || provider == "fb") {
			alert("공유되었습니다.");
		}
		console.log("success", provider, response);
	}
});

2. UI변경으로 새로운 공유항목 추가시
var sns = $("body").data("ui-sns");
console.log(sns);
sns.reload();


*/
(function ($) {
    $.widget("ui.sns", {

        instance: null,
        options: {
            isApp: false,
            kakaoKey: '',
            facebookKey: '',
            onSuccess: function (provider, response) { }
        },

        reload: function () {
            var self = this.instance;

            self._setEvents(self);
        },

        _init: function () {

            this.instance = this;
            var self = this, o = this.options;

            $.getScript("https://developers.kakao.com/sdk/js/kakao.min.js", function (data, textStatus, jqxhr) {
                //console.log(data); // Data returned
                //	console.log(textStatus); // Success
                //	console.log(jqxhr.status); // 200

                Kakao.init(o.kakaoKey);	// kakao 

            });

            window.fbAsyncInit = function () {
                FB.init({
                    appId: o.facebookKey,
                    xfbml: true,
                    version: 'v2.6'
                });
            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) { return; }
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/ko_KR/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));


            self._setEvents(self);
        },

        _setEvents: function (self) {
            var o = self.options;

            var targets = self.element.find("[data-role='sns']");
            $.each(targets, function () {
                var provider = $(this).data("provider");
                switch (provider) {
                    case "ks":
                        self._bindKakaoStory(self, $(this));
                        break;
                    case "kt":
                        self._bindKakaoTalk(self, $(this));
                        break;
                    case "fb":
                        self._bindFacebook(self, $(this));
                        break;
                    case "tw":
                        self._bindTwitter(self, $(this));
                        break;
                    case "sms":
                        self._bindSMS(self, $(this));
                        break;
                    case "line":
                        self._bindLine(self, $(this));
                        break;
                    case "copy":		// copy to clipboard 
                        self._bindClipboard(self, $(this));
                        break;
                }
            })
        },

        _bindClipboard: function (self, el) {
            var o = self.options;

            el.unbind("click");
            el.click(function () {
                var url = el.data("url");
                if (!url)
                    url = $("[property='og:url']").attr("content");
                window.prompt("아래의 주소를 선택하여 복사해 주세요", url);

                return false;
            })
        },

        _bindTwitter: function (self, el) {
            var o = self.options;

            el.unbind("click");
            el.click(function () {
                var url = el.data("url");
                if (!url)
                    url = $("[property='og:url']").attr("content");

                var title = el.data("title");
                if (!title)
                    title = $("[property='og:title']").attr("content");

                var url = "https://twitter.com/intent/tweet?app_ex=2&text=" + encodeURIComponent(title + ' ' + url);
                window.open(url, "_blank", "width=550,height=350,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no");

                if (o.onSuccess) {
                    o.onSuccess("tw", null);
                }

                return false;
            })
        },

        _bindFacebook: function (self, el) {
            var o = self.options;

            el.unbind("click");
            el.click(function () {
                var link = el.data("url");
                if (!link)
                    link = $("[property='og:url']").attr("content");
                
                if (link.indexOf("?") == -1) {
                    link += "?app_ex=2";

                } else {
                    link += "&app_ex=2";
                }
                console.log(link)
                var url = "http://www.facebook.com/share.php?u=" + encodeURIComponent(link);
                window.open(url);
                
            });

            /*
			el.unbind("click");
			el.click(function () {
				var url = el.data("url");
				if (!url) 
					url = $("[property='og:url']").attr("content");

				FB.ui({
					method: 'feed',
					link: url,
					//caption: $("#og_description").attr("content") ,
				}, function (response) {
					if (response) {
					
						if (o.onSuccess) {
							o.onSuccess("fb", response);
						}

					}
					
				});

				return false;
			})
			*/
        },


        _bindKakaoStory: function (self, el) {
            var o = self.options;

            el.unbind("click");
            el.click(function () {
                var url = el.data("url");
                if (!url)
                    url = $("[property='og:url']").attr("content");


                //	안드로이드 에러 발생시 주석 코드로 사용
                var title = el.data("title");
                if (!title)
                    title = $("[property='og:title']").attr("content");

                var picture = el.data("picture");
                if (!picture && $("[property='og:image']").length > 0) {
                    picture = $("[property='og:image']").attr("content");
                }

                var userInfo = {
                    title: '한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을',
                    desc: '',
                    name: ''
                };
                if (picture != "") {
                    userInfo.images = [picture];
                }

                Kakao.Story.open({
                    url: url,
                    text: title,
                    urlInfo: userInfo
                });

                /*
				Kakao.Auth.login({
					success: function () {
						// 로그인 성공시, API를 호출합니다.
						Kakao.API.request({
							url: '/v1/api/story/linkinfo',
							data: {
								url: url
							}
						}).then(function (res) {
							return Kakao.API.request({
								url: '/v1/api/story/post/link',
								data: {
									link_info: res
								}
							});
						}).then(function (res) {
							return Kakao.API.request({
								url: '/v1/api/story/mystory',
								data: { id: res.id }
							});
						}).then(function (res) {

							if (o.onSuccess) {
								o.onSuccess("ks", res);
							}

						}, function (err) {
							alert(JSON.stringify(err));
						});

					}
				});
			*/

                return false;

            });

        },

        _bindKakaoTalk: function (self, el) {
            var o = self.options;

            el.unbind("click");
            el.click(function () {

                var url = el.data("url");
                if (!url)
                    url = $("[property='og:url']").attr("content");

                var title = el.data("title");
                if (!title)
                    title = $("[property='og:title']").attr("content");

                var description = el.data("desc");
                if (!description)
                    description = $("[property='og:description']").attr("content");

                var picture = el.data("picture");
                if (!picture && $("[property='og:image']").length > 0) {
                    picture = $("[property='og:image']").attr("content");
                }
                
                var msg = title.replace(/(?:\r\n|\r|\n)/g, '') + " \n" + description + " \n" + url;
                if (picture == '') {
                    Kakao.Link.sendTalkLink({
                        label: msg
                    });

                } else {

                    // 카카오톡 이미지 설정 width, height가 최소 80px이상이어야 함
                    var imgObj = new Image();
                    imgObj.src = picture;
                                        
                    var width = imgObj.width;
                    var height = imgObj.height;

                    if (width > height) {
                        width = (width / height) * 100;
                        height = 100;
                    } else if (height > width) {
                        height = (height / width) * 100;
                        width = 100;
                    } else {
                        width = 100;
                        height = 100;
                    }



                    var image = {
                        src: encodeURI(picture),
                        width: width,
                        height: height
                    }
                    //alert(image.src);
                     
                    Kakao.Link.sendTalkLink({
                        label: msg,
                        image: image
                    });
                }


                return false;

            });

        },

        _bindLine: function (self, el) {
            var o = self.options;

            el.unbind("click");
            el.click(function () {


                var link = el.data("url");
                if (!link)
                    link = $("[property='og:url']").attr("content");

                var title = el.data("title");
                if (!title)
                    title = $("[property='og:title']").attr("content");

                var description = el.data("desc");
                if (!description)
                    description = $("[property='og:description']").attr("content");
                if (!description)
                    description = "";

                /*
				if (o.isApp) {
					var url = "http://line.me/R/msg/text/?" + encodeURIComponent(title + "\r" + description + " , " + link);
					location.href = url;
				} else {
					
					var url = "http://line.me/R/msg/text/?" + encodeURIComponent(title + "\r" + description + " , " + link);
					location.href = url;
					return false;
				}
				*/
                if (link.indexOf("?") == -1) {
                    link += "?app_ex=2";

                } else {
                    link += "&app_ex=2";
                }

                var url = "http://line.me/R/msg/text/?" + encodeURIComponent(title + "\r" + description + " , " + link);
                window.open(url);
                console.log("link", link)
            });

        },

        _bindSMS: function (self, el) {
            var o = self.options;

            el.unbind("click");
            el.click(function () {

                var link = el.data("url");
                if (!link)
                    link = $("[property='og:url']").attr("content");

                var title = el.data("title");
                if (!title)
                    title = $("[property='og:title']").attr("content");

                var description = el.data("desc");
                if (!description)
                    description = $("[property='og:description']").attr("content");
                if (!description)
                    description = "";

                var ua = navigator.userAgent.toLowerCase();

                var url = "sms:";
                url += (ua.indexOf("iphone") > -1 || ua.indexOf("ipad") > -1) ? "&" : "?";		// ; &
                url += "body=" + encodeURIComponent(title + "\r" + description + " , " + link);
                location.href = url;

                return false;

            });

        }

    });
}(jQuery));