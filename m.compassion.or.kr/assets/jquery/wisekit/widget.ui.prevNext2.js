// 

(function ($) {

	$.widget("ui.prevNext2", {
		options: {
			edge: "fill",
			start: 0,
			next: null, // ex: "#next"
			prev: null, // ex: "#prev"
			moving_unit: "page", // page , item
			visible: 1,
			always_arrow_show: true,

			// private
			_target : 0 , 
			_animating: false,
			_items : null , 
			_container : null 
		},

		_create: function () {

			var self = this, o = this.options;
			
			self.reset(self);

			var id = this.element.attr("id");

			if (o._items.length <= o.visible) {
				self.showArrow(self, false, false);
			} else {
				self.showArrow(self, false, true);
			}

			if (o.screen == "mobile") {
				$(window).on("resize", function (event) {
					self.reset(self);
				});
			}

			$(o.next).click(function () {
				
				if (o._animating) return false;
				o._animating = true;
				
				if (o.moving_unit && o.moving_unit == "page") {
					o._target -= o.visible;
				} else {
					o._target--;
				}

				if (o._target <= 0) {
					self.showArrow(self, false, true);
					o._target = 0;
				} else {
					self.showArrow(self, true, true);
				}
				self.render(self);

				if (o.onMoved) {
					o.onMoved(o._target + 1, o._items.length);
				}

				return false;
			});

			$(o.prev).click(function () {
				
				if (o._animating) return false;
				o._animating = true;

				var after = o._target;
				if (o.moving_unit && o.moving_unit == "page") {
					after += o.visible;
				} else {
					after++;
				}

				if (after > o._items.length) {
					o._animating = false;
					self.showArrow(self, true, false);
				} else if (after + o.visible >= o._items.length) {
					o._target = after;
					self.showArrow(self, true, false);
					if (o.edge == "fill") {
						o._target = o._items.length - o.visible;
					}
					self.render(self);
				} else {
					o._target = after;
					self.showArrow(self, true, true);
					self.render(self);
				}

				if (o.onMoved) {
					o.onMoved(o._target + 1, o._items.length);
				}
				return false;
			});

		},

		reset: function (self) {
			var o = self.options;
			var id = self.element.attr("id");
			
			o._container = $("#" + id + " > .pn_container");
			o._items = $("#" + id + " > .pn_container > .pn_item");
			if (o._items.length > 0) {

				var item = $(o._items[0]);
				o._items.length = o._items.length;

				var w = 0;
				o._items.each(function (i) {
					
					if (o._container.attr("data-width") && o._container.attr("data-width") == "full" && o.visible == 1) {
						item_width = $("#" + id).width();
					} else {
						item_width = $(this).width();
					}
					w += item_width;
				/*	
					var margin = parseInt($(this).css("margin-left").replace("px", "")) + parseInt($(this).css("margin-right").replace("px", ""));
					margin += parseInt($(this).find("a").css("margin-left").replace("px", "")) + parseInt($(this).find("a").css("margin-right").replace("px", ""));
					w += margin;
					*/
					$(this).width(item_width + "px");

				});
			//	alert(w)
				o._container.css({
					height: o._container.height() + "px",
					width : w , 
					top: 0,
					left: -self.getLeft(self, o.start) , 
					position: "relative"
				});	
			}

		},

		getLeft: function (self, index) {
			var o = self.options;
			var left = 0;
			for (i = 0 ; i < index ; i++) {
				var obj = $(o._items[i]);
				//left += obj.width() - 1;
				left += obj.width();
			}
			return left;

		} , 

		showArrow: function (self, prev, next) {
			var o = self.options;
			var id = self.element.attr("id");

			if (!o.always_arrow_show) {
				$(o.next).css("display", prev ? "block" : "none");
				$(o.prev).css("display", next ? "block" : "none");
			}
		},

		render: function (self) {
			var o = self.options;
			
			var left = self.getLeft(self , o._target);
			
			o._container.animate({
				left: -left
			}, 500, function () {
				o._animating = false;
			});
		},

		refresh: function () {
			var progress = this.options.value + "%";
			this.element.text(progress);
		}

	});


})(jQuery);

