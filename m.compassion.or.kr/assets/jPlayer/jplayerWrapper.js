﻿

var jPlayerWrapper = {
	_is_pause : false , 
	_format: "mp3",
	playlist: null,
	_cur_index : -1,

	/*
	urls : [{mp3:"http://www.jplayer.org/audio/mp3/TSP-01-Cro_magnon_man.mp3"}]
	*/
	init: function (urls , playNext , onReady) {
	
		if ($("#divjPlayerList").length > 0)
			$("#divjPlayerList").remove();

		if ($("#divjPlayer").length > 0)
			$("#divjPlayer").remove();

		$("body").append($("<div id='divjPlayerList' style='display:none'><div class='jp-playlist' style='display:none'><ul><li></li></ul></div></div>"));
		// display:none 으로 할경우 iOS 6+ 에서 동작하지 않는다.
		$("body").append($("<div id='divjPlayer'></div>"));		
		
		jPlayerWrapper._is_pause = true;
		jPlayerWrapper.playlist = new jPlayerPlaylist(
			{ jPlayer: "#divjPlayer", cssSelectorAncestor: "#divjPlayerList" },
			urls,
			{
				playlistOptions: {
					
					autoPlay: false,
					loopOnPrevious: false,
					enableRemoveControls: true
				},
				/*preload : "auto" , */
				swfPath: "../../common/js/jplayer",
				solution: "flash, html",
				supplied: "mp3",
				smoothPlayBar: true,
				keyEnabled: false,
				audioFullScreen: false
			}
		);
		
		// 전체듣기가 아니면 선택된 음성만 플레이한다.
		if (!playNext) {
			$("#divjPlayer").bind($.jPlayer.event.ended, function (event) {
				jPlayerWrapper.stop();
			});
		}

		if (onReady) {
			$("#divjPlayer").bind($.jPlayer.event.ready, function (event) {
				onReady();
			});
		}
	},

	toggle: function () {

		jPlayerWrapper._is_pause = !jPlayerWrapper._is_pause;
		if (jPlayerWrapper._is_pause) {
			$("#divjPlayer").jPlayer("pause");
		} else {
			$("#divjPlayer").jPlayer("play");
		}
	},

	select: function (index , force) {
		//alert(JSON.stringify(jPlayerWrapper.playlist))
		var self = jPlayerWrapper;
		if (self._cur_index == index && !force) {
			self.toggle();
		} else {
			self._cur_index = index;
			self._is_pause = false;
			self.playlist.play(index);
		}

		return !self._is_pause;		// play = true , pause(stop) = false
		
	} , 

	play: function () {
		if (!jPlayerWrapper._is_pause) return;
		if (jPlayerWrapper._is_pause) {
			jPlayerWrapper._is_pause = false;
			$("#divjPlayer").jPlayer("play");
		} 
	},

	pause: function () {
		try {
			if (jPlayerWrapper._is_pause) return;
			jPlayerWrapper._is_pause = true;
			$("#divjPlayer").jPlayer("pause");
		} catch (e) { }
	} , 

	stop: function () {
		try{
			$("#divjPlayer").jPlayer("stop");
		} catch (e) { }
	},

	destroy: function () {
		try{
			$("#divjPlayer").jPlayer("destroy");
		} catch (e) { }
	}

	
}

