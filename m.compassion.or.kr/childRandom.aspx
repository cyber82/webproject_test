﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="childRandom.aspx.cs" Inherits="childRandom" MasterPageFile="~/top_without_header.master" %>
<%@ MasterType VirtualPath="~/top_without_header.master" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	<meta name="keywords" content="한국컴패션,어린이후원"  />
	<meta name="description" content="꿈을 잃은 어린이들에게 그리스도의 사랑을" />
	<meta property="og:url" content="http://m.compassion.or.kr" />
	<meta property="og:title" content="한국컴패션" />
	<meta property="og:description" content="꿈을 잃은 어린이들에게 그리스도의 사랑을" />
	<meta property="og:image" content="http://m.compassion.or.kr/common/img/sns_default_image.jpg" />
</asp:Content>
<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <asp:PlaceHolder runat="server" ID="ph_child_birthday">
        <section id="main_recmd1" runat="server" class="main-field1" style="background-image: initial; height:610px;">
            <style>
                .child-info >li, .child-txtintro >p { color:#666 !important; }
                 
            </style>
            <div class="pos-photo">
                <span id="main_recmd1_pic" runat="server" class="photo" style="border-color: #ddd; background: url('http://ws.compassion.or.kr/Files/Child/GU/GU4980204.jpg') no-repeat"></span>
            </div>
            <ul class="child-info">
                <li style="background: url(/common/img/main/icon_nation.png) center top no-repeat;">
                    <asp:Literal runat="server" ID="main_recmd1_country" /></li>
                <li style="background: url(/common/img/main/icon_age.png) center top no-repeat;">
                    <asp:Literal runat="server" ID="main_recmd1_age" />세</li>
                <li style="background: url(/common/img/main/icon_sex.png) center top no-repeat;">
                    <asp:Literal runat="server" ID="main_recmd1_gender" /></li>
            </ul>


            <div class="child-txtintro">
                <p class="txt1">예수님의 사랑을 생각하며<br />결연을 결심할 때입니다.</p>
                <p class="txt2">
                    십자가의 놀라운 사랑으로 우리도 희망을 찾았습니다.<br />
                    아직 사랑을 경험해보지 못한 연약한 어린이들.<br />
                    우리가 받은 큰 사랑을,<br />
                    <asp:Literal runat="server" ID="main_recmd1_name" />에게 보내주지 않으시겠어요?<br />
                </p>
            </div>

            <a class="bt-type6" style="width:100%;" href="/sponsor/children/?c={childMasterId}" runat="server" id="main_recmd1_link" target="_blank">
                한 어린이 결연 결심하기</a>

            <%--<a class="bt-type7" style="width:100%; margin-top:10px;" href="#" runat="server" id="main_recmd1_sns"  ng-click="sns.show($event , {url : '/sponsor/children/?c=' +  'item.childmasterid', title : '', desc : '' , picture : 'item.pic'})">공유하기</a>--%>
            <%--<a class="bt-type7" style="width:100%; margin-top:10px;" href="#" id="main_recmd1_sns"  ng-click="sns.show($event , {url : '/sponsor/children/?c=' +  '<%=ViewState["ChildMasterID"].ToString() %>', title : '', desc : '' , picture : '<%=ViewState["Pic"].ToString() %>'})">공유하기</a>--%>

            <a class="bt-type7" href="/sponsor/children/" style="width:100%; margin-top:10px;" target="_blank">어린이 더보기</a>
        </section>
    </asp:PlaceHolder>
    <div id="main_recmd1_empty" runat="server" visible="false">
        어린이가 존재하지 않습니다.
    </div>
</asp:Content>