﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class bluebook_apply: MobileFrontBasePage {

    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();

        if(!UserInfo.IsLogin) {
            Response.ClearContent();
            return;
        }


        /*
		국내거주인경우 회원주소/연락처를를 컴파스에서 가져온다.
		*/
        UserInfo sess = new UserInfo();


        // 이름
        if(sess.UserName != "") {
            name.Value = sess.UserName;
            //name.Disabled = true;
        }

        /*
		국내거주인경우 회원주소/연락처를를 컴파스에서 가져온다.
		*/

        locationType.Value = sess.LocationType;

        if(sess.LocationType == "국내") {

            var addr_result = new SponsorAction().GetAddress();
            if(!addr_result.success) {
                base.AlertWithJavascript(addr_result.message, "goBack()");
                return;
            }

            SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;

            addr1.Value = addr_data.Addr1;
            addr2.Value = addr_data.Addr2;
            addressType.Value = addr_data.AddressType;
			dspAddrDoro.Value = addr_data.DspAddrDoro;
			dspAddrJibun.Value = addr_data.DspAddrJibun;


			if (!string.IsNullOrEmpty(addr_data.Zipcode)) {
                zipcode.Value = addr_data.Zipcode;
                zipcode.Disabled = true;
                //popup.Disabled = true;
                //popup.Attributes["disabled"] = "true";
                popup.Visible = false;
            }


            if(!string.IsNullOrEmpty(addr_data.Zipcode)) {

                addr1.Value = addr_data.Addr1;
                addr1.Disabled = true;
            }

            if(!string.IsNullOrEmpty(addr_data.Zipcode)) {
                addr2.Value = addr_data.Addr2;
                addr2.Disabled = true;
            }


            var comm_result = new SponsorAction().GetCommunications();
            if(!comm_result.success) {
                base.AlertWithJavascript(comm_result.message, "goBack()");
                return;
            }

            SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
            phone.Value = comm_data.Mobile.TranslatePhoneNumber();
            if(phone.Value != "") {
               // phone.Disabled = true;
            }
        }
    }
}