﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class sympathy_vision_individual : MobileFrontBasePage {


	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);

		GetNation();
	}


	protected override void OnAfterPostBack() {
		base.OnAfterPostBack();
	}

	protected void GetNation()
    {
        using (FrontDataContext dao = new FrontDataContext())
        {

            //var list = dao.nation_code.Where(p => p.nc_display == true).OrderBy(p => p.nc_depth1).ThenBy(p => p.nc_order).ToList();
            var list = www6.selectQ<nation_code>("nc_display", 1).OrderBy(p => p.nc_depth1).ThenBy(p => p.nc_order).ToList();

            // string query = string.Format("select * from nation_code where nc_display=1 order by nc_depth1 asc , nc_order asc");
            // var list = dao.ExecuteQuery<nation_code>(query);

            nation_value.Value = list.ToJson();
        }
	}
}