﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="bluebook-apply.aspx.cs" Inherits="bluebook_apply" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript">
   

        (function () {


        

            var app = angular.module('cps.page', []);

            app.controller("defaultCtrl", function ($scope, $http, popup, $address) {
                $scope.requesting = false;
                $scope.apply = {

                    request: function ($event) {

                        if (!validateForm([
                            { id: "#name", msg: "이름을 입력하세요" },
                            { id: "#phone", msg: "휴대폰번호를 입력하세요", type: "phone" },
                            { id: "#zipcode", msg: "우편번호를 입력하세요" },
                            { id: "#addr1", msg: "주소를 입력하세요" },
                            { id: "#addr2", msg: "주소를 입력하세요" }
                        ])) {
                            return;
                        }

                        if ($scope.requesting) return;
                        $scope.requesting = true;

                        var name = $("#name").val();
                        var phone = $("#phone").val();
                        var zipcode = $("#zipcode").val();
                        var addr1 = $("#addr1").val();
                        var addr2 = $("#addr2").val();
                        var addressType = $("#addressType").val();
                        
                        $http.post("/api/sympathy.ashx?t=apply", { name: name, phone: phone, zipcode: zipcode, addr1: addr1, addr2: addr2, addressType: addressType }).success(function (r) {
                            $scope.requesting = false;
                            console.log(1,r)
                            if (r.success) {
                                alert("신청이 완료되었습니다.\n후원자님의 귀한 섬김에 언제나 감사드립니다.");
                            } else {
                                alert(r.message);
                                
                            }
                            location.href = "/sympathy/bluebook";
                        });
                    }

                }
                $scope.findAddr = function ($event) {
                    $event.preventDefault();

                    popup.init($scope, "/common/popup/address", function (modal) {
                    	initAddress($scope, $address, modal, function (zipcode, addr1, addr2, jibun) {		// callback

                            $("#zipcode").val(zipcode);
                            $("#addr1").val(addr1 + "//" + jibun);
                            $("#addr2").val(addr2);

                            // 화면에 표시
                            $("#zipcode").val(zipcode);

                            $("#addr_road").text("[도로명주소] " + addr1 + " " + addr2);
                            $("#addr_jibun").text("[지번주소] " + jibun + " " + addr2);

                        });

                    }, { top: 0, iscroll: true, removeWhenClose: true });

                }

            })
        })();


        $(function () {

			
        	// 컴파스의 데이타를 불러오는경우 
        	if ($("#dspAddrDoro").val() != "") {
        		$("#addr_road").text("[도로명주소] (" + $("#zipcode").val() + ") " + $("#dspAddrDoro").val());
        		if ($("#dspAddrJibun").val() != "") {
        			$("#addr_jibun").text("[지번] (" + $("#zipcode").val() + ") " + $("#dspAddrJibun").val());
        		}

        	} else {

        		addr_array = $("#addr1").val().split("//");
        		$("#addr_road").text("[도로명주소] " + addr_array[0] + " " + $("#addr2").val());
        		if (addr_array[1]) {
        			$("#addr_jibun").text("[지번주소] " + addr_array[1] + " " + $("#addr2").val());
        		}
        	}
        })
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type="hidden" runat="server" id="addressType" value="" />
	<input type="hidden" runat="server" id="locationType" value="" />
    <input type="hidden" runat="server" id="dspAddrJibun" value="" />
    <input type="hidden" runat="server" id="dspAddrDoro" value="" />
    <div class="appContent" ng-app="cps" ng-controller="defaultCtrl">
    
    <div class="more-form more-bluebook">
    	
		<div class="member-join">			
			<fieldset class="frm-input">
				<legend>블루북 신청</legend>
				<div class="row-table">
					<div class="col-td" style="width:100%">

						<p class="txt-title">이름</p>
						<span class="row">
                            <input type="text" runat="server" name="name" id="name" placeholder="" style="width:100%" />
						</span>

						<p class="txt-title">휴대폰</p>
						<span class="row">
                            <input type="text" runat="server" name="phone" id="phone" class="number_only" maxlength="11" placeholder="" style="width:100%" />
						</span>


						<p class="txt-title">주소</p>
                        <span class="row pos-relative2">
							<input type="hidden" id="addr1" runat="server" />
							<input type="hidden" id="addr2" runat="server" />


                            <input type="text" runat="server" name="zipcode" class="zipcode" id="zipcode" placeholder="주소" readonly="readonly" style="width: 100%" />
                        
                            <a class="bt-type8 pos-btn" style="width: 70px" runat="server" id="popup" ng-click="findAddr($event)">주소찾기</a>
                        </span>

						<span id="addr_road" class="row fs13 mt15"></span>
						<span id="addr_jibun" class="row fs13"></span>

					</div>
				</div>

			</fieldset>
		</div>

		<div class="gray-box">
			배송받으실 주소와 연락처가 정확한지 다시 한 번 확인 부탁드립니다.<br />
			<strong>신청하시겠습니까?</strong>
		</div>

		<div class="wrap-bt mb30">
			<a class="bt-type6" style="width:100%" href="#" ng-click="apply.request($event)">확인</a>
		</div>

	</div>
    
</div>

</asp:Content>
