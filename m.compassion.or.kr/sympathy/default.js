﻿$(function () {
    var sly = new Sly($("#header_menu"), {    // Call Sly on frame

        horizontal: 1,
        itemNav: 'centered',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 3,
        scrollBy: 1,
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1

    });

    sly.init();



    $.each($("#header_menu a"), function (i) {
        if ($(this).hasClass("selected")) {
            sly.activate(i);
        }
    })



});

(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, popup, paramService) {

        $scope.requesting = false;

        $scope.total = -1;
        $scope.list = [];
        $scope.nationList = [];
        $scope.cur_nation = null;

        $scope.params = {
            page: 1,
            rowsPerPage: 10,
            s_type: $("#s_type").val(),
            s_column: 'reg_date',
            depth2: ""
        };

        // 파라미터 초기화
        $scope.params = $.extend($scope.params, paramService.getParameterValues());

        // 검색
        $scope.search = function () {
            $scope.params.page = 1;
            $scope.getList();
        }

        // 국가별로 보기
        $scope.getNationList = function () {
            var nationJson = $("#nation_value").val();
            if (nationJson && nationJson != "") {
                var data = $.parseJSON(nationJson);

                var countries = $.grep(data, function (r) {
                    return r.nc_depth2 != ""
                });

                $scope.nationList = countries;
            }
        }

        // list
        $scope.getList = function (params) {

            $scope.params = $.extend($scope.params, params);
            //console.log("getList", $scope.params);
            $http.get("/api/sympathy.ashx?t=list", { params: $scope.params }).success(function (result) {
                $scope.list = $.extend({}, result.data);
                $scope.total = result.data.length > 0 ? result.data[0].total : 0;
                $.each($scope.list, function () {
                    console.log(this.depth2_name == null && this.sa_nc_text != "")
                    if (this.depth2_name == null && this.sa_nc_text != "") {
                        this.depth2_name = this.sa_nc_text;
                    }

                })
                console.log($scope.list)


            });
            if (params) scrollTo($("body"));
        }

        // 상세페이지
        $scope.goView = function (idx) {
            $scope.params.fromMain = "N";
            //$http.post("/api/sympathy.ashx?t=hits&idx=" + idx).then().finally(function () {
            //    location.href = "/sympathy/view/" + $scope.params.s_type + "/" + idx + "?" + $.param($scope.params);
            //});

            location.href = "/sympathy/view/" + $scope.params.s_type + "/" + idx + "?" + $.param($scope.params);
        }

        // 정렬
        $scope.sort = function (s_column) {
            $scope.params.s_column = s_column;
            $scope.params.page = 1;
            $scope.getList();
            // console.log('최신', $scope.sort);
        }

        $scope.getNationList();

        $scope.getList();


        $scope.parseDate = function (datetime) {
            return new Date(datetime);
        }

        // 메인 페이지별 제목
        $scope.showTitle = function () {
            $("#tit_" + $scope.list.board_id).show();
            return;
        }
        $scope.showTitle();

        // 메인 상세페이지
        $scope.goView_main = function (board_id, idx) {
            $scope.params.s_type = board_id;
            $scope.params.fromMain = "Y";
            $http.post("/api/sympathy.ashx?t=hits&idx=" + idx).then().finally(function () {
                location.href = "/sympathy/view/" + $scope.params.s_type + "/" + idx + "?" + $.param($scope.params);
            });
        }
        $scope.apply = function () {
            if (common.checkLogin()) {

                if ($("#locationType").val() != "국내") {
                    alert("국내거주 회원만 신청가능합니다.");
                    return false;
                }

                if ($("#register_book").val() == "N") {
                    alert("이미 신청하셨습니다.");
                    return false;
                }

                if ($("#sponsor_id").val() == "N") {
                    alert("블루북은 기존 후원자만 신청 가능합니다.");
                    return false;
                }

                location.href = "/sympathy/bluebook-apply"
            }
        }
    });


})();