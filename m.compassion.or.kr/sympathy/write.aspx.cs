﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using CommonLib;

public partial class sympathy_write : MobileFrontBasePage {

	const string listPath = "/sympathy/essay/";


	public override bool RequireLogin{
		get{
			return true;
		}
	}

	protected override void OnBeforePostBack() {
		base.OnBeforePostBack();
		
		btnList.HRef = listPath + "?" + this.ViewState["q"].ToString();

		foreach (var a in StaticData.Code.GetList(this.Context).Where(p => p.cd_display == true && p.cd_group == "essay_type").OrderBy(p => p.cd_order)) {
			essay_type.Items.Add(new ListItem(a.cd_value, a.cd_key));
		}

		/*
		국내거주인경우 회원주소/연락처를를 컴파스에서 가져온다.
		*/
		UserInfo sess = new UserInfo();
		name.Text = sess.UserName;
		if (sess.LocationType == "국내") {
			
			var comm_result = new SponsorAction().GetCommunications();
			if (!comm_result.success) {
				base.AlertWithJavascript(comm_result.message, "goBack()");
				return;
			}

			SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
			phone.Text = comm_data.Mobile.TranslatePhoneNumber();
			email.Text = comm_data.Email;
			
		}


	}


	protected override void loadComplete(object sender, EventArgs e) {
		
		
	}

	protected void btnAdd_Click(object sender, EventArgs e)
    {
        using (FrontDataContext dao = new FrontDataContext())
        {
            var entity = new ssb_article()
            {
                board_id = "essay",
                board_group = essay_type.SelectedValue,
                view_num = 0,
                title = b_title.Text,
                body = content.InnerHtml.ToHtml(),
                user_id = new UserInfo().UserId,
                user_name = name.Text,
                ip_address = Request.ServerVariables["REMOTE_ADDR"].ToString(),
                cell_1 = phone.Text.Split('-')[0],
                cell_2 = phone.Text.Split('-')[1],
                cell_3 = phone.Text.Split('-')[2],
                sub_title = "",
                reg_date = DateTime.Now,
                view_date = DateTime.Now,
                email = email.Text,
                yn1 = true, // 관리자 검토전
                status = "normal",
                thumb = temp_file_name.Value

            };

            //dao.ssb_article.InsertOnSubmit(entity);
            www6.insert(entity);
            //dao.SubmitChanges();

            Response.Write("<script>alert('등록이 완료되었습니다. 후원자님의 귀한 섬김에 언제나 감사드립니다.'); location.href='" + btnList.HRef + "';</script>");
        }
	}
}