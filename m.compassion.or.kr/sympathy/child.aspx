﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="child.aspx.cs" Inherits="sympathy_child" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
    <script type="text/javascript" src="/sympathy/default.js?v=1"></script>

    <script type="text/javascript">
        $(function () {

        })
    </script>

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <input type="hidden" id="s_type" value="child" />
    <input type="hidden" runat="server" id="nation_value" value="" />

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <div class="menu-sympathy sectionsub-margin1" id="header_menu" style="position: relative; overflow: hidden;">

            <ul>
                <!--임시 inline style(수정가능)-->
                <li style="display: inline-block; width: 80px"><a href="/sympathy/all">전체</a></li>
                <li style="display: inline-block; width: 100px"><a class="selected" href="/sympathy/child/">꿈꾸는 어린이</a></li>
                <li style="display: inline-block; width: 117px"><a href="/sympathy/sponsor/">꿈을 심는 후원자</a></li>
                <li style="display: inline-block; width: 127px"><a href="/sympathy/vision/vision/">비전트립 다이어리</a></li>
                <li style="display: inline-block; width: 117px"><a href="/sympathy/essay/">함께 쓰는 에세이</a></li>
                <li style="display: inline-block; width: 97px"><a href="/sympathy/bluebook/">컴패션블루북</a></li>
                <li style="display: inline-block"><a href=""></a></li>
            </ul>

        </div>


        <div class="sympathy-list">

            <div class="frm-country">
                <select style="width: 100%" ng-options="item.nc_depth2 as item.nc_name for item in nationList" ng-model="params.depth2" ng-change="search()">
                    <option value="">국가별로 보기</option>
                </select>
            </div>

            <div class="align-list">
                <a class="selected" ng-class="{'selected':params.s_column == 'reg_date'}" ng-click="sort('reg_date')">최신순</a>
                <span class="bar">|</span>
                <a ng-class="{'selected':params.s_column == 'view_count'}" ng-click="sort('view_count')">인기순</a>
            </div>

            <!--썸네일 리스트-->

            <div class="wrap-thumbil">

                <div class="no-result" ng-show="total == 0">
                    <strong class="txt-title">검색 결과가 없습니다.</strong>
                    <p class="txt-msg">다른 국가를 선택해주세요.</p>
                </div>

                <ul class="list-thumbil width100">
                    <li ng-repeat="item in list"><a class="box-block" ng-click="goView(item.idx)">
                        <span class="photo" style="background: url('{{item.thumb}}') no-repeat center top; background-size: cover;"></span>
                        <span class="sympathy-info">
                            <span class="txt-category">꿈꾸는 어린이</span>
                            <span class="txt-title">{{item.title}}</span>
                            <span class="txt-date">{{parseDate(item.reg_date) | date:'yyyy.MM.dd'}}</span>
                            <span class="txt-country" ng-hide="!item.depth2_name">{{item.depth2_name}}</span>
                            <div style="height: 32px;" ng-show="!item.depth2_name"></div>
                        </span>
                    </a></li>

                </ul>
            </div>

            <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page});"></paging>

            <!--
            <div class="page-count-indicator">
                <a class="prev" href="#">이전</a>
                <span class="recent">1</span> / 50
				<a class="next" href="#">다음</a>
            </div>
			-->

        </div>

        <!--//-->
    </div>



</asp:Content>
