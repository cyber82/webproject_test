﻿(function () {

    var app = angular.module('cps.page', []);

    app.controller("defaultCtrl", function ($scope, $http, paramService) {

        $scope.isLogin = common.isLogin();
        $scope.userId = common.getUserId();

        $scope.total = -1;
        $scope.page = 1;
        $scope.rowsPerPage = 10;
        $scope.content = "";

        $scope.list = [];
        $scope.params = {
            id: $("#id").val(),
            page: $scope.page,
            rowsPerPage: $scope.rowsPerPage,
            type: "story",


        };
        $scope.paramValue = $.extend($scope.paramValue, paramService.getParameterValues());

        $scope.getList = function (params) {
            $scope.params = $.extend($scope.params, params);
            $http.get("/api/sympathy_reply.ashx?t=list", { params: $scope.params }).success(function (result) {
                if (result.success) {
                	$scope.list = $.merge($scope.list , result.data);
                    $scope.total = result.data.length > 0 ? result.data[0].total : 0;
                }
            });
            //if (params)scrollTo($(".angular_area"), 10);
        }

    	$scope.showMore = function ($event) {
    		$scope.getList();
    		$scope.params.page++;
    	},

        $scope.checkLogin = function ($event) {
            if (!common.checkLogin()) {
        		$("#reply").trigger("blur");
                $event.preventDefault();
                return;
            }
        },

        $scope.update = function ($event) {

            if (!common.checkLogin()) {
                $event.preventDefault();
                return;
            }

            if ($scope.content == "") {
                alert("댓글을 입력해주세요.");
                $("#reply").focus();
                $event.preventDefault();
                return;
            }

            // 신규 등록
            if ($scope.updateId == -1) {


                $http.post("/api/sympathy_reply.ashx", { t: 'add', article_idx: $("#id").val(), content: $scope.content }).success(function (result) {
                    //console.log(result);
                    if (result.success) {
                        $scope.list = [];
                        $scope.getList({ page: 1 });
                    } else {
                        //  alert(result.message);
                        alert("댓글을 입력해주세요.");
                    }
                })
            } else {
                $http.post('/api/sympathy_reply.ashx', { t: 'update', id: $scope.updateId, content: $scope.content }).success(function (result) {
                    if (result.success) {
                        $scope.list = [];
                        $scope.getList();
                    } else {
                         alert(result.message);
                    }

                    $scope.updateId = -1;
                })
            }

            $scope.content = "";
            $event.preventDefault();
        }


        $scope.remove = function (id) {
            if (confirm("정말 삭제하실 건가요?")) {
                $http.post("/api/sympathy_reply.ashx?t=remove&id=" + id).success(function (result) {
                    if (result.success) {
                        alert("삭제 되었습니다.");
                        $scope.list = [];
                        $scope.getList();
                    } else {
                        alert(result.message);
                    }
                });
            } else {
                alert("삭제가 취소되었습니다.");
            }
        }

        $scope.updateId = -1;
        $scope.modify = function (entity) {
            $scope.updateId = entity.idx;
            $scope.content = entity.body
        }

        $scope.getList();

        // 페이지별 제목
        $scope.showTitle = function () {
        	$("#tit_" + $("#hidden_s_type").val()).show();
            //console.log($scope.paramValue.s_type)
            return;
        }
        $scope.showTitle();

    	

    });

})();
