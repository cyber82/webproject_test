﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="write.aspx.cs" Inherits="sympathy_write" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/ajaxupload/ajaxupload.3.6.wisekit.js"></script>
    <script type="text/javascript">

        $(function () {
            $("#content").textCount($("#count"), { limit: 2000 });
            // 썸네일
            var thumbUploader = attachUploader("thumb");
            thumbUploader._settings.data.fileDir = "<%:Uploader.GetRoot(Uploader.FileGroup.image_sympathy)%>";
	        thumbUploader._settings.data.rename = "y";
	        $("#phone").mobileFormat();


        });

        var attachUploader = function (button) {
            return new AjaxUpload(button, {
                action: '/common/handler/upload',
                responseType: 'json',
                onChange: function () {
                },
                onSubmit: function (file, ext) {
                    this.disable();
                },
                onComplete: function (file, response) {
                    this.enable();
                    if (response.success) {
                        var c = $("#" + button).attr("class").replace(" ", "");
                        $(".temp_file_type").val(c.indexOf("image") > -1 ? "thumb" : "file");
                        $(".temp_file_name").val(response.name);
                        $("#attachFile").val(response.name);
                        $(".temp_file_size").val(response.size);
                        
                    } else
                        alert(response.msg);
                }
            });
        }


        function onSubmit() {

            if ($("#b_title").val() == "") {
                $("#msg_title").show();
                $("#b_title").focus();
                return false;
            }

            if ($("#essay_type option:selected").text() == "선택해 주세요") {
                $("#msg_subject").show();
                $("#essay_type").focus();
                return false;
            }

            if ($("#content").val() == "") {
                $("#msg_content").show();
                $("#content").focus();
                return false;
            }
            
            if ($("#email").val() == "") {
                $("#msg_email").show();
                $("#email").focus();
                return false;
            }
            var regEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            if (!$("#email").val().match(regEmail)) {
                $("#msg_email").html('이메일 주소가 유효하지 않습니다').show();
                $("#email").focus();
                return false;
            }
            if ($("#phone").val() == "") {
                $("#msg_phone").show();
                $("#phone").focus();
                return false;
            }
          
            if (!/(01[016789])[-](\d{4}|\d{3})[-]\d{4}$/g.test($("#phone").val())) {
                $("#msg_phone").html("올바른 휴대폰 형식이 아닙니다.").show();
                $("#phone").focus();
                return false;
            }

            return true;
        }



    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<input type=hidden runat=server id=temp_file_type class=temp_file_type value="" />
	<input type=hidden runat=server id=temp_file_name class=temp_file_name value="" />
	<input type=hidden runat=server id=temp_file_size class=temp_file_size value="" />
    <div class="appContent">

        <div class="more-form essay-write">

            <div class="member-join">
                <fieldset class="frm-input">
                    <legend>에세이 작성</legend>
                    <div class="row-table">
                        <div class="col-td" style="width: 100%">

                            <p class="txt-title">제목</p>
                            <span class="row">
                                <asp:TextBox runat="server" ID="b_title" placeholder="제목을 입력해 주세요" Style="width: 100%" />
                            </span>
                            <span class="txt-error" id="msg_title" style="display:none">제목을 입력해 주세요.</span>
                            <!--에러메시지 경우에만 출력-->

                            <p class="txt-title">주제</p>
                            <asp:DropDownList class="subject" Style="width: 100%;" runat="server" ID="essay_type">
                                <asp:ListItem>선택해 주세요</asp:ListItem>
                            </asp:DropDownList>
                            <span class="txt-error" id="msg_subject" style="display:none">주제를 선택해 주세요.</span>

                            <p class="txt-title">내용</p>
                            <div class="frm-row box mt0">

                                <textarea name="body" id="content" runat="server" placeholder="내용을 입력해 주세요" style="height: 150px;width:100%" maxlength="2000"></textarea>
                                <p class="txt-byte"><em id="count">0</em>/2000</p>
                            </div>
                            <span class="txt-error" id="msg_content" style="display:none">내용을 입력해 주세요.</span>
                            <p class="txt-title">사진첨부</p>
                            <span class="row pos-relative2">
                                <input type="text" value="" placeholder="선택된 파일이 없습니다" style="width: 100%" id="attachFile" />
                                <input type="file" class="fileInputHidden" style="width: 100%;" value="" id="thumb" />
                                <a class="bt-type10 pos-btn" style="width: 70px" href="#">찾기</a>
                            </span>
                            <div class="lineheight1">
                                파일 크기는 2MB 이하, JPG, PNG 또는 GIF형식의 파일만<br />
                                가능합니다.
                            </div>
                            <span class="txt-error" id="msg_thumb" style="display:none">파일을 등록해 주세요.</span>
                            <div class="writer-area">
                                <p class="txt-title">이름</p>
                                <span class="writer">
                                    <asp:Literal runat="server" ID="name"></asp:Literal></span>
                            </div>

                            <p class="txt-title">이메일</p>
                            <span class="row">
                                <asp:TextBox runat="server" ID="email" Style="width: 100%"></asp:TextBox>
                            </span>
                            <span class="txt-error" id="msg_email" style="display:none">이메일을 입력해 주세요.</span>
                            <p class="txt-title">휴대폰</p>
                            <span class="row">
                                <asp:TextBox runat="server" ID="phone" class="number_only" Style="width: 100%"></asp:TextBox>

                            </span>
                            <span class="txt-error" id="msg_phone" style="display:none">휴대폰번호를 입력해 주세요.</span>
                        </div>
                    </div>

                </fieldset>
            </div>

            <div class="gray-box">
                작성해 주신 글은 잘 다듬어 홈페이지에 게시할 예정입니다.<br />
                풍부한 이야기 표현을 위해 사진 자료 등을 추가 요청드릴 수 있으니 위의 연락처가 틀리지 않은지 한 번 더 확인 후 등록해 주세요.
            </div>

            <div class="wrap-bt mb30">
                <a class="bt-type4 fl" style="width: 49%" href="#" runat="server" id="btnList">취소</a>
                <asp:LinkButton runat="server" ID="btnAdd" OnClick="btnAdd_Click" OnClientClick="return onSubmit()" class="bt-type6 fr" Style="width: 49%">확인</asp:LinkButton>
            </div>

        </div>

    </div>

</asp:Content>
