﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class sympathy_essay : MobileFrontBasePage {


	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();
		base.LoadComplete += new EventHandler(list_LoadComplete);

        if(AppSession.HasCookie(this.Context)) {
            mobile_ver.Visible = false;
            app_ver.Visible = true;
        }

	}


	protected override void OnAfterPostBack() {
	}
}