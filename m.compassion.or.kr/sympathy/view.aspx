﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="sympathy_view" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

    <meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>" />
    <meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
    <meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
    <meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
    <meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />

</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    
    <script type="text/javascript" src="/sympathy/view.js"></script>
    <script type="text/javascript">
        $(function () {
            // 조회수 증가
            $.post("/api/sympathy.ashx?t=hits&idx=" + $("#id").val());
        });

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <input type="hidden" id="id" runat="server" />
    <input type="hidden" id="hidden_s_type" runat="server" />

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">
        <!---->

        <div class="sympathy-view">

            <div class="wrap-board">

                <div class="view-board">
                    <div class="txt-title sectionsub-margin1">
                        <span class="txt-blue" id="tit_bluebook" style="display: none">컴패션블루북</span>
                        <span class="txt-blue" id="tit_vision" style="display: none">비전트립 다이어리 &gt; 비전트립</span>
                        <span class="txt-blue" id="tit_individual" style="display: none">비전트립 다이어리 &gt; 개인방문</span>
                        <span class="txt-blue" id="tit_sponsor" style="display: none">꿈을 심는 후원자</span>
                        <span class="txt-blue" id="tit_child" style="display: none">꿈꾸는 어린이</span>
                        <span class="txt-blue" id="tit_essay" style="display: none">함께 쓰는 에세이</span>
                        <asp:Literal runat="server" ID="title" />
                    </div>

                    <div class="sympathy-info">
                        <p class="txt1">
                            <asp:Literal runat="server" ID="nation" />
                        </p>
                        <p class="txt2">
                            <asp:Literal runat="server" ID="sub_title" />
                        </p>

                    </div>

                    <div class="txt-date">
                        <asp:Literal runat="server" ID="reg_date" /><span class="bar">|</span><span class="viewcount">조회수</span><asp:Literal runat="server" ID="view_count" />
                    </div>
                    <div class="editor-html" style="color: #767676">
                        <asp:Literal runat="server" ID="article" />
                    </div>
                    <div class="share-sns sns_ani">
                        <a style="display:block;width:40px;margin:20px auto;" ng-click="sns.show($event)"><img src="/common/img/icon/share2.png" alt="SNS 공유" /></a>
                    </div>

                </div>

            </div>

			<div class="wrap-reply">
                <div class="box-gray">
                    <p class="txt-count">댓글&nbsp;<em>{{total}}</em></p>
                    <span class="row">
                        <input type="text" value="" placeholder="댓글을 입력해주세요." id="reply" name="content" ng-model="content" ng-focus="checkLogin($event)" ng-required="true" maxlength="300" />
                        <a class="bt-reg" ng-click="update($event)">등록</a>
                    </span>
                    <p class="txt-byte" id="count">{{content.length ? content.length : 0 }}/300자</p>
                </div>
                <ul class="list-reply">
                    <li ng-repeat="item in list" ng-click="goView(item.idx)">
                        <p class="txt-id">{{item.user_id}}</p>
                        <p class="txt-reply">{{item.body}}</p>
                        <time class="txt-time">{{item.reg_date | date : 'yyyy-MM-dd HH:mm:ss' }}</time>

                        <span class="action">
                            <span class="modify" ng-click="modify(item)" ng-show="item.is_owner">수정</span>
                            <span class="delete" ng-click="remove(item.idx)" ng-show="item.is_owner">삭제</span>
                        </span>
                    </li>
					
					<!-- 댓글 없을때 -->
					<li class="no-result" ng-if="total == 0">등록된 댓글이 없습니다.</li>
					<!--//  -->

                </ul>

                <span class="more-loading" ng-if="total > list.length" ng-click="showMore($event);"><span>더보기</span></span>
            </div>

            <div class="page-indigator">
                <a class="prev" runat="server" id="btnPrev"><span class="arrow"></span>이전 글</a>
                <a class="next" runat="server" id="btnNext">다음 글<span class="arrow"></span></a>
            </div>

            <div class="wrap-bt"><a style="width: 100%" class="bt-type5" runat="server" id="btnList">목록</a></div>

        </div>

        <!--//-->
    </div>



</asp:Content>
