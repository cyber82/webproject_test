﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="bluebook-is.aspx.cs" Inherits="bluebook_is" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">


    <div class="wrap-sectionsub">
        <!---->

        <div class="sympathy-bluebook">
            <p class="txt1">
                ‘블루북’은 후원자가 직접 만드는<br />
                <strong>세상에 단 하나뿐인 다이어리</strong>입니다.
            </p>
            <div class="youtube">
                <iframe src="https://www.youtube.com/embed/tTnkK5CblOs?rel=0&amp;showinfo=0wmode=transparent" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="intro-bluebook sectionsub-margin1">
                <img src="/common/img/page/sympathy/img-bluebook1.jpg" alt="" />
                <p class="txt-pos1">
                    <strong>블루북은요,</strong>
                    ‘블루북’은 미국의 예술 프로젝트인 ‘더 스케치북 프로젝트’ 에서 
                영감을 받아 시작한 프로젝트로, 이를 통해 수집된 블루북은 
                컴패션체험전 및 다양한 통로로 전시되어 더 많은 분들을 
                만나게 됩니다.
                </p>
            </div>

            <p class="txt2">블루북, 이렇게 진행됩니다</p>
            <ol class="list-number">
                <li><strong>1. 블루북 작성</strong>
                    <span class="photo">
                        <img src="/common/img/page/sympathy/img-bluebook2.jpg" alt="" /></span>
                    <ul>
                        <li>글, 그림, 사진 등 다양한 방법으로 블루북을 완성해 주세요.</li>
                        <li>노트 표지 및 내지의 자유로운 변형이 가능합니다. 다만, 내가 만든 블루북이 여러 사람에게 읽힐 수 있으니, 최대한 튼튼하게 만들어 주세요!</li>
                    </ul>
                </li>
                <li><strong>2. 지인에게 소개</strong>
                    <span class="photo">
                        <img src="/common/img/page/sympathy/img-bluebook3.jpg" alt="" /></span>
                    <ul>
                        <li>블루북에는 아직 후원자님을 만나지 못한 컴패션어린이  정보가 함께 들어있습니다. 이 어린이가 후원자를 만날 수  있도록 기도해 주세요.</li>
                        <li>가장 소중한 분에게 블루북과 함께 어린이를 소개해 주세요.</li>
                    </ul>
                </li>
                <li><strong>3. 완성된 블루북 반송</strong>
                    <span class="photo">
                        <img src="/common/img/page/sympathy/img-bluebook4.jpg" alt="" /></span>
                    <ul>
                        <li>지인이 결연을 원하는 경우, 블루북과 결연서를 함께    반송봉투에 넣어 보내주세요.</li>
                        <li>지인이 결연을 원하지 않는 경우, 블루북만 반송봉투에 넣어   보내주세요.</li>
                    </ul>
                </li>
            </ol>

            <div class="box-gray">
                <p class="bu">블루북을 작성해주신 모든 분들에게는 컴패션에서 준비한 선물을 보내드립니다.</p>
            </div>

            <p class="txt2">자주 묻는 질문</p>
            <ul class="list-faq">
                <li>
                    <p>완성된 블루북은 언제까지 보내면 되나요?</p>
                    블루북을 받으신 날짜로부터 한 달 동안 즐거운 시간을
                보내신 뒤, 컴패션에 보내주세요. 그러나 블루북 완성이 
                늦어지는 경우에도 걱정하지 마세요. 완성되는 대로 
                반송봉투에 넣어서 컴패션으로 보내주시면 됩니다. 
                단, 어린이가 오랫동안 후원자를 기다리지 않도록 하기 
                위해 어린이 결연은 결연서에 기재된 날짜까지만 
                가능합니다.
                </li>
                <li>
                    <p>블루북만 보내도 되나요?</p>
                    물론입니다!
                <br />
                    후원자님의 블루북을 통해 더 많은 분들이 후원의 
                가치를 알 수 있도록 컴패션 행사와 체험전 등을 통해 
                블루북을 전시할 예정입니다. 소개하고 싶은 지인이 
                없다면, 불루북만 반송봉투에 넣어서 보내주세요.
                </li>
                <li>
                    <p>블루북을 여러 지인에게 소개하고 싶은데 어쩌죠?</p>
                    동봉된 어린이 정보와 결연서가 더 필요하신 경우, 
                한국컴패션으로 연락해주세요. 담당자 확인 후, 
                후원자님 댁으로 발송해드리겠습니다.
                </li>
            </ul>

            
        </div>

        <!--//-->
    </div>

</asp:Content>
