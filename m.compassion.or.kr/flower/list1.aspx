﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="list.aspx.cs" Inherits="sponsor_children_default" MasterPageFile="~/main_without_header.Master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">	
	<meta name="keywords" content="<%:this.ViewState["meta_keyword"].ToString() %>"  />
	<meta name="description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:url" content="<%:this.ViewState["meta_url"].ToString() %>" />
	<meta property="og:title" content="<%:this.ViewState["meta_title"].ToString() %>" />
	<meta property="og:description" content="<%:this.ViewState["meta_description"].ToString() %>" />
	<meta property="og:image" content="<%:this.ViewState["meta_image"].ToString() %>" />
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script type="text/javascript" src="/sponsor/children/default.js"></script>
	<script type="text/javascript" src="/flower/list.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
    <input type="hidden" runat="server" id="s_country_list"/>
	<link rel="stylesheet" href="/flower/common/css/list.css">
 <style>
footer {
    display: none;
}
  </style>
<script type="text/javascript"> 

  var _ntp = {}; 

  _ntp.host = (('https:' == document.location.protocol) ? 'https://' : 'http://') 

  _ntp.dID = 779; 

  document.write(unescape("%3Cscript src='" + _ntp.host + "nmt.nsmartad.com/content?cid=1' type='text/javascript'%3E%3C/script%3E")); 

</script> 

<script>

 callback = function(){}

</script>
    
	<div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
<ul class="main-list main-list--red">
	<li ng-repeat="item in list">
		<div class="main-list__time">
			{{item.waitingdays}}일
		</div>
		<div class="main-list__wrap">
			<div class="main-list__img">
				<div class="main-list__imginner" style="background:url({{item.pic}}) no-repeat center top;">
					<span>{{item.name}}</span>
				</div>	
			</div>
			<div class="main-list__info">
				<div class="main-list__info-name">
					{{item.name}}
				</div>
				<div class="main-list__info-meta">
					{{item.countryname}}<br>
					{{item.birthdate | date:'yyyy.MM.dd'}}<br>
					{{item.age}}세, {{item.gender}}
				</div>
			</div>
		</div>
		<div class="main-list__btn">
<a href="/sponsor/children/?c={{item.childmasterid}}&<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_mWEB_direct&utm_source=mWEB_direct&utm_campaign=2017flower​&utm_medium=mWEB" %>" class="btn btn-donation" target="_blank" ng-click="showChild($event , item)" onclick="_nto.callTrack('5573', callback());" >후원하기</a>
		</div>
	</li>
</ul>
    
	</div>

</asp:Content>
