﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	<meta name="keywords" content="한국컴패션,compassion,어린이꽃이피었습니다,꽃들의이야기">
	<meta name="description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<meta property="og:title" content="한국컴패션">
	<meta property="og:url" content="http://www.compassion.or.kr/flower/">
	<meta property="og:image" content="images/logo.gif">
	<meta property="og:description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<link rel="shortcut icon" href="favicon.ico" >
	<script src="common/js/jquery-1.10.2.min.js"></script>
	<script src="common/js/bx/jquery.bxslider.min.js"></script>
	<link rel="stylesheet" href="common/css/common.css">
	<link rel="stylesheet" href="common/js/bx/jquery.bxslider.css">
</head>
<body>
<!--  LOG corp Web Analitics & Live Chat  START -->
<script  type="text/javascript">
//<![CDATA[
function logCorpAScript_full(){
	HTTP_MSN_MEMBER_NAME="";/*member name*/
	var prtc=(document.location.protocol=="https:")?"https://":"http://";
	var hst=prtc+"asp6.http.or.kr";
	var rnd="r"+(new Date().getTime()*Math.random()*9);
	this.ch=function(){
		if(document.getElementsByTagName("head")[0]){logCorpAnalysis_full.dls();}else{window.setTimeout(logCorpAnalysis_full.ch,30)}
	}
	this.dls=function(){
		var h=document.getElementsByTagName("head")[0];
		var s=document.createElement("script");s.type="text/jav"+"ascript";try{s.defer=true;}catch(e){};try{s.async=true;}catch(e){};
		if(h){s.src=hst+"/HTTP_MSN/UsrConfig/compassion/js/ASP_Conf.js?s="+rnd;h.appendChild(s);}
	}
	this.init= function(){
		document.write('<img src="'+hst+'/sr.gif?d='+rnd+'" style="width:1px;height:1px;position:absolute;display:none" onload="logCorpAnalysis_full.ch()" alt="" />');
	}
}
if(typeof logCorpAnalysis_full=="undefined"){var logCorpAnalysis_full=new logCorpAScript_full();logCorpAnalysis_full.init();}
//]]>
</script>
<noscript><img src="http://asp6.http.or.kr/HTTP_MSN/Messenger/Noscript.php?key=compassion" style="display:none;width:0;height:0;" alt="" /></noscript>
<!-- LOG corp Web Analitics & Live Chat END -->
<script type="text/javascript"> 

  var _ntp = {}; 

  _ntp.host = (('https:' == document.location.protocol) ? 'https://' : 'http://') 

  _ntp.dID = 779; 

  document.write(unescape("%3Cscript src='" + _ntp.host + "nmt.nsmartad.com/content?cid=1' type='text/javascript'%3E%3C/script%3E")); 

</script> 

<script>

 callback = function(){}

</script>

<div id="wrap" class="lighten">
	<!-- header -->
	<header class="header">
		<div class="header-wrap">
			<h1 class="logo">
                <script>
                    function getHost() {
                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'm.compassion.or.kr';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'm.compassionkr.com';
                    else if (loc.indexOf('localhost') != -1) host = 'localhost:35563';
                    return host;
                }

                function goLink(param) {
                    var host = getHost();
                    if (host != '') location.href = 'http://' + host + param;
                }
                </script>
				<a href="javascript:goLink('')" onclick="_nto.callTrack('5617', callback());"><img src="images/logo.jpg" alt="한국컴패션"></a>
			</h1>
			<div class="snb">
				<a href="https://www.facebook.com/compassion.Korea/" target="_blank">
					<img src="images/btn_facebook.png" alt="페이스북">
				</a>
				<a href="https://www.instagram.com/compassionkorea/" target="_blank">
					<img src="images/btn_instagram.png" alt="인스타그램">
				</a>
				<a href="http://happylog.naver.com/compassion.do" target="_blank">
					<img src="images/btn_naver.png" alt="네이버">
				</a>
			</div>
		</div>
		<div class="gnb">
			<a href="/flower/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">꽃들의 이야기</a>
			<a href="people.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">꽃 피우는 사람들</a>
			<a href="donation.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">꽃으로 전하는 행복</a>
		</div>
		<a class="btn btn-top" href="#">
			<img src="images/btn_top.png" alt="TOP">
		</a>
	</header>
	<!-- //header -->
	<div id="container">
        <a class="main-link" href="http://m.compassion.or.kr/sponsor/children/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_mWEB_direct&utm_source=mWEB_direct&utm_campaign=2017flower &utm_medium=mWEB" %>&#anchor_childlist" target="_blank" onclick="_nto.callTrack('5618', callback());">1:1 어린이 후원하기</a>	
		<section class="content content-donation">
			<article class="cd-intro">
				<img src="images/img_donation.jpg" alt="행복이 피어나길, 플라워 서브스크립션 브랜드 모이에서 한국컴패션 어린이 꽃이 피었습니다 캠페인을 응원합니다.">
			</article>
			<article class="cd-info">
				<div class="cd-meta">
					<img src="images/img_donationinfo.png" alt="1:1어린이 후원을 시작하신 모든 분께 Blooming wishes box를 드립니다. 후원을 시작하신 모든분, 캠페인 사이트내 후원하기 버튼을 클릭하여 시작하신 분들에 한함, 후원 감사 꽃다발과 엽서 카드 증정, 2017.06.30~09.10, 후원완료후 2주 이내 배송예정">
					<a href="https://goo.gl/forms/Z3nQdG8aceMKJAvg2" target="_blank" onclick="_nto.callTrack('5623', callback());">
						<img src="images/btn_donation1.png" alt="1:1어린이 후원하기">
					</a>
				</div>
				<div class="cd-present">
					<img src="images/img_donationinfo2.png" alt="메시지를 남기면 송은이, 김범수가 간다!	메시지 작성하기 -> 개인정보 입력후 후원다짐 또는 추천 메시지 작성 ->당첨자 개별 연락">
					<a href="http://m.compassion.or.kr/sponsor/children/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_mWEB_direct&utm_source=mWEB_direct&utm_campaign=2017flower​&utm_medium=mWEB" %>&#anchor_childlist" onclick="_nto.callTrack('5576', callback());" >
						<img src="images/btn_story.png" alt="메시지 작성하기">
					</a>
				</div>
			</article>
		</section>
		<script>
		$(function () {
			// scroll
			$(window).scroll(function () {
				var top = $(window).scrollTop();
				if (top > 200) {
					$('.header').addClass('scrolled');
				} else {
					$('.header').removeClass('scrolled');					
				}
			})
		})
		</script>		
	</div>
	<!-- footer -->
	<footer class="footer">
		<div class="footer-logo">
			<img src="images/logo_footer.png" alt="한국컴패션">
		</div>
		<div class="footer-info">
			한국컴패션<br>
			사업자등록번호 : 108-82-05789     |    대표자 : 서정인<br>
			(04418) 서울시 용산구 한남대로 102-5<br>
			(한남동 723-41) 석전빌딩<br>
			후원상담/안내 : 02-740-1000 (평일 09:00 ~ 18:00) <br>
			팩스 : 02-740-1001 이메일 : info@compassion.or.kr
		</div>
		<div class="footer-copyright">
			© COMPASSION KOREA All Rights Reserved. Contact us for more information
		</div>
	</footer>
	<!-- //footer -->
</div>
</body>
</html>