﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class TopMaster : System.Web.UI.MasterPage {

	protected void Page_Load(object sender, EventArgs e) {

		if(!IsPostBack) {
            
			this.OnBeforePostBack();
		}

		/*
		var userAddr = Request.UserHostAddress;
		if (!(userAddr.IndexOf("192.168") > -1
			|| userAddr == "127.0.0.1"
			|| userAddr.IndexOf("10.10.100") > -1		//vpn
			|| userAddr == "183.98.89.248"		// 개발서버
			|| userAddr.IndexOf("211.35.74") > -1   // 펜타내부
			)) {
			Response.Clear();
			Response.End();
		}
		*/

		//	if (!IsPostBack) {
		/*
		is_login.Value = "N";
		if(UserInfo.IsLogin) {
			var sess = new UserInfo();
			ph_after_login.Visible = true;
			is_login.Value = "Y";
			_userid.Value = sess.UserId;
			user_id.Text = string.Format("{0}님&nbsp;" , sess.UserName);

			var actionResult = new LetterAction().GetUnreadCount();
			if(actionResult.success) {
				var count = ((LetterAction.UnreadEntity)actionResult.data).cnt;
				letter_count.Visible = count > 0;
				letter_count.InnerHtml = count > 99 ? "99+" : count.ToString();
			} else {
				letter_count.Visible = false;
			}
			

		} else {
			ph_before_login.Visible = true;
		}
		*/

		//	_loggedin.Value = FrontLoginSession.HasCookie(this.Context) ? "1" : "0";
		//	this.OnBeforePostBack();
		//	}

	}

	protected virtual void OnBeforePostBack() {

        this.ViewState["is_login"] = "N";
		this.ViewState["_userid"] = "";
		this.ViewState["_appUserInfo"] = "";

		var isApp = AppSession.HasCookie(this.Context);
		//	mobileweb_gnb.Visible = ph_mobileweb_content1.Visible = ph_mobileweb_content2.Visible = !isApp;
		//	app_gnb.Visible = ph_app_header.Visible = isApp;
		ph_mobileweb_content1.Visible = ph_mobileweb_content2.Visible = !isApp;
		ph_app_header.Visible = isApp;

		floating_mw.Visible = !isApp;
		floating_app.Visible = isApp;

		

	}

	public virtual ContentPlaceHolder Content
	{
		get
		{
			return this.body;
		}
	}

	public virtual string Title
	{
		get
		{
			return title.InnerText;
		}
		set
		{
			title.InnerText = value;
		}
	}

}
