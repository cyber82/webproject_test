﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	<meta name="keywords" content="한국컴패션,compassion,어린이꽃이피었습니다,꽃들의이야기">
	<meta name="description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<meta property="og:title" content="한국컴패션">
	<meta property="og:url" content="http://www.compassion.or.kr/flower/">
	<meta property="og:image" content="images/logo.gif">
	<meta property="og:description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<link rel="shortcut icon" href="favicon.ico" >
	<script src="common/js/jquery-1.10.2.min.js"></script>
	<script src="common/js/bx/jquery.bxslider.min.js"></script>
	<link rel="stylesheet" href="common/css/common.css">
	<link rel="stylesheet" href="common/js/bx/jquery.bxslider.css">
</head>
<body>
<!--  LOG corp Web Analitics & Live Chat  START -->
<script  type="text/javascript">
//<![CDATA[
function logCorpAScript_full(){
	HTTP_MSN_MEMBER_NAME="";/*member name*/
	var prtc=(document.location.protocol=="https:")?"https://":"http://";
	var hst=prtc+"asp6.http.or.kr";
	var rnd="r"+(new Date().getTime()*Math.random()*9);
	this.ch=function(){
		if(document.getElementsByTagName("head")[0]){logCorpAnalysis_full.dls();}else{window.setTimeout(logCorpAnalysis_full.ch,30)}
	}
	this.dls=function(){
		var h=document.getElementsByTagName("head")[0];
		var s=document.createElement("script");s.type="text/jav"+"ascript";try{s.defer=true;}catch(e){};try{s.async=true;}catch(e){};
		if(h){s.src=hst+"/HTTP_MSN/UsrConfig/compassion/js/ASP_Conf.js?s="+rnd;h.appendChild(s);}
	}
	this.init= function(){
		document.write('<img src="'+hst+'/sr.gif?d='+rnd+'" style="width:1px;height:1px;position:absolute;display:none" onload="logCorpAnalysis_full.ch()" alt="" />');
	}
}
if(typeof logCorpAnalysis_full=="undefined"){var logCorpAnalysis_full=new logCorpAScript_full();logCorpAnalysis_full.init();}
//]]>
</script>
<noscript><img src="http://asp6.http.or.kr/HTTP_MSN/Messenger/Noscript.php?key=compassion" style="display:none;width:0;height:0;" alt="" /></noscript>
<!-- LOG corp Web Analitics & Live Chat END -->
<script type="text/javascript"> 

  var _ntp = {}; 

  _ntp.host = (('https:' == document.location.protocol) ? 'https://' : 'http://') 

  _ntp.dID = 779; 

  document.write(unescape("%3Cscript src='" + _ntp.host + "nmt.nsmartad.com/content?cid=1' type='text/javascript'%3E%3C/script%3E")); 

</script> 

<script>

 callback = function(){}

</script>

<div id="wrap" class="lighten">
	<!-- header -->
	<header class="header">
		<div class="header-wrap">
			<h1 class="logo">
                <script>
                    function getHost() {
                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'm.compassion.or.kr';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'm.compassionkr.com';
                    else if (loc.indexOf('localhost') != -1) host = 'localhost:35563';
                    return host;
                }

                function goLink(param) {
                    var host = getHost();
                    if (host != '') location.href = 'http://' + host + param;
                }
                </script>
				<a href="javascript:goLink('');" onclick="_nto.callTrack('5614', callback());"><img src="images/logo.jpg" alt="한국컴패션"></a>
			</h1>
			<div class="snb">
				<a href="https://www.facebook.com/compassion.Korea/" target="_blank">
					<img src="images/btn_facebook.png" alt="페이스북">
				</a>
				<a href="https://www.instagram.com/compassionkorea/" target="_blank">
					<img src="images/btn_instagram.png" alt="인스타그램">
				</a>
				<a href="http://happylog.naver.com/compassion.do" target="_blank">
					<img src="images/btn_naver.png" alt="네이버">
				</a>
			</div>
		</div>
		<div class="gnb">
			<a href="/flower/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">꽃들의 이야기</a>
			<a href="people.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">꽃 피우는 사람들</a>
			<a href="donation.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">꽃으로 전하는 행복</a>
		</div>
		<a class="btn btn-top" href="#">
			<img src="images/btn_top.png" alt="TOP">
		</a>
	</header>
	<!-- //header -->
	<div id="container">	
		<!-- 고정 링크 -->
		<a class="main-link" href="http://m.compassion.or.kr/sponsor/children/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_mWEB_direct&utm_source=mWEB_direct&utm_campaign=2017flower​&utm_medium=mWEB" %>&#anchor_childlist" target="_blank" onclick="_nto.callTrack('5615', callback());">1:1 어린이 후원하기</a>
		<section class="content content-people">
			<article class="cp-intro">
				<img src="images/img_people1.jpg" alt="메마른 땅에서도 피어나는 아름다운 꽃처럼 존재 자체만으로 존귀하고 랑받아 마땅한  어린이들이 꽃처럼 활짝 피어날 수 있도록 모인 사람들이 있습니다. 카네이션   꽃 한 송은이  꽃범수이들과 함께하는 어린이 꽃피우기, 시작합니다!">
			</article>
			<article class="cp-sean">
				<img src="images/img_people2.jpg" alt="활짝 꽃 피어야 할 많은 어린이들을 위해 어린이 미소를 닮은 꽃다발을 만들고,이 꽃다발의 의미를 후원자님들과 함께 공유하고 싶어요.- 션 정혜영 부부 -">
				<%--<a href="#none">
					<img src="images/img_people3.jpg" alt="1일 플로리스트 션 정혜영 부부와의 만남을 공개합니다!">
				</a>--%>
                <div class="cp-sean__wrap">
					<img src="images/img_people3.jpg" alt="1일 플로리스트 션 정혜영 부부와의 만남을 공개합니다!">
					<iframe src="https://www.youtube.com/embed/c5-AwGNv3sY?rel=0&autoplay=1" frameborder="0" allowfullscreen=""></iframe>
				</div>
				<img src="images/img_people4.jpg" alt="한국컴패션">
			</article>
			<article class="cp-guest">
				<img src="images/img_people5.jpg" alt="어린이 꽃을 피우기 위해 꽃다발 라이더 송은이, 김범수가 당신을 만나러 갑니다!">
				<div class="cp-guest__video">
					<div class="t">
						<div class="i">
							<img src="images/img_big.jpg" alt="김범수">							
						</div>
					</div>
					<div class="b">
						<div class="l">
							<a href="#none" class="p p1" data-video='<iframe src="https://www.youtube.com/embed/SZwUGe6iAZU" frameborder="0" allowfullscreen></iframe>'>
								<img src="images/img_play1.jpg" alt="김범수" >
							</a>
						</div>
						<div class="r">
							<a href="#none" class="p p2" data-video='<iframe src="https://www.youtube.com/embed/3OxXtheMTIY" frameborder="0" allowfullscreen></iframe>'>
								<img src="images/img_play2.jpg" alt="송은이">
							</a>
						</div>
					</div>
				</div>

				<img src="images/img_people5.jpg" alt="어린이 꽃을 피우기 위해 꽃다발 라이더 송은이, 김범수가 당신을 만나러 갑니다!">
				<a href="donation.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>" onclick="_nto.callTrack('5582', callback());" >
					<img src="images/img_people6.jpg" alt="1일 플로리스트 션 정혜영 부부와의 만남을 공개합니다!">
				</a>
				<img src="images/img_people7.jpg" alt="한국컴패션">	
				<div class="cp-guest__wrap">
					<img src="images/img_people8.jpg" alt="송은이 김범수">
				</div>
			</article>
			<article class="cp-concert">
				<img src="images/img_people9.jpg" alt="컴패션 꽃들이 한 자리에 총 출동! 어린이 꽃 피우기 좋은 여름 밤에 찾아갑니다! 세부 일정, 장소, 라인업 정보는 조금 더 기다려주세요.">			
				<div class="cp-concert__btn">
					<a href="http://m.compassion.or.kr/participation/event/view/50?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flowercert_mWEB_direct&utm_source=mWEB_direct&utm_campaign=2017flowercert​&utm_medium=mWEB" %>">
						<img src="images/btn_peopledetail.png" alt="꽃서트 자세히 보기">
					</a>
				</div>	
			</article>
			<article class="cp-donation">
				<div class="cp-donation__tit">
					<img src="images/img_people11.png" alt="어린이 꽃에게 당신의 손길을 내어주세요. 어린이 꽃을 피우기 위한 사람들의 만남과 만남이 더해져 컴패션이 커지고 커져가는 컴패션 만큼 더 많은어린이 꽃이 피어납니다.">
				</div>
				<div class="cp-donation__btn">
					<a href="http://m.compassion.or.kr/sponsor/children/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_mWEB_direct&utm_source=mWEB_direct&utm_campaign=2017flower​&utm_medium=mWEB" %>&#anchor_childlist" target="_blank" onclick="_nto.callTrack('5584', callback());">
						<img src="images/btn_peopledonation.png" alt="1:1어린이 후원하기">
					</a>
				</div>
			</article>
		</section>
		<script>
		$(function () {
			$('[data-video]').click(function (e) {				
				var con = $(this).attr('data-video');
				var tit = $(this).attr('data-tit');
				$('.cp-guest__video .i').html(con);
				$('.cp-guest__video .tit').html(tit);
				$(this).addClass('active');
				$('[data-video]').not($(this)).removeClass('active');
				$('.p2').removeClass('active');
				e.preventDefault();
			})
			$('.p2').click(function () {
				$(this).addClass('active');
				$('[data-video]').not($(this)).removeClass('active');				
			})
			// scroll
			$(window).scroll(function () {
				var top = $(window).scrollTop();
				if (top > 200) {
					$('.header').addClass('scrolled');
				} else {
					$('.header').removeClass('scrolled');					
				}
			})
		})
		</script>		
	</div>
	<!-- footer -->
	<footer class="footer">
		<div class="footer-logo">
			<img src="images/logo_footer.png" alt="한국컴패션">
		</div>
		<div class="footer-info">
			한국컴패션<br>
			사업자등록번호 : 108-82-05789     |    대표자 : 서정인<br>
			(04418) 서울시 용산구 한남대로 102-5<br>
			(한남동 723-41) 석전빌딩<br>
			후원상담/안내 : 02-740-1000 (평일 09:00 ~ 18:00) <br>
			팩스 : 02-740-1001 이메일 : info@compassion.or.kr
		</div>
		<div class="footer-copyright">
			© COMPASSION KOREA All Rights Reserved. Contact us for more information
		</div>
	</footer>
	<!-- //footer -->
</div>
</body>
</html>