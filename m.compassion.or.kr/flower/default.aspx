﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>한국컴패션 - 꿈을 잃은 어린이들에게 그리스도의 사랑을</title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	<meta name="keywords" content="한국컴패션,compassion,어린이꽃이피었습니다,꽃들의이야기">
	<meta name="description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<meta property="og:title" content="한국컴패션">
	<meta property="og:url" content="http://www.compassion.or.kr/flower/">
	<meta property="og:image" content="images/logo.gif">
	<meta property="og:description" content="어린이 꽃이 피었습니다. 한국컴패션">
	<link rel="shortcut icon" href="favicon.ico" >
	<script src="common/js/jquery-1.10.2.min.js"></script>
	<script src="common/js/bx/jquery.bxslider.min.js"></script>
	<script src="common/js/jquery.easing.1.3.min.js"></script>
	<link rel="stylesheet" href="common/css/common.css">
	<link rel="stylesheet" href="common/js/bx/jquery.bxslider.css">
</head>
<body>
<!--  LOG corp Web Analitics & Live Chat  START -->
<script  type="text/javascript">
//<![CDATA[
function logCorpAScript_full(){
	HTTP_MSN_MEMBER_NAME="";/*member name*/
	var prtc=(document.location.protocol=="https:")?"https://":"http://";
	var hst=prtc+"asp6.http.or.kr";
	var rnd="r"+(new Date().getTime()*Math.random()*9);
	this.ch=function(){
		if(document.getElementsByTagName("head")[0]){logCorpAnalysis_full.dls();}else{window.setTimeout(logCorpAnalysis_full.ch,30)}
	}
	this.dls=function(){
		var h=document.getElementsByTagName("head")[0];
		var s=document.createElement("script");s.type="text/jav"+"ascript";try{s.defer=true;}catch(e){};try{s.async=true;}catch(e){};
		if(h){s.src=hst+"/HTTP_MSN/UsrConfig/compassion/js/ASP_Conf.js?s="+rnd;h.appendChild(s);}
	}
	this.init= function(){
		document.write('<img src="'+hst+'/sr.gif?d='+rnd+'" style="width:1px;height:1px;position:absolute;display:none" onload="logCorpAnalysis_full.ch()" alt="" />');
	}
}
if(typeof logCorpAnalysis_full=="undefined"){var logCorpAnalysis_full=new logCorpAScript_full();logCorpAnalysis_full.init();}
//]]>
</script>
<noscript><img src="http://asp6.http.or.kr/HTTP_MSN/Messenger/Noscript.php?key=compassion" style="display:none;width:0;height:0;" alt="" /></noscript>
<!-- LOG corp Web Analitics & Live Chat END -->
<div id="wrap">
	<!-- header -->
<script type="text/javascript"> 

  var _ntp = {}; 

  _ntp.host = (('https:' == document.location.protocol) ? 'https://' : 'http://') 

  _ntp.dID = 779; 

  document.write(unescape("%3Cscript src='" + _ntp.host + "nmt.nsmartad.com/content?cid=1' type='text/javascript'%3E%3C/script%3E")); 

</script> 

<script>

 callback = function(){}

</script>


	<header class="header">
		<div class="header-wrap">
			<h1 class="logo">
                <script>
                    function getHost() {
                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'm.compassion.or.kr';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'm.compassionkr.com';
                    else if (loc.indexOf('localhost') != -1) host = 'localhost:35563';
                    return host;
                }

                function goLink(param) {
                    var host = getHost();
                    if (host != '') location.href = 'http://' + host + param;
                }
                </script>
				<a href="javascript:goLink('')" onclick="_nto.callTrack('5571', callback());"><img src="images/logo.jpg" alt="한국컴패션"></a>
			</h1>
			<div class="snb">
				<a href="https://www.facebook.com/compassion.Korea/" target="_blank">
					<img src="images/btn_facebook.png" alt="페이스북">
				</a>
				<a href="https://www.instagram.com/compassionkorea/" target="_blank">
					<img src="images/btn_instagram.png" alt="인스타그램">
				</a>
				<a href="http://happylog.naver.com/compassion.do" target="_blank">
					<img src="images/btn_naver.png" alt="네이버">
				</a>
			</div>
		</div>
		<div class="gnb">
			<a href="?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">꽃들의 이야기<span>어린이 스토리</span></a>
			<a href="people.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">꽃 피우는 사람들<span>션·정혜영의<br>1일 플로리스트</span></a>
			<a href="donation.aspx?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>">꽃으로 전하는 행복<span>송은이·김범수의<br>꽃배달</span></a>
		</div>
        <div class="lnb"></div>
		<a class="btn btn-top" href="#">
			<img src="images/btn_top.png" alt="TOP">
		</a>
	</header>
	<!-- //header -->
	<div id="container">
		<!-- 고정 링크 -->
		<a class="main-link" href="javascript:goLink('/sponsor/children/?<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('_').Length > 1 ? HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "e_id=13&e_src=2017flower_mWEB_direct&utm_source=mWEB_direct&utm_campaign=2017flower​&utm_medium=mWEB" %>&#anchor_childlist');" class="btn-maindonation" target="_blank" onclick="_nto.callTrack('5572', callback());">1:1 어린이 후원하기</a>
		<!-- 메인 슬라이드 -->
		<section class="main-visual">
			<ul class="main-slider">
				<li class="first">
					<img src="images/kid_1_black.jpg" alt="어린이 꽃이 피었습니다">
					<img class="main-slider__color" src="images/kid_1_color.jpg" alt="어린이 꽃이 피었습니다">
				</li>
				<li>
					<img src="images/kid_2_black.jpg" alt="어린이 꽃이 피었습니다">
					<img class="main-slider__color" src="images/kid_2_color.jpg" alt="어린이 꽃이 피었습니다">
				</li>
				<li>
					<img src="images/kid_3_black.jpg" alt="어린이 꽃이 피었습니다">
					<img class="main-slider__color" src="images/kid_3_color.jpg" alt="어린이 꽃이 피었습니다">
				</li>
				<li>
					<img src="images/kid_4_black.jpg" alt="어린이 꽃이 피었습니다">
					<img class="main-slider__color" src="images/kid_4_color.jpg" alt="어린이 꽃이 피었습니다">
				</li>
			</ul>
			<div class="btn btn-scrolldown">
				<img src="images/btn_down.png" alt="DOWN">
			</div>
		</section>
		<!-- //메인 슬라이드 -->
		<!-- 메인 스토리 -->
		<!-- [D] - 스토리 컬러에 따라서 클래스 추가 -->
		<section class="main-story main-story--red">
			<div class="main-story__img">
				<img src="images/img_story1.jpg" alt="">
			</div>
			<!-- [D] 첫번째 후원 리스트 들어갈곳 -->
			<div class="main-listwrap">
				<iframe id="ifrm1" src="" frameborder="0" class="main-story__iframe" scrolling="no"></iframe>
                <script>
                    var host = getHost();
                    $('#ifrm1').attr('src', 'http://'+host+'/flower/list1?gender=F&minAge=3&maxAge=10&<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>');
                </script>
			</div>
		</section>
		<!-- [D] - 스토리 컬러에 따라서 클래스 추가 -->		
		<section class="main-story main-story--blue">
			<div class="main-story__img">
				<img src="images/img_story2.jpg" alt="">
			</div>
			<!-- [D] 두번째 후원 리스트 들어갈곳 -->
			<div class="main-listwrap">
				<iframe id="ifrm2" src="" frameborder="0" class="main-story__iframe" scrolling="no"></iframe>
                <script>
                    var host = getHost();
                    $('#ifrm2').attr('src', 'http://'+host+'/flower/list2?gender=M&minAge=3&maxAge=10&<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>');
                </script>
			</div>
		</section>
		<!-- [D] - 스토리 컬러에 따라서 클래스 추가 -->		
		<section class="main-story main-story--yellow">
			<div class="main-story__img">
				<img src="images/img_story3.jpg" alt="">
			</div>
			<!-- [D] 세번째 후원 리스트 들어갈곳 -->
			<div class="main-listwrap">
				<iframe id="ifrm3" src="" frameborder="0" class="main-story__iframe" scrolling="no"></iframe>
                <script>
                    var host = getHost();
                    $('#ifrm3').attr('src', 'http://'+host+'/flower/list3?minAge=8&<%= HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?').Length > 1 ? "" + HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split('?')[1] : "" %>');
                </script>
			</div>
		</section>
		<!-- //메인 스토리 -->
<script>
		$(function () {
			// scroll
			$('.btn-scrolldown').bind('click',function (e) {
				var top = $('.main-story--red').position().top;
				$('html,body').animate({
					'scrollTop' : top
				},800,'easeInOutExpo');
				e.preventDefault();
			})
			$('.main-slider').bxSlider({
				pager:false,
				controls:false,
				oneToOneTouch:false,
				auto:true,
				onSliderLoad : function () {
					$('.main-slider li.first').eq(0).find('.main-slider__color').fadeIn();
				},
				onSlideAfter : function ($slideElement, oldIndex, newIndex) {
					$('.main-slider li').not($slideElement).find('.main-slider__color').hide();
					$slideElement.find('.main-slider__color').fadeIn();
				}
			})
			// scroll
			$(window).scroll(function () {
				var top = $(window).scrollTop();
				if (top > 200) {
					$('.header').addClass('scrolled');
				} else {
					$('.header').removeClass('scrolled');					
				}
			})
		})
		</script>
	<!-- footer -->
	<footer class="footer">
		<div class="footer-logo">
			<img src="images/logo_footer.png" alt="한국컴패션">
		</div>
		<div class="footer-info">
			한국컴패션<br>
			사업자등록번호 : 108-82-05789     |    대표자 : 서정인<br>
			(04418) 서울시 용산구 한남대로 102-5<br>
			(한남동 723-41) 석전빌딩<br>
			후원상담/안내 : 02-740-1000 (평일 09:00 ~ 18:00) <br>
			팩스 : 02-740-1001 이메일 : info@compassion.or.kr
		</div>
		<div class="footer-copyright">
			© COMPASSION KOREA All Rights Reserved. Contact us for more information
		</div>
	</footer>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '235171356890594'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=235171356890594&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->




	<!-- //footer -->
</div>
</body>
</html>