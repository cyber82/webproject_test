﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class join : FrontBasePage {


	protected override void OnBeforePostBack() {

//
		var url = "";
		if(Request.UrlReferrer != null)
			url = Request.UrlReferrer.AbsoluteUri;
		Response.Redirect(ConfigurationManager.AppSettings["domain_auth"] + "/m/join/?app_ex=1&r=" + HttpUtility.UrlEncode(url.Replace("default.aspx", "").Replace(".aspx", "")));
	}

}