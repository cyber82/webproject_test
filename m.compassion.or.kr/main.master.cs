﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class MainMaster : System.Web.UI.MasterPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            this.OnBeforePostBack();
        }

    }

    protected virtual void OnBeforePostBack()
    {
        var isApp = AppSession.HasCookie(this.Context);

        if (isApp)
        {
            ph_app_bottom.Visible = true;

            var sponsorType = new UserInfo().SponsorType;
            if (sponsorType == "머니" || sponsorType == "머니/일반")
            {
                tab_letter.HRef = "javascript:alert('후원자님께서는 어린이와 편지를 주고 받지 않으시는 머니 후원 중이십니다.감사합니다.');";
            }

        }

    }

    public virtual string Title
    {
        get
        {
            return Master.Title;
        }
        set
        {
            Master.Title = value;
        }
    }

}
