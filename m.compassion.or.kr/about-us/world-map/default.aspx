﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="world_map" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script>

		$(function () {
			$(".country").click(function () {
				country = $(this).data("id");

				location.href = "/about-us/world-map/view/?c=" + country;
				return false;
			})

			$(".teb_menu").click(function () {

				if ($(this).hasClass("selected")) {
					$(this).find(".sub-country").slideUp(300);
					$(this).removeClass("selected");
				} else {
					$(this).find(".sub-country").slideDown(300);
					$(this).addClass("selected");

				}
			})
		})
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	<div class="wrap-sectionsub">
	<!---->
    
		<div class="about-with">
    	
			<div class="wrap-visualtop sectionsub-margin1" style="background:url('/common/img/page/about/img9.jpg') no-repeat">
				<div class="dim"></div>    <!--dim-->
				<div class="contain" style="padding-top:30px">    <!--padding으로 컨텐츠 가운데에 맞게 조정-->
					<strong class="txt-slogon">전 세계 컴패션은 함께 일합니다</strong>
					컴패션은 어린이를 양육하는<br />
					현지 26개 수혜국과 12개 후원국이 하나가 되어,<br />
					서로의 역할을 인식하고, 서로의 관점을 인정하며<br />
					협력하여 일합니다.
				</div>
			</div>
        
			<p class="txt-country sectionsub-margin1">수혜국<em>FIELD COUNTRY</em></p>
			<ul class="menu-country sectionsub-margin1">
        		<li class="teb_menu"><span class="field" onClick="">아시아<span class="bu"></span></span>    <!--열린상태 : <li class="selected">  /  닫힌상태 : <li>-->
            		<ul class="sub-country">
                		<li><a class="country" data-id="BD" href="#" >방글라데시</a></li>
						<li><a class="country" data-id="IO" href="#" >인도네시아</a></li>
						<li><a class="country" data-id="PH" href="#" >필리핀</a></li>
						<li><a class="country" data-id="TH" href="#" >태국</a></li>
						<li><a class="country" data-id="LK" href="#" >스리랑카</a></li>
					</ul>
				</li>
				<li class="teb_menu"><span class="field" onClick="">아프리카<span class="bu"></span></span>    <!--열린상태 : <li class="selected">  /  닫힌상태 : <li>-->
            		<ul class="sub-country">
                		<li><a class="country" data-id="BF" href="#" >부르키나파소</a></li>
						<li><a class="country" data-id="ET" href="#" >에티오피아</a></li>
						<li><a class="country" data-id="GH" href="#" >가나</a></li>
						<li><a class="country" data-id="KE" href="#" >케냐</a></li>
						<li><a class="country" data-id="RW" href="#" >르완다</a></li>
						<li><a class="country" data-id="TZ" href="#" >탄자니아</a></li>
						<li><a class="country" data-id="UG" href="#" >우간다</a></li>
						<li><a class="country" data-id="TG" href="#" >토고</a></li>
					</ul>
				</li>
				<li class="teb_menu"><span class="field" onClick="">남미<span class="bu"></span></span>    <!--열린상태 : <li class="selected">  /  닫힌상태 : <li>-->
            		<ul class="sub-country">
                		<li><a class="country" data-id="BO" href="#" >볼리비아</a></li>
						<li><a class="country" data-id="BR" href="#" >브라질</a></li>
						<li><a class="country" data-id="CO" href="#" >콜롬비아</a></li>
						<li><a class="country" data-id="EC" href="#" >에콰도르</a></li>
						<li><a class="country" data-id="PE" href="#" >페루</a></li>
					</ul>
				</li>
				<li class="teb_menu"><span class="field" onClick="">중미와 카리브연안<span class="bu"></span></span>    <!--열린상태 : <li class="selected">  /  닫힌상태 : <li>-->
            		<ul class="sub-country">
                		<li><a class="country" data-id="DR" href="#" >도미니카 공화국</a></li>
						<li><a class="country" data-id="ES" href="#" >엘살바도르</a></li>
						<li><a class="country" data-id="GU" href="#" >과테말라</a></li>
						<li><a class="country" data-id="HA" href="#" >아이티</a></li>
						<li><a class="country" data-id="HO" href="#" >온두라스</a></li>
						<li><a class="country" data-id="ME" href="#" >멕시코</a></li>
						<li><a class="country" data-id="NI" href="#" >니카라과</a></li>
					</ul>
				</li>
			</ul>
        
			<div class="partner-office sectionsub-margin1">
        		<p class="txt-caption">후원국<em>GLOBAL PARTNER OFFICES</em></p>
				<ul class="list-partner">
            		<li>한국</li>
					<li>미국</li>
					<li>프랑스</li>
					<li>뉴질랜드</li>
					<li>독일</li>
					<li>영국</li>
					<li>이탈리아</li>
					<li>호주</li>
					<li>스위스</li>
					<li>네덜란드</li>
					<li>캐나다</li>
					<li>노르덴*</li>
				</ul>
				<p class="txt">* 노르덴컴패션(Compassion Norden)은 노르웨이, 스웨덴, 핀란드, 덴마크의 북유럽 국가들에서 컴패션 후원자와 함께하고 있습니다.</p>
			</div>

        
		</div>
    
	<!--//-->
	</div>





</asp:Content>
