﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="world_map_view" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
	<script>

		$(function () {


			country = {
				"GH": "가나",
				"BF": "부르키나파소",
				"TG": "토고",
				"UG": "우간다",
				"RW": "르완다",
				"LK": "스리랑카",
				"ET": "에티오피아",
				"KE": "케냐",
				"TZ": "탄자니아",
				"IN": "인도",
				"BD": "방글라데시",
				"TH": "태국",
				"PH": "필리핀",
				"IO": "인도네시아",
				"ME": "멕시코",
				"GU": "과테말라",
				"ES": "엘살바도르",
				"NI": "니카리과",
				"CO": "콜롬비아",
				"EC": "에콰도르",
				"PE": "페루",
				"BO": "볼리비아",
				"HA": "아이티",
				"DR": "도미니카공화국",
				"HO": "온두라스",
				"BR": "브라질"
			}

			var en_c = getParameterByName("c");
			var ko_c = country[en_c];

			target = $(".wrap-board[data-id=" + en_c + "]");

			target.show();

			

		})

		

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
	
<div class="wrap-sectionsub">
<!---->
    
    <div class="about-with">
    	
		<!-- 가나 GH -->
		<div class="wrap-board" style="display:none;" data-id="GH">
        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>가나(GH)</em></span>
                    가나 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/GH_가나.jpg" alt="" width="100%" />
                    <strong>가나컴패션은 2005년부터 지금까지 계속되고 있습니다.</strong>
                    가나 정부는 기본적인 교육을 아주 중요하게 다루며 지원하고 있습니다. 
                    1996년 의회는 자율적이고, 힘있는 기초적인 교육 시스템을 시작하는 조항을 만들었으며 이것은 서아프리카에서 가장 주목받고 있는 교육 프로그램 중 하나입니다. 
                    2004년에는 가나의 500개가 넘는 고등학교에서 9만 명의 졸업생을 배출했으며, 엄청난 성장을 보여주었습니다. 하지만 교육 시설에는 많은 한계가 있습니다. 
                    많은 어린이들은 학교에 가기 위해 먼 길을 걸어가야 하며 대부분의 교실은 기본 준비물과 학습 자료를 충분히 갖추지 못하고 있습니다. <br /><br />
                    가나컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하며, 균형 잡힌 식사를 하지 못하고 배가 고픈 상태로 오는 어린이들을 위해 식사를 제공합니다. 
                    특별히 영양실조에 처한 아이들에게는 달걀, 우유, 콩, 쌀, 기름과 같은 식료품을 제공합니다. 다양한 청소년 직업훈련을 실시하며 학부모 모임에서는 학부모를 대상으로 올바른 양육 훈련과 수입창출 교육을 받습니다. 
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    도시와 농촌의 양극화 현상 완화를 위해 농촌과 도심에 다양한 직업의 기회가 제공되도록 기도해주세요.<br /><br />
                    도심 지역의 인구 밀집 현상 해소를 위해 기도해주세요.<br /><br />
                    어린이들이 안전한 집에 거주할 수 있도록 기도해주세요.<br /><br />
                    어린이들이 안전한 물을 마음껏 마실 수 있도록 기도해주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=gh">가나컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>

		<!-- 과테말라 GU -->
		<div class="wrap-board" style="display:none;" data-id="GU">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>과테말라(GU)</em></span>
                    과테말라 어린이의 손을 잡아주세요!
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/GU_과테말라.jpg" alt="" width="100%" />
                    <strong>과테말라컴패션은 1976년부터 지금까지 계속되고 있습니다.</strong>
                    과테말라의 많은 사람들은 학교에 가지 않고 어린 나이에 가족의 생계를 돕기 위해 일터로 나갑니다. 
                    또 일찍 결혼하거나 조직폭력단에 들어가는 사람들도 있습니다. 과테말라 정부는 이같은 상황을 정리하기 위해 많은 노력을 하고 있지만 여전히 많은 학교에서 교육 재료나 책상, 의자가 없거나 어떤 지역에는 아예 학교가 없습니다. <br /><br />
                    선생님에 대한 낮은 급여는 선생님 수의 부족을 야기시킵니다. 과테말라컴패션은 어린이에게 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고 방과 후 수업을 진행하며 무료로 공공기관에서 예방접종을 받을 수 있도록 돕습니다. 
                    또 리더십 함양을 위한 단체 교육 및 미래 설계 교육을 진행합니다. 다양한 직업훈련과 학부모 교육을 제공합니다. 과테말라컴패션은 졸업생들의 정기 모임이 활발하게 운영되고 있습니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    영양실조 및 어린이들의 노동 착취로 인해 과테말라 어린이들은 고통받고 있습니다. 또한 어린이들에 대한 신체적.언어적 폭력이 심각한 상황입니다. 어린이들의 인권이 보호되고 그리스도안에서 건강한 정체성을 가질 수 있도록 기도해주세요.<br /><br />
                    가난한 삶 속에서 어린이들은 수입을 얻기 위해 약물 운반책으로 활동하는 경우가 빈번합니다. 어린이들이 약물의 위험으로부터 멀어질 수 있도록 기도해주세요.<br /><br />
                    대부분의 과테말라 어린이들에게는 충분한 교육의 기회가 주어지지 않습니다. 컴패션어린이센터를 통해 어린이들이 충분한 교육을 받을 수 있도록 기도해주세요.

                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="sponsor/children/?country=gu">과테말라컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 니카라과 NI -->
		<div class="wrap-board" style="display:none;" data-id="NI">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>니카라과(NI)</em></span>
                    니카라과 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/NI_니카라과.jpg" alt="" width="100%" />
                    <strong>니카라과컴패션은 2002년부터 지금까지 계속되고 있습니다.</strong>
                    1979년까지 라틴 아메리카에서 가장 낙후되어 있었던 니카라과는 교육 시스템을 개선하기 위해 심혈을 기울였습니다. 덕분에 많은 어린이들이 학교 교육을 받을 수 있게 되었지만, 
                    아직도 많은 사람들이 니카라과에서 교육을 받지 못하고 있습니다. 가난으로 많은 가정에서 자녀들을 학교에 보낼 수 없는 상황입니다. <br /><br />
                    니카라과컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고, 학교 교육에 필요한 기타 경비를 제공합니다. 
                    무료로 예방접종을 받게 하며 연령에 맞춰 다양한 직업훈련을 진행하고 정기적인 학부모 모임을 진행, 어린이 양육에 대해 교육합니다. 
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    영양실조와 질병으로 니카라과 어린이들이 나이에 맞게 성장하지 못하고 있습니다. 어린이들이 컴패션의 양육 아래에서 건강하게 자랄 수 있도록 기도해주세요.<br /><br />
                    니카라과에는 여전히 교육의 기회가 부족합니다. 컴패션의 도움으로 더 많은 어린이들에게 교육의 기회에 제공될 수 있도록 기도해주세요.<br /><br />
                    이혼 가정이 많아 어린이들이 위험한 환경에 노출되는 경우가 많습니다. 그리스도 안에서 온전한 가정의 모습의 회복되어 어린이들이 꿈을 품으로 자랄 수 있도록 기도해주세요.
                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=ni">니카라과컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 도미니카공화국 DR -->
		<div class="wrap-board" style="display:none;" data-id="DR">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>도미니카공화국(DR)</em></span>
                    도미니카공화국 어린이의 손을 잡아주세요!
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/DR_도미니카공화국.jpg" alt="" width="100%" />
                    <strong>도미니카공화국컴패션은 1970년부터 지금까지 계속되고 있습니다.</strong>
                    자녀교육에 대한 인식이 부족하거나 교육비가 높아 교육에 대한 의지가 있다 하더라도 경제적으로 가난한 부모들은 자녀를 학교에 보내는 것을 포기하고 있습니다. 
                    도미니카공화국컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고, 극심한 가난을 겪는 어린이들을 위해 영양적으로 균형 있는 식사를 제공하려는 노력을 합니다. 
                    실질적인 직업훈련을 제공하며 청소년들에게는 봉사활동도 적극 권장합니다. 
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    도미니카공화국에는 위생시설 부족으로 인해 질병을 겪는 어린이들이 많습니다. 또한, 기초 수입이 적기 때문에 어린이들의 영양실조가 만연해있으며, 이와 함께 어린이들을 교육해야 하는 것에 대한 개념이 부족합니다. 어린이들이 질병으로부터 안전하게 보호되고, 컴패션어린이센터를 통해 전인적인 양육을 온전히 받을 수 있도록 기도해주세요.<br /><br />
                    어린이 노동문제가 심각하며, 가정 폭력 문제가 극심합니다. 어린이들의 인권이 보호받을 수 있도록 기도해주세요.<br /><br />
                    허리케인으로 인한 자연재해 피해가 큽니다. 어린이와 가정이 안전할 수 있도록 기도해주세요.
                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=dr">도미니카공화국컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 르완다 RW -->
		<div class="wrap-board" style="display:none;" data-id="RW">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>르완다(RW)</em></span>
                    르완다 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/RW_르완다.jpg" alt="" width="100%" />
                    <strong>르완다컴패션은 1980년부터 지금까지 계속되고 있습니다.</strong>
                    르완다에서는 교육을 미래에 대한 희망으로 여깁니다. 초등교육은 무료로 제공되지만, 교육 환경이 열악합니다. 
                    부모들은 대부분 자녀들의 학교 수업에 필요한 교재, 교복 등을 감당하지 못하고 있는 실정입니다. 
                    르완다컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고, 균형 잡힌 식사와 예방접종, 건강검진, 의료 혜택을 제공하고 연령별 방과 후 수업을 진행합니다. <br /><br />
                    특히 신체적으로, 정서적으로 변화를 이해하도록 하는 청소년들을 위한 수업과 직업훈련이 있습니다. 몇몇 청소년들은 어린이센터의 모든 과정을 수료 후 협력교회의 직원으로 고용됩니다. 
                    정기적으로 학부모 모임을 진행하여 학부모님들은 열악한 환경의 집을 고쳐 주는 등 지역사회를 위한 활동에 참여합니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    농촌 지방에서는 식수를 구하기 위해 어린이들이 수 km를 걸어갑니다. 깨끗한 식수 공급이 원활하게 이루어지도록 기도해주세요.<br /><br />
                    어린이들이 안전한 집에서 생활할 수 있도록 기도해주세요.<br /><br />
                    도심에서는 비싼 거주비로 인해 이사가 잦습니다. 부모님들이 안정적인 일을 찾을 수 있도록 기도해주세요. <br /><br />
                    위생적이지 못한 하수도 시스템으로 인해 발생하는 말라리아, 설사 등의 이유로 많은 어린이들이 목숨을 잃어가고 있습니다. 어린이들이 질병으로부터 안전하고 사회의 기초 구조가 잘 세워지도록 기도해주세요.<br /><br />
                    도심의 슬럼에서 거주하는 우리 어린이들이 마약, 갱단, 매춘, 폭력성의 범죄들로부터 안전할 수 있도록 기도해주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=rw">르완다컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 멕시코 ME -->
		<div class="wrap-board" style="display:none;" data-id="ME">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>멕시코(ME)</em></span>
                    멕시코 어린이의 손을 잡아주세요!
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/ME_멕시코.jpg" alt="" width="100%" />
                    <strong>멕시코컴패션은 1979년부터 지금까지 계속되고 있습니다.</strong>
                    멕시코 정부는 학교 교육 과정을 개발하고 초등 교육을 무료로 바꾸어 의무화 시켰습니다. 
                    하지만 교육개혁에 어려움을 겪고 있는데다가 빈부차가 극심해 교육에 필요한 제반 비용을 감당하지 못하는 많은 가정의 어린이들이 학교에 가지 못하고 있습니다. <br /><br />
                    멕시코컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고 12~18세 어린이들을 대상으로 정부에서 인증한 리더십 학습 프로그램 운영합니다. 
                    또한 매년 영성캠프 활동, 현장학습 및 생일잔치를 진행합니다. 실질적인 직업훈련을 제공하며 정기적인 학부모 모임을 진행, 자녀양육 훈련을 진행합니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    멕시코에는 학교 부족으로 교육의 기회가 적습니다. 어린이들이 컴패션의 도움으로 온전한 교육을 받을 수 있도록 기도해주세요.<br /><br />
                    위생시설이 부족하여 어린이들이 각종 질병에 노출되어 있습니다. 멕시코 어린이들이 건강할 수 있도록 기도해주세요.<br /><br />
                    미혼모가 많으며 불완전하게 가정이 형성되는 경우가 많습니다. 또한 부모님이 일하러 가시는 동안 어린이들을 돌봐줄 수 있는 시설이 많이 부족한 상황입니다. 어린이들이 방치되지 않고 온전한 가정안에서 건강하게 자랄 수 있도록 기도해주세요.
                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=me">멕시코컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 방글라데시 BD -->
		<div class="wrap-board" style="display:none;" data-id="BD">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>방글라데시(BD)</em></span>
                    방글라데시 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/BD_방글라데시.jpg" alt="" width="100%" />
                    <strong>방글라데시컴패션은 2003년부터 지금까지 계속되고 있습니다.</strong>
                    방글라데시에서 교육에 대한 열정은 높습니다. 방글라데시 정부는 지속적으로 교육 시설을 늘려가고 있지만 아직도 학교의 숫자가 부족한 상태입니다. 
                    많은 학생들이 학교를 마치지 못하거나 영양실조로 고통받고 있습니다. 방글라데시컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고 균형 잡힌 영양식과 방과 후 수업을 제공합니다. <br /><br />
                    더불어 계절 과일 및 신선한 우유를 간식으로 먹습니다. 예방접종과 정기검진으로 건강을 지키며 특히 11세 이상 후원어린이들에게 리더십 개발, 협동심 향상, 다양한 직업교육과 기술 습득을 위한 프로그램을 제공합니다. 
                    그외 즐거운 특별활동과 캠프가 있으며 각 어린이센터는 한 달에 한 번씩 부모님 모임을 통해 위생, 청결, 교육의 중요성 등을 교육합니다. 또한 가정방문을 통해서 남•여 어린이의 모두에게 교육의 필요성을 강조합니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
					방글라데시는 매년 고온 다습한 몬순 기후로 인해 폭우, 홍수와 같은 자연재해로부터 피해를 많이 입습니다. 어린이들과 가족들이 무사할 수 있도록 기도해주세요.<br /><br />
                    많은 부모들이 어린이들을 학교 대신 농사일을 돕게 하거나, 일을 하게 합니다. 부모님들이 교육의 중요성을 깨닫고 어린이들을 학교와 어린이센터에 보낼 수 있도록 기도해주세요.<br /><br />
                    부모들이 안정적인 직업을 갖게 되어 어린이들을 충분히 양육할 수 있도록 기도해주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=bd">방글라데시컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 볼리비아 BO -->
		<div class="wrap-board" style="display:none;" data-id="BO">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>볼리비아(BO)</em></span>
                    볼리비아 어린이의 손을 잡아주세요!
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/BO_볼리비아.jpg" alt="" width="100%" />
                    <strong>볼리비아컴패션은 1975년부터 지금까지 계속되고 있습니다.</strong>
                    볼리비아 사람들은 어린이 교육을 매우 중요시합니다. 교육을 통해 그들의 소득을 높이고 삶의 질을 향상시킬 수 있다고 생각합니다. 
                    하지만 60%에 육박하는 사람들은 교육 수준과 상관없이 직업이 없습니다. 또 직업이 있는 사람 중 상당수가 교육 수준에 직종에 종사하거나 낮은 급여를 받고 있습니다. <br /><br />
                    볼리비아컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고 방과 후 양육을 통해 청소년 상담, 수련회, 지역 사회 봉사를 실시하며 컴퓨터, 제과 제빵, 의류 제작 등과 같은 직업 기술 교육을 진행하고 있습니다.
                    어린이는 물론 부모와 형제들을 위한 치과 진료 및 안과 진료를 제공하며, 매월 학부모 모임을 가지고 어린이 양육 세미나를 진행합니다. 
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    농촌 산간지역의 대중교통은 흔하지 않아 많은 어린이들이 안전하지 않은 환경에서 컴패션어린이센터 출석을 위해 장거리를 걸어옵니다. 어린이들의 안전을 위해 기도해주세요.<br /><br />
					어린이들이 조직폭력단으로부터 안전할 수 있도록 기도해주세요.<br /><br />
					더운 기온과 습한 환경은 심각한 피부 질환을 가져옵니다. 연약한 어린이들의 피부 질환이 속히 나을 수 있도록 기도해주세요. 
                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=bo">볼리비아컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 부르키나파소 BF -->
		<div class="wrap-board" style="display:none;" data-id="BF">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>부르키나파소(BF)</em></span>
                    부르키나파소 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/BF_부르키나파소.jpg" alt="" width="100%" />
                    <strong>부르키나파소컴패션은 2004년부터 지금까지 계속되고 있습니다.</strong>
                    개발도상국 중에서도 경제적 상황이 어려운 부르키나파소는 학비가 매우 비싸고 학교 수도 부족한 편입니다. 
                    지역에 따라, 몇몇 부족은 여자아이들이 교육을 받는 것을 금지하며 15세 미만의 여자 어린이들에게 조혼을 강요하기도 합니다. 
                    어린이 노동과 같은 문제도 존재합니다. 부르키나파소컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하며, 식사와 간식을 제공합니다. <br /><br />
                    또 더 극심한 가난에 처한 후원어린이 가정에는 쌀, 기장, 기름과 같은 음식들이 제공됩니다. 어린이들은 박물관, 국립공원 등으로 소풍이나 캠프를 가기도 하고 봉사활동에도 참여합니다. 
                    다양한 직업훈련을 받으며 학부모 모임에서는 청결 유지, 말라리아 방지, 영양섭취 및 경제적 자립을 위한 활동 등 교육을 실시하며 구충제, 말라리아 예방을 위한 모기장 등을 제공합니다
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    만연한 가난과 가뭄으로 인해 식량부족으로 고통받는 어린이들을 위해 기도해주세요.<br /><br />
                    말라리아로 소중한 생명을 잃어가는 어린이들이 하루속히 말라리아의 위협으로부터 자유로울 수 있도록 기도해주세요.<br /><br />
                    에이즈 감염률이 특히 높은 부르키나파소에 하나님의 치유의 손길이 임하시며 위생 교육을 통해 부모님들의 사고가 전환 되도록 기도해주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=bf">부르키나파소컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 브라질 BR -->
		<div class="wrap-board" style="display:none;" data-id="BR">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>브라질(BR)</em></span>
                    브라질 어린이의 손을 잡아주세요!
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/BR_브라질.jpg" alt="" width="100%" />
                    <strong>브라질컴패션은 1975년부터 지금까지 계속되고 있습니다.</strong>
                    브라질 정부는 어린이들을 위한 시설을 확충하였고 부모님들은 7~14세의 어린이들을 공립학교에 보낼 수 있게 되었습니다. <br /><br />
                    하지만 이 같은 발전에도 불구하고 브라질 교육 시스템은 아직 나아가야 할 길이 남아있습니다. 제대로 훈련 받지 못한 선생님들이 많아 어린이들은 읽기, 쓰기 및 수학 등 기초적인 것을 다 익히지 못한 채 학교를 떠나기도 합니다.<br /><br />
                    브라질컴패션은 등록된 어린이들이 학교에 갈 수 있도록 하는 것은 물론 추가교육을 제공하고 있습니다. 또한 미용, 손톱관리, 기계수리, 사무능력 등을 통해 자립할 수 있는 역량을 갖추게 하며 다양한 야외 활동과 캠프를 진행하고 가족들을 위한 도움을 제공합니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    브라질에서는 높은 실업률로 인해 부모님들이 자녀를
                    제대로 돌보지 못해 많은 어린이들이 방치되고 있습니다.
                    브라질에 더 많은 일자리가 창출되어 어린이들이
                    가정에서 올바른 양육을 받을 수 있도록 기도해주세요.<br /><br />
                    
                    폭력, 학대, 마약거래의 위험에 어린이들이 노출되어
                    있습니다. 후원어린이들이 컴패션의 양육 아래 건강한
                    가치관을 가지고 안전하고 건강하게 자랄 수 있도록
                    기도해주세요.
                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=br">브라질컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 스리랑카 LK -->
		<div class="wrap-board" style="display:none;" data-id="LK">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>스리랑카(LK)</em></span>
                    스리랑카 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/LK_스리랑카.jpg" alt="" width="100%" />
                    <strong>스리랑카컴패션은 2010년부터 지금까지 계속되고 있습니다.</strong>
                    스리랑카는 항상 교육의 중요성을 강조하고 있습니다. 1948년 독립한 이후, 정부는 교육을 주요 사업의 우선순위 중 하나로 꼽고 있습니다. 
                    그 결과 어린이들의 90% 이상 초등 교육을 받았고, 그들 중 많은 어린이들이 중등 교육을 받고 있습니다. 
                    하지만 아동학대와 청소년 성매매 등의 문제가 여전히 존재하며 가난한 가정은 학교 대신 일자리로 자녀들을 내보내야 합니다. <br /><br />
                    스리랑카컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하며 점심식사와 간식으로 제공합니다. 
                    지역 의료기간과 연계하여 어린이들에게 필수 예방접종을 실시하고, 어린이 캠프, 체육 활동, 야외활동, 학예회 등을 진행합니다. 
                    정기적인 학부모 모임을 통해 어린이센터 활동을 안내하고, 선생님들과 유대감을 형성하여 어린이들이 가정과 센터에서 잘 양육될 수 있도록 돕습니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
					스리랑카는 청소년 성매매가 빈번합니다. 후원 어린이들이 그리스도안에서 올바른 성의 의미를 깨닫고, 성적 위협으로부터 안전할 수 있도록 기도해주세요.<br /><br />
                    부모들이 안정적인 수입을 얻는 일자리를 구해서, 어린이들이 이른 나이에 돈을 벌지 않고 학업에 온전히 집중할 수 있도록 기도해주세요.<br /><br />
                    문화적/정서적 차이로 인해 여전히 컴패션 어린이센터에서 양육을 받기 힘들어하는 어린이들과 부모님들이 있습니다. 어린이들이 컴패션의 양육시스템에 잘 적응하고 복음을 받아들일 수 있도록 기도해주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=lk">스리랑카컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 아이티 HA -->
		<div class="wrap-board" style="display:none;" data-id="HA">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>아이티(HA)</em></span>
                    아이티 어린이의 손을 잡아주세요!
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/HA_아이티.jpg" alt="" width="100%" />
                    <strong>아이티컴패션은 1968년부터 지금까지 계속되고 있습니다.</strong>
                    아이티의 초등교육은 의무이지만 학비를 내야 합니다. 아이티에서의 교육은 삶의 성공과 지위를 얻고 더 나은 직업을 얻을 수 있는 길이지만, 어린이들은 대부분 집안의 경제적인 어려움 때문에 다음 과정으로 진학하지 않습니다. 
                    아이티는 불어, 크리올어 등 두 개의 공용어가 있는데, 교육을 받지 않은 사람들은 크리올어를 사용하고 교육을 받은 사람들은 두 언어를 모두 사용합니다. 하지만 불어를 사용해야 더 성공할 확률이 높습니다. <br /><br />
                    아이티컴패션은 학교교육을 제공하는 것은 물론 각종 직업훈련과 리더십훈련을 진행합니다. 정기적인 학부모 교육을 통해 어린이양육 세미나를 진행하고 있습니다. 
                    아이티컴패션 졸업생들은 의료봉사, 재난 시 구호 등 활발한 활동으로 자신의 어린 시절과 같이 어려운 어린이들을 돕는 데 앞장서고 있습니다. 
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    아이티는 기본적인 사회 시설이 남미의 타 국가들에 비해 현저히 부족합니다. 이로 인해 교육의 기회도 넉넉하지 않습니다. 더 많은 어린이들이 컴패션을 통해 온전한 양육을 받을 수 있도록 기도해주세요.<br /><br />
					아이티에는 대중교통수단, 깨끗한 물, 위생시설 등이 부족합니다. 또한 의료시설과 환자를 돌볼 의사가 턱없이 부족합니다. 모든 것이 넉넉치않은 아이티에서 어린이들이 질병으로 인해 고통 받지 않고 건강하게 자랄 수 있도록 기도해주세요.
                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=ha">아이티컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 엘살바도르 ES -->
		<div class="wrap-board" style="display:none;" data-id="ES">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>엘살바도르(ES)</em></span>
                    엘살바도르 어린이의 손을 잡아주세요!
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/ES_엘살바도르.jpg" alt="" width="100%" />
                    <strong>엘살바도르컴패션은 1977년부터 지금까지 계속되고 있습니다.</strong>
                    엘살바도르는 계속해서 교육의 질을 향상시켜 왔지만 아직도 많은 수의 어린이들이 가족의 생계를 돕기 위해 진학을 하지 못하고 일터에 나가게 됩니다. 
                    이들은 보통 커피, 설탕 농장, 가사, 노점상 등의 일을 합니다. 이 같은 현실은 경제적인 상태가 더 좋지 않은 시외로 갈수록 심각해집니다. <br /><br />
                    엘살바도르의 많은 가정들은 하루에 1달러 미만으로 살아가고 음식값을 감당하기조차 어려운 실정입니다. 이 지역의 어린이들은 보통 6-7살 때부터 일을 하기 시작합니다. 
                    엘살바도르컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고 예방접종과 식사와 간식, 직업훈련 등을 제공합니다. 
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    열대기후와 건조기후를 오가는 극명한 날씨 차이로 인해 농작물 생산에 어려움이 많습니다. 이를 위해 기도해주세요. <br /><br />
                    엘살바도르는 국가적으로 교육의 기회가 적습니다. 또한 일자리가 부족한 상황입니다. 어린이의 부모님들이 안정적인 수입을 얻을 수 있고, 어린이들이 온전한 교육을 받을 수 있도록 기도해주세요.<br /><br />
                    조직폭력단의 위협이 심각합니다. 어린이들이 조직폭력단 가입 및 조직폭력단의 위협으로부터 안전할 수 있도록 기도해주세요.
                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=es">엘살바도르컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 에콰도르 EC -->
		<div class="wrap-board" style="display:none;" data-id="EC">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>에콰도르(EC)</em></span>
                    에콰도르 어린이의 손을 잡아주세요!
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/EC_에콰도르.jpg" alt="" width="100%" />
                    <strong>에콰도르컴패션은 1974년부터 지금까지 계속되고 있습니다.</strong>
                    에콰도르의 중요 도시에는 많은 대학교가 있지만 대학교육을 받는 인구는 극소수에 불과합니다.
                    또한 경제적으로 가난하거나 도시에서 멀리 떨어진 곳에 살고 있는 어린이들에게는 공교육의 혜택이 미치지 않는 경우가 많습니다.
                    에콰도르컴패션은 등록된 어린이들이 학교에 다닐 수 있도록 하고, 풍부한 식사와 간식을 제공합니다. <br /><br />
                    또한 세분화된 주제에 맞춰 직업훈련 프로그램을 운영하고, 연령에 맞춘 눈높이 직업훈련을 통해 어린이들의 자립을 돕습니다. 
                    지역에 따라 방과 후 교실 개념의 수업 시행하며 축구, 미술, 음악 교실, 캠프 등을 진행합니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    에콰도르에는 부모님으로부터 충분한 보살핌을 받지 못해 영양실조, 조기 임신, 학습장애 등의 문제가 빈번히 발생하고 있습니다. 어린이들이 가정에서 올바르게 양육 받고 컴패션의 전인적인 가르침으로 인해 온전한 성인으로 자랄 수 있도록 기도해주세요.<br /><br />
					에콰도르는 위생시설 부족으로 인한 만성질병에 시달리고 있습니다. 어린이들이 질병에 걸리지 않고 건강하게 자랄 수 있도록 기도해주세요.


                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=ec">에콰도르컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 에티오피아 ET -->
		<div class="wrap-board" style="display:none;" data-id="ET">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>에티오피아(ET)</em></span>
                    에티오피아 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/ET_에티오피아.jpg" alt="" width="100%" />
                    <strong>에티오피아컴패션은 1993년부터 지금까지 계속되고 있습니다.</strong>
                    초등학교부터 대학교까지 무상 교육을 제공하지만 학업 중단이 많고 여자아이들은 여전히 교육받는 기회를 갖지 못하는 경우가 많습니다. 
                    에티오피아컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하는 한편, 빵과 과자, 차를 제공합니다. 
                    가족들에게 식량이 제공되며 영양이 특별히 더 필요한 어린이들에게는 의사의 처방에 따른 음식이 제공됩니다. <br /><br />
                    주식이 제공 되지 않는 이유는 정부에서 식사를 어린이들에게 제공하는 것을 지양하였기 때문입니다. 
                    이외에도 다양한 스포츠 활동, 나무 심기와 같은 환경 돌보기 등 다양한 활동이 있으며, 정기적인 학부모 모임을 통해 부모님들은 자녀 양육법과 수입 창출을 위한 교육을 받습니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    위험한 지역에 거주하는 어린이들을 하나님께서 보호하시고 안전하게 지키시기를 기도해주세요.<br /><br />
                    어린이들이 자존감 향상을 통해 꿈을 이룰 수 있다는 생각을 가질 수 있도록 기도해주세요.<br /><br />
                    부모님들이 안정적이고 고정적인 수입을 창출할 수 있는 직장을 가질 수 있도록 기도해주세요.<br /><br />
                    말라리아로 소중한 생명을 잃어가는 어린이들이 하루속히 말라리아의 위협으로부터 자유로울 수 있도록 기도해주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=et">에티오피아컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 온두라스 HO -->
		<div class="wrap-board" style="display:none;" data-id="HO">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>온두라스(HO)</em></span>
                    온두라스 어린이의 손을 잡아주세요!
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/HO_온두라스.jpg" alt="" width="100%" />
                    <strong>온두라스컴패션은 1974년부터 지금까지 계속되고 있습니다.</strong>
                    생계를 위해 학교를 그만두는 학생들의 비율이 높습니다. 온두라스컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고 특별히 영양이 많이 부족한 온두라스 어린이들을 위해 영양학 전문가들이 마련한 균형 잡힌 식단으로 점심식사를 제공합니다. <br /><br />
                    무료로 예방접종을 받을 수 있도록 하고 체육활동을 활발히 진행합니다. 
                    식수 사용, 쓰레기 문제, 뎅기열 등 환경적인 부분에 대한 교육과 직업훈련, 정기적인 학부모 교육이 진행되며 각종 현장학습도 있습니다. 온두라스컴패션 졸업생들은 활발히 지역사회 공헌, 가난한 어린이들 돕기를 합니다. 
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    온두라스의 시골은 가난의 정도가 극심하여 일자리를 구하는 데 많은 어려움이 있습니다. 어린이들의 부모님이 안정적인 수입을 통해 자녀들을 온전히 양육할 수 있도록 기도해주세요.<br /><br />
                    어린이들에 대한 조직폭력단 가입 유혹이 날로 증가하고 있습니다. 어린이들이 그리스도 안에서 바른 가치관을 가지고 심신이 건강한 성인으로 자랄 수 있도록 기도해주세요.<br /><br />
                    온두라스 어린이들은 위생상태와 영양실조가 심각한 수준입니다. 어린이들이 건강하게 자랄 수 있도록 기도해주세요.
                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=ho">온두라스컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 우간다 UG -->
		<div class="wrap-board" style="display:none;" data-id="UG">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>우간다(UG)</em></span>
                    우간다 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/UG_우간다.jpg" alt="" width="100%" />
                    <strong>우간다컴패션은 1980년부터 지금까지 계속되고 있습니다.</strong>
                    우간다에서 교육은 성공으로 가는 중요한 디딤돌입니다. 부모님들은 대부분 자녀를 학교에 보내기 원하며 많은 대학들이 세워지고 졸업생 숫자도 증가하고 있습니다. 
                    하지만 여전히 빈부차가 심하고 빈민가가 존재하며 에이즈로 인한 피해가 존재합니다. 우간다컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고, 점심식사와 간식을 줍니다. <br /><br />
                    영양이 매우 부족한 어린이들은 3~6개월의 영양치료를 받게 됩니다. 영양치료 시 사용되는 재료는 가정에 공개되어 부모님들의 식사 준비를 위한 교육에 활용됩니다. 
                    또한 후원어린이들은 다양한 교외활동과 직업 기술 교육을 받습니다. 학부모 모임을 통해 문맹률 개선을 위한 언어 수업이 매달 진행되며 위생, 올바른 양육, 수입 창출을 위한 수업이 있습니다. 
                    어린이센터 교과 과정을 평가하기 위한 미팅도 매월 진행됩니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
					중부지방에서는 부모님들이 돈을 벌기 위해 오랜 시간 집을 비우거나, 혹은 가정 내 폭력으로 인해 어린이들이 올바른 양육을 받지 못하고 있습니다. 가정의 평화와 어린이들이 가정에서 사랑받고 존중받을 수 있도록 기도해주세요. <br /><br />
                    우간다의 동부 지방에서는 여전히 여성 할례가 이루어지고 있습니다. 여자 어린이들이 이로부터 보호될 수 있도록 기도해주세요.<br /><br />
                    북부 지방의 주민들은 ‘신의 저항군(Lord’s Resistance Army)’의 테러로 오랫동안 고통받았습니다. 이 지역의 빠른 회복을 위해 기도해주세요. <br /><br />
                    많은 어린이들이 에이즈 관련 질병으로 부모님을 잃었습니다. 어린이들의 안전과 올바른 성장을 위해 기도해주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=ug">우간다컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 인도 IN -->
		<div class="wrap-board" style="display:none;" data-id="IN">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>인도(IN/EI)</em></span>
                    인도 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/IN_인도_EI_동인도.jpg" alt="" width="100%" />
                    <strong>인도컴패션은 1968년부터 지금까지 계속되고 있습니다.</strong>
                    고등학교까지 공교육이 이루어지지만 많은 학생들이 여전히 혜택을 받지 못하고 특히 여성들이 교육받기란 더욱 어렵습니다. 
                    여성은 보통 가족 내에서 동생들을 돌보거나 가정 일을 하는 등의 책임을 져야 합니다. 또 공립학교 학비는 무료지만 책, 교복, 교통비 등 추가적으로 드는 비용은 가난한 가정에게 큰 짐이 됩니다. 
                    인도컴패션은 등록된 어린이들이 학교에 다닐 수 있도록 하고 제반 비용과 균형 잡힌 영양식을 제공하며, 영양실조에 걸린 어린이들의 경우, 특별식을 추가 제공합니다. <br /><br />
                    후원어린이들은 건강한 자아인식, 대인관계 기술, 영어 말하기 등의 프로그램에 참여하고 악기, 컴퓨터, 무술 등을 배웁니다. 
                    또한 청소년 캠프, 봉사활동, 어르신 방문 등을 통해 사회•정서적인 활동을 하며, 실질적인 직업교육을 제공합니다. 
                    매월 학부모 모임을 통해 자녀양육훈련, 사회문제, 교육의 중요성에 대한 주제를 놓고 교육을 합니다. 이 시간을 통해 학부모들은 후원 어린이들을 올바르게 양육하고 교육할 수 있는 방법을 배웁니다. 
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
					인도는 유아 사망, 영양실조, 아동학대가 많은 나라 중 하나입니다. 어린이들이 안전할 수 있도록 기도해주세요.<br /><br />
                    홍수, 산사태 등 자연재해로 인해 어린이들이 거주지를 잃는 등 많은 피해가 나타나고 있습니다. 하나님께서 어린이들을 보호해주시도록 기도해주세요.<br /><br />
                    부모들이 교육의 중요성을 크게 인식하여 후원어린이들의 학업을 전폭적으로 지지해줄 수 있도록 기도해주세요. 특히 여자 어린이들에게 교육받을 수 있는 기회가 열릴 수 있도록 기도해 주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=in">인도컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 인도네시아 IO -->
		<div class="wrap-board" style="display:none;" data-id="IO">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>인도네시아(IO/ID)</em></span>
                    인도네시아 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/IO_인도네시아_ID_동인도네시아.jpg" alt="" width="100%" />
                    <strong>인도네시아컴패션은 1968년부터 지금까지 계속되고 있습니다.</strong>
                    인도네시아의 교육 시스템은 다양한 종교, 문화, 가난 속에서도 빠르게 증가하는 인구와 성장하는 나라의 모습을 반영하여 초등학교를 짓는 데 많은 투자가 있었습니다. 
                    하지만 여전히 자격 있는 선생님을 구하고, 급여를 주는 문제에 있어서 어려움이 있습니다. 특히 섬 지방에 있는 학교에 교과서와 수업 재료를 공급하는 것 또한 큰 문제입니다. <br /><br />
                    또한 무슬림 중에서는 공립학교에 보내지 않고 자신들만의 학교에 보내기를 원하는 가정들이 있습니다. 인도네시아컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고, 고단백의 점심식사를 제공합니다. 
                    어린이들은 체육활동과 캠프에 참가하며, 12세 이상의 어린이들은 전통춤, 미술, 노래, 악기 연주 등의 문화활동을 합니다. 
                    청소년들은 직업활동에 필요한 기술들을 배우고, 매월 학부모 모임을 통해 자녀양육에 관한 교육을 진행합니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
					에이즈, 말라리아 등의 질병으로부터 어린이들이 안전할 수 있도록 기도해주세요.<br /><br />
                    부모들이 안정적인 수입을 얻어 어린이들을 잘 양육할 수 있도록 기도해주세요.<br /><br />
                    인도네시아컴패션은 자국 내 많은 섬들에서 학교 교육 시설과 교과서가 부족하여 수업이 어려운 것을 알고 있으며 이들 어린이들에게도 컴패션 양육프로그램을 전하기 위해 노력하고 있습니다. 이들에게 하나님의 사랑이 전해질 수 있도록 기도해주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=io">인도네시아컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 케냐 KE -->
		<div class="wrap-board" style="display:none;" data-id="KE">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>케냐(KE)</em></span>
                    케냐 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/KE_케냐.jpg" alt="" width="100%" />
                    <strong>케냐컴패션은 1980년부터 지금까지 계속되고 있습니다.</strong>
                    케냐는 자연재해는 없는 편이나 지역에 따라 건기에 가뭄으로 인한 심각한 물 부족 현상이 나타납니다. 
                    케냐 사람들은 개인과 사회의 성공을 위한 열쇠로 교육에 커다란 가치를 두고 있습니다. 
                    하지만 교육비가 비싸 케냐 대부분의 가정에 큰 부담으로 작용합니다. <br /><br />
                    케냐컴패션 어린이센터에서는 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고 청소년을 위한 바틱 만들기, 구슬 공예, 비누 제작, 바구니 제작 등의 교육 및 효율적인 
                    농업을 위해 가뭄에 강한 농작물 교육과 비닐하우스 재배, 염소와 토끼 같은 가축 사육 교육을 통한 수입 창출 교육을 진행합니다. 
                    학부모 모임에서 학부모들은 올바른 어린이 양육 훈련과 수입창출 교육을 받습니다. 
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    농촌에 거주하는 어린이들이 홍수나 가뭄과 같은 척박한 환경에서도 학교에 잘 다니며 건강하게 성장할 수 있도록 기도해 주세요. <br /><br />
                    부족 간의 갈등 지역에 거주하는 어린이들이 안전하게 생활하도록 기도해주세요.<br /><br />
                    농촌 지역에 거주하는 절반 이상의 어린이들이 건강하게 성장하기 위한 필수 요소 두 가지인 깨끗한 물과 위생환경에서 자라지 못하고 있습니다. 우리 어린이들이 깨끗한 환경과 물에서 자랄 수 있도록 기도해 주세요.<br /><br />
                    도심의 슬럼에 거주하는 어린이들이 위험하고 위협적인 환경에서부터 자유로울 수 있도록 기도해 주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=ke">케냐컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>

		<!-- 콜롬비아 CO -->
		<div class="wrap-board" style="display:none;" data-id="CO">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>콜롬비아(CO)</em></span>
                    콜롬비아 어린이의 손을 잡아주세요!
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/CO_콜롬비아.jpg" alt="" width="100%" />
                    <strong>콜롬비아컴패션은 1970년부터 지금까지 계속되고 있습니다.</strong>
                    콜롬비아는 마약, 어린이 가정폭력, 학대 등의 문제가 있습니다. 또 주요 도시에는 좋은 대학교들이 있지만 대부분 사립 대학교로 교육비가 비싸고 시골의 초등학교에서의 낙제 비율이 상당히 높습니다.<br /><br />
                    이것은 경제적 가난과 교육 인프라 부족으로 인한 결과로 보입니다. 콜롬비아컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 해주며 특별활동과 수련회를 진행하고 있습니다.
                    격월로 학부모 모임을 진행하여 자녀양육, 어린이보호에 대한 교육을 진행하고 있습니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    콜롬비아 어린이들이 마약의 유혹을 이겨내고 그리스도 안에서 건강한 가치관을 형성하며 성장할 수 있도록 기도해주세요.<br /><br />
                    많은 어린이들이 가정폭력과 성적 학대로 인해 고통을 겪고 있습니다. 하나님께서 어린이들을 보호해주시도록 기도해주세요.<br /><br />
                    많은 부모님들이 안정적인 수입을 얻어 어린이들에게 교육의 기회를 보장해줄 수 있도록 기도해주세요.

                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=co">콜롬비아컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 탄자니아 TZ -->
		<div class="wrap-board" style="display:none;" data-id="TZ">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>탄자니아(TZ)</em></span>
                    탄자니아 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/TZ_탄자니아.jpg" alt="" width="100%" />
                    <strong>탄자니아컴패션은 1999년부터 지금까지 계속되고 있습니다.</strong>
                    탄자니아에서 교육은 사람들에게 기술, 자신감, 능력을 가져다주는 것으로 이해됩니다. 
                    하지만 탄자니아 정부가 무상으로 교육하는 공립학교는 시설이 열악하고 선생님 숫자도 적습니다. 
                    무상교육이라고는 하나, 양극화 현상이 심해 학교에 갈 수 없는 어린이들이 많이 있습니다. 
                    탄자니아컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고 식사를 제공합니다. <br /><br />
                    아침과 점심 식단을 따로 운영하며 특별히 집에서 단백질을 섭취할 수 없는 어린이들을 위해 단백질은 매 끼니 제공됩니다. 
                    성가대 연습과 체육 시간이 운영되고 전통문화를 배웁니다. 또한 다양한 야외활동과 자원봉사 활동을 진행합니다. 
                    급격한 신체 변화, 감정 변화에 적응할 수 있도록 청소년 교육을 실시하고, 직업 교육을 제공합니다. 
                    학부모 모임을 통해서 부모님들은 음식 준비를 도와주며 어린이센터의 기초를 닦는 작업에 동참합니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    가난한 가정의 보호자들은 대부분 소작농 농부입니다. 농작의 현대화와 효율적인 기술 습득을 통한 추수의 기쁨을 위해 기도해주세요. <br /><br />
                    대부분의 부모님들은 교육을 받지 못했기 때문에 부모님들이 자녀들의 교육에 더욱 관심을 가질 수 있도록 기도해주세요. <br /><br />
                    가난으로 인해 이른 임신을 하는 여자 어린이들이 교육을 받고 학업을 지속하여 가난의 고리를 끊어낼 수 있도록 기도해주세요.<br /><br />
                    매해 6만 명이 말라리아로 죽어갑니다. 또한 에이즈 관련 질병으로 고통받는 사람이 전체 인구의 9%에 달한다고 합니다. 어린이들이 각종 질병으로부터 안전하도록 기도해주세요.<br /><br />
                    도심 지역에서는 물가가 너무 급격히 상승되어 식재료가 몇 년 사이에 2배가 되었다고 합니다. 물가의 안정을 위해 기도해주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=tz">탄자니아컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 태국 TH -->
		<div class="wrap-board" style="display:none;" data-id="TH">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>태국(TH)</em></span>
                    태국 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/TH_태국.jpg" alt="" width="100%" />
                    <strong>태국컴패션은 1970년부터 지금까지 계속되고 있습니다.</strong>
                    태국 정부는 모든 사람이 교육을 받을 수 있도록 교육의 질을 높이고 국제적인 수준으로 높이는 데 총력을 기울여 왔습니다. 
                    그 결과, 14세 이하의 어린이에게까지 무료로 교육이 이루어지며 선생님을 양성하는 등 많은 변화가 일어났습니다. 
                    하지만 여전히 학교보다는 일터로 가야 하는 어린이들이 있으며 학대와 차별이 존재합니다. 
                    태국컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하며, 균형 잡힌 점심식사를 제공합니다. <br /><br />
                    영양실조에 걸린 어린이들에게는 맞춤형 식단을 마련해주고, 고기를 구입하지 못하는 대부분 가정의 상황에 따라 단백질 섭취를 위해 고기를 제공합니다. 
                    태국컴패션 어린이센터에서는 정부에서 무상으로 제공하는 예방접종 외에 추가 예방접종을 실시합니다. 후원어린이들은 직업교육을 받고 매년 다양한 캠프에 참가합니다. 
                    졸업생들은 어린이센터에서 일을 하거나, 어린이센터에서 제공하는 직업정보를 제공받게 됩니다. 
                    매년 학부모 모임을 통해 부모님들은 어린이들의 센터활동을 대한 소식을 듣고, 각 어린이들의 재능을 가정에서 어떻게 개발할 수 있는지를 배웁니다. 
                    또한 직원들은 가정방문을 통해 면담을 진행하고 어린이와의 유대감을 쌓는 데 도움을 줍니다. 어린이센터는 학부모들에게 직업훈련을 시키고 이를 통해 가정의 추가수입을 얻을 수 있도록 돕습니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
					극심한 가난과 노동, 학대 속에서 사는 태국 어린이들이 그리스도 안에서 꿈을 잃지 않고 소망을 품으로 살아갈 수 있도록 기도해주세요.<br /><br />
                    태국에는 국경지대 및 산간지방 부족 마을에서 사는 고립된 어린이들이 많습니다. 이들이 안전할 수 있도록 기도해주세요.<br /><br />
                    태국 어린이들이 자라날수록 더욱더 하나님의 사랑을 깨달을 수 있도록 기도해주세요.


                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=th">태국컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 토고 TG -->
		<div class="wrap-board" style="display:none;" data-id="TG">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>토고(TG)</em></span>
                    토고 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/TG_토고.jpg" alt="" width="100%" />
                    <strong>토고컴패션은 2008년부터 지금까지 계속되고 있습니다.</strong>
                    2008년 초등교육이 의무화되었으나 많은 학부모들은 여전히 자녀들의 학비를 감당하기 힘들어합니다. 또 학교 시설은 낙후되었고 대부분 전기가 없습니다. 
                    토고컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고 식사를 제공합니다. 또한 어린이들이 학업에 열심히 임할 수 있도록 학습 돌봄이 시스템을 운영하고 있습니다. 
                    학습 돌봄이 시스템의 혜택을 받은 어린이들은 85% 이상의 높은 성취 율을 이루었습니다. 또한 질병 예방을 위해 위생 교육을 실시하고 깨끗한 식수 공급을 위해 집에서 사용 가능한 정수기를 제공합니다. <br /><br />
                    청소년 프로그램을 통해 급격한 신체 변화, 감정 변화에 적응할 수 있도록 교육을 실시하고, 스스로 수입을 창출할 수 있도록 직업 교육을 실시합니다. 
                    양육보완프로그램 기금으로 학부모들에게 수입 창출을 위한 교육을 제공합니다. 또한 학부모님들은 어린이센터의 음식 준비를 도와주며 어린이센터를 돌보는 작업에 동참하고 있습니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
					깨끗한 식수 부족과 열악한 위생 환경으로 인해 많은 어린이들이 영양실조와 말라리아에 노출되어 있습니다. 위생환경의 개선과 원활한 식수 공급을 위해 기도해주세요. <br /><br />
                    도심 지역에 거주하는 어린이들의 높은 혼전 임신율이 바른 교육, 가정에서의 사랑을 통해 낮아지도록 기도해주세요.<br /><br />
                    농촌 지역에서는 아동 인신매매가 만연합니다. 어린이들의 안전을 위해 기도해주세요.

                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=tg">토고컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 페루 PE -->
		<div class="wrap-board" style="display:none;" data-id="PE">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>페루(PE)</em></span>
                    페루 어린이의 손을 잡아주세요!
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/PE_페루.jpg" alt="" width="100%" />
                    <strong>페루컴패션은 1985년부터 지금까지 계속되고 있습니다.</strong>
                    페루정부는 어려운 사람들을 위해 공립 학교를 세우고 있습니다. 하지만 많은 가정이 교통비, 교재비, 교복 등의 비용을 감당하지 못해 여전히 학교에 가지 못하는 어린이들이 많이 있습니다.
                    아마존 정글 지역과 안데스의 고지대에는 학교가 아주 적게 있고 선생님의 숫자도 부족합니다. 부모가 농부인 경우, 자녀들을 학교에 보내지 않고 농장 일을 시키거나 집안일을 시키기도 합니다. <br /><br />
                    페루컴패션은 등록된 모든 어린이들이 학교에 다닐 수 있도록 하고, 정기 건강검진과 필수 예방접종을 제공합니다. 적절한 식사를 제공하고 청소년들에게는 지역 상황에 맞춰 직업훈련 프로그램 운영합니다. 
                    또한 정기적인 학부모 모임을 통해 자녀 양육 훈련을 진행합니다. 
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
                    페루는 의료시설, 위생시설 부족으로 인한 만성질병 및 뎅기열, 벌레로부터 오는 질병 등이 만연합니다. 또한 제대로 된 주거시설을 가지지 못한 가정의 어린이들이 많습니다. 어린이들이 각종 질병으로부터 안전하고 깨끗한 주거시설에서 살 수 있도록 기도해주세요.<br /><br />
					일자리를 얻기 위해 도시로 왔지만 일자리를 얻기 힘들고 주거문제로 어려움을 겪으며 슬럼가에서 거주하는 경우 많습니다. 이로 인해 갱단, 마약의 위험에 노출되어 있습니다. 페루 어린이들이 건강하게 생활할 수 있도록 기도해주세요.
                    
                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=pe">페루컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>
		
		<!-- 필리핀 PH -->
		<div class="wrap-board" style="display:none;" data-id="PH">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                	<span class="txt-box"><em>필리핀(PH)</em></span>
                    필리핀 어린이의 손을 잡아주세요! 
				</div>
                
                <div class="editor-html">
					<img src="/common/img/page/about/nation_img/PH_필리핀.jpg" alt="" width="100%" />
                    <strong>필리핀컴패션은 1972년부터 지금까지 계속되고 있습니다.</strong>
                    보편적으로 필리핀의 가정은 대부분 교육을 중요시하며 때로 학비를 마련하기 위해 땅과 소유물을 팔기도 합니다. 
                    정부는 초등학교와 중학교까지의 교육 과정을 무료로 제공하고 있습니다. 하지만 공립 학교에서는 교실, 선생님, 교재가 부족해서 좋은 질의 수업을 받기가 어렵습니다. 
                    필리핀컴패션은 등록된 모든 어린이들을 학교에 다닐 수 있도록 하며 균형 잡힌 식사와 간식을 제공합니다. 
                    기본적인 예방접종은 정부에서 무상 제공되며, 어린이센터 근처에 있는 의료센터에서 그 외의 추가 예방접종을 받습니다. <br /><br />
                    다양한 사회·정서적 활동과 야외활동 및 학예회가 개최되며, 고학년이 되면, 한 달에 두 번씩 특별활동에 참여합니다. 
                    필리핀컴패션은 연령에 맞춘 눈높이 직업훈련 프로그램 운영하고, 학부모 모임을 통해 성경공부, 자녀에게 편지쓰기 등을 활동을 하며 어린이들의 어린이센터 활동을 지원할 수 있도록 독려합니다. 
                    필리핀컴패션 졸업생들은 활발한 활동을 통해, 자신과 같은 어린 시절을 보낸, 어린이들을 돕고 지원하고자 노력합니다.
                </div>
                <div class="box-pray mt30">
        			<p class="txt-slogon">기도해주세요</p>
					아직도 많은 필리핀 어린이들이 어린 나이에 일을 시작합니다. 부모님들과 어린이들이 교육의 중요성을 깨닫고 학교와 어린이센터 수업에 집중하며 꿈을 키워나갈 수 있도록 기도해주세요.<br /><br />
                    필리핀의 많은 슬럼지역에서 어린이들은 마약과 조직폭력단 가입의 유혹을 받습니다. 이런 유혹으로부터 어린이들이 안전할 수 있도록 기도해주세요.<br /><br />
                    극심한 빈부격차로 인해 특히 도시 근교의 빈민가 어린이들에게 정서적으로 힘든 일들이 많이 있습니다. 어린이들이 자신의 존귀함을 깨닫고 사랑받는 자로서 일어날 수 있도록 기도해 주세요.


                    <div class="wrap-bt"><a class="bt-type1" style="width:100%" href="/sponsor/children/?country=ph">필리핀컴패션 어린이<br /> 1:1어린이양육 신청하기</a></div>
		        </div>
            </div>
            
        </div>


        <div class="wrap-bt mb30"><a style="width:100%" class="bt-type5" href="/about-us/world-map/">목록</a></div>
        
    </div>
    
<!--//-->
</div>






</asp:Content>
