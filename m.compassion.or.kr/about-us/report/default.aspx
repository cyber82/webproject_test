﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="about_us_notice_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/about-us/report/default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl" id="l">
<!---->
    
    <div class="wrap-tab3 sectionsub-margin1">
        <a style="width:50%" class="selected" href="/about-us/report/">사업성과보고서</a>
        <a style="width:50%" href="/about-us/revenue/">재무/감사보고</a>
    </div>

    <div class="about-report">
    	
        <p class="txt-caption mt30">“전인적 양육을 위한 한국컴패션의 활동”</p>
		한국컴패션은 재정의 건실함과 투명성을 유지하기 위해 매년 KPMG(삼정회계법인)을 통한 외부 감사를 받고 있으며 국제컴패션은 후원자님의 후원금이 각 수혜 지역에서 적절하게 사용되고 있는지를 확실하게 검증하기 위해 양육 사업 국가별로 외부감사와 내부감사를 철저히 실시하고 있습니다.

        <ul class="report-business mt30">
        	<li ng-repeat="item in list">
				<span class="report-thumb" style="background:url('{{item.b_thumb}}') no-repeat center top;"></span>{{item.b_title}}
            	<a class="bt-type6 mt10" ng-click="goView(item.b_id)"><span class="ic1"></span>보고서 보기</a>
                <a class="bt-type5 mt5" href="{{item.b_file}}"><span class="ic2"></span>PDF 다운로드</a>
            </li>
        </ul>
       
		<div class="mb30"> 
			<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging> 
		</div>

    </div>
    
<!--//-->
</div>

			
    
</asp:Content>