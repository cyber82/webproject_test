﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="about_us_notice_view" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub">
<!---->
    
	<div class="about-report">
	
		<div class="wrap-board">

        	<div class="view-board">
            	<div class="txt-title sectionsub-margin1">
                    <asp:Literal runat="server" ID="b_title" />
				</div>
                
                <div class="editor-html">
					<!---->
                    
                        <asp:Literal runat="server" ID="b_content" />
                    
                    <!--//-->
                </div>
                    
            </div>
            
        </div>
        <div class="wrap-bt mb30"><a style="width:100%" class="bt-type5" runat="server" id="btnList">목록</a></div>
        
        
    </div>
    
<!--//-->
</div>
	


</asp:Content>