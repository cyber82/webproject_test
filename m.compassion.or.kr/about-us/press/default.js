﻿(function () {

	var app = angular.module('cps.page', []);

	app.controller("defaultCtrl", function ($scope, $http, popup, $location, paramService) {

		$scope.total = -1;
		$scope.list = [];

		$scope.params = {
			page: 1,
			rowsPerPage: 10
		};

		// 파라미터 초기화
		$scope.params = $.extend($scope.params, paramService.getParameterValues());
		if ($scope.params.k_word) $("#k_word").val($scope.params.k_word);

		// 검색
		$scope.search = function (params) {
			$scope.params = $.extend($scope.params, params);
			$scope.params.k_word = $("#k_word").val();
			$scope.getList();
		}

		// list
		$scope.getList = function (params) {
		    
			$scope.params = $.extend($scope.params, params);
			$http.get("/api/press.ashx?t=list", { params: $scope.params }).success(function (result) {
				$scope.list = result.data;
				$.each($scope.list, function () {
					if (this.p_content == null) this.p_content = "";
				})

				$scope.total = result.data.length > 0 ? result.data[0].total : 0;
				console.log($scope.list);

				if (params)
					scrollTo($("#l"), 10);
			});
		}

		$scope.parseDate = function (datetime) {
			return new Date(datetime);
		}

        //상세페이지
		$scope.goView = function (id) {
		    console.log("상세 클릭");
		    $http.post("/api/press.ashx?t=hits&id=" + id).then().finally(function () {
		        location.href = "/about-us/press/view/" + id + "?" + $.param($scope.params);
		        console.log("상세 진입 : "+id);
		    });
		};


		$scope.getList();



	});

})();