﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="about_us_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
	<script>
		$(function () {


			$(".teb_list li[data-idx=1] a").addClass("selected");

			var sly = new Sly($("#header_menu"), {    // Call Sly on frame

				horizontal: 1,
				itemNav: 'centered',
				smart: 1,
				activateOn: 'click',
				mouseDragging: 1,
				touchDragging: 1,
				releaseSwing: 1,
				startAt: 3,
				scrollBy: 1,
				activatePageOn: 'click',
				speed: 300,
				elasticBounds: 1,
				easing: 'easeOutExpo',
				dragHandle: 1,
				dynamicHandle: 1,
				clickBar: 1

			});

			sly.init();


			$.each($("#header_menu a"), function (i) {
				if ($(this).hasClass("selected")) {
					sly.activate(i);
				}
			})

		})

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
	
	<div class="wrap-sectionsub">
	<!---->
    
		<div id="header_menu" class="menu-sympathy sectionsub-margin1" style="position: relative; overflow: hidden;">
    	
			<ul class="teb_list" style="position:absolute;left:0px;top:0;">  <!--임시 inline style(수정가능)-->
        		<li style="display:inline-block"><a class="selected" href="/about-us/about/">컴패션은</a></li>
				<li style="display:inline-block"><a href="/about-us/about/soul/">컴패션의 정신</a></li>
				<li style="display:inline-block"><a href="/about-us/about/policy-child/">어린이 보호 정책</a></li>
				<li style="display:inline-block"><a href="/about-us/about/ci/">한국컴패션 CI</a></li>
				<li style="display:inline-block"><a href="/about-us/about/map/">찾아오시는 길</a></li>
				<li style="display:inline-block"><a href="#"></a></li>
			</ul>
        
		</div>

		<!-- 컴패션은 -->
		<div class="about-introduce teb_content" data-idx="1">
    	
			<p class="txt-caption mt20">함께 아파하는 마음,<br />컴패션(COMPASSION)</p>
			<div class="mt20"><img src="/common/img/page/about/img1.jpg" alt="" width="100%" /></div>
			<p class="txt-sub mt15 mb20">국제어린이양육기구 컴패션은 전 세계 도움이 필요한 어린이들을 1:1로 결연하여 자립 가능한 성인이 될 때까지 양육하며 현재 25개국 180만 명 이상의 어린이들과 함께하고 있습니다.</p>
        
			<div class="compassion-start sectionsub-margin1">
        		<p class="txt-slogon">컴패션의 시작<em>컴패션은 한국에서 시작되었습니다.</em></p>
				<div class="youtube mt20">
					<iframe src="https://www.youtube.com/embed/cNX_DzClfZ4?rel=0&amp;showinfo=0wmode=transparent" frameborder="0" allowfullscreen></iframe>
				</div>
				<strong class="mt20">컴패션의 시작은 ‘한 사람’이었습니다.</strong><br />
				1952년 겨울, 미국인 에버렛 스완슨(Everett Swanson) 목사는 차가운 새벽 거리를 걷던 중, 길가에 널려진 쓰레기 더미를 군용트럭으로 던지는 인부들을 만났습니다. <br /><br />
				그들을 도우려고 다가간 순간, 스완슨 목사는 그것이 쓰레기가 아니라 밤새 혹독한 추위와 배고픔을 견디지 못해 얼어 죽은 어린이들이라는 것을 알게 되었습니다. 큰 충격을 받은 스완슨 목사의 마음에 “너는 이것을 보았다, 이제 무엇을 할 것인가?”라는 질문이 들려왔습니다. 미국으로 돌아간 스완슨 목사는 전 지역을 돌아다니며 외쳤습니다.<br /><br />
				“한국의 어린이를 잊지 말아 주세요. 배고픔과 질병으로 죽어가는 한국 어린이의 후원자가 되어주세요!”<br />
				이것이 컴패션의 시작이었습니다.
			</div>
        
			<p class="txt-bible mt20">
        		예수께서 제자들을 불러 이르시되 내가 무리를 불쌍히 여기노라 (compassion, 컴패션) 그들이 나와 함께 있은 지 이미 사흘이매 먹을 것이 없도다 길에서 기진할까 하여 굶겨 보내지 못하겠노라
				<em>(마태복음 15:32)</em>
			</p>
        
			<p class="txt-caption mt30">컴패션의 양육</p>
			<div class="mt20"><img src="/common/img/page/about/img3.jpg" alt="" width="100%" /></div>
			<p class="txt-caption2 mt20">컴패션은 한 어린이를<br />전인적으로 끝까지 양육합니다.</p>
			<p class="txt-sub mt15">
        		컴패션은, 가난으로 인해 하나님께서 주신 고유한 잠재력을 발휘할 기회를 송두리째 빼앗겨 버린 어린이들에게 기회와 소망을 줌으로써, 어린이가 공동체를 변화시킬 수 있는 주인공으로 자라날 것을 믿습니다. <br /><br />
				1952년 어린이 양육을 시작한 컴패션은, 1954년 1:1결연 방식으로 다양한 영역에서 도움을 주기 시작했습니다. 
				후원자와의 1:1 만남을 통해 어린이에게 자신이 중요한 사람이라는 것과 하나님이 자신을 소중한 사람으로 창조하셨다는 믿음을 심어 주고자 한 것입니다. 이는 부모가 자녀를 돌보는 것처럼 지적, 사회·정서적, 신체적, 영적 영역이라는 온전한 영역에서 
				균형 잡힌 양육을 제공, 자립 가능한 성인이 될 때까지 도움을 받는 전인적 양육(Holistic Child Development Model)을 가능케 했습니다.
			</p>
        
			<p class="txt-bible mt20">
        		예수는 지혜(지적 영역)와 키가 자라가며(신체적 영역) 하나님과(영적 영역) 사람에게(사회·정서적 영역) 더욱 사랑스러워 가시더라
				<em>(누가복음 2:52)</em>
			</p>
        
			<div class="ceo sectionsub-margin1 mt30">
        		<img src="/common/img/page/about/KR_VTHA_090311_[이홍석]_IMG_2221.jpg" alt="" width="100%" />
				<div class="txt"><strong>인사말</strong>한국컴패션을 방문해 주신 분들께 감사드립니다.</div>
			</div>
			<p class="txt-caption2 mt20"><span class="color1">경이로운 대상, 어린이</span></p>
			<p class="txt-sub mt15">
        		전 세계 컴패션어린이들과 만나면서 저는 항상 놀라움을 경험합니다. 상상할 수 없이 힘든 환경 속에서도 어린이들이 티 없이 맑은 미소를 잃지 않기 때문입니다. 아프리카와 남미의 거대한 슬럼가와 필리핀의 쓰레기 마을, 무덤 마을 등에서 발견하는 어린이들의 환한 웃음은 기적과도 같습니다.<br /><br />
				<span class="color1">이 모든 기적이 후원자님의 사랑에서 시작되었습니다.</span> 지금도 수많은 한국후원자 분들께서 꿈을 잃은 어린이들의 연약한 손을 잡아주고 계십니다. <span class="color1">컴패션은 전인적인 양육으로 어린이를 섬기며, 보내주신 사랑을 전하는 기적의 통로가 되겠습니다.</span><br /><br />
				감사합니다.
			</p>
			<p class="txt-ceosign">한국컴패션 대표 <strong>서정인</strong> 드림</p>
        
			<p class="txt-bible mt20">
        		어린아이들이 내게 오는 것을 용납하고 금하지 말라
				하나님의 나라가 이런 자의 것이니라
				<em>(마가복음 10:14)</em>
			</p>


				<!-- @Cream : 조직도 추가 Start -->
				<style type="text/css">
				.organization { margin:30px 0 0; text-align:center; }
				.organization .title { height:245px; margin:0 -20px 27px; color:#fff; text-align:center; background:url('http://m.compassion.or.kr/common/img/page/about/img-organization.jpg') center center no-repeat; background-size:cover; }
				.organization .title:before { content:''; display:inline-block; width:1px; height:100%; margin-right:-6px; vertical-align:middle; }
				.organization .title .txt{display:inline-block;max-width:291px;font-size:12px;line-height:18px;color:#fff;vertical-align:middle;word-wrap:break-word; word-break:keep-all;}
				.organization .title .txt>strong{display:block;font-weight:normal;font-family:'noto_m';font-size:15px;line-height:19px;color:#fff;}
				.organization .title .txt>strong:after { content:''; display:block; width:1px; height:25px; margin:11px auto 14px; background-color:#b5b5b5; }
				.organization .chart span { font-family:'noto_r'; }
				.organization .chart .bg1 { position:relative; z-index:2; display:inline-block; width:98px; height:26px; vertical-align:middle; color:#f4faff; font-size:13px; background-color:#005dab; white-space:nowrap; }
				.organization .chart .bg1.h2 { width:72px; height:50px; }
				.organization .chart .bg1.fs { width:72px; font-size:11px; }
				.organization .chart .bg2 { display:inline-block; width:auto; height:22px; padding:0 6px; vertical-align:middle; color:#333; font-size:13px; background-color:#f0f7fe; white-space:nowrap; }
				.organization .chart .bg2.h2 { width:43px; height:43px; padding:0; }
                .organization .chart .bg2.h3 { width: 43px; height: 60px; padding: 0; }
				.organization .chart .bg1:before,
				.organization .chart .bg2:before { content:''; display:inline-block; height:100%; vertical-align:middle; }
				.organization .chart .mid { display:inline-block; vertical-align:middle; }
				.organization .row { width:100%; max-width:350px; overflow:hidden; margin:0 auto; text-align:left; margin-top:12px; }
				.organization .row h3 { margin:18px 0 9px; font-size:20px; color:#333; font-family:'noto_m'; }
				.organization .row .name { display:table-cell; width:35%; font-size:13px; color:#333; padding-top:2px; }
				.organization .row .jobs { display:table-cell; width:65%; font-size:11px; color:#767676; padding-top:2px; }
				.organization .row ul { display:table; width:100%; }
				.organization .row li { display:table-row; }
				.organization .chart ul { position:relative; }
				.organization .chart .child.ty1 > li { display:inline-block; *display:block; *zoom:0; vertical-align:top; margin:0 4px; }
				.organization .chart .child.ty2 > li {  }
				.organization .chart .line:before, .organization .chart .line:after { content:''; background:#e8e8e8; }
				.organization .chart .line.l1:before { display:block; width:1px; height:38px; margin:10px auto; }
				.organization .chart .line.l1+.l1:before { height:55px; }
				.organization .chart .line.l2:before { display:inline-block; width:40px; height:1px; margin-right:10px; vertical-align:middle; }
				.organization .chart .line.l3:before { display:inline-block; width:20px; height:1px; margin-right:10px; vertical-align:middle; }
				.organization .chart .line.l3:after { position:absolute; left:0; top:50%; width:1px; height:40px; margin:0 auto; }
				.organization .chart .line.l5:before { display:block; width:1px; height:15px; margin:5px auto; }
				.organization .chart .abs { position:absolute !important; }
				.organization .chart .abs.a1 { left:50%; margin:26px 0 0 10px; }
				.organization .chart .abs.a2 { right:50%; top:0; margin:-49px 10px 0 0; }
				/* 2018-02-27 PENTABREED MODIFY : S */
				.organization .chart .abs.a3 { left:calc(50% + 120px); margin:13px 0 0 10px; }
				.organization .chart .abs.a3:after {content:""; height:55px;}
				.organization .chart .line.l7:before { display:block; width:1px; height:85px; margin:5px auto; }
                .organization .chart .abs.a3 .l8{ position:absolute; left:-55px;top:16px;}
				.organization .chart .abs.a3 .l8:before{ display:inline-block; width:35px; height:1px; margin-right:0; vertical-align:middle; position:absolute; top:-7px; right:0; }
				@media all and (max-width : 372px){
					.organization .chart .abs.a3 { left:50%; margin:13px 0 0 10px; }
				}
				/* 2018-02-27 PENTABREED MODIFY : E */
				@media all and (max-width : 320px){
					.organization .chart .bg1.h2 { width:55px; }
					.organization .chart .bg1.fs { width:75px; }
					.organization .row .name { width:40%; }
					.organization .row .jobs { width:60%; }
				}
				</style>

				<div class="organization">
					<h2 class="title">
						<span class="txt"><strong>조직도</strong>컴패션은 꿈을 잃은 어린이들을 그리스도의 사랑으로 양육하기 위해 존재합니다.</span>
					</h2>
					<ol class="chart">
						<li>
							<span class="bg1">이사회</span>
						</li>
						<li class="line l1">
							<span class="bg1"><span class="mid">대표</span></span>
							<ol class="abs a1">
								<li class="line l2"><span class="bg2"><span class="mid">비서실</span></span></li>
							</ol>
						</li>
						<li class="line l1">
							<ul class="child ty1">
								<li class="line l6"><span class="bg1 h2"><span class="mid">사역<br />개발실</span></span>
									<ol class="line l4">
										<li class="abs a2 line l3"><span class="bg2"><span class="mid">사역개발본부</span></span></li>
										<li>
											<ul class="child ty2">
												<li class="line l5"><span class="bg2 h3"><span class="mid">교회<br />협력<br />1</span></span></li>
												<li class="line l5"><span class="bg2 h3"><span class="mid">교회<br />협력<br />2</span></span></li>
												<li class="line l5"><span class="bg2 h2"><span class="mid">애드<br />보킷</span></span></li>
												<li class="line l5"><span class="bg2 h2"><span class="mid">비전<br />트립</span></span></li>
											</ul>
										</li>
									</ol>
								</li>
								<li class="line l6"><span class="bg1 h2 fs"><span class="mid">Marketing &<br />Engagement</span></span>
									<ul class="child ty2 line l4">
									<!-- @Cream : 조직도 수정 20170407 Start -->
									<!-- 2018-02-27 PENTABREED MODIFY : S -->
										<li class="line l5"><span class="bg2 h2"><span class="mid">마케팅</span></span></li>
										<li class="line l5"><span class="bg2 h2"><span class="mid">PR</span></span></li>
										<li class="line l5"><span class="bg2 h2"><span class="mid">프로<br />모션</span></span></li>
									<!-- 2018-02-27 PENTABREED MODIFY : E -->
									<!-- @Cream : 조직도 수정 End -->
									</ul>
								</li>
								<li class="line l6"><span class="bg1 h2"><span class="mid">후원<br />지원실</span></span>
									<ul class="child ty2 line l4">
										<!-- 2018-02-27 PENTABREED MODIFY : S -->
										<li class="line l5"><span class="bg2 h2"><span class="mid">Engage<br>-ment</span></span></li>
										<!-- 2018-02-27 PENTABREED MODIFY : E -->
										<li class="line l5"><span class="bg2 h2"><span class="mid">후원자<br />서비스</span></span></li>
										<li class="line l5"><span class="bg2 h2"><span class="mid">후원<br />지원</span></span></li>
										<li class="line l5"><span class="bg2 h2"><span class="mid">IT</span></span></li>
									</ul>
								</li>
								<li class="line l6">
									<span class="bg1 h2"><span class="mid">경영<br />지원실</span></span>
									<!-- 2018-02-27 PENTABREED MODIFY : S -->
									<ol class="abs a3">
                                        <li class="line l8"><span class="bg2 h2"><span class="mid">정보<br>보안</span></span></li>
										<li class="line l2"><span class="bg2 h2"><span class="mid">경영<br>관리</span></span></li>
									</ol>
									<ul class="child ty2 line l4">
										<li class="line l7"><span class="bg2 h2"><span class="mid">인사<br />총무</span></span></li>
										<li class="line l5"><span class="bg2 h2"><span class="mid">재경</span></span></li>
									</ul>
									<!-- 2018-02-27 PENTABREED MODIFY : E -->
								</li>
							</ul>
						</li>
					</ol>

					<div class="row">
						<div class="directors">
							<h3>이사</h3>
							<ul>
								<li><span class="name">이범 (이사장)</span> <span class="jobs">(주) 그라찌에 회장 / 한국사회과학자료원 이사장</span></li>
								<li><span class="name">서정인 (대표이사)</span> <span class="jobs">한국컴패션 대표</span></li>
								<li><span class="name">김명호</span> <span class="jobs">일산 대림교회 담임목사</span></li>
								<li><span class="name">남창호</span> <span class="jobs">(주) 범창종합기술 대표</span></li>
								<li><span class="name">Ed Anderson</span> <span class="jobs">전 국제컴패션 부총재</span></li>
								<li><span class="name">김도형</span> <span class="jobs">(유) 하이코스 대표</span></li>
								<li><span class="name">남기형</span> <span class="jobs">용산가정폭력관련상담소 소장</span></li>
								<li><span class="name">김홍은</span> <span class="jobs">구립 청파어린이집 원장</span></li>
							</ul>
						</div>
						<div class="auditors">
							<h3>감사</h3>
							<ul>
								<li><span class="name">서윤</span> <span class="jobs">한영회계법인 전무, 미국 공인회계사</span></li>
								<li><span class="name">허규만</span> <span class="jobs">딜로이트 안진회계법인 공인회계사</span></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- @Cream : 조직도 추가 End -->     
        
        
		</div>
		<!--// 컴패션은 -->
   


	<!--//-->
	</div>


</asp:Content>
