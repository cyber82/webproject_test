﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="map.aspx.cs" Inherits="about_us_map" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
	<script>
		$(function () {

			var sly = new Sly($("#header_menu"), {    // Call Sly on frame

				horizontal: 1,
				itemNav: 'centered',
				smart: 1,
				activateOn: 'click',
				mouseDragging: 1,
				touchDragging: 1,
				releaseSwing: 1,
				startAt: 3,
				scrollBy: 1,
				activatePageOn: 'click',
				speed: 300,
				elasticBounds: 1,
				easing: 'easeOutExpo',
				dragHandle: 1,
				dynamicHandle: 1,
				clickBar: 1

			});

			sly.init();


			$.each($("#header_menu a"), function (i) {
				if ($(this).hasClass("selected")) {
					sly.activate(i);
				}
			})

			appendGoogleMapApi();

		})

		
		function appendGoogleMapApi() {
			if (typeof google === 'object' && typeof google.maps === 'object') {
				initMap();
			} else {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.src = "https://maps.googleapis.com/maps/api/js?language=ko&region=kr&key=<%:this.ViewState["googleMapApiKey"].ToString() %>&callback=initMap";
				document.body.appendChild(script);
			}
		}


		function initMap() {

			map = new google.maps.Map(document.getElementById('map'), {
				scrollwheel: true,
				center: { lat: 37.537772136407604, lng: 127.00514361262321 },
				zoom: 16,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});

			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(37.537772136407604, 127.00514361262321),
				icon: "/common/img/icon/pin.png",
				map: map
			});


			map2 = new google.maps.Map(document.getElementById('map2'), {
				scrollwheel: true,
				center: { lat: 37.5418203, lng: 127.0013571 },
				zoom: 16,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});

			var marker2 = new google.maps.Marker({
			    position: new google.maps.LatLng(37.5418161, 127.0035511),
				icon: "/common/img/icon/pin.png",
				map: map2
			});

		
		};
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
	
	<div class="wrap-sectionsub">
	<!---->
    
		<div id="header_menu" class="menu-sympathy sectionsub-margin1" style="position: relative; overflow: hidden;">
    	
			<ul class="teb_list" style="position:absolute;left:0px;top:0;">  <!--임시 inline style(수정가능)-->
        		<li style="display:inline-block"><a href="/about-us/about/">컴패션은</a></li>
				<li style="display:inline-block"><a href="/about-us/about/soul/">컴패션의 정신</a></li>
				<li style="display:inline-block"><a href="/about-us/about/policy-child/">어린이 보호 정책</a></li>
				<li style="display:inline-block"><a href="/about-us/about/ci/">한국컴패션 CI</a></li>
				<li style="display:inline-block"><a class="selected" href="/about-us/about/map/">찾아오시는 길</a></li>
				<li style="display:inline-block"><a href="#"></a></li>
			</ul>
        
		</div>

		<!-- 찾아 오시는 길 -->
		<div class="about-introduce teb_content">
    	
			<div id="map" class="sectionsub-margin1" style="height:294px;"></div>
			<p class="location1 sectionsub-margin1">본사</p>
			<dl class="define-address mt20">
        		<dt>주소</dt>
				<dd>서울시 용산구 한남대로 102-5 (한남동 723-41) 석전빌딩</dd>
			</dl>
			<dl class="define-address">
        		<dt>대표번호</dt>
				<dd>02-740-1000 (평일 09:00 ~18:00/토,일 휴무)</dd>
			</dl>
			<dl class="define-address">
        		<dt>팩스</dt>
				<dd>02-740-1001</dd>
			</dl>
			<dl class="define-address">
        		<dt>이메일</dt>
				<dd>info@compassion.or.kr</dd>
			</dl>
        
			<p class="location2 sectionsub-margin1 mt20">지하철 이용 시</p>
			<ul class="define-subway">
        		<li><em class="subwaytype1">6</em>호선 한강진역 2번 출구 → 한남 외국인 아파트/한남초교 방면 도보로 10~15분</li>
				<li><em class="subwaytype1">6</em>호선 한강진역 2번 출구 → 110B 버스로 환승 → 순천향 대학 병원 정류장 하차 → 육교 건너 도보로 5분 </li>
			</ul>
        
			<p class="location3 sectionsub-margin1 mt20">버스 이용 시</p>
			<ul class="define-bus">
        		<li><em class="bustype1">간선</em>110A, 110B, 140,142, 144, 402, 405, 407, 408, 420, 470, 471, 472 </li>
				<li><em class="bustype2">지선</em>0018, 0213, 3011, 6211, 8620</li>
				<li><em class="bustype3">광역</em>9401, 9409</li>
				<li><em class="bustype4">순환</em>03</li>
				<li><em class="bustype3">직행</em>1005-2,1500, 5000, 5005, 5007, 5500-1, 5500-2, 8100, 8150, 8130, 8200, 9000, 9001, 9003</li>
				<li><em class="bustype5">공항</em>6030</li>
			</ul>
        
        
        
			<div id="map2" class="sectionsub-margin1 mt40" style="height:294px;"></div>
			<p class="location1 sectionsub-margin1">별관</p>
			<dl class="define-address mt20">
        		<dt>주소</dt>
				<dd>서울특별시 용산구 한남대로 150 신동빌딩 5층</dd>
			</dl>
			<dl class="define-address">
        		<dt>대표번호</dt>
				<dd>02-740-1000  (평일 09:00 ~18:00/토,일 휴무)</dd>
			</dl>
			<dl class="define-address">
        		<dt>팩스</dt>
				<dd>02-740-1001</dd>
			</dl>
			<dl class="define-address">
        		<dt>이메일</dt>
				<dd>info@compassion.or.kr</dd>
			</dl>
        
			<p class="location2 sectionsub-margin1 mt20">지하철 이용 시</p>
			<ul class="define-subway">
        		<li><em class="subwaytype2">6</em>호선 한강진역 2번 출구 → 한남 외국인 아파트/한남초교 방면 도보로 도보로 5~10분</li>
			</ul>
        
			<p class="location3 sectionsub-margin1 mt20">버스 이용 시</p>
			<ul class="define-bus">
				<li><em class="bustype1">간선</em>142, 144, 400, 402, 407, 420, N13, 110B</li>
				<li><em class="bustype2">지선</em>3011, 6211</li>
			</ul>
        
		</div>
		<!-- //찾아 오시는 길-->



	<!--//-->
	</div>


</asp:Content>
