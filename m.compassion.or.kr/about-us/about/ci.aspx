﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ci.aspx.cs" Inherits="about_us_ci" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
	<script>
		$(function () {

			var sly = new Sly($("#header_menu"), {    // Call Sly on frame

				horizontal: 1,
				itemNav: 'centered',
				smart: 1,
				activateOn: 'click',
				mouseDragging: 1,
				touchDragging: 1,
				releaseSwing: 1,
				startAt: 3,
				scrollBy: 1,
				activatePageOn: 'click',
				speed: 300,
				elasticBounds: 1,
				easing: 'easeOutExpo',
				dragHandle: 1,
				dynamicHandle: 1,
				clickBar: 1

			});

			sly.init();


			$.each($("#header_menu a"), function (i) {
				if ($(this).hasClass("selected")) {
					sly.activate(i);
				}
			})

		});
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
	
	<div class="wrap-sectionsub">
	<!---->
    
		<div id="header_menu" class="menu-sympathy sectionsub-margin1" style="position: relative; overflow: hidden;">
    	
			<ul class="teb_list" style="position:absolute;left:0px;top:0;">  <!--임시 inline style(수정가능)-->
        		<li style="display:inline-block"><a href="/about-us/about/">컴패션은</a></li>
				<li style="display:inline-block"><a href="/about-us/about/soul/">컴패션의 정신</a></li>
				<li style="display:inline-block"><a href="/about-us/about/policy-child/">어린이 보호 정책</a></li>
				<li style="display:inline-block"><a class="selected" href="/about-us/about/ci/">한국컴패션 CI</a></li>
				<li style="display:inline-block"><a href="/about-us/about/map/">찾아오시는 길</a></li>
				<li style="display:inline-block"><a href="#"></a></li>
			</ul>
        
		</div>

		<!-- 한국컴패션CI -->
		<div class="about-introduce teb_content">
    	
			<p class="txt-caption mt20">컴패션 로고</p>
			<p class="txt-sub color1 mt15 align-center">컴패션 양육을 진정성 있게 전하기 위해<br />전 세계 컴패션이 일관성 있게 소통합니다.</p>
			<div class="mt20"><img src="/common/img/page/about/ci1.jpg" alt="" width="100%" /></div>
			<p class="txt-sub mt20  mb20">
        		컴패션 로고는 전 세계 모든 컴패션에서 동일하게 사용하며, 로고의 각 요소는 단독으로 사용하지 않습니다. 모든 컴패션의 커뮤니케이션에 표시되어야 합니다. 
				로고는 어떤 일이 있어도 변형되거나 재창조 되어서는 안되며, 어떠한 첨가나 삭제도 있어서는 안 됩니다.
			</p>
		
			<div class="compassion-ci sectionsub-margin1">
        		<div><img src="/common/img/page/about/ci2.jpg" alt="" width="100%" /></div>
				<p class="txt-ci">컴패션 <em>COMPASSION</em></p>
				“예수께서 제자들을 불러 이르시되 내가 무리를 불쌍히 여기노라(compassion, 컴패션)(마태복음 15:32)”는 성경 말씀을 나타냅니다. 예수님이 이 땅에서 우리에게 품으셨던 마음, ‘함께 아파하는 마음’을 의미합니다.
            
				<div class="mt30"><img src="/common/img/page/about/ci3.jpg" alt="" width="100%" /></div>
				<p class="txt-ci">캐치프레이즈 <em>Catchphrase</em></p>
				“꿈을 잃은 어린이들에게 그리스도의 사랑을”이라는 캐치프레이즈는 컴패션이 가난 때문에 잠재력을 발휘하지 못하고 있는 연약한 어린이를 대상으로 하며, 하나님의 사랑을 바탕으로 하는 어린이 양육 사역임을 분명히 하고 있습니다. 

				<div class="mt30"><img src="/common/img/page/about/ci4.jpg" alt="" width="100%" /></div>
				<p class="txt-ci">스키피 <em>skippy</em></p>
				꿈을 잃은 어린이가 꿈을 찾고 높이 도약하는 모습(SKIP)을 담고 있습니다. 스키피는 단독으로 사용하지 않습니다.
			</div>

			<p id="blue" class="txt-caption pt30">컴패션 블루코너(BLUE CORNER)</p>
			<p class="txt-sub color1 mt15 align-center">도움이 필요한 어린이를 위해 일한다는<br />컴패션의 정체성을 드러냅니다.</p>
			<div class="mt20"><img src="/common/img/page/about/ci5.jpg" alt="" width="100%" /></div>
			<p class="txt-sub mt20">
        		추수할 때, 가난한 자와 객을 위하여 밭 모퉁이까지 다 베지 말라는 말씀에서 ‘밭 모퉁이’ 를 상징하는 컴패션 블루코너는, 우리가 도움이 필요한 어린이들을 돕고 대변하는 목소리임을 나타내고자 하는 컴패션 브랜드의 중요한 요소입니다. <br />
				이를 컴패션이 소통하는 모든 매체 앞 표지 오른쪽 위에 새겨 넣어 하나님이 성경에서 이를 말씀으로 명하고 계심과 우리가 누구인지를 잊지 않고자 합니다. 
			</p>
        
			<p class="txt-bible mt20">
				너희 땅의 곡물을 벨 때에 밭 모퉁이 (코너, the corners)까지 다 베지 말며 떨어진 것을 줍지 말고 너는 그것을 가난한 자와 객을 위하여 버려두라 나는 너희 하나님 여호와니라
				<em>(레위기 23:22)</em>
			</p>
        
			<div class="compassion-ci2 sectionsub-margin1 mt30">
        		<p class="txt-slogon">컴패션 블루<em>컴패션은 깊고 강한 파란색입니다.</em></p>
            
				<div class="ci-color">
            		<img src="/common/img/page/about/ci6.jpg" alt="" />
					팬톤 286<em>(PANTON 286)</em>
				</div>
            
				컴패션은 색을 통해 우리가 경험하고 있는 세계의 진짜 모습을 표현하고자 합니다. 또한 컴패션을 나타내는 대표색은 짙은 파란색이며, 일반적으로 파란색은 평온과 책임, 신뢰를 의미합니다. <br /><br />
				그 중에서도 컴패션의 파란색, ‘팬톤 286’은 깊고 강한 파란색입니다. 자신감 있고, 희망적이며, 신뢰할 만하고 효과적이라는 의미를 담고자 합니다. 또한 컴패션이 하나님의 사랑에 의해 어린이를 양육하고 있음을 보여줍니다.(문화에 따라 파란 색은 기독교성을 나타냅니다.)
			</div>
        

        
		</div>
		<!-- //한국컴패션CI -->



	<!--//-->
	</div>


</asp:Content>
