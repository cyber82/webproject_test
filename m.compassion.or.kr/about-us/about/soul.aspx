﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="soul.aspx.cs" Inherits="about_us_soul" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
	<script>
		$(function () {

			var sly = new Sly($("#header_menu"), {    // Call Sly on frame

				horizontal: 1,
				itemNav: 'centered',
				smart: 1,
				activateOn: 'click',
				mouseDragging: 1,
				touchDragging: 1,
				releaseSwing: 1,
				startAt: 3,
				scrollBy: 1,
				activatePageOn: 'click',
				speed: 300,
				elasticBounds: 1,
				easing: 'easeOutExpo',
				dragHandle: 1,
				dynamicHandle: 1,
				clickBar: 1

			});

			sly.init();



			$.each($("#header_menu a"), function (i) {
				if ($(this).hasClass("selected")) {
					sly.activate(i);
				}
			})


		})

		
	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
	
	<div class="wrap-sectionsub">
	<!---->
    
		<div id="header_menu" class="menu-sympathy sectionsub-margin1" style="position: relative; overflow: hidden;">
    	
			<ul class="teb_list" style="position:absolute;left:0px;top:0;">  <!--임시 inline style(수정가능)-->
        		<li style="display:inline-block"><a href="/about-us/about/">컴패션은</a></li>
				<li style="display:inline-block"><a class="selected" href="/about-us/about/soul/">컴패션의 정신</a></li>
				<li style="display:inline-block"><a href="/about-us/about/policy-child/">어린이 보호 정책</a></li>
				<li style="display:inline-block"><a href="/about-us/about/ci/">한국컴패션 CI</a></li>
				<li style="display:inline-block"><a href="/about-us/about/map/">찾아오시는 길</a></li>
				<li style="display:inline-block"><a href="#"></a></li>
			</ul>
        
		</div>

	
		<!-- 컴패션의 정신-->
		<div class="about-introduce teb_content">
    	
			<div class="compassion-soul sectionsub-margin1">
        		<p class="txt-slogon">컴패션의 약속<em>컴패션 사명선언문</em></p>
				컴패션은 꿈을 잃은 어린이들을 그리스도의 사랑으로 양육하기 위해 존재합니다.
			</div>
        
			<div class="compassion-soul2 sectionsub-margin1">
        		<p class="txt-slogon">컴패션 사역의 3가지 중심<em>컴패션 3C</em></p>
            
				<p class="txt-define bg1">
            		<span class="txt-dt">그리스도 중심<em>CHRIST-CENTERED</em></span>
            		예수님을 중심으로 일합니다.
				</p>
				<p class="txt-define bg2">
            		<span class="txt-dt">어린이 대상<em>CHLID-FOCUSED</em></span>
            		어린이를 대상으로 합니다.
				</p>
				<p class="txt-define bg3">
            		<span class="txt-dt">교회 기반<em>CHURCH-BASED</em></span>
            		지역교회를 기반으로 어린이를 양육합니다.
				</p>
			</div>
        
			<div class="compassion-soul3 mt30">
        		<p class="txt-slogon">컴패션 핵심가치<em>“컴패션은 <span class="color2">지역교회와<br />
					협력하는[Committed to the Church]<br />
					그리스도 중심[Christ-centered]의 기관</span>으로<br />
					핵심가치를 가지고 있습니다.”</em>
				</p>
            	
				<p class="txt-define">
					<span class="txt-dt">정직<em>Integrity</em></span>
					정직을 최우선으로<br />합니다.
				</p>
				<p class="txt-define">
					<span class="txt-dt">탁월<em>Excellence</em></span>
					항상 탁월한 성과를 향한<br />끊임없는 시도와 노력을<br />추구합니다.
				</p>
				<p class="txt-define">
					<span class="txt-dt">청지기 정신<em>Stewardship</em></span>
					하나님의 청지기로서<br />투명하고 성실하게<br />일합니다.
				</p>
				<p class="txt-define">
					<span class="txt-dt">존귀<em>Dignity</em></span>
					어린이를 비롯한 모든 사람을<br />하나님의 사랑하는 자녀로<br />존귀하게 여깁니다.
				</p>
			</div>
        
        
		</div>
		<!-- //컴패션의 정신-->


	<!--//-->
	</div>


</asp:Content>
