﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="policy-child.aspx.cs" Inherits="about_us_policy" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>
<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
	<script>
		$(function () {


			var sly = new Sly($("#header_menu"), {    // Call Sly on frame

				horizontal: 1,
				itemNav: 'centered',
				smart: 1,
				activateOn: 'click',
				mouseDragging: 1,
				touchDragging: 1,
				releaseSwing: 1,
				startAt: 3,
				scrollBy: 1,
				activatePageOn: 'click',
				speed: 300,
				elasticBounds: 1,
				easing: 'easeOutExpo',
				dragHandle: 1,
				dynamicHandle: 1,
				clickBar: 1

			});

			sly.init();


			$.each($("#header_menu a"), function (i) {
				if ($(this).hasClass("selected")) {
					sly.activate(i);
				}
			})

		})

	</script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

	
	
	<div class="wrap-sectionsub">
	<!---->
    
		<div id="header_menu" class="menu-sympathy sectionsub-margin1" style="position: relative; overflow: hidden;">
    	
			<ul class="teb_list" style="position:absolute;left:0px;top:0;">  <!--임시 inline style(수정가능)-->
        		<li style="display:inline-block"><a href="/about-us/about/">컴패션은</a></li>
				<li style="display:inline-block"><a href="/about-us/about/soul/">컴패션의 정신</a></li>
				<li style="display:inline-block"><a class="selected" href="/about-us/about/policy-child/">어린이 보호 정책</a></li>
				<li style="display:inline-block"><a href="/about-us/about/ci/">한국컴패션 CI</a></li>
				<li style="display:inline-block"><a href="/about-us/about/map/">찾아오시는 길</a></li>
				<li style="display:inline-block"><a href="#"></a></li>
			</ul>
        
		</div>
		
		<!-- 어린이 보호 정책-->
		<div class="about-introduce teb_content">
    	
			<p class="txt-caption mt20">컴패션의 믿음</p>
			<div class="mt20"><img src="/common/img/page/about/img6.jpg" alt="" width="100%" /></div>
			<p class="txt-caption2 mt20">어린이 보호에 대한 컴패션의 믿음 </p>
			<p class="txt-sub mt15 mb20">컴패션은 모든 어린이가 모든 형태의 학대, 착취, 방치로부터 보호되어야 한다고 믿습니다. 컴패션 이사회 정책은 다음과 같이 선언하고 있습니다.
				“어린이에 대한 관심은 컴패션이 세워진 주춧돌이다. 우리는 모든 형태의 어린이 학대와 착취에 반대하며, 우리의 양육프로그램에 등록된 모든 어린이에게는 어떤 위험도 가해지지 않도록 우리가 할 수 있는 권한 안에서 최선을 다할 것이다.”
			</p>
        
			<div class="policy-child sectionsub-margin1">
        		<p class="txt-slogon">어린이 보호 정책 개요<em>어린이 보호는 컴패션에 등록된 모든 어린이들을<br />
					학대와 착취 같은 극단적인 취약으로부터 안전하게<br />
					보호하는 것을 의미합니다. </em>
				</p>
				<div class="list-bu mt20">
            		<ul>
                		<li>어린이들이 자신의 잠재력을 성장시켜 나갈 수 있도록 긍정적이고 안전한 환경을 제공합니다. </li>
						<li>어린이를 항상 존귀하게 여기고, 존중하며, 정직하게 보살필 수 있는 교사 및 양육자의 적극적인 헌신이 필요합니다.</li>
						<li>어린이 보호 관련 정책과 전략, 절차 등을 통해 학대 및 기타 취약성의 발생을 방지하고 줄여나갑니다.</li>
						<li>신체적, 성적, 감정적, 언어에 의한 폭력에 관련하여 수용할 수 있는, 그리고 수용할 수 없는 한계에 대해 어린이들을 교육시킵니다.</li>
					</ul>
				</div>
			</div>
        
			<div class="policy-child2 mt30">
        		<p class="txt-slogon">어린이 보호 서약</p>
				컴패션의 모든 직원(교역자 포함) , 각국 협력기관, 자원봉사자(양육프로그램에 소속되어 있지 않으나 컴패션어린이에  직접적인 접근이 가능한 모든 이 포함), 그리고 현지를 방문하도록 허가 받은 후원자와 참가자들은, 어린이 보호와 이와 관련된 행동 강령을 포함(학대와 착취로부터 어린이를 보호하고 옹호하는 것)하는 다음의 헌신 서약에 서명하고 이를 준수하여야 합니다. <br /><br />
				(본 서약문은 국제컴패션의 ‘어린이 보호에 대한 헌신서약(행동 강령 포함)’의 일부를 담고 있습니다.) 
			
				<ol class="mt20">
            		<li><em>01.</em>나는 모든 어린이들에 대하여 존귀와 존경의 마음을 가질 것이며, 인종, 성별, 연령, 종교, 장애, 사회적 배경 및 문화에 상관없이 그들에게 예수님의 사랑을 실천하겠습니다. </li>
					<li><em>02.</em>나는 언어적, 감성적, 성적 또는 신체적 학대 등 어린이에게 수치심을 유발하거나 모욕적일 수 있는 그 어떤 부적절한 행동도 절대 하지 않겠습니다. </li>
					<li><em>03.</em>나는 성경적 원리에 입각하여 어린이들의 존귀함과 가치가 존중될 수 있도록 어린이 보호에 관한 방침들을 만들어 가는 데 적극 힘쓰겠습니다. </li>
					<li><em>04.</em>어린이들에게 연령에 맞는 책임감을 가르치고, 어린이의 능력과 요구되는 기대치에 맞게 운영하겠습니다. </li>
					<li><em>05.</em>나는 결코 어린이와 성적인 관계를 가지지 않겠습니다. </li>
					<li><em>06.</em>나는 어린이와 연령에 맞는 대화를 할 것입니다. 또한, 소셜 미디어, 온라인, 글, 그림, 비디오, 대면해서 나누는 대화 등, 어떠한 방법을 통해서라도 어린이와 부적절한 대화를 나누지 않겠습니다. </li>
					<li><em>07.</em>나는 후원어린이를 훈육할 때 절대로 부적절한 언어나 신체적으로 학대(때리기, 치기, 막대로 치기 등)하지 않겠습니다. </li>
					<li><em>08.</em>나는 허가 없이는 후원어린이와 따로 시간을 갖거나 여행하는 것을 피하겠습니다. </li>
					<li><em>09.</em>나는 한 명 이상의 허가 받은 성인이 있을 시에만 후원어린이와 공개적 또는 가시적 장소에서만 활동에 참여하겠습니다. </li>
					<li><em>10.</em>나는 해당 지역법을 따라 컴패션어린이 또는 미성년 어린이와 ‘사적 관계’를 요구하지 않겠습니다. </li>
					<li><em>11.</em>나는 어린이가 학대당하는 것을 보거나, 위험에 처한 사실을 목격하거나, 어린이가 나에게 학대 사실을 제보한다면, 이를 곧바로 해당 직원이나 기관에 보고하겠습니다. 위험에 빠진 어린이를 구하기 위해서 나의 모든 노력을 다하겠습니다. </li>
				</ol>
			</div>
        
			<div class="policy-child3 mt20">
        		나는 컴패션 어린이 보호 서약 내용에 관한 나의 의무와 책임을 이해하며, 컴패션 및 나의 업무와 관련된 이 서약 내용을 준수하겠습니다.<br /><br />
				나는 컴패션이 필수로 지정한 어린이학대혐의는 엄밀히 수사되는 범죄임을 숙지하겠습니다.<br /><br />
				이 행동 강령을 위반하게 되면 지역법에 따라 조사받을 수 있으며, 기관 교육 방법 등에 따라 처벌받게 됩니다.<br /><br />
				이는 퇴사, 현지 어린이센터의 협력 결렬, 결연 취소 등과 같이 모든 컴패션과 연관된 활동이 금지됨을 뜻합니다.<br /><br />
				나는 서약을 통해서 컴패션 안에서 사역과 더불어 내가 해야 하는 업무의 기대치가 무엇인지 숙지하였습니다. <br /><br />
				<strong>나는 어린이 보호 서약서 내용을 준수함에 동의합니다.</strong>
			</div>
        
			<p class="txt-bible mt30">
        		하나님 아버지 앞에서 정결하고 더러움이 없는 경건은 곧 고아와 과부를 그 환난중에 돌보고 또 자기를 지켜 세속에 물들지 아니하는 그것이니라 
				<em>(야고보서 1:27)</em><br />
				예수께서 이르시되 어린 아이들을 용납하고 내게 오는 것을 금하지 말라 천국이 이런 사람의 것이니라 하시고
				<em>(마태복음 19:14)</em>
			</p>

        
		</div>
		<!-- //어린이 보호 정책-->

	<!--//-->
	</div>


</asp:Content>
