﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Configuration;

public partial class about_us_newsletter_default : MobileFrontBasePage {


    public override bool RequireLogin
    {
        get
        {
            return true;
        }
    }

    protected override void OnBeforePostBack() {
        base.OnBeforePostBack();

        this.ViewState["auth_domain"] = ConfigurationManager.AppSettings["domain_auth"];

        /*
		국내거주인경우 회원주소/연락처를를 컴파스에서 가져온다.
		*/
        UserInfo sess = new UserInfo();
        locationType.Value = sess.LocationType;

        var addr_result = new SponsorAction().GetAddress();
        SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;
        hfAddressType.Value = addr_data.AddressType;

        BtnAddDisplay();
    }

    protected void BtnAddDisplay()
    {
        if(UserInfo.IsLogin)
        {
            UserInfo sess = new UserInfo();
			if (sess.Birth.EmptyIfNull() != "" && sess.Birth.Length > 3)
            {
				var age = DateTime.Now.Year - int.Parse(sess.Birth.Substring(0, 4));
				//Response.Write(age);
				// 40세 이상이고 conId가 있는 회원(conId가 6자리여야만 유효한 conId임)
				if((age >= 40 && sess.ConId.Length == 6) || (age >= 40 && sess.ConId.Length == 5))
                {
                    using (FrontDataContext dao = new FrontDataContext())
                    {
                        var exist = www6.selectQ<subscription>("s_userid", sess.UserId);
                        //if (dao.subscription.Any(p => p.s_userid == sess.UserId))
                        if(exist.Any())
                        {
                            cancel.Style["display"] = "block";
                        }
                        else
                        {
                            phAdd.Style["display"] = "block";
                        }
                    }
				}
			}

        }
    }
}