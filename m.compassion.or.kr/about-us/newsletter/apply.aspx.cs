﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;

public partial class about_us_apply : MobileFrontBasePage {
	

	public override bool RequireLogin{
		get{
			return true;
		}
	}

	protected override void OnBeforePostBack() {

		base.OnBeforePostBack();

		if (!UserInfo.IsLogin) {
			Response.ClearContent();
			return;
		}

		var userInfo = new UserInfo();
		var age = DateTime.Now.Year - int.Parse(userInfo.Birth.Substring(0, 4));
        
		// 40세 이상이고 conId가 있는 회원(conId가 6자리나 5자리여야만 유효한 conId임)
        
		if (!((age >= 40 && userInfo.ConId.Length == 6) || (age >= 40 && userInfo.ConId.Length == 5))) {
			Response.ClearContent();
			return;
		}
        
		/*
		국내거주인경우 회원주소/연락처를를 컴파스에서 가져온다.
		*/
		UserInfo sess = new UserInfo();

		name.InnerText = sess.UserName;

		if (sess.LocationType == "국내") {

			var addr_result = new SponsorAction().GetAddress();
			if(!addr_result.success) {
				base.AlertWithJavascript(addr_result.message, "goBack()");
				return;
			}

			SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;
			zipcode.Value = addr_data.Zipcode;
			addr1.Value = addr_data.Addr1;
			addr2.Value = addr_data.Addr2;

			var comm_result = new SponsorAction().GetCommunications();
			if(!comm_result.success) {
				base.AlertWithJavascript(comm_result.message, "goBack()");
				return;
			}

			SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
            phone.InnerText = comm_data.Mobile;
		}
		
	}
	
}