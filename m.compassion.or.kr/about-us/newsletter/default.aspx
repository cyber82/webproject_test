﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="about_us_newsletter_default" Culture="auto" UICulture="auto" MasterPageFile="~/main.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/main.master" %>
<%@ Register Src="/common/breadcrumb.ascx" TagPrefix="uc" TagName="breadcrumb" %>
<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="default.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#btn_subscription").click(function () {
                $.get("/api/my/account.ashx?t=check-newsletter", function (r) {
                    if (r.success) {
                        if (r.data.email) {
                            alert("이미 구독 중입니다.");
                        } else {
                            location.href = "/my/account/";
                        }
                    }

                    return false;
                });
            })
        });

        function jusoCallback(zipNo, addr1, addr2, roadFullAddr, roadAddrPart1, addrDetail, roadAddrPart2, engAddr, jibunAddr, admCd, rnMgtSn, bdMgtSn) {
            $(".zipcode").val(zipNo);
            $(".addr1").val(addr1);
            $(".addr2").val(addr2);
        }

    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <script type="text/javascript" src="<%:this.ViewState["auth_domain"].ToString() %>/cert/cert.js"></script>
    <input type="hidden" runat="server" id="hfAddressType" value="" />
    <input type="hidden" runat="server" id="locationType" value="" />

    <section class="sub_body" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <!-- 타이틀 -->
        <div class="page_tit">
            <div class="titArea">
                <h1>뉴스<em>레터</em></h1>
                <span class="desc">어린이들과 후원자님들의 감동적인 이야기와 다양한 컴패션 소식을 전해드립니다</span>

                <uc:breadcrumb runat="server" />
            </div>
        </div>
        <!--// -->

        <!-- s: sub contents -->
        <div class="subContents padding0 aboutus">

            <div class="newsletter tac">


				<div class="visual_intro">
					<h2 class="subTit_m wh">한국컴패션 뉴스레터입니다</h2>
					<span class="bar"></span>
					<p class="con_wh">후원자님이 궁금해하시는 현지 어린이들의 감동적인 이야기와 후원자님의 따뜻한 삶의 고백,<br />
					컴패션 뉴스와 행사 안내 등 다양한 컴패션 소식을 만나실 수 있습니다.</p>
				</div>

                <div class="news_con">
                    <ul class="clear2">
                        <!--최신 뉴스레터 이미지 사이즈 645 * 478 -->

                        <li ng-class="{newest:$first}" ng-repeat="item in list">
                            <!-- 작은 이미지 사이즈 310 * 230 -->
                            <img ng-src="{{item.b_thumb}}" alt="뉴스 리스트 이미지" />
                            <img class="year" ng-src="/common/img/page/about-us/b_{{item.b_sub_type}}.png" alt="발행년도 표시 이미지" ng-if="$first" />
                            <img class="year" ng-src="/common/img/page/about-us/s_{{item.b_sub_type}}.png" alt="발행년도 표시 이미지" ng-if="!$first" />

                            <!-- 마우스 오버 시 -->
                            <a href="#" class="over"><span class="btn_view1" ng-click="goView(item.b_id)">VIEW</span></a>
                        </li>
                        
                    </ul>

                    <div>
                        <button class="btn_com_more" ng-click="showMore($event)" ng-hide="params.page > total/6"><span>더 보기</span></button>
                    </div>

                </div>

                <div class="tal w980">
                    <p class="s_tit1 mb25">이메일로 더 다양한 컴패션의 소식을 받아보시겠어요?</p>
                    <a href="#" id="btn_subscription">
                        <img src="/common/img/page/about-us/img_subscription.jpg" alt="컴패션 소식 구독 신청" /></a>

                    <div class="conb">
                        <div runat="server" id="phAdd" style="display:none">
                            <p class="s_tit2">오프라인으로 뉴스레터를 받아보시려면 <em>[신청하기]</em> 버튼을 눌러 주세요.</p>
                            <a href="#" class="btn_s_type1" ng-click="modal.show()">신청하기</a>
                        </div>
                        <div id="cancel" runat="server"  style="display:none">
                            <p class="s_tit2">오프라인 뉴스레터를 구독 중입니다. <em>[구독관리]</em> 버튼을 눌러 주세요.</p>
                            <a href="#" class="btn_s_type1" ng-click="cancelModal.show()">구독관리</a>
                        </div>
                    </div>

                </div>


            </div>

        </div>


        <!--// e: sub contents -->

        <div class="h100"></div>


    </section>



</asp:Content>
