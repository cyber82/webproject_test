﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cancel.aspx.cs" Inherits="about_us_cancel" %>

<div style="background-color:transparent; width: 600px;">

    <div class="pop_type1 w600 relative">
		<div class="pop_title">
			<span>오프라인 뉴스레터 구독 관리</span>
			<button class="pop_close"><span><img src="/common/img/btn/close_1.png" alt="팝업닫기" ng-click="cancelModal.close($event)"/></span></button>
		</div>

		<div class="pop_content newspop">

			<div class="tableWrap2">
				<table class="tbl_type1">
					<caption>정보입력 테이블</caption>
					<colgroup>
						<col style="width:14%" />
						<col style="width:86%" />
					</colgroup>
					<tbody>
						<tr>
							<th scope="row"><label for="name">이름</label></th>
							<td><p type="text" name="name" class="name con_bl" runat="server" id="name"></p></td>
						</tr>
						<tr>
							<th scope="row"><label for="phone">휴대폰</label></th>
							<td><p name="phone" class="con_bl phone" runat="server" id="phone"></p></td>
						</tr>
						<tr class="address">
							<th scope="row"><span>주소</span></th>
							<td>
								<div class="codeNum">
									<label for="postcode" class="hidden">우편번호</label>
                                    <input type="text" name="zipcode" runat="server" id="zipcode"  class="input_type2 mb10 zipcode" style="width:150px;" readonly="readonly"/>
								</div>
								<div>
									<label for="name" class="hidden">기본주소</label>
									<input type="text" name="addr1" runat="server" id="addr1"  class="input_type2 mb10 addr1" style="width:400px;" readonly="readonly"/>
								</div>
								<div>
									<label for="name" class="hidden">상세주소</label>
									<input type="text" name="addr2" runat="server" id="addr2" class="input_type2 addr2" style="width:400px;" readonly="readonly"/>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="conb">				
				<p class="s_con3">입력된 개인정보가 일치하지 않는 경우,<br />
					<a href="/my/account/" class="fc_black">마이컴패션>개인정보 수정</a>으로 이동하여 변경해주시기 바랍니다.</p>
			</div>

			<div class="tac">
				<a href="#" class="btn_type1" ng-click="cancelModal.request($event)">구독해지</a>
			</div>

			<!-- 시스템 팝업 -->
            	<!--/
			<div class="systempop_x" style="display:none">
				<div class="con">
					<p class="s_tit5">구독해지</p>
					<p class="s_con3">
						현재 <em>홍길동</em> 후원자님은<br />
						오프라인 뉴스레터를 구독하고 계십니다.<br />
						정말 구독을 해지하실 건가요?
					</p>
				</div>
				<div class="conb"><a href="#" class="btn_s_type2">확인</a></div>
				<button class="close">닫기</button>
			</div>
		/  -->

		</div>
	</div>
</div>
