﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="about_us_notice_default" culture="auto" uiculture="auto" MasterPageFile="~/main.Master" enableEventValidation="false" %>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
	
</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="default.js"></script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <div class="wrap-sectionsub" ng-app="cps" ng-cloak  ng-controller="defaultCtrl">
<!---->
    
    <div class="wrap-tab3 sectionsub-margin1">
        <a style="width:50%" href="/about-us/report/">사업성과보고서</a>
        <a style="width:50%" class="selected" href="/about-us/revenue/">재무/감사보고</a>
    </div>

    <div class="about-report">
    	
        <div class="revenue-desc mt30">
			<p class="tit">효율적이고 정직하게 지켜 나가겠습니다!</p>
			<div class="link-area">
				<a href="http://www.ecfa.org/MemberProfile.aspx?ID=4466" class="logo_ecfa" target="_blank"><img src="/common/img/page/about/logo-ecfa.jpg" alt="ECFA Charter" /></a>
				<a href="https://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=3555" class="logo_charity" target="_blank"><img src="/common/img/page/about/logo-charity.jpg" alt="Charity Navigator" /></a>
				<a href="http://www.give.org/charity-reviews/national/child-sponsorship/compassion-international-in-colorado-springs-co-3017" class="logo_bbb" target="_blank"><img src="/common/img/page/about/logo-bbb.jpg" alt="BBB" /></a>
			</div>
			<p class="txt">
				컴패션은 후원금의 80% 이상을 반드시 어린이양육을 위해 사용한다는 원칙을 철저히 지킵니다. 후원금을 효율적이고 정직하게 사용하고자 주력하며 이를 위해 전 세계 컴패션의 각 수혜국과 후원국은 정기적으로 엄격한 내·외부 감사를 받고 있습니다. 이로써 컴패션은 ECFA<span class="small-txt">(Evangelical Council for Financial Accountability)</span>,
				채리티내비게이터<span class="small-txt">(charitynavigator)</span>, BBB<span class="small-txt">(Better Business Bureau)</span> 등 국제적인 평가기관에서 좋은 평가를 받고 있습니다. 채리티내비게이터는	정직성과 관련된 <span class="fc-blue">‘재정투명성과 책무성<span class="small-txt">(Accountability and Transparency)</span>’ 항목에서 가장 높은 점수인 100점으로 최고점</span>을 받았으며, <span class="fc-blue">전체평가에서도 14년 연속 별 4개</span>를 획득했습니다.<br />
				<br />
				<span class="fc-black">80:20원칙은 정직하고 효율적인 후원금 사용을 위한 컴패션의 약속입니다.</span>
			</p>

			<div class="kpmg-desc">
				한국컴패션은 재정의 건실함과 투명성을 유지하기 위하여 삼정회계법인(KPMG)으로부터 외부감사를 받으며 각 수혜국은 내·외부 감사를 실시하고 있습니다.
			</div>
		</div>
		
        <ul class="report-finance mt30" id="l">
        	<li ng-repeat="item in list">{{item.b_title}} 
            	<span class="btn">
                	<a class="bt-type6" style="width:80px" ng-click="goView(item.b_id)">보고서 보기</a>
                    <a class="bt-type5" style="width:80px" href="{{item.b_file}}">PDF 다운로드</a>
                </span>
            </li>

        </ul>
        
		<div class="mb30">	
			<paging class="small" page="params.page" page-size="rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>
        </div>
    </div>
    
<!--//-->
</div>
    
</asp:Content>