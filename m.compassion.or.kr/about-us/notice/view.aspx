﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="about_us_notice_view" MasterPageFile="~/main.Master"%>
<%@ MasterType virtualpath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

</asp:Content>


<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">
    <!-- sub body -->
	<section class="wrap-sectionsub">

        <div class="wrap-board">

            <div class="view-board">
                <div class="txt-title sectionsub-margin1" style="word-break:break-all;"><asp:Literal runat="server" ID="b_title" /></div>
            
                <div class="txt-date">
                    <asp:Literal runat="server" ID="b_regdate" /><span class="bar">|</span><span class="viewcount">조회수</span><asp:Literal runat="server" ID="b_hits" />
                </div>
                <div class="editor-html">
                    <asp:Literal runat="server" ID="b_content" />
                </div>
            
            </div>
        </div>
        
	    <div class="wrap-bt mb30"><a style="width:100%" class="bt-type5" href="#" runat="server" id="btnList">목록</a></div>    


    </section>
    <!--// sub body -->
    
</asp:Content>