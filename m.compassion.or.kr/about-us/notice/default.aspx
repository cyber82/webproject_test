﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="about_us_notice_default" MasterPageFile="~/main.Master" %>

<%@ MasterType VirtualPath="~/main.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">
</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/about-us/notice/default.js"></script>
    <script type="text/javascript">
        $(function () {
            if (cookie.get("cps.app")) {
                $(".appPageNav").find(".back").attr("href", "javascript:history.back(-1);");
            }
        })
    </script>
</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <!-- sub body -->
    <section class="wrap-sectionsub" ng-app="cps" ng-cloak ng-controller="defaultCtrl">

        <div class="about-notice">

            <fieldset class="frm-search mt30 mb10">
                <legend>검색입력</legend>
                <input type="text" value="" name="k_word" id="k_word" placeholder="검색어를 입력해 주세요" ng-enter="search()" />
                <span class="search" ng-click="search()">검색</span>
            </fieldset>

            <div class="wrap-board">

                <ul class="list-board" id="l">
                    <li ng-repeat="item in list" ng-click="goView(item.b_id)">
                        <a class="box-block" href="#">
                            <strong class="txt-title">
                                <!--새로운 글 경우만  "crop-new"  css 사용바랍니다.-->
                                <span class="textcrop-1row" style="display:block;width:95%;">{{item.b_title}}</span>
                                <!--<span class="new"></span>-->
                            </strong>
                            <span class="txt-date">{{parseDate(item.b_regdate) | date:'yyyy.MM.dd'}}
								<span class="bar">|</span>
                                <span class="txt-viewcount">{{item.b_hits}}</span>
                            </span>
                        </a>
                    </li>

                    <!--결과없음-->
                    <li class="no-result" ng-if="resNone">
                        <strong class="txt-title">검색 결과가 없습니다.</strong>
                    </li>
                    <!--//-->

                    <!-- 게시글 없을때 -->
                    <li class="no-content" ng-if="total == 0 && !resNone">
                        <strong class="txt-title">등록된 글이 없습니다.</strong>
                    </li>
                    <!--//-->
                </ul>

            </div>

            <!-- page navigation -->
            <div class="page-count-indicator mb30">
                <paging class="small" page="params.page" page-size="params.rowsPerPage" total="total" show-prev-next="true" show-first-last="true" paging-action="getList({page : page})"></paging>
            </div>
            <!--// page navigation -->
        </div>

    </section>

</asp:Content>
