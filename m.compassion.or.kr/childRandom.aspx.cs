﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CommonLib;
using Newtonsoft.Json.Linq;
using System.Configuration;

public partial class childRandom : MobileFrontBasePage
{
    protected override void OnBeforePostBack()
    {
        var today = DateTime.Now.ToString("yyyyMMdd");
        var action = new ChildAction();

        #region 생일이 어제인 어린이
        {

            var yesterday = DateTime.Now.AddDays(-1);
            var month = Convert.ToInt32(yesterday.ToString("MM"));
            var day = Convert.ToInt32(yesterday.ToString("dd"));
            /*
			var exist = false;
			if (Application["main_visual_child_type1"] != null) {
				var data = (KeyValuePair<string, object>)Application["main_visual_child_type1"];
				if (data.Key == today) {
					exist = true;
				} else {
					Application["main_visual_child_type1"] = null;
				}
			}

			if(!exist) {
				var actionResult = action.GetChildren(null, null, month, day, null, 20, null, null, 1, 3, "new", -1, -1);
				if(actionResult.success) {
					Random rnd = new Random();
					var items = (List<ChildAction.ChildItem>)actionResult.data;
					var index = rnd.Next(items.Count);

					var item = items[index];
					var data = new KeyValuePair<string, object>(today, item);
					Application["main_visual_child_type1"] = data;
				}
			}
			
			if(Application["main_visual_child_type1"] != null) {
				var data = (KeyValuePair<string, object>)Application["main_visual_child_type1"];
				var item = (ChildAction.ChildItem)data.Value;

				//main_recmd1_pic.Style["background"] = "url('"+ item.Pic +"') no-repeat";

				main_recmd1_country.Text = main_recmd1_country2.Text = item.CountryName;
				main_recmd1_age.Text = main_recmd1_age2.Text = item.Age.ToString();
				main_recmd1_gender.Text = item.Gender;
				main_recmd1_waitingdays.Text = item.WaitingDays.ToString();
				main_recmd1_name.Text = main_recmd1_name2.Text = item.Name;
				main_recmd1_link.HRef = main_recmd1_link.HRef.Replace("{childMasterId}", item.ChildMasterId);

			}
			*/

            var actionResult = new JsonWriter();
            if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
            {
                actionResult = action.GetChildren(null, null, month, day, null, 20, null, null, 1, 3, "new", -1, -1);
            }
            else if (ConfigurationManager.AppSettings["dbgp_kind"] == "2")
            {
                actionResult = action.GetChildrenGp(null, null, month, day, null, 20, null, null, 1, 3, "new", -1, -1);
            }

            if (actionResult.success)
            {
                Random rnd = new Random();
                var items = (List<ChildAction.ChildItem>)actionResult.data;

                if (items.Count < 1)
                {
                    if (ConfigurationManager.AppSettings["dbgp_kind"] == "1")
                    {
                        actionResult = action.GetChildren(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);
                    }
                    else if (ConfigurationManager.AppSettings["dbgp_kind"] == "2")
                    {
                        actionResult = action.GetChildrenGp(null, null, null, null, null, 20, null, null, 1, 10, "waiting", -1, -1);
                    }
                    if (actionResult.success)
                    {
                        items = (List<ChildAction.ChildItem>)actionResult.data;
                    }
                }
                else
                {
                    
                }

                if (items.Count > 0)
                {
                    var index = rnd.Next(items.Count);

                    var item = items[index];

                    main_recmd1_pic.Style["background"] = "url('" + item.Pic + "') no-repeat";
                    main_recmd1_country.Text = item.CountryName;
                    main_recmd1_age.Text = item.Age.ToString();
                    main_recmd1_gender.Text = item.Gender;
                    main_recmd1_name.Text = item.Name;
                    main_recmd1_link.HRef = main_recmd1_link.HRef.Replace("{childMasterId}", item.ChildMasterId);
                }
                else
                {
                    ph_child_birthday.Visible = false;
                }
            }
            else
            {
                ph_child_birthday.Visible = false;
            }

        }
        #endregion

        /* 이벤트 정보 등록 -- 20170327 이정길 */
        var eventID = Request["e_id"].EmptyIfNull();
        var eventSrc = Request["e_src"].EmptyIfNull();

        if (!String.IsNullOrEmpty(eventID))
        {
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }
        else if (Request.Cookies["e_id"] != null)
        {
            eventID = Request.Cookies["e_id"].Value;
            eventSrc = Request.Cookies["e_src"].Value;
            Session["eventID"] = eventID;
            Session["eventSrc"] = eventSrc;
        }

        Response.Cookies["e_id"].Value = eventID;
        Response.Cookies["e_src"].Value = eventSrc;
        Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));

        //김태형 2017.06.01 
        if (Request.Url.Host.IndexOf('.') != -1)
        {
            Response.Cookies["e_id"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
            Response.Cookies["e_src"].Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf('.'));
        }
        else
        {
            Response.Cookies["e_id"].Domain = "localhost";
            Response.Cookies["e_src"].Domain = "localhost";
        }
    }
}