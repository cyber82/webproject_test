﻿$(function () {
	
	$(".header").addClass("type-main");

	mainVisual.init();


});

var mainVisual = {
	animating: false,

	init: function () {

		var beforeY = -1;
		$(".intro_visual").bind('touchmove', function (event) {
			event.preventDefault();

			if (mainVisual.animating) return false;

			var e = event.originalEvent;
			var curY = e.targetTouches[0].pageY;
			if (beforeY < 0)
				beforeY = curY;

			if (beforeY > curY + 100) {
				mainVisual.moveToMain();
			}
			return true;

		});

		if (cookie.get("mainVisual")) {
		    //$(".intro_visual").unbind('touchmove');
		    popupManager.openAll(true, "layer", "mobile"); //팝업실행
		} else {
			$(".intro_visual").show();
		}


		$(".intro_visual .scroll").click(function () {
			mainVisual.moveToMain();
			return false;
		}).css("cursor", "pointer");


		var swiper = new Swiper('.swiper-container', {
			pagination: '.swiper-container .page-count',
			paginationClickable: true,
			nextButton: '.swiper-container .page-next',
			prevButton: '.swiper-container .page-prev',
			paginationBulletRender: function (index, className) {
				return '<span class="' + className + '">' + (index + 1) + '</span>';
			},
			spaceBetween: 30
		});


		var swiper2 = new Swiper('.swiper-container2', {
			pagination: '.swiper-container2 .page-count',
			nextButton: '.swiper-container2 .page-next',
			prevButton: '.swiper-container2 .page-prev',
			spaceBetween: 30
		});

	},

	moveToMain: function () {
	    popupManager.openAll(true, "layer", "mobile"); //팝업실행
		cookie.set("mainVisual", 1);
		if (mainVisual.animating) return;

		mainVisual.animating = true;
		var top = $(".header").offset().top;
		$("body,html").animate({ scrollTop: top }, 1000, "easeInOutQuint", function () {

			mainVisual.animating = false;
			$(".intro_visual").unbind('touchmove');

			//$(".intro_visual").fadeOut(function () {
			//	

			//})
			//$(".intro_visual").hide();
			//	
		    //	
			
			$(".intro_visual").css({ background: "url('')" });
			$(".intro_visual").css({ "background-color": "transparent" });
			$(".intro_visual").empty();
			
			setTimeout(function () {
				//$(".intro_visual").animate({ opacity: 0}, 100, function () {
				$(".intro_visual").remove();
				$("body,html").scrollTop(0);
				//	})

				//	
				//$("body,html").scrollTop(0);	
				//	scrollTo($("body,html") );
                
			}, 100);
			


		});
	}
};

