﻿<%@ WebHandler Language="C#" Class="DownloadHandler" %>
using System;
using System.Web;
using System.IO;
using System.Data;
using System.Linq;
using NLog;
using System.Collections.Generic;
using CommonLib;

public class DownloadHandler : IHttpHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public void ProcessRequest (HttpContext context) {
        string path = context.Request["path"];

        int idx;
        if (int.TryParse(path.Split('.')[path.Split('.').Length - 1], out idx)) {
            /*
			try {
				using (AdminDataContext dao = new AdminDataContext()) {
					var entity = dao.files.First(p => p.idx == idx);
					var obj = new file_download_count();
					obj.f_group = entity.f_group;
					obj.f_obj_id = entity.obj_id;
					obj.f_idx = entity.idx;
					obj.fd_regdate = DateTime.Now;
					obj.fd_ymd = DateTime.Now.ToString("yyyyMMdd");
					dao.file_download_counts.InsertOnSubmit(obj);
					dao.SubmitChanges();
				}
			}catch(Exception ex){
				Logger.Warn(ex.Message);	
			}
			path = path.Substring(0, path.LastIndexOf('.'));
			//HttpContext.Current.Response.Write("<script>alert(\""+path+"\");history.back();</script>");
			*/
        }

        path = context.Server.MapPath(path);
        if (!File.Exists(path)){
            HttpContext.Current.Response.Write ( "<script>alert(\"File Not Found\");history.back();</script>" );
            return;
        }

        foreach (var ext in new List<string> { ".js", ".aspx", ".config", ".html", ".css" }) {
            if (path.IndexOf(ext) > -1) {
                HttpContext.Current.Response.Write("<script>alert(\"File Not Found.(500)\");history.back();</script>");
                return;
            }
        }

        FileStream inStr = null;
        byte[] buffer = new byte[1024];
        long byteCount;
        try
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
            HttpContext.Current.Response.Buffer = false;
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(Path.GetFileName(path)));

            inStr = new FileStream(path,FileMode.Open,FileAccess.Read,FileShare.Read,1024,false);
            while ((byteCount = inStr.Read(buffer, 0, buffer.Length)) > 0){
                if (HttpContext.Current.Response.IsClientConnected){
                    HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
                    HttpContext.Current.Response.Flush();
                }else{
                    break;
                }
            }
        }
        catch (Exception)
        {
            //throw ex;
        }
        finally
        {
            if (inStr != null) inStr.Close();
            HttpContext.Current.Response.End();
        }
    }

    public bool IsReusable {
        get {return false;}
    }


}