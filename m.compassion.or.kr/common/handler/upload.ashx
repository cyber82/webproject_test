﻿<%@ WebHandler Language="C#" Class="common_handler_upload" %>

using System;
using System.Web;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;

public class common_handler_upload : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {
        HttpPostedFile file = context.Request.Files[0];

        if (file != null && file.ContentLength > 0) {

            bool rename = context.Request["rename"].ValueIfNull("y") == "y";
            string path = context.Request["fileDir"];
            int limit = Convert.ToInt32(context.Request["limit"].ValueIfNull("2048"));
            string fileType = context.Request["fileType"].EmptyIfNull();
            string tempDir = ConfigurationManager.AppSettings["temp"];
            string fileName = file.FileName.ToLower();

            Uploader.UploadResult result = new Uploader.UploadResult();

            if (file.ContentLength / 1024 > limit) {
                result.success = false;
                result.msg = string.Format("최대 {0}kb까지 업로드 가능합니다. 업로드 파일 {1}kb" , limit , file.ContentLength / 1024);
                JsonWriter.Write(result, context);
                context.Response.ContentType = "text/html";
                return;
            }

            if (fileType == "image") {
                if (fileName.IndexOf(".jpg") < 0 && fileName.IndexOf(".jpeg") < 0 && fileName.IndexOf(".gif") < 0 && fileName.IndexOf(".png") < 0) {

                    result.success = false;
                    result.msg = "jpg , jpeg , gif , png 확장자만 업로드 가능합니다.";
                    JsonWriter.Write(result, context);
                    context.Response.ContentType = "text/html";
                    return;
                }
            }else if(fileType == "movie") {
                 if (fileName.IndexOf(".zip") < 0 && fileName.IndexOf(".mp4") < 0 && fileName.IndexOf(".wmv") < 0 && fileName.IndexOf(".avi") < 0) {

                    result.success = false;
                    result.msg = "ZIP , MP4, WMV , AVI 확장자만 업로드 가능합니다.";
                    JsonWriter.Write(result, context);
                    context.Response.ContentType = "text/html";
                    return;
                }
            }

            result = new Uploader().Upload(context, rename, path, file);

            JsonWriter.Write("<html><head><script>document.domain = '"+ context.Request.Url.Host +"';</script></head><body>"  +result.ToJson() + "</body></html>", context);
            context.Response.ContentType = "text/html";

        }
    }


}