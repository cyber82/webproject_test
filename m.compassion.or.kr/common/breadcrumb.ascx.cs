﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Common
{
    public partial class breadcrumb : System.Web.UI.UserControl
    {

        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                if (value)
                {

                    ShowMenu();

                }
                base.Visible = value;
            }
        }

        void ShowMenu()
        {
            SiteMapNode currentNode;
            var isApp = AppSession.HasCookie(this.Context);

            if (isApp)
            {
                app.Visible = true;
                currentNode = SiteMap.Providers["app"].CurrentNode;
            }
            else
            {
                mobileweb.Visible = true;
                currentNode = SiteMap.Providers["mobileweb"].CurrentNode;
            }

            if (currentNode != null)
            {

                sitemap_resourcekey.Value = currentNode.ResourceKey + "|";
                title.InnerText = title2.InnerText = title3.InnerText = title4.InnerText = title5.InnerText = title6.InnerText = currentNode.Title;

                if (currentNode.ParentNode == null)
                {
                    mobileweb.Visible = false;
                }
                else
                {

                    currentNode = currentNode.ParentNode;
                    sitemap_resourcekey.Value = currentNode.ResourceKey + "|" + sitemap_resourcekey.Value;
                    link.HRef = link3.HRef = link4.HRef = link5.HRef = link6.HRef = link7.HRef = currentNode.Url;

                    if (currentNode.ParentNode != null)
                    {
                        currentNode = currentNode.ParentNode;
                        sitemap_resourcekey.Value = currentNode.ResourceKey + "|" + sitemap_resourcekey.Value;

                        if (currentNode.ParentNode != null)
                        {
                            currentNode = currentNode.ParentNode;
                            sitemap_resourcekey.Value = currentNode.ResourceKey + "|" + sitemap_resourcekey.Value;

                        }
                    }
                }
            }

            // 편지 어린이 목록인경우 제목에 기능이 있음
            if (Request.Path.StartsWith("/my/letter/child"))
            {
                title2.InnerHtml = string.Format("<a id='btn_title'>{0}</a>", Request["name"].EmptyIfNull());
            }
            else if (Request.Path.StartsWith("/my/letter/default"))
            {
                title2.InnerHtml = string.Format("<a id='btn_title'>{0}</a>", "편지");
            }
            else if (Request.Path.StartsWith("/my/letter/view"))
            {
                title2.InnerHtml = Request["name"];
                if (Request["direction"] == "send")
                {
                    title2.InnerHtml = "보낸 편지";
                }
            }

        }

        protected override void OnLoad(EventArgs e)
        {

            base.OnLoad(e);


        }



    }
}