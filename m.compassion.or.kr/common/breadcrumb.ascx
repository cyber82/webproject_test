﻿<%@ Control Language="C#" AutoEventWireup="true" codefile="breadcrumb.ascx.cs" Inherits="Common.breadcrumb" %>
<%@OutputCache Duration="36000" varybyparam="none" varybycustom="url"  %>
<asp:PlaceHolder runat="server" ID="mobileweb" Visible="false">
    
	<script type="text/javascript">
	
		$(function () {

			var isShowMenu = location.pathname.startsWith("/my/");
			if (isShowMenu) {
				$(".mymenu").show();
			}

			$("div.title-depth .mymenu").click(function () {
				$(".sub-mymenu").toggle();
				return false;
			})

			if (location.pathname.startsWith("/my/")) {

				var tpl = $("#mymenu_tpl").text();
				$(".wrap-sectionsub-my").prepend(tpl);
			}
            
			if (location.pathname.startsWith("/store/")) {
			    var store_menu = $("#store_menu").text();
			    $(".wrap-sectionsub").prepend(store_menu);
			   
			}

			if (location.pathname.startsWith("/store/notice")) {
			    $(".store_nav").removeClass("selected");
			    $("[data-id=notice]").addClass("selected");

			} else if (location.pathname.startsWith("/store/review")) {
			    $(".store_nav").removeClass("selected");
			    $("[data-id=review]").addClass("selected");
			} else if (location.pathname.startsWith("/store/complete") || location.pathname.startsWith("/store/fail") || location.pathname.startsWith("/store/order") || location.pathname.startsWith("/store/cart")) {
			    $(".store_nav").removeClass("selected");
			    $("[data-id=cart]").addClass("selected");
			}
		});
	
	</script>
    <textarea id="store_menu" style="display: none">
        <div class="menu-store sectionsub-margin1" >
            <ul style="position: absolute; left: 0px; top: 0; width: 100%">
                <li style="display: inline-block; width: 30%"><a class="store_nav selected" href="/store/">컴패션스토어</a></li>
                <li style="display: inline-block; width: 22%"><a class="store_nav" data-id="cart" href="/store/cart/">장바구니</a></li>
                <li style="display: inline-block; width: 22%"><a class="store_nav" data-id="review" href="/store/review/">후기/문의</a></li>
                <li style="display: inline-block; width: 22%"><a class="store_nav" data-id="notice" href="/store/notice/">공지사항</a></li>
            </ul>
        </div>
    </textarea>

	 <div class="title-depth">
		<h2 runat="server" id="title"></h2>
		 <a class="mymenu" style="display:none" href="#">마이메뉴</a>
		<a class="backgo" runat="server" id="link" href="#">뒤로가기</a>
	</div>
	<script>
		$("a.backgo").click(function () {
			if ($("#btnList").length > 0) {

				location.href = $("#btnList").attr("href");
				return false;
			} else {
				return true;
			}

		})
	</script>

	<textarea id="mymenu_tpl" style="display:none">
		<div class="sub-mymenu">
    		<ul>
				<li class="i-home"><a href="/my/">마이컴패션 홈</a></li>
				<li class="i-child"><a href="/my/children/">나의 어린이</a></li>
				<li class="i-sponsor"><a href="/my/sponsor/commitment/">후원 관리</a></li>
				<li class="i-funding"><a href="/my/user-funding/join">나의 펀딩 관리</a></li>
				<li class="i-active"><a href="/my/activity/CAD">나의 참여/활동</a></li>
				<li class="i-oderlist"><a href="/my/store/order/">스토어 주문내역</a></li>
				<li class="i-faqlist"><a href="/my/qna/">문의내역</a></li>
			</ul>
		</div>
	</textarea>
</asp:PlaceHolder>

<!-- app -->
<asp:PlaceHolder runat="server" ID="app" Visible="false">
    <style>
        .letter_prev {
            width: 15px;
            height: 15px;
            background-size: cover;
            text-indent: -9999px;
            background: url(/common/img/btn/prev_5.png) no-repeat;
            margin-right: 5px;
            font-family: 'noto_d';
            border: 0;
            cursor: pointer;
            outline-style: none;
            background-size: cover;
        }
        .letter_next {
            width: 15px;
            height: 15px;
            background-size: cover;
            text-indent: -9999px;
            background: url(/common/img/btn/next_5.png) no-repeat;
            margin-left: 5px;
            font-family: 'noto_d';
            border: 0;
            cursor: pointer;
            outline-style: none;
            background-size: cover;
        }
    </style>
    <script type="text/javascript" src="/assets/sly/sly.min.js"></script>
  
	<script type="text/javascript">
		
		$(function () {

			if ($("#is_login").val() == "N") {
				$(".loginAction").hide();
			}

			// 새창으로 열경우 열린 팝업에서 back 버튼 클릭시 새창 닫기
			var app_ex = getParameterByName("app_ex");
			if (app_ex == "2") {
				$("header").hide();
				$(".appTabBar").hide();
				return;
			}

			$("section.appPageNav .back").click(function () {
				
				if (customBackAction) return;
				
				if ($("#btnList").length > 0 && $("#btnList").attr("href") != "#") {
				//	alert("1 > " + $("#btnList").attr("href"))
					location.href = $("#btnList").attr("href");
					return false;
				} else {
			//		alert("2 > " + $("section.appPageNav .back").attr("href"))
					return true;
				}
			})
		
			// 섹션 root 페이지인경우
			var roots = ["/sponsor/children/", "/sponsor/user-funding/", "/sponsor/special/", "/app/children/", "/participation/event/"];
			var subs = ["/my/letter/write", "/my/letter/write-pic", "/my/letter/temp", "/my/letter/letter-video", "/my/letter/ready", "/my/letter/update", "/my/letter/update-pic", "/my/letter/complete", "/my/letter/smart-letter/introduce", "/my/letter/smart-letter/apply"];

			if ($.grep(roots, function (r) {
				return r == location.pathname;
			}).length > 0) {
			    $("section.appPageNav[data-type=root]").show();
			    

				//읽지 않은 메세지
				hybrid_api.getNewPushCount();

			} else if ($.grep(subs, function (r) {
				return location.pathname.startsWith(r);
			}).length > 0) {
				$("section.appPageNav[data-type=sub]").show();
			}else if(location.pathname.startsWith("/app/main")) {
				$("section.appPageNav[data-type=main]").show();

				//읽지 않은 메세지
				hybrid_api.getNewPushCount();

			} else if (location.pathname.startsWith("/app/push/")) {
				$("section.appPageNav[data-type=push]").show();
			} else if (location.pathname.startsWith("/app/more")) {
				$("section.appPageNav[data-type=more]").show();

			} else if (location.pathname.startsWith("/my/letter/view/")) {
				var direction = getParameterByName("direction");
				if (direction == "send") {		// 보낸 편지의 경우
					$(".header_title").text("보낸 편지");
				} else if (direction == "receive") {		// 받은 편지의 경우
				    $(".header_title").html('<span class="bluetxt">' + getParameterByName("name") + "</span>의 편지");

				    var strPrevNext = '';
				    strPrevNext += '<button class="letter_prev" onclick="mSwiper.slidePrev();">이전</button>';
				    strPrevNext += '<span class="fc_blue" style="font-size:12pt; color:#005dab !important;"><span class="headerLetterDirecionCurPage"></span></span><span style="font-size:12pt;">&nbsp;/ <span class="headerLetterDirecionTotalPage"></span></span>';
				    strPrevNext += '<button class="letter_next" onclick="mSwiper.slideNext();">다음</button>';
				    $('.prevnext').html(strPrevNext);
				    
				}
				$("section.appPageNav[data-type=sub]").show();

			} else if (location.pathname.startsWith("/my/letter/child/")) {

				//읽지 않은 메세지
				hybrid_api.getNewPushCount();

				$(".header_title").text(getParameterByName("name"));

				if (getParameterByName("header") == "root") {		// 대상 어린이가 1명인경우 
					$("section.appPageNav[data-type=letter_child_root]").show();

					//읽지 않은 메세지
					hybrid_api.getNewPushCount();

				} else {
					$("section.appPageNav[data-type=letter_child]").show();
				}

			} else if (location.pathname.startsWith("/my/letter/")) {
				$("section.appPageNav[data-type=letter]").show();

				//읽지 않은 메세지
				hybrid_api.getNewPushCount();

			} else if (location.pathname.startsWith("/app/my") || location.pathname.startsWith("/my/")) {
				$("section.appPageNav[data-type=my]").show();
			} else if (location.pathname.startsWith("/app/children/view/") && getParameterByName("header") == "root") {
				$("section.appPageNav[data-type=children_root]").show();

				//읽지 않은 메세지
				hybrid_api.getNewPushCount();
			} else if (location.pathname.startsWith("/app/children/nodata") ) {
				$("section.appPageNav[data-type=children_root]").show();

				//읽지 않은 메세지
				hybrid_api.getNewPushCount();

			} else {
				$("section.appPageNav[data-type=sub]").show();
			}

			// 후원메인인경우 
			var isSponsorMain = location.pathname == "/sponsor/children/" || location.pathname == "/sponsor/user-funding/" || location.pathname == "/sponsor/special/";
			if (isSponsorMain) {
				var tpl = $("#sponsor_tpl").text();
				$(".wrap-sectionsub").prepend(tpl);
				$(".sponsor_submenu").removeClass("selected");
				$(".sponsor_submenu[href='" + location.pathname + "']").addClass("selected");

				var sly = new Sly($("#slide"), {    // Call Sly on frame

				    horizontal: 1,
				    itemNav: 'centered',
				    smart: 1,
				    activateOn: 'click',
				    mouseDragging: 1,
				    touchDragging: 1,
				    releaseSwing: 1,
				    startAt: 0,
				    scrollBy: 1,
				    activatePageOn: 'click',
				    speed: 300,
				    elasticBounds: 1,
				    easing: 'easeOutExpo',
				    dragHandle: 1,
				    dynamicHandle: 1,
				    clickBar: 1

				});

				sly.init();
				$.each($("#slide a"), function (i) {
				    if ($(this).hasClass("selected")) {
				        sly.activate(i);
				    }
				})

				setTimeout(function () {
				    if ($("#slide_menu").width() < $("#slide").width()) {
				        $("#slide_menu").css("left", "4%");
				    }

				}, 10);
			}


			// 마이페이지 경우
			var isMyPage = (location.pathname.startsWith("/app/my") || location.pathname.startsWith("/my/")) && !location.pathname.startsWith("/my/letter");
			if (isMyPage) {
				var tpl = $("#mymenu_tpl").text();
				$(".wrap-sectionsub-my").prepend(tpl);

				$(".wrap-sectionsub-my").find(".i-child").remove();

			}

			$("section.appPageNav .mymenu").click(function () {
				$(".sub-mymenu").toggle();
				return false;
			})

			
			
		})
	
    </script>
	
	<section class="appPageNav" style="display:none" data-type="main">
		<div class="left"><a class="msg loginAction" href="/app/push/"><span class="unread_push" style="display:none">99+</span></a></div>	<!--- 메시지알림 배지 <span>99+</span> 추가 --->
		<div class="center"><img class="logo" src="/common/img/app/logo.png" alt="Compassion" /></div>
		<div class="right"><a class="mypage loginAction" href="/app/my/"></a></div>
	</section>

	<section class="appPageNav" style="display:none" data-type="letter">
		<div class="left"><a class="msg loginAction" href="/app/push/"><span class="unread_push" style="display:none">99+</span></a></div>
		<div class="center">
			<span><span class="txt">편지</span><span class="arr"></span></span></div>
		<div class="right"><a class="mypage loginAction" href="/app/my/"></a></div>
	</section>

	<section class="appPageNav" style="display:none" data-type="letter_child_root">
		<div class="left"><a class="msg loginAction" href="/app/push/"><span class="unread_push" style="display:none">99+</span></a></div>
		<div class="center"><span><span class="txt header_title"></span><span class="arr"></span></span></div>
		<div class="right"><a class="mypage loginAction" href="/app/my/"></a></div>
	</section>

	<section class="appPageNav" style="display:none" data-type="letter_child">
		<div class="left"><a class="back backgo" runat="server" id="link6"  href="#"></a></div>
		<div class="center"><span><span class="txt header_title"></span><span class="arr"></span></span></div>
	</section>

	<section class="appPageNav"  style="display:none" data-type="root">
        <div class="left"><a class="msg loginAction" href="/app/push/"><span class="unread_push" style="display:none">99+</span></a></div>	<!--- 메시지알림 배지 <span>99+</span> 추가 --->
		<div class="left" style="display:none"><a class="back backgo" runat="server" id="link7"  href="#"></a></div>
        <!--
		<div class="left"><a class="msg loginAction" href="/app/push/"><span class="unread_push" style="display:none">99+</span></a></div>
        -->
		<div class="center"><span class="txt" runat="server" id="title2"></span></div>
		<div class="right"><a class="mypage loginAction" href="/app/my/"></a></div>
	</section>

	<section class="appPageNav" style="display:none" data-type="my">
		<div class="left"><a class="back" runat="server" id="link3"  href="#"></a></div>
		<div class="center"><span class="txt" runat="server" id="title3"></span></div>
		<div class="right"><a class="menu mymenu loginAction" href="#"></a></div>
	</section>

	<section class="appPageNav"  style="display:none" data-type="push">
		<div class="left"><a class="back" runat="server" id="link4"  href="#"></a></div>
		<div class="center"><span class="txt" runat="server" id="title4"></span></div>

		<div class="right"><a class="alarm loginAction" href="/app/setting"></a></div>
	</section>

	<section class="appPageNav" style="display:none" data-type="children_root">
		<div class="left"><a class="msg loginAction" href="/app/push/"><span class="unread_push" style="display:none">99+</span></a></div>
		<div class="center"><span class="txt header_title" runat="server" id="title6"></span></div>
		<div class="right"><a class="mypage loginAction" href="/app/my/"></a></div>
	</section>

	<section class="appPageNav"  style="display:none" data-type="sub">
		<div class="left"><a class="back backgo" runat="server" id="link5"  ></a></div>
		<div class="center"><span class="txt header_title" runat="server" id="title5"></span></div>
        <div class="right"><div class="prevnext"></div></div>
	</section>

	<section class="appPageNav"  style="display:none" data-type="more">
		<div class="left"><a class="setting" href="/app/setting"></a></div>
		<div class="center"><span class="txt">더보기</span></div>
		<div class="right"><a class="mypage" href="/app/my/"></a></div>
	</section>

	<textarea id="sponsor_tpl" style="display:none">
		<div class="menu-sympathy sectionsub-margin1" style="position: relative; overflow: hidden;" id="slide">
			<ul style="position:absolute" id="slide_menu">
				<li style="display: inline-block; width:130px" id="sp_ch"><a style="padding-left:5px;padding-right:5px" class="sponsor_submenu selected" href="/sponsor/children/">1:1어린이양육</a></li>
				<li style="display: inline-block; width:120px" id="sp_spc"><a class="sponsor_submenu" href="/sponsor/special/">특별한 나눔</a></li>
				<li style="display: inline-block; width:100px" id="sp_uf"><a class="sponsor_submenu" href="/sponsor/user-funding/">나눔펀딩</a></li>
                <li style="display: inline-block;"></li>
                
			</ul>
		</div>

	</textarea>

	<textarea id="mymenu_tpl" style="display:none">
		<div class="sub-mymenu">
    		<ul>
				<li class="i-home"><a href="/app/my">마이컴패션 홈</a></li>
				<li class="i-child"><a href="/my/children/">나의 어린이</a></li>
				<li class="i-sponsor"><a href="/my/sponsor/commitment/">후원 관리</a></li>
				<li class="i-funding"><a href="/my/user-funding/join">나의 펀딩 관리</a></li>
				<li class="i-active"><a href="/my/activity/CAD">나의 참여/활동</a></li>
				<li class="i-oderlist"><a href="/my/store/order/">스토어 주문내역</a></li>
				<li class="i-faqlist"><a href="/my/qna/">문의내역</a></li>
			</ul>
		</div>
	</textarea>



</asp:PlaceHolder>

<input type="hidden" runat="server" id="sitemap_resourcekey" />
