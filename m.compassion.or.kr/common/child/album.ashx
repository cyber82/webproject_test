﻿<%@ WebHandler Language="C#" Class="common_child_album_ashx" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;
using System.Drawing;



public class common_child_album_ashx : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();
        if(t == "download") {
            Download(context);
        }
    }


    void Download( HttpContext context ) {
        var data = context.Request["data"].Replace("data:image/jpeg;base64," , "");
        var filename = context.Request["filename"];
        string tempDir = ConfigurationManager.AppSettings["image_user_child"];
        string temp_file = context.Server.MapPath(tempDir) + filename;

        try {

            byte[] byteArray = Convert.FromBase64String(data);
            using (var ms = new MemoryStream(byteArray))
            {
                Image img = Image.FromStream(ms);
                img.Save(temp_file);
            }

            JsonWriter result = new JsonWriter() {
                success = true,
                data = tempDir + filename,
                message = filename
            };

            result.Write(context);

        } catch(Exception ex) {

            ErrorLog.Write(context, 0, ex.ToString());
        }


    }
}