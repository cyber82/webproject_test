﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="album.aspx.cs" Inherits="common_child_album"  %>

<div style="background:transparent;width:100%;" class="fn_pop_container" id="popAlbum">
	<!--레이어 안내 팝업-->
	<div class="wrap-layerpop" style="width:80%;left:10%;">
        <div class="header">
        	<p class="txt-title">성장앨범</p>
        </div>
        <div class="contents">
            
            <div class="my-mychild">
            	<time class="txt-time" datetime="{{modalAlbum.images[modalAlbum.index].date}}">{{modalAlbum.images[modalAlbum.index].date}}</time>
                
                <div class="wrap-slider">
                    <div style="width:200px;height:205px;overflow:hidden;position:relative;margin:0 auto">   <!--임시-->
                    	<ul style="position:absolute;left:0;top:0;width:100%">   <!--임시-->
                        	<li style="float:left" ng-repeat="item in modalAlbum.images" ng-show="$index == modalAlbum.index"><span class="photo" style="background:url('{{item.image}}') no-repeat center !important; background-size:contain !important" ></span></li>
                        </ul>
                    </div>
                    
                    <div class="page-prev" style="left:0" ng-click="modalAlbum.prev()" ng-show="modalAlbum.index > 0">이전보기</div>
                    <div class="page-next" style="right:0" ng-click="modalAlbum.next()" ng-show="modalAlbum.index +1 < modalAlbum.images.length">다음보기</div>
                </div>
               
                <div class="wrap-bt"><a class="bt-type6" style="width:140px" ng-show="downloadable" ng-click="modalAlbum.download($event)">다운로드</a></div>
				
            </div>
            
        </div>
        <div class="close"><span class="ic-close" ng-click="modalAlbum.close($event)">레이어 닫기</span></div>
    </div>
    
	<input type="hidden" runat="server" id="album_data" />
<script src="/assets/download/download.js"></script>
	<script type="text/javascript">
	
			var activateAlbum = function ($scope, childkey , modal) {
	
				$scope.downloadable = true;
				var app = cookie.get("cps.app");
				if (app){
					var entity = $.parseJSON(app);
					var appDevice = entity.device;
					if (appDevice == "iphone") {
					//	$scope.downloadable = false;
					}
				}
				

				$scope.modalAlbum = {
			
					images : $.parseJSON($("#album_data").val()) , 
					index: 0,
					modal : modal , 

					close : function($event){
						$event.preventDefault();
						$scope.modalAlbum.modal.hide();
 					} , 
					prev: function () {
						if ($scope.modalAlbum.index - 1 < 0)
							return;

						$scope.modalAlbum.index--;
					},

					next: function () {
						if ($scope.modalAlbum.index + 1 >= $scope.modalAlbum.images.length)
							return;

						$scope.modalAlbum.index++;
						
					},

					download: function ($event) {
                        $event.preventDefault();
                        var image = $scope.modalAlbum.images[$scope.modalAlbum.index].image;
                        var param = {
                            data: image,
                            filename: childkey + "_" + ($scope.modalAlbum.index + 1) + ".jpg"
                        }
                        $.post("/common/child/album.ashx?t=download", param).success(function (r) {
                            if (r.success) {
                                if ($("#btn_download").length > 0) {
                                    $("#btn_download").remove();
                                }
                                var btn_download = '<a href="' + r.data + '" download title="' + r.message + '" id="btn_download" style="display:none;width:0px;height:0px;"></a>';
                                $("body").append(btn_download);
                                document.getElementById('btn_download').click();

                                var entity = $.parseJSON(app);
                                if (entity && entity.device == "android") {
                                    location.href = "multi-download:" + getDomain() + r.data;
                                }

                                if (app) {
                                    alert("다운로드 되었습니다.")
                                }
                            }
                        });

                        return;
					}
				};

				loading.hide();
			}

	
		</script>
</div>    

