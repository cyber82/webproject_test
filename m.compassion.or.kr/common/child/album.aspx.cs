﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using System.IO;
using CommonLib;

public partial class common_child_album : MobileFrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}


	protected override void OnBeforePostBack() {

		var requests = Request.GetFriendlyUrlSegments();
		
		if(requests.Count < 1) {
			return;
		}
		
		var childMasterId = requests[0];

        //	WWWService.Service _wwwService = new WWWService.Service();  //WebService
        //	var obj = _wwwService.getChildImageList_New(childMasterId);

        //String sqlText = " SELECT tChildImage_EN.image_file as ChildImage " +
        //"      , tChildImage.ChildImage AS ChildImage_2 " +
        //"      , ChildSmallImage " +
        //"      , Convert(varchar(10), ImageDate, 121) AS CaseStudyDate " +
        //"      FROM " +
        //"      kr_compass4.dbo.tChildMaster CM WITH(NOLOCK) " +
        //"      INNER JOIN   kr_Compass4_image.dbo.tChildImage tChildImage WITH(NOLOCK) ON tChildImage.ChildMasterID = CM.ChildMasterID COLLATE Korean_Wansung_CI_AS " +
        //"      INNER JOIN   compass_image.dbo.need_image tChildImage_EN WITH(NOLOCK) " +
        //"      ON tChildImage_EN.image_date = tChildImage.ImageDate " +
        //"      AND tChildImage_EN.ChildKey = CM.ChildKey  COLLATE Korean_Wansung_CI_AS " +
        //"      WHERE CM.ChildMasterID IS NOT NULL " +
        //"      AND CM.ChildMasterID = '" + childMasterId + "' " +
        //"      ORDER BY ImageDate DESC";

        String sqlText = " SELECT tChildImage.ChildImage as ChildImage  " +
        "      , tChildImage.ChildImage AS ChildImage_2 " +
        "      , ChildSmallImage " +
        "      , Convert(varchar(10), ImageDate, 121) AS CaseStudyDate " +
        "      FROM " +
        "      kr_compass4.dbo.tChildMaster CM WITH(NOLOCK) " +
        "      INNER JOIN   kr_Compass4_image.dbo.tChildImage tChildImage WITH(NOLOCK) ON tChildImage.ChildMasterID = CM.ChildMasterID COLLATE Korean_Wansung_CI_AS " +
        "      WHERE CM.ChildMasterID IS NOT NULL " +
        "      AND(tChildImage.IsDel IS NULL OR tChildImage.IsDel = 0) " +
        "      AND CM.ChildMasterID = '" + childMasterId + "' " +
        "      ORDER BY ImageDate DESC";

        /*
        String sqlText = " SELECT tChildImage_EN.image_file as ChildImage " +
					 "      , tChildImage.ChildImage AS ChildImage_2 " +
					 "      , ChildSmallImage " +
					 "      , Convert(varchar(10), ImageDate, 121) AS CaseStudyDate " +
					 " FROM  kr_Compass4_image.dbo.tChildImage tChildImage WITH (NOLOCK) " +
					 " INNER JOIN   compass_image.dbo.need_image tChildImage_EN WITH (NOLOCK) " +
					 "   ON  tChildImage_EN.image_date = tChildImage.ImageDate " +
                     "  AND  tChildImage_EN.childkey = tChildImage.ChildKey COLLATE Korean_Wansung_CI_AS " +
					 "  WHERE ChildMasterID IS NOT NULL " +
					 "  AND  ChildMasterID = '" + childMasterId + "'" +
					 "  ORDER BY ImageDate DESC ";
                     */
        WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
		Object[] objSql = new object[1] { sqlText };
		DataSet obj = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "Text", null, null);

		if(obj == null) {
			album_data.Value = "[]";
			return;
		}
		var dt = obj.Tables[0];

		List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();

		foreach(DataRow dr in dt.Rows) {
			var item = new Dictionary<string, object>();
			var bytes = (byte[])dr["ChildImage"]; //어린이 이미지
			string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
			string img = "data:image/jpeg;base64," + base64String;
            string date = Convert.ToDateTime(dr["CaseStudyDate"]).ToString("yyyy.MM.dd");
            if (item.ContainsKey(date))
                continue;
            item.Add("date", date);
            item.Add("image", img);
			data.Add(item);
		}

		album_data.Value = data.ToLowerCaseJson();



	}
	
}
