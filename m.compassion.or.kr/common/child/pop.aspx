﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pop.aspx.cs" Inherits="common_child_pop"  %>

<div style="background:transparent;" class="fn_pop_container" id="childModalview">
    

	<div class="wrap-layerpop fn_pop_content" style="width:100%;left:0;">
		<div class="wrap-layerpop">
        <div class="header2">
        	<p class="txt-title">어린이 정보</p>
        </div>
        <div class="contents">
            
            <div class="sponsor-nurture">
            
				<ul class="list-childselect">
                    <li>
                        <span class="wrap-caption">
                            <span class="txt-clock">{{modalChild.item.waitingdays}}일</span>
                            <span class="share" id="btn_sns" runat="server" ng-click="sns.show($event , {url : '/sponsor/children/?c=' +  modalChild.item.childmasterid, title : '[한국컴패션-1:1어린이양육]' + modalChild.item.name , desc : '' , picture : modalChild.item.Pic})">
                                <img src="/common/img/icon/share2.png" alt="sns공유" /></span>
                        </span>
                        <span class="pos-photo"><span class="photo" background-img="{{modalChild.item.pic}}" data-default-image="/common/img/temp/temp1.jpg" style="background:no-repeat"></span></span>
                        <span class="txt-name">{{modalChild.item.name}}</span>
                        <span class="txt-info">
                            국가 : <span class="color1">{{modalChild.item.countryname}}</span><br />
                            생일 : <span class="color1">{{modalChild.item.birthdate | date:'yyyy.MM.dd'}} ({{modalChild.item.age}}세)</span><br />
                            성별 : <span class="color1">{{modalChild.item.gender}}</span>
                        </span>
                    </li>
                </ul>
                
                <p class="txt-childinfo1 mt30">안녕하세요?<br />제 이름은 <strong>{{modalChild.item.namekr}}</strong>입니다.</p>
                <p class="txt-childinfo2 mt15">
					제 나이는 <strong>{{modalChild.item.age}}살</strong>이고요, 생일은 <strong>{{modalChild.item.birthdate | date:'yyyy년 M월 d일'}}</strong>이에요.<br />
                    저는 <strong>{{modalChild.item.countryname}}</strong>에 살아요. <br />
                    <span id="pop_family"></span> <span id="pop_hobby"></span>
					
					<span class="disease" id="pop_disease"><span id="pop_health"></span></span><br />

                    후원자님과 함께 컴패션에서 꿈을 키우고 싶어요.<br />
					저와 함께해 주시겠어요?
                </p>
                <div class="wrap-bt"><a class="bt-type6" style="width:100%" href="#" ng-click="modalChild.goPay($event)" onclick="_nto.callTrack(5577, callback()); " runat="server" id="btn_commitment">결연하기</a></div>
                
                <p class="txt-childinfo1 mt30">지금 {{modalChild.item.countryname}}는(은)요,</p>
                <div class="mt20 map" id="map" style="height:240px;"></div>
                <div class="wrap-weather">
                    <p class="state"><%:ViewState["weather"].ToString() %></p>
                    <p class="temperature"><%:ViewState["temperature"].ToString() %>˚</p>
                    <img src="/common/img/page/sponsor/weather/<%:ViewState["weather_icon"].ToString() %>.png" class="icon" alt="맑음" />
                    <p class="time"><%:ViewState["time"].ToString() %></p>
                </div>

				<asp:Literal runat="server" id="content" />
                
                
            <div>
            
        </div>
        <div class="close2"><span class="ic-close"  ng-click="modalChild.hide($event)">레이어 닫기</span></div>
    </div>
    
</div>    
<!--//레이어 안내 팝업-->




		
	</div>

	
	<script type="text/javascript">
	
	var map;

	function initChildPop($http, $scope, modal, item) {
		console.log("qdqw",item)
		$scope.modalChild = {

			instance: modal,
			item: item ,

			hide: function ($event) {
				$event.preventDefault();
				$scope.modalChild.instance.hide();
			},

			goPay_old1: function ($event) {
				$event.preventDefault();

				$http.get("/sponsor/pay-gateway.ashx?t=go-cdsp", { params: { childMasterId: $scope.modalChild.item.childmasterid } }).success(function (r) {
					if (r.success) {
						alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
						location.href = r.data;
					} else {
						alert(r.message);						
					}
				});
			},

			goPay: function ($event) {
			    $event.preventDefault();
			    //[이종진]로딩추가 
			    var zIndexBg = $("#loading_bg").css("z-index");
			    var zIndexCon = $("#loading_container").css("z-index");
			    $("#loading_bg").css("z-index", "1000000");
			    $("#loading_container").css("z-index", "1000001");
			    loading.show();

			    $http.get("/sponsor/pay-gateway.ashx?t=go-cdsp", {
			        params: {
                        childMasterId: $scope.modalChild.item.childmasterid, childKey: '<%:this.ViewState["childKey"].ToString()%>', Pic: $scope.modalChild.item.pic
                        , IsOrphan: $scope.modalChild.item.isorphan == true ? "Y" : "N", IsHandicapped: $scope.modalChild.item.ishandicapped == true ? "Y" : "N", WaitingDays: $scope.modalChild.item.waitingdays
                        , Age: $scope.modalChild.item.age, BirthDate: $scope.modalChild.item.birthdate, CountryCode: $scope.modalChild.item.countrycode
                        , Gender: $scope.modalChild.item.gender == "남자" ? "M" : "F", HangulName: $scope.modalChild.item.hangulname, HangulPreferredName: $scope.modalChild.item.hangulpreferredname
                        , FullName: $scope.modalChild.item.fullname, PreferredName: $scope.modalChild.item.preferredname

			        }
			    }).success(function (r) {
			        if (r.success) {
			            alert("원활한 결연 진행을 위해 어린이는 1시간 동안만 후원자님께 확인됩니다.");
			            location.href = r.data;
			        } else {
			            alert(r.message);
			        }
			        //[이종진]로딩추가
			        $("#loading_bg").css("z-index", zIndexBg);
			        $("#loading_container").css("z-index", zIndexCon);
			        loading.hide();
			    });
			    
			}


		}

		$scope.$apply();

		setTimeout(function () {
			common.bindSNSAction();
			
		}, 500);

	}

		$(function () {
			setTimeout(function () {
				common.bindSNSAction();
			}, 500);

			appendGoogleMapApi();

			getCaseStudy();
		})

		function getCaseStudy() {
			$.get("/api/tcpt.ashx", { t: "get-casestudy", childKey: '<%:this.ViewState["childKey"].ToString()%>', childMasterId: '<%:this.ViewState["childMasterId"].ToString()%>' }, function (r) {

				loading.hide();

				console.log(r);
				if (r.success) {

					if (r.data.family) {
						$("#pop_family").html("<strong>" + r.data.family + "</strong>께서 저를 돌봐주세요.<br/>");
					}
					if (r.data.hobby) {
						$("#pop_hobby").html("저는 <strong>"+ r.data.hobby +"</strong>(을)를 좋아해요.<br/><br/>");
					}
					if (r.data.health) {
						$("#pop_health").html("(" + r.data.health + ")");
					} else {
					//	$("#pop_health").html("없음");
						if ($("#pop_ishandicapped").val() == "없음") {
					//		$("#pop_disease").hide();
						}
					}

				}
			})
		}

		function appendGoogleMapApi() {
			if (typeof google === 'object' && typeof google.maps === 'object') {
				initMap();
			} else {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.src = "https://maps.googleapis.com/maps/api/js?language=ko&region=kr&key=<%:this.ViewState["googleMapApiKey"].ToString() %>&callback=initMap";
				document.body.appendChild(script);
			}
		}


		function initMap() {

			lat = parseFloat(<%:this.ViewState["lat"].ToString() %>);
			lng = parseFloat(<%:this.ViewState["lng"].ToString() %>);

			map = new google.maps.Map(document.getElementById('map'), {
				scrollwheel: false,
				center: { lat: lat, lng: lng },
				zoom: 5,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});

			
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(lat, lng),
				icon: "/common/img/icon/pin.png",
				map: map
			});
			

		};


	</script>

    <script type="text/javascript"> 

    var _ntp = {}; 

    _ntp.host = (('https:' == document.location.protocol) ? 'https://' : 'http://') 

    _ntp.dID = 779; 

  
    //document.write(unescape("%3Cscript src='" + _ntp.host + "nmt.nsmartad.com/content?cid=1' type='text/javascript'%3E%3C/script%3E"));
  

    var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = unescape(_ntp.host + "nmt.nsmartad.com/content?cid=1");
	document.body.appendChild(script);

</script> 

<script>

 callback = function(){}

</script>

</div>