﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using System.IO;
using CommonLib;

public partial class common_child_pop : MobileFrontBasePage {

	public override bool IgnoreProtocol {
		get {
			return true;
		}
	}


	protected override void OnBeforePostBack() {

		if (Request["fn"].EmptyIfNull() == "hide") {
			btn_sns.Visible = btn_commitment.Visible = false;
		}

		this.ViewState["googleMapApiKey"] = ConfigurationManager.AppSettings["googleMapApiKey"];

		ViewState["weather_icon"] = "";
		ViewState["temperature"] = "";
		ViewState["weather"] = "";
		ViewState["time"] = "";
		var requests = Request.GetFriendlyUrlSegments();
		
		if(requests.Count < 3) {
			return;
		}
		var countryCode = requests[0];
		var childMasterId = requests[1];
		var childKey = requests[2];

		this.ViewState["childMasterId"] = childMasterId;
		this.ViewState["childKey"] = childKey;

		var path = Server.MapPath("/common/child/country-info/" + countryCode + ".html");

		using(StreamReader stream = File.OpenText(path)) {
			content.Text = stream.ReadToEnd();
		}

		var actionResult = new CountryAction().GetInfoById(countryCode);
		if(!actionResult.success) {
			base.AlertWithJavascript(actionResult.message);
			return;
		}

		var entity = (country)actionResult.data;

		ViewState["weather_icon"] = entity.c_weather_icon;
		ViewState["temperature"] = entity.c_temperature.HasValue ? Convert.ToInt32(entity.c_temperature.Value).ToString() : "";
		ViewState["weather"] = entity.c_weather.ToUpper();
		ViewState["lat"] = entity.c_lat.ToString();
		ViewState["lng"] = entity.c_lng.ToString();

		//Response.Write(entity.ToJson());
		if(entity.c_timeoffset.HasValue) {
			ViewState["time"] = DateTime.Now.AddSeconds(entity.c_timeoffset.Value).ToString("tt h:mm").Replace("오전" , "am").Replace("오후", "pm");
			
		}

	}
	
}
