﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cert-template.aspx.cs" Inherits="common_popup_cert_template" %>

<div style="background: #fff; position: absolute; top: 0; left: 0; width: 100%; height: 100%; z-index: 100000" class="fn_pop_container" id="certFrm">
	<div class="wrap-layerpop fn_pop_content" style="width: 100%; left: 0;">
		<div class="wrap-layerpop">
			<div class="header2">
				<p class="txt-title">본인인증</p>
			</div>
			<div class="contents">

				<iframe runat="server" id="iframe" width="375px" height="500px" frameborder="0"></iframe>

			</div>
			<div class="close2"><span class="ic-close">레이어 닫기</span></div>
		</div>

	</div>
	<!--//레이어 안내 팝업-->
		
	<script type="text/javascript">
		
		$("body").bind("touchmove", function (e) {
			e.preventDefault();
		})

		$(".ic-close").click(function () {
			$(".fn_pop_container").fadeOut(function () {
				$(".fn_pop_container").remove();
			})
		})
		
	</script>
</div>