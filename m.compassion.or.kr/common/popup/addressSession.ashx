﻿<%@ WebHandler Language="C#" Class="common_popup_addressSession" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class common_popup_addressSession : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest(HttpContext context) {

		var zipCode = context.Request["zipCode"].EmptyIfNull();
		var addrRoad = context.Request["roadAddrPart1"].EmptyIfNull();
		var reference = context.Request["roadAddrPart2"].EmptyIfNull();
		var nnmz = context.Request["bdMgtSn"].EmptyIfNull();
			var addrJibun = context.Request["addrJibun"].EmptyIfNull();
		var addr2 = context.Request["addr2"].EmptyIfNull();

		DataSet dsAddress = new DataSet();

			dsAddress.Tables.Add();

			dsAddress.Tables[0].Columns.Add("inputZip");
			dsAddress.Tables[0].Columns.Add("inputAddr1");
			dsAddress.Tables[0].Columns.Add("inputAddr2");
			dsAddress.Tables[0].Columns.Add("inputCode");
			dsAddress.Tables[0].Columns.Add("jibunZip");
			dsAddress.Tables[0].Columns.Add("jibunAddr1");
			dsAddress.Tables[0].Columns.Add("jibunAddr2");
			dsAddress.Tables[0].Columns.Add("jibunCode");
			dsAddress.Tables[0].Columns.Add("roadZip");
			dsAddress.Tables[0].Columns.Add("roadAddr1");
			dsAddress.Tables[0].Columns.Add("roadAddr2");
			dsAddress.Tables[0].Columns.Add("roadCode");
			dsAddress.Tables[0].Columns.Add("AddressUsage");
			dsAddress.Tables[0].Columns.Add("Reference");
			dsAddress.Tables[0].Columns.Add("NNMZ");
			dsAddress.Tables[0].Columns.Add("MSG");
			
			dsAddress.Tables[0].Rows.Add(zipCode, addrRoad , addr2, "1",
										 zipCode, addrJibun, addr2, "2",
										 zipCode, addrRoad, addr2, "3",
										 "3", reference, nnmz, "");
			context.Session["dsHouseAddress"] = dsAddress;

		
	}
		
}