﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="share_pop.aspx.cs" Inherits="common_share_pop" %>

<div style="background:transparent; width:100%;" class="fn_pop_container">
	<!--레이어 안내 팝업-->
	
		<div class="wrap-layerpop" style="width:80%;left:10%;">
        <div class="header">
            <p class="txt-title">공유하실 채널을 선택해 주세요</p>
        </div>
        <div class="contents">
            <div class="advocate-compassion">
                <div class="share-channel2">
                    <ul>

                        <li><a data-role="sns" data-provider="fb" data-url="{{sns.args.url}}" data-title="{{sns.args.title}}" data-picture="{{sns.args.picture}}" data-desc="{{sns.args.desc}}">
                            <img src="/common/img/icon/facebook.png" alt="페이스북" /></a></li>

                        <li><a data-role="sns" data-provider="tw" data-url="{{sns.args.url}}" data-title="{{sns.args.title}}" data-picture="{{sns.args.picture}}" data-desc="{{sns.args.desc}}">
                            <img src="/common/img/icon/twitter.png" alt="트위터" /></a></li>

                        <li><a data-role="sns" data-provider="ks" data-url="{{sns.args.url}}" data-title="{{sns.args.title}}" data-picture="{{sns.args.picture}}" data-desc="{{sns.args.desc}}">
                            <img src="/common/img/icon/kakaostory.png" alt="카카오스토리" /></a></li>

                        <li><a data-role="sns" data-provider="kt" data-url="{{sns.args.url}}" data-title="{{sns.args.title}}" data-picture="{{sns.args.picture}}" data-desc="{{sns.args.desc}}">
                            <img src="/common/img/icon/kakao.png" alt="카카오톡" /></a></li>

                        <li><a href="#"><a data-role="sns" data-provider="line" data-url="{{sns.args.url}}" data-title="{{sns.args.title}}" data-picture="{{sns.args.picture}}" data-desc="{{sns.args.desc}}">
                            <img src="/common/img/icon/line.png" alt="라인" /></a></li>

                        <li><a data-role="sns" data-provider="copy" data-url="{{sns.args.url}}" data-title="{{sns.args.title}}" data-picture="{{sns.args.picture}}" data-desc="{{sns.args.desc}}">
                            <img src="/common/img/icon/urlshare.png" alt="URL 공유" /></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="close"><span class="ic-close" ng-click="sns.hide($event)">레이어 닫기</span></div>
    </div>

	<script type="text/javascript">
		$(function () {
			common.bindSNSAction();
		})
	</script>

</div>

<!--//레이어 안내 팝업-->


