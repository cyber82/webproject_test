﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="address.aspx.cs" Inherits="_common_popup_address" %>
<div style="background:#fff;" class="fn_pop_container" id="addressModal">

	<div class="wrap-layerpop fn_pop_content" style="width:100%;left:0;">

		<script type="text/javascript">



		    // 아이폰에서 키보드 올라오면 팝업창 내려가는 현상
            /*
		    $("input").bind("focus", function () {
		        var top = $("#bodyTag").scrollTop();
		        setTimeout(function () {
		            $("#bodyTag").scrollTop(top);
		        }, 500);
		    });
            */
		
			function initAddress($scope , $address , modal , fn, closeFn) {

				var containerEl = $("#addressModal");
				$scope.address = {
					curStep: 0,
					addr1 : "" ,
					addr2: "",
					addrRoad: null,
					addrJibun: null,

					instance: null,
					list: [],
					total: -1,
					hasMore: false,
					
					params: {
						currentPage: 1,
						countPerPage: 5,
						confmKey: "<%:this.ViewState["confirmKey"].ToString()%>"
					},

					search: function (params) {
						document.activeElement.blur();

						if (containerEl.find(".keyword").val() == "") {
							alert("도로명주소, 건물명 또는 지번을 입력하세요.");
							containerEl.find(".keyword").focus();
							return false;
						}

						$scope.address.curStep = 1;
						$scope.address.params.keyword = containerEl.find(".keyword").val();

						$scope.address.params = $.extend($scope.address.params, params);
					

						$address.search($scope.address.params, function (result) {

							$scope.$apply(function () {
								if (result.success) {

									$scope.address.total = result.total;
									
									if (result.total > 0) {

										console.log(result.data);
										$scope.address.list = result.data;
										$scope.address.hasMore = result.hasMore;
										setTimeout(function () {
											modal.iscroll.refresh();
										}, 300);
									}

								} else {
									alert("오류가 발생하였습니다. 잠시 뒤 다시 시도해주세요.");
								}

							});
						});


					},

					// 결과 선택
					selectAddress: function (item) {
						$scope.address.zipcode = item.zipcode;
						//$scope.address.addr1 = item.road + "<br>[지번] " + item.jibun;
						$scope.address.addr1 = item.road;
						$scope.address.addrRoad = item.road;
						$scope.address.addrJibun = item.jibun;
						$scope.address.roadAddrPart1 = item.roadAddrPart1;	// 도로명 주소2
						$scope.address.roadAddrPart2 = item.roadAddrPart2;	// reference 
						$scope.address.rnMgtSn = item.rnMgtSn;
						$scope.address.bdMgtSn = item.bdMgtSn;
						$scope.address.curStep = 2;
						
						modal.iscroll.scrollTo(0, 0);

					},

					// 완료버튼
					setAddress: function ($event) {
					
						if ($scope.address.addr2 == "") {
							alert("상세주소를 입력해주세요");
							containerEl.find(".addr2").focus();
							return false;
						}

						$.post("/common/popup/addressSession.ashx", {
							zipCode: $scope.address.zipcode, addrRoad: $scope.address.addrRoad, addrJibun: $scope.address.addrJibun, addr2: $scope.address.addr2
							, roadAddrPart1: $scope.address.roadAddrPart1
							, roadAddrPart2: $scope.address.roadAddrPart2
							, rnMgtSn: $scope.address.rnMgtSn
							, bdMgtSn: $scope.address.bdMgtSn
						}, function () {
							fn($scope.address.zipcode, $scope.address.addr1, $scope.address.addr2, $scope.address.addrJibun);
							$scope.address.close($event);
						});
						
					},


					// 닫기
					close: function ($event) {
					    
						if (!$scope.address.instance)
							return;


						$scope.address.instance.hide();
						

						containerEl.find(".keyword").val("");
						containerEl.find(".detail_addr").val("");
						containerEl.find(".step1").show();
						containerEl.find(".step2").hide();
						containerEl.find(".hasMore").hide();
						
						$scope.address.list = [];

						$event.preventDefault();
						document.activeElement.blur();

						if (closeFn) closeFn()
					}

				};

				// init
				$scope.address.instance = modal;
				modal.show();

			}

		</script>



		
		<div class="header2">
			<p class="txt-title">주소찾기</p>
		</div>
		<div class="contents" >
			 <div class="etc-addressfind">

				<!-- 검색 -->
				 <fieldset class="input-address">
					<legend>주소 입력</legend>
					<input type="text" class="keyword" ng-enter="address.search(true)" placeholder="도로명 주소, 건물명 또는 지번입력" />
					<a class="action" ng-click="address.search(true)">검색</a>
				</fieldset>
				<p class="txt-ex">검색어 예 : 도로명(반포대로 58), 건물명(독립기념관), 지번(삼성동 25)</p>
			


				<!--검색 결과-->
				<div class="no-result" ng-show="address.total == 0" >검색된 내용이 없습니다.</div>
			 
			 
				  <div class="wrap-tableW mt15" ng-show="address.curStep == 1 && address.total != 0">
                    <table class="tableW">
                        <colgroup><col width="70%" /><col width="30%" /></colgroup>
                        <caption>도로명주소,우편번호</caption>
                        <thead>
                            <tr>
                                <th>도로명 주소</th>
                                <th>우편번호</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="item in address.list" ng-click="address.selectAddress(item)">
                                <td class="align-left">
									{{item.road}}
                                </td>
                                <td>{{item.zipcode}}</td>
                            </tr>
                        </tbody>
                    </table>

					<paging ng-if="total != 0" class="small" page="address.params.currentPage" page-size="address.params.countPerPage" total="address.total" show-prev-next="true" show-first-last="true" paging-action="address.search({currentPage : page});"></paging>  
                </div>

				<!--//검색 결과-->


				 <!--상세주소-->
				 <div  ng-show="address.curStep == 2">

					<p class="txt-title">상세주소 입력</p>
					<fieldset class="frm-input mt15">
						<legend>상세주소 입력</legend>
						<div class="row-table">
							<div class="col-th" style="width:30%">도로명 주소</div>
							<div class="col-td" style="width:70%"><span class="txt" ng-bind-html="address.addr1"></span></div>
						</div>
						<div class="row-table">
							<div class="col-th" style="width:30%">상세주소입력</div>
							<div class="col-td" style="width:70%">
                        		<span class="row"><input type="text" class="addr2" ng-model="address.addr2" style="width:100%" /></span>
							</div>
						</div>
					</fieldset>
					<div class="wrap-bt">
						<a class="bt-type6 fr" style="width:98%" ng-click="address.setAddress($event)">주소입력</a>
					</div>
				</div>
                <!--//상세주소-->
 
				<div class="close2" ng-click="address.close($event)" onfocus="blur();"><span class="ic-close">레이어 닫기</span></div>
			 </div>
		</div>

		

	</div>
</div>