﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="organization.aspx.cs" Inherits="_common_popup_organization" %>
<div style="background:#fff;" class="fn_pop_container" id="organizationModal">

	<div class="wrap-layerpop fn_pop_content" style="width:100%;left:0;">
		<!--레이어 안내 팝업-->
		<script type="text/javascript">
			function hasMore (total, page, rows) {
				var pageCount = Math.floor(total / rows);
				if (total % rows > 0) pageCount++;
				return page < pageCount;
			}
			
			function initOrganization($scope, $http, modal, fn) {

				var containerEl = $("#organizationModal");
				$scope.organization = {
				
					instance: null,
					list: [],
					total: -1,
					hasMore: false,
					
					params: {
						currentPage: 1,
						countPerPage: 2
					},

					search: function (params) {
						document.activeElement.blur();
						console.log("test");

						if (containerEl.find(".keyword").val() == "") {
							alert("교회 또는 단체를 입력하세요.");
							containerEl.find(".keyword").focus();
							return false;
						}
						

						$scope.organization.params.name = containerEl.find(".keyword").val();
						$scope.organization.params.type = containerEl.find(".type").val();

						$scope.organization.params = $.extend($scope.organization.params, params);




						$http.get("/api/sponsor.ashx?t=organization-list", { params: $scope.organization.params }).success(function (result) {
								data = result.data.torganizationdetail;
								array = [];


								$.each(data, function () {
									var entity = {
										id: this.sponsorid,
										name: this.sponsorname
									};
									array.push(entity);
								});

								$scope.organization.list = array;
								$scope.organization.total = array.length;

								console.log($scope.organization.list);
								//$scope.organization.hasMore = hasMore($scope.organization.total, $scope.organization.params.currentPage, $scope.organization.params.countPerPage);

								setTimeout(function () {
									modal.iscroll.refresh();
								}, 300);
						});

					},


					// 결과 선택
					selectOrganization: function (id,name, $event) {

						fn(id, name);
						$scope.organization.close($event);
					},

					// 닫기
					close: function ($event) {

						if (!$scope.organization.instance)
							return;

						$scope.organization.instance.hide();

						containerEl.find(".keyword").val("");
						containerEl.find(".hasMore").hide();
						
						$scope.organization.list = [];

						$event.preventDefault();
						document.activeElement.blur();
					}

				};

				// init
				$scope.organization.instance = modal;
				modal.show();

			}

			
			
		</script>

		<div class="wrap-layerpop">
			<div class="header2">
        		<p class="txt-title">교회 검색</p>
			</div>
			<div class="contents">
            
				<div class="sponsor-special">
            
            		<fieldset class="search-church">
                		<legend>교회/단체명  입력 검색</legend>
						<p>찾으시는 교회/단체 명을 정확히 입력해 주세요.</p>
						<div class="row">
							<select class="type" style="width:85px">
								<option value="교회">교회</option>
								<option value="기업">단체</option>
							</select>
							<input type="text" class="keyword"  ng-enter="organization.search(true)"  placeholder="교회/단체명" style="width:100%" />
							<span class="bt-type8" style="width:70px" ng-click="organization.search(true)">검색</span>
						</div>
					</fieldset>    
                
					<div class="wrap-tableW mt20" >
						<table class="tableW style-add2  church">
							<caption>캠페인 제목,캠페인 기간</caption>
							<colgroup><col width="48%" /><col width="52%" /></colgroup>
							<thead>
								<tr>
									<th>교회/단체 ID</th>
									<th>교회/단체 명</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="item in organization.list" ng-click="organization.selectOrganization(item.id , item.name, $event)">
									<td><span class="txt-emp">{{item.id}}</span></td>
									<td>{{item.name}}</td>
								</tr>
									
                            
								<!--검색없음-->
								<tr ng-if="organization.total == 0">
									<td colspan="2" class="no-result">검색결과가 없습니다.</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="wrap-bt"><a class="bt-type8" style="width:80px"  ng-click="organization.close($event)">닫기</a></div>

				<div class="close2"><span class="ic-close"  ng-click="organization.close($event)">레이어 닫기</span></div>
			</div>
		</div>    
	<!--//레이어 안내 팝업-->
	</div>
</div>