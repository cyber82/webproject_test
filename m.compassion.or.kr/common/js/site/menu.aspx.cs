﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class common_js_site_menu : MobileFrontBasePage
{

    public override bool IgnoreProtocol
    {
        get
        {
            return true;
        }
    }


    protected override void OnBeforePostBack()
    {

        Response.ContentType = "text/javascript";
        Response.Clear();

        var mode = Request["mode"].ValueIfNull("mw");

        if (mode == "mw")
            mode = "mobileweb";

        var root = SiteMap.Providers[mode].RootNode;

        /*
        string id = AppSession.HasCookie(this.Context) ? "app" : "mobileweb";
		var root = SiteMap.Providers[id].RootNode;
        */
        data.Text = root.ToMenu().ToJson();
    }


}