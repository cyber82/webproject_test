﻿/*
app_ex
 1 : app 외부 새로운 윈도우 열기
 2 : app 내부 새로운 activity 열기
 3 : app main browser 에서 열기
*/

(function () {  
	var app = angular.module('cps.bridge', []);

	app.factory("bridge", function ($rootScope) {
		return {

			setRootScope : function(arg){
				$bridge_rootScope = arg;
			},

			// 로그오프 정보 앱으로 전달
			// impl me.
			logout: function () {
				var appDevice = $rootScope.appDevice;
				if (appDevice == "android") {
					if (typeof JSInterface !== "undefined") {
						setTimeout(function () {
							JSInterface.logout();

						//	location.href = "#/login/";

						}, 100);
					}

				} else if (appDevice == "iphone") {
					window.location = "app://logout";
				} else {

				//	location.href = "#/login/";

				}
			} , 

			// 사용자정보 앱으로 전달
			setUserInfo: function (u) {
				var appDevice = $rootScope.appDevice;
				
				console.log("APP으로 사용자정보 전달");
				
				//var u_agree_push = u.u_agree_push ? "1" : "0";
				var u_agree_push = "1";
				var custId = u.custid;	// 이름
				var custname = u.custname;	// 이름
				var age = u.age;	// 연령대
				//var data1 = u.data1;		// 마니아 회원등급
				var location1 = u.locationtype;	// 지역
				var gender = u.gender;		// 성별
				console.log('push test' + JSON.stringify(u));

				if (appDevice == "android") {
					if (typeof JSInterface !== "undefined") {
						setTimeout(function () {
							// u_agree_loc , u_login_id , u_provider , cust_id (@fb , @ka 등 provider 에 따라 변경되는 회원아이디)
							// 앱액션 : PMS에 로그인 , 위치정보 수신여부 업데이트
							JSInterface.setUserInfo(u_agree_push, custId, custname, age, location1, gender);
						}, 100);
					}

				} else if (appDevice == "iphone") {
					// 반드시 settimeout 사용 , ios 
					setTimeout(function () {
						
						window.location = "app://setUserInfo?push=" + u_agree_push + "&custid=" + custId
						 + "&custName=" + custname + "&age=" + age + "&location1=" + location1 + "&gender=" + gender;
					}, 100);
				}
			},

			// push data 요청
			getPushData: function ($scope, page , rows , methodName) {
				var appDevice = $rootScope.appDevice;
				$bridge_scope = $scope;
				$bridge_pushStateMethodName = methodName;
				
				if (appDevice == "android") {
				
					if (typeof JSInterface !== "undefined") {
						JSInterface.getPushMessageList(page, rows);
					}

					//var json = $.parseJSON(JSInterface.getPushMessageList("1", "2"));	
					//eval('$bridge_scope.' + $bridge_pushDataContainerId + ' = json');
					//this.setPushData(json);
				
				} else if (appDevice == "iphone") {

					window.location = "app://getPushMessageList?page=" + page + "&rows=" + rows;
				
				}
			},

			// push data callback hybrid 어플에서 호출(iOS)
			setPushData: function (json) {
			//	console.log("MW setPushData > " + json);
				
				setTimeout(function () {
					//$bridge_scope.$apply(function () {
						//eval('$bridge_scope.' + $bridge_pushDataContainerId + ' = json');
					eval('$bridge_scope.' + $bridge_pushStateMethodName + '(' + angular.toJson(json) + ')');

					//});
				}, 100);

			},

			// push data 요청
			/*
			getNewPushCount: function () {

				var appDevice = $rootScope.appDevice;
				
				// hybrid_api.setNewPushCount 응답
				if (appDevice == "android") {

					JSInterface.getNewPushCount();

				} else if (appDevice == "iphone") {

					window.location = "app://getNewPushCount";

				}
			},

			setNewPushCount: function (count) {
				
				// $bridge_scope = $rootScope
				var rootScope = angular.element($("body")).scope();
				rootScope.$apply(function () {
					rootScope.hasUnreadPushMsg = count > 0;
				})
				
				//$bridge_scope.$broadcast('onReceivePushUnreadCount', {count : count});
				
			},
			*/

			// push msg 읽음 처리
			readPush: function ( msgId) {

				var appDevice = $rootScope.appDevice;
				
				if (appDevice == "android") {
					
					if (typeof JSInterface !== "undefined") {
						JSInterface.readPush(msgId);
					}

				} else if (appDevice == "iphone") {

					window.location = "app://readPush?msgId=" + msgId;

				}
			},

			// 푸시 수신 여부 
			getPushState: function ($scope, methodName) {
			
				var appDevice = $rootScope.appDevice;

				$bridge_scope = $scope;
				$bridge_pushStateMethodName = methodName;

				var self = this;

				setTimeout(function () {
					if (appDevice == "android") {
						if (typeof JSInterface !== "undefined") {
							var state = JSInterface.getPushState();
							self.setPushState(state);
						}
						
					} else if (appDevice == "iphone") {
						window.location = "app://getPushState";

					}
				}, 100);

			} , 

			// UI switch 를 갱신
			setPushState: function (state) {
				
				eval('$bridge_scope.' + $bridge_pushStateMethodName + '('+state+')');
			
			},

			// APP으로 변경된 상태 전송
			// 미사용
			updatePushState: function (state) {

				var appDevice = $rootScope.appDevice;
				
				$.get("/api/app.ashx?t=set-push", { "yn": state ? "Y" : "N" });

				if (appDevice == "android") {
					// msgFlag , notiFlag
					/*
					msgFlag : 메시지함 적재 여부 & 푸쉬 메세지를 받을 것인지 여부 ("Y" & "N")
					notiFlag : 메세지함에 적재는 하지만 푸쉬 메세지를 받을 것인지 여부 ("Y" & "N")
					Y & N = 메세지함에는 적재하되 푸쉬메세지만 거절 시 
					N & N = 메세지함과 푸쉬메세지 여부를 모두다 거절시 
					*/
					if (typeof JSInterface !== "undefined") JSInterface.setPushState(true, state);
				} else if (appDevice == "iphone") {

					window.location = "app://setPushState?notiFlag=" + (state ? "1" : "0") + "&msgFlag=1";

				}

			},
			/*
			// 갱신된 사용자 이미지 경로로 업데이트
			updateUserImage: function (u_picture) {
				$.post("/api/user.ashx", { action: "update_picture", value: u_picture }, function () {
					
				});
				//console.log($bridge_rootScope);
				$bridge_rootScope.$apply(function () {
					$bridge_rootScope.user.u_picture = u_picture;
				})
			},
			*/

			cookieSync: function () {
				if ($rootScope.appDevice == "android")
					if (typeof JSInterface !== "undefined") JSInterface.cookieSync();
			}


		}
	});

})();