﻿// 후원계기
var $motive = {

	code : null , 
	data: null,
	sel_motive1: null,
	sel_motive2: null,
	hd_motive_code:null,
	hd_motive_name:null,

	init: function (hd_motive_code , hd_motive_name , sel_motive1 , sel_motive2) {

		$motive.hd_motive_code = hd_motive_code;
		$motive.hd_motive_name = hd_motive_name;
		$motive.sel_motive1 = sel_motive1;
		$motive.sel_motive2 = sel_motive2;

		code = hd_motive_code.val();
		sel_motive1.append($("<option value=''>선택하세요</option>"));
		sel_motive2.append($("<option value=''>선택하세요</option>"));

		sel_motive1.change(function () {
			
			if ($("#motive2_etc").length > 0)
				$("#motive2_etc").val("");

			$("#pn_motive2_etc").hide();

			var val = $(this).val();
			if (val == "") {
				sel_motive2.empty();
				$motive.sel_motive2.append($("<option value=''>선택하세요</option>"));
				$motive.hd_motive_code.val("");
				$motive.hd_motive_name.val("");
			} else {

				if ($motive.data) {

					$.each($motive.data, function (i, v) {

						if (i == val) {

							sel_motive2.empty();
							sel_motive2.append($("<option value=''>선택하세요</option>"));

							$.each(v, function () {
								var opt = $("<option value='" + this.codeid + "'>" + this.codename + "</option>");
								sel_motive2.append(opt);

								if (this.codeid == code) {
									sel_motive2.val(this.codeid);
									sel_motive2.trigger("change")
									code = "";
								}
							})
						}

					});

					$motive.loaded = true;

				}

			}
		})
		
		sel_motive2.change(function () {

			if ($("#motive2_etc").length > 0)
				$("#motive2_etc").val("");
			
			var key = $(this).val();
			var value = key == "" ? "" : sel_motive2.find("option:selected").text().replace("\n", "");
			hd_motive_code.val(key);

			if (value == "기타" && $("#pn_motive2_etc").length > 0) {
				$("#pn_motive2_etc").show();
				if (!$motive.loaded) {
					$("#motive2_etc").val(hd_motive_name.val());
					$motive.loaded = true;
				}
			} else {
				$("#pn_motive2_etc").hide();
				hd_motive_name.val(value);
			}

		});

		if ($("#motive2_etc").length > 0) {
			$("#motive2_etc").blur(function () {

				hd_motive_name.val($(this).val());

			});

		}
		$.get("/api/motive.ashx", { t: "list" }, function (r) {

			$motive.data = r;
		//	console.log(r);

			$.each($motive.data, function (i,v) {

				//console.log(v);
				var opt = $("<option value='" + i + "'>" + i + "</option>")
				sel_motive1.append(opt);
			})

			if (hd_motive_code.val() != "") {

				$.each($motive.data, function (i, v) {

					$.each(v, function () {
						if (this.codeid == hd_motive_code.val()) {
							sel_motive1.val(i);
						}
					})
					
				});

				
				sel_motive1.trigger("change");

			}

		});

	}

}