﻿$.ajaxSetup({ cache: false });

var kakaoKey = "dd71aedea41f82616eca293786f838f5";
var facebookKey = "330228967342270";

var debugging = true;
if (!debugging || typeof console == "undefined" || typeof console.log == "undefined") var console = { log: function () { } };
var customBackAction = false;

$(function () {


	$.each($("button"), function () {
		if (!$(this).attr("type")) {
			$(this).attr("type", "button");
		}
	})

	/*
	안드로이드에서 e.keyCode가 229만나옴
	안드로이드에서 maxlength적용안됨
	$(".number_only").unbind("keydown");
	$(".number_only").keydown(function (e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
			// Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
			// Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
			// Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}

		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	*/
	
	$(".number_only").unbind("keyup");
	$(".number_only").keyup(function (e) {
		var val = this.value;
		var maxlength = $(this).attr("maxlength");
		val = new String(val);
		var regex = /[^0-9]/g;
		this.value = val.replace(regex, '').substr(0, maxlength);
	});

	common.bindSNSAction();

	setCurrenyFormat();

	// app intro 체크
	var app = cookie.get("cps.app");
	var appIntro = cookie.get("appIntro");

	console.log("app = " + app)
	console.log("appIntro = " + appIntro)
	console.log("location.pathname = " + location.pathname)
	
	if (app && !appIntro && !location.pathname.startsWith("/app/gateway")) {
		hybrid_api.hideIntro();
		cookie.set("appIntro", "Y");
	} else if (app && !location.pathname.startsWith("/app/gateway")) {
		hybrid_api.hideIntro();
	}
})

var common = {

	isLogin: function () {
		return $("#is_login").val() == "Y";	// top.master 
	},

	getUserId: function () {
		return $("#_userid").val();
	},

	checkLogin: function (r) {
		if (!common.isLogin()) {
			if (confirm("로그인이 필요합니다. \n로그인 페이지로 이동하시겠습니까?")) {
				location.href = "/login" + (r ? "?mr="+r : "");
				
			}
			return false;
		}

		return true;
	},

	bindSNSAction: function () {

		var timeout = null;

		$(".sns_ani").unbind("click");
		$(".sns_ani").bind("click", function () {

			var self = $(this);

			if (self.hasClass("on")) {

				var param = { "width": "0", "opacity": "0" };
				if ($(this).hasClass("down")) {
					param = { "height": "0", "opacity": "0" };
				}

				$(".common_sns_group").stop().animate(param, 300);
				self.removeClass("on");

			} else {
				var param = { "width": "185px", "opacity": "1" };
				if ($(this).hasClass("down")) {
					param = { "height": "185px", "opacity": "1" };
				}
				self.find(".common_sns_group").stop().animate(param, 300);
				self.addClass("on");
			}
		});




		// widget 이 이미 생성되었으면 이벤트 새로 적용
		try {
			if ($("body").is(":data('ui-sns')")) {
				var sns = $("body").data("ui-sns");
				sns.reload();

			} else {

				
				$("body").sns({
					isApp : cookie.get("cps.app") ? true : false ,	
					kakaoKey: kakaoKey,
					facebookKey: facebookKey,
					onSuccess: function (provider, response) {
						if (provider == "ks" || provider == "fb") {
							alert("공유되었습니다.");
						}
						//	console.log("success", provider, response);
					}
				});
			}
		} catch (e) { }
	}

};

var goBack = function () {
	history.back();
};

var goList = function () {
	location.href = $("#btnList").attr("href");
};

var setDatePicker = function (obj) {

	obj.datepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: '',
		numberOfMonths: 1,
		monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		showTime: false,
		showHour: false,
		showMinute: false,
		closeText: '닫기',
		currentText: '오늘',

		buttonImageOnly: false,
		changeYear: true,
		onSelect: function (text, e) {
			$(this).datepicker("hide");

		}
	});

};

var setCurrenyFormat = function () {
	$(".use_digit").currencyFormat();
};

var setPlaceholder = function (obj, str) {

	obj.focus(function () {
		if ($(this).val() == str) {
			$(this).val("")
		}
	}).focusout(function () {
		if ($(this).val() == "") {
			$(this).val(str)
		}
	})

};

var scrollTo = function (obj, offset, retryWhenFindObj, cb, count) {

	if (retryWhenFindObj && obj.length < 1) {

		if (!count) count = 0;
		count++;
		if (count > 3) return;

		setTimeout(function () {

			scrollTo(obj, offset, retryWhenFindObj, cb, count);
		}, 300)
		return;
	}

	offset = offset || 0;
	if (obj && obj.offset) {
		var top = obj.offset().top - $(".header_bottom").height() - offset;
		//$("html,body").scrollTop(top);
		$("html,body").animate({ "scrollTop": top }, 0, function () {
			if (cb)
				cb();
		})

		//console.log(top);
	}
};

// 
var loading = {
	show: function (msg) {

		$("#loading_bg , #loading_container").bind("touchmove", function (e) {
			e.preventDefault();
			return false;
		})

		/*
		$(window).resize(function () {
			loading._resize();
		});
		*/


		$("#loading_bg").css({ opacity: 0, display: "block" });
		$("#loading_bg").animate({
			opacity: 0.6
		}, 'fast', function () {
			$("body").css({ overflow: "hidden" })
			var container = $("#loading_container");

			if (msg)
				$("#loading_container .msg").html(msg);
			else {
				$("#loading_container .msg").html("로딩 중입니다. 잠시만 기다려주세요.");	// 임시 , 이미지로 처리예정
			}
			container.show();

		})


	},

	hide: function () {

		$(window).unbind("resize", loading._resize);

		$("body").css({ overflow: "auto" })

		$("#loading_bg").animate({
			opacity: 0.0
		}, 'fast', function () {
			$("#loading_bg").css({ opacity: 0, display: "none" });

			var container = $("#loading_container");
			container.hide();

			$("#loading_container .msg").html("");

		});



	},

	_resize: function () {
		var windowHeight = window.innerHeight ? window.innerHeight : $(window).height();
		$('#loading_bg').css({ 'width': $(window).width(), 'height': windowHeight });
		// hegiht delay 현상이 있어서 1초뒤 다시 계산
		$("#loading_container").css({ top: "40%", height: windowHeight });
		console.log(windowHeight);
		setTimeout(function () {
			$("#loading_container").css({ top: "40%", height: windowHeight });
		}, 1000)
		console.log(windowHeight);
	}
};

var getDevice = function () {

	var aOS = navigator.userAgent.toLowerCase().indexOf("android") > -1;
	var iOS = (navigator.platform.indexOf("iPhone") != -1) || (navigator.platform.indexOf("iPod") != -1);

	if (aOS) return 'iphone';
	if (iOS) return 'android';

	return 'android';


}

var setFullHeight = function () {
	// 앱화면에서 컨텐츠 길이 짧을경우 컨텐츠 height값 full size로 조정.
	if ($("section").hasClass("appPageNav")) {
		var pageH = $(".fullHeight").height();
		var deviceH = $(document).height() - 62;
		
		if (pageH < deviceH) {

			$(".fullHeight").css({ "height": deviceH });

		}
	}
}