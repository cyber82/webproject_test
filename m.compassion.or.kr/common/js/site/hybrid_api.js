﻿/*
app_ex
 1 : app 외부 새로운 윈도우 열기
 2 : app 내부 새로운 activity 열기
 3 : app main browser 에서 열기
*/

// ios , android 에서 호출
var hybrid_api = {

	// 새로운 버젼 앱 설치
	installNewVersion: function (info) {
		var entity = $.parseJSON(cookie.get("cps.app"));
		var appDevice = entity.device;
		var url = info.dv_link.appendQuery("app_exit=1").appendQuery("app_ex=1");
				
		if (appDevice == "android") {
			window.open(url , "", "");

			//hybrid_api.killApp();

		} else if (appDevice == "iphone") {
			window.location = url;

			//hybrid_api.killApp();

		}

	},


	// 앱종료 
	killApp: function () {

		var entity = $.parseJSON(cookie.get("cps.app"));
		var appDevice = entity.device;
		
		if (appDevice == "android") {

			setTimeout(function () {
				if (typeof JSInterface !== "undefined") JSInterface.killApp();
			}, 100);

		} else if (appDevice == "iphone") {
			window.location = "app://killApp";

		}
	} , 

	setPushData: function (json) {
		
		angular.injector(['ng', 'cps.bridge']).get("bridge").setPushData(json);
	
	},

	setPushState: function (state) {	// 1 or 0
		angular.injector(['ng', 'cps.bridge']).get("bridge").setPushState(state);
	
	}, 

	

	// APP push 를 클릭했을경우 push 에 지정된 url 정보를 app 에서 mobile web 으로 전달한다. 
	openPushLink: function (url) {
		var arg = "1";
		if (url.indexOf("m.compassion.or.kr") > -1 || url.indexOf("m.compassionko.org") > -1 || url.indexOf("m.compassionkr.com") > -1) {
			arg = "3";
		}

	//	alert(url + " , " + arg);

		location.href = url.appendQuery("app_ex=" + arg);
	},

	// 사이트내부에서 클릭되는 링크
	openLink: function (url) {

		var entity = $.parseJSON(cookie.get("cps.app"));
		var appDevice = entity.device;

		var arg = "3";
		if (url.indexOf("http://") > -1 || url.indexOf("https://") > -1) {
			arg = "2";
		}

		if (appDevice == "iphone") {
			location.href = url.appendQuery("app_ex=" + arg);
		} else {

			if (url.indexOf("intent:") > -1) {
				arg = "1";
			}
			
			window.open(url.appendQuery("app_ex=" + arg));
		}

	} , 

	getNewPushCount: function () {

		var entity = $.parseJSON(cookie.get("cps.app"));
		var appDevice = entity.device;
		// hybrid_api.setNewPushCount 응답
		if (appDevice == "android") {
			if (typeof JSInterface !== "undefined") JSInterface.getNewPushCount();

		} else if (appDevice == "iphone") {
			setTimeout(function () {
				window.location = "app://getNewPushCount";
			},300)
		}
		
	},

	// 읽지 않은 push 메세지
	setNewPushCount: function (count) {
	    
	    if (count > 0) {
	        $(".unread_push").show();
	    }
	    if (count > 99) {
	        $(".unread_push").text("99+");
	    } else {
	        $(".unread_push").text(count);
	    }
	//    alert(count);
	},

	submitUserImage: function (path, base64, cb) {
		//$('#base').html(base64);
		/*
		exif.js 
		orientation 참고 : http://stackoverflow.com/questions/7584794/accessing-jpeg-exif-rotation-data-in-javascript-on-the-client-side
		*/
		var image = new Image();
		image.onload = function () {
			EXIF.getData(image, function () {

				// exif = undefined => 1
				var orientation = EXIF.getTag(this, "Orientation") || "1";
				//alert(orientation)
				//alert(EXIF.pretty(this));
                
                $.post("/api/user-profile-image.ashx", { path: path, base64: base64, orientation: orientation }, function (r) {
                    
					if (r.success) {
                        if (cb) cb(r);
					}
				})

			});
		};

		image.src = base64;

	},

	submitHybridUpload: function (base64, id, path) {
		var image = new Image();
		image.onload = function () {
			EXIF.getData(image, function () {
				var orientation = EXIF.getTag(this, "Orientation") || "1";
				$.post("/api/user-profile-image.ashx", { path: path, base64: base64, orientation: orientation }, function (r) {
					if (r.success) {

						if (id == "hd_uf_image") {
							var img = r.data;
							$("#hd_uf_image").val(img);
							$("#btn_uf_image").find(".img_guide").hide();
							$("#btn_uf_image").find(".img_guid2").css({ opacity: 0 });
							$("#btn_uf_image").css({ "background-image": "url('" + $("#hd_image_domain").val() + $("#hd_uf_image").val() + "')" });
							if ($("#hd_uf_image").val() != "") {
								$("#btn_uf_image_del").show();
							}
							setTimeout(function () {
								$("input[name=userfile]").css("display", "block");
							}, 300);
						} else if (id == "hd_user_pic") {
							var img = r.data;
							$("#hd_user_pic").val(img);
							$("#image_UserPic").val(img.replace(/^.*[\\\/]/, ''));

							$("#btn_UserPic").css({ "background-image": "url('" + $("#hd_image_domain").val() + $("#hd_user_pic").val() + "')" });
							$("#btn_UserPic").find(".img_guide3").css({ opacity: 0 });
							$("#image_UserPic_del").show();

							setTimeout(function () {
								$("input[name=userfile]").css("display", "block");
							}, 300);
						} else if (id.startsWith("uf_content_image")) {
							var img = r.data;
							var index = id.replace("uf_content_image_", "");
							$("input[name=userfile]").css("display", "block");
							$("#uf_content_image_" + index).attr("data-url", img);
							$("#uf_content_image_" + index).find(".img_guide").css({ opacity: 0 });
							$("#btn_uf_image").find("#btn_uf_image_del").show();
							$("#uf_content_image_" + index).css({ "background-image": "url('" + $("#hd_image_domain").val() + img + "')" });
							$("#uf_content_image_del").show();
							
							var data = "";
							$.each($(".uf_content_image"), function () {
								var url = $(this).attr("data-url");
								if (url != "") data += "|" + url;
							});
							if (data.length > 0) data = data.substring(1);
							$("#hd_uf_content_image").val(data);
						}else if(id == "hd_uf_image_update"){
							var img = r.data;
							$("#hd_uf_image").val(img);
							$("#btn_uf_image").find(".img_guide").css({ opacity: 0 });
							$("#btn_uf_image").find("#btn_uf_image_del").show();
							$("#btn_uf_image").css({ "background-image": "url('" + $("#hd_image_domain").val() + $("#hd_uf_image").val() + "')" });
						} else if (id == "letterwrite") {
							if (r.success) {
								var img = r.data;
								$("#btn_file_remove").show();
								alert("첨부 파일에 내용이 있는 경우 번역이 되지 않습니다");
								$("#file_path").val(img);
								$(".attach_img").attr("src", ($("#domain_image").val() + img)).show();
								$(".txt-noimg").hide();
							}
						} else if (id == "letterwritepic") {
							if (r.success) {
								var img = r.data;
								$("#btn_file_remove").show();
								alert("첨부 파일에 내용이 있는 경우 번역이 되지 않습니다");
								$("#file_path").val(img);
								$(".attach_img").attr("src", ($("#domain_image").val() + img)).show();
								$(".txt-noimg").hide();
							}
						} else if (id == "letterupdatepic") {
							if (r.success) {
								var img = r.data;
								$("#btn_file_remove").show();
								alert("사진을 첨부하실 경우 편지는 일반우편과 동일한방식으로 배송됩니다.\n\r첨부 파일이 텍스트인 경우 번역이 되지 않으니 이 점 유의해 주세요.");
								$("#file_path").val(img);
								$("#file_size").val(fileSize);
								$(".attach_img").attr("src", ($("#domain_image").val() + img)).show();
								$(".txt-noimg").hide();
							}
						} else if (id == "createfunding") {
							if (r.success) {
								var img = r.data;
								var url = $("#hd_image_domain").val() + img;
								$("span.pic").css({ "background-image": "url('" + url + "')" });
								$.post("/api/sponsor.ashx", { t: "update-userpic", path: img });
							}
						}
					}
				})

			});
		};

		image.src = base64;
	},

	hideIntro: function () {
		var sec = 0;
		
		var entity = $.parseJSON(cookie.get("cps.app"));
		
		var appDevice = entity.device;
	
		if (appDevice == "android") {

			setTimeout(function () {
				if (typeof JSInterface !== "undefined") JSInterface.hideIntro(sec);
			}, 100);

		} else if (appDevice == "iphone") {
			
			window.location = "app://hideIntro";

		}
	}

};
 
