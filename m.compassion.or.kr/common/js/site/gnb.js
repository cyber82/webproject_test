﻿var m1, m2, m3, m4, m5;

$(function () {

    if ($("#wrap_lnb_container").length < 1) {
        return;		// APP 인경우
    }
    //console.log(_menu.sub);
    var getMenu = function (key) {
        return $.grep(_menu.sub, function (r) {
            return r.key == key;
        })[0];
    }

    m1 = getMenu("about-us");			// 소개
    m2 = getMenu("sympathy");			// 공감
    m3 = getMenu("participation");		// 참여
    m4 = getMenu("activity");			// 애드보킷
    m5 = getMenu("my");					// 마이컴패션

    lnb.setCurrent();

    lnb.init();

    footer.init();

});

var footer = {

    list: null,

    init: function () {

        $("#btn_footer_more").click(function () {
            $(".footer-more").slideToggle('fast', function () {
                $("html, body").animate({ scrollTop: $(document).height() }, 400);
                $("#btn_footer_more").toggleClass("on");
            });
            return false;
        })

    }

}

var lnb = {

    list: null,
    visible: false,
    scrollController: null,
	clicked : false,			// 갤럭시s3에서 클릭이 2번됨
    is_move: false,
    move_container: null,
    init: function () {

        //this.left_scoll = new IScroll('#wrap_lnb_container', { click: true, bounce: false });		// 5.2
        this.scrollController = new iScroll('wrap_lnb_container', { click: true, bounce: false, onRefresh: lnb.onRefresh });		// 4.0

        $("#lnb_bg").bind("touchmove", function (e) {
            e.preventDefault();
            return false;
        })

        $("#lnb_bg, .close-lnb").click(function () {
            lnb.close();
            return false;
        })

        $("[data-role='open-lnb']").click(function () {

            lnb.open();
            return false;
        })

        //	lnb.open();

        $(window).resize(function () {
            if (lnb.visible) {
                var windowHeight = window.innerHeight ? window.innerHeight : $(window).height();
                $('#lnb_bg').css({ 'width': $(window).width(), 'height': windowHeight });

                // hegiht delay 현상이 있어서 1초뒤 다시 계산
                $("#wrap_lnb_container").css({ top: $(window).scrollTop(), height: windowHeight });
                setTimeout(function () {
                    $("#wrap_lnb_container").css({ top: $(window).scrollTop(), height: windowHeight });
                }, 1000)

            }
        });

        this.list = [m1, m2, m3, m4, m5];

        $.each(this.list, function (i) {

            var container = $($('[data-role="lnb-menu-container"]').find(".depth1")[i]);
            lnb.bindEvent(this, container, 2);

        });

        lnb.setDefault();

    },

    onRefresh: function () {

        if (lnb.is_move) {
            //setTimeout(function () {
            var top = lnb.move_container.offset().top - $(window).scrollTop();
            var winHeight = $(window).innerHeight();
            var navHeight = $(".wrap-lnb").height();
            var scrollArea = navHeight - winHeight;
            var curScroll = lnb.scrollController.y;

            if (winHeight < navHeight) {

                if (scrollArea < top) {
                    top = scrollArea + curScroll;
                }
                
                if (curScroll < 0) {
                    top += curScroll;
                }
                lnb.scrollController.scrollTo(0, top, 300, true);

            }

        }
    },

    bindEvent: function (obj, container, depth) {

        if (depth > 3) return;
        var root = $('<span class="sub-depth' + depth + ' d' + depth + '"/>');
        container.parent().append(root);

        if (obj.sub && obj.sub.length > 0) {
        	container.click(function () {
        		if (lnb.clicked) return;
        		lnb.clicked = true;

                var data_id = container.data("id");
                var is_open = $("span.edge[data-id='" + data_id + "']").hasClass("edge-active");
                lnb.is_move = false;

                

                $(".sub-depth" + depth).removeClass("sub-depth" + depth + "-active");
                $("span.edge[data-depth='" + depth + "']").removeClass("edge-active");
                
                if (!is_open) {
                    $("span.edge[data-id='" + data_id + "']").addClass("edge-active");
                    root.addClass("sub-depth" + depth + "-active");
					
                    if (depth == 2) {
                        lnb.is_move = true;
                        lnb.move_container = container;

                    }
                }
                setTimeout(function () {
                    lnb.scrollController.refresh();
                }, 300)

                setTimeout(function () {
                	lnb.clicked = false;
                }, 500)




            })
        } else {
            container.parent().find(".edge").remove();
            container.attr("href", obj.url + "?lnb=Y");
        }

        if (obj.selected && obj.sub && obj.sub.length) {
            var data_id = container.data("id");
            $("span.edge[data-id='" + data_id + "']").addClass("edge-active");
            root.addClass("sub-depth" + depth + "-active");
        }

        if (obj.sub) {
        	$.each(obj.sub, function (i) {
        		var item = null;
        		if (this.sub && this.sub.length) {
        			var data_id = "m" + depth + "" + i;
        			item = $('<a class="depth' + (depth) + ' a' + depth + '" data-id="' + data_id + '">' + this.name + '<span class="edge" data-id="' + data_id + '" data-depth="' + (depth + 1) + '"></span></a>');
        		} else {
        			item = $('<a href="' + this.url + '?lnb=Y" class="depth' + (depth) + ' a' + depth + '">' + this.name + '</a>');
        		}

        		root.append(item);

        		if (this.selected) {
        			item.find("a").addClass("lnb_selected");
        		}

        		if (this.sub) {
        			lnb.bindEvent(this, item, depth + 1);
        		}
        	})
        }
    },

    setDefault: function () {
        $.each(lnb.list, function (i) {
            if (this.selected) {
                $("a.lnb_top.m" + (i + 1)).addClass("on").addClass("lnb_selected");
            }
        });

        var targets = $(".lnbsub_wrap .lnb_selected");
        targets.addClass("on");
        targets.show();
    },

    setCurrent: function () {

        if ($("#sitemap_resourcekey").length < 1) return;

        var cur_key = $("#sitemap_resourcekey").val();
        //console.log(cur_key);
        var setSelected = function (obj) {

            $.each(cur_key.split('|'), function () {

                if (this.startsWith(obj.key))
                    obj.selected = true;

            })

            if (obj.sub) {

                $.each(obj.sub, function () {
                    setSelected(this);

                });
            }

        }

        $.each(_menu.sub, function () {
            setSelected(this);
        });
    },

    open: function () {
        lnb.visible = true;

        var windowHeight = window.innerHeight ? window.innerHeight : $(window).height();
        $("#lnb_bg").css({ opacity: 0, display: "block" });
        $("#wrap_lnb_container").css({ right: "-90%", display: "block", top: $(window).scrollTop(), height: windowHeight });
        lnb.scrollController.scrollTo(0, 0);

        $("#lnb_bg").animate({
            opacity: 0.8
        }, 'fast', function () {

            $("#wrap_lnb_container").animate({
                right: 0
            }, 'fast', function () {
                $("body").css({ overflow: "hidden" })
                lnb.scrollController.refresh();
            })

        })

    },

    close: function () {
        lnb.visible = false;
        $("body").css({ overflow: "auto" })

        $("#wrap_lnb_container").animate({
            right: "-90%"
        }, 'fast', function () {
            $("#lnb_bg").animate({
                opacity: 0.0
            }, 'fast', function () {
                $("#lnb_bg").css({ opacity: 0, display: "none" });
                $("#wrap_lnb_container").css({ right: "-90%", display: "none" });
            });
        })


    }

}

