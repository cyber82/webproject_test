﻿
(function () {
	var app = angular.module('cps', ['paging-short', 'cps.page',
		'cps.bridge' , 'cps.menu' , 
		'ngSanitize', 'angularMoment'], function ($httpProvider) {
		// http://stackoverflow.com/questions/19254029/angularjs-http-post-does-not-send-data
		// Use x-www-form-urlencoded Content-Type
		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

		/**
		 * The workhorse; converts an object to x-www-form-urlencoded serialization.
		 * @param {Object} obj
		 * @return {String}
		 */
		var param = function (obj) {
			var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

			for (name in obj) {
				value = obj[name];

				if (value instanceof Array) {
					for (i = 0; i < value.length; ++i) {
						subValue = value[i];
						fullSubName = name + '[' + i + ']';
						innerObj = {};
						innerObj[fullSubName] = subValue;
						query += param(innerObj) + '&';
					}
				}
				else if (value instanceof Object) {
					for (subName in value) {
						subValue = value[subName];
						fullSubName = name + '[' + subName + ']';
						innerObj = {};
						innerObj[fullSubName] = subValue;
						query += param(innerObj) + '&';
					}
				}
				else if (value !== undefined && value !== null)
					query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
			}

			return query.length ? query.substr(0, query.length - 1) : query;
		};

		// Override $http service's default transformRequest
		$httpProvider.defaults.transformRequest = [function (data) {
			return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
		}];

	});


	app.factory("utils", function () {
		return {
			getPageRoot: function () {
				return $("body[ng-app]");
			},

			getImage: function (src, callback) {
				var img = new Image();
				img.onload = function () {
					callback(this);
					img = null;
				};
				img.src = src;

			},

			windowHeight: function () {
				return window.innerHeight ? window.innerHeight : $(window).height();
			}
		}
	});

	app.factory("popup", function ($compile, $http, utils) {
		return {

			init: function ($scope, path, callback, options) {

				var self = this;
				options = options || {};
				var opacity = options.opacity || 0.8;
				var backgroundClick = options.backgroundClick || "y";
				var init_animate = options.animate || true;
				var removeWhenClose = options.removeWhenClose || false;
				//if (init_animate == undefined) init_animate = true;
				var background;
				$http.get(path).success(function (data, status, headers, config) {

					var width = $(data).width();
					if ($(data).css("width").indexOf("%") > -1) {
						width = $(window).width() * parseInt($(data).css("width").replace("%", "")) / 100;
					}

					var content = $("<div/>").html(data);
					content.width(width);

					var pop = $("<div class='loading-container'/>");
					pop.css({
						position: "fixed",
						zIndex: 100000,
						width: "100%",
						height: "100%",
						left: 0,
						top: 0,
						opacity: 0.001

					});

					background = $("<div/>");
					background.css({
						position: "absolute",
						"backgroundColor": "#000",
						opacity: opacity,
						width: $(document).width() + "px",
						height: utils.windowHeight() + "px",
						top: 0,
						left: 0
					})
					pop.append(background);
					pop.append(content);

					$(window).resize(function () {
						background.css({ 'width': $(document).width(), 'height': $(document).height() });
						content.css({ 'left': ($(document).width() - content.width()) / 2 });

						if (options.iscroll) {
							var iscroll_container = pop.find(".fn_pop_container");
							iscroll_container.width($(window).width());

							var iscroll_content = pop.find(".fn_pop_content");
							iscroll_content.css({ 'left': ($(document).width() - iscroll_content.width()) / 2 });

						} else {
							content.css({ 'left': ($(document).width() - content.width()) / 2 });
						}
					});

					utils.getPageRoot().append(pop);

					self.loadAll(pop, function () {

						// 리소스 전부 로드되면,
						var left = ($(window).width() - content.width()) / 2;
						var top = (utils.windowHeight() - content.height()) / 2;
						var contentHeight = content.height();

						if (options.iscroll) {
						    var container = pop.find(".fn_pop_container");
						    container.css({"overflow-y": "auto"});
						} else {
							content.css({ left: left + "px", top: utils.windowHeight() + "px", position: "absolute" });
						}

						pop.hide();

						var modal = {
							obj: pop,
							iscroll : null , 
							show: function (animate) {
								$("body").css({ "overflow": "hidden" })

								background.css({ 'width': $(document).width(), 'height': $(document).height() });
								if (animate == undefined) animate = false;
								
								//pop.bind("touchmove", function (e) {
								//	e.preventDefault();
								//});
								
								pop.css({ opacity: 1 });

								content.css({ top: utils.windowHeight() + "px" });
								background.css({ height: utils.windowHeight() + "px" });

								// 주소창의 유무에 따라 높이가 변하기때문에, 노출될때마다 계산
								top = (utils.windowHeight() - contentHeight) / 2;

								if (options.top != undefined)
									top = options.top;

								// iscroll.js 사용인경우
								if (options.iscroll) {

									this._applyIScroll(pop);

								}

								var is_animation = false;
								if (animate)
									is_animation = true;
								else
									is_animation = init_animate;

								if (is_animation) {
									background.css({ opacity: 0.0 });
									content.css({ opacity: 0.0, top: top });
									pop.show();

									background.stop().animate({
										opacity: opacity
									}, 'fast', function () {

										//	content.css({ top: utils.windowHeight() + "px" });
										content.stop().animate({
											//top: top,
											opacity: 1.0
										}, 'fast', function () {
											pop.show();
										});
									});



								} else {
									background.css({ opacity: opacity });
									content.css({ top: top + "px" });
									pop.show();
								}
							},

							_applyIScroll: function (pop) {
								var container = pop.find(".fn_pop_container");
								var content = pop.find(".fn_pop_content");

								container.css({
									"position": "absolute", "z-index": 300000, "overflow-y": "auto",
									width: $(window).width(), height: utils.windowHeight()

								});
								
								content.css({
									"position": "absolute",
									"left": content.css("width") == "100%" ? 0 : ($(window).width() - content.width()) / 2
								})
								
								content.bind("click", function (e) {

									var tagName = e.target.tagName;
									//console.log(tagName);
									if (tagName == "INPUT" || tagName == "LABEL") return true;

									e.preventDefault();
									return false;
								})


								container.bind("click", function (e) {
									//console.log("container click");
									var tagName = e.target.tagName;
									if (tagName == "INPUT" || tagName == "LABEL") return true;
									if (backgroundClick == "y")
										modal.hide();

								})
								
								//setTimeout(function () {
								//	// iscroll 은 radio,checkbox 클릭이 안된다.
								//	//modal.iscroll = new IScroll("#" + container.attr("id"), { click: true, bounce: false, useTransition: false });
								//	modal.iscroll = new iScroll(container.attr("id"), { click: true, bounce: false, useTransition: false });
								//}, 500);
							},

							hide: function (force_animate) {

								$("body").css({ "overflow": "auto" })
								hide_animate = init_animate;
								if (force_animate != undefined)
									hide_animate = force_animate;

								pop.unbind("touchmove");

								if (hide_animate) {

									content.stop().animate({
										//top: utils.windowHeight() + "px"
										opacity: 0.0
									}, 'fast', function () {

									});

									background.stop().animate({
										opacity: 0.0
									}, 'fast', function () {

										if (removeWhenClose) {
											pop.remove();
										} else {
											pop.hide();
										}

									});

								} else {
									pop.hide();
								}

								modal.whenHide();
							},

							whenHide: function () { }
						}

						background.bind("click", function () {
							if (backgroundClick == "y")
								modal.hide();
						})

						callback(modal);

					});
					$compile(pop)($scope);
					//setTimeout(function () {
					//    // iscroll 은 radio,checkbox 클릭이 안된다.
					//    //modal.iscroll = new IScroll("#" + container.attr("id"), { click: true, bounce: false, useTransition: false });
					//    var container = pop.find(".fn_pop_container");
					//    $scope.modalChild.instance.iscroll = new iScroll(container.attr("id"), { click: true, bounce: false, useTransition: false });
					//}, 1000);
				})
			},

			loadAll: function (el, callback) {
				var images = el.find("img");
				var image_count = images.length;

				if (image_count < 1)
					callback();

				$.each(images, function () {

					if (!$(this).attr("src") || $(this).attr("src") == "") {
						image_count--;
						if (image_count < 1) {
							callback();
						}

					} else {
						utils.getImage($(this).attr("src"), function () {

							image_count--;
							if (image_count < 1) {
								callback();
							}
						})
					}
				})


			}

		}
	})

	app.factory("sns", function ($compile, $http, popup) {
		return {

			instance : null , 
			show: function ($rootScope , $event) {
				popup.init($rootScope, "/common/popup/share_pop", function (modal) {
					instance = modal;
					modal.show();

				}, { removeWhenClose : true});

			},

			hide: function ($event) {
				$event.preventDefault();
				if (!instance)
					return;
				instance.hide();

			}

		}
	})

	app.factory("loading", function ($rootScope, $compile, utils) {
		return {

			obj: null,

			show: function ($scope, msg, options) {
				return;
				var self = this;
				options = options || {};
				var opacity = options.opacity || 0.3;

				var content = $("<div class='loading'/>").html("<span>" + msg + "</span>");

				var pop = $("<div class='loading-container loading-layer'/>");
				pop.css({
					position: "fixed",
					zIndex: 100000,
					width: "100%",
					height: "100%",
					left: 0,
					top: 0

				});

				var background = $("<div/>");
				background.css({
					position: "absolute",
					"backgroundColor": "#000",
					opacity: opacity,
					width: $(document).width() + "px",
					height: utils.windowHeight() + "px",
					top: 0,
					left: 0
				})

				pop.append(background);
				pop.append(content);

				utils.getPageRoot().append(pop);
				//$("body").append(pop);

				// 리소스 전부 로드되면,
				var left = ($(document).width() - content.width()) / 2;
				var top = (utils.windowHeight() - content.height()) / 2;

				$compile(pop)($scope);

				obj = pop;

				pop.bind("touchmove", function (e) {
					e.preventDefault();
				});
			},

			hide: function () {

				$(".loading-layer").remove().unbind("touchmove");

			}

		}
	})

	app.factory('paramService', function () {
		return {

			hasParameter: function () {
				var sPageURL = decodeURIComponent(window.location.search.substring(1));
				return sPageURL.split('&').length > 0;
			},

			getParameter: function (sParam) {
				var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

				for (i = 0; i < sURLVariables.length; i++) {
					sParameterName = sURLVariables[i].split('=');

					if (sParameterName[0] === sParam) {
						return sParameterName[1] === undefined ? true : sParameterName[1];
					}
				}
			},

			getParameterValues: function () {

				var sPageURL = decodeURIComponent(window.location.search.substring(1));
				//var sPageURL = window.location.search.substring(1);
				var sURLVariables = sPageURL.split('&')
				var sParameterName = "";
				var result = {};

				for (var i = 0; i < sURLVariables.length; i++) {
					sParameterName = sURLVariables[i].split('=');

					if (sParameterName[0] != "" && sParameterName[1] !== undefined) {
						result[sParameterName[0]] = sParameterName[1] === undefined ? true : sParameterName[1];
					}
				}

				return result;
			}
		};
	});

	app.factory('$address', function ($http) {
		return {
			search: function (param, callback) {
				var self = this;
				var result = { success: true, total: 0, data: [] , hasMore : false};

				$.ajax({
					//url: location.protocol + "//www.juso.go.kr/addrlink/addrLinkApiJsonp.do"
					url: "/api/zipcode.ashx?t=search"
					, data: param
					, success: function (r) {

						if (r.success) {
							var data = $.parseXML(r.data);

							// 에러
							if ($(data).find("errorCode").text() != "0") {
								result.success = false;
							}

							var totalCount = $(data).find("totalCount").text();
							result.total = totalCount;

							$(data).find("juso").each(function () {
								var entity = {
									road: $(this).find("roadAddr").text(),
									jibun: $(this).find("jibunAddr").text(),
									roadAddrPart1: $(this).find("roadAddrPart1").text(),
									roadAddrPart2: $(this).find("roadAddrPart2").text(),
									rnMgtSn: $(this).find("rnMgtSn").text(),
									bdMgtSn: $(this).find("bdMgtSn").text(),
									zipcode: $(this).find("zipNo").text()
								};
								result.data.push(entity);
							});

							result.hasMore = self.hasMore(totalCount, param.currentPage, param.countPerPage);

							if (callback) callback(result);
						} else {
							alert("오류가 발생하였습니다. 잠시 뒤 다시 시도해주세요.");
						}
					}
					, error: function (xhr, status, error) {
						alert("오류가 발생하였습니다. 잠시 뒤 다시 시도해주세요.");
					}
				});
			},

			hasMore: function (total, page, rows) {
				var pageCount = Math.floor(total / rows);
				if (total % rows > 0) pageCount++;
				return page < pageCount;
			}

			
		};
	});

	// 후원페이지 하단 고정 SNS , 바로후원하기 기능
	app.factory("footerSponsorAction", function ($compile, $http, utils, $rootScope) {
		return {

			self: this,
			callback : null , 
			init: function ($scope, frequency, use_option, callback) {

				var self = this;
				self.callback = callback;
				$http.get("/sponsor/pop-pay/" + frequency).success(function (data, status, headers, config) {

					var root = $("[data-id=footer_action]");

					root.append(data);

					$compile(data)($scope);

					initPopPay($scope, self, use_option);

					root.find(".sns-share").click(function () {
						$rootScope.sns.show();
					})

				})
			},

			setPay: function (frequency, amount) {
				
				var self = this;
				self.callback(frequency , amount);
			}



		}
	})

	/*
	background:url('{url}') 에서 사용
	ex : <span class="pic" background-img="{{summary.creator_image}}" data-default-image="/common/img/page/my/no_pic.png" style="background:no-repeat center;background-size:139px;"></span>
	*/
	app.directive('backgroundImg', function ($http) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				
				if (attrs.defaultImage)
					element.css('background-image', "url('" + attrs.defaultImage + "')"); // set default image

				attrs.$observe('backgroundImg', function (url) {
					if (url == "") return;

					var image = new Image();
					image.onerror = function () {
						if (attrs.defaultImage)
							element.css('background-image', "url('" + attrs.defaultImage + "')"); // set default image
					}

					image.onload = function () {
						element.css('background-image', "url('" + url + "')"); // set default image

					};

					image.src = url;
				});
			}
		};
	});

	/*
	특정 영역안에 이미지를 표헌할때 가로/세로 비율에 따라 이미지를 채운다.
	<div class="pic" >
		<fill-img data-image="{{latest.uf_image}}" data-default-image="/common/img/page/my/no_pic.png" />
	</div>
	*/
	app.directive('fillImg', function ($sce) {
		return {
			restrict: 'E',
			link: function (scope, element, attrs) {

				attrs.$observe('image', function (src) {
					
					var image = new Image();
					image.onerror = function () {

						if (attrs.defaultImage) {
							var src = attrs.defaultImage;
							var img = $("<img src='" + src + "' alt='이미지'/>");
							element.append(img);
						}
					}

					image.onload = function () {

				//		console.log("resizeImage", attrs, image.width, image.height, element.parent().width(), element.parent().height());

						var pw = element.parent().width();
						var ph = element.parent().height();
						var w = image.width;
						var h = image.height;
						var new_w = 0;
						var new_h = 0;
						var margin_left = 0;
						var margin_top = 0;

						if (w > h){
							new_w = pw;
							new_h = pw * h / w;
							console.log("xxx" , new_w);
							if (new_h < ph) {
								new_h = ph;
								new_w = ph * w / h;
							}
						} else {
							new_h = ph;
							new_w = ph * w / h;

							if (new_w < pw) {
								new_w = pw;
								new_h = pw * h / w;
							}
						}
						
						margin_left = (pw - new_w) / 2;
						margin_top = (ph - new_h) / 2;

						element.parent().css({"overflow" : "hidden" , "position" : "relative"})
						element.css({ position: "absolute", left: margin_left , top : margin_top });

						var img = $("<img src='" + src + "' alt='이미지'/>");
						img.css({ width: new_w + "px", height: new_h + "px" })
						element.empty();
						element.append(img);

						console.log(new_w, new_h, margin_left);

					};
	
					image.src = src;

				});
			}
		};
	});

	app.directive('ngEnter', function () {
	    return function (scope, element, attrs) {
	        element.bind("keydown keypress", function (event) {
	            if (event.which === 13) {
	                scope.$apply(function () {
	                    scope.$eval(attrs.ngEnter);
	                });

	                event.preventDefault();
	            }
	        });
	    };
	});

	app.filter('percentage', ['$filter', function ($filter) {
		return function (input, decimals) {
			return $filter('number')(input * 100, decimals) + '%';
		};
	}]);
	
	app.filter('trusted', function ($sce) {
		return function (html) {
			return $sce.trustAsHtml(html)
		}
	});

	app.filter('limitHtml', function () {
		return function (text, limit) {

			var changedString = String(text).replace(/<[^>]+>/gm, '');
			var length = changedString.length;

			return changedString.length > limit ? changedString.substr(0, limit - 1) + " ..." : changedString;
		}
	});

	app.run(function ($rootScope, paramService, bridge , sns) {

		$rootScope.setAppInfo = function () {

			$rootScope.isApp = true;
			var entity = $.parseJSON(cookie.get("cps.app"));
			$rootScope.appDevice = entity.device;
			$rootScope.appVersion = entity.version;
			bridge.setRootScope($rootScope);

		};

		$rootScope.isApp = cookie.get("cps.app") ? true : false;
		if ($rootScope.isApp) {
			$rootScope.setAppInfo();
		}

		if (paramService.getParameter("auth-callback") == "Y"){
			console.info("auth.completed", cookie.get("cps.app"));

			// app 에서 호출하는 경우
			if ($rootScope.isApp) {

				$rootScope.setAppInfo();
				if ($("#_appUserInfo").val() != "") {
					bridge.setUserInfo($.parseJSON($("#_appUserInfo").val()));
				}

			//	console.info("앱에서 호출" , entity.device , entity.version);

			}
		};
		
		$rootScope.sns = {
			args : {
				url: "",
				title: "",
				desc: "",
				picture: ""
			},

			show: function ($event, args) {

				if ($event) $event.preventDefault();

				if (args) {
					if (!args.url.startsWith("http")) {
						args.url = location.protocol + "//" + location.host + args.url;
					}
					$rootScope.sns.args = args;
					
				}

				console.log(args);
				sns.show($rootScope);
			},

			hide: function ($event) {
				sns.hide($event);
			}
		}

		$rootScope.logout = function ($event) {
			$event.preventDefault();

			//alert("앱여부 " + $rootScope.isApp)
			if ($rootScope.isApp) {
				bridge.logout();
			}

			setTimeout(function () {
				location.href = "/logout";
			}, 500);


		}

	});

})();


angular.module('cps.menu', []).controller("menuCtrl", function ($rootScope, $scope, $http, $filter, paramService, bridge) {
	/*
	$scope.logout = function ($event) {
		$event.preventDefault();
		
		//alert("앱여부 " + $rootScope.isApp)
		if ($rootScope.isApp) {
			bridge.logout();
		}
		
		setTimeout(function () {
			location.href = "/logout";
		}, 500);
		

	}
	*/
});

angular.module('cps.page', []);
