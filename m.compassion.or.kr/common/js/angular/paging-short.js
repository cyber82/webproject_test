/**
 * @ngDoc directive
 * @name ng.directive:paging
 *
 * @description
 * A directive to aid in paging large datasets
 * while requiring a small amount of page
 * information.
 *
 * @element EA
 *
 */
angular.module('paging-short', []).directive('paging', function () {

    return {

        restrict: 'EA',

        link: fieldLink,
        template: fieldTemplate,
        
        scope: {
            page: '=',
            pageSize: '=',
            total: '=',
            pagingAction: '&'
        }
          
		
    };

    function fieldLink(scope, el, attrs) {

    	scope.prev = function ($event) {
    		$event.preventDefault();
    		if (scope.page > 1) {
    			scope.pagingAction({
    				page: scope.page-1,
    				pageSize: scope.pageSize,
    				total: scope.total
    			});
    		}
    	};

    	scope.next = function ($event) {
    		$event.preventDefault();
    		if (scope.page < scope.tot_page) {
    			scope.pagingAction({
    				page: scope.page+1,
    				pageSize: scope.pageSize,
    				total: scope.total
    			});
    		}
    	};

    	scope.$watchCollection('[page,pageSize,total]', function () {
    		scope.page = parseInt(scope.page) || 1;
    		scope.total = parseInt(scope.total) || 0;

    		scope.tot_page = parseInt(scope.total / scope.pageSize);
    		if (scope.total % scope.pageSize > 0) scope.tot_page++;

    		scope.Hide = (scope.total < 1);
    		
    	});

    	scope.Hide = false;
    }

    function fieldTemplate(el, attrs) {
    	
    	return '<div data-ng-hide="Hide" data-ng-class="" class="page-count-indicator"> ' +
                ' <a class="prev" href="#" ng-click="prev($event)">����</a><span class="recent">{{page}}</span> / {{tot_page}}<a class="next" ng-click="next($event)" href="#">����</a>' +
            '</div>'

                }


});
