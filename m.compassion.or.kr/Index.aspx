﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" MasterPageFile="~/top.Master" %>

<%@ MasterType VirtualPath="~/top.master" %>

<asp:Content runat="server" ID="head_meta" ContentPlaceHolderID="head_meta">

	<meta name="keywords" content="한국컴패션,어린이후원"  />
	<meta name="description" content="꿈을 잃은 어린이들에게 그리스도의 사랑을" />
	<meta property="og:url" content="http://m.compassion.or.kr" />
	<meta property="og:title" content="한국컴패션" />
	<meta property="og:description" content="꿈을 잃은 어린이들에게 그리스도의 사랑을" />
	<meta property="og:image" content="http://m.compassion.or.kr/common/img/sns_default_image.jpg" />


</asp:Content>

<asp:Content ID="head_script" runat="server" ContentPlaceHolderID="head_script">
    <script type="text/javascript" src="/assets/jquery/wisekit/jquery.slides.wisekit.js"></script>
    <script type="text/javascript" src="/default.js?v=07242"></script>
    <script type="text/javascript" src="/assets/jquery/wisekit/cookie.js"></script>
    <script src="/common/js/util/swiper.min.js"></script>
    <link href="/common/css/swiper.css" rel="stylesheet" />

    <script type="text/javascript" src="/popup/popupManager.js"></script>
    <script type="text/javascript">
        $(function () {
            if ($(".intro_visual").is(":visible")) {
                var total = $("#children").val();
                $({ count: 0 }).animate({ count: total }, {
                    duration: 2000,
                    easing: "swing",
                    step: function () {
                        return $("#children_count").text(Math.round(this.count));
                    },
                    complete: function () {
                        return $("#children_count").text(total);
                    }
                });
            }
        })
    </script>

    <script type="text/javascript"> 

      var _ntp = {}; 

      _ntp.host = (('https:' == document.location.protocol) ? 'https://' : 'http://') 

      _ntp.dID = 779; 

      document.write(unescape("%3Cscript src='" + _ntp.host + "nmt.nsmartad.com/content?cid=1' type='text/javascript'%3E%3C/script%3E")); 

    </script> 

    <script>

     callback = function(){}

    </script>

</asp:Content>
<asp:Content runat="server" ID="header_top" ContentPlaceHolderID="header_top">
    <input type="hidden" id="children" runat="server" value="0" />

<!-- intro -->
    <section class="intro-visual intro_visual" style="display: none; position: relative">
        <div class="wrap-visual"></div>
        <span class="logo">
            <img src="/common/img/common/logo1.png" alt="꿈을 일은 어린이들에게 그리스도의 사랑을 Compassion" /></span>
            <script>
                function getHostM() {
                    var host = '';
                    var loc = String(location.href);
                    if (loc.indexOf('compassion.or.kr') != -1) host = 'm.compassion.or.kr';
                    else if (loc.indexOf('compassionkr.com') != -1) host = 'm.compassionkr.com';
                    else if (loc.indexOf('localhost') != -1) host = 'localhost:35563';
                    return host;
                }

                function goLinkM() {
                    var host = getHostM();
                    if (host != '')
                        location.href = 'http://' + host + '/sponsor/children/';//?e_id=2&e_src=mWEB_intro&utm_source=mWEB_intro&utm_campaign=20170401_easter&utm_medium=mWEB_intro';
                    //if (host != '') location.href = 'http://' + host + '/flower/?e_id=13&e_src=2017flower_WEB_intro&utm_source=WEB_intro&utm_campaign=2017flower​&utm_medium=WEB';//';
                    //if (host != '') location.href = 'http://' + host + '/flower/?e_id=13&e_src=2017flower_mWEB_intro&utm_source=mWEB_intro&utm_campaign=2017flower​&utm_medium=mWEB';//';
                }
            </script>
            <%--<a style="left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute;" href="javascript:goLinkM()" onclick="_nto.callTrack('5566', callback());"></a>--%>
        <a style="left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute;" href="javascript:goLinkM()"></a>

        <div class="count">
            <em id="children_count">0</em>
            <p class="txt">
                <asp:Literal runat="server" ID="main_content"></asp:Literal></p>
            <a href="/sponsor/children/" class="bt-type1">어린이 만나러 가기</a>
        </div>
        <div class="scroll">
            <img src="/common/img/main/arrow-scroll.png" alt="Scroll Down" /></div>
    </section>

    <!--// intro -->

</asp:Content>

<asp:Content runat="server" ID="body" ContentPlaceHolderID="body">

    <!--1:1어린이양육-->
    <asp:PlaceHolder runat="server" ID="ph_child_birthday">
        <section class="main-field1">
            <h2>1:1어린이양육</h2>
            <div class="pos-photo">
                <span id="main_recmd1_pic" runat="server" class="photo" style="background: url('http://ws.compassion.or.kr/Files/Child/GU/GU4980204.jpg') no-repeat"></span>

            </div>
            <ul class="child-info">
                <li>
                    <asp:Literal runat="server" ID="main_recmd1_country" /></li>
                <li>
                    <asp:Literal runat="server" ID="main_recmd1_age" />세</li>
                <li>
                    <asp:Literal runat="server" ID="main_recmd1_gender" /></li>
            </ul>


            <div class="child-txtintro">
                <asp:PlaceHolder runat="server" ID="ph_recmd1" Visible="false">
                <p class="txt1">어제는 <strong>저의 생일</strong>이었어요</p>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="ph_recmd2" Visible="false">
                <p class="txt1">내일을 꿈꾸고 싶어요</p>
                </asp:PlaceHolder>
                <p class="txt2">
                    안녕하세요, <strong>
                        <asp:Literal runat="server" ID="main_recmd1_country2" /></strong>에 사는 <strong>
                            <asp:Literal runat="server" ID="main_recmd1_name" /></strong>입니다.<br />
                    저는 오늘로 <strong>
                        <asp:Literal runat="server" ID="main_recmd1_waitingdays" /></strong>일째 후원자님을 기다리고 있어요.<br />
                    지난 생일은 후원자님과 함께하지 못했지만,<br />
                    <asp:Literal runat="server" ID="main_recmd1_age2" />살이 된 오늘은 함께할 수 있을까요?
                </p>
            </div>


            <a class="bt-type2" href="/sponsor/children/?c={childMasterId}" runat="server" id="main_recmd1_link">
                <asp:Literal runat="server" ID="main_recmd1_name2" />에 대해 자세히 알고 싶어요</a>
            <a class="bt-type1" href="/sponsor/children/">다른 어린이 만나러 가기</a>
        </section>
    </asp:PlaceHolder>

    <div class="main-linkgo">
        <a href="/sponsor/children/">1:1어린이양육</a>
        <a href="/sponsor/special/">특별한 나눔</a>
        <a href="/sponsor/user-funding/">나눔펀딩</a>
    </div>

    <!--가정의달캠페인-->
    <section class="main-field2">
        <h2>가정의 달 캠페인</h2>
        <div class="wrap-slider swiper-container">
            <div class="swiper-wrapper">

                <asp:Repeater runat="server" ID="repeater_event">
                    <ItemTemplate>

                        <div class="swiper-slide"><a href="<%#Eval("mp_image_link") %>" target="<%#Eval("mp_is_new_window").ToString() == "True" ? "_blank" : "_self" %>" onclick="_nto.callTrack('5567', callback());">
                            <img src="<%#Eval("mp_image").ToString().WithFileServerHost()%>" alt="" width="100%" /></a></div>

                    </ItemTemplate>
                </asp:Repeater>
            </div>


            <div class="page-prev">이전보기</div>
            <div class="page-next">다음보기</div>
            <div class="page-count">
                <span class="recent">1페이지</span>
                <span>2페이지</span>
                <span>3페이지</span>
            </div>
        </div>
    </section>

    <!--꿈을 잃은 어린이들에게-->
    <section class="main-field3">
        <h2>꿈을 잃은 어린이들에게<span>그리스도의 사랑을</span></h2>

        <img src="/common/img/main/church1.png" alt="" class="img1" />
        <strong>WE LOVE JESUS</strong>
        <p>
            컴패션은 예수님이 맡겨주신 사명에 따라, 그 분의 사랑을
	바탕으로 어린이들을 가난으로부터 자유롭게 합니다.
        </p>

        <img src="/common/img/main/church2.png" alt="" class="img2" />
        <strong>WE LOVE CHILDREN</strong>
        <p>컴패션의 모든 사역은 어린이를 중심으로 진행됩니다.</p>

        <img src="/common/img/main/church3.png" alt="" class="img3" />
        <strong>WE LOVE THE CHURCH</strong>
        <p>
            컴패션은 교회를 통해 일합니다.
    25개 수혜국들의 현지 협력교회와 파트너십을 맺고
    현지 교회가 어린이들을 직접 양육할 수 있도록 돕습니다.
        </p>
    </section>

    <!--컴패션후원금은 이렇게 사용됩니다.-->
   <section class="main-field4">
		<h2><strong>컴패션 후원금</strong>은 이렇게 <strong>사용</strong>됩니다!</h2>
		<div class="wrap-graph">
    		<div class="graph">
				<img src="/common/img/main/graph.png" alt="87.5%" />
				<p class="txt1">
					어린이 양육프로그램(86.6%)<br />
					보조활동비(13.4%)
				</p>
			</div>
       		<p class="txt1"><strong>정직하고 효율적인</strong> 후원금 사용을 위한 <strong>80:20 원칙</strong></p>
			<p class="txt2">컴패션은 후원금의 80% 이상을 반드시 어린이양육을 위해 사용한다는 원칙을 철저히 지킴으로 후원금을 효율적이고 정직하게 사용하고자 합니다. 
			이를 지키기 위해 전 세계 컴패션의 각 수혜국과 후원국은 <strong>정기적으로 엄격한 내·외부 감사</strong>를 받고 있습니다.</p>
		</div>
		<div class="wrap-bt"><a class="bt-type3" href="/customer/faq/?k_word=후원금">후원금 FAQ</a></div>
	</section>

    <!--후원이 가져온 변화?-->
    <section class="main-field5">
        <h2><strong>후원이 가져온 변화</strong>를 느껴 보세요</h2>
        <div class="wrap-slider swiper-container2">
            <div class="swiper-wrapper">
                <asp:Repeater runat="server" ID="repeater_sympathy">
                    <ItemTemplate>
                        <!--slider item per-->
                        <div class="item-field swiper-slide">
                            <div class="photo" style="background: url('<%#Eval("thumb") == null ? "" : Eval("thumb").ToString().WithFileServerHost() %>') no-repeat center top; background-size: cover;"></div>
                            <div class="caption textcrop-1row"><%#Eval("board_name") %></div>
                            <div class="txt">
                                <p class="textcrop-1row"><%#Eval("title") %></p>
                                <p class="textcrop-1row"><%#Eval("sub_title") %></p>
                            </div>
                            <div class="wrap-bt"><a class="bt-type4" href="/sympathy/view/<%#Eval("board_id") %>/<%#Eval("idx") %>">더보기</a></div>
                        </div>
                        <!--//slider item per-->

                    </ItemTemplate>
                </asp:Repeater>
            </div>


            <div class="page-prev">이전보기</div>
            <div class="page-next">다음보기</div>
        </div>
    </section>

    <!--컴패션 블루코너-->
    <section class="main-field6">
        <div class="ic_blue">
            <img src="/common/img/main/icon_blue.png" alt="컴패션 불루 아이콘" /></div>
        <h2>컴패션 블루코너</h2>
        <p>
            너희 땅의 곡물을 벨 때에 밭 모퉁이까지 다 베지 말며 떨어진 
    것을 줍지 말고 그것을 가난한 자와 거류민을 위하여 남겨두라
    나는 너희의 하나님 여호와이니라
        </p>
        <em>- 레위기 23:22</em>
        <a href="/about-us/about/ci/#blue" class="bt-type1">자세히 보기</a>
    </section>

    <!--모바일 출첵-->
    <section class="main-field7">
        <a href="/mobile-attendance/">
            <h2>모바일 출첵</h2>
            <p>오프라인에서 활동하고, 모바일에서 확인하자!</p>
        </a>
    </section>



</asp:Content>

