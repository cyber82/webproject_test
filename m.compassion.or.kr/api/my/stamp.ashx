﻿<%@ WebHandler Language="C#" Class="api_my_stamp" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_my_stamp : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest( HttpContext context ) {

		var t = context.Request["t"].EmptyIfNull();

		// 스탬프조회 가능한년도
		if(t == "years") {
			this.GetYears(context);
			// 스탬프 목록
		} else if(t == "list") {
			this.GetList(context);

		}
	}

	void GetList(HttpContext context ) {
		var year = context.Request["year"].ValueIfNull(DateTime.Now.ToString("yyyy"));
		new StampAction().GetList(year).Write(context);
	}

	void GetYears(HttpContext context ) {
		new StampAction().GetYears().Write(context);
	}

}