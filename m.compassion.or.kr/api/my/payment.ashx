﻿<%@ WebHandler Language="C#" Class="api_my_payment" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_my_payment : BaseHandler {
	public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

	public override void OnRequest( HttpContext context ) {

		var t = context.Request["t"].EmptyIfNull();

		if(t == "history") {
			this.GetHistoryList(context);
		} else if(t == "reservation") {
			this.GetReservationList(context);
		} else if(t == "gift-reservation") {
			this.GetGiftReservationList(context);
		} else if(t == "gift-payment") {
			this.GetGiftPaymentList(context);
		} else if(t == "gift-giftable") {
			this.GiftableChildCount(context);
		} else if(t == "gift-delete-reservation") {
			this.GiftDeleteReservation(context);
		} else if(t == "way") {
			this.GetPaymentWay(context);
		} else if(t == "virtual-account") {
			this.GetVirtualAccountList(context);
		} else if(t == "set-virtual-account") {
			this.SetVirtualAccount(context);
		} else if(t == "get-monthly-history-by-commitmentId") {
			this.GetMonthlyHistoriesByCommitmentId(context);
		} 
	}

	void GiftDeleteReservation( HttpContext context ) {
		var commitmentId = context.Request["c"].EmptyIfNull();
		new PaymentAction().DeleteGiftReservation(commitmentId).Write(context);

	}

	void GiftableChildCount( HttpContext context ) {

		new PaymentAction().GiftableChildCount().Write(context);

	}

	void GetGiftReservationList( HttpContext context ) {

		var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
		var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
		var childMasterId = context.Request["childMasterId"].EmptyIfNull();
		new PaymentAction().GetGiftReservations(page, rowsPerPage , childMasterId).Write(context);

	}

	void GetGiftPaymentList( HttpContext context ) {

		var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
		var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
		var childMasterId = context.Request["childMasterId"].EmptyIfNull();
		new PaymentAction().GetGiftPayments(page, rowsPerPage , childMasterId).Write(context);

	}

	void GetMonthlyHistoriesByCommitmentId( HttpContext context ) {
		var commitmentId = context.Request["c"].EmptyIfNull();
		var year = context.Request["year"].EmptyIfNull();

		if(string.IsNullOrEmpty(commitmentId)) {
			new JsonWriter() { success = false, message = "commitmentId가 필요합니다." }.Write(context);
			return;
		}

		new PaymentAction().GetMonthlyHistoriesByCommitmentId(commitmentId, year).Write(context);

	}

	void GetHistoryList( HttpContext context ) {

		var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
		var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
		var startDate = context.Request["date_begin"].EmptyIfNull();
		var endDate = context.Request["date_end"].EmptyIfNull();

		new PaymentAction().GetHistories(page, rowsPerPage, startDate, endDate).Write(context);

	}

	void GetReservationList( HttpContext context ) {

		var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
		var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
		new PaymentAction().GetReservations(page, rowsPerPage).Write(context);

	}

	void GetPaymentWay( HttpContext context ) {
		new PaymentAction().GetPaymentWay().Write(context);
	}

	void GetVirtualAccountList( HttpContext context ) {
		new PaymentAction().GetVirtualAccounts().Write(context);
	}

	void SetVirtualAccount( HttpContext context ) {

		var bankCode = context.Request["bankCode"].EmptyIfNull();
		var bankName = context.Request["bankName"].EmptyIfNull();

		if(string.IsNullOrEmpty(bankCode) || string.IsNullOrEmpty(bankName)) {
			new JsonWriter() { success = false, message = "은행정보가 필요합니다." }.Write(context);
			return;
		}

		var action = new PaymentAction();
		var result = action.SetVirtualAccount(bankCode, bankName);
		if(result.success) {

			var list = action.GetVirtualAccounts();
			result.data = list.data;
			result.Write(context);

		} else {
			result.Write(context);
		}

	}
}