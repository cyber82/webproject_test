﻿<%@ WebHandler Language="C#" Class="api_board" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_board : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();
        if(t == "list") {
            this.GetList(context);
        }else if(t == "apply") {
            this.Apply(context);
        }else if(t == "cancel") {
            this.Cancel(context);
        }else if(t == "confirm") {
            this.Confirm(context);
        }
    }

    void Confirm(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;
        UserInfo sess = new UserInfo();

        var user_id = sess.UserId;
        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var name = sess.UserName;

        if (id == -1)
        {
            result.success = false;
            result.message = "잘못 된 요청입니다. 이벤트 ID가 없습니다.";
            JsonWriter.Write(result, context);
            return;
        }


        using (MainLibDataContext dao = new MainLibDataContext())
        {
            //dao.event_winner.DeleteAllOnSubmit(dao.event_winner.Where(p => p.ew_e_id == Convert.ToInt32(id) && p.ew_user_id == user_id));
            //www6.cud(MakeSQL.delQ(0, "event_winner", "ew_e_id", Convert.ToInt32(id), "ew_user_id", user_id));
            var list = www6.selectQ<event_winner>("ew_e_id", Convert.ToInt32(id), "ew_user_id", user_id);
            www6.delete(list);

            var entity = new event_winner()
            {
                ew_e_id = Convert.ToInt32(id),
                ew_user_id = user_id.ToString(),
                ew_user_name = name.ToString(),
                ew_regdate = DateTime.Now
            };
            //dao.event_winner.InsertOnSubmit(entity);
            www6.insert(entity);

            //dao.SubmitChanges();
            result.message = "확정되었습니다..";
        }

        JsonWriter.Write(result, context);
    }


    void GetList(HttpContext context)
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var k_word = context.Request["k_word"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_event_list_f(page, rowsPerPage, k_word).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword" };
            Object[] op2 = new Object[] { page, rowsPerPage, k_word };
            var list = www6.selectSP("sp_event_list_f", op1, op2).DataTableToList<sp_event_list_fResult>();

            foreach (var item in list)
            {
                item.e_thumb = item.e_thumb.WithFileServerHost();
                item.e_thumb_m = item.e_thumb_m.WithFileServerHost();
            }
            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void Apply(HttpContext context) {

        JsonWriter result = new JsonWriter() {
            success = true
        };

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        UserInfo sess = new UserInfo();

        var user_id = new UserInfo().UserId;
        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var name = context.Request["name"].EmptyIfNull();
        var phone = context.Request["phone"].EmptyIfNull();
        var count = Convert.ToInt32(context.Request["count"].ValueIfNull("1"));
        var date = context.Request["date"].EmptyIfNull();
        var route = context.Request["route"].EmptyIfNull();
        var title = context.Request["title"].EmptyIfNull();
        var request_contents = context.Request["request_contents"].EmptyIfNull();

        if (string.IsNullOrEmpty(route))
        {
            route = context.Request["e_src"].EmptyIfNull();
        }

        if(user_id == "" || id == -1 || name == "" || phone == "") {
            result.success = false;
            result.message = "필수정보가 부족합니다.1";
            JsonWriter.Write(result, context);
            return;
        }


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var eventEntity = dao.@event.FirstOrDefault(p => p.e_id == id);
            var eventEntity = www6.selectQF<@event>("e_id", id);

            if (eventEntity == null)
            {
                result.success = false;
                result.message = "잘못된 요청입니다.";
                JsonWriter.Write(result, context);
                return;
            }

            if (eventEntity.e_type == "experience" && date == "")
            {
                result.success = false;
                result.message = "필수정보가 부족합니다.2";
                JsonWriter.Write(result, context);
                return;
            }
            if (eventEntity.e_target == "sponsor")
            {
                var commitInfo = new SponsorAction().GetCommitInfo();
                if (!commitInfo.success)
                {
                    JsonWriter.Write(commitInfo, context);
                    return;
                }
                if (sess.ConId.Length != 5 && sess.ConId.Length != 6)
                {
                    result.success = false;
                    result.message = "후원자만 신청 가능한 이벤트입니다.";
                    JsonWriter.Write(result, context);
                    return;
                }
            }

            //if(eventEntity.e_closed || Convert.ToDateTime(eventEntity.e_end).CompareTo(DateTime.Now) < 0) {
            //    result.success = false;
            //    result.message = "지원이 마감되었습니다.";
            //    JsonWriter.Write(result, context);
            //    return;
            //}

            // 2017.11.13 김태형 : #12549
            // 이석호과장의 요청으로 문희원과장이 주석 처리하였으나..처험전의 경우만 체크하도록 주석 제거함
            if (eventEntity.e_type.Equals("experience"))
            {
                if (eventEntity.e_closed || Convert.ToDateTime(eventEntity.e_end).CompareTo(DateTime.Now) < 0)
                {
                    result.success = false;
                    result.message = "지원이 마감되었습니다.";
                    JsonWriter.Write(result, context);
                    return;
                }
            }

            if (eventEntity.e_type.Equals("experience"))
            {
                //var applyList = dao.sp_event_apply_count_list_f(id).Where(p => p.er_date == Convert.ToDateTime(date)).FirstOrDefault();
                Object[] op1 = new Object[] { "e_id" };
                Object[] op2 = new Object[] { id };
                var list = www6.selectSP("sp_event_apply_count_list_f", op1, op2).DataTableToList<sp_event_apply_count_list_fResult>();
                var applyList = list.Where(p => p.er_date == Convert.ToDateTime(date)).FirstOrDefault();

                if (applyList != null)
                {
                    if (applyList.apply_count + 1 > eventEntity.e_maximum)
                    {
                        result.success = false;
                        result.message = "선택하신 일시에 지원할 수 있는 팀 수를 초과하였습니다.\n남은 수는 " + (eventEntity.e_maximum - applyList.apply_count).ToString() + "팀입니다.";
                        JsonWriter.Write(result, context);
                        return;
                    }
                    if (applyList.apply_person_count + count > eventEntity.e_maximum_person)
                    {
                        result.success = false;
                        result.message = "선택하신 일시에 지원할 수 있는 인원 수를 초과하였습니다.\n남은 수는 " + (eventEntity.e_maximum_person - applyList.apply_person_count).ToString() + "명입니다.";
                        JsonWriter.Write(result, context);
                        return;
                    }

                }

            }


            var updateType = "add";
            //var entity = dao.event_apply.FirstOrDefault(p => p.er_e_id == id && p.er_user_id == user_id);
            var entity = www6.selectQF<event_apply>("er_e_id", id, "er_user_id", user_id);

            if (entity == null)
            {
                // 날짜 선택
                if (eventEntity.e_type == "date")
                {
                    //var maximum = dao.event_timetable.Where(p => p.et_e_id == id && p.et_date == Convert.ToDateTime(date)).First().et_limit; // 시간대 최대 가능 인원수
                    var maximum = www6.selectQF<event_timetable>("et_e_id", id, "et_date", Convert.ToDateTime(date)).et_limit;

                    //var apply_count = dao.event_apply.Count(p => p.er_e_id == id && p.er_date == Convert.ToDateTime(date));
                    var apply_count = www6.selectQ<event_apply>("er_e_id", id, "er_date", Convert.ToDateTime(date)).Count;

                    if (apply_count >= maximum)
                    {
                        result.success = false;
                        result.message = "해당 시간은 현재 마감되었습니다.\n다른 시간을 선택해주세요.";
                        JsonWriter.Write(result, context);
                        return;
                    }
                }

                if (eventEntity.e_type == "count" && eventEntity.e_limit != -1)
                {
                    // 선착순
                    var apply_count = 0;
                    var exist = www6.selectQ2<event_apply>("er_e_id = ", id, "er_count != ", -1);
                    //if (dao.event_apply.Any(p => p.er_e_id == id && p.er_count != -1))
                    if(exist.Any())
                    {
                        apply_count = exist.Sum(p=> p.er_count); //dao.event_apply.Where(p => p.er_e_id == id && p.er_count != -1).Sum(p => p.er_count);
                    }

                    if (apply_count + count > eventEntity.e_limit)
                    {
                        result.success = false;
                        if (eventEntity.e_limit - apply_count == 0)
                        {
                            result.message = "해당 시간은 현재 마감되었습니다.\n다른 시간을 선택해주세요.";
                        }
                        else
                        {
                            result.message = "현재 " + (eventEntity.e_limit - apply_count) + "명까지 신청 가능합니다.";
                        }
                        JsonWriter.Write(result, context);
                        return;
                    }

                    // 선착순 인원이 다 찬경우 이벤트 마감시킴
                    if (apply_count + count >= eventEntity.e_limit)
                    {
                        eventEntity.e_closed = true;
                    }
                }

                if (eventEntity.e_type == "complex")
                {
                    //var maximum = dao.event_timetable.Where(p => p.et_e_id == id && p.et_date == Convert.ToDateTime(date)).First().et_limit; // 시간대 최대 가능 인원수
                    var maximum = www6.selectQF<event_timetable>("et_e_id", id, "et_date", Convert.ToDateTime(date)).et_limit;

                    var apply_count = 0;
                    var exist = www6.selectQ2<event_apply>("er_e_id = ", id, "er_date", Convert.ToDateTime(date), "er_count != ", -1);
                    //if (dao.event_apply.Any(p => p.er_e_id == id && p.er_date == Convert.ToDateTime(date) && p.er_count != -1))
                    if(exist.Any())
                    {
                        apply_count = exist.Sum(p=> p.er_count); //dao.event_apply.Where(p => p.er_e_id == id && p.er_date == Convert.ToDateTime(date) && p.er_count != -1).Sum(p => p.er_count);
                    }
                    if (apply_count + count > maximum)
                    {
                        result.success = false;
                        if (maximum - apply_count == 0)
                        {
                            result.message = "해당 시간은 현재 마감되었습니다.\n다른 시간을 선택해주세요.";
                        }
                        else
                        {
                            result.message = "현재 " + (maximum - apply_count) + "명까지 신청 가능합니다.";
                        }
                        JsonWriter.Write(result, context);
                        return;
                    }
                }

                entity = new event_apply()
                {
                    er_e_id = id,
                    er_user_id = user_id,
                    er_name = name,
                    er_phone = phone,
                    er_regdate = DateTime.Now,
                    er_count = count,
                    er_route = route,
                    er_a_id = -1,
                    er_request_contents = request_contents
                };

                if (date != "") entity.er_date = Convert.ToDateTime(date);
                //dao.event_apply.InsertOnSubmit(entity);
                www6.insert(entity);


            }
            else
            {
                // 날짜 선택
                if (eventEntity.e_type == "date")
                {
                    //var maximum = dao.event_timetable.Where(p => p.et_e_id == id && p.et_date == Convert.ToDateTime(date)).First().et_limit; // 시간대 최대 가능 인원수
                    var maximum = www6.selectQF<event_timetable>("et_e_id", id, "et_date", Convert.ToDateTime(date)).et_limit;

                    //var apply_count = dao.event_apply.Count(p => p.er_e_id == id && p.er_date == Convert.ToDateTime(date) && p.er_user_id != user_id);
                    var apply_count = www6.selectQ2<event_apply>("er_e_id = ", id, "er_date = ", Convert.ToDateTime(date), "er_user_id != ", user_id).Count;

                    if (apply_count >= maximum)
                    {
                        result.success = false;
                        result.message = "해당 시간은 현재 마감되었습니다.\n다른 시간을 선택해주세요.";
                        JsonWriter.Write(result, context);
                        return;
                    }
                }


                if (eventEntity.e_type == "count" && eventEntity.e_limit != -1)
                {
                    // 선착순
                    var apply_count = 0;
                    var exist = www6.selectQ2<event_apply>("er_e_id = ", id, "er_count != ", -1, "er_user_id !=", user_id);
                    //if (dao.event_apply.Any(p => p.er_e_id == id && p.er_count != -1 && p.er_user_id != user_id))
                    if(exist.Any())
                    {
                        apply_count = exist.Sum(p => p.er_count); //dao.event_apply.Where(p => p.er_e_id == id && p.er_count != -1 && p.er_user_id != user_id).Sum(p => p.er_count);
                    }

                    if (apply_count + count > eventEntity.e_limit)
                    {
                        result.success = false;
                        if (eventEntity.e_limit - apply_count == 0)
                        {
                            result.message = "해당 시간은 현재 마감되었습니다.\n다른 시간을 선택해주세요.";
                        }
                        else
                        {
                            result.message = "현재 " + (eventEntity.e_limit - apply_count) + "명까지 신청 가능합니다.";
                        }
                        JsonWriter.Write(result, context);
                        return;
                    }

                    // 선착순 인원이 다 찬경우 이벤트 마감시킴
                    if (apply_count + count >= eventEntity.e_limit)
                    {
                        eventEntity.e_closed = true;
                    }
                }


                if (eventEntity.e_type == "complex")
                {
                    //var maximum = dao.event_timetable.Where(p => p.et_e_id == id && p.et_date == Convert.ToDateTime(date)).First().et_limit; // 시간대 최대 가능 인원수
                    var maximum = www6.selectQF<event_timetable>("et_e_id", id, "et_date", Convert.ToDateTime(date)).et_limit;

                    var apply_count = 0;
                    var exist = www6.selectQ2<event_apply>("er_e_id = ", id, "er_date = ", Convert.ToDateTime(date), "er_count != ", -1, "er_user_id !=", user_id);
                    //if (dao.event_apply.Any(p => p.er_e_id == id && p.er_date == Convert.ToDateTime(date) && p.er_count != -1 && p.er_user_id != user_id))
                    if(exist.Any())
                    {
                        apply_count = exist.Sum(p => p.er_count); //dao.event_apply.Where(p => p.er_e_id == id && p.er_date == Convert.ToDateTime(date) && p.er_count != -1 && p.er_user_id != user_id).Sum(p => p.er_count);
                    }
                    if (apply_count + count > maximum)
                    {
                        result.success = false;
                        if (maximum - apply_count == 0)
                        {
                            result.message = "해당 시간은 현재 마감되었습니다.\n다른 시간을 선택해주세요.";
                        }
                        else
                        {
                            result.message = "현재 " + (maximum - apply_count) + "명까지 신청 가능합니다.";
                        }

                        JsonWriter.Write(result, context);
                        return;
                    }
                }

                entity.er_name = name;
                entity.er_phone = phone;
                entity.er_count = count;
                entity.er_route = route;
                entity.er_request_contents = request_contents;
                if (date != "") entity.er_date = Convert.ToDateTime(date);
                updateType = "update";

                www6.update(entity);
            }

            //dao.SubmitChanges();
            www6.update(eventEntity);

            // 앞날짜가 모두 마감이면 마감처리
            if (eventEntity.e_type != "experience")
            {
                //var list = dao.sp_event_apply_date_list_f(id).Where(p => p.et_date > DateTime.Now);
                Object[] op1 = new Object[] { "e_id" };
                Object[] op2 = new Object[] { id };
                var list = www6.selectSP("sp_event_apply_date_list_f", op1, op2).DataTableToList<sp_event_apply_date_list_fResult>().Where(p => p.et_date > DateTime.Now);

                //var listCount = dao.sp_event_apply_date_list_f(id).Where(p => p.et_date > DateTime.Now).Count();
                Object[] op3 = new Object[] { "e_id" };
                Object[] op4 = new Object[] { id };
                var listCount = www6.selectSP("sp_event_apply_date_list_f", op3, op4).DataTableToList<sp_event_apply_date_list_fResult>().Where(p => p.et_date > DateTime.Now).Count();

                var closed = listCount > 0;
                foreach (var data in list)
                {
                    if (data.et_limit > data.apply_count) closed = false;
                }

                if (!eventEntity.e_closed && closed)
                {
                    eventEntity.e_closed = true;
                    //dao.SubmitChanges();
                    www6.update(eventEntity);
                }
            }

            // 예약 확인 문자 발송
            if (eventEntity.e_type == "experience")
            {
                SendSMS(eventEntity, entity, updateType, context);
            }

            /*         이벤트 정보 등록  -- 20170327 이정길       */
            string sponsorType = "이벤트신청(" + title + ")";
            string sponsorTypeEng = "EVENT_SUBMIT";
            var eventID = context.Request["e_id"].EmptyIfNull();
            var eventSrc = context.Request["e_src"].EmptyIfNull();

            if (eventID == null)
            {
                if (context.Request.Cookies["e_id"] != null)
                {
                    eventID = context.Request.Cookies["e_id"].Value;
                    eventSrc = context.Request.Cookies["e_src"].Value;
                }
            }

            try
            {
                using (FrontDataContext dda = new FrontDataContext())
                {
                    if (eventID != null && eventSrc != null && !eventID.Equals(""))
                    {
                        string sponsorID = sess.SponsorID;
                        string userID = sess.UserId;
                        if (sponsorID == "")
                        {
                            using (AuthDataContext data = new AuthDataContext())
                            {
                                //var sm = data.tSponsorMaster.First(p => p.UserID == userID);
                                var sm = www6.selectQF<tSponsorMaster>("UserID", userID);
                                sponsorID = sm.tempSponsorID;
                            }
                        }

                        //dda.sp_tEventApply_insert_f(int.Parse(eventID.ToString()),
                        //                            new UserInfo().UserId,
                        //                            sponsorID,
                        //                            new UserInfo().ConId,
                        //                            eventSrc.ToString(),
                        //                            sponsorType,
                        //                            sponsorTypeEng,
                        //                            0,
                        //                            "");
                        //dda.SubmitChanges();
                        Object[] op1 = new Object[] { "evt_id", "UserID", "SponsorID", "ConID", "evt_route", "SponsorType", "SponsorTypeEng", "SponsorAmount", "CommitmentID" };
                        Object[] op2 = new Object[] { int.Parse(eventID.ToString()), new UserInfo().UserId, sponsorID, new UserInfo().ConId, eventSrc.ToString(), sponsorType, sponsorTypeEng, 0, ""};
                        www6.selectSP("sp_tEventApply_insert_f", op1, op2);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.Write(HttpContext.Current, 0, "이벤트신청 이벤트 정보 등록 error :: " + ex.Message);
            }
        }

        JsonWriter.Write(result, context);
    }


    void SendSMS(@event eventEntity, event_apply entity, string updateType, HttpContext context) {
        try {
            var smsMessage = "";
            if (updateType == "add") {
                smsMessage = "[컴패션] "+entity.er_name+"님, 체험전 예약신청이 완료되었습니다. "+Convert.ToDateTime(entity.er_date).ToString("M\\/d(ddd) ttHH:mm")+" / "+entity.er_count+"명 감사합니다";
            }else {
                smsMessage = "[컴패션] "+entity.er_name+"님, 체험전 예약신청이 변경되었습니다. "+Convert.ToDateTime(entity.er_date).ToString("M\\/d(ddd) ttHH:mm")+" / "+entity.er_count+"명 감사합니다";
            }
            util.SendSMS.SMSSend(entity.er_phone, entity.er_name, smsMessage);
        } catch (Exception e) {
            ErrorLog.Write(context , 0 , e.ToString());
        }
    }

    void Cancel(HttpContext context)
    {
        JsonWriter result = new JsonWriter()
        {
            success = true
        };

        if (!UserInfo.IsLogin)
        {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var user_id = new UserInfo().UserId;
        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));

        if (user_id == "" || id == -1)
        {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            // 데이터 삭제
            //var entity = dao.event_apply.First(p => p.er_e_id == id && p.er_user_id == user_id);
            //dao.event_apply.DeleteOnSubmit(entity);
            //
            //dao.SubmitChanges();
            //www6.cud(MakeSQL.delQ(0, "event_apply", "er_e_id", id, "er_user_id", user_id));
            var entity = www6.selectQF<event_apply>("er_e_id", id, "er_user_id", user_id);
            www6.delete(entity);
        }

        JsonWriter.Write(result, context);
    }

}