﻿<%@ WebHandler Language="C#" Class="api_app" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;

using System.Net.Http;
using System.Xml;


public class api_app : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();
        if(t == "search") {
            this.Search(context);
        }
    }

    void Search(HttpContext context){
        var keyword = HttpUtility.UrlEncode(context.Request["keyword"].EmptyIfNull());
        var currentPage = context.Request["currentPage"].ValueIfNull("1");
        var countPerPage = context.Request["countPerPage"].ValueIfNull("10");
        var apikey = ConfigurationManager.AppSettings["addressApiKey"];

        JsonWriter result = new JsonWriter() {
            success = false
        };

        if (keyword == "") {
            result.Write(context);
            return;
        }

        using (WebClient wc = new WebClient()) {
            wc.Encoding = Encoding.UTF8;

            try {
                var data = wc.DownloadString(string.Format("http://www.juso.go.kr/addrlink/addrLinkApi.do?currentPage="+currentPage+"&countPerPage="+countPerPage+"&keyword="+keyword+"&confmKey="+apikey));
                result.data = data;
                result.success = true;
            } catch(Exception e) {
                ErrorLog.Write(context, 0, e.Message);
                result.message = "오류발생";
            }

        };
        result.Write(context);


    }




}