﻿<%@ WebHandler Language="C#" Class="api_mobile_attendance" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_mobile_attendance : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "list") {
            this.GetList(context);
        } else if(t == "check") {
            this.CheckId(context);
        } else if(t == "add") {
            this.Add(context);
        }
    }

    void GetList( HttpContext context )
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));


        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_mobile_attendance_list_f(page, rowsPerPage, "", "", "").ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "keyword", "startdate", "enddate" };
            Object[] op2 = new Object[] { page, rowsPerPage, "", "", "" };
            var list = www6.selectSP("sp_mobile_attendance_list_f", op1, op2).DataTableToList<sp_mobile_attendance_list_fResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void CheckId( HttpContext context )
    {
        JsonWriter result = new JsonWriter();
        result.success = true;

        using (FrontDataContext dao = new FrontDataContext())
        {
            var idx = context.Request["idx"].ValueIfNull("-1");

            var userInfo = new UserInfo();
            var user_id = userInfo.UserId;

            //var list = dao.mobile_attendance_user.Any(p => p.mu_ma_id == idx && p.UserID == user_id);
            var exist = www6.selectQ<mobile_attendance_user>("mu_ma_id", Convert.ToInt32(idx), "UserID", user_id);
            if (exist.Any())
            {
                var entity = www6.selectQ<mobile_attendance_user>("mu_ma_id", idx, "UserID", user_id).Any();
                result.data = entity;
            }

        }
        JsonWriter.Write(result, context);
    }

    void Add( HttpContext context ) {

        JsonWriter result = new JsonWriter() {
            success = false
        };

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        var idx = Convert.ToInt32(context.Request["idx"].ValueIfNull("-1"));
        var s_idx = context.Request["s_id"];

        var content = context.Request["content"].EmptyIfNull();
        if(idx == -1) {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            var sess = new UserInfo();

            var user_id = sess.UserId;
            var user_name = sess.UserName;
            var user_email = sess.Email;
            var user_birth = sess.Birth;
            var user_addr = "";
            var user_phone = "";


            if (sess.LocationType == "국내")
            {
                var addr_result = new SponsorAction().GetAddress();
                if (addr_result.success)
                {
                    SponsorAction.AddressResult addr_data = (SponsorAction.AddressResult)addr_result.data;
                    user_addr = "(" + addr_data.Zipcode + ")" + addr_data.Addr1 + " " + addr_data.Addr2;
                }
                var comm_result = new SponsorAction().GetCommunications();
                if (comm_result.success)
                {
                    SponsorAction.CommunicationResult comm_data = (SponsorAction.CommunicationResult)comm_result.data;
                    user_phone = comm_data.Mobile.TranslatePhoneNumber();
                }
            }


            // 출석체크 등록
            //dao.sp_mobile_attendance_user_insert_f(idx.ToString(), user_id, user_name, user_birth.Encrypt(), user_phone.Encrypt(), user_email.Encrypt(), user_addr.Encrypt());
            Object[] op1 = new Object[] { "mu_ma_id", "UserID", "userName", "userBirth", "userPhone", "userEmail", "userAddr" };
            Object[] op2 = new Object[] { idx.ToString(), user_id, user_name, user_birth.Encrypt(), user_phone.Encrypt(), user_email.Encrypt(), user_addr.Encrypt() };
            www6.selectSP("sp_mobile_attendance_user_insert_f", op1, op2);

            // 스템프 발급
            //dao.sp_stamp_user_insert_f(s_idx, user_id, user_name);
            Object[] op3 = new Object[] { "s_id", "userID", "userName" };
            Object[] op4 = new Object[] { s_idx, user_id, user_name };
            www6.selectSP("sp_stamp_user_insert_f", op3, op4);

        }

        result.success = true;
        JsonWriter.Write(result, context);
    }



}