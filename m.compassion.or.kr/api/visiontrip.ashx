﻿<%@ WebHandler Language="C#" Class="api_visiontrip" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_visiontrip : BaseHandler
{
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "list") {
            this.GetList(context);
        }
        else if (t == "schedulelist")
        {
            this.GetScheduleList(context);
        }
        else if(t == "get-years") {
            this.GetYears(context);
        }
    }

    // PayItemSession 에 값이 있는 경우만 가능
    void GetList(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var type = context.Request["type"].EmptyIfNull();
        var year = context.Request["year"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_visiontrip_list_f(1, 9999, type, year).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "year" };
            Object[] op2 = new Object[] { 1, 9999, type, year };
            var list = www6.selectSP("sp_visiontrip_list_f", op1, op2).DataTableToList<sp_visiontrip_list_fResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    // PayItemSession 에 값이 있는 경우만 가능
    void GetScheduleList(HttpContext context)
    {

        JsonWriter result = new JsonWriter();
        result.success = true;

        var type = context.Request["type"].EmptyIfNull();
        var year = context.Request["year"].EmptyIfNull();
        var requestkey = context.Request["requestkey"].EmptyIfNull();
        var userid = context.Request["userid"].EmptyIfNull();

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_tVisionTripSchedule_list_f(1, 9999, type, year, requestkey, userid).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "type", "year", "requestkey", "userid" };
            Object[] op2 = new Object[] { 1, 9999, type, year, requestkey, userid };
            var list = www6.selectSP("sp_tVisionTripSchedule_list_f", op1, op2).DataTableToList<sp_tVisionTripSchedule_list_fResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

    void GetYears(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_visiontrip_years_f().ToList();
            Object[] op1 = new Object[] {  };
            Object[] op2 = new Object[] {  };
            var list = www6.selectSP("sp_visiontrip_years_f", op1, op2).DataTableToList<sp_visiontrip_years_fResult>();

            result.data = list;
        }
        JsonWriter.Write(result, context);
    }

}