﻿<%@ WebHandler Language="C#" Class="api_sympathy_reply" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;


public class api_sympathy_reply : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest(HttpContext context) {

        var t = context.Request["t"].EmptyIfNull();

        if (t == "list") {
            this.GetList(context);
        } else if (t == "remove") {
            this.Remove(context);
        } else if (t == "update") {
            this.Update(context);
        } else if(t == "add") {
            this.Add(context);
        }
    }

    public class sp_comment_list_fResultEx : sp_comment_list_fResult {
        public bool is_owner { get; set; }
    }

    void GetList(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        UserInfo sess = new UserInfo();
        var userId = sess.UserId;

        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));
        var type = context.Request["type"].EmptyIfNull();

        if (id == -1) {
            result.success = false;
            result.message = "error";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            //var list = dao.sp_comment_list_f(page, rowsPerPage, type, id, -1).ToList();
            Object[] op1 = new Object[] { "page", "rowsPerPage", "c_ref_name", "c_ref_id", "c_group" };
            Object[] op2 = new Object[] { page, rowsPerPage, type, id, -1 };
            var list = www6.selectSP("sp_comment_list_f", op1, op2).DataTableToList<sp_comment_list_fResult>();

            List<sp_comment_list_fResultEx> data = new List<sp_comment_list_fResultEx>();
            foreach (var entity in list)
            {

                data.Add(new sp_comment_list_fResultEx()
                {
                    total = entity.total,
                    rownum = entity.rownum,
                    c_id = entity.c_id,
                    c_group = entity.c_group,
                    c_group2 = entity.c_group2,
                    c_ref_id = entity.c_ref_id,
                    c_ref_name = entity.c_ref_name,
                    c_user_name = entity.c_user_name,
                    c_content = entity.c_content,
                    c_deleted = entity.c_deleted,
                    c_a_id = entity.c_a_id,
                    is_owner = entity.c_user_id == userId,
                    c_user_id = entity.c_user_id.Mask("*", 3),
                    c_regdate = entity.c_regdate
                });
            }
            result.data = data;
        }
        JsonWriter.Write(result, context);
    }



    void Remove(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }


        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        if (id == -1) {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }



        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();
            var exist = www6.selectQ<comment>("c_id", id, "c_user_id", userInfo.UserId);
            //if (!dao.comment.Any(p => p.c_id == id && p.c_user_id == userInfo.UserId))
            if(!exist.Any())
            {
                result.success = false;
                result.message = "올바른 접근이 아닙니다.";
                JsonWriter.Write(result, context);
                return;
            }

            //var entity = dao.comment.First(p => p.c_id == id && p.c_user_id == userInfo.UserId);
            var entity = www6.selectQF<comment>("c_id", id, "c_user_id", userInfo.UserId);
            //dao.comment.DeleteOnSubmit(entity);
            //dao.SubmitChanges();
            www6.delete(entity);
        }

        JsonWriter.Write(result, context);
    }



    void Update(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }


        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var content = context.Request["content"].EmptyIfNull();
        if (id == -1 || content == "") {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();
            var exist = www6.selectQ<comment>("c_id", id, "c_user_id", userInfo.UserId);
            //if (!dao.comment.Any(p => p.c_id == id && p.c_user_id == userInfo.UserId))
            if(!exist.Any())
            {
                result.success = false;
                result.message = "올바른 접근이 아닙니다.";
                JsonWriter.Write(result, context);
                return;
            }


            var entity = exist[0]; //dao.comment.First(p => p.c_id == id && p.c_user_id == userInfo.UserId);
            entity.c_content = content;

            //dao.SubmitChanges();
            www6.update(entity);
        }

        JsonWriter.Write(result, context);
    }


    void Add(HttpContext context) {

        JsonWriter result = new JsonWriter();
        result.success = true;

        if (!UserInfo.IsLogin) {
            result.success = false;
            result.message = "로그인 후 이용가능합니다.";
            JsonWriter.Write(result, context);
            return;
        }


        var id = Convert.ToInt32(context.Request["id"].ValueIfNull("-1"));
        var content = context.Request["content"].EmptyIfNull();
        var group = Convert.ToInt32(context.Request["group"].ValueIfNull("0"));
        var type = context.Request["type"].EmptyIfNull();

        if (id == -1 || content == "" || type == "") {
            result.success = false;
            result.message = "필수정보가 부족합니다.";
            JsonWriter.Write(result, context);
            return;
        }

        using (FrontDataContext dao = new FrontDataContext())
        {
            var userInfo = new UserInfo();

            var entity = new comment()
            {
                c_group = group,
                c_ref_name = type,
                c_ref_id = id,
                c_user_id = userInfo.UserId,
                c_user_name = userInfo.UserName,
                c_content = content,
                c_regdate = DateTime.Now,
                c_deleted = false
            };
            //dao.comment.InsertOnSubmit(entity);
            www6.insert(entity);
            //dao.SubmitChanges();
        }

        JsonWriter.Write(result, context);
    }
}