﻿<%@ WebHandler Language="C#" Class="api_cad" %>

using System;
using System.Web;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Dynamic;
using System.Reflection;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.IO;
using drawing = System.Drawing;
using NLog;
using System.Net;
using CommonLib;
using System.Data;
using Newtonsoft.Json;
using TCPTModel;
using TCPTModel.Request.Hold.Beneficiary;
using TCPTModel.Response.Beneficiary;
using TCPTModel.Response.Hold.Beneficiary;


public class api_cad : BaseHandler {
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public override void OnRequest( HttpContext context ) {

        var t = context.Request["t"].EmptyIfNull();

        if(t == "request") {
            string strdbgp_kind = ConfigurationManager.AppSettings["dbgp_kind"].ToString();
            if(strdbgp_kind == "1") { this.RequestDB(context); }
            else { this.RequestGP(context); }
        } else if(t == "can-request") {
            this.CanRequest(context);
        } else if(t == "can-request-check") {
            this.CanRequestCheck(context);
        } else if(t == "list") {
            this.GetList(context);
        }

    }

    void CanRequest( HttpContext context ) {

        var childKey = context.Request["childKey"].EmptyIfNull().EscapeSqlInjection();      // TCPT : LocalBeneficiaryId
        var globalId = context.Request["globalId"].EmptyIfNull().EscapeSqlInjection();      // TCPT : GlobalId
        JsonWriter.Write(new CADAction().CanRequest(globalId, childKey), context);
    }

    void CanRequestCheck( HttpContext context ) {

        var childKey = context.Request["childKey"].EmptyIfNull().EscapeSqlInjection();      // TCPT : LocalBeneficiaryId
        var globalId = context.Request["globalId"].EmptyIfNull().EscapeSqlInjection();      // TCPT : GlobalId
        JsonWriter.Write(new CADAction().CanRequestCheck(globalId, childKey), context);
    }

    void RequestDB( HttpContext context ) {
        var sess = new UserInfo();
        string test = "";
        var childMasterID = context.Request["globalId"].EmptyIfNull().EscapeSqlInjection();
        var childKey = context.Request["childKey"].EmptyIfNull().EscapeSqlInjection();
        var CampaignID = context.Request["CampaignID"].EmptyIfNull().EscapeSqlInjection();
        //var childKey = context.Request["childKey"].EmptyIfNull().EscapeSqlInjection();      // TCPT : LocalBeneficiaryId
        var globalId = context.Request["globalId"].EmptyIfNull().EscapeSqlInjection();      // TCPT : GlobalId
        var childName = context.Request["childName"].EmptyIfNull().EscapeSqlInjection();
        var msg = context.Request["msg"].EmptyIfNull().EscapeSqlInjection();

        JsonWriter jResult = new JsonWriter() { success = false };

        // 어린이 hold 정보 먼저 업데이트 
        WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();

        // 어린이 hold 정보 조회
        Object[] objParam = new object[] { "ChildMasterID" };
        Object[] objValue = new object[] { childMasterID };
        Object[] objSql = new object[] { "sp_S_ChildHoldStatus" };
        DataTable dt = _www6Service.NTx_ExecuteQuery("SqlCompass4", objSql, "SP", objParam, objValue).Tables[0];

        // 어린이 홀드 정보 조회 오류
        if (dt == null || dt.Rows.Count == 0)
        {
            //base.AlertWithJavascript("어린이 정보 조회에 실패했습니다.");
            ErrorLog.Write(HttpContext.Current, 0, "어린이 정보 조회에 실패했습니다.");
            //return false;
        }

        DataRow dr = dt.Rows[0];

        #region | HOLD |
        // Hold Update
        string endDT = DateTime.Now.AddDays(8).ToString("yyyy-MM-dd");
        bool updateHold = false;
        Guid newHoldUID = Guid.NewGuid();
        try
        {
            TCPTService.Service svc = new TCPTService.Service();
            svc.Timeout = 180000;

            TCPTModel.Request.Hold.Beneficiary.BeneficiaryHoldRequestList hKit = new TCPTModel.Request.Hold.Beneficiary.BeneficiaryHoldRequestList();
            hKit.Beneficiary_GlobalID = dr["Beneficiary_GlobalID"].ToString();
            hKit.BeneficiaryState = "Consignment Hold";
            hKit.HoldEndDate = endDT + "T23:59:59Z";
            hKit.IsSpecialHandling = false;
            hKit.PrimaryHoldOwner = sess.UserId;
            hKit.GlobalPartner_ID = "KR";
            hKit.HoldID = dr["HoldID"].ToString();
            string HoldUID = dr["HoldUID"].ToString();

            string hJson = Newtonsoft.Json.JsonConvert.SerializeObject(hKit);
            string result = svc.BeneficiaryHoldSingle_PUT(hKit.Beneficiary_GlobalID, hKit.HoldID, hJson);
            TCPTModel.TCPTResponseMessage updateResult = Newtonsoft.Json.JsonConvert.DeserializeObject<TCPTModel.TCPTResponseMessage>(result);
            if (updateResult.IsSuccessStatusCode)
            {
                test += "hold 업데이트 성공 / ";
                test += "기존 hold  expired  처리 시작 / ";
                // 기존 hold  expired  처리
                bool b = svc.UpdateHoldStatus(new Guid(HoldUID), hKit.HoldID, hKit.BeneficiaryState, endDT, "Expired", "2000", "CAD Hold로 Expired 처리", sess.UserId, sess.UserId);

                if (b)
                {
                    test += "기존 hold  expired  처리 성공 / ";
                }
                else
                {
                    test += "기존 hold  expired  처리 실패 / ";
                }
                // 신규  hold insert
                test += "신규 hold insert / ";
                b = svc.InsertHoldHistory(newHoldUID, hKit.HoldID, hKit.Beneficiary_GlobalID, hKit.BeneficiaryState, endDT, sess.UserId, "", "2000", "CAD Hold");
                updateHold = true;
            }
            else
            {
                // hold update error
                test += "Consignment Hold 업데이트 실패 " + updateResult.RequestMessage.ToString() + " / ";
                //base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다. 다른 어린이를 선택 해주세요.");
                jResult.message = "CAD 생성이 실패하였습니다. 다른 어린이를 선택해주세요.";
                jResult.success = false;
                ErrorLog.Write(context, 0, test);
                JsonWriter.Write(jResult, context);
                return;
            }
        }
        catch (Exception ex)
        {
            test += "Consignment Hold 로 업데이트 오류 " + ex.Message + " / ";
            jResult.message = "CAD 생성이 실패하였습니다. 다른 어린이를 선택해주세요.";
            jResult.success = false;
            ErrorLog.Write(context, 0, test);
            JsonWriter.Write(jResult, context);
            //return false;
        }

        if (!updateHold)
        {
            //base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다.");
            jResult.message = "CAD 생성이 실패하였습니다";
            jResult.success = false;
            ErrorLog.Write(context, 0, test);
            JsonWriter.Write(jResult, context);
        }
        #endregion

        #region | 신규 캠페인 저장 |
        try
        {
            // 신규 캠페인에 홀드 정보 insert
            Object[] objParam2 = new object[] { "CampaignID", "ChildMasterID", "RegisterID", "RegisterName", "HoldUID" };
            Object[] objValue2 = new object[] { CampaignID, childMasterID, sess.UserId, sess.UserName, newHoldUID.ToString() };
            Object[] objSql2 = new object[] { "sp_I_CampaignConsign" };
            int n = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2);
        }
        catch (Exception ex)
        {
            //base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다.");
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            //return false;
        }

        try
        {

            // CAD 메시지가 발송되었어요!
            Pushpia.Send("44d7e97fb4694cbe93bd69746d6968c1", "CAD 메시지가 발송되었어요!", "CAD 발송완료! 발송된 CAD 메시지가 유효한 일주일 동안 기도로 함께해 주세요~", "", sess.UserId);

        }
        catch
        {

        }
        #endregion

        //CAD 생성
        JsonWriter.Write(new CADAction().Request(CampaignID, globalId, childKey, childName, msg), context);
    }

    void RequestGP( HttpContext context ) {
        var sess = new UserInfo();
        WWW6Service.SoaHelper _www6Service = new WWW6Service.SoaHelper();
        string test = "";
        var childMasterID = context.Request["globalId"].EmptyIfNull().EscapeSqlInjection();
        var childKey = context.Request["childKey"].EmptyIfNull().EscapeSqlInjection();
        var CampaignID = context.Request["CampaignID"].EmptyIfNull().EscapeSqlInjection();
        var globalId = context.Request["globalId"].EmptyIfNull().EscapeSqlInjection();      // TCPT : GlobalId
        var childName = context.Request["childName"].EmptyIfNull().EscapeSqlInjection();
        var msg = context.Request["msg"].EmptyIfNull().EscapeSqlInjection();

        Guid newHoldUID = Guid.NewGuid();

        JsonWriter jResult = new JsonWriter() { success = false };

        //[이종진]ConsignmentHold 
        #region | 어린이 ConsignmentHold |

        // Hold Update
        string endDT = DateTime.Now.AddDays(8).ToString("yyyy-MM-dd");
        bool updateHold = false;

        try
        {
            // TCPT Hold 처리용 킷 생성
            BeneficiaryHoldRequestList item = new BeneficiaryHoldRequestList();
            item.Beneficiary_GlobalID = childMasterID.Substring(2);
            item.BeneficiaryState = "Consignment Hold";
            item.HoldEndDate = endDT + "T23:59:59Z";
            item.IsSpecialHandling = false;
            item.PrimaryHoldOwner = sess.UserId;
            item.GlobalPartner_ID = "KR";

            string json = JsonConvert.SerializeObject(item);
            string strHoldResult = string.Empty;

            CommonLib.TCPTService.ServiceSoap _OffRampService = new CommonLib.TCPTService.ServiceSoapClient();

            // 홀드요청
            strHoldResult = _OffRampService.BeneficiaryHoldRequest_Single_POST(childMasterID.Substring(2), json);
            TCPTResponseMessage msgResult = JsonConvert.DeserializeObject<TCPTResponseMessage>(strHoldResult);
            //실패
            if (!msgResult.IsSuccessStatusCode)
            {
                // hold update error
                test += "Consignment Hold 실패 " + msgResult.RequestMessage.ToString() + " / ";
                jResult.data = "어린이 나눔펀딩 생성에 실패했습니다. 다른 어린이를 선택 해주세요.";
                jResult.success = false;
                ErrorLog.Write(HttpContext.Current, 0, test);
                jResult.Write(context);
                return;
            }

            // 성공
            BeneficiaryHoldRequestResponse_Kit response = JsonConvert.DeserializeObject<BeneficiaryHoldRequestResponse_Kit>(msgResult.RequestMessage.ToString());
            BeneficiaryHoldResponseList_Kit res = JsonConvert.DeserializeObject<BeneficiaryHoldResponseList_Kit>(msgResult.RequestMessage.ToString());
            response = res.BeneficiaryHoldResponseList[0];

            // 신규  hold insert TCPT_Beneficiary_HOLD
            test += "신규 hold insert / ";
            updateHold = new ChildAction().InsertHoldHistory(newHoldUID, response.HoldID, childMasterID.Substring(2), item.BeneficiaryState, endDT, sess.UserId, sess.UserName, "2000", "나눔펀딩 Hold");

        }
        catch (Exception ex)
        {
            test += "Consignment Hold 생성 오류 " + ex.Message + " / ";
            jResult.data = "어린이 나눔펀딩 생성에 실패했습니다. 다른 어린이를 선택 해주세요.";
            jResult.success = false;
            ErrorLog.Write(HttpContext.Current, 0, test);
            jResult.Write(context);
            return;
        }

        if (!updateHold)
        {
            jResult.data = "어린이 나눔펀딩 생성에 실패했습니다.";
            jResult.success = false;
            ErrorLog.Write(HttpContext.Current, 0, test);
            jResult.Write(context);
            return;
        }

        #endregion

        //[이종진]어린이 정보 insert
        #region | 어린이 정보 insert |

        //beneficiary kit(어린이 상세정보) 조회
        JsonWriter childDetail = new JsonWriter();
        ChildAction childAction = new ChildAction();
        childDetail = childAction.Beneficiary_GET(childMasterID);
        if (!childDetail.success)
        {
            jResult.data = "어린이 상세정보 조회에 실패하였습니다. 다른 어린이를 선택해주세요.";
            jResult.success = false;
            ErrorLog.Write(HttpContext.Current, 0, test);
            jResult.Write(context);
            return;
        }

        //조회 후, 어린이 정보 및 TCPT_Bene 정보 insert
        //신규조회방법일 경우, 초기 ChildMasterID가 "09"+ChildGlobalID임. -> 존재하는 어린이면 존재하는 어린이의 ChildMasterID를 return함
        JsonWriter insertResult = new JsonWriter();
        insertResult = childAction.InsertTCPTBene_tChild(childDetail.data, childMasterID, childKey);
        if (insertResult.success)
        {
            childMasterID = insertResult.data.ToString();
        }
        else
        {
            jResult.data = "어린이 정보 생성이 실패하였습니다. 다른 어린이를 선택해주세요.";
            jResult.success = false;
            jResult.Write(context);
            return;
        }

        #endregion


        #region | 신규 캠페인 저장 |
        try
        {
            // 신규 캠페인에 홀드 정보 insert
            Object[] objParam2 = new object[] { "CampaignID", "ChildMasterID", "RegisterID", "RegisterName", "HoldUID" };
            Object[] objValue2 = new object[] { CampaignID, childMasterID, sess.UserId, sess.UserName, newHoldUID.ToString() };
            Object[] objSql2 = new object[] { "sp_I_CampaignConsign" };
            int n = _www6Service.Tx_ExecuteQuery("SqlCompass4", objSql2, "SP", objParam2, objValue2);
        }
        catch (Exception ex)
        {
            //base.AlertWithJavascript("어린이 나눔펀딩 생성에 실패했습니다.");
            ErrorLog.Write(HttpContext.Current, 0, ex.Message);
            //return false;
        }

        try
        {

            // CAD 메시지가 발송되었어요!
            Pushpia.Send("44d7e97fb4694cbe93bd69746d6968c1", "CAD 메시지가 발송되었어요!", "CAD 발송완료! 발송된 CAD 메시지가 유효한 일주일 동안 기도로 함께해 주세요~", "", sess.UserId);

        }
        catch
        {

        }
        #endregion

        //CAD 생성
        JsonWriter.Write(new CADAction().Request(CampaignID, globalId, childKey, childName, msg), context);
    }

    void GetList( HttpContext context ) {
        var page = Convert.ToInt32(context.Request["page"].ValueIfNull("1"));
        var rowsPerPage = Convert.ToInt32(context.Request["rowsPerPage"].ValueIfNull("10"));

        JsonWriter.Write(new CADAction().GetList(page, rowsPerPage), context);
    }


}